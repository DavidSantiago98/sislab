VERSION 5.00
Begin VB.Form FormStkArtigos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormStkArtigos"
   ClientHeight    =   4620
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7155
   Icon            =   "FormStkArtigos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4620
   ScaleWidth      =   7155
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcReferencia 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4350
      TabIndex        =   2
      Top             =   120
      Width           =   1425
   End
   Begin VB.CommandButton BtPesqTipo 
      Height          =   315
      Left            =   5760
      Picture         =   "FormStkArtigos.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
      Top             =   840
      Width           =   375
   End
   Begin VB.TextBox EcDescrTipo 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   1560
      Locked          =   -1  'True
      TabIndex        =   29
      Top             =   840
      Width           =   4215
   End
   Begin VB.TextBox EcCodTipo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   990
      MaxLength       =   4
      TabIndex        =   4
      Top             =   840
      Width           =   615
   End
   Begin VB.Frame Frame2 
      Height          =   795
      Left            =   120
      TabIndex        =   12
      Top             =   3720
      Width           =   6885
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   18
         Top             =   480
         Width           =   2295
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   17
         Top             =   195
         Width           =   2055
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   16
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   15
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4200
      TabIndex        =   11
      Top             =   4680
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   10
      Top             =   4680
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4200
      TabIndex        =   9
      Top             =   5040
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   8
      Top             =   5040
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   990
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   120
      Width           =   1425
   End
   Begin VB.TextBox EcDescricao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   990
      TabIndex        =   3
      Top             =   480
      Width           =   4755
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2130
      ItemData        =   "FormStkArtigos.frx":0596
      Left            =   120
      List            =   "FormStkArtigos.frx":0598
      TabIndex        =   7
      Top             =   1560
      Width           =   6855
   End
   Begin VB.TextBox EcHrCri 
      Enabled         =   0   'False
      Height          =   285
      Left            =   6480
      TabIndex        =   6
      Top             =   4680
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.TextBox EcHrAct 
      Enabled         =   0   'False
      Height          =   285
      Left            =   6480
      TabIndex        =   0
      Top             =   5040
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.Label Label3 
      Caption         =   "Refer�ncia"
      Height          =   255
      Index           =   1
      Left            =   3360
      TabIndex        =   32
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label12 
      Caption         =   "Tipo"
      Height          =   255
      Left            =   5280
      TabIndex        =   31
      Top             =   1320
      Width           =   855
   End
   Begin VB.Label Label4 
      Caption         =   "Tipo"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   30
      Top             =   840
      Width           =   615
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   2700
      TabIndex        =   28
      Top             =   1320
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   120
      TabIndex        =   27
      Top             =   1320
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   26
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   120
      TabIndex        =   25
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2760
      TabIndex        =   24
      Top             =   5040
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2880
      TabIndex        =   23
      Top             =   4680
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   120
      TabIndex        =   22
      Top             =   5040
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   21
      Top             =   4680
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcHrCri"
      Height          =   255
      Index           =   0
      Left            =   5160
      TabIndex        =   20
      Top             =   4680
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "EcHrAct"
      Height          =   255
      Index           =   1
      Left            =   5160
      TabIndex        =   19
      Top             =   5040
      Visible         =   0   'False
      Width           =   1215
   End
End
Attribute VB_Name = "FormStkArtigos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub
Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub

Private Sub EcDescricao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDescricao_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Artigo"
    Me.left = 540
    Me.top = 450
    Me.Width = 7245
    Me.Height = 5040 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = gBD_PREFIXO_TAB & "stk_artigo"
    Set CampoDeFocus = EcDescricao
    
    NumCampos = 10
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "codigo"
    CamposBD(1) = "descricao"
    CamposBD(2) = "user_cri"
    CamposBD(3) = "dt_cri"
    CamposBD(4) = "user_act"
    CamposBD(5) = "dt_act"
    CamposBD(6) = "hr_cri"
    CamposBD(7) = "hr_act"
    CamposBD(8) = "cod_tipo"
    CamposBD(9) = "referencia"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcDescricao
    Set CamposEc(2) = EcUtilizadorCriacao
    Set CamposEc(3) = EcDataCriacao
    Set CamposEc(4) = EcUtilizadorAlteracao
    Set CamposEc(5) = EcDataAlteracao
    Set CamposEc(6) = EcHrCri
    Set CamposEc(7) = EcHrAct
    Set CamposEc(8) = EcCodTipo
    Set CamposEc(9) = EcReferencia
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    'TextoCamposObrigatorios(0) = "C�digo"
    TextoCamposObrigatorios(1) = "Descri��o"
    TextoCamposObrigatorios(8) = "Tipo de Artigo"
    TextoCamposObrigatorios(9) = "Refer�ncia"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "codigo"
    Set ChaveEc = EcCodigo
    
    CamposBDparaListBox = Array("codigo", "descricao", "referencia")
    NumEspacos = Array(22, 32, 40)
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormStkArtigos = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    EcDescrTipo = ""
    
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_act", EcDataAlteracao, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_cri", EcDataAlteracao, mediTipoHora
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao & " " & EcHrCri
        LaDataAlteracao = EcDataAlteracao & " " & EcHrAct
        eccodtipo_Validate False
    End If
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY codigo ASC, descricao ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        Call BG_Mensagem(mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, " Procurar")
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        EcHrCri = Bg_DaHora_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcCodigo = BG_DaMAX(NomeTabela, "codigo") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        EcHrAct = Bg_DaHora_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
        
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant

    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE codigo=" & EcCodigo
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub




Private Sub eccodtipo_Validate(Cancel As Boolean)
    
    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset
    
    EcCodTipo.Text = UCase(EcCodTipo.Text)
    If Trim(EcCodTipo) <> "" Then
        
        sql = "SELECT " & _
              "     descricao " & _
              "FROM " & _
              gBD_PREFIXO_TAB & "stk_tipo_artigo " & _
              "WHERE " & _
              "     codigo=" & (EcCodTipo)
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        
        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Tipo Artigo inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrTipo = ""
            EcCodTipo = ""
        Else
            EcDescrTipo.Text = rsCodigo!descricao
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrTipo.Text = ""
        EcCodTipo = ""
    End If
    
End Sub


Private Sub BtPesqTipo_Click()
    


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "codigo"
    CamposEcran(1) = "codigo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descricao"
    CamposEcran(2) = "descricao"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_stk_tipo_Artigo"
    CampoPesquisa1 = "descricao"
    ClausulaWhere = "  "
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tipos de Artigos")
    
    mensagem = "N�o foi encontrada nenhum Tipo de Artigo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodTipo.Text = resultados(1)
            EcDescrTipo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
End Sub



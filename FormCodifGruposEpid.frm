VERSION 5.00
Begin VB.Form FormCodifGruposEpid 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   7755
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   6720
   Icon            =   "FormCodifGruposEpid.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7755
   ScaleWidth      =   6720
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkNumAdmissoesInst 
      Caption         =   "N� Admiss�es Instit."
      Height          =   255
      Left            =   4680
      TabIndex        =   50
      Top             =   3360
      Width           =   1815
   End
   Begin VB.CheckBox CkDataGeraExcel 
      Caption         =   "Data Gera��o Excel"
      Height          =   255
      Left            =   2640
      TabIndex        =   49
      Top             =   3360
      Width           =   1815
   End
   Begin VB.TextBox EcTipoNotif 
      Height          =   285
      Left            =   3360
      TabIndex        =   48
      Top             =   6720
      Width           =   615
   End
   Begin VB.ComboBox CbTipoNotificacao 
      Height          =   315
      Left            =   4200
      Style           =   2  'Dropdown List
      TabIndex        =   47
      Top             =   1440
      Width           =   2055
   End
   Begin VB.CheckBox CkProcesso 
      Caption         =   "Processo Hosp."
      Height          =   255
      Left            =   360
      TabIndex        =   45
      Top             =   3360
      Width           =   2055
   End
   Begin VB.CheckBox CkInfCompl 
      Caption         =   "Inf. Complementar"
      Height          =   255
      Left            =   4680
      TabIndex        =   44
      Top             =   3120
      Width           =   2055
   End
   Begin VB.CheckBox CkIdade 
      Caption         =   "Idade"
      Height          =   255
      Left            =   4680
      TabIndex        =   43
      Top             =   2880
      Width           =   2055
   End
   Begin VB.TextBox EcDescrInstituicao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1200
      TabIndex        =   42
      Top             =   1080
      Width           =   5055
   End
   Begin VB.CheckBox CkDtInt 
      Caption         =   "Data Internamento"
      Height          =   255
      Left            =   2640
      TabIndex        =   40
      Top             =   2880
      Width           =   2055
   End
   Begin VB.CheckBox CkPt 
      Caption         =   "C�digo PT"
      Height          =   255
      Left            =   4680
      TabIndex        =   39
      Top             =   2160
      Width           =   1335
   End
   Begin VB.CheckBox CkProduto 
      Caption         =   "Produto"
      Height          =   255
      Left            =   2640
      TabIndex        =   38
      Top             =   3120
      Width           =   2055
   End
   Begin VB.TextBox EcEmail 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1200
      TabIndex        =   35
      Top             =   720
      Width           =   5055
   End
   Begin VB.TextBox EcIdLab 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1200
      TabIndex        =   34
      Top             =   1440
      Width           =   1215
   End
   Begin VB.CheckBox CkEnvioEmail 
      Caption         =   "Envio por Email"
      Height          =   255
      Left            =   4680
      TabIndex        =   32
      Top             =   2400
      Width           =   2415
   End
   Begin VB.CheckBox CkAvaliaContinua 
      Caption         =   "Avalia��o Continua"
      Height          =   255
      Left            =   4680
      TabIndex        =   31
      Top             =   2640
      Width           =   2415
   End
   Begin VB.CheckBox CkNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   360
      TabIndex        =   30
      Top             =   2160
      Width           =   1935
   End
   Begin VB.CheckBox CkReq 
      Caption         =   "Requisi��o"
      Height          =   255
      Left            =   360
      TabIndex        =   29
      Top             =   2400
      Width           =   1935
   End
   Begin VB.CheckBox CkData 
      Caption         =   "Data Requisi��o"
      Height          =   255
      Left            =   360
      TabIndex        =   28
      Top             =   2640
      Width           =   2055
   End
   Begin VB.CheckBox CkServ 
      Caption         =   "Servi�o"
      Height          =   255
      Left            =   360
      TabIndex        =   27
      Top             =   2880
      Width           =   2055
   End
   Begin VB.CheckBox CkTipoEpis 
      Caption         =   "Tipo Epis�dio"
      Height          =   255
      Left            =   360
      TabIndex        =   26
      Top             =   3120
      Width           =   2055
   End
   Begin VB.CheckBox CkDtNasc 
      Caption         =   "Data Nascimento"
      Height          =   255
      Left            =   2640
      TabIndex        =   25
      Top             =   2160
      Width           =   1935
   End
   Begin VB.CheckBox CkSexo 
      Caption         =   "Sexo Utente"
      Height          =   255
      Left            =   2640
      TabIndex        =   24
      Top             =   2400
      Width           =   2055
   End
   Begin VB.CheckBox CkSNS 
      Caption         =   "N�mero SNS"
      Height          =   255
      Left            =   2640
      TabIndex        =   23
      Top             =   2640
      Width           =   2055
   End
   Begin VB.CheckBox CkInvisivel 
      Appearance      =   0  'Flat
      Caption         =   "Item Cancelado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   4320
      TabIndex        =   12
      Top             =   400
      Width           =   1935
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   18
      Top             =   6840
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4440
      TabIndex        =   17
      Top             =   7920
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   16
      Top             =   7560
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4440
      TabIndex        =   15
      Top             =   7560
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcHoraAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4860
      TabIndex        =   14
      Top             =   7920
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcHoraCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4860
      TabIndex        =   13
      Top             =   7560
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   135
      TabIndex        =   5
      Top             =   5280
      Width           =   6405
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   195
         Width           =   675
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   480
         Width           =   705
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   9
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   8
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   7
         Top             =   195
         Width           =   2175
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   6
         Top             =   480
         Width           =   2175
      End
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      Height          =   1395
      Left            =   120
      TabIndex        =   4
      Top             =   3840
      Width           =   6500
   End
   Begin VB.TextBox EcDescricao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3000
      TabIndex        =   1
      Top             =   120
      Width           =   3255
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1200
      TabIndex        =   0
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label3 
      Caption         =   "Tipo de Notifica��o"
      Height          =   255
      Index           =   4
      Left            =   2520
      TabIndex        =   46
      Top             =   1440
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "Institui��o"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   41
      Top             =   1080
      Width           =   855
   End
   Begin VB.Label Label6 
      Caption         =   "Email"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   37
      Top             =   720
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Identifica��o"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   36
      Top             =   1440
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "Dados Apresentar"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   33
      Top             =   1920
      Width           =   1695
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   375
      Left            =   120
      TabIndex        =   22
      Top             =   7560
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label11 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   375
      Left            =   0
      TabIndex        =   21
      Top             =   6840
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataCriacao"
      Height          =   375
      Left            =   3120
      TabIndex        =   20
      Top             =   7560
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label13 
      Caption         =   "EcDataAlteracao"
      Height          =   375
      Left            =   3000
      TabIndex        =   19
      Top             =   7920
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o"
      Height          =   255
      Index           =   0
      Left            =   2040
      TabIndex        =   3
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "FormCodifGruposEpid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 24/04/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos

'BRUNODSANTOS 23.02.2017 - Glintt-HS-14510
Dim s_email As String
Dim s_aux As String
'
Public rs As ADODB.recordset

'BRUNODSANTOS 23.02.2017 - Glintt-HS-14510
Private Sub CbTipoNotificacao_Change()
    
        
    If CbTipoNotificacao.ListIndex <> mediComboValorNull Then
        If CbTipoNotificacao.ListIndex = tipo_notificacao.email Then 'EMAIL
            EcEmail.Enabled = True
            EcEmail.BackColor = &H80000005
            CkEnvioEmail.Enabled = True
        Else
            EcEmail.text = ""
            EcEmail.Enabled = False
            EcEmail.BackColor = &H8000000F
            CkEnvioEmail.value = vbUnchecked
            CkEnvioEmail.Enabled = False
            CkDataGeraExcel.Enabled = False
            CkNumAdmissoesInst.Enabled = False
        End If
    End If
    
End Sub

'BRUNODSANTOS 23.02.2017 - Glintt-HS-14510
Private Sub CbTipoNotificacao_Click()
        
       If CbTipoNotificacao.ListIndex <> mediComboValorNull Then
        If CbTipoNotificacao.ListIndex = tipo_notificacao.email Then 'EMAIL
            EcEmail.Enabled = True
            EcEmail.BackColor = &H80000005
            CkEnvioEmail.Enabled = True
            'edgar.parada Glintt-HS-19848
            EcTipoNotif = CbTipoNotificacao.ListIndex
            '
        Else
            EcEmail.text = ""
            EcEmail.Enabled = False
            EcEmail.BackColor = &H8000000F
            CkEnvioEmail.value = vbUnchecked
            CkEnvioEmail.Enabled = False
            CkDataGeraExcel.Enabled = False
            CkNumAdmissoesInst.Enabled = False
            'edgar.parada Glintt-HS-19848
            EcTipoNotif = CbTipoNotificacao.ListIndex
            '
        End If
    End If
    
End Sub

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If

End Sub

Private Sub EcCodigo_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodigo_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.text = UCase(EcCodigo.text)

End Sub

Private Sub EcDescricao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDescricao_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Codifica��o de Grupos Epidemiol�gicos"
    Me.left = 50
    Me.top = 50
    Me.Width = 6900
    Me.Height = 6600 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_cod_gr_epid"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 31
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "cod_gr_epid"
    CamposBD(1) = "descr_gr_epid"
    CamposBD(2) = "id_lab"
    CamposBD(3) = "user_cri"
    CamposBD(4) = "dt_cri"
    CamposBD(5) = "email_to"
    CamposBD(6) = "user_act"
    CamposBD(7) = "dt_act"
    CamposBD(8) = "flg_invisivel"
    CamposBD(9) = "flg_envio_email"
    CamposBD(10) = "flg_avaliacao_continua"
    CamposBD(11) = "hr_Cri"
    CamposBD(12) = "hr_act"
    CamposBD(13) = "flg_nome"
    CamposBD(14) = "flg_req"
    CamposBD(15) = "flg_dt_chega"
    CamposBD(16) = "flg_proven"
    CamposBD(17) = "flg_t_sit"
    CamposBD(18) = "flg_dt_nasc"
    CamposBD(19) = "flg_sexo"
    CamposBD(20) = "flg_sns"
    CamposBD(21) = "descr_instituicao"
    CamposBD(22) = "flg_prod"
    CamposBD(23) = "flg_pt"
    CamposBD(24) = "flg_dt_int"
    CamposBD(25) = "flg_Idade"
    CamposBD(26) = "flg_inf_complementar"
    CamposBD(27) = "flg_processo"
    'BRUNODSANTOS CHVNG-7389 - 02.06.2016
    CamposBD(28) = "flg_data_geracao"
    CamposBD(29) = "flg_n_admissoes_inst"
    '
    'BRUNODSANTOS 23.02.2017 - Glintt-HS-14510
    CamposBD(30) = "tipo_notificacao"
    '
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcDescricao
    Set CamposEc(2) = EcIdLab
    Set CamposEc(3) = EcUtilizadorCriacao
    Set CamposEc(4) = EcDataCriacao
    Set CamposEc(5) = EcEmail
    Set CamposEc(6) = EcUtilizadorAlteracao
    Set CamposEc(7) = EcDataAlteracao
    Set CamposEc(8) = CkInvisivel
    Set CamposEc(9) = CkEnvioEmail
    Set CamposEc(10) = CkAvaliaContinua
    Set CamposEc(11) = EcHoraCriacao
    Set CamposEc(12) = EcHoraAlteracao
    Set CamposEc(13) = CkNome
    Set CamposEc(14) = CkReq
    Set CamposEc(15) = CkData
    Set CamposEc(16) = CkServ
    Set CamposEc(17) = CkTipoEpis
    Set CamposEc(18) = CkDtNasc
    Set CamposEc(19) = CkSexo
    Set CamposEc(20) = CkSNS
    Set CamposEc(21) = EcDescrInstituicao
    Set CamposEc(22) = CkProduto
    Set CamposEc(23) = CkPt
    Set CamposEc(24) = CkDtInt
    Set CamposEc(25) = CkIdade
    Set CamposEc(26) = CkInfCompl
    Set CamposEc(27) = CkProcesso
    'BRUNODSANTOS CHVNG-7389 - 02.06.2016
    Set CamposEc(28) = CkDataGeraExcel
    Set CamposEc(29) = CkNumAdmissoesInst
    '
    'BRUNODSANTOS 23.02.2017 - Glintt-HS-14510
    'Set CamposEc(30) = CbTipoNotificacao
    Set CamposEc(30) = EcTipoNotif
    '
     
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Descri��o do Grupo"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_gr_epid"
    Set ChaveEc = EcCodigo
    
    
    
    CamposBDparaListBox = Array("cod_gr_epid", "descr_gr_epid")
    'CamposBDparaListBox = Array("cod_postal", "area_geografica")
    NumEspacos = Array(13, 100)

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormCodifGruposEpid = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    CkInvisivel.value = vbGrayed
    CkAvaliaContinua.value = vbGrayed
    CkEnvioEmail.value = vbGrayed
    CkNome.value = vbGrayed
    CkReq.value = vbGrayed
    CkData.value = vbGrayed
    CkServ.value = vbGrayed
    CkTipoEpis.value = vbGrayed
    CkDtNasc.value = vbGrayed
    CkSexo.value = vbGrayed
    CkSNS.value = vbGrayed
    CkProduto.value = vbGrayed
    CkPt.value = vbGrayed
    CkDtInt.value = vbGrayed
    CkIdade.value = vbGrayed
    CkInfCompl.value = vbGrayed
    CkProcesso.value = vbGrayed
    'BRUNODSANTOS CHVNG-7389 - 02.06.2016
    CkDataGeraExcel.value = vbGrayed
    CkNumAdmissoesInst.value = vbGrayed
    '
    CbTipoNotificacao.ListIndex = mediComboValorNull
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
    
    

End Sub

Sub PreencheValoresDefeito()
    
    DoEvents
    CkInvisivel.value = vbGrayed
    CkAvaliaContinua.value = vbGrayed
    CkEnvioEmail.value = vbGrayed
    CkNome.value = vbGrayed
    CkReq.value = vbGrayed
    CkData.value = vbGrayed
    CkServ.value = vbGrayed
    CkTipoEpis.value = vbGrayed
    CkDtNasc.value = vbGrayed
    CkSexo.value = vbGrayed
    CkSNS.value = vbGrayed
    CkProduto.value = vbGrayed
    CkPt.value = vbGrayed
    CkDtInt.value = vbGrayed
    CkIdade.value = vbGrayed
    CkInfCompl.value = vbGrayed
    CkProcesso.value = vbGrayed
    'BRUNODSANTOS CHVNG-7389 - 02.06.2016
    CkDataGeraExcel.value = vbGrayed
    CkNumAdmissoesInst.value = vbGrayed
    '
    'BRUNODSANTOS 23.02.2017 - Glintt-HS-14510
    EcEmail.Enabled = False
    CbTipoNotificacao.Clear
    
    'CbTipoNotificacao.AddItem "Email"
    'CbTipoNotificacao.ItemData(CbTipoNotificacao.NewIndex) = tipo_notificacao.email
    'CbTipoNotificacao.AddItem "SINAVE"
    'CbTipoNotificacao.ItemData(CbTipoNotificacao.NewIndex) = tipo_notificacao.ws_sinave
   
    BG_PreencheComboBD_ADO "sl_tbf_notif_snv", "cod_notif", "descr_notif", CbTipoNotificacao
    '
    
    
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        If CkInvisivel.value = vbChecked Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao & EcHoraCriacao
        LaDataAlteracao = EcDataAlteracao & EcHoraAlteracao
        'PreencheListaAntib EcCodigo.text
        CbTipoNotificacao.ListIndex = CInt(EcTipoNotif.text)
    End If
    
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    Dim str_aux1 As String
    Dim str_aux2 As String
    Dim str_aux3 As String
    
    On Error GoTo Trata_Erro    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
               
    CriterioTabela = ConstroiCriterio_SELECT(NomeTabela, CamposBD, CamposEc, SelTotal)

              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        
        BG_PreencheListBoxMultipla_ADO EcLista, _
                                       rs, _
                                       CamposBDparaListBox, _
                                       NumEspacos, _
                                       CamposBD, _
                                       CamposEc, _
                                       "SELECT"
        
        ' Fim do preenchimento de 'EcLista'
        CbTipoNotificacao.ListIndex = CInt(EcTipoNotif.text)
      
        
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
        
    End If
    Exit Sub
Trata_Erro:
    BG_LogFile_Erros "Erro ao procurar: -> " & Err.Description, "FormCodifGruposEpid", "FruncaoProcurar"
    Exit Sub
    Resume Next	   
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = CDate(Bg_DaData_ADO)
        EcHoraCriacao = CDate(Bg_DaHora_ADO)
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    EcCodigo.text = BL_HandleNull(BG_DaMAX(NomeTabela, ChaveBD), 0) + 1
    
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    'If GravaAntibioticos(EcCodigo.text) = False Then
    '    Exit Sub
    'End If
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = CDate(Bg_DaData_ADO)
        EcHoraAlteracao = Bg_DaHora_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        EcHoraAlteracao = CDate(Bg_DaHora_ADO)
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    'If GravaAntibioticos(EcCodigo.text) = False Then
    '    Exit Sub
    'End If
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal

End Sub

Sub FuncaoRemover()
    Dim iRes As Boolean
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer cancelar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        CkInvisivel.value = vbChecked
        EcDataAlteracao = CDate(Bg_DaData_ADO & " " & Bg_DaHora_ADO)
        EcHoraAlteracao = CDate(Bg_DaHora_ADO)
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If
    
End Sub
Function ConstroiCriterio_SELECT(NomeTabela As String, _
                                 NomesCampo As Variant, _
                                 NomesControl As Variant, _
                                 SelTotal As Boolean) As String
    
    Dim nTipo As Integer
    Dim i As Integer
    Dim nPassou As Integer
    Dim iCount As Integer
    Dim res As Integer
    Dim iTempDecimal As Integer
    Dim Criterio, criterioSelect As String
    Dim sTempDecimal As String
    Dim ValorCampo, ValorCampo1, ValorControl As Variant
    Dim pos As Integer

    i = 0
    iCount = 0
    nPassou = 0
    
    'cria a instru�ao de SELECT
    criterioSelect = "SELECT "
    For Each ValorCampo1 In NomesCampo
      criterioSelect = criterioSelect & ValorCampo1 & ", "
    Next
    criterioSelect = Mid(criterioSelect, 1, Len(criterioSelect) - 2)
    Criterio = criterioSelect & " FROM " & NomeTabela & " WHERE "
    'Tratar e retirar o conteudo dos campos para criar o criterio
    For Each ValorCampo In NomesCampo
        'TextBox, Label, MaskEdBox
        If TypeOf NomesControl(i) Is TextBox Or TypeOf NomesControl(i) Is Label Or TypeOf NomesControl(i) Is MaskEdBox Then
            ValorControl = NomesControl(i)
            ValorControl = BG_CvPlica(ValorControl)
        ElseIf TypeOf NomesControl(i) Is RichTextBox Then
        'RichtTextBox: Se estiver vazia (o TEXT) n�o inclui no crit�rio
        '(caso contr�rio entrava na query a formata��o do texto vazio!)
            If Trim(NomesControl(i).Text) <> "" Then
                ValorControl = NomesControl(i)
                ValorControl = BG_CvPlica(ValorControl)
            Else
                ValorControl = ""
            End If
        'DataCombo
        ElseIf TypeOf NomesControl(i) Is DataCombo Then
            ValorControl = BG_DataComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        'ComboBox
        ElseIf TypeOf NomesControl(i) Is ComboBox Then
            ValorControl = BG_DaComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        'CheckBox
        ElseIf TypeOf NomesControl(i) Is CheckBox Then
            If NomesControl(i).value = 0 Then
                ValorControl = "0"
            ElseIf NomesControl(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        ' Nada ?
        Else
        End If
        
        'se o campo do form esta vazio nao entra no criterio
        If ValorControl = "" Then
            iCount = iCount + 1
        'se o campo do form tem conteudo, formata o conteudo para o statment SQL
        Else
            'controlar o primeiro campo para nao levar virgula
            If nPassou <> 0 Then
                Criterio = Criterio & " AND "
            End If

            If Trim(NomesControl(i).Tag) = "" Then
                'retirar o tipo de campo da base de dados
                BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                nTipo = gFieldObjectProperties(0).tipo
            Else
                'caso j� esteja defenido
                pos = InStr(NomesControl(i).Tag, "(")
                If pos <> 0 Then
                    'tratar o decimal
                    nTipo = CInt(Mid(NomesControl(i).Tag, 1, pos - 1))
                Else
                    nTipo = CInt(NomesControl(i).Tag)
                End If
                
                'verificar se n�o se trata duma defini��o local
                If nTipo = mediTipoCapitalizado Or nTipo = mediTipoData _
                   Or nTipo = mediTipoDefeito Or nTipo = mediTipoHora _
                   Or nTipo = mediTipoMaiusculas Or nTipo = mediTipoMaiusculas _
                   Or nTipo = mediTipoMinusculas Then
                    BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                    nTipo = gFieldObjectProperties(0).tipo
                End If
            End If

        
            Select Case nTipo
            'numericos: adDecimal, adNumeric, adSingle, adVarNumeric,
            '           adDouble, adInteger
            Case adDecimal, adNumeric, adSingle, adVarNumeric, adDouble, adInteger
                Criterio = Criterio & ValorCampo & " = " & BG_CvDecimalParaWhere_ADO(ValorControl)
            'data e hora: adDate, adDBDate, adDBTimeStamp, adDBTime
            Case adDate, adDBDate, adDBTimeStamp, adDBTime
                If NomesControl(i).Tag = CStr(mediTipoHora) Then
                    'hora
                    Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvHora(ValorControl) & "'"
                Else
                    'data
                    Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvDataParaWhere_ADO(ValorControl) & "'"
                End If
            Case adBSTR, adChar, adCurrency, adLongVarChar, adLongVarWChar, adVarChar, adVariant, adVarWChar, adWChar
                If InStr(ValorControl, "*") <> 0 Or InStr(ValorControl, "?") <> 0 Then
                    ValorControl = BG_CvWilcard(ValorControl)
                    Criterio = Criterio & ValorCampo & " LIKE " & "'" & ValorControl & "'"
                Else
                    Criterio = Criterio & ValorCampo & " = " & "'" & ValorControl & "'"
                End If
            'outro
            Case Else
                Criterio = Criterio & ValorCampo & " = " & ValorControl
            End Select
            
            nPassou = 1
        End If
        
        i = i + 1
    Next

    'caso todos os campos estejam vazios, selecciona tudo
    If iCount = i Then
        Criterio = criterioSelect & " FROM " & NomeTabela
        SelTotal = True
    End If
    
    ConstroiCriterio_SELECT = Criterio
End Function			

VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormFactusFichARS 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFactusFichARS"
   ClientHeight    =   8190
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   12870
   Icon            =   "FormFactusFichARS.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8190
   ScaleWidth      =   12870
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2520
      Top             =   7440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormFactusFichARS.frx":000C
            Key             =   "KeyLote"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormFactusFichARS.frx":08E6
            Key             =   "CRED"
         EndProperty
      EndProperty
   End
   Begin MSComCtl2.DTPicker EcAuxData 
      Height          =   255
      Left            =   0
      TabIndex        =   24
      Top             =   720
      Visible         =   0   'False
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   450
      _Version        =   393216
      CalendarBackColor=   12648447
      Format          =   144769025
      CurrentDate     =   41015
   End
   Begin MSComctlLib.TreeView TvLotes 
      Height          =   6135
      Left            =   120
      TabIndex        =   22
      Top             =   1920
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   10821
      _Version        =   393217
      Style           =   7
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin VB.TextBox EcAux 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      Height          =   285
      Left            =   0
      TabIndex        =   21
      Top             =   0
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Frame FrameEdicao 
      Caption         =   "Edi��o"
      Height          =   615
      Left            =   120
      TabIndex        =   14
      Top             =   1200
      Width           =   12735
      Begin VB.CommandButton BtInserir 
         Enabled         =   0   'False
         Height          =   495
         Left            =   5880
         Picture         =   "FormFactusFichARS.frx":15C0
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "Inserir"
         Top             =   120
         Width           =   495
      End
      Begin VB.CommandButton BtRemover 
         Enabled         =   0   'False
         Height          =   495
         Left            =   6480
         Picture         =   "FormFactusFichARS.frx":1D2A
         Style           =   1  'Graphical
         TabIndex        =   19
         ToolTipText     =   "remover"
         Top             =   120
         Width           =   495
      End
      Begin VB.TextBox EcCredencial 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3840
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   240
         Width           =   1935
      End
      Begin VB.TextBox EcEpisodio 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label LbCredencial 
         Caption         =   "Credencial"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2520
         TabIndex        =   18
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label LbEpisodio 
         Caption         =   "Epis�dio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame FramePesquisa 
      Caption         =   "Pesquisa"
      Height          =   735
      Left            =   120
      TabIndex        =   4
      Top             =   0
      Width           =   12735
      Begin VB.TextBox EcCredencialPesq 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7320
         TabIndex        =   11
         Top             =   240
         Width           =   2895
      End
      Begin VB.TextBox EcLote 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4680
         TabIndex        =   9
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcNr 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2400
         TabIndex        =   6
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcSerie 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   5
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label LbLote 
         Height          =   135
         Left            =   10320
         TabIndex        =   23
         Top             =   140
         Width           =   735
      End
      Begin VB.Label LbLinha 
         Height          =   135
         Left            =   10320
         TabIndex        =   13
         Top             =   400
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Credencial"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6120
         TabIndex        =   12
         Top             =   240
         Width           =   975
      End
      Begin VB.Label LbLotea 
         Caption         =   "Lote N�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3720
         TabIndex        =   10
         Top             =   240
         Width           =   855
      End
      Begin VB.Label LbNr 
         Caption         =   "/"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   8
         Top             =   240
         Width           =   135
      End
      Begin VB.Label LbSerie 
         Caption         =   "S�rie / N�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.CommandButton BtDuplicacao 
      Caption         =   "Duplica��es"
      Height          =   615
      Left            =   8640
      TabIndex        =   0
      Top             =   12600
      Width           =   1095
   End
   Begin MSComDlg.CommonDialog NomeFicheiro 
      Left            =   13920
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSFlexGridLib.MSFlexGrid FgRequisicoes 
      Height          =   6015
      Left            =   2640
      TabIndex        =   1
      Top             =   1920
      Width           =   10095
      _ExtentX        =   17806
      _ExtentY        =   10610
      _Version        =   393216
      BackColorBkg    =   -2147483633
      ScrollBars      =   2
      AllowUserResizing=   1
      BorderStyle     =   0
   End
   Begin VB.Label LbEntidade 
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   840
      Width           =   7815
   End
   Begin VB.Label LbTotLotes 
      Height          =   255
      Left            =   8160
      TabIndex        =   2
      Top             =   840
      Width           =   2175
   End
End
Attribute VB_Name = "FormFactusFichARS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Dim bContinuar As Boolean
Dim lCodEfr As Integer

Private Type Requis
    serie_fac As String
    n_fac As Long
    n_lote As Integer
    nr_req_ars As String
    doente As String
    nome_doente As String
    cod_isen_doe As String
    episodio As String
    dt_ini_real As String
End Type
Private Type lote
    n_lote As Integer
    estrutRequis() As Requis
    totalEstrutRequis As Long
End Type
Dim estrutLotes() As lote
Dim totalLotes As Integer
    

Const lColnumero = 0
Const lColDoente = 1
Const lColCredencial = 2
Const lColNome = 3
Const lColIsento = 4
Const lColEpisodio = 5
Const lcolDtIniReal = 6

Dim loteActivo As Integer

Private Sub CarregaFlexGrid()
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim i As Integer
    Dim flg_encontrou As Boolean
    

    Set rs = New ADODB.recordset
    TvLotes.Nodes.Clear
    
    sSql = "SELECT DISTINCT doente, nr_req_ars, nome_doente, cod_isen_doe, episodio, serie_fac, n_Fac, n_lote,dt_ini_real "
    sSql = sSql & " FROM fa_lin_fact WHERE doente = doente "
    sSql = sSql & " AND serie_fac = " & BL_TrataStringParaBD(EcSerie.Text)
    sSql = sSql & " AND n_fac = " & EcNr.Text
    sSql = sSql & " AND n_lote IS NOT NULL "
    sSql = sSql & " ORDER BY n_lote, nr_req_ars "
    
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria
    If rs.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation
    Else
        totalLotes = 0
        ReDim estrutLotes(0)
        While Not rs.EOF
            flg_encontrou = False
            For i = 1 To totalLotes
                If estrutLotes(i).n_lote = BL_HandleNull(rs!n_lote, mediComboValorNull) Then
                    flg_encontrou = True
                    Exit For
                End If
            Next
            If flg_encontrou = False Then
                totalLotes = totalLotes + 1
                ReDim Preserve estrutLotes(totalLotes)
                estrutLotes(totalLotes).n_lote = BL_HandleNull(rs!n_lote, mediComboValorNull)
                estrutLotes(totalLotes).totalEstrutRequis = 0
                ReDim estrutLotes(totalLotes).estrutRequis(0)
                i = totalLotes
                Dim root As Node
                Set root = TvLotes.Nodes.Add(, tvwChild, "K" & estrutLotes(totalLotes).n_lote, "Lote: " & estrutLotes(totalLotes).n_lote, "CRED")
                root.Expanded = False
            End If
            
            estrutLotes(i).totalEstrutRequis = estrutLotes(i).totalEstrutRequis + 1
            ReDim Preserve estrutLotes(i).estrutRequis(estrutLotes(i).totalEstrutRequis)
            estrutLotes(i).estrutRequis(estrutLotes(i).totalEstrutRequis).serie_fac = BL_HandleNull(rs!serie_fac, "")
            estrutLotes(i).estrutRequis(estrutLotes(i).totalEstrutRequis).n_fac = BL_HandleNull(rs!n_fac, "")
            estrutLotes(i).estrutRequis(estrutLotes(i).totalEstrutRequis).n_lote = BL_HandleNull(rs!n_lote, "")
            estrutLotes(i).estrutRequis(estrutLotes(i).totalEstrutRequis).doente = BL_HandleNull(rs!doente, "")
            estrutLotes(i).estrutRequis(estrutLotes(i).totalEstrutRequis).nr_req_ars = BL_HandleNull(rs!nr_req_ars, "")
            estrutLotes(i).estrutRequis(estrutLotes(i).totalEstrutRequis).nome_doente = BL_HandleNull(rs!nome_doente, "")
            estrutLotes(i).estrutRequis(estrutLotes(i).totalEstrutRequis).cod_isen_doe = BL_HandleNull(rs!cod_isen_doe, "")
            estrutLotes(i).estrutRequis(estrutLotes(i).totalEstrutRequis).episodio = BL_HandleNull(rs!episodio, "")
            estrutLotes(i).estrutRequis(estrutLotes(i).totalEstrutRequis).dt_ini_real = BL_HandleNull(rs!dt_ini_real, "")
            
            If EcCredencialPesq <> "" Then
                If EcCredencialPesq = estrutLotes(i).estrutRequis(estrutLotes(i).totalEstrutRequis).nr_req_ars Then
                    LbLinha = "LINHA:" & estrutLotes(i).totalEstrutRequis
                    LbLote = "LOTE:" & i
                End If
            Else
                LbLinha = ""
                LbLote = ""
            End If
            rs.MoveNext
        Wend
    End If
    rs.Close
    Set rs = Nothing
End Sub

Private Sub BtDuplicacao_Click()
    Dim rs As ADODB.recordset
    Dim sSql As String
    Dim i As Integer

    EcLote.Text = ""
    FgRequisicoes.rows = 2
    For i = 0 To FgRequisicoes.Cols - 1
        FgRequisicoes.TextMatrix(1, i) = ""
    Next
    
    'BG_Mensagem mediMsgBox, "N�o dispon�vel", vbOKOnly + vbExclamation
    'Exit Sub
    
    Set rs = New ADODB.recordset
    sSql = "SELECT n_seq_prog, cod_rubr FROM fa_lin_fact" & _
            " WHERE serie_fac = " & BL_TrataStringParaBD(EcSerie.Text) & _
            " AND n_fac = " & EcNr.Text & _
            " GROUP BY n_seq_prog, cod_rubr HAVING COUNT(*) > 1"
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria
    If rs.RecordCount > 0 Then
        BG_Mensagem mediMsgBox, "ERRO - h� duplica��es", vbOKOnly + vbExclamation
    Else
        BG_Mensagem mediMsgBox, "N�o h� duplica��es, pode facturar", vbOKOnly + vbExclamation
    End If
    rs.Close
    Set rs = Nothing

End Sub

Private Sub BtInserir_Click()
    Dim msg As String
    Dim rs As ADODB.recordset
    Dim Rs1 As ADODB.recordset
    Dim sSql As String
    Dim Erro As Integer
    Dim idade As String
    ' Vari�veis com dados da BD
    Dim n_lin_fac As Long
    Dim n_seq_prog As Integer
    Dim cod_prog As Integer
    Dim t_doente As String
    Dim doente As String
    Dim t_episodio As String
    Dim episodio As String
    Dim n_ord As Integer
    Dim dt_ini_real As Date
    Dim cod_tip_rubr As Integer
    Dim cod_grupo As Integer
    Dim cod_rubr As Integer
    Dim cod_serv_exec As Integer
    Dim qtd As Double
    Dim cod_isen_doe As String
    Dim chave_prog As String
    Dim n_ord_ins As Integer
    Dim empresa_id As String
    Dim nome_doe As String
    Dim dt_nasc As Date
    Dim n_benef_doe As String
    Dim cod_dom As String
    Dim qtd_dom As Double
    Dim descr_tip_rubr As String
    Dim descr_grupo As String
    Dim descr_serv_exec As String
    Dim descr_grupo_efr As String
    Dim val_k As Double
    Dim val_c As Double
    Dim Portaria As Integer
    Dim cod_rubr_efr As String
    Dim descr_rubr_efr As String
    Dim val_pag_doe As Double
    Dim t_fac As String
    Dim nr_k As Double
    Dim nr_c As Double
    Dim val_pr_unit As Double
    Dim val_total As Double
    Dim sld_qtd_cred As Double
    Dim sld_val_cred As Double
    Dim descr_dom As String
    Dim val_pr_unit_dom As Double
    Dim val_total_dom As Double
    Dim tot_qtd_dom_1 As Double
    Dim tot_qtd_dom_2 As Double
    Dim tot_qtd_dom_3 As Double
    On Error GoTo TrataErro
    
    If EcEpisodio.Text = "" Then
        BG_Mensagem mediMsgBox, "Indique o Epis�dio a Inserir", vbOKOnly + vbExclamation
        EcEpisodio.SetFocus
        Exit Sub
    End If
    
    If EcCredencial.Text = "" Then
        BG_Mensagem mediMsgBox, "Indique a Credencial a Inserir", vbOKOnly + vbExclamation
        EcCredencial.SetFocus
        Exit Sub
    End If
    
    ' Se o lote para a entidade n�o est� bloqueado
    Set rs = New ADODB.recordset
    sSql = "SELECT * FROM fa_lot_lock_area WHERE cod_efr = " & lCodEfr
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria
    If rs.RecordCount > 0 Then
        BG_Mensagem mediMsgBox, "Os registos que est� a tentar aceder est�o bloqueados", vbOKOnly + vbExclamation
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If
    rs.Close
    Set rs = Nothing
    
    ' Se a credencial existe para facturar para a entidade do lote
    Set rs = New ADODB.recordset
    sSql = "SELECT * FROM fa_movi_fact WHERE nr_req_ars = " & BL_TrataStringParaBD(EcCredencial.Text) & _
            " AND episodio = " & BL_TrataStringParaBD(EcEpisodio.Text) & _
            " AND flg_estado = 1 AND episodio IN (SELECT episodio FROM fa_movi_resp " & _
            " WHERE episodio = fa_movi_fact.episodio AND n_ord = fa_movi_fact.n_ord " & _
            " AND cod_efr = " & lCodEfr & ")"
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria
    If rs.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "O Epis�dio/Credencial " & EcEpisodio.Text & "/" & EcCredencial.Text & " n�o existe para facturar", vbOKOnly + vbExclamation
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If
    
    msg = "Pretende INSERIR a Credencial " & EcCredencial.Text & _
            " no Lote " & EcLote.Text & "?"
    If (BG_Mensagem(mediMsgBox, msg, vbQuestion + vbYesNo, "Inserir Credencial")) = vbNo Then
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If
    rs.Close
    Set rs = Nothing
    
    ' �ltimo n� da linha da factura
    Set rs = New ADODB.recordset
    sSql = "SELECT MAX(n_lin_fac) n_lin_fac FROM fa_lin_fact" & _
            " WHERE serie_fac = " & BL_TrataStringParaBD(EcSerie.Text) & _
            " AND n_fac = " & EcNr.Text
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria
    n_lin_fac = rs!n_lin_fac
    rs.Close
    Set rs = Nothing
    ' Linhas da credencial a facturar
    Set rs = New ADODB.recordset
    sSql = "SELECT DISTINCT n_seq_prog, cod_prog, t_doente, doente, t_episodio, episodio, n_ord," & _
            " dt_ini_real, cod_tip_rubr, cod_grupo, cod_rubr, cod_rubr_efr, descr_rubr, cod_serv_exec," & _
            " qtd, cod_isen_doe, chave_prog, n_ord_ins, empresa_id, flg_tip_req" & _
            " FROM fa_movi_fact WHERE nr_req_ars = " & BL_TrataStringParaBD(EcCredencial.Text) & _
            " AND episodio = " & BL_TrataStringParaBD(EcEpisodio.Text) & _
            " AND flg_estado = 1"
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria
    Erro = 0
    While Not rs.EOF
        If rs!cod_isen_doe = "0" Then
            cod_isen_doe = "N"
        Else
            cod_isen_doe = "S"
        End If

        ' Nome Utente e data nasc.
        Set Rs1 = New ADODB.recordset
        sSql = "SELECT nome_doe, dt_nasc FROM fa_doe_fact WHERE doente = " & BL_TrataStringParaBD(rs!doente)
        Rs1.CursorType = adOpenStatic
        Rs1.CursorLocation = adUseServer
        Rs1.Open sSql, gConexaoSecundaria
        If Rs1.RecordCount <= 0 Then Erro = 1
        nome_doe = Rs1!nome_doe
        dt_nasc = Rs1!dt_nasc
        Rs1.Close
        Set Rs1 = Nothing
        
        ' N� Benef. Utente e domic�lios
        Set Rs1 = New ADODB.recordset
        sSql = "SELECT n_benef_doe, cod_dom, qtd_dom FROM fa_movi_resp WHERE episodio = " & BL_TrataStringParaBD(rs!episodio) & _
                " AND n_ord = " & rs!n_ord
        Rs1.CursorType = adOpenStatic
        Rs1.CursorLocation = adUseServer
        Rs1.Open sSql, gConexaoSecundaria
        If Rs1.RecordCount <= 0 Then Erro = 1
        n_benef_doe = Rs1!n_benef_doe
        cod_dom = BL_HandleNull(Rs1!cod_dom, "")
        qtd_dom = Rs1!qtd_dom
        Rs1.Close
        Set Rs1 = Nothing
    
        ' Descri��o tipo rubrica
        Set Rs1 = New ADODB.recordset
        sSql = "SELECT descr_tip_rubr FROM fa_tip_rubricas WHERE cod_tip_rubr = " & rs!cod_tip_rubr
        Rs1.CursorType = adOpenStatic
        Rs1.CursorLocation = adUseServer
        Rs1.Open sSql, gConexaoSecundaria
        If Rs1.RecordCount <= 0 Then Erro = 1
        descr_tip_rubr = Rs1!descr_tip_rubr
        Rs1.Close
        Set Rs1 = Nothing
        
        ' Descri��o grupo rubrica
        Set Rs1 = New ADODB.recordset
        sSql = "SELECT descr_grupo FROM fa_grup_rubr WHERE cod_grupo = " & rs!cod_grupo & _
                " AND cod_tip_rubr = " & rs!cod_tip_rubr
        Rs1.CursorType = adOpenStatic
        Rs1.CursorLocation = adUseServer
        Rs1.Open sSql, gConexaoSecundaria
        If Rs1.RecordCount <= 0 Then Erro = 1
        descr_grupo = Rs1!descr_grupo
        Rs1.Close
        Set Rs1 = Nothing
        
        ' Descri��o servi�o executante
        Set Rs1 = New ADODB.recordset
        sSql = "SELECT descr_serv FROM sd_serv WHERE cod_serv = " & rs!cod_serv_exec
        Rs1.CursorType = adOpenStatic
        Rs1.CursorLocation = adUseServer
        Rs1.Open sSql, gConexaoSecundaria
        If Rs1.RecordCount = 1 Then
            descr_serv_exec = BL_HandleNull(Rs1!descr_serv, "")
        End If
        Rs1.Close
        Set Rs1 = Nothing
        
        ' Descri��o grupo efr e valor de C e K
        Set Rs1 = New ADODB.recordset
        sSql = "SELECT c.descr_grupo_efr descr_grupo_efr, c.valor_k valor_k, c.valor_c valor_c" & _
                " FROM fa_efr a, fa_portarias b, fa_pr_grup_rubr c" & _
                " WHERE a.cod_efr = " & lCodEfr & _
                " AND a.tab_utilizada = b.cod_efr" & _
                " AND b.dt_fim IS NULL" & _
                " AND b.portaria = c.portaria" & _
                " AND c.cod_grupo = " & rs!cod_grupo
        Rs1.CursorType = adOpenStatic
        Rs1.CursorLocation = adUseServer
        Rs1.Open sSql, gConexaoSecundaria
        If Rs1.RecordCount <= 0 Then
            descr_grupo_efr = ""
            val_k = 0
            val_c = 0
        Else
            descr_grupo_efr = Rs1!descr_grupo_efr
            val_k = BL_HandleNull(Rs1!valor_k, 0)
            val_c = BL_HandleNull(Rs1!valor_c, 0)
        End If
        Rs1.Close
        Set Rs1 = Nothing
                
        ' Portaria da data da credencial
        Set Rs1 = New ADODB.recordset
        sSql = "SELECT b.portaria portaria, c.cod_rubr_efr cod_rubr_efr, c.descr_rubr_efr descr_rubr_efr," & _
                " c.val_pag_doe val_pag_doe, c.t_fac t_fac, c.nr_k nr_k, c.nr_c nr_c, c.val_pag_ent val_pag_ent, c.val_taxa" & _
                " FROM fa_efr a, fa_portarias b, fa_pr_rubr c" & _
                " WHERE a.cod_efr = " & lCodEfr & _
                " AND a.tab_utilizada = b.cod_efr" & _
                " AND " & BL_TrataDataParaBD(rs!dt_ini_real) & " >= b.dt_ini" & _
                " AND (" & BL_TrataDataParaBD(rs!dt_ini_real) & " <= b.dt_fim OR b.dt_fim IS NULL)" & _
                " AND b.portaria = c.portaria" & _
                " AND c.cod_rubr = " & rs!cod_rubr
        Rs1.CursorType = adOpenStatic
        Rs1.CursorLocation = adUseServer
        Rs1.Open sSql, gConexaoSecundaria
        If Rs1.RecordCount <= 0 Then Erro = 1
        Portaria = Rs1!Portaria
        If BL_HandleNull(rs!cod_rubr_efr, "") = "" Then
            cod_rubr_efr = BL_HandleNull(Rs1!cod_rubr_efr, "")
            descr_rubr_efr = BL_HandleNull(Rs1!descr_rubr_efr, "")
        Else
            cod_rubr_efr = BL_HandleNull(Rs1!cod_rubr_efr, "")
            descr_rubr_efr = BL_HandleNull(Rs1!descr_rubr_efr, "")
        End If
        val_pag_doe = BL_HandleNull(Rs1!val_Taxa, 0)
        t_fac = BL_HandleNull(Rs1!t_fac, "")
        nr_k = BL_HandleNull(Rs1!nr_k, 0)
        nr_c = BL_HandleNull(Rs1!nr_c, 0)
        val_pr_unit = BL_HandleNull(Rs1!val_pag_ent, 0)
        Rs1.Close
        Set Rs1 = Nothing
        
        sld_qtd_cred = BL_HandleNull(rs!qtd, 1)
        val_total = val_pr_unit * BL_HandleNull(rs!qtd, 1)
        sld_val_cred = val_total
        If cod_isen_doe = "S" Then
            val_pag_doe = 0
        Else
            ' mais de 65 anos paga metade
            idade = BG_CalculaIdade(BL_HandleNull(dt_nasc, Bg_DaData_ADO), CDate(rs!dt_ini_real))
            If CLng(Mid(idade, 1, InStr(1, idade, " "))) >= 965 Then
                val_pag_doe = val_pag_doe / 2
                sld_val_cred = sld_val_cred - val_pag_doe
            End If
        End If
        
        ' Domicilios
        If CStr(qtd_dom) <> "" And (qtd_dom) > 0 Then
            Set Rs1 = New ADODB.recordset
            sSql = "SELECT descr_dom, pr_dom" & _
                    " FROM fa_param_dom" & _
                    " WHERE portaria = " & Portaria & _
                    " AND cod_dom = " & cod_dom
            Rs1.CursorType = adOpenStatic
            Rs1.CursorLocation = adUseServer
            Rs1.Open sSql, gConexaoSecundaria
            If Rs1.RecordCount <= 0 Then Erro = 1
            descr_dom = Rs1!descr_dom
            val_pr_unit_dom = Rs1!pr_dom
            Rs1.Close
            Set Rs1 = Nothing
        Else
            descr_dom = ""
            val_pr_unit_dom = 0
        End If
        val_total_dom = val_pr_unit_dom * qtd_dom
        
        If Erro = 1 Then
            BG_Mensagem mediMsgBox, "Erro ao inserir a credencial " & EcCredencial.Text, vbOKOnly + vbExclamation
            Exit Sub
        End If
    
        n_lin_fac = n_lin_fac + 1
        sSql = "INSERT INTO fa_lin_fact (serie_fac, n_fac, n_lin_fac, t_doente, doente, nome_doente, n_benef_doe," & _
                " t_episodio, episodio, dt_ini_real, cod_tip_rubr, descr_tip_rubr, cod_grupo, descr_grupo," & _
                " descr_grupo_efr, cod_rubr, cod_rubr_efr, descr_rubr, cod_serv_exec, descr_serv_exec, t_fac," & _
                " val_k, val_c, nr_k, nr_c, val_pr_unit, val_pag_doe, qtd, val_total, cod_isen_doe, cod_prog," & _
                " n_seq_prog, n_lote, nr_req_ars, user_cri, dt_cri, chave_prog, sld_qtd_cred, sld_val_cred," & _
                " qtd_dom, descr_dom, val_pr_unit_dom, val_total_dom, n_ord_ins, empresa_id, cod_tip_req, cod_dom) " & _
                " VALUES ( " & BL_TrataStringParaBD(EcSerie.Text) & ", " & EcNr.Text & ", " & n_lin_fac & ", " & _
                BL_TrataStringParaBD(rs!t_doente) & ", " & BL_TrataStringParaBD(rs!doente) & ", " & _
                BL_TrataStringParaBD(nome_doe) & ", " & BL_TrataStringParaBD(n_benef_doe) & ", " & _
                BL_TrataStringParaBD(rs!t_episodio) & ", " & BL_TrataStringParaBD(rs!episodio) & ", " & _
                BL_TrataDataParaBD(rs!dt_ini_real) & ", " & rs!cod_tip_rubr & ", " & BL_TrataStringParaBD(descr_tip_rubr) & _
                ", " & rs!cod_grupo & ", " & BL_TrataStringParaBD(descr_grupo) & ", " & BL_TrataStringParaBD(descr_grupo_efr) & _
                ", " & rs!cod_rubr & ", " & BL_TrataStringParaBD(cod_rubr_efr) & ", " & BL_TrataStringParaBD(descr_rubr_efr) & _
                ", " & rs!cod_serv_exec & ", " & BL_TrataStringParaBD(descr_serv_exec) & ", " & BL_TrataStringParaBD(t_fac) & _
                ", " & Replace(val_k, ",", ".") & ", " & Replace(val_c, ",", ".") & ", " & Replace(nr_k, ",", ".") & ", " & Replace(nr_c, ",", ".") & ", " & Replace(val_pr_unit, ",", ".") & ", " & Replace(val_pag_doe, ",", ".") & ", " & _
                Replace(rs!qtd, ",", ".") & ", " & Replace(val_total, ",", ".") & ", " & BL_TrataStringParaBD(cod_isen_doe) & ", " & rs!cod_prog & ", " & _
                rs!n_seq_prog & ", " & EcLote.Text & ", " & BL_TrataStringParaBD(EcCredencial.Text) & ", " & "'LOTES'" & ", " & _
                BL_TrataDataParaBD(Bg_DaData_ADO) & ", " & BL_TrataStringParaBD(rs!chave_prog) & ", " & Replace(sld_qtd_cred, ",", ".") & _
                ", " & Replace(sld_val_cred, ",", ".") & ", " & Replace(qtd_dom, ",", ".") & ", " & BL_TrataStringParaBD(descr_dom) & ", " & Replace(val_pr_unit_dom, ",", ".") & ", " & _
                Replace(val_total_dom, ",", ".") & ", " & rs!n_ord_ins & ", " & BL_TrataStringParaBD(rs!empresa_id) & ", " & _
                rs!flg_tip_req & "," & BL_TrataStringParaBD(cod_dom) & ")"
        If gSGBD = gSqlServer Then
            gConexaoSecundaria.Execute sSql
        Else
            BG_ExecutaQuery_ADO sSql
        End If

        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
    
    ' Totais qtd domicilios
'    If qtd_dom > 0 Then
'        Set Rs = New ADODB.Recordset
'        sSql = "SELECT DISTINCT tot_qtd_dom_1, tot_qtd_dom_2, tot_qtd_dom_3 FROM fa_lin_fact" & _
'                " WHERE serie_fac = " & BL_TrataStringParaBD(EcSerie.Text) & _
'                " AND n_fac = " & EcNr.Text & _
'                " AND n_lote = " & EcLote.Text & _
'                " AND qtd_dom > 0" & _
'                " AND (tot_qtd_dom_1 IS NOT NULL OR tot_qtd_dom_2 IS NOT NULL OR tot_qtd_dom_3 IS NOT NULL)"
'        Rs.CursorType = adOpenStatic
'        Rs.CursorLocation = adUseServer
'        Rs.Open sSql, gConexaoSecundaria
'        tot_qtd_dom_1 = BL_HandleNull(Rs!tot_qtd_dom_1, 0)
'        tot_qtd_dom_2 = BL_HandleNull(Rs!tot_qtd_dom_2, 0)
'        tot_qtd_dom_3 = BL_HandleNull(Rs!tot_qtd_dom_3, 0)
'        If cod_dom = 1 Then
'            tot_qtd_dom_1 = tot_qtd_dom_1 + qtd_dom
'        Else
'            If cod_dom = 2 Then
'                tot_qtd_dom_2 = tot_qtd_dom_2 + qtd_dom
'            Else
'                If cod_dom = 3 Then
'                    tot_qtd_dom_3 = tot_qtd_dom_3 + qtd_dom
'                End If
'            End If
'        End If
'        Rs.Close
'        Set Rs = Nothing
'        sSql = "UPDATE fa_lin_fact SET tot_qtd_dom_1 = " & Replace(tot_qtd_dom_1, ",", ".") & "," & _
'                " tot_qtd_dom_2 = " & Replace(tot_qtd_dom_2, ",", ".") & ", tot_qtd_dom_3 = " & Replace(tot_qtd_dom_3, ",", ".") & _
'                " WHERE serie_fac = " & BL_TrataStringParaBD(EcSerie.Text) & _
'                " AND n_fac = " & EcNr.Text & _
'                " AND n_lote = " & EcLote.Text & _
'                " AND qtd_dom > 0"
'        BG_ExecutaQuery_ADO sSql
'    End If
    
    sSql = "UPDATE fa_movi_fact SET flg_estado = 3, serie_fac = " & BL_TrataStringParaBD(EcSerie.Text) & ", " & _
            "n_fac = " & EcNr.Text & ", flg_estado_doe = 3, flg_estado_tx = 3 " & _
            "WHERE nr_req_ars = " & BL_TrataStringParaBD(EcCredencial.Text) & _
            " AND episodio = " & BL_TrataStringParaBD(EcEpisodio.Text) & _
            " AND flg_estado = 1"
    If gSGBD = gSqlServer Then
        gConexaoSecundaria.Execute sSql
    Else
        BG_ExecutaQuery_ADO sSql
    End If
    
    sSql = "COMMIT"
    If gSGBD = gSqlServer Then
        gConexaoSecundaria.Execute sSql
    Else
        BG_ExecutaQuery_ADO sSql
    End If
    
    EcEpisodio.Text = ""
    EcCredencial.Text = ""
    CarregaFlexGrid
    FgRequisicoes.row = FgRequisicoes.rows - 1
    FgRequisicoes.Col = 0
    FgRequisicoes.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao adicionar credencial " & Err.Description & " " & sSql, Me.Name, "btinserir_click", True
    Exit Sub
    Resume Next
End Sub

Public Sub FuncaoProcurar()
    Dim i As Integer
    FgRequisicoes.rows = 2
    For i = 0 To FgRequisicoes.Cols - 1
        FgRequisicoes.TextMatrix(1, i) = ""
    Next
    TvLotes.Nodes.Clear
    
    If (EcSerie.Text = "" Or EcNr = "") Then
        BG_Mensagem mediMsgBox, "Indique a S�rie, Fact e Lote ou N�mero de Credencial a Alterar", vbOKOnly + vbExclamation
        EcSerie.SetFocus
        Exit Sub
    End If
    

    
    CarregaFlexGrid
    If totalLotes > 0 Then
        TvLotes.Nodes.item("K1").Selected = True
    End If
    If EcLote <> "" Then
        If IsNumeric(EcLote) Then
            If EcLote > mediComboValorNull And EcLote < totalLotes Then
             TvLotes.Nodes.item("K" & EcLote).Selected = True
            End If
        End If
    End If
    TvLotes_Click
    
    If EcSerie <> "LOTES" & CStr(Year(Date)) And EcSerie <> "LOTES" & CStr(Year(Date) - 1) Then
        BtRemover.Enabled = False
        EcCredencial.Locked = True
        EcEpisodio.Locked = True
    Else
        BtRemover.Enabled = True
        EcCredencial.Locked = False
        EcEpisodio.Locked = False
    End If
    TvLotes.SetFocus
End Sub


Private Sub BtRemover_Click()
    Dim msg As String
    Dim sql As String
    Dim SelLinha As Integer
    Dim SelCol As Integer
    Dim rs As ADODB.recordset
    
    SelLinha = FgRequisicoes.row
    SelCol = FgRequisicoes.Col
    If FgRequisicoes.TextMatrix(SelLinha, 1) = "" Then
        FgRequisicoes.SetFocus
        Exit Sub
    End If
    
    ' Se o lote para a entidade n�o est� bloqueado
    Set rs = New ADODB.recordset
    sql = "SELECT * FROM fa_lot_lock_area WHERE cod_efr = " & lCodEfr
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sql, gConexaoSecundaria
    If rs.RecordCount > 0 Then
        BG_Mensagem mediMsgBox, "Os registos que est� a tentar aceder est�o bloqueados", vbOKOnly + vbExclamation
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If
    rs.Close
    Set rs = Nothing
    
    msg = "Pretende REMOVER a Credencial " & FgRequisicoes.TextMatrix(FgRequisicoes.row, 1) & _
            " do Lote " & EcLote.Text & "?"
    If (BG_Mensagem(mediMsgBox, msg, vbQuestion + vbYesNo, "Remover Credencial")) = vbYes Then
        sql = "DELETE FROM fa_lin_fact WHERE serie_fac = " & BL_TrataStringParaBD(EcSerie.Text) & _
                " AND n_fac = " & EcNr.Text & " AND n_lote = " & estrutLotes(loteActivo).n_lote & _
                " AND nr_req_ars = " & BL_TrataStringParaBD(estrutLotes(loteActivo).estrutRequis(FgRequisicoes.row).nr_req_ars)
        If gSGBD = gSqlServer Then
            gConexaoSecundaria.Execute sql
        Else
            BG_ExecutaQuery_ADO sql
        End If
        sql = "UPDATE fa_movi_fact SET flg_estado = 1, serie_fac = null, n_fac = null, " & _
                "flg_estado_doe = null, serie_fac_doe = null, n_fac_doe = null, flg_estado_tx = null " & _
                "WHERE serie_fac = " & BL_TrataStringParaBD(EcSerie.Text) & _
                " AND n_fac = " & EcNr.Text & _
                " AND nr_req_ars = " & BL_TrataStringParaBD(estrutLotes(loteActivo).estrutRequis(FgRequisicoes.row).nr_req_ars)
        If gSGBD = gSqlServer Then
            gConexaoSecundaria.Execute sql
        Else
            BG_ExecutaQuery_ADO sql
        End If
        sql = "COMMIT"
        If gSGBD = gSqlServer Then
            gConexaoSecundaria.Execute sql
        Else
            BG_ExecutaQuery_ADO sql
        End If
        CarregaFlexGrid
        FgRequisicoes.row = SelLinha
        FgRequisicoes.Col = SelCol
        FgRequisicoes.SetFocus
    End If

End Sub



Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        If EcAux <> "" And FgRequisicoes.row <= estrutLotes(loteActivo).totalEstrutRequis Then
            AlteraCredencial loteActivo, FgRequisicoes.row, EcAux
            EcAux_LostFocus
        End If
    End If
End Sub

Private Sub EcAux_LostFocus()
    EcAux = ""
    EcAux.Visible = False
End Sub



Private Sub EcAuxData_Change()
     AlteraDtIniReal loteActivo, FgRequisicoes.row, CStr(EcAuxData.value)
     EcAuxData_LostFocus
End Sub

Private Sub EcAuxData_LostFocus()
    'EcAuxData = ""
    EcAuxData.Visible = False
End Sub

Private Sub EcCredencialPesq_Validate(Cancel As Boolean)
    Dim sSql As String
    Dim rsCred As New ADODB.recordset
    If EcCredencial = "" Then Exit Sub
    sSql = "SELECT DISTINCT doente, nr_req_ars, nome_doente, cod_isen_doe, episodio, serie_fac, n_Fac, n_lote "
    sSql = sSql & " FROM fa_lin_fact WHERE doente = doente "
    If EcSerie <> "" And EcNr <> "" Then
        sSql = sSql & " AND serie_fac = " & BL_TrataStringParaBD(EcSerie.Text)
        sSql = sSql & " AND n_fac = " & EcNr.Text
    End If
    If EcLote <> "" Then
        sSql = sSql & " AND n_lote =  " & EcLote
    End If
    sSql = sSql & " AND n_lote IS NOT NULL  AND nr_req_ars = " & BL_TrataStringParaBD(EcCredencialPesq)
    sSql = sSql & " ORDER BY n_lote, nr_req_ars "
    rsCred.CursorType = adOpenStatic
    rsCred.CursorLocation = adUseServer
    rsCred.Open sSql, gConexaoSecundaria
    If rsCred.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "Credencial inv�lida !", vbExclamation
        EcCredencialPesq = ""
        Exit Sub
    Else
        EcSerie = BL_HandleNull(rsCred!serie_fac, "")
        EcNr = BL_HandleNull(rsCred!n_fac, "")
    End If
    rsCred.Close
    Set rsCred = Nothing
End Sub

Private Sub EcEpisodio_Change()
    
    BtInserir.Enabled = True
    BtRemover.Enabled = False
    
End Sub

Private Sub EcCredencial_Change()
    
    BtInserir.Enabled = True
    BtRemover.Enabled = False
    
End Sub

Private Sub EcSerie_Change()
    
    BtRemover.Enabled = False
    BtInserir.Enabled = False

End Sub

Private Sub EcNr_LostFocus()
    Dim rs As ADODB.recordset
    Dim sSql As String
    
    BtRemover.Enabled = False
    BtInserir.Enabled = False
    
    EcLote.Text = ""
    EcEpisodio.Text = ""
    EcCredencial.Text = ""
    LbEntidade.caption = ""
    LbTotLotes.caption = ""
    If EcNr = "" Then Exit Sub
    Set rs = New ADODB.recordset
    sSql = "SELECT cod_efr, descr_efr FROM fa_fact" & _
            " WHERE serie_fac = " & BL_TrataStringParaBD(EcSerie.Text) & _
            " AND n_fac = " & EcNr.Text
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria
    If rs.RecordCount = 0 Then
        rs.Close
        BG_Mensagem mediMsgBox, "N�o h� lotes fechados", vbOKOnly + vbExclamation
        Exit Sub
    End If
    LbEntidade.caption = rs!descr_efr
    lCodEfr = rs!cod_efr
    rs.Close
    Set rs = Nothing

    Set rs = New ADODB.recordset
    sSql = "SELECT MIN(n_lote) min_lote, MAX(n_lote) max_lote FROM fa_lin_fact" & _
            " WHERE serie_fac = " & BL_TrataStringParaBD(EcSerie.Text) & " AND n_fac = " & EcNr.Text
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria
    If rs.RecordCount = 0 Then
        sSql = "DELETE FROM fa_fact" & _
                " WHERE serie_fac = " & BL_TrataStringParaBD(EcSerie.Text) & " AND n_fac = " & EcNr.Text
        If gSGBD = gSqlServer Then
            gConexaoSecundaria.Execute sSql
        Else
            BG_ExecutaQuery_ADO sSql
        End If
        sSql = "COMMIT"
        If gSGBD = gSqlServer Then
            gConexaoSecundaria.Execute sSql
        Else
            BG_ExecutaQuery_ADO sSql
        End If
        BG_Mensagem mediMsgBox, "N�o h� lotes fechados", vbOKOnly + vbExclamation
        Exit Sub
    End If
    LbTotLotes = "Lotes fechados " & rs!min_lote & " a " & rs!max_lote
    rs.Close

End Sub




Private Sub FgRequisicoes_Click()
    If EcSerie = "LOTES" & CStr(Year(Date)) And FgRequisicoes.row <= estrutLotes(loteActivo).totalEstrutRequis Then
        BtRemover.Enabled = True
    Else
        BtRemover.Enabled = False
    End If
End Sub

Private Sub FgRequisicoes_DblClick()
    If FgRequisicoes.row <= estrutLotes(loteActivo).totalEstrutRequis And FgRequisicoes.Col = lColCredencial Then
        EcAux.top = FgRequisicoes.top + FgRequisicoes.CellTop
        EcAux.left = FgRequisicoes.left + FgRequisicoes.CellLeft
        EcAux.Width = FgRequisicoes.CellWidth
        EcAux = estrutLotes(loteActivo).estrutRequis(FgRequisicoes.row).nr_req_ars
        EcAux.Visible = True
        EcAux.SetFocus
    ElseIf FgRequisicoes.row <= estrutLotes(loteActivo).totalEstrutRequis And FgRequisicoes.Col = lcolDtIniReal Then
        EcAuxData.top = FgRequisicoes.top + FgRequisicoes.CellTop
        EcAuxData.left = FgRequisicoes.left + FgRequisicoes.CellLeft
        EcAuxData.Width = FgRequisicoes.CellWidth
        EcAuxData.value = estrutLotes(loteActivo).estrutRequis(FgRequisicoes.row).dt_ini_real
        EcAuxData.Visible = True
        EcAuxData.SetFocus
    End If
    
End Sub

Private Sub Form_Activate()
    
    Call EventoActivate
    
End Sub

Private Sub Form_Load()
    
    Call EventoLoad
    
End Sub



Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    Call BL_InicioProcessamento(Me, "Inicializar �cran.")
    
    'Preenche Combos
    Call PreencheValoresDefeito
    
    'Define Campos do ecran e das tabelas
    Call Inicializacoes
    
    'Define o tipo de campos
    BL_Abre_Conexao_Secundaria gOracle
    Call DefTipoCampos
    
    Call BG_ParametrizaPermissoes_ADO(Me.Name)
    
    estado = 1
    Call BG_StackJanelas_Push(Me)
    Call BL_FimProcessamento(Me)
End Sub

Private Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If gConexaoSecundaria.state <> adStateClosed Then
        BL_Fecha_Conexao_Secundaria
    End If
    Set FormFactusFichARS = Nothing

End Sub

Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub

Private Sub Inicializacoes()
        
    Me.caption = " Edi��o de Lotes fechados "
    
    Me.left = 5
    Me.top = 5
    Me.Width = 12960
    Me.Height = 8610
    
    
    Set CampoDeFocus = EcSerie
    
    NomeTabela = ""
    
    NumCampos = 0
        
End Sub
Sub DefTipoCampos()
    Dim rs As ADODB.recordset
    Dim sSql As String
    
    EcSerie.Tag = mediTipoMaiusculas
    EcNr.Tag = mediTipoDefeito
    EcLote.Tag = mediTipoDefeito
    
    EcLote.Text = ""
    LbEntidade.caption = ""
    LbTotLotes.caption = ""
    With FgRequisicoes
        .Cols = 7
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        .ScrollBars = flexScrollBarVertical
        
        .ColAlignment(lColnumero) = flexAlignCenterCenter
        .ColWidth(lColnumero) = 300
        .TextMatrix(0, lColnumero) = "#"
        
        .ColAlignment(lcolDtIniReal) = flexAlignCenterCenter
        .ColWidth(lcolDtIniReal) = 1000
        .TextMatrix(0, lcolDtIniReal) = "Data"
        
        .ColAlignment(lColDoente) = flexAlignCenterCenter
        .ColWidth(lColDoente) = 900
        .TextMatrix(0, lColDoente) = "N� Doente"
        
        .ColAlignment(lColCredencial) = flexAlignCenterCenter
        .ColWidth(lColCredencial) = 1845
        .TextMatrix(0, lColCredencial) = "Credencial"
        
        .ColAlignment(lColNome) = flexAlignLeftCenter
        .ColWidth(lColNome) = 4500
        .TextMatrix(0, lColNome) = "Nome"
        
        .ColAlignment(lColIsento) = flexAlignCenterCenter
        .ColWidth(lColIsento) = 600
        .TextMatrix(0, lColIsento) = "Isento"
    
        .ColAlignment(lColEpisodio) = flexAlignCenterCenter
        .ColWidth(lColEpisodio) = 855
        .TextMatrix(0, lColEpisodio) = "Epis�dio"
        
        .Width = .ColWidth(0) + .ColWidth(1) + .ColWidth(2) + .ColWidth(3) + .ColWidth(4) + .ColWidth(5) + .ColWidth(6) + 275
    End With
    
    Set rs = New ADODB.recordset
    sSql = "SELECT f.serie_fac, f.n_fac, f.cod_efr, f.descr_efr FROM fa_fact f, fa_num_gen ng" & _
            " WHERE f.serie_fac LIKE 'LOTES%' AND f.serie_fac = ng.serie AND f.n_fac = ng.n_lote"
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria
    If rs.RecordCount = 0 Then
        rs.Close
        Set rs = New ADODB.recordset
        sSql = "SELECT MAX(serie_fac) serie_fac, MAX(n_fac) n_fac, cod_efr, descr_efr FROM fa_fact " & _
                " WHERE serie_fac LIKE 'LOTES%' GROUP BY cod_efr, descr_efr"
        rs.CursorType = adOpenStatic
        rs.CursorLocation = adUseServer
        rs.Open sSql, gConexaoSecundaria
        If rs.RecordCount = 0 Then
            rs.Close
            BG_Mensagem mediMsgBox, "N�o h� lotes fechados", vbOKOnly + vbExclamation
            Exit Sub
        End If
    End If
    EcSerie.Text = rs!serie_fac
    EcNr.Text = rs!n_fac
    LbEntidade.caption = rs!descr_efr
    lCodEfr = rs!cod_efr
    rs.Close
    Set rs = Nothing
    
    Set rs = New ADODB.recordset
    sSql = "SELECT MIN(n_lote) min_lote, MAX(n_lote) max_lote FROM fa_lin_fact" & _
            " WHERE serie_fac = " & BL_TrataStringParaBD(EcSerie.Text) & " AND n_fac = " & EcNr.Text
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria
    If rs.RecordCount = 0 Then
        sSql = "DELETE FROM fa_fact" & _
                " WHERE serie_fac = " & BL_TrataStringParaBD(EcSerie.Text) & " AND n_fac = " & EcNr.Text
        If gSGBD = gSqlServer Then
            gConexaoSecundaria.Execute sSql
        Else
            BG_ExecutaQuery_ADO sSql
        End If
        sSql = "COMMIT"
        If gSGBD = gSqlServer Then
            gConexaoSecundaria.Execute sSql
        Else
            BG_ExecutaQuery_ADO sSql
        End If
        BG_Mensagem mediMsgBox, "N�o h� lotes fechados", vbOKOnly + vbExclamation
        Exit Sub
    End If
    LbTotLotes = "Lotes fechados " & rs!min_lote & " a " & rs!max_lote
    rs.Close
    loteActivo = mediComboValorNull
    TvLotes.LineStyle = tvwRootLines
    TvLotes.ImageList = ImageList1
    
End Sub

Public Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
    
End Sub

Public Sub FuncaoInserir()
    
  ' nada

    
End Sub
Private Function ValidaCamposEc() As Integer

' nada


End Function

Public Sub FuncaoModificar()
    
' nada

    
End Sub

Public Sub FuncaoRemover()
' nada

    
End Sub

Public Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoEstadoAnterior()
    
     If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Private Sub PreencheValoresDefeito()
    
' nada
End Sub

Private Sub BD_Insert()
' nada
    

End Sub

Private Sub BD_Delete()
' nada
    

    
End Sub

Private Sub BD_Update()
' nada
    

End Sub

Private Sub LimpaCampos()
    Dim i As Integer
    FgRequisicoes.rows = 2
    For i = 0 To FgRequisicoes.Cols - 1
        FgRequisicoes.TextMatrix(1, i) = ""
    Next
    EcCredencialPesq = ""
    EcLote = ""
    totalLotes = 0
    ReDim estrutLotes(0)
    EcCredencial.Locked = True
    EcEpisodio.Locked = True
    EcEpisodio = ""
    EcCredencial = ""
    loteActivo = mediComboValorNull
    TvLotes.Nodes.Clear
End Sub

Private Sub PreencheCampos()
    ' nada

End Sub



Private Sub AlteraCredencial(lote As Integer, linha As Long, novaCredencial As String)
    Dim sSql As String
    Dim credencialAntiga As String
    Dim registos As Long
    On Error GoTo TrataErro
    
    ' Se o lote para a entidade n�o est� bloqueado
    Set rs = New ADODB.recordset
    sSql = "SELECT * FROM fa_lot_lock_area WHERE cod_efr = " & lCodEfr
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria
    If rs.RecordCount > 0 Then
        BG_Mensagem mediMsgBox, "Os registos que est� a tentar aceder est�o bloqueados", vbOKOnly + vbExclamation
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If
    rs.Close
    Set rs = Nothing
    
    credencialAntiga = estrutLotes(lote).estrutRequis(linha).nr_req_ars
    estrutLotes(lote).estrutRequis(linha).nr_req_ars = novaCredencial
    FgRequisicoes.TextMatrix(linha, lColCredencial) = novaCredencial
    'inicia as transacoes
    gConexaoSecundaria.BeginTrans
    BG_BeginTransaction
    registos = 0
    
    'FA_LIN_FACT
    sSql = "UPDATE fa_lin_fact SET nr_req_ars = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).nr_req_ars)
    sSql = sSql & " WHERE serie_fac = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).serie_fac)
    sSql = sSql & " AND n_fac = " & BL_TrataStringParaBD(CStr(estrutLotes(lote).estrutRequis(linha).n_fac))
    sSql = sSql & " AND t_episodio ='SISLAB' "
    sSql = sSql & " AND episodio = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).episodio)
    sSql = sSql & " AND nr_req_ars = " & BL_TrataStringParaBD(credencialAntiga)
    gConexaoSecundaria.Execute sSql, registos
    If registos <= 0 Then
        GoTo TrataErro
    End If
    
    'FA_MOVI_FACT
    sSql = "UPDATE fa_movi_fact SET nr_req_ars = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).nr_req_ars)
    sSql = sSql & " WHERE t_episodio ='SISLAB' "
    sSql = sSql & " AND episodio = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).episodio)
    sSql = sSql & " AND nr_req_ars = " & BL_TrataStringParaBD(credencialAntiga)
    gConexaoSecundaria.Execute sSql, registos
    If registos <= 0 Then
        GoTo TrataErro
    End If
    
    'SL_REQ_ARS
    sSql = "UPDATE sl_Req_ars SET nr_req_ars = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).nr_req_ars)
    sSql = sSql & " WHERE n_req = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).episodio)
    sSql = sSql & " AND nr_req_ars = " & BL_TrataStringParaBD(credencialAntiga)
    registos = BG_ExecutaQuery_ADO(sSql)
    If registos <= 0 Then
        GoTo TrataErro
    End If
    
    'SL_REQ_ARS
    sSql = "UPDATE sl_recibos_det SET p1 = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).nr_req_ars)
    sSql = sSql & " WHERE n_req = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).episodio)
    sSql = sSql & " AND p1 = " & BL_TrataStringParaBD(credencialAntiga)
    registos = BG_ExecutaQuery_ADO(sSql)
    If registos <= 0 Then
        GoTo TrataErro
    End If
    
    gConexaoSecundaria.CommitTrans
    BG_CommitTransaction
Exit Sub
TrataErro:
    gConexaoSecundaria.RollbackTrans
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro ao alterar credencial :" & Err.Description & vbCrLf & sSql, Me.Name, "AlteraCredencial", True
    Exit Sub
    Resume Next
End Sub


Private Sub AlteraDtIniReal(lote As Integer, linha As Long, novaData As String)
    Dim sSql As String
    Dim credencialAntiga As String
    Dim registos As Long
    On Error GoTo TrataErro
    
    ' Se o lote para a entidade n�o est� bloqueado
    Set rs = New ADODB.recordset
    sSql = "SELECT * FROM fa_lot_lock_area WHERE cod_efr = " & lCodEfr
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexaoSecundaria
    If rs.RecordCount > 0 Then
        BG_Mensagem mediMsgBox, "Os registos que est� a tentar aceder est�o bloqueados", vbOKOnly + vbExclamation
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If
    rs.Close
    Set rs = Nothing
    
    estrutLotes(lote).estrutRequis(linha).dt_ini_real = novaData
    FgRequisicoes.TextMatrix(linha, lcolDtIniReal) = novaData
    'inicia as transacoes
    gConexaoSecundaria.BeginTrans
    registos = 0
    
    'FA_LIN_FACT
    sSql = "UPDATE fa_lin_fact SET dt_ini_real = " & BL_TrataDataParaBD(estrutLotes(lote).estrutRequis(linha).dt_ini_real)
    sSql = sSql & " WHERE serie_fac = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).serie_fac)
    sSql = sSql & " AND n_fac = " & BL_TrataStringParaBD(CStr(estrutLotes(lote).estrutRequis(linha).n_fac))
    sSql = sSql & " AND t_episodio ='SISLAB' "
    sSql = sSql & " AND episodio = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).episodio)
    sSql = sSql & " AND nr_req_ars = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).nr_req_ars)
    gConexaoSecundaria.Execute sSql, registos
    If registos <= 0 Then
        GoTo TrataErro
    End If
    
    'FA_MOVI_FACT
    sSql = "UPDATE fa_movi_fact SET dt_ini_real = " & BL_TrataDataParaBD(estrutLotes(lote).estrutRequis(linha).dt_ini_real)
    sSql = sSql & " WHERE t_episodio ='SISLAB' "
    sSql = sSql & " AND episodio = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).episodio)
    sSql = sSql & " AND nr_req_ars = " & BL_TrataStringParaBD(estrutLotes(lote).estrutRequis(linha).nr_req_ars)
    gConexaoSecundaria.Execute sSql, registos
    If registos <= 0 Then
        GoTo TrataErro
    End If
    
    
    gConexaoSecundaria.CommitTrans
Exit Sub
TrataErro:
    gConexaoSecundaria.RollbackTrans
    BG_LogFile_Erros "Erro ao alterar data :" & Err.Description & vbCrLf & sSql, Me.Name, "AlteraDtIniReal", True
    Exit Sub
    Resume Next
End Sub


Private Sub TvLotes_Click()
    Dim i As Integer
    If TvLotes.Nodes.Count > 0 Then
        loteActivo = TvLotes.SelectedItem.Index
        If loteActivo <= totalLotes And loteActivo > 0 Then
            FgRequisicoes.rows = 2
            For i = 0 To FgRequisicoes.Cols - 1
                FgRequisicoes.TextMatrix(1, i) = ""
            Next
            For i = 1 To estrutLotes(loteActivo).totalEstrutRequis
                FgRequisicoes.TextMatrix(i, lColnumero) = i
                FgRequisicoes.TextMatrix(i, lColDoente) = estrutLotes(loteActivo).estrutRequis(i).doente
                FgRequisicoes.TextMatrix(i, lColCredencial) = estrutLotes(loteActivo).estrutRequis(i).nr_req_ars
                FgRequisicoes.TextMatrix(i, lColEpisodio) = estrutLotes(loteActivo).estrutRequis(i).episodio
                FgRequisicoes.TextMatrix(i, lColNome) = estrutLotes(loteActivo).estrutRequis(i).nome_doente
                FgRequisicoes.TextMatrix(i, lColIsento) = estrutLotes(loteActivo).estrutRequis(i).cod_isen_doe
                FgRequisicoes.TextMatrix(i, lcolDtIniReal) = estrutLotes(loteActivo).estrutRequis(i).dt_ini_real
                FgRequisicoes.AddItem ""
            Next
        End If
    End If

End Sub

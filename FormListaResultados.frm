VERSION 5.00
Begin VB.Form FormListagemResultados 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   2880
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6885
   Icon            =   "FormListaResultados.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2880
   ScaleWidth      =   6885
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Resultados"
      Height          =   855
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   6615
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   3000
         TabIndex        =   10
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   1440
         TabIndex        =   9
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "e "
         Height          =   255
         Left            =   2760
         TabIndex        =   11
         Top             =   360
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "Validados entre"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "An�lise"
      Height          =   1695
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   6615
      Begin VB.CommandButton BtPesqRapP 
         Height          =   315
         Left            =   5880
         Picture         =   "FormListaResultados.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1320
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapC 
         Height          =   315
         Left            =   5880
         Picture         =   "FormListaResultados.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   840
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapS 
         Height          =   315
         Left            =   5880
         Picture         =   "FormListaResultados.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   15
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   360
         Width           =   375
      End
      Begin VB.TextBox EcDescrAnaS 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2280
         TabIndex        =   6
         Top             =   360
         Width           =   3615
      End
      Begin VB.TextBox EcCodAnaS 
         Height          =   285
         Left            =   1440
         TabIndex        =   5
         Top             =   360
         Width           =   855
      End
      Begin VB.TextBox EcCodAnaC 
         Height          =   285
         Left            =   1440
         TabIndex        =   4
         Top             =   840
         Width           =   855
      End
      Begin VB.TextBox EcCodAnaP 
         Height          =   285
         Left            =   1440
         TabIndex        =   3
         Top             =   1320
         Width           =   855
      End
      Begin VB.TextBox EcDescrAnaC 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2280
         TabIndex        =   2
         Top             =   840
         Width           =   3615
      End
      Begin VB.TextBox EcDescrAnaP 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2280
         TabIndex        =   1
         Top             =   1320
         Width           =   3615
      End
      Begin VB.Label Label5 
         Caption         =   "Perfil"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "Complexa"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Simples"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   360
         Width           =   615
      End
   End
End
Attribute VB_Name = "FormListagemResultados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim NRegistos As Long
Dim NAnaSMarc As Long
Dim NAnaCMarc As Long
Dim NAnaSReal As Long
Dim NAnaCReal As Long
Dim NAnaSReq As Long
Dim NAnaCReq As Long
Dim NAnaTabela As Long

Public rs As ADODB.recordset

Sub Preenche_Listagem()
    
    Dim sql As String
    Dim continua As Boolean
            
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estat�stica por Laborat�rio") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial de valida��o dos resultados.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final de valida��o dos resultados.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    If EcCodAnaS.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a an�lise simples para o qual deseja listar os resultados.", vbOKOnly + vbExclamation, App.ProductName)
        EcCodAnaS.SetFocus
        Exit Sub
    End If
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("ListagemResultados", "Listagem de Resultados", crptToPrinter)
    Else
        continua = BL_IniciaReport("ListagemResultados", "Listagem de Resultados", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    Call Cria_TmpRec_Listagem
    
    PreencheTabelaTemporaria
    
'    If NAnaTabela = 0 Then
'        Call BG_Mensagem(mediMsgBox, "N�o foram encontradas an�lises requisitadas para iniciar um Relat�rio", vbOKOnly + vbExclamation, App.ProductName)
'        Exit Sub
'    End If
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT N_REQ, DT_CHEGA,DT_MARCA,DT_VAL,NOME_UTE,RESULT,DESCR_OBS,cod_perfil,cod_ana_c,cod_ana_s " & _
        " FROM SL_CR_RES" & gNumeroSessao & " SL_CR_RES "

    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    Report.formulas(2) = "Perfil=" & BL_TrataStringParaBD("" & EcDescrAnaP.Text)
    Report.formulas(3) = "Complexa=" & BL_TrataStringParaBD("" & EcDescrAnaC.Text)
    Report.formulas(4) = "Simples=" & BL_TrataStringParaBD("" & EcDescrAnaS.Text)
    
    Me.SetFocus
    
    Call BL_ExecutaReport
 
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    Call BL_RemoveTabela("SL_CR_RES" & gNumeroSessao)
    
End Sub

Sub PreencheTabelaTemporaria()
    Dim sql As String
    Dim SqlC As String
    Dim RsTipoRes As ADODB.recordset
    Dim TipoRes As Integer
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
    'Data de valida��o
    SqlC = " AND r.dt_val between " & BL_TrataDataParaBD(EcDtIni.Text) & " and " & BL_TrataDataParaBD(EcDtFim.Text)
    
    'An�lise
    If EcCodAnaS.Text <> "" Then
        SqlC = SqlC & " AND r.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS.Text)
    End If
    If EcCodAnaC.Text <> "" Then
        SqlC = SqlC & " AND r.cod_ana_c = " & BL_TrataStringParaBD(EcCodAnaC.Text)
    End If
    If EcCodAnaP.Text <> "" Then
        SqlC = SqlC & " AND r.cod_perfil = " & BL_TrataStringParaBD(EcCodAnaP.Text)
    End If
    
    If EcCodAnaS.Text <> "" Then
        Set RsTipoRes = New ADODB.recordset
        sql = "select t_result from sl_ana_s where cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS.Text)
        RsTipoRes.CursorLocation = adUseServer
        RsTipoRes.CursorType = adOpenStatic
        RsTipoRes.Open sql, gConexao
        If RsTipoRes.RecordCount > 0 Then
            TipoRes = RsTipoRes!t_result
        End If
        Select Case TipoRes
            Case 0, 1, 5
                sql = " insert into sl_cr_res" & gNumeroSessao & " (n_req,dt_chega, dt_marca, dt_val, nome_ute,result,descr_obs) " & _
                      " select s.n_req,s.dt_chega, r.dt_chega,r.dt_val, nome_ute,result, descr_obs_ana " & _
                      " from sl_realiza r, sl_res_alfan a, " & tabela_aux & " i , sl_requis s, sl_obs_ana o " & _
                      " where i.seq_utente = s.seq_utente and " & _
                      " s.n_req = r.n_req and r.seq_realiza = a.seq_realiza and " & _
                      " r.seq_realiza = o.seq_realiza(+) "
                sql = sql & SqlC
            Case 2
                sql = " insert into sl_cr_res" & gNumeroSessao & " (n_req,dt_chega, dt_marca, dt_val, nome_ute,result,descr_obs) " & _
                      " select s.n_req,s.dt_chega, r.dt_chega,r.dt_val,nome_ute,descr_frase,descr_obs_ana " & _
                      " from sl_realiza r, sl_res_frase a, " & tabela_aux & " i , sl_requis s, sl_dicionario d, sl_obs_ana o " & _
                      " where i.seq_utente = s.seq_utente and " & _
                      " s.n_req = r.n_req and r.seq_realiza = a.seq_realiza and " & _
                      " a.cod_frase = d.cod_frase and " & _
                      " r.seq_realiza = o.seq_realiza(+) "
                sql = sql & SqlC
            Case Else
        End Select
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
        
    End If
    
End Sub

Sub Cria_TmpRec_Listagem()

    Dim TmpRec(10) As DefTable
    
    TmpRec(1).NomeCampo = "N_REQ"
    TmpRec(1).tipo = "INTEGER"
    TmpRec(1).tamanho = 9
    
    TmpRec(2).NomeCampo = "DT_CHEGA"
    TmpRec(2).tipo = "DATE"
    
    TmpRec(3).NomeCampo = "DT_MARCA"
    TmpRec(3).tipo = "DATE"
    
    TmpRec(4).NomeCampo = "DT_VAL"
    TmpRec(4).tipo = "DATE"
    
    TmpRec(5).NomeCampo = "NOME_UTE"
    TmpRec(5).tipo = "STRING"
    TmpRec(5).tamanho = 80
    
    TmpRec(6).NomeCampo = "RESULT"
    TmpRec(6).tipo = "STRING"
    TmpRec(6).tamanho = 4000
    
    TmpRec(7).NomeCampo = "DESCR_OBS"
    TmpRec(7).tipo = "STRING"
    TmpRec(7).tamanho = 4000
    
    TmpRec(8).NomeCampo = "COD_ANA_S"
    TmpRec(8).tipo = "STRING"
    TmpRec(8).tamanho = 9
    
    TmpRec(9).NomeCampo = "COD_ANA_C"
    TmpRec(9).tipo = "STRING"
    TmpRec(9).tamanho = 9
    
    TmpRec(10).NomeCampo = "COD_PERFIL"
    TmpRec(10).tipo = "STRING"
    TmpRec(10).tamanho = 9
    
    Call BL_CriaTabela("SL_CR_RES" & gNumeroSessao, TmpRec)
    
End Sub

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Private Sub BtPesqRapC_Click()
    
    Dim ChavesPesq(1 To 3) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 3) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_c"
    CamposEcran(1) = "cod_ana_c"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_c"
    CamposEcran(2) = "descr_ana_c"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    ChavesPesq(3) = "descr_sgr_ana"
    CamposEcran(3) = "descr_sgr_ana"
    Tamanhos(3) = 2000
    Headers(3) = "SubGrupo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_c, sl_sgr_ana "
    CWhere = "sl_ana_c.sgr_ana = sl_sgr_ana.cod_sgr_ana(+) "
    If EcCodAnaP.Text <> "" Then
        CWhere = CWhere & " and cod_ana_c in (select cod_analise from sl_ana_perfis where cod_perfis = " & BL_TrataStringParaBD(EcCodAnaP)
    End If
    CampoPesquisa = "descr_ana_c"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Complexas")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaC.Text = resultados(2)
            EcCodAnaC.Text = resultados(1)
'            EcCodAna.Text = Resultados(1)
'            EcDescrAna.Text = Resultados(2)
            EcCodAnaC.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises complexas", vbExclamation, "ATEN��O"
        EcCodAnaC.SetFocus
    End If

End Sub

Private Sub BtPesqRapP_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_perfis"
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_perfis"
    CampoPesquisa = "descr_perfis"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Perfis")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAnaP.Text = resultados(1)
            EcDescrAnaP.Text = resultados(2)
'            EcCodAna.Text = Resultados(1)
'            EcDescrAna.Text = Resultados(2)
            EcCodAnaP.Enabled = True
            EcCodAnaP.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem perfis", vbExclamation, "ATEN��O"
        EcCodAnaP.SetFocus
    End If

End Sub


Private Sub BtPesqRapS_Click()
    
    Dim ChavesPesq(1 To 3) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 3) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_s"
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    ChavesPesq(3) = "descr_sgr_ana"
    CamposEcran(3) = "descr_sgr_ana"
    Tamanhos(3) = 2000
    Headers(3) = "SubGrupo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_s, sl_sgr_ana "
    CWhere = "sl_ana_s.sgr_ana = sl_sgr_ana.cod_sgr_ana(+) "
    If EcCodAnaC.Text <> "" Then
        CWhere = CWhere & " and cod_ana_s in (select cod_membro from sl_membro where cod_ana_c = " & BL_TrataStringParaBD(EcCodAnaC.Text)
    End If
    If EcCodAnaP.Text <> "" Then
        CWhere = CWhere & " and cod_ana_s in (select cod_analise from sl_ana_perfis where cod_perfis = " & BL_TrataStringParaBD(EcCodAnaP.Text)
    End If
    CampoPesquisa = "descr_ana_s"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Simples")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaS.Text = resultados(2)
            EcCodAnaS.Text = resultados(1)
'            EcCodAna.Text = Resultados(1)
'            EcDescrAna.Text = Resultados(2)
            EcCodAnaS.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises simples", vbExclamation, "ATEN��O"
        EcCodAnaS.SetFocus
    End If

End Sub

Private Sub EcCodAnaC_Validate(Cancel As Boolean)
    If EcCodAnaC.Text <> "" Then
        Dim RsDescrAnaC As ADODB.recordset
        Set RsDescrAnaC = New ADODB.recordset
        EcCodAnaC.Text = UCase(EcCodAnaC.Text)
        With RsDescrAnaC
            .Source = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c= " & UCase(BL_TrataStringParaBD(EcCodAnaC.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaC.RecordCount > 0 Then
            EcDescrAnaC.Text = "" & RsDescrAnaC!Descr_Ana_C
        Else
            Cancel = True
            EcDescrAnaC.Text = ""
            BG_Mensagem mediMsgBox, "An�lise complexa inexistente!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
            EcCodAnaC.Text = ""
        End If
        RsDescrAnaC.Close
        Set RsDescrAnaC = Nothing
    Else
        EcCodAnaC.Text = ""
        EcDescrAnaC.Text = ""
    End If
End Sub

Private Sub EcCodAnaP_Validate(Cancel As Boolean)
    If EcCodAnaP.Text <> "" Then
        Dim RsDescrAnaP As ADODB.recordset
        Set RsDescrAnaP = New ADODB.recordset
        EcCodAnaP.Text = UCase(EcCodAnaP.Text)
        With RsDescrAnaP
            .Source = "SELECT descr_perfis FROM sl_perfis WHERE cod_perfis= " & UCase(BL_TrataStringParaBD(EcCodAnaP.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaP.RecordCount > 0 Then
            EcDescrAnaP.Text = "" & RsDescrAnaP!descr_perfis
        Else
            Cancel = True
            EcDescrAnaP.Text = ""
            BG_Mensagem mediMsgBox, "Perfil inexistente!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
            EcCodAnaP.Text = ""
        End If
        RsDescrAnaP.Close
        Set RsDescrAnaP = Nothing
    Else
        EcCodAnaP.Text = ""
        EcDescrAnaP.Text = ""
    End If
End Sub

Private Sub EcCodAnaS_Validate(Cancel As Boolean)
    If EcCodAnaS.Text <> "" Then
        
        Dim RsDescrAnaS As ADODB.recordset
        Set RsDescrAnaS = New ADODB.recordset
        
        EcCodAnaS.Text = UCase(EcCodAnaS.Text)
        With RsDescrAnaS
            .Source = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s= " & UCase(BL_TrataStringParaBD(EcCodAnaS.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaS.RecordCount > 0 Then
            EcDescrAnaS.Text = "" & RsDescrAnaS!descr_ana_s
        Else
            Cancel = True
            EcDescrAnaS.Text = ""
            BG_Mensagem mediMsgBox, "An�lise simples inexistente!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
            EcCodAnaS.Text = ""
        End If
        RsDescrAnaS.Close
        Set RsDescrAnaS = Nothing
    Else
        EcCodAnaS.Text = ""
        EcDescrAnaS.Text = ""
    End If
End Sub


Private Sub EcCodProveniencia_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodEFR_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Listagem de Resultados"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 6975
    Me.Height = 3360 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcCodAnaS
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Listagem Resultados")
    
    Set FormListagemResultados = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    EcCodAnaS.Text = ""
    EcCodAnaC.Text = ""
    EcCodAnaP.Text = ""
    EcDescrAnaS.Text = ""
    EcDescrAnaC.Text = ""
    EcDescrAnaP.Text = ""

End Sub

Sub DefTipoCampos()

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Listagem
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Listagem

End Sub




Private Sub OptComplexas_Click()
    EcCodAnaS.Text = ""
    EcDescrAnaS.Text = ""
    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    
    EcCodAnaC.Enabled = True
    BtPesqRapC.Enabled = True
    
    EcCodAnaP.Text = ""
    EcDescrAnaP.Text = ""
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    
    EcCodAnaC.SetFocus
End Sub

Private Sub OptPerfis_Click()
    EcCodAnaS.Text = ""
    EcDescrAnaS.Text = ""
    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    
    EcCodAnaC.Text = ""
    EcDescrAnaC.Text = ""
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    
    EcCodAnaP.Enabled = True
    BtPesqRapP.Enabled = True
    
    EcCodAnaP.SetFocus
End Sub

Private Sub OptSimples_Click()
    EcCodAnaS.Enabled = True
    BtPesqRapS.Enabled = True
    
    EcCodAnaC.Text = ""
    EcDescrAnaC.Text = ""
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    
    EcCodAnaP.Text = ""
    EcDescrAnaP.Text = ""
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    
    EcCodAnaS.SetFocus
End Sub



VERSION 5.00
Begin VB.Form FormPergTubo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   4470
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6390
   Icon            =   "FormPergTubo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4470
   ScaleWidth      =   6390
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrDefeito 
      Height          =   4455
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6375
      Begin VB.CommandButton BtSim 
         Caption         =   "&Marcar Tubos"
         Height          =   375
         Left            =   1440
         TabIndex        =   5
         Top             =   3360
         Width           =   1455
      End
      Begin VB.PictureBox PicTubo 
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         Height          =   480
         Left            =   120
         Picture         =   "FormPergTubo.frx":000C
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   240
         Width           =   480
      End
      Begin VB.CommandButton BtNao 
         Caption         =   "&N�o Marcar"
         Height          =   375
         Left            =   3360
         TabIndex        =   3
         Top             =   3360
         Width           =   1455
      End
      Begin VB.CheckBox CkUltima 
         Caption         =   "N�o &perguntar de novo."
         Height          =   255
         Left            =   240
         TabIndex        =   2
         ToolTipText     =   $"FormPergTubo.frx":08D6
         Top             =   4050
         Value           =   1  'Checked
         Width           =   2895
      End
      Begin VB.ListBox ListaT 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2220
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   1
         Top             =   840
         Width           =   6135
      End
      Begin VB.Label LblInfo 
         Caption         =   "()"
         Height          =   375
         Left            =   720
         TabIndex        =   6
         Top             =   240
         Width           =   5415
      End
      Begin VB.Line Line1 
         BorderColor     =   &H8000000C&
         X1              =   45
         X2              =   6360
         Y1              =   3945
         Y2              =   3945
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000009&
         BorderWidth     =   2
         X1              =   45
         X2              =   6360
         Y1              =   3960
         Y2              =   3960
      End
   End
End
Attribute VB_Name = "FormPergTubo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

Dim gTexto() As String
Dim gLista() As String
Dim gEscolha As Integer
Dim gUltima As Boolean

Function PerguntaTubo(Tipo As TipoPergunta, UltimaVez As Boolean, descrAna As String, Texto() As String, ByRef EscolhaL() As String) As Integer
    Dim i As Integer
    Dim k As Integer
    Dim Txt() As String
    Dim Txt1() As String
    
    
    ReDim gTexto(UBound(Texto))
    For i = 1 To UBound(Texto)
        gTexto(i) = Texto(i)
    Next i
    gEscolha = -1
    FormPergTubo.Width = 6375
    FormPergTubo.Height = 4875
    FormPergTubo.Caption = descrAna
    
    If Tipo = TipoPergunta.ConfirmarDefeito Then
        
        CkUltima.Value = UltimaVez
        LblInfo.Caption = "ATEN��O: N�o � possivel registar a(s) seguinte(s) an�lise(s) sem marcar o seu tubo respectivo :"
        
        For i = 1 To UBound(Texto)
            If Trim(Texto(i)) <> "" Then
                Txt = Split(Texto(i), ";")
                ListaT.AddItem Txt(2)
                ListaT.Selected(ListaT.ListCount - 1) = True
            End If
        Next i
        
        BtSim.Caption = "&Marcar Tubos"
        BtNao.Caption = "&N�o Marcar"
        ListaT.ToolTipText = "Pode escolher quais os tubos a marcar (ir� condicionar a marca��o das an�lises)."
        BtSim.ToolTipText = "Marcar os tubos seleccionados."
        BtNao.ToolTipText = "N�o marcar os tubos, e por conseguinte n�o marcar as an�lises associadas."
        
        'FormPergTubo.Show vbModal
        gEscolha = 0
        Unload Me
    
        UltimaVez = gUltima
        PerguntaTubo = gEscolha
        
        If Tipo = TipoPergunta.ConfirmarDefeito Then
            ReDim EscolhaL(UBound(gLista))
            For i = 1 To UBound(gLista)
                EscolhaL(i) = gLista(i)
            Next i
        End If
            
        gEscolha = 0
        Unload Me
        Set FormPergTubo = Nothing

    ElseIf Tipo = TipoPergunta.ConfirmarDtChega Then
        
        If gF_REQUIS = 1 Then
        
        If FormGestaoRequisicao.EcDataChegada.text <> "" And gLAB <> "HFM" And gTipoInstituicao = "HOSPITALAR" Then
            LblInfo.Caption = " Deseja efectuar a chegada do(s) seguinte(s) tubo(s) com a data de hoje (" & Bg_DaData_ADO & ") ?"
    
            For i = 1 To UBound(Texto)
                For k = 1 To UBound(EscolhaL)
                    If InStr(1, Texto(i), EscolhaL(k)) <> 0 Then
                        If Trim(Texto(i)) <> "" Then
                            Txt = Split(Texto(i), ";")
                            Txt1 = Split(Txt(2), "-")
                            ListaT.AddItem Txt1(0)
                            ListaT.Selected(ListaT.ListCount - 1) = True
                        End If
                    End If
                Next k
            Next i
    
            BtSim.Caption = "&Sim"
            BtNao.Caption = "&N�o"
            ListaT.ToolTipText = "Pode escolher quais os tubos quer dar chegada."
            BtSim.ToolTipText = "Colocar a data de hoje na data de chegada dos tubos."
            BtNao.ToolTipText = "N�o efectuar a chegada dos tubos."
            
            FormPergTubo.Show vbModal
        
            UltimaVez = gUltima
            PerguntaTubo = gEscolha
            
            If Tipo = TipoPergunta.ConfirmarDefeito Then
                ReDim EscolhaL(UBound(gLista))
                For i = 1 To UBound(gLista)
                    EscolhaL(i) = gLista(i)
                Next i
            End If
                
            Set FormPergTubo = Nothing
        ElseIf gLAB <> "HFM" And gTipoInstituicao = "PRIVADA" Then
            LblInfo.Caption = " Deseja efectuar a chegada do(s) seguinte(s) tubo(s) com a data de hoje (" & Bg_DaData_ADO & ") ?"
    
            For i = 1 To UBound(Texto)
                For k = 1 To UBound(EscolhaL)
                    If InStr(1, Texto(i), EscolhaL(k)) <> 0 Then
                        If Trim(Texto(i)) <> "" Then
                            Txt = Split(Texto(i), ";")
                            Txt1 = Split(Txt(2), "-")
                            ListaT.AddItem Txt1(0)
                            ListaT.Selected(ListaT.ListCount - 1) = True
                        End If
                    End If
                Next k
            Next i
    
            BtSim.Caption = "&Sim"
            BtNao.Caption = "&N�o"
            ListaT.ToolTipText = "Pode escolher quais os tubos quer dar chegada."
            BtSim.ToolTipText = "Colocar a data de hoje na data de chegada dos tubos."
            BtNao.ToolTipText = "N�o efectuar a chegada dos tubos."
            
            FormPergTubo.Show vbModal
        
            UltimaVez = gUltima
            PerguntaTubo = gEscolha
            
            If Tipo = TipoPergunta.ConfirmarDefeito Then
                ReDim EscolhaL(UBound(gLista))
                For i = 1 To UBound(gLista)
                    EscolhaL(i) = gLista(i)
                Next i
            End If
                
            Set FormPergTubo = Nothing
        Else
'            For i = 0 To listat.ListCount - 1
'                listat.Selected(i) = False
'            Next i
            
            UltimaVez = gUltima
            PerguntaTubo = -1
            
        End If
        
        ElseIf gF_REQCONS = 1 Then
        If FormGesReqCons.EcDataChegada.text <> "" And gLAB <> "HFM" Then
            LblInfo.Caption = " Deseja efectuar a chegada do(s) seguinte(s) tubo(s) com a data de hoje (" & Bg_DaData_ADO & ") ?"
    
            For i = 1 To UBound(Texto)
                For k = 1 To UBound(EscolhaL)
                    If InStr(1, Texto(i), EscolhaL(k)) <> 0 Then
                        If Trim(Texto(i)) <> "" Then
                            Txt = Split(Texto(i), ";")
                            Txt1 = Split(Txt(2), "-")
                            ListaT.AddItem Txt1(0)
                            ListaT.Selected(ListaT.ListCount - 1) = True
                        End If
                    End If
                Next k
            Next i
    
            BtSim.Caption = "&Sim"
            BtNao.Caption = "&N�o"
            ListaT.ToolTipText = "Pode escolher quais os tubos quer dar chegada."
            BtSim.ToolTipText = "Colocar a data de hoje na data de chegada dos tubos."
            BtNao.ToolTipText = "N�o efectuar a chegada dos tubos."
            
            FormPergTubo.Show vbModal
        
            UltimaVez = gUltima
            PerguntaTubo = gEscolha
            
            If Tipo = TipoPergunta.ConfirmarDefeito Then
                ReDim EscolhaL(UBound(gLista))
                For i = 1 To UBound(gLista)
                    EscolhaL(i) = gLista(i)
                Next i
            End If
                
            Set FormPergTubo = Nothing
        Else
'            For i = 0 To listat.ListCount - 1
'                listat.Selected(i) = False
'            Next i
            
            UltimaVez = gUltima
            PerguntaTubo = -1
            
        End If
        End If
    End If


    
End Function


Private Sub BtNao_Click()
    Dim i As Integer
    
    For i = 0 To ListaT.ListCount - 1
        ListaT.Selected(i) = False
    Next i
    gEscolha = -1
    Unload Me
End Sub



Private Sub BtSim_Click()
    gEscolha = 0
    Unload Me
End Sub



Private Sub CkUltima_Click()
    If CkUltima.Value = 0 Then
        gUltima = False
    Else
        gUltima = True
    End If
End Sub


Private Sub Form_Activate()
    BtSim.SetFocus
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim k As Integer
    Dim i As Integer
    Dim Txt() As String
    
    ReDim gLista(0)
    k = 0
    For i = 0 To ListaT.ListCount - 1
        If ListaT.Selected(i) = True Then
            k = k + 1
            ReDim Preserve gLista(k)
            Txt = Split(gTexto(i + 1), ";")
            gLista(k) = Txt(0) & ";" & Txt(1)
        End If
    Next i

    If gF_REQUIS = 1 Then
        FormGestaoRequisicao.Enabled = True
    ElseIf gF_REQUIS_PRIVADO = 1 Then
        FormGestaoRequisicaoPrivado.Enabled = True
    ElseIf gF_REQCONS = 1 Then
        FormGesReqCons.Enabled = True
    End If
End Sub








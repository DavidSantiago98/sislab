VERSION 5.00
Begin VB.Form FormNEtiq 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormNEtiq"
   ClientHeight    =   4245
   ClientLeft      =   2760
   ClientTop       =   3705
   ClientWidth     =   4710
   Icon            =   "FormNEtiq.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4245
   ScaleWidth      =   4710
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Caption         =   "Imprimir etiquetas para"
      Height          =   735
      Left            =   120
      TabIndex        =   9
      Top             =   2760
      Width           =   4455
      Begin VB.ComboBox CmbTipoImp 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   240
         Width           =   4215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Impressora"
      Height          =   735
      Left            =   120
      TabIndex        =   8
      Top             =   3480
      Width           =   4455
      Begin VB.ComboBox CmbPrinters 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   240
         Width           =   4215
      End
   End
   Begin VB.Frame FrNEtiq 
      Height          =   2655
      Left            =   120
      TabIndex        =   5
      Top             =   0
      Width           =   4455
      Begin VB.CheckBox CkResumo 
         Caption         =   "Imprimir folha resumo da requisi��o"
         Height          =   195
         Left            =   960
         TabIndex        =   12
         Top             =   1560
         Width           =   2895
      End
      Begin VB.TextBox EcCopias 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2520
         TabIndex        =   11
         Tag             =   "131(3,0)"
         Text            =   "1"
         Top             =   840
         Width           =   1095
      End
      Begin VB.TextBox EcAdm 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2520
         MaxLength       =   3
         TabIndex        =   0
         Tag             =   "131(3,0)"
         Text            =   "1"
         Top             =   1200
         Width           =   855
      End
      Begin VB.CommandButton BtCancNetiq 
         Caption         =   "&Fechar"
         Height          =   375
         Left            =   2280
         TabIndex        =   2
         Top             =   1920
         Width           =   1095
      End
      Begin VB.CommandButton BtOkNetiq 
         Caption         =   "&Confimar"
         Height          =   375
         Left            =   960
         TabIndex        =   1
         Top             =   1920
         Width           =   1095
      End
      Begin VB.Label LbDataConclusao 
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   2280
         Width           =   3735
      End
      Begin VB.Label LaCopiasTubos 
         Caption         =   "(Copias de etiquetas para tubos                          )"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   840
         Width           =   3975
      End
      Begin VB.Label Label1 
         Caption         =   "N� de etiquetas administrativas "
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   1200
         Width           =   2415
      End
      Begin VB.Label LaDescr 
         Caption         =   "LaDescr"
         Height          =   495
         Left            =   240
         TabIndex        =   6
         Top             =   240
         Width           =   3975
      End
   End
End
Attribute VB_Name = "FormNEtiq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 17/04/2002
' T�cnico Paulo Costa

Private Sub BtCancNetiq_Click()
    
    EcAdm = "-1"
    Unload Me

End Sub

Private Sub BtOkNetiq_Click()
    
    Unload Me

End Sub
 
Private Sub CmbTipoImp_Click()
    
    Select Case CmbTipoImp.ListIndex
        Case 0
            'Tubos e Administrativos
            LaDescr.Visible = True
            Label1.Visible = True
            If LaDescr.caption <> "Nenhuma etiqueta para tubos foi gerada" Then
                LaCopiasTubos.Visible = True
                EcCopias.Visible = True
            Else
                LaCopiasTubos.Visible = False
                EcCopias.Visible = False
            End If
        Case 1
            'Administrativos
            LaDescr.Visible = False
            LaCopiasTubos.Visible = False
            EcCopias.Visible = False
            Label1.Visible = True
        Case 2
            'Tubos
            LaDescr.Visible = True
            Label1.Visible = False
            If LaDescr.caption <> "Nenhuma etiqueta para tubos foi gerada" Then
                LaCopiasTubos.Visible = True
                EcCopias.Visible = True
            Else
                LaCopiasTubos.Visible = False
                EcCopias.Visible = False
            End If
        Case 3
            'Separa��o de Soros
            LaDescr.Visible = True
            Label1.Visible = False
            If LaDescr.caption <> "Nenhuma etiqueta para tubos foi gerada" Then
                LaCopiasTubos.Visible = True
                EcCopias.Visible = True
            Else
                LaCopiasTubos.Visible = False
                EcCopias.Visible = False
            End If
    End Select
      
End Sub

Private Sub EcAdm_GotFocus()
    
    EcAdm.SelStart = 0
    EcAdm.SelLength = Len(EcAdm)

End Sub

Private Sub EcAdm_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, EcAdm

End Sub

Private Sub Form_Activate()
    
    BtOkNetiq.SetFocus

End Sub

Private Sub Form_Load()
    Dim impress  As String
    Dim impress2  As String
    Dim Index As Long
    Dim i As Integer
    
    If BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ResumoRequisicoes") = "1" Then
        CkResumo.Visible = True
        CkResumo.value = 0
    ElseIf BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ResumoRequisicoes") = "2" Then
        CkResumo.Visible = True
        CkResumo.value = 1
    Else
        CkResumo.Visible = False
        CkResumo.value = 0
    End If
    
    Me.caption = " Etiquetas"
    If gF_REQUIS = 1 Then
        If CLng(FormGestaoRequisicao.EcEtqNTubos) > 1 Then
            LaDescr.caption = "Foram geradas " & FormGestaoRequisicao.EcEtqNTubos & " etiquetas para tubos"
        ElseIf CLng(FormGestaoRequisicao.EcEtqNTubos) = 1 Then
            LaDescr.caption = "Foi gerada " & FormGestaoRequisicao.EcEtqNTubos & " etiqueta para tubos"
        Else
            LaCopiasTubos.Visible = False
            EcCopias.Visible = False
            LaDescr.caption = "Nenhuma etiqueta para tubos foi gerada"
        End If
    ElseIf gF_REQCONS = 1 Then
        If CLng(FormGesReqCons.EcEtqNTubos) > 1 Then
            LaDescr.caption = "Foram geradas " & FormGesReqCons.EcEtqNTubos & " etiquetas para tubos"
        ElseIf CLng(FormGesReqCons.EcEtqNTubos) = 1 Then
            LaDescr.caption = "Foi gerada " & FormGesReqCons.EcEtqNTubos & " etiqueta para tubos"
        Else
            LaCopiasTubos.Visible = False
            EcCopias.Visible = False
            LaDescr.caption = "Nenhuma etiqueta para tubos foi gerada"
        End If
        
    ElseIf gF_FILA_ESPERA = 1 Then
        If CLng(FormFilaEspera.EcEtqNTubos) > 1 Then
            LaDescr.caption = "Foram geradas " & FormFilaEspera.EcEtqNTubos & " etiquetas para tubos"
        ElseIf CLng(FormFilaEspera.EcEtqNTubos) = 1 Then
            LaDescr.caption = "Foi gerada " & FormFilaEspera.EcEtqNTubos & " etiqueta para tubos"
        Else
            LaCopiasTubos.Visible = False
            EcCopias.Visible = False
            LaDescr.caption = "Nenhuma etiqueta para tubos foi gerada"
        End If
    Else
        If gTipoInstituicao = gTipoInstituicaoPrivada Then
            If CLng(FormGestaoRequisicaoPrivado.EcEtqNTubos) > 1 Then
                LaDescr.caption = "Foram geradas " & FormGestaoRequisicaoPrivado.EcEtqNTubos & " etiquetas para tubos"
            ElseIf CLng(FormGestaoRequisicaoPrivado.EcEtqNTubos) = 1 Then
                LaDescr.caption = "Foi gerada " & FormGestaoRequisicaoPrivado.EcEtqNTubos & " etiqueta para tubos"
            Else
                LaCopiasTubos.Visible = False
                EcCopias.Visible = False
                LaDescr.caption = "Nenhuma etiqueta para tubos foi gerada"
            End If
        End If
    End If
    
    If gF_REQUIS = 1 Then
        LbDataConclusao.caption = "Data prevista de conclus�o: " & DevolveDataValidade(FormGestaoRequisicao.EcNumReq)
    ElseIf gF_REQUIS_PRIVADO = 1 Then
        LbDataConclusao.caption = "Data prevista de conclus�o: " & DevolveDataValidade(FormGestaoRequisicaoPrivado.EcNumReq)
    End If
    
    EcAdm = "1"
    
    If Printers.Count = 0 Then
        CmbPrinters.Enabled = False
    Else
        CmbPrinters.Clear
        For i = 0 To Printers.Count - 1
            If InStr(1, UCase(Printers(i).DeviceName), "IN SESSION") > 0 Then
                impress = Mid(Printers(i).DeviceName, 1, InStr(1, UCase(Printers(i).DeviceName), "IN SESSION") - 1)
            Else
                impress = Printers(i).DeviceName
            End If
            
            CmbPrinters.AddItem Printers(i).DeviceName
            If gF_REQUIS = 1 Then
                If InStr(1, UCase(FormGestaoRequisicao.EcPrinterEtiq), "IN SESSION") > 0 Then
                    impress2 = Mid(FormGestaoRequisicao.EcPrinterEtiq, 1, InStr(1, UCase(FormGestaoRequisicao.EcPrinterEtiq), "IN SESSION") - 1)
                Else
                    impress2 = FormGestaoRequisicao.EcPrinterEtiq
                End If
                If Trim(impress2) = Trim(impress) Then
                    Index = i
                End If
            ElseIf gF_REQCONS = 1 Then
                If InStr(1, UCase(FormGesReqCons.EcPrinterEtiq), "IN SESSION") > 0 Then
                    impress2 = Mid(FormGesReqCons.EcPrinterEtiq, 1, InStr(1, UCase(FormGesReqCons.EcPrinterEtiq), "IN SESSION") - 1)
                Else
                    impress2 = FormGestaoRequisicao.EcPrinterEtiq
                End If
                
                If Trim(impress2) = Trim(impress) Then
                    Index = i
                End If
            ElseIf gF_FILA_ESPERA = 1 Then
                If InStr(1, UCase(FormFilaEspera.EcPrinterEtiq), "IN SESSION") > 0 Then
                    impress2 = Mid(FormFilaEspera.EcPrinterEtiq, 1, InStr(1, UCase(FormFilaEspera.EcPrinterEtiq), "IN SESSION") - 1)
                Else
                    impress2 = FormFilaEspera.EcPrinterEtiq
                End If
                If Trim(impress2) = Trim(impress) Then
                    Index = i
                End If
            Else
                If gTipoInstituicao = gTipoInstituicaoPrivada Then
                    If InStr(1, UCase(FormGestaoRequisicaoPrivado.EcPrinterEtiq), "IN SESSION") > 0 Then
                        impress2 = Mid(FormGestaoRequisicaoPrivado.EcPrinterEtiq, 1, InStr(1, UCase(FormGestaoRequisicaoPrivado.EcPrinterEtiq), "IN SESSION") - 1)
                    Else
                        impress2 = FormGestaoRequisicaoPrivado.EcPrinterEtiq
                    End If
                    
                    If Trim(impress2) = Trim(impress) Then
                        Index = i
                    End If
                End If
            End If
        Next i
        
        CmbPrinters.ListIndex = Index
    End If

    CmbTipoImp.Clear
    CmbTipoImp.AddItem "Tubos e fins administrativos"
    CmbTipoImp.AddItem "Fins administrativos"
    CmbTipoImp.AddItem "Tubos"
'    CmbTipoImp.AddItem "Separa��o de Soros"
    CmbTipoImp.ListIndex = 0

    ' Inicializa com o n�mero de grupos da requisi��o.
    If (gNumEtiqAdmin >= 1) Then
        Dim nEtiq As Integer
        If gF_REQUIS = 1 Then
            nEtiq = REQUISICAO_Conta_Grupos_Req(FormGestaoRequisicao.EcNumReq.Text)
        ElseIf gF_REQCONS = 1 Then
            nEtiq = REQUISICAO_Conta_Grupos_Req(FormGesReqCons.EcNumReq.Text)
        ElseIf gF_REQUIS_PRIVADO Then
            nEtiq = REQUISICAO_Conta_Grupos_Req(FormGestaoRequisicaoPrivado.EcNumReq.Text)
        End If
        Select Case nEtiq
            Case 0
                EcAdm.Text = 1
            Case Else
                If gLAB = "BIO" Then
                    EcAdm.Text = 2
                Else
                    EcAdm.Text = nEtiq * gNumEtiqAdmin
                End If
        End Select
    Else
        ' Chave n�o definida.
        If gNumEtiqAdminDefeito >= 0 Then
           EcAdm.Text = gNumEtiqAdminDefeito
        Else
            EcAdm.Text = 1
        End If
    End If

    If (gLAB = cHSMARTA) Then
        
        If (REQUISICAO_Tem_Microbiologia(Trim(FormGestaoRequisicao.EcNumReq.Text))) Then
            Me.EcAdm.Text = 4
        End If
    ElseIf gLAB = "CHVNG" Then
        If (REQUISICAO_Tem_Microbiologia(Trim(FormGestaoRequisicao.EcNumReq.Text))) Then
            Me.EcAdm.Text = 2
        End If
        
    End If

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    
    If UnloadMode = 0 Then
        EcAdm = "-1"
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    If Trim(EcAdm.Text) = "-1" Then
        If gF_REQUIS = 1 Then
            FormGestaoRequisicao.EcEtqTipo = "-1"
        ElseIf gF_REQCONS = 1 Then
            FormGesReqCons.EcEtqTipo = "-1"
        ElseIf gF_FILA_ESPERA = 1 Then
            FormFilaEspera.EcEtqTipo = "-1"
        ElseIf gF_REQUIS_PRIVADO = 1 Then
            FormGestaoRequisicaoPrivado.EcEtqTipo = "-1"
        End If
    Else
        If Trim(EcAdm) <> "" Then
            If gF_REQUIS = 1 Then
                FormGestaoRequisicao.EcEtqNAdm = EcAdm
            ElseIf gF_REQCONS = 1 Then
                FormGesReqCons.EcEtqNAdm = EcAdm
            ElseIf gF_FILA_ESPERA = 1 Then
                FormFilaEspera.EcEtqNAdm = EcAdm
            ElseIf gF_REQUIS_PRIVADO = 1 Then
                FormGestaoRequisicaoPrivado.EcEtqNAdm = EcAdm
            End If
        Else
            If gF_REQUIS = 1 Then
                FormGestaoRequisicao.EcEtqNAdm = "0"
            ElseIf gF_REQCONS = 1 Then
                FormGesReqCons.EcEtqNAdm = "0"
            ElseIf gF_FILA_ESPERA = 1 Then
                FormFilaEspera.EcEtqNAdm = "0"
            ElseIf gF_REQUIS_PRIVADO = 1 Then
                FormGestaoRequisicaoPrivado.EcEtqNAdm = "0"
            End If
        End If
        If Trim(EcCopias) <> "" Then
            If gF_REQUIS = 1 Then
                FormGestaoRequisicao.EcEtqNCopias = EcCopias
            ElseIf gF_REQCONS = 1 Then
                FormGesReqCons.EcEtqNCopias = EcCopias
            ElseIf gF_FILA_ESPERA = 1 Then
                FormFilaEspera.EcEtqNCopias = EcCopias
            ElseIf gF_REQUIS_PRIVADO = 1 Then
                FormGestaoRequisicaoPrivado.EcEtqNCopias = EcCopias
            End If
        Else
            If gF_REQUIS = 1 Then
                FormGestaoRequisicao.EcEtqNCopias = "0"
            ElseIf gF_REQCONS = 1 Then
                FormGesReqCons.EcEtqNCopias = "0"
            ElseIf gF_FILA_ESPERA = 1 Then
                FormFilaEspera.EcEtqNCopias = "0"
            ElseIf gF_REQUIS_PRIVADO = 1 Then
                FormGestaoRequisicaoPrivado.EcEtqNCopias = "0"
            End If
        End If
        If gF_REQUIS = 1 Then
            FormGestaoRequisicao.EcEtqTipo = CmbTipoImp.ListIndex
        ElseIf gF_REQCONS = 1 Then
            FormGesReqCons.EcEtqTipo = CmbTipoImp.ListIndex
        ElseIf gF_FILA_ESPERA = 1 Then
            FormFilaEspera.EcEtqTipo = CmbTipoImp.ListIndex
        ElseIf gF_REQUIS_PRIVADO = 1 Then
            FormGestaoRequisicaoPrivado.EcEtqTipo = CmbTipoImp.ListIndex
        End If
        If CmbPrinters.ListIndex <> -1 Then
            If gF_REQUIS = 1 Then
                FormGestaoRequisicao.EcPrinterEtiq = CmbPrinters.Text
            ElseIf gF_REQCONS = 1 Then
                FormGesReqCons.EcPrinterEtiq = CmbPrinters.Text
            ElseIf gF_FILA_ESPERA = 1 Then
                FormFilaEspera.EcPrinterEtiq = CmbPrinters.Text
            ElseIf gF_REQUIS_PRIVADO = 1 Then
                FormGestaoRequisicaoPrivado.EcPrinterEtiq = CmbPrinters.Text
            End If
        End If
        If gF_REQUIS = 1 Then
            FormGestaoRequisicao.EcImprimirResumo = CStr(CkResumo.value)
        ElseIf gF_REQCONS = 1 Then
            FormGesReqCons.EcImprimirResumo = CStr(CkResumo.value)
        ElseIf gF_FILA_ESPERA = 1 Then
        ElseIf gF_REQUIS_PRIVADO = 1 Then
            FormGestaoRequisicaoPrivado.EcImprimirResumo = CStr(CkResumo.value)
        End If
    End If
    
End Sub


Private Function DevolveDataValidade(n_req As String) As String
    Dim sql As String
    Dim rs As New ADODB.recordset
    
    BL_VerificaConclusaoRequisicao n_req, Bg_DaData_ADO
    
    '----------------------------------
    ' DATA VALIDADE ACTUAL
    sql = "SELECT dt_conclusao FROM sl_requis WHERE n_req = " & n_req
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    If rs.RecordCount > 0 Then
        DevolveDataValidade = BL_HandleNull(rs!dt_conclusao, CDate(1))
    End If
    rs.Close
    Set rs = Nothing
    '----------------------------------

End Function


Attribute VB_Name = "ModuleListItem"
'      .............................
'     .                             .
'    .   Paulo Ferreira 2009.09.29   .
'     .       � 2009 Glintt-HS      .
'      .............................

Option Explicit

' Declare function lib ScreenToClient.
Public Declare Function ScreenToClient Lib "user32" (ByVal hWnd As Long, lpPoint As PointAPI) As Long

' Declare function lib GetKeyState.
Public Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As KeyCodeConstants) As Integer

' Declare function lib SetParent.
Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long

' Declare function lib MapWindowPoints.
Public Declare Function MapWindowPoints Lib "user32" (ByVal hwndFrom As Long, ByVal hwndTo As Long, lppt As Any, ByVal cPoints As Long) As Long

' Declare function lib SendMessage.
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long

' Constant to define listview no item.
Public Const LVI_NOITEM = -1

' Constant to define listview first item.
Public Const LVM_FIRST = &H1000

' Constant to define listview sub item rect.
Public Const LVM_GETSUBITEMRECT = (LVM_FIRST + 56)

' Constant to define listview sub item hit test.
Public Const LVM_SUBITEMHITTEST = (LVM_FIRST + 57)

' Constant to define listview item hit test.
Public Const LVM_HITTEST = (LVM_FIRST + 18)

' Constant to define a listview item RECT state icon.
Public Const LVIR_ICON = 1

' Constant to define a listview item RECT state label.
Public Const LVIR_LABEL = 2

' Constant to define listview hit test info flags.
Public Const LVHT_ONITEMLABEL = &H4
Public Const LVHT_ONITEMICON = &H2
Public Const LVHT_ONITEMSTATEICON = &H8

' Define structure listview hit test info.
Public Type LVHITTESTINFO
    pt As PointAPI
    flags As Long
    iItem As Long
    iSubItem As Long
End Type

' Get listview sub item RECT value.
Public Function ListView_GetSubItemRect(�hWnd� As Long, �iItem� As Long, �iSubItem� As Long, �code� As Long, �prc� As Rect) As Boolean
  
  �prc�.top = �iSubItem�
  �prc�.left = �code�
  ListView_GetSubItemRect = SendMessage(�hWnd�, LVM_GETSUBITEMRECT, ByVal �iItem�, �prc�)
  
End Function

' Get listview sub item hit test.
Public Function ListView_SubItemHitTest(�hWnd� As Long, �plvhti� As LVHITTESTINFO) As Long
  
  ListView_SubItemHitTest = SendMessage(�hWnd�, LVM_SUBITEMHITTEST, Empty, �plvhti�)
  
End Function

' Get listview item hit test.
Public Function ListView_ItemHitTest(�hWnd� As Long, �plvhti� As LVHITTESTINFO) As Long

    ListView_ItemHitTest = SendMessage(�hWnd�, LVM_HITTEST, Empty, �plvhti�)
    
End Function

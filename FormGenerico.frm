VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormGenerico 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "/edrw2"
   ClientHeight    =   5535
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4965
   Icon            =   "FormGenerico.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5535
   ScaleWidth      =   4965
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   4335
      Left            =   120
      TabIndex        =   8
      Top             =   1080
      Width           =   4695
      Begin VB.CommandButton BtInsere 
         Height          =   495
         Left            =   3600
         Picture         =   "FormGenerico.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   3000
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Height          =   495
         Left            =   3600
         Picture         =   "FormGenerico.frx":079E
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   2400
         Width           =   615
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Validar"
         Height          =   375
         Left            =   3600
         TabIndex        =   10
         Top             =   3720
         Width           =   855
      End
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
         Height          =   3735
         Left            =   240
         TabIndex        =   9
         Top             =   360
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   6588
         _Version        =   393216
         Rows            =   30
         Cols            =   3
         FixedCols       =   0
         ScrollBars      =   2
         Appearance      =   0
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Requisi��o"
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1695
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   1200
         TabIndex        =   6
         Top             =   840
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Text3 
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   840
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.TextBox Text4 
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   840
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Label Label2 
         Caption         =   "n_req"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   840
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "cod_ana"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   840
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "p1"
         Height          =   255
         Left            =   360
         TabIndex        =   4
         Top             =   840
         Visible         =   0   'False
         Width           =   1215
      End
   End
End
Attribute VB_Name = "FormGenerico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim CampoDeFocus As Object

' 1 - quando entra no Form;
' 2 - quando est� a procurar alguma coisa.
Dim Estado As Integer

Public Rs As ADODB.Recordset

Private Sub Form_Activate()

    Call EventoActivate
    
End Sub

Private Sub Form_Load()

    Call EventoLoad
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Call EventoUnload

End Sub

Private Sub EventoActivate()

'    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
'    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
'    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
'    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
'    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
'    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
'    BL_Toolbar_BotaoEstado "Remover", "InActivo"
'    BL_Toolbar_BotaoEstado "Anterior", "InActivo"
'    BL_Toolbar_BotaoEstado "Seguinte", "InActivo"
'    BL_Toolbar_BotaoEstado "EstadoAnterior", "InActivo"
    
    Call BG_StackJanelas_Actualiza(Me)
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    Call BL_ToolbarEstadoN(Estado)
    If (Estado = 2) Then
        Call BL_Toolbar_BotaoEstado("Remover", "Activo")
    End If
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
   
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "EstadoAnterior", "InActivo"
    
End Sub

Private Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    
    Call BL_InicioProcessamento(Me, "Inicializar �cran.")
    Call Inicializacoes

    Call DefTipoCampos
    Call PreencheValoresDefeito
    Call BG_ParametrizaPermissoes_ADO(Me.Name)
    
    Estado = 1
    Call BG_StackJanelas_Push(Me)
    Call BL_FimProcessamento(Me)

    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "EstadoAnterior", "InActivo"

End Sub

Private Sub Inicializacoes()

    Me.Caption = "Form Gen�rico"
    Me.Left = 500
    Me.Top = 450
    Me.Width = 5055
    Me.Height = 5910 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    ' Nome da tabela suporte.
    NomeTabela = "sl_credenciais"
    
    Set CampoDeFocus = Me.MSFlexGrid1
    
    NumCampos = 4
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0)
    
    ' Campos da Base de Dados

    CamposBD(0) = "n_req"
    CamposBD(1) = "cod_ana"
    CamposBD(2) = "n_req_orig"
    CamposBD(3) = "p1"
    
    ' Campos do Ecr�

    Set CamposEc(0) = Me.Text2
    Set CamposEc(1) = Me.Text3
    Set CamposEc(2) = Me.Text1
    Set CamposEc(3) = Me.Text4
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "Requisi��o Original"
    
'    ChaveBD = "seq_ana_s"
'    Set ChaveEc = EcCodSequencial
    
'    CamposBDparaListBox = Array("descr_produto")
    
    ' Objectos enabled.
    
    Me.Text1.Enabled = True
    Me.Text2.Enabled = True
    Me.Text3.Enabled = True
    Me.Text4.Enabled = True
    
End Sub

Private Sub DefTipoCampos()

    ' Nada.
    
End Sub

Private Sub PreencheValoresDefeito()
    
    ' Nada.

End Sub

Private Sub EventoUnload()
    
    Call BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    Call BL_ToolbarEstadoN(0)
    
    If (Not Rs Is Nothing) Then
        Rs.Close
        Set Rs = Nothing
    End If
    
End Sub

Private Sub FuncaoEstadoAnterior()

    If (Estado = 2) Then
        Estado = 1
        Call BL_ToolbarEstadoN(Estado)
        
        Call LimpaCampos
        CampoDeFocus.SetFocus
        If (Not Rs Is Nothing) Then
            Rs.Close
            Set Rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Private Sub LimpaCampos()

    Me.SetFocus

    Me.Text1.Text = ""
    Me.Text2.Text = ""
    Me.Text3.Text = ""
    Me.Text4.Text = ""
    DoEvents

End Sub

Private Sub PreencheCampos()
    
    Me.SetFocus
    
    If (Rs.RecordCount = 0) Then
        Call FuncaoLimpar
    Else
        Call BG_PreencheCampoEc_Todos_ADO(Rs, CamposBD, CamposEc)
    End If
    
End Sub

Sub FuncaoLimpar()
            
    On Error GoTo ErrorHandler
    
    'Para os valores de refer�ncia
    Call BG_RollbackTransaction
    Call BG_BeginTransaction
            
    If (Estado = 2) Then
        Call FuncaoEstadoAnterior
    Else
        Call LimpaCampos
        CampoDeFocus.SetFocus
    End If
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_Mensagem(mediMsgBox, "Erro !             ", vbCritical, "Limpar")
            Call BG_LogFile("---------------------" & vbCrLf & _
                            Date & " " & Time & vbCrLf & _
                           "INESPERADO " & vbCrLf & _
                           "FuncaoLimpar (FormGenerico) " & vbCrLf & _
                            Err.Number & ", " & Err.Description)
            Exit Sub
    End Select
End Sub

Sub FuncaoAnterior()

    On Error GoTo ErrorHandler
    
    Call BL_InicioProcessamento(Me, "A pesquisar registo anterior.")
    Rs.MovePrevious
    
    If (Rs.BOF) Then
        Rs.MoveNext
        Call BL_FimProcessamento(Me)
        Call BG_MensagemAnterior
    Else
        'Para os valores de refer�ncia
        Call BG_RollbackTransaction
        Call BG_BeginTransaction
        
        Call LimpaCampos
        Call PreencheCampos
        Call BL_FimProcessamento(Me)
    End If
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_Mensagem(mediMsgBox, "Erro !             ", vbCritical, "Anterior")
            Call BG_LogFile("---------------------" & vbCrLf & _
                            Date & " " & Time & vbCrLf & _
                           "INESPERADO " & vbCrLf & _
                           "FuncaoAnterior (FormGenerico) " & vbCrLf & _
                            Err.Number & ", " & Err.Description)
            Exit Sub
    End Select
End Sub

Sub FuncaoModificar()

    Dim iRes As Integer
        
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If (gMsgResp = vbYes) Then
    
        Call BL_InicioProcessamento(Me, "A modificar registo.")
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        If CkEtiqueta.Value = 2 Then CkEtiqueta.Value = 0
        If CkEtiqOrd.Value = 2 Then CkEtiqOrd.Value = 0
        If CkMarcAuto.Value = 2 Then CkMarcAuto.Value = 0
        iRes = ValidaCamposEc
        
        If (iRes = True) Then
            Call BD_Update
        End If
        
        Call BL_FimProcessamento(Me)
        
    End If

End Sub

' Activado pelo bot�o PROCURAR do MDI Form.

Sub FuncaoProcurar()
'    On Error GoTo ErrorHandler
    
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    'Para os valores de refer�ncia
    Call BG_RollbackTransaction
    Call BG_BeginTransaction
    
    Call BL_InicioProcessamento(Me, "A pesquisar registos.")
    
    Set Rs = New ADODB.Recordset
    
'   SELECT gen�rico com todos os campos.
'    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, _
                                                    CamposBD, _
                                                    SelTotal)
    ' Constroi o crit�rio de pesquisa.
    ' Verifica se h� um crit�rio de pesquisa.
    If ((Len(Me.Text2.Text) = 0) And _
        (Len(Me.Text3.Text) = 0)) Then
        If BG_Mensagem(mediMsgBox, "N�o colocou crit�rios de pesquisa, esta opera��o pode demorar algum tempo." & vbNewLine & "Confirma a selec��o de todos as requisi��es ?", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
            Set Rs = Nothing
            Exit Sub
        End If
        
        If (gSGBD = "ORACLE") Then
            CriterioTabela = "SELECT " & vbCrLf & _
                             "      n_req, " & vbCrLf & _
                             "      cod_ana, " & vbCrLf & _
                             "      n_req_orig, " & vbCrLf & _
                             "      p1 " & vbCrLf & _
                             "FROM " & vbCrLf & _
                             "      " & NomeTabela & " " & vbCrLf & _
                             "ORDER BY " & vbCrLf & _
                             "      LPAD(n_req,10,' ') ASC, " & vbCrLf & _
                             "      LPAD(cod_ana,10,' ') ASC"
        End If
        
        If (gSGBD = "SQL_SERVER") Then
            CriterioTabela = "SELECT " & vbCrLf & _
                             "      n_req, " & vbCrLf & _
                             "      cod_ana, " & vbCrLf & _
                             "      n_req_orig, " & vbCrLf & _
                             "      p1 " & vbCrLf & _
                             "FROM " & vbCrLf & _
                             "      " & NomeTabela & " " & vbCrLf & _
                             "ORDER BY " & vbCrLf & _
                             "      n_req ASC, " & vbCrLf & _
                             "      cod_ana ASC"
        End If
    
    Else
        If (gSGBD = "ORACLE") Then
            CriterioTabela = "SELECT " & vbCrLf & _
                            "      n_req_orig, " & vbCrLf & _
                            "      n_req, " & vbCrLf & _
                            "      cod_ana, " & vbCrLf & _
                            "      p1 " & vbCrLf & _
                            "FROM " & vbCrLf & _
                            "      " & NomeTabela & " " & vbCrLf & _
                            "WHERE " & vbCrLf & _
                            "      n_req LIKE '%" & BG_CvWilcard(Me.Text2.Text) & "%' AND " & vbCrLf & _
                             "      cod_ana LIKE '%" & BG_CvWilcard(Me.Text3.Text) & "%' " & vbCrLf & _
                            "ORDER BY " & vbCrLf & _
                            "      LPAD(n_req,10,' ') ASC, " & vbCrLf & _
                            "      LPAD(cod_ana,10,' ') ASC"
        End If
        
        If (gSGBD = "SQL_SERVER") Then
            CriterioTabela = "SELECT " & vbCrLf & _
                            "      n_req_orig, " & vbCrLf & _
                            "      n_req, " & vbCrLf & _
                            "      cod_ana, " & vbCrLf & _
                            "      p1 " & vbCrLf & _
                            "FROM " & vbCrLf & _
                            "      " & NomeTabela & " " & vbCrLf & _
                            "WHERE " & vbCrLf & _
                            "      n_req LIKE '%" & BG_CvWilcard(Me.Text2.Text) & "%' AND " & vbCrLf & _
                             "      cod_ana LIKE '%" & BG_CvWilcard(Me.Text3.Text) & "%' " & vbCrLf & _
                            "ORDER BY " & vbCrLf & _
                            "      n_req ASC, " & vbCrLf & _
                            "      cod_ana ASC"
        End If
    End If
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    Rs.CursorType = adOpenStatic
    Rs.CursorLocation = adUseClient
    Rs.Open CriterioTabela, gConexao
    
    If (Rs.RecordCount <= 0) Then
        Call BL_FimProcessamento(Me)
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        Call FuncaoLimpar
    Else
        Estado = 2
        Call LimpaCampos
        Call PreencheCampos
                
        Call BL_ToolbarEstadoN(Estado)
        Call BL_Toolbar_BotaoEstado("Remover", "Activo")
        
        Call BL_FimProcessamento(Me)
    End If
      
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "EstadoAnterior", "InActivo"

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_Mensagem(mediMsgBox, "Erro ao procurar registos !        ", vbCritical, "Procurar")
            Call BG_LogFile("---------------------" & vbCrLf & _
                            Date & " " & Time & vbCrLf & _
                           "INESPERADO " & vbCrLf & _
                           "FuncaoProcurar (FormGenerico) " & vbCrLf & _
                            Err.Number & ", " & Err.Description)
            Call BL_FimProcessamento(Me)
            Call FuncaoLimpar
            Exit Sub
    End Select
End Sub

Sub FuncaoSeguinte()

    On Error GoTo ErrorHandler
        
    Call BL_InicioProcessamento(Me, "A pesquisar registo seguinte.")
    Rs.MoveNext
    
    If (Rs.EOF) Then
        Rs.MovePrevious
        Call BL_FimProcessamento(Me)
        Call BG_MensagemSeguinte
    Else
        'Para os valores de refer�ncia
        Call BG_RollbackTransaction
        Call BG_BeginTransaction
        
        Call LimpaCampos
        Call PreencheCampos
        Call BL_FimProcessamento(Me)
    End If
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_Mensagem(mediMsgBox, "Erro !             ", vbCritical, "Seguinte")
            Call BG_LogFile("---------------------" & vbCrLf & _
                            Date & " " & Time & vbCrLf & _
                           "INESPERADO " & vbCrLf & _
                           "FuncaoSeguinte (FormGenerico) " & vbCrLf & _
                            Err.Number & ", " & Err.Description)
            Exit Sub
    End Select
End Sub


Private Function Exp(gr1 As MSFlexGrid) As Integer
    
    On Error Resume Next
    
    Dim i As Integer
    Dim j As Integer

    gr2.Clear
    
    gr2.Cols = 10
    gr2.Rows = 20
    
    ' Copia formata��o.
    gr2.BackColor = gr1.BackColor
    gr2.ForeColor = gr1.ForeColor
    
    For i = 0 To gr2.Cols - 1
        gr2.ColWidth(i) = gr1.ColWidth(i)
        gr2.ColAlignment(i) = gr1.ColAlignment(i)
    Next
    
    ' ---------------------
    
    ' Copia valores.
    For j = 0 To gr2.Rows - 1
        For i = 0 To gr2.Cols - 1
            gr2.TextMatrix(j, i) = gr1.TextMatrix(j, i)
        Next
    Next
    
    ' ---------------------
    
    Exp = 1

End Function




VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormPesqRapidaAvancada 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form Pesquisa Rapida"
   ClientHeight    =   5790
   ClientLeft      =   1050
   ClientTop       =   1335
   ClientWidth     =   6270
   Icon            =   "FormPesqRapidaAvancada.frx":0000
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5790
   ScaleWidth      =   6270
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton CmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   3360
      TabIndex        =   3
      Top             =   5160
      Width           =   1455
   End
   Begin VB.CommandButton CmdOK 
      Caption         =   "&OK"
      Height          =   375
      Left            =   1440
      TabIndex        =   2
      Top             =   5160
      Width           =   1455
   End
   Begin MSComctlLib.ListView LwPesquisa 
      Height          =   3975
      Left            =   240
      TabIndex        =   1
      Top             =   960
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   7011
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   16121597
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.TextBox EcPesquisa 
      BackColor       =   &H00F5FEFD&
      Height          =   285
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   5895
   End
   Begin VB.Label Label1 
      Caption         =   "Digite o texto que pretende pesquisar"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   240
      Width           =   5535
   End
End
Attribute VB_Name = "FormPesqRapidaAvancada"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private ChavesPesquisa() As String
Private CamposEcra() As String
Private Tamanhos() As String
Private CamposRetorno As ClassPesqResultados
Private Headers() As String
Private NumeroChavesPesquisa As Integer
Private NumeroCamposEcra As Integer
Private ClausulaWhere As String
Private ClausulaFrom As String
Private MyDBConnection As ADODB.Connection
Private CampoPesquisa As String
Private campoPesquisaCod As String
Private sql As String
Private PesquisaCancelada As Variant
Private rs As New ADODB.recordset
Dim Inicializa As Boolean

Dim order_by_local As Variant

Public Function InicializaFormPesqAvancada(gConexao As ADODB.Connection, _
                                           TChavesPesq() As String, _
                                           TCamposEcra() As String, _
                                           TCamposRetorno As ClassPesqResultados, _
                                           TTamanhos() As Long, _
                                           THeaders() As String, _
                                           TClausulaWhere As String, _
                                           TClausulaFrom As String, _
                                           TPesquisaInicial As String, _
                                           TCampoPesquisa As String, _
                                           order_by As String, _
                                           Optional TTitulo As String, _
                                           Optional TCampoPesquisaCod As String) As Boolean
    Dim i As Integer
    Dim n As Integer
    
    ' valor por defeito
    InicializaFormPesqAvancada = True
    Inicializa = True
    Me.caption = TTitulo
    ' copiar os campos chave
    n = UBound(TChavesPesq)
    NumeroChavesPesquisa = n
    ReDim ChavesPesquisa(1 To n)
    For i = 1 To n
        ChavesPesquisa(i) = TChavesPesq(i)
    Next i
    ' copiar os campos de ecra
    n = UBound(TCamposEcra)
    NumeroCamposEcra = n
    ReDim CamposEcra(1 To n)
    For i = 1 To n
        CamposEcra(i) = TCamposEcra(i)
    Next i
    ' copiar os campos de retorno
    'n = UBound(TCamposRetorno)
    Set CamposRetorno = TCamposRetorno
    TCamposRetorno.PesquisaCancelada True

'    ReDim CamposRetorno(1 To n)
'    For i = 1 To n
'        CamposRetorno(i) = TCamposRetorno(i)
'    Next i
    ' copiar os campos de Tamanho de colunas
    n = UBound(TTamanhos)
    If n <> NumeroCamposEcra Then
        MsgBox "O n�mero de elementos do vector de Tamanhos tem de ser igual ao do de Campos de Ecra"
        Unload Me
        Exit Function
    End If
    ReDim Tamanhos(1 To n)
    For i = 1 To n
        Tamanhos(i) = TTamanhos(i)
    Next i
    ' copiar o nome da tabela
    If TClausulaFrom = "" Then
        MsgBox "Tem de Indicar o nome da tabela para pesquisa rapida"
        Unload Me
        Exit Function
    End If
    ClausulaFrom = TClausulaFrom
    ClausulaWhere = TClausulaWhere
    Set MyDBConnection = gConexao
    If TCampoPesquisaCod <> "" Then
        campoPesquisaCod = TCampoPesquisaCod
    Else
        campoPesquisaCod = ""
    End If
    CampoPesquisa = TCampoPesquisa
    EcPesquisa = TPesquisaInicial
    ' copiar os headers
    n = UBound(THeaders)
    If n <> NumeroCamposEcra Then
        MsgBox "O n�mero de elementos do vector de Headers tem de ser igual ao do de Campos de Ecra"
        Unload Me
        Exit Function
    End If
    
    ReDim Headers(1 To n)
    For i = 1 To n
        Headers(i) = IIf(THeaders(i) = "", TCamposEcra(i), THeaders(i))
    Next i
    
    Call Inicializacoes
    If InStr(1, TClausulaWhere, "SELECT") < 1 Or InStr(1, TClausulaWhere, "SELECT") > 5 Then
        Call ConstroiSQLPesquisaRapida
    Else
        sql = TClausulaWhere
    End If
    Call LimpaAliases
    
    If (IsMissing(order_by)) Then
        If (PreencheLista() = False) Then
            InicializaFormPesqAvancada = False
            Unload Me
        End If
    Else
        order_by_local = order_by
        If (PreencheLista(order_by) = False) Then
            InicializaFormPesqAvancada = False
            Unload Me
        End If
    End If
    Inicializa = False

End Function

Sub LimpaAliases()

    ' Esta sub retira os aliases tipo fa_movi_fact.t_doente -> t_doente do array camposecra.
    
    Dim i As Integer
    Dim posicao As Integer
    For i = 1 To NumeroCamposEcra
        posicao = InStr(1, CamposEcra(i), ".") + 1
        If posicao > 0 Then
            CamposEcra(i) = Mid(CamposEcra(i), posicao)
        End If
    Next i
    For i = 1 To NumeroChavesPesquisa
        posicao = InStr(1, ChavesPesquisa(i), ".") + 1
        If posicao > 0 Then
            ChavesPesquisa(i) = Mid(ChavesPesquisa(i), posicao)
        End If
    Next i

End Sub

Private Sub ConstroiSQLPesquisaRapida()
    
    Dim TmpSQL As String
    Dim i As Integer, n As Integer
    Dim CampoDuplicado As Boolean
    
    TmpSQL = "SELECT "
    ' acrescentar os campos de ecra
    For i = 1 To NumeroCamposEcra
        CampoDuplicado = False
        For n = 1 To NumeroChavesPesquisa
            If UCase(Trim(CamposEcra(i))) = UCase(Trim(ChavesPesquisa(n))) Then
                CampoDuplicado = True
                Exit For
            End If
        Next n
        If Not CampoDuplicado Then
            TmpSQL = TmpSQL & IIf(TmpSQL <> "SELECT ", ",", "") & CamposEcra(i)
        End If
    Next i
    ' selecccionar os campos chave
    For i = 1 To NumeroChavesPesquisa
        TmpSQL = TmpSQL & IIf(TmpSQL <> "SELECT ", ",", "") & ChavesPesquisa(i)
    Next i
    ' acrescentar as tabelas
    TmpSQL = TmpSQL & " FROM " & ClausulaFrom & " "
    If Trim(ClausulaWhere) <> "" Then
        TmpSQL = TmpSQL & "WHERE " & ClausulaWhere
    End If
    sql = TmpSQL

End Sub

Private Function PreencheLista(Optional order_by) As Boolean
    
    Dim TmpSQL As String
    Dim i As Integer
    Dim Lt As ListItem
    Dim LwCol As ListItems
    Dim ChaveItem As String
    Dim rs As New ADODB.recordset
    Dim nCount As Long
    Dim PosORDER As Integer
    Dim TmpStr1 As String
    Dim TmpStr As String
    
    On Error GoTo Trata_Erro
    
    ' valor de retorno por defeito
    PreencheLista = True
    TmpStr = ""
    TmpStr1 = sql
    If Trim(ClausulaWhere) = "" And EcPesquisa <> "" Then
        TmpSQL = " WHERE "
    ElseIf EcPesquisa <> "" Then
        TmpSQL = TmpSQL & " AND "
    End If
    
    ''
    PosORDER = 0
    PosORDER = InStr(1, sql, "ORDER")
    If PosORDER > 0 Then
        TmpStr1 = Mid(sql, 1, PosORDER - 1)
        TmpStr = Mid(sql, PosORDER, Len(sql))
    End If
    
    'TmpSQL = Sql & TmpSQL
    ''
    TmpSQL = TmpStr1 & TmpSQL
    
    If EcPesquisa <> "" Then

        If (gPesquisaDentroCampo) Then
            TmpSQL = TmpSQL & "(UPPER(" & CampoPesquisa & ") LIKE '%" & UCase(Trim(BG_CvWilcard(EcPesquisa))) & "%'"
            If gPermitePesquisaCodigo = mediSim And campoPesquisaCod <> "" Then
                TmpSQL = TmpSQL & "OR UPPER(" & campoPesquisaCod & ") LIKE '%" & UCase(Trim(BG_CvWilcard(EcPesquisa))) & "%'"
            End If
            TmpSQL = TmpSQL & ")"
        Else
            TmpSQL = TmpSQL & "(UPPER(" & CampoPesquisa & ") LIKE '" & UCase(Trim(BG_CvWilcard(EcPesquisa))) & "%'"
            If gPermitePesquisaCodigo = mediSim And campoPesquisaCod <> "" Then
                TmpSQL = TmpSQL & "OR UPPER(" & campoPesquisaCod & ") LIKE '" & UCase(Trim(BG_CvWilcard(EcPesquisa))) & "%'"
            End If
            TmpSQL = TmpSQL & ")"
        End If
        
        TmpSQL = TmpSQL & " " & TmpStr
    Else
        TmpSQL = TmpSQL & " " & TmpStr
    End If

    
    ' ----------------------------------------------------
    ' ORDER BY
    ' ----------------------------------------------------
    If Not (IsMissing(order_by)) Then
        If InStr(1, UCase(order_by), "ORDER BY ") = 0 And Trim(order_by) <> "" Then
            TmpSQL = TmpSQL & " ORDER BY " & CStr(order_by)
        Else
            TmpSQL = TmpSQL & " " & CStr(order_by)
        End If
    End If
    ' ----------------------------------------------------
    
    With rs
        .CacheSize = 100
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
    End With
'    rs.CursorType = adOpenStatic
'    rs.CursorLocation = adUseServer
    
    BL_MudaCursorRato vbHourglass, Me

    rs.Open TmpSQL, MyDBConnection
    
    LwPesquisa.ListItems.Clear
    If rs.EOF Then
       rs.Close
       Set rs = Nothing
        PreencheLista = False
        BL_MudaCursorRato vbNormal, Me
       Exit Function
    End If
    Set LwCol = LwPesquisa.ListItems
    nCount = 0
    Do While Not rs.EOF And nCount < 30000
        nCount = nCount + 1
        ChaveItem = ""
        For i = 1 To NumeroChavesPesquisa
            ChaveItem = ChaveItem & "<KEY>" & rs(ChavesPesquisa(i))
        Next i
        
        For i = 1 To NumeroCamposEcra
            If i = 1 Then
                Set Lt = LwCol.Add(, ChaveItem, Trim(BL_HandleNull(rs(CamposEcra(i)), "")))
            Else
                Lt.SubItems(i - 1) = Trim(BL_HandleNull(rs(CamposEcra(i)), ""))
            End If
        Next i
        rs.MoveNext
    Loop
    rs.Close
    Set rs = Nothing
    BL_MudaCursorRato vbNormal, Me
        
    Exit Function
    
Trata_Erro:
   
    BG_LogFile_Erros "ERRO NA FormPesquisaRapidaAvancada : " & Err.Number & "-" & Err.Description
    
    If Not rs Is Nothing Then
        Set rs = Nothing
    End If

    PreencheLista = False
    BL_MudaCursorRato vbNormal, Me
    Exit Function
    Resume Next
End Function

Private Sub Inicializacoes()
    FormPesqRapidaAvancada.Width = 6360
    LwPesquisa.Width = 5895
    EcPesquisa.Width = 5895
    InicializaListView
    With rs
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
    End With

    EcPesquisa.ToolTipText = cMsgWilcards

End Sub

Private Sub InicializaListView()
   
    Dim i As Integer
    
    LwPesquisa.View = lvwReport
    LwPesquisa.LabelEdit = lvwManual
    For i = 1 To NumeroCamposEcra
        LwPesquisa.ColumnHeaders.Add , , Headers(i), Tamanhos(i)
    Next i
    LwPesquisa.MultiSelect = False

End Sub

Private Sub CmdCancelar_Click()
    
    PesquisaCancelada = True
    Unload Me

End Sub

Private Sub cmdOK_Click()
    
    CamposRetorno.PesquisaCancelada False
    RetornaChaves
    Unload Me

End Sub

Private Sub EcPesquisa_Change()
    
    If Not Inicializa Then
        PreencheLista (order_by_local)
    End If

End Sub

Private Sub LwPesquisa_DblClick()
    
    CamposRetorno.PesquisaCancelada False
    If Not (LwPesquisa.SelectedItem Is Nothing) Then
        RetornaChaves
        Unload Me
    End If

End Sub

Private Sub RetornaChaves()
    
    Dim i As Long
    Dim Str1 As String, Str2 As String
    Dim NumeroElementos As Long
    Dim PosicaoSelec As Long
    Dim ColItems As ListItems
    Dim pos1 As Integer, pos2 As Integer
    Set ColItems = LwPesquisa.ListItems
    
    NumeroElementos = ColItems.Count
    For i = 1 To NumeroElementos
        If ColItems(i).Selected Then
            PosicaoSelec = i
            Exit For
        End If
    Next i
    If PosicaoSelec = 0 Then
        Set ColItems = Nothing
        Exit Sub
    End If
    
    Str1 = ColItems(PosicaoSelec).Key
    For i = 1 To NumeroChavesPesquisa
        pos1 = InStr(1, Str1, "<KEY>") + 5
        Str2 = Mid(Str1, pos1)
        pos2 = InStr(1, Str2, "<KEY>")
        
        If pos2 > 0 Then
            Str1 = Mid(Str2, pos2)
            CamposRetorno.ResPesquisaLet i, Mid(Str2, 1, pos2 - 1)
        Else
            CamposRetorno.ResPesquisaLet i, Str2
        End If
    Next i
    i = 0
    PosicaoSelec = 0
    Str1 = ""
    Str2 = ""
    Set ColItems = Nothing
    NumeroElementos = 0

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    LwPesquisa.ListItems.Clear
    Set FormPesqRapidaAvancada = Nothing
    Erase ChavesPesquisa
    Erase CamposEcra
    Erase Tamanhos
    Set CamposRetorno = Nothing
    Erase Headers
    ClausulaFrom = ""
    ClausulaWhere = ""
    NumeroCamposEcra = 0
    NumeroChavesPesquisa = 0
    CampoPesquisa = ""
    sql = ""

End Sub

Private Sub LwPesquisa_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Set LwPesquisa.SelectedItem = LwPesquisa.HitTest(X, Y)

End Sub

VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{2200CD23-1176-101D-85F5-0020AF1EF604}#1.7#0"; "barcod32.ocx"
Begin VB.Form FormImpCartoes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormImpCartoes"
   ClientHeight    =   2850
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5175
   Icon            =   "FormImpCartoes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2850
   ScaleWidth      =   5175
   ShowInTaskbar   =   0   'False
   Begin VB.ListBox ListaPostos 
      Height          =   645
      Left            =   960
      MultiSelect     =   2  'Extended
      TabIndex        =   6
      Top             =   1080
      Width           =   3720
   End
   Begin VB.CommandButton BtPesquisaPostos 
      Height          =   315
      Left            =   4680
      Picture         =   "FormImpCartoes.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
      Top             =   1080
      Width           =   375
   End
   Begin VB.TextBox EcPrinterCartoes 
      Height          =   285
      Left            =   1080
      TabIndex        =   4
      Top             =   3240
      Width           =   1335
   End
   Begin VB.CommandButton BtImprimir 
      Height          =   855
      Left            =   1920
      Picture         =   "FormImpCartoes.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Imprimir novos Utentes no intervalo de datas."
      Top             =   1920
      Width           =   975
   End
   Begin MSComCtl2.DTPicker EcDtInicio 
      Height          =   300
      Left            =   480
      TabIndex        =   0
      Top             =   600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   529
      _Version        =   393216
      Format          =   199950337
      CurrentDate     =   39346
   End
   Begin MSComCtl2.DTPicker EcDtFim 
      Height          =   300
      Left            =   3480
      TabIndex        =   1
      Top             =   600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   529
      _Version        =   393216
      Format          =   199950337
      CurrentDate     =   39346
   End
   Begin BarcodLib.Barcod MBarcode 
      Height          =   375
      Left            =   720
      TabIndex        =   3
      Top             =   3840
      Width           =   1335
      _Version        =   65543
      _ExtentX        =   2355
      _ExtentY        =   661
      _StockProps     =   75
      BackColor       =   16777215
      BarWidth        =   0
      Direction       =   0
      Style           =   3
      UPCNotches      =   3
      Alignment       =   0
      Extension       =   ""
   End
   Begin VB.Label Label1 
      Caption         =   "Postos"
      Height          =   255
      Index           =   3
      Left            =   0
      TabIndex        =   7
      Top             =   1080
      Width           =   1215
   End
End
Attribute VB_Name = "FormImpCartoes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Private Type Utentes
    seq_utente As Long
    nome As String
    t_utente As String
    Utente As String
    data_nasc As String
    
End Type

Dim estrutUtentes() As Utentes
Dim TotalEstrutUtentes As Long

Public rs As ADODB.recordset


Private Sub BtImprimir_Click()
    Dim sSql As String
    Dim RsUtente As New ADODB.recordset
    Dim lImprCartoesEltron As Integer
    Dim lImprCartoesCrystal As Integer
    Dim i As Integer
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    
    ' IMPRIME CARTOES DE GRUPOS SANGUE EM IMPRESSORA ELTRON
    lImprCartoesEltron = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPR_CARTOES_ELTRON")
    
    ' IMPRIME CARTOES DE GRUPOS SANGUE EM CRYSTAL
    lImprCartoesCrystal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPR_CARTOES_CRYSTAL")
    
    TotalEstrutUtentes = 0
    ReDim estrutUtentes(0)
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
           tabela_aux = "slv_identif"
        Else
           tabela_aux = "sl_identif"
    End If
    
    If EcDtInicio.value <> "" And EcDtFim.value <> "" Then
        sSql = "SELECT * FROM " & tabela_aux & " WHERE dt_cri between " & BL_TrataDataParaBD(EcDtInicio.value)
        sSql = sSql & " AND " & BL_TrataDataParaBD(EcDtFim.value) & " AND dt_impr_cartao IS NULL "
        sSql = sSql & " AND cod_genero = " & gCodGeneroDefeito & " "
        sSql = sSql & " AND (nao_impr_cartao IS NULL OR nao_impr_cartao = 0) "
        ' ------------------------------------------------------------------------------
        ' POSTOS PREENCHIDA
        ' ------------------------------------------------------------------------------
        If ListaPostos.ListCount > 0 Then
            sSql = sSql & " AND seq_utente IN (SELECT seq_utente FROM SL_REQUIS, SL_COD_SALAS WHERE sl_requis.cod_sala = sl_cod_salas.cod_sala AND sl_cod_salas.seq_sala IN ("
            For i = 0 To ListaPostos.ListCount - 1
                sSql = sSql & ListaPostos.ItemData(i) & ", "
            Next i
            sSql = Mid(sSql, 1, Len(sSql) - 2) & ") )"
        End If

        
        RsUtente.CursorType = adOpenStatic
        RsUtente.CursorLocation = adUseServer
        RsUtente.Open sSql, gConexao
        
        If RsUtente.RecordCount >= 0 Then
            While Not RsUtente.EOF
                If lImprCartoesEltron = mediSim Then
                    If BL_ImprimeCartaoEltron(EcPrinterCartoes, RsUtente!nome_ute, BL_HandleNull(RsUtente!dt_nasc_ute, ""), RsUtente!seq_utente, _
                                     RsUtente!t_utente, RsUtente!Utente, MBarcode, False, BL_HandleNull(RsUtente!n_benef_ute, "")) = True Then
                                                     
                        TotalEstrutUtentes = TotalEstrutUtentes + 1
                        ReDim Preserve estrutUtentes(TotalEstrutUtentes)
                        estrutUtentes(TotalEstrutUtentes).seq_utente = RsUtente!seq_utente
                        estrutUtentes(TotalEstrutUtentes).nome = RsUtente!nome_ute
                        estrutUtentes(TotalEstrutUtentes).t_utente = RsUtente!t_utente
                        estrutUtentes(TotalEstrutUtentes).Utente = RsUtente!Utente
                        estrutUtentes(TotalEstrutUtentes).data_nasc = RsUtente!dt_nasc_ute
                    End If
                
                ElseIf lImprCartoesCrystal = mediSim Then
                    If BL_ImprimeCartaoCrystal(EcPrinterCartoes, RsUtente!nome_ute, BL_HandleNull(RsUtente!dt_nasc_ute, ""), RsUtente!seq_utente, _
                                     RsUtente!t_utente, RsUtente!Utente, MBarcode, False, BL_HandleNull(RsUtente!n_benef_ute, "")) = True Then
                                                     
                        TotalEstrutUtentes = TotalEstrutUtentes + 1
                        ReDim Preserve estrutUtentes(TotalEstrutUtentes)
                        estrutUtentes(TotalEstrutUtentes).seq_utente = RsUtente!seq_utente
                        estrutUtentes(TotalEstrutUtentes).nome = RsUtente!nome_ute
                        estrutUtentes(TotalEstrutUtentes).t_utente = RsUtente!t_utente
                        estrutUtentes(TotalEstrutUtentes).Utente = RsUtente!Utente
                        estrutUtentes(TotalEstrutUtentes).data_nasc = RsUtente!dt_nasc_ute
                    End If
                
                Else
                    If BL_ImprimeCartao(EcPrinterCartoes, RsUtente!nome_ute, RsUtente!dt_nasc_ute, RsUtente!seq_utente, _
                                     RsUtente!t_utente, RsUtente!Utente, MBarcode, False, BL_HandleNull(RsUtente!n_benef_ute, "")) = True Then
                                                     
                        TotalEstrutUtentes = TotalEstrutUtentes + 1
                        ReDim Preserve estrutUtentes(TotalEstrutUtentes)
                        estrutUtentes(TotalEstrutUtentes).seq_utente = RsUtente!seq_utente
                        estrutUtentes(TotalEstrutUtentes).nome = RsUtente!nome_ute
                        estrutUtentes(TotalEstrutUtentes).t_utente = RsUtente!t_utente
                        estrutUtentes(TotalEstrutUtentes).Utente = RsUtente!Utente
                        estrutUtentes(TotalEstrutUtentes).data_nasc = RsUtente!dt_nasc_ute
                    End If
                End If
                RsUtente.MoveNext
            Wend
        End If
        RsUtente.Close
        Set RsUtente = Nothing
    End If
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = "Imprimir Cart�es de Utente - S�rie"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 5265
    Me.Height = 3270 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtInicio
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "InActivo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormImpCartoes = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    TotalEstrutUtentes = 0
    ReDim estrutUtentes(0)
    
    EcDtInicio.value = ""
    EcDtFim.value = Bg_DaData_ADO
    
End Sub

Sub DefTipoCampos()

        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    EcPrinterCartoes = BL_SelImpressora("Cartoes.rpt")
    EcDtInicio.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
End Sub

Sub FuncaoProcurar()
          
End Sub

Private Sub BtPesquisaPostos_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_sala"
    CamposEcran(1) = "cod_sala"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_sala"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_salas"
    CWhere = ""
    CampoPesquisa = "descr_sala"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_sala ", _
                                                                           " Postos Colheita")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaPostos.ListCount = 0 Then
                ListaPostos.AddItem BL_SelCodigo("sl_cod_salas", "descr_sala", "seq_sala", resultados(i))
                ListaPostos.ItemData(0) = resultados(i)
            Else
                ListaPostos.AddItem BL_SelCodigo("sl_cod_salas", "descr_sala", "seq_sala", resultados(i))
                ListaPostos.ItemData(ListaPostos.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub




Private Sub ListaPostos_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaPostos.ListCount > 0 Then     'Delete
        ListaPostos.RemoveItem (ListaPostos.ListIndex)
    End If
End Sub



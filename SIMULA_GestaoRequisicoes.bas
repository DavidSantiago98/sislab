Attribute VB_Name = "SIMULA_GestaoRequisicoes"
Option Explicit

' Actualiza��o : 29/01/2003
' T�cnico Paulo Costa

' ------------------------------------------------------
' Serve para os forms :
'   FormGestaoRequisicao
'   FormGesReqCons
' ------------------------------------------------------

Public Function SIM_Coloca_Utente(f As Form, _
                                  tipo_utente As String, _
                                  cod_utente As String) As Boolean

    ' V�lido para os forms f e FormGesReqCons.
    ' Simula a introdu��o de um utente por um utilizador do SISLAB.
    
    On Error GoTo ErrorHandler
    Dim i As Integer
    
    tipo_utente = UCase(Trim(tipo_utente))
    cod_utente = UCase(Trim(cod_utente))

    If ((Len(tipo_utente) > 0) And _
        (Len(cod_utente) > 0)) Then
        
        f.SSTGestReq.Tab = 0
        f.FuncaoLimpar
        
        For i = 0 To f.CbTipoUtente.ListCount - 1
            f.CbTipoUtente.ListIndex = i
            If (StrComp(f.CbTipoUtente.Text, tipo_utente) = 0) Then
                Exit For
            End If
        Next
        
        f.EcUtente.Text = cod_utente
        f.EcUtente_Validate (False)
        
    End If

    SIM_Coloca_Utente = True

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("SIM_Coloca_Utente (SIMULA_UTILIZADOR) -> " & Err.Number & " : " & Err.Description)
            SIM_Coloca_Utente = False
            Exit Function
    End Select
End Function

Public Function SIM_Coloca_Analise(f As Form, _
                                   cod_analise As String) As Boolean

    ' V�lido para os forms f e FormGesReqCons.
    ' Simula a introdu��o de uma an�lise por um utilizador do SISLAB.
    
    On Error GoTo ErrorHandler
    
    cod_analise = UCase(Trim(cod_analise))
    
    If (Len(cod_analise) > 0) Then
    
        f.SSTGestReq.Tab = 2
        Call f.FgAna_KeyDown(13, 0)
        f.EcAuxAna.Text = cod_analise
        Call f.EcAuxAna_KeyDown(13, 0)
        
    End If

    SIM_Coloca_Analise = True

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("SIM_Coloca_Analise (SIMULA_UTILIZADOR) -> " & Err.Number & " : " & Err.Description)
            SIM_Coloca_Analise = False
            Exit Function
    End Select
End Function

Public Function SIM_Coloca_Dados_Requisicao(f As Form, _
                                            dt_prevista As String, _
                                            situacao As String, _
                                            urgencia As String, _
                                            cod_proveniencia As String, _
                                            cod_efr As String, _
                                            cod_medico As String, _
                                            isento As Boolean, _
                                            tipo_isencao As String, _
                                            obs_final As String, _
                                            obs_proveniencia As String, _
                                            nr_benef As String, _
                                            req_associada As String, _
                                            episodio As String, user_cri As String, dt_cri As String, hr_Cri As String, _
                                            dt_chegada As Variant, n_prescricao As String, cod_destino As Integer, _
                                            cod_valencia As String, inf_complementar As String, prioridade_colheita As Integer) As Boolean

    ' V�lido para os forms f e FormGesReqCons.
    ' Simula a introdu��o de um utente por um utilizador do SISLAB.
    
    On Error GoTo ErrorHandler
    Dim i As Integer
    
    ' Situa��o.
    For i = 0 To f.CbSituacao.ListCount - 1
        f.CbSituacao.ListIndex = i
        If (StrComp(f.CbSituacao.Text, situacao) = 0) Then
            Exit For
        End If
    Next
    ' Urg�ncia.
    For i = 0 To f.CbUrgencia.ListCount - 1
        f.CbUrgencia.ListIndex = i
        If (StrComp(f.CbUrgencia.Text, urgencia) = 0) Then
            Exit For
        End If
    Next
    
    ' Tipo de isen��o.
    If (isento) Then
        f.CkIsento.Value = 1
        For i = 0 To f.CbTipoIsencao.ListCount - 1
            f.CbTipoIsencao.ListIndex = i
            If (StrComp(f.CbTipoIsencao.Text, tipo_isencao) = 0) Then
                Exit For
            End If
        Next
    End If
    
    ' destino
    If (cod_destino > mediComboValorNull) Then
        For i = 0 To f.CbDestino.ListCount - 1
            f.CbDestino.ListIndex = i
            If f.CbDestino.ItemData(i) = cod_destino Then
                f.CbDestino.ListIndex = i
                Exit For
            End If
        Next
    End If
    
    ' prioridade colheita
    If (prioridade_colheita > mediComboValorNull) Then
        For i = 0 To f.CbPrioColheita.ListCount - 1
            f.CbPrioColheita.ListIndex = i
            If f.CbPrioColheita.ItemData(i) = prioridade_colheita Then
                f.CbPrioColheita.ListIndex = i
                Exit For
            End If
        Next
    End If
    
    ' Data prevista.
    f.EcDataPrevista.Text = dt_prevista
    
    ' Observa��o final.
    f.EcObsReq.Text = obs_final
    
    ' Observa��o da proveni�ncia.
    f.EcObsProveniencia.Text = obs_proveniencia
    
    ' N.� de benefici�rio.
    f.EcNumBenef.Text = nr_benef
    
    ' Requisi��o associoada.
    f.EcReqAssociada.Text = req_associada
    
    ' Episodio.
    f.EcEpisodio.Text = episodio
    
    ' Proveni�ncia.
    f.EcCodProveniencia.Text = cod_proveniencia
    f.EcCodProveniencia_Validate (False)
    
    f.EcCodValencia.Text = cod_valencia
    f.EcCodValencia_Validate False
    
    ' EFR.
    f.EcCodEFR.Text = cod_efr
    f.EcCodEFR_Validate (False)
    
    ' M�dico.
    f.EcCodMedico.Text = cod_medico
    f.EcCodMedico_Validate (False)
    
    ' UTILIZADOR CRIACAO
    f.EcUtilizadorCriacao.Text = user_cri
    f.EcDataCriacao.Text = dt_cri
    f.EcHoraCriacao.Text = hr_Cri
    
    f.EcPrescricao = n_prescricao
    f.EcInfComplementar = inf_complementar
    If Not IsMissing(dt_chegada) Then
        f.EcDataChegada.Text = dt_chegada
    End If
    
    SIM_Coloca_Dados_Requisicao = True

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("SIM_Coloca_Dados_Requisicao (SIMULA_UTILIZADOR) -> " & Err.Number & " : " & Err.Description)
            SIM_Coloca_Dados_Requisicao = False
            Exit Function
    End Select
End Function

Public Function SIM_Insere_Requisicao(f As Form) As Boolean

    ' Simula o bot�o Inserir.
    ' Depois de inseridos os dados de utente e da requisi��o.
    
    On Error GoTo ErrorHandler

    Call f.FuncaoInserir

    SIM_Insere_Requisicao = True

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("SIM_Insere_Requisicao (SIMULA_UTILIZADOR) -> " & Err.Number & " : " & Err.Description)
            SIM_Insere_Requisicao = False
            Exit Function
    End Select
End Function

Public Function SIM_Coloca_Produto(f As Form, _
                                   cod_produto As String) As Boolean

    ' V�lido para os forms f e FormGesReqCons.
    ' Simula a introdu��o de uma an�lise por um utilizador do SISLAB.
    
    On Error GoTo ErrorHandler
    
    cod_produto = UCase(Trim(cod_produto))
    
    If (Len(cod_produto) > 0) Then
    
        f.SSTGestReq.Tab = 1
        
        Call f.FgProd_KeyDown(13, 0)
        f.EcAuxProd.Text = cod_produto
        Call f.EcAuxProd_KeyDown(13, 0)
        
    End If

    SIM_Coloca_Produto = True

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("SIM_Coloca_Produto (SIMULA_UTILIZADOR) -> " & Err.Number & " : " & Err.Description)
            SIM_Coloca_Produto = False
            Exit Function
    End Select
End Function

Public Function SIM_Coloca_Tubo(f As Form, _
                                   cod_tubo As String) As Boolean

    ' V�lido para os forms f e FormGesReqCons.
    ' Simula a introdu��o de um tubo por um utilizador do SISLAB.
    
    On Error GoTo ErrorHandler
    
    cod_tubo = UCase(Trim(cod_tubo))
    
    If (Len(cod_tubo) > 0) Then
    
        f.SSTGestReq.Tab = 1
        
        Call f.FGTubos_KeyDown(13, 0)
        f.EcAuxTubo.Text = cod_tubo
        Call f.EcAuxTubo_KeyDown(13, 0)
        f.EcAuxTubo.Visible = False
    End If

    SIM_Coloca_Tubo = True

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("SIM_Coloca_Tubo (SIMULA_UTILIZADOR) -> " & Err.Number & " : " & Err.Description)
            SIM_Coloca_Tubo = False
            Exit Function
    End Select
End Function


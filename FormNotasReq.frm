VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form FormNotasReq 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormNotasReq"
   ClientHeight    =   8385
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12600
   Icon            =   "FormNotasReq.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8385
   ScaleWidth      =   12600
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcSeqReqTubo 
      Height          =   285
      Left            =   7680
      Locked          =   -1  'True
      TabIndex        =   19
      Top             =   6360
      Width           =   495
   End
   Begin VB.Frame Frame1 
      Caption         =   "Gravar Nota"
      Height          =   4455
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   12375
      Begin VB.ComboBox CbGrupoNota 
         Height          =   315
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   480
         Width           =   4935
      End
      Begin VB.CommandButton BtFecharFrame 
         Height          =   495
         Left            =   11400
         Picture         =   "FormNotasReq.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Voltar"
         Top             =   3240
         Width           =   735
      End
      Begin VB.CommandButton BtGravar 
         Height          =   495
         Left            =   10440
         Picture         =   "FormNotasReq.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Gravar"
         Top             =   3240
         Width           =   735
      End
      Begin VB.TextBox EcNota 
         Appearance      =   0  'Flat
         Height          =   2295
         Left            =   120
         TabIndex        =   9
         Top             =   840
         Width           =   12015
      End
      Begin VB.Label Label1 
         Caption         =   "Grupo de Notas"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   480
         Width           =   1335
      End
   End
   Begin MSComctlLib.TreeView Tv 
      Height          =   3975
      Left            =   120
      TabIndex        =   18
      Top             =   120
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   7011
      _Version        =   393217
      Style           =   7
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin VB.TextBox EcSeqRecibo 
      Height          =   285
      Left            =   7080
      Locked          =   -1  'True
      TabIndex        =   17
      Top             =   6360
      Width           =   495
   End
   Begin VB.TextBox EcCodGrNota 
      Height          =   285
      Left            =   6480
      Locked          =   -1  'True
      TabIndex        =   16
      Top             =   6360
      Width           =   495
   End
   Begin VB.TextBox EcSeqUtente 
      Height          =   285
      Left            =   5880
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   6360
      Width           =   495
   End
   Begin VB.CommandButton BtApagaNota 
      Caption         =   "Eliminar"
      Height          =   615
      Left            =   11520
      Picture         =   "FormNotasReq.frx":19A0
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   3240
      Width           =   855
   End
   Begin VB.TextBox EcDescrObs 
      Height          =   285
      Left            =   4440
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   6360
      Width           =   1335
   End
   Begin VB.TextBox EcCodAgrup 
      Height          =   285
      Left            =   3480
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   6360
      Width           =   735
   End
   Begin VB.TextBox EcHrCri 
      Height          =   285
      Left            =   2640
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   6360
      Width           =   735
   End
   Begin VB.TextBox EcDtCri 
      Height          =   285
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   6360
      Width           =   735
   End
   Begin VB.TextBox EcUserCri 
      Height          =   285
      Left            =   960
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   6360
      Width           =   735
   End
   Begin VB.TextBox EcNumReq 
      Height          =   285
      Left            =   360
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   6360
      Width           =   495
   End
   Begin VB.CommandButton BtNovaNota 
      Caption         =   "Nova"
      Height          =   615
      Left            =   10560
      Picture         =   "FormNotasReq.frx":210A
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Nova Nota"
      Top             =   3240
      Width           =   855
   End
   Begin MSComctlLib.ImageList ImageList 
      Left            =   8760
      Top             =   5400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormNotasReq.frx":2874
            Key             =   "grupos"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormNotasReq.frx":2C0E
            Key             =   "anulado"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormNotasReq.frx":2FA8
            Key             =   "verde"
         EndProperty
      EndProperty
   End
   Begin VB.Label LbNota 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   3015
      Left            =   5520
      TabIndex        =   1
      Top             =   120
      Width           =   6855
   End
End
Attribute VB_Name = "FormNotasReq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

' --------------------------------------
' Constantes de cor
' --------------------------------------
Private Const CC_GRAY = &H8000000F
Private Const CC_FORMGRAY = &H8000000F
Private Const CC_LIGHTBLUE = &HFFD3A4
Private Const CC_LIGHTYELLOW = &H80000018

Const NUM_LINHAS = 10


Dim TrataReq As Boolean
Dim TrataAna As Boolean

' --------------------------------------
' Variaveis para acesso a BD '
' --------------------------------------
Dim NumCampos As Integer
Dim CamposBD() As String
Dim CamposEc() As Object
Dim ChaveBD As String
Dim ChaveEc As Object
Dim NomeTabela As String
Dim CriterioTabela As String
Public rs As ADODB.recordset


' --------------------------------------
' Estrutura ds NOTAS
' --------------------------------------
Private Type Notas
    seq_utente As Long
    n_req As String
    seq_obs As Long
    cod_ana As String
    descr_ana As String
    descr_obs As String
    user_cri As String
    nome_cri As String
    dt_cri As String
    hr_cri As String
    flg_invisivel As Integer
    user_anul As String
    nome_anul As String
    dt_anul As String
    hr_anul As String
    indice As Integer
End Type

Private Type grNotas
    cod_gr_nota As String
    descr_gr_nota As String
    indice As Integer
    TotalNotas As Long
    Notas() As Notas
End Type

Dim totalGrNotas As Integer
Dim estrutGrNotas() As grNotas


Public grupoNota As Integer


Private Sub BtApagaNota_Click()
    Dim iGr As Integer
    Dim iNota As Integer
    For iGr = 1 To totalGrNotas
        If estrutGrNotas(iGr).indice = Tv.SelectedItem.Index Then
            Exit Sub
        End If
        For iNota = 1 To estrutGrNotas(iGr).TotalNotas
            If estrutGrNotas(iGr).Notas(iNota).indice = Tv.SelectedItem.Index Then
                ApagaNota iGr, iNota
            End If
        Next iNota
    Next iGr
    PreencheTV

End Sub

Private Sub BtFecharFrame_Click()
    EcNota = ""
    Frame1.Visible = False
End Sub

Private Sub BtGravar_Click()

    If CbGrupoNota.ListIndex = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "Obrigat�rio indicar o grupo de notas!", vbOKOnly + vbInformation, "Notas"
        Exit Sub
    End If
    'BRUNODSANTOS CHPVVC-1252
    BL_GravaObsAnaReq EcNumReq.Text, "", EcNota.Text, CLng(0), CInt(CbGrupoNota.ItemData(CbGrupoNota.ListIndex)), CLng(EcSeqUtente.Text), CLng(IIf(EcSeqReqTubo.Text = "", -1, EcSeqReqTubo.Text))
    EcDescrObs = ""
    Frame1.Visible = False
    FuncaoProcurar
End Sub

Private Sub BtNovaNota_Click()
    Dim i As Integer
    EcNota = ""
    Frame1.Visible = True
    Frame1.top = 0
    Frame1.left = 0
    EcNota.SetFocus
    If grupoNota > -1 Then
        CbGrupoNota.Enabled = False
        For i = 0 To CbGrupoNota.ListCount - 1
            If CbGrupoNota.ItemData(i) = grupoNota Then
                CbGrupoNota.ListIndex = i
                Exit For
            End If
        Next
    Else
        For i = 0 To CbGrupoNota.ListCount - 1
            If UCase(CbGrupoNota.List(i)) = "GERAL" Then
                CbGrupoNota.ListIndex = i
                Exit For
            End If
        Next
    End If
    
End Sub


Private Sub CbGrupoNota_KeyDown(KeyCode As Integer, Shift As Integer)
    BG_LimpaOpcao CbGrupoNota, KeyCode
End Sub




Private Sub CbGrupoNota_Validate(Cancel As Boolean)
    If CbGrupoNota.ListIndex > mediComboValorNull Then
        If CbGrupoNota.ItemData(CbGrupoNota.ListIndex) = gGrupoNotasTubo Then
            If EcSeqReqTubo.Text = "" Then
                BG_Mensagem mediMsgBox, "Obrigat�rio associar um tubo (Chegada de tubos)!", vbOKOnly + vbInformation, "Notas"
                Cancel = True
                CbGrupoNota.ListIndex = mediComboValorNull
            End If
        End If
    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    ' Propriedades do form
    Me.caption = "Notas da Requisi��o"
    Me.left = 540
    Me.top = 450
    Me.Width = 12690
    Me.Height = 5220
    
    ' Configuracao de parametros para BD
    NomeTabela = "sl_obs_ana_req"
    NumCampos = 9
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim estrutRequis(0)
    
    ' Campos de BD
    CamposBD(0) = "n_req"
    CamposBD(1) = "user_cri"
    CamposBD(2) = "dt_cri"
    CamposBD(3) = "hr_cri"
    CamposBD(4) = "descr_obs"
    CamposBD(5) = "cod_agrup"
    CamposBD(6) = "seq_utente"
    CamposBD(7) = "seq_recibo"
    CamposBD(8) = "seq_req_tubo"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcUserCri
    Set CamposEc(2) = EcDtCri
    Set CamposEc(3) = EcHrCri
    Set CamposEc(4) = EcDescrObs
    Set CamposEc(5) = EcCodAgrup
    Set CamposEc(6) = EcSeqUtente
    Set CamposEc(7) = EcSeqRecibo
    Set CamposEc(8) = EcSeqReqTubo
    
    ' Activa recordset
    Set rs = New ADODB.recordset
    
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    ' Verifica se a sala ja esta definida
    
        
    ' Padrao
    estado = 0
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
    
End Sub

' --------------------------------------
' Evento form activo
' --------------------------------------
Sub EventoActivate()

    ' Padrao
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
End Sub

' --------------------------------------
' Evento de saida do form
' --------------------------------------
Sub EventoUnload()
    Dim i As Integer
    ' Padrao
    LimpaCampos
    BG_StackJanelas_Pop
    If gF_REQUIS_PRIVADO = mediSim Then
        Set gFormActivo = FormGestaoRequisicaoPrivado
        FormGestaoRequisicaoPrivado.Enabled = True
    ElseIf gF_REQUIS = mediSim Then
        Set gFormActivo = FormGestaoRequisicao
        FormGestaoRequisicao.Enabled = True
    ElseIf gF_FILA_ESPERA = mediSim Then
        Set gFormActivo = FormFilaEspera
        FormFilaEspera.Enabled = True
    ElseIf gF_ENTREGA_RESULTADOS = mediSim Then
        Set gFormActivo = FormEntregaResultados
        FormEntregaResultados.Enabled = True
    ElseIf gF_IDENTIF_VET = 1 Then
        Set gFormActivo = FormIdentificaVet
        FormIdentificaVet.Enabled = True
    ElseIf gF_REQUIS_VET = 1 Then
        FormGestaoRequisicaoVet.Enabled = True
        Set gFormActivo = FormGestaoRequisicaoVet
    ElseIf gF_REQUIS_AGUAS = 1 Then
        FormGestaoRequisicaoAguas.Enabled = True
        Set gFormActivo = FormGestaoRequisicaoAguas
    ElseIf gF_CHEGA_TUBOS = 1 Then
        FormChegadaPorTubosV2.Enabled = True
        Set gFormActivo = FormChegadaPorTubosV2
        If EcSeqReqTubo.Text <> "" Then
            For i = 1 To UBound(eTubos)
                If eTubos(i).seq_req_tubo = EcSeqReqTubo.Text Then
                    If EcNota.Text <> "" Then
                        eTubos(i).flg_nota = mediSim
                    Else
                        eTubos(i).flg_nota = mediNao
                    End If
                End If
            Next
        End If
    End If
    
    BL_ToolbarEstadoN 0
    
End Sub

' --------------------------------------
' Limpa campos do form '
' --------------------------------------
Sub LimpaCampos()
    totalGrNotas = 0
    ReDim estrutGrNotas(0)
    BG_LimpaCampo_Todos CamposEc
    Frame1.Visible = False
     
End Sub


' --------------------------------------
' Define campos do form
' --------------------------------------
Sub DefTipoCampos()
    EcDtCri.Tag = adDate
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    Tv.LineStyle = tvwTreeLines
    Tv.ImageList = ImageList

    
End Sub

' --------------------------------------
' Procura notas
' --------------------------------------
Public Sub FuncaoProcurar()
    Dim iGr As Integer
    Dim flgEncontrou As Boolean
    Dim sSql As String
    Dim RsNotas As New ADODB.recordset
    On Error GoTo Trata_Erro
    gSQLError = 0

    DefTipoCampos
    If EcNumReq = "" And EcSeqUtente = "" Then
        Exit Sub
    End If
    If EcSeqUtente = "" Then
        EcSeqUtente = BL_SelCodigo("SL_REQUIS", "SEQ_UTENTE", "N_REQ", EcNumReq)
    End If
    ReDim estrutGrNotas(0)
    totalGrNotas = 0
    
    ' ----------------------------------------
    ' PROCURA NOTAs ASSOCIADAS A REQUISI��O
    ' ----------------------------------------
    sSql = "SELECT x0.seq_utente, x0.n_req, x0.cod_agrup, x0.user_cri, x0.dt_cri, x0.hr_cri, x0.flg_invisivel, "
    sSql = sSql & " x0.user_anul, x0.dt_anul, x0.hr_anul, x1.cod_gr_nota, x1.descr_gr_nota,x2.cod_ana, x2.descr_ana, x0.descR_obs, x0.seq_obs, "
    sSql = sSql & " x3.nome nome_anul, x4.nome nome_cri "
    sSql = sSql & " FROM sl_obs_ana_req x0 LEFT OUTER JOIN SLV_ANALISES x2 ON x0.cod_Agrup = x2.cod_ana"
    sSql = sSql & "  LEFT OUTER JOIN sl_idutilizador x3 ON x0.user_anul= x3.cod_utilizador "
    sSql = sSql & ", SL_TBF_GR_NOTA x1, sl_idutilizador x4 "
    sSql = sSql & " WHERE x0.cod_gr_nota = X1.cod_gr_nota And seq_utente = " & EcSeqUtente
    sSql = sSql & " AND x0.user_cri = x4.cod_utilizador "
    If EcNumReq <> "" Then
        sSql = sSql & " AND n_req = " & EcNumReq
    End If
    If grupoNota > -1 Then
        sSql = sSql & " AND x0.cod_gr_nota = " & grupoNota
    End If
    sSql = sSql & " ORDER BY cod_gr_nota, dt_cri ASC, hr_cri ASC "
    

    RsNotas.CursorType = adOpenStatic
    RsNotas.CursorLocation = adUseServer
    RsNotas.Open sSql, gConexao
    If RsNotas.RecordCount > 0 Then
        While Not RsNotas.EOF
            flgEncontrou = False
            For iGr = 1 To totalGrNotas
                If estrutGrNotas(iGr).cod_gr_nota = BL_HandleNull(RsNotas!cod_gr_nota, "") Then
                    flgEncontrou = True
                    Exit For
                End If
            Next iGr
            
            If flgEncontrou = False Then
                totalGrNotas = totalGrNotas + 1
                ReDim Preserve estrutGrNotas(totalGrNotas)
                estrutGrNotas(totalGrNotas).cod_gr_nota = BL_HandleNull(RsNotas!cod_gr_nota, "")
                estrutGrNotas(totalGrNotas).descr_gr_nota = BL_HandleNull(RsNotas!descr_gr_nota, "")
                ReDim estrutGrNotas(totalGrNotas).Notas(0)
                estrutGrNotas(totalGrNotas).TotalNotas = 0
                iGr = totalGrNotas
            End If
            PreencheEstrutNotas iGr, BL_HandleNull(RsNotas!seq_utente, -1), BL_HandleNull(RsNotas!n_req, ""), _
                                BL_HandleNull(RsNotas!user_cri, ""), BL_HandleNull(RsNotas!dt_cri, ""), BL_HandleNull(RsNotas!hr_cri, ""), _
                                BL_HandleNull(RsNotas!descr_obs, ""), RsNotas!seq_obs, _
                                BL_HandleNull(RsNotas!flg_invisivel, 0), BL_HandleNull(RsNotas!user_anul, ""), BL_HandleNull(RsNotas!dt_anul, ""), _
                                BL_HandleNull(RsNotas!hr_anul, ""), BL_HandleNull(RsNotas!cod_ana, ""), BL_HandleNull(RsNotas!descr_ana, ""), _
                                BL_HandleNull(RsNotas!nome_cri, ""), BL_HandleNull(RsNotas!nome_anul, "")
            RsNotas.MoveNext
        Wend
        
    End If
    RsNotas.Close
    Set RsNotas = Nothing
    PreencheTV
Exit Sub
Trata_Erro:
    BL_LogFile_BD Me.Name, "FuncaoProcurar", Err.Number, Err.Description, ""
    Me.MousePointer = vbArrow
    Exit Sub
    Resume Next
End Sub


' --------------------------------------
' Funcao limpar do form
' --------------------------------------
Public Sub FuncaoLimpar()
    LimpaCampos
End Sub




Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "SL_TBF_GR_NOTA", "cod_gR_nota", "descr_gr_nota", CbGrupoNota
    LimpaCampos
    grupoNota = -1
    
End Sub


' ------------------------------------------------------------------------------------------------------------------

' PREENCHE A ESTRUTURA DE REQUISICOES

' ------------------------------------------------------------------------------------------------------------------
Private Sub PreencheEstrutNotas(iGr As Integer, seq_utente As Long, n_req As String, UserCri As String, DtCri As String, _
                                HrCri As String, descrObs As String, seq_obs As Long, flg_invisivel As Integer, _
                                UserAnul As String, DtAnul As String, HrAnul As String, cod_ana As String, descr_ana As String, _
                                nome_cri As String, nome_anul As String)
                                
    On Error GoTo TrataErro
    estrutGrNotas(iGr).TotalNotas = estrutGrNotas(iGr).TotalNotas + 1
    ReDim Preserve estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas)
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).seq_obs = seq_obs
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).n_req = n_req
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).user_cri = UserCri
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).dt_cri = DtCri
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).hr_cri = HrCri
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).user_anul = UserAnul
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).dt_anul = DtAnul
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).hr_anul = HrAnul
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).descr_obs = descrObs
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).flg_invisivel = flg_invisivel
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).cod_ana = cod_ana
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).descr_ana = descr_ana
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).nome_cri = nome_cri
    estrutGrNotas(iGr).Notas(estrutGrNotas(iGr).TotalNotas).nome_anul = nome_anul
Exit Sub
TrataErro:
    BL_LogFile_BD Me.Name, "PreencheEstrutNotas", Err.Number, Err.Description, ""
    Me.MousePointer = vbArrow
    Exit Sub
    Resume Next
End Sub


Private Sub ApagaNota(iGr As Integer, iNota As Integer)
    Dim sSql As String
    If iNota > mediComboValorNull And iGr > mediComboValorNull Then
        If estrutGrNotas(iGr).Notas(iNota).flg_invisivel = mediNao Then
            estrutGrNotas(iGr).Notas(iNota).flg_invisivel = mediSim
            estrutGrNotas(iGr).Notas(iNota).user_anul = CStr(gCodUtilizador)
            estrutGrNotas(iGr).Notas(iNota).dt_anul = Bg_DaData_ADO
            estrutGrNotas(iGr).Notas(iNota).hr_anul = Bg_DaHora_ADO
            sSql = "UPDATE sl_obs_ana_req SET flg_invisivel = 1, user_anul = " & BL_TrataStringParaBD(estrutGrNotas(iGr).Notas(iNota).user_anul)
            sSql = sSql & ", dt_anul = " & BL_TrataDataParaBD(estrutGrNotas(iGr).Notas(iNota).dt_anul)
            sSql = sSql & ", hr_anul = " & BL_TrataStringParaBD(estrutGrNotas(iGr).Notas(iNota).hr_anul) & " WHERE seq_obs = " & estrutGrNotas(iGr).Notas(iNota).seq_obs
            BG_ExecutaQuery_ADO sSql
        
        If EcSeqReqTubo.Text <> "" Then
            sSql = "UPDATE sl_req_tubo set flg_nota = 0 WHERE seq_req_tubo = " & EcSeqReqTubo.Text
            BG_ExecutaQuery_ADO sSql
        End If
        End If
    End If
End Sub


Private Sub PreencheTV()
    Dim iGr As Integer
    Dim iNota As Integer
    Dim indice As Integer
    Dim root As Node
    Dim descricao As String
    Dim Icon As String
    Tv.Nodes.Clear
    indice = 1
    For iGr = 1 To totalGrNotas
        estrutGrNotas(iGr).indice = indice
        Set root = Tv.Nodes.Add(, tvwChild, "KeyGR" & estrutGrNotas(iGr).cod_gr_nota, estrutGrNotas(iGr).descr_gr_nota, "grupos")
        root.Expanded = True
        indice = indice + 1
        For iNota = 1 To estrutGrNotas(iGr).TotalNotas
            estrutGrNotas(iGr).Notas(iNota).indice = indice
            descricao = estrutGrNotas(iGr).Notas(iNota).nome_cri & "(" & estrutGrNotas(iGr).Notas(iNota).dt_cri & " " & estrutGrNotas(iGr).Notas(iNota).hr_cri & ")"
            If estrutGrNotas(iGr).Notas(iNota).flg_invisivel = mediSim Then
                descricao = descricao & " ANULADO - " & estrutGrNotas(iGr).Notas(iNota).nome_anul & "(" & estrutGrNotas(iGr).Notas(iNota).dt_anul & " " & estrutGrNotas(iGr).Notas(iNota).hr_anul & ")"
                Icon = "anulado"
            Else
                Icon = "verde"
            End If
            Set root = Tv.Nodes.Add(estrutGrNotas(iGr).indice, tvwChild, "KeyN" & estrutGrNotas(iGr).Notas(iNota).seq_obs, _
                    descricao, Icon)
            indice = indice + 1
        Next iNota
    Next iGr
End Sub

Private Sub Tv_Click()
    Dim iGr As Integer
    Dim iNota As Integer
    For iGr = 1 To totalGrNotas
        If estrutGrNotas(iGr).indice = Tv.SelectedItem.Index Then
            LbNota.caption = ""
            Exit Sub
        End If
        For iNota = 1 To estrutGrNotas(iGr).TotalNotas
            If estrutGrNotas(iGr).Notas(iNota).indice = Tv.SelectedItem.Index Then
                LbNota.caption = estrutGrNotas(iGr).Notas(iNota).descr_obs
            End If
        Next iNota
    Next iGr
End Sub

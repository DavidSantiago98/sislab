Attribute VB_Name = "Config_Aplicacoes"
Option Explicit

' Actualiza��o : 02/04/2002
' T�cnico Paulo Costa
Public Function Transforma_ANASIBAS() As Integer

    On Error GoTo ErrorHandler
    
    ' Bot�o GESCOM.
    If (MDIFormInicio.Toolbar2.Buttons(13).Key = "GESCOM") Then
'        MDIFormInicio.Toolbar2.Buttons(13).Visible = False
'        MDIFormInicio.Toolbar2.Buttons(13).Enabled = False
    End If
    
  ' Bot�o SistemaInterrogacao.
    If (MDIFormInicio.Toolbar2.Buttons(14).Key = "SistemaInterrogacao") Then
        MDIFormInicio.Toolbar2.Buttons(14).Visible = False
        MDIFormInicio.Toolbar2.Buttons(14).Enabled = False
    End If
    
    ' MENU 03.01.04
    MDIFormInicio.IDM_REQCANCELADAS.Visible = False
    MDIFormInicio.IDM_REQCANCELADAS.Enabled = False

    ' MENU 03.01.05
    MDIFormInicio.IDM_MAPAREG.Visible = False
    MDIFormInicio.IDM_MAPAREG.Enabled = False
        

    
    ' MENU 03.01.08.05
    MDIFormInicio.IDM_TRANSFNUM(0).Visible = False
    MDIFormInicio.IDM_TRANSFNUM(0).Enabled = False

    ' MENU 03.01.10
    MDIFormInicio.IDM_DOCS(0).Visible = False
    MDIFormInicio.IDM_DOCS(0).Enabled = False
    
    ' MENU 03.02
    MDIFormInicio.IDM_FOLHASTRAB.Visible = False
    MDIFormInicio.IDM_FOLHASTRAB.Enabled = False
    
    
    ' MENU 03.04.08
    MDIFormInicio.IDM_PROTOCOLOS(0).Visible = False
    MDIFormInicio.IDM_PROTOCOLOS(0).Enabled = False
    
    ' Serarador
'    MDIFormInicio.SEP_MENU_UTILITARIOS_1.Visible = False
'    MDIFormInicio.SEP_MENU_UTILITARIOS_1.Enabled = False

    'Menu de Factura��o
    MDIFormInicio.IDM_MENU_FACTURACAO_PR.Visible = False
    MDIFormInicio.IDM_MENU_FACTURACAO_PR.Enabled = False

'    ' MENU 04.08
'    MDIFormInicio.IDM_CRIAR_SEQ_FACTURACAO.Visible = False
'    MDIFormInicio.IDM_CRIAR_SEQ_FACTURACAO.Enabled = False
'
'    ' MENU 04.09
'    MDIFormInicio.IDM_ENV_FACT.Visible = False
'    MDIFormInicio.IDM_ENV_FACT.Enabled = False
'
'    ' MENU 04.10
'    MDIFormInicio.IDM_ESTAT_FACTUS.Visible = False
'    MDIFormInicio.IDM_ESTAT_FACTUS.Enabled = False
    
    ' MENU 05
'    MDIFormInicio.IDM_MENU_ESTATISTICAS.Visible = False
'    MDIFormInicio.IDM_MENU_ESTATISTICAS.Enabled = False
    
    MDIFormInicio.IDM_MENU_ESTATISTICAS(0).Visible = False
    MDIFormInicio.IDM_MENU_ESTATISTICAS(0).Enabled = False
    
    MDIFormInicio.IDM_MENU_ESTATISTICAS(1).Visible = False
    MDIFormInicio.IDM_MENU_ESTATISTICAS(1).Enabled = False
    
    'MDIFormInicio.IDM_ESTAT_GRUPO.Visible = False
    'MDIFormInicio.IDM_ESTAT_GRUPO.Enabled = False
    
    MDIFormInicio.IDM_MENU_ESTATISTICAS(3).Visible = False
    MDIFormInicio.IDM_MENU_ESTATISTICAS(3).Enabled = False
    
    MDIFormInicio.IDM_MENU_ESTATISTICAS(4).Visible = False
    MDIFormInicio.IDM_MENU_ESTATISTICAS(4).Enabled = False
    
    MDIFormInicio.IDM_MENU_ESTATISTICAS(5).Visible = False
    MDIFormInicio.IDM_MENU_ESTATISTICAS(5).Enabled = False
    
    
    ' MENU 07
    MDIFormInicio.IDM_MENU_FERRAMENTAS_PR.Visible = False
    MDIFormInicio.IDM_MENU_FERRAMENTAS_PR.Enabled = False
    
    ' MENU 07.01
    MDIFormInicio.IDM_MENU_FERRAMENTAS(0).Visible = False
    MDIFormInicio.IDM_MENU_FERRAMENTAS(0).Enabled = False
        
    Transforma_ANASIBAS = 1
        
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Transforma_ANASIBAS (Config_Aplicacoes) -> " & Err.Description)
            Transforma_ANASIBAS = -1
            Exit Function
    End Select
End Function

Public Function Transforma_SISLAB() As Integer

    On Error GoTo ErrorHandler
    
    
    ' MENU 07.02
    MDIFormInicio.IDM_MENU_FERRAMENTAS(2).Visible = False
    MDIFormInicio.IDM_MENU_FERRAMENTAS(2).Enabled = False
    
    ' Separador.
    MDIFormInicio.IDM_MENU_FERRAMENTAS(3).Visible = False
    MDIFormInicio.IDM_MENU_FERRAMENTAS(3).Enabled = False

    If gTipoInstituicao = "PRIVADA" Then
        'MDIFormInicio.IDM_MENU_ESTATISTICAS(10).Visible = False
        'MDIFormInicio.IDM_MENU_ESTATISTICAS(10).Enabled = False
    End If
    
    Transforma_SISLAB = 1
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Transforma_SISLAB (Config_Aplicacoes) -> " & Err.Description)
            Transforma_SISLAB = -1
            Exit Function
    End Select
End Function




VERSION 5.00
Begin VB.Form FormAjudaValAuto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAjudaValAuto"
   ClientHeight    =   7470
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7980
   Icon            =   "FormAjudaValAuto.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7470
   ScaleWidth      =   7980
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox PcFormula 
      Height          =   530
      Left            =   5400
      Picture         =   "FormAjudaValAuto.frx":000C
      ScaleHeight     =   465
      ScaleWidth      =   480
      TabIndex        =   13
      Top             =   120
      Width           =   545
   End
   Begin VB.Frame Frame1 
      Caption         =   " Ajuda "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6735
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   7695
      Begin VB.Frame Frame3 
         Caption         =   " Exemplo "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1935
         Left            =   120
         TabIndex        =   10
         Top             =   4800
         Width           =   7455
         Begin VB.TextBox EcExemplo 
            Enabled         =   0   'False
            Height          =   285
            Left            =   240
            TabIndex        =   11
            Text            =   "(E=1 & I<30 &  Z3 & !T=4 &  P>34 ) & ($S1 > 10)"
            Top             =   240
            Width           =   6975
         End
         Begin VB.Label Label7 
            Caption         =   $"FormAjudaValAuto.frx":0316
            Height          =   1215
            Left            =   240
            TabIndex        =   12
            Top             =   600
            Width           =   7095
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Operadores :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1815
         Left            =   3840
         TabIndex        =   5
         Top             =   240
         Width           =   3735
         Begin VB.Label Label8 
            Caption         =   "&& � o operador l�gico [e]"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9.75
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   9
            Top             =   240
            Width           =   3015
         End
         Begin VB.Label Label10 
            Caption         =   "| � o operador l�gico [ou]"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9.75
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   8
            Top             =   600
            Width           =   3255
         End
         Begin VB.Label Label12 
            Caption         =   "! � nega��o da condi��o seguinte"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9.75
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   7
            Top             =   960
            Width           =   3255
         End
         Begin VB.Label Label14 
            Caption         =   "( ) � o delimitador de prioridades"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9.75
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   6
            Top             =   1320
            Width           =   3255
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Vari�veis :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4575
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   3615
         Begin VB.Label Label4 
            Caption         =   "@[c�digo da an�lise simples: segundo resultado]"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9.75
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   22
            Top             =   2640
            Width           =   3255
         End
         Begin VB.Label Label16 
            Caption         =   "V [ Volume]"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9.75
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   21
            Top             =   3240
            Width           =   3255
         End
         Begin VB.Label Label17 
            Caption         =   "P[Peso]"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9.75
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   20
            Top             =   3600
            Width           =   3255
         End
         Begin VB.Label Label18 
            Caption         =   "A [Altura]"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9.75
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   19
            Top             =   3960
            Width           =   3255
         End
         Begin VB.Label Label19 
            Caption         =   "I[Idade]"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9.75
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   18
            Top             =   4320
            Width           =   975
         End
         Begin VB.Label Label16 
            Caption         =   "$[An�lise Simples Primeiro Resultado]"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Index           =   1
            Left            =   120
            TabIndex        =   17
            Top             =   2160
            Width           =   3135
         End
         Begin VB.Label Label16 
            Caption         =   "R[Avaliar os Valores de Refer�ncia em conjunto com a Formula]"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Index           =   0
            Left            =   120
            TabIndex        =   16
            Top             =   1560
            Width           =   3135
         End
         Begin VB.Label Label13 
            Caption         =   "E[Sexo do Utente]"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   15
            Top             =   1200
            Width           =   2655
         End
         Begin VB.Label Label9 
            Caption         =   "T[c�digo da proveni�ncia do pedido anal�tico]"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   14
            Top             =   600
            Width           =   3135
         End
         Begin VB.Label Label6 
            Caption         =   "Z[c�digo de varia��es de resultados]"
            BeginProperty Font 
               Name            =   "Bookman Old Style"
               Size            =   9
               Charset         =   0
               Weight          =   300
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   4
            Top             =   240
            Width           =   3255
         End
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "OK"
      Height          =   495
      Left            =   6720
      TabIndex        =   0
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "F�rmula da Valida��o Autom�tica"
      BeginProperty Font 
         Name            =   "Bookman Old Style"
         Size            =   14.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   150
      Width           =   5415
   End
End
Attribute VB_Name = "FormAjudaValAuto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

Dim Estado As Integer

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    BG_ParametrizaPermissoes_ADO Me.Name
    Estado = 0
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    Set gFormActivo = Me
    BL_ToolbarEstadoN Estado
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub Inicializacoes()
    Me.caption = " Ajuda"
    Me.Left = 540
    Me.Top = 450
    Me.Width = 8070
    Me.Height = 7860 ' Normal
End Sub

Sub EventoUnload()
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
'    Set FormAjudaDeltaCheck = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub




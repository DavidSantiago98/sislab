VERSION 5.00
Begin VB.Form FormEstatDoentes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   3105
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6870
   Icon            =   "FormEstatDoentes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3105
   ScaleWidth      =   6870
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   2295
      Left            =   120
      TabIndex        =   5
      Top             =   720
      Width           =   6615
      Begin VB.ComboBox EcDescrSexo 
         Height          =   315
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   240
         Width           =   1455
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   1035
         Width           =   1455
      End
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   630
         Width           =   1455
      End
      Begin VB.TextBox EcCodEFR 
         Height          =   315
         Left            =   1320
         TabIndex        =   11
         Top             =   1920
         Width           =   735
      End
      Begin VB.TextBox EcDescrEFR 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   1920
         Width           =   4095
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   6120
         Picture         =   "FormEstatDoentes.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   1920
         Width           =   375
      End
      Begin VB.TextBox EcCodProveniencia 
         Height          =   315
         Left            =   1320
         TabIndex        =   8
         Top             =   1440
         Width           =   735
      End
      Begin VB.TextBox EcDescrProveniencia 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   1440
         Width           =   4095
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   6120
         Picture         =   "FormEstatDoentes.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1440
         Width           =   375
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Se&xo"
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   360
      End
      Begin VB.Label Label6 
         Caption         =   "Prio&ridade"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   1035
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   630
         Width           =   735
      End
      Begin VB.Label Label10 
         Caption         =   "E.&F.R."
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   1920
         Width           =   495
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   1440
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6615
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   1320
         TabIndex        =   2
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   2880
         TabIndex        =   1
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2610
         TabIndex        =   4
         Top             =   240
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   735
      End
   End
End
Attribute VB_Name = "FormEstatDoentes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'Option Explicit
'
'' Actualiza��o : 03/09/2002
'' T�cnico
'
'' Vari�veis Globais para este Form.
'
'Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.
'
'Dim CampoActivo As Object
'Dim CampoDeFocus As Object
'
'
'Public rs As ADODB.Recordset
'
'Sub Preenche_Estatistica()
'
'    Dim Sql As String
'    Dim Continua As Boolean
'
'    '1.Verifica se a Form Preview j� estava aberta!!
'    'If BL_PreviewAberto("Estat�stica por Laborat�rio") = True Then Exit Sub
'
'    '2. Verifica Campos Obrigat�rios
'    If EcDtIni.Text = "" Then
'        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
'        EcDtIni.SetFocus
'        Exit Sub
'    End If
'    If EcDtFim.Text = "" Then
'        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
'        EcDtFim.SetFocus
'        Exit Sub
'    End If
'
'    'Printer Common Dialog
'        If gImprimirDestino = 1 Then
'            Continua = BL_IniciaReport("EstatisticaDoentes", "Estat�stica de Doentes", crptToPrinter)
'        Else
'            Continua = BL_IniciaReport("EstatisticaDoentes", "Estat�stica de Doentes", crptToWindow)
'        End If
'        If Continua = False Then Exit Sub
'
'    Call Cria_TmpRec_Estatistica
'
'    PreencheTabelaTemporaria
'
''    If NAnaTabela = 0 Then
''        Call BG_Mensagem(mediMsgBox, "N�o foram encontradas an�lises requisitadas para iniciar um Relat�rio", vbOKOnly + vbExclamation, App.ProductName)
''        Exit Sub
''    End If
'
'    'Imprime o relat�rio no Crystal Reports
'    Dim Report As CrystalReport
'    Set Report = Forms(0).Controls("Report")
'
'    Report.SQLQuery = "SELECT SL_CR_ESTATDOENTES.DESCR_GRUPO, SL_CR_ESTATGRUPO.TIPO_ANA, SL_CR_ESTATGRUPO.DESCRICAO" & _
'        " FROM SL_CR_REL" & gNumeroSessao & " SL_CR_ESTATGRUPO ORDER BY DESCR_GRUPO, TIPO_ANA, DESCRICAO "
'
'    'F�rmulas do Report
'
'    Report.Formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
'    Report.Formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
'    If EcCodEFR = "" Then
'        Report.Formulas(2) = "EFR=" & BL_TrataStringParaBD("Todas")
'    Else
'        Report.Formulas(2) = "EFR=" & BL_TrataStringParaBD("" & EcDescrEFR.Text)
'    End If
'    If EcCodProveniencia = "" Then
'        Report.Formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("Todas")
'    Else
'        Report.Formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("" & EcDescrProveniencia.Text)
'    End If
'    If CbUrgencia.ListIndex = -1 Then
'        Report.Formulas(4) = "Urgencia=" & BL_TrataStringParaBD("-")
'    Else
'        Report.Formulas(4) = "Urgencia=" & BL_TrataStringParaBD("" & CbUrgencia.Text)
'    End If
'    If CbSituacao.ListIndex = -1 Then
'        Report.Formulas(5) = "Situacao=" & BL_TrataStringParaBD("Todas")
'    Else
'        Report.Formulas(5) = "Situacao=" & BL_TrataStringParaBD("" & CbSituacao.Text)
'    End If
'    If EcDescrSexo.ListIndex = -1 Then
'        Report.Formulas(6) = "Sexo=" & BL_TrataStringParaBD("Todos")
'    Else
'        Report.Formulas(6) = "Sexo=" & BL_TrataStringParaBD("" & EcDescrSexo.Text)
'    End If
'    If CbAnalises.ListIndex = -1 Then
'        Report.Formulas(7) = "Analises= " & BL_TrataStringParaBD("Todas")
'    Else
'        Report.Formulas(7) = "Analises= " & BL_TrataStringParaBD("" & CbAnalises.Text)
'    End If
'    If Flg_DescrAna.Value = 1 Then
'        Report.Formulas(8) = "DescrAna=" & BL_TrataStringParaBD(" S")
'    Else
'        Report.Formulas(8) = "DescrAna=" & BL_TrataStringParaBD(" N")
'    End If
'    If Flg_DetAna.Value = 1 Then
'        Report.Formulas(9) = "DetAna=" & BL_TrataStringParaBD(" S")
'    Else
'        Report.Formulas(9) = "DetAna=" & BL_TrataStringParaBD(" N")
'    End If
'    If Flg_PesoEstat.Value = 1 Then
'        Report.Formulas(10) = "PesoEstat=" & BL_TrataStringParaBD(" S")
'    Else
'        Report.Formulas(10) = "PesoEstat=" & BL_TrataStringParaBD(" N")
'    End If
''    Me.SetFocus
'
'    Report.SubreportToChange = "SubRepEstatGrupo"
'    Report.Connect = "DSN=" & gDSN
'
'    Report.SubreportToChange = ""
'
'    Call BL_ExecutaReport
'    'Report.PageShow Report.ReportLatestPage
'
'    'Elimina as Tabelas criadas para a impress�o dos relat�rios
'    Call BL_RemoveTabela("SL_CR_REL" & gNumeroSessao)
'
'End Sub
'
'Sub PreencheTabelaTemporaria()
'
'    Dim Sql As String
'    Dim SqlC As String
'    Dim RsAnaCReal As ADODB.Recordset
'    Dim RsAnaSReal As ADODB.Recordset
'    Dim RsAnaCMarc As ADODB.Recordset
'    Dim RsAnaSMarc As ADODB.Recordset
'    Dim RsAnaCUnion As ADODB.Recordset
'    Dim RSAnaSUnion As ADODB.Recordset
'    Dim flg_tabela As Boolean
'
'    'verifica os campos preenchidos
'    'se grupo preenchido
'    If (CbGrupo.ListIndex <> -1) Then
'        SqlC = SqlC & " AND sl_gr_ana.descr_gr_ana = " & BL_TrataStringParaBD(CbGrupo)
'    End If
'
'    'se sexo preenchido
'    If (EcDescrSexo.ListIndex <> -1) Then
'        SqlC = SqlC & " AND sl_identif.sexo_ute = " & EcDescrSexo.ListIndex
'    End If
'
'    'Data preenchida
'    SqlC = SqlC & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
'
'    'Situa��o preenchida?
'    If (CbSituacao.ListIndex <> -1) Then
'        SqlC = SqlC & " AND sl_requis.T_sit= " & CbSituacao.ListIndex
'    End If
'
'    'Urg�ncia preenchida?
'    If (CbUrgencia.ListIndex <> -1) Then
'        SqlC = SqlC & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(Left(CbUrgencia.Text, 1))
'    End If
'
'    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
'    If EcCodProveniencia.Text <> "" Then
'        SqlC = SqlC & " AND sl_requis.Cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
'    End If
'
'    'C�digo da EFR preenchido?
'    If EcCodEFR.Text <> "" Then
'        SqlC = SqlC & " AND sl_requis.Cod_efr=" & BL_TrataStringParaBD(EcCodEFR.Text)
'    End If
'
'    If CbAnalises.ListIndex = 0 Then
'        '___________________________________________________________________________
'        'insere na tabela tempor�ria todos os perfis c/ resultado que satisfazem os campos preenchidos
'        Sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ) " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Perfis', sl_perfis.descr_perfis, sl_perfis.cod_perfis, sl_perfis.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_perfis " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_perfis.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_perfis.peso <> '0' "
'
'        Sql = Sql & SqlC
'
'        BG_ExecutaQuery_ADO Sql
'        BG_Trata_BDErro
'
'        '______________________________________________________________________________
'        'insere na tabela tempor�ria todas as anas. complexas c/ resultado que satisfazem os campos preenchidos
'        Sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ) " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_c " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_realiza.cod_perfil = '0' and sl_ana_c.peso <> 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_c, sl_perfis " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_realiza.cod_perfil <> '0' and sl_ana_c.peso <> 0 and sl_perfis.peso = 0 "
'
'        Sql = Sql & SqlC
'
'        BG_ExecutaQuery_ADO Sql
'        BG_Trata_BDErro
'
'        '_____________________________________________________________________________
'        'insere na tabela tempor�ria todas anas. simples com resultado q satisfa�am os campos preenchidos
'        Sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, PESO, N_REQ) " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_realiza.cod_ana_c = '0' and sl_realiza.cod_perfil = '0' and sl_ana_s.peso <> 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_ana_c " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_realiza.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_ana_c.peso = 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_perfis " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_realiza.cod_perfil <> '0' and sl_realiza.cod_ana_c = '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_perfis, sl_ana_c " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_realiza.cod_perfil <> '0' and sl_realiza.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 and sl_ana_c.peso = 0 "
'
'        Sql = Sql & SqlC
'
'        BG_ExecutaQuery_ADO Sql
'        BG_Trata_BDErro
'
'
'    ElseIf CbAnalises.ListIndex = 1 Then
'        '____________________________________________________________________________
'        'insere na tabela tempor�ria todas os perfis s/ resultado que satisfazem os campos preenchidos
'        Sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ) " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Perfis', sl_perfis.descr_perfis, sl_perfis.cod_perfis, sl_perfis.peso, sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_perfis " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_perfis.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_perfis.peso <> 0 "
'
'        Sql = Sql & SqlC
'
'        BG_ExecutaQuery_ADO Sql
'        BG_Trata_BDErro
'
'        '_____________________________________________________________________________
'        'insere na tabela tempor�ria todas as anas. complexas s/ resultado que satisfazem os campos preenchidos
'        Sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ) " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso, sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_c " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_marcacoes.cod_perfil = '0' and sl_ana_c.peso <> 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso, sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_c, sl_perfis " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_marcacoes.cod_perfil <> '0' and sl_ana_c.peso <> 0 and sl_perfis.peso = 0 "
'
'        Sql = Sql & SqlC
'
'        BG_ExecutaQuery_ADO Sql
'        BG_Trata_BDErro
'
'        '__________________________________________________________________________
'        'obtem todas anas. simples sem resultado q satisfa�am os campos preenchidos
'        'Set RsAnaSMarc = New ADODB.Recordset
'        Sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, PESO, N_REQ)" & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_marcacoes.cod_ana_c = '0' and sl_marcacoes.cod_perfil = '0' and sl_ana_s.peso <> 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_ana_c " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_marcacoes.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_ana_c.peso = 0 "
'
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_perfis " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_marcacoes.cod_perfil <> '0' and sl_marcacoes.cod_ana_c = '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 "
'
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_perfis, sl_ana_c " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_marcacoes.cod_perfil <> '0' and sl_marcacoes.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 and sl_ana_c.peso = 0 "
'
'        Sql = Sql & SqlC
'
'        BG_ExecutaQuery_ADO Sql
'        BG_Trata_BDErro
'
'
'    Else
'        '_____________________________________________________________________________
'        'insere na tabela tempor�ria a uni�o de perfis com resultados com perfis sem resultados q perte�am ao grupo e a requisi��o em quest�o
'        Sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ) " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Perfis', sl_perfis.descr_perfis, sl_perfis.cod_perfis, sl_perfis.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_perfis " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_perfis.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_perfis.peso <> 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Perfis', sl_perfis.descr_perfis, sl_perfis.cod_perfis, sl_perfis.peso,sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_perfis, sl_proven " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_perfis.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_requis.cod_proven = sl_proven.cod_proven " & _
'        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_perfis.peso <> 0 "
'
'        Sql = Sql & SqlC
'
'        BG_ExecutaQuery_ADO Sql
'        BG_Trata_BDErro
'
'        '______________________________________________________________________________
'        'insere na tabela tempor�ria a uni�o de anas. complexas com resultados com anas. complexas sem resultados q perte�am ao grupo e a requisi��o em quest�o
'        Sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ)" & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso,sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_c, sl_proven " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_requis.cod_proven = sl_proven.cod_proven " & _
'        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_realiza.cod_perfil = '0' and sl_ana_c.peso <> 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_c, sl_perfis " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_realiza.cod_perfil <> '0' and sl_ana_c.peso <> 0 and sl_perfis.peso = 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso,sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_c, sl_proven " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_requis.cod_proven = sl_proven.cod_proven " & _
'        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_marcacoes.cod_perfil = '0' and sl_ana_c.peso <> 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso, sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_c, sl_perfis " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_marcacoes.cod_perfil <> '0' and sl_ana_c.peso <> 0 and sl_perfis.peso = 0 "
'
'        Sql = Sql & SqlC
'
'        BG_ExecutaQuery_ADO Sql
'        BG_Trata_BDErro
'
'        '______________________________________________________________________________
'        'insere na tabela tempor�ria a uni�o de anas. simples sem resultado com anas. simples com resultados q perten�am ao grupo e a requisi��o em quest�o
'        Sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, PESO,N_REQ) " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_proven " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_requis.cod_proven = sl_proven.cod_proven " & _
'        "AND sl_realiza.cod_ana_c = '0' and sl_realiza.cod_perfil = '0' and sl_ana_s.peso <> 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_ana_c " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_realiza.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_ana_c.peso = 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_perfis " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_realiza.cod_perfil <> '0' and sl_realiza.cod_ana_c = '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_realiza.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_perfis, sl_ana_c " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_realiza.n_req = sl_requis.n_req " & _
'        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_realiza.cod_perfil <> '0' and sl_realiza.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 and sl_ana_c.peso = 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_proven " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_requis.cod_proven = sl_proven.cod_proven " & _
'        "AND sl_marcacoes.cod_ana_c = '0' and sl_marcacoes.cod_perfil = '0' and sl_ana_s.peso <> 0 "
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_ana_c " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_marcacoes.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_ana_c.peso = 0 "
'
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_perfis " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_marcacoes.cod_perfil <> '0' and sl_marcacoes.cod_ana_c = '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 "
'
'
'        Sql = Sql & SqlC
'
'        Sql = Sql & " UNION ALL " & _
'        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.peso, sl_marcacoes.n_req " & _
'        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_perfis, sl_ana_c " & _
'        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
'        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
'        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
'        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
'        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
'        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
'        "AND sl_marcacoes.cod_perfil <> '0' and sl_marcacoes.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 and sl_ana_c.peso = 0 "
'
'        Sql = Sql & SqlC
'
'        BG_ExecutaQuery_ADO Sql
'        BG_Trata_BDErro
'
'
'    End If
'
'End Sub
'
'Sub Cria_TmpRec_Estatistica()
'
'    Dim TmpRec(6) As DefTable
'
'    TmpRec(1).NomeCampo = "DESCR_GRUPO"
'    TmpRec(1).Tipo = "STRING"
'    TmpRec(1).Tamanho = 40
'
'    TmpRec(2).NomeCampo = "TIPO_ANA"
'    TmpRec(2).Tipo = "STRING"
'    TmpRec(2).Tamanho = 10
'
'    TmpRec(3).NomeCampo = "DESCRICAO"
'    TmpRec(3).Tipo = "STRING"
'    TmpRec(3).Tamanho = 80
'
'    TmpRec(4).NomeCampo = "CODIGO"
'    TmpRec(4).Tipo = "STRING"
'    TmpRec(4).Tamanho = 10
'
'    TmpRec(5).NomeCampo = "PESO"
'    TmpRec(5).Tipo = "INTEGER"
'    TmpRec(5).Tamanho = 3
'
'    TmpRec(6).NomeCampo = "N_REQ"
'    TmpRec(6).Tipo = "INTEGER"
'    TmpRec(6).Tamanho = 9
'
'    Call BL_CriaTabela("SL_CR_REL" & gNumeroSessao, TmpRec)
'
'End Sub
'
'Function Funcao_DataActual()
'
'    Select Case UCase(CampoActivo.Name)
'        Case "ECDTFIM"
'            EcDtFim = Bg_DaData_ADO
'        Case "ECDTINI"
'            EcDtIni = Bg_DaData_ADO
'    End Select
'
'End Function
'
'Private Sub BtPesquisaEntFin_Click()
'
'    Dim ChavesPesq(1 To 2) As String
'    Dim CampoPesquisa As String
'    Dim CamposEcran(1 To 2) As String
'    Dim CWhere As String
'    Dim CFrom As String
'    Dim CamposRetorno As New ClassPesqResultados
'    Dim Tamanhos(1 To 2) As Long
'    Dim Headers(1 To 2) As String
'    Dim CancelouPesquisa As Boolean
'    Dim Resultados(1 To 2)  As Variant
'    Dim PesqRapida As Boolean
'
'    PesqRapida = False
'
'    ChavesPesq(1) = "descr_efr"
'    CamposEcran(1) = "descr_efr"
'    Tamanhos(1) = 4000
'    Headers(1) = "Descri��o"
'
'    ChavesPesq(2) = "cod_efr"
'    CamposEcran(2) = "cod_efr"
'    Tamanhos(2) = 1000
'    Headers(2) = "C�digo"
'
'    CamposRetorno.InicializaResultados 2
'
'    CFrom = "sl_efr"
'    CampoPesquisa = "descr_efr"
'
'    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
'            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Entidades Finaceiras")
'
'    If PesqRapida = True Then
'        FormPesqRapidaAvancada.Show vbModal
'        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
'        If Not CancelouPesquisa Then
'            EcCodEFR.Text = Resultados(2)
'            EcDescrEFR.Text = Resultados(1)
'            BtPesquisaEntFin.SetFocus
'        End If
'    Else
'        BG_Mensagem mediMsgBox, "N�o existem entidades financeiras", vbExclamation, "EFR"
'        EcCodEFR.SetFocus
'    End If
'
'End Sub
'
'Private Sub BtPesquisaProveniencia_Click()
'
'    'Campos do RecordSet
'    Dim ChavesPesq(1 To 2) As String
'
'    'Campo do Crit�rio na Query do RecordSet
'    Dim CampoPesquisa1 As String
'
'    'Campos do RecordSet a Visualizar no ecran
'    Dim CamposEcran(1 To 2) As String
'
'    'Contru��o da Query
'    Dim ClausulaWhere As String
'    Dim ClausulaFrom As String
'
'    'Classe
'    Dim CamposRetorno As New ClassPesqResultados
'
'    'Tamanho dos campos Ecran da List
'    Dim Tamanhos(1 To 2) As Long
'
'    'Cabe�alhos
'    Dim Headers(1 To 2) As String
'
'    'Mensagem do resultado da pesquisa
'    Dim Mensagem As String
'
'    Dim PesqRapida As Boolean
'    Dim CancelouPesquisa As Boolean
'
'    Dim Resultados(1 To 2) As Variant
'
'    'Defini��o dos campos a retornar
'    ChavesPesq(1) = "cod_proven"
'    CamposEcran(1) = "cod_proven"
'    Tamanhos(1) = 2000
'
'    ChavesPesq(2) = "descr_proven"
'    CamposEcran(2) = "descr_proven"
'    Tamanhos(2) = 3000
'
'    'Cabe�alhos
'    Headers(1) = "C�digo"
'    Headers(2) = "Descri��o"
'
'    'N� de Campos a pesquisar na Classe=>Resultaods
'    CamposRetorno.InicializaResultados 2
'
'    'Query
'    ClausulaFrom = "sl_proven"
'    CampoPesquisa1 = "descr_proven"
'
'    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
'
'    Mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
'
'    If PesqRapida = True Then
'        FormPesqRapidaAvancada.Show vbModal
'        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
'        If Not CancelouPesquisa Then
'            EcCodProveniencia.Text = Resultados(1)
'            EcDescrProveniencia.Text = Resultados(2)
'        End If
'    Else
'        BG_Mensagem mediMsgBox, Mensagem, vbExclamation, "..."
'    End If
'
'End Sub
'
'Private Sub CbUrgencia_GotFocus()
'
'    Set CampoActivo = Me.ActiveControl
'
'End Sub
'
'Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
'
'    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1
'
'End Sub
'
'Private Sub EcCodProveniencia_GotFocus()
'
'    Set CampoActivo = Me.ActiveControl
'
'End Sub
'
'Private Sub EcCodEFR_GotFocus()
'
'    Set CampoActivo = Me.ActiveControl
'
'End Sub
'
'Private Sub EcCodEFR_Validate(Cancel As Boolean)
'
'    Dim RsDescrEFR As ADODB.Recordset
'
'    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
'    If Cancel = True Then
'        Exit Sub
'    End If
'
'    If Trim(EcCodEFR.Text) <> "" Then
'        Set RsDescrEFR = New ADODB.Recordset
'
'        With RsDescrEFR
'            .Source = "SELECT descr_efr FROM sl_efr WHERE cod_efr= " & BL_TrataStringParaBD(EcCodEFR.Text)
'            .CursorLocation = adUseServer
'            .CursorType = adOpenStatic
'            .ActiveConnection = gConexao
'            .Open
'        End With
'
'        If RsDescrEFR.RecordCount > 0 Then
'            EcDescrEFR.Text = "" & RsDescrEFR!descr_efr
'        Else
'            Cancel = True
'            EcDescrEFR.Text = ""
'            BG_Mensagem mediMsgBox, "Entidade Financeira inexistente!", vbOKOnly + vbExclamation, App.ProductName
'            SendKeys ("{HOME}+{END}")
'        End If
'        RsDescrEFR.Close
'        Set RsDescrEFR = Nothing
'    Else
'        EcDescrEFR.Text = ""
'    End If
'
'End Sub
'
'Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
'
'    Dim RsDescrProv As ADODB.Recordset
'
'    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
'    If Cancel = True Then
'        Exit Sub
'    End If
'
'    If Trim(EcCodProveniencia.Text) <> "" Then
'        Set RsDescrProv = New ADODB.Recordset
'
'        With RsDescrProv
'            .Source = "SELECT descr_proven FROM sl_proven WHERE cod_proven= " & BL_TrataStringParaBD(EcCodProveniencia.Text)
'            .CursorLocation = adUseServer
'            .CursorType = adOpenStatic
'            .ActiveConnection = gConexao
'            .Open
'        End With
'
'        If RsDescrProv.RecordCount > 0 Then
'            EcDescrProveniencia.Text = "" & RsDescrProv!Descr_Proven
'        Else
'            Cancel = True
'            EcDescrProveniencia.Text = ""
'            BG_Mensagem mediMsgBox, "A Proveni�ncia indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
'            SendKeys ("{HOME}+{END}")
'        End If
'        RsDescrProv.Close
'        Set RsDescrProv = Nothing
'    Else
'        EcDescrProveniencia.Text = ""
'    End If
'
'End Sub
'
'Private Sub EcDtFim_GotFocus()
'
'    If Trim(EcDtFim.Text) = "" Then
'        EcDtFim.Text = Bg_DaData_ADO
'    End If
'    Set CampoActivo = Me.ActiveControl
'
'End Sub
'
'Private Sub EcDtFim_Validate(Cancel As Boolean)
'
'    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
'
'End Sub
'
'Private Sub EcDtIni_GotFocus()
'
'    If Trim(EcDtIni.Text) = "" Then
'        EcDtIni.Text = Bg_DaData_ADO
'    End If
'    Set CampoActivo = Me.ActiveControl
'
'End Sub
'
'Private Sub EcDtIni_Validate(Cancel As Boolean)
'
'    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
'
'End Sub
'
'Private Sub Form_Deactivate()
'
'    Call BL_FimProcessamento(Screen.ActiveForm, "")
'
'End Sub
'
'Sub Form_Load()
'
'    EventoLoad
'
'End Sub
'
'Sub Form_Activate()
'
'    EventoActivate
'
'End Sub
'
'Sub Form_Unload(Cancel As Integer)
'
'    EventoUnload
'
'End Sub
'
'Sub EventoLoad()
'
'    BL_InicioProcessamento Me, "Inicializar �cran..."
'    Inicializacoes
'    Set CampoActivo = Me.ActiveControl
'
'    DefTipoCampos
'    PreencheValoresDefeito
'    BG_ParametrizaPermissoes_ADO Me.Name
'
'    Estado = 0
'    BG_StackJanelas_Push Me
'    BL_FimProcessamento Me
'
'End Sub
'
'Sub Inicializacoes()
'
'    Me.Caption = " Estat�stica por Laborat�rio"
'
'    Me.Left = 540
'    Me.Top = 450
'    Me.Width = 7725
'    Me.Height = 5700 ' Normal
'    'Me.Height = 5700 ' Campos Extras
'
'    Set CampoDeFocus = EcCodProveniencia
'
'End Sub
'
'Sub EventoActivate()
'
'    BG_StackJanelas_Actualiza Me
'    Set gFormActivo = Me
'    CampoDeFocus.SetFocus
'
'    BL_ToolbarEstadoN Estado
'    BL_Toolbar_BotaoEstado "Limpar", "Activo"
'    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
'    BL_Toolbar_BotaoEstado "DataActual", "Activo"
'
'    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
'    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
'
'    Me.MousePointer = vbArrow
'    MDIFormInicio.MousePointer = vbArrow
'
'End Sub
'
'Sub EventoUnload()
'
'    BG_StackJanelas_Pop
'    Set gFormActivo = MDIFormInicio
'    BL_ToolbarEstadoN 0
'
'    If Not rs Is Nothing Then
'        rs.Close
'        Set rs = Nothing
'    End If
'
'    Call BL_FechaPreview("Estat�stica por Laborat�rio")
'
'    Set FormEstatGrupo = Nothing
'
'End Sub
'
'Sub LimpaCampos()
'    Me.SetFocus
'
'    CbAnalises.ListIndex = mediComboValorNull
'    CbUrgencia.ListIndex = mediComboValorNull
'    CbSituacao.ListIndex = mediComboValorNull
'    EcDescrSexo.ListIndex = mediComboValorNull
'    CbGrupo.ListIndex = mediComboValorNull
'    EcCodProveniencia.Text = ""
'    EcDescrProveniencia.Text = ""
'    EcCodEFR.Text = ""
'    EcDescrEFR.Text = ""
'    EcDtFim.Text = ""
'    EcDtIni.Text = ""
'    Flg_DescrAna.Value = 0
'    Flg_DetAna.Value = 0
'    Flg_PesoEstat.Value = 0
'
'End Sub
'
'Sub DefTipoCampos()
'
'    'Tipo VarChar
'    EcCodProveniencia.Tag = "200"
'    EcCodEFR.Tag = "200"
'
'    'Tipo Data
'    EcDtIni.Tag = "104"
'    EcDtFim.Tag = "104"
'
'    'Tipo Inteiro
''    EcNumFolhaTrab.Tag = "3"
'
'    EcCodProveniencia.MaxLength = 5
'    EcCodEFR.MaxLength = 9
'    EcDtIni.MaxLength = 10
'    EcDtFim.MaxLength = 10
''    EcNumFolhaTrab.MaxLength = 10
'
'End Sub
'
'Sub FuncaoLimpar()
'
'    If Estado = 2 Then
'        FuncaoEstadoAnterior
'    Else
'        LimpaCampos
'        CampoDeFocus.SetFocus
'    End If
'
'End Sub
'
'Sub FuncaoEstadoAnterior()
'
'    If Estado = 2 Then
'        Estado = 0
'        LimpaCampos
'        CampoDeFocus.SetFocus
'        If Not rs Is Nothing Then
'            rs.Close
'            Set rs = Nothing
'        End If
'    Else
'        Unload Me
'    End If
'
'End Sub
'
'Sub PreencheValoresDefeito()
'
'    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
'    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
'    BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", CbGrupo
'
'    'Preenche Combo Urgencia
'    CbUrgencia.AddItem "Normal"
'    CbUrgencia.AddItem "Urgente"
'
'    'Preenche Combo Analises
'    CbAnalises.AddItem "Com Resultado"
'    CbAnalises.AddItem cSemResultado
'    CbAnalises.AddItem "Todas"
'
'End Sub
'
'Sub FuncaoProcurar()
'
'End Sub
'
'Sub FuncaoImprimir()
'
'    Call Preenche_EstatisticaGrupo
'
'End Sub
'
'Sub ImprimirVerAntes()
'
'    Call Preenche_EstatisticaGrupo
'
'End Sub
'
'
'
'
'
'

Attribute VB_Name = "ModuloPrincipal"
Option Explicit

' Actualiza��o : 15/02/2002
' Tcnico Paulo Costa

Dim NomeBD_AttachOuLocal As String
Dim TipoBD As String

Sub Main()

    Dim AbriuBD As Integer

    On Error GoTo TrataErroAbertura
            
    gCodLocal = "1"
    ' Faz a leitura de par�metros do ficheiro INI.
    ' /bin/Sislab.INI
    Call Get_INI
    
    'FormApresentacao.Visible = False
    
    cAPLICACAO_NOME_CURTO = "Sislab"
    cAPLICACAO_NOME = "Sistema Integrado de Gest�o Laboratorial"
    cAPLICACAO_AUTOR = "Copyright (c), GLINTT HS"
    
    If Command = "" Then
        FormApresentacao.Show vbModal
    End If
    'FormApresentacao.Visible = True
        
    If Dir(App.Path & "\SISLAB_REG.ini") <> "" Then
        BL_CarregaFichINI
    Else
        If (InicializacoesINI = False) Then
            Exit Sub
        End If
    End If
    'BG_LogFile_Clear
    
    BG_LogFile_Erros String(25 + Len(cAPLICACAO_NOME), "-")
    BG_LogFile_Erros " In�cio do LogFile para: " & cAPLICACAO_NOME
    BG_LogFile_Erros String(25 + Len(cAPLICACAO_NOME), "-")
    BG_LogFile_Erros "Vers�o: " & gAPLICACAO_VERSAO & "   -   Data: " & Format(Date, gFormatoData) & "   -   Hora: " & Format(time, gFormatoHora)

    ' DEFINIR LIGA��O:
    ' No sislab n�o haverer� liga��es locais pois vai utilizar ADO,
    ' Todos os clientes e o servidor poder�o aceder � base de dados via OBDC.
    gBD_LocalOuRemota = "REMOTA"

    'abre gConexao ADO por OLEDB
    AbriuBD = BG_Abre_ConexaoBD_ADO
    If AbriuBD = 0 Then
        MDIFormInicio.Show
        'FormAbertura.Show
        AplicacaoVazia True
        Dim X
        'X = Shell("notepad.exe " & gDirCliente & "\bin\logs\" & "LogFile.Log", 1)
        Form_Ini.Show
        Exit Sub
    End If
        gComputador = BG_SYS_GetComputerName
        gTerminal = BG_SYS_GetTerminalName
        
    ' Inicializar variaveis globais ------------------------------------------
    gModoDebug = 0
    gSQLError = 0
    gSQLISAM = 0
'    gModoRes = cModoResNada
    gSituacaoActiva = mediComboValorNull
    gEpisodioActivo = ""
    gReqAssocActiva = ""
    
    gF_AUTORES = 0
    gF_AUTORESSERIE = 0
    gF_GRANTIB = 0
    gF_GRUPOSTRAB = 0
    gF_IDENTIF = 0
    gF_REQUIS = 0
    gF_REQUIS_PRIVADO = 0
    gF_FILA_ESPERA = 0
    gF_MODELOLIVRE = 0
    gF_PERFISANA = 0
    gF_EXAMES = 0
    gF_PROVENIENCIA = 0
    gF_GRUPOSTRAB = 0
    gF_MODELOLIVRE = 0
    gF_PROVENIENCIA = 0
    gF_2RES = 0
    gF_PRODUTOS = 0
    gF_REQUTE = 0
    gF_RESULT = 0
    gF_RESMICRO = 0
    gF_LRES = 0
    gF_TRANSF = 0
    gF_REQPENDANA = 0
    gF_REQPENDELECT = 0
    gF_TRATAREJ = 0
    gF_REQCONS = 0
    gF_INFCLI = 0
    gF_SELRES = 0
    
    gImpZebra = -1
    gRequisicaoActiva = 0
    BL_LimpaDadosUtente
    'gImp_Res_Ant = vbUnchecked
            
    gInsRegistoDuplo = Array(-239, 1)
    gUpdRegistoDuplo = Array(-346)
    gInsRegistoNulo = Array(-391, 1407, 1400)
    gErroSyntax = Array(-201)
    gValoresInsuf = Array(-236, 947)
        
    
    BL_CriaNumeroSessao
    
    'Multi Local
    BL_TestaLocal
    BL_PreencheEstrutEstadosReq
    BL_PreencheEstrutEstadosAna
    
    BL_PreencheEstrutTURG
    DIC_InicializaEstruturasGlobais
    Call CarregaParamAmbiente
    BL_CriaImpressora

    If gUsaProfiles = "1" Then
        gCmdParamAcessos.CommandText = " SELECT * FROM " & cParamAcessos & " WHERE ( cod_grupo= ? OR cod_util= ? ) AND nome_form = ?  AND upper(descr_prop )not in( 'VISIBLE', 'ENABLED') ORDER BY seq_param_acess ASC "
    Else
        gCmdParamAcessos.CommandText = " SELECT * FROM " & cParamAcessos & " WHERE ( cod_grupo= ? OR cod_util= ? ) AND nome_form = ? ORDER BY seq_param_acess ASC "
    End If
    
    'carrega config sms
    'RGONCALVES 16.06.2013 ICIL-470 - adicionado gEnvioResultadosSMSActivo e gEnvioSMSAvulsoActivo
    If gEnvioSMSActivo = mediSim Or gEnvioResultadosSMSActivo = mediSim Or gEnvioSMSAvulsoActivo = mediSim Then
        SMS_CarregaConfig
    End If
    
    ' CARREGA CARACTERIZACOES MICROBIOLOGIA
    MICRO_PreencheCaracMicro
    
    'Iniciar estado da transa��o(N�o existe transa��o)
    gEstadoTransacao = False
    'SOLIVEIRA 09.10.2007
    Dim argumentos As Variant
    
    'FormVerificarUtilizador.Show vbModal
    If Command = "" Then
        
        FormVerificarUtilizador.Show vbModal
    Else
        BG_LogFile_Erros Command, "ModuloPrincipal", "Main", False
        'argumentos separados por ";"
        argumentos = Split(Command, ";")
        
        Dim rsUtil As ADODB.recordset
        Set rsUtil = New ADODB.recordset
        rsUtil.CursorLocation = adUseServer
        rsUtil.CursorType = adOpenStatic
        If gSGBD = gOracle Then
            rsUtil.Source = "select * from sl_idutilizador, sl_computador where upper(utilizador) = " & BL_TrataStringParaBD(UCase(argumentos(0)))
            rsUtil.Source = rsUtil.Source & " AND sl_idutilizador.computador = sl_computador.descr_computador (+) "
        ElseIf gSGBD = gSqlServer Then
            rsUtil.Source = "select * from sl_idutilizador LEFT OUTER JOIN sl_computador ON sl_idutilizador.computador = sl_computador.descr_computador "
            rsUtil.Source = rsUtil.Source & " where upper(utilizador) = " & BL_TrataStringParaBD(UCase(argumentos(0)))
        End If
        rsUtil.ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros rsUtil.Source
        rsUtil.Open
        If rsUtil.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Utilizador inv�lido!", vbInformation
            Exit Sub
        Else
            gIdUtilizador = rsUtil!utilizador
            gCodUtilizador = rsUtil!cod_utilizador
            gCodUtilizadorColheita = BL_HandleNull(rsUtil!cod_util_colh, "")
            gSenhaUtilizador = BL_HandleNull(rsUtil!senha, "")
            gNomeUtilizador = BL_HandleNull(rsUtil!nome, "")
            gTituloUtilizador = BL_HandleNull(rsUtil!Titulo, "")
            gCodGrupo = rsUtil!cod_gr_util
            gCodGrAnaUtilizador = "" & rsUtil!gr_ana
            gCodGrTrabUtilizador = "" & BL_HandleNull(rsUtil!gr_trab, "")
            gPermResUtil = BL_HandleNull(rsUtil!permissao_res, 0)
            gCodTAgendaDefeito = BL_HandleNull(rsUtil!cod_t_agenda, "")
            
            If gUsaProfiles = mediSim Then
                If BL_HandleNull(rsUtil!cod_profile, "") = "" Then
                    BG_Mensagem mediMsgBox, "Utilizador tem que estar associado a um profile!", vbExclamation, "Verificar Utilizador"
                    rsUtil.Close
                    Exit Sub
                Else
                    gProfileUtilizador = BL_HandleNull(rsUtil!cod_profile, "")
                End If
            Else
                gProfileUtilizador = BL_HandleNull(rsUtil!cod_profile, "")
            End If
        End If
        gCodSalaAssocComp = BL_HandleNull(rsUtil!cod_sala, 0)
        gCodSalaAssocUser = BL_HandleNull(rsUtil!cod_posto, 0)
        
        rsUtil.Close
        Set rsUtil = Nothing
        
        gArgTEpisodio = BL_HandleNull(Trim(argumentos(1)), "")
        gArgNEpisodio = BL_HandleNull(Trim(argumentos(2)), "")
        gArgTUtente = BL_HandleNull(Trim(argumentos(3)), "")
        gArgNUtente = BL_HandleNull(Trim(argumentos(4)), "")
        gArgServReq = BL_HandleNull(Trim(argumentos(5)), "")
        gArgEFR = BL_HandleNull(Trim(argumentos(6)), "")
        gArgNBenef = BL_HandleNull(Trim(argumentos(7)), "")
        gArgCodMed = BL_HandleNull(Trim(argumentos(8)), "")
        If UBound(argumentos) = 9 Then
            gArgModoServidor = BL_HandleNull(Trim(argumentos(9)), 0)
        End If
        
    End If

    
    If CInt(gDEMO) = 1 Then
        BG_Mensagem mediMsgBox, "Vers�o de Demonstra��o", vbInformation, cAPLICACAO_NOME_CURTO & " " & gAPLICACAO_VERSAO
    End If
    
    MDIFormInicio.Show
    ' Fim de Inicializar variaveis globais ------------------------------------
    
    
    
    AplicacaoVazia False
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    BG_LimpaPassaParams
    BG_StackJanelas_Inicializa
    
    BG_LogFile_Erros "Utilizador: " & gCodUtilizador & " - " & gIdUtilizador & vbCrLf
        
    ActualizarEntradaNaAplicacao
    BG_ParametrizaPermissoes_ADO "MDIFormInicio"
    ActualizaEntradaNaAplicacaoDepoisDosProfiles
    BL_AplicaProfile gProfileUtilizador
    'Inicializa o Handle que cont�m a Form MDICLIENT para os Reports!!
    gParentChild = FindWindowEx(MDIFormInicio.hwnd, ByVal 0&, "MDICLIENT", vbNullString)
    
    'Inicializa o handle com o cursor normal
    CursorNor = GetClassLong(ByVal MDIFormInicio.hwnd, GCL_HCURSOR)
    
    'Impress�o de hist�rico de an�lises no boletim de resultados
    gImp_Res_Ant = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMP_RES_ANT")
    If gImp_Res_Ant = -1 Then gImp_Res_Ant = vbUnchecked

    If Trim(BG_DaParamAmbiente_ADO(mediAmbitoUtilizador, "ESTADO_MENU_PERSONALIZADO")) = "ABERTO" Then
        FormMenusPersonalizados.Show
    End If
    
    DoEvents
    
    'Ajustar barra para o form ficar todo visivel
    MDIFormInicio.CoolBar1.Bands(1).Style = cc3BandFixedSize
    MDIFormInicio.CoolBar1.Bands(1).Style = cc3BandNormal
    MDIFormInicio.CoolBar1.Bands(1).Width = 8700
    MDIFormInicio.CoolBar1.Bands(2).Width = 2955
    
    If gBloqueiaAplicacao = mediSim Then
        MDIFormInicio.IDLETimer1.MinutosDeIntervalo = gTimeOutMinutos
    Else
        MDIFormInicio.IDLETimer1.MinutosDeIntervalo = 1000
    End If
    'SOLIVEIRA 09.10.2007
    If Command <> "" And gArgModoServidor = 0 Then
        BG_AbreForm FormGestaoRequisicao, "FormGestaoRequisicao"
    End If
     
    'edgar.parada BACKLOG-11940 (COLHEITAS) 24.01.2019
    If gAtiva_Alertas = mediSim Then
         MDIFormInicio.Toolbar2.Buttons(25).Visible = True
         BL_Executa_Procedimento_Alertas (True)
         eAlertas = BL_Devolve_Existem_Alertas_Activos()
         MDIFormInicio.ActualizarIconeAlertas eAlertas
         If eAlertas.existem_alertas = True Then
            FormAlertas.Show
         End If
         MDIFormInicio.TimerAlertas.Interval = 1000
     Else
         MDIFormInicio.Toolbar2.Buttons(25).Visible = False
     End If
     '
          
    Exit Sub

TrataErroAbertura:
     If Err = 3024 Or Err = 3031 Or Err = 3146 Or Err = 3151 Then
        ' 3024: Couldn't find file <name>.
        ' 3031: Not a valid password.
        ' 3146: ODBC - call failed.
        ' 3151: ODBC�- connection to <name> failed.
        
        BG_Mensagem mediMsgBox, "ERRO FATAL N� " & str(Err) & ": " & Error(Err), vbCritical, cAPLICACAO_NOME_CURTO & ": Fim do Programa!"
        BG_LogFile_Erros "ERRO FATAL N� " & str(Err) & ": " & Error(Err) & ": Fim do Programa!"
        End
    Else
        BG_TrataErro "- Main -", Err.Number
        Beep
        Resume Next
    End If

End Sub

Public Sub ActualizarEntradaNaAplicacao()
    Dim sDia As String
    Dim sData As String
    
    sData = Format(Bg_DaData_ADO, gFormatoData)
    BG_DaValorData sData, "DIA", sDia
    sDia = left(sDia, 3) & "."
    
    MDIFormInicio.Timers(TimersEnum.e_timer_mensagens).Interval = 0
    
    MDIFormInicio.StatusBar1.Panels.Item("Mensagem").text = ""
    MDIFormInicio.StatusBar1.Panels.Item("Versao").text = gAPLICACAO_VERSAO
    MDIFormInicio.StatusBar1.Panels.Item("Local").text = BL_PesquisaCodDes("gr_empr_inst", "cod_empr", "nome_empr", CStr(gCodLocal), "DES")
    MDIFormInicio.StatusBar1.Panels.Item("Grupo").text = BL_PesquisaCodDes("sl_gr_ana", "cod_gr_ana", "descr_gr_ana", gCodGrAnaUtilizador, "DES")
    MDIFormInicio.StatusBar1.Panels.Item("Data").text = sData & " (" & sDia & ") "
    MDIFormInicio.StatusBar1.Panels.Item("Hora").text = Format(Bg_DaHora_ADO, gFormatoHora) & " "
    MDIFormInicio.StatusBar1.Panels.Item("Utilizador").text = gIdUtilizador & " "
    MDIFormInicio.StatusBar1.Panels.Item("Remota").text = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "DB_UPDATE_LEVEL")
    'MDIFormInicio.StatusBar1.Panels.Item("Remota").Bevel = sbrInset
    

    
    If gTipoInstituicao = "HOSPITALAR" Then
        MDIFormInicio.IDM_REQUISICOES(0).Visible = True
        MDIFormInicio.IDM_REQUISICOES(1).Visible = True
        MDIFormInicio.IDM_REQUISICOES_PRIVADO.Visible = False
        MDIFormInicio.IDM_PED_NOVA_AMOSTRA(4).Visible = False
        'MDIFormInicio.IDM_FOLHAS_TRAB(2).Visible = False
        MDIFormInicio.IDM_IMP_PRONTOS.Visible = True
        MDIFormInicio.IDM_IMP_JA_LISTADAS(0).Visible = True
    
        MDIFormInicio.IDM_TRANSFNUM(1).Visible = False
    ElseIf gTipoInstituicao = "PRIVADA" Then
        MDIFormInicio.IDM_REQUISICOES(0).Visible = False
        MDIFormInicio.IDM_REQUISICOES(1).Visible = False
        MDIFormInicio.IDM_REQUISICOES_PRIVADO.Visible = True
        
        If gUsaFimSemana <> 1 Then
            MDIFormInicio.IDM_REQUISICOES_PRIV(1).Visible = False
        End If
        
        If gEnvioEmail = 1 Then
            MDIFormInicio.IDM_IMP_JA_LISTADAS(1).Visible = True
        Else
            MDIFormInicio.IDM_IMP_JA_LISTADAS(1).Visible = False
        End If
        
        MDIFormInicio.IDM_PED_NOVA_AMOSTRA(4).Visible = True
        MDIFormInicio.IDM_FOLHAS_TRAB(2).Visible = True
        MDIFormInicio.IDM_IMP_PRONTOS.Visible = False
        MDIFormInicio.IDM_IMP_JA_LISTADAS(0).Visible = False
        MDIFormInicio.IDM_PROTOCOLOS(0).Visible = False
        MDIFormInicio.IDM_PROTOCOLOS(1).Visible = True
        If gCodSalaAssocUser > 0 Then
            Dim sSql As String
            Dim rsSala As New ADODB.recordset
            sSql = "SELECT flg_colheita,flg_inibe_imp_analises FROM sl_cod_salas WHERE cod_sala = " & gCodSalaAssocUser
            Set rsSala = BG_ExecutaSELECT(sSql)
            If rsSala.RecordCount > 0 Then
                If BL_HandleNull(rsSala!flg_colheita, 1) = 0 Then
                    If BL_HandleNull(rsSala!flg_inibe_imp_analises, 0) = mediSim Then
                        FormMenusPersonalizados.BtRes.Enabled = False
                    Else
                        FormMenusPersonalizados.BtRes.Enabled = True
                        If gCodSalaAssocUser <> 0 Then
                            MDIFormInicio.IDM_RESULTADOS.Visible = False
                            MDIFormInicio.IDM_RES(0).Visible = False
                            MDIFormInicio.IDM_RES(1).Visible = False
                            'MDIFormInicio.IDM_RES(2).Visible = False
                            MDIFormInicio.IDM_RES(3).Visible = False
                            MDIFormInicio.IDM_REQPEND_GRUPO(1).Visible = False
                            MDIFormInicio.IDM_REQPEND_GRUPO(3).Visible = False
                            MDIFormInicio.IDM_PROTOCOLOS(0).Visible = False
                            MDIFormInicio.IDM_PROTOCOLOS(1).Visible = False
                        End If
                    End If
                End If
            End If
            BG_DestroiRecordSet rsSala
        End If
        
        'MDIFormInicio.IDM_MENU_ESTATISTICAS(0).Visible = False
        MDIFormInicio.IDM_MENU_ESTATISTICAS(1).Visible = False
        MDIFormInicio.IDM_MENU_ESTATISTICAS(3).Visible = False
    
        MDIFormInicio.IDM_TRANSFNUM(1).Visible = True
        
    End If
 
    RegistoAcessos_Entrada

    If gCodSala <> 0 Then
        If gNovoEcraResultados = mediSim Then
            MDIFormInicio.IDM_RES(0).Visible = True
            MDIFormInicio.IDM_RES(0).caption = "Resultados - Ecr� Antigo"
            MDIFormInicio.IDM_RES(1).Visible = True
            MDIFormInicio.IDM_RES(3).Visible = True
        Else
            MDIFormInicio.IDM_RES(0).Visible = True
            MDIFormInicio.IDM_RES(1).Visible = True
            MDIFormInicio.IDM_RES(3).Visible = False
        End If
    End If
    
    'NELSONPSILVA 16.01.2018 UnimedVitoria-1152
    If gMostraMisDataDiscovery = mediSim Then
        MDIFormInicio.Toolbar2.Buttons(24).Visible = True
    Else
        MDIFormInicio.Toolbar2.Buttons(24).Visible = False
    End If
    
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.acesso_apl) & " - " & MDIFormInicio.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", "{}", "{}"
    End If
    
End Sub


Private Sub ActualizaEntradaNaAplicacaoDepoisDosProfiles()
    
    If geResults = False Then
        MDIFormInicio.IDM_AGENDA(2).Visible = False
    End If
    
    If gCodGrupo <> gGrupoAdministradores Then
        MDIFormInicio.IDM_MENU_FERRAMENTAS(8).Visible = False
    ElseIf gCodGrupo = gGrupoAdministradores Then
        MDIFormInicio.IDM_MENU_FERRAMENTAS(8).Visible = True
    End If
    If gUsaNovaFolhaTrab = 1 Then
        MDIFormInicio.IDM_FOLHAS_TRAB(0).Visible = False
        MDIFormInicio.IDM_FOLHAS_TRAB(1).Visible = False
    Else
        MDIFormInicio.IDM_FOLHAS_TRAB(2).Visible = False
    End If
    
    If gUsaFilaEspera = mediSim Then
        ' nada
    Else
        MDIFormInicio.IDM_REQUISICOES_PRIV(2).Visible = False
    End If
    If gUsaSeroteca = mediSim Then
        ' nada
    Else
        MDIFormInicio.IDM_SEROTECA(0).Visible = False
        MDIFormInicio.IDM_CODIFICAO_SEROTECA(0).Visible = False
    End If
    
    If gUsaControloQualidade = mediSim Then
        ' nada
    Else
        MDIFormInicio.IDM_CONTROLO_QUALIDADE.Visible = False
        MDIFormInicio.IDM_CQ.Visible = False
    End If
    If gAssinaturaActivo = mediSim Then
        ' nada
    Else
        MDIFormInicio.IDM_REQPEND(2).Visible = False
        MDIFormInicio.IDM_REQPEND(3).Visible = False
    End If
    
    If gUsaStocks = mediSim Or gUsaStocksV2 = mediSim Then
        If gUsaStocksV2 = mediSim Then
            MDIFormInicio.IDM_STOCK_MENU(0).Visible = False
            MDIFormInicio.IDM_STOCK_MENU(1).Visible = True
            MDIFormInicio.IDM_MENU_CODIFICACOES_STK_OPT(0).Visible = False
            MDIFormInicio.IDM_MENU_CODIFICACOES_STK_OPT(1).Visible = True
            MDIFormInicio.IDM_MENU_CODIFICACOES_STK_OPT(2).Visible = True
            MDIFormInicio.IDM_MENU_CODIFICACOES_STK_OPT(3).Visible = True
            MDIFormInicio.IDM_MENU_CODIFICACOES_STK_OPT(4).Visible = True
            MDIFormInicio.IDM_MENU_CODIFICACOES_STK_OPT(5).Visible = True
        Else
            MDIFormInicio.IDM_STOCK_MENU(0).Visible = True
            MDIFormInicio.IDM_STOCK_MENU(1).Visible = False
            MDIFormInicio.IDM_MENU_CODIFICACOES_STK_OPT(0).Visible = True
            MDIFormInicio.IDM_MENU_CODIFICACOES_STK_OPT(1).Visible = False
            MDIFormInicio.IDM_MENU_CODIFICACOES_STK_OPT(2).Visible = False
            MDIFormInicio.IDM_MENU_CODIFICACOES_STK_OPT(3).Visible = False
            MDIFormInicio.IDM_MENU_CODIFICACOES_STK_OPT(4).Visible = False
            MDIFormInicio.IDM_MENU_CODIFICACOES_STK_OPT(5).Visible = False
        End If
    Else
        MDIFormInicio.IDM_STOCK.Visible = False
        MDIFormInicio.IDM_MENU_CODIFICACOES_STK.Visible = False
    End If

End Sub
Public Sub AplicacaoVazia(op As Boolean)
    
    
    MDIFormInicio.CoolBar1.Visible = Not op
    MDIFormInicio.StatusBar1.Visible = Not op
    
    If gLAB <> "HSJ" Then
        MDIFormInicio.IDM_AGENDA(1).Visible = False
    End If
    If gTipoInstituicao <> gTipoInstituicaoPrivada Then
        MDIFormInicio.Toolbar2.Buttons(17).Visible = False
    End If
    If gSinave = mediSim Then
        MDIFormInicio.Toolbar2.Buttons(21).Visible = True
    Else
        MDIFormInicio.Toolbar2.Buttons(21).Visible = False
    End If
    'NELSONPSILVA 16.01.2018 UnimedVitoria-1152
    'If gMostraMisDataDiscovery = mediSim Then
        'MDIFormInicio.Toolbar2.Buttons(24).Visible = True
    'Else
        MDIFormInicio.Toolbar2.Buttons(24).Visible = False
    'End If
    
    
    
End Sub

Public Sub DefineAmbitoDeCadaGrupo()
    
    If gCodGrupo = gGrupoAdministradores Then
        BG_LogFile_Erros "........................................"
        BG_LogFile_Erros "NomeBD_AttachOuLocal = " & Chr(34) & NomeBD_AttachOuLocal & Chr(34)
        BG_LogFile_Erros "TipoBD = " & Chr(34) & TipoBD & Chr(34)
        BG_LogFile_Erros "LigacaoODBC = " & Chr(34) & gDSN & Chr(34)
        BG_LogFile_Erros "........................................"
        BG_LogFile_Erros ""
    End If
End Sub

Function InicializacoesINI() As Boolean
    
    Dim iniNomeBD_AttachOuLocal, iniTipoBD, iniDataSource, iniUID, iniPWD, iniLoginTimeout As String
    Dim odbciniDatabase, odbciniHostName As String
    Dim sTeste As String

    'carregar defeni�oes locais que possam nao existir no registo do Windows
    Call ControlPanel

    ' Verifica se j� existe refer�ncia no Registry e atribui gDirServidor
    sTeste = GetSetting(cREGISTRY_NOME, "Geral", "DirServidor", "Nada")
    If sTeste = "Nada" Then
        InicializacoesINI = False
        MDIFormInicio.Show
        AplicacaoVazia True
        Form_Ini.Show
        Exit Function
    Else
        InicializacoesINI = True
        gDirServidor = sTeste
        gDirCliente = GetSetting(cREGISTRY_NOME, "Geral", "DirCliente", "Nada")
    End If

    ' Inicializa��o de formatos
    ' formato do control panel
    gFormatoData = BG_DaRegionalSetting("sShortDate")
    If gFormatoData = "Nada" Then
        gFormatoData = DefLocal.DatasFormato
    End If
    gFormatoHora = BG_DaRegionalSetting("sTimeFormat")
    If gFormatoData = "Nada" Then
        gFormatoData = DefLocal.HorasFormato
    End If
    gSimboloDecimal = DefLocal.ValoresSDecimal
    
    ' Inicializa��o do gSGBD
    iniNomeBD_AttachOuLocal = GetSetting(cREGISTRY_NOME, "SGBD", "NomeBD_AttachOuLocal", "Nada")
    
    iniTipoBD = GetSetting(cREGISTRY_NOME, "SGBD", "TipoBD", "Nada")
    If StrComp(iniTipoBD, "LOCAL", 1) <> 0 And StrComp(iniTipoBD, "ODBC", 1) <> 0 Then
        BG_Mensagem mediMsgBox, "No ficheiro de inicializa��es ter�o que indicar se os dados est�o numa Base de Dados Local ou Remota!", vbCritical, cAPLICACAO_NOME_CURTO & ": Fim do Programa!"
        End
    ElseIf StrComp(iniTipoBD, "LOCAL", 1) = 0 Then
        NomeBD_AttachOuLocal = iniNomeBD_AttachOuLocal
        TipoBD = iniTipoBD
        gDSN = ""
    Else
        iniDataSource = GetSetting(cREGISTRY_NOME, "SGBD", "DataSource", "Nada")
        odbciniHostName = GetSetting(cINI_ODBC, iniDataSource, "HostName", "Nada")
        odbciniDatabase = GetSetting(cINI_ODBC, iniDataSource, "Database", "Nada")
        iniUID = GetSetting(cREGISTRY_NOME, "SGBD", "UID", "Nada")
        iniPWD = GetSetting(cREGISTRY_NOME, "SGBD", "PWD", "Nada")
        
        
        iniPWD = BG_CryptStringDecode(iniPWD)
        iniLoginTimeout = GetSetting(cREGISTRY_NOME, "SGBD", "LoginTimeout", "Nada")
    
        NomeBD_AttachOuLocal = iniNomeBD_AttachOuLocal
        TipoBD = iniTipoBD
        gDSN = iniDataSource
        If odbciniHostName <> "Nada" And Trim(odbciniHostName) <> "" Then gDSN = gDSN & ";HOST=" & odbciniHostName
        If odbciniDatabase <> "Nada" And Trim(odbciniDatabase) <> "" Then gDSN = gDSN & ";DB=" & odbciniDatabase
        If iniUID <> "Nada" And Trim(iniUID) <> "" Then gDSN = gDSN & ";UID=" & iniUID
        If iniPWD <> "Nada" And Trim(iniPWD) <> "" Then
            gDSN = gDSN & ";PWD=" & iniPWD
            gBDPwd = iniPWD
        End If
    End If
    
End Function

Public Sub RegistoAcessos_Saida()
    
    Dim SQLQueryAux As String
    
    If gConexao.state = adStateClosed Then
        Exit Sub
    End If
    
    SQLQueryAux = "UPDATE sl_reg_acessos " & _
                  "SET dt_saida = '" & BG_CvDataParaWhere_ADO(Bg_DaData_ADO) & "', hr_saida = '" & BG_CvHora(Bg_DaHora_ADO) & "' " & _
                  "WHERE cod_acesso = " & gCodAcesso
    
    If (gCodGrupo = gGrupoAdministradores) Then
        BG_LogFile_Erros "Sa�da             -   Data: " & Bg_DaData_ADO & "   -   Hora: " & Bg_DaHora_ADO
    End If
    
    BG_ExecutaQuery_ADO SQLQueryAux

End Sub

Public Sub RegistoAcessos_Entrada()
    
    Dim obTabelaAux As ADODB.recordset
    Dim obComputador As ADODB.recordset
    Dim SQLQueryAux As String
    Dim executavel As String
    Dim versao As String
    executavel = App.Path & "\" & App.EXEName
    versao = App.Major & "." & App.Minor & "." & App.Revision
    If gConexao.state = adStateClosed Then
        Exit Sub
    End If
    
    ' In�cio: Determinar Novo C�digo para Acesso
    Set obTabelaAux = New ADODB.recordset
    
    SQLQueryAux = "SELECT MAX(cod_acesso) AS maximo FROM sl_reg_acessos "
    
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLQueryAux
    obTabelaAux.Open SQLQueryAux, gConexao, adOpenStatic, adLockReadOnly
        
    If BL_HandleNull(obTabelaAux!maximo, "") = "" Then
        gCodAcesso = 1
    Else
        gCodAcesso = obTabelaAux!maximo + 1
    End If
    
    obTabelaAux.Close
    Set obTabelaAux = Nothing
    
    ' Fim: Determinar Novo C�digo para Acesso
    
    SQLQueryAux = "INSERT INTO sl_reg_acessos " & _
                  "     (cod_acesso, cod_computador, cod_utilizador, dt_entrada, hr_entrada, dt_saida, hr_saida, cod_terminal,exe, versao) " & _
                  "VALUES " & _
                  "     ('" & gCodAcesso & "', '" & BL_GetComputerCodigo & "', '" & gCodUtilizador & "', '" & BG_CvDataParaWhere_ADO(Bg_DaData_ADO) & "', '" & BG_CvHora(Bg_DaHora_ADO) & "', null, null, " & _
                  BL_GetTerminalCodigo & "," & BL_TrataStringParaBD(executavel) & "," & BL_TrataStringParaBD(gMajorRelease & versao) & " )"
    
'    If gCodGrupo = gGrupoAdministradores Then BG_LogFile_Erros SQLQueryAux
    BG_ExecutaQuery_ADO SQLQueryAux, "Erro"

End Sub







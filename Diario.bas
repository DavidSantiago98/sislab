Attribute VB_Name = "Diario"
Option Explicit

Private Type frases_det
    cod_frase As String
    descr_frase As String
    ordem As Integer
End Type

Private Type diario_meio_det
    seq_diario_meio_Det As Long
    cod_actividade As Integer
    descR_actividade As String
    cod_estado As Integer
    flg_estado_final As Integer
    flg_estado_final_tec As Integer
    descr_estado As String
    dt_cri As String
    user_cri As String
    nome_cri As String
    nome_act As String
    descricao As String
    indiceTV As Integer
    frases() As frases_det
    totalFrases As Integer
End Type

Private Type diario_meio
    seq_diario_meio As Long
    seq_diario_medio_pai As Long
    cod_meio As Integer
    descr_meio As String
    descr_estado As String
    cod_estado As Integer
    flg_estado_final As Integer
    flg_estado_final_tec As Integer
    dt_cri As String
    user_cri As String
    dt_act As String
    user_act As String
    nome_cri As String
    nome_act As String
    indiceTV As Integer
    detalhe() As diario_meio_det
    totalDetalhe As Integer
End Type

Public Type tDiario
    seq_utente As Long
    t_utente As String
    Utente As String
    nome_ute As String
    sexo_ute As Integer
    dt_nasc_ute As String
    
    n_req As Long
    dt_chega As String
    hr_chega As String
    t_sit As String
    n_epis As String
    estado_req As String
    cod_proven As String
    descr_proven As String
    
    cod_agrup As String
    descr_ana As String
    cod_produto As String
    descr_produto As String
    
    seq_diario As Long
    flg_terminado As Integer
    dt_cri As String
    user_cri As String
    dt_act As String
    user_act As String
    indiceTV As Integer
    seq_req_tubo As Long
    meios() As diario_meio
    totalMeios As Integer
End Type

Global gFrasesDiario() As frases_det
Global gTotalFrases As Integer

' ------------------------------------------------------------------------------------------------

' INICIALIZA  MEIOS DEFEITO

' ------------------------------------------------------------------------------------------------
Public Sub DIA_PreencheMeiosDefeitoAna(formAna As Form)
    Dim sSql As String
    Dim rsMeios As New ADODB.recordset
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_cod_meio where flg_invisivel = 0 or flg_invisivel IS NULL ORDER BY descr_meio "
    Set rsMeios = BG_ExecutaSELECT(sSql)
    If rsMeios.RecordCount > 0 Then
        While Not rsMeios.EOF
        
            formAna.EcMeios.AddItem BL_HandleNull(rsMeios!descr_meio, "")
            formAna.EcMeios.ItemData(formAna.EcMeios.NewIndex) = BL_HandleNull(rsMeios!cod_meio, "")
            rsMeios.MoveNext
        Wend
    End If
    rsMeios.Close
    Set rsMeios = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "DIA_PreencheMeiosDefeitoAna: " & Err.Description, "DIARIO", "DIA_PreencheMeiosDefeitoAna"
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------------------------

' LIMPA DADOS DOS

' ------------------------------------------------------------------------------------------------
Public Sub DIA_LimpaMeiosAna(formE As Form)
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 0 To formE.EcMeios.ListCount - 1
        formE.EcMeios.Selected(i) = False
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "DIA_LimpaMeiosAna: " & Err.Description, "DIARIO", "DIA_LimpaMeiosAna"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' Grava DADOS DOS CAMPOS

' ------------------------------------------------------------------------------------------------
Public Function DIA_GravaMeiosAna(formE As Form, codAna As String) As Boolean
    Dim sSql As String
    Dim RsOrdem As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    DIA_GravaMeiosAna = False
    
    sSql = "DELETE FROM sl_ass_ana_meio WHERE cod_ana = " & BL_TrataStringParaBD(codAna)
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To formE.EcMeios.ListCount - 1
        
        If formE.EcMeios.Selected(i) = True Then
            
            sSql = "INSERT INTO sl_ass_ana_meio (cod_ana, cod_meio) VALUES("
            sSql = sSql & BL_TrataStringParaBD(codAna) & "," & formE.EcMeios.ItemData(i) & ")"
            BG_ExecutaQuery_ADO sSql
        End If
    Next
    DIA_GravaMeiosAna = True
Exit Function
TrataErro:
    DIA_GravaMeiosAna = False
    BG_LogFile_Erros "DIA_GravaMeiosAna: " & Err.Description, "DIARIO", "DIA_GravaMeiosAna", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' LIMPA DADOS DOS CAMPOS DE INFORMA��O DA ANALISE NOS FORMS DE CODIFICACAO DE ANALISE

' ------------------------------------------------------------------------------------------------
Public Sub DIA_PreencheMeiosAna(formE As Form, codAna As String)
    Dim sSql As String
    Dim rsAnaMeio As New ADODB.recordset
    Dim totalFolhasTrab As Long
    Dim i As Integer
    
    
    sSql = "SELECT cod_ana, cod_meio FROM sl_ass_ana_meio WHERE cod_Ana = " & BL_TrataStringParaBD(codAna)
    Set rsAnaMeio = BG_ExecutaSELECT(sSql)
    If rsAnaMeio.RecordCount > 0 Then
        While Not rsAnaMeio.EOF
        
            For i = 0 To formE.EcMeios.ListCount - 1
                If formE.EcMeios.ItemData(i) = BL_HandleNull(rsAnaMeio!cod_meio, mediComboValorNull) Then
                    formE.EcMeios.Selected(i) = True
                End If
            Next
            rsAnaMeio.MoveNext
        Wend
    End If
    rsAnaMeio.Close
    Set rsAnaMeio = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "DIA_PreencheMeiosAna" & Err.Description, "DIARIO", "DIA_PreencheMeiosAna", True
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' INSERE NOVO REGISTO PARA DIARIO

' ------------------------------------------------------------------------------------------------
Public Function DIA_RegistaDiario(seq_utente As String, n_req As String, cod_ana As String, force As Boolean) As Boolean
    Dim sSql As String
    Dim rsDiario As New ADODB.recordset
    Dim rsMeios As New ADODB.recordset
    Dim rsSeq As New ADODB.recordset
    Dim rsReq As New ADODB.recordset
    Dim seq_diario As Long
    Dim rsTubo As New ADODB.recordset
    Dim iReg As Integer
    Dim seq_req_tubo As Long
    
    On Error GoTo TrataErro
    DIA_RegistaDiario = False
    
    
    If seq_utente = mediComboValorNull Then
        Exit Function
    End If
    ' -------------------------------------------------------------------------------------
    ' VERIFICA SE ANALISE J� TEM DIARIO CRIADO
    ' -------------------------------------------------------------------------------------
    sSql = "SELECT * FROM sl_diario WHERE n_req = " & n_req & " AND cod_Agrup = " & BL_TrataStringParaBD(cod_ana)
    rsDiario.CursorLocation = adUseServer
    rsDiario.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDiario.Open sSql, gConexao
    If rsDiario.RecordCount = 0 Then
        ' -------------------------------------------------------------------------------------
        ' VERIFICA SE ANALISE TEM MEIOS CODIFICADOS
        ' -------------------------------------------------------------------------------------
        sSql = "SELECT * FROM sl_ass_ana_meio WHERE cod_ana = " & BL_TrataStringParaBD(cod_ana)
        rsMeios.CursorLocation = adUseServer
        rsMeios.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsMeios.Open sSql, gConexao
        If rsMeios.RecordCount >= 1 Or force = True Then
            
            sSql = "SELECT seq_req_tubo from sl_marcacoes WHERE n_req = " & n_req & " AND COD_AGRUP = " & BL_TrataStringParaBD(cod_ana)
            sSql = sSql & " UNION select seq_req_tubo FROM sl_realiza WHERE n_req  = " & n_req & " AND COD_AGRUP = " & BL_TrataStringParaBD(cod_ana)
            rsTubo.CursorLocation = adUseServer
            rsTubo.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsTubo.Open sSql, gConexao
            If rsTubo.RecordCount > 0 Then
                seq_req_tubo = BL_HandleNull(rsTubo!seq_req_tubo, mediComboValorNull)
            End If
            rsTubo.Close
            Set rsTubo = Nothing
            
            ' -------------------------------------------------------------------------------------
            ' INSERE LINHA NA TABELA SL_DIARIO
            ' -------------------------------------------------------------------------------------
            sSql = "SELECT seq_diario.nextval proximo FROM dual "
            rsSeq.CursorLocation = adUseServer
            rsSeq.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsSeq.Open sSql, gConexao
            If rsSeq.RecordCount = 1 Then
                seq_diario = BL_HandleNull(rsSeq!proximo, mediComboValorNull)
            End If
            rsSeq.Close
            Set rsSeq = Nothing
            
            
            sSql = "INSERT INTO sl_diario(seq_diario,n_req, cod_Agrup, flg_terminado, dt_Cri, user_cri, seq_utente, seq_req_tubo) VALUES("
            sSql = sSql & seq_diario & "," & n_req & "," & BL_TrataStringParaBD(cod_ana) & ",0, sysdate," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & seq_utente & ","
            If seq_req_tubo > mediComboValorNull Then
                sSql = sSql & seq_req_tubo & ")"
            Else
                sSql = sSql & " NULL)"
            End If
            iReg = BG_ExecutaQuery_ADO(sSql)
            If iReg <> 1 Then
                GoTo TrataErro
            End If
            
            While Not rsMeios.EOF
                ' -------------------------------------------------------------------------------------
                ' INSERE MEIO CODIFICADO PARA A ANALISE
                ' -------------------------------------------------------------------------------------
                sSql = "INSERT into sl_diario_meio(seq_diario_meio, seq_diario, cod_meio, cod_estado, dt_cri, user_cri) VALUES("
                sSql = sSql & " seq_diario_meio.nextval, " & seq_diario & "," & BL_HandleNull(rsMeios!cod_meio, mediComboValorNull)
                sSql = sSql & ", 0, sysdate, " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ")"
                iReg = BG_ExecutaQuery_ADO(sSql)
                If iReg <> 1 Then
                    GoTo TrataErro
                End If
                rsMeios.MoveNext
            Wend
        End If
        rsMeios.Close
        Set rsMeios = Nothing
    End If
    rsDiario.Close
    Set rsDiario = Nothing
    DIA_RegistaDiario = True
Exit Function
TrataErro:
    DIA_RegistaDiario = False
    BG_LogFile_Erros "DIARIO: DIA_RegistaDiario: " & Err.Description, "DIARIO", "DIA_RegistaDiario", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTUTURA DO DIARIO PARA UMA REQUISI��O

' ------------------------------------------------------------------------------------------------
Public Function DIA_PreencheDiario(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, ByVal n_req As String, ByVal exame As String) As Boolean
    Dim sSql As String
    Dim rsDiario As New ADODB.recordset
    On Error GoTo TrataErro
    DIA_PreencheDiario = False
    
    sSql = "SELECT x3.seq_utente, x3.t_utente, x3.utente, x3.nome_ute, x3.sexo_ute, x3.dt_nasc_ute, x5.n_req, "
    sSql = sSql & " x5.dt_chega, x5.hr_chega, x5.t_sit, x5.n_epis, x5.estado_req, x5.cod_proven, x6.descr_proven, "
    sSql = sSql & " x1.cod_agrup, x2.cod_produto, x4.descr_produto, x1.dt_cri, x1.dt_act, x1.flg_terminado, "
    sSql = sSql & " x1.seq_diario, x1.user_act, x1.user_cri, x2.descr_ana, x1.seq_req_tubo "
    sSql = sSql & " FROM sl_diario x1 ,slv_analises x2 LEFT OUTER JOIN sl_produto x4 ON x2.cod_produto = x4.cod_produto, "
    sSql = sSql & " sl_identif x3, sl_requis x5  LEFT OUTER JOIN sl_proven x6 ON x5.cod_proven = x6.cod_proven "
    sSql = sSql & " WHERE x1.seq_utente = x3.seq_utente AND x1.cod_agrup = x2.cod_ana AND x1.n_Req = x5.n_Req "
    sSql = sSql & " AND x5.seq_utente = x3.seq_utente AND x1.n_req = " & n_req & " AND x1.cod_agrup= " & BL_TrataStringParaBD(exame)
    rsDiario.CursorLocation = adUseServer
    rsDiario.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDiario.Open sSql, gConexao
    If rsDiario.RecordCount > 0 Then
       If DIA_PreencheEstrutDiario(estrutDiario, totalDiario, BL_HandleNull(rsDiario!seq_utente, mediComboValorNull), BL_HandleNull(rsDiario!t_utente, ""), _
                                   BL_HandleNull(rsDiario!Utente, ""), BL_HandleNull(rsDiario!nome_ute, ""), BL_HandleNull(rsDiario!sexo_ute, mediComboValorNull), _
                                   BL_HandleNull(rsDiario!dt_nasc_ute, ""), BL_HandleNull(rsDiario!n_req, ""), BL_HandleNull(rsDiario!dt_chega, ""), _
                                   BL_HandleNull(rsDiario!hr_chega, ""), BL_HandleNull(rsDiario!t_sit, ""), BL_HandleNull(rsDiario!n_epis, ""), _
                                   BL_HandleNull(rsDiario!estado_req, ""), BL_HandleNull(rsDiario!cod_proven, ""), BL_HandleNull(rsDiario!descr_proven, ""), _
                                   BL_HandleNull(rsDiario!cod_agrup, ""), BL_HandleNull(rsDiario!descr_ana, ""), BL_HandleNull(rsDiario!cod_produto, ""), _
                                   BL_HandleNull(rsDiario!descr_produto, ""), BL_HandleNull(rsDiario!dt_act, ""), BL_HandleNull(rsDiario!dt_cri, ""), _
                                   BL_HandleNull(rsDiario!flg_terminado, 0), BL_HandleNull(rsDiario!seq_diario, mediComboValorNull), _
                                   BL_HandleNull(rsDiario!user_act, ""), BL_HandleNull(rsDiario!user_cri, ""), BL_HandleNull(rsDiario!seq_req_tubo, mediComboValorNull)) = False Then
            GoTo TrataErro
            
        End If
        
        DIA_PreencheDiarioHist estrutDiario, totalDiario
    End If
    rsDiario.Close
    Set rsDiario = Nothing
    DIA_PreencheDiario = True
Exit Function
TrataErro:
    DIA_PreencheDiario = False
    BG_LogFile_Erros "DIARIO: DIA_PreencheDiario: " & Err.Description, "DIARIO", "DIA_PreencheDiario", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA

' ------------------------------------------------------------------------------------------------
Private Function DIA_PreencheEstrutDiario(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, seq_utente As Long, t_utente As String, Utente As String, nome_ute As String, sexo_ute As Integer, _
                                          dt_nasc_ute As String, n_req As Long, dt_chega As String, hr_chega As String, t_sit As String, n_epis As String, _
                                          estado_req As String, cod_proven As String, descr_proven As String, cod_agrup As String, descr_ana As String, _
                                          cod_produto As String, descr_produto As String, dt_act As String, dt_cri As String, flg_terminado As Integer, _
                                          seq_diario As Long, user_act As String, user_cri As String, seq_req_tubo As Long) As Boolean
    Dim sSql As String
    Dim rsSeq As New ADODB.recordset
    
    DIA_PreencheEstrutDiario = False
    On Error GoTo TrataErro
    totalDiario = totalDiario + 1
    ReDim Preserve estrutDiario(totalDiario)
    
    estrutDiario(totalDiario).seq_req_tubo = seq_req_tubo
    estrutDiario(totalDiario).seq_utente = seq_utente
    estrutDiario(totalDiario).t_utente = t_utente
    estrutDiario(totalDiario).Utente = Utente
    estrutDiario(totalDiario).nome_ute = nome_ute
    estrutDiario(totalDiario).sexo_ute = sexo_ute
    estrutDiario(totalDiario).dt_nasc_ute = dt_nasc_ute
    
    estrutDiario(totalDiario).n_req = n_req
    estrutDiario(totalDiario).dt_chega = dt_chega
    estrutDiario(totalDiario).hr_chega = hr_chega
    estrutDiario(totalDiario).t_sit = t_sit
    estrutDiario(totalDiario).n_epis = n_epis
    estrutDiario(totalDiario).estado_req = estado_req
    estrutDiario(totalDiario).cod_proven = cod_proven
    estrutDiario(totalDiario).descr_proven = descr_proven
    
    estrutDiario(totalDiario).cod_agrup = cod_agrup
    estrutDiario(totalDiario).descr_ana = descr_ana
    estrutDiario(totalDiario).cod_produto = cod_produto
    estrutDiario(totalDiario).descr_produto = descr_produto
    
    estrutDiario(totalDiario).dt_act = dt_act
    estrutDiario(totalDiario).dt_cri = dt_cri
    estrutDiario(totalDiario).flg_terminado = flg_terminado
    estrutDiario(totalDiario).seq_diario = seq_diario
    estrutDiario(totalDiario).user_act = user_act
    estrutDiario(totalDiario).user_cri = user_cri
    estrutDiario(totalDiario).totalMeios = 0
    ReDim estrutDiario(totalDiario).meios(0)
    If DIA_PreencheDiarioMeio(estrutDiario, totalDiario, totalDiario) = False Then
        GoTo TrataErro
    End If
    
    DIA_PreencheEstrutDiario = True
Exit Function
TrataErro:
    DIA_PreencheEstrutDiario = False
    BG_LogFile_Erros "DIARIO: DIA_PreencheEstrutDiario: " & Err.Description, "DIARIO", "DIA_PreencheEstrutDiario", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' PREENCHE HISTORICO DE OUTRAS REQUISI��ES PARA MESMO UTENTE

' ------------------------------------------------------------------------------------------------
Public Function DIA_PreencheDiarioHist(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer) As Boolean
    Dim sSql As String
    Dim rsDiario As New ADODB.recordset
    On Error GoTo TrataErro
    DIA_PreencheDiarioHist = False
    
    sSql = "SELECT x3.seq_utente, x3.t_utente, x3.utente, x3.nome_ute, x3.sexo_ute, x3.dt_nasc_ute, x5.n_req, "
    sSql = sSql & " x5.dt_chega, x5.hr_chega, x5.t_sit, x5.n_epis, x5.estado_req, x5.cod_proven, x6.descr_proven, "
    sSql = sSql & " x1.cod_agrup, x2.cod_produto, x4.descr_produto, x1.dt_cri, x1.dt_act, x1.flg_terminado, "
    sSql = sSql & " x1.seq_diario, x1.user_act, x1.user_cri, x2.descr_ana, x1.seq_req_tubo "
    sSql = sSql & " FROM sl_diario x1 ,slv_analises x2 LEFT OUTER JOIN sl_produto x4 ON x2.cod_produto = x4.cod_produto, "
    sSql = sSql & " sl_identif x3, sl_requis x5  LEFT OUTER JOIN sl_proven x6 ON x5.cod_proven = x6.cod_proven "
    sSql = sSql & " WHERE x1.seq_utente = x3.seq_utente AND x1.cod_agrup = x2.cod_ana AND x1.n_Req = x5.n_Req "
    sSql = sSql & " AND x5.seq_utente = x3.seq_utente AND x1.seq_utente =" & estrutDiario(1).seq_utente
    sSql = sSql & " ORDER BY x1.dt_Cri ASC "
    rsDiario.CursorLocation = adUseServer
    rsDiario.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDiario.Open sSql, gConexao
    If rsDiario.RecordCount > 0 Then
        While Not rsDiario.EOF
            If BL_HandleNull(rsDiario!seq_diario, mediComboValorNull) <> estrutDiario(1).seq_diario Then
               If DIA_PreencheEstrutDiario(estrutDiario, totalDiario, BL_HandleNull(rsDiario!seq_utente, mediComboValorNull), BL_HandleNull(rsDiario!t_utente, ""), _
                                           BL_HandleNull(rsDiario!Utente, ""), BL_HandleNull(rsDiario!nome_ute, ""), BL_HandleNull(rsDiario!sexo_ute, mediComboValorNull), _
                                           BL_HandleNull(rsDiario!dt_nasc_ute, ""), BL_HandleNull(rsDiario!n_req, ""), BL_HandleNull(rsDiario!dt_chega, ""), _
                                           BL_HandleNull(rsDiario!hr_chega, ""), BL_HandleNull(rsDiario!t_sit, ""), BL_HandleNull(rsDiario!n_epis, ""), _
                                           BL_HandleNull(rsDiario!estado_req, ""), BL_HandleNull(rsDiario!cod_proven, ""), BL_HandleNull(rsDiario!descr_proven, ""), _
                                           BL_HandleNull(rsDiario!cod_agrup, ""), BL_HandleNull(rsDiario!descr_ana, ""), BL_HandleNull(rsDiario!cod_produto, ""), _
                                           BL_HandleNull(rsDiario!descr_produto, ""), BL_HandleNull(rsDiario!dt_act, ""), BL_HandleNull(rsDiario!dt_cri, ""), _
                                           BL_HandleNull(rsDiario!flg_terminado, 0), BL_HandleNull(rsDiario!seq_diario, mediComboValorNull), _
                                           BL_HandleNull(rsDiario!user_act, ""), BL_HandleNull(rsDiario!user_cri, ""), BL_HandleNull(rsDiario!seq_req_tubo, mediComboValorNull)) = False Then
                    GoTo TrataErro
                    
                End If
            End If
            rsDiario.MoveNext
        Wend
    End If
    rsDiario.Close
    Set rsDiario = Nothing
    DIA_PreencheDiarioHist = True
Exit Function
TrataErro:
    DIA_PreencheDiarioHist = False
    BG_LogFile_Erros "DIARIO: DIA_PreencheDiarioHist: " & Err.Description, "DIARIO", "DIA_PreencheDiarioHist", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' PARA CADA DIARIO PREENCHE OS MEIOS ASSOCIADOS

' ------------------------------------------------------------------------------------------------
Public Function DIA_PreencheDiarioMeio(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, indice As Integer) As Boolean
    Dim sSql As String
    Dim rsDiario As New ADODB.recordset
    Dim flg_existe As Boolean
    On Error GoTo TrataErro
    
    DIA_PreencheDiarioMeio = False
        
    sSql = "SELECT x1.cod_estado, x1.cod_meio, descr_meio, seq_diario_meio, x1.user_cri, x1.user_act, x1.dt_act, x1.dt_cri,x3.descr_estado, "
    sSql = sSql & " x3.flg_estado_final, x3.flg_estado_final_tec, x1.seq_diario_meio_pai "
    sSql = sSql & " FROM sl_diario_meio x1, sl_cod_meio x2, sl_tbf_estado_meio x3 WHERE seq_diario = " & estrutDiario(indice).seq_diario
    sSql = sSql & " AND x1.cod_meio = x2.cod_meio and x1.cod_Estado = x3.cod_estado ORDER by seq_diario_meio_pai DESC , seq_diario_meio ASC "
    rsDiario.CursorLocation = adUseServer
    rsDiario.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDiario.Open sSql, gConexao
    If rsDiario.RecordCount > 0 Then
        estrutDiario(indice).totalMeios = 0
        ReDim estrutDiario(indice).meios(0)
        While Not rsDiario.EOF
        
            If DIA_AdicionaEstrutMeio(estrutDiario, totalDiario, indice, BL_HandleNull(rsDiario!cod_estado, mediComboValorNull), BL_HandleNull(rsDiario!cod_meio, mediComboValorNull), _
                                       BL_HandleNull(rsDiario!descr_meio, ""), BL_HandleNull(rsDiario!descr_estado, ""), BL_HandleNull(rsDiario!flg_estado_final, mediComboValorNull), _
                                       BL_HandleNull(rsDiario!seq_diario_meio, mediComboValorNull), BL_HandleNull(rsDiario!user_cri, ""), BL_HandleNull(rsDiario!user_act, ""), _
                                       BL_HandleNull(rsDiario!dt_act, ""), BL_HandleNull(rsDiario!dt_cri, ""), BL_HandleNull(rsDiario!flg_estado_final_tec, mediComboValorNull), _
                                        BL_HandleNull(rsDiario!seq_diario_meio_pai, mediComboValorNull)) = False Then
                GoTo TrataErro
            End If
            If DIA_PreencheDiarioMeioDetalhe(estrutDiario, totalDiario, indice, estrutDiario(indice).totalMeios) = False Then
                GoTo TrataErro
            End If
            rsDiario.MoveNext
        Wend
    End If
    rsDiario.Close
    Set rsDiario = Nothing
    DIA_PreencheDiarioMeio = True
Exit Function
TrataErro:
    DIA_PreencheDiarioMeio = False
    BG_LogFile_Erros "DIARIO: DIA_PreencheDiarioMeio: " & Err.Description, "DIARIO", "DIA_PreencheDiarioMeio", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' PARA CADA MEIO, PREENCHE OS DETALHES ASSOCIADOS

' ------------------------------------------------------------------------------------------------
Private Function DIA_PreencheDiarioMeioDetalhe(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, iDia As Integer, iMeio As Integer) As Boolean
    Dim sSql As String
    Dim rsDiario As New ADODB.recordset
    Dim flg_existe As Boolean
    On Error GoTo TrataErro
    
    DIA_PreencheDiarioMeioDetalhe = False
    DIA_LimpaDiarioMeioDetalhe estrutDiario, totalDiario, iDia, iMeio
        
    sSql = "select x1.seq_diario_meio_det, x1.cod_actividade, x3.descr_actividade,x1.cod_estado, x2.descr_estado,x1.descricao, x1.dt_cri,x1.user_cri, x2.flg_estado_final, x2.flg_estado_final_Tec "
    sSql = sSql & " FROM sl_diario_meio_det x1, sl_tbf_estado_meio x2, sl_cod_actividade x3 where x1.cod_estado = x2.cod_estado "
    sSql = sSql & " AND x1.cod_actividade = x3.cod_Actividade AND x1.seq_diario_meio =" & estrutDiario(iDia).meios(iMeio).seq_diario_meio
    rsDiario.CursorLocation = adUseServer
    rsDiario.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDiario.Open sSql, gConexao
    If rsDiario.RecordCount > 0 Then
        
        While Not rsDiario.EOF
            If DIA_NovoDiarioMeioDetalhe(estrutDiario, totalDiario, iDia, iMeio, BL_HandleNull(rsDiario!cod_actividade, ""), BL_HandleNull(rsDiario!cod_estado, ""), _
                                         BL_HandleNull(rsDiario!descR_actividade, ""), BL_HandleNull(rsDiario!descr_estado, ""), _
                                         BL_HandleNull(rsDiario!flg_estado_final, 0), BL_HandleNull(rsDiario!descricao, ""), _
                                         BL_HandleNull(rsDiario!dt_cri, ""), BL_HandleNull(rsDiario!seq_diario_meio_Det, mediComboValorNull), _
                                         BL_HandleNull(rsDiario!user_cri, ""), BL_HandleNull(rsDiario!flg_estado_final_tec, 0)) = False Then
                GoTo TrataErro
            End If
            rsDiario.MoveNext
        Wend
    End If
    rsDiario.Close
    Set rsDiario = Nothing
    DIA_PreencheDiarioMeioDetalhe = True
Exit Function
TrataErro:
    DIA_PreencheDiarioMeioDetalhe = False
    BG_LogFile_Erros "DIARIO: DIA_PreencheDiarioMeioDetalhe: " & Err.Description, "DIARIO", "DIA_PreencheDiarioMeioDetalhe", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' ADICIONA DETALHE � ESTRUTURA

' ------------------------------------------------------------------------------------------------

Public Function DIA_NovoDiarioMeioDetalhe(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, iDia As Integer, iMeio As Integer, cod_actividade As Integer, cod_estado As Integer, _
                                     descR_actividade As String, descr_estado As String, flg_estado_final As Integer, descricao As String, _
                                     dt_cri As String, seq_dario_meido_det As Long, user_cri As String, flg_estado_final_tec As Integer) As Boolean
    On Error GoTo TrataErro
    DIA_NovoDiarioMeioDetalhe = False
    estrutDiario(iDia).meios(iMeio).totalDetalhe = estrutDiario(iDia).meios(iMeio).totalDetalhe + 1
    ReDim Preserve estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe)
    
    estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).cod_actividade = cod_actividade
    estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).cod_estado = cod_estado
    estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).descR_actividade = descR_actividade
    estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).descr_estado = descr_estado
    estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).flg_estado_final = flg_estado_final
    estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).flg_estado_final_tec = flg_estado_final_tec
    estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).descricao = descricao
    estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).seq_diario_meio_Det = seq_dario_meido_det
    estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).dt_cri = dt_cri
    estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).user_cri = user_cri
    If estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).totalFrases = 0 Then
        estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).totalFrases = gTotalFrases
        estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).frases = gFrasesDiario
    End If
    
    If user_cri <> "" Then
        estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).nome_cri = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", estrutDiario(iDia).meios(iMeio).detalhe(estrutDiario(iDia).meios(iMeio).totalDetalhe).user_cri)
    End If
    If seq_dario_meido_det = mediComboValorNull Then
        If DIA_NovoDiarioMeioDetalheBD(estrutDiario, totalDiario, iDia, iMeio, estrutDiario(iDia).meios(iMeio).totalDetalhe) = False Then
            GoTo TrataErro
        End If
        If DIA_NovoDiarioMeioDetalheFrasesBD(estrutDiario, totalDiario, iDia, iMeio, estrutDiario(iDia).meios(iMeio).totalDetalhe) = False Then
            GoTo TrataErro
        End If
        
        If DIA_ActualizaEstadoMeio(estrutDiario, totalDiario, iDia, iMeio, estrutDiario(iDia).meios(iMeio).totalDetalhe) = False Then
            GoTo TrataErro
        End If
    End If
    DIA_NovoDiarioMeioDetalhe = True
Exit Function
TrataErro:
    DIA_NovoDiarioMeioDetalhe = False
    BG_LogFile_Erros "DIARIO: DIA_NovoDiarioMeioDetalhe: " & Err.Description, "DIARIO", "DIA_NovoDiarioMeioDetalhe", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' LIMPA DETALHES DE CADA MEIO

' ------------------------------------------------------------------------------------------------
Private Sub DIA_LimpaDiarioMeioDetalhe(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, iDia As Integer, iMeio As Integer)
    estrutDiario(iDia).meios(iMeio).totalDetalhe = 0
    ReDim estrutDiario(iDia).meios(iMeio).detalhe(0)
End Sub

' ------------------------------------------------------------------------------------------------

' INSERE MEIO NA BASE DADOS SL_DIARIO_MEIO_DET

' ------------------------------------------------------------------------------------------------
Private Function DIA_NovoDiarioMeioDetalheBD(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, iDia As Integer, iMeio As Integer, iActividade As Integer) As Boolean
    Dim sSql As String
    Dim rsSeq As New ADODB.recordset
    On Error GoTo TrataErro
    DIA_NovoDiarioMeioDetalheBD = False
    
    sSql = "SELECT seq_diario_meio_det.nextval proximo, sysdate data FROM dual"
    rsSeq.CursorLocation = adUseServer
    rsSeq.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSeq.Open sSql, gConexao
    If rsSeq.RecordCount = 1 Then
        estrutDiario(iDia).meios(iMeio).detalhe(iActividade).dt_cri = BL_HandleNull(rsSeq!data, "")
        estrutDiario(iDia).meios(iMeio).detalhe(iActividade).nome_cri = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", estrutDiario(iDia).meios(iMeio).detalhe(iActividade).user_cri)
        estrutDiario(iDia).meios(iMeio).detalhe(iActividade).user_cri = CStr(gCodUtilizador)
        estrutDiario(iDia).meios(iMeio).detalhe(iActividade).seq_diario_meio_Det = BL_HandleNull(rsSeq!proximo, mediComboValorNull)
        estrutDiario(iDia).meios(iMeio).detalhe(iActividade).flg_estado_final = BL_SelCodigo("sl_tbf_estado_meio", "flg_estado_final", "cod_estado", estrutDiario(iDia).meios(iMeio).detalhe(iActividade).cod_estado)
        estrutDiario(iDia).meios(iMeio).detalhe(iActividade).flg_estado_final_tec = BL_SelCodigo("sl_tbf_estado_meio", "flg_estado_final_tec", "cod_estado", estrutDiario(iDia).meios(iMeio).detalhe(iActividade).cod_estado)
        
        sSql = "INSERT INTO sl_diario_meio_det(seq_diario_meio_Det,seq_diario_meio, cod_actividade, cod_estado, descricao, user_cri, dt_cri) VALUES ("
        sSql = sSql & estrutDiario(iDia).meios(iMeio).detalhe(iActividade).seq_diario_meio_Det & ","
        sSql = sSql & estrutDiario(iDia).meios(iMeio).seq_diario_meio & ","
        sSql = sSql & estrutDiario(iDia).meios(iMeio).detalhe(iActividade).cod_actividade & ","
        If estrutDiario(iDia).meios(iMeio).detalhe(iActividade).cod_estado > mediComboValorNull Then
            sSql = sSql & estrutDiario(iDia).meios(iMeio).detalhe(iActividade).cod_estado & ","
        Else
            sSql = sSql & " NULL,"
        End If
        sSql = sSql & BL_TrataStringParaBD(estrutDiario(iDia).meios(iMeio).detalhe(iActividade).descricao) & ","
        sSql = sSql & BL_TrataStringParaBD(estrutDiario(iDia).meios(iMeio).detalhe(iActividade).user_cri) & ","
        sSql = sSql & "sysdate)"
        If BG_ExecutaQuery_ADO(sSql) <> 1 Then
            GoTo TrataErro
        End If
    End If
    DIA_NovoDiarioMeioDetalheBD = True
Exit Function
TrataErro:
    DIA_NovoDiarioMeioDetalheBD = False
    BG_LogFile_Erros "DIARIO: DIA_NovoDiarioMeioDetalheBD: " & Err.Description, "DIARIO", "DIA_NovoDiarioMeioDetalheBD", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' INSERE NA BD FRASES ASSOCIADAS A CADA DETALHE DE CADA MEIO

' ------------------------------------------------------------------------------------------------
Private Function DIA_NovoDiarioMeioDetalheFrasesBD(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, iDia As Integer, iMeio As Integer, iActividade As Integer) As Boolean
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    DIA_NovoDiarioMeioDetalheFrasesBD = False
    For i = 1 To estrutDiario(iDia).meios(iMeio).detalhe(iActividade).totalFrases
        sSql = "INSERT INTO sl_diario_meio_det_frase(seq_diario_meio_Det, COD_FRASE, ordem) VALUES ("
        sSql = sSql & estrutDiario(iDia).meios(iMeio).detalhe(iActividade).seq_diario_meio_Det & ","
        sSql = sSql & BL_TrataStringParaBD(estrutDiario(iDia).meios(iMeio).detalhe(iActividade).frases(i).cod_frase) & ","
        sSql = sSql & i & ") "
        If BG_ExecutaQuery_ADO(sSql) <> 1 Then
            GoTo TrataErro
        End If
    Next i
    DIA_NovoDiarioMeioDetalheFrasesBD = True
Exit Function
TrataErro:
    DIA_NovoDiarioMeioDetalheFrasesBD = False
    BG_LogFile_Erros "DIARIO: DIA_NovoDiarioMeioDetalheFrasesBD: " & Err.Description, "DIARIO", "DIA_NovoDiarioMeioDetalheFrasesBD", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' INSERE MEIO NA BASE DADOS

' ------------------------------------------------------------------------------------------------
Private Function DIA_NovoDiarioMeioeBD(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, iDia As Integer, iMeio As Integer) As Boolean
    Dim sSql As String
    Dim rsSeq As New ADODB.recordset
    On Error GoTo TrataErro
    
    DIA_NovoDiarioMeioeBD = False
    sSql = "SELECT seq_diario_meio.nextval proximo, sysdate data FROM dual"
    rsSeq.CursorLocation = adUseServer
    rsSeq.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSeq.Open sSql, gConexao
    If rsSeq.RecordCount = 1 Then
        estrutDiario(iDia).meios(iMeio).dt_cri = BL_HandleNull(rsSeq!data, "")
        estrutDiario(iDia).meios(iMeio).user_cri = CStr(gCodUtilizador)
        estrutDiario(iDia).meios(iMeio).seq_diario_meio = BL_HandleNull(rsSeq!proximo, mediComboValorNull)
        
        sSql = "INSERT INTO sl_diario_meio(seq_diario_meio, seq_diario, cod_estado, cod_meio, user_cri, dt_cri,seq_diario_meio_pai) VALUES ("
        sSql = sSql & estrutDiario(iDia).meios(iMeio).seq_diario_meio & ","
        sSql = sSql & estrutDiario(iDia).seq_diario & ","
        sSql = sSql & estrutDiario(iDia).meios(iMeio).cod_estado & ","
        sSql = sSql & estrutDiario(iDia).meios(iMeio).cod_meio & ","
        sSql = sSql & BL_TrataStringParaBD(estrutDiario(iDia).meios(iMeio).user_cri) & ","
        sSql = sSql & "sysdate,"
        If estrutDiario(iDia).meios(iMeio).seq_diario_medio_pai > mediComboValorNull Then
            sSql = sSql & estrutDiario(iDia).meios(iMeio).seq_diario_medio_pai
        Else
            sSql = sSql & " NULL "
        End If
        sSql = sSql & ")"
        
        If BG_ExecutaQuery_ADO(sSql) <> 1 Then
            GoTo TrataErro
        End If
    End If
    DIA_NovoDiarioMeioeBD = True
Exit Function
TrataErro:
    DIA_NovoDiarioMeioeBD = False
    BG_LogFile_Erros "DIARIO: DIA_NovoDiarioMeioeBD: " & Err.Description, "DIARIO", "DIA_NovoDiarioMeioeBD", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' INSERE MEIO NA ESTRUTURA

' ------------------------------------------------------------------------------------------------
Public Function DIA_AdicionaEstrutMeio(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, ByVal indice As Integer, ByVal cod_estado As Integer, ByVal cod_meio As Long, ByVal descr_meio As String, _
                                        ByVal descr_estado As String, ByVal flg_estado_final As Integer, ByVal seq_diario_meio As Long, _
                                        ByVal user_cri As String, ByVal user_act As String, ByVal dt_act As String, ByVal dt_cri As String, _
                                        ByVal flg_estado_final_tec As Integer, ByVal seq_diario_meio_pai As Long) As Boolean
    On Error GoTo TrataErro
    DIA_AdicionaEstrutMeio = False
    
    estrutDiario(indice).totalMeios = estrutDiario(indice).totalMeios + 1
    ReDim Preserve estrutDiario(indice).meios(estrutDiario(indice).totalMeios)
    
    estrutDiario(indice).meios(estrutDiario(indice).totalMeios).cod_meio = cod_meio
    estrutDiario(indice).meios(estrutDiario(indice).totalMeios).seq_diario_medio_pai = seq_diario_meio_pai
    If descr_meio = "" Then
        If DIA_PreencheDescrMeio(estrutDiario, totalDiario, indice, estrutDiario(indice).totalMeios) = False Then
            GoTo TrataErro
        End If
    Else
        estrutDiario(indice).meios(estrutDiario(indice).totalMeios).descr_meio = descr_meio
    End If
    
    ' SE N�O TIVER ESTADO DEFINIDO � PORQUE SE TRATA DE UM ESTADO INICIAL
    If cod_estado = mediComboValorNull Then
        If DIA_PreencheEstadoInicial(estrutDiario, totalDiario, indice, estrutDiario(indice).totalMeios) = False Then
            GoTo TrataErro
        End If
    Else
        estrutDiario(indice).meios(estrutDiario(indice).totalMeios).cod_estado = cod_estado
        estrutDiario(indice).meios(estrutDiario(indice).totalMeios).descr_estado = descr_estado
        estrutDiario(indice).meios(estrutDiario(indice).totalMeios).flg_estado_final = flg_estado_final
        estrutDiario(indice).meios(estrutDiario(indice).totalMeios).flg_estado_final_tec = flg_estado_final_tec
    End If
    
    ' SE N�O TIVER SEQUENCIAL E PORQUE SE TRATA DE UM NOVO REGISTO E INSERE NA BD
    If seq_diario_meio = mediComboValorNull Then
        If DIA_NovoDiarioMeioeBD(estrutDiario, totalDiario, indice, estrutDiario(indice).totalMeios) = False Then
            GoTo TrataErro
        End If
    Else
        estrutDiario(indice).meios(estrutDiario(indice).totalMeios).seq_diario_meio = seq_diario_meio
        estrutDiario(indice).meios(estrutDiario(indice).totalMeios).user_cri = user_cri
        estrutDiario(indice).meios(estrutDiario(indice).totalMeios).dt_cri = dt_cri
    End If
    estrutDiario(indice).meios(estrutDiario(indice).totalMeios).user_act = user_act
    estrutDiario(indice).meios(estrutDiario(indice).totalMeios).user_act = user_act
    estrutDiario(indice).meios(estrutDiario(indice).totalMeios).dt_act = dt_act
    estrutDiario(indice).meios(estrutDiario(indice).totalMeios).nome_act = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", estrutDiario(indice).meios(estrutDiario(indice).totalMeios).user_act)
    estrutDiario(indice).meios(estrutDiario(indice).totalMeios).nome_cri = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", estrutDiario(indice).meios(estrutDiario(indice).totalMeios).user_cri)
    estrutDiario(indice).meios(estrutDiario(indice).totalMeios).totalDetalhe = 0
    ReDim estrutDiario(indice).meios(estrutDiario(indice).totalMeios).detalhe(0)
    DIA_AdicionaEstrutMeio = True
Exit Function
TrataErro:
    DIA_AdicionaEstrutMeio = False
    BG_LogFile_Erros "DIARIO: DIA_AdicionaEstrutMeio: " & Err.Description, "DIARIO", "DIA_AdicionaEstrutMeio", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' QUANDO CRIA UM MEIO ASSOCIA UM ESTADO INICIAL

' ------------------------------------------------------------------------------------------------
Private Function DIA_PreencheEstadoInicial(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, indiceDiario As Integer, indiceMeio As Integer) As Boolean
    Dim sSql As String
    Dim RsEstado As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_tbf_estado_meio WHERE flg_estado_inicial = 1 "
    RsEstado.CursorLocation = adUseServer
    RsEstado.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEstado.Open sSql, gConexao
    If RsEstado.RecordCount = 1 Then
        estrutDiario(indiceDiario).meios(indiceMeio).cod_estado = BL_HandleNull(RsEstado!cod_estado, mediComboValorNull)
        estrutDiario(indiceDiario).meios(indiceMeio).descr_estado = BL_HandleNull(RsEstado!descr_estado, mediComboValorNull)
        DIA_PreencheEstadoInicial = True
    End If
    RsEstado.Close
    Set RsEstado = Nothing
Exit Function
TrataErro:
    DIA_PreencheEstadoInicial = False
    BG_LogFile_Erros "DIARIO: DIA_PreencheEstadoInicial: " & Err.Description, "DIARIO", "DIA_PreencheEstadoInicial", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' PREENCHE DESCRICAO DO MEIO

' ------------------------------------------------------------------------------------------------
Private Function DIA_PreencheDescrMeio(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, indiceDiario As Integer, indiceMeio As Integer) As Boolean
    Dim sSql As String
    Dim RsEstado As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_cod_meio WHERE cod_meio = " & estrutDiario(indiceDiario).meios(indiceMeio).cod_meio
    RsEstado.CursorLocation = adUseServer
    RsEstado.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEstado.Open sSql, gConexao
    If RsEstado.RecordCount = 1 Then
        estrutDiario(indiceDiario).meios(indiceMeio).descr_meio = BL_HandleNull(RsEstado!descr_meio, "")
        DIA_PreencheDescrMeio = True
    End If
    RsEstado.Close
    Set RsEstado = Nothing
Exit Function
TrataErro:
    DIA_PreencheDescrMeio = False
    BG_LogFile_Erros "DIARIO: DIA_PreencheDescrMeio: " & Err.Description, "DIARIO", "DIA_PreencheDescrMeio", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' DEPOIS DE INSERIR DETALHE, ACTUALIZA ESTADO DO MEIO

' ------------------------------------------------------------------------------------------------
Public Function DIA_ActualizaEstadoMeio(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, indiceDiario As Integer, indiceMeio As Integer, indiceActividade As Integer) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    DIA_ActualizaEstadoMeio = False
    
    estrutDiario(indiceDiario).meios(indiceMeio).cod_estado = estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).cod_estado
    estrutDiario(indiceDiario).meios(indiceMeio).descr_estado = estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).descr_estado
    estrutDiario(indiceDiario).meios(indiceMeio).user_act = estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).user_cri
    estrutDiario(indiceDiario).meios(indiceMeio).dt_act = estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).dt_cri
    estrutDiario(indiceDiario).meios(indiceMeio).flg_estado_final = estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).flg_estado_final
    estrutDiario(indiceDiario).meios(indiceMeio).flg_estado_final_tec = estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).flg_estado_final_tec
    
    sSql = "UPDATE sl_diario_meio SET cod_estado =" & estrutDiario(indiceDiario).meios(indiceMeio).cod_estado & ","
    sSql = sSql & "user_act = " & BL_TrataStringParaBD(estrutDiario(indiceDiario).meios(indiceMeio).user_act) & ","
    sSql = sSql & "dt_Act = " & BL_TrataDataParaBD(estrutDiario(indiceDiario).meios(indiceMeio).dt_act)
    sSql = sSql & " WHERE seq_diario_meio = " & estrutDiario(indiceDiario).meios(indiceMeio).seq_diario_meio
    If BG_ExecutaQuery_ADO(sSql) <> 1 Then
        GoTo TrataErro
    End If
    DIA_ActualizaEstadoMeio = True
Exit Function
TrataErro:
    DIA_ActualizaEstadoMeio = False
    BG_LogFile_Erros "DIARIO: DIA_ActualizaEstadoMeio: " & Err.Description, "DIARIO", "DIA_ActualizaEstadoMeio", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' PREENCHE ESTRUTURA DE FRASES ASSOCIADAS A UM DETALHE

' ------------------------------------------------------------------------------------------------
Private Function DIA_PreencheEstrutFrases(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, indiceDiario As Integer, indiceMeio As Integer, indiceActividade As Integer) As Boolean
    Dim sSql As String
    Dim rsFrases As New ADODB.recordset
    On Error GoTo TrataErro
    DIA_PreencheEstrutFrases = False
    
    sSql = "SELECT x1.cod_frase, x2.descr_Frase FROM sl_diario_meio_det_frase x1, sl_dicionario x2 WHERE x1.cod_frase = x2.cod_frase ORDER by x1.ordem"
    rsFrases.CursorLocation = adUseServer
    rsFrases.CursorType = adOpenStatic
    rsFrases.Open sSql, gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    If rsFrases.RecordCount >= 1 Then
        ReDim estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).frases(0)
        estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).totalFrases = 0
        While Not rsFrases.EOF
            estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).frases(estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).totalFrases).cod_frase = BL_HandleNull(rsFrases!cod_frase, "")
            estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).frases(estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).totalFrases).cod_frase = BL_HandleNull(rsFrases!descr_frase, "")
            estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).frases(estrutDiario(indiceDiario).meios(indiceMeio).detalhe(indiceActividade).totalFrases).cod_frase = BL_HandleNull(rsFrases!ordem, mediComboValorNull)
            rsFrases.MoveNext
        Wend
    End If
    rsFrases.Close
    Set rsFrases = Nothing
    
    
    DIA_PreencheEstrutFrases = True
Exit Function
TrataErro:
    DIA_PreencheEstrutFrases = False
    BG_LogFile_Erros "DIARIO: DIA_PreencheEstrutFrases: " & Err.Description, "DIARIO", "DIA_PreencheEstrutFrases", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' CONSTROI CRITERIO PARA PREENCHER ESTRUTURA DIARIO, COM MEIOS PENDENTES

' ------------------------------------------------------------------------------------------------

Public Function DIA_PreencheEstrutDiarioPendentes(ByRef estrutDiario() As tDiario, ByRef totalDiario As Integer, PermValidacao As Integer, condicao As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsPend As New ADODB.recordset
    
    On Error GoTo TrataErro
    DIA_PreencheEstrutDiarioPendentes = False
    totalDiario = 0
    ReDim estrutDiario(0)
    
    sSql = "SELECT x3.seq_utente, x3.t_utente, x3.utente, x3.nome_ute, x3.sexo_ute, x3.dt_nasc_ute, x5.n_req, "
    sSql = sSql & " x5.dt_chega, x5.hr_chega, x5.t_sit, x5.n_epis, x5.estado_req, x5.cod_proven, x6.descr_proven, "
    sSql = sSql & " x1.cod_agrup, x2.cod_produto, x4.descr_produto, x1.dt_cri, x1.dt_act, x1.flg_terminado, "
    sSql = sSql & " x1.seq_diario, x1.user_act, x1.user_cri, x2.descr_ana, x1.seq_req_tubo "
    sSql = sSql & " FROM sl_diario x1 ,slv_analises x2 LEFT OUTER JOIN sl_produto x4 ON x2.cod_produto = x4.cod_produto "
    sSql = sSql & " LEFT OUTER JOIN sl_gr_ana x9 ON x2.gr_ana = x9.cod_gr_ana, "
    sSql = sSql & " sl_identif x3, sl_requis x5  LEFT OUTER JOIN sl_proven x6 ON x5.cod_proven = x6.cod_proven "
    sSql = sSql & " WHERE x1.seq_utente = x3.seq_utente AND x1.cod_agrup = x2.cod_ana AND x1.n_Req = x5.n_Req "
    sSql = sSql & " AND x5.seq_utente = x3.seq_utente AND x1.seq_diario IN ("
    sSql = sSql & " SELECT  x7.seq_diario from sl_diario_meio x7 , sl_tbf_estado_meio x8 where x7.seq_diario = x1.seq_diario "
    sSql = sSql & " AND x7.cod_estado = x8.cod_estado "
    If PermValidacao = cValMedRes Then
        sSql = sSql & " AND ( x8.flg_estado_final = 0 OR x8.flg_estado_final IS NULL)"
    ElseIf PermValidacao = cValTecRes Then
        sSql = sSql & " AND ( x8.flg_estado_final_tec = 0 OR x8.flg_estado_final_tec IS NULL)"
    End If
    sSql = sSql & ")"
    If condicao <> "" Then
        sSql = sSql & condicao
    End If
    sSql = sSql & " ORDER BY x3.seq_utente ASC , x5.n_req ASC, x1.seq_diario ASC "
    rsPend.CursorLocation = adUseServer
    rsPend.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsPend.Open sSql, gConexao
    If rsPend.RecordCount > 0 Then
        While Not rsPend.EOF
            If DIA_PreencheEstrutDiario(estrutDiario, totalDiario, BL_HandleNull(rsPend!seq_utente, mediComboValorNull), BL_HandleNull(rsPend!t_utente, ""), _
                                        BL_HandleNull(rsPend!Utente, ""), BL_HandleNull(rsPend!nome_ute, ""), BL_HandleNull(rsPend!sexo_ute, mediComboValorNull), _
                                        BL_HandleNull(rsPend!dt_nasc_ute, ""), BL_HandleNull(rsPend!n_req, ""), BL_HandleNull(rsPend!dt_chega, ""), _
                                        BL_HandleNull(rsPend!hr_chega, ""), BL_HandleNull(rsPend!t_sit, ""), BL_HandleNull(rsPend!n_epis, ""), _
                                        BL_HandleNull(rsPend!estado_req, ""), BL_HandleNull(rsPend!cod_proven, ""), BL_HandleNull(rsPend!descr_proven, ""), _
                                        BL_HandleNull(rsPend!cod_agrup, ""), BL_HandleNull(rsPend!descr_ana, ""), BL_HandleNull(rsPend!cod_produto, ""), _
                                        BL_HandleNull(rsPend!descr_produto, ""), BL_HandleNull(rsPend!dt_act, ""), BL_HandleNull(rsPend!dt_cri, ""), _
                                        BL_HandleNull(rsPend!flg_terminado, 0), BL_HandleNull(rsPend!seq_diario, mediComboValorNull), _
                                        BL_HandleNull(rsPend!user_act, ""), BL_HandleNull(rsPend!user_cri, ""), BL_HandleNull(rsPend!seq_req_tubo, mediComboValorNull)) = False Then
                 GoTo TrataErro
                 
             End If
            rsPend.MoveNext
        Wend
    End If
    rsPend.Close
    Set rsPend = Nothing
    DIA_PreencheEstrutDiarioPendentes = True
Exit Function
TrataErro:
    DIA_PreencheEstrutDiarioPendentes = False
    BG_LogFile_Erros "DIARIO: DIA_PreencheEstrutDiarioPendentes: " & Err.Description, "DIARIO", "DIA_PreencheEstrutDiarioPendentes", True
    Exit Function
    Resume Next
End Function

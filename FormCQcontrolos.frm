VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormCQcontrolos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCQcontrolos"
   ClientHeight    =   10275
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   7320
   Icon            =   "FormCQcontrolos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10275
   ScaleWidth      =   7320
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPrinterEtiq 
      Height          =   375
      Left            =   720
      TabIndex        =   23
      Top             =   7920
      Width           =   735
   End
   Begin VB.CommandButton BtImprEtiq 
      Height          =   495
      Left            =   6480
      Picture         =   "FormCQcontrolos.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   6360
      Width           =   615
   End
   Begin VB.TextBox EcNumEtiq 
      Height          =   285
      Left            =   5880
      TabIndex        =   21
      Top             =   6480
      Width           =   495
   End
   Begin VB.PictureBox PictureListColor 
      Height          =   495
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   555
      TabIndex        =   19
      Top             =   8400
      Width           =   615
   End
   Begin VB.TextBox EcUserCri 
      Height          =   285
      Left            =   2400
      TabIndex        =   18
      Top             =   7080
      Width           =   1335
   End
   Begin VB.TextBox EcUserAct 
      Height          =   285
      Left            =   4200
      TabIndex        =   17
      Top             =   7080
      Width           =   1335
   End
   Begin VB.Frame Frame2 
      Height          =   945
      Left            =   360
      TabIndex        =   8
      Top             =   5040
      Width           =   6765
      Begin VB.TextBox EcHrAct 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   5520
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox EcDtAct 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   3840
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox EcUserActNome 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   600
         Width           =   2055
      End
      Begin VB.TextBox EcHrCri 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   5520
         Locked          =   -1  'True
         TabIndex        =   13
         Top             =   195
         Width           =   735
      End
      Begin VB.TextBox EcDtCri 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   3840
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   195
         Width           =   1215
      End
      Begin VB.TextBox EcUserCriNome 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   195
         Width           =   2055
      End
      Begin VB.Label Label6 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   600
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcSeqControlo 
      Height          =   285
      Left            =   720
      TabIndex        =   7
      Top             =   7080
      Width           =   1335
   End
   Begin VB.CommandButton BtLotes 
      Height          =   735
      Left            =   1440
      Picture         =   "FormCQcontrolos.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Lotes Associados"
      Top             =   6120
      Width           =   855
   End
   Begin VB.CommandButton BtRegras 
      Height          =   735
      Left            =   480
      Picture         =   "FormCQcontrolos.frx":19A0
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Regras Westgard"
      Top             =   6120
      Width           =   855
   End
   Begin VB.TextBox EcDescrControlo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3360
      TabIndex        =   4
      Top             =   360
      Width           =   3735
   End
   Begin VB.TextBox EcCodControlo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      TabIndex        =   2
      Top             =   360
      Width           =   1215
   End
   Begin MSComctlLib.ListView LvControlos 
      Height          =   3975
      Left            =   360
      TabIndex        =   0
      Top             =   960
      Width           =   6735
      _ExtentX        =   11880
      _ExtentY        =   7011
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   0
   End
   Begin VB.Label Label2 
      Caption         =   "N� Etiquetas"
      Height          =   255
      Left            =   4800
      TabIndex        =   20
      Top             =   6480
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Descri��o"
      Height          =   255
      Index           =   1
      Left            =   2400
      TabIndex        =   3
      Top             =   360
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo"
      Height          =   255
      Index           =   0
      Left            =   360
      TabIndex        =   1
      Top             =   360
      Width           =   615
   End
End
Attribute VB_Name = "FormCQcontrolos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset
' Constant to define listview text color.
Const cLightGray = &H808080


Const cLightYellow = &H80000018
Const cWhite = &HFFFFFF
Const cLightBlue = &HFCE7D8

Private Type controlo
    seq_controlo As Long
    cod_controlo As String
    descr_controlo As String
    user_cri As String
    dt_cri As String
    hr_cri As String
    user_act As String
    dt_act As String
    hr_act As String
End Type
Dim estrutControlo() As controlo
Dim totalControlo As Integer

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub
Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormCQcontrolos = Nothing
End Sub

Sub Inicializacoes()

    Me.caption = "Controlos"
    Me.left = 5
    Me.top = 5
    Me.Width = 7590
    Me.Height = 7395 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "SL_CQ_CONTROLO"
    Set CampoDeFocus = EcCodControlo
    
    NumCampos = 9
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_controlo"
    CamposBD(1) = "cod_controlo"
    CamposBD(2) = "descr_controlo"
    CamposBD(3) = "user_cri"
    CamposBD(4) = "dt_cri"
    CamposBD(5) = "hr_cri"
    CamposBD(6) = "user_act"
    CamposBD(7) = "dt_act"
    CamposBD(8) = "hr_act"
    
    
    ' Campos do Ecr�
    Set CamposEc(0) = ecSeqcontrolo
    Set CamposEc(1) = EcCodControlo
    Set CamposEc(2) = EcDescrControlo
    Set CamposEc(3) = EcUserCri
    Set CamposEc(4) = EcDtCri
    Set CamposEc(5) = EcHrCri
    Set CamposEc(6) = EcUserAct
    Set CamposEc(7) = EcDtAct
    Set CamposEc(8) = EcHrAct
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo do Controlo"
    TextoCamposObrigatorios(2) = "Descr��o do Controlo"
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_controlo"
    Set ChaveEc = ecSeqcontrolo
    EcPrinterEtiq.Text = BL_SelImpressora("Etiqueta.rpt")
        
End Sub
Sub DefTipoCampos()
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDtCri, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDtAct, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_cri", EcHrCri, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_act", EcHrAct, mediTipoHora
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    InicializaTreeView
End Sub
Sub PreencheValoresDefeito()
End Sub


Private Sub BtImprEtiq_Click()
    If BL_HandleNull(EcCodControlo, "") <> "" Then
        If EcNumEtiq = "" Then
            EcNumEtiq = "1"
        End If
        BG_Mensagem mediMsgStatus, "A Imprimir...", mediMsgBeep + mediMsgPermanece
        DoEvents
        
        If Not LerEtiqInI Then
            BG_Mensagem mediMsgBox, "Ficheiro de impressao n�o foi encontrado.", vbExclamation, App.ProductName
        Else
            If Not BL_EtiqOpenPrinter(EcPrinterEtiq) Then
                BG_Mensagem mediMsgBox, "Impressora n�o est� disponivel.", vbExclamation, App.ProductName
                Exit Sub
            Else
                EtiqPrint EcCodControlo, EcNumEtiq
                
                BL_EtiqClosePrinter
            End If
        End If
        
        BG_Mensagem mediMsgStatus, "", mediMsgBeep + mediMsgPermanece
    End If
    

End Sub

Private Sub BtLotes_Click()
    If EcCodControlo <> "" And ecSeqcontrolo <> "" Then
        FormCQlotes.Show
        FormCQlotes.EcCodControlo = EcCodControlo
        FormCQlotes.EcCodControlo_Validate False
        'FormCQlotes.FuncaoProcurar
        FormCQlotes.EcControlo = "1"
        Set gFormActivo = FormCQlotes
    End If
End Sub

Private Sub BtRegras_Click()
    If EcCodControlo <> "" Then
        FormCQRegras.Show
        Set gFormActivo = FormCQRegras
        FormCQRegras.EcCodControlo = EcCodControlo
        FormCQRegras.FuncaoProcurar
    End If
End Sub

Private Sub EcCodControlo_Validate(Cancel As Boolean)
    EcCodControlo = UCase(EcCodControlo)
End Sub

Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub
Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub
Sub LimpaCampos()
    Me.SetFocus
    EcCodControlo.Locked = False

    BG_LimpaCampo_Todos CamposEc
    EcUserActNome = ""
    EcUserCriNome = ""
    LvControlos.ListItems.Clear
    totalControlo = 0
    ReDim estrutControlo(0)
End Sub
Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUserCri = gCodUtilizador
        EcDtCri = Bg_DaData_ADO
        EcHrCri = Bg_DaHora_ADO
        
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcUserAct = gCodUtilizador
        EcDtAct = Bg_DaData_ADO
        EcHrAct = Bg_DaHora_ADO
        
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & ecSeqcontrolo
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    rs.Requery
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
End Sub


Sub BD_Insert()
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    ecSeqcontrolo = BG_DaMAX(NomeTabela, "seq_controlo") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoRemover()
    
End Sub

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY Descr_controlo ASC "
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        PreencheLV
        EcCodControlo.Locked = True
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub
Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        BL_FimProcessamento Me
        PreencheCampos
    End If
End Sub


Private Sub InicializaTreeView()
    With LvControlos
        .ColumnHeaders.Add(, , "C�digo", 2000, lvwColumnLeft).Key = "CODIGO"
        .ColumnHeaders.Add(, , "Descri��o", 4700, lvwColumnCenter).Key = "DESCRICAO"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .FullRowSelect = True
        .ForeColor = cLightGray
        .MultiSelect = False
    End With
    ColocaCores LvControlos, PictureListColor, cLightBlue, cWhite
    Exit Sub

End Sub

Public Sub ColocaCores(lv As ListView, pb As PictureBox, cor1 As Long, Optional cor2 As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo TrataErro
    
    lColor1 = cor1
    lColor2 = cor2
    lv.Picture = LoadPicture("")
    lv.Refresh
    pb.Cls
    pb.AutoRedraw = True
    pb.BorderStyle = vbBSNone
    pb.ScaleMode = vbTwips
    pb.Visible = False
    lv.PictureAlignment = lvwTile
    pb.Font = lv.Font
    pb.top = lv.top
    pb.Font = lv.Font
    With pb.Font
        .Size = lv.Font.Size + 2
        .Bold = lv.Font.Bold
        .Charset = lv.Font.Charset
        .Italic = lv.Font.Italic
        .Name = lv.Font.Name
        .Strikethrough = lv.Font.Strikethrough
        .Underline = lv.Font.Underline
        .Weight = lv.Font.Weight
    End With
    pb.Refresh
    lLineHeight = pb.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = lv.Width
    pb.Height = lBarHeight * 2
    pb.Width = lBarWidth
    pb.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    pb.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    pb.AutoSize = True
    lv.Picture = pb.Image
    lv.Refresh
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "Erro: " & Err.Description, Me.Name, "ColocaCores", False
    Exit Sub
End Sub



Private Sub PreencheLV()
    Dim i As Integer
    LvControlos.ListItems.Clear
    totalControlo = 0
    ReDim estrutControlo(0)
    While Not rs.EOF
        totalControlo = totalControlo + 1
        ReDim Preserve estrutControlo(totalControlo)
        
        estrutControlo(totalControlo).seq_controlo = BL_HandleNull(rs!seq_controlo, -1)
        estrutControlo(totalControlo).cod_controlo = BL_HandleNull(rs!cod_controlo, "")
        estrutControlo(totalControlo).descr_controlo = BL_HandleNull(rs!descr_controlo, "")
        estrutControlo(totalControlo).user_cri = BL_HandleNull(rs!user_cri, "")
        estrutControlo(totalControlo).user_act = BL_HandleNull(rs!user_act, "")
        estrutControlo(totalControlo).dt_cri = BL_HandleNull(rs!dt_cri, "")
        estrutControlo(totalControlo).dt_act = BL_HandleNull(rs!dt_act, "")
        estrutControlo(totalControlo).hr_cri = BL_HandleNull(rs!hr_cri, "")
        estrutControlo(totalControlo).hr_act = BL_HandleNull(rs!hr_act, "")
        
        With LvControlos.ListItems.Add(totalControlo, "KEY_" & rs!seq_controlo, rs!cod_controlo)
            .ListSubItems.Add , , rs!descr_controlo
        End With
        rs.MoveNext
    Wend
End Sub



Private Sub LvControlos_Click()
    If LvControlos.ListItems.Count = 0 Then Exit Sub
        ecSeqcontrolo = estrutControlo(LvControlos.SelectedItem.Index).seq_controlo
        EcCodControlo = estrutControlo(LvControlos.SelectedItem.Index).cod_controlo
        EcDescrControlo = estrutControlo(LvControlos.SelectedItem.Index).descr_controlo
        EcUserCri = estrutControlo(LvControlos.SelectedItem.Index).user_cri
        EcUserAct = estrutControlo(LvControlos.SelectedItem.Index).user_act
        EcDtCri = estrutControlo(LvControlos.SelectedItem.Index).dt_cri
        EcDtAct = estrutControlo(LvControlos.SelectedItem.Index).dt_act
        EcHrCri = estrutControlo(LvControlos.SelectedItem.Index).hr_cri
        EcHrAct = estrutControlo(LvControlos.SelectedItem.Index).hr_act
        EcUserCriNome = BL_SelNomeUtil(EcUserCri.Text)
        EcUserActNome = BL_SelNomeUtil(EcUserAct.Text)
    
End Sub

Private Sub LvControlos_DblClick()
    BtLotes_Click
End Sub

Private Function EtiqPrint(ByVal CodControlo As String, numEtiq As Integer) As Boolean

    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{COD_CONTROLO}", CodControlo)
    sWrittenData = Replace(sWrittenData, "{NUM_ETIQ}", numEtiq)
    
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True
End Function

Private Function LerEtiqInI() As Boolean
    Dim aux As String
    On Error GoTo Erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Etiq_CQ.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Etiq_cq.ini"
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqInI = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While Mid(aux, 1, 1) <> "P"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqInI = True
    Exit Function
Erro:
    If Err.Number = 55 Then
        Close 1
        Open s For Input As 1
        Resume Next
    End If
Exit Function
Resume Next
End Function


VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormCodListasCorreio 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionFormCodListasCorreio"
   ClientHeight    =   7875
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7050
   Icon            =   "FCodListasCorreio.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7875
   ScaleWidth      =   7050
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Height          =   495
      Left            =   1080
      ScaleHeight     =   435
      ScaleWidth      =   555
      TabIndex        =   24
      Top             =   6480
      Width           =   615
   End
   Begin VB.Frame Frame2 
      Height          =   855
      Left            =   120
      TabIndex        =   17
      Top             =   4440
      Width           =   5175
      Begin VB.Label LaCriacao 
         AutoSize        =   -1  'True
         Caption         =   "Cria��o"
         Height          =   195
         Left            =   120
         TabIndex        =   23
         Top             =   240
         Width           =   540
      End
      Begin VB.Label LaAlteracao 
         AutoSize        =   -1  'True
         Caption         =   "Altera��o"
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   480
         Width           =   675
      End
      Begin VB.Label LaUtilCri 
         Caption         =   "LaUtilCri"
         Height          =   195
         Left            =   2160
         TabIndex        =   21
         Top             =   240
         Width           =   2295
      End
      Begin VB.Label LaUtilAct 
         Caption         =   "LaUtilAct"
         Height          =   195
         Left            =   2160
         TabIndex        =   20
         Top             =   480
         Width           =   2295
      End
      Begin VB.Label LaDataAct 
         Caption         =   "LaDataAct"
         Height          =   195
         Left            =   1080
         TabIndex        =   19
         Top             =   480
         Width           =   810
      End
      Begin VB.Label LaDataCri 
         Caption         =   "LaDataCri"
         Height          =   195
         Left            =   1080
         TabIndex        =   18
         Top             =   240
         Width           =   810
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4455
      Left            =   120
      TabIndex        =   6
      Top             =   0
      Width           =   5175
      Begin VB.CommandButton BtRetirarTodos 
         Height          =   1095
         Left            =   4560
         Picture         =   "FCodListasCorreio.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "Remover Destinat�rios"
         Top             =   3240
         Width           =   495
      End
      Begin VB.CommandButton BtRetirar 
         Height          =   1095
         Left            =   4560
         Picture         =   "FCodListasCorreio.frx":0776
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   "Remover Destinat�rio Seleccionado"
         Top             =   2160
         Width           =   495
      End
      Begin VB.CommandButton BtAdicionar 
         Height          =   1095
         Left            =   4560
         Picture         =   "FCodListasCorreio.frx":0EE0
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "Adicionar Destinat�rio"
         Top             =   1080
         Width           =   495
      End
      Begin VB.TextBox EcDescricao 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   120
         TabIndex        =   11
         Top             =   360
         Width           =   4455
      End
      Begin VB.ComboBox EcListas 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   1080
         Width           =   4455
      End
      Begin VB.OptionButton EcOptUtilizadores 
         Caption         =   "Utilizadores"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   720
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.OptionButton EcOptListas 
         Caption         =   "Listas de utilizadores"
         Height          =   255
         Left            =   3240
         TabIndex        =   8
         Top             =   720
         Width           =   1815
      End
      Begin VB.TextBox EcCodigo 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4560
         TabIndex        =   7
         Top             =   360
         Visible         =   0   'False
         Width           =   495
      End
      Begin MSComctlLib.ListView EcMembros 
         Height          =   2895
         Left            =   120
         TabIndex        =   12
         Top             =   1440
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   5106
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ColHdrIcons     =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Label Label1 
         Caption         =   "Descri��o"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   120
         Width           =   4455
      End
   End
   Begin VB.TextBox EcHoraCri 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2040
      TabIndex        =   5
      Top             =   6000
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcHoraAct 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4920
      TabIndex        =   4
      Top             =   6000
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcDataCri 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1155
      TabIndex        =   3
      Top             =   6000
      Visible         =   0   'False
      Width           =   885
   End
   Begin VB.TextBox EcUtilCri 
      Enabled         =   0   'False
      Height          =   285
      Left            =   240
      TabIndex        =   2
      Top             =   6000
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox EcDataAct 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      TabIndex        =   1
      Top             =   6000
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.TextBox EcUtilAct 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2880
      TabIndex        =   0
      Top             =   6000
      Visible         =   0   'False
      Width           =   915
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   360
      Top             =   6480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCodListasCorreio.frx":164A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FCodListasCorreio.frx":19E4
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormCodListasCorreio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim rsTabela As ADODB.recordset
Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim bFonte As Boolean
Dim bTamanho As Boolean

Dim sFonte As String
Dim iTamanho As Integer

Public bPrimeiraVez As Boolean

Public RsConsulta As ADODB.recordset
Private Sub ConfiguraListView()
     
    EcMembros.View = lvwReport
    
    EcMembros.ColumnHeaders.Add , , "Tipo", 650
    EcMembros.ColumnHeaders.Add , , "Descri��o", 3800
    SetListViewColor EcMembros, Picture1, &H80000018, &HFFFFFF
    
End Sub

Private Sub PreencheLista()
    Dim RsMembros As ADODB.recordset
    Dim sCodigo As String
    Dim sDescricao As String
    Dim iIcon As Integer
    Dim sSql As String
    
    On Error GoTo Trata_Erro
    
    sSql = "SELECT * FROM " & gTabelaCodListasMembros & " WHERE cod_lista=" & rsTabela!Codigo
    Set RsMembros = New ADODB.recordset
    RsMembros.CursorLocation = adUseServer
    RsMembros.Open sSql, gConexao, adOpenStatic
    If RsMembros.RecordCount > 0 Then
        Do While Not RsMembros.EOF
            If RsMembros!flg_lista = 1 Then
                sCodigo = "L-"
                iIcon = 1
                sDescricao = BL_PesquisaCodDes(gTabelaCodListas, "codigo", "nome", RsMembros!cod_membro, "DES")
            Else
                sCodigo = "U-"
                iIcon = 2
                sDescricao = BL_PesquisaCodDes(gTabelaUtilizadores, gCampoCodUtilizador, gCampoNomeUtilizador, RsMembros!cod_membro, "DES")
            End If
            EcMembros.ListItems.Add , sCodigo & RsMembros!cod_membro, , iIcon, iIcon
            EcMembros.ListItems(EcMembros.ListItems.Count).SubItems(1) = sDescricao
        

            RsMembros.MoveNext
        Loop
    End If
    RsMembros.Close
    Set RsMembros = Nothing
    Exit Sub
Trata_Erro:
    BG_LogFile_Erros "Erro: " & Err.Description & " ao preencher os membros da lista " & rsTabela!descricao
    Resume Next
End Sub

Private Sub BtAdicionar_Click()

    If EcListas.ListIndex <> mediComboValorNull Then
        On Error GoTo TrataErro
    
        Dim iTipo As Integer
        
        If EcOptUtilizadores.value = True Then
            iTipo = 2
            EcMembros.ListItems.Add , "U-" & EcListas.ItemData(EcListas.ListIndex), , iTipo, iTipo
        Else
            If EcCodigo <> "" Then
                If EcCodigo = EcListas.ItemData(EcListas.ListIndex) Then
                    BG_Mensagem mediMsgBox, "N�o poder� este item a lista.", vbCritical, "..."
                    Exit Sub
                End If
            End If
    
            iTipo = 1
            EcMembros.ListItems.Add , "L-" & EcListas.ItemData(EcListas.ListIndex), , iTipo, iTipo
        End If
        
        EcMembros.ListItems(EcMembros.ListItems.Count).SubItems(1) = EcListas.List(EcListas.ListIndex)
    End If
    Exit Sub
TrataErro:
    If Err.Number = 35602 Then
        'item j� existe na lista
        BG_Mensagem mediMsgBox, "O item j� se encontra seleccionado", vbInformation, "..."
    Else
        BG_Mensagem mediMsgBox, "Erro: " & Err.Number & " - " & Err.Description, vbCritical, "..."
    End If
End Sub

Private Sub BtRetirar_Click()
    If EcMembros.ListItems.Count > 0 Then
        EcMembros.ListItems.Remove EcMembros.SelectedItem.Index
    End If
End Sub

Private Sub BtRetirarTodos_Click()
    If EcMembros.ListItems.Count > 0 Then
        EcMembros.ListItems.Clear
    End If
End Sub

Private Sub EcOptListas_Click()
    If EcOptListas.value = True Then
        Dim sSql As String
            
        sSql = "SELECT * FROM " & gTabelaCodListas
        BG_PreencheComboBD_ADO sSql, "codigo", "descricao", EcListas, mediAscComboDesignacao
    End If
End Sub

Private Sub EcOptUtilizadores_Click()
    If EcOptUtilizadores.value = True Then
        Dim sSql As String
        
        sSql = "SELECT " & gCampoCodUtilizador & "," & gCampoNomeUtilizador & " FROM " & gTabelaUtilizadores & " b WHERE flg_removido=0 or flg_removido is null"
        sSql = sSql & " AND cod_local = " & gCodLocal
        
        BG_PreencheComboBD_ADO sSql, gCampoCodUtilizador, gCampoNomeUtilizador, EcListas, mediAscComboDesignacao
        
    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = "Listas de Utilizadores"
    Me.left = 350
    Me.top = 300
    Me.Width = 5505
    Me.Height = 5895
    
    NomeTabela = gTabelaCodListas
    Set CampoDeFocus = EcDescricao
    
    NumCampos = 8
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "codigo"
    CamposBD(1) = "descricao"
    CamposBD(2) = "user_cri"
    CamposBD(3) = "dt_cri"
    CamposBD(4) = "hr_cri"
    CamposBD(5) = "user_act"
    CamposBD(6) = "dt_act"
    CamposBD(7) = "hr_act"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcDescricao
    Set CamposEc(2) = EcUtilCri
    Set CamposEc(3) = EcDataCri
    Set CamposEc(4) = EcHoraCri
    Set CamposEc(5) = EcUtilAct
    Set CamposEc(6) = EcDataAct
    Set CamposEc(7) = EcHoraAct
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Descricao"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    'ChaveBD = "codigo"
    'Set ChaveEc = EcCodigo
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    
    estado = 1
    BG_StackJanelas_Push Me
    
    
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    
    If Not rsTabela Is Nothing Then
        If Not rsTabela.state = adStateClosed Then
            rsTabela.Close
        End If
        Set rsTabela = Nothing
    End If
End Sub


Sub LimpaCampos()
    Me.SetFocus
    
    BG_LimpaCampo_Todos CamposEc
    
    LaUtilCri.caption = ""
    LaUtilAct.caption = ""
    LaDataCri.caption = ""
    LaDataAct.caption = ""
    
    EcMembros.ListItems.Clear
End Sub

Sub DefTipoCampos()
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCri, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_cri", EcHoraCri, mediTipoHora

    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAct, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_act", EcHoraAct, mediTipoHora

'    BG_DefTipoCampoEc_Todos NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

Private Sub PreencheValoresDefeito()
    EcDescricao = ""
    EcListas.Clear
    EcMembros.ListItems.Clear
    EcListas.Clear
       
    
    LaDataCri = ""
    LaUtilCri = ""
    LaDataAct = ""
    LaUtilAct = ""
    
    ConfiguraListView
    EcOptUtilizadores_Click
End Sub

Sub PreencheCampos()

    Me.SetFocus
    
    If rsTabela.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rsTabela, CamposBD, CamposEc
        
        BL_PreencheControloAcessos EcUtilCri, EcUtilAct, LaUtilCri, LaUtilAct
        LaDataCri = EcDataCri
        LaDataAct = EcDataAct
        
        PreencheLista
    End If
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        'CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        LimpaCampos
        CampoDeFocus.SetFocus
        rsTabela.Close
    Else
        Unload Me
    End If
    
End Sub

Function ValidaCamposEc() As Integer

    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
    On Error GoTo TrataErro

    Dim bSelTotal As Boolean
        
    BL_InicioProcessamento Me, "A pesquisar registos..."
        
    EcHoraCri = ""
    EcHoraAct = ""
    
    Set rsTabela = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, True)
        
    rsTabela.CursorLocation = adUseServer
    rsTabela.Open CriterioTabela, gConexao, adOpenStatic
        
    If rsTabela.RecordCount = 0 Then
        FuncaoLimpar
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo! ", vbExclamation, "Procurar"
    Else
        estado = 2
        LimpaCampos
        PreencheCampos
        BL_ToolbarEstadoN estado
    End If
    
    bPrimeiraVez = False
    BL_FimProcessamento Me
    Exit Sub

TrataErro:
    BG_Mensagem mediMsgBox, "Erro : " & Err.Number & " - " & Err.Description, vbInformation, "..."

End Sub

Sub FuncaoAnterior()

    rsTabela.MovePrevious
    
    If rsTabela.BOF Then
        rsTabela.MoveNext
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
    End If
    
End Sub

Sub FuncaoSeguinte()

    rsTabela.MoveNext
    
    If rsTabela.EOF Then
        rsTabela.MovePrevious
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
    End If
    
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        EcUtilCri = gCodUtilizador
        EcDataCri = Bg_DaData_ADO
        EcHoraCri = Bg_DaHora_ADO
        
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
    End If
    
End Sub

Sub BD_Insert()
    Dim RsTeste As ADODB.recordset
    Dim sCodigo() As String
    Dim sSql As String
    Dim i As Integer
    Dim iRes As Integer

    On Error GoTo Trata_Erro
    
    
    If EcMembros.ListItems.Count = 0 Then
        BG_Mensagem mediMsgBox, "A lista tem que incluir pelo menos um item.", vbInformation, "..."
        Exit Sub
    End If
            
    sSql = "SELECT * FROM " & NomeTabela & " WHERE descricao=" & BG_VfValor(EcDescricao, "'")
    Set RsTeste = New ADODB.recordset
    RsTeste.CursorLocation = adUseServer
    RsTeste.Open sSql, gConexao, adOpenStatic
    If RsTeste.RecordCount > 0 Then
        BG_Mensagem mediMsgBox, "J� existe um registo com essa descri��o.", vbInformation, "..."
        RsTeste.Close
        Set RsTeste = Nothing
        Exit Sub
    End If
    RsTeste.Close
    Set RsTeste = Nothing
    
    EcCodigo = BG_DaMAX(NomeTabela, "codigo") + 1
    
    sSql = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    iRes = BG_ExecutaQuery_ADO(sSql)
    
    If iRes = -1 Then
        BG_Trata_BDErro
    Else
        For i = 1 To EcMembros.ListItems.Count
            sCodigo = Split(EcMembros.ListItems(i).Key, "-")
            sSql = "INSERT INTO " & gTabelaCodListasMembros & " (cod_lista,cod_membro,flg_lista) VALUES("
            sSql = sSql & EcCodigo.Text & ","
            sSql = sSql & sCodigo(1) & ","
            If sCodigo(0) = "U" Then
                sSql = sSql & "0)"
            Else
                sSql = sSql & "1)"
            End If
            
            BG_ExecutaQuery_ADO sSql
        Next
        
        
        i = EcCodigo
        FuncaoLimpar
        EcCodigo = i
        FuncaoProcurar
    End If
    
    Exit Sub

Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro: " & Err.Number & " - " & Err.Description, vbCritical, "..."

End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    
    If gCodUtilizador <> rsTabela!user_cri And gCodGrupo <> gGrupoAdministradores Then
        BG_Mensagem mediMsgBox, "N�o tem permiss�es para modificar este registo.", vbCritical, "..."
        Exit Sub
    End If
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        EcUtilAct = gCodUtilizador
        EcDataAct = Bg_DaData_ADO
        EcHoraAct = Bg_DaHora_ADO
        
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Update
        End If
    End If
    
End Sub

Sub BD_Update()
    Dim sCondicao As String
    Dim sSql As String
    Dim MarcaLocal As Variant
    
    
    gSQLError = 0
    gSQLISAM = 0
    
    If EcMembros.ListItems.Count = 0 Then
        BG_Mensagem mediMsgBox, "A lista tem que incluir pelo menos um item.", vbInformation, "..."
        Exit Sub
    End If
    
    sCondicao = "codigo=" & EcCodigo
    sSql = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, sCondicao)
    BG_ExecutaQuery_ADO sSql
    
    BG_Trata_BDErro
    
    Dim i As Integer
    Dim sCodigo() As String
    
    sSql = "DELETE FROM " & gTabelaCodListasMembros & " WHERE cod_lista=" & rsTabela!Codigo
    BG_ExecutaQuery_ADO sSql
    
    BG_Trata_BDErro
    
    For i = 1 To EcMembros.ListItems.Count
        sCodigo = Split(EcMembros.ListItems(i).Key, "-")
        sSql = "INSERT INTO " & gTabelaCodListasMembros & " (cod_lista,cod_membro,flg_lista) VALUES("
        sSql = sSql & EcCodigo.Text & ","
        sSql = sSql & sCodigo(1) & ","
        If sCodigo(0) = "U" Then
            sSql = sSql & "0)"
        Else
            sSql = sSql & "1)"
        End If
        
        BG_ExecutaQuery_ADO sSql
    Next

    MarcaLocal = rsTabela.Bookmark
    rsTabela.Requery
    
    If rsTabela.BOF And rsTabela.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    rsTabela.Bookmark = MarcaLocal
    
    LimpaCampos
    PreencheCampos
    
End Sub

Sub FuncaoRemover()
    
    If gCodUtilizador <> rsTabela!user_cri And gCodGrupo <> gGrupoAdministradores Then
        BG_Mensagem mediMsgBox, "N�o tem permiss�es para remover este registo.", vbCritical, "..."
        Exit Sub
    End If

    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer remover este registo ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BD_Delete
    End If
    
End Sub

Sub BD_Delete()
    Dim sSql As String
    Dim sCondicao As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    gSQLISAM = 0
    
    sCondicao = "codigo=" & EcCodigo
    sSql = "DELETE FROM " & NomeTabela & " WHERE " & sCondicao
    BG_ExecutaQuery_ADO sSql
    
    BG_Trata_BDErro
    
    sSql = "DELETE FROM " & gTabelaCodListasMembros & " WHERE cod_lista=" & rsTabela!Codigo
    BG_ExecutaQuery_ADO sSql
    
    BG_Trata_BDErro
    
    MarcaLocal = rsTabela.Bookmark
    rsTabela.Requery
        
    If rsTabela.BOF And rsTabela.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If rsTabela.EOF Then
        rsTabela.MovePrevious
    Else
        If MarcaLocal <> 1 Then
            rsTabela.Bookmark = MarcaLocal - 1
        End If
    End If
    
    LimpaCampos
    PreencheCampos
    
End Sub


' pferreira 2007.08.27
' Paint a list view with colored bars.
Public Sub SetListViewColor(�lvCtrlListView� As ListView, �pCtrlPictureBox� As PictureBox, �lColorOne� As Long, Optional �lColorTwo� As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo ErrorHandler
    lColor1 = �lColorOne�
    lColor2 = �lColorTwo�
    �lvCtrlListView�.Picture = LoadPicture("")
    �lvCtrlListView�.Refresh
    �pCtrlPictureBox�.Cls
    �pCtrlPictureBox�.AutoRedraw = True
    �pCtrlPictureBox�.BorderStyle = vbBSNone
    �pCtrlPictureBox�.ScaleMode = vbTwips
    �pCtrlPictureBox�.Visible = False
    �lvCtrlListView�.PictureAlignment = lvwTile
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    �pCtrlPictureBox�.top = �lvCtrlListView�.top
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    With �pCtrlPictureBox�.Font
        .Size = �lvCtrlListView�.Font.Size + 2
        .Bold = �lvCtrlListView�.Font.Bold
        .Charset = �lvCtrlListView�.Font.Charset
        .Italic = �lvCtrlListView�.Font.Italic
        .Name = �lvCtrlListView�.Font.Name
        .Strikethrough = �lvCtrlListView�.Font.Strikethrough
        .Underline = �lvCtrlListView�.Font.Underline
        .Weight = �lvCtrlListView�.Font.Weight
    End With
    �pCtrlPictureBox�.Refresh
    lLineHeight = �pCtrlPictureBox�.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = �lvCtrlListView�.Width
    �pCtrlPictureBox�.Height = lBarHeight * 2
    �pCtrlPictureBox�.Width = lBarWidth
    �pCtrlPictureBox�.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    �pCtrlPictureBox�.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    �pCtrlPictureBox�.AutoSize = True
    �lvCtrlListView�.Picture = �pCtrlPictureBox�.Image
    �lvCtrlListView�.Refresh
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'SetListViewColor' in form FormCodListasCorreio (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Attribute VB_Name = "Email"
Option Explicit

Const lEMAILEstadoInicial = "NP"
Const lEMAILEstadoEnviada = "P"

Dim lServidor As String
Dim lEmailFROM As String
Dim lPassword As String
Dim lUtilizador As String
Dim lAssunto As String
Dim lMensagem As String



Public Function EMAIL_CriaMensagem(n_req As String, codAgrega As String, emailDestino As String, _
                              destinoPDF As String, numAgrega As Long) As Long
                              
    Dim sSql As String
    On Error GoTo TrataErro
    
    If gSGBD = "ORACLE" Then
        sSql = " INSERT INTO sl_email_resultados (seq_mensagem, cod_agrega, num_agrega, n_req, destino_pdf, "
        sSql = sSql & " email_destino,email_origem, assunto, mensagem, estado, user_cri, dt_cri) VALUES( "
        sSql = sSql & " SEQ_EMAIL_MENSAGEM.NEXTVAL, "
        sSql = sSql & BL_TrataStringParaBD(codAgrega) & ", "
        sSql = sSql & numAgrega & ", "
        sSql = sSql & n_req & ", "
        sSql = sSql & BL_TrataStringParaBD(destinoPDF) & ", "
        sSql = sSql & BL_TrataStringParaBD(emailDestino) & ", "
        sSql = sSql & BL_TrataStringParaBD(lEmailFROM) & ", "
        sSql = sSql & BL_TrataStringParaBD(lAssunto) & ", "
        sSql = sSql & BL_TrataStringParaBD(lMensagem) & ", "
        sSql = sSql & BL_TrataStringParaBD(lEMAILEstadoInicial) & ", "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ")"
        
    ElseIf gSGBD = "SQL_SERVER" Then
        sSql = "INSERT INTO sl_email_resultados( cod_agrega, num_agrega, n_req, destino_pdf, "
        sSql = sSql & " email_destino,email_origem, assunto, mensagem, estado, user_cri, dt_cri) VALUES( "
        sSql = sSql & BL_TrataStringParaBD(codAgrega) & ", "
        sSql = sSql & numAgrega & ", "
        sSql = sSql & n_req & ", "
        sSql = sSql & BL_TrataStringParaBD(destinoPDF) & ", "
        sSql = sSql & BL_TrataStringParaBD(emailDestino) & ", "
        sSql = sSql & BL_TrataStringParaBD(lEmailFROM) & ", "
        sSql = sSql & BL_TrataStringParaBD(lAssunto) & ", "
        sSql = sSql & BL_TrataStringParaBD(lMensagem) & ", "
        sSql = sSql & BL_TrataStringParaBD(lEMAILEstadoInicial) & ", "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ")"
    End If
    BG_ExecutaQuery_ADO sSql

Exit Function
TrataErro:
    BL_LogFile_BD "EMAIL", "EMAIL_CriaMensagem", Err.number, Err.Description, ""
    EMAIL_CriaMensagem = -1
    Exit Function
    Resume Next
End Function



Public Sub EMAIL_CarregaConfig(codigo As String)
    Dim sSql As String
    Dim rsConfig As New ADODB.recordset
    
    sSql = "SELECT * from sl_email_config WHERE codigo = " & BL_TrataStringParaBD(codigo)
    rsConfig.CursorType = adOpenStatic
    rsConfig.CursorLocation = adUseClient
    rsConfig.Open sSql, gConexao
    If rsConfig.RecordCount > 0 Then
        lServidor = BL_HandleNull(rsConfig!servirod, "")
        lUtilizador = BL_HandleNull(rsConfig!UserName, "")
        lPassword = BL_HandleNull(rsConfig!password, "")
        lEmailFROM = BL_HandleNull(rsConfig!email_from, "")
        lAssunto = BL_HandleNull(rsConfig!assunto, "")
        lMensagem = BL_HandleNull(rsConfig!mensagem, "")
    End If
    rsConfig.Close
    Set rsConfig = Nothing
End Sub


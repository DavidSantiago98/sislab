VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormMonitorEresults 
   BackColor       =   &H00FFE0C7&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   8010
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   15105
   Icon            =   "FormMonitorEresults.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8010
   ScaleWidth      =   15105
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameLegenda 
      BackColor       =   &H00FFE0C7&
      Height          =   1695
      Left            =   10560
      TabIndex        =   32
      Top             =   0
      Width           =   4455
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Sem Valida��o"
         Height          =   255
         Index           =   7
         Left            =   2640
         TabIndex        =   46
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label5 
         BackColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   4
         Left            =   2280
         TabIndex        =   45
         Top             =   240
         Width           =   255
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "N�o Indexada"
         Height          =   255
         Index           =   4
         Left            =   480
         TabIndex        =   44
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label5 
         BackColor       =   &H0000FFFF&
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   43
         Top             =   960
         Width           =   255
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "N�o Enviada"
         Height          =   255
         Index           =   5
         Left            =   480
         TabIndex        =   42
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label5 
         BackColor       =   &H000080FF&
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   41
         Top             =   600
         Width           =   255
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Dispon�vel no eR."
         Height          =   255
         Index           =   3
         Left            =   2640
         TabIndex        =   40
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label Label5 
         BackColor       =   &H0080FF80&
         Height          =   255
         Index           =   0
         Left            =   2280
         TabIndex        =   39
         Top             =   600
         Width           =   255
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Erro ao enviar"
         Height          =   255
         Index           =   6
         Left            =   480
         TabIndex        =   38
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label5 
         BackColor       =   &H008080FF&
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   37
         Top             =   240
         Width           =   255
      End
      Begin VB.Label Label5 
         BackColor       =   &H00FEA381&
         Height          =   255
         Index           =   5
         Left            =   2280
         TabIndex        =   36
         Top             =   960
         Width           =   255
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Reenviar para eR"
         Height          =   255
         Index           =   10
         Left            =   2640
         TabIndex        =   35
         Top             =   960
         Width           =   1455
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Datas Incoerentes"
         Height          =   255
         Index           =   11
         Left            =   480
         TabIndex        =   34
         Top             =   1320
         Width           =   1575
      End
      Begin VB.Label Label5 
         BackColor       =   &H00808080&
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   33
         Top             =   1320
         Width           =   255
      End
   End
   Begin VB.CheckBox CkTodos 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFE0C7&
      Caption         =   "Selecionar Todas"
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   120
      TabIndex        =   31
      Top             =   1800
      Width           =   3855
   End
   Begin VB.CommandButton BtAtualizar 
      Height          =   600
      Left            =   14400
      Picture         =   "FormMonitorEresults.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   28
      ToolTipText     =   "Atualizar Monitoriza��o"
      Top             =   7440
      Width           =   615
   End
   Begin VB.Frame FrameDet 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFE0C7&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   6255
      Left            =   0
      TabIndex        =   22
      Top             =   8640
      Width           =   15015
      Begin VB.CommandButton BTDetVoltar 
         Height          =   600
         Left            =   14280
         Style           =   1  'Graphical
         TabIndex        =   25
         ToolTipText     =   "Resultados"
         Top             =   4680
         Width           =   615
      End
      Begin VB.ListBox EcListaDetER 
         Appearance      =   0  'Flat
         Height          =   4905
         Left            =   7080
         TabIndex        =   24
         Top             =   480
         Width           =   6615
      End
      Begin VB.ListBox EcListaDetEnvio 
         Appearance      =   0  'Flat
         Height          =   4905
         Left            =   360
         TabIndex        =   23
         Top             =   480
         Width           =   6615
      End
   End
   Begin VB.CommandButton BtReenviar 
      Height          =   600
      Left            =   13680
      Picture         =   "FormMonitorEresults.frx":0776
      Style           =   1  'Graphical
      TabIndex        =   27
      ToolTipText     =   "Reenviar Requisi��es Selecionadas"
      Top             =   7440
      Width           =   615
   End
   Begin VB.CommandButton BtdocER 
      Height          =   600
      Left            =   12960
      Picture         =   "FormMonitorEresults.frx":0EE0
      Style           =   1  'Graphical
      TabIndex        =   26
      ToolTipText     =   "Consultar Documento eR"
      Top             =   7440
      Width           =   615
   End
   Begin VB.CommandButton BtResultados 
      Height          =   600
      Left            =   12240
      Picture         =   "FormMonitorEresults.frx":164A
      Style           =   1  'Graphical
      TabIndex        =   15
      ToolTipText     =   "Resultados"
      Top             =   7440
      Width           =   615
   End
   Begin VB.Frame FrameDetalhe 
      BackColor       =   &H00FFE0C7&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   0
      TabIndex        =   8
      Top             =   7560
      Width           =   11655
      Begin VB.Label LbDataER 
         BackColor       =   &H00FFE0C7&
         Height          =   255
         Left            =   9120
         TabIndex        =   14
         Top             =   120
         Width           =   2055
      End
      Begin VB.Label LbDataEnvio 
         BackColor       =   &H00FFE0C7&
         Height          =   255
         Left            =   5640
         TabIndex        =   13
         Top             =   120
         Width           =   1935
      End
      Begin VB.Label LbDataVal 
         BackColor       =   &H00FFE0C7&
         Height          =   255
         Left            =   1440
         TabIndex        =   12
         Top             =   120
         Width           =   2415
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "�ltimo Indexa��o:"
         Height          =   255
         Index           =   2
         Left            =   7800
         TabIndex        =   11
         Top             =   120
         Width           =   1335
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "�ltimo envio para eR"
         Height          =   255
         Index           =   1
         Left            =   4080
         TabIndex        =   10
         Top             =   120
         Width           =   1695
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "�ltima Valida��o"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   120
         Width           =   1335
      End
   End
   Begin MSFlexGridLib.MSFlexGrid FgReq 
      Height          =   5295
      Left            =   0
      TabIndex        =   7
      Top             =   2040
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   9340
      _Version        =   393216
      BackColorBkg    =   16769223
      BorderStyle     =   0
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00FFE0C7&
      Height          =   1695
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   10335
      Begin VB.TextBox EcNumReq 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   960
         TabIndex        =   30
         Top             =   1080
         Width           =   1335
      End
      Begin VB.CommandButton BtPesquisaLocais 
         Height          =   315
         Left            =   9860
         Picture         =   "FormMonitorEresults.frx":2314
         Style           =   1  'Graphical
         TabIndex        =   20
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   960
         Width           =   375
      End
      Begin VB.ListBox EcListaLocais 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   5400
         TabIndex        =   19
         Top             =   960
         Width           =   4455
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   9860
         Picture         =   "FormMonitorEresults.frx":269E
         Style           =   1  'Graphical
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   240
         Width           =   375
      End
      Begin VB.ListBox EcListaProven 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   5400
         TabIndex        =   16
         Top             =   240
         Width           =   4455
      End
      Begin VB.ComboBox cbUrgencia 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   600
         Width           =   1335
      End
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   255
         Left            =   960
         TabIndex        =   2
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   145555457
         CurrentDate     =   39588
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   2640
         TabIndex        =   3
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   145555457
         CurrentDate     =   39588
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Requisi��o"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   29
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Locais"
         Height          =   255
         Index           =   9
         Left            =   4320
         TabIndex        =   21
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Proveni�ncias"
         Height          =   255
         Index           =   8
         Left            =   4320
         TabIndex        =   18
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label3 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Da Data "
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FFE0C7&
         Caption         =   "a"
         Height          =   255
         Left            =   2370
         TabIndex        =   5
         Top             =   240
         Width           =   225
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Urg�ncia"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   855
      End
   End
End
Attribute VB_Name = "FormMonitorEresults"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim NumCampos As Integer

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object


Const Verde = &H80FF80
Const vermelho = &H8080FF
Const Amarelo = &HFFFF&
Const laranja = &H80FF&
Const Branco = vbWhite
Const azul = &HFEA381
Const Cinza = &H808080

'Linha da grid selecionada.
Dim LINHA_SEL As Integer
Dim COLUNA_SEL As Integer

Const TAM_LINHA = 19

Private Type req
    t_utente As String
    Utente As String
    num_requis As String
    dt_chega As String
    estado_req As String
    cod_local As String
    cod_proven As String
    descr_proven As String
    dt_ult_valid As String
    hr_ult_valid As String
    flg_ana_disponiveis As Boolean
    
    estado_envio As String
    dt_ult_envio As String
    hr_ult_envio As String
    
    dt_ult_eR As String
    hr_ult_eR As String
    cor As Long
    flg_seleccionada As Boolean
End Type

Private Type linha
    linha As Integer
    requisicoes() As req
    colunasOcupadas As Long
End Type
Dim estrutTabela() As linha
Dim totalTabela As Long

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    
    Call Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    Call PreencheValoresDefeito
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
    gF_FACTUSENVIO = 1
    
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    gF_FACTUSENVIO = 0
    

        Set gFormActivo = MDIFormInicio

    
    BL_ToolbarEstadoN 0
    
    Set FormMonitorEresults = Nothing
    
End Sub

Sub Inicializacoes()

    Me.caption = " Monotoriza��o de Disponibiliza��o de Resultados"
    Me.left = 100
    Me.top = 20
    Me.Width = 15195
    Me.Height = 8385 ' Normal
    'Me.Height = 8700 ' Campos Extras
    
    Set CampoDeFocus = EcDtInicio
    
    
    LINHA_SEL = 0
    
End Sub

 Sub DefTipoCampos()
    Dim i As Integer
    With FgReq
        .Cols = TAM_LINHA
        .rows = 1
        .FixedCols = 0
        .FixedRows = 0
        
        For i = 0 To TAM_LINHA - 1
            .Col = i
            .ColWidth(i) = 770
            .MergeCells = flexMergeFree
        Next
        
    End With
    FrameDet.Visible = False
 End Sub
 


Private Sub BtAtualizar_Click()
    FuncaoProcurar
End Sub

Private Sub BtdocER_Click()
    Dim sURL As String
    If FgReq.row <= totalTabela - 1 Then
        If FgReq.Col <= estrutTabela(FgReq.row + 1).colunasOcupadas - 1 Then
            ER_AbreLinkEResults_V2 estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).num_requis, estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).cod_local, _
                estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).Utente, estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).t_utente
        End If
    End If
End Sub

Private Sub BtPesquisaProveniencia_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = " (flg_nao_envia_er is null OR flg_nao_envia_er = 0)"
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaProven.ListCount = 0 Then
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(0) = resultados(i)
            Else
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(EcListaProven.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub BtPesquisaLocais_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "Empresa_id"
    CamposEcran(1) = "Empresa_id"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome_empr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "gr_empr_inst"
    CWhere = ""
    CampoPesquisa = "nome_empr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome_empr ", _
                                                                           " Locais")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaLocais.ListCount = 0 Then
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", resultados(i))
                EcListaLocais.ItemData(0) = resultados(i)
            Else
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", resultados(i))
                EcListaLocais.ItemData(EcListaLocais.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub




Private Sub BTDetVoltar_Click()
    FrameDet.Visible = False
    FgReq.SetFocus
End Sub

Private Sub BtReenviar_Click()
    Dim linha As Integer
    Dim coluna As Integer
    Dim i As Integer
    gMsgMsg = "Tem a certeza que quer Enviar para o eResults as requisi��es selecionadas?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        i = 0
        For linha = 1 To totalTabela
            For coluna = 1 To estrutTabela(linha).colunasOcupadas
                If estrutTabela(linha).requisicoes(coluna).flg_seleccionada = True Then
                    BL_InsereEResults estrutTabela(linha).requisicoes(coluna).num_requis
                    i = i + 1
                End If
            Next coluna
        Next linha
        BG_Mensagem mediMsgBox, "Foram marcadas para reenviar " & i & " requisi��es.", vbExclamation, "Reenvio"
        FuncaoProcurar
    End If
End Sub

Private Sub BtResultados_Click()
    If FgReq.row <= totalTabela - 1 Then
        If FgReq.Col <= estrutTabela(FgReq.row + 1).colunasOcupadas - 1 Then
            FormResultadosNovo.Show
            FormResultadosNovo.EcPesqNumReq = estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).num_requis
            FormResultadosNovo.EcCodLocal = gCodLocal
            FormResultadosNovo.FuncaoProcurar
        End If
    End If
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbUrgencia.ListIndex = mediComboValorNull
    End If
End Sub

Private Sub CkTodos_Click()
    Dim i As Integer
    Dim j As Integer
    FgReq.Visible = False
    For i = 1 To totalTabela
        For j = 1 To estrutTabela(i).colunasOcupadas
            FgReq.row = i - 1
            FgReq.Col = j - 1
            If CkTodos.value = vbChecked Then
                estrutTabela(i).requisicoes(j).flg_seleccionada = True
                FgReq.CellBackColor = azul
            Else
                estrutTabela(i).requisicoes(j).flg_seleccionada = False
                FgReq.CellBackColor = estrutTabela(i).requisicoes(j).cor
            End If
            
        Next j
    Next i
    FgReq.Visible = True
End Sub

Private Sub EclistaLocais_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaLocais, KeyCode, Shift
End Sub

Private Sub EclistaProven_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaProven, KeyCode, Shift
End Sub
Private Sub FgReq_Click()
    If FgReq.row <= totalTabela - 1 Then
        If FgReq.Col <= estrutTabela(FgReq.row + 1).colunasOcupadas - 1 Then
            LbDataEnvio = estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).dt_ult_envio & " " & estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).hr_ult_envio
            LbDataER = estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).dt_ult_eR & " " & estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).hr_ult_eR
            LbDataVal = estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).dt_ult_valid & " " & estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).hr_ult_valid
        End If
    End If
End Sub

Private Sub FgReq_DblClick()
    If FgReq.row <= totalTabela - 1 Then
        If FgReq.Col <= estrutTabela(FgReq.row + 1).colunasOcupadas - 1 Then
            If estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).flg_seleccionada = False Then
                estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).flg_seleccionada = True
                FgReq.CellBackColor = azul
            Else
                estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).flg_seleccionada = False
                FgReq.CellBackColor = estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).cor
            End If
        End If
    End If
End Sub

Private Sub FgReq_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        If FgReq.row <= totalTabela - 1 Then
            If FgReq.Col <= estrutTabela(FgReq.row + 1).colunasOcupadas - 1 Then
                MDIFormInicio.PopupMenu MDIFormInicio.IDM_ERESULTS_HIDE
            End If
        End If
    End If
End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub FuncaoProcurar()
    Dim sSql As String
    Dim RsRequis As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    LbDataEnvio.caption = ""
    LbDataER.caption = ""
    LbDataVal.caption = ""
    
    totalTabela = 0
    ReDim estrutTabela(totalTabela)
    LimpaFGReq
    
    sSql = "SELECT x2.n_req, x2.dt_chega, x2.estado_req, x2.cod_local, x1.utente, x1.t_utente, x3.cod_proven, x3.descr_proven "
    sSql = sSql & " FROM SL_REQUIS x2, sl_identif x1, sl_proven x3 WHERE  x1.seq_utente = x2.seq_utente AND x2.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value)
    sSql = sSql & " AND " & BL_TrataDataParaBD(EcDtFim.value)
    sSql = sSql & " AND x2.cod_local = " & gCodLocal
    sSql = sSql & " AND x2.cod_proven = x3.cod_proven "
    sSql = sSql & " AND (x3.flg_invisivel is null or x3.flg_invisivel = 0) "
    sSql = sSql & " AND (x3.flg_nao_envia_er is null or x3.flg_nao_envia_er = 0) "
    
    If gTipoInstituicao = "PRIVADA" Then
        sSql = sSql & " AND x1.N_CARTAO_UTE IS NOT NULL "
    End If
    If gLAB = "IPO PT" Then
        sSql = sSql & " AND x1.t_utente IN ('IPO','SSO') AND x2.t_sit is not null AND x2.n_epis IS NOT NULL "
    End If

    
    If CbUrgencia.ListIndex > mediComboValorNull Then
        sSql = sSql & " AND  x2.T_urg = " & BL_TrataStringParaBD(BG_DaComboSel(CbUrgencia))
    End If
    
    'SE PROVENI�NCIAS PREENCHIDAS
    If EcListaProven.ListCount > 0 Then
        sSql = sSql & " AND x3.seq_proven  IN ("
        For i = 0 To EcListaProven.ListCount - 1
            sSql = sSql & EcListaProven.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' LOCAL PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaLocais.ListCount > 0 Then
        sSql = sSql & " AND x2.cod_local IN ("
        For i = 0 To EcListaLocais.ListCount - 1
            sSql = sSql & EcListaLocais.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    
    If EcNumReq.Text <> "" Then
        sSql = sSql & " AND x2.n_req = " & EcNumReq.Text
    End If
    
    sSql = sSql & " ORDER BY x2.n_req ASC "
    
    RsRequis.CursorType = adOpenStatic
    RsRequis.CursorLocation = adUseServer
    If gModoDebug = mediSim Then
        BG_LogFile_Erros sSql
    End If
    RsRequis.Open sSql, gConexao
    While Not RsRequis.EOF
        PreencheEstrutReq BL_HandleNull(RsRequis!n_req, ""), BL_HandleNull(RsRequis!dt_chega, ""), _
                          BL_HandleNull(RsRequis!estado_req, ""), BL_HandleNull(RsRequis!cod_local, ""), _
                          BL_HandleNull(RsRequis!t_utente, ""), BL_HandleNull(RsRequis!Utente, ""), _
                          BL_HandleNull(RsRequis!cod_proven, ""), BL_HandleNull(RsRequis!descr_proven, "")
                          
        RsRequis.MoveNext
    Wend
    RsRequis.Close
    Set RsRequis = Nothing
    PreencheFGReq
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    DoEvents
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Procurar: " & Err.Description, Me.Name, "FuncaoProcurar", True
    Exit Sub
    Resume Next
End Sub

Sub FuncaoLimpar()
    LimpaCampos
End Sub

Private Sub LimpaCampos()
    EcDtInicio.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    CbUrgencia.ListIndex = mediComboValorNull
    LbDataEnvio = ""
    LbDataER = ""
    LbDataVal = ""
    totalTabela = 0
    ReDim estrutTabela(totalTabela)
    FrameDet.Visible = False
    LbDataEnvio.caption = ""
    LbDataER.caption = ""
    LbDataVal.caption = ""
End Sub

Sub PreencheValoresDefeito()
    EcDtInicio.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia, mediAscComboCodigo
    
End Sub

' ---------------------------------------------------------------------------

' LIMPA A GRELHA DAS ANALISES

' ---------------------------------------------------------------------------

Private Sub LimpaFGReq()
    Dim i As Integer
    Dim j As Integer
    FgReq.rows = 1
    
    For i = 0 To FgReq.Cols - 1
        FgReq.Col = i
        FgReq.CellBackColor = vbWhite
        FgReq.TextMatrix(0, i) = ""
    Next
    DoEvents
End Sub

' ---------------------------------------------------------------------------

' PREENCHE Estrutura requisicoes

' ---------------------------------------------------------------------------
Private Sub PreencheEstrutReq(n_req As String, dt_chega As String, estado_req As String, cod_local As String, _
                              t_utente As String, Utente As String, cod_proven As String, descr_proven As String)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    If estrutTabela(totalTabela).colunasOcupadas >= TAM_LINHA Or totalTabela = 0 Then
        totalTabela = totalTabela + 1
        ReDim Preserve estrutTabela(totalTabela)
        estrutTabela(totalTabela).linha = totalTabela - 1
        estrutTabela(totalTabela).colunasOcupadas = 0
        ReDim estrutTabela(totalTabela).requisicoes(0)
    End If
    
    estrutTabela(totalTabela).colunasOcupadas = estrutTabela(totalTabela).colunasOcupadas + 1
    ReDim Preserve estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas)
    estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).t_utente = t_utente
    estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).Utente = Utente
    estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).num_requis = n_req
    estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).dt_chega = dt_chega
    estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).estado_req = estado_req
    estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).cod_local = cod_local
    estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).cod_proven = cod_proven
    estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).descr_proven = descr_proven
    estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).flg_seleccionada = False
    
    'VERIFICA SE EXISTEM ANALISES DISPONIVEIS PARA ER (VALIDADAS OU ASSINADAS DEPENDE DE PARAM
    estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).flg_ana_disponiveis = False
    If gAssinaturaActivo = mediSim And gEnviaRelatorioEresultsChegada <> mediSim Then
        sSql = "SELECT * FROM sl_realiza WHERE n_Req = " & n_req & " AND ( n_req in (SELECT sl_req_assinatura.n_Req from sl_req_assinatura where sl_req_assinatura.n_req = sl_realiza.n_req)"
        sSql = sSql & " or  FLG_assinado = 1 ) ORDER BY dt_val desc, hr_val desc "
    Else
        sSql = "SELECT * FROM sl_realiza WHERE n_Req = " & n_req & " AND  flg_estado in ('3','4','5') ORDER BY dt_val desc, hr_val desc "
    End If
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then
        BG_LogFile_Erros sSql
    End If
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount >= 1 Then
        estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).dt_ult_valid = BL_HandleNull(rsAna!dt_val, "")
        estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).hr_ult_valid = BL_HandleNull(rsAna!hr_val, "")
        estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).flg_ana_disponiveis = True
    End If
    rsAna.Close
    Set rsAna = Nothing

    If estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).flg_ana_disponiveis Then
        'PreencheDataVal totalTabela, estrutTabela(totalTabela).colunasOcupadas
            estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).cor = laranja
            PreencheDataEnvEr totalTabela, estrutTabela(totalTabela).colunasOcupadas
            If estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).dt_ult_envio <> "" Then
                PreencheDataIndER2 totalTabela, estrutTabela(totalTabela).colunasOcupadas
            End If
    Else
        estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).cor = Branco
    End If
    If estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).dt_ult_envio <> "" Then
        If estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).estado_envio = 1 Then
            estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).cor = Amarelo
        ElseIf estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).estado_envio = 0 Then
            estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).cor = laranja
        Else
            estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).cor = vermelho
        End If
    End If
    If VerificaDatas(CInt(totalTabela), CInt(estrutTabela(totalTabela).colunasOcupadas)) = False Then
        estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).cor = Cinza
    End If
    If estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).dt_ult_eR <> "" Then
        estrutTabela(totalTabela).requisicoes(estrutTabela(totalTabela).colunasOcupadas).cor = Verde
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao PReencher EstrutRequisicoes: " & Err.Description, Me.Name, "PreencheEstrutReq", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheFGReq()
    Dim i As Integer
    Dim j As Integer
    On Error GoTo TrataErro
    FgReq.rows = totalTabela
    For i = 1 To totalTabela
        FgReq.RowHeight(i - 1) = 350
        FgReq.row = i - 1
        For j = 1 To estrutTabela(i).colunasOcupadas
            FgReq.Col = j - 1
            If estrutTabela(i).requisicoes(j).estado_req = "C" Then
                FgReq.TextMatrix(i - 1, j - 1) = estrutTabela(i).requisicoes(j).num_requis & "(C)"
            Else
                FgReq.TextMatrix(i - 1, j - 1) = estrutTabela(i).requisicoes(j).num_requis
            End If
            FgReq.CellBackColor = estrutTabela(i).requisicoes(j).cor
        Next
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao PReencher FlexGrid: " & Err.Description, Me.Name, "PreencheFgReq", True
    Exit Sub
    Resume Next
End Sub


Private Sub PreencheDataEnvEr(linha As Long, coluna As Long)
    Dim sSql As String
    Dim rsReal As New ADODB.recordset
    sSql = "SELECT x1.flg_estado, x1.dt_cri, x2.dt_cri dt_envio, x2.hr_cri hr_envio FROM sl_eresults x1 left outer join sl_eresults_envio x2 on x1.cod_original = x2.n_Req "
    sSql = sSql & " WHERE x1.cod_original = " & BL_TrataStringParaBD(estrutTabela(linha).requisicoes(coluna).num_requis)
    sSql = sSql & " ORDER BY x1.dt_cri desc "
    rsReal.CursorType = adOpenStatic
    rsReal.CursorLocation = adUseServer
    If gModoDebug = mediSim Then
        BG_LogFile_Erros sSql
    End If
    rsReal.Open sSql, gConexao
    If rsReal.RecordCount >= 1 Then
        estrutTabela(linha).requisicoes(coluna).estado_envio = BL_HandleNull(rsReal!flg_estado, "")
        If estrutTabela(linha).requisicoes(coluna).estado_envio <> 0 Then
            estrutTabela(linha).requisicoes(coluna).dt_ult_envio = BL_HandleNull(rsReal!dt_envio, BL_HandleNull(rsReal!dt_cri, ""))
            estrutTabela(linha).requisicoes(coluna).hr_ult_envio = BL_HandleNull(rsReal!hr_envio, "")
        End If
    Else
        rsReal.Close
        Set rsReal = Nothing
        Exit Sub
    End If
    rsReal.Close
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao PReencher Data do Ultimo envio eResutls: " & Err.Description, Me.Name, "PreencheDataEnvEr", True
    Exit Sub
    Resume Next
End Sub


Private Sub PreencheDataIndER2(linha As Long, coluna As Long)
    Dim sSql As String
    Dim rsReal As New ADODB.recordset
    Dim cod_origem As String
    On Error GoTo TrataErro
    
    sSql = "SELECT cod_origem_xml FROM gr_empr_inst WHERE cod_empr = " & estrutTabela(linha).requisicoes(coluna).cod_local
    rsReal.CursorType = adOpenStatic
    rsReal.CursorLocation = adUseServer
    If gModoDebug = mediSim Then
        BG_LogFile_Erros sSql
    End If
    rsReal.Open sSql, gConexao
    If rsReal.RecordCount > 0 Then
        cod_origem = BL_HandleNull(rsReal!cod_origem_xml, geResultsCodOrigem)
    End If
    rsReal.Close
    If cod_origem <> "" Then
        sSql = "SELECT dt_cri from er_document_idx_xml WHERE app_origem_id  = " & BL_TrataStringParaBD(cod_origem)
        sSql = sSql & " AND tipo_documento_id = " & BL_TrataStringParaBD(geResultsTipoDoc)
        sSql = sSql & " AND documento = " & BL_TrataStringParaBD(estrutTabela(linha).requisicoes(coluna).num_requis)
        sSql = sSql & " ORDER BY dt_cri desc "
        rsReal.CursorType = adOpenStatic
        rsReal.CursorLocation = adUseServer
        If gModoDebug = mediSim Then
            BG_LogFile_Erros sSql
        End If
        rsReal.Open sSql, gConexao
        If rsReal.RecordCount > 0 Then
            estrutTabela(linha).requisicoes(coluna).dt_ult_eR = BL_HandleNull(rsReal!dt_cri, "")
        End If
        rsReal.Close
        Set rsReal = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao PReencher Data indexa��o 2: " & Err.Description, Me.Name, "PreencheDataIndER2", True
    Exit Sub
    Resume Next
End Sub

Public Sub ApagaRegisto()
    ER_CancelaEresults_v2 estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).num_requis, estrutTabela(FgReq.row + 1).requisicoes(FgReq.Col + 1).cod_local
End Sub

Public Sub EnviaRegisto()
    
End Sub




Private Sub MostraDet(NumReq As String)
    FrameDet.top = 1800
    FrameDet.left = 0
    FrameDet.Visible = True
End Sub


Private Function VerificaDatas(linha As Integer, coluna As Integer) As Boolean
    VerificaDatas = True
    If estrutTabela(linha).requisicoes(coluna).flg_ana_disponiveis = True Then
        If estrutTabela(linha).requisicoes(coluna).dt_ult_envio <> "" Then
            If estrutTabela(linha).requisicoes(coluna).dt_ult_envio = estrutTabela(linha).requisicoes(coluna).dt_ult_valid And estrutTabela(linha).requisicoes(coluna).hr_ult_valid > estrutTabela(linha).requisicoes(coluna).hr_ult_envio Then
                VerificaDatas = False
                Exit Function
            ElseIf estrutTabela(linha).requisicoes(coluna).dt_ult_envio < estrutTabela(linha).requisicoes(coluna).dt_ult_valid Then
                VerificaDatas = False
                Exit Function
            End If
            If estrutTabela(linha).requisicoes(coluna).dt_ult_eR <> "" Then
                If estrutTabela(linha).requisicoes(coluna).dt_ult_envio = estrutTabela(linha).requisicoes(coluna).dt_ult_eR And estrutTabela(linha).requisicoes(coluna).hr_ult_eR < estrutTabela(linha).requisicoes(coluna).hr_ult_envio Then
                    VerificaDatas = False
                    Exit Function
                ElseIf estrutTabela(linha).requisicoes(coluna).dt_ult_envio > estrutTabela(linha).requisicoes(coluna).dt_ult_eR Then
                    VerificaDatas = False
                    Exit Function
                End If
            End If
        End If
    End If
End Function


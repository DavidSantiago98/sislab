VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormHistUtente 
   BackColor       =   &H00FFE0C7&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormHistUtente"
   ClientHeight    =   21090
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15270
   Icon            =   "FormHistUtente.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   21090
   ScaleWidth      =   15270
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox PicVeAlerta 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      FillColor       =   &H8000000F&
      ForeColor       =   &H8000000F&
      Height          =   255
      Left            =   1320
      Picture         =   "FormHistUtente.frx":000C
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   50
      Top             =   9120
      Width           =   255
   End
   Begin SISLAB.CabecalhoResultados CabecalhoResultados1 
      Height          =   1095
      Left            =   0
      TabIndex        =   46
      Top             =   120
      Width           =   15255
      _ExtentX        =   26908
      _ExtentY        =   1931
   End
   Begin VB.CommandButton BtImprimir 
      Height          =   495
      Left            =   14640
      Picture         =   "FormHistUtente.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   45
      ToolTipText     =   "Exportar dados para Excel"
      Top             =   1320
      Width           =   615
   End
   Begin RichTextLib.RichTextBox RText 
      Height          =   615
      Left            =   4080
      TabIndex        =   44
      Top             =   8640
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   1085
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"FormHistUtente.frx":0E60
   End
   Begin VB.CommandButton BtExpandir 
      Height          =   495
      Left            =   14640
      Picture         =   "FormHistUtente.frx":0EE2
      Style           =   1  'Graphical
      TabIndex        =   43
      ToolTipText     =   "Expandir Grelha Resultados"
      Top             =   1800
      Width           =   615
   End
   Begin VB.Frame FrameFGAntib 
      BackColor       =   &H00FFE0C7&
      Height          =   5655
      Left            =   120
      TabIndex        =   30
      Top             =   2880
      Width           =   15015
      Begin MSFlexGridLib.MSFlexGrid FgAntib 
         Height          =   4935
         Left            =   0
         TabIndex        =   31
         Top             =   240
         Width           =   13095
         _ExtentX        =   23098
         _ExtentY        =   8705
         _Version        =   393216
         Rows            =   1
         Cols            =   12
         FixedRows       =   0
         FixedCols       =   0
         BackColorBkg    =   16769223
         GridColor       =   -2147483633
         HighLight       =   0
         GridLines       =   0
         ScrollBars      =   0
         SelectionMode   =   1
         MergeCells      =   1
         BorderStyle     =   0
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFE0C7&
      Height          =   1695
      Left            =   0
      TabIndex        =   0
      Top             =   1200
      Width           =   6375
      Begin RichTextLib.RichTextBox EcObsAna 
         Height          =   1455
         Left            =   0
         TabIndex        =   49
         Top             =   120
         Width           =   6375
         _ExtentX        =   11245
         _ExtentY        =   2566
         _Version        =   393217
         Enabled         =   -1  'True
         Appearance      =   0
         TextRTF         =   $"FormHistUtente.frx":7734
      End
      Begin VB.CheckBox CkUnidades 
         BackColor       =   &H00FFE0C7&
         Caption         =   " Unidades"
         Height          =   195
         Left            =   4080
         TabIndex        =   27
         Top             =   1200
         Width           =   1095
      End
      Begin VB.CheckBox CkValRef 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Val. Ref."
         Height          =   255
         Left            =   5160
         TabIndex        =   26
         Top             =   1200
         Width           =   1095
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   2760
         TabIndex        =   21
         Top             =   1200
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   147259393
         CurrentDate     =   39058
      End
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   255
         Left            =   1440
         TabIndex        =   10
         Top             =   1200
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   147259393
         CurrentDate     =   39058
      End
      Begin VB.CommandButton BtPesquisaRapidaGrAnalises 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   5880
         Picture         =   "FormHistUtente.frx":77B6
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   190
         Width           =   375
      End
      Begin VB.TextBox EcDescrGrAnalises 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   240
         Width           =   3735
      End
      Begin VB.TextBox EcGrAnalises 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   1440
         TabIndex        =   4
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox EcDescrGrTrab 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   720
         Width           =   3735
      End
      Begin VB.CommandButton BtPesquisaRapidaGrTrab 
         Height          =   375
         Left            =   5880
         Picture         =   "FormHistUtente.frx":7D40
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "Pesquisa R�pida de Grupos de Trabalho"
         Top             =   670
         Width           =   375
      End
      Begin VB.TextBox EcCodGrTrab 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   1440
         TabIndex        =   1
         Top             =   720
         Width           =   735
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Intervalo Datas"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   9
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Grupo de An�lises"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Grupo Trabalho"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   720
         Width           =   1335
      End
   End
   Begin VB.Frame FrameFGAna 
      BackColor       =   &H00FFE0C7&
      Height          =   5535
      Left            =   0
      TabIndex        =   28
      Top             =   2880
      Width           =   15015
      Begin VB.CommandButton BtExpandeCelula 
         Height          =   255
         Left            =   120
         Picture         =   "FormHistUtente.frx":82CA
         Style           =   1  'Graphical
         TabIndex        =   42
         ToolTipText     =   "Aumentar Tamanho Colunas"
         Top             =   5200
         Width           =   375
      End
      Begin VB.CommandButton BtDiminuirCelula 
         Height          =   255
         Left            =   600
         Picture         =   "FormHistUtente.frx":EB1C
         Style           =   1  'Graphical
         TabIndex        =   41
         ToolTipText     =   "Diminuir Tamanho Colunas"
         Top             =   5200
         Width           =   375
      End
      Begin MSFlexGridLib.MSFlexGrid FGAna 
         Height          =   4815
         Left            =   120
         TabIndex        =   29
         Top             =   240
         Width           =   14775
         _ExtentX        =   26061
         _ExtentY        =   8493
         _Version        =   393216
         Rows            =   1
         Cols            =   12
         FixedRows       =   0
         FixedCols       =   0
         BackColorBkg    =   16769223
         HighLight       =   0
         GridLines       =   0
         ScrollBars      =   0
         SelectionMode   =   1
         MergeCells      =   1
         BorderStyle     =   0
         Appearance      =   0
      End
   End
   Begin VB.Frame FrameListaMicro 
      BackColor       =   &H00FFE0C7&
      Height          =   1575
      Left            =   7200
      TabIndex        =   32
      Top             =   1200
      Width           =   6975
      Begin VB.CommandButton BtPesquisaRapidaProd 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   4440
         Picture         =   "FormHistUtente.frx":1536E
         Style           =   1  'Graphical
         TabIndex        =   40
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   910
         Width           =   375
      End
      Begin VB.TextBox EcDescrProduto 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2040
         TabIndex        =   39
         Top             =   960
         Width           =   2295
      End
      Begin VB.TextBox EcCodProduto 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   1440
         TabIndex        =   38
         Top             =   960
         Width           =   615
      End
      Begin VB.CommandButton BtPesquisaRapidaMicro 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   4440
         Picture         =   "FormHistUtente.frx":158F8
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   310
         Width           =   375
      End
      Begin VB.TextBox EcDescrMicro 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2040
         TabIndex        =   35
         Top             =   360
         Width           =   2295
      End
      Begin VB.TextBox EcCodMicro 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   1440
         TabIndex        =   34
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Produto"
         Height          =   255
         Index           =   7
         Left            =   240
         TabIndex        =   37
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Microrganismo"
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   33
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame FrameListaAnalises 
      BackColor       =   &H00FFE0C7&
      Height          =   1695
      Left            =   7200
      TabIndex        =   11
      Top             =   1200
      Width           =   6975
      Begin VB.CommandButton BtRemoveTodos 
         Height          =   375
         Left            =   3840
         Picture         =   "FormHistUtente.frx":15E82
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   3240
         Width           =   495
      End
      Begin VB.CommandButton BtAdicionaTodos 
         Height          =   375
         Left            =   3840
         Picture         =   "FormHistUtente.frx":1C6D4
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   2280
         Width           =   495
      End
      Begin VB.TextBox EcAnalise 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   120
         TabIndex        =   22
         Top             =   330
         Width           =   2895
      End
      Begin VB.OptionButton Optperfis 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Perfis"
         Height          =   255
         Left            =   2280
         TabIndex        =   20
         Top             =   1320
         Width           =   975
      End
      Begin VB.OptionButton OptComplexas 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Complexas"
         Height          =   255
         Left            =   1200
         TabIndex        =   19
         Top             =   1320
         Width           =   1095
      End
      Begin VB.OptionButton OptSimples 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Simples"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CommandButton BtAdiciona 
         Height          =   375
         Left            =   3120
         Picture         =   "FormHistUtente.frx":22F26
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   480
         Width           =   495
      End
      Begin VB.CommandButton BtRemove 
         Height          =   375
         Left            =   3120
         Picture         =   "FormHistUtente.frx":29778
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   840
         Width           =   495
      End
      Begin VB.ListBox EcOrigem 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   120
         TabIndex        =   13
         Top             =   600
         Width           =   2895
      End
      Begin VB.ListBox EcDestino 
         Appearance      =   0  'Flat
         Height          =   810
         Left            =   3720
         TabIndex        =   12
         Top             =   360
         Width           =   2895
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "An�lises"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   17
         Top             =   120
         Width           =   975
      End
      Begin VB.Label Label12 
         BackColor       =   &H00FFE0C7&
         Caption         =   "An�lises Seleccionadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3720
         TabIndex        =   16
         Top             =   120
         Width           =   2895
      End
   End
   Begin MSComDlg.CommonDialog NomeFicheiro 
      Left            =   12960
      Top             =   1920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton BtAna 
      Height          =   495
      Left            =   6480
      Picture         =   "FormHistUtente.frx":2FFCA
      Style           =   1  'Graphical
      TabIndex        =   47
      ToolTipText     =   "An�lises"
      Top             =   1440
      Width           =   615
   End
   Begin VB.CommandButton BtMicro 
      Height          =   495
      Left            =   6480
      Picture         =   "FormHistUtente.frx":30734
      Style           =   1  'Graphical
      TabIndex        =   48
      ToolTipText     =   "Antibi�ticos"
      Top             =   2040
      Width           =   615
   End
   Begin VB.Label LbSexos 
      Caption         =   "asd"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7800
      TabIndex        =   25
      Top             =   9000
      Visible         =   0   'False
      Width           =   1215
   End
End
Attribute VB_Name = "FormHistUtente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ------------------------------------------------------

' VERSAO: 26-02-2007 - FG

' ------------------------------------------------------

Option Explicit
' Vari�veis Globais para este Form.
Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Const LinhasDefeito = 18
Const ColunasDefeito = 16

Dim colunaActual As Integer
Dim linhaActual As Integer

Dim SeqUtente As Long
Dim analises As String

Public sCodMicro As String

Dim TipoPesquisa As Integer ' 0 - Microbiologia; 1 - Outras Analises
Const micro = 0
Const ALFANUM = 1

' ------------------------------------------------------
' CORES DISPONIVEIS
' ------------------------------------------------------
Const Amarelo = &HC0C0&
Const AmareloClaro = &HC0FFFF
Const azul = &HFFC0C0
Const CastanhoClaro = &H80FF&
Const CastanhoEscuro = &H4080&
Const Cinza = &HE0E0E0
Const Cinza2 = &HC0C0C0
Const Cinza3 = &H404040
Const cinzento = &H8000000F
Const laranja = &HC0E0FF
Const Verde = &H808000
Const VerdeClaro = &HC0FFC0
Const vermelhoClaro = &HBDC8F4
Const vermelho = &HC0&

' ------------------------------------------------------
' CORES USADAS NA GRELHA INICIAL
' ------------------------------------------------------
Const COR3 = cinzento
Const cor2 = azul
Const cor1 = AmareloClaro

' ------------------------------------------------------
' ESTRUTURA USADA PARA CARREGAR AS LISTBOX DAS ANALISES
' ------------------------------------------------------
Private Type ana
    cod_ana As String
    descr_ana As String
End Type

Dim EstrutAnalisesOrigem() As ana
Dim TotalAnalisesOrigem As Integer
Dim EstrutAnalisesDestino() As ana
Dim TotalAnalisesDestino As Integer

Private Type ordenacao
    cod_ana_s As String
    cod_ana_c As String
    cod_perfis As String
End Type
Dim AnaOrd() As ordenacao

Dim AntibOrd() As String

' ------------------------------------------------------
' ESTRUTURA USADA PARA REQUISICOES
' ------------------------------------------------------
Private Type req
    n_req As String
    dt_chega As String
    hr_chega As String
    coluna As Integer
    cod_produto As String
    estado As String
    linha As Integer
End Type
Dim EstrutReq() As req
Dim totalReq As Integer


' ------------------------------------------------------
' ESTRUTURA USADA PARA ANALISES DA GRELHA
' ------------------------------------------------------
    Private Type simples
        cod_ana_s As String
        descr_ana_s As String
        unidade As String
        val_ref As String
    End Type
    
    Private Type Complexas
        cod_ana_c As String
        Descr_Ana_C As String
        EstrutSimples() As simples
        TotalAnaSimples As Integer
    End Type
    
    Private Type analises
        cod_perfis As String
        descr_perfis As String
        EstrutComplexas() As Complexas
        TotalAnaComplexas As Integer
    End Type
    
    Dim estrutAna() As analises
    Dim totalAna As Integer

' ------------------------------------------------------
' ESTRUTURA USADA PARA RESULTADOS DAS ANALISES
' ------------------------------------------------------
Private Type AnaResultados
    seq_realiza As String
    cod_ana_s As String
    cod_ana_c As String
    cod_perfis As String
    n_req As String
    resultado As String
    flg_estado As String
    tipo As String
    dt_chega As String
    linha As Integer
    DescrObsAna As String
End Type
Dim EstrutAnaRes() As AnaResultados
Dim TotalAnaRes As Integer


' ------------------------------------------------------
' ESTRUTURA USADA PARA ANTIBIOTICOS
' ------------------------------------------------------
Private Type antibioticos
    cod_antib As String
    descr_antib As String
End Type
Dim EstrutAntib() As antibioticos
Dim totalAntib As Integer

' ------------------------------------------------------
' ESTRUTURA USADA PARA RESULTADOS DOS ANTIBIOTICOS
' ------------------------------------------------------
Private Type AntibResultados
    cod_antib As String
    n_req As String
    resultado As String
    cod_micro As String
    descr_micro As String
    abrev_micro As String
    cod_produto As String
    cod_carac_micro As String
    flg_ve As Integer
End Type
Dim EstrutAntibRes() As AntibResultados
Dim TotalAntibRes As Integer

Dim FlgPesquisa As Boolean

Public Sub BtAna_Click()
    FrameFGAna.Visible = True
    FrameListaAnalises.Visible = True
    FrameFGAntib.Visible = False
    FrameFGAna.top = 2880
    FrameFGAna.left = 120
    FrameListaAnalises.Visible = True
    FrameListaAnalises.top = 1200
    FrameListaAnalises.left = 7200
    FrameListaMicro.Visible = False
    TipoPesquisa = ALFANUM
    FuncaoProcurar
End Sub


Private Sub BtDiminuirCelula_Click()
    Dim contador As Integer
    For contador = 6 To FGAna.Cols - 1
        If FGAna.ColWidth(contador) > 100 Then
            FGAna.ColWidth(contador) = FGAna.ColWidth(contador) - 100
        End If
    Next
End Sub

Private Sub BtExpandeCelula_Click()
    Dim contador As Integer
    For contador = 6 To FGAna.Cols - 1
        FGAna.ColWidth(contador) = FGAna.ColWidth(contador) + 100
    Next
End Sub

Private Sub BtExpandir_Click()
If FrameFGAna.top = 2880 Then
    FrameFGAna.top = 360
    FrameFGAna.Height = 8055
    FGAna.Height = 7575
    
    FrameFGAntib.top = 360
    FrameFGAntib.Height = 8055
    FgAntib.Height = 7575
    
    Frame1.Visible = False
    FrameListaAnalises.Visible = False
    FrameListaMicro.Visible = False
    BtExpandeCelula.top = 7800
    BtDiminuirCelula.top = 7800
Else
    FrameFGAna.top = 2880
    FrameFGAna.Height = 5535
    FGAna.Height = 4935
    
    FrameFGAntib.top = 2880
    FrameFGAntib.Height = 5535
    FgAntib.Height = 4935
    
    Frame1.Visible = True
    FrameListaAnalises.Visible = True
    FrameListaMicro.Visible = True
    BtExpandeCelula.top = 5200
    BtDiminuirCelula.top = 5200
End If
End Sub

' -----------------------------------------------------------------------------------

' IMPRIMIR HISTORICO - COME�ADO MAS N�O TERMINADO
' FG

' -----------------------------------------------------------------------------------
Private Sub BtImprimir_Click()
    Dim sNomeFicheiro As String
    Dim coluna As Long
    Dim lContadorLinhas As Long
    Dim iContadorColunas As Integer
    Dim iContadorlinhas As Long
    'Constantes de formata��o de Excel
    Const xlEdgeLeft = 7
    Const xlEdgeBottom = 9
    Const xlHAlignCenter = &HFFFFEFF4
    Const xlHAlignLeft = &HFFFFEFDD
    Const xlHAlignRight = &HFFFFEFC8
    Const xlContinuous = 1
    Const xlColorIndexAutomatic = &HFFFFEFF7
    Const xlDash = &HFFFFEFED
    Const xlHairline = 1
    Const xlMedium = &HFFFFEFD6

    NomeFicheiro.Filter = "Ficheiro Excel (*.xls)|*.xls"
    
    Dim fFicheiroExcel As Object
    
    Dim GridAUsar As DataGrid
        
    On Error GoTo TrataErro
    
    sNomeFicheiro = DevolveNomeFicheiro
    If sNomeFicheiro = "" Then
        Exit Sub
    End If

    If Dir(sNomeFicheiro) <> "" Then
        gMsgResp = MsgBox("A informa��o contida no ficheiro actual ser� perdida!" & vbCrLf & "Deseja proseguir?", vbQuestion + vbYesNo)
        If gMsgResp = vbYes Then
            Kill sNomeFicheiro
        Else
            Exit Sub
        End If
    End If

    BL_InicioProcessamento Me



    Dim FolhaExcel As Object

    'Criar ficheiro
    With CreateObject("Excel.application")
        On Error GoTo TrataErro
        With .Workbooks.Add
            Set fFicheiroExcel = .Worksheets(1)
            fFicheiroExcel.Activate
            Set FolhaExcel = fFicheiroExcel.cells(1, 1)
            FolhaExcel.value = "NOME: " & CabecalhoResultados1.RetornaNome
            Set FolhaExcel = fFicheiroExcel.cells(1, 6)
            FolhaExcel.value = CabecalhoResultados1.RetornaUtente
            
            If TipoPesquisa = ALFANUM Then
                For iContadorColunas = 0 To FgAntib.Cols - 5
                    Set FolhaExcel = fFicheiroExcel.cells(1, iContadorColunas + 1)
                    FolhaExcel.Borders.item(xlEdgeBottom).ColorIndex = xlColorIndexAutomatic
                    FolhaExcel.Borders.item(xlEdgeBottom).LineStyle = xlContinuous
                Next
                'Adicionar colunas com titulo dos campos
                For iContadorlinhas = 0 To FGAna.rows - 1
                    For iContadorColunas = 0 To FGAna.Cols - 1
                    
                        If iContadorColunas > 5 Then
                            coluna = iContadorColunas - 5
                        Else
                            coluna = iContadorColunas
                        End If
                        Set FolhaExcel = fFicheiroExcel.cells(iContadorlinhas + 2, coluna + 1)
                        If iContadorColunas <> 1 And iContadorColunas <> 2 And iContadorColunas <> 3 And iContadorColunas <> 4 And iContadorColunas <> 5 Then
                            If iContadorlinhas = 0 Then
                                FolhaExcel.value = Replace(FGAna.TextMatrix(iContadorlinhas, iContadorColunas), "-", ".")
                            Else
                                FolhaExcel.value = Replace(FGAna.TextMatrix(iContadorlinhas, iContadorColunas), "-", ".")
                            End If
                        End If
                        'FolhaExcel.Interior.Color = &HC0C0C0
                        'FolhaExcel.Borders.Item(xlEdgeLeft).Color = vbBlack
                        FolhaExcel.Borders.item(xlEdgeLeft).LineStyle = xlContinuous
                        FolhaExcel.Borders.item(xlEdgeBottom).ColorIndex = xlColorIndexAutomatic
                        FolhaExcel.Borders.item(xlEdgeBottom).LineStyle = xlContinuous
                        
                    Next iContadorColunas
                Next
            ElseIf TipoPesquisa = micro Then
                For iContadorColunas = 0 To FgAntib.Cols - 2
                    Set FolhaExcel = fFicheiroExcel.cells(1, iContadorColunas + 1)
                    FolhaExcel.Borders.item(xlEdgeBottom).ColorIndex = xlColorIndexAutomatic
                    FolhaExcel.Borders.item(xlEdgeBottom).LineStyle = xlContinuous
                Next
                
                
                For iContadorlinhas = 0 To FgAntib.rows - 1
                    For iContadorColunas = 0 To FgAntib.Cols - 1
                        If iContadorColunas <> 1 Then
                            If iContadorColunas >= 2 Then
                                coluna = iContadorColunas - 1
                            Else
                                coluna = iContadorColunas
                            End If
                            Set FolhaExcel = fFicheiroExcel.cells(iContadorlinhas + 2, coluna + 1)
                            If iContadorlinhas = 0 Then
                                FolhaExcel.value = Replace(FgAntib.TextMatrix(iContadorlinhas, iContadorColunas), "-", ".")
                            Else
                                FolhaExcel.value = FgAntib.TextMatrix(iContadorlinhas, iContadorColunas)
                            End If
                            'FolhaExcel.Interior.Color = &HC0C0C0

                            FolhaExcel.Borders.item(xlEdgeLeft).color = vbBlack
                            FolhaExcel.Borders.item(xlEdgeLeft).LineStyle = xlContinuous
                            FolhaExcel.Borders.item(xlEdgeBottom).ColorIndex = xlColorIndexAutomatic
                            FolhaExcel.Borders.item(xlEdgeBottom).LineStyle = xlContinuous
                        End If
                    Next iContadorColunas
                Next
            End If
            Set FolhaExcel = Nothing
            Set fFicheiroExcel = Nothing

            .SaveAs sNomeFicheiro, , , , , , , , False
        End With
        .Quit
    End With

    BL_FimProcessamento Me
    MsgBox "Exporta��o conclu�da.", vbInformation
Exit Sub
TrataErro:
    '
    Resume Next
End Sub




Public Sub BtMicro_Click()
    FrameFGAna.Visible = False
    FrameListaAnalises.Visible = False
    FrameFGAntib.Visible = True
    FrameFGAntib.top = 2880
    FrameFGAntib.left = 120
    FrameListaAnalises.Visible = False
    FrameListaMicro.top = 1200
    FrameListaMicro.left = 7200
    FrameListaMicro.Visible = True
    TipoPesquisa = micro
    FuncaoProcurar
End Sub



Private Sub CkUnidades_Click()
    If CkUnidades.value = vbChecked Then
        FGAna.ColWidth(5) = 1000
        PreencheUnidades
    Else
        FGAna.ColWidth(5) = 0
        LimpaUnidades
    End If
End Sub

Private Sub CkValRef_Click()
    If CkValRef.value = vbChecked Then
        FGAna.ColWidth(4) = 1000
        PreencheValRef
    Else
        FGAna.ColWidth(4) = 0
        LimpaValRef
    End If
End Sub


Private Sub Command1_Click()

End Sub

Private Sub EcAnalise_Change()
    PreencheAnalisesOrigem BL_HandleNull(EcGrAnalises, ""), BL_HandleNull(EcCodGrTrab, ""), BL_HandleNull(EcAnalise, "")
End Sub





Private Sub FgAna_Click()
    Dim i As Long
    EcObsAna = ""
    If FGAna.Col > 5 And FGAna.row > 2 Then
        If FGAna.TextMatrix(1, FGAna.Col) <> "" Then
            For i = 1 To TotalAnaRes
                If FGAna.TextMatrix(FGAna.row, 1) = EstrutAnaRes(i).cod_ana_s And EstrutAnaRes(i).n_req = FGAna.TextMatrix(1, FGAna.Col) Then
                    EcObsAna = BL_HandleNull(EstrutAnaRes(i).DescrObsAna, "")
                End If
            Next
        End If
    End If
    
    If Trim(EcObsAna.Text) <> "" Then
        EcObsAna.Visible = True
    Else
        EcObsAna.Visible = False
    End If
End Sub

Private Sub FgAntib_DblClick()
    Dim NumReq As String
    Dim CodUteSeq As String
    
    If FgAntib.Col > 1 Then
        If FgAntib.TextMatrix(0, FgAntib.Col) <> "" Then
            NumReq = FgAntib.TextMatrix(0, FgAntib.Col)
            
            Dim GrupoAna As String
            On Error Resume Next
            GrupoAna = EcGrAnalises
        
            gImprimirDestino = crptToWindow
            Call IR_ImprimeResultados(False, False, NumReq, , , , , , False, GrupoAna, , , True, True, , , , , , False)
        End If
    End If
End Sub
Private Sub Fgana_DblClick()
    Dim NumReq As String
    Dim EstadoReq As String
    Dim CodUteSeq As String
    Dim sSql As String
    Dim RsRequis As New ADODB.recordset
    
    If (FGAna.Col = 0 Or FGAna.Col = 1) And FGAna.TextMatrix(FGAna.row, 0) <> "" Then
        ImprimeGrafico FGAna.row
    End If
    
    If FGAna.Col > 5 Then
        If FGAna.TextMatrix(1, FGAna.Col) <> "" Then
            NumReq = FGAna.TextMatrix(1, FGAna.Col)
            sSql = "SELECT estado_Req FROM sl_Requis WHERE n_req = " & NumReq
            Set RsRequis = New ADODB.recordset
            RsRequis.CursorType = adOpenStatic
            RsRequis.CursorLocation = adUseServer
            RsRequis.Open sSql, gConexao, adOpenStatic
            EstadoReq = BL_HandleNull(RsRequis!estado_req, "")
            RsRequis.Close
            Set RsRequis = Nothing
            Dim GrupoAna As String
            On Error Resume Next
            GrupoAna = EcGrAnalises
        
            gImprimirDestino = crptToWindow
            Call IR_ImprimeResultados(False, False, NumReq, EstadoReq, , , , , False, GrupoAna, , , True, True, , , , , , False)
        End If
    End If
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub



Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    Set CampoActivo = Me.ActiveControl
    BG_ParametrizaPermissoes_ADO Me.Name
    estado = 1
    
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub Inicializacoes()

    Me.caption = " Hist�rico do Utente"
    Me.left = 10
    Me.top = 0
    Me.Width = 15360
    Me.Height = 9060  ' Normal
    Set CampoDeFocus = EcAnalise
End Sub

Sub EventoActivate()

    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormHistUtente = Nothing
    If gPassaParams.Param(4) = 0 Then
        FormResMicro.Enabled = True
    Else
        FormResultadosNovo.Enabled = True
    End If
End Sub

Sub LimpaCampos()
    
    'Me.SetFocus
    FGAna.Clear
    FGAna.TextMatrix(0, 0) = " "
    FGAna.TextMatrix(1, 0) = " "
    IIf gPassaParams.Param(4) = micro, FGAna.TextMatrix(2, 0) = " ", False
    FGAna.Col = 0
    FGAna.row = 0
    FgAntib.Clear
    FGAntib_Cores
    
    totalAna = 0
    ReDim estrutAna(0)
    TotalAnaRes = 0
    ReDim EstrutAnaRes(0)
    totalReq = 0
    ReDim EstrutReq(0)
    totalAntib = 0
    ReDim EstrutAntib(0)
    TotalAntibRes = 0
    ReDim EstrutAntibRes(0)
    ReDim AnaOrd(0)
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        Me.caption = "Hist�rico do Utente"
        EcDestino.Clear
        LimpaEstrutAnalisesDestino
        OptSimples.value = True
        
        EcGrAnalises = ""
        EcGrAnalises_Validate False
        
        EcCodGrTrab = ""
        EcCodGrTrab_Validate False
        
        EcDtFim.value = Bg_DaData_ADO
        EcDtInicio.value = Bg_DaData_ADO
        LimpaCampos
    End If
End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If
    
    
End Sub

Sub PreencheValoresDefeito()
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    CabecalhoResultados1.LimpaCampos &HFFE0C7
    CabecalhoResultados1.ProcuraDados CStr(gPassaParams.Param(6))
    SeqUtente = CabecalhoResultados1.RetornaSeqUtente
    TipoPesquisa = gPassaParams.Param(4)
    OptSimples.value = True
    
    If BL_SeleccionaGrupoAnalises = True Then
        If gCodGrAnaUtilizador <> "" Then
            EcGrAnalises = gCodGrAnaUtilizador
            EcGrAnalises_Validate False
        End If
    End If
    
    EcDtFim.value = BL_HandleNull(gPassaParams.Param(5), Bg_DaData_ADO)
    If gLAB = "CHVNG" Then
        EcDtInicio.value = EcDtFim.value - 61
    Else
        EcDtInicio.value = RetornaDtPrimeiraReq
    End If
    
    'BG_LimpaPassaParams
    PreencheAnalisesOrigem "", "", ""
    If (sCodMicro <> "") Then
        EcCodMicro.Text = sCodMicro
        EcCodMicro_Validate (False)
    End If
    'BtAna_Click

End Sub

Sub FuncaoProcurar()
    Dim sSql As String, cod_prod As String
    Dim RsRequis As New ADODB.recordset
    
    LimpaCampos
    
    If SeqUtente = -1 Then
        Call BG_Mensagem(mediMsgBox, "N�o foi indicado nenhum Utente v�lido", vbOKOnly + vbExclamation, App.ProductName)
        EcDtInicio.SetFocus
        Exit Sub
    End If
    
    If EcDtInicio = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da Requisi��o.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtInicio.SetFocus
        Exit Sub
    End If
    
     If EcDtFim = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da Requisi��o.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
        
    sSql = ConstroiCriterioREQUIS
    Set RsRequis = New ADODB.recordset
    RsRequis.CursorType = adOpenStatic
    RsRequis.CursorLocation = adUseServer
    RsRequis.Open sSql, gConexao, adOpenStatic
    If RsRequis.RecordCount > 0 Then
    
        If TipoPesquisa = ALFANUM Then
            ' ------------------------------------------------------------------------
            ' PESQUISA REQUISICOES COM RESULTADOS TIPO ALFANUMERICOS, NUMERICOS, FRASE
            ' ------------------------------------------------------------------------
            CkUnidades.Enabled = True
            CkValRef.Enabled = True
            While Not RsRequis.EOF
                ' ---------------------------------------------------------------
                ' PARA CADA REQ. PREENCHE ESTRUTURA E PROCURA ANALISES DESSA REQ
                ' ---------------------------------------------------------------
                cod_prod = FuncaoProcuraProdutos(RsRequis!n_req)
                PreencheEstrutRequis RsRequis!n_req, BL_HandleNull(RsRequis!dt_chega, ""), BL_HandleNull(RsRequis!hr_chega, ""), BL_HandleNull(RsRequis!estado_req, ""), cod_prod

                If totalReq = FGAna.Cols - 5 Then
                    FGAna.Cols = FGAna.Cols + 1
                End If
                
                FGAna.ColAlignment(totalReq + 5) = flexAlignCenterCenter
                FGAna.TextMatrix(0, totalReq + 5) = RsRequis!dt_chega
                FGAna.TextMatrix(1, totalReq + 5) = RsRequis!n_req
                FGAna.TextMatrix(2, totalReq + 5) = cod_prod
    
                FuncaoProcurarAnalises RsRequis!n_req, BL_HandleNull(RsRequis!estado_req, "")
                'FGAna_Redimensiona LinhasDefeito
                RsRequis.MoveNext
            Wend
            ' ---------------------------------------------------------------
            ' NO FIM PREENCHE A GRELHA
            ' ---------------------------------------------------------------
            PreencheGridAnaOrd
            PreencheGridAnaRes
            CoresFg
            
            FGAna.TextMatrix(0, 0) = " "
            FGAna.TextMatrix(1, 0) = " "
            FGAna.TextMatrix(2, 0) = " "
            FGAna.MergeCol(0) = True
            FGAna.MergeRow(0) = True
            FGAna.MergeRow(1) = True
            
        ElseIf TipoPesquisa = micro Then
            ' ---------------------------------------------------------------
            ' PESQUISA DE REQUISICOES COM TIPO DE RESULTADOS ANTIBIOGRAMA
            ' ---------------------------------------------------------------
            CkUnidades.Enabled = False
            CkValRef.Enabled = False
            While Not RsRequis.EOF
                ' ---------------------------------------------------------------
                ' PARA CADA REQ. PROCURA ANTIBIOTICOS E RESPECTIVA SENSIBILIDADE
                ' ---------------------------------------------------------------
                PreencheEstrutRequis RsRequis!n_req, BL_HandleNull(RsRequis!dt_chega, ""), BL_HandleNull(RsRequis!hr_chega, ""), BL_HandleNull(RsRequis!estado_req, "")
                FuncaoProcurarAntibioticos RsRequis!n_req, BL_HandleNull(RsRequis!dt_chega, "")
                RsRequis.MoveNext
            Wend
            PreencheGridAntib
            PreencheGridAntibRes
            FGAntib_Cores
        End If
    Else
        Call BG_Mensagem(mediMsgBox, "N�o Foi Encontrada nenhuma Requisi��o Para O Utente", vbOKOnly + vbExclamation, App.ProductName)
        Exit Sub
    End If
    RsRequis.Close
    Set RsRequis = Nothing
End Sub


' ----------------------------------------------------------------------------------

' PROCURA PRODUTOS DE ANALISES PARA UMA REQUISICAO (APENAS PARA MICRORGANISMO)

' ----------------------------------------------------------------------------------
Private Function FuncaoProcuraProdutos(NReq As String) As String
    
    Dim RsProd As ADODB.recordset
    Set RsProd = New ADODB.recordset
    RsProd.CursorType = adOpenStatic
    RsProd.CursorLocation = adUseServer
    RsProd.Open "select cod_prod from sl_req_prod where n_req = " & NReq, gConexao, adOpenStatic
    
    While (Not RsProd.EOF)
        FuncaoProcuraProdutos = FuncaoProcuraProdutos & ";" & BL_HandleNull(RsProd!cod_prod, "")
        RsProd.MoveNext
    Wend
    FuncaoProcuraProdutos = Mid(FuncaoProcuraProdutos, 2)
    RsProd.Close
End Function


' ----------------------------------------------------------------------------------

' PROCURA RESULTADOS DE ANALISES PARA UMA REQUISICAO

' ----------------------------------------------------------------------------------
Private Sub FuncaoProcurarAnalises(n_req As String, EstadoReq As String)
    Dim sSql As String
    Dim RsRealiza As New ADODB.recordset
    
    Dim tabela_realiza As String
    Dim tabela_alfan As String
    Dim tabela_frase As String
    Dim tabela_micro As String
    Dim tabela_tsq As String
    
    If EstadoReq = gEstadoReqHistorico Then
        tabela_alfan = " SL_RES_ALFAN_H "
        tabela_realiza = " SL_REALIZA_H "
        tabela_frase = " SL_RES_FRASE_H "
        tabela_micro = " SL_RES_MICRO_H "
        tabela_tsq = " SL_RES_TSQ_H "
    Else
        tabela_alfan = " SL_RES_ALFAN "
        tabela_realiza = " SL_REALIZA "
        tabela_frase = " SL_RES_FRASE "
        tabela_micro = " SL_RES_MICRO "
        tabela_tsq = " SL_RES_TSQ "
    End If
    
    sSql = "SELECT r.seq_realiza, r.dt_chega, r.cod_ana_s,cod_ana_c, cod_perfil, result, flg_estado, "
    sSql = sSql & " n_req, ord_ana,cod_agrup,ord_ana_perf,ord_ana_compl, 'ALFANUMERICO' TIPO "
    sSql = sSql & " FROM " & tabela_realiza & " r, " & tabela_alfan & " res "
    ' ---------------------------------------------------------------
    ' SE PREENCHEU GRUPO DE ANALISES OU GRUPO DE TRABALHO
    ' ---------------------------------------------------------------
    If EcGrAnalises <> "" Or analises <> "" Then
        sSql = sSql & ", sl_ana_s "
    End If
    If EcGrAnalises <> "" And EcCodGrTrab <> "" Then
        sSql = sSql & ", sl_ana_trab "
    ElseIf EcGrAnalises = "" And EcCodGrTrab <> "" Then
        sSql = sSql & ",sl_ana_s, sl_ana_trab "
    End If
    ' ---------------------------------------------------------------
    sSql = sSql & " Where "
    If analises <> "" Then
        sSql = sSql & analises & " AND "
    End If
    sSql = sSql & " r.seq_realiza = res.seq_realiza AND n_res = 1 AND r.cod_ana_s <>'S99999' AND "
    sSql = sSql & " n_req = " & n_req
    
    ' ---------------------------------------------------------------
    ' SE PREENCHEU GRUPO DE ANALISES OU GRUPO DE TRABALHO
    ' ---------------------------------------------------------------
    If EcGrAnalises <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = r.cod_ana_s AND sl_ana_s.gr_ana = " & BL_TrataStringParaBD(EcGrAnalises)
    ElseIf analises <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = r.cod_ana_s"
    End If
    If EcGrAnalises <> "" And EcCodGrTrab <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = sl_ana_trab.cod_analise AND sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
    ElseIf EcGrAnalises = "" And EcCodGrTrab <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = sl_ana_trab.cod_analise AND sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
        sSql = sSql & " AND sl_ana_s.cod_ana_s = r.cod_ana_s "
    End If
    ' ---------------------------------------------------------------
    
    
    sSql = sSql & " UNION "
    sSql = sSql & "SELECT r.seq_realiza, r.dt_chega, r.cod_ana_s,cod_ana_c, cod_perfil,sl_dicionario.descr_frase result, flg_estado, "
    sSql = sSql & " n_req, ord_ana,cod_agrup,ord_ana_perf,ord_ana_compl, 'FRASE' TIPO "
    sSql = sSql & " FROM " & tabela_realiza & " r , " & tabela_frase & " f , sl_dicionario  "
    ' ---------------------------------------------------------------
    ' SE PREENCHEU GRUPO DE ANALISES OU GRUPO DE TRABALHO
    ' ---------------------------------------------------------------
    If EcGrAnalises <> "" Or analises <> "" Then
        sSql = sSql & ", sl_ana_s "
    End If
    If EcGrAnalises <> "" And EcCodGrTrab <> "" Then
        sSql = sSql & ", sl_ana_trab "
    ElseIf EcGrAnalises = "" And EcCodGrTrab <> "" Then
        sSql = sSql & ",sl_ana_s, sl_ana_trab "
    End If
    ' ---------------------------------------------------------------
    
    sSql = sSql & " WHERE sl_dicionario.cod_frase= f.cod_frase "
    If analises <> "" Then
        sSql = sSql & " AND " & analises
    End If
    sSql = sSql & " AND r.seq_realiza = f.seq_realiza  AND r.cod_ana_s <>'S99999' AND "
    sSql = sSql & " n_req = " & n_req
    ' ---------------------------------------------------------------
    ' SE PREENCHEU GRUPO DE ANALISES OU GRUPO DE TRABALHO
    ' ---------------------------------------------------------------
    If EcGrAnalises <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = r.cod_ana_s AND sl_ana_s.gr_ana = " & BL_TrataStringParaBD(EcGrAnalises)
    ElseIf analises <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = r.cod_ana_s "
    End If
    If EcGrAnalises <> "" And EcCodGrTrab <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = sl_ana_trab.cod_analise AND sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
    ElseIf EcGrAnalises = "" And EcCodGrTrab <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = sl_ana_trab.cod_analise AND sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
        sSql = sSql & " AND sl_ana_s.cod_ana_s = r.cod_ana_s "
    End If
    ' ---------------------------------------------------------------
    
    
    sSql = sSql & " UNION "
    sSql = sSql & "SELECT  r.seq_realiza, r.dt_chega, r.cod_ana_s,cod_ana_c, cod_perfil,sl_microrg.DESCR_microrg result, flg_estado, "
    sSql = sSql & " n_req, ord_ana,cod_agrup,ord_ana_perf,ord_ana_compl, 'MICRO' TIPO  "
    sSql = sSql & " FROM " & tabela_realiza & " r , " & tabela_micro & " m , sl_microrg  "
    ' ---------------------------------------------------------------
    ' SE PREENCHEU GRUPO DE ANALISES OU GRUPO DE TRABALHO
    ' ---------------------------------------------------------------
    If EcGrAnalises <> "" Or analises <> "" Then
        sSql = sSql & ", sl_ana_s "
    End If
    If EcGrAnalises <> "" And EcCodGrTrab <> "" Then
        sSql = sSql & ", sl_ana_trab "
    ElseIf EcGrAnalises = "" And EcCodGrTrab <> "" Then
        sSql = sSql & ",sl_ana_s, sl_ana_trab "
    End If
    ' ---------------------------------------------------------------
    sSql = sSql & " WHERE sl_microrg.cod_microrg= m.cod_micro "
    If analises <> "" Then
        sSql = sSql & " AND " & analises
    End If
    sSql = sSql & " AND r.seq_realiza = m.seq_realiza AND r.cod_ana_s <>'S99999' AND "
    sSql = sSql & " n_req = " & n_req
    ' ---------------------------------------------------------------
    ' SE PREENCHEU GRUPO DE ANALISES OU GRUPO DE TRABALHO
    ' ---------------------------------------------------------------
    If EcGrAnalises <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = r.cod_ana_s AND sl_ana_s.gr_ana = " & BL_TrataStringParaBD(EcGrAnalises)
    ElseIf analises <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = r.cod_ana_s "
    End If
    
    If EcGrAnalises <> "" And EcCodGrTrab <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = sl_ana_trab.cod_analise AND sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
    ElseIf EcGrAnalises = "" And EcCodGrTrab <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = sl_ana_trab.cod_analise AND sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
        sSql = sSql & " AND sl_ana_s.cod_ana_s = r.cod_ana_s "
    End If
    ' ---------------------------------------------------------------
    
    If gSGBD = gOracle Then
        sSql = sSql & " ORDER BY n_req, ord_ana, cod_agrup, cod_perfil, "
        sSql = sSql & " ord_ana_perf, cod_ana_c, ord_ana_compl, cod_ana_s "
    Else
        sSql = sSql & " ORDER BY n_req, ord_ana, cod_agrup, r.cod_perfil, "
        sSql = sSql & " ord_ana_perf, r.cod_ana_c, ord_ana_compl, r.cod_ana_s "
    End If
    
    RsRealiza.CursorType = adOpenStatic
    RsRealiza.CursorLocation = adUseServer
    RsRealiza.Open sSql, gConexao, adOpenStatic
    If RsRealiza.RecordCount > 0 Then
        While Not RsRealiza.EOF
            PreencheEstrutAnaRes RsRealiza!seq_realiza, n_req, RsRealiza!cod_ana_s, RsRealiza!cod_ana_c, RsRealiza!Cod_Perfil, BL_HandleNull(RsRealiza!result, ""), RsRealiza!flg_estado, RsRealiza!tipo, BL_HandleNull(RsRealiza!dt_chega, "")
            PreencheEstrutAna RsRealiza!cod_ana_s, RsRealiza!cod_ana_c, RsRealiza!Cod_Perfil
            RsRealiza.MoveNext
        Wend
    End If
    RsRealiza.Close
    Set RsRealiza = Nothing
    
End Sub

Sub FuncaoImprimir()
End Sub
    
Sub DefTipoCampos()
    Dim contador As Integer
    With FGAna
        .Font.Size = 6
        .rows = LinhasDefeito
        If (gPassaParams.Param(4) = micro) Then
            .FixedRows = 3
        Else
            .FixedRows = 2
        End If
        .Cols = ColunasDefeito
        .FixedRows = 3
        .FixedCols = 1
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarNone
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridNone
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        .ColWidth(0) = 3000
        .Col = 0
        '.ColWidth(1) = 600
        .ColWidth(1) = 0
        .Col = 1
        .CellAlignment = flexAlignCenterCenter
        '.ColWidth(2) = 600
        .ColWidth(2) = 0
        .Col = 2
        .CellAlignment = flexAlignCenterCenter
        '.ColWidth(3) = 600
        .ColWidth(3) = 0
        .Col = 3
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(4) = 0
        .Col = 4
        .ColAlignment(4) = flexAlignCenterCenter
        .ColWidth(5) = 0
        .Col = 5
        .ColAlignment(5) = flexAlignCenterCenter
        For contador = 6 To FGAna.Cols - 1
            .ColWidth(contador) = 1600
            .Col = contador
            .ColAlignment(contador) = flexAlignLeftCenter
        Next
        .Col = 0
    End With
    'FGAna.Width = 13300
    FGAna.Height = 5000
    FGAna.ScrollBars = flexScrollBarBoth
    FGAna.Col = 0
    FGAna.row = 0

    With FgAntib
        .Font.Size = 6
        .rows = LinhasDefeito
        .FixedRows = 4
        .Cols = ColunasDefeito
        .FixedCols = 1
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarNone
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridNone
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        .ColWidth(0) = 2670
        .Col = 0
        .ColWidth(1) = 0
        .Col = 1
        For contador = 2 To FgAntib.Cols - 1
            .ColWidth(contador) = 1500
            .Col = contador
            .ColAlignment(contador) = flexAlignCenterCenter
        Next
        .Col = 0
    End With
    FgAntib.Width = 13300
    FgAntib.Height = 5000
    FgAntib.ScrollBars = flexScrollBarBoth
    FGAntib_Cores
    EcObsAna.Visible = False
End Sub


' ----------------------------------------------------------------------

' MUDA AS CORES DA FLEXGRID

' ----------------------------------------------------------------------

Private Sub FGAntib_Cores()
    Dim linha As Integer
    Dim coluna As Integer
    Dim corActual
    
    FgAntib.row = 3
    For coluna = 0 To FgAntib.Cols - 1
        FgAntib.Col = coluna
        FgAntib.CellBackColor = COR3
        corActual = COR3

    Next
    
    
    For linha = 4 To FgAntib.rows - 1
        FgAntib.row = linha
        If FgAntib.TextMatrix(linha, 1) <> FgAntib.TextMatrix(linha - 1, 1) And corActual = COR3 Then
            corActual = cor2
        ElseIf FgAntib.TextMatrix(linha, 1) <> FgAntib.TextMatrix(linha - 1, 1) And corActual = cor2 Then
            corActual = COR3
        ElseIf FgAntib.TextMatrix(linha, 1) = "" And corActual = COR3 Then
            corActual = cor2
        ElseIf FgAntib.TextMatrix(linha, 1) = "" And corActual = cor2 Then
            corActual = COR3
        End If
        
        For coluna = 1 To FgAntib.Cols - 1
            FgAntib.Col = coluna
            FgAntib.CellBackColor = corActual
        Next
    Next
    FgAntib.Col = 0
    FgAntib.row = 2
    FgAntib.topRow = 4
    FgAntib.Col = 0
End Sub


' ----------------------------------------------------------------------------------

' PESQUISA GRUPOS DE ANALISES DISPONIVEIS

' ----------------------------------------------------------------------------------
Private Sub BtPesquisaRapidaGrAnalises_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    ClausulaWhere = " cod_local = " & gCodLocal
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupos de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcGrAnalises.Text = resultados(1)
            EcDescrGrAnalises.Text = resultados(2)
            PreencheAnalisesOrigem BL_HandleNull(EcGrAnalises, ""), BL_HandleNull(EcCodGrTrab, ""), BL_HandleNull(EcAnalise, "")
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub


' ----------------------------------------------------------------------------------

' ACTUALIZA A PESQUISA PARA O GRUPO DE ANALISES ESCOLHIDO

' ----------------------------------------------------------------------------------
Private Sub EcGrAnalises_Validate(Cancel As Boolean)
    Dim RsDescrGrAnalises As ADODB.recordset
    
    If Trim(EcGrAnalises.Text) <> "" Then
        Set RsDescrGrAnalises = New ADODB.recordset
        
        With RsDescrGrAnalises
            .Source = "SELECT descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana= " & BL_TrataStringParaBD(EcGrAnalises.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrAnalises.RecordCount > 0 Then
            EcDescrGrAnalises.Text = RsDescrGrAnalises!descr_gr_ana
            PreencheAnalisesOrigem BL_HandleNull(EcGrAnalises, ""), BL_HandleNull(EcCodGrTrab, ""), BL_HandleNull(EcAnalise, "")
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
        Else
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
            EcDescrGrAnalises.Text = ""
            PreencheAnalisesOrigem BL_HandleNull(EcGrAnalises, ""), BL_HandleNull(EcCodGrTrab, ""), BL_HandleNull(EcAnalise, "")
            BG_Mensagem mediMsgBox, "O Grupo de An�lise indicado n�o existe!", vbOKOnly + vbExclamation, "Aten��o!"
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrGrAnalises.Text = ""
        PreencheAnalisesOrigem BL_HandleNull(EcGrAnalises, ""), BL_HandleNull(EcCodGrTrab, ""), BL_HandleNull(EcAnalise, "")
    End If
    
End Sub


' ----------------------------------------------------------------------------------

' PESQUISA GRUPOS DE TRABALHOS DISPONIVEIS

' ----------------------------------------------------------------------------------
Private Sub BtPesquisaRapidaGrTrab_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_trab"
    CamposEcran(1) = "cod_gr_trab"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_trab"
    CamposEcran(2) = "descr_gr_trab"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_trab"
    CampoPesquisa1 = "descr_gr_trab"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupos de Trabalho")
    mensagem = "N�o foi encontrada nenhum Grupo de Trabalho."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrTrab.Text = resultados(1)
            EcDescrGrTrab.Text = resultados(2)
            PreencheAnalisesOrigem BL_HandleNull(EcGrAnalises, ""), BL_HandleNull(EcCodGrTrab, ""), BL_HandleNull(EcAnalise, "")
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

' ----------------------------------------------------------------------------------

' ACTUALIZA A PESQUISA PARA O GRUPO DE TRABALHO ESCOLHIDO

' ----------------------------------------------------------------------------------
Private Sub EcCodGrTrab_Validate(Cancel As Boolean)
    Dim rsDescr As ADODB.recordset
    
    If Trim(EcCodGrTrab.Text) <> "" Then
        Set rsDescr = New ADODB.recordset
        
        With rsDescr
            .Source = "SELECT descr_gr_trab FROM sl_gr_trab WHERE cod_gr_trab= " & BL_TrataStringParaBD(EcCodGrTrab.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If rsDescr.RecordCount > 0 Then
            EcDescrGrTrab.Text = rsDescr!descr_gr_trab
            PreencheAnalisesOrigem BL_HandleNull(EcGrAnalises, ""), BL_HandleNull(EcCodGrTrab, ""), BL_HandleNull(EcAnalise, "")
            rsDescr.Close
            Set rsDescr = Nothing

        Else
            rsDescr.Close
            Set rsDescr = Nothing
            EcDescrGrTrab.Text = ""
            PreencheAnalisesOrigem BL_HandleNull(EcGrAnalises, ""), BL_HandleNull(EcCodGrTrab, ""), BL_HandleNull(EcAnalise, "")
            BG_Mensagem mediMsgBox, "O Grupo de Trabalho n�o existe!", vbOKOnly + vbExclamation, "Aten��o!"
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrGrTrab.Text = ""
        PreencheAnalisesOrigem BL_HandleNull(EcGrAnalises, ""), BL_HandleNull(EcCodGrTrab, ""), BL_HandleNull(EcAnalise, "")
    End If
End Sub


' ----------------------------------------------------------------------------------

' PESQUISA MICRORGANISMOS DISPONIVEIS

' ----------------------------------------------------------------------------------
Private Sub BtPesquisaRapidaMicro_Click()
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_microrg"
    CamposEcran(1) = "cod_microrg"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_microrg"
    CamposEcran(2) = "descr_microrg"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_microrg"
    CampoPesquisa1 = "descr_microrg"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar  Microrganismos")
    mensagem = "N�o foi encontrada nenhum Microrganismo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMicro.Text = resultados(1)
            EcDescrMicro.Text = resultados(2)
            FuncaoProcurar
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

' ----------------------------------------------------------------------------------

' ACTUALIZA A PESQUISA PARA O GRUPO DE TRABALHO ESCOLHIDO

' ----------------------------------------------------------------------------------
Private Sub EcCodMicro_Validate(Cancel As Boolean)
    Dim rsDescr As ADODB.recordset
    
    If Trim(EcCodMicro.Text) <> "" Then
        EcCodMicro = UCase(EcCodMicro)
        Set rsDescr = New ADODB.recordset
        
        With rsDescr
            .Source = "SELECT descr_microrg FROM sl_microrg WHERE cod_microrg= " & BL_TrataStringParaBD(EcCodMicro.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If rsDescr.RecordCount > 0 Then
            EcDescrMicro.Text = rsDescr!descr_microrg
            rsDescr.Close
            Set rsDescr = Nothing
            FuncaoProcurar
        Else
            rsDescr.Close
            Set rsDescr = Nothing
            EcDescrMicro.Text = ""
            BG_Mensagem mediMsgBox, "O Microrganismo n�o existe!", vbOKOnly + vbExclamation, "Aten��o!"
            Cancel = True
            Sendkeys ("{HOME}+{END}")
            FuncaoProcurar
        End If
    Else
        EcDescrMicro.Text = ""
    End If
End Sub


Private Sub BtPesquisaRapidaProd_Click()
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_produto"
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_produto"
    CampoPesquisa1 = "descr_produto"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Produtos")
    mensagem = "N�o foi encontrada nenhum Produto."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProduto.Text = resultados(1)
            EcDescrProduto.Text = resultados(2)
            FuncaoProcurar
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

' ----------------------------------------------------------------------------------

' ACTUALIZA A PESQUISA PARA O PRODUTO ESCOLHIDO

' ----------------------------------------------------------------------------------
Private Sub EcCodProduto_Validate(Cancel As Boolean)
    Dim rsDescr As ADODB.recordset
    
    If Trim(EcCodProduto.Text) <> "" Then
        EcCodProduto = UCase(EcCodProduto)
        Set rsDescr = New ADODB.recordset
        
        With rsDescr
            .Source = "SELECT descr_produto FROM sl_produto WHERE cod_produto= " & UCase(BL_TrataStringParaBD(EcCodProduto.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If rsDescr.RecordCount > 0 Then
            EcDescrProduto.Text = rsDescr!descr_produto
            FuncaoProcurar
            rsDescr.Close
            Set rsDescr = Nothing

        Else
            rsDescr.Close
            Set rsDescr = Nothing
            EcDescrMicro.Text = ""
            BG_Mensagem mediMsgBox, "O Produto n�o existe!", vbOKOnly + vbExclamation, "Aten��o!"
            Cancel = True
            Sendkeys ("{HOME}+{END}")
            FuncaoProcurar
        End If
    Else
        EcDescrProduto.Text = ""
    End If
End Sub






' ----------------------------------------------------------------------------------

' ACTUALIZA A PESQUISA PARA O TIPO DE ANALISES SELECCIONADAS

' ----------------------------------------------------------------------------------
Private Sub OptComplexas_Click()
    PreencheAnalisesOrigem BL_HandleNull(EcGrAnalises, ""), BL_HandleNull(EcCodGrTrab, ""), BL_HandleNull(EcAnalise, "")
End Sub


' ----------------------------------------------------------------------------------

' ACTUALIZA A PESQUISA PARA O TIPO DE ANALISES SELECCIONADAS

' ----------------------------------------------------------------------------------
Private Sub OptPerfis_Click()
    PreencheAnalisesOrigem BL_HandleNull(EcGrAnalises, ""), BL_HandleNull(EcCodGrTrab, ""), BL_HandleNull(EcAnalise, "")
End Sub


' ----------------------------------------------------------------------------------

' ACTUALIZA A PESQUISA PARA O TIPO DE ANALISES SELECCIONADAS

' ----------------------------------------------------------------------------------
Private Sub OptSimples_Click()
    PreencheAnalisesOrigem BL_HandleNull(EcGrAnalises, ""), BL_HandleNull(EcCodGrTrab, ""), BL_HandleNull(EcAnalise, "")
End Sub



' ----------------------------------------------------------------------------------

' ADICIONA UM ELEMENTO DA LISTA DE TODAS ANALISES PARA AS SELECIONADAS

' ----------------------------------------------------------------------------------
Private Sub BtAdiciona_Click()
    Dim i As Integer
    Dim j, m, total As Integer
    
    For i = 0 To EcOrigem.ListCount - 1
        If EcOrigem.Selected(i) Then
            
            PreencheEstrutAnalisesDestino EstrutAnalisesOrigem(i + 1).cod_ana, EstrutAnalisesOrigem(i + 1).descr_ana
            EcDestino.AddItem EcOrigem.List(i)
            EcDestino.ItemData(EcDestino.NewIndex) = EcOrigem.ItemData(i)
        End If
    Next i

    j = EcOrigem.SelCount
    m = 0
    For i = EcOrigem.ListCount - 1 To 0 Step -1
        If EcOrigem.Selected(i) Then
            total = i
            If j > 1 Then
                m = EcOrigem.ListIndex
                j = 1
            End If
            EcOrigem.RemoveItem i
            RemoveEstrutAnalisesOrigem i
        End If
    Next i
    
End Sub


' ----------------------------------------------------------------------------------

' ADICIONA TODOS ELEMENTOS DA LISTA DE TODAS ANALISES PARA AS SELECIONADAS

' ----------------------------------------------------------------------------------
Private Sub BtAdicionaTodos_Click()
    Dim i As Integer
    For i = 1 To TotalAnalisesOrigem
        EcOrigem.ListIndex = 0
        BtAdiciona_Click
    Next
End Sub


' ----------------------------------------------------------------------------------

' REMOVE UM ITEM DA LISTA DAS SELECIONADAS

' ----------------------------------------------------------------------------------
Private Sub BtRemove_Click()
    EcOrigem.AddItem EcDestino.List(EcDestino.ListIndex)
    PreencheEstrutAnalisesOrigem EstrutAnalisesDestino(EcDestino.ListIndex + 1).cod_ana, EstrutAnalisesDestino(EcDestino.ListIndex + 1).descr_ana
    EcOrigem.ItemData(EcOrigem.NewIndex) = EcDestino.ItemData(EcDestino.ListIndex)
    RemoveEstrutAnalisesSel (EcDestino.ListIndex + 1)
    EcDestino.RemoveItem (EcDestino.ListIndex)
End Sub


' ----------------------------------------------------------------------------------

' REMOVE TODOS ITENS DA LISTA DAS SELECIONADAS

' ----------------------------------------------------------------------------------
Private Sub BtRemoveTodos_Click()
    Dim i As Integer
    For i = 1 To TotalAnalisesDestino
        EcDestino.ListIndex = 0
        BtRemove_Click
    Next
End Sub

' ----------------------------------------------------------------------------------

' PREENCHE ESTRURURA DE ANALISES

' ----------------------------------------------------------------------------------
Private Sub PreencheEstrutAnalisesOrigem(cod_ana As String, descr_ana As String)
    
    TotalAnalisesOrigem = TotalAnalisesOrigem + 1
    ReDim Preserve EstrutAnalisesOrigem(TotalAnalisesOrigem)
    
    EstrutAnalisesOrigem(TotalAnalisesOrigem).cod_ana = cod_ana
    EstrutAnalisesOrigem(TotalAnalisesOrigem).descr_ana = descr_ana

    
End Sub


' ----------------------------------------------------------------------------------

' LIMPA ESTRUTURA DE ANALISES

' ----------------------------------------------------------------------------------
Private Sub LimpaEstrutAnalisesOrigem()
    TotalAnalisesOrigem = 0
    ReDim EstrutAnalisesOrigem(TotalAnalisesOrigem)
End Sub


' ----------------------------------------------------------------------------------

' LIMPA ESTRUTURA DE ANALISES SELECIONADAS

' ----------------------------------------------------------------------------------
Private Sub LimpaEstrutAnalisesDestino()
    TotalAnalisesDestino = 0
    ReDim EstrutAnaSel(TotalAnalisesDestino)
End Sub

' ----------------------------------------------------------------------------------

' PREENCHE ESTRUTURA DE ANALISES SELECIONADAS

' ----------------------------------------------------------------------------------
Private Sub PreencheEstrutAnalisesDestino(cod_ana As String, descr_ana As String)
    
    TotalAnalisesDestino = TotalAnalisesDestino + 1
    ReDim Preserve EstrutAnalisesDestino(TotalAnalisesDestino)
    
    EstrutAnalisesDestino(TotalAnalisesDestino).cod_ana = cod_ana
    EstrutAnalisesDestino(TotalAnalisesDestino).descr_ana = descr_ana
    
End Sub


' ----------------------------------------------------------------------------------

' REMOVE UM ITEM DA ESTRUTURA DE ANALISES

' ----------------------------------------------------------------------------------
Private Sub RemoveEstrutAnalisesOrigem(indice As Integer)
    Dim i As Integer
        
    If TotalAnalisesOrigem > 1 Then
        For i = indice To TotalAnalisesOrigem - 1
            EstrutAnalisesOrigem(i).cod_ana = EstrutAnalisesOrigem(i + 1).cod_ana
            EstrutAnalisesOrigem(i).descr_ana = EstrutAnalisesOrigem(i + 1).descr_ana
        Next
    End If
    
    TotalAnalisesOrigem = TotalAnalisesOrigem - 1
    ReDim Preserve EstrutAnalisesOrigem(TotalAnalisesOrigem)
End Sub

' ----------------------------------------------------------------------------------

' REMOVE UM ITEM DA ESTRUTURA DE ANALISES SELECIONADAS

' ----------------------------------------------------------------------------------
Private Sub RemoveEstrutAnalisesSel(indice As Integer)
    Dim i As Integer
        
    If TotalAnalisesDestino > 1 Then
        For i = indice To TotalAnalisesDestino - 1
            EstrutAnalisesDestino(i).cod_ana = EstrutAnalisesDestino(i + 1).cod_ana
            EstrutAnalisesDestino(i).descr_ana = EstrutAnalisesDestino(i + 1).descr_ana
        Next
    End If
    
    TotalAnalisesDestino = TotalAnalisesDestino - 1
    ReDim Preserve EstrutAnalisesDestino(TotalAnalisesDestino)
End Sub


' ----------------------------------------------------------------------------------

' PREENCHE ESTRURURA DE REQUISICOES

' ----------------------------------------------------------------------------------
Private Sub PreencheEstrutRequis(n_req As String, dt_chega As String, hr_chega As String, estado_req As String, Optional cod_produto As String)
    
    totalReq = totalReq + 1
    ReDim Preserve EstrutReq(totalReq)
    
    EstrutReq(totalReq).n_req = n_req
    EstrutReq(totalReq).dt_chega = dt_chega
    EstrutReq(totalReq).hr_chega = hr_chega
    EstrutReq(totalReq).coluna = totalReq
    EstrutReq(totalReq).estado = estado_req
    EstrutReq(totalReq).cod_produto = cod_produto
    
End Sub



' ----------------------------------------------------------------------------------

' PREENCHE ESTRURURA DE RESULTADOS DE REQUISICOES

' ----------------------------------------------------------------------------------
Private Sub PreencheEstrutAnaRes(seq_realiza As String, n_req As String, cod_ana_s As String, cod_ana_c As String, cod_perfis As String, _
                resultado As String, flg_estado As String, tipo As String, dt_chega As String)
    
    TotalAnaRes = TotalAnaRes + 1
    ReDim Preserve EstrutAnaRes(TotalAnaRes)
    
    EstrutAnaRes(TotalAnaRes).seq_realiza = seq_realiza
    EstrutAnaRes(TotalAnaRes).cod_ana_s = cod_ana_s
    EstrutAnaRes(TotalAnaRes).cod_ana_c = cod_ana_c
    EstrutAnaRes(TotalAnaRes).cod_perfis = cod_perfis
    EstrutAnaRes(TotalAnaRes).n_req = n_req
    EstrutAnaRes(TotalAnaRes).resultado = resultado
    EstrutAnaRes(TotalAnaRes).flg_estado = flg_estado
    EstrutAnaRes(TotalAnaRes).tipo = tipo
    EstrutAnaRes(TotalAnaRes).dt_chega = dt_chega
    EstrutAnaRes(TotalAnaRes).DescrObsAna = VerificaObsANa(CLng(seq_realiza))
End Sub


' ----------------------------------------------------------------------------------

' PREENCHE ESTRURURA DAS ANALISES QUE VAO APARECER NA GRELHA

' ----------------------------------------------------------------------------------
Private Sub PreencheEstrutAna(cod_ana_s As String, cod_ana_c As String, cod_perfis As String)
    Dim indice_perfil As Integer
    Dim indice_complexa As Integer
    Dim indice_simples As Integer
    Dim FlgExistePerfil As Integer
    Dim FlgExisteComplexa As Integer
    Dim cor As String
    
    ' GUARDA O CODIGO DA ANALISE NO VECTOR DE ORDENACAO DE ANALISES
    GuardaAnaOrd cod_ana_s, cod_ana_c, cod_perfis
    
    FlgExisteComplexa = 0
    FlgExistePerfil = 0
    For indice_perfil = 1 To totalAna
        If estrutAna(indice_perfil).cod_perfis = cod_perfis Then
            FlgExistePerfil = indice_perfil
            For indice_complexa = 1 To estrutAna(indice_perfil).TotalAnaComplexas
                If estrutAna(indice_perfil).EstrutComplexas(indice_complexa).cod_ana_c = cod_ana_c Then
                    FlgExisteComplexa = indice_complexa
                    For indice_simples = 1 To estrutAna(indice_perfil).EstrutComplexas(indice_complexa).TotalAnaSimples
                        If estrutAna(indice_perfil).EstrutComplexas(indice_complexa).EstrutSimples(indice_simples).cod_ana_s = cod_ana_s Then
                            Exit Sub
                        End If
                    Next
                
                End If
            Next
        End If
    Next
    
    
    If FlgExistePerfil = 0 Then
        ' NAO EXISTE PERFIL. CRIA-SE NOVO PERFIL COM UMA COMPLEXA E A SIMPLES DENTRO DA COMPLEXA
        totalAna = totalAna + 1
        ReDim Preserve estrutAna(totalAna)
        estrutAna(totalAna).cod_perfis = cod_perfis
        If cod_perfis <> "0" Then
            estrutAna(totalAna).descr_perfis = BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", cod_perfis)
        End If
            
        estrutAna(totalAna).TotalAnaComplexas = 1
        ReDim Preserve estrutAna(totalAna).EstrutComplexas(1)
        estrutAna(totalAna).EstrutComplexas(1).cod_ana_c = cod_ana_c
        If cod_ana_c <> "0" Then
            estrutAna(totalAna).EstrutComplexas(1).Descr_Ana_C = BL_SelCodigo("SL_ANA_C", "DESCR_ANA_C", "COD_ANA_C", cod_ana_c)
        End If
        
        estrutAna(totalAna).EstrutComplexas(1).TotalAnaSimples = 1
        ReDim Preserve estrutAna(totalAna).EstrutComplexas(1).EstrutSimples(1)
        estrutAna(totalAna).EstrutComplexas(1).EstrutSimples(1).cod_ana_s = cod_ana_s
        estrutAna(totalAna).EstrutComplexas(1).EstrutSimples(1).descr_ana_s = BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", cod_ana_s)
        estrutAna(totalAna).EstrutComplexas(1).EstrutSimples(1).unidade = BL_SelCodigo("SL_ANA_S", "UNID_1", "COD_ANA_S", cod_ana_s)
        estrutAna(totalAna).EstrutComplexas(1).EstrutSimples(1).val_ref = DevolveValRef(cod_ana_s)
    
    Else
        If FlgExisteComplexa = 0 Then
            ' EXISTE PERFIL MAS NAO EXISTE COMPLEXA
            estrutAna(FlgExistePerfil).TotalAnaComplexas = estrutAna(FlgExistePerfil).TotalAnaComplexas + 1
            ReDim Preserve estrutAna(FlgExistePerfil).EstrutComplexas(estrutAna(FlgExistePerfil).TotalAnaComplexas)
            estrutAna(FlgExistePerfil).EstrutComplexas(estrutAna(FlgExistePerfil).TotalAnaComplexas).cod_ana_c = cod_ana_c
            If cod_ana_c <> "0" Then
                estrutAna(FlgExistePerfil).EstrutComplexas(estrutAna(FlgExistePerfil).TotalAnaComplexas).Descr_Ana_C = BL_SelCodigo("SL_ANA_C", "DESCR_ANA_C", "COD_ANA_C", cod_ana_c)
            End If
            
            estrutAna(FlgExistePerfil).EstrutComplexas(estrutAna(FlgExistePerfil).TotalAnaComplexas).TotalAnaSimples = 1
            ReDim Preserve estrutAna(FlgExistePerfil).EstrutComplexas(estrutAna(FlgExistePerfil).TotalAnaComplexas).EstrutSimples(1)
            estrutAna(FlgExistePerfil).EstrutComplexas(estrutAna(FlgExistePerfil).TotalAnaComplexas).EstrutSimples(1).cod_ana_s = cod_ana_s
            estrutAna(FlgExistePerfil).EstrutComplexas(estrutAna(FlgExistePerfil).TotalAnaComplexas).EstrutSimples(1).descr_ana_s = BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", cod_ana_s)
            estrutAna(FlgExistePerfil).EstrutComplexas(estrutAna(FlgExistePerfil).TotalAnaComplexas).EstrutSimples(1).unidade = BL_SelCodigo("SL_ANA_S", "UNID_1", "COD_ANA_S", cod_ana_s)
            estrutAna(FlgExistePerfil).EstrutComplexas(estrutAna(FlgExistePerfil).TotalAnaComplexas).EstrutSimples(1).val_ref = DevolveValRef(cod_ana_s)
        
        Else
            ' JA EXISTE PERFIL E COMPLEXA
            Dim tamanhoSimples As Integer
            tamanhoSimples = estrutAna(FlgExistePerfil).EstrutComplexas(FlgExisteComplexa).TotalAnaSimples + 1
            estrutAna(FlgExistePerfil).EstrutComplexas(FlgExisteComplexa).TotalAnaSimples = tamanhoSimples
            ReDim Preserve estrutAna(FlgExistePerfil).EstrutComplexas(FlgExisteComplexa).EstrutSimples(tamanhoSimples)
            estrutAna(FlgExistePerfil).EstrutComplexas(FlgExisteComplexa).EstrutSimples(tamanhoSimples).cod_ana_s = cod_ana_s
            estrutAna(FlgExistePerfil).EstrutComplexas(FlgExisteComplexa).EstrutSimples(tamanhoSimples).descr_ana_s = BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", cod_ana_s)
            estrutAna(FlgExistePerfil).EstrutComplexas(FlgExisteComplexa).EstrutSimples(tamanhoSimples).unidade = BL_SelCodigo("SL_ANA_S", "UNID_1", "COD_ANA_S", cod_ana_s)
            estrutAna(FlgExistePerfil).EstrutComplexas(FlgExisteComplexa).EstrutSimples(tamanhoSimples).val_ref = DevolveValRef(cod_ana_s)
        End If
    End If

End Sub

' ----------------------------------------------------------------------------------

' PREENCHE O VECTOR DE ORDENACAO DE ANALISES

' ----------------------------------------------------------------------------------
Private Sub GuardaAnaOrd(cod_ana_s As String, cod_ana_c As String, cod_perfis As String)

    ' SE O CODIGO DA ANALISE JA EXISTIR NAO VOLTA A INSERIR
    Dim cod As Variant
    Dim conta As Integer
    For conta = 1 To UBound(AnaOrd)
        If (AnaOrd(conta).cod_ana_s = cod_ana_s And AnaOrd(conta).cod_ana_c = cod_ana_c And AnaOrd(conta).cod_perfis = cod_perfis) Then
            Exit Sub
        End If
    Next
    
    ' INSERE O CODIGO DA ANALISE NO VECTOR DE ORDENACAO DE ANALISES
    ReDim Preserve AnaOrd(UBound(AnaOrd) + 1)
    AnaOrd(UBound(AnaOrd)).cod_ana_s = cod_ana_s
    AnaOrd(UBound(AnaOrd)).cod_ana_c = cod_ana_c
    AnaOrd(UBound(AnaOrd)).cod_perfis = cod_perfis

End Sub

' ----------------------------------------------------------------------------------

' PREENCHE A GRID DE ACORDO COM O VECTOR DE ANALISES ORDENADO

' ----------------------------------------------------------------------------------
Private Sub PreencheGridAnaOrd()
    Dim cod As Variant
    Dim conta As Integer
    Dim cor As Long
    Dim flg_mudaCor As Boolean
    
    ' PERCORRE O VECTOR DE ANALISES ORDENADO
    cor = COR3
    For conta = 1 To UBound(AnaOrd)
        flg_mudaCor = False
        If AnaOrd(conta).cod_perfis <> AnaOrd(conta - 1).cod_perfis Then
            flg_mudaCor = True
        ElseIf AnaOrd(conta).cod_perfis = "0" Then
            If AnaOrd(conta).cod_ana_c <> AnaOrd(conta - 1).cod_ana_c Then
                flg_mudaCor = True
            ElseIf AnaOrd(conta).cod_ana_c = "0" Then
                If AnaOrd(conta).cod_ana_s <> AnaOrd(conta - 1).cod_ana_s Then
                    flg_mudaCor = True
                End If
            End If
        End If
        If flg_mudaCor = True Then
            If cor = COR3 Then
                cor = cor2
            Else
                cor = COR3
            End If
        End If
        PreencheGridAna AnaOrd(conta).cod_ana_s, AnaOrd(conta).cod_ana_c, AnaOrd(conta).cod_perfis, cor
    Next

End Sub


' ----------------------------------------------------------------------------------

' PREENCHE A GRID COM AS ANALISES QUE EXISTEM NA ESTRUTURA

' ----------------------------------------------------------------------------------
Private Sub PreencheGridAna(cod_ana_s As String, cod_ana_c As String, cod_perfis As String, cor As Long)
    Dim indice_perfis As Integer
    Dim indice_complexas As Integer
    Dim indice_simples As Integer
    Dim linha As Integer
    Dim TotalSimples As Integer
    Dim prefixo_perfil As String
    Dim prefixo_complexa As String
    Dim i As Integer
    Dim bPerfil As Boolean
    Dim bComplexa As Boolean
    
    bPerfil = False
    bComplexa = False
    linha = 3
    linha = FGAna.rows
    For i = 3 To FGAna.rows - 1
        If FGAna.TextMatrix(i, 3) = cod_perfis And cod_perfis <> "0" Then
            bPerfil = True
        End If
        If FGAna.TextMatrix(i, 3) = cod_perfis And FGAna.TextMatrix(i, 2) = cod_ana_c And cod_ana_c <> "0" Then
            bComplexa = True
        End If
                
        If FGAna.TextMatrix(i, 0) = "" Then
            linha = i
            Exit For
        End If
    Next
    
    ' --------------------------------------------------------------------------
    ' ENCONTRAMOS OS INDICES QUE QUEREMOS.
    ' - SE FOR PERFIL, APENAS ENCONTRAMOS O INDICE PERFIL
    ' - SE FOR COMPLEXA ENCONTRAMOS O INDICE PERFIL + INDICE
    ' - SE FOR SIMPLES ENCONTRAMOS OS TRES INDICES
    ' --------------------------------------------------------------------------
    For indice_perfis = 1 To totalAna
        If cod_perfis = estrutAna(indice_perfis).cod_perfis Then
            Exit For
        End If
    Next
    For indice_complexas = 1 To estrutAna(indice_perfis).TotalAnaComplexas
        If estrutAna(indice_perfis).EstrutComplexas(indice_complexas).cod_ana_c = cod_ana_c Then
            Exit For
        End If
    Next
        
    For indice_simples = 1 To estrutAna(indice_perfis).EstrutComplexas(indice_complexas).TotalAnaSimples
        If estrutAna(indice_perfis).EstrutComplexas(indice_complexas).EstrutSimples(indice_simples).cod_ana_s = cod_ana_s Then
            Exit For
        End If
    Next
    
    If cod_perfis <> "0" Then
        prefixo_perfil = "   "
    Else
        prefixo_perfil = ""
    End If
    If cod_ana_c <> "0" Then
        prefixo_complexa = prefixo_perfil & "   "
    Else
        prefixo_complexa = prefixo_perfil
    End If
    
    
    If bPerfil = False And cod_perfis <> "0" Then
        
        If linha = FGAna.rows Then
            FGAna.AddItem (linha)
        End If
        FGAna.RowHeight(linha) = 200
        FGAna.TextMatrix(linha, 0) = estrutAna(indice_perfis).descr_perfis
        FGAna.TextMatrix(linha, 3) = estrutAna(indice_perfis).cod_perfis
        FGAna.row = linha
'        FGAna.Col = 0
'        FGAna.CellBackColor = cor

        linha = linha + 1
    End If
    
    ' ----------------------------------------------------------------------------------
    ' PARA CADA COMPLEXA (SE TIVER SIDO SELECIONADO UM PERFIL.......
    ' ----------------------------------------------------------------------------------
    If bComplexa = False And cod_ana_c <> "0" Then
        If linha = FGAna.rows Then
            FGAna.AddItem (linha)
        End If
        FGAna.RowHeight(linha) = 200
        FGAna.TextMatrix(linha, 0) = prefixo_perfil & estrutAna(indice_perfis).EstrutComplexas(indice_complexas).Descr_Ana_C
        FGAna.TextMatrix(linha, 2) = estrutAna(indice_perfis).EstrutComplexas(indice_complexas).cod_ana_c
        FGAna.TextMatrix(linha, 3) = estrutAna(indice_perfis).cod_perfis
        FGAna.row = linha
'        FGAna.Col = 0
'        FGAna.CellBackColor = cor
        
        linha = linha + 1
    End If
        
    For i = 1 To TotalAnaPorRequisicao(estrutAna(indice_perfis).EstrutComplexas(indice_complexas).EstrutSimples(indice_simples).cod_ana_s, estrutAna(indice_perfis).EstrutComplexas(indice_complexas).cod_ana_c, estrutAna(indice_perfis).cod_perfis)
        If linha = FGAna.rows Then
            FGAna.rows = FGAna.rows + 1
        End If
        FGAna.RowHeight(linha) = 200
        FGAna.TextMatrix(linha, 0) = prefixo_complexa & estrutAna(indice_perfis).EstrutComplexas(indice_complexas).EstrutSimples(indice_simples).descr_ana_s
        FGAna.TextMatrix(linha, 1) = estrutAna(indice_perfis).EstrutComplexas(indice_complexas).EstrutSimples(indice_simples).cod_ana_s
        FGAna.TextMatrix(linha, 2) = estrutAna(indice_perfis).EstrutComplexas(indice_complexas).cod_ana_c
        FGAna.TextMatrix(linha, 3) = estrutAna(indice_perfis).cod_perfis
        FGAna.row = linha
'        FGAna.Col = 0
'        FGAna.CellBackColor = cor
        linha = linha + 1
    Next
End Sub


' ----------------------------------------------------------------------------------

' PREENCHE A GRID COM OS RESULTADOS DAS ANALISES

' ----------------------------------------------------------------------------------
Private Sub PreencheGridAnaRes()
    Dim indcReq As Integer
    Dim indcRes As Integer
    Dim indcAnaSel As Integer
    Dim linha As Integer
    Dim tipoResultado As Integer
    Dim i As Integer
    
    For indcReq = 1 To totalReq
        For indcRes = 1 To TotalAnaRes
            If EstrutAnaRes(indcRes).n_req = EstrutReq(indcReq).n_req Then
                For linha = 2 To FGAna.rows - 1
                    
                    
                    If FGAna.TextMatrix(linha, 1) = EstrutAnaRes(indcRes).cod_ana_s And EstrutAnaRes(indcRes).cod_perfis = FGAna.TextMatrix(linha, 3) Then
                        If FGAna.TextMatrix(linha, indcReq + 5) = "" Then
                            FGAna.row = linha
                            FGAna.Col = indcReq + 5
                            If IsNumeric(EstrutAnaRes(indcRes).resultado) Then
                                FGAna.CellAlignment = flexAlignCenterCenter
                            Else
                                FGAna.CellAlignment = flexAlignLeftCenter
                            End If
                            If EstrutAnaRes(indcRes).DescrObsAna = "" Then
                                FGAna.TextMatrix(linha, indcReq + 5) = EstrutAnaRes(indcRes).resultado
                            Else
                                FGAna.TextMatrix(linha, indcReq + 5) = EstrutAnaRes(indcRes).resultado & " *OBS*"
                            End If
                            EstrutAnaRes(indcRes).linha = linha
                            If EstrutAnaRes(indcRes).flg_estado <> "3" And EstrutAnaRes(indcRes).flg_estado <> "4" Then
                                FGAna.CellForeColor = vermelho
                            Else
                                FGAna.CellForeColor = vbBlack
                            End If
                            Exit For
                        Else
                            For i = linha + 1 To FGAna.rows - 2
                                If FGAna.TextMatrix(i, indcReq + 5) = "" And (FGAna.TextMatrix(i - 1, 1) = FGAna.TextMatrix(i, 1)) Then
                                    FGAna.row = i
                                    FGAna.Col = indcReq + 5
                                    If IsNumeric(EstrutAnaRes(indcRes).resultado) Then
                                        FGAna.CellAlignment = flexAlignCenterCenter
                                    Else
                                        FGAna.CellAlignment = flexAlignLeftCenter
                                    End If
                                    If EstrutAnaRes(indcRes).DescrObsAna = "" Then
                                        FGAna.TextMatrix(i, indcReq + 5) = EstrutAnaRes(indcRes).resultado
                                    Else
                                        FGAna.TextMatrix(i, indcReq + 5) = EstrutAnaRes(indcRes).resultado & " *OBS*"
                                    End If
                                    EstrutAnaRes(indcRes).linha = i
                                    If EstrutAnaRes(indcRes).flg_estado <> "3" And EstrutAnaRes(indcRes).flg_estado <> "4" Then
                                        FGAna.CellForeColor = vermelho
                                    Else
                                        FGAna.CellForeColor = vbBlack
                                    End If
                                    Exit For
                                End If
                            Next
                            Exit For
                        End If
                    End If
                Next
            End If
        Next
    Next
    FGAna.MergeCol(0) = True
End Sub

Private Function DevolveSeqUtente(Utente As String, TUtente As String) As Long
    Dim sSql As String
    Dim RsIdentif As New ADODB.recordset
    
    On Error GoTo TrataErro
    
    sSql = " SELECT seq_utente FROM sl_identif WHERE utente = " & BL_TrataStringParaBD(Utente)
    sSql = sSql & " AND t_utente = " & BL_TrataStringParaBD(TUtente)
    
    Set RsIdentif = New ADODB.recordset
    RsIdentif.CursorType = adOpenStatic
    RsIdentif.CursorLocation = adUseServer
    RsIdentif.Open sSql, gConexao, adOpenStatic
    If RsIdentif.RecordCount > 0 Then
        DevolveSeqUtente = BL_HandleNull(RsIdentif!seq_utente, -1)
    Else
        DevolveSeqUtente = -1
    End If
    RsIdentif.Close
    Set RsIdentif = Nothing
    
Exit Function
TrataErro:
    Call BG_LogFile_Erros("-------------------------------------------------------------------------")
    Call BG_LogFile_Erros("FormHistUtente - DevolveSeqUtente: " & Err.Description)
    Call BG_LogFile_Erros(sSql)
    Call BG_LogFile_Erros("-------------------------------------------------------------------------")
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------------------

' CONSTROI CONDICAO PARA TODAS AS ANALISES ESCOLHIDAS

' ----------------------------------------------------------------------------------
Private Function ConstroiCondicaoAnalises() As String
    Dim sSql As String
    Dim i As Integer
        If TotalAnalisesDestino > 0 Then
            sSql = "(" & BL_TrataStringParaBD(EstrutAnalisesDestino(1).cod_ana)
            For i = 2 To TotalAnalisesDestino
                sSql = sSql & ", " & BL_TrataStringParaBD(EstrutAnalisesDestino(i).cod_ana)
            Next
            sSql = sSql & ")"
            ConstroiCondicaoAnalises = sSql
        Else
            ConstroiCondicaoAnalises = ""
        End If
End Function


' ----------------------------------------------------------------------------------

' CONSTROI UM CRITERIO PARA AS ANALISES SIMPLES ESCOLHIDAS

' ----------------------------------------------------------------------------------
Private Function ConstroiCondicaoAnalisesSimples() As String
    Dim sSql As String
    Dim i As Integer
    If TotalAnalisesDestino > 0 Then
        For i = 1 To TotalAnalisesDestino
            If Mid(EstrutAnalisesDestino(i).cod_ana, 1, 1) = "S" Then
                sSql = "(" & BL_TrataStringParaBD(EstrutAnalisesDestino(i).cod_ana)
                Exit For
            End If
        Next
        i = i + 1
        If i < TotalAnalisesDestino Then
            For i = i To TotalAnalisesDestino
                If Mid(EstrutAnalisesDestino(i).cod_ana, 1, 1) = "S" Then
                    sSql = sSql & "," & BL_TrataStringParaBD(EstrutAnalisesDestino(i).cod_ana)
                End If
            Next
        End If
    End If
    If sSql <> "" Then sSql = sSql & ")"
    ConstroiCondicaoAnalisesSimples = sSql
End Function


' ----------------------------------------------------------------------------------

' CONSTROI UM CRITERIO PARA AS ANALISES COMPLEXAS SELECIONADAS

' ----------------------------------------------------------------------------------
Private Function ConstroiCondicaoAnalisesComplexas() As String
    Dim sSql As String
    Dim i As Integer
    If TotalAnalisesDestino > 0 Then
        For i = 1 To TotalAnalisesDestino
            If Mid(EstrutAnalisesDestino(i).cod_ana, 1, 1) = "C" Then
                sSql = "(" & BL_TrataStringParaBD(EstrutAnalisesDestino(i).cod_ana)
                Exit For
            End If
        Next
        i = i + 1
        If i < TotalAnalisesDestino Then
            For i = i To TotalAnalisesDestino
                If Mid(EstrutAnalisesDestino(i).cod_ana, 1, 1) = "C" Then
                    sSql = "," & BL_TrataStringParaBD(EstrutAnalisesDestino(i).cod_ana)
                End If
            Next
        End If
    End If
    If sSql <> "" Then sSql = sSql & ")"
    ConstroiCondicaoAnalisesComplexas = sSql
End Function


' ----------------------------------------------------------------------------------

' CONSTROI UM CRITERIO PARA TODOS OS PERFIS SELECIONADOS

' ----------------------------------------------------------------------------------
Private Function ConstroiCondicaoAnalisesPerfis() As String
    Dim sSql As String
    Dim i As Integer
    If TotalAnalisesDestino > 0 Then
        For i = 1 To TotalAnalisesDestino
            If Mid(EstrutAnalisesDestino(i).cod_ana, 1, 1) = "P" Then
                sSql = "(" & BL_TrataStringParaBD(EstrutAnalisesDestino(i).cod_ana)
                Exit For
            End If
        Next
        i = i + 1
        If i < TotalAnalisesDestino Then
            For i = i To TotalAnalisesDestino
                If Mid(EstrutAnalisesDestino(i).cod_ana, 1, 1) = "P" Then
                    sSql = sSql & "," & BL_TrataStringParaBD(EstrutAnalisesDestino(i).cod_ana)
                End If
            Next
        End If
    End If
    If sSql <> "" Then sSql = sSql & ")"
    ConstroiCondicaoAnalisesPerfis = sSql
End Function


' ----------------------------------------------------------------------------------

' PREENCHE A LISTBOX DE ANALISES DISPONIVEIS MEDIANTE GRUPO TRABALHO, GRUPO ANALISES
' E DESCRICAO INSERIDA NO FILTRO

' ----------------------------------------------------------------------------------
Private Sub PreencheAnalisesOrigem(GrupoAna As String, GrupoTrab As String, descrAna As String)
    Dim sSql As String
    Dim sWhere As String
    Dim sGroupBy As String
    Dim sFrom As String
    Dim rsAnalises As New ADODB.recordset
    Dim analisesExcluidas As String
    
    sWhere = ""
    sFrom = ""
    LimpaEstrutAnalisesOrigem
    analisesExcluidas = ConstroiCondicaoAnalises
    
    If OptSimples = True Then
        ' ------------------------------------------------------
        ' PREENCHE AS ANALISES SIMPLES
        ' ------------------------------------------------------
        If GrupoAna <> "" Then
            sWhere = sWhere & " AND sl_ana_s.gr_ana = " & BL_TrataStringParaBD(GrupoAna)
        End If
        If GrupoTrab <> "" Then
            sFrom = ", sl_ana_trab "
            sWhere = sWhere & " AND cod_gr_trab = " & BL_TrataStringParaBD(GrupoTrab)
            sWhere = sWhere & " AND sl_Ana_trab.cod_analise = sl_ana_s.cod_ana_s "
        End If
        If descrAna <> "" Then
            sWhere = sWhere & " AND upper(descr_ana_s) LIKE '" & UCase(descrAna) & "%'"
        End If
        
        sSql = "SELECT sl_ana_s.cod_ana_s codigo, descr_ana_s descricao, t_result"
        sSql = sSql & " FROM sl_ana_s, sl_realiza, sl_identif, sl_requis " & sFrom
        sSql = sSql & " WHERE  "
        
        sSql = sSql & "  sl_identif.seq_utente = " & SeqUtente & sWhere
        
        sSql = sSql & " AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s"
        sSql = sSql & " AND sl_realiza.dt_chega >='01-jan-1900'" ' isto e para usar um indice(Ficar optimizado)
        'sSql = sSql & " AND sl_realiza.cod_agrup = sl_realiza.cod_ana_S"
        sSql = sSql & " AND sl_identif.seq_utente = sl_requis.seq_utente "
        sSql = sSql & " AND sl_requis.n_req= sl_realiza.n_req "
        sSql = sSql & " AND (sl_ana_s.flg_invisivel IS NULL OR sl_ana_s.flg_invisivel = 0)"
        If analisesExcluidas <> "" Then
            sSql = sSql & " AND sl_ana_s.cod_ana_s NOT IN" & analisesExcluidas
        End If
        
        sSql = sSql & " UNION SELECT sl_ana_s.cod_ana_s codigo, descr_ana_s descricao, t_result"
        sSql = sSql & " FROM sl_ana_s, sl_realiza_h r, sl_identif, sl_requis " & sFrom
        sSql = sSql & " WHERE  "
        
        sSql = sSql & "  sl_identif.seq_utente = " & SeqUtente & sWhere
        
        sSql = sSql & " AND r.cod_ana_s = sl_ana_s.cod_ana_s"
        sSql = sSql & " AND r.dt_chega >='01-jan-1900'" ' isto e para usar um indice(Ficar optimizado)
        'sSql = sSql & " AND sl_realiza.cod_agrup = sl_realiza.cod_ana_S"
        sSql = sSql & " AND sl_identif.seq_utente = sl_requis.seq_utente "
        sSql = sSql & " AND sl_requis.n_req= r.n_req "
        sSql = sSql & " AND (sl_ana_s.flg_invisivel IS NULL OR sl_ana_s.flg_invisivel = 0)"
        If analisesExcluidas <> "" Then
            sSql = sSql & " AND sl_ana_s.cod_ana_s NOT IN" & analisesExcluidas
        End If
        sGroupBy = "  GROUP BY  sl_ana_s.cod_ana_s, descr_ana_s, t_result"
    
    ElseIf OptComplexas = True Then
        ' ------------------------------------------------------
        ' PREENCHE AS ANALISES COMPLEXAS
        ' ------------------------------------------------------
        If GrupoAna <> "" Then
            sWhere = sWhere & " AND gr_ana = " & BL_TrataStringParaBD(GrupoAna)
        End If
        If GrupoTrab <> "" Then
            sFrom = ", sl_ana_trab "
            sWhere = sWhere & " AND cod_gr_trab = " & BL_TrataStringParaBD(GrupoTrab)
            sWhere = sWhere & " AND sl_Ana_trab.cod_analise = sl_ana_c.cod_ana_c "
        End If
        If descrAna <> "" Then
            sWhere = sWhere & " AND upper(descr_ana_c) LIKE '" & UCase(descrAna) & "%'"
        End If
        
        sSql = "SELECT sl_ana_c.cod_ana_c codigo, descr_ana_c descricao, null as t_result"
        sSql = sSql & " FROM sl_ana_c, sl_realiza, sl_identif, sl_requis " & sFrom
        sSql = sSql & " WHERE  (sl_ana_c.flg_invisivel IS NULL OR sl_ana_c.flg_invisivel = 0) "
        sSql = sSql & " AND sl_realiza.cod_agrup = sl_ana_c.cod_ana_c "
        sSql = sSql & " AND sl_identif.seq_utente = " & SeqUtente & sWhere
        sSql = sSql & " AND sl_identif.seq_utente = sl_requis.seq_utente "
        sSql = sSql & " AND sl_requis.n_req= sl_realiza.n_req "
        If analisesExcluidas <> "" Then
            sSql = sSql & " AND sl_ana_c.cod_ana_c NOT IN" & analisesExcluidas
        End If
        If gSGBD = gOracle Then
            sGroupBy = "  GROUP BY  sl_ana_c.cod_ana_c, descr_ana_c, null"
        ElseIf gSGBD = gSqlServer Then
            sGroupBy = "  GROUP BY  sl_ana_c.cod_ana_c, descr_ana_c "
        End If
        
    ElseIf OptPerfis = True Then
        ' ------------------------------------------------------
        ' PREENCHE OS PERFIS
        ' ------------------------------------------------------
        If GrupoAna <> "" Then
            sWhere = sWhere & " AND gr_ana = " & BL_TrataStringParaBD(GrupoAna)
        End If
        If GrupoTrab <> "" Then
            sFrom = ", sl_ana_trab "
            sWhere = sWhere & " AND cod_gr_trab = " & BL_TrataStringParaBD(GrupoTrab)
            sWhere = sWhere & " AND sl_Ana_trab.cod_analise = sl_perfis.cod_perfis "
        End If
        If descrAna <> "" Then
            sWhere = sWhere & " AND upper(descr_perfis) LIKE '" & UCase(descrAna) & "%'"
        End If
        sSql = "SELECT sl_perfis.cod_perfis codigo, descr_perfis descricao, null as t_result"
        sSql = sSql & " FROM sl_perfis, sl_realiza, sl_identif, sl_requis " & sFrom
        sSql = sSql & " WHERE sl_identif.seq_utente = " & SeqUtente
        sSql = sSql & " AND sl_realiza.cod_agrup = sl_perfis.cod_perfis "
        sSql = sSql & " AND sl_identif.seq_utente = sl_requis.seq_utente "
        sSql = sSql & " AND sl_requis.n_req= sl_realiza.n_req "
        sSql = sSql & " AND (sl_perfis.flg_invisivel IS NULL OR sl_perfis.flg_invisivel = 0) " & sWhere
        If analisesExcluidas <> "" Then
            sSql = sSql & " AND sl_perfis.cod_perfis NOT IN" & analisesExcluidas
        End If
        If gSGBD = gOracle Then
            sGroupBy = " GROUP BY sl_perfis.cod_perfis, descr_perfis, null"
        ElseIf gSGBD = gSqlServer Then
            sGroupBy = " GROUP BY sl_perfis.cod_perfis, descr_perfis"
        End If
    End If
    sSql = sSql & sGroupBy
    sSql = sSql & " ORDER BY descricao "
    
    Set rsAnalises = New ADODB.recordset
    rsAnalises.CursorType = adOpenStatic
    rsAnalises.CursorLocation = adUseServer
    rsAnalises.Open sSql, gConexao, adOpenStatic
    
    EcOrigem.Clear
    While Not rsAnalises.EOF
        EcOrigem.AddItem Trim(rsAnalises!Codigo) & Space$(8 - Len(Trim(rsAnalises!Codigo))) & Trim(rsAnalises!descricao)
        PreencheEstrutAnalisesOrigem rsAnalises!Codigo, rsAnalises!descricao
        rsAnalises.MoveNext
    Wend
End Sub



' ----------------------------------------------------------------------------------

' LIMPA AS UNIDADES DA FLEXGRID

' ----------------------------------------------------------------------------------
Private Sub LimpaUnidades()
    Dim idc As Integer
    
    FGAna.TextMatrix(0, 5) = ""
    FGAna.TextMatrix(1, 5) = ""
    FGAna.MergeCol(5) = False
    For idc = 2 To FGAna.rows - 1
        FGAna.TextMatrix(idc, 5) = ""
    Next
End Sub

' ----------------------------------------------------------------------------------

' LIMPA AS VALORES DE REFERENCIA DA FLEXGRID

' ----------------------------------------------------------------------------------
Private Sub LimpaValRef()
    Dim idc As Integer
    FGAna.TextMatrix(0, 4) = ""
    FGAna.TextMatrix(1, 4) = ""
    FGAna.MergeCol(4) = False
    
    For idc = 2 To FGAna.rows - 1
        FGAna.TextMatrix(idc, 4) = ""
    Next
End Sub

' ----------------------------------------------------------------------------------

' PREENCHE AS UNIDADES DA FLEXGRID COM OS VALORES DA ESTRUTURA

' ----------------------------------------------------------------------------------
Private Sub PreencheUnidades()
    Dim indcFGAna As Integer
    Dim indcPerfis As Integer
    Dim indcComplexas As Integer
    Dim indcSimples As Integer
    
    FGAna.TextMatrix(0, 5) = "Unidades"
    For indcFGAna = 2 To FGAna.rows - 1
        If FGAna.TextMatrix(indcFGAna, 1) <> "" Then
            If Mid(FGAna.TextMatrix(indcFGAna, 1), 1, 1) = "S" Then
                For indcPerfis = 1 To totalAna
                    For indcComplexas = 1 To estrutAna(indcPerfis).TotalAnaComplexas
                        For indcSimples = 1 To estrutAna(indcPerfis).EstrutComplexas(indcComplexas).TotalAnaSimples
                            If FGAna.TextMatrix(indcFGAna, 1) = estrutAna(indcPerfis).EstrutComplexas(indcComplexas).EstrutSimples(indcSimples).cod_ana_s Then
                                FGAna.TextMatrix(indcFGAna, 5) = estrutAna(indcPerfis).EstrutComplexas(indcComplexas).EstrutSimples(indcSimples).unidade
                            End If
                        Next
                    Next
                Next
            End If
        Else
            'Exit For
        End If
    Next
End Sub


' ----------------------------------------------------------------------------------

' PREENCHE AS VALORES REFERENCIA DA FLEXGRID COM OS VALORES DA ESTRUTURA

' ----------------------------------------------------------------------------------
Private Sub PreencheValRef()
    Dim indcFGAna As Integer
    Dim indcPerfis As Integer
    Dim indcComplexas As Integer
    Dim indcSimples As Integer
    
    FGAna.TextMatrix(0, 4) = "Val. Ref."
    
    For indcFGAna = 2 To FGAna.rows - 1
        If FGAna.TextMatrix(indcFGAna, 1) <> "" Then
            If Mid(FGAna.TextMatrix(indcFGAna, 1), 1, 1) = "S" Then
                For indcPerfis = 1 To totalAna
                    For indcComplexas = 1 To estrutAna(indcPerfis).TotalAnaComplexas
                        For indcSimples = 1 To estrutAna(indcPerfis).EstrutComplexas(indcComplexas).TotalAnaSimples
                            If FGAna.TextMatrix(indcFGAna, 1) = estrutAna(indcPerfis).EstrutComplexas(indcComplexas).EstrutSimples(indcSimples).cod_ana_s Then
                                FGAna.TextMatrix(indcFGAna, 4) = estrutAna(indcPerfis).EstrutComplexas(indcComplexas).EstrutSimples(indcSimples).val_ref
                            End If
                        Next
                    Next
                Next
            End If
        Else
            'Exit For
        End If
    Next
End Sub


' ----------------------------------------------------------------------------------

' DEVOLVE VALORES DE REFERENCIA PARA UMA DETERMINADA ANALISE

' ----------------------------------------------------------------------------------
Private Function DevolveValRef(cod_ana_s As String) As String
    Dim sSql As String
    Dim rsValRef As New ADODB.recordset
    
    If (CabecalhoResultados1.RetornaSexo) = gT_Masculino Then
        sSql = "SELECT f_ref_min minimo, f_ref_max maximo FROM "
    Else
        sSql = "SELECT f_ref_min minimo, f_ref_max maximo FROM "
    End If
    sSql = sSql & " sl_val_ref, sl_ana_ref "
    sSql = sSql & " WHERE sl_ana_ref.cod_ana_s  =  " & BL_TrataStringParaBD(cod_ana_s)
    sSql = sSql & " AND sl_val_ref.cod_ana_ref = sl_ana_ref.seq_ana_ref "
    
    Set rsValRef = New ADODB.recordset
    rsValRef.CursorType = adOpenStatic
    rsValRef.CursorLocation = adUseServer
    rsValRef.Open sSql, gConexao, adOpenStatic
    If rsValRef.RecordCount = 1 Then
        DevolveValRef = BL_HandleNull(rsValRef!minimo) & " - " & BL_HandleNull(rsValRef!maximo)
    End If
    rsValRef.Close
    Set rsValRef = Nothing
End Function

' ----------------------------------------------------------------------------------

' ????

' ----------------------------------------------------------------------------------

Private Sub FuncaoProcurarAntibioticos(n_req As String, dt_chega As String)
    Dim sSql As String
    Dim rsTSQ As New ADODB.recordset
    Dim RsFLG_TSQ As New ADODB.recordset
    Dim rsMicro As New ADODB.recordset
    sSql = "SELECT sl_res_micro.flg_tsq, sl_res_micro.cod_micro, sl_res_micro.cod_carac_micro, sl_res_micro.flg_ve FROM sl_res_micro, sl_realiza " & _
                   " WHERE sl_res_micro.seq_realiza = sl_realiza.seq_realiza " & _
                   " AND sl_realiza.n_req = " & n_req & _
                   IIf(EcCodMicro.Text <> "", " AND sl_res_micro.cod_micro = " & BL_TrataStringParaBD(EcCodMicro.Text), "")
    
    RsFLG_TSQ.Open sSql, gConexao, adOpenStatic, adLockOptimistic
    
    
     While (Not RsFLG_TSQ.EOF)
        If (BL_HandleNull(RsFLG_TSQ!flg_tsq, "") = "N") Then
            
            rsMicro.Open "SELECT sl_microrg.cod_microrg, sl_microrg.descr_microrg," & _
                         " sl_microrg.abrev_microrg , sl_perfis.cod_produto" & _
                         " FROM sl_res_micro, sl_microrg, sl_realiza, sl_perfis" & _
                         " WHERE sl_realiza.n_req = " & n_req & _
                         " AND sl_realiza.seq_realiza = sl_res_micro.seq_realiza" & _
                         " AND sl_res_micro.cod_micro = sl_microrg.cod_microrg" & _
                         " AND sl_realiza.cod_perfil = sl_perfis.cod_perfis" & _
                         " AND sl_res_micro.flg_tsq = 'N' AND sl_res_micro.cod_micro = " & BL_TrataStringParaBD(RsFLG_TSQ!cod_micro) _
                         , gConexao, adOpenStatic, adLockOptimistic
            If rsMicro.RecordCount > 0 Then
                PreencheEstrutAntibRes n_req, dt_chega, "", BL_HandleNull(rsMicro!cod_microrg, ""), _
                                       BL_HandleNull(rsMicro!descr_microrg, ""), "", BL_HandleNull(rsMicro!cod_produto, ""), _
                                       BL_HandleNull(rsMicro!abrev_microrg, BL_HandleNull(rsMicro!descr_microrg, "")), _
                                       BL_HandleNull(RsFLG_TSQ!cod_carac_micro, ""), BL_HandleNull(RsFLG_TSQ!flg_ve, 0)
            End If
            rsMicro.Close
        Else
            If gSGBD = gOracle Then
                sSql = "SELECT sl_res_tsq.cod_micro, sl_microrg.descr_microrg, sl_antibio.cod_antibio, sl_microrg.abrev_microrg,"
                sSql = sSql & " sl_antibio.abrev_antibio ,sl_perfis.cod_produto, "
                sSql = sSql & " sl_res_tsq.res_sensib || ' '  || sl_res_tsq.cmi res_sensib"
                sSql = sSql & " FROM sl_realiza, sl_res_tsq,sl_res_micro, sl_antibio, sl_microrg, sl_perfis "
                sSql = sSql & " Where sl_realiza.seq_realiza = sl_res_tsq.seq_realiza"
                sSql = sSql & " AND sl_antibio.cod_antibio = sl_res_tsq.cod_antib"
                sSql = sSql & " AND sl_microrg.cod_microrg = sl_res_tsq.cod_micro"
                sSql = sSql & " AND sl_realiza.n_req = " & n_req
                sSql = sSql & " AND sl_realiza.cod_perfil = sl_perfis.cod_perfis "
                sSql = sSql & " AND sl_res_micro.cod_micro = sl_Res_tsq.cod_micro and sl_res_micro.seq_realiza = sl_res_tsq.seq_realiza  "
            ElseIf gSGBD = gSqlServer Then
                sSql = "SELECT sl_res_tsq.cod_micro, sl_microrg.descr_microrg, sl_antibio.cod_antibio, sl_microrg.abrev_microrg,"
                sSql = sSql & " sl_antibio.abrev_antibio ,sl_perfis.cod_produto, "
                sSql = sSql & " sl_res_tsq.res_sensib + ' '  + sl_res_tsq.cmi res_sensib"
                sSql = sSql & " FROM sl_realiza, sl_res_tsq, sl_antibio, sl_microrg, sl_perfis "
                sSql = sSql & " Where sl_realiza.seq_realiza = sl_res_tsq.seq_realiza"
                sSql = sSql & " AND sl_antibio.cod_antibio = sl_res_tsq.cod_antib"
                sSql = sSql & " AND sl_microrg.cod_microrg = sl_res_tsq.cod_micro"
                sSql = sSql & " AND sl_realiza.n_req = " & n_req
                sSql = sSql & " AND sl_realiza.cod_perfil = sl_perfis.cod_perfis "
            End If
            If EcCodProduto <> "" Then
                sSql = sSql & " AND sl_perfis.cod_produto = " & BL_TrataStringParaBD(EcCodProduto)
            End If
            If EcCodMicro <> "" Then
                sSql = sSql & " AND sl_res_tsq.cod_micro = " & BL_TrataStringParaBD(EcCodMicro)
            End If
            sSql = sSql & " ORDER BY sl_antibio.descr_antibio"
            
            rsTSQ.CursorType = adOpenStatic
            rsTSQ.CursorLocation = adUseServer
            rsTSQ.Open sSql, gConexao, adOpenStatic
            
            While Not rsTSQ.EOF
                If rsTSQ.RecordCount > 0 Then
                    PreencheEstrutAntib BL_HandleNull(rsTSQ!cod_antibio, ""), BL_HandleNull(rsTSQ!abrev_antibio, "")
                    PreencheEstrutAntibRes n_req, dt_chega, BL_HandleNull(rsTSQ!cod_antibio, ""), _
                            BL_HandleNull(rsTSQ!cod_micro, ""), BL_HandleNull(rsTSQ!descr_microrg, ""), _
                            BL_HandleNull(rsTSQ!res_sensib, ""), BL_HandleNull(rsTSQ!cod_produto, ""), _
                            BL_HandleNull(rsTSQ!abrev_microrg, BL_HandleNull(rsTSQ!descr_microrg, "")), _
                            BL_HandleNull(RsFLG_TSQ!cod_carac_micro, ""), BL_HandleNull(RsFLG_TSQ!flg_ve, 0)
                            
                End If
                rsTSQ.MoveNext
            Wend
            rsTSQ.Close
        End If
        RsFLG_TSQ.MoveNext
    Wend
    RsFLG_TSQ.Close
End Sub


' ----------------------------------------------------------------------------------

' PREENCHE A ESTRUTURA COM OS RESULTADOS DOS ANTIBIOTICOS AOS MICRORGANISMOS

' ----------------------------------------------------------------------------------
Private Sub PreencheEstrutAntibRes(n_req As String, dt_chega As String, cod_antib As String, cod_micro As String, _
                descr_micro As String, res_sensib As String, cod_produto As String, abrev_micro As String, _
                cod_carac_micro As String, flg_ve As Integer)
    
    TotalAntibRes = TotalAntibRes + 1
    ReDim Preserve EstrutAntibRes(TotalAntibRes)
    
    EstrutAntibRes(TotalAntibRes).n_req = n_req
    EstrutAntibRes(TotalAntibRes).cod_antib = cod_antib
    EstrutAntibRes(TotalAntibRes).cod_micro = cod_micro
    EstrutAntibRes(TotalAntibRes).descr_micro = descr_micro
    EstrutAntibRes(TotalAntibRes).resultado = res_sensib
    EstrutAntibRes(TotalAntibRes).cod_produto = cod_produto
    EstrutAntibRes(TotalAntibRes).abrev_micro = abrev_micro
    EstrutAntibRes(TotalAntibRes).flg_ve = flg_ve
    EstrutAntibRes(TotalAntibRes).cod_carac_micro = cod_carac_micro
    
End Sub



' ----------------------------------------------------------------------------------

' PREENCHE A ESTRUTURA COM OS ANTIBIOTICOS

' ----------------------------------------------------------------------------------
Private Sub PreencheEstrutAntib(cod_antib As String, descr_antib As String)
    Dim indc As Integer
    For indc = 1 To totalAntib
        If EstrutAntib(indc).cod_antib = cod_antib Then
            Exit Sub
        End If
    Next
    
    totalAntib = totalAntib + 1
    ReDim Preserve EstrutAntib(totalAntib)
    
    EstrutAntib(totalAntib).cod_antib = cod_antib
    EstrutAntib(totalAntib).descr_antib = descr_antib
    
End Sub



' ----------------------------------------------------------------------------------

' PREENCHE A GRELHA COM OS VARIOS ANTIBIOTICOS

' ----------------------------------------------------------------------------------
Private Sub PreencheGridAntib()
    Dim indc As Integer
    
    For indc = 1 To totalAntib
        If indc = FgAntib.rows - 3 Then
            FgAntib.rows = FgAntib.rows + 1
        End If
        FgAntib.TextMatrix(indc + 3, 0) = EstrutAntib(indc).descr_antib
        FgAntib.TextMatrix(indc + 3, 1) = EstrutAntib(indc).cod_antib
    Next
End Sub


' ----------------------------------------------------------------------------------

' PREENCHE A GRELHA COM OS RESULTADOS DOS ANTIBIOTICOS

' ----------------------------------------------------------------------------------
Private Sub PreencheGridAntibRes()
    Dim indc_req As Integer
    Dim indc_res As Integer
    Dim indc As Integer
    Dim coluna As Integer
    Dim flgExisteAntib As Boolean
    Dim i, j As Integer
    coluna = 2
    For indc_req = 1 To totalReq
        If coluna = FgAntib.Cols - 1 Then
            FgAntib.Cols = FgAntib.Cols + 1
            FgAntib.ColAlignment(FgAntib.Cols - 1) = flexAlignCenterCenter
            FgAntib.ColWidth(FgAntib.Cols - 1) = 1500
        End If
        FgAntib.TextMatrix(0, coluna) = EstrutReq(indc_req).n_req
        FgAntib.TextMatrix(1, coluna) = EstrutReq(indc_req).dt_chega & " " & EstrutReq(indc_req).hr_chega
        
        For indc_res = 1 To TotalAntibRes
            flgExisteAntib = False
            If FgAntib.TextMatrix(0, coluna) = EstrutAntibRes(indc_res).n_req Then
                For i = 2 To FgAntib.Cols
                    If FgAntib.TextMatrix(0, i) = EstrutAntibRes(indc_res).n_req Then
                        Exit For
                    End If
                Next
                For i = i To FgAntib.Cols
                    If FgAntib.TextMatrix(0, i) = EstrutAntibRes(indc_res).n_req Then
                        If InStr(FgAntib.TextMatrix(2, i), EstrutAntibRes(indc_res).abrev_micro) > 0 Then
                            flgExisteAntib = True
                            Exit For
                        ElseIf FgAntib.TextMatrix(2, i) = "" Then
                            flgExisteAntib = False
                            Exit For
                        End If
                    Else
                        flgExisteAntib = False
                        Exit For
                    End If
                Next
                If flgExisteAntib = True Then
                    For j = 3 To FgAntib.rows
                        If FgAntib.TextMatrix(j, 1) = EstrutAntibRes(indc_res).cod_antib Then
                            FgAntib.TextMatrix(j, i) = EstrutAntibRes(indc_res).resultado
                            Exit For
                        End If
                    Next
                Else
                    If i = FgAntib.Cols - 1 Then
                        FgAntib.Cols = FgAntib.Cols + 1
                        FgAntib.ColAlignment(FgAntib.Cols - 1) = flexAlignCenterCenter
                        FgAntib.ColWidth(FgAntib.Cols - 1) = 1500
                    End If
                    If FgAntib.TextMatrix(0, i) = "" Then
                        FgAntib.TextMatrix(0, i) = FgAntib.TextMatrix(0, i - 1)
                        FgAntib.TextMatrix(1, i) = FgAntib.TextMatrix(1, i - 1)
                        FgAntib.TextMatrix(3, i) = FgAntib.TextMatrix(3, i - 1)
                        FgAntib.row = 2
                        FgAntib.Col = i
                        If EstrutAntibRes(indc_res).flg_ve = True Then
                            Set FgAntib.CellPicture = PicVeAlerta.Image
                        Else
                            Set FgAntib.CellPicture = Nothing
                        End If
                        FgAntib.TextMatrix(2, i) = EstrutAntibRes(indc_res).abrev_micro
                        If EstrutAntibRes(indc_res).cod_carac_micro = "2" Then
                            FgAntib.TextMatrix(2, i) = FgAntib.TextMatrix(2, i) & "(Dup.)"
                        End If
                        For j = 4 To FgAntib.rows - 1
                            If FgAntib.TextMatrix(j, 1) = EstrutAntibRes(indc_res).cod_antib Then
                                FgAntib.TextMatrix(j, i) = EstrutAntibRes(indc_res).resultado
                                Exit For
                            End If
                        Next
                    ElseIf FgAntib.TextMatrix(2, i) = "" Then
                        FgAntib.row = 2
                        FgAntib.Col = i
                        If EstrutAntibRes(indc_res).flg_ve = mediSim Then
                            Set FgAntib.CellPicture = PicVeAlerta.Image
                        Else
                            Set FgAntib.CellPicture = Nothing
                        End If
                        FgAntib.TextMatrix(2, i) = EstrutAntibRes(indc_res).abrev_micro
                        If EstrutAntibRes(indc_res).cod_carac_micro = "2" Then
                            FgAntib.TextMatrix(2, i) = FgAntib.TextMatrix(2, i) & "(Dup.)"
                        End If
                        FgAntib.TextMatrix(3, i) = EstrutAntibRes(indc_res).cod_produto
                        FgAntib.MergeRow(3) = True
                        For j = 4 To FgAntib.rows - 1
                            If FgAntib.TextMatrix(j, 1) = EstrutAntibRes(indc_res).cod_antib Then
                                FgAntib.TextMatrix(j, i) = EstrutAntibRes(indc_res).resultado
                                Exit For
                            End If
                        Next
                    End If
                    coluna = i
                End If
            End If
        Next
        coluna = coluna + 1
    Next
    FgAntib.TextMatrix(0, 0) = " "
    FgAntib.TextMatrix(1, 0) = " "
    FgAntib.TextMatrix(2, 0) = " "
    FgAntib.TextMatrix(3, 0) = " "
    FgAntib.MergeCol(0) = True
    FgAntib.MergeRow(0) = True
    FgAntib.MergeRow(1) = True

End Sub

' ----------------------------------------------------------------------------------

' RETORNA NUMERO DE LINHAS NECESSARIAS PARA CADA ANALISE

' ----------------------------------------------------------------------------------
Private Function TotalAnaPorRequisicao(cod_ana_s As String, cod_ana_c As String, cod_perfis As String) As Integer
    Dim indc_anaRes As Integer
    Dim indc_req As Integer
    Dim conta As Integer
    Dim maximo As Integer
    maximo = 0
    For indc_req = 1 To totalReq
        conta = 0
        For indc_anaRes = 1 To TotalAnaRes
            If EstrutAnaRes(indc_anaRes).n_req = EstrutReq(indc_req).n_req Then
                If cod_ana_s = EstrutAnaRes(indc_anaRes).cod_ana_s And cod_ana_c = EstrutAnaRes(indc_anaRes).cod_ana_c And cod_perfis = EstrutAnaRes(indc_anaRes).cod_perfis Then
                    conta = conta + 1
                    If conta > maximo Then
                        maximo = conta
                    End If
                End If
            End If
        Next
    Next
    TotalAnaPorRequisicao = maximo
End Function



' ----------------------------------------------------------------------------------

' CONTROI O CRITERIO DE SELE��O BASEADO NOS PARAMETROS PREENCHIDOS,
' GR TRABALHO, GR ANALISES, DATAS, ANALISES, PRODUTO, MICRORGANISMO

' ----------------------------------------------------------------------------------
Private Function ConstroiCriterioREQUIS() As String
    Dim ana_s As String
    Dim ana_c As String
    Dim perfis As String
    Dim sSql As String
    Dim sSql_H As String
    
    ' -----------------------------------------------------------
    ' CONSTROI O CRITERIO DE PESQUISA DAS ANALISES INDICADAS
    ' -----------------------------------------------------------
    ana_s = ConstroiCondicaoAnalisesSimples
    ana_c = ConstroiCondicaoAnalisesComplexas
    perfis = ConstroiCondicaoAnalisesPerfis
    analises = ""
    
    ' -----------------------------------------------------------
    ' APENAS RESTRINGE AS ANALISES SE FOR ALGUMA SELECIONADA.
    ' SE N�O FOR NENHUMA PESQUISA TODAS!
    ' -----------------------------------------------------------
    If TotalAnalisesDestino > 0 Then
        If ana_s <> "" Then
            analises = "( sl_ana_s.cod_ana_s IN " & ana_s
        End If
        If ana_c <> "" And analises <> "" Then
            analises = analises & " OR cod_ana_c IN " & ana_c
        ElseIf ana_c <> "" And analises = "" Then
            analises = "( sl_ana_c.cod_ana_c IN " & ana_c
        End If
        If perfis <> "" And analises <> "" Then
            analises = analises & " OR cod_perfis IN " & perfis
        ElseIf perfis <> "" And analises = "" Then
            analises = "( sl_perfis.cod_perfil IN " & perfis
        End If
        analises = analises & ")"
    End If
    ' ---------------------------------------------------------------
    ' PESQUISA REQUISI��ES COM AS ANALISES INDICADAS
    ' ---------------------------------------------------------------
    sSql = "(SELECT sl_requis.n_req, sl_requis.dt_chega, sl_requis.estado_req, sl_requis.hr_chega "
    sSql_H = " UNION (SELECT sl_requis.n_req, sl_requis.dt_chega, sl_requis.estado_req, sl_requis.hr_chega "
    ' ---------------------------------------------------------------
    ' SE PESQUISA � DE ANTIBIOTICOS
    ' ---------------------------------------------------------------
    If TipoPesquisa = micro Then
        sSql = sSql & ", sl_perfis.cod_produto  "
        sSql_H = sSql_H & ", sl_perfis.cod_produto  "
    End If
    
    sSql = sSql & " FROM sl_realiza, sl_requis  "
    sSql_H = sSql_H & " FROM sl_realiza_h, sl_requis  "
    
    ' ---------------------------------------------------------------
    ' SE PESQUISA � DE ANTIBIOTICOS
    ' ---------------------------------------------------------------
    If TipoPesquisa = micro Then
        sSql = sSql & ", sl_perfis, sl_res_micro "
        sSql_H = sSql_H & ", sl_perfis, sl_res_micro_h "
    End If
    ' ---------------------------------------------------------------
    ' SE PREENCHEU GRUPO DE ANALISES OU GRUPO DE TRABALHO
    ' ---------------------------------------------------------------
    If EcGrAnalises <> "" Then
        sSql = sSql & ", sl_ana_s, sl_ana_c "
        sSql_H = sSql_H & ", sl_ana_s, sl_ana_c "
    ElseIf analises <> "" Then
        sSql = sSql & ", sl_ana_s "
        sSql_H = sSql_H & ", sl_ana_s "
    End If
    
    If EcGrAnalises <> "" And EcCodGrTrab <> "" Then
        sSql = sSql & ", sl_ana_trab "
        sSql_H = sSql_H & ", sl_ana_trab "
    ElseIf EcGrAnalises = "" And EcCodGrTrab <> "" Then
        sSql_H = sSql_H & ",sl_ana_s, sl_ana_trab "
    End If
    
    
    sSql = sSql & " WHERE  "
    sSql = sSql & " sl_requis.n_req = sl_realiza.n_req AND sl_requis.seq_utente = " & SeqUtente
    sSql = sSql & " AND sl_requis.dt_chega IS NOT NULL "
    sSql = sSql & " AND (sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtInicio) & " AND " & BL_TrataDataParaBD(EcDtFim) & ")"
    
    sSql_H = sSql_H & " WHERE  "
    sSql_H = sSql_H & " sl_requis.n_req = sl_realiza_h.n_req AND sl_requis.seq_utente = " & SeqUtente
    sSql_H = sSql_H & " AND sl_requis.dt_chega IS NOT NULL "
    sSql_H = sSql_H & " AND (sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtInicio) & " AND " & BL_TrataDataParaBD(EcDtFim) & ")"
    ' ---------------------------------------------------------------
    ' SE PREENCHEU GRUPO DE ANALISES OU GRUPO DE TRABALHO
    ' ---------------------------------------------------------------
    If EcGrAnalises <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = sl_realiza.cod_ana_s AND sl_ana_s.gr_ana = " & BL_TrataStringParaBD(EcGrAnalises)
        sSql_H = sSql_H & " AND sl_ana_s.cod_ana_s = sl_realiza_h.cod_ana_s AND sl_ana_s.gr_ana = " & BL_TrataStringParaBD(EcGrAnalises)
    ElseIf analises <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = sl_realiza.cod_ana_s "
        sSql_H = sSql_H & " AND sl_ana_s.cod_ana_s = sl_realiza_h.cod_ana_s "
    End If
    If EcGrAnalises <> "" And EcCodGrTrab <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = sl_ana_trab.cod_analise AND sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
        sSql_H = sSql_H & " AND sl_ana_s.cod_ana_s = sl_ana_trab.cod_analise AND sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
    ElseIf EcGrAnalises = "" And EcCodGrTrab <> "" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s = sl_ana_trab.cod_analise AND sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
        sSql = sSql & " AND sl_ana_s.cod_ana_s = sl_realiza.cod_ana_s "
    
        sSql_H = sSql_H & " AND sl_ana_s.cod_ana_s = sl_ana_trab.cod_analise AND sl_ana_trab.cod_gr_trab = " & BL_TrataStringParaBD(EcCodGrTrab)
        sSql_H = sSql_H & " AND sl_ana_s.cod_ana_s = sl_realiza.cod_ana_s "
    End If
    ' ---------------------------------------------------------------
    ' SE RESTRINGIU A PESQUISA A ALGUMA ANALISE SELECCIONADA
    ' ---------------------------------------------------------------
    If analises <> "" Then
        sSql = sSql & " AND " & analises
        sSql_H = sSql_H & " AND " & analises
    End If
    ' ---------------------------------------------------------------
    ' SE PESQUISA � DE ANTIBIOTICOS
    ' ---------------------------------------------------------------
    If TipoPesquisa = micro Then
        sSql = sSql & " AND sl_realiza.seq_realiza = sl_res_micro.seq_realiza "
        sSql = sSql & " AND sl_realiza.cod_perfil = sl_perfis.cod_perfis "
    
        sSql_H = sSql_H & " AND sl_realiza_h.seq_realiza = sl_res_micro_h.seq_realiza "
        sSql_H = sSql_H & " AND sl_realiza_h.cod_perfil = sl_perfis.cod_perfis "
    End If
    ' ---------------------------------------------------------------
    ' SE INDICOU UM PRODUTO PARA O ANTIBIOGRAMA
    ' ---------------------------------------------------------------
    If TipoPesquisa = micro And EcCodProduto <> "" Then
        sSql = sSql & " AND sl_perfis.cod_produto = " & BL_TrataStringParaBD(EcCodProduto)
        sSql_H = sSql_H & " AND sl_perfis.cod_produto = " & BL_TrataStringParaBD(EcCodProduto)
    End If
    If TipoPesquisa = micro And EcCodMicro <> "" Then
        sSql = sSql & " AND sl_res_micro.cod_micro = " & BL_TrataStringParaBD(EcCodMicro)
        sSql_H = sSql_H & " AND sl_res_micro_h.cod_micro = " & BL_TrataStringParaBD(EcCodMicro)
    End If
        
    If TipoPesquisa = micro Then
        sSql = sSql & " GROUP BY sl_requis.n_req, sl_requis.dt_chega, sl_requis.estado_req, sl_requis.hr_chega, sl_perfis.cod_produto)  "
        sSql_H = sSql_H & " GROUP BY sl_requis.n_req, sl_requis.dt_chega, sl_requis.estado_req, sl_requis.hr_chega, sl_perfis.cod_produto) "
    Else
        sSql = sSql & " GROUP BY sl_requis.n_req, sl_requis.dt_chega, sl_requis.estado_req, sl_requis.hr_chega) "
        sSql_H = sSql_H & " GROUP BY sl_requis.n_req, sl_requis.dt_chega, sl_requis.estado_req, sl_requis.hr_chega)"
    End If
    If gSGBD = gOracle Then
        sSql = sSql & sSql_H & " ORDER BY dt_chega DESC, hr_chega DESC  "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & sSql_H & " ORDER BY sl_requis.dt_chega DESC, sl_requis.hr_chega DESC "
    End If
    ConstroiCriterioREQUIS = sSql
End Function



' -----------------------------------------------------------------------------------

' RETORNA DATA DA PRIMEIRA REQUISI��O PARA O DOENTE EM CAUSA PARA PREENCHER DT_INICIO

' -----------------------------------------------------------------------------------
Private Function RetornaDtPrimeiraReq()
    Dim sSql As String
    Dim RsRequis As New ADODB.recordset
    
    sSql = "SELECT Min(dt_chega) minimo from sl_requis where dt_chega is not null and seq_utente = " & SeqUtente
    Set RsRequis = New ADODB.recordset
    RsRequis.CursorType = adOpenStatic
    RsRequis.CursorLocation = adUseServer
    RsRequis.Open sSql, gConexao, adOpenStatic
    If RsRequis.RecordCount > 0 Then
        RetornaDtPrimeiraReq = RsRequis!minimo
    Else
        RetornaDtPrimeiraReq = Bg_DaData_ADO
    End If
    RsRequis.Close
    Set RsRequis = Nothing
End Function

Private Sub CoresFg()
    Dim linha As Integer
    Dim coluna As Integer
    Dim cor As Long
    linha = 3
        For linha = 3 To FGAna.rows - 1
            coluna = 0
            FGAna.Col = coluna
            FGAna.row = linha
            cor = FGAna.CellBackColor
            For coluna = 1 To FGAna.Cols - 1
                FGAna.Col = coluna
                FGAna.CellBackColor = cor
            Next
        Next
        FGAna.Col = 0
        FGAna.row = 0
        FGAna.topRow = 3
        FGAna.LeftCol = 1
End Sub


Private Sub ImprimeGrafico(linha As Integer)
    
    Dim valores As Variant
    Dim Datas As Variant
    Dim Requis As Variant
    
    Dim linhas As Integer
    Dim colunas As Integer
    Dim i As Integer
    Dim j As Integer
    
    
    If FGAna.TextMatrix(linha, 1) = "" Then
        Exit Sub
    End If
    'Inicializa as propriedades do Layout
    Load FormGrafico
    
    '_____________________________________________________________________________________________________
    
    With FormGrafico.MSChart1
        'DADOS:
        FormGrafico.FormName = Me.Name
        .RandomFill = False
        
        'N� de linhas e colunas da Matriz
        ReDim valores(0)
        ReDim Datas(0)
        ReDim Requis(0)
        j = 0
        For i = 1 To TotalAnaRes
            If EstrutAnaRes(i).cod_ana_s = FGAna.TextMatrix(linha, 1) Then
                j = j + 1
                If j > UBound(valores) + 1 And EstrutAnaRes(i).n_req <> "" Then
                    ReDim Preserve valores(j)
                    ReDim Preserve Datas(j)
                    ReDim Preserve Requis(j)
                End If
                valores(j - 1) = EstrutAnaRes(i).resultado
                Datas(j - 1) = EstrutAnaRes(i).dt_chega
                Requis(j - 1) = EstrutAnaRes(i).n_req
            End If
        Next
            j = j - 1
            ReDim Preserve valores(j)
            ReDim Preserve Datas(j)
            ReDim Preserve Requis(j)
        
        linhas = UBound(Datas) + 1
        colunas = 1
        
        .RowCount = linhas
        .ColumnCount = colunas
        
        'Quando se atribui valores ao gr�fico (.data) incrementa a linha na coluna activa
        'ou a coluna se a linha da coluna for a �ltima
        .AutoIncrement = False
    
        For j = 1 To colunas
            .Column = j
            For i = 1 To linhas
                .row = i
                .data = BL_String2Double(valores(i - 1))
            Next i
        Next j
        '.ChartData = Matriz
        
        'LABELS (S� DEPOIS DA ATRIBUI��O=>1 n�vel de indenta��o por Label!)
        For i = 1 To linhas
            .Column = 1
            .row = i
            .RowLabelCount = 2
            .RowLabelIndex = 1
            .RowLabel = Datas(i - 1)
            .RowLabelIndex = 2
            .RowLabel = "Req. " & Requis(i - 1)
        Next i
        
        .Column = 1
        .ColumnLabelCount = 1
        .ColumnLabelIndex = 1
        .ColumnLabel = FGAna.TextMatrix(linha, 0)
        
        .title.Text = gDUtente.nome_ute & vbNewLine & "EVOLU��O DE RESULTADOS - " & FGAna.TextMatrix(linha, 0) & ":"
         
        With .Plot

            .DataSeriesInRow = False
            With .SeriesCollection
                '1� S�rie
                With .item(1)
                    .SeriesMarker.Show = False
                    .SeriesMarker.Auto = True
                    .ShowLine = True
                    With .Pen
                        .Style = VtPenStyleSolid
                        .Cap = VtPenCapRound
                        .Limit = 10
                        .Join = VtPenJoinBevel
                        .VtColor.Set 0, 0, 0
                    End With
                End With
            End With
            
        End With
            
    End With
    
    '_____________________________________________________________________________________________________
    
    Me.Visible = False
    FormGrafico.Show
    
End Sub

Private Sub PreencheTabelaTemporaria()
    Dim i As Integer
    Dim sSql As String
    Dim dt_chega As String
    Dim descr_produtos As String
    Dim descr_ana_s As String
    Dim Descr_Ana_C As String
    Dim descr_perfis As String
    For i = 1 To TotalAnaRes
        dt_chega = ""
        descr_produtos = ""
        Call DevolveDadosRequisicao(EstrutAnaRes(i).n_req, dt_chega, descr_produtos)
        DevolveDadosAnalises EstrutAnaRes(i).cod_perfis, EstrutAnaRes(i).cod_ana_c, EstrutAnaRes(i).cod_ana_s, descr_perfis, Descr_Ana_C, descr_ana_s
        
        sSql = " INSERT INTO sl_cr_hist_utente( n_req, dt_chega,descr_produtos,cod_ana_s, descr_ana_s, cod_ana_c, descr_ana_c, "
        sSql = sSql & " cod_perfil, descr_perfil, resultado, nome_computador, ordem) VALUES ("
        sSql = sSql & EstrutAnaRes(i).n_req & ", " & BL_TrataDataParaBD(dt_chega) & ", " & BL_TrataStringParaBD(descr_produtos) & " , "
        sSql = sSql & BL_TrataStringParaBD(EstrutAnaRes(i).cod_ana_s) & ", " & BL_TrataStringParaBD(descr_ana_s) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutAnaRes(i).cod_ana_c) & ", " & BL_TrataStringParaBD(Descr_Ana_C) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutAnaRes(i).cod_perfis) & ", " & BL_TrataStringParaBD(descr_perfis) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutAnaRes(i).resultado) & ", " & BL_TrataStringParaBD(gComputador) & "," & EstrutAnaRes(i).linha & ")"
        BG_ExecutaQuery_ADO sSql
    Next
    
End Sub

Private Sub DevolveDadosRequisicao(n_req As String, ByRef dt_chega As String, ByRef descr_produtos As String)
    Dim i As Integer
    For i = 1 To totalReq
        If EstrutReq(i).n_req = n_req Then
            dt_chega = EstrutReq(i).dt_chega
            descr_produtos = EstrutReq(i).cod_produto
            Exit Sub
        End If
    Next
End Sub

Private Sub DevolveDadosAnalises(Cod_Perfil As String, cod_ana_c As String, cod_ana_s As String, ByRef Descr_Perfil, _
                                 ByRef Descr_Ana_C As String, ByRef descr_ana_s As String)
    Dim i_p As Long
    Dim i_c As Long
    Dim i_s As Long
    
    For i_p = 1 To totalAna
        If estrutAna(i_p).cod_perfis = Cod_Perfil Then
            Descr_Perfil = estrutAna(i_p).descr_perfis
            For i_c = 1 To estrutAna(i_p).TotalAnaComplexas
                If estrutAna(i_p).EstrutComplexas(i_c).cod_ana_c = cod_ana_c Then
                    Descr_Ana_C = estrutAna(i_p).EstrutComplexas(i_c).Descr_Ana_C
                    For i_s = 1 To estrutAna(i_p).EstrutComplexas(i_c).TotalAnaSimples
                        If estrutAna(i_p).EstrutComplexas(i_c).EstrutSimples(i_s).cod_ana_s = cod_ana_s Then
                            descr_ana_s = estrutAna(i_p).EstrutComplexas(i_c).EstrutSimples(i_s).descr_ana_s
                        End If
                    Next
                End If
            Next
        End If
    Next
End Sub


Public Sub ProcuraAnalise(codAnalise As String)
    Dim i As Integer
    If codAnalise = "" Then
        BtAna_Click
    Else
        If Mid(codAnalise, 1, 1) = "S" Then
            FormHistUtente.OptSimples.value = True
        ElseIf Mid(codAnalise, 1, 1) = "C" Then
            FormHistUtente.OptComplexas.value = True
        ElseIf Mid(codAnalise, 1, 1) = "P" Then
            FormHistUtente.OptPerfis.value = True
        End If
        For i = 1 To TotalAnalisesOrigem
            If codAnalise = EstrutAnalisesOrigem(i).cod_ana Then
                EcOrigem.ListIndex = i - 1
                BtAdiciona_Click
                BtAna_Click
                Exit For
            End If
        Next
    End If
    
End Sub


Private Function VerificaObsANa(seq_realiza As Long) As String
    Dim sSql As String
    Dim RsRequis As New ADODB.recordset
    
    VerificaObsANa = ""
    sSql = "SELECT * FROM sl_obs_ana WHERE seq_realiza = " & seq_realiza
    Set RsRequis = New ADODB.recordset
    RsRequis.CursorType = adOpenStatic
    RsRequis.CursorLocation = adUseServer
    RsRequis.Open sSql, gConexao, adOpenStatic
    If RsRequis.RecordCount > 0 Then
        VerificaObsANa = BL_HandleNull(RsRequis!descr_obs_ana, "")
    End If
    RsRequis.Close
End Function

Private Function DevolveNomeFicheiro() As String
    DevolveNomeFicheiro = ""
    
    
    NomeFicheiro.FilterIndex = 1
    NomeFicheiro.CancelError = True
    NomeFicheiro.InitDir = gDirCliente & "\"
    NomeFicheiro.FileName = ""
    On Error Resume Next
    NomeFicheiro.DialogTitle = "Nome do ficheiro"
    NomeFicheiro.ShowSave
    
    DevolveNomeFicheiro = NomeFicheiro.FileName

End Function


VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormGestaoRequisicaoVet 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormGestaoRequisicaoVet"
   ClientHeight    =   1.11105e5
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   14625
   Icon            =   "FormGestaoRequisicaoVet.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1.11105e5
   ScaleWidth      =   14625
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcNumColheita 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000040C0&
      Height          =   315
      Left            =   11640
      Locked          =   -1  'True
      TabIndex        =   278
      Top             =   10320
      Width           =   1215
   End
   Begin VB.TextBox EcFlgDiagSec 
      Height          =   285
      Left            =   3480
      TabIndex        =   269
      Top             =   12240
      Width           =   375
   End
   Begin VB.Frame Frame7 
      Height          =   700
      Index           =   1
      Left            =   13200
      TabIndex        =   266
      ToolTipText     =   "Gravar"
      Top             =   6000
      Width           =   1335
      Begin VB.CommandButton BtGravar 
         Height          =   525
         Left            =   840
         Picture         =   "FormGestaoRequisicaoVet.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   268
         ToolTipText     =   "Gravar"
         Top             =   120
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Gravar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   267
         ToolTipText     =   "Gravar"
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.TextBox EcUtilConf 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   261
      Top             =   18840
      Width           =   615
   End
   Begin VB.TextBox EcHrConf 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   6480
      Locked          =   -1  'True
      TabIndex        =   260
      Top             =   18840
      Width           =   735
   End
   Begin VB.TextBox EcDtConf 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   5520
      Locked          =   -1  'True
      TabIndex        =   259
      Top             =   18840
      Width           =   975
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2535
      Left            =   120
      TabIndex        =   53
      Top             =   -120
      Width           =   14415
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   13800
         Picture         =   "FormGestaoRequisicaoVet.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   277
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   720
         Width           =   375
      End
      Begin VB.ComboBox CbTipoIsencao 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   275
         Top             =   1920
         Width           =   1935
      End
      Begin VB.TextBox EcDescrEspecie 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9480
         Locked          =   -1  'True
         TabIndex        =   272
         TabStop         =   0   'False
         Top             =   1440
         Width           =   4335
      End
      Begin VB.TextBox EcDescrMedico 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1200
         TabIndex        =   4
         Top             =   1080
         Width           =   4695
      End
      Begin VB.CommandButton BtPesquisaMedico 
         Height          =   315
         Left            =   5520
         Picture         =   "FormGestaoRequisicaoVet.frx":1260
         Style           =   1  'Graphical
         TabIndex        =   250
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de M�dicos "
         Top             =   1080
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox EcNomeMedico 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   265
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   247
         TabStop         =   0   'False
         Top             =   1080
         Visible         =   0   'False
         Width           =   3375
      End
      Begin VB.TextBox EcDataChegada 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   2
         Top             =   720
         Width           =   1095
      End
      Begin VB.TextBox EcNomeDono 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9480
         Locked          =   -1  'True
         TabIndex        =   265
         TabStop         =   0   'False
         Top             =   1800
         Width           =   4335
      End
      Begin VB.CommandButton BtCancelarReq 
         Height          =   315
         Left            =   5400
         Picture         =   "FormGestaoRequisicaoVet.frx":17EA
         Style           =   1  'Graphical
         TabIndex        =   252
         TabStop         =   0   'False
         ToolTipText     =   "Raz�o cancelamento"
         Top             =   240
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.ComboBox CbDestino 
         Height          =   315
         Left            =   3960
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1440
         Width           =   1935
      End
      Begin VB.ComboBox CbUrgencia 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1440
         Width           =   1935
      End
      Begin VB.TextBox EcNumReqAssoc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H002C7124&
         Height          =   315
         Left            =   4800
         TabIndex        =   3
         Top             =   720
         Width           =   1095
      End
      Begin VB.TextBox EcDescrEstado 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   253
         Top             =   240
         Width           =   3255
      End
      Begin VB.TextBox EcNumReq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H002C7124&
         Height          =   315
         Left            =   1200
         TabIndex        =   1
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton BtGetSONHO 
         Enabled         =   0   'False
         Height          =   315
         Left            =   5400
         MaskColor       =   &H00C0C0FF&
         Picture         =   "FormGestaoRequisicaoVet.frx":1E74
         Style           =   1  'Graphical
         TabIndex        =   251
         ToolTipText     =   "Importar do SONHO"
         Top             =   240
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.TextBox EcCodMedico 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   265
         Left            =   1200
         TabIndex        =   248
         Top             =   1080
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.TextBox EcDescrEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10440
         Locked          =   -1  'True
         TabIndex        =   245
         TabStop         =   0   'False
         Top             =   720
         Width           =   3375
      End
      Begin VB.TextBox EcCodEFR 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9480
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   720
         Width           =   975
      End
      Begin VB.ComboBox CbTipoUtente 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   9480
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9480
         Locked          =   -1  'True
         TabIndex        =   56
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   2160
         Width           =   1095
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10560
         TabIndex        =   8
         Top             =   240
         Width           =   3255
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9480
         Locked          =   -1  'True
         TabIndex        =   55
         TabStop         =   0   'False
         Top             =   1080
         Width           =   4335
      End
      Begin VB.TextBox EcIdade 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   10560
         Locked          =   -1  'True
         TabIndex        =   54
         Top             =   2160
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Factura��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   120
         TabIndex        =   276
         Top             =   1920
         Width           =   855
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Esp�cie"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   8520
         TabIndex        =   273
         Top             =   1440
         Width           =   570
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Dono"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   8520
         TabIndex        =   264
         Top             =   1800
         Width           =   375
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Destino"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   3240
         TabIndex        =   258
         ToolTipText     =   "Data de Nascimento"
         Top             =   1440
         Width           =   540
      End
      Begin VB.Label Label1 
         Caption         =   "Dt &chegada"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   257
         Top             =   780
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Urg�ncia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   256
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label LbNumero 
         AutoSize        =   -1  'True
         Caption         =   "N�m.Anterior"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   1
         Left            =   3720
         TabIndex        =   255
         Top             =   720
         Width           =   945
      End
      Begin VB.Label LbNumero 
         AutoSize        =   -1  'True
         Caption         =   "&Requisi��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   120
         TabIndex        =   254
         Top             =   240
         Width           =   945
      End
      Begin VB.Label Label1 
         Caption         =   "&M�dico"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   249
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label Label10 
         Caption         =   "Entidade"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   8520
         TabIndex        =   246
         Top             =   720
         Width           =   615
      End
      Begin VB.Label LbData 
         AutoSize        =   -1  'True
         Caption         =   "Data Nasc."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   8520
         TabIndex        =   59
         ToolTipText     =   "Data de Nascimento"
         Top             =   2160
         Width           =   795
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   8520
         TabIndex        =   58
         Top             =   240
         Width           =   465
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   8520
         TabIndex        =   57
         Top             =   1080
         Width           =   405
      End
   End
   Begin VB.Frame Frame7 
      Height          =   600
      Index           =   0
      Left            =   13200
      TabIndex        =   238
      ToolTipText     =   "Consulta de Requisi��es"
      Top             =   5400
      Width           =   1335
      Begin VB.CommandButton BtConsReq 
         Enabled         =   0   'False
         Height          =   480
         Left            =   840
         Picture         =   "FormGestaoRequisicaoVet.frx":2186
         Style           =   1  'Graphical
         TabIndex        =   239
         ToolTipText     =   "Consulta de Requisi��es"
         Top             =   110
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Consulta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   16
         Left            =   120
         TabIndex        =   240
         ToolTipText     =   "Consulta de Requisi��es"
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame4 
      Height          =   600
      Left            =   13200
      TabIndex        =   235
      ToolTipText     =   "Informa��o Cl�nica do Utente"
      Top             =   4800
      Width           =   1335
      Begin VB.CommandButton BtInfClinica 
         Enabled         =   0   'False
         Height          =   480
         Left            =   840
         Picture         =   "FormGestaoRequisicaoVet.frx":2510
         Style           =   1  'Graphical
         TabIndex        =   236
         ToolTipText     =   "Informa��o Cl�nica do Utente"
         Top             =   110
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Inf. C&l�nica"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   237
         ToolTipText     =   "Informa��o Cl�nica do Utente"
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Frame3 
      Height          =   600
      Left            =   13200
      TabIndex        =   231
      ToolTipText     =   "Notas Associadas � Requisi��o"
      Top             =   4200
      Width           =   1335
      Begin VB.CommandButton BtNotasVRM 
         Enabled         =   0   'False
         Height          =   480
         Left            =   840
         Picture         =   "FormGestaoRequisicaoVet.frx":2A9A
         Style           =   1  'Graphical
         TabIndex        =   233
         ToolTipText     =   "Notas"
         Top             =   120
         Width           =   495
      End
      Begin VB.CommandButton BtNotas 
         Enabled         =   0   'False
         Height          =   480
         Left            =   840
         Picture         =   "FormGestaoRequisicaoVet.frx":3204
         Style           =   1  'Graphical
         TabIndex        =   232
         ToolTipText     =   "Notas"
         Top             =   110
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Notas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   14
         Left            =   120
         TabIndex        =   234
         ToolTipText     =   "Notas"
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame5 
      Height          =   600
      Left            =   13200
      TabIndex        =   228
      ToolTipText     =   "Impress�o de Etiquetas"
      Top             =   3600
      Width           =   1335
      Begin VB.CommandButton BtFichaCruzada 
         Enabled         =   0   'False
         Height          =   480
         Left            =   840
         Picture         =   "FormGestaoRequisicaoVet.frx":396E
         Style           =   1  'Graphical
         TabIndex        =   229
         ToolTipText     =   "Impress�o de Etiquetas"
         Top             =   110
         Width           =   495
      End
      Begin VB.Label LaFichaCruz 
         Caption         =   "Etiquetas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   230
         ToolTipText     =   "Etiquetas"
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame9 
      Height          =   600
      Left            =   13200
      TabIndex        =   225
      ToolTipText     =   "Identifica��o"
      Top             =   2400
      Width           =   1335
      Begin VB.CommandButton BtIdentif 
         Enabled         =   0   'False
         Height          =   480
         Left            =   840
         Picture         =   "FormGestaoRequisicaoVet.frx":40D8
         Style           =   1  'Graphical
         TabIndex        =   226
         ToolTipText     =   "Identifica��o"
         Top             =   110
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Identif."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   227
         ToolTipText     =   "Identifica��o"
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame10 
      Height          =   600
      Left            =   13200
      TabIndex        =   222
      ToolTipText     =   "Resumo"
      Top             =   3000
      Width           =   1335
      Begin VB.CommandButton BtResumo 
         Height          =   480
         Left            =   840
         Picture         =   "FormGestaoRequisicaoVet.frx":4DA2
         Style           =   1  'Graphical
         TabIndex        =   223
         ToolTipText     =   "Folha Resumo"
         Top             =   120
         Width           =   495
      End
      Begin VB.Label Label6 
         Caption         =   "Resumo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   224
         ToolTipText     =   "Folha Resumo"
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   8655
      Left            =   240
      TabIndex        =   172
      Top             =   13920
      Visible         =   0   'False
      Width           =   12735
      Begin VB.TextBox EcSexo 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1440
         TabIndex        =   243
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   4680
         Width           =   1935
      End
      Begin VB.CheckBox CkAvisarSMS 
         Caption         =   "Enviar SMS quando resultados completos"
         Height          =   255
         Left            =   2640
         TabIndex        =   242
         Top             =   5520
         Width           =   3375
      End
      Begin VB.CheckBox CkReqHipocoagulados 
         Caption         =   "Requisi��o de hipocoagulados"
         Height          =   255
         Left            =   0
         TabIndex        =   241
         Top             =   5520
         Width           =   2535
      End
      Begin VB.Frame FrameDig 
         Height          =   600
         Left            =   9600
         TabIndex        =   219
         ToolTipText     =   "Domicilio"
         Top             =   5160
         Width           =   1335
         Begin VB.CommandButton BtDigitalizacao 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoVet.frx":550C
            Style           =   1  'Graphical
            TabIndex        =   220
            ToolTipText     =   "Digitalizar Documento"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Digitalizar"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   31
            Left            =   120
            TabIndex        =   221
            ToolTipText     =   "Domicilio"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame8 
         Height          =   600
         Index           =   0
         Left            =   6480
         TabIndex        =   216
         ToolTipText     =   "Domicilio"
         Top             =   5760
         Width           =   1335
         Begin VB.CommandButton BtDomicilio 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoVet.frx":5C76
            Style           =   1  'Graphical
            TabIndex        =   217
            ToolTipText     =   "Domicilio"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Domicilio"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   17
            Left            =   120
            TabIndex        =   218
            ToolTipText     =   "Domicilio"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.CommandButton BtPesqPais 
         Height          =   315
         Left            =   12360
         Picture         =   "FormGestaoRequisicaoVet.frx":6000
         Style           =   1  'Graphical
         TabIndex        =   215
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Pa�ses"
         Top             =   1500
         Width           =   375
      End
      Begin VB.CommandButton BtPesqCodPostal 
         Height          =   315
         Left            =   12360
         Picture         =   "FormGestaoRequisicaoVet.frx":658A
         Style           =   1  'Graphical
         TabIndex        =   214
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
         Top             =   1080
         Width           =   375
      End
      Begin VB.Frame FrameDomicilio 
         Caption         =   "Domicilio"
         Height          =   1815
         Left            =   6480
         TabIndex        =   193
         Top             =   2400
         Width           =   6135
         Begin VB.CheckBox CkCobrarDomicilio 
            Caption         =   "Emitir Recibo do Domicilio"
            Height          =   255
            Left            =   240
            TabIndex        =   196
            Top             =   1320
            Width           =   2295
         End
         Begin VB.ComboBox CbCodUrbano 
            Height          =   315
            Left            =   1440
            Style           =   2  'Dropdown List
            TabIndex        =   195
            Top             =   480
            Width           =   1455
         End
         Begin VB.TextBox EcKm 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1425
            TabIndex        =   194
            Top             =   960
            Width           =   855
         End
         Begin VB.Label Label3 
            Caption         =   "C�digo Urbano"
            Height          =   255
            Left            =   240
            TabIndex        =   198
            Top             =   480
            Width           =   1215
         End
         Begin VB.Label Label4 
            Caption         =   "Km"
            Height          =   255
            Left            =   240
            TabIndex        =   197
            Top             =   960
            Width           =   495
         End
      End
      Begin VB.TextBox EcDoenteProf 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7680
         TabIndex        =   211
         Top             =   3720
         Width           =   3075
      End
      Begin VB.TextBox EcObsProveniencia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7800
         TabIndex        =   210
         Top             =   3240
         Width           =   3075
      End
      Begin VB.TextBox EcCodPais 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7680
         TabIndex        =   205
         Top             =   1500
         Width           =   615
      End
      Begin VB.TextBox EcMorada 
         Appearance      =   0  'Flat
         Height          =   735
         Left            =   7680
         MultiLine       =   -1  'True
         TabIndex        =   204
         Top             =   240
         Width           =   4935
      End
      Begin VB.TextBox EcCodPostal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   7680
         MaxLength       =   4
         TabIndex        =   203
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox EcRuaPostal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   8280
         MaxLength       =   3
         TabIndex        =   202
         Top             =   1080
         Width           =   490
      End
      Begin VB.TextBox EcNumBenef 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7680
         TabIndex        =   201
         Top             =   1995
         Width           =   3060
      End
      Begin VB.TextBox EcDescrPais 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8280
         Locked          =   -1  'True
         TabIndex        =   200
         TabStop         =   0   'False
         Top             =   1500
         Width           =   3975
      End
      Begin VB.TextBox EcDescrPostal 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   8760
         TabIndex        =   199
         Top             =   1080
         Width           =   3495
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   5520
         Picture         =   "FormGestaoRequisicaoVet.frx":6B14
         Style           =   1  'Graphical
         TabIndex        =   190
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   3120
         Width           =   375
      End
      Begin VB.TextBox EcDescrSala 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   189
         TabStop         =   0   'False
         Top             =   3120
         Width           =   3375
      End
      Begin VB.TextBox EcCodSala 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         TabIndex        =   188
         Top             =   3120
         Width           =   735
      End
      Begin VB.TextBox EcCodProveniencia 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         TabIndex        =   187
         Top             =   3600
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   5520
         Picture         =   "FormGestaoRequisicaoVet.frx":709E
         Style           =   1  'Graphical
         TabIndex        =   186
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   3600
         Width           =   375
      End
      Begin VB.TextBox EcDescrProveniencia 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   185
         TabStop         =   0   'False
         Top             =   3600
         Width           =   3375
      End
      Begin VB.ComboBox CbHemodialise 
         Height          =   315
         Left            =   4560
         Style           =   2  'Dropdown List
         TabIndex        =   178
         Top             =   2340
         Width           =   1335
      End
      Begin VB.TextBox EcDtEntrega 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   177
         Top             =   840
         Width           =   1335
      End
      Begin VB.TextBox EcDtPretend 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1440
         TabIndex        =   176
         Top             =   840
         Width           =   1335
      End
      Begin VB.ComboBox CbConvencao 
         BackColor       =   &H00C0FFFF&
         Height          =   315
         Left            =   4560
         Style           =   2  'Dropdown List
         TabIndex        =   175
         Top             =   1875
         Width           =   1335
      End
      Begin VB.ComboBox CbPrioColheita 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   174
         Top             =   1380
         Width           =   1335
      End
      Begin VB.TextBox EcDataPrevista 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         TabIndex        =   173
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Sexo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   28
         Left            =   120
         TabIndex        =   244
         ToolTipText     =   "Data de Nascimento"
         Top             =   4680
         Width           =   375
      End
      Begin VB.Label Labe41 
         Caption         =   "N� Doente Prof."
         Height          =   255
         Index           =   2
         Left            =   6480
         TabIndex        =   213
         Top             =   3720
         Width           =   1500
      End
      Begin VB.Label Labe41 
         Caption         =   "Obs. Proven."
         Height          =   255
         Index           =   5
         Left            =   6480
         TabIndex        =   212
         Top             =   3240
         Width           =   1500
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Pa�s"
         Height          =   195
         Index           =   36
         Left            =   6480
         TabIndex        =   209
         Top             =   1500
         Width           =   330
      End
      Begin VB.Label Label1 
         Caption         =   "Morada"
         Height          =   255
         Index           =   11
         Left            =   6480
         TabIndex        =   208
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Cod.Pos&tal"
         Height          =   195
         Index           =   12
         Left            =   6480
         TabIndex        =   207
         Top             =   1140
         Width           =   765
      End
      Begin VB.Label Label1 
         Caption         =   "N�. Benef."
         Height          =   255
         Index           =   10
         Left            =   6480
         TabIndex        =   206
         Top             =   1995
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Sala / Posto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   192
         Top             =   3120
         Width           =   1155
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   191
         Top             =   3600
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Hemodi�lise"
         Height          =   255
         Index           =   13
         Left            =   3600
         TabIndex        =   184
         Top             =   2340
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Dt Entrega"
         Height          =   255
         Index           =   7
         Left            =   3600
         TabIndex        =   183
         Top             =   855
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Dt Pretendida"
         Height          =   255
         Index           =   29
         Left            =   240
         TabIndex        =   182
         Top             =   855
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Conven��o"
         Height          =   255
         Index           =   26
         Left            =   3600
         TabIndex        =   181
         Top             =   1875
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Prioridade"
         Height          =   375
         Index           =   3
         Left            =   240
         TabIndex        =   180
         Top             =   1380
         Width           =   1395
      End
      Begin VB.Label Label1 
         Caption         =   "Dt previs&ta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   179
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Registo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1335
      Left            =   120
      TabIndex        =   60
      Top             =   7200
      Width           =   14415
      Begin VB.TextBox EcLocal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9240
         Locked          =   -1  'True
         TabIndex        =   76
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox EcUtilizadorFecho 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   75
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox EcHoraFecho 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5880
         Locked          =   -1  'True
         TabIndex        =   74
         Top             =   960
         Width           =   735
      End
      Begin VB.TextBox EcDataFecho 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4920
         Locked          =   -1  'True
         TabIndex        =   73
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcDataAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4920
         Locked          =   -1  'True
         TabIndex        =   72
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcHoraAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5880
         Locked          =   -1  'True
         TabIndex        =   71
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox EcUtilizadorAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   70
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox EcHoraImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   13320
         Locked          =   -1  'True
         TabIndex        =   69
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9240
         Locked          =   -1  'True
         TabIndex        =   68
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox EcDataImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   12360
         Locked          =   -1  'True
         TabIndex        =   67
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcHoraImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   13320
         Locked          =   -1  'True
         TabIndex        =   66
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9240
         Locked          =   -1  'True
         TabIndex        =   65
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox EcDataImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   12360
         Locked          =   -1  'True
         TabIndex        =   64
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcHoraCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5880
         Locked          =   -1  'True
         TabIndex        =   63
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox EcDataCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4920
         Locked          =   -1  'True
         TabIndex        =   62
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   61
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label19 
         Caption         =   "Local Requis."
         Height          =   255
         Left            =   8040
         TabIndex        =   88
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label LbNomeLocal 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   9840
         TabIndex        =   87
         Top             =   960
         Width           =   4455
      End
      Begin VB.Label Label2 
         Caption         =   "Fecho"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   86
         Top             =   960
         Width           =   855
      End
      Begin VB.Label LbNomeUtilFecho 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   85
         Top             =   960
         Width           =   3255
      End
      Begin VB.Label LbNomeUtilImpressao2 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   9840
         TabIndex        =   84
         Top             =   600
         Width           =   2535
      End
      Begin VB.Label LbNomeUtilImpressao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   9840
         TabIndex        =   83
         Top             =   240
         Width           =   2535
      End
      Begin VB.Label LbNomeUtilCriacao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   82
         Top             =   240
         Width           =   3255
      End
      Begin VB.Label LbNomeUtilAlteracao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   81
         Top             =   600
         Width           =   3255
      End
      Begin VB.Label Label18 
         Caption         =   "�ltima 2� Via"
         Height          =   255
         Left            =   8040
         TabIndex        =   80
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label17 
         Caption         =   "Impress�o"
         Height          =   255
         Left            =   8040
         TabIndex        =   79
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label16 
         Caption         =   "Altera��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   78
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Cria��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   77
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.TextBox EcPrColheita 
      Height          =   285
      Left            =   7920
      TabIndex        =   52
      Top             =   12600
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcinfComplementar 
      Height          =   285
      Left            =   6240
      TabIndex        =   51
      Top             =   12840
      Width           =   735
   End
   Begin VB.TextBox EcPesqRapTubo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   9000
      TabIndex        =   50
      Top             =   11400
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcAuxTubo 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   49
      Top             =   12840
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcHoraChegada 
      Height          =   285
      Left            =   3960
      Locked          =   -1  'True
      TabIndex        =   48
      Top             =   12840
      Width           =   975
   End
   Begin VB.TextBox EcGrupoAna 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3960
      TabIndex        =   47
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   8520
      TabIndex        =   46
      Top             =   12480
      Width           =   1095
   End
   Begin VB.TextBox EcPrinterResumo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4560
      TabIndex        =   45
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcImprimirResumo 
      Height          =   285
      Left            =   8520
      TabIndex        =   44
      Top             =   12120
      Width           =   975
   End
   Begin VB.TextBox EcTEFR 
      BackColor       =   &H00EDFAED&
      Enabled         =   0   'False
      Height          =   285
      Left            =   6240
      TabIndex        =   43
      Top             =   12480
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcAuxAna 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   3960
      MaxLength       =   20
      TabIndex        =   42
      Top             =   12480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcAuxProd 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   41
      Top             =   12480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcCodIsencao 
      Height          =   285
      Left            =   3960
      TabIndex        =   40
      Top             =   11280
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox CodAnaTemp 
      Height          =   285
      Left            =   3960
      TabIndex        =   39
      Top             =   11520
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapProduto 
      Enabled         =   0   'False
      Height          =   285
      Left            =   8280
      TabIndex        =   38
      Top             =   11400
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcSeqUtente 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   37
      Top             =   11400
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Timer TimerRS 
      Interval        =   60000
      Left            =   0
      Top             =   8520
   End
   Begin VB.ComboBox CbEstadoReqAux 
      Height          =   315
      Left            =   7080
      Style           =   2  'Dropdown List
      TabIndex        =   36
      TabStop         =   0   'False
      Top             =   11760
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.TextBox EcPesqRapProven 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3960
      TabIndex        =   35
      Top             =   11040
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEstadoReq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   34
      Top             =   11160
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPagUte 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   33
      Top             =   11640
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcUrgencia 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   32
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ComboBox CbEstadoReq 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      Height          =   315
      Left            =   2400
      Style           =   1  'Simple Combo
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   10200
      Width           =   3135
   End
   Begin VB.TextBox EcCodEfr2 
      Height          =   285
      Left            =   5760
      TabIndex        =   30
      Top             =   11040
      Width           =   375
   End
   Begin VB.TextBox EcDescrEfr2 
      Height          =   285
      Left            =   5760
      TabIndex        =   29
      Top             =   11400
      Width           =   375
   End
   Begin VB.TextBox EcCodPostalAux 
      Height          =   285
      Left            =   5760
      TabIndex        =   28
      Top             =   11760
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapMedico 
      Height          =   285
      Left            =   1920
      TabIndex        =   27
      Top             =   12120
      Width           =   375
   End
   Begin VB.TextBox EcObsReq 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1560
      MultiLine       =   -1  'True
      TabIndex        =   26
      Top             =   13320
      Width           =   1215
   End
   Begin VB.TextBox EcPrinterRecibo 
      Height          =   285
      Left            =   3960
      TabIndex        =   25
      Top             =   13320
      Width           =   1215
   End
   Begin VB.ComboBox CbSala 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6960
      Style           =   2  'Dropdown List
      TabIndex        =   24
      Top             =   13440
      Width           =   1335
   End
   Begin VB.TextBox EcFimSemana 
      Height          =   285
      Left            =   7680
      TabIndex        =   23
      Top             =   11040
      Width           =   375
   End
   Begin VB.TextBox EcEtqNCopias 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   22
      Top             =   11160
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNEsp 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   21
      Top             =   11400
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqTipo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   20
      Top             =   12120
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPrinterEtiq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   19
      Top             =   10920
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNTubos 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   18
      Top             =   11640
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNAdm 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   17
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcFlgFacturado 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   16
      Top             =   12480
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcAuxRec 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   11400
      TabIndex        =   15
      Top             =   13080
      Width           =   375
   End
   Begin VB.TextBox EcSMSEnviada 
      Height          =   285
      Left            =   1920
      TabIndex        =   14
      Top             =   10560
      Width           =   375
   End
   Begin VB.TextBox EcAbrevUte 
      Height          =   285
      Left            =   7680
      TabIndex        =   13
      Top             =   10560
      Width           =   1335
   End
   Begin VB.TextBox EcHemodialise 
      Height          =   285
      Left            =   3360
      TabIndex        =   12
      Top             =   10440
      Width           =   975
   End
   Begin VB.PictureBox PicCredencial 
      Height          =   735
      Left            =   960
      ScaleHeight     =   675
      ScaleWidth      =   795
      TabIndex        =   11
      Top             =   9120
      Width           =   855
   End
   Begin RichTextLib.RichTextBox EcInfo 
      Height          =   375
      Left            =   4080
      TabIndex        =   0
      Top             =   9000
      Width           =   495
      _ExtentX        =   873
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"FormGestaoRequisicaoVet.frx":7628
   End
   Begin TabDlg.SSTab SSTGestReq 
      Height          =   4815
      Left            =   120
      TabIndex        =   89
      Top             =   2400
      Width           =   12975
      _ExtentX        =   22886
      _ExtentY        =   8493
      _Version        =   393216
      Style           =   1
      Tabs            =   7
      Tab             =   1
      TabsPerRow      =   7
      TabHeight       =   520
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&Dados da Requisi��o"
      TabPicture(0)   =   "FormGestaoRequisicaoVet.frx":76B3
      Tab(0).ControlEnabled=   0   'False
      Tab(0).ControlCount=   0
      TabCaption(1)   =   "&An�lises"
      TabPicture(1)   =   "FormGestaoRequisicaoVet.frx":76CF
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "FrAnalises"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Pro&dutos"
      TabPicture(2)   =   "FormGestaoRequisicaoVet.frx":76EB
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "FrProdutos"
      Tab(2).Control(1)=   "FrBtProd"
      Tab(2).Control(2)=   "FrameObsEspecif"
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "Tu&bos"
      TabPicture(3)   =   "FormGestaoRequisicaoVet.frx":7707
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "FGTubos"
      Tab(3).Control(1)=   "FrBtTubos"
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Factura��o"
      TabPicture(4)   =   "FormGestaoRequisicaoVet.frx":7723
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "FrameAnaRecibos"
      Tab(4).Control(1)=   "FrameAdiantamentos"
      Tab(4).Control(2)=   "FGRecibos"
      Tab(4).ControlCount=   3
      TabCaption(5)   =   "An�lises &EFR"
      TabPicture(5)   =   "FormGestaoRequisicaoVet.frx":773F
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Label1(37)"
      Tab(5).Control(1)=   "Label1(35)"
      Tab(5).Control(2)=   "Label1(34)"
      Tab(5).Control(3)=   "Label1(33)"
      Tab(5).Control(4)=   "Label1(32)"
      Tab(5).Control(5)=   "FgAnaEFR"
      Tab(5).Control(6)=   "CkEnviaFactus"
      Tab(5).Control(7)=   "BTDigi"
      Tab(5).Control(8)=   "BtPesquisaEFR2"
      Tab(5).Control(8).Enabled=   0   'False
      Tab(5).Control(9)=   "CbEFR"
      Tab(5).Control(10)=   "EcIsencaoAct"
      Tab(5).Control(11)=   "EcBenefAct"
      Tab(5).Control(12)=   "EcMedAct"
      Tab(5).Control(13)=   "EcCredAct"
      Tab(5).Control(14)=   "EcAnaEfr"
      Tab(5).ControlCount=   15
      TabCaption(6)   =   "Informa��es / Quest�es"
      TabPicture(6)   =   "FormGestaoRequisicaoVet.frx":775B
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "EcAuxQuestao"
      Tab(6).Control(1)=   "FgInformacao"
      Tab(6).Control(2)=   "FgQuestoes"
      Tab(6).ControlCount=   3
      Begin VB.Frame FrBtTubos 
         BorderStyle     =   0  'None
         Height          =   735
         Left            =   -74880
         TabIndex        =   131
         Top             =   4140
         Width           =   12615
         Begin VB.Label LaTubos 
            Height          =   465
            Left            =   1320
            TabIndex        =   132
            Top             =   120
            Width           =   9135
         End
      End
      Begin VB.Frame FrAnalises 
         BorderStyle     =   0  'None
         Height          =   4455
         Left            =   120
         TabIndex        =   126
         Top             =   360
         Width           =   12615
         Begin VB.CommandButton BtPesquisaAna 
            Height          =   495
            Left            =   11880
            Picture         =   "FormGestaoRequisicaoVet.frx":7777
            Style           =   1  'Graphical
            TabIndex        =   128
            Top             =   3960
            Width           =   615
         End
         Begin VB.Frame FrameObsAna 
            BorderStyle     =   0  'None
            Caption         =   "Observa��o da An�lise"
            Height          =   4455
            Left            =   8640
            TabIndex        =   127
            Top             =   0
            Width           =   4095
            Begin VB.TextBox EcObsAna 
               Appearance      =   0  'Flat
               Height          =   4215
               Left            =   120
               TabIndex        =   271
               Top             =   0
               Width           =   3855
            End
         End
         Begin MSFlexGridLib.MSFlexGrid FGAna 
            Height          =   4290
            Left            =   120
            TabIndex        =   10
            TabStop         =   0   'False
            ToolTipText     =   "F7 - Para introduzir observa��o"
            Top             =   120
            Width           =   6105
            _ExtentX        =   10769
            _ExtentY        =   7567
            _Version        =   393216
            BackColorBkg    =   -2147483633
            SelectionMode   =   1
            AllowUserResizing=   3
            BorderStyle     =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSFlexGridLib.MSFlexGrid FgAnaRec 
            Height          =   3735
            Left            =   6360
            TabIndex        =   274
            Top             =   120
            Width           =   6015
            _ExtentX        =   10610
            _ExtentY        =   6588
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
         End
         Begin VB.Label Label7 
            Caption         =   "Total An�lises:"
            Height          =   255
            Left            =   10320
            TabIndex        =   130
            Top             =   4080
            Width           =   1095
         End
         Begin VB.Label LbTotalAna 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   11400
            TabIndex        =   129
            Top             =   4080
            Width           =   375
         End
      End
      Begin VB.Frame FrProdutos 
         BorderStyle     =   0  'None
         Height          =   3615
         Left            =   -74880
         TabIndex        =   124
         Top             =   480
         Width           =   12615
         Begin MSFlexGridLib.MSFlexGrid FgProd 
            Height          =   2235
            Left            =   120
            TabIndex        =   125
            Top             =   120
            Width           =   10500
            _ExtentX        =   18521
            _ExtentY        =   3942
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame FrBtProd 
         BorderStyle     =   0  'None
         Height          =   495
         Left            =   -74760
         TabIndex        =   119
         Top             =   4200
         Width           =   12495
         Begin VB.CommandButton BtObsEspecif 
            Enabled         =   0   'False
            Height          =   375
            Left            =   840
            Picture         =   "FormGestaoRequisicaoVet.frx":7D01
            Style           =   1  'Graphical
            TabIndex        =   122
            ToolTipText     =   "Obs. Especifica��o"
            Top             =   120
            Width           =   375
         End
         Begin VB.CommandButton BtPesquisaProduto 
            Height          =   375
            Left            =   0
            Picture         =   "FormGestaoRequisicaoVet.frx":808B
            Style           =   1  'Graphical
            TabIndex        =   121
            ToolTipText     =   "Pesquisa R�pida de Produtos"
            Top             =   120
            Width           =   375
         End
         Begin VB.CommandButton BtPesquisaEspecif 
            Height          =   375
            Left            =   360
            Picture         =   "FormGestaoRequisicaoVet.frx":8615
            Style           =   1  'Graphical
            TabIndex        =   120
            ToolTipText     =   "Pesquisa R�pida de Especifica��es de Produtos"
            Top             =   120
            Width           =   375
         End
         Begin VB.Label LaProdutos 
            Height          =   345
            Left            =   1320
            TabIndex        =   123
            Top             =   120
            Width           =   9135
         End
      End
      Begin VB.Frame FrameObsEspecif 
         Caption         =   "Observa��o do Produto/Especifica��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2295
         Left            =   -69000
         TabIndex        =   115
         Top             =   480
         Width           =   4575
         Begin VB.CommandButton BtOkObsEspecif 
            Caption         =   "OK"
            Height          =   375
            Left            =   1920
            TabIndex        =   117
            Top             =   1800
            Width           =   735
         End
         Begin VB.TextBox EcObsEspecif 
            Height          =   1335
            Left            =   240
            TabIndex        =   116
            Top             =   360
            Width           =   4095
         End
      End
      Begin VB.Frame FrameAnaRecibos 
         Caption         =   "An�lises"
         Height          =   4455
         Left            =   -74880
         TabIndex        =   107
         Top             =   360
         Width           =   12615
         Begin VB.CommandButton BtFgAnaRecSair 
            Height          =   615
            Left            =   10560
            Picture         =   "FormGestaoRequisicaoVet.frx":8B9F
            Style           =   1  'Graphical
            TabIndex        =   114
            Top             =   240
            Width           =   735
         End
         Begin VB.CommandButton BtFgAnaRecRecalcula 
            Height          =   615
            Left            =   10560
            Picture         =   "FormGestaoRequisicaoVet.frx":9869
            Style           =   1  'Graphical
            TabIndex        =   113
            ToolTipText     =   "Recalcula Pre�o Factura Recibo"
            Top             =   960
            Width           =   735
         End
         Begin VB.TextBox EcCodAnaAcresc 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   240
            TabIndex        =   112
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox EcDescrAnaAcresc 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1200
            Locked          =   -1  'True
            TabIndex        =   111
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox EcTaxaAnaAcresc 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   3000
            Locked          =   -1  'True
            TabIndex        =   110
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox EcQtdAnaAcresc 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2160
            TabIndex        =   109
            Top             =   720
            Width           =   735
         End
         Begin VB.CommandButton BtAnaAcresc 
            Height          =   375
            Left            =   5880
            Picture         =   "FormGestaoRequisicaoVet.frx":A533
            Style           =   1  'Graphical
            TabIndex        =   108
            Top             =   720
            Width           =   375
         End
      End
      Begin VB.Frame FrameAdiantamentos 
         Caption         =   "Adiantamentos"
         Height          =   4455
         Left            =   -74880
         TabIndex        =   102
         Top             =   360
         Width           =   12615
         Begin VB.CommandButton BtAnularAdiamento 
            BackColor       =   &H80000004&
            Height          =   495
            Left            =   9720
            Picture         =   "FormGestaoRequisicaoVet.frx":A8BD
            Style           =   1  'Graphical
            TabIndex        =   105
            ToolTipText     =   "Devolver Adiantamento"
            Top             =   3840
            Width           =   615
         End
         Begin VB.CommandButton BtImprimirAdiantamento 
            Height          =   495
            Left            =   10560
            Picture         =   "FormGestaoRequisicaoVet.frx":B587
            Style           =   1  'Graphical
            TabIndex        =   104
            ToolTipText     =   "ReImprimir Factura Recibo"
            Top             =   3840
            Width           =   615
         End
         Begin VB.CommandButton BtSairAdiantamento 
            Height          =   495
            Left            =   8880
            Picture         =   "FormGestaoRequisicaoVet.frx":C251
            Style           =   1  'Graphical
            TabIndex        =   103
            Top             =   3840
            Width           =   615
         End
         Begin MSFlexGridLib.MSFlexGrid FgAdiantamentos 
            Height          =   2655
            Left            =   120
            TabIndex        =   106
            Top             =   360
            Width           =   8775
            _ExtentX        =   15478
            _ExtentY        =   4683
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
            Appearance      =   0
         End
      End
      Begin VB.TextBox EcAnaEfr 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -74880
         TabIndex        =   100
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox EcCredAct 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   205
         Left            =   -69960
         Locked          =   -1  'True
         TabIndex        =   99
         Top             =   480
         Width           =   1215
      End
      Begin VB.TextBox EcMedAct 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   205
         Left            =   -68040
         Locked          =   -1  'True
         TabIndex        =   98
         Top             =   480
         Width           =   975
      End
      Begin VB.TextBox EcBenefAct 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   205
         Left            =   -66240
         Locked          =   -1  'True
         TabIndex        =   97
         Top             =   480
         Width           =   1025
      End
      Begin VB.TextBox EcIsencaoAct 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   205
         Left            =   -64560
         Locked          =   -1  'True
         TabIndex        =   96
         Top             =   480
         Width           =   975
      End
      Begin VB.ComboBox CbEFR 
         Height          =   315
         Left            =   -73080
         Style           =   2  'Dropdown List
         TabIndex        =   95
         Top             =   480
         Width           =   1815
      End
      Begin VB.CommandButton BtPesquisaEFR2 
         Height          =   315
         Left            =   -71280
         Picture         =   "FormGestaoRequisicaoVet.frx":CF1B
         Style           =   1  'Graphical
         TabIndex        =   94
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton BTDigi 
         Height          =   495
         Left            =   -64440
         Picture         =   "FormGestaoRequisicaoVet.frx":D4A5
         Style           =   1  'Graphical
         TabIndex        =   93
         Top             =   4320
         Width           =   495
      End
      Begin VB.CheckBox CkEnviaFactus 
         Caption         =   "Enviar para FACTUS"
         Height          =   195
         Left            =   -74760
         TabIndex        =   92
         Top             =   4560
         Width           =   2175
      End
      Begin VB.TextBox EcAuxQuestao 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -74160
         TabIndex        =   90
         Top             =   1560
         Visible         =   0   'False
         Width           =   1215
      End
      Begin MSFlexGridLib.MSFlexGrid FgInformacao 
         Height          =   2175
         Left            =   -74880
         TabIndex        =   91
         Top             =   480
         Width           =   10935
         _ExtentX        =   19288
         _ExtentY        =   3836
         _Version        =   393216
         BackColorBkg    =   -2147483644
         BorderStyle     =   0
      End
      Begin MSFlexGridLib.MSFlexGrid FgAnaEFR 
         Height          =   3375
         Left            =   -74880
         TabIndex        =   101
         Top             =   840
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   5953
         _Version        =   393216
         BackColorBkg    =   -2147483644
         BorderStyle     =   0
      End
      Begin MSFlexGridLib.MSFlexGrid FGRecibos 
         Height          =   3135
         Left            =   -74880
         TabIndex        =   118
         Top             =   600
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   5530
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
         Appearance      =   0
      End
      Begin MSFlexGridLib.MSFlexGrid FGTubos 
         Height          =   3315
         Left            =   -74760
         TabIndex        =   133
         Top             =   600
         Width           =   12420
         _ExtentX        =   21908
         _ExtentY        =   5847
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid FgQuestoes 
         Height          =   2175
         Left            =   -74880
         TabIndex        =   134
         Top             =   2640
         Width           =   10935
         _ExtentX        =   19288
         _ExtentY        =   3836
         _Version        =   393216
         BackColorBkg    =   -2147483644
         BorderStyle     =   0
      End
      Begin VB.Label Label1 
         Caption         =   "Credencial"
         Height          =   255
         Index           =   32
         Left            =   -70800
         TabIndex        =   139
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "M�dico"
         Height          =   255
         Index           =   33
         Left            =   -68640
         TabIndex        =   138
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "N�Benef."
         Height          =   255
         Index           =   34
         Left            =   -66960
         TabIndex        =   137
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Isen��o"
         Height          =   255
         Index           =   35
         Left            =   -65160
         TabIndex        =   136
         Top             =   480
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "EFR"
         Height          =   255
         Index           =   37
         Left            =   -73440
         TabIndex        =   135
         Top             =   480
         Width           =   495
      End
   End
   Begin MSComctlLib.ImageList ListaImagensAna 
      Left            =   8280
      Top             =   13080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":E16F
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":E2C9
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":E423
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":E57D
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":E6D7
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":E831
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":E98B
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":EAE5
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":EC3F
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":ED99
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":EEF3
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":F04D
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageListSearch 
      Left            =   480
      Top             =   9600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   24
      ImageHeight     =   24
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":F1A7
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":F921
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":1009B
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoVet.frx":10815
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label LbNumColheita 
      AutoSize        =   -1  'True
      Caption         =   "N�Colheita"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   10680
      TabIndex        =   279
      Top             =   10320
      Width           =   750
   End
   Begin VB.Label Label41 
      Caption         =   "EcFlgDiagSec"
      Enabled         =   0   'False
      Height          =   255
      Index           =   29
      Left            =   2160
      TabIndex        =   270
      Top             =   12240
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label EcUtilNomeConf 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   2280
      TabIndex        =   263
      Top             =   18840
      Width           =   3255
   End
   Begin VB.Label Label1 
      Caption         =   "Confirma��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   30
      Left            =   720
      TabIndex        =   262
      Top             =   18840
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcPrColheita"
      Enabled         =   0   'False
      Height          =   255
      Index           =   24
      Left            =   7200
      TabIndex        =   171
      Top             =   12600
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label41 
      Caption         =   "EcinfComplementar"
      Height          =   255
      Index           =   21
      Left            =   5400
      TabIndex        =   170
      Top             =   12840
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxTubo"
      Enabled         =   0   'False
      Height          =   255
      Index           =   22
      Left            =   480
      TabIndex        =   169
      Top             =   12840
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcGrupoAna"
      Enabled         =   0   'False
      Height          =   255
      Index           =   13
      Left            =   2520
      TabIndex        =   168
      Top             =   11880
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcTEFR"
      Enabled         =   0   'False
      Height          =   255
      Index           =   20
      Left            =   5400
      TabIndex        =   167
      Top             =   12480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxAna"
      Enabled         =   0   'False
      Height          =   255
      Index           =   17
      Left            =   2880
      TabIndex        =   166
      Top             =   12480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcCodIsencao"
      Height          =   255
      Index           =   19
      Left            =   2520
      TabIndex        =   165
      Top             =   11280
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "CodAnaTemp"
      Height          =   255
      Index           =   0
      Left            =   2520
      TabIndex        =   164
      Top             =   11520
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapProduto"
      Enabled         =   0   'False
      Height          =   255
      Index           =   14
      Left            =   6840
      TabIndex        =   163
      Top             =   11400
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcSeqUtente"
      Enabled         =   0   'False
      Height          =   255
      Index           =   8
      Left            =   840
      TabIndex        =   162
      Top             =   11400
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapProven"
      Enabled         =   0   'False
      Height          =   255
      Index           =   6
      Left            =   2520
      TabIndex        =   161
      Top             =   11040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcEstadoReq"
      Enabled         =   0   'False
      Height          =   255
      Index           =   1
      Left            =   840
      TabIndex        =   160
      Top             =   11160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcPagUte"
      Enabled         =   0   'False
      Height          =   255
      Index           =   9
      Left            =   840
      TabIndex        =   159
      Top             =   11640
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcUrgencia"
      Enabled         =   0   'False
      Height          =   255
      Index           =   10
      Left            =   840
      TabIndex        =   158
      Top             =   11880
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxProd"
      Enabled         =   0   'False
      Height          =   255
      Index           =   5
      Left            =   480
      TabIndex        =   157
      Top             =   12480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcCodEfr2"
      Enabled         =   0   'False
      Height          =   255
      Index           =   2
      Left            =   4440
      TabIndex        =   156
      Top             =   11040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcDescrEfr2"
      Enabled         =   0   'False
      Height          =   255
      Index           =   3
      Left            =   4440
      TabIndex        =   155
      Top             =   11400
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcCodPostalAux"
      Enabled         =   0   'False
      Height          =   255
      Index           =   4
      Left            =   4440
      TabIndex        =   154
      Top             =   11640
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapMedico"
      Enabled         =   0   'False
      Height          =   255
      Index           =   7
      Left            =   840
      TabIndex        =   153
      Top             =   12120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label15 
      Caption         =   "Obs. &Final"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   360
      TabIndex        =   152
      Top             =   13320
      Width           =   975
   End
   Begin VB.Label Label15 
      Caption         =   "EcPrinterRecibo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   2880
      TabIndex        =   151
      Top             =   13320
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcFimSemana"
      Enabled         =   0   'False
      Height          =   255
      Index           =   11
      Left            =   6240
      TabIndex        =   150
      Top             =   11040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Labe41 
      Caption         =   "EcEtqNCopias"
      Enabled         =   0   'False
      Height          =   255
      Index           =   0
      Left            =   10200
      TabIndex        =   149
      Top             =   11160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Labe41 
      Caption         =   "EcEtqNEsp"
      Enabled         =   0   'False
      Height          =   255
      Index           =   1
      Left            =   10200
      TabIndex        =   148
      Top             =   11400
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqTipo"
      Enabled         =   0   'False
      Height          =   255
      Index           =   15
      Left            =   10200
      TabIndex        =   147
      Top             =   12120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcPrinterEtiq"
      Enabled         =   0   'False
      Height          =   255
      Index           =   12
      Left            =   10200
      TabIndex        =   146
      Top             =   10920
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqNTubos"
      Enabled         =   0   'False
      Height          =   255
      Index           =   16
      Left            =   10200
      TabIndex        =   145
      Top             =   11640
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqNAdm"
      Enabled         =   0   'False
      Height          =   255
      Index           =   18
      Left            =   10200
      TabIndex        =   144
      Top             =   11880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcFlgFacturado"
      Enabled         =   0   'False
      Height          =   255
      Index           =   23
      Left            =   10200
      TabIndex        =   143
      Top             =   12480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxRec"
      Enabled         =   0   'False
      Height          =   255
      Index           =   26
      Left            =   10320
      TabIndex        =   142
      Top             =   13080
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcSMSEnviada"
      Enabled         =   0   'False
      Height          =   255
      Index           =   27
      Left            =   600
      TabIndex        =   141
      Top             =   10560
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcHemodialise"
      Enabled         =   0   'False
      Height          =   255
      Index           =   28
      Left            =   2400
      TabIndex        =   140
      Top             =   10440
      Visible         =   0   'False
      Width           =   1455
   End
End
Attribute VB_Name = "FormGestaoRequisicaoVet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 27/03/2003
' T�cnico : Paulo Costa

' Vari�veis  para este Form.
Const Verde = &HC0FFC0
Const vermelho = &HC0C0FF
Const azul = &HFFC0C0
Const Amarelo = &HC0FFFF
Const rosa = &HFFC0FF
Const azul2 = &HFFFFC0

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim CamposEcBckp() As String
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Public CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim Formato1 As String
Dim Formato2 As String


Public rs As ADODB.recordset
Public MarcaLocal As Variant

'Controlo dos produtos
Public RegistosP As New Collection
Dim LastColP As Integer
Dim LastRowP As Integer
Dim ExecutaCodigoP As Boolean


'Flag que indica se a entidade em causa � ARS
Dim FlgARS As Boolean

'Flag que indica se a entidade em causa � ADSE
Dim FlgADSE As Boolean

'Controlo de analises
Public RegistosA As New Collection
Dim LastColA As Integer
Dim LastRowA As Long
Dim ExecutaCodigoA As Boolean

'Controlo de Recibos
Public RegistosR As New Collection
Dim LastColR As Integer
Dim LastRowR As Integer
Dim ExecutaCodigoR As Boolean

'Controlo de Recibos Manuais
Public RegistosRM As New Collection

'Controlo de Adiantamentos
Public RegistosAD As New Collection

'acumula o numero do P1 para quando este for superior a 7 incrementar 1 ao P1
Dim ContP1 As Integer
Dim Flg_IncrementaP1 As Boolean

'Comando utilizados obter a ordem correcta das an�lises
Dim CmdOrdAna As New ADODB.Command

'Comando utilizados para seleccionar as an�lises da requisi��o mais os seu dados
Dim CmdAnaReq As String
Dim CmdAnaReq_H As String


'Flag usada para controlar a coloca��o da data actual no lostfocus da EcDataPrevista
'quando se faz o limpacampos
Dim Flg_LimpaCampos As Boolean

'flag que indica se se deve preencher a data de chegada de todos os produtos na altura do Insert ou UpDate
Dim Flg_PreencherDtChega As Boolean

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind, Max  As Integer

'valor do indice nas combos de Pagamento (TM,Ent,Ute) quando se executa a fun��o procurar
Dim CbTMIndex As Integer
Dim CbEntIndex As Integer
Dim CbUteIndex As Integer

Dim gRec As Long

'Flag que evita os beep dos enters nas textbox
Dim Enter As Boolean


Dim Flg_PergProd As Boolean
Dim Flg_DtChProd As Boolean
Dim Resp_PergProd As Boolean
Dim Resp_PergDtChProd As Boolean

Dim Flg_PergTubo As Boolean
Dim Flg_DtChTubo As Boolean
Dim Resp_PergTubo As Boolean
Dim Resp_PergDtChTubo As Boolean


'contem a data de chegada da verificacao dos produtos para utilizacao na marcacao de analises
Dim gColocaDataChegada As String

'Flag que indica a entrada no form de pesquisa de utentes
Dim Flg_PesqUtente As Boolean

'Estruturas utilizadas para gerar as etiquetas
Private Type Tipo_Tubo
    Especial As Boolean
    Cheio As Boolean
    DescrEtiqAna As String
    GrAna As String
    CodProd As String
    codTuboBar As String
    CodTubo As String
    QtMax As Double
    QtOcup As Double
    Designacao_tubo As String
    abrAna As String
    inf_complementar As String
    num_copias As Integer
    num_ana As Integer
    num_max_ana As Integer
    Descr_etiq_ana As String
    normaTubo As String
    dt_chega As String
End Type
Dim Tubos() As Tipo_Tubo

'Variaveis utilizadas na marca��o de an�lises GhostMember na pr�pria requisi�ao
Dim GhostPerfil As String
Dim GhostComplexa As String

Dim Flg_DadosAnteriores As Boolean

' ---------------------------------------------------

' 02


Dim Flg_Gravou As Boolean


'Estrutura que guarda as an�lises eliminadas
Private Type Tipo_AnalisesEliminadas
    codAna As String
    NReq As String
End Type
Dim Eliminadas() As Tipo_AnalisesEliminadas
Dim TotalEliminadas As Integer

'Estrutura que guarda as an�lises acrescentadas
Private Type Tipo_AnalisesAcrescentadas
    cod_agrup As String
    NReq As String
    
End Type
Dim Acrescentadas() As Tipo_AnalisesAcrescentadas
Dim TotalAcrescentadas As Integer

Public Req_FDS As Boolean


Const lColFgAnaCodigo = 0
Const lColFgAnaDescricao = 1
Const lColFgAnaP1 = 2
Const lColFgAnaMedico = 3
Const lColFgAnaCodEFR = 4
Const lColFgAnaDescrEFR = 5
Const lColFgAnaNrBenef = 6
Const lColFgAnaPreco = 7

Const lColFgAnaEFRlinha = 0
Const lColFgAnaEFRCodigo = 1
Const lColFgAnaEFRDescricao = 2
Const lColFgAnaEFRP1 = 3
Const lColFgAnaEFROrdemP1 = 4
Const lColFgAnaEFRMedico = 5
Const lColFgAnaEFRCodEFR = 6
Const lColFgAnaEFRNrBenef = 7
Const lColFgAnaEFRIsen = 8
Const lColFgAnaEFRFact = 9
Const lColFgAnaEFRCB = 10
Const lColFgAnaEFRQtd = 11
Const lColFgAnaEFRRec = 12
Const lColFgAnaEFRPreco = 13
Const lColFgAnaEFRPrecoEFR = 14

Const lColFgAnaRecCodAna = 0
Const lColFgAnaRecDescrAna = 1
Const lColFgAnaRecQtd = 2
Const lColFgAnaRecTaxa = 3

Const lCodUrbano = 2
Const lCodNaoUrbano = 3
Const lCodUrbanoPortoLisboa = 1
Const lCodUrbanoBiDiario = 4

Dim linhaAnaliseOBS As Long
Dim CodEfrAUX As String


' ESTRUTURA COM ANALISES PERTENCENTES A UM RECIBO
Private Type AnaRecibo
    codAna As String
    descrAna As String
    taxa As String
    qtd As Integer
    tipo As String  ' MARCADA - MANUAL
    indice As Long  ' INDICE NA ESTRUTURA
    Flg_adicionada As Integer
    flgRetirada As Integer
    flgFacturada As Integer
End Type
Dim EstrutAnaRecibo() As AnaRecibo
Dim TotalAnaRecibo As Integer


Const CorBorla = rosa
Const CorIsento = azul2

Dim TuboColunaActual As Long
Dim TuboLinhaActual As Long

Dim flgEmissaoRecibos As Boolean

' -------------------------------------------------------------------
' COLUNAS NA GRELHA RECIBOS
' -------------------------------------------------------------------
Const lColRecibosNumDoc = 0
Const lColRecibosEntidade = 1
Const lColRecibosValorPagar = 2
Const lColRecibosCaucao = 7
Const lColRecibosModoPag = 8
Const lColRecibosDtEmissao = 4
Const lColRecibosDtPagamento = 5
Const lColRecibosEstado = 6
Const lColRecibosPercentagem = 3

Const lColAdiantamentosNumDoc = 0
Const lColAdiantamentosValor = 1
Const lColAdiantamentosEstado = 2
Const lColAdiantamentosDtEmissao = 3
Const lColAdiantamentosDtAnulacao = 4
Dim flg_Aberto As Boolean
Dim flg_preencheCampos As Boolean
Dim flg_ReqBloqueada As Boolean
Dim flg_codBarras As Boolean
Dim Acumula As String
Dim Cabecalho As String

Private Type entidades
    cod_efr As String
    descr_efr As String
    flg_perc_facturar As String
    flg_controla_isencao As String
    flg_obriga_recibo As String
    deducao_ute As String
    flg_acto_med As String
    cod_empresa As String
    flg_compart_dom As String
    flg_dom_defeito As String
    flg_nao_urbano As String
    flg_65anos As String
    flg_separa_dom As String
    flg_insere_cod_rubr_efr As String
    flgActivo As Boolean
    flg_novaArs As Integer
End Type
Dim EstrutEFR() As entidades
Dim totalEFR As Integer

Const laranja = &HC0E0FF

Private Type informacao
    cod_ana As String
    cod_informacao As String
    descr_informacao As String
    informacao As String
End Type
Dim EstrutInfo() As informacao
Dim totalInfo As Integer

Private Type Questoes
    Resposta As String
    cod_questao As String
    descr_questao As String
End Type
Dim EstrutQuestao() As Questoes
Dim totalQuestao As Integer

Dim flg_novaArs As Integer
Const vermelhoClaro = &HBDC8F4

Private Type Tipo_tuboEliminado
    seq_req_tubo As Long
    n_req As String
End Type
Dim tubosEliminados() As Tipo_tuboEliminado
Dim TotalTubosEliminados As Integer
'RGONCALVES 29.05.2013 Cedivet-30
Dim codEfrOriginal As String 'gerir altera��o do c�digo da entidade
'

'NELSONPSILVA Glintt-HS-18011 04.04.2018 - Acesso contextualizado quando chamado pelo form anterior (Log RGPD)
Public ExternalAccess As Boolean
'

' ---------------------------------------------------

Sub Elimina_Mareq(codAgrup As String)

    Dim i As Integer

    On Error GoTo Trata_Erro


    i = 1
    While i <= UBound(MaReq)
        If Trim(UCase(codAgrup)) = Trim(UCase(MaReq(i).cod_agrup)) Then
            BL_Actualiza_Estrutura_MAReq i
        Else
            i = i + 1
        End If
    Wend
        
    If UBound(MaReq) = 0 Then ReDim MaReq(1)

Exit Sub
Trata_Erro:
    If Err.Number <> 35601 Then
        BG_LogFile_Erros Me.Name & "Elimina_Mareq: " & Err.Description
    End If
    
    Resume Next
End Sub

Sub Elimina_RegistoA(codAgrup As String)
    On Error GoTo TrataErro
    Dim i As Long

    For i = 1 To RegistosA.Count - 1
        If Trim(codAgrup) = Trim(RegistosA(i).codAna) Then
            RegistosA.Remove i
            FGAna.RemoveItem i
        End If
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "Elimina_RegistoA: " & Err.Number & " - " & Err.Description, Me.Name, "Elimina_RegistoA"
    Exit Sub
    Resume Next
End Sub

Function Insere_Nova_Analise(indice As Long, codAgrup As String, AnaMarcada As String, codBarras As Boolean, multiAna As String) As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    Dim descricao As String
    Dim Marcar() As String
    Dim reqComAnalise As String
    Dim DataReq As String
    Dim CodFacturavel As String
    

    If EcSeqUtente.text = "" Then Exit Function
    
    Insere_Nova_Analise = False
    If BG_DaComboSel(CbTipoIsencao) = mediComboValorNull Then
        For i = 0 To CbTipoIsencao.ListCount - 1
            If CbTipoIsencao.ItemData(i) = gTipoIsencaoNaoIsento Then
                CbTipoIsencao.ListIndex = i
                Exit For
            End If
        Next i
    End If
    
    descricao = SELECT_Descr_Ana(codAgrup)
    If codAgrup <> AnaMarcada Then
        If Mid(codAgrup, 2) <> UCase(AnaMarcada) Then
            CodFacturavel = AnaMarcada
        Else
            CodFacturavel = codAgrup
        End If
        
    Else
        CodFacturavel = codAgrup
    End If
    If descricao = "" Then
        BG_Mensagem mediMsgBox, "An�lise inexistente!", vbOKOnly + vbInformation, "An�lises"
        EcAuxAna.text = ""
    Else
        If VerificaRegraSexoMarcar(codAgrup, EcSeqUtente) = False Then
            BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
            EcAuxAna.text = ""
            Insere_Nova_Analise = False
            Exit Function
        End If
        gColocaDataChegada = ""
        If Verifica_Ana_Prod(codAgrup, Marcar) Then
            If Trim(RegistosA(indice).codAna) <> "" Then
                Elimina_Mareq RegistosA(indice).codAna
                RegistosA(indice).codAna = ""
            End If
        
            ' --------------------------------------------------------------------------------
            
            If (gProcAnPendentesAoRegistar = mediSim) Then
            
                ' Indica a requisi��o mais recente com esta an�lise pendente, para este utente.
                            
                reqComAnalise = ""
                reqComAnalise = REQUISICAO_Get_Com_Analise_Pendente(UCase(EcAuxAna.text), _
                                                                    EcSeqUtente.text, _
                                                                    DataReq)
        
                If (Len(reqComAnalise)) Then
                    MsgBox "A an�lise " & UCase(EcAuxAna.text) & " est� pendente na requisi��o " & reqComAnalise & "        " & vbCrLf & _
                           "datada de " & DataReq & ".", vbInformation, " Pesquisa de An�lises Pendentes "
                End If
            
            End If
            ' --------------------------------------------------------------------------------
            Select Case Mid(Trim(UCase(codAgrup)), 1, 1)
                Case "P"
                
                    SELECT_Dados_Perfil Marcar, indice, Trim(UCase(codAgrup)), CodFacturavel, multiAna
                Case "C"
                    SELECT_Dados_Complexa Marcar, indice, Trim(UCase(codAgrup)), multiAna, , , , CodFacturavel
                Case "S"
                    If SELECT_Dados_Simples(Marcar, indice, Trim(UCase(codAgrup)), multiAna, , , , , , CodFacturavel) = False Then
                        BG_Mensagem mediMsgBox, "An�lise inexistente!", vbOKOnly + vbInformation, "An�lises"
                        EcAuxAna.text = ""
                        Exit Function
                    End If
                Case Else
            End Select
            
            PreencheAnaRecibos
            
            Insere_Nova_Analise = True
        End If
        
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Insere_Nova_Analise: " & Err.Number & " - " & Err.Description, Me.Name, "Insere_Nova_Analise"
    Insere_Nova_Analise = False
    Exit Function
    Resume Next
End Function


Sub InsereAutoProd(CodProd As String, DtChega As String)
    On Error GoTo TrataErro

    Dim LastLastRP As Integer
    Dim i As Integer
    
    EcAuxProd.text = CodProd
    
    LastLastRP = LastRowP
    LastRowP = FgProd.rows - 1
    
    'Verificar se o produto j� est� na lista
    For i = 1 To LastRowP
        If Trim(FgProd.TextMatrix(i, 0)) = Trim(CodProd) Then
            Exit Sub
        End If
    Next
    
    If SELECT_Descr_Prod = True Then
        ExecutaCodigoP = False
        
        If Trim(DtChega) <> "" Then
            FgProd.TextMatrix(LastRowP, 5) = Trim(DtChega)
            RegistosP(LastRowP).DtPrev = Trim(DtChega)
            FgProd.TextMatrix(LastRowP, 6) = Trim(DtChega)
            RegistosP(LastRowP).DtChega = Trim(DtChega)
            gColocaDataChegada = Trim(DtChega)
            If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto Then
                EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
            End If
        Else
            FgProd.TextMatrix(LastRowP, 5) = EcDataPrevista
            RegistosP(LastRowP).DtPrev = EcDataPrevista
        End If

        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
        FgProd.TextMatrix(LastRowP, 7) = RegistosP(LastRowP).EstadoProd
                        
        FgProd.TextMatrix(LastRowP, 0) = UCase(CodProd)
        RegistosP(LastRowP).CodProd = UCase(CodProd)
        
        'Cria linha vazia
        FgProd.AddItem ""
        CriaNovaClasse RegistosP, FgProd.rows - 1, "PROD"
        
        ExecutaCodigoP = True
        
        FgProd.row = FgProd.rows - 1
        FgProd.Col = 0
        LastColP = FgProd.Col
        LastRowP = FgProd.row
    Else
        LastRowP = LastLastRP
    End If
    
    EcAuxProd.text = ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "InsereAutoProd: " & Err.Number & " - " & Err.Description, Me.Name, "InsereAutoProd"
    Exit Sub
    Resume Next
End Sub

Sub InsereGhostMember(codAnaS As String)
    On Error GoTo TrataErro

    Dim RsCompl As ADODB.recordset
    Dim sql As String
    Dim descricao As String
    Dim Marcar(1) As String
    Dim indice  As Long
    Dim i As Long
    Dim OrdAna As Long
    Dim CodPerfil As String
    Dim DescrPerfil As String
    Dim OrdPerf As Integer
    Dim CodAnaC As String
    Dim DescrAnaC As String

codAnaS = Trim(codAnaS)

'For�ar a marca��o
Marcar(1) = codAnaS & ";"

indice = -1
For i = 1 To UBound(MaReq)
    If Trim(GhostPerfil) = Trim(MaReq(i).Cod_Perfil) And _
       Trim(GhostComplexa) = Trim(MaReq(i).cod_ana_c) Then
       indice = i
       Exit For
    End If
Next i

If indice = -1 Then
    BG_Mensagem mediMsgBox, "Erro a inserir an�lise!", vbCritical + vbOKOnly, App.ProductName
    Exit Sub
End If

gColocaDataChegada = MaReq(indice).dt_chega

'Dados da Complexa
sql = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c = " & BL_TrataStringParaBD(GhostComplexa)
Set RsCompl = New ADODB.recordset
With RsCompl
    .Source = sql
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    .Open , gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
End With

If RsCompl.RecordCount > 0 Then
    descricao = BL_HandleNull(RsCompl!Descr_Ana_C, "?")
Else
    BG_LogFile_Erros Me.Name & " - SELECT_Dados_Complexa: Erro a seleccionar dados da complexa!"
End If
RsCompl.Close
Set RsCompl = Nothing

OrdAna = MaReq(indice).Ord_Ana
CodPerfil = MaReq(indice).Cod_Perfil
DescrPerfil = MaReq(indice).Descr_Perfil
OrdPerf = MaReq(indice).Ord_Ana_Perf
CodAnaC = MaReq(indice).cod_ana_c
DescrAnaC = MaReq(indice).Descr_Ana_C

'Dados do Membro da complexa (CodAnaS)
sql = "SELECT ordem FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(GhostComplexa) & _
        " AND cod_membro = " & BL_TrataStringParaBD(codAnaS)
Set RsCompl = New ADODB.recordset
With RsCompl
    .Source = sql
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    .Open , gConexao
End With

If Not RsCompl.EOF Then
    If GhostPerfil <> "0" Then
        SELECT_Dados_Simples Marcar, OrdAna, codAnaS, "", CodPerfil, DescrPerfil, OrdPerf, CodAnaC, DescrAnaC, BL_HandleNull(RsCompl!ordem, " ")
    Else
        SELECT_Dados_Simples Marcar, OrdAna, codAnaS, "", , , , CodAnaC, DescrAnaC, BL_HandleNull(RsCompl!ordem, " ")
    End If
End If

RsCompl.Close
Set RsCompl = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "InsereGhostMember: " & Err.Number & " - " & Err.Description, Me.Name, "InsereGhostMember"
    Exit Sub
    Resume Next
End Sub

Function Pesquisa_RegistosA(codAgrup As String) As Long
    On Error GoTo TrataErro

    Dim i As Long
    Dim lRet As Long
    
    lRet = -1
    
    For i = 1 To RegistosA.Count - 1
        If Trim(codAgrup) = Trim(RegistosA(i).codAna) Then
            lRet = i
            Exit For
        End If
    Next i
    
    Pesquisa_RegistosA = lRet
Exit Function
TrataErro:
    BG_LogFile_Erros "Pesquisa_RegistosA: " & Err.Number & " - " & Err.Description, Me.Name, "Pesquisa_RegistosA"
    Pesquisa_RegistosA = -1
    Exit Function
    Resume Next
End Function

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    Inicializacoes
    flgEmissaoRecibos = False

    Max = -1
    ReDim FOPropertiesTemp(0)
    
    DefTipoCampos
    
    EcUtente.text = ""
    DoEvents
    
    PreencheValoresDefeito
        
    gF_REQUIS_VET = 1
    Flg_PesqUtente = False
    
    Estado = 1
    BG_StackJanelas_Push Me
        
    SSTGestReq.Tab = 1
    DoEvents
    Flg_DadosAnteriores = False

End Sub

Sub Inicializacoes()
    On Error GoTo TrataErro
    Cabecalho = " Gest�o de Requisi��es "
    Me.caption = Cabecalho
    Me.left = 5
    Me.top = 20
    Me.Width = 14715
    Me.Height = 9090
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_requis"
    Set CampoDeFocus = EcNumReq
    NumCampos = 55
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim CamposEcBckp(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "n_req"
    CamposBD(1) = "seq_utente"
    CamposBD(2) = "dt_previ"
    CamposBD(3) = "dt_chega"
    CamposBD(4) = "dt_imp"
    CamposBD(5) = "t_urg"
    CamposBD(6) = "cod_efr"
    CamposBD(7) = "n_benef"
    If gListaMedicos = 1 Then
        CamposBD(8) = "cod_med"
    Else
        CamposBD(8) = "descr_medico"
    End If
    CamposBD(9) = "obs_req"
    CamposBD(10) = "hr_imp"
    CamposBD(11) = "user_imp"
    CamposBD(12) = "dt_imp2"
    CamposBD(13) = "hr_imp2"
    CamposBD(14) = "user_imp2"
    CamposBD(15) = "estado_req"
    CamposBD(16) = "user_cri"
    CamposBD(17) = "hr_cri"
    CamposBD(18) = "dt_cri"
    CamposBD(19) = "user_act"
    CamposBD(20) = "hr_act"
    CamposBD(21) = "dt_act"
    CamposBD(22) = "cod_isencao"
    CamposBD(23) = "gr_ana"
    CamposBD(24) = "hr_chega"
    CamposBD(25) = "cod_local"
    CamposBD(26) = "user_fecho"
    CamposBD(27) = "dt_fecho"
    CamposBD(28) = "hr_fecho"
    CamposBD(29) = "inf_complementar"
    CamposBD(30) = "cod_t_colheita"
    CamposBD(31) = "cod_sala"
    CamposBD(32) = "cod_destino"
    CamposBD(33) = "hemodialise"
    CamposBD(34) = "morada"
    CamposBD(35) = "cod_postal"
    CamposBD(36) = "n_req_assoc"
    CamposBD(37) = "cod_proven"
    CamposBD(38) = "cod_urbano"
    CamposBD(39) = "km"
    CamposBD(40) = "convencao"
    CamposBD(41) = "FLG_COBRA_DOMICILIO"
    CamposBD(42) = "flg_facturado"
    CamposBD(43) = "dt_pretend"
    CamposBD(44) = "flg_aviso_sms"
    CamposBD(45) = "flg_sms_enviada"
    CamposBD(46) = "dt_conclusao"
    CamposBD(47) = "user_confirm"
    CamposBD(48) = "dt_confirm"
    CamposBD(49) = "hr_confirm"
    CamposBD(50) = "obs_proven"
    ' pferreira 2011.05.31
    CamposBD(51) = "flg_hipo"
    CamposBD(52) = "num_doe_prof"
    CamposBD(53) = "cod_pais"
    CamposBD(54) = "flg_diag_sec"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcSeqUtente
    Set CamposEc(2) = EcDataPrevista
    Set CamposEc(3) = EcDataChegada
    Set CamposEc(4) = EcDataImpressao
    Set CamposEc(5) = CbUrgencia
    Set CamposEc(6) = EcCodEFR
    Set CamposEc(7) = EcNumBenef
    If gListaMedicos = 1 Then
        Set CamposEc(8) = EcCodMedico
    Else
        Set CamposEc(8) = EcDescrMedico
    End If
    Set CamposEc(9) = EcObsReq
    Set CamposEc(10) = EcHoraImpressao
    Set CamposEc(11) = EcUtilizadorImpressao
    Set CamposEc(12) = EcDataImpressao2
    Set CamposEc(13) = EcHoraImpressao2
    Set CamposEc(14) = EcUtilizadorImpressao2
    Set CamposEc(15) = EcEstadoReq
    Set CamposEc(16) = EcUtilizadorCriacao
    Set CamposEc(17) = EcHoraCriacao
    Set CamposEc(18) = EcDataCriacao
    Set CamposEc(19) = EcUtilizadorAlteracao
    Set CamposEc(20) = EcHoraAlteracao
    Set CamposEc(21) = EcDataAlteracao
    Set CamposEc(22) = CbTipoIsencao
    Set CamposEc(23) = EcGrupoAna
    Set CamposEc(24) = EcHoraChegada
    Set CamposEc(25) = EcLocal
    Set CamposEc(26) = EcUtilizadorFecho
    Set CamposEc(27) = EcDataFecho
    Set CamposEc(28) = EcHoraFecho
    Set CamposEc(29) = EcinfComplementar
    Set CamposEc(30) = EcPrColheita
    Set CamposEc(31) = EcCodSala
    Set CamposEc(32) = CbDestino
    Set CamposEc(33) = EcHemodialise
    Set CamposEc(34) = EcMorada
    Set CamposEc(35) = EcCodPostalAux
    Set CamposEc(36) = EcNumReqAssoc
    Set CamposEc(37) = EcCodEFR ' atencao CEDIVET
    Set CamposEc(38) = CbCodUrbano
    Set CamposEc(39) = EcKm
    Set CamposEc(40) = CbConvencao
    Set CamposEc(41) = CkCobrarDomicilio
    Set CamposEc(42) = EcFlgFacturado
    Set CamposEc(43) = EcDtPretend
    Set CamposEc(44) = CkAvisarSMS
    Set CamposEc(45) = EcSMSEnviada
    Set CamposEc(46) = EcDtEntrega
    Set CamposEc(47) = EcUtilConf
    Set CamposEc(48) = EcDtConf
    Set CamposEc(49) = EcHrConf
    Set CamposEc(50) = EcObsProveniencia
    ' pferreira 2011.05.31
    Set CamposEc(51) = CkReqHipocoagulados
    Set CamposEc(52) = EcDoenteProf
    Set CamposEc(53) = EcCodPais
    Set CamposEc(54) = EcFlgDiagSec
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Utente"
    TextoCamposObrigatorios(2) = "Data activa��o da requisi��o"
    TextoCamposObrigatorios(5) = "Tipo de urg�ncia"
    TextoCamposObrigatorios(22) = "Factura��o"
    
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "n_req"
    Set ChaveEc = EcNumReq
                
    'inicializa��es das estruturas de mem�ria (an�lises e recibos)
    Inicializa_Estrutura_Analises
    
    'O TAB de an�lises est� desactivado at� se introduzir uma entidade financeira
    ' e o de produtos at� introduzir uma data prevista
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabEnabled(5) = False
    SSTGestReq.TabEnabled(6) = False

    FlgARS = False
    FlgADSE = False
    gFlg_JaExisteReq = False
    Flg_LimpaCampos = False
    
    BtPesquisaEspecif.Enabled = False
    BtPesquisaProduto.Enabled = False
    
    'colocar o bot�o de cancelar requisi��es invisivel
    BtCancelarReq.Visible = False
    
    
    'Inicializa comando para ir buscar as an�lises da requisi��o e os seus dados
    
    If gSGBD = gOracle Then
        CmdAnaReq = "( select  -1 seq_realiza, m.n_folha_trab, m.n_req, m.dt_chega, m.cod_agrup, m.ord_ana_perf, m.ord_ana_compl, m.ord_ana, m.cod_perfil, descr_perfis, m.cod_ana_c, descr_ana_c, m.cod_ana_s, descr_ana_s, m.flg_apar_trans, '-1' as flg_estado, m.flg_facturado,m.ord_marca, m.transmit_psm, m.n_envio from sl_marcacoes m, sl_perfis p, sl_ana_c c, sl_ana_s s where m.n_req = ? and p.cod_perfis (+) = m.cod_perfil and c.cod_ana_c (+) = m.cod_ana_c and s.cod_ana_s (+) = m.cod_ana_s   )" & _
                                " Union " & _
                                "( select  r.seq_realiza, r.n_folha_trab, r.n_req, r.dt_chega, r.cod_agrup, r.ord_ana_perf, r.ord_ana_compl, r.ord_ana, r.cod_perfil, descr_perfis, r.cod_ana_c, descr_ana_c, r.cod_ana_s, descr_ana_s, '-1' as flg_apar_trans, r.flg_estado, r.flg_facturado,r.ord_marca, 1 transmit_psm, 1 n_envio from sl_realiza r, sl_perfis p, sl_ana_c c, sl_ana_s s where r.n_req = ? and p.cod_perfis (+) = r.cod_perfil and c.cod_ana_c (+) = r.cod_ana_c and s.cod_ana_s (+) = r.cod_ana_s )" & _
                                "order by ord_marca,ord_ana, cod_agrup, ord_ana_perf, ord_ana_compl"
    ElseIf gSGBD = gSqlServer Then
        CmdAnaReq = "( Select  -1 seq_realiza, m.n_folha_trab, m.n_req, m.dt_chega, m.cod_agrup, m.ord_ana_perf, m.ord_ana_compl, m.ord_ana, m.cod_perfil, descr_perfis, m.cod_ana_c, descr_ana_c, m.cod_ana_s, descr_ana_s, m.flg_apar_trans, '-1' as flg_estado, m.flg_facturado,m.ord_marca, m.transmit_psm, m.n_envio from sl_marcacoes m LEFT OUTER JOIN sl_perfis p ON p.cod_perfis = m.cod_perfil LEFT OUTER JOIN sl_ana_c c on c.cod_ana_c = m.cod_ana_c LEFT OUTER JOIN sl_ana_s s ON s.cod_ana_s = m.cod_ana_s   where m.n_req =  ?  )" & _
                                " Union " & _
                                "( select  r.seq_realiza, r.n_folha_trab, r.n_req, r.dt_chega, r.cod_agrup, r.ord_ana_perf, r.ord_ana_compl, r.ord_ana, r.cod_perfil, descr_perfis, r.cod_ana_c, descr_ana_c, r.cod_ana_s, descr_ana_s, '-1' as flg_apar_trans, r.flg_estado, r.flg_facturado,r.ord_marca,1 transmit_psm, 1 n_envio from  sl_realiza r LEFT OUTER JOIN sl_perfis p ON p.cod_perfis = r.cod_perfil LEFT OUTER JOIN sl_ana_c c ON c.cod_ana_c = r.cod_ana_c LEFT OUTER JOIN sl_ana_s s ON s.cod_ana_s = r.cod_ana_s  where r.n_req =  ? )" & _
                                "order by ord_marca, ord_ana, cod_agrup, ord_ana_perf, ord_ana_compl"
    End If
    
    ' HISTORICO
    If gSGBD = gOracle Then
        CmdAnaReq_H = "select  h.seq_realiza, h.n_folha_trab, h.n_req, h.dt_chega, h.cod_agrup, h.ord_ana_perf, h.ord_ana_compl, h.ord_ana, h.cod_perfil, descr_perfis, h.cod_ana_c, descr_ana_c, h.cod_ana_s, descr_ana_s, '-1' as flg_apar_trans, h.flg_estado, h.flg_facturado,h.ord_marca, 1 transmit_psm, 1 n_envio from sl_realiza_h h, sl_perfis p, sl_ana_c c, sl_ana_s s where h.n_req = ? and p.cod_perfis (+) = h.cod_perfil and c.cod_ana_c (+) = h.cod_ana_c and s.cod_ana_s (+) = h.cod_ana_s " & _
                                "order by ord_marca,ord_ana, cod_agrup, ord_ana_perf, ord_ana_compl"
    ElseIf gSGBD = gSqlServer Then
        CmdAnaReq_H = " select  h.seq_realiza,h.n_folha_trab, h.n_req, h.dt_chega, h.cod_agrup, h.ord_ana_perf, h.ord_ana_compl, h.ord_ana, h.cod_perfil, descr_perfis, h.cod_ana_c, descr_ana_c, h.cod_ana_s, descr_ana_s, '-1' as flg_apar_trans, h.flg_estado, h.flg_facturado,h.ord_marca, 1 transmit_psm, 1 n_envio from  sl_realiza_h h LEFT OUTER JOIN sl_perfis p ON p.cod_perfis = h.cod_perfil LEFT OUTER JOIN sl_ana_c c ON c.cod_ana_c = h.cod_ana_c LEFT OUTER JOIN sl_ana_s s ON s.cod_ana_s = h.cod_ana_s  where h.n_req =  ? " & _
                                "order by ord_marca, ord_ana, cod_agrup, ord_ana_perf, ord_ana_compl"
    End If
            
        
    'Inicializa comando para ir buscar a ordem e o grupo das an�lises
    Set CmdOrdAna.ActiveConnection = gConexao
    CmdOrdAna.CommandType = adCmdText
    CmdOrdAna.CommandText = " SELECT ordem, gr_ana FROM sl_ana_s WHERE cod_ana_s = ? " & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_ana_c WHERE cod_ana_c = ? " & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_perfis WHERE cod_perfis = ? "
    CmdOrdAna.Prepared = True

        
    'Inicializa��o das variaveis que guardam os indices nas combos de pagamentos
    CbTMIndex = -1
    CbUteIndex = -1
    CbEntIndex = -1
    
    EcPrinterRecibo = BL_SelImpressora("ReciboReq.rpt")
    EcPrinterEtiq = BL_SelImpressora("Etiqueta.rpt")
    
    FrameObsEspecif.Visible = False
    FrameObsEspecif.left = 6120
    FrameObsEspecif.top = 360
    
    Flg_Gravou = False
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    If EcFimSemana = "" Then
        If Req_FDS = True Then
            EcFimSemana = "1"
        Else
            EcFimSemana = "0"
        End If
    End If
    flg_preencheCampos = False
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Inicializacoes: " & Err.Number & " - " & Err.Description, Me.Name, "Inicializacoes"
    Exit Sub
    Resume Next
End Sub

Function FuncaoProcurar_PreencheProd(NumReq As Long)
    On Error GoTo TrataErro
    
    ' Selecciona todos os produtos da requisi��o e preenche a lista

    Dim i As Long
    Dim rsDescr As ADODB.recordset
    Dim RsProd As ADODB.recordset
    Dim CmdDescrP As New ADODB.Command
    Dim PmtCodProd As ADODB.Parameter
    Dim CmdDescrE As New ADODB.Command
    Dim PmtCodEsp As ADODB.Parameter
      
    Set RsProd = New ADODB.recordset
    With RsProd
        .Source = "SELECT cod_prod, cod_especif,dt_previ, dt_chega, volume, obs_especif FROM sl_req_prod " & _
                   "WHERE n_req =" & NumReq
                   
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With

    'Inicializa o comando ADO para selec��o da descri��o do produto
    Set CmdDescrP.ActiveConnection = gConexao
    CmdDescrP.CommandType = adCmdText
    CmdDescrP.CommandText = "SELECT descr_produto,especif_obrig FROM sl_produto WHERE " & _
                            "cod_produto =?"
    CmdDescrP.Prepared = True
    Set PmtCodProd = CmdDescrE.CreateParameter("COD_PRODUTO", adVarChar, adParamInput, 5)
    CmdDescrP.Parameters.Append PmtCodProd
    
    'Inicializa o comando ADO para selec��o da descri��o da especifica��o
     Set CmdDescrE.ActiveConnection = gConexao
     CmdDescrE.CommandType = adCmdText
     CmdDescrE.CommandText = "SELECT descr_especif FROM sl_especif WHERE " & _
                              "cod_especif =?"
     CmdDescrE.Prepared = True
     Set PmtCodEsp = CmdDescrE.CreateParameter("COD_ESPECIF", adVarChar, adParamInput, 5)
     CmdDescrE.Parameters.Append PmtCodEsp
     
     i = 1
     LimpaFgProd
     While Not RsProd.EOF
        RegistosP(i).CodProd = RsProd!cod_prod
        RegistosP(i).Volume = BL_HandleNull(RsProd!Volume, "")
        CmdDescrP.Parameters(0).value = Trim(UCase(RsProd!cod_prod))
        Set rsDescr = CmdDescrP.Execute
        RegistosP(i).DescrProd = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(i).EspecifObrig = rsDescr!especif_obrig
        RegistosP(i).ObsEspecif = BL_HandleNull(RsProd!obs_especif, "")
        rsDescr.Close

        RegistosP(i).CodEspecif = BL_HandleNull(RsProd!cod_Especif, "")
        'descri��o da especifica��o
        If Trim(RegistosP(i).CodEspecif) <> "" Then
            CmdDescrE.Parameters(0) = UCase(RsProd!cod_Especif)
            Set rsDescr = CmdDescrE.Execute
            RegistosP(i).DescrEspecif = BL_HandleNull(rsDescr!descr_especif, "")
            rsDescr.Close
        End If
        RegistosP(i).DtPrev = BL_HandleNull(RsProd!dt_previ, "")
        RegistosP(i).DtChega = BL_HandleNull(RsProd!dt_chega, "")
        
        RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
        
        FgProd.TextMatrix(i, 0) = RegistosP(i).CodProd
        FgProd.TextMatrix(i, 1) = RegistosP(i).DescrProd
        FgProd.TextMatrix(i, 2) = RegistosP(i).CodEspecif
        FgProd.TextMatrix(i, 3) = RegistosP(i).DescrEspecif
        FgProd.TextMatrix(i, 4) = RegistosP(i).Volume
        FgProd.TextMatrix(i, 5) = RegistosP(i).DtPrev
        FgProd.TextMatrix(i, 6) = RegistosP(i).DtChega
        FgProd.TextMatrix(i, 7) = RegistosP(i).EstadoProd
        
        'Se h� produtos que j� chegaram ent�o estado n�o deve estar "� espera de prod."
        If (Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto) And Trim(FgProd.TextMatrix(i, 5)) <> "" Then
            EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        End If
        
        RsProd.MoveNext
        i = i + 1
        CriaNovaClasse RegistosP, i, "PROD"
        FgProd.AddItem "", i
        FgProd.row = i
     Wend
            
     Set CmdDescrE = Nothing
     Set CmdDescrP = Nothing
     Set rsDescr = Nothing
     Set RsProd = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar_PreencheProd: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar_PreencheProd"
    Exit Function
    Resume Next
End Function

Sub FuncaoProcurar_PreencheAna(NumReq As Long)
    On Error GoTo TrataErro

    Dim i As Long
    Dim k As Long
    Dim MudaIcon As Integer
    Dim LastIntermedio As String
    Dim LastPai As String
    Dim Pai As String
    Dim Ja_Existe As Boolean
    Dim rsAna As ADODB.recordset
    Dim EstadoFinal As Integer
    Dim EArs As Boolean
    Dim ImgPai As Integer
    Dim ImgPaiSel As Integer
    Dim ObsAnaReq As String
    Dim seqObsAnaReq As Long
    Dim sql As String
    Dim flg_estado_aux As String
    
    ' PARA REQUISICOES QUE ESTAO NO HISTORICO APENAS VAI TABELA SL_REALIZA_H
    If EcEstadoReq = gEstadoReqHistorico Then
        sql = Replace(CmdAnaReq_H, "?", NumReq)
    Else
        sql = Replace(CmdAnaReq, "?", NumReq)
    End If
    
    Set rsAna = New ADODB.recordset
    With rsAna
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockPessimistic
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open
    End With
i = 1
LimpaFGAna
LimpaFGAnaEfr
ExecutaCodigoA = False
ContP1 = 0
FGAna.Col = 0
LastIntermedio = ""
LastPai = ""
Pai = ""
MudaIcon = 0
ImgPai = 5
ImgPaiSel = 6

EArs = EFR_Verifica_ARS(EcCodEFR)

While Not rsAna.EOF
    flg_estado_aux = BL_HandleNull(rsAna!flg_estado, "-1")
    If BL_HandleNull(rsAna!flg_estado, "-1") <> "-1" Then
        If VerificaAnaliseJaExisteRealiza(NumReq, BL_HandleNull(rsAna!Cod_Perfil, ""), BL_HandleNull(rsAna!cod_ana_c, ""), BL_HandleNull(rsAna!cod_ana_s, ""), False) = 0 Then
            flg_estado_aux = -1
        Else
            flg_estado_aux = BL_HandleNull(rsAna!flg_estado, "-1")
        End If
    Else
        flg_estado_aux = BL_HandleNull(rsAna!flg_estado, "-1")
    End If
    
        'Debug.Print RsAna!Cod_Ana_C & "-" & RsAna!cod_ana_s
        BL_Preenche_MAReq FGTubos, BL_HandleNull(rsAna!Cod_Perfil, ""), _
                        BL_HandleNull(rsAna!descr_perfis, ""), _
                        BL_HandleNull(rsAna!cod_ana_c, ""), _
                        BL_HandleNull(rsAna!Descr_Ana_C, ""), _
                        BL_HandleNull(rsAna!cod_ana_s, ""), _
                        BL_HandleNull(rsAna!descr_ana_s, ""), _
                        BL_HandleNull(rsAna!Ord_Ana, 0), _
                        BL_HandleNull(rsAna!Ord_Ana_Compl, 0), _
                        BL_HandleNull(rsAna!Ord_Ana_Perf, 0), _
                        BL_HandleNull(rsAna!cod_agrup, ""), _
                        BL_HandleNull(rsAna!N_Folha_Trab, 0), _
                        BL_HandleNull(rsAna!dt_chega, ""), _
                        BL_HandleNull(rsAna!flg_apar_trans, "0"), _
                        flg_estado_aux, _
                        BL_HandleNull(rsAna!Flg_Facturado, 0), _
                         BL_HandleNull(rsAna!Ord_Marca, 0), _
                         BL_HandleNull(rsAna!transmit_psm, 0), _
                         BL_HandleNull(rsAna!n_envio, 0), EcDataPrevista
            
        Ja_Existe = False
        For k = 1 To RegistosA.Count
            If Trim(RegistosA(k).codAna) = Trim(MaReq(i).cod_agrup) Then
                Ja_Existe = True
                Exit For
            End If
        Next k


        If Flg_DadosAnteriores = True Then
            'For�a o estado da an�lise para sem resultado quando se copia uma requisi��o
            MaReq(i).flg_estado = "-1"
        End If
        
        If MaReq(i).flg_estado = "-1" Then
            MudaIcon = 0
        Else
            MudaIcon = 6
        End If

        If Not Ja_Existe Then
            LbTotalAna = LbTotalAna + 1
            RegistosA(FGAna.rows - 1).codAna = MaReq(i).cod_agrup
            If Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).Cod_Perfil) Then
                RegistosA(FGAna.rows - 1).descrAna = MaReq(i).Descr_Perfil
                ImgPai = 1 + MudaIcon
                ImgPaiSel = 2 + MudaIcon
            ElseIf Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).cod_ana_c) Then
                RegistosA(FGAna.rows - 1).descrAna = MaReq(i).Descr_Ana_C
                ImgPai = 3 + MudaIcon
                ImgPaiSel = 4 + MudaIcon
            ElseIf Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).cod_ana_s) Then
                RegistosA(FGAna.rows - 1).descrAna = MaReq(i).descr_ana_s
                ImgPai = 5 + MudaIcon
                ImgPaiSel = 6 + MudaIcon
            Else
                RegistosA(FGAna.rows - 1).descrAna = ""
                ImgPai = 5 + MudaIcon
                ImgPaiSel = 6 + MudaIcon
            End If
            
            ObsAnaReq = ""
            seqObsAnaReq = 0
            BL_DevolveObsAnaReq NumReq, MaReq(i).cod_agrup, ObsAnaReq, seqObsAnaReq
            RegistosA(FGAna.rows - 1).ObsAnaReq = ObsAnaReq
            RegistosA(FGAna.rows - 1).seqObsAnaReq = seqObsAnaReq
            
            ' ------------------------------------------------------------------------------------------
            ' PREENCHE DADOS DOS RECIBOS ASSOCIADO A ANALISE EM CAUSA
            ' ------------------------------------------------------------------------------------------
            FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaCodigo) = RegistosA(FGAna.rows - 1).codAna
            FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaDescricao) = RegistosA(FGAna.rows - 1).descrAna
            RegistosA(FGAna.rows - 1).DtChega = BL_HandleNull(rsAna!dt_chega, "")
            
            If MaReq(i).flg_estado <> "-1" And Trim(FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaCodigo)) <> "" Then
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbRed
            Else
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbBlack
            End If
            
            
            CriaNovaClasse RegistosA, 0, "ANA"
            FGAna.AddItem ""
            FGAna.row = FGAna.rows - 1
        
        End If
        
        Pai = Trim(MaReq(i).cod_agrup)
        
        If Pai <> LastPai Then
            LastPai = Pai
        End If
        
        
        If Trim(MaReq(i).cod_ana_c) <> "0" And Trim(MaReq(i).cod_ana_c) <> "" And Trim(Pai) <> Trim(MaReq(i).cod_ana_c) Then
            'Se existir complexa e for diferente do Pai insere-a como intermedia entre o Pai e a simples
            If LastIntermedio = MaReq(i).cod_ana_c Then
                'j� foi inserida uma vez -> insere-se directamente a simples
            Else
                '� a primeira vez -> insere a complexa e a simples por debaixo da mesma
                LastIntermedio = MaReq(i).cod_ana_c
                
            End If
        Else
            'N�o existe complexa ou � o pai
            If Trim(Pai) <> Trim(MaReq(i).cod_ana_s) Then
                'Se o pai nao for a propria simples insere-a de baixo do pai
                
            End If
        End If
        
        
        i = i + 1
        rsAna.MoveNext
        
        'Se encontrou analises o estado n�o deve estar "Sem Analises"
        If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Then
            EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        End If
Wend

For i = 1 To FGAna.rows - 2
    EstadoFinal = -1
    For k = 1 To UBound(MaReq)
        If Trim(RegistosA(i).codAna) = Trim(MaReq(k).cod_agrup) Then
            If MaReq(k).flg_estado <> "-1" Then
                EstadoFinal = 0
                Exit For
            End If
        End If
    Next k
    RegistosA(i).Estado = EstadoFinal
Next i
RegistosA(FGAna.rows - 1).Estado = "-1"

rsAna.Close
Set rsAna = Nothing

ExecutaCodigoA = True

FGAna.Col = 0
FGAna.row = 1
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar_PreencheAna: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar_PreencheAna"
    Exit Sub
    Resume Next
End Sub

Sub DefTipoCampos()
     On Error GoTo TrataErro
    If gLAB = "CL" Then
        CbDestino.BackColor = &HC0FFFF
    Else
        CbDestino.BackColor = vbWhite
    End If
    If gGestaoColheita <> mediSim Then
        LbNumColheita.Visible = False
        EcNumColheita.Visible = False
    Else
        LbNumColheita.Visible = True
        EcNumColheita.Visible = True
    End If
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_previ", EcDataPrevista, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_chega", EcDataChegada, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_imp", EcDataImpressao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_imp", EcHoraImpressao, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_imp2", EcDataImpressao2, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_imp2", EcHoraImpressao2, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_fecho", EcDataFecho, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_pretend", EcDtPretend, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_conclusao", EcDtEntrega, mediTipoData
    
    BG_DefTipoCampoEc_ADO NomeTabela, "user_imp", EcUtilizadorImpressao, adInteger
    BG_DefTipoCampoEc_ADO NomeTabela, "user_imp2", EcUtilizadorImpressao2, adInteger
    BG_DefTipoCampoEc_ADO NomeTabela, "user_cri", EcUtilizadorCriacao, adInteger
    BG_DefTipoCampoEc_ADO NomeTabela, "user_act", EcUtilizadorAlteracao, adInteger
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_confirm", EcDtConf, mediTipoData
    
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    ExecutaCodigoP = False
    
    With FGAna
        .rows = 2
        .FixedRows = 1
        .Cols = 8
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        
        .ColWidth(lColFgAnaCodigo) = 1000
        .Col = lColFgAnaCodigo
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaCodigo) = "C�digo"
        
        .ColWidth(lColFgAnaDescricao) = 5000
        .Col = lColFgAnaDescricao
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaDescricao) = "Descri��o"
        
        .ColWidth(lColFgAnaP1) = 0
        .Col = lColFgAnaP1
        .TextMatrix(0, lColFgAnaP1) = "Credencial"
        
        .ColWidth(lColFgAnaMedico) = 0
        .Col = lColFgAnaMedico
        .TextMatrix(0, lColFgAnaMedico) = "M�dico"
        
        .ColWidth(lColFgAnaCodEFR) = 0
        .Col = lColFgAnaCodEFR
        .TextMatrix(0, lColFgAnaCodEFR) = ""
        
        .ColWidth(lColFgAnaDescrEFR) = 0
        .Col = lColFgAnaDescrEFR
        .TextMatrix(0, lColFgAnaDescrEFR) = "Entidade"
        
        .ColWidth(lColFgAnaNrBenef) = 0
        .Col = lColFgAnaNrBenef
        .TextMatrix(0, lColFgAnaNrBenef) = "Nr. Benef."
        
        .ColWidth(lColFgAnaPreco) = 0
        .Col = lColFgAnaPreco
        .TextMatrix(0, lColFgAnaPreco) = "Taxa �"
        
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    LastColA = 0
    LastRowA = 1
    CriaNovaClasse RegistosA, 1, "ANA", True
    
    With FgAnaEFR
        .rows = 2
        .FixedRows = 1
        .Cols = 15
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        
        .ColWidth(lColFgAnaEFRlinha) = 0
        .Col = lColFgAnaEFRlinha
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaEFRlinha) = "Linha"
        
        .ColWidth(lColFgAnaEFRCodigo) = 800
        .Col = lColFgAnaEFRCodigo
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaEFRCodigo) = "C�digo"
        
        .ColWidth(lColFgAnaEFRDescricao) = 2500
        .Col = lColFgAnaEFRDescricao
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaEFRDescricao) = "Descri��o"
        
        .ColWidth(lColFgAnaEFRP1) = 1250
        .Col = lColFgAnaEFRP1
        .TextMatrix(0, lColFgAnaEFRP1) = "Credencial"
        
        .ColWidth(lColFgAnaEFROrdemP1) = 600
        .Col = lColFgAnaEFROrdemP1
        .TextMatrix(0, lColFgAnaEFROrdemP1) = "Ordem"
        
        .ColWidth(lColFgAnaEFRMedico) = 800
        .Col = lColFgAnaEFRMedico
        .TextMatrix(0, lColFgAnaEFRMedico) = "M�dico"
        
        .ColWidth(lColFgAnaEFRCodEFR) = 0
        .Col = lColFgAnaEFRCodEFR
        .TextMatrix(0, lColFgAnaEFRCodEFR) = ""
                
        .ColWidth(lColFgAnaEFRNrBenef) = 1100
        .Col = lColFgAnaEFRNrBenef
        .TextMatrix(0, lColFgAnaEFRNrBenef) = "Nr. Benef."
        
        .ColWidth(lColFgAnaEFRQtd) = 500
        .Col = lColFgAnaEFRQtd
        .TextMatrix(0, lColFgAnaEFRQtd) = "Qtd."
        
        
        .ColWidth(lColFgAnaEFRIsen) = 400
        .Col = lColFgAnaEFRIsen
        .TextMatrix(0, lColFgAnaEFRIsen) = "Ise."
        
        .ColWidth(lColFgAnaEFRFact) = 400
        .Col = lColFgAnaEFRFact
        .TextMatrix(0, lColFgAnaEFRFact) = "Fact"
        
        .ColWidth(lColFgAnaEFRCB) = 300
        .Col = lColFgAnaEFRCB
        .TextMatrix(0, lColFgAnaEFRCB) = "CB"
        
        .ColWidth(lColFgAnaEFRRec) = 900
        .Col = lColFgAnaEFRRec
        .TextMatrix(0, lColFgAnaEFRRec) = "Recibo"
        
        .ColWidth(lColFgAnaEFRPreco) = 600
        .Col = lColFgAnaEFRPreco
        .TextMatrix(0, lColFgAnaEFRPreco) = "Taxa �"
        
        .ColWidth(lColFgAnaEFRPrecoEFR) = 1000
        .Col = lColFgAnaEFRPrecoEFR
        .TextMatrix(0, lColFgAnaEFRPrecoEFR) = "Pre�o EFR �"
        
        .WordWrap = False
        .row = 1
        .Col = 0
    End With

    With FgProd
        .rows = 2
        .FixedRows = 1
        .Cols = 8
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 900
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, 0) = "Produto"
        .ColWidth(1) = 2700
        .Col = 1
        .TextMatrix(0, 1) = "Descri��o"
        .ColWidth(2) = 850
        .Col = 2
        .TextMatrix(0, 2) = "Especifica."
        .ColWidth(3) = 2700
        .Col = 3
        .TextMatrix(0, 3) = "Descri��o"
        .Col = 4
        .ColWidth(4) = 750
        .TextMatrix(0, 4) = "Volume"
        .ColWidth(5) = 900
        .Col = 5
        .TextMatrix(0, 5) = "Previsto"
        .ColWidth(6) = 900
        .Col = 6
        .TextMatrix(0, 6) = "Chegada"
        .ColWidth(7) = 600
        .Col = 7
        .TextMatrix(0, 7) = "Estado"
        .WordWrap = False
        .row = 1
        .Col = 0
    End With

    LastColP = 0
    LastRowP = 1
    CriaNovaClasse RegistosP, 1, "PROD", True
    
    
    TB_DefTipoFGTubos FGTubos

    With FGRecibos
        .rows = 2
        .FixedRows = 1
        .Cols = 9
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        
        .ColWidth(0) = 1500
        
        .Col = lColRecibosNumDoc
        .TextMatrix(0, lColRecibosNumDoc) = "N�mero"
        .ColAlignment(lColRecibosNumDoc) = flexAlignLeftCenter
        
        .ColWidth(lColRecibosEntidade) = 2500
        .Col = lColRecibosEntidade
        .TextMatrix(0, lColRecibosEntidade) = "Entidade"
        
        .ColWidth(lColRecibosValorPagar) = 700
        .Col = lColRecibosValorPagar
        .TextMatrix(0, lColRecibosValorPagar) = "Valor �"
        
        .ColWidth(lColRecibosCaucao) = 0
        .Col = lColRecibosCaucao
        .TextMatrix(0, lColRecibosCaucao) = "Cau��o"
                
        .ColWidth(lColRecibosDtEmissao) = 1500
        .Col = lColRecibosDtEmissao
        .TextMatrix(0, lColRecibosDtEmissao) = "Data Emiss�o"
        
        .ColWidth(lColRecibosDtPagamento) = 1500
        .Col = lColRecibosDtPagamento
        .TextMatrix(0, lColRecibosDtPagamento) = "Data Pagamento"
        
        .ColWidth(lColRecibosEstado) = 1500
        .Col = lColRecibosEstado
        .TextMatrix(0, lColRecibosEstado) = "Estado"
        
        .ColWidth(lColRecibosPercentagem) = 1000
        .Col = lColRecibosPercentagem
        .TextMatrix(0, lColRecibosPercentagem) = "Percent %"
        
        .ColWidth(lColRecibosModoPag) = 1200
        .Col = lColRecibosModoPag
        .TextMatrix(0, lColRecibosModoPag) = "Modo Pag."
        
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    
    With FgAdiantamentos
        .rows = 2
        .FixedRows = 1
        .Cols = 5
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        
        .ColWidth(0) = 1200
        
        .Col = lColAdiantamentosNumDoc
        .TextMatrix(0, lColAdiantamentosNumDoc) = "N�mero"
        .ColAlignment(lColAdiantamentosNumDoc) = flexAlignLeftCenter
        
        
        .ColWidth(lColAdiantamentosValor) = 1000
        .Col = lColAdiantamentosValor
        .TextMatrix(0, lColAdiantamentosValor) = "Valor �"
        
                
        .ColWidth(lColAdiantamentosDtEmissao) = 1500
        .Col = lColAdiantamentosDtEmissao
        .TextMatrix(0, lColAdiantamentosDtEmissao) = "Data Emiss�o"
        
        .ColWidth(lColAdiantamentosDtAnulacao) = 1500
        .Col = lColAdiantamentosDtAnulacao
        .TextMatrix(0, lColAdiantamentosDtAnulacao) = "Data Anula��o"
        
        
        .ColWidth(lColAdiantamentosEstado) = 2000
        .Col = lColAdiantamentosEstado
        .TextMatrix(0, lColAdiantamentosEstado) = "Estado"
        
        
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    
    With FgAnaRec
        .rows = 2
        .FixedRows = 1
        .Cols = 4
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        
        .ColWidth(lColFgAnaRecCodAna) = 1000
        .Col = lColFgAnaRecCodAna
        .TextMatrix(0, lColFgAnaRecCodAna) = "C�d Ana"
        .ColAlignment(lColFgAnaRecCodAna) = flexAlignLeftCenter
        
        .ColWidth(lColFgAnaRecDescrAna) = 3000
        .Col = lColFgAnaRecDescrAna
        .TextMatrix(0, lColFgAnaRecDescrAna) = "An�lise"
        
        .ColWidth(lColFgAnaRecQtd) = 600
        .Col = lColFgAnaRecQtd
        .TextMatrix(0, lColFgAnaRecQtd) = "Qtd"
        
        .ColWidth(lColFgAnaRecTaxa) = 1000
        .Col = lColFgAnaRecTaxa
        .TextMatrix(0, lColFgAnaRecTaxa) = "Valor �"
        
        .row = 1
        .Col = 0
    End With
    
    With FgInformacao
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        
        .ColWidth(0) = 2000
        .Col = 0
        .TextMatrix(0, 0) = "Nome"
        .ColAlignment(0) = flexAlignLeftCenter
        
        .ColWidth(1) = 8000
        .Col = 1
        .TextMatrix(0, 1) = "Informacao"
        .ColAlignment(1) = flexAlignLeftCenter
         
        .row = 1
        .Col = 0
    End With
    
    With FgQuestoes
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        
        .ColWidth(0) = 7000
        .Col = 0
        .TextMatrix(0, 0) = "Quest�o"
        .ColAlignment(0) = flexAlignLeftCenter
        
        .ColWidth(1) = 3000
        .Col = 1
        .TextMatrix(0, 1) = "Resposta"
        .ColAlignment(1) = flexAlignLeftCenter
         
        .row = 1
        .Col = 0
    End With
    EcAuxRec.MaxLength = 10
    
    
    Flg_PergProd = False
    Resp_PergProd = False
    Flg_DtChProd = False
    Resp_PergDtChProd = False
    
    Flg_PergTubo = False
    Resp_PergTubo = False
    Flg_DtChTubo = False
    Resp_PergDtChTubo = False
    
    
    ExecutaCodigoA = False
    ExecutaCodigoP = False
    

    SSTGestReq.Enabled = True
    
    BtResumo.Enabled = False
    
    FrameObsAna.Visible = False
    
    FrameDomicilio.Visible = False
    CkCobrarDomicilio.value = vbGrayed
    FrameAnaRecibos.Visible = False
    CkAvisarSMS.value = vbGrayed
    GereFrameAdiantamentos False
    CbHemodialise.Enabled = False
    SSTGestReq.TabVisible(0) = False
    SSTGestReq.TabVisible(2) = False
    'SSTGestReq.TabVisible(4) = False
    SSTGestReq.TabVisible(5) = False
    SSTGestReq.TabVisible(6) = False
    PreencheEntidadesDefeito
    CkReqHipocoagulados.value = vbGrayed
    EcFimSemana = ""
    
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    ReDim reqColh(0)
    TotalReqColh = 0
    PM_LimpaEstrutura
Exit Sub
TrataErro:
    BG_LogFile_Erros "DefTipoCampos: " & Err.Number & " - " & Err.Description, Me.Name, "DefTipoCampos"
    Exit Sub
    Resume Next
End Sub

Sub PreencheDadosTubo(indice As Long, _
                      DescrGrAna As String, _
                      abrAna As String, _
                      CodProd As String, _
                      CodTubo As String, _
                      Especial As Boolean, _
                      Cheio As Boolean, _
                      codTuboBar As String, _
                      QtMax As Double, _
                      QtOcup As Double, _
                      Designacao As String, _
                      inf_complementar As String, _
                      num_copias As Integer, _
                      Descr_etiq_ana As String, _
                      num_ana As Integer, _
                      num_max_ana As Integer, norma_tubo As String)
    On Error GoTo TrataErro

    Tubos(indice).CodProd = CodProd
    Tubos(indice).CodTubo = CodTubo
    Tubos(indice).GrAna = Trim(DescrGrAna)
    
    ' Espec�fico HUCs.
    ' Impress�o de etiquetas.
    If gLAB = cHUCs Then
        'OrdAna = 1
    End If
    
    'If OrdAna <> 0 Then
        Tubos(indice).abrAna = abrAna
    'Else
    '    Tubos(Indice).AbrAna = ""
    'End If
    
    Tubos(indice).Especial = Especial
    Tubos(indice).Cheio = Cheio
    Tubos(indice).codTuboBar = codTuboBar
    Tubos(indice).QtMax = QtMax
    Tubos(indice).QtOcup = QtOcup
    Tubos(indice).Designacao_tubo = Designacao
    Tubos(indice).inf_complementar = inf_complementar
    Tubos(indice).num_copias = num_copias
    Tubos(indice).num_ana = num_ana
    Tubos(indice).num_max_ana = num_max_ana
    Tubos(indice).Descr_etiq_ana = Descr_etiq_ana
    Tubos(indice).normaTubo = norma_tubo
    Tubos(indice).dt_chega = BL_RetornaDtChegaTubo(EcNumReq, CodTubo)
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDadosTubo: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDadosTubo"
    Exit Sub
    Resume Next
End Sub

Sub PreencheValoresDefeito()
    On Error GoTo TrataErro
    
    EcNumColheita.text = ""
    
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    
    
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia, mediAscComboCodigo
    
    BG_PreencheComboBD_ADO "sl_isencao", "cod_isencao", "descr_isencao", CbTipoIsencao
    
    ' PFerreira 08.03.2007
    BG_PreencheComboBD_ADO "sl_cod_prioridades", "cod_prio", "nome_prio", CbPrioColheita, mediAscComboCodigo
    
    ' PFerreira 16.03.2007
    BG_PreencheComboBD_ADO "sl_cod_salas", "seq_sala", "descr_sala", CbSala, mediAscComboCodigo
    
    ' PFerreira 04.04.2007
    BG_PreencheComboBD_ADO "sl_tbf_t_destino", "cod_t_dest", "descr_t_dest", CbDestino, mediAscComboCodigo
    
    
    BG_PreencheComboBD_ADO "sl_cod_hemodialise", "seq_hemodialise", "descr_hemodialise", CbHemodialise, mediAscComboDesignacao

    BG_PreencheComboBD_ADO "sl_tbf_t_urbano", "cod_t_urbano", "descr_t_urbano", CbCodUrbano
    
    BG_PreencheComboBD_ADO "sl_tbf_convencao", "cod_convencao", "descr_convencao", CbConvencao
    
    
    
    TotalEliminadas = 0
    ReDim Eliminadas(0)
    
    Flg_IncrementaP1 = False
    EcAnaEfr = ""
    EcMedAct = ""
    EcCredAct = ""
    EcBenefAct = ""
    EcIsencaoAct = ""
    AlteraFlgCodBarras False
    CbEFR.Clear
    PreencheEntidadesDefeito
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheValoresDefeito: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheValoresDefeito"
    Exit Sub
    Resume Next
End Sub

Sub EventoActivate()
    On Error GoTo TrataErro
    
    Dim i As Integer
    
    If FormGestaoRequisicaoVet.Enabled = False Then
        FormGestaoRequisicaoVet.Enabled = True
    End If
    If flgEmissaoRecibos = True And FGRecibos.rows > 2 Then
        PreencheFgRecibos
        PreencheFgAdiantamentos
        flgEmissaoRecibos = False
    End If
    Set gFormActivo = Me
    
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If
    
    BG_StackJanelas_Actualiza Me
    
    BL_ToolbarEstadoN Estado
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BG_ParametrizaPermissoes_ADO Me.Name
    
    If Estado = 2 Then
       BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        Err.Clear
        
        If EcEstadoReq = gEstadoReqCancelada Then
            
            Select Case Err.Number
                Case 0
                    ' OK
                Case 3021
                    ' BOF ou EOF � verdadeiro ou o registo actual foi eliminado; a opera��o pedida necessita de um registo actual.
                    rs.Requery
                Case Else
                
            End Select
            
            BtCancelarReq.Visible = True
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        Else
            BtCancelarReq.Visible = False
            BL_Toolbar_BotaoEstado "Remover", "Activo"
            BL_Toolbar_BotaoEstado "Modificar", "Activo"
        End If
    End If
       
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    Dim RsPesqUte As ADODB.recordset
    Dim sql As String
    If gDUtente.seq_utente <> "" And (Flg_PesqUtente = True Or gF_IDENTIF_VET = 1 Or gF_IDENTIF_PESQ = 1) Then
        If gDUtente.seq_utente = "-1" And gF_IDENTIF_VET = 0 Then
'            ' UTENTE HIS
            Set RsPesqUte = New ADODB.recordset
            With RsPesqUte
                .Source = "SELECT * from sl_identif where dt_nasc_ute = " & BL_TrataDataParaBD(gDUtente.dt_nasc_ute) & _
                        " and nome_ute = " & BL_TrataStringParaBD(gDUtente.nome_ute) & _
                        " and sexo_ute = " & BL_TrataStringParaBD(gDUtente.sexo_ute) & _
                        " and t_utente = " & BL_TrataStringParaBD(gDUtente.t_utente) & _
                        " and utente = " & BL_TrataStringParaBD(gDUtente.Utente)
                .CursorType = adOpenStatic
                .CursorLocation = adUseServer
                If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                .Open , gConexao
            End With
            If RsPesqUte.RecordCount > 0 Then
                EcSeqUtente = RsPesqUte!seq_utente
                PreencheDadosUtente
            Else
                BD_Insert_Utente
            End If
        ElseIf gDUtente.seq_utente = "-1" And (gF_IDENTIF_VET = 1) Then
            FormIdentificaVet.Flg_PesqUtente = True
            Unload Me
            Exit Sub
        Else
            'j� tem utente
            EcSeqUtente = gDUtente.seq_utente
            PreencheDadosUtente
            Flg_PesqUtente = False
        End If
        Flg_PesqUtente = False

    End If
    '-------------------------------------------------------------------
    
    
    ' PFerreira 10.04.2007
    ' Caso o ecra de identificacao de utentes for o ecra anterior preenche a prioridade de colheita
    If (Flg_PesqUtente = True Or gF_IDENTIF_VET = 1 Or gF_IDENTIF_PESQ = 1) Then
        CbPrioColheita.ListIndex = 1
    End If
    If EcNumReq.Enabled = True And EcNumReq.locked = False Then
        EcNumReq.SetFocus
    End If
    
    If EcNumReq <> "" Then
        PreencheNotas
    End If
    
    If gListaMedicos = 1 Then
        EcDescrMedico.Visible = False
        EcCodMedico.Visible = True
        EcNomeMedico.Visible = True
        BtPesquisaMedico.Visible = True
    End If
    
    'soliveira lacto
    If gFichaCruz = 1 Then
        LaFichaCruz.caption = "Ficha Crz"
    Else
        LaFichaCruz.caption = "Etiquetas"
    End If

'Coloca codigo da sala por defeito

    If gCodSalaDefeito <> "-1" Then

        If EcCodSala.text = "" Then

            EcCodSala.text = gCodSalaDefeito

        End If

    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheValoresDefeito: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheValoresDefeito"
    Exit Sub
    Resume Next
End Sub

Sub PreencheDadosUtente()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim Campos(9) As String
    Dim retorno() As String
    Dim iret As Integer
    
    Campos(0) = "utente"
    Campos(1) = "t_utente"
    Campos(2) = "nome_ute"
    Campos(3) = "dt_nasc_ute"
    Campos(4) = "sexo_ute"
    Campos(5) = "abrev_ute"
    Campos(6) = "nome_dono"
    Campos(7) = "cod_genero"
    Campos(8) = "cod_efr_ute"

    iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)
    
    If iret = -1 Then
        BG_Mensagem mediMsgBox, "Erro a seleccionar utente!", vbCritical + vbOKOnly, App.ProductName
        CbTipoUtente.ListIndex = mediComboValorNull
        EcUtente.text = ""
        Exit Sub
    End If
    EcDataPrevista.text = Bg_DaData_ADO
    EcUtente.text = retorno(0)
    CbTipoUtente.text = retorno(1)
    EcDataPrevista_GotFocus
    EcDataPrevista_Validate False
    EcNome.text = retorno(2)
    EcDataNasc.text = retorno(3)
    If retorno(4) = gT_Masculino Then
        EcSexo = "Masculino"
    Else
        EcSexo = "Feminino"
    End If
    EcAbrevUte.text = retorno(5)
    EcNomeDono.text = retorno(6)
    If EcCodEFR.text = "" Then
        EcCodEFR.text = retorno(8)
        EcCodEFR_Validate False
    End If
    If EcDataNasc <> "" Then
        EcIdade = BG_CalculaIdade(CDate(EcDataNasc.text))
    End If
    EcDescrEspecie = BL_HandleNull(BL_SelCodigo("SL_GENERO", "DESCR_GENERO", "COD_GENERO", retorno(7)), "")
    
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim And Me.ExternalAccess = True Then
    Dim contextJson As String
    Dim responseJson As String
    Dim requestJson As String
   
        contextJson = "{" & Chr(34) & "EcTipoUtente" & Chr(34) & ":" & Chr(34) & retorno(1) & Chr(34) & "," & Chr(34) & "EcUtente" & Chr(34) & ":" & Chr(34) & retorno(0) & Chr(34) & "}"
        
        responseJson = "{" & Chr(34) & "EcTipoUtente" & Chr(34) & ":" & Chr(34) & retorno(1) & ", " & Chr(34) & "EcUtente" & Chr(34) & ":" & Chr(34) & retorno(0) & Chr(34) & _
                ", " & Chr(34) & "EcNome" & Chr(34) & ":" & Chr(34) & retorno(2) & Chr(34) & ", " & Chr(34) & "EcDataNasc" & Chr(34) & ":" & Chr(34) & retorno(3) & Chr(34) & _
                ", " & Chr(34) & "EcSexo" & Chr(34) & ":" & Chr(34) & EcSexo & Chr(34) & ", " & Chr(34) & "EcAbrevUte" & Chr(34) & ":" & Chr(34) & retorno(5) & Chr(34) & _
                ", " & Chr(34) & "EcNomeDono" & Chr(34) & ":" & Chr(34) & retorno(6) & Chr(34) & ", " & Chr(34) & "EcDescrEspecie" & Chr(34) & ":" & Chr(34) & EcDescrEspecie & Chr(34) & "}"
        
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, contextJson, contextJson, IIf(responseJson = vbNullString, "{}", responseJson)

    End If
    
    BtInfClinica.Enabled = True
    BtDomicilio.Enabled = True
    BtConsReq.Enabled = True
    BtIdentif.Enabled = True
    If CbTipoIsencao.ListIndex = mediComboValorNull Then
        For i = 0 To CbTipoIsencao.ListCount - 1
            If CbTipoIsencao.ItemData(i) = gTipoIsencaoNaoIsento Then
                CbTipoIsencao.ListIndex = i
                Exit For
            End If
        Next
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDadosUtente: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDadosUtente"
    Exit Sub
    Resume Next
End Sub

Sub PreencheEFRUtente()
    
    On Error GoTo ErrorHandler
    Dim i As Integer
    Dim Campos(1) As String
    Dim retorno() As String
    Dim iret As Integer
    Dim SQLEFR As String
    Dim tabelaEFR As ADODB.recordset
    Dim SelEntUte As Boolean
    
'    If (Trim(EcCodEFR.Text) = "") Then
    
        SelEntUte = False
        If Trim(EcSeqUtente) <> "" Then
            If rs Is Nothing Then
                SelEntUte = True
            ElseIf rs.state = adStateClosed Then
                SelEntUte = True
            End If
        End If
        
        If SelEntUte = True Then
            Campos(0) = "cod_efr_ute"
            Campos(1) = "n_benef_ute"
            iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)
            
            If iret = -1 Then
                BG_Mensagem mediMsgBox, "Erro a seleccionar E.F.R. do utente!", vbCritical + vbOKOnly, App.ProductName
                Exit Sub
            End If
            
            EcCodEFR.text = retorno(0)
            
            ' PARA ENTIDADE GNR FORMATA O NUMERO DO CARTAO
            If gCodEfrGNR = EcCodEFR And gTipoInstituicao = "PRIVADA" Then
                If Mid(retorno(1), 1, 1) = "G" Then
                    EcNumBenef = Mid(retorno(1), 3, 10)
                Else
                    EcNumBenef = Mid(retorno(1), 1, 10)
                End If
            ' PARA ENTIDADE PT FORMATA O NUMERO DO CARTAO
            ElseIf gCodEfrPT = EcCodEFR And gTipoInstituicao = "PRIVADA" Then
                If Len(Trim(retorno(1))) = 14 Then
                    EcNumBenef.text = Mid(retorno(1), 3, 8) & "/" & Mid(retorno(1), 11, 2)
                    While Mid(EcNumBenef.text, 1, 1) = "0"
                        EcNumBenef.text = Mid(EcNumBenef.text, 2)
                    Wend
                Else
                    EcNumBenef.text = retorno(1)
                End If
            Else
                EcNumBenef.text = retorno(1)
            End If
            
            Set tabelaEFR = New ADODB.recordset
            tabelaEFR.CursorType = adOpenStatic
            tabelaEFR.CursorLocation = adUseServer
            
            SQLEFR = "SELECT " & _
                     "      descr_efr, cod_destino " & _
                     "FROM " & _
                     "      sl_efr " & _
                     "WHERE " & _
                     "      cod_efr = " & Trim(EcCodEFR.text)
            
            If gModoDebug = mediSim Then BG_LogFile_Erros SQLEFR
            tabelaEFR.Open SQLEFR, gConexao
            If tabelaEFR.RecordCount > 0 Then
                EcDescrEFR.text = tabelaEFR!descr_efr
                If CbDestino.ListIndex = mediComboValorNull Then
                    If BL_HandleNull(tabelaEFR!cod_destino, mediComboValorNull) > mediComboValorNull Then
                        For i = 0 To CbDestino.ListCount - 1
                            If CbDestino.ItemData(i) = tabelaEFR!cod_destino Then
                                CbDestino.ListIndex = i
                                Exit For
                            End If
                        Next
                    Else
                        CbDestino.ListIndex = mediComboValorNull
                    End If
                End If
            End If
        
            tabelaEFR.Close
            Set tabelaEFR = Nothing
        End If
        PreencheEntidades
'        ValidarARS EcCodEFR
    
'    End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros "Erro Inesperado : PreencheEFRUtente (FormGestaoRequisicaoVet) -> " & Err.Description
            Exit Sub
    End Select
End Sub
Sub EventoUnload()
    On Error GoTo TrataErro
    
    BG_StackJanelas_Pop
    BL_ToolbarEstadoN 0
    
    
    ' Descarregar todos os objectos
    Set RegistosP = Nothing
    
    If Not rs Is Nothing Then
        If rs.state <> adStateClosed Then
            rs.Close
        End If
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Isencao")
    Call BL_FechaPreview("Recibo Requisi��o")
    
    Set FormGestaoRequisicaoVet = Nothing
    
    Call BG_RollbackTransaction
    Call Apaga_DS_TM
        
'    If gF_IDENTIF_VET = 1 Then
'        FormIdentificaUtente.Enabled = True
'        Set gFormActivo = FormIdentificaUtente
'    Else
'        Set gFormActivo = MDIFormInicio
'    End If
    
    gF_REQUIS_VET = 0
    'Hoje
    'gEpisodioActivo = ""
    'gSituacaoActiva = mediComboValorNull

    If (gFormGesReqCons_Aberto = True) Then
        Flg_PesqUtente = False
        ' Quando este form foi invocado atrav�s de FormGesReqCons.
        gFormGesReqCons_Aberto = False
        Set gFormActivo = FormGesReqCons
        FormGesReqCons.Enabled = True
    ElseIf gF_IDENTIF_VET = 1 Then
        FormIdentificaVet.Enabled = True
        FormIdentificaVet.FuncaoLimpar
        Set gFormActivo = FormIdentificaVet
    ElseIf gF_FILA_ESPERA = mediSim Then
        FormFilaEspera.Enabled = True
        Set gFormActivo = FormFilaEspera
    Else
        Set gFormActivo = MDIFormInicio
    End If

 Exit Sub
TrataErro:
    BG_LogFile_Erros "EventoUnload: " & Err.Number & " - " & Err.Description, Me.Name, "EventoUnload"
    Exit Sub
    Resume Next
End Sub

Sub SELECT_Dados_Perfil(Marcar() As String, indice As Long, CodPerfil As String, CodFacturavel As String, multiAna As String)
    On Error GoTo TrataErro

    Dim RsPerf As ADODB.recordset
    Dim sql As String
    Dim Activo As String
    Dim descricao As String
    Dim JaExiste As Boolean
    Dim flgIncrmenta As Boolean
    Dim qtd As Integer
    Dim qtdMapeada As String
    Dim flg_FdsValFixo   As Integer
    CodPerfil = UCase(Trim(CodPerfil))
    
    'Activo ?
    sql = "SELECT descr_perfis, flg_activo FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(CodPerfil)
    Set RsPerf = New ADODB.recordset
    With RsPerf
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open , gConexao
    End With
    
    Activo = "-1"
    If RsPerf.RecordCount > 0 Then
        Activo = BL_HandleNull(RsPerf!flg_activo, "0")
        descricao = BL_HandleNull(RsPerf!descr_perfis, " ")
    Else
        BG_LogFile_Erros Me.Name & " - SELECT_Dados_Perfil: Erro a seleccionar dados do perfil!"
    End If
    RsPerf.Close
    Set RsPerf = Nothing
    
    'Membros do perfil
    sql = "SELECT cod_analise, ordem FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(CodPerfil)
    
    Set RsPerf = New ADODB.recordset
    With RsPerf
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open , gConexao
    End With
    
    If RsPerf.RecordCount <> 0 Then
        If Activo = "1" Then
            LbTotalAna = LbTotalAna + 1
            'soliveira 03.12.2007 Acrescentar analise "C99999" para descri��o do perfil no ecra de resultados
            BL_Preenche_MAReq FGTubos, CodPerfil, descricao, "C99999", " ", gGHOSTMEMBER_S, " ", indice, -1, -1, CodPerfil, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
            AdicionaInformacaoAna CodPerfil
            AdicionaQuestaoAna CodPerfil
        Else
            If VerificaAnalisesJaMarcadasPerfil(CodPerfil) = False Then
                Exit Sub
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "Perfil '" & descricao & "' sem membros!", vbInformation + vbOKOnly, App.ProductName
    End If
    
    'SE Perfil MARCADA PARA ENVIAR QUANTIDADE PARA FACTUS
    qtdMapeada = RetornaQtd(CodPerfil, EcCodEFR)
    If qtdMapeada <> "" Then
        qtd = qtdMapeada
        flgIncrmenta = False
    Else
        If IF_ContaMembros(CodPerfil, EcCodEFR) > 0 Then
            qtd = 0
            flgIncrmenta = True
        Else
            qtd = 1
            flgIncrmenta = False
        End If
    End If
    
    FGAna.CellBackColor = vbWhite
    FGAna.CellForeColor = vbBlack
    While Not RsPerf.EOF
        JaExiste = Verifica_Ana_ja_Existe(indice, BL_HandleNull(RsPerf!cod_analise, " "))
    
        If Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "S" Then
            If Activo = "1" Then
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), multiAna, CodPerfil, descricao, BL_HandleNull(RsPerf!ordem, 0), , , , CodFacturavel
            ElseIf Activo = "0" And JaExiste = False Then
                If VerificaRegraSexoMarcar(BL_HandleNull(RsPerf!cod_analise, " "), EcSeqUtente) = False Then
                    BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
                    JaExiste = True
                Else
                    PM_AdicionaItemEstrutura mediComboValorNull, mediComboValorNull, CodPerfil, BL_HandleNull(RsPerf!cod_analise, " "), "", "", ""
                    If SELECT_Dados_Simples(Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), multiAna, , , , , , , CodFacturavel) = False Then
                          indice = indice - 1
                    End If
                End If
            End If
        ElseIf Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "C" Then
            If Activo = "1" Then
                SELECT_Dados_Complexa Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), multiAna, CodPerfil, descricao, BL_HandleNull(RsPerf!ordem, 0), CodFacturavel
            ElseIf Activo = "0" And JaExiste = False Then
                If VerificaRegraSexoMarcar(BL_HandleNull(RsPerf!cod_analise, " "), EcSeqUtente) = False Then
                    BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
                    JaExiste = True
                Else
                    PM_AdicionaItemEstrutura mediComboValorNull, mediComboValorNull, CodPerfil, BL_HandleNull(RsPerf!cod_analise, " "), "", "", ""
                    SELECT_Dados_Complexa Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), multiAna, , , , CodFacturavel
                End If
            End If
        ElseIf Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "P" Then
            If Activo = "1" Then
                SELECT_Dados_Perfil Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), CodFacturavel, multiAna
            ElseIf Activo = "0" And JaExiste = False Then
                If VerificaRegraSexoMarcar(BL_HandleNull(RsPerf!cod_analise, " "), EcSeqUtente) = False Then
                    BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
                    JaExiste = True
                Else
                    PM_AdicionaItemEstrutura mediComboValorNull, mediComboValorNull, CodPerfil, BL_HandleNull(RsPerf!cod_analise, " "), "", "", ""
                    SELECT_Dados_Perfil Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), CodFacturavel, multiAna
                End If
            End If
        End If
        RsPerf.MoveNext
        
        If Not RsPerf.EOF And Activo = "0" And JaExiste = False Then
            ExecutaCodigoA = False
            indice = indice + 1
            FGAna.AddItem "", indice
            Insere_aMeio_RegistosA FGAna.row + 1
            FGAna.row = FGAna.row + 1
            FGAna.Col = 0
            ExecutaCodigoA = True
        End If
        If flgIncrmenta = True Then
            qtd = qtd + 1
        End If
    Wend
    
    If flgIncrmenta = True Then
        FlgIncrementa indice, qtd
    End If
    RsPerf.Close
    Set RsPerf = Nothing
 Exit Sub
TrataErro:
    BG_LogFile_Erros "SELECT_Dados_Perfil: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Dados_Perfil"
    Exit Sub
    Resume Next
End Sub

Sub SELECT_Dados_Complexa(Marcar() As String, indice As Long, CodComplexa As String, multiAna As String, Optional Perfil As Variant, _
                          Optional DescrPerfil As Variant, Optional OrdemP As Variant, Optional CodFacturavel As String)
    On Error GoTo TrataErro

    Dim RsCompl As ADODB.recordset
    Dim sql As String
    Dim Activo As String
    Dim descricao As String
    Dim qtd As Integer
    Dim flgIncrmenta As Boolean
    Dim qtdMapeada As String
    Dim flg_FdsValFixo   As Integer
    'Activo ?
    sql = "SELECT descr_ana_c, flg_marc_memb " & _
          "FROM sl_ana_c " & _
          "WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa)
    
    Set RsCompl = New ADODB.recordset

    With RsCompl
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open , gConexao
    End With

    Activo = "-1"
    If RsCompl.RecordCount > 0 Then
        Activo = BL_HandleNull(RsCompl!flg_marc_memb, "0")
        descricao = BL_HandleNull(RsCompl!Descr_Ana_C, "?")
    Else
        BG_LogFile_Erros Me.Name & " - SELECT_Dados_Complexa: Erro a seleccionar dados da complexa!"
    End If
    RsCompl.Close
    Set RsCompl = Nothing
    
    If IsMissing(Perfil) Then
        AdicionaInformacaoAna CodComplexa
        AdicionaQuestaoAna CodComplexa
    ElseIf Perfil = "" Or Perfil = "0" Then
        AdicionaInformacaoAna CodComplexa
        AdicionaQuestaoAna CodComplexa
    End If
    
    If Activo = "1" Then
        'soliveira teste
        If Not IsMissing(Perfil) Then
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, multiAna, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, -1, CodFacturavel
        Else
            LbTotalAna = LbTotalAna + 1
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, multiAna, , , , CodComplexa, descricao, -1, CodFacturavel
        End If
        
        'Membros da complexa
        sql = "SELECT cod_membro, ordem FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa) & _
            " AND t_membro = 'A' ORDER BY ordem "
        Set RsCompl = New ADODB.recordset
        With RsCompl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
        
        If RsCompl.RecordCount = 0 Then
            BG_Mensagem mediMsgBox, "Complexa '" & descricao & "' sem membros!", vbInformation + vbOKOnly, App.ProductName
        Else
        End If
        
        'SE COMPLEXA MARCADA PARA ENVIAR QUANTIDADE PARA FACTUS
        qtdMapeada = RetornaQtd(CodComplexa, EcCodEFR)
        If qtdMapeada <> "" Then
            qtd = qtdMapeada
            flgIncrmenta = False
        Else
            If IF_ContaMembros(CodComplexa, EcCodEFR) > 0 Then
                qtd = 0
                flgIncrmenta = True
            Else
                qtd = 1
                flgIncrmenta = False
            End If
        End If
        
        
        While Not RsCompl.EOF
            If Not IsMissing(Perfil) Then
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), multiAna, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " "), CodFacturavel
            Else
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), multiAna, , , , CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " "), CodFacturavel
            End If
            RsCompl.MoveNext
            
            If flgIncrmenta = True Then
                qtd = qtd + 1
            End If
        Wend
        If flgIncrmenta = True Then
            FlgIncrementa indice, qtd
        End If
    Else
        If Not IsMissing(Perfil) Then
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, multiAna, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, -1, CodFacturavel
        Else
            LbTotalAna = LbTotalAna + 1
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, multiAna, , , , CodComplexa, descricao, -1, CodFacturavel
        End If
    
        'Membros da complexa
        sql = "SELECT cod_membro, sl_membro.ordem FROM sl_membro,sl_ana_s " & _
            " WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa) & _
            " AND cod_ana_s = cod_membro " & _
            " AND flg_marc_auto = '1' " & _
            " AND t_membro = 'A' ORDER BY sl_membro.ordem "
        Set RsCompl = New ADODB.recordset
        With RsCompl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
        
        If Not RsCompl.EOF Then
            While Not RsCompl.EOF
                
                If Not IsMissing(Perfil) Then
                    SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), multiAna, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " ")
                Else
                    SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), multiAna, , , , CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " ")
                End If
                RsCompl.MoveNext

            Wend
            If flgIncrmenta = True Then
                FlgIncrementa indice, qtd
            End If
        Else
            If IsMissing(Perfil) Then
                GhostPerfil = "0"
                GhostComplexa = CodComplexa
                Lista_GhostMembers GhostComplexa, descricao
            End If
        End If
        
    End If
    RsCompl.Close
    Set RsCompl = Nothing
 Exit Sub
TrataErro:
    BG_LogFile_Erros "SELECT_Dados_Complexa: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Dados_Complexa"
    Exit Sub
    Resume Next
End Sub

Function SELECT_Dados_Simples(Marcar() As String, indice As Long, CodSimples As String, multiAna As String, Optional Perfil As Variant, Optional DescrPerfil As Variant, Optional OrdemP As Variant, Optional complexa As Variant, Optional DescrComplexa As Variant, Optional OrdemC As Variant, Optional CodFacturavel As String) As Boolean
    On Error GoTo TrataErro
    Dim PerfilFact As Integer
    Dim RsSimpl As ADODB.recordset
    Dim sql As String
    Dim descricao As String
    Dim codAgrup As String
    Dim descrAgrup As String
    Dim i As Integer
    Dim NaoMarcar As Boolean
    Dim taxa As String
    Dim codAnaEfr As String
    Dim mens As String
    Dim p1 As Long
    Dim corP1 As String
    Dim AnaRegra As String
    Dim anaFacturar As String
    Dim flg_FdsValFixo As Integer
    Dim iRec As Long
    SELECT_Dados_Simples = True

    If CodSimples = gGHOSTMEMBER_S Then
        If Not IsMissing(complexa) Then
            If Not IsMissing(Perfil) Then
                codAgrup = Perfil
                descrAgrup = DescrPerfil
                BL_Preenche_MAReq FGTubos, Perfil, DescrPerfil, complexa, DescrComplexa, CodSimples, " ", indice, OrdemC, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
            Else
                codAgrup = complexa
                descrAgrup = DescrComplexa
            
                BL_Preenche_MAReq FGTubos, "0", " ", complexa, DescrComplexa, CodSimples, " ", indice, OrdemC, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
            End If
        End If
    Else
    
        sql = "SELECT descr_ana_s, flg_invisivel FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(CodSimples)
        Set RsSimpl = New ADODB.recordset
        With RsSimpl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
        
        If RsSimpl.RecordCount > 0 Then
            descricao = BL_HandleNull(RsSimpl!descr_ana_s, " ")
            'N�o marcar a an�lise se flg_invisivel = 1
            If RsSimpl!flg_invisivel = 1 Then
                RsSimpl.Close
                Set RsSimpl = Nothing
                SELECT_Dados_Simples = False
                Exit Function
            End If
        End If
        RsSimpl.Close
        Set RsSimpl = Nothing
        
        If Not IsMissing(complexa) Then
             If Not IsMissing(Perfil) Then
                codAgrup = Perfil
                descrAgrup = DescrPerfil
                BL_Preenche_MAReq FGTubos, Perfil, DescrPerfil, complexa, DescrComplexa, CodSimples, descricao, indice, OrdemC, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
             Else
                codAgrup = complexa
                descrAgrup = DescrComplexa
             
                BL_Preenche_MAReq FGTubos, "0", " ", complexa, DescrComplexa, CodSimples, descricao, indice, OrdemC, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
             End If
        Else
             If Not IsMissing(Perfil) Then
                codAgrup = Perfil
                descrAgrup = DescrPerfil
                
                BL_Preenche_MAReq FGTubos, Perfil, DescrPerfil, "0", " ", CodSimples, descricao, indice, 0, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
             Else
                LbTotalAna = LbTotalAna + 1
                codAgrup = CodSimples
                descrAgrup = descricao
             
                BL_Preenche_MAReq FGTubos, "0", " ", "0", " ", CodSimples, descricao, indice, 0, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
             End If
        End If
        
    End If

    If Verifica_Ana_ja_Existe(0, codAgrup) = False Then
        If Trim(RegistosA(indice).codAna) = "" Then
            If codAgrup = CodSimples Then
                AdicionaInformacaoAna CodSimples
                AdicionaQuestaoAna CodSimples
            End If
            
            TotalAcrescentadas = TotalAcrescentadas + 1
            ReDim Preserve Acrescentadas(TotalAcrescentadas)
            Acrescentadas(TotalAcrescentadas).cod_agrup = codAgrup
            
            RegistosA(indice).codAna = codAgrup
            RegistosA(indice).descrAna = descrAgrup
            RegistosA(indice).Estado = "-1"
            RegistosA(indice).ObsAnaReq = ""
            RegistosA(indice).seqObsAnaReq = 0
            RegistosA(indice).multiAna = multiAna
                
            ' ------------------------------------------------------------------------------------------
            ' PARA ANALISES QUE NAOOOOO SAO MARCADAS DENTRO DE NENHUM PERFIL DE MARCACAO
            ' ------------------------------------------------------------------------------------------
            If RegistosA(indice).codAna <> "" And flg_codBarras = False Then
                If RegistosA(indice).codAna <> BL_HandleNull(UCase(CodFacturavel), "0") And Mid(UCase(CodFacturavel), 1, 1) = "P" Then
                    PerfilFact = BL_HandleNull(BL_SelCodigo("SL_PERFIS", "flg_nao_facturar", "cod_perfis", UCase(CodFacturavel), "V"), 0)
                    If PerfilFact = 0 Then
                        ' PERFIL DE MARCACAO. COMO TAL VERIFICAR SE JA FOI MARCADO
                        If VerificaPerfilMarcacaoJaMarcado(BL_HandleNull(UCase(CodFacturavel), "0")) = False Then
                            AdicionaReciboManual 0, EcCodEFR, "0", "0", BL_HandleNull(UCase(CodFacturavel), "0"), 1, "", BL_HandleNull(UCase(CodFacturavel), "0"), False
                        End If
                    Else
                        AdicionaReciboManual 0, EcCodEFR, "0", "0", BL_HandleNull(UCase(RegistosA(indice).codAna), "0"), 1, "", RegistosA(indice).codAna, False
                    End If
                Else
                    AdicionaReciboManual 0, EcCodEFR, "0", "0", BL_HandleNull(UCase(CodFacturavel), "0"), 1, "", RegistosA(indice).codAna, False
                End If
            End If
        
            If indice <> 1 Then
                If Trim(RegistosA(indice - 1).NReqARS) <> "" Then
                    RegistosA(indice).NReqARS = RegistosA(indice - 1).NReqARS
                Else
                    If Trim(EcNumReq) <> "" Then
                        RegistosA(indice).NReqARS = EcNumReq
                    Else
                        RegistosA(indice).NReqARS = "A gerar"
                    End If
                End If
            Else
                If Trim(EcNumReq) <> "" Then
                    RegistosA(indice).NReqARS = EcNumReq
                Else
                    RegistosA(indice).NReqARS = "A gerar"
                End If
            End If
            iRec = DevIndice(codAgrup)
            FGAna.TextMatrix(indice, lColFgAnaCodigo) = codAgrup
            FGAna.TextMatrix(indice, lColFgAnaDescricao) = descrAgrup
            If iRec > -1 Then
                FGAna.TextMatrix(indice, lColFgAnaP1) = RegistosRM(iRec).ReciboP1
                FGAna.TextMatrix(indice, lColFgAnaMedico) = RegistosRM(iRec).ReciboCodMedico
                FGAna.TextMatrix(indice, lColFgAnaCodEFR) = RegistosRM(iRec).ReciboEntidade
                FGAna.TextMatrix(indice, lColFgAnaDescrEFR) = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(iRec).ReciboEntidade)
                FGAna.TextMatrix(indice, lColFgAnaNrBenef) = RegistosRM(iRec).ReciboNrBenef
                FGAna.TextMatrix(indice, lColFgAnaPreco) = RegistosRM(iRec).ReciboTaxa
            End If
            FGAna.row = indice
            FGAna.Col = 1
            If multiAna <> "" Then
                FGAna.CellBackColor = laranja
            Else
                FGAna.CellBackColor = vbWhite
            End If

        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "SELECT_Dados_Simples: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Dados_Simples"
    SELECT_Dados_Simples = False
    Exit Function
    Resume Next
End Function

Function SELECT_Descr_Ana(codAna As String) As String
    On Error GoTo TrataErro

    Dim rsDescr As ADODB.recordset
    Set rsDescr = New ADODB.recordset
    Dim Flg_SemTipo As Boolean
    
    codAna = UCase(codAna)
    If InStr(1, "SCP", Mid(codAna, 1, 1)) = 0 Then
        Flg_SemTipo = True
    End If
    
    With rsDescr
        If Flg_SemTipo = True Then
            .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                            "cod_ana_s ='S" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_s in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                        " UNION " & _
                            "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                            "cod_ana_c='C" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_c in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                        " UNION " & _
                            "(SELECT descr_perfis as descricao, 'P' Tipo, null inibe_marcacao FROM sl_perfis WHERE " & _
                            "cod_perfis='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND (cod_perfis in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ") OR flg_activo = 0))"
                            
        Else
            .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                            "cod_ana_s ='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_s in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                        " UNION " & _
                            "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                            "cod_ana_c='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_c in (Select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ")) " & _
                        " UNION " & _
                            "(SELECT descr_perfis as descricao, 'P' Tipo,null inibe_marcacao FROM sl_perfis WHERE " & _
                            "cod_perfis='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND (cod_perfis in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ") OR flg_activo = 0))"

                        
        End If
        .ActiveConnection = gConexao
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With
    
    If rsDescr.RecordCount <= 0 Then
        SELECT_Descr_Ana = ""
    Else
        If BL_HandleNull(rsDescr!inibe_marcacao, "0") = "1" Then
            SELECT_Descr_Ana = ""
        Else
            SELECT_Descr_Ana = BL_HandleNull(rsDescr!descricao, "")
            If Flg_SemTipo = True Then
                codAna = rsDescr!tipo & codAna & ""
            End If
        End If
    End If
    
    rsDescr.Close
    Set rsDescr = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "SELECT_Descr_Ana: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Descr_Ana"
    SELECT_Descr_Ana = ""
    Exit Function
    Resume Next
End Function

Function Verifica_Ana_Prod(codAgrup As String, ByRef Marcar() As String) As Boolean
    On Error GoTo TrataErro

    Dim RsAnaProd As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    Dim k As Integer
    Dim CodProduto() As String
    Dim Resp As Integer
    Dim ListaProd() As String
    Dim JaTemProduto() As String
    Dim TotJaTemProduto As Integer
    Dim SQLAux As String
    Dim RsPerf As New ADODB.recordset
    
    gColocaDataChegada = ""
    codAgrup = UCase(Trim(codAgrup))

    'PERFIL Activo ?
    If Mid(codAgrup, 1, 1) = "P" Then
        sql = "SELECT  flg_activo FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup)
        Set RsPerf = New ADODB.recordset
        With RsPerf
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
        
        If RsPerf.RecordCount > 0 Then
            If BL_HandleNull(RsPerf!flg_activo, 0) = 0 Then
                RsPerf.Close
                sql = "SELECT * FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup)
                With RsPerf
                    .Source = sql
                    .CursorLocation = adUseServer
                    .CursorType = adOpenStatic
                    If gModoDebug = mediSim Then BG_LogFile_Erros sql
                    .Open , gConexao
                End With
                If RsPerf.RecordCount > 0 Then
                    While Not RsPerf.EOF
                        Verifica_Ana_Prod BL_HandleNull(RsPerf!cod_analise, ""), Marcar
                        RsPerf.MoveNext
                    Wend
                End If
            End If
        End If
        RsPerf.Close
        Set RsPerf = Nothing
    
    End If

'----------- PROCURAR NOS PRODUTOS JA MARCADOS, OS PRODUTOS DA ANALISE

If gSGBD = gOracle Then
    sql = "SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto " & _
        " FROM sl_ana_s a, sl_produto p " & _
        " Where a.cod_produto = p.cod_produto (+) And " & _
        " Cod_Ana_S = " & BL_TrataStringParaBD(codAgrup) & _
        " Union " & _
        " SELECT ac.cod_ana_c cod_ana_s, ac.descr_ana_c descr_ana_s, ac.cod_produto, p.descr_produto " & _
        " FROM sl_ana_c ac, sl_produto p " & _
        " Where ac.cod_produto = p.cod_produto (+) And " & _
        " ac.Cod_Ana_c in (select cod_analise from sl_ana_perfis where cod_perfis = " & BL_TrataStringParaBD(codAgrup) & ")" & _
        " Union " & _
        " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto " & _
        " FROM sl_ana_s a, sl_produto p " & _
        " WHERE a.cod_produto = p.cod_produto (+) AND " & _
        " cod_ana_s IN ( SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(codAgrup) & _
        " AND t_membro = 'A' )" & _
        " Union " & _
        " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto " & _
        " FROM sl_ana_s a, sl_produto p " & _
        " WHERE a.cod_produto = p.cod_produto (+) AND " & _
        " cod_ana_s IN " & _
        "(( SELECT cod_analise as cod_ana_s FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & _
        " AND cod_analise LIKE 'S%' ) Union " & _
        " ( SELECT cod_membro as cod_ana_s FROM sl_membro WHERE cod_ana_c IN ( SELECT cod_analise FROM " & _
        " sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & " AND cod_analise LIKE 'C%' ) AND t_membro = 'A' )) "
ElseIf gSGBD = gInformix Then
    sql = "SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a, OUTER sl_produto p" & _
        " Where a.cod_produto = p.cod_produto And Cod_Ana_S = " & BL_TrataStringParaBD(codAgrup) & _
        " Union " & _
        " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a, OUTER sl_produto p " & _
        " WHERE a.cod_produto = p.cod_produto AND cod_ana_s IN ( SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(codAgrup) & _
        " AND t_membro = 'A' )" & _
        " Union " & _
        " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a, OUTER sl_produto p " & _
        " WHERE a.cod_produto = p.cod_produto AND cod_ana_s IN " & _
        "(( SELECT cod_analise as cod_ana_s FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & _
        " AND cod_analise LIKE 'S%' ) Union " & _
        " ( SELECT cod_membro as cod_ana_s FROM sl_membro WHERE cod_ana_c IN ( SELECT cod_analise FROM " & _
        " sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & " AND cod_analise LIKE 'C%' ) AND t_membro = 'A' ))"
ElseIf gSGBD = gSqlServer Then
    sql = "SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a RIGHT OUTER JOIN sl_produto p " & _
        " ON a.cod_produto = p.cod_produto WHERE Cod_Ana_S = " & BL_TrataStringParaBD(codAgrup) & _
        " Union " & _
        "SELECT ac.cod_ana_c cod_ana_s, ac.descr_ana_c descr_ana_s, ac.cod_produto, p.descr_produto FROM sl_ana_c ac RIGHT OUTER JOIN sl_produto p " & _
        " ON ac.cod_produto = p.cod_produto WHERE ac.Cod_Ana_c in (select cod_analise from sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & ")" & _
        " Union " & _
        " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a RIGHT OUTER JOIN sl_produto p " & _
        " ON a.cod_produto = p.cod_produto WHERE cod_ana_s IN ( SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(codAgrup) & _
        " AND t_membro = 'A' )" & _
        " Union " & _
        " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto FROM sl_ana_s a RIGHT OUTER JOIN sl_produto p " & _
        " ON a.cod_produto = p.cod_produto WHERE cod_ana_s IN " & _
        "(( SELECT cod_analise as cod_ana_s FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & _
        " AND cod_analise LIKE 'S%' ) Union " & _
        " ( SELECT cod_membro as cod_ana_s FROM sl_membro WHERE cod_ana_c IN ( SELECT cod_analise FROM " & _
        " sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & " AND cod_analise LIKE 'C%' ) AND t_membro = 'A' ))"
End If

SQLAux = " select cod_perfis cod_ana_s, descr_perfis descr_ana_s, sl_perfis.cod_produto,descr_produto from sl_perfis, sl_produto where " & _
          " sl_perfis.cod_produto = sl_produto.cod_produto and cod_perfis = " & BL_TrataStringParaBD(codAgrup)
    
Set RsAnaProd = New ADODB.recordset
With RsAnaProd
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    .Source = SQLAux
    .ActiveConnection = gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLAux
    .Open
End With


Verifica_Ana_Prod = False

ReDim JaTemProduto(0)
TotJaTemProduto = 0
ReDim ListaProd(0)
ReDim Marcar(0)
k = 0
If RsAnaProd.RecordCount <= 0 Then
    SQLAux = " select cod_ana_c cod_ana_s, descr_ana_c descr_ana_s, sl_ana_c.cod_produto,descr_produto from sl_ana_c, sl_produto where " & _
              " sl_ana_c.cod_produto = sl_produto.cod_produto and cod_ana_c = " & BL_TrataStringParaBD(codAgrup)
    Set RsAnaProd = New ADODB.recordset
    With RsAnaProd
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .Source = SQLAux
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros SQLAux
        .Open
    End With
    If RsAnaProd.RecordCount <= 0 Then
        Set RsAnaProd = New ADODB.recordset
        With RsAnaProd
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .Source = sql
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open
        End With
    End If
End If

While Not RsAnaProd.EOF
    If Trim(BL_HandleNull(RsAnaProd!cod_produto, "")) <> "" Then
        For i = 1 To RegistosP.Count - 1
            If Trim(RegistosP(i).CodProd) = Trim(BL_HandleNull(RsAnaProd!cod_produto, "")) Then
                Verifica_Ana_Prod = True
                gColocaDataChegada = RegistosP(i).DtChega
                Exit For
            Else
                Verifica_Ana_Prod = False
            End If
        Next i
        
        If Verifica_Ana_Prod = False Then
            k = k + 1
            ReDim Preserve ListaProd(k)
            ListaProd(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";" & BL_HandleNull(RsAnaProd!cod_produto, "") & ";" & BL_HandleNull(RsAnaProd!descr_produto, "") & Space(35 - Len(Trim(BL_HandleNull(RsAnaProd!descr_produto, "")))) & " -> " & Trim(BL_HandleNull(RsAnaProd!descr_ana_s, ""))
        Else
            TotJaTemProduto = TotJaTemProduto + 1
            ReDim Preserve JaTemProduto(TotJaTemProduto)
            JaTemProduto(TotJaTemProduto) = BL_HandleNull(RsAnaProd!cod_ana_s, "")
        End If
    End If
    
    RsAnaProd.MoveNext
Wend
    
'----------- ALGUM NAO ESTA NA LISTA

If k <> 0 Then
    
    Verifica_Ana_Prod = False
    
    'Pergunta 1: Quer marcar o produto da analise ?
        
    If Flg_PergProd = True Then
        'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
        'que se deve adoptar a ultima resposta � pergunta
            
        If Resp_PergProd = True Then
            'respondeu na ultima vez que sim � pergunta 1
            Verifica_Ana_Prod = True
                
            'Pergunta 2: Quer inserir a data de hoje na chegada do produto ?
            
            If Flg_DtChProd = True Then
                'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
                'que se deve adoptar a ultima resposta � pergunta
                If Flg_PreencherDtChega = True Then
                    'respondeu na ultima vez que sim � pergunta 2
                    For i = 1 To UBound(ListaProd)
                        CodProduto = Split(ListaProd(i), ";")
                        InsereAutoProd CodProduto(1), Bg_DaData_ADO
                    Next i
                Else
                    'respondeu na ultima vez que n�o � pergunta 2
                    For i = 1 To UBound(ListaProd)
                        CodProduto = Split(ListaProd(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                End If
            Else
                'O utilizador ainda n�o escolheu que a pergunta n�o
                'deve ser efectuada de novo, logo questiona-se a pergunta 2
                k = 0
                ReDim Marcar(0)
                For i = 1 To UBound(ListaProd)
                    CodProduto = Split(ListaProd(i), ";")
                    k = k + 1
                    ReDim Preserve Marcar(k)
                    Marcar(k) = CodProduto(0) & ";" & CodProduto(1)
                Next i
                
                FormGestaoRequisicaoVet.Enabled = False

                If FormPergProd.PerguntaProduto(ConfirmarDtChega, Flg_DtChProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
                    'respondeu sim � pergunta 2
                    Flg_PreencherDtChega = True
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), Bg_DaData_ADO
                    Next i
                Else
                    'respondeu n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                    Flg_PreencherDtChega = False
                End If
                
            End If
        Else
            'respondeu na ultima vez que n�o � pergunta 1
            Verifica_Ana_Prod = True
        End If
    Else
        'O utilizador ainda n�o escolheu que a pergunta n�o
        'deve ser efectuada de novo, logo questiona-se a pergunta 1
        
        'FormGestaoRequisicaoVet.Enabled = False
            
        If FormPergProd.PerguntaProduto(ConfirmarDefeito, Flg_PergProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
            'respondeu sim � pergunta 1
            Resp_PergProd = True
            Verifica_Ana_Prod = True
            
            'Pergunta 2: Quer inserir a data de hoje na chegada do produto ?
            
            If Flg_DtChProd = True Then
                'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
                'que se deve adoptar a ultima resposta � pergunta
                If Flg_PreencherDtChega = True Then
                    'respondeu na ultima vez que sim � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), Bg_DaData_ADO
                    Next i
                Else
                    'respondeu na ultima vez que n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                End If
            Else
                If gRegChegaTubos <> 1 Then
                    If gIgnoraPerguntaProdutoTubo = 1 Then
                        For i = 1 To UBound(Marcar)
                            CodProduto = Split(Marcar(i), ";")
                            InsereAutoProd CodProduto(1), Bg_DaData_ADO
                        Next i
                        Flg_PreencherDtChega = True
                    Else
                        'O utilizador ainda n�o escolheu que a pergunta n�o
                        'deve ser efectuada de novo, logo questiona-se a pergunta 2
                        If FormPergProd.PerguntaProduto(ConfirmarDtChega, Flg_DtChProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
                            'respondeu sim � pergunta 2
                            Flg_PreencherDtChega = True
                            For i = 1 To UBound(Marcar)
                                CodProduto = Split(Marcar(i), ";")
                                InsereAutoProd CodProduto(1), Bg_DaData_ADO
                            Next i
                        Else
                            'respondeu n�o � pergunta 2
                            For i = 1 To UBound(Marcar)
                                CodProduto = Split(Marcar(i), ";")
                                InsereAutoProd CodProduto(1), ""
                            Next i
                            Flg_PreencherDtChega = False
                        End If
                    End If
                Else
                    'respondeu n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                    Flg_PreencherDtChega = False
                End If
            End If
        Else
            'respondeu n�o � pergunta 1
            Resp_PergProd = False
        End If
        
    End If

    ' retirar do array de an�lises a marcar o codigo do produto (fica "S1;S2;S3;S4;")
    For i = 1 To UBound(Marcar)
        CodProduto = Split(Marcar(i))
        Marcar(i) = CodProduto(0) & ";"
    Next i
     
    '----------- PERMITIR MARCAR AS ANALISES QUE NAO TEM PRODUTO ASSOCIADO
    
    If RsAnaProd.RecordCount <> 0 Then RsAnaProd.MoveFirst
    k = UBound(Marcar)
    While Not RsAnaProd.EOF
        If BL_HandleNull(RsAnaProd!cod_produto, "") = "" Then
            k = k + 1
            ReDim Preserve Marcar(k)
            Marcar(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";"
            If gColocaDataChegada = "" Then gColocaDataChegada = Bg_DaData_ADO
            Verifica_Ana_Prod = True
        End If
        RsAnaProd.MoveNext
    Wend
    
    '----------- MARCAR AS ANALISES QUE J� TINHAM O PRODUTO REGISTADO
    
    k = UBound(Marcar)
    If TotJaTemProduto <> 0 Then
        For i = 1 To TotJaTemProduto
            k = k + 1
            ReDim Preserve Marcar(k)
            Marcar(k) = JaTemProduto(i) & ";"
        Next i
    End If
    
Else
    Verifica_Ana_Prod = True
    
    'Marca todas
    If RsAnaProd.RecordCount <> 0 Then RsAnaProd.MoveFirst
    k = 0
    ReDim Marcar(0)
    If Not RsAnaProd.EOF Then
        If gColocaDataChegada = "" And EcDataChegada.text <> "" Then    'sdo em vez de: If gColocaDataChegada = "" Then gColocaDataChegada = BG_DaData_ADO
            gColocaDataChegada = EcDataChegada.text                     'sdo
        ElseIf EcDataChegada.text = "" Then                             'sdo
            gColocaDataChegada = ""                                     'sdo
        End If                                                          'sdo
    End If
    While Not RsAnaProd.EOF
        k = k + 1
        ReDim Preserve Marcar(k)
        Marcar(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";"
        RsAnaProd.MoveNext
    Wend

End If

RsAnaProd.Close
Set RsAnaProd = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Ana_Prod: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Ana_Prod"
    Verifica_Ana_Prod = False
    Exit Function
    Resume Next
End Function

Private Sub BtAdiantamentos_Click()
    GereFrameAdiantamentos True
End Sub

Private Sub BtAnularAdiamento_Click()
    If FgAdiantamentos.row < FgAdiantamentos.rows - 1 And FgAdiantamentos.row > 0 Then
        If RegistosAD(FgAdiantamentos.row).NumAdiantamento > 0 Then
            If RegistosAD(FgAdiantamentos.row).UserAnul = "" And RegistosAD(FgAdiantamentos.row).DtAnul = "" Then
                gMsgTitulo = "Adiantamento"
                gMsgMsg = "Tem a certeza que quer devolver o adiantamento: " & RegistosAD(FgAdiantamentos.row).NumAdiantamento & "?"
                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                If gMsgResp = vbYes Then
                    If RegistosAD(FgAdiantamentos.row).UserAnul = "" And RegistosAD(FgAdiantamentos.row).DtAnul = "" Then
                        RECIBO_AnulaAdiantamento CStr(EcNumReq), FgAdiantamentos.row, RegistosAD
                        PreencheFgAdiantamentos
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub BtAnularRecibo_Click()
    On Error GoTo TrataErro
    If RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca Or RegistosR(FGRecibos.row).Estado = gEstadoReciboPago Then
        FormGestaoRequisicaoVet.Enabled = False
        FormCancelarRecibos.Show
    ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboAnulado Then
        FormGestaoRequisicaoVet.Enabled = False
        FormCancelarRecibos.Show
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtAnularRecibo_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtAnularRecibo_Click"
    Exit Sub
    Resume Next
End Sub



Private Sub BtCancelarReq_Click()
    On Error GoTo TrataErro

    Max = UBound(gFieldObjectProperties)
    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind
    
    MarcaLocal = rs.Bookmark
    FormGestaoRequisicaoVet.Enabled = False
    FormCancelarRequisicoes.Show
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtCancelarReq_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtCancelarReq_Click"
    Exit Sub
    Resume Next
End Sub




Private Sub BtConfirm_Click()
    If EcNumReq <> "" And EcUtilConf = "" And EcDtConf = "" And EcHrConf = "" Then
        EcUtilConf = gCodUtilizador
        EcUtilNomeConf.caption = BL_SelNomeUtil(EcUtilConf.text)
        EcDtConf = Bg_DaData_ADO
        EcHrConf = Bg_DaHora_ADO
        BD_Update
    End If
End Sub

Private Sub BtConsReq_Click()
    On Error GoTo TrataErro
    If EcNumReq.text <> "" Then
        BL_LimpaDadosUtente
        gRequisicaoActiva = EcNumReq
        FormGestaoRequisicaoVet.Enabled = False
        FormConsNovo.Show
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtConsReq_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtConsReq_Click"
    Exit Sub
    Resume Next
End Sub



Private Sub BTDigi_Click()
    Dim nBarcodes As Long
    Dim sBarcode As String
    Dim sType As String
    Dim sDirection As String
    Dim nPage As Long
    Dim barLen As Long
    Dim i As Integer
    Dim textResult As String
    Dim res As String
    textResult = ""
    FgAnaEFR.Visible = False
    DoEvents
    
    SetRegistrationCode ("HVFU5QQXAM9JYR738YL7")
    SetReadCode39 (1)
    SetScanRight (1)
    nBarcodes = ReadBarcode(gDirCliente & "\cred.tif")
    
    res = ""
    For i = 0 To nBarcodes - 1
    
        ' determine the required size to store barcode string
        barLen = GetBarcodeString(i, sBarcode, 0)
        sBarcode = Space(barLen)
        Call GetBarcodeString(i, sBarcode, barLen)
        
        
        ' remove null-terminatiors
        sBarcode = Trim(Replace(sBarcode, Chr(0), ""))
        EcAnaEfr = sBarcode
        EcAnaEfr_KeyDown vbKeyReturn, 0
        res = res & sBarcode & "|"
        sBarcode = ""
        DoEvents
    Next i
    
    textResult = res
    If Len(res) = 0 Then
        textResult = "Not found."
    End If
    FgAnaEFR.Visible = True
    
End Sub



Private Sub BtDomicilio_Click()
    On Error GoTo TrataErro
    If FrameDomicilio.Visible = True Then
        FrameDomicilio.Visible = False
    Else
        FrameDomicilio.Visible = True
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtDomicilio_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtDomicilio_Click"
    Exit Sub
    Resume Next
End Sub


Private Sub BtFgAnaRecRecalcula_Click()
    On Error GoTo TrataErro
    Dim i As Long
    Dim taxa As Double
    If RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido Or RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo Or _
        RegistosR(FGRecibos.row).Estado = gEstadoReciboPerdido Then
        taxa = 0
        For i = 1 To TotalAnaRecibo
            taxa = taxa + EstrutAnaRecibo(i).taxa
        Next
        RegistosR(FGRecibos.row).ValorPagar = taxa
        RegistosR(FGRecibos.row).ValorOriginal = taxa
        RegistosR(FGRecibos.row).NumAnalises = TotalAnaRecibo
        FGRecibos.TextMatrix(FGRecibos.row, lColRecibosValorPagar) = taxa
        
        If taxa = 0 And TotalAnaRecibo = 0 Then
            FGRecibos.RemoveItem FGRecibos.row
            LimpaColeccao RegistosR, FGRecibos.row
            
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtFgAnaRecRecalcula_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtFgAnaRecRecalcula_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtFgAnaRecSair_Click()
    On Error GoTo TrataErro
    FrameAnaRecibos.Visible = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtFgAnaRecSair_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtFgAnaRecSair_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtFichaCruzada_Click()
    On Error GoTo TrataErro
    ImprimeEtiqNovo
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtFichaCruzada_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtFichaCruzada_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtGravar_Click()
    If EcNumReq <> "" And EcUtilizadorCriacao <> "" Then
        FuncaoModificar
    Else
        FuncaoInserir
    End If
End Sub

Private Sub BtIdentif_Click()
    On Error GoTo TrataErro
    Dim i As Integer
    For i = 0 To 20
        If StackJanelas(i).Name = "FormIdentificaVet" Then
            Exit Sub
        End If
    Next
    If EcSeqUtente <> "" Then
        FormIdentificaVet.Show
        Set gFormActivo = FormIdentificaVet
        FormIdentificaVet.EcCodSequencial = EcSeqUtente
        FormIdentificaVet.FuncaoProcurarPesquisa
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtIdentif_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtIdentif_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtImprimirAdiantamento_Click()
    If FgAdiantamentos.row < FgAdiantamentos.rows - 1 And FgAdiantamentos.row > 0 Then
        If RegistosAD(FgAdiantamentos.row).NumAdiantamento > 0 Then
            RECIBO_ImprimeAdiantamento EcNumReq, RegistosAD(FgAdiantamentos.row).NumAdiantamento, EcPrinterRecibo
        End If
    End If
End Sub

Public Sub BtImprimirRecibo_Click()
    On Error GoTo TrataErro
    If gUsaCopiaRecibo <> mediSim Then
        ImprimeRecibo ""
    Else
        MDIFormInicio.PopupMenu MDIFormInicio.HIDE_IMPR_REC
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtImprimirRecibo_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtImprimirRecibo_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtInfClinica_Click()
    On Error GoTo TrataErro

    'for�ar a fazer o procurar da inf. clinica para a requisi��o em causa
    'caso j� exista uma
    
    BL_PreencheDadosUtente EcSeqUtente
    FormGestaoRequisicaoVet.Enabled = False
    FormInformacaoClinica.Show
    'If EcNumReq.Text <> "" Or EcSeqUtente.Text <> "" Then
        FormInformacaoClinica.FuncaoProcurar
    'End If
    FormInformacaoClinica.EcInfCompl = EcinfComplementar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtInfClinica_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtInfClinica_Click"
    Exit Sub
    Resume Next
End Sub



Private Sub BtNotas_Click()
    On Error GoTo TrataErro
    FormGestaoRequisicaoVet.Enabled = False
    FormNotasReq.Show
    FormNotasReq.EcNumReq = EcNumReq
    FormNotasReq.EcSeqUtente = EcSeqUtente
    FormNotasReq.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotas_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotas_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtNotasVRM_Click()
    On Error GoTo TrataErro
    FormGestaoRequisicaoVet.Enabled = False
    FormNotasReq.Show
    FormNotasReq.EcNumReq = EcNumReq
    FormNotasReq.EcSeqUtente = EcSeqUtente
    FormNotasReq.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotasVRM_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotasVRM_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtObsEspecif_Click()
    On Error GoTo TrataErro
    EcObsEspecif = RegistosP(LastRowP).ObsEspecif
    FrameObsEspecif.Visible = True
    EcObsEspecif.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtObsEspecif_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtObsEspecif_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtOkObsEspecif_Click()
    On Error GoTo TrataErro
    RegistosP(LastRowP).ObsEspecif = Trim(EcObsEspecif)
    FgProd.SetFocus
    EcObsEspecif = ""
    FrameObsEspecif.Visible = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtOkObsEspecif_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtOkObsEspecif_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaAna_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error GoTo TrataErro
    FGAna.Col = 0
    FGAna.row = FGAna.rows - 1
    ExecutaCodigoA = True
    If Button = 1 Then
        If RegistosA(FGAna.row).Estado <> "-1" And RegistosA(FGAna.row).codAna <> "" And FGAna.Col = 0 Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser alterada: j� cont�m resultados!"
        Else
            MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANAMARC
        End If
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaAna_MouseDown: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaAna_MouseDown"
    Exit Sub
    Resume Next
End Sub



Private Sub BtPesquisaEspecif_Click()
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "sl_especif.descr_especif"
    CamposEcran(1) = "sl_especif.descr_especif"
    Tamanhos(1) = 3000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "sl_prod_esp.cod_especif"
    CamposEcran(2) = "sl_prod_esp.cod_especif"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = " sl_prod_esp,sl_especif "
    CWhere = " sl_prod_esp.cod_produto='" & UCase(FgProd.TextMatrix(FgProd.row, 0)) & "' AND sl_prod_esp.cod_especif=sl_especif.cod_especif"
    CampoPesquisa = ""
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "ORDER BY DESCR_especif ", " Especifica��es")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            FgProd.TextMatrix(FgProd.row, 3) = Resultados(1)
            FgProd.TextMatrix(FgProd.row, 2) = Resultados(2)
            FgProd.row = 4
        End If
    Else
        Set CamposRetorno = Nothing
        Set CamposRetorno = New ClassPesqResultados
    
        PesqRapida = False

        ChavesPesq(1) = "descr_especif"
        CamposEcran(1) = "descr_especif"
        Tamanhos(1) = 3000
        Headers(1) = "Descri��o"
        
        ChavesPesq(2) = "cod_especif"
        CamposEcran(2) = "cod_especif"
        Tamanhos(2) = 1000
        Headers(2) = "C�digo"
        
        CamposRetorno.InicializaResultados 2
        
        CFrom = " sl_especif "
        CWhere = ""
        CampoPesquisa = "descr_especif"
        
        PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Especifica��es")
        
        If PesqRapida = True Then
            FormPesqRapidaAvancada.Show vbModal
            CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
            If Not CancelouPesquisa Then
                FgProd.TextMatrix(FgProd.row, 3) = Resultados(1)
                FgProd.TextMatrix(FgProd.row, 2) = Resultados(2)
                RegistosP(LastRowP).CodEspecif = UCase(Resultados(2))
                FgProd.Col = 4
            End If
        Else
            BG_Mensagem mediMsgBox, "N�o existem especifica��es codificadas!", vbExclamation, "Especifica��es"
            FgProd.Col = 2
        End If
    End If
    FgProd.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaEspecif_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaEspecif_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaMedico_Click()
    On Error GoTo TrataErro


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_med"
    CamposEcran(1) = "cod_med"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "nome_med"
    CamposEcran(2) = "nome_med"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_medicos"
    CampoPesquisa1 = "nome_med"
    ClausulaWhere = " (flg_invisivel is null or flg_invisivel = 0) "
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar M�dicos")
    
    mensagem = "N�o foi encontrado nenhum M�dico."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMedico.text = Resultados(1)
            EcNomeMedico.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaMedico_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaMedico_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaProduto_click()
    On Error GoTo TrataErro
    
    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
                        "descr_produto", "seq_produto", _
                         EcPesqRapProduto
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaProduto_click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaProduto_click"
    Exit Sub
    Resume Next
End Sub

Sub Funcao_CopiaUte()
    On Error GoTo TrataErro
    
    If Trim(gDUtente.seq_utente) <> "" Then
        EcSeqUtente = CLng(gDUtente.seq_utente)
        PreencheDadosUtente
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe utente activo!", , "Copiar utente"
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_CopiaUte: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_CopiaUte"
    Exit Sub
    Resume Next
End Sub

Sub Insere_aMeio_RegistosA(inicio As Integer)
    On Error GoTo TrataErro

    Dim j As Long

    CriaNovaClasse RegistosA, -1, "ANA"
    
    If inicio <> RegistosA.Count Then
        For j = RegistosA.Count To inicio + 1 Step -1
            RegistosA(j).codAna = RegistosA(j - 1).codAna
            RegistosA(j).descrAna = RegistosA(j - 1).descrAna
            RegistosA(j).NReqARS = RegistosA(j - 1).NReqARS
            RegistosA(j).p1 = RegistosA(j - 1).p1
            RegistosA(j).Estado = RegistosA(j - 1).Estado
            RegistosA(j).DtChega = RegistosA(j - 1).DtChega
            RegistosA(j).ObsAnaReq = RegistosA(j - 1).ObsAnaReq
            RegistosA(j).seqObsAnaReq = RegistosA(j - 1).seqObsAnaReq
        
        Next
    End If

    RegistosA(inicio).codAna = ""
    RegistosA(inicio).descrAna = ""
    RegistosA(inicio).NReqARS = ""
    RegistosA(inicio).p1 = ""
    RegistosA(inicio).Estado = ""
    RegistosA(inicio).DtChega = ""
    RegistosA(inicio).ObsAnaReq = ""
    RegistosA(inicio).seqObsAnaReq = ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "Insere_aMeio_RegistosA: " & Err.Number & " - " & Err.Description, Me.Name, "Insere_aMeio_RegistosA"
    Exit Sub
    Resume Next
End Sub

Function Calcula_Valor(valor As Double, TaxaConv) As Double
    On Error GoTo TrataErro
    
    Calcula_Valor = Round(valor / TaxaConv)
Exit Function
TrataErro:
    BG_LogFile_Erros "Calcula_Valor: " & Err.Number & " - " & Err.Description, Me.Name, "Insere_aMeCalcula_Valorio_RegistosA"
    Calcula_Valor = 0
    Exit Function
    Resume Next
End Function

Private Sub BtRegistaMedico_Click()
    On Error GoTo TrataErro
    If gLAB = "BIO" Then
        FormGestaoRequisicaoVet.Enabled = False
        FormMedicosReq.Show
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtRegistaMedico_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtRegistaMedico_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtSairAdiantamento_Click()
    GereFrameAdiantamentos False
End Sub

Private Sub CbEFR_Click()
    If CbEFR.ListIndex > mediComboValorNull Then
        PreencheFgAnaEFR_todos CLng(CbEFR.ItemData(CbEFR.ListIndex))
    Else
        LimpaFGAnaEfr
    End If
End Sub

Private Sub CbHemodialise_Click()
    If CbHemodialise.ListIndex <> mediComboValorNull Then
        If EcCodIsencao <> gTipoIsencaoIsento Then
            BG_Mensagem mediMsgBox, "Para indicar uma doen�a o Utente ter� que ser isento.", vbInformation
            CbHemodialise.ListIndex = mediComboValorNull
        Else
            BL_ColocaComboTexto "sl_cod_hemodialise", "seq_hemodialise", "cod_hemodialise", EcHemodialise, CbHemodialise
        End If
    Else
        EcHemodialise = ""
    End If
End Sub


Private Sub CbTipoUtente_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    BG_LimpaOpcao CbTipoUtente, KeyCode

Exit Sub
TrataErro:
    BG_LogFile_Erros "CbTipoUtente_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbTipoUtente_KeyDown"
    Exit Sub
    Resume Next
End Sub

Public Sub CbTipoUtente_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    
    Call EcUtente_Validate(cancel)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbTipoUtente_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "CbTipoUtente_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub CbUrgencia_Click()
'    EcUrgencia.Text = Mid(CbUrgencia.Text, 1, 1)
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    BG_LimpaOpcao CbUrgencia, KeyCode

Exit Sub
TrataErro:
    BG_LogFile_Erros "CbUrgencia_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbUrgencia_KeyDown"
    Exit Sub
    Resume Next
End Sub



Private Sub CkCobrarDomicilio_Click()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim cod_efr As String
    If flg_preencheCampos = True Then
        Exit Sub
    End If
    
    If CkCobrarDomicilio.value = vbChecked Then

        If EcCodEFR <> "" Then
            If IF_EFRComparticipaDomicilio(EcCodEFR) = True Then
                cod_efr = EcCodEFR
            Else
                cod_efr = gEfrParticular
            End If
        End If
        
        If BG_DaComboSel(CbCodUrbano) = lCodUrbano And gCodAnaDomUrbano <> "" And gCodAnaDomUrbano <> "-1" Then
            AdicionaReciboManual 1, cod_efr, 0, "0", gCodAnaDomUrbano, 1, "", gCodAnaDomUrbano, False
        ElseIf BG_DaComboSel(CbCodUrbano) = lCodNaoUrbano And gCodAnaDomNaoUrbano <> "" And gCodAnaDomNaoUrbano <> "-1" Then
            AdicionaReciboManual 1, cod_efr, 0, "0", gCodAnaDomNaoUrbano, CInt(BL_HandleNull(EcKm, 0)), "", gCodAnaDomNaoUrbano, False
        End If
    ElseIf CkCobrarDomicilio.value = vbUnchecked Then
        If BG_DaComboSel(CbCodUrbano) = lCodUrbano And gCodAnaDomUrbano <> "" And gCodAnaDomUrbano <> "-1" Then
            For i = 1 To RegistosRM.Count
                If RegistosRM(i).ReciboCodFacturavel = gCodAnaDomUrbano Then
                    EliminaReciboManual CLng(i), RegistosRM(i).ReciboCodFacturavel, True
                    EliminaFgAnaEfr CLng(i)
                    LimpaColeccao RegistosRM, CLng(i)

                    Exit Sub
                End If
            Next
            
        ElseIf BG_DaComboSel(CbCodUrbano) = lCodNaoUrbano And gCodAnaDomNaoUrbano <> "" And gCodAnaDomNaoUrbano <> "-1" Then
            For i = 1 To RegistosRM.Count
                If RegistosRM(i).ReciboCodFacturavel = gCodAnaDomNaoUrbano Then
                    EliminaReciboManual CLng(i), RegistosRM(i).ReciboCodFacturavel, True
                    EliminaFgAnaEfr CLng(i)
                    LimpaColeccao RegistosRM, CLng(i)
                    Exit Sub
                End If
            Next
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "CkCobrarDomicilio_Click: " & Err.Number & " - " & Err.Description, Me.Name, "CkCobrarDomicilio_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub cmdOK_Click()
    On Error GoTo TrataErro
    
    FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "cmdOK_Click: " & Err.Number & " - " & Err.Description, Me.Name, "cmdOK_Click"
    Exit Sub
    Resume Next
End Sub





Private Sub BtResumo_Click()
    ImprimeResumo
End Sub

Private Sub btAnaAcresc_Click()
    ' ---------------------------------------------------------------------------
    ' VERIFICA SE ANALISE ESTA MAPEADA
    ' ---------------------------------------------------------------------------
    AdicionaReciboManual 1, RegistosR(FGRecibos.row).codEntidade, RegistosR(FGRecibos.row).NumDoc, RegistosR(FGRecibos.row).SerieDoc, EcCodAnaAcresc, BL_HandleNull(EcQtdAnaAcresc, 1), "", EcCodAnaAcresc, False
    
    EcCodAnaAcresc = ""
    EcDescrAnaAcresc = ""
    EcQtdAnaAcresc = ""
    EcTaxaAnaAcresc = ""

End Sub


Private Sub EcAnaEfr_GotFocus()
        cmdOK.Default = False
End Sub

Private Sub EcAnaEfr_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        If UCase(Mid(EcAnaEfr, 1, 1)) = "M" Then
            EcMedAct = UCase(EcAnaEfr)
        ElseIf Len(EcAnaEfr) = 13 Or Len(EcAnaEfr) = 1 Then
            EcCredAct = EcAnaEfr
        ElseIf (Len(EcAnaEfr)) >= 1 Then
            FGAna.row = FGAna.rows - 1
            LastRowA = FGAna.row
            AlteraFlgCodBarras True
            
        End If
        EcAnaEfr = ""
        EcAnaEfr.SetFocus
    End If
End Sub

Private Sub EcAuxAna_GotFocus()
    On Error GoTo TrataErro
    
    Dim i As Integer
    
    On Local Error Resume Next
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i
    
    If EcAuxAna.SelLength = 0 Then
        Sendkeys ("{END}")
    End If
   
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxAna_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxAna_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxAna_LostFocus()
    On Error GoTo TrataErro

    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i
    
    EcAuxAna.Visible = False
    If FGAna.Enabled = True Then FGAna.SetFocus
    Me.Enabled = True
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxAna_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxAna_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxAna_KeyPress(KeyAscii As Integer)
    On Error GoTo TrataErro
    
    If Enter = True Then KeyAscii = 0

Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxAna_KeyPress: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxAna_KeyPress"
    Exit Sub
    Resume Next
End Sub


Public Sub EcAuxAna_KeyDown(KeyCode As Integer, Shift As Integer)
     On Error GoTo TrataErro
    Dim a1 As String
    Dim CodigoMarcado As String
    Dim i As Long
    Dim aux As String
    Dim an_aux As String
    Dim rv As Integer
    Dim iRec As Long
    
    ' UTILIZADOR MARCA UMA MAS PODE MAPEAR PARA OUTRA. ESTA VARIAVEL GUARDA O QUE ELE REALMENTE DIGITA
    Dim AnaFacturavel As String
    a1 = EcAuxAna
    ' For�a o formato "0".
    'DoEvents
    Enter = False
    If (KeyCode = 27 And Shift = 0) Or (KeyCode = 13 And Shift = 0 And Trim(EcAuxAna.text) = "") Then
        Enter = True
        EcAuxAna.Visible = False
        FGAna.SetFocus
    ElseIf (KeyCode = 13 And Shift = 0) Or (KeyCode = 9 And Shift = 0) Then
        Enter = True
        DoEvents
        CodigoMarcado = EcAuxAna
        
        Select Case LastColA
            Case lColFgAnaCodigo ' Codigo da An�lise
                AdicionaAnalise CodigoMarcado, False, False, ""
            Case lColFgAnaP1 ' P1
                iRec = DevIndice(RegistosA(LastRowA).codAna)
                If FGAna.TextMatrix(LastRowA, lColFgAnaCodigo) <> "" Then
                    If iRec > -1 Then
                        For i = 1 To RegistosRM.Count - 1
                            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                RegistosRM(i).ReciboP1 = CodigoMarcado
                                PreencheFgAnaEFR CLng(i), CLng(EcCodEFR)
                                FGAna.TextMatrix(LastRowA, lColFgAnaP1) = CodigoMarcado
                                RegistosRM(i).ReciboOrdemMarcacao = RetornaOrdemP1(i)
                            End If
                        Next
                    End If
                End If
            Case lColFgAnaNrBenef ' nr benef
                iRec = DevIndice(RegistosA(LastRowA).codAna)
                CodigoMarcado = UCase(CodigoMarcado)
                'If FGAna.TextMatrix(LastRowA, lColFgAnaNrBenef) <> "" Then
                    If iRec > -1 Then
                        For i = 1 To RegistosRM.Count - 1
                            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                RegistosRM(i).ReciboNrBenef = CodigoMarcado
                                PreencheFgAnaEFR CLng(i), CLng(EcCodEFR)
                                FGAna.TextMatrix(LastRowA, lColFgAnaNrBenef) = CodigoMarcado
                            End If
                        Next
                    End If
                'End If
            Case lColFgAnaMedico ' MEDICO
                iRec = DevIndice(RegistosA(LastRowA).codAna)
                If FGAna.TextMatrix(LastRowA, lColFgAnaCodigo) <> "" Then
                    If iRec > -1 Then
                        For i = 1 To RegistosRM.Count - 1
                            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                RegistosRM(i).ReciboCodMedico = CodigoMarcado
                                PreencheFgAnaEFR CLng(i), CLng(EcCodEFR)
                                FGAna.TextMatrix(i, lColFgAnaMedico) = CodigoMarcado
                            End If
                        Next
                    End If
                End If
            Case lColFgAnaPreco ' TAXA
                iRec = DevIndice(RegistosA(LastRowA).codAna)
                aux = Replace(Trim(CodigoMarcado), ",", ".")
                If (IsNumeric(aux)) Then
                    If (CLng(aux) = 0) Then
                        CodigoMarcado = "0"
                    End If
                End If
                CodigoMarcado = Replace(CodigoMarcado, ".", ",")
                If FGAna.TextMatrix(LastRowA, lColFgAnaCodigo) <> "" Then
                    If iRec > -1 Then
                        For i = 1 To RegistosRM.Count - 1
                            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                If EliminaReciboManual(i, RegistosRM(i).ReciboCodFacturavel, False) = True Then
                                    If (RegistosRM(i).ReciboIsencao = gTipoIsencaoIsento Or RegistosRM(i).ReciboIsencao = gTipoIsencaoBorla) And CodigoMarcado > 0 Then
                                        RegistosRM(i).ReciboIsencao = gTipoIsencaoNaoIsento
                                        PreencheFgAnaEFR CLng(i), CLng(EcCodEFR)
                                        BG_Mensagem mediMsgBox, "An�lise passou a N�O ISENTA", vbInformation, ""
                                    End If
                                    RegistosRM(i).ReciboTaxa = CodigoMarcado
                                    AdicionaAnaliseAoReciboManual i, False
                                    FGAna.TextMatrix(LastRowA, lColFgAnaPreco) = EcAuxAna
                                End If
                            End If
                        Next
                    End If
                End If
        End Select
        CodigoMarcado = ""
        EcAuxAna = ""
        EcAuxAna.Visible = False
        If FGAna.row <= FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
            FGAna.row = FGAna.row + 1
            'FgAna.Col = 1
        Else
            Fgana_RowColChange
        End If
        If FGAna.Enabled = True Then
            FGAna.SetFocus
        End If
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxAna_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxAna_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxProd_GotFocus()
     On Error GoTo TrataErro
   
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i
    
    If LastColP = 5 Then
        EcAuxProd.Tag = adDate
    Else
        EcAuxProd.Tag = ""
    End If
    
    If EcAuxProd.SelLength = 0 Then
        Sendkeys ("{END}")
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxProd_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxProd_GotFocus"
    Exit Sub
    Resume Next
End Sub





Private Sub EcAuxQuestao_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EstrutQuestao(FgQuestoes.row).Resposta = EcAuxQuestao
        FgQuestoes.TextMatrix(FgQuestoes.row, 1) = EstrutQuestao(FgQuestoes.row).Resposta
        FgQuestoes.SetFocus
    End If
End Sub

Private Sub EcAuxQuestao_LostFocus()
    EcAuxQuestao.Visible = False
    EcAuxQuestao = ""
End Sub

Private Sub EcAuxRec_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    If KeyCode = vbKeyReturn Then
        If FGRecibos.Col = lColRecibosPercentagem Then
            RegistosR(FGRecibos.row).Desconto = BL_HandleNull(CDbl(100 - CDbl(EcAuxRec)), 0)
            RegistosR(FGRecibos.row).ValorPagar = Round((RegistosR(FGRecibos.row).ValorOriginal * CDbl(EcAuxRec)) / 100, 2)
            FGRecibos.TextMatrix(FGRecibos.row, lColRecibosPercentagem) = EcAuxRec
            FGRecibos.TextMatrix(FGRecibos.row, lColRecibosValorPagar) = RegistosR(FGRecibos.row).ValorPagar
            EcAuxRec.Visible = False
            EcAuxRec = ""
            If RegistosR(FGRecibos.row).ValorPagar > 0 Then
            
                RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "N�o Emitido"
            ElseIf RegistosR(FGRecibos.row).ValorPagar = 0 Then
                RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "Recibo Nulo"
            End If
            If RegistosR(FGRecibos.row).ValorPagar > 0 Then
                RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido
            Else
                RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo
            End If
        ElseIf FGRecibos.Col = lColRecibosValorPagar Then
            If IsNumeric(EcAuxRec) Then
                RegistosR(FGRecibos.row).Desconto = CDbl(100 - BL_HandleNull(Replace(EcAuxRec, ".", ",") * 100 / (RegistosR(FGRecibos.row).ValorOriginal), 0))
                RegistosR(FGRecibos.row).ValorPagar = Replace(EcAuxRec, ".", ",")
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosPercentagem) = Round(CDbl(100 - BL_HandleNull(CDbl(RegistosR(FGRecibos.row).Desconto), 0)), 2)
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosValorPagar) = RegistosR(FGRecibos.row).ValorPagar
                EcAuxRec.Visible = False
                EcAuxRec = ""
                If RegistosR(FGRecibos.row).ValorPagar > 0 Then
                
                    RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido
                    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "N�o Emitido"
                ElseIf RegistosR(FGRecibos.row).ValorPagar = 0 Then
                    RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo
                    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "Recibo Nulo"
                End If
                If RegistosR(FGRecibos.row).ValorPagar > 0 Then
                    RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido
                Else
                    RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo
                End If
            End If
        Else
            EcAuxRec.Visible = False
            EcAuxRec = ""
        End If
    End If
Exit Sub
TrataErro:
    RegistosR(FGRecibos.row).Desconto = 0
    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosPercentagem) = "100"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxRec_LostFocus()
    On Error GoTo TrataErro
    EcAuxRec.Visible = False
    EcAuxRec = ""

Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxRec_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxRec_LostFocus"
    Exit Sub
    Resume Next
End Sub

Public Sub EcAuxProd_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
   
    Enter = False
    
    If (KeyCode = 27 And Shift = 0) Or (KeyCode = 13 And Shift = 0 And Trim(EcAuxProd.text) = "") Then
        Enter = True
        EcAuxProd.Visible = False
        If FgProd.Enabled = True Then FgProd.SetFocus
    ElseIf (KeyCode = 13 And Shift = 0) Or (KeyCode = 9 And Shift = 0) Then
        Enter = True
        Select Case LastColP
            Case 0 ' Codigo do produto
                If Verifica_Prod_ja_Existe(LastRowP, EcAuxProd.text) = False Then
                    If SELECT_Descr_Prod = True Then
                        If Trim(EcDataPrevista.text) <> "" Then
                            FgProd.TextMatrix(LastRowP, 5) = Trim(EcDataPrevista.text)
                            RegistosP(LastRowP).DtPrev = Trim(EcDataPrevista.text)
                        End If
                        If Trim(EcDataChegada.text) <> "" Then
                            FgProd.TextMatrix(LastRowP, 6) = Trim(EcDataChegada.text)
                            RegistosP(LastRowP).DtChega = Trim(EcDataChegada.text)
                        End If

                        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
                        FgProd.TextMatrix(LastRowP, 7) = RegistosP(LastRowP).EstadoProd
                        
                        FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                        RegistosP(LastRowP).CodProd = UCase(EcAuxProd.text)
                        EcAuxProd.Visible = False
                        If FgProd.row = FgProd.rows - 1 Then
                            'Cria linha vazia
                            FgProd.AddItem ""
                            CriaNovaClasse RegistosP, FgProd.row + 1, "PROD"
                            FgProd.row = FgProd.row + 1
                            FgProd.Col = 0
                        Else
                            FgProd.Col = 2
                        End If
                    End If
                Else
                    Beep
                    BG_Mensagem mediMsgStatus, "Produto j� indicado!"
                End If
            Case 2 ' Codigo da especificacao
                If SELECT_Especif = True Then
                    FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                    RegistosP(LastRowP).CodEspecif = UCase(EcAuxProd.text)
                    EcAuxProd.Visible = False
                    FgProd.Col = 5
                End If
            Case 4 ' volume
                If Trim(EcAuxProd.text) <> "" Then
                    FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                    RegistosP(LastRowP).Volume = UCase(EcAuxProd.text)
                    EcAuxProd.Visible = False
                    FgProd.Col = 0
                End If
            Case 5 ' Data Prevista
                If Trim(EcAuxProd.text) <> "" Then
                    If BG_ValidaTipoCampo_ADO(Me, EcAuxProd) = True Then
                        FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                        RegistosP(LastRowP).DtPrev = UCase(EcAuxProd.text)
                        EcAuxProd.Visible = False
                        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
                        FgProd.TextMatrix(LastRowP, 7) = RegistosP(LastRowP).EstadoProd
                        FgProd.Col = 0
                    Else
                        Beep
                        BG_Mensagem mediMsgStatus, "Data inv�lida!"
                    End If
                    
                End If
        End Select
        If FgProd.Enabled = True Then FgProd.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxProd_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxProd_KeyDown"
    Exit Sub
    Resume Next
End Sub
Private Sub EcAuxProd_KeyPress(KeyAscii As Integer)
    On Error GoTo TrataErro
    
    If Enter = True Then KeyAscii = 0
    Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxProd_KeyPress: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxProd_KeyPress"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxProd_LostFocus()
    On Error GoTo TrataErro
   
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i
    
    
    EcAuxProd.Visible = False
    FgProd.SetFocus
    Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxProd_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxProd_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxTubo_LostFocus()
     On Error GoTo TrataErro
   
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i
    
    
    EcAuxTubo.Visible = False
    FGTubos.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxTubo_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxTubo_KeyDown"
    Exit Sub
    Resume Next
End Sub
Private Sub EcCodAnaAcresc_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim sSql As String
    Dim rsAnalises As New ADODB.recordset
    Dim taxa As String
    Dim mens As String
    Dim i As Long
    On Error GoTo TrataErro
    
    If KeyCode = 13 Then
        If UCase(Mid(EcCodAnaAcresc, 1, 1)) <> "S" And UCase(Mid(EcCodAnaAcresc, 1, 1)) <> "C" And UCase(Mid(EcCodAnaAcresc, 1, 1)) <> "P" Then
            If gSGBD = gSqlServer Then
                sSql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE substring(cod_ana,2,10) = " & BL_TrataStringParaBD(UCase(EcCodAnaAcresc.text))
            ElseIf gSGBD = gOracle Then
                sSql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE upper(substr(cod_ana,2,10))= " & BL_TrataStringParaBD(UCase(EcCodAnaAcresc.text))
            End If
            rsAnalises.CursorLocation = adUseServer
            rsAnalises.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAnalises.Open sSql, gConexao
            
            If rsAnalises.RecordCount > 0 Then
                EcCodAnaAcresc = rsAnalises!cod_ana
                EcDescrAnaAcresc = rsAnalises!descr_ana
                EcQtdAnaAcresc = "1"
                rsAnalises.Close
            Else
                EcCodAnaAcresc = ""
                EcDescrAnaAcresc = ""
                EcQtdAnaAcresc = ""
                rsAnalises.Close
                Exit Sub
            End If
        Else
            If gSGBD = gSqlServer Then
                sSql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE upper(cod_ana) = " & BL_TrataStringParaBD(UCase(EcCodAnaAcresc.text))
            ElseIf gSGBD = gOracle Then
                sSql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE upper(cod_ana)= " & BL_TrataStringParaBD(UCase(EcCodAnaAcresc.text))
            End If
            rsAnalises.CursorLocation = adUseServer
            rsAnalises.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAnalises.Open sSql, gConexao
            
            If rsAnalises.RecordCount > 0 Then
                EcCodAnaAcresc = rsAnalises!cod_ana
                EcDescrAnaAcresc = rsAnalises!descr_ana
                EcQtdAnaAcresc = "1"
                EcTaxaAnaAcresc = RetornaPrecoAnaRec(EcCodAnaAcresc, RegistosR(FGRecibos.row).codEntidade, CInt(EcQtdAnaAcresc))
                rsAnalises.Close
            Else
                EcCodAnaAcresc = ""
                EcDescrAnaAcresc = ""
                EcQtdAnaAcresc = ""
                rsAnalises.Close
            End If
        End If
        EcQtdAnaAcresc.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodAnaAcresc_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodAnaAcresc_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub EcCodAnaAcresc_LostFocus()
    EcCodAnaAcresc_KeyDown 13, 0
End Sub

Private Sub EcCodEFR_LostFocus()
    Dim iRec As Long
    On Error GoTo TrataErro
    Dim i As Long
    
    If EcCodEFR <> "" And CbTipoIsencao.ListIndex > mediComboValorNull Then
        If CbTipoIsencao.ItemData(CbTipoIsencao.ListIndex) = gTipoIsencaoIsento Then
            CbHemodialise.ListIndex = mediComboValorNull
            CbHemodialise_Click
            CbHemodialise.Enabled = False
            If VerificaEfrHemodialise(EcCodEFR) = True Then
                CbHemodialise.Enabled = True
            End If
        Else
            CbHemodialise.ListIndex = mediComboValorNull
            CbHemodialise_Click
            CbHemodialise.Enabled = False
        End If
    Else
        CbHemodialise.ListIndex = mediComboValorNull
        CbHemodialise_Click
        CbHemodialise.Enabled = False
    End If
    
    If EcCodEFR <> "" And EcCodEFR <> CodEfrAUX And RegistosA.Count > 1 Then
        CodEfrAUX = EcCodEFR
            
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboNumDoc = "0" Or RegistosRM(i).ReciboNumDoc = "" Then
                AlteraEntidadeReciboManual i, EcCodEFR, EcDescrEFR, "", -1
                VerificaEFRCodigoFilho RegistosRM(i).ReciboCodFacturavel, EcCodEFR, EcDescrEFR, i
            End If
        Next
        'BRUNODSANTOS Cedivet-218
'        For i = 1 To RegistosA.Count - 1
'            iRec = DevIndice(RegistosA(i).codAna)
'            If iRec = -1 Then
'                AdicionaReciboManual 0, EcCodEFR, "0", "0", RegistosA(i).codAna, 1, "", RegistosA(i).codAna, False
'            End If
'        Next
        '
        If EcNumReq <> "" And EcUtilizadorCriacao <> "" Then
            FuncaoModificar (True)
        End If
        
        PreencheEntidades

    End If
    PreencheEntidades
    ActualizaLinhasRecibos -1
    If CbEFR.ListIndex > -1 Then
        PreencheFgAnaEFR_todos CbEFR.ItemData(CbEFR.ListIndex)
    Else
        LimpaFGAnaEfr
    End If
    CodEfrAUX = EcCodEFR
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodEFR_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodEFR_LostFocus"
    Exit Sub
    Resume Next
End Sub

Public Sub EcCodEFR_Validate(cancel As Boolean)
    cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, "")
End Sub




Private Sub EcCodMedico_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodMedico.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT nome_med FROM sl_medicos WHERE cod_med='" & Trim(EcCodMedico.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Tabela.Close
            sql = "SELECT cod_med, nome_med FROM sl_medicos WHERE cod_ordem ='" & Replace(UCase(Trim(EcCodMedico.text)), "M", "") & "'"
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            Tabela.Open sql, gConexao
            If Tabela.RecordCount = 0 Then
            
                cancel = True
                gMsgTitulo = "Pesquisa M�dico"
                gMsgMsg = " N�o existe m�dico com esse c�digo! " & vbCrLf & " Deseja criar? "
                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                If gMsgResp = vbYes Then
                    FormMedicos.Show
                Else
                    EcCodMedico.text = ""
                    EcNomeMedico.text = ""
                End If
            Else
                EcNomeMedico.text = Tabela!nome_med
                EcCodMedico.text = Tabela!cod_med
            End If
        Else
            EcNomeMedico.text = Tabela!nome_med
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcNomeMedico.text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodMedico_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodMedico_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataAlteracao_Validate(cancel As Boolean)
     On Error GoTo TrataErro
   
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataAlteracao)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataAlteracao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataAlteracao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataChegada_GotFocus()
    On Error GoTo TrataErro

    If Trim(EcDataChegada.text) = "" Then
        EcDataChegada.text = Bg_DaData_ADO
    End If
    EcDataChegada.SelStart = 0
    EcDataChegada.SelLength = Len(EcDataChegada)
 Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataChegada_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataChegada_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataChegada_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    
    If EcDataChegada.text <> "" Then
        cancel = Not ValidaDataChegada
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataChegada_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataChegada_Validate"
    Exit Sub
    Resume Next
End Sub




Private Sub EcDtPretend_LostFocus()
  'FrameDtPretend.Visible = False
End Sub

Private Sub EcDtPretend_Validate(cancel As Boolean)
     On Error GoTo TrataErro
   
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtPretend)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDtPretend_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDtPretend_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataCriacao_Validate(cancel As Boolean)
     On Error GoTo TrataErro
   
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataCriacao)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataCriacao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataCriacao_Validate"
    Exit Sub
    Resume Next
End Sub


Private Sub EcDataImpressao_Validate(cancel As Boolean)
     On Error GoTo TrataErro
   
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataImpressao)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataImpressao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataImpressao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataImpressao2_Validate(cancel As Boolean)
     On Error GoTo TrataErro
   
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataImpressao2)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataImpressao2_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataImpressao2_Validate"
    Exit Sub
    Resume Next
End Sub

Public Sub EcDataPrevista_GotFocus()
    On Error GoTo TrataErro
    Dim Campos(5) As String
    Dim retorno() As String
    Dim iret As Integer
    Dim i As Integer
    'Para evitar que a nova entidade seja reposta pela codificada
    ' no utente
    If gPreencheEFRUtente = 1 Then
        If (Trim(EcCodEFR.text) = "") Then
            Call PreencheEFRUtente
        End If
    End If
    If EcMorada = "" And EcUtente <> "" Then
        Campos(0) = "descr_mor_ute"
        Campos(1) = "cod_postal_ute"
        Campos(2) = "n_benef_ute"
        Campos(3) = "cod_isencao_ute"
        Campos(4) = "cod_pais"
        Campos(5) = "num_doe_prof"
        iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)
        
        If iret = -1 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar utente!", vbCritical + vbOKOnly, App.ProductName
            CbTipoUtente.ListIndex = mediComboValorNull
            EcUtente.text = ""
            Exit Sub
        End If
        
        EcMorada = retorno(0)
        EcCodPostal = Mid(retorno(1), 1, 4)
        EcRuaPostal = Mid(retorno(1), 6)
        EcCodPostalAux = Mid(retorno(1), 1, 8)
        EcDescrPostal = BL_SeleccionaDescrPostal(EcCodPostalAux)
        EcCodPais = retorno(4)
        EcCodpais_Validate False
        EcDoenteProf = retorno(5)
        'RGONCALVES 29.05.2013 Cedivet-29
        If BG_DaComboSel(CbTipoIsencao) = mediComboValorNull Then
        '
            For i = 0 To CbTipoIsencao.ListCount - 1
                If CbTipoIsencao.ItemData(i) = gTipoIsencaoNaoIsento Then
                    CbTipoIsencao.ListIndex = i
                    Exit For
                End If
            Next
        End If
        
        'PreencheCodigoPostal
        
        If retorno(3) <> "" Then
            'RGONCALVES 29.05.2013 Cedivet-29
            If BG_DaComboSel(CbTipoIsencao) = mediComboValorNull Then
            '
                For i = 0 To CbTipoIsencao.ListCount - 1
                    If CbTipoIsencao.ItemData(i) = retorno(3) Then
                        CbTipoIsencao.ListIndex = i
                        Exit For
                    End If
                Next
            End If
        End If
        If CbUrgencia.ListCount > 0 Then
            CbUrgencia.ListIndex = 0
        End If
        If CbConvencao.ListCount > 0 Then
            CbConvencao.ListIndex = 0
        End If
        'EcNumBenef = Retorno(2)
    End If
    
    Call ValidaDataPrevista
    
    
    EcDataPrevista.SelStart = 0
    EcDataPrevista.SelLength = Len(EcDataPrevista)
    If EcDataChegada = "" Then
        EcDataChegada = EcDataPrevista
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataPrevista_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataPrevista_GotFocus"
    Exit Sub
    Resume Next
End Sub

Sub Preenche_Data_Prev_Prod()
    On Error GoTo TrataErro
    
    Dim i As Integer
    
    ExecutaCodigoP = False
    For i = 1 To RegistosP.Count - 1
        If RegistosP(i).CodProd <> "" Then
            If Trim(RegistosP(i).DtPrev) = "" Then
                RegistosP(i).DtPrev = EcDataPrevista.text
                FgProd.TextMatrix(i, 4) = EcDataPrevista.text
                RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
                FgProd.TextMatrix(i, 6) = RegistosP(i).EstadoProd
            End If
        If RegistosP(i).EstadoProd = "" Then
            RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
            FgProd.TextMatrix(i, 6) = RegistosP(i).EstadoProd
        End If
        End If
    Next i
    
    ExecutaCodigoP = True
Exit Sub
TrataErro:
    BG_LogFile_Erros "Preenche_Data_Prev_Prod: " & Err.Number & " - " & Err.Description, Me.Name, "Preenche_Data_Prev_Prod"
    Exit Sub
    Resume Next
End Sub

Function ValidaDataChegada() As Boolean
    On Error GoTo TrataErro

    ValidaDataChegada = False
    If EcDataChegada.text <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcDataChegada) Then
'            If DateValue(EcDataChegada.Text) > DateValue(Bg_DaData_ADO) Then
'                BG_Mensagem mediMsgBox, "A data de Chegada tem que ser inferior ou igual � data actual ", vbInformation, "Aten��o"
'                SendKeys ("{HOME}+{END}")
'                If EcDataChegada.Enabled = True Then
'                    EcDataChegada.SetFocus
'                End If
'            Else
'                ValidaDataChegada = True
'            End If
            ValidaDataChegada = True
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaDataChegada: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaDataChegada"
    ValidaDataChegada = False
    Exit Function
    Resume Next
End Function

Function ValidaDataPrevista() As Boolean
    On Error GoTo TrataErro

    ValidaDataPrevista = False
    If EcDataPrevista.text <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcDataPrevista) And EcDataPrevista.Enabled = True Then
            ValidaDataPrevista = True
        End If
    ElseIf EcDataPrevista.text = "" Then
        If Flg_LimpaCampos = False Then
            If EcFimSemana <> "1" Then
                EcDataPrevista.text = Format(Bg_DaData_ADO, gFormatoData)
                ValidaDataPrevista = True
            End If
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaDataPrevista: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaDataPrevista"
    ValidaDataPrevista = False
    Exit Function
    Resume Next
End Function

Public Sub EcDataPrevista_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    
    
    If Trim(EcDataPrevista.text) <> "" Then
        cancel = Not ValidaDataPrevista
    End If
    If cancel = True Then Exit Sub
    
    If EcDataPrevista.Enabled = True And Trim(EcSeqUtente.text) <> "" Then
        Preenche_Data_Prev_Prod
        SSTGestReq.TabEnabled(1) = True
        SSTGestReq.TabEnabled(2) = True
        SSTGestReq.TabEnabled(3) = True
        SSTGestReq.TabEnabled(4) = True
        SSTGestReq.TabEnabled(5) = True
        SSTGestReq.TabEnabled(6) = True
        FrProdutos.Enabled = True
        FrBtProd.Enabled = True
        FgProd.row = LastRowP
        FgProd.Col = LastColP
        ExecutaCodigoP = True
        FrBtTubos.Enabled = True
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataPrevista_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataPrevista_Validate"
    Exit Sub
    Resume Next
End Sub


Private Sub EcNome_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsNome As New ADODB.recordset
    Dim SeqUtente As String
    
    'soliveira LJM 06.11.2008
    If EcNumReq.text = "" And EcNome.text <> "" Then
        sSql = BL_Upper_Campo("SELECT seq_utente FROM sl_identif WHERE nome_ute = " & BL_TrataStringParaBD(EcNome), "nome_ute", gPesquisaDentroCampo)
        Set RsNome = BG_ExecutaSELECT(sSql)
        SeqUtente = ""
        If RsNome.RecordCount >= 1 Then
            SeqUtente = "("
            While Not RsNome.EOF
                SeqUtente = SeqUtente & RsNome!seq_utente & ", "
                RsNome.MoveNext
            Wend
            SeqUtente = Mid(SeqUtente, 1, Len(SeqUtente) - 2) & ")"
            
            EcSeqUtente.Tag = adVarChar
            EcSeqUtente.MaxLength = 100
            EcSeqUtente = SeqUtente
        'soliveira ualia
        Else
            EcSeqUtente = -1
        End If
        BG_DestroiRecordSet RsNome
    'soliveira ualia
    ElseIf EcNumReq.text = "" And EcNome.text = "" Then
        EcSeqUtente = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNome_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcNome_Validate"
    Exit Sub
    Resume Next
End Sub



Private Sub EcNumBenef_GotFocus()
    On Error GoTo TrataErro
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodEFR.text <> "" And Trim(EcNumBenef.text) = "" Then
        Set Tabela = New ADODB.recordset
        Tabela.CursorLocation = adUseServer
        Tabela.CursorType = adOpenStatic
        sql = "SELECT sigla,formato1,formato2 FROM sl_efr WHERE cod_efr=" & EcCodEFR.text
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            Formato1 = Trim(BL_HandleNull(Tabela!Formato1, ""))
            Formato2 = Trim(BL_HandleNull(Tabela!Formato2, ""))
            EcNumBenef.text = Trim(BL_HandleNull(Tabela!sigla, ""))
            Sendkeys ("{END}")
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumBenef_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumBenef_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumBenef_LostFocus()
    On Error GoTo TrataErro
    EcNumBenef = UCase(EcNumBenef)
    If EcNumBenef = "" Then Exit Sub
    ' PARA ENTIDADE GNR FORMATA O NUMERO DO CARTAO
    
    If gCodEfrGNR = EcCodEFR And gTipoInstituicao = "PRIVADA" Then
        If Mid(EcNumBenef, 1, 1) = "G" Then
            EcNumBenef = Mid(EcNumBenef, 3, 10)
        Else
            EcNumBenef = Mid(EcNumBenef, 1, 10)
        End If
    ' PARA ENTIDADE PT FORMATA O NUMERO DO CARTAO
    ElseIf gCodEfrPT = EcCodEFR And gTipoInstituicao = "PRIVADA" Then
        If Len(Trim(EcNumBenef)) = 14 Then
            EcNumBenef.text = Mid(EcNumBenef, 3, 8) & "/" & Mid(EcNumBenef, 11, 2)
            While Mid(EcNumBenef.text, 1, 1) = "0"
                EcNumBenef.text = Mid(EcNumBenef.text, 2)
            Wend
        Else
            EcNumBenef.text = EcNumBenef
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumBenef_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumBenef_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumBenef_Validate(cancel As Boolean)
     On Error GoTo TrataErro
   
    Dim Formato As Boolean
    
    Formato = Verifica_Formato(UCase(EcNumBenef.text), Formato1, Formato2)
    If Formato = False And EcNumBenef.text <> "" Then
        cancel = True
        Sendkeys ("{END}")
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumBenef_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumBenef_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumReq_GotFocus()
    On Error GoTo TrataErro
    Set CampoActivo = Me.ActiveControl
    If gLAB <> "CL" And gLAB <> "CEDIVET" Then
        cmdOK.Default = True
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumReq_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumReq_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumReq_LostFocus()
    On Error GoTo TrataErro
    
    cmdOK.Default = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumReq_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumReq_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumReq_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcNumReq)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumReq_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumReq_Validate"
    Exit Sub
    Resume Next
End Sub



Private Sub EcObsAna_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    If EcObsAna.text = "" Then
        Acumula = ""
        
    End If
    If KeyCode = 27 Or KeyCode = 13 Then
        EcObsAna_LostFocus
    ElseIf (KeyCode) = 220 Then
        Acumula = Acumula & "\"
    ElseIf KeyCode = 32 Then
        If Len(Acumula) > 2 Then
            If Mid(Acumula, 1, 2) = "\\" Then
                EcObsAna = (EcObsAna)
                EcObsAna.text = Replace(EcObsAna.text, (Acumula), BL_SelCodigo("SL_DICIONARIO", "DESCR_FRASE", "COD_FRASE", Mid(Trim(UCase(Acumula)), 3), "V"))
                EcObsAna.SetFocus
                Sendkeys ("{END}")
                Acumula = ""
                Exit Sub
            Else
                'EcObsAna = Replace(EcObsAna, acumula, "")
                Acumula = ""
                'EcObsAna.SetFocus
                'SendKeys ("{END}")
            End If
        Else
            If Acumula <> "" Then
                'EcObsAna = Replace(EcObsAna, acumula, "")
                Acumula = ""
                'EcObsAna.SetFocus
                'SendKeys ("{END}")
            End If
        End If
        Acumula = ""
    ElseIf KeyCode = vbKeyBack And Acumula <> "" Then
        Acumula = Mid(Acumula, 1, Len(Acumula) - 1)
    ElseIf ((KeyCode >= 65 And KeyCode <= 226) Or (KeyCode >= vbKey0 And KeyCode <= vbKey9)) And Shift = 0 Then
        Acumula = Acumula & LCase(Chr(KeyCode))
    ElseIf (KeyCode >= 65 And KeyCode <= 226) And Shift = 1 Then
        Acumula = Acumula & UCase(Chr(KeyCode))
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcObsAna_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "EcObsAna_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub EcObsAna_LostFocus()
    On Error GoTo TrataErro
    Acumula = ""
    If linhaAnaliseOBS = -1 Then
        linhaAnaliseOBS = LastRowA
    End If
    RegistosA(linhaAnaliseOBS).ObsAnaReq = Trim(EcObsAna.text)
        
'    If EcNumReq <> "" Then
'        Grava_ObsAnaReq (linhaAnaliseOBS)
'    End If
    If RegistosA(linhaAnaliseOBS).codAna <> "" And RegistosA(linhaAnaliseOBS).ObsAnaReq <> "" Then
        BL_GravaObsAnaReq BL_HandleNull((EcNumReq), 0), RegistosA(linhaAnaliseOBS).codAna, Trim(EcObsAna.text), _
                          CLng(RegistosA(linhaAnaliseOBS).seqObsAnaReq), 0, BL_HandleNull((EcSeqUtente), 0), mediComboValorNull
        PreencheNotas
    End If
    
    EcObsAna.text = ""
    FrameObsAna.Visible = False
    FGAna.SetFocus
    linhaAnaliseOBS = -1
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcObsAna_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcObsAna_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcPesqRapMedico_Change()
     On Error GoTo TrataErro
   Dim rsCodigo As ADODB.recordset
    Dim sql As String
    
    If EcPesqRapMedico.text <> "" Then
        Set rsCodigo = New ADODB.recordset

        sql = "SELECT cod_med,nome_med FROM sl_medicos WHERE seq_med = '" & EcPesqRapMedico & "'"
        
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do m�dico!", vbExclamation, "Pesquisa r�pida"
            EcPesqRapMedico.text = ""
        Else
            EcCodMedico.text = Trim(rsCodigo!cod_med)
            EcNomeMedico.text = Trim(rsCodigo!nome_med)
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "EcPesqRapMedico_Change: " & Err.Number & " - " & Err.Description, Me.Name, "EcPesqRapMedico_Change"
    Exit Sub
    Resume Next
End Sub

Private Sub EcPesqRapProduto_Change()
    On Error GoTo TrataErro
    
    Dim rsCodigo As ADODB.recordset
    Dim indice As Integer
    
    If EcPesqRapProduto.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        rsCodigo.Open "SELECT cod_produto FROM sl_produto WHERE seq_produto = " & EcPesqRapProduto, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do produto!", vbExclamation, "Pesquisa r�pida"
        Else
            EcAuxProd.text = BL_HandleNull(rsCodigo!cod_produto, "")
            EcAuxProd.left = FgProd.CellLeft + 270
            EcAuxProd.top = FgProd.CellTop + 2060
            EcAuxProd.Width = FgProd.CellWidth + 20
            EcAuxProd.Visible = True
            EcAuxProd.Enabled = True
            EcAuxProd.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcPesqRapProduto_Change: " & Err.Number & " - " & Err.Description, Me.Name, "EcPesqRapProduto_Change"
    Exit Sub
    Resume Next
End Sub



Private Sub EcPesqRapTubo_Change()
    On Error GoTo TrataErro
    
    Dim rsCodigo As ADODB.recordset
    Dim indice As Integer
    
    If EcPesqRapTubo.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT cod_tubo FROM sl_tubo WHERE seq_tubo = " & EcPesqRapTubo, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do tubo!", vbExclamation, "Pesquisa r�pida"
        Else
            EcAuxTubo.text = BL_HandleNull(rsCodigo!cod_tubo, "")
            EcAuxTubo.left = FGTubos.CellLeft + 270
            EcAuxTubo.top = FGTubos.CellTop + 2060
            EcAuxTubo.Width = FGTubos.CellWidth + 20
            EcAuxTubo.Visible = True
            EcAuxTubo.Enabled = True
            EcAuxTubo.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcPesqRapTubo_Change: " & Err.Number & " - " & Err.Description, Me.Name, "EcPesqRapTubo_Change"
    Exit Sub
    Resume Next
End Sub



Private Sub EcQtdAnaAcresc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        If IsNumeric(EcQtdAnaAcresc) = False Then
            EcQtdAnaAcresc = "1"
        End If
        EcTaxaAnaAcresc = RetornaPrecoAnaRec(EcCodAnaAcresc, RegistosR(FGRecibos.row).codEntidade, CInt(EcQtdAnaAcresc))
        If BtAnaAcresc.Enabled = True Then
            btAnaAcresc_Click
        End If
    End If
End Sub

Private Sub EcQtdAnaAcresc_LostFocus()
    EcQtdAnaAcresc_KeyDown 13, 0
End Sub

Public Sub EcUtente_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    
    Dim sql As String
    Dim Campos(1) As String
    Dim retorno() As String
    Dim iret As Integer
    
    'Limpa a estrutura de dados do utente
    If CbTipoUtente.ListIndex <> -1 And EcUtente.text <> "" Then
        'Procurar o sequencial e chamar a fun��o que preenche a identifica��o do utente
        Campos(0) = "seq_utente"
        
        iret = BL_DaDadosUtente(Campos, retorno, , CbTipoUtente.text, EcUtente.text)
        
        If iret = -1 Then
            cancel = True
            BG_Mensagem mediMsgBox, "Utente inexistente!", vbInformation + vbOKOnly, "Pesquisa ao utente!"
            EcNome.text = ""
            EcNomeDono.text = ""
            EcDataNasc.text = ""
            EcSexo = ""
            EcIdade.text = ""
        Else
            BtInfClinica.Enabled = True
            BtDomicilio.Enabled = True

            BtConsReq.Enabled = True
            
            EcSeqUtente.text = retorno(0)
            BL_PreencheDadosUtente EcSeqUtente
            
            PreencheDadosUtente
            
        End If
    Else
        EcNome.text = ""
        EcNomeDono.text = ""
        EcDataNasc.text = ""
        EcSexo = ""
        EcIdade.text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtente_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtente_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorAlteracao_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcUtilizadorAlteracao)
    If cancel = False Then
        LbNomeUtilAlteracao = BL_SelNomeUtil(EcUtilizadorAlteracao.text)
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorAlteracao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorAlteracao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorCriacao_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    
    cancel = Not BG_ValidaTipoCampo_ADO(Me, EcUtilizadorCriacao)
    If cancel = False Then
        LbNomeUtilCriacao = BL_SelNomeUtil(EcUtilizadorCriacao.text)
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorCriacao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorCriacao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorImpressao_GotFocus()
    On Error GoTo TrataErro
    
    Set CampoActivo = Me.ActiveControl
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorImpressao_LostFocus()
    On Error GoTo TrataErro
    
    LbNomeUtilImpressao = BL_SelNomeUtil(EcUtilizadorImpressao.text)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorImpressao_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    
    cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorImpressao2_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    
    LbNomeUtilImpressao2 = BL_SelNomeUtil(EcUtilizadorImpressao2.text)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao2_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao2_Validate"
    Exit Sub
    Resume Next
End Sub




Private Sub Fgana_DblClick()
    On Error GoTo TrataErro
    FgAna_KeyDown 13, 0
Exit Sub
TrataErro:
    BG_LogFile_Erros "Fgana_DblClick: " & Err.Number & " - " & Err.Description, Me.Name, "Fgana_DblClick"
    Exit Sub
    Resume Next
End Sub

'Private Sub FGAna_Click()
'
'    MsgBox Me.FGAna_Recibo.ColSel, vbInformation, "ZZ"
'
'End Sub

Private Sub FGAna_GotFocus()
    On Error GoTo TrataErro
    
    If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo)) <> "" Then
        FGAna.CellBackColor = vbRed
        FGAna.CellForeColor = vbWhite
    Else
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbBlack
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGAna_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FGAna_GotFocus"
    Exit Sub
    Resume Next
End Sub

Public Sub FgAna_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    Dim ana As String
    Dim taxa As String
    Dim mens As String
    Dim codAnaEfr As String
    Dim flgPermite As Boolean
    Dim i As Long
    Dim k As Long
    Dim iRec As Long
    
    flgPermite = False
    If LastRowA > FGAna.rows - 1 Then
        LastRowA = FGAna.rows - 1
    End If
    If FGAna.row < RegistosA.Count Then
        iRec = DevIndice(RegistosA(FGAna.row).codAna)
    Else
        iRec = -1
    End If
    Select Case KeyCode
        Case 96
            KeyCode = 48
        Case 97
            KeyCode = 49
        Case 98
            KeyCode = 50
        Case 99
            KeyCode = 51
        Case 100
            KeyCode = 52
        Case 101
            KeyCode = 53
        Case 102
            KeyCode = 54
        Case 103
            KeyCode = 55
        Case 104
            KeyCode = 56
        Case 105
            KeyCode = 57
    End Select
    
    ' --------------------------------------------------------------------------------------------
    ' TECLAS DE ATALHO PARA MUDAR DADOS DAS ANALISES
    ' --------------------------------------------------------------------------------------------
    If KeyCode = 113 Then                   'F2 - Muda credencial
        FGAna.Col = lColFgAnaP1
        KeyCode = 13
        flgPermite = True
    ElseIf KeyCode = 114 Then               'F3 - Muda M�dico
        FGAna.Col = lColFgAnaMedico
        KeyCode = 13
        flgPermite = True
    ElseIf KeyCode = 115 Then               'F4 - Muda entidade
        FGAna.Col = lColFgAnaDescrEFR
        KeyCode = 13
        flgPermite = True
    ElseIf KeyCode = 116 And gPermAltPreco = 1 Then               'F5 - Muda taxa
        FGAna.Col = lColFgAnaPreco
        KeyCode = 13
        flgPermite = True
    ElseIf KeyCode = 122 Then               'F11 - Incrementa n� P1
        Flg_IncrementaP1 = True
        Exit Sub
        flgPermite = True
    End If
    
    
    
    If (FGAna.Col = lColFgAnaCodigo Or (FGAna.Col = lColFgAnaP1 And KeyCode <> 13) Or FGAna.Col = lColFgAnaMedico Or (FGAna.Col = lColFgAnaPreco And gPermAltPreco = 1)) And (KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)) Then ' Enter = Editar
        If FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo) <> "" And FGAna.Col = lColFgAnaCodigo Then Exit Sub
        ' --------------------------------------------------------------------------------------------
        ' SE RECIBO EMITIDO NAO DEIXA ALTERAR VALOR
        ' --------------------------------------------------------------------------------------------
        If FGAna.row < RegistosA.Count And iRec > 0 Then
            If RegistosRM(iRec).ReciboNumDoc <> "0" And RegistosRM(iRec).ReciboNumDoc <> "" And FGAna.Col = 5 Then
                BG_Mensagem mediMsgBox, "J� foi emitido recibo.N�o � possivel alterar TAXA", vbOKOnly + vbInformation, "Taxas"
                Exit Sub
            End If
        End If
        
        If RegistosA(FGAna.row).Estado <> "-1" And RegistosA(FGAna.row).codAna <> "" And FGAna.Col = lColFgAnaCodigo Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser alterada: j� cont�m resultados!"
        Else
            If LastColA <> lColFgAnaCodigo And Trim(FGAna.TextMatrix(LastRowA, lColFgAnaCodigo)) = "" Then
                Beep
                BG_Mensagem mediMsgStatus, "Tem que indicar primeiro uma an�lise!"
            Else
                If EcDataFecho.text <> "" Then
                    Beep
                    BG_Mensagem mediMsgStatus, "N�o pode alterar uma requisi��o fechada!"
                    Exit Sub
                End If
                ExecutaCodigoA = False
                EcAuxAna.text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf(((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)), Chr(KeyCode), "")
                ExecutaCodigoA = True
                If KeyCode = 8 Then
                    If Len(EcAuxAna.text) > 1 Then EcAuxAna.text = Mid(EcAuxAna.text, 1, Len(EcAuxAna.text) - 1)
                End If
                If KeyCode = 13 Then
                    EcAuxAna.SelStart = 0
                    EcAuxAna.SelLength = Len(EcAuxAna)
                Else
                    'edgar.parada Cedivet-248
                    EcAuxAna.SelStart = Len(EcAuxAna)
                    '
                    EcAuxAna.SelLength = 0
                End If

                EcAuxAna.left = FGAna.CellLeft + 360
                EcAuxAna.top = SSTGestReq.top + FGAna.CellTop + 460
                EcAuxAna.Width = FGAna.CellWidth + 20
                EcAuxAna.Visible = True
                EcAuxAna.Enabled = True
                EcAuxAna.SetFocus
                
            End If
        End If
        
    ElseIf FGAna.Col = lColFgAnaP1 And KeyCode = 13 Then
        ' --------------------------------------------------------------------------------------------
        ' DUPLO CLICK ALTERA COR DO P1
        ' --------------------------------------------------------------------------------------------
        If iRec <> -1 Then
            If RegistosRM(iRec).ReciboNumDoc <> "0" Then
                BG_Mensagem mediMsgBox, "J� foi emitido recibo.N�o � possivel alterar tipo P1", vbOKOnly + vbInformation, "Taxas"
                Exit Sub
            End If
            
            EcAuxAna.text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf(((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)), Chr(KeyCode), "")
            If KeyCode = 13 Then
                EcAuxAna.SelStart = 0
                EcAuxAna.SelLength = Len(EcAuxAna)
            Else
                EcAuxAna.SelLength = 0
            End If
    
            EcAuxAna.left = FGAna.CellLeft + 350
            EcAuxAna.top = SSTGestReq.top + FGAna.CellTop + 460
            EcAuxAna.Width = FGAna.CellWidth + 20
            EcAuxAna.Visible = True
            EcAuxAna.Enabled = True
            EcAuxAna.SetFocus
        End If
        
        'AlteraCorP1
    ElseIf FGAna.Col = lColFgAnaNrBenef And KeyCode = 13 Then
        ' --------------------------------------------------------------------------------------------
        ' DUPLO CLICK EDITA NR BENEF
        ' --------------------------------------------------------------------------------------------
        If RegistosRM(iRec).ReciboNumDoc <> "0" And FGAna.Col = 5 Then
            BG_Mensagem mediMsgBox, "J� foi emitido recibo.N�o � possivel alterar N� Benefici�rio", vbOKOnly + vbInformation, "Taxas"
            Exit Sub
        End If
        EcAuxAna.text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf(((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)), Chr(KeyCode), "")
        If KeyCode = 13 Then
            EcAuxAna.SelStart = 0
            EcAuxAna.SelLength = Len(EcAuxAna)
        Else
            EcAuxAna.SelLength = 0
        End If

        EcAuxAna.left = FGAna.CellLeft + 350
        EcAuxAna.top = SSTGestReq.top + FGAna.CellTop + 460
        EcAuxAna.Width = FGAna.CellWidth + 20
        EcAuxAna.Visible = True
        EcAuxAna.Enabled = True
        EcAuxAna.SetFocus
    ElseIf (FGAna.Col = lColFgAnaDescrEFR) And (KeyCode = 13 And Shift = 0) And FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo) <> "" Then  ' Enter = Editar
        ' --------------------------------------------------------------------------------------------
        ' SE DA ENTER NA COLUNA DA ENTIDADE
        ' --------------------------------------------------------------------------------------------
            If iRec > 0 Then
                If BL_HandleNull(RegistosRM(iRec).ReciboNumDoc, "0") <> "0" Then
                    ' JA NAO E MARTELANCO--SOLIVEIRA LHL MARTELAN�O S� NAO PODE MUDAR A ENTIDADE SE O RECIBO ESTIVER NO ESTADO PAGO
                    Exit Sub
                End If
            End If
                        
            PesquisaEntFin
            If EcCodEfr2 <> "" Then

                AlteraEntidadeReciboManual iRec, EcCodEfr2, EcDescrEfr2, "", FGAna.row
                If CbEFR.ListIndex > -1 Then
                    PreencheFgAnaEFR CLng(iRec), CbEFR.ItemData(CbEFR.ListIndex)
                End If
                iRec = DevIndice(RegistosA(FGAna.row).codAna)
                VerificaEFRCodigoFilho RegistosRM(iRec).ReciboCodFacturavel, EcCodEfr2, EcDescrEfr2, FGAna.row
                If CbEFR.ListIndex > mediComboValorNull Then
                    PreencheEntidades
                End If
            End If
    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga inha
        If RegistosA(FGAna.row).Estado <> "-1" And RegistosA(FGAna.row).codAna <> "" Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser eliminada: j� cont�m resultados!"
        ElseIf RegistosA(FGAna.row).codAna <> "" Then
            Dim TempPerfilMarcacao As String
            iRec = DevIndice(RegistosA(FGAna.row).codAna)

            If iRec > 0 Then
                TempPerfilMarcacao = RegistosRM(iRec).ReciboCodFacturavel
            
                If TempPerfilMarcacao = "" Then
                    If EliminaReciboManual(iRec, ana, True) = True Then
                        LimpaColeccao RegistosRM, iRec
                        If CbEFR.ListIndex > mediComboValorNull Then
                            PreencheFgAnaEFR_todos (CbEFR.ItemData(CbEFR.ListIndex))
                        Else
                            LimpaFGAnaEfr
                        End If
                    End If
                    iRec = -1
                    EliminaCodigoFilho RegistosA(FGAna.row).codAna
                    TotalEliminadas = TotalEliminadas + 1
                    ReDim Preserve Eliminadas(TotalEliminadas)
                    Eliminadas(TotalEliminadas).codAna = RegistosA(FGAna.row).codAna
                    Eliminadas(TotalEliminadas).NReq = EcNumReq.text
                    Elimina_Mareq RegistosA(FGAna.row).codAna
                    LbTotalAna = LbTotalAna - 1
                    If LbTotalAna < 0 Then
                        LbTotalAna = 0
                    End If
                    ana = RegistosA(FGAna.row).codAna
                    RegistosA.Remove FGAna.row
                    FGAna.RemoveItem FGAna.row
                    EliminaTuboEstrut
                    EliminaProduto ana
                Else
                    For i = RegistosA.Count - 1 To 1 Step -1
                        iRec = DevIndice(RegistosA(i).codAna)
                        If iRec > -1 Then
                            If RegistosRM(iRec).ReciboCodFacturavel = TempPerfilMarcacao Then
                                If EliminaReciboManual(iRec, ana, True) = True Then
                                    EliminaCodigoFilho RegistosRM(iRec).ReciboCodFacturavel
                                    LimpaColeccao RegistosRM, iRec
                                Else
                                    RegistosRM(iRec).ReciboFlgMarcacaoRetirada = "1"
                                End If
                                TotalEliminadas = TotalEliminadas + 1
                                ReDim Preserve Eliminadas(TotalEliminadas)
                                Eliminadas(TotalEliminadas).codAna = RegistosA(i).codAna
                                Eliminadas(TotalEliminadas).NReq = EcNumReq.text
                                Elimina_Mareq RegistosA(i).codAna
                                LbTotalAna = LbTotalAna - 1
                                If LbTotalAna < 0 Then
                                    LbTotalAna = 0
                                End If
                                ana = RegistosA(i).codAna
                                RegistosA.Remove i
                                FGAna.RemoveItem i
                                EliminaTuboEstrut
                                EliminaProduto ana
                                'Actualiza ord_marca da estrutura MaReq
                                k = 1
                                While k <= UBound(MaReq)
                                    If MaReq(k).Ord_Marca >= FGAna.row Then
                                        MaReq(k).Ord_Marca = MaReq(k).Ord_Marca - 1
                                    End If
                                    k = k + 1
                                Wend
                            End If
                        End If
                        iRec = -1
                    Next
                    PreencheAnaRecibos
                    If CbEFR.ListIndex > mediComboValorNull Then
                        PreencheFgAnaEFR_todos (CbEFR.ItemData(CbEFR.ListIndex))
                    Else
                        LimpaFGAnaEfr
                    End If
                End If
            Else
                EliminaCodigoFilho RegistosA(FGAna.row).codAna
                TotalEliminadas = TotalEliminadas + 1
                ReDim Preserve Eliminadas(TotalEliminadas)
                Eliminadas(TotalEliminadas).codAna = RegistosA(FGAna.row).codAna
                Eliminadas(TotalEliminadas).NReq = EcNumReq.text
                Elimina_Mareq RegistosA(FGAna.row).codAna
                LbTotalAna = LbTotalAna - 1
                If LbTotalAna < 0 Then
                    LbTotalAna = 0
                End If
                ana = RegistosA(FGAna.row).codAna
                RegistosA.Remove FGAna.row
                FGAna.RemoveItem FGAna.row
                EliminaTuboEstrut
                EliminaProduto ana
            End If
        Else
            If FGAna.TextMatrix(FGAna.row, 0) <> "" Then
            'Situa��es que se adicionou linha mas que n�o se acrescentou nada
            'Selecciou op��o delete ou elimina linha
            FGAna.RemoveItem FGAna.row
            'Actualiza ord_marca da estrutura MaReq
            k = 1
            While k <= UBound(MaReq)
                If MaReq(k).Ord_Marca >= FGAna.row Then
                    MaReq(k).Ord_Marca = MaReq(k).Ord_Marca - 1
                End If
                k = k + 1
            Wend
            End If
        End If
        
        FGAna.SetFocus
    
    ElseIf KeyCode = 118 Then ' F7 - INSERIR OBSERVA��O DA AN�LISE
        If EcNumReq = "" Then
            BG_Mensagem mediMsgBox, "Tem que criar a requisi��o primeiro!", vbExclamation
            Exit Sub
        End If
        If FGAna.TextMatrix(LastRowA, lColFgAnaCodigo) <> "" And LastRowA <> 0 Then
            linhaAnaliseOBS = LastRowA
            FrameObsAna.caption = "Observa��o da An�lise: " & FGAna.TextMatrix(LastRowA, lColFgAnaDescricao)
            FrameObsAna.Visible = True
            FrameObsAna.left = 8280
            FrameObsAna.top = 0
            EcObsAna.text = RegistosA(LastRowA).ObsAnaReq
            EcObsAna.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgAna_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "FgAna_KeyDown"
    Exit Sub
    Resume Next
End Sub


Private Sub FGAna_LostFocus()
    On Error GoTo TrataErro

    If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo)) <> "" Then
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbRed
    Else
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbBlack
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGAna_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FGAna_LostFocus"
    Exit Sub
    Resume Next
    
End Sub

Private Sub FGAna_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error GoTo TrataErro
    If Button = 2 And FGAna.Col = 0 And FGAna.row > 0 Then
        MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANA
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGAna_MouseDown: " & Err.Number & " - " & Err.Description, Me.Name, "FGAna_MouseDown"
    Exit Sub
    Resume Next
End Sub

Private Sub Fgana_RowColChange()
    On Error GoTo TrataErro
    
    Dim tmpCol As Integer
    Dim tmpRow As Integer
        
    If ExecutaCodigoA = True Then
        ExecutaCodigoA = False
        
        tmpCol = FGAna.Col
        tmpRow = FGAna.row
        If FGAna.Col <> LastColA Then FGAna.Col = LastColA
        If FGAna.row <> LastRowA Then FGAna.row = LastRowA
        
        If FGAna.row > 0 And FGAna.row <= RegistosA.Count Then
            If RegistosA(LastRowA).Estado <> "-1" And LastColA = 0 And Trim(FGAna.TextMatrix(LastRowA, 0)) <> "" Then
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbRed
            Else
                'FGAna.CellBackColor = vbWhite
                'FGAna.CellForeColor = vbBlack
            End If
        End If
        
        If FGAna.Col <> tmpCol Then FGAna.Col = tmpCol
        If FGAna.row <> tmpRow Then FGAna.row = tmpRow
        
        ' Controlar as colunas
        If FGAna.Cols = 4 Then
            If FGAna.Col = 3 Then
'                FGAna.Col = 0          'sliveira 10-12-2003
'                If FGAna.Row < FGAna.Rows - 1 Then FGAna.Row = FGAna.Row + 1
            ElseIf FGAna.Col = 1 Then
                If LastColA = 2 Then
                    FGAna.Col = 0
                Else
                    FGAna.Col = 2
                End If
            End If
        Else
            If FGAna.Col = 1 Then
             '   FgAna.Col = 0
             '   If FgAna.row < FgAna.rows - 1 Then FgAna.row = FgAna.row + 1
            End If
        End If
        
        LastColA = FGAna.Col
        LastRowA = FGAna.row
        
        If FGAna.row > 0 And FGAna.row <= RegistosA.Count Then
            If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, 0)) <> "" Then
                FGAna.CellBackColor = vbRed
                FGAna.CellForeColor = vbWhite
            Else
                'FGAna.CellBackColor = azul
                'FGAna.CellForeColor = vbWhite
            End If
        End If
        
        ExecutaCodigoA = True
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Fgana_RowColChange: " & Err.Number & " - " & Err.Description, Me.Name, "Fgana_RowColChange"
    Exit Sub
    Resume Next
End Sub


Private Sub FgAnaEFR_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim linha As Long
    If KeyCode = vbKeyDelete Then
        If FgAnaEFR.TextMatrix(FgAnaEFR.row, lColFgAnaEFRlinha) <> "" Then
            linha = FgAnaEFR.TextMatrix(FgAnaEFR.row, lColFgAnaEFRlinha)
            If RegistosRM(linha).ReciboFlgFacturado = 1 Then
                Exit Sub
            End If
            If EliminaReciboManual(linha, RegistosRM(linha).ReciboCodFacturavel, False) = True Then
                LimpaColeccao RegistosRM, linha
                ActualizaLinhasRecibos -1
                If CbEFR.ListIndex > mediComboValorNull Then
                    PreencheFgAnaEFR_todos (CbEFR.ItemData(CbEFR.ListIndex))
                Else
                    LimpaFGAnaEfr
                End If
            End If
       End If
    End If
End Sub


Private Sub FgProd_GotFocus()
    On Error GoTo TrataErro
    Dim i As Integer
    FgProd.CellBackColor = azul
    FgProd.CellForeColor = vbWhite
    
    If FgProd.Col = 0 Then
        BtPesquisaProduto.Enabled = True
        BtPesquisaEspecif.Enabled = False
    ElseIf FgProd.Col = 2 Then
        BtPesquisaProduto.Enabled = False
        BtPesquisaEspecif.Enabled = True
        BtObsEspecif.Enabled = True
    Else
        BtPesquisaProduto.Enabled = False
        BtPesquisaEspecif.Enabled = False
    End If
    
    If gLAB = "CHVNG" Then
        BtPesquisaProduto.Enabled = False
        For i = 1 To FGAna.rows - 1
            If FGAna.TextMatrix(i, lColFgAnaCodigo) = "PCULMCB" Then
                BtPesquisaProduto.Enabled = True
            End If
        Next
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "FgProd_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FgProd_GotFocus"
    Exit Sub
    Resume Next
End Sub


Private Sub FgQuestoes_DblClick()
    If FgQuestoes.Col = 1 Then
        If FgQuestoes.row > 0 And FgQuestoes.row <= totalQuestao Then
            EcAuxQuestao = EstrutQuestao(FgQuestoes.row).Resposta
            EcAuxQuestao.left = FgQuestoes.CellLeft + FgQuestoes.left
            EcAuxQuestao.top = FgQuestoes.CellTop + FgQuestoes.top
            EcAuxQuestao.Width = FgQuestoes.CellWidth + 20
            EcAuxQuestao.Visible = True
            EcAuxQuestao.SetFocus
        End If
    End If
End Sub


Private Sub PreencheAnaRecibos()
    On Error GoTo TrataErro
    Dim i As Long
    Dim j As Integer
    FGRecibos.row = 1
    If FGRecibos.row > 0 And FGRecibos.TextMatrix(FGRecibos.row, 0) <> "" Then
        TotalAnaRecibo = 0
        ReDim EstrutAnaRecibo(0)
        
        For i = 1 To RegistosRM.Count
            If RegistosRM(i).ReciboEntidade = RegistosR(FGRecibos.row).codEntidade Then
                If CLng(RegistosRM(i).ReciboNumDoc) = CLng(BL_HandleNull(RegistosR(FGRecibos.row).NumDoc, "0")) Then
                    TotalAnaRecibo = TotalAnaRecibo + 1
                    ReDim Preserve EstrutAnaRecibo(TotalAnaRecibo)
                    
                    EstrutAnaRecibo(TotalAnaRecibo).codAna = RegistosRM(i).ReciboCodFacturavel
                    EstrutAnaRecibo(TotalAnaRecibo).descrAna = BL_SelCodigo("SLV_ANALISES_FACTUS", "DESCR_ANA", "COD_ANA", RegistosRM(i).ReciboCodFacturavel)
                    EstrutAnaRecibo(TotalAnaRecibo).indice = i
                    EstrutAnaRecibo(TotalAnaRecibo).Flg_adicionada = RegistosRM(i).ReciboFlgAdicionado
                    EstrutAnaRecibo(TotalAnaRecibo).taxa = RegistosRM(i).ReciboValorEFR
                    EstrutAnaRecibo(TotalAnaRecibo).qtd = RegistosRM(i).ReciboQuantidade
                    EstrutAnaRecibo(TotalAnaRecibo).flgRetirada = BL_HandleNull(RegistosRM(i).ReciboFlgRetirado, 0)
                    EstrutAnaRecibo(TotalAnaRecibo).flgFacturada = BL_HandleNull(RegistosRM(i).ReciboFlgFacturado, 0)
                    If EstrutAnaRecibo(i).flgFacturada = mediSim Then
                        EcCodEFR.locked = True
                        BtPesquisaEntFin.Enabled = False
                    End If
                    If RegistosRM(i).ReciboFlgReciboManual = 1 Then
                        EstrutAnaRecibo(TotalAnaRecibo).tipo = "MANUAL"
                    Else
                        EstrutAnaRecibo(TotalAnaRecibo).tipo = "AUTOM"
                    End If
                    
                End If
            End If
        Next
        LimpaFgRecAna
        For i = 1 To TotalAnaRecibo
            
            FgAnaRec.row = i
            For j = 0 To FgAnaRec.Cols - 1
                FgAnaRec.Col = j
                If EstrutAnaRecibo(i).flgRetirada = mediSim Then
                    FgAnaRec.CellBackColor = Amarelo
                ElseIf EstrutAnaRecibo(i).flgFacturada = mediSim Then
                    FgAnaRec.CellBackColor = Verde
                ElseIf EstrutAnaRecibo(i).Flg_adicionada = mediSim Then
                    FgAnaRec.CellBackColor = azul
                Else
                    FgAnaRec.CellBackColor = vbWhite
                End If
            Next
            
            FgAnaRec.TextMatrix(i, lColFgAnaRecCodAna) = EstrutAnaRecibo(i).codAna
            FgAnaRec.TextMatrix(i, lColFgAnaRecDescrAna) = EstrutAnaRecibo(i).descrAna
            FgAnaRec.TextMatrix(i, lColFgAnaRecQtd) = EstrutAnaRecibo(i).qtd
            FgAnaRec.TextMatrix(i, lColFgAnaRecTaxa) = EstrutAnaRecibo(i).taxa
            FgAnaRec.AddItem ""
        Next
         'InicializaFrameAnaRecibos
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheAnaRecibos: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheAnaRecibos"
    Exit Sub
    Resume Next
End Sub

Public Sub FgProd_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    If KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Then ' Enter = Editar
        If LastColP <> 0 And Trim(FgProd.TextMatrix(LastRowP, 0)) = "" Then
            Beep
            BG_Mensagem mediMsgStatus, "Tem que indicar primeiro um produto!"
        Else
            If LastColP = 6 And EcDataPrevista.Enabled = False Then
                Beep
                BG_Mensagem mediMsgStatus, "J� n�o pode alterar a data prevista a este produto!"
            Else
                If LastColP = 0 And gLAB = "CHVNG" Then
                Else
                EcAuxProd.text = FgProd.TextMatrix(FgProd.row, FgProd.Col) & IIf((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0), Chr(KeyCode), "")
                If KeyCode = 8 Then
                    EcAuxProd.text = Mid(EcAuxProd.text, 1, Len(EcAuxProd.text) - 1)
                End If
                If KeyCode = 13 Then
                    EcAuxProd.SelStart = 0
                    EcAuxProd.SelLength = Len(EcAuxProd)
                Else
                    EcAuxProd.SelLength = 0
                End If
                EcAuxProd.left = FgProd.CellLeft + 270
                EcAuxProd.top = FgProd.CellTop + 2060
                EcAuxProd.Width = FgProd.CellWidth + 20
                EcAuxProd.Visible = True
                EcAuxProd.Enabled = True
                EcAuxProd.SetFocus
                End If
            End If
        End If
    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga celula
        Select Case FgProd.Col
            Case 0
                If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Procura_Prod_ana = False Then
                    If FgProd.row < FgProd.rows - 1 Then
                        RegistosP.Remove FgProd.row
                        FgProd.RemoveItem FgProd.row
                    Else
                        FgProd.TextMatrix(FgProd.row, 0) = ""
                        FgProd.TextMatrix(FgProd.row, 1) = ""
                        FgProd.TextMatrix(FgProd.row, 2) = ""
                        FgProd.TextMatrix(FgProd.row, 3) = ""
                        FgProd.TextMatrix(FgProd.row, 4) = ""
                        FgProd.TextMatrix(FgProd.row, 5) = ""
                        FgProd.TextMatrix(FgProd.row, 6) = ""
                        FgProd.TextMatrix(FgProd.row, 7) = ""
                        RegistosP(FgProd.row).CodProd = ""
                        RegistosP(FgProd.row).DescrProd = ""
                        RegistosP(FgProd.row).EspecifObrig = ""
                        RegistosP(FgProd.row).CodEspecif = ""
                        RegistosP(FgProd.row).DescrEspecif = ""
                        RegistosP(FgProd.row).DtPrev = ""
                        RegistosP(FgProd.row).DtChega = ""
                        RegistosP(FgProd.row).EstadoProd = ""
                        RegistosP(FgProd.row).Volume = ""
                    End If
                End If
            Case 2
                FgProd.TextMatrix(FgProd.row, 2) = ""
                FgProd.TextMatrix(FgProd.row, 3) = ""
                RegistosP(FgProd.row).CodEspecif = ""
                RegistosP(FgProd.row).DescrEspecif = ""
            Case 4
                FgProd.TextMatrix(FgProd.row, 4) = ""
                RegistosP(FgProd.row).Volume = ""
            Case 5
                FgProd.TextMatrix(FgProd.row, 5) = ""
                RegistosP(FgProd.row).DtPrev = ""
            Case 6
                FgProd.TextMatrix(FgProd.row, 6) = ""
                RegistosP(FgProd.row).DtChega = ""
        End Select
        FgProd.SetFocus
    ElseIf KeyCode = 46 And Shift = 1 Then ' Shift + Delete = Apaga linha
        If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Procura_Prod_ana = False Then
            If BG_Mensagem(mediMsgBox, "Confirma a elimina��o do " & FgProd.TextMatrix(FgProd.row, 1) & " " & FgProd.TextMatrix(FgProd.row, 3) & " ?", vbYesNo + vbQuestion, "Eliminar produto.") = vbYes Then
                If FgProd.row < FgProd.rows - 1 Then
                    RegistosP.Remove FgProd.row
                    FgProd.RemoveItem FgProd.row
                Else
                    FgProd.TextMatrix(FgProd.row, 0) = ""
                    FgProd.TextMatrix(FgProd.row, 1) = ""
                    FgProd.TextMatrix(FgProd.row, 2) = ""
                    FgProd.TextMatrix(FgProd.row, 3) = ""
                    FgProd.TextMatrix(FgProd.row, 4) = ""
                    FgProd.TextMatrix(FgProd.row, 5) = ""
                    FgProd.TextMatrix(FgProd.row, 6) = ""
                    FgProd.TextMatrix(FgProd.row, 7) = ""
                    RegistosP(FgProd.row).CodProd = ""
                    RegistosP(FgProd.row).DescrProd = ""
                    RegistosP(FgProd.row).EspecifObrig = ""
                    RegistosP(FgProd.row).CodEspecif = ""
                    RegistosP(FgProd.row).DescrEspecif = ""
                    RegistosP(FgProd.row).DtPrev = ""
                    RegistosP(FgProd.row).DtChega = ""
                    RegistosP(FgProd.row).EstadoProd = ""
                    RegistosP(FgProd.row).Volume = ""
                    
                End If
            End If
        End If
        FgProd.SetFocus
    Else
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgProd_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "FgProd_KeyDown"
    Exit Sub
    Resume Next

End Sub



Function SELECT_Descr_Prod() As Boolean
     On Error GoTo TrataErro
   
    'Fun��o que procura a descri��o do produto
    'A fun��o devolve TRUE se encontrou a descri��o caso contr�rio devolve FALSE
    
    Dim rsDescr As ADODB.recordset
    Set rsDescr = New ADODB.recordset
    
    SELECT_Descr_Prod = False
    With rsDescr
        .Source = "SELECT descr_produto,especif_obrig FROM sl_produto WHERE " & _
                  "cod_produto = " & BL_TrataStringParaBD(UCase(EcAuxProd.text))
        .ActiveConnection = gConexao
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With
    
    If rsDescr.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "Produto inexistente!", vbOKOnly + vbInformation, "Produtos"
        EcAuxProd.text = ""
    Else
        FgProd.TextMatrix(LastRowP, 1) = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(LastRowP).DescrProd = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(LastRowP).EspecifObrig = rsDescr!especif_obrig
        SELECT_Descr_Prod = True
    End If
    
    rsDescr.Close
    Set rsDescr = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "SELECT_Descr_Prod: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Descr_Prod"
    SELECT_Descr_Prod = False
    Exit Function
    Resume Next
End Function

Function SELECT_Especif() As Boolean
     On Error GoTo TrataErro
   
    Dim RsDescrEspecif As ADODB.recordset
    Dim encontrou As Boolean
    
    SELECT_Especif = False
    encontrou = False
    If Trim(EcAuxProd.text) <> "" Then
        Set RsDescrEspecif = New ADODB.recordset
        With RsDescrEspecif
            .Source = "SELECT descr_especif FROM sl_especif WHERE " & _
                    "cod_especif=" & UCase(BL_TrataStringParaBD(EcAuxProd.text))
            .ActiveConnection = gConexao
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
        If RsDescrEspecif.RecordCount > 0 Then
            FgProd.TextMatrix(LastRowP, 3) = BL_HandleNull(RsDescrEspecif!descr_especif, "")
            RegistosP(LastRowP).DescrEspecif = BL_HandleNull(RsDescrEspecif!descr_especif, "")
            SELECT_Especif = True
        Else
            BG_Mensagem mediMsgBox, "Especifica��o inexistente!", vbOKOnly + vbInformation, "Especifica��es"
        End If
        RsDescrEspecif.Close
        Set RsDescrEspecif = Nothing
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "SELECT_Especif: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Especif"
    SELECT_Especif = False
    Exit Function
    Resume Next

End Function

Private Sub FgProd_LostFocus()
    On Error GoTo TrataErro

    FgProd.CellBackColor = vbWhite
    FgProd.CellForeColor = vbBlack
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgProd_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FgProd_LostFocus"
    Exit Sub
    Resume Next
    
End Sub

Private Sub FGTubos_LostFocus()

    FGTubos.CellBackColor = vbWhite
    FGTubos.CellForeColor = vbBlack
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGTubos_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FGTubos_LostFocus"
    Exit Sub
    Resume Next
    
End Sub

Private Sub FgProd_RowColChange()
     On Error GoTo TrataErro
   
    Dim tmpCol As Integer
    Dim tmpRow As Integer
    Dim RsEspecif As ADODB.recordset
    
    If ExecutaCodigoP = True Then
        ExecutaCodigoP = False
        
        BtObsEspecif.Enabled = False
        
        If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Trim(FgProd.TextMatrix(FgProd.row, 2)) <> "" Then
            BtObsEspecif.Enabled = True
        End If
        
        If Trim(FgProd.TextMatrix(LastRowP, 0)) <> "" And Trim(FgProd.TextMatrix(LastRowP, 2)) = "" Then
    
            'Escolher a especifica��o de defeito do produto
            
            Set RsEspecif = New ADODB.recordset
                
            With RsEspecif
                .Source = "select sl_produto.cod_especif, sl_especif.descr_especif from sl_produto, sl_especif where sl_produto.cod_especif = sl_especif.cod_especif and cod_produto = " & BL_TrataStringParaBD(Trim(FgProd.TextMatrix(LastRowP, 0)))
                .CursorLocation = adUseServer
                .CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                .Open , gConexao
            End With
            
            If Not RsEspecif.EOF Then
                FgProd.TextMatrix(LastRowP, 2) = Trim(BL_HandleNull(RsEspecif!cod_Especif, ""))
                FgProd.TextMatrix(LastRowP, 3) = Trim(BL_HandleNull(RsEspecif!descr_especif, ""))
            End If
            
            RsEspecif.Close
        End If
            
        Set RsEspecif = Nothing
        
        tmpCol = FgProd.Col
        tmpRow = FgProd.row
        If FgProd.Col <> LastColP Then FgProd.Col = LastColP
        If FgProd.row <> LastRowP Then FgProd.row = LastRowP
        FgProd.CellBackColor = vbWhite
        FgProd.CellForeColor = vbBlack
        If FgProd.Col <> tmpCol Then FgProd.Col = tmpCol
        If FgProd.row <> tmpRow Then FgProd.row = tmpRow
        
        If FgProd.row <> LastRowP Then
            Preenche_LaProdutos FgProd.row
        End If
        
        ' Controlar as colunas
        If FgProd.Col = 1 Then
            If LastColP >= 2 Then
                FgProd.Col = 0
            Else
                FgProd.Col = 2
            End If
        ElseIf FgProd.Col = 3 Then
            If LastColP >= 5 Then
                FgProd.Col = 2
            Else
                FgProd.Col = 5
            End If
        ElseIf LastColP = 5 And LastRowP = FgProd.row And FgProd.Col <> 6 Then
            FgProd.Col = 0
            If FgProd.row <> FgProd.rows - 1 Then FgProd.row = FgProd.row + 1
        ElseIf FgProd.Col > 4 Then
            FgProd.Col = 0
            If FgProd.row <> FgProd.rows - 1 Then FgProd.row = FgProd.row + 1
        End If
        
        LastColP = FgProd.Col
        LastRowP = FgProd.row
        
        FgProd.CellBackColor = azul
        FgProd.CellForeColor = vbWhite
        
        If LastColP = 0 Then
            BtPesquisaProduto.Enabled = True
            BtPesquisaEspecif.Enabled = False
        ElseIf LastColP = 2 Then
            BtPesquisaProduto.Enabled = False
            BtPesquisaEspecif.Enabled = True
            BtObsEspecif.Enabled = True
        Else
            BtPesquisaProduto.Enabled = False
            BtPesquisaEspecif.Enabled = False
        End If
        If gLAB = "CHVNG" Then
            BtPesquisaProduto.Enabled = False
        End If
        ExecutaCodigoP = True
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgProd_RowColChange: " & Err.Number & " - " & Err.Description, Me.Name, "FgProd_RowColChange"
    Exit Sub
    Resume Next
End Sub


Private Sub Form_Activate()

    EventoActivate

End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Me, "")

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_QueryUnload(cancel As Integer, UnloadMode As Integer)
    On Error GoTo TrataErro

    If UnloadMode <> 1 Then
        If Flg_Gravou = False Then
            If BG_Mensagem(mediMsgBox, "Deseja realmente sair sem gravar?", vbQuestion + vbYesNo, "Sislab") = vbNo Then
                'respondeu n�o
                cancel = True
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Form_QueryUnload: " & Err.Number & " - " & Err.Description, Me.Name, "Form_QueryUnload"
    Exit Sub
    Resume Next
End Sub

Private Sub Form_Unload(cancel As Integer)
    
    EventoUnload

End Sub

Sub FuncaoInserir(Optional not_msg As Boolean)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsNumBenef As New ADODB.recordset
    Dim iRes As Integer
    Dim i As Integer
    Dim TotalReg As Long
    Dim rv As Integer
    Dim mostraMgmPagar As Boolean
    mostraMgmPagar = False
    Dim RsVerifNReq As New ADODB.recordset
    
    If BL_VerificaCampoRequis(Me, EcNumReq, EcCodSala) = False Then
        Exit Sub
    End If
    
    
    If EcDataPrevista.text = "" Then
        EcDataPrevista.text = Bg_DaData_ADO
    End If
    If EcDataChegada.text = "" Then
        EcDataChegada.text = Bg_DaData_ADO
    End If
    
    
    If EcNumBenef = "" And VerificaObrigaNrBenef(EcCodEFR) = True Then
        BG_Mensagem mediMsgBox, "N�mero de benefici�rio obrigat�rio! ", vbInformation, "Aten��o"
        EcNumBenef.text = ""
        EcNumBenef.SetFocus
        Exit Sub
    End If
    

    
    If EcCodEFR = "" Then
            BG_Mensagem mediMsgBox, "Tem de indicar uma entidade fin.", vbInformation, "Aten��o"
            Sendkeys ("{HOME}+{END}")
            EcCodEFR = ""
            EcCodEFR.SetFocus
            Exit Sub
    End If
    

    
    
    If not_msg = True Then
        gMsgResp = vbYes
    Else
        gMsgTitulo = "Inserir"
        gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    End If
    
    Me.SetFocus

    If gMsgResp = vbYes Then
        
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        EcHoraCriacao = Bg_DaHora_ADO
        
        'se a data prevista estiver vazia
        If EcDataPrevista.text = "" Then
            EcDataPrevista.text = Format(Bg_DaData_ADO, gFormatoData)
        End If
        
        ValidaDataPrevista
'        If DateValue(EcDataPrevista.Text) < DateValue(Bg_DaData_ADO) Then Exit Sub
        
        ' Obriga a especifica��o de produtos.
        If (gObrigaProduto) Then
            If Not (ValidaProdutos) Then
                MsgBox "N�o est� especificado nenhum produto.    " & vbCrLf & _
                    "A inser��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
                Exit Sub
            End If
        End If
        
        If Len(EcGrupoAna.text) = 1 Then
            If ProdutoUnico(EcGrupoAna.text) And RegistosP.Count > 1 Then
                MsgBox "Esta requisi��o s� pode ter um produto associado. " & vbCrLf & _
                    "A inser��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
            End If
        End If
        
        
        
        If Not ValidaEspecifObrig Then Exit Sub
        
        iRes = ValidaCamposEc
        If iRes = True Then
        
            Call BD_Insert
            If Trim(EcNumReq.text) <> "" Then
                'Se n�o houve erro
                If gNReqPreImpressa <> mediSim Then
                    ImprimeEtiqNovo
                End If
                DoEvents
                BL_Toolbar_BotaoEstado "Inserir", "InActivo"
                BL_Toolbar_BotaoEstado "Procurar", "Activo"
                BL_Toolbar_BotaoEstado "Limpar", "Activo"
                'Mart. ergonomico
                Call FuncaoProcurar
                
            End If
        End If
    End If

    If (gFormGesReqCons_Aberto = True) Then
        ' Quando este form foi invocado atrav�s de FormGesReqCons,
        ' apaga a marca��o correspondente.
        
        rv = MARCACAO_Apaga(FormGesReqCons.EcNumReq.text)
        FormGesReqCons.LimpaCampos
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoInserir: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoInserir"
    Exit Sub
    Resume Next
End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    Dim i As Integer
    Dim k As Integer
    Dim req As Long
    Dim ProdJaChegou As Boolean
    Dim rv As Integer
    
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0
    
    'soliveira arunce
    'Utiliza etiquetas pr�-impressas com indica��o do n�mero de requisi��o final
    If gNReqPreImpressa = 1 Then
        req = EcNumReq.text
    Else
        'Determinar o novo n�mero da requisi��o (TENTA 10 VEZES - LOCK!)
        i = 0
        req = -1
        While req = -1 And i <= 10
            req = BL_GeraNumero("N_REQUIS")
            i = i + 1
        Wend
        
        If req = -1 Then
            BG_Mensagem mediMsgBox, "Erro a gerar n�mero de requisi��o !", vbCritical + vbOKOnly, "ERRO"
            Exit Sub
        End If
        
        EcNumReq.text = ""
        EcNumReq.text = req
    End If
    
    
    
    If EcNumReqAssoc = "" Then
        EcNumReqAssoc = EcNumReq
    End If
    gRequisicaoActiva = CLng(EcNumReq.text)
    EcLocal = gCodLocal
    
    BG_BeginTransaction
    
    If EcNumColheita.text = "" Then
        EcNumColheita.text = COLH_InsereNovaColheita(CLng(EcSeqUtente.text))
    End If
    If EcNumColheita.text = "" Then
        BG_Mensagem mediMsgBox, "Erro ao gravar colheita!", vbCritical + vbOKOnly, "ERRO"
        Exit Sub
    Else
        If COLH_AssociaReqColheita(EcNumReq.text, EcNumColheita.text, CLng(EcSeqUtente.text)) = "" Then
            BG_Mensagem mediMsgBox, "Erro ao associar requisi��o a colheita!", vbCritical + vbOKOnly, "ERRO"
            Exit Sub
        End If
    End If
    
    ' Produtos da requisi��o
    Grava_Prod_Req
    If gSQLError <> 0 Then
        'Erro a gravar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os produtos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' Tubos da requisi��o
    'RGONCALVES 15.12.2014 - adicionado parametro dt_chega
    TB_GravaTubosReq EcNumReq, EcDataChegada.text
    If gSQLError <> 0 Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os tubos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'actualizar as obs das analises
    Grava_ObsAnaReq
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar as observa��es das an�lises !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'ENVIA PARA FACTUS
    If FacturaReqVet = False Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro ao enviar para FACTUS!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    


    'actualizar as obs das analises
    Grava_ObsAnaReq
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar as observa��es das an�lises !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'GRAVA PERFIS MARCADOS
    If PM_GravaPerfisMarcados(EcNumReq.text) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar perfis marcados", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Actualizar estado da requisi��o em conformidade com an�lises marcadas, produtos marcados e sua data de chegada
    If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Or EcEstadoReq = gEstadoReqEsperaProduto Or EcEstadoReq = gEstadoReqEsperaResultados Or EcEstadoReq = gEstadoReqSemAnalises Then
        ProdJaChegou = False
        If FGAna.rows >= 1 And Trim(FGAna.TextMatrix(1, lColFgAnaCodigo)) <> "" Then
            If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
                For k = 1 To RegistosP.Count - 1
                    If Trim(RegistosP(1).DtChega) <> "" Then
                        ProdJaChegou = True
                    End If
                Next k
                If ProdJaChegou = True Then
                    EcEstadoReq = gEstadoReqEsperaResultados
                Else
                    EcEstadoReq = gEstadoReqEsperaProduto
                End If
            Else
                EcEstadoReq = gEstadoReqEsperaResultados
                EcDataChegada.text = Bg_DaData_ADO
            End If
        Else
           EcEstadoReq = " "
        End If
    End If
    EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
    
    
    EcGrupoAna.text = ""
    ' An�lises da requisi��o
    Call Grava_Ana_Req
    If gSQLError <> 0 Then
        'Erro a gravar as an�lises da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar as an�lises da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
                
    ' Diagn�sticos secund�rios e terapeuticas/medica��o
    Grava_DS_TM
    If gSQLError <> 0 Then
        'Erro a actualizar diagn�sticos secund�rios e terapeuticas/medica��o
        BG_Mensagem mediMsgBox, "Erro a actualizar diagn�sticos secund�rios e terap�uticas/medica��o!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' Grava informacoes das analises
    If GravaInfoAna(EcNumReq) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar informa��es das an�lises!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Grava questoes das analises
    If GravaQuestoesAna(EcNumReq) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar Quest�es das an�lises!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    If EcDataChegada.text <> "" Then
        If EcHoraChegada.text = "" Then
            EcHoraChegada.text = Bg_DaHora_ADO
        End If
    Else
        EcHoraChegada.text = ""
    End If

    EcDtEntrega = BL_VerificaConclusaoRequisicao(EcNumReq, EcDataPrevista.text)
    
    ' Gravar os restantes dados da requisi��o
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    ' RECIBOS DA REQUISICAO
    If RECIBO_GravaRecibo(EcNumReq, RegistosA, RegistosR, RegistosRM, False) = False Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os recibos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    ' ADIANTAMENTOS DA REQUISICAO
    If RECIBO_GravaAdiantamento(EcNumReq, RegistosAD) = False Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os adiantamentos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    

    If gSQLError <> 0 Then
        GoTo Trata_Erro
    Else
        BG_CommitTransaction
        gRequisicaoActiva = CLng(EcNumReq.text)
        Flg_Gravou = True
        
    End If
    
    Exit Sub
       
Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro a inserir requisi��o!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormGestaoRequisicaoVet: BD_Insert -> " & Err.Description
    BG_RollbackTransaction
    gRequisicaoActiva = 0
    FuncaoLimpar
    Exit Sub
    Resume Next
End Sub


Sub Grava_Ana_Req()
    On Error GoTo TrataErro
    Dim seqReqTubo  As String
    Dim i As Integer
    Dim k As Integer
    Dim sSql As String
    
    Dim maximo As Long
    Dim sql As String
    Dim RsOrd As New ADODB.recordset
    Dim analises As String
    Dim flg_realiza As Boolean
    
    Set RsOrd = New ADODB.recordset


    If UBound(MaReq) = 0 Then
        BG_Mensagem mediMsgBox, "N�o foram registadas an�lises!", vbInformation, "Grava��o de an�lises"
        SSTGestReq.Tab = 2
    End If
    
    flg_realiza = False

    'Marca��o de an�lises (sl_marcacoes)
    For i = 1 To UBound(MaReq)

        MaReq(i).dt_chega = EcDataChegada.text  'Bruno & sdo

        'S� s�o gravadas as an�lise "novas" ou que estavam na tabela de marca��es, as an�lises da tabela de realiza��es n�o s�o marcadas
            '-1 -> an�lise nova ou an�lise da tabela sl_marca�oes
        sSql = " SELECT ordem, gr_ana FROM sl_ana_s WHERE cod_ana_s =  " & BL_TrataStringParaBD(MaReq(i).cod_agrup) & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_ana_c WHERE cod_ana_c =  " & BL_TrataStringParaBD(MaReq(i).cod_agrup) & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_perfis WHERE cod_perfis =  " & BL_TrataStringParaBD(MaReq(i).cod_agrup)
        
        RsOrd.CursorType = adOpenStatic
        RsOrd.CursorLocation = adUseServer
        RsOrd.LockType = adLockReadOnly
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        RsOrd.Open sSql, gConexao
        
        'Set RsOrd = CmdOrdAna.Execute

        If Not RsOrd.EOF Then
            If BL_HandleNull(RsOrd!ordem, 0) <> 0 Then
                MaReq(i).Ord_Ana = BL_HandleNull(RsOrd!ordem, 0)
            Else
                maximo = 0
                For k = 1 To UBound(MaReq)
                    If MaReq(k).Ord_Ana > maximo Then maximo = MaReq(k).Ord_Ana
                Next k
                maximo = maximo + 1
                'Colocar a an�lise para o fim
                MaReq(i).Ord_Ana = maximo
            End If
            If RsOrd!gr_ana <> "" Then
                If EcGrupoAna.text = "" Then
                    EcGrupoAna.text = RsOrd!gr_ana
                Else
                    If InStr(1, EcGrupoAna.text, RsOrd!gr_ana) <> 0 Then
                    Else
                        EcGrupoAna.text = EcGrupoAna.text & ";" & RsOrd!gr_ana
                    End If
                End If
            End If
        Else
            'Colocar a an�lise para o fim
            maximo = 0
            For k = 1 To UBound(MaReq)
                If MaReq(k).Ord_Ana > maximo Then maximo = MaReq(k).Ord_Ana
            Next k
            maximo = maximo + 1
            'Colocar a an�lise para o fim
            MaReq(i).Ord_Ana = maximo
        End If
        RsOrd.Close

        'MaReq(i).Ord_Ana = i

        If MaReq(i).flg_estado = "-1" Then
            If VerificaAnaliseJaExisteRealiza(CLng(Trim(EcNumReq.text)), UCase((MaReq(i).Cod_Perfil)), UCase((MaReq(i).cod_ana_c)), UCase((MaReq(i).cod_ana_s)), True) > 0 Then
                flg_realiza = True
            Else
                Dim RsTrans As ADODB.recordset
                'verifica se a flag "flg_transmanual" na an�lise est� a 1,
                'se sim coloca "flg_apar_trans" a 2 em sl_marcacoes
                Set RsTrans = New ADODB.recordset
                RsTrans.CursorLocation = adUseServer
                RsTrans.CursorType = adOpenStatic
                sql = "SELECT flg_transmanual FROM sl_ana_s WHERE cod_ana_s=" & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
                If gModoDebug = mediSim Then BG_LogFile_Erros sql
                RsTrans.Open sql, gConexao
    
                If (Not (RsTrans.EOF)) Then
                    If (RsTrans!flg_transManual = 1 And MaReq(i).Flg_Apar_Transf = 0) Then
                        MaReq(i).Flg_Apar_Transf = 2
                    End If
                End If
                
                If MaReq(i).seq_req_tubo > mediComboValorNull Then
                    seqReqTubo = MaReq(i).seq_req_tubo
                Else
                    seqReqTubo = " NULL "
                End If
                sql = "INSERT INTO sl_marcacoes(n_req,cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup,n_folha_trab,dt_chega,flg_apar_trans,ord_marca, flg_facturado, transmit_psm, n_envio, seq_req_tubo"
                sql = sql & ") VALUES (" & CLng(Trim(EcNumReq.text)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s)) & "," & MaReq(i).Ord_Ana
                sql = sql & "," & MaReq(i).Ord_Ana_Compl & "," & MaReq(i).Ord_Ana_Perf & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_agrup)) & "," & MaReq(i).N_Folha_Trab & "," & IIf((MaReq(i).dt_chega = ""), "NULL", BL_TrataDataParaBD(MaReq(i).dt_chega)) & ","
                sql = sql & UCase(BL_TrataStringParaBD(MaReq(i).Flg_Apar_Transf)) & "," & MaReq(i).Ord_Marca & ", " & MaReq(i).Flg_Facturado & ", " & MaReq(i).transmit_psm & ", " & MaReq(i).n_envio & ", " & seqReqTubo & ")"
                BG_ExecutaQuery_ADO sql
                
    
                If gSQLError <> 0 Then
                    Exit For
                End If
    
                RsTrans.Close
                Set RsTrans = Nothing
            End If

        'soliveira 08-09-2003
        'para alterar a flag flg_facturado quando as analises j� se encontram na tabela sl_realiza
        'ElseIf MaReq(i).Flg_Facturado = 1 Then 'alterei por causa do tratamento de rejeitados
        Else
            If EcEstadoReq = gEstadoReqHistorico Then
                sql = "UPDATE sl_realiza_h set flg_facturado = " & MaReq(i).Flg_Facturado & _
                        " WHERE n_req = " & BL_TrataStringParaBD(CStr(Trim(EcNumReq.text))) & _
                        " AND cod_perfil = " & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & _
                        " AND cod_ana_c = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & _
                        " AND cod_ana_s = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
            Else
                sql = "UPDATE sl_realiza set flg_facturado = " & MaReq(i).Flg_Facturado & _
                        " WHERE n_req = " & BL_TrataStringParaBD(CStr(Trim(EcNumReq.text))) & _
                        " AND cod_perfil = " & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & _
                        " AND cod_ana_c = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & _
                        " AND cod_ana_s = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
            End If
            BG_ExecutaQuery_ADO sql
        End If

        '----------------
        If InStr(1, analises, MaReq(i).cod_agrup) = 0 Then
            analises = analises & IIf(BL_HandleNull(MaReq(i).cod_agrup, "") <> "", BL_TrataStringParaBD(MaReq(i).cod_agrup) & ",", "")
        End If
        
    Next i

    Set RsOrd = Nothing

    Call RegistaEliminadas
    Call RegistaTubosEliminados
    Call RegistaAcrescentadas
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_Ana_Req: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_Ana_Req"
    Exit Sub
    Resume Next
End Sub

Sub Grava_Prod_Req()
    On Error GoTo TrataErro
    
    Dim sql As String
    Dim i As Integer
    Dim MenorData As String
    Dim DataPrevistaAux As String
    Dim TotalRegistos As Long
    Dim Perguntou As Boolean
     
    Perguntou = False
    If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
        'determinar a menor data da lista de produtos
        MenorData = RegistosP(1).DtPrev
        For i = 1 To RegistosP.Count - 1
            If (RegistosP(i).DtChega) <> "" Then
                If DateValue(RegistosP(i).DtPrev) < DateValue(MenorData) And DateValue(RegistosP(i).DtChega) > DateValue(EcDataPrevista) Then MenorData = RegistosP(i).DtPrev
            End If
        Next i
        If DateValue(MenorData) > DateValue(EcDataPrevista.text) Then
            gMsgTitulo = "ATEN��O"
            gMsgMsg = "Existe um produto com data inferior � data prevista, deseja alterar a data prevista para " & MenorData & "?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                DataPrevistaAux = EcDataPrevista.text
                EcDataPrevista = DateValue(MenorData)
                sql = "UPDATE sl_requis SET dt_previ=" & BL_TrataDataParaBD(EcDataPrevista.text) & " WHERE dt_previ=" & BL_TrataDataParaBD(DataPrevistaAux) & " AND n_req=" & EcNumReq.text
                BG_ExecutaQuery_ADO sql, gConexao
                If gSQLError <> 0 Then Exit Sub
            End If
        End If
        
        TotalRegistos = RegistosP.Count - 1
        For i = 1 To TotalRegistos
            If Trim(RegistosP(i).CodProd) <> "" Then
                If RegistosP(i).DtChega = "" And EcDataChegada.text <> "" Then
                    'se a data de chegada estiver vazia vai preenche-la
                    If Flg_PreencherDtChega = True Then
                        RegistosP(i).DtChega = DateValue(EcDataChegada.text)
                        FgProd.TextMatrix(i, 5) = RegistosP(i).DtChega
                    ElseIf Perguntou = False Then
'                        If gLAB <> "BIO" Then
'                        If BG_Mensagem(mediMsgBox, "Deseja preencher a data de chegada dos produtos com a data '" & EcDataChegada.Text & "' ?", vbQuestion + vbYesNo, "Data Chegada") = vbYes Then
'                            RegistosP(i).DtChega = DateValue(EcDataChegada.Text)
'                            FgProd.TextMatrix(i, 5) = RegistosP(i).DtChega
'                            Flg_PreencherDtChega = True
'                        End If
                        Perguntou = True
'                        End If
                    End If
                End If
                sql = "INSERT INTO sl_req_prod(n_req,cod_prod,cod_especif,dt_previ,dt_chega, volume, obs_especif) VALUES(" & EcNumReq.text & _
                    "," & UCase(BL_TrataStringParaBD(RegistosP(i).CodProd)) & "," & UCase(BL_TrataStringParaBD(RegistosP(i).CodEspecif)) & "," & BL_TrataDataParaBD(RegistosP(i).DtPrev) & "," & BL_TrataDataParaBD(RegistosP(i).DtChega) & ", " & BL_TrataStringParaBD(RegistosP(i).Volume) & ", " & BL_TrataStringParaBD(RegistosP(i).ObsEspecif) & ")"
                BG_ExecutaQuery_ADO sql
                
                If gSQLError <> 0 Then
                    i = TotalRegistos + 1
                End If
            End If
        Next i
    
        If gSQLError <> 0 Then Exit Sub
        
        'Actualizar o estado da requisi��o se foi introduzida data de chegada (dt_chega)
        If Flg_PreencherDtChega Then
            sql = "UPDATE sl_requis SET estado_req='A' WHERE n_req=" & EcNumReq.text
            BG_ExecutaQuery_ADO sql
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_Prod_Req: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_Prod_Req"
    Exit Sub
    Resume Next
End Sub

Sub Grava_DS_TM()
    On Error GoTo TrataErro
    
    If gLAB = "CITO" Then
        Exit Sub
    Else
        Dim sql As String
    
        'Faz o UPDATE na tabela diagn�sticos secund�rios criados temporariamente com o NumeroSessao
        sql = "UPDATE sl_diag_sec SET n_req=" & CLng(EcNumReq.text) & " WHERE n_req= -" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        If gSQLError = 0 Then
            'Faz o UPDATE na tabela terap�uticas e medica��o criados temporariamente com o NumeroSessao
            sql = "UPDATE sl_tm_ute SET n_req=" & CLng(EcNumReq.text) & " WHERE n_req= -" & gNumeroSessao
            BG_ExecutaQuery_ADO sql
        End If
        
        If gSQLError = 0 Then
            'Faz o UPDATE na tabela de motivos da requisi��o criados temporariamente com o NumeroSessao
            sql = "UPDATE sl_motivo_req SET n_req= " & CLng(EcNumReq.text) & " WHERE n_req= -" & gNumeroSessao
            BG_ExecutaQuery_ADO sql
        End If
        
        If gSQLError = 0 Then
            'Faz o UPDATE na tabela de m�dicos da requisi��o criados temporariamente com o NumeroSessao
            sql = "UPDATE sl_medicos_req SET n_req= " & CLng(EcNumReq.text) & " WHERE n_req= -" & gNumeroSessao
            BG_ExecutaQuery_ADO sql
        End If
        
        Select Case gLAB
            Case "HSMARIA", "BIO", "GM", "CHVNG"
                If gSQLError = 0 Then
                    'Faz o UPDATE � informa��o cl�nica da requisi��o criada temporariamete com o NumeroSessao
                    sql = "UPDATE sl_inf_clinica SET n_req = " & CLng(EcNumReq.text) & " WHERE n_req = -" & gNumeroSessao
                    BG_ExecutaQuery_ADO sql
                End If
        End Select
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_DS_TM: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_DS_TM"
    Exit Sub
    Resume Next
End Sub

Sub Apaga_DS_TM()
    On Error GoTo TrataErro
    
    If gLAB = "CITO" Or gLAB = "GM" Then
        Exit Sub
    Else
        Dim sql As String
    
        'Delete dos diagnosticos secund�rios criados temporariamente com o NumeroSessao
        sql = "DELETE FROM sl_diag_sec " & _
              "WHERE n_req=-" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        'BG_LogFile_Erros "FormGestaoRequisicaoPrivado: apagar diag. secund�rios -> (" & Sql & " ) ErrBD " & gSQLError
        
        'Delete das terap�uticas e medica��es criados temporariamente com o NumeroSessao
        sql = "DELETE FROM sl_tm_ute " & _
              "WHERE n_req=-" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        'Delete dos motivos da requisi��o criados temporariamente com o NumeroSessao
        sql = "DELETE FROM sl_motivo_req " & _
              "WHERE n_req=-" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        'Delete dos medicos da requisi��o criados temporariamente com o NumeroSessao
        sql = "DELETE FROM sl_medicos_req " & _
              "WHERE n_req=-" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        
        Select Case gLAB
            Case "HSMARIA", "BIO", "GM", "CHVNG"
            'Delete da informa��o clinica da requisi��o criados temporariamente com o NumeroSessao
            sql = "DELETE FROM sl_inf_clinica " & _
                  "WHERE n_req=-" & gNumeroSessao
            BG_ExecutaQuery_ADO sql
        End Select
        
        ' BG_LogFile_Erros "FormGestaoRequisicaoPrivado: apagar terap./medica��o -> (" & Sql & " ) ErrBD " & gSQLError
    End If
        
Exit Sub
TrataErro:
    BG_LogFile_Erros "Apaga_DS_TM: " & Err.Number & " - " & Err.Description, Me.Name, "Apaga_DS_TM"
    Exit Sub
    Resume Next
End Sub

Public Sub FuncaoModificar(Optional not_msg As Boolean)
     On Error GoTo TrataErro
   
    Dim iRes As Integer
    Dim i As Integer
    Dim TotalReg As Long
    
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    ' Obriga a especifica��o de produtos.
    If (gObrigaProduto) Then
        If Not (ValidaProdutos) Then
            MsgBox "N�o est� especificado nenhum produto.    " & vbCrLf & _
                "A modifica��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
            Exit Sub
        End If
    End If
    If Len(EcGrupoAna.text) = 1 Then
        If ProdutoUnico(EcGrupoAna.text) And RegistosP.Count - 1 > 1 Then
            MsgBox "Esta requisi��o s� pode ter um produto associado. " & vbCrLf & _
                "A inser��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
            Exit Sub
        End If
    End If
    
    If EcCodEFR = "" Then
        BG_Mensagem mediMsgBox, "Tem de indicar uma entidade fin.", vbInformation, "Aten��o"
        Sendkeys ("{HOME}+{END}")
        EcCodEFR = ""
        EcCodEFR.SetFocus
        Exit Sub
    End If
    
    ' --------------------------------------------------------------------------------------------------------------
    ' SE ENTROU NO ECRA PARA FIM DE SEMANA - REQUISICOES TEM OBRIGATORIAMENTE DE TER DATA DE FIM DE SEMANA
    ' --------------------------------------------------------------------------------------------------------------
    If Req_FDS = True Then
        If BG_Mensagem(mediMsgBox, "A data de chegada tem de ser um Domingo. A data indicada � um feriado ou s�bado? ", vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo) = vbYes Then
            'nada
        Else
            Sendkeys ("{HOME}+{END}")
            EcDataChegada.text = ""
            EcDataChegada.SetFocus
            Exit Sub
        End If

    End If
    
    If EcDataPrevista.text = "" Then
        EcDataPrevista.text = Bg_DaData_ADO
    End If
    If EcDataChegada.text = "" Then
        EcDataChegada.text = Bg_DaData_ADO
    End If
    
    
    If not_msg = True Then
        gMsgResp = vbYes
    Else
        gMsgTitulo = "Modificar"
        gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    End If
    
    If gMsgResp = vbYes Then
    
        'Impedir que se alterem valores de cria��o / impress�o de registos
        EcDataCriacao = BL_HandleNull(rs!dt_cri, "")
        EcUtilizadorCriacao = BL_HandleNull(rs!user_cri, "")
        EcHoraCriacao = BL_HandleNull(rs!hr_cri, "")
        
        EcDataImpressao = BL_HandleNull(rs!dt_imp, "")
        EcUtilizadorImpressao = BL_HandleNull(rs!user_imp, "")
        EcHoraImpressao = BL_HandleNull(rs!hr_imp, "")
        
        EcDataImpressao2 = BL_HandleNull(rs!dt_imp2, "")
        EcUtilizadorImpressao2 = BL_HandleNull(rs!user_imp2, "")
        EcHoraImpressao2 = BL_HandleNull(rs!hr_imp2, "")
        
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        EcHoraAlteracao = Bg_DaHora_ADO
        
        'se a data prevista estiver vazia
        If EcDataPrevista.text = "" Then
            EcDataPrevista.text = Format(Bg_DaData_ADO, gFormatoData)
        End If
        
        
        
        If UBound(MaReq) > 0 Then
            If Trim(MaReq(1).cod_ana_s) <> "" Then
                If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
                    EcEstadoReq = EcEstadoReq
                ElseIf EcEstadoReq = gEstadoReqEsperaProduto Then
                    EcEstadoReq = gEstadoReqEsperaResultados
                End If
            Else
                EcEstadoReq = gEstadoReqSemAnalises
            End If
        Else
            EcEstadoReq = gEstadoReqSemAnalises
        End If
        EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        
        
        If Not ValidaEspecifObrig Then Exit Sub
                
        EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        
        
        iRes = ValidaCamposEc
        If iRes = True Then
        
            Call BD_Update
            If Trim(EcNumReq.text) <> "" Then
                'Se n�o houve erro
                If gRequisicaoActiva = 0 Then
                Else
                    gRequisicaoActiva = EcNumReq.text
                End If
                
                RegistaAcrescentadas
                RegistaAlteracoesRequis EcNumReq
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoModificar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoModificar"
    Exit Sub
    Resume Next
End Sub

Function ValidaNumBenef() As Boolean
    On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim Formato As Boolean
    
    If EcCodEFR.text <> "" Then
        Set Tabela = New ADODB.recordset
        Tabela.CursorLocation = adUseServer
        Tabela.CursorType = adOpenStatic
        sql = "SELECT sigla,formato1,formato2 FROM sl_efr WHERE cod_efr=" & EcCodEFR.text
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            Formato1 = BL_HandleNull(Tabela!Formato1, "")
            Formato2 = BL_HandleNull(Tabela!Formato2, "")
            'SendKeys ("{END}")
        End If
        Formato = Verifica_Formato(UCase(EcNumBenef.text), Formato1, Formato2)
        If Formato = False And EcNumBenef.text <> "" Then
            'SendKeys ("{END}")
            EcNumBenef.SetFocus
            ValidaNumBenef = False
        Else
            ValidaNumBenef = True
        End If
    Else
        ValidaNumBenef = True
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaNumBenef: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaNumBenef"
    ValidaNumBenef = False
    Exit Function
    Resume Next
End Function

Function Verifica_Formato(NumBenef As String, Formato1 As String, Formato2 As String) As Boolean
    On Error GoTo TrataErro
    
    If Trim(Formato1) = "" And Trim(Formato2) = "" Then
        Verifica_Formato = True
        Exit Function
    End If
    
    If Trim(Formato1) = "" Then
        Verifica_Formato = False
    End If
    If Len(NumBenef) <> Len(Formato1) Then
        Verifica_Formato = False
    Else
        Verifica_Formato = Percorre_Formato(NumBenef, Formato1)
    End If
    
    'caso o formato 1 n�o esteja correcto vai verificar o formato 2
    If Verifica_Formato = False Then
        If Trim(Formato2) = "" Then
            Verifica_Formato = False
        End If
        If Len(NumBenef) <> Len(Formato2) Then
            Verifica_Formato = False
        Else
            Verifica_Formato = Percorre_Formato(NumBenef, Formato2)
        End If
        If Verifica_Formato = False And EcNumBenef.text <> "" Then
            gMsgTitulo = "Aten��o"
            gMsgMsg = "N�mero Benefici�rio incorrecto." & Chr(13) & "Deseja continuar ?" & Chr(13) & Chr(13) & "Exemplo de formato(s) correcto(s): " & Chr(13) & "       " & Formato1 & IIf(Trim(Formato1) <> "" And Trim(Formato2) <> "", ", ", "") & Formato2
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbInformation, gMsgTitulo)
            If gMsgResp = vbYes Then
                Verifica_Formato = True
            Else
                Verifica_Formato = False
            End If
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Formato: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Formato"
    Verifica_Formato = False
    Exit Function
    Resume Next
End Function

Function Percorre_Formato(NumBenef As String, Formato As String) As Boolean
     On Error GoTo TrataErro
   
    Dim pos As Integer
    Dim comp As Integer
    Dim CharActualF As String
    Dim CharActualN As String
    
    Percorre_Formato = True
    comp = Len(Formato)
    pos = 1
    Do While pos <= comp
        CharActualF = Mid(Formato, pos, 1)
        CharActualN = Mid(NumBenef, pos, 1)
        Select Case CharActualF
            Case "A"
                If CharActualN < "A" Or CharActualN > "Z" Then
                    Percorre_Formato = False
                End If
            Case "9"
                If CharActualN < "0" Or CharActualN > "9" Then
                    Percorre_Formato = False
                End If
            Case " "
                If CharActualN <> " " Then
                    Percorre_Formato = False
                End If
        End Select
        pos = pos + 1
    Loop

Exit Function
TrataErro:
    BG_LogFile_Erros "Percorre_Formato: " & Err.Number & " - " & Err.Description, Me.Name, "Percorre_Formato"
    Percorre_Formato = False
    Exit Function
    Resume Next
End Function

Sub BD_Update()
   
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim k As Integer
    Dim ProdJaChegou As Boolean
    
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0
    
    BG_BeginTransaction
        
        
    'actualizar os produtos da requisi��o
    UPDATE_Prod_Req
    If gSQLError <> 0 Then
        'Erro a actualizar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar os produtos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'actualizar os tubos da requisi��o
    UPDATE_Tubos_Req
    If gSQLError <> 0 Then
        'Erro a actualizar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar os tubos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
            
    
    'actualizar as obs das analises
    Grava_ObsAnaReq
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar as observa��es das an�lises !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'ENVIA PARA FACTUS
    If FacturaReqVet = False Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro ao enviar para FACTUS!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'actualizar os recibos da requisi��o
    If UPDATE_Recibos_Req = False Then
        'Erro a actualizar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar os recibos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' ADIANTAMENTOS DA REQUISICAO
    If RECIBO_GravaAdiantamento(EcNumReq, RegistosAD) = False Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os adiantamentos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
             
    'actualizar as an�lises da requisi��o
    UPDATE_Ana_Req
    If gSQLError <> 0 Then
        'Erro a actualizar as an�lises da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar as an�lises da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
                                        
    'actualizar as terap�uticas e medica��es da requisi��o
    Grava_DS_TM
    If gSQLError <> 0 Then
        'Erro a actualizar as terap�uticas e medica��es da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar as terap�uticas e medica��es da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' Grava informacoes das analises
    If GravaInfoAna(EcNumReq) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar informa��es das an�lises!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Grava questoes das analises
    If GravaQuestoesAna(EcNumReq) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar Quest�es das an�lises!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
        
    'GRAVA PERFIS MARCADOS
    If PM_GravaPerfisMarcados(EcNumReq.text) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar perfis marcados", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    If EcDataChegada.text <> "" Then
        If EcHoraChegada.text = "" Then
            EcHoraChegada.text = Bg_DaHora_ADO
        End If
    Else
        EcHoraChegada.text = ""
    End If
   
    EcDtEntrega = BL_VerificaConclusaoRequisicao(EcNumReq, EcDataPrevista.text)
    
    'RGONCALVES 29.05.2013 Cedivet-27
    'actualiza identifica��o se for alterado o c�digo da entidade
    If codEfrOriginal <> EcCodEFR.text Then
        If ActualizaEntidadeIdentif(EcSeqUtente.text, EcCodEFR.text) = False Then
            BG_Mensagem mediMsgBox, "Erro ao actualizar Entidade na identifica��o do utente", vbError, "ERRO"
            GoTo Trata_Erro
        End If
        codEfrOriginal = EcCodEFR.text
    End If
    '
    
    'gravar restantes dados da requisicao
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    ' PFerreira 16.03.2007 -- Insere utente na fila de espera
    'If (IsToInsert) Then: InsertFilaEspera
    
   
    If gSQLError <> 0 Then
        GoTo Trata_Erro
    Else
        Flg_Gravou = True
        gRequisicaoActiva = CLng(EcNumReq)
        BG_CommitTransaction
        
        EcEstadoReq.text = BL_MudaEstadoReq(CLng(EcNumReq))
        
        If (EcEstadoReq.text = gEstadoReqSemAnalises) Then
            Me.EcDataPrevista.Enabled = True
            Me.EcDataChegada.Enabled = True
        Else
            Me.EcDataPrevista.Enabled = False
            Me.EcDataChegada.Enabled = False
        End If
        
        EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)

    End If
    
    MarcaLocal = rs.Bookmark
    rs.Requery
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    rs.Bookmark = MarcaLocal
    
    'CbSituacao.SetFocus
    EcNumReq.SetFocus
    Exit Sub
    
Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro a modificar requisi��o!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormGestaoRequisicaoVet: BD_Update -> " & Err.Description
    BG_RollbackTransaction
    gRequisicaoActiva = 0
    FuncaoLimpar
    Resume Next

End Sub



Sub UPDATE_Prod_Req()
    On Error GoTo TrataErro
   Dim sql As String
    
   gSQLError = 0
   sql = "DELETE FROM sl_req_prod WHERE n_req=" & Trim(EcNumReq.text)
   BG_ExecutaQuery_ADO sql
   
   If gSQLError <> 0 Then
        Exit Sub
   End If
   Grava_Prod_Req
Exit Sub
TrataErro:
    BG_LogFile_Erros "UPDATE_Prod_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Prod_Req"
    Exit Sub
    Resume Next
End Sub
Sub UPDATE_Tubos_Req()
    On Error GoTo TrataErro
   
    TB_GravaTubosReq EcNumReq, EcDataChegada.text
Exit Sub
TrataErro:
    BG_LogFile_Erros "UPDATE_Tubos_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Tubos_Req"
    Exit Sub
    Resume Next
End Sub

Function UPDATE_Recibos_Req() As Boolean
     On Error GoTo TrataErro
     UPDATE_Recibos_Req = RECIBO_GravaRecibo(EcNumReq, RegistosA, RegistosR, RegistosRM, False)
Exit Function
TrataErro:
    UPDATE_Recibos_Req = False
    BG_LogFile_Erros "UPDATE_Recibos_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Recibos_Req"
    Exit Function
    Resume Next
End Function

Sub UPDATE_Ana_Req()
    On Error GoTo TrataErro
    
    Dim sql As String
    ActualizaFlgAparTrans
    gSQLError = 0
    'Apagar todas as requisi��es que n�o t�m dt_chega
    sql = "DELETE FROM sl_marcacoes WHERE n_req=" & Trim(EcNumReq.text)
    BG_ExecutaQuery_ADO sql
    
    If gSQLError <> 0 Then
        Exit Sub
    End If

    EcGrupoAna.text = ""
    
    Call Grava_Ana_Req
Exit Sub
TrataErro:
    BG_LogFile_Erros "UPDATE_Ana_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Ana_Req"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoRemover()
    On Error GoTo TrataErro
    Dim i As Integer
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    For i = 1 To RegistosR.Count
        If RegistosR(i).NumDoc <> "0" And RegistosR(i).NumDoc <> "" Then
            If RegistosR(i).Estado = gEstadoReciboPago Then
                Call BG_Mensagem(mediMsgBox, "N�o pode cancelar requisi��es com recibos emitidos!", vbExclamation + vbOKOnly, App.ProductName)
                Exit Sub
            End If
        End If
    Next
    If Trim(EcEstadoReq) <> Trim(gEstadoReqSemAnalises) And EcEstadoReq <> gEstadoReqEsperaProduto And EcEstadoReq <> gEstadoReqEsperaResultados Then
        Call BG_Mensagem(mediMsgBox, "N�o pode cancelar requisi��es com resultados!", vbExclamation + vbOKOnly, App.ProductName)
        Exit Sub
    End If
    
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Max = UBound(gFieldObjectProperties)

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind

    MarcaLocal = rs.Bookmark
    
    FormGestaoRequisicaoVet.Enabled = False
    
    FormCancelarRequisicoes.Show
    
    SSTGestReq.TabEnabled(1) = False
    SSTGestReq.TabEnabled(2) = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabEnabled(5) = False
    SSTGestReq.TabEnabled(6) = False
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoRemover: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoRemover"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoProcurar()
          
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    On Error GoTo Trata_Erro
    
    
    If Req_FDS = True Then
        EcFimSemana = "1"
    Else
        EcFimSemana = "0"
    End If
    
    Set rs = New ADODB.recordset
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    CriterioTabela = Replace(CriterioTabela, "seq_utente = '(", "seq_utente IN (")
    CriterioTabela = Replace(CriterioTabela, "'(", "(")
    CriterioTabela = Replace(CriterioTabela, ")'", ")")
    If SelTotal = True Then
        If BG_Mensagem(mediMsgBox, "N�o colocou crit�rios de pesquisa, esta opera��o pode demorar algum tempo." & vbNewLine & "Confirma a selec��o de todos as requisi��es ?", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
            Set rs = Nothing
            Exit Sub
        End If
    Else
    End If
    
    If Req_FDS = True Then
        EcFimSemana = "1"
    Else
        EcFimSemana = "0"
    End If
                   

    CriterioTabela = CriterioTabela & "  ORDER BY dt_chega DESC, n_req DESC"
              
    BL_InicioProcessamento Me, "A pesquisar registos."
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioTabela
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbInformation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        BL_ToolbarEstadoN Estado
        
        'inicializa��es da lista de marca��es de an�lises
        Inicializa_Estrutura_Analises
        
        'limpar grids
        LimpaFGAna
        LimpaFGAnaEfr
        LimpaFgProd
        TB_LimpaFgTubos FGTubos
        LimpaFgRecibos
        
        AlteraFlgCodBarras False
        Call PreencheCampos
        
        
        Set CampoDeFocus = EcDataPrevista
        
        ' SE NAO TIVER IMPRESSO VERIFICA SE JA FOI DISPONIBILIZADO PARA O ER
        ' FGONCALVES HMACEDO CAVALEIROS
        
        If BL_HandleNull(rs!dt_imp, "") = "" Then
            Dim rsEresults As New ADODB.recordset
            Dim sSqlEresults As String
            
            If gSGBD = gOracle Then
                sSqlEresults = " SELECT trunc(dt_cri) dt_imp, to_char(dt_cri, 'HH24:MI:SS') hr_imp FROM sl_eresults WHERE "
                sSqlEresults = sSqlEresults & " flg_estado = 1 AND cod_original = " & BL_TrataStringParaBD(rs!n_req)
            ElseIf gSGBD = gSqlServer Then
                sSqlEresults = " SELECT convert(VARCHAR(10),dt_cri ,105) dt_imp, convert(VARCHAR(10),dt_cri ,108) hr_imp FROM sl_eresults WHERE "
                sSqlEresults = sSqlEresults & " flg_estado = 1 AND cod_original = " & BL_TrataStringParaBD(rs!n_req)
            End If
            rsEresults.CursorType = adOpenStatic
            rsEresults.CursorLocation = adUseServer
            rsEresults.LockType = adLockReadOnly
            If gModoDebug = mediSim Then BG_LogFile_Erros sSqlEresults
            rsEresults.Open sSqlEresults, gConexao
            If rsEresults.RecordCount > 0 Then
                EcDataImpressao = BL_HandleNull(rsEresults!dt_imp, "")
                EcHoraImpressao = BL_HandleNull(rsEresults!hr_imp, "")
                If EcDataImpressao <> "" Then
                    LbNomeUtilImpressao = "eResults"
                Else
                    LbNomeUtilImpressao = ""
                End If
            End If
            rsEresults.Close
            Set rsEresults = Nothing
        End If
        
        BL_FimProcessamento Me
        gRequisicaoActiva = CLng(EcNumReq.text)

        If rs!estado_req = gEstadoReqSemAnalises Or EcDataChegada.text = "" Then
            Me.EcDataPrevista.Enabled = True
            Me.EcDataChegada.Enabled = True
        Else
            Me.EcDataChegada.Enabled = False
            Me.EcDataPrevista.Enabled = False
        End If
        
        'RGONCALVES 29.05.2013 Cedivet-27
        codEfrOriginal = EcCodEFR.text
        '
    End If
    
    Exit Sub
    
Trata_Erro:
    
    BG_LogFile_Erros Me.Name & ": FuncaoProcurar (" & Err.Number & "-" & Err.Description & ")"
    Err.Clear
    BL_FimProcessamento Me
    BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbInformation, "Procurar"
    FuncaoLimpar
    Resume Next

End Sub

Sub Funcao_CopiaReq()
    On Error GoTo TrataErro
    
    If gRequisicaoActiva <> 0 Then
        EcNumReq = gRequisicaoActiva
        FuncaoProcurar
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe requisi��o activa!", , "Copiar requisi��o"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_CopiaReq: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_CopiaReq"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoLimpar()
     On Error GoTo TrataErro
   
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        gMsgTitulo = "Limpar"
        gMsgMsg = "Tem a certeza que quer LIMPAR o ecr�?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            LimpaCampos
            EcUtente.text = ""
            DoEvents
            
            CampoDeFocus.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoLimpar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoLimpar"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoEstadoAnterior()
    On Error GoTo TrataErro
    
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoEstadoAnterior: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoEstadoAnterior"
    Exit Sub
    Resume Next
End Sub

Function ValidaCamposEc() As Integer
    On Error GoTo TrataErro
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaCamposEc: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaCamposEc"
    ValidaCamposEc = -1
    Exit Function
    Resume Next
End Function

Sub LimpaCampos()
    On Error GoTo TrataErro
    
    'Me.SetFocus

    
    BG_LimpaCampo_Todos CamposEc
    
    'colocar o bot�o de cancelar requisi��es invisivel
    EcNumColheita.text = ""
    BtCancelarReq.Visible = False
    flg_novaArs = -1
    LbNomeUtilAlteracao.caption = ""
    LbNomeUtilAlteracao.ToolTipText = ""
    LbNomeUtilCriacao.caption = ""
    LbNomeUtilCriacao.ToolTipText = ""
    LbNomeUtilImpressao.caption = ""
    LbNomeUtilImpressao.ToolTipText = ""
    LbNomeUtilImpressao2.caption = ""
    LbNomeUtilImpressao2.ToolTipText = ""
    LbNomeUtilFecho.caption = ""
    LbNomeUtilFecho.ToolTipText = ""
    LbNomeLocal.caption = ""
    LbNomeLocal.ToolTipText = ""
    EcUtilNomeConf = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    EcDataNasc.text = ""
    EcSexo = ""
    EcIdade.text = ""
    EcNome.text = ""
    EcNomeDono.text = ""
    EcUtente.text = ""
    EcDescrEFR.text = ""
    EcDescrMedico = ""
    EcNomeMedico = ""
    EcPesqRapProven.text = ""
    EcPesqRapProduto.text = ""
    EcPesqRapTubo.text = ""
    EcDataNasc.text = ""
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    FrBtTubos.Enabled = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabEnabled(5) = False
    SSTGestReq.TabEnabled(6) = False
    EcCodEFR.locked = False
    EcNumBenef.locked = False
    
    EcCodEFR.ToolTipText = ""
    EcNumBenef.ToolTipText = ""

    EcCodEFR.ForeColor = vbBlack
    EcNumBenef.ForeColor = vbBlack
    CbTipoIsencao.Enabled = True
    CbPrioColheita.ListIndex = mediComboValorNull
    CbSala.ListIndex = mediComboValorNull
    CbHemodialise.ListIndex = mediComboValorNull
    CbHemodialise.Enabled = False
    CbDestino.ListIndex = mediComboValorNull
    CbDestino.ListIndex = mediComboValorNull
    EcCodEfr2 = ""
    EcDescrEfr2 = ""
    EcMorada.text = ""
    EcCodPostal.text = ""
    EcRuaPostal.text = ""
    EcDescrPostal.text = ""
    EcObsAna.text = ""
    FrameObsAna.Visible = False
    
    EcDescrProveniencia = ""
    EcDescrSala = ""
    EcDescrPais = ""
    'Fun��o que limpa os produtos da requisi��o
    LaProdutos.caption = ""
    EcDescrEstado = ""
    'Fun��o que limpa os tubos da requisi��o
    LaTubos.caption = ""
    
    CodEfrAUX = ""
    
    'inicializa��es da lista de marca��es de an�lises
    Inicializa_Estrutura_Analises
     
         
    'colocar cabe�alho e data prevista enabled a false
'    EcNumReq.Enabled = True
    EcNumReq.locked = False
    EcNumReqAssoc.locked = False
    
    CbTipoUtente.Enabled = True
    EcUtente.Enabled = True
    EcDataPrevista.Enabled = True
    EcDataChegada.Enabled = True
    
    BtInfClinica.Enabled = False
    BtDomicilio.Enabled = False
    BtDigitalizacao.Enabled = False
    BtConsReq.Enabled = False
    Set CampoDeFocus = EcNumReq
    
    gFlg_JaExisteReq = False
    Flg_LimpaCampos = True

    BtPesquisaEspecif.Enabled = False
    BtPesquisaProduto.Enabled = False
    BtNotas.Enabled = False
    BtNotasVRM.Enabled = False
    BtFichaCruzada.Enabled = False
    
    BtResumo.Enabled = False
    LimpaFgProd
    TB_LimpaFgTubos FGTubos
    LimpaFGAna
    LimpaFGAnaEfr
    LimpaFgRecibos
    
    LimpaFgInfo
    FrameAnaRecibos.Visible = False
    SSTGestReq.Tab = 1
    DoEvents
    
    
    CbTipoIsencao.ListIndex = mediComboValorNull
    
    
    Call Apaga_DS_TM
    
    
    
    Flg_DadosAnteriores = False
    
    SSTGestReq.Enabled = True
    
    TotalEliminadas = 0
    ReDim Eliminadas(0)
    
    FrameDomicilio.Visible = False
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    CkCobrarDomicilio.value = vbGrayed
    BtResumo.Enabled = False
    
    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)
    
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
    
    CkAvisarSMS.value = vbGrayed
    flgEmissaoRecibos = False
    GereFrameAdiantamentos False
        
    flg_ReqBloqueada = False
    EcAnaEfr = ""
    EcMedAct = ""
    EcCredAct = ""
    EcBenefAct = ""
    EcIsencaoAct = ""
    AlteraFlgCodBarras False
    CbEFR.Clear
    LimpaFgRecAna
    ' pferreira 2011.05.31
    CkReqHipocoagulados.value = vbGrayed
    EcFimSemana = ""
    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    PM_LimpaEstrutura
    EcCodEFR.locked = False
    BtPesquisaEntFin.Enabled = True
    
    'RGONCALVES 29.05.2013 Cedivet-30
    codEfrOriginal = ""
    '
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaCampos: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaCampos"
    Exit Sub
    Resume Next
End Sub

Sub LimpaCampos2()
    On Error GoTo TrataErro
    
    'Me.SetFocus

    
    BG_LimpaCampo_Todos CamposEc
    
    'colocar o bot�o de cancelar requisi��es invisivel
    EcNumColheita.text = ""
    BtCancelarReq.Visible = False
    flg_novaArs = -1
    LbNomeUtilAlteracao.caption = ""
    LbNomeUtilAlteracao.ToolTipText = ""
    LbNomeUtilCriacao.caption = ""
    LbNomeUtilCriacao.ToolTipText = ""
    LbNomeUtilImpressao.caption = ""
    LbNomeUtilImpressao.ToolTipText = ""
    LbNomeUtilImpressao2.caption = ""
    LbNomeUtilImpressao2.ToolTipText = ""
    LbNomeUtilFecho.caption = ""
    LbNomeUtilFecho.ToolTipText = ""
    LbNomeLocal.caption = ""
    LbNomeLocal.ToolTipText = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    EcDataNasc.text = ""
    EcSexo = ""
    EcIdade.text = ""
    EcNome.text = ""
    EcNomeDono.text = ""
    EcUtente.text = ""
    EcDescrEFR.text = ""
    EcDescrMedico = ""
    EcPesqRapProven.text = ""
    EcPesqRapProduto.text = ""
    EcDataNasc.text = ""
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    FrBtTubos.Enabled = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabEnabled(5) = False
    SSTGestReq.TabEnabled(6) = False
    EcCodEFR.locked = False
    EcNumBenef.locked = False
    
    EcCodEFR.ToolTipText = ""
    EcNumBenef.ToolTipText = ""
    
    EcCodEFR.ForeColor = vbBlack
    EcNumBenef.ForeColor = vbBlack
    CbTipoIsencao.Enabled = True
    CbPrioColheita.ListIndex = mediComboValorNull
    CbSala.ListIndex = mediComboValorNull
    EcCodEfr2 = ""
    EcDescrEfr2 = ""
    EcMorada.text = ""
    EcCodPostal.text = ""
    EcRuaPostal.text = ""
    EcDescrPostal.text = ""
    CbDestino.ListIndex = mediComboValorNull
    EcObsAna.text = ""
    FrameObsAna.Visible = False
    
    EcDescrProveniencia = ""
    EcDescrSala = ""
    EcUtilNomeConf = ""
    EcDescrEstado = ""
    EcDescrPais = ""
    
    'Fun��o que limpa os produtos da requisi��o
    LaProdutos.caption = ""
    
    'inicializa��es da lista de marca��es de an�lises
    Inicializa_Estrutura_Analises
     
         
    'colocar cabe�alho e data prevista enabled a false
'    EcNumReq.Enabled = True
    EcNumReq.locked = False
    EcNumReqAssoc.locked = False
    
    CbTipoUtente.Enabled = True
    EcUtente.Enabled = True
    EcDataPrevista.Enabled = True
    EcDataChegada.Enabled = True
    
    BtInfClinica.Enabled = False
    BtDomicilio.Enabled = False
    BtDigitalizacao.Enabled = False
    
    BtConsReq.Enabled = False
    Set CampoDeFocus = EcNumReq
    
    gFlg_JaExisteReq = False
    Flg_LimpaCampos = True

    BtPesquisaEspecif.Enabled = False
    BtPesquisaProduto.Enabled = False
    
    LimpaFgProd
    TB_LimpaFgTubos FGTubos
    LimpaFGAna
    LimpaFGAnaEfr
    LimpaFgRecibos
    
    LimpaFgInfo
    SSTGestReq.Tab = 0
    DoEvents
    
    
    Call Apaga_DS_TM
    
    
    Flg_DadosAnteriores = False
    
    TotalEliminadas = 0
    ReDim Eliminadas(0)
    
    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)
    
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
   
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    
    CkAvisarSMS.value = vbGrayed
    flgEmissaoRecibos = False
    GereFrameAdiantamentos False

    CbHemodialise.Enabled = False
    flg_ReqBloqueada = False
    EcAnaEfr = ""
    EcMedAct = ""
    EcCredAct = ""
    EcBenefAct = ""
    EcIsencaoAct = ""
    AlteraFlgCodBarras False
    flg_codBarras = False
    CbEFR.Clear
    CkReqHipocoagulados.value = vbGrayed
    EcFimSemana = ""
    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    PM_LimpaEstrutura
    LimpaFgRecAna
    EcCodEFR.locked = False
    BtPesquisaEntFin.Enabled = True

Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaCampos2: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaCampos2"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoAnterior()
    On Error GoTo TrataErro
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BG_MensagemAnterior
    Else
        
        Call LimpaCampos
        gFlg_JaExisteReq = True
        EcDataPrevista.Enabled = False
        Call PreencheCampos
        
        'Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados
        If rs!estado_req = gEstadoReqCancelada Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabEnabled(5) = False
            SSTGestReq.TabEnabled(6) = False
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(3) = True
            SSTGestReq.TabEnabled(4) = True
            SSTGestReq.TabEnabled(5) = True
            SSTGestReq.TabEnabled(6) = True
            BtCancelarReq.Visible = False
        
        End If
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoAnterior: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoAnterior"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoSeguinte()

    On Error GoTo TrataErro
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BG_MensagemSeguinte
    Else
        
        Call LimpaCampos
        gFlg_JaExisteReq = True
        EcDataPrevista.Enabled = False
        Call PreencheCampos
        
        ' Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados.
        If rs!estado_req = gEstadoReqCancelada Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabEnabled(5) = False
            SSTGestReq.TabEnabled(6) = False
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(3) = True
            SSTGestReq.TabEnabled(4) = True
            SSTGestReq.TabEnabled(5) = True
            SSTGestReq.TabEnabled(6) = True
            BtCancelarReq.Visible = False
            
        End If
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoSeguinte: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoSeguinte"
    Exit Sub
    Resume Next
End Sub

Sub PreencheCampos()
    Dim aux As String
    On Error GoTo TrataErro
    Dim i As Integer
    'Me.SetFocus
    flg_preencheCampos = True
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BtCancelarReq.Visible = True
        BtNotas.Enabled = True
        BtNotasVRM.Enabled = True
        BtFichaCruzada.Enabled = True
        BtResumo.Enabled = True
        BtDigitalizacao.Enabled = True
        
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        
        BL_PreencheVectorBackup NumCampos, CamposEc, CamposEcBckp
        aux = EcHemodialise
        EcHemodialise = aux
        BL_ColocaTextoCombo "sl_cod_hemodialise", "seq_hemodialise", "cod_hemodialise", EcHemodialise, CbHemodialise
        EcNumColheita.text = COLH_DevolveNumColheita(EcNumReq.text)
        
        LbNomeUtilCriacao.caption = BL_SelNomeUtil(EcUtilizadorCriacao.text)
        LbNomeUtilCriacao.ToolTipText = LbNomeUtilCriacao.caption
        LbNomeUtilAlteracao.caption = BL_SelNomeUtil(EcUtilizadorAlteracao.text)
        LbNomeUtilAlteracao.ToolTipText = LbNomeUtilAlteracao.caption
        LbNomeUtilImpressao.caption = BL_SelNomeUtil(EcUtilizadorImpressao.text)
        LbNomeUtilImpressao.ToolTipText = LbNomeUtilImpressao.caption
        LbNomeUtilImpressao2.caption = BL_SelNomeUtil(EcUtilizadorImpressao2.text)
        LbNomeUtilImpressao2.ToolTipText = LbNomeUtilImpressao2.caption
        LbNomeUtilFecho.caption = BL_SelNomeUtil(EcUtilizadorFecho.text)
        LbNomeUtilFecho.ToolTipText = LbNomeUtilFecho.caption
        LbNomeLocal.caption = BL_SelCodigo("gr_empr_inst", "nome_empr", "empresa_id", EcLocal, "V")
        LbNomeLocal.ToolTipText = LbNomeLocal.caption
        EcUtilNomeConf.caption = BL_SelNomeUtil(EcUtilConf.text)
        EcUtilNomeConf.ToolTipText = EcUtilNomeConf.caption

        BL_PreencheDadosUtente EcSeqUtente.text
        PreencheDescEntFinanceira
        PreencheEstadoRequisicao
        PreencheDescMedico
        EcCodpais_Validate False
        EcNumReq.locked = True
        EcNumReqAssoc.locked = True
        PreencheDadosUtente
        PM_CarregaPerfisMarcados EcNumReq.text
        If EcDataNasc <> "" Then
            EcIdade = BG_CalculaIdade(CDate(EcDataNasc.text))
        End If
                
        'colocar o bot�o de cancelar requisi��es activo
        gRequisicaoActiva = CLng(EcNumReq.text)
        gFlg_JaExisteReq = True
        
        
        CbTipoUtente.Enabled = False
        EcUtente.Enabled = False
        FrProdutos.Enabled = True
        FrBtProd.Enabled = True
        FrBtTubos.Enabled = True
        If Trim(EcDataChegada.text) <> "" Then
            EcDataChegada.Enabled = False
            EcDataPrevista.Enabled = False
        Else
            EcDataChegada.Enabled = True
            EcDataPrevista.Enabled = True
        End If
        i = InStr(1, EcCodPostalAux, "-")
        If i > 0 Then
            EcCodPostal = Mid(EcCodPostalAux, 1, i - 1)
            EcRuaPostal = Mid(EcCodPostalAux, i + 1, Len(EcCodPostalAux))
        ElseIf i = 0 And EcCodPostalAux.text <> "" Then
            EcCodPostal = Mid(EcCodPostalAux, 1, 4)
            EcRuaPostal = Mid(EcCodPostalAux, 5, 7)
        End If
        EcDescrPostal = BL_SeleccionaDescrPostal(EcCodPostalAux)
        'PreencheCodigoPostal
        FuncaoProcurar_PreencheProd CLng(Trim(EcNumReq.text))
        TB_ProcuraTubos FGTubos, CLng(Trim(EcNumReq.text)), True
        FuncaoProcurar_PreencheAna CLng(Trim(EcNumReq.text))
        RECIBO_PreencheCabecalho EcNumReq, RegistosR
        RECIBO_PreencheDetManuais Trim(EcNumReq.text), RegistosRM, CbTipoUtente, EcUtente
        PreencheEntidades
        PreencheInformacao EcNumReq
        PreencheQuestao EcNumReq

        If CbEFR.ListIndex > mediComboValorNull Then
            PreencheFgAnaEFR_todos CbEFR.ItemData(CbEFR.ListIndex)
        End If
        RECIBO_PreencheAdiantamento Trim(EcNumReq.text), RegistosAD
        ActualizaLinhasRecibos -1
        
        PreencheFgRecibos
        PreencheAnaRecibos
        PreencheFgAdiantamentos
        PreencheNotas
        'CbSituacao.SetFocus
        'EcNumReq.SetFocus
        
        'Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados
        If rs!estado_req = gEstadoReqCancelada Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabEnabled(5) = False
            SSTGestReq.TabEnabled(6) = False
            BtCancelarReq.Visible = True
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(3) = True
            SSTGestReq.TabEnabled(4) = True
            SSTGestReq.TabEnabled(5) = True
            SSTGestReq.TabEnabled(6) = True

            
            BtCancelarReq.Visible = False
            If flg_ReqBloqueada = False Then
                BL_Toolbar_BotaoEstado "Remover", "Activo"
                BL_Toolbar_BotaoEstado "Modificar", "Activo"
            Else
                BL_Toolbar_BotaoEstado "Remover", "InActivo"
                BL_Toolbar_BotaoEstado "Modificar", "InActivo"
            End If
            
            
        End If
        
        ' PFerreira 15.03.2007
        If CbPrioColheita.ListCount > 0 Then
            CbPrioColheita.ListIndex = 1
        End If
        
        CodEfrAUX = EcCodEFR
        If BL_HandleNull(rs!flg_cobra_domicilio, 0) = 1 Then
            CkCobrarDomicilio.value = vbChecked
            CkCobrarDomicilio.Enabled = True
        End If
    End If
    flg_preencheCampos = False
    
Exit Sub
TrataErro:
    flg_preencheCampos = False
    BG_LogFile_Erros "PreencheCampos: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheCampos"
    Exit Sub
    Resume Next
End Sub

Sub PreencheEstadoRequisicao()
    On Error GoTo TrataErro
    If EcEstadoReq = gEstadoReqBloqueada Then
       EcDescrEstado.BackColor = vermelhoClaro
    Else
       EcDescrEstado.BackColor = 14737632
    End If
   EcDescrEstado = BL_DevolveEstadoReq(Trim(BL_HandleNull(rs!estado_req, "")))
   
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheEstadoRequisicao: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheEstadoRequisicao"
    Exit Sub
    Resume Next
    
End Sub


Sub PreencheDescEntFinanceira()
    On Error GoTo TrataErro
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    If EcCodEFR.text <> "" Then
        If VerificaEfrHemodialise(EcCodEFR) = True Then
            CbHemodialise.Enabled = True
        End If
        Set Tabela = New ADODB.recordset
        sql = "SELECT descr_efr, flg_nova_ars FROM sl_efr WHERE cod_efr=" & Trim(EcCodEFR.text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Entidade inexistente!", vbOKOnly + vbInformation, "Entidades"
            EcCodEFR.text = ""
            EcDescrEFR.text = ""
            flg_novaArs = -1
            If (gLAB = cHSMARTA) Then
                Me.EcCodEFR = "0"
                Call EcCodEFR_Validate(True)
            End If
            
            EcCodEFR.SetFocus
        Else
            flg_novaArs = BL_HandleNull(Tabela!flg_nova_ars, 0)
            EcDescrEFR.text = Tabela!descr_efr
        End If
        Tabela.Close
        Set Tabela = Nothing

    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDescEntFinanceira: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDescEntFinanceira"
    Exit Sub
    Resume Next
    
End Sub



Private Sub SSTGestReq_Click(PreviousTab As Integer)
    On Error GoTo TrataErro
    Dim i
    Dim j As Integer
    DoEvents
    
    If SSTGestReq.Tab = 1 Then
        If EcDataFecho.text <> "" Then
            BtPesquisaAna.Enabled = False
        Else
            BtPesquisaAna.Enabled = True
            FGAna.SetFocus
            FGAna.row = FGAna.rows - 1
            FGAna.Col = 0
            
        End If
        
    ElseIf SSTGestReq.Tab = 0 Then
        EcAuxAna.Visible = False
        EcAuxProd.Visible = False
        If EcDataPrevista.Enabled = True And EcDataPrevista.Visible = True Then
            If gMultiReport <> 1 Then
                EcDataPrevista.SetFocus
            End If
        ElseIf EcDataChegada.Enabled = True And EcDataChegada.Visible = True Then
            'CbSituacao.SetFocus             'sdo
        End If
    
    ElseIf SSTGestReq.Tab = 2 Then
        If FgProd.Enabled = True Then FgProd.SetFocus
    ElseIf SSTGestReq.Tab = 5 Then
        EcAnaEfr.SetFocus
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "SSTGestReq_Click: " & Err.Number & " - " & Err.Description, Me.Name, "SSTGestReq_Click"
    Exit Sub
    Resume Next
End Sub


Sub Funcao_DataActual()
    On Error GoTo TrataErro

    If Me.ActiveControl.Name = "EcDataCriacao" Then
        BL_PreencheData EcDataCriacao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataAlteracao" Then
        BL_PreencheData EcDataAlteracao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataImpressao" Then
        BL_PreencheData EcDataImpressao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataImpressao2" Then
        BL_PreencheData EcDataImpressao2, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataPrevista" Then
        BL_PreencheData EcDataPrevista, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataChegada" Then
        BL_PreencheData EcDataChegada, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtPretend" Then
        BL_PreencheData EcDtPretend, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcAuxProd" Then
        If EcAuxProd.Tag = adDate Then
            BL_PreencheData EcAuxProd, Me.ActiveControl
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_DataActual: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_DataActual"
    Exit Sub
    Resume Next
    
End Sub

Function Coloca_Estado(DtPrev, DtChega, Optional DtElim = "") As String
    On Error GoTo TrataErro
    If DateValue(DtPrev) < DateValue(Bg_DaData_ADO) And DtChega = "" Then
        Coloca_Estado = "Falta"
    ElseIf DateValue(DtPrev) >= DateValue(Bg_DaData_ADO) And DtChega = "" Then
        Coloca_Estado = "Espera"
    ElseIf DtChega <> "" Then
        Coloca_Estado = "Entrou"
    Else
        Coloca_Estado = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Coloca_Estado: " & Err.Number & " - " & Err.Description, Me.Name, "Coloca_Estado"
    Coloca_Estado = ""
    Exit Function
    Resume Next

End Function

Sub Preenche_LaProdutos(indice As Integer)
    On Error GoTo TrataErro
    
    Dim TamDescrProd As Integer
    Dim TamDescrEspecif As Integer
    Dim i As Integer
    Dim Str1 As String
    Dim Str2 As String
    Dim Str3 As String
    
    LaProdutos.caption = ""
    If RegistosP(indice).CodProd <> "" Then
        Str1 = "Produto vai chegar a: "
        Str2 = "Produto devia ter chegado a: "
        Str3 = "Produto chegou a: "
        If RegistosP(indice).EstadoProd = "Entrou" Then
            LaProdutos.caption = LaProdutos.caption & Str3 & RegistosP(indice).DtChega
        ElseIf RegistosP(indice).EstadoProd = "Falta" Then
            LaProdutos.caption = LaProdutos.caption & Str2 & RegistosP(indice).DtPrev
        ElseIf EcDataPrevista.text <> "" Then
            LaProdutos.caption = LaProdutos.caption & Str1 & RegistosP(indice).DtPrev
        End If
        TamDescrProd = Len(Trim(UCase(RegistosP(indice).DescrProd)))
        TamDescrEspecif = Len(Trim(UCase(RegistosP(indice).DescrEspecif)))
        For i = 1 To 80 - (TamDescrProd + TamDescrEspecif)
            LaProdutos.caption = LaProdutos.caption & Chr(32)
        Next i
        LaProdutos.caption = LaProdutos.caption & Trim(UCase(RegistosP(indice).DescrProd)) & " " & Trim(UCase(RegistosP(indice).DescrEspecif))
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Preenche_LaProdutos: " & Err.Number & " - " & Err.Description, Me.Name, "Preenche_LaProdutos"
    Exit Sub
    Resume Next

End Sub

Sub Inicializa_Estrutura_Analises()
    On Error GoTo TrataErro
    
    'inicializa��es da lista de marca��es de an�lises
    ReDim MaReq(0 To 0)
    MaReq(0).Cod_Perfil = ""
    MaReq(0).Descr_Perfil = ""
    MaReq(0).cod_ana_c = ""
    MaReq(0).Descr_Ana_C = ""
    MaReq(0).cod_ana_s = ""
    MaReq(0).descr_ana_s = ""
    MaReq(0).Ord_Ana = 0
    MaReq(0).Ord_Ana_Compl = 0
    MaReq(0).Ord_Ana_Perf = 0
    MaReq(0).cod_agrup = ""
    MaReq(0).N_Folha_Trab = 0
    MaReq(0).dt_chega = ""
    MaReq(0).Flg_Apar_Transf = ""
    MaReq(0).flg_estado = ""
    MaReq(0).Flg_Facturado = 0
    LbTotalAna = "0"
Exit Sub
TrataErro:
    BG_LogFile_Erros "Inicializa_Estrutura_Analises: " & Err.Number & " - " & Err.Description, Me.Name, "Inicializa_Estrutura_Analises"
    Exit Sub
    Resume Next

End Sub

Sub ImprimeResumo()
    On Error GoTo TrataErro
    If gImprFolhaResumoParaRecibos <> mediSim Then
        ImprimeResumoCrystal
    Else
        FR_ImprimeResumoParaImpressoraRecibos EcNumReq
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumo: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumo"
    Exit Sub
    Resume Next
End Sub

Private Sub CriaNovaClasse(Coleccao As Collection, Index As Long, tipo As String, Optional EmCascata As Variant)
    On Error GoTo TrataErro

    ' Tipo = ANA ou PROD
    ' EmCascata = True : Cria todos elementos at� o Index
    ' EmCascata = False: Cria apenas o elemento Index

    Dim Cascata As Boolean
    Dim i As Long
    Dim AuxRegEnt As New ClRegRecibos
    Dim AuxRegProd As New ClRegProdutos
    Dim AuxRegAna As New ClRegAnalises
    Dim AuxRegTubo As New ClRegTubos
    Dim AuxRegRecManuais As New ClRegRecibosAna
    Dim AuxAdiantamento As New ClAdiantamentos
    
    If IsMissing(EmCascata) Then
        Cascata = False
    Else
        Cascata = EmCascata
    End If
    
    If tipo = "PROD" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegProd
            Next i
        Else
            Coleccao.Add AuxRegProd
        End If
    ElseIf tipo = "TUBO" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegTubo
            Next i
        Else
            Coleccao.Add AuxRegTubo
        End If
    ElseIf tipo = "REC" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegEnt
            Next i
        Else
            Coleccao.Add AuxRegEnt
        End If
    ElseIf tipo = "ADI" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxAdiantamento
            Next i
        Else
            Coleccao.Add AuxAdiantamento
        End If
    ElseIf tipo = "REC_MAN" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegRecManuais
            Next i
        Else
            Coleccao.Add AuxRegRecManuais
        End If
    Else
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegAna
            Next i
        Else
            Coleccao.Add AuxRegAna
        End If
    End If
    
    Set AuxRegAna = Nothing
    Set AuxAdiantamento = Nothing
    Set AuxRegProd = Nothing
    Set AuxRegTubo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "CriaNovaClasse: " & Err.Number & " - " & Err.Description, Me.Name, "CriaNovaClasse"
    Exit Sub
    Resume Next
    
End Sub

Private Sub LimpaColeccao(Coleccao As Collection, Index As Long)
    On Error GoTo TrataErro

    ' Index = -1 : Apaga todos elementos
    
    Dim i As Long
    Dim Tot As Long

    Tot = Coleccao.Count
    If Index = -1 Then
        i = Tot
        While i > 0
            Coleccao.Remove i
            i = i - 1
        Wend
    Else
        If Index <= Coleccao.Count Then
            Coleccao.Remove Index
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaColeccao: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaColeccao"
    Exit Sub
    Resume Next

End Sub

Function Verifica_Prod_ja_Existe(indice As Integer, Produto As String) As Boolean
    On Error GoTo TrataErro
    
    Dim i As Integer
    Dim k As Integer
    Dim TotalRegistos As Integer
    
    Verifica_Prod_ja_Existe = False
    TotalRegistos = RegistosP.Count - 1
    If indice >= 0 Then
        i = 1
        While i <= TotalRegistos And Verifica_Prod_ja_Existe = False
            If UCase(Trim(Produto)) = UCase(Trim(RegistosP(i).CodProd)) And indice <> i Then
                Verifica_Prod_ja_Existe = True
            End If
            i = i + 1
        Wend
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Prod_ja_Existe: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Prod_ja_Existe"
    Verifica_Prod_ja_Existe = True
    Exit Function
    Resume Next

End Function


Function Verifica_Ana_ja_Existe(indice As Long, codAgrup As String) As Boolean
        
    Dim i As Integer
    Dim TotalRegistos As Integer
    Dim CodAgrupAux As String
    Dim codAna As String
    
    codAna = codAgrup
    SELECT_Descr_Ana codAna
    

        
    Verifica_Ana_ja_Existe = False
    TotalRegistos = RegistosA.Count - 1
   
   If InStr(1, "SCP", UCase(Mid(codAgrup, 1, 1))) > 0 Then
        If indice >= 0 Then
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAgrup)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) And indice <> i Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        Else
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAgrup)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        End If
    Else
        If indice >= 0 Then
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAna)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) And indice <> i Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        Else
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAna)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        End If
    End If

    codAgrup = CodAgrupAux & codAgrup

Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Ana_ja_Existe: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Ana_ja_Existe"
    Verifica_Ana_ja_Existe = True
    Exit Function
    Resume Next

End Function

Sub LimpaFgProd()
     On Error GoTo TrataErro
   
    Dim j As Long
    
    ExecutaCodigoP = False
    
    j = FgProd.rows - 1
    While j > 0
        If j > 1 Then
            FgProd.RemoveItem j
        Else
            FgProd.TextMatrix(j, 0) = ""
            FgProd.TextMatrix(j, 1) = ""
            FgProd.TextMatrix(j, 2) = ""
            FgProd.TextMatrix(j, 3) = ""
            FgProd.TextMatrix(j, 4) = ""
            FgProd.TextMatrix(j, 5) = ""
            FgProd.TextMatrix(j, 6) = ""
            FgProd.TextMatrix(j, 7) = ""
        End If
        
        j = j - 1
    Wend
    
    ExecutaCodigoP = True
    LastColP = 0
    LastRowP = 1
        
    CriaNovaClasse RegistosP, 1, "PROD", True
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgProd: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgProd"
    Exit Sub
    Resume Next

End Sub

Sub LimpaFGAna()
     On Error GoTo TrataErro
   Dim i As Integer
    Dim j As Long
    
    ExecutaCodigoA = False
    
    j = FGAna.rows - 1
    While j > 0
        If j > 1 Then
            FGAna.RemoveItem j
        Else
            For i = 0 To FGAna.Cols - 1
                FGAna.TextMatrix(j, i) = ""
            Next
            BL_MudaCorFg FGAna, j, vbWhite
        End If
        
        j = j - 1
    Wend
    
    ExecutaCodigoA = True
    LastColA = 0
    LastRowA = 1
        
    CriaNovaClasse RegistosA, 1, "ANA", True
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFGAna: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFGAna"
    Exit Sub
    Resume Next

End Sub
Sub LimpaFGAnaEfr()
     On Error GoTo TrataErro
    Dim j As Long
    Dim i As Integer
    j = FgAnaEFR.rows - 1
    While j > 0
        If j > 1 Then
            FgAnaEFR.RemoveItem j
        Else
            For i = 0 To FgAnaEFR.Cols - 1
                FgAnaEFR.TextMatrix(j, i) = ""
            Next
        End If
        
        j = j - 1
    Wend
    
        
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFGAnaEFR: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFGAnaEFR"
    Exit Sub
    Resume Next

End Sub

Sub LimpaFgRecAna()
    On Error GoTo TrataErro
    
    Dim j As Long
    
    FgAnaRec.rows = 2
    FgAnaRec.row = 1
    For j = 0 To FgAnaRec.Cols - 1
        FgAnaRec.Col = j
        FgAnaRec.CellBackColor = vbWhite
        FgAnaRec.TextMatrix(1, j) = ""
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgRecAna: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgRecAna"
    Exit Sub
    Resume Next
End Sub

Private Sub LimpaFgInfo()
    Dim i As Integer
    FgInformacao.rows = 2
    For i = 0 To FgInformacao.Cols - 1
        FgInformacao.TextMatrix(1, i) = ""
    Next
    
    FgQuestoes.rows = 2
    For i = 0 To FgQuestoes.Cols - 1
        FgQuestoes.TextMatrix(1, i) = ""
    Next
End Sub

Function Procura_Prod_ana() As Boolean

    On Error GoTo TrataErro
    'Verifica se existem an�lises com o produto que se pretende eliminar
    'No caso de existirem n�o deixa apagar o produto sem que se tenha apagado a an�lise

    Dim RsProd As ADODB.recordset
    Dim sql As String
    Dim lista As String
    Dim ListaP As String
    Dim i As Integer
    Dim CmdPesqGhost As ADODB.Command
    Dim RsPesqGhost As ADODB.recordset
    
    Procura_Prod_ana = False
    
    Set CmdPesqGhost = New ADODB.Command
    Set CmdPesqGhost.ActiveConnection = gConexao
    CmdPesqGhost.CommandType = adCmdText
    CmdPesqGhost.CommandText = "SELECT COD_MEMBRO FROM SL_MEMBRO WHERE COD_ANA_C = ? AND T_MEMBRO = 'A'"
    CmdPesqGhost.Prepared = True
    CmdPesqGhost.Parameters.Append CmdPesqGhost.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
    
    If UBound(MaReq) >= 1 Then
        If MaReq(1).cod_ana_s <> "" Then
            lista = ""
            ListaP = ""
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                    'Pesquisa os GhostMembers
                    CmdPesqGhost.Parameters(0) = MaReq(i).cod_ana_c
                    Set RsPesqGhost = CmdPesqGhost.Execute
                    While Not RsPesqGhost.EOF
                        lista = lista & BL_TrataStringParaBD(BL_HandleNull(RsPesqGhost!cod_membro, "-1")) & ","
                        RsPesqGhost.MoveNext
                    Wend
                    RsPesqGhost.Close
                    Set RsPesqGhost = Nothing
                Else
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_s) & ","
                End If
            Next i
            
            lista = Mid(lista, 1, Len(lista) - 1)
            
            sql = "SELECT descr_ana_s from sl_ana_s WHERE cod_produto = " & BL_TrataStringParaBD(FgProd.TextMatrix(FgProd.row, 0)) & _
                    " AND cod_ana_s IN ( " & lista & ")"
            
            Set RsProd = New ADODB.recordset
            RsProd.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            RsProd.Open sql, gConexao, adOpenStatic, adLockReadOnly
                   
            lista = ""
            While Not RsProd.EOF
                lista = lista & BL_HandleNull(RsProd!descr_ana_s, "") & ","
                RsProd.MoveNext
            Wend
            
            For i = 1 To UBound(MaReq)
                If MaReq(i).Cod_Perfil <> "" Then
                    If InStr(1, ListaP, MaReq(i).Cod_Perfil) = 0 Then
                        ListaP = ListaP & BL_TrataStringParaBD(MaReq(i).Cod_Perfil)
                    End If
                End If
            Next i
            
            sql = "SELECT descr_perfis from sl_perfis WHERE cod_produto = " & BL_TrataStringParaBD(FgProd.TextMatrix(FgProd.row, 0)) & _
                    " AND cod_perfis IN ( " & ListaP & ")"
            Set RsProd = New ADODB.recordset
            RsProd.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            RsProd.Open sql, gConexao, adOpenStatic, adLockReadOnly
            
            ListaP = ""
            While Not RsProd.EOF
                lista = lista & BL_HandleNull(RsProd!descr_perfis, "") & ","
                RsProd.MoveNext
            Wend
            
            If Trim(lista) <> "" Then
                lista = Mid(lista, 1, Len(lista) - 1)
                Procura_Prod_ana = True
                
                If RsProd.RecordCount > 1 Then
                    BG_Mensagem mediMsgBox, "As an�lises " & lista & " necessitam do produto " & FgProd.TextMatrix(FgProd.row, 1) & " para serem realizadas ! " & Chr(13) & "Remova primeiro as an�lises, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Produtos"
                Else
                    BG_Mensagem mediMsgBox, "A an�lise " & lista & " necessita do produto " & FgProd.TextMatrix(FgProd.row, 1) & " para ser realizada ! " & Chr(13) & "Remova primeiro a an�lise, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Produtos"
                End If
            End If
            
            RsProd.Close
            Set RsProd = Nothing
        End If
    End If
    
    Set CmdPesqGhost = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "Procura_Prod_ana: " & Err.Number & " - " & Err.Description, Me.Name, "Procura_Prod_ana"
    Procura_Prod_ana = False
    Exit Function
    Resume Next
    
End Function

Function Procura_Tubo_ana() As Boolean
    On Error GoTo TrataErro

    'Verifica se existem an�lises com o tubo que se pretende eliminar
    'No caso de existirem n�o deixa apagar o tubo sem que se tenha apagado a an�lise

    Dim rsTubo As ADODB.recordset
    Dim sql As String
    Dim lista As String
    Dim i As Integer
    Dim CmdPesqGhost As ADODB.Command
    Dim RsPesqGhost As ADODB.recordset
    
    Procura_Tubo_ana = False
    
    Set CmdPesqGhost = New ADODB.Command
    Set CmdPesqGhost.ActiveConnection = gConexao
    CmdPesqGhost.CommandType = adCmdText
    CmdPesqGhost.CommandText = "SELECT COD_MEMBRO FROM SL_MEMBRO WHERE COD_ANA_C = ? AND T_MEMBRO = 'A'"
    CmdPesqGhost.Prepared = True
    CmdPesqGhost.Parameters.Append CmdPesqGhost.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
    
    If UBound(MaReq) >= 1 Then
        If MaReq(1).cod_ana_s <> "" Then
            lista = ""
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                    'Pesquisa os GhostMembers
                    CmdPesqGhost.Parameters(0) = MaReq(i).cod_ana_c
                    Set RsPesqGhost = CmdPesqGhost.Execute
                    While Not RsPesqGhost.EOF
                        lista = lista & BL_TrataStringParaBD(BL_HandleNull(RsPesqGhost!cod_membro, "-1")) & ","
                        RsPesqGhost.MoveNext
                    Wend
                    RsPesqGhost.Close
                    Set RsPesqGhost = Nothing
                Else
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_s) & ","
                End If
            Next i
            lista = Mid(lista, 1, Len(lista) - 1)
            
            sql = "SELECT descr_ana_s from sl_ana_s WHERE cod_tubo = " & BL_TrataStringParaBD(FGTubos.TextMatrix(FGTubos.row, 0)) & _
                    " AND cod_ana_s IN ( " & lista & ")"
            
            Set rsTubo = New ADODB.recordset
            rsTubo.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            rsTubo.Open sql, gConexao, adOpenStatic, adLockReadOnly
                   
            lista = ""
            While Not rsTubo.EOF
                lista = lista & BL_HandleNull(rsTubo!descr_ana_s, "") & ","
                rsTubo.MoveNext
            Wend
            
            If Trim(lista) <> "" Then
                lista = Mid(lista, 1, Len(lista) - 1)
                Procura_Tubo_ana = True
                
                If rsTubo.RecordCount > 1 Then
                    BG_Mensagem mediMsgBox, "As an�lises " & lista & " necessitam do tubo " & FgProd.TextMatrix(FgProd.row, 1) & " para serem realizadas ! " & Chr(13) & "Remova primeiro as an�lises, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Tubos"
                Else
                    BG_Mensagem mediMsgBox, "A an�lise " & lista & " necessita do tubo " & FgProd.TextMatrix(FgProd.row, 1) & " para ser realizada ! " & Chr(13) & "Remova primeiro a an�lise, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Tubos"
                End If
            End If
            
            rsTubo.Close
            Set rsTubo = Nothing
        End If
    End If
    
    Set CmdPesqGhost = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "Procura_Tubo_ana: " & Err.Number & " - " & Err.Description, Me.Name, "Procura_Tubo_ana"
    Procura_Tubo_ana = False
    Exit Function
    Resume Next
      
End Function


Function ValidaEspecifObrig() As Boolean
    On Error GoTo TrataErro

    Dim i As Integer
    Dim n As Integer
    
    n = RegistosP.Count
    If n > 0 Then
        n = n - 1
        For i = 1 To n
            If RegistosP(i).CodProd <> "" Then
                If RegistosP(i).EspecifObrig = "S" Then
                    If RegistosP(i).CodEspecif = "" Then
                        BG_Mensagem mediMsgBox, "O produto " & RegistosP(i).DescrProd & " exige especifica��o !", vbInformation, ""
                        SSTGestReq.Tab = 1
                        Exit Function
                    End If
                End If
            End If
        Next
    End If
    ValidaEspecifObrig = True
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaEspecifObrig: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaEspecifObrig"
    ValidaEspecifObrig = False
    Exit Function
    Resume Next
      
End Function

Function ValidaProdutos() As Boolean
    

    On Error GoTo ErrorHandler
    
    Dim ret As Boolean
    ret = True
    
    If (RegistosP.Count > 1) Then
        ret = True
    Else
        If (RegistosA.Count > 1) Then
            ret = False
        Else
            ret = True
        End If
    End If
    
    ValidaProdutos = ret
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : EFR_Descricao (ValidaProdutos) -> " & Err.Description)
            ValidaProdutos = True
            Exit Function
    End Select
End Function


Sub BD_Insert_Utente()
    
    
    Dim SQLQuery As String
    Dim RsNUtente As ADODB.recordset
    Dim Query As String
    Dim i As Integer
    Dim seq As Long
    
    'se a data de inscri��o estiver vazia
    If gDUtente.dt_inscr = "" Then
        gDUtente.dt_inscr = Format(Bg_DaData_ADO, gFormatoData)
    End If
   
    If gDUtente.dt_nasc_ute = "" Then
        gDUtente.dt_nasc_ute = "01-01-1900"
    End If
   
   ' caso o tipo de utente nao esteja preenchido
    If gDUtente.t_utente = "" Then
        gDUtente.t_utente = "LAB"           'obrigar a que seja por defeito criado um utente LAB
    End If

    
    On Error GoTo Trata_Erro
        
    If Trim(gDUtente.Utente) = "" Then
        If BG_Mensagem(mediMsgBox, "Quer gerar um n�mero sequencial para o utente do tipo " & gDUtente.t_utente & "?", vbYesNo + vbDefaultButton2 + vbQuestion, "Gerar Sequencial") = vbNo Then
            Exit Sub
        End If
        
        'Determinar o novo numero para o Tipo Utente (TENTA 10 VEZES - LOCK!)
        i = 0
        seq = -1
        While seq = -1 And i <= 10
            seq = BL_GeraUtente(gDUtente.t_utente)
            i = i + 1
        Wend
        
        If seq = -1 Then
            BG_Mensagem mediMsgBox, "Erro a gerar n�mero do utente!", vbCritical + vbOKOnly, "ERRO"
            Exit Sub
        End If
        
        EcUtente = seq
    Else
        Set RsNUtente = New ADODB.recordset
        RsNUtente.CursorLocation = adUseServer
        RsNUtente.Open "SELECT nome_ute, dt_nasc_ute FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(gDUtente.t_utente) & " AND utente = " & BL_TrataStringParaBD(gDUtente.Utente), gConexao, adOpenStatic, adLockReadOnly
        
        If RsNUtente.RecordCount > 0 Then
            BG_Mensagem mediMsgBox, "J� existe um utente do tipo " & gDUtente.t_utente & " com o n�mero " & gDUtente.Utente & "!" & Chr(13) & "Nome : " & BL_HandleNull(RsNUtente!nome_ute) & Chr(13) & "Dt. Nasc : " & BL_HandleNull(RsNUtente!dt_nasc_ute), vbOKOnly + vbExclamation, "Inserir Utente"
            RsNUtente.Close
            Set RsNUtente = Nothing
            Exit Sub
        Else
            RsNUtente.Close
            Set RsNUtente = Nothing
        End If
    End If
        
    'Determinar o novo numero sequencial (TENTA 10 VEZES - LOCK!)
    i = 0
    seq = -1
    While seq = -1 And i <= 10
        seq = BL_GeraNumero("SEQ_UTENTE")
        i = i + 1
    Wend
    
    If seq = -1 Then
        BG_Mensagem mediMsgBox, "Erro a gerar n�mero de sequencial interno do utente!", vbCritical + vbOKOnly, "ERRO"
        Exit Sub
    End If
    
    EcSeqUtente = seq
    
    gSQLError = 0
    gSQLISAM = 0
    
    
    SQLQuery = "INSERT INTO sl_identif (seq_utente,t_utente,utente,n_proc_1,n_proc_2," & _
            " dt_inscr,n_cartao_ute,nome_ute,dt_nasc_ute,sexo_ute,est_civ_ute, " & _
            " descr_mor_ute,cod_postal_ute,telef_ute, cod_efr_ute,user_cri,dt_cri )" & _
            " values (" & EcSeqUtente & "," & BL_TrataStringParaBD(gDUtente.t_utente) & "," & BL_TrataStringParaBD(gDUtente.Utente) & "," & BL_TrataStringParaBD(gDUtente.n_proc_1) & "," & BL_TrataStringParaBD(gDUtente.n_proc_2) & _
            "," & BL_TrataDataParaBD(gDUtente.dt_inscr) & "," & BL_TrataStringParaBD(gDUtente.n_cartao_ute) & "," & BL_TrataStringParaBD(gDUtente.nome_ute) & "," & BL_TrataStringParaBD(gDUtente.dt_nasc_ute) & "," & BL_TrataStringParaBD(gDUtente.sexo_ute) & "," & BL_TrataStringParaBD(gDUtente.est_civil) & _
            "," & BL_TrataStringParaBD(gDUtente.morada) & "," & BL_TrataStringParaBD(gDUtente.cod_postal) & "," & BL_TrataStringParaBD(gDUtente.telefone) & "," & BL_TrataStringParaBD(gDUtente.cod_efr) & ",'" & gCodUtilizador & "'," & BL_TrataDataParaBD(Bg_DaData_ADO) & ")"
            
    BG_ExecutaQuery_ADO SQLQuery
    BG_Trata_BDErro
    
    If gSQLError <> 0 Then
        'Erro a inserir utente
        
        GoTo Trata_Erro
    End If
    

    gDUtente.seq_utente = BL_String2Double(EcSeqUtente)
    PreencheDadosUtente
    
    BG_CommitTransaction
    
    Exit Sub
    
Trata_Erro:
    BG_LogFile_Erros "FormIdentificaUtente: BD_Insert_Utente -> " & Err.Description
    BG_RollbackTransaction
    
    LimpaCampos

End Sub



Function Verifica_Ana_Pertence_Local(codAgrup As String) As Boolean
    On Error GoTo TrataErro
    Dim RsLocal As ADODB.recordset
    
    Set RsLocal = New ADODB.recordset
    RsLocal.CursorLocation = adUseServer
    RsLocal.CursorType = adOpenStatic
    RsLocal.Source = "select * from slv_analises where cod_ana = " & BL_TrataStringParaBD(codAgrup) & " and cod_local = " & gCodLocal
    RsLocal.ActiveConnection = gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros RsLocal.Source
    RsLocal.Open
    If RsLocal.RecordCount <= 0 Then
        Verifica_Ana_Pertence_Local = False
    Else
        Verifica_Ana_Pertence_Local = True
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Ana_Pertence_Local: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Ana_Pertence_Local"
    Verifica_Ana_Pertence_Local = False
    Exit Function
    Resume Next

End Function



' FGONCALVES
' 30 JUN 2006 - CHVNG


Private Function VerificaPodeEliminarTubo(CodTubo As String) As Boolean
    Dim i As Integer
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    VerificaPodeEliminarTubo = True
    
    For i = 1 To RegistosA.Count - 1
        
        If Mid(RegistosA(i).codAna, 1, 1) = "C" Then
            flg_Elimina = VerificaPodeEliminarTubo_C(CodTubo, RegistosA(i).codAna)
        ElseIf Mid(RegistosA(i).codAna, 1, 1) = "P" Then
            flg_Elimina = VerificaPodeEliminarTubo_P(CodTubo, RegistosA(i).codAna)
        Else
            flg_Elimina = VerificaPodeEliminarTubo_S(CodTubo, RegistosA(i).codAna)
        End If
        
        If flg_Elimina = False Then
            VerificaPodeEliminarTubo = flg_Elimina
            Exit Function
        End If
    Next
    VerificaPodeEliminarTubo = True
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarTubo: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarTubo"
    Exit Function
    Resume Next
    VerificaPodeEliminarTubo = False
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarTubo_C(CodTubo As String, analise As String) As Boolean
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    flg_Elimina = False
    
    sSql = "SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(analise)
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        While Not rsAna.EOF
            flg_Elimina = VerificaPodeEliminarTubo_S(CodTubo, rsAna!cod_membro)
            If flg_Elimina = False Then
                VerificaPodeEliminarTubo_C = flg_Elimina
                rsAna.Close
                Set rsAna = Nothing
                Exit Function
            End If
            rsAna.MoveNext
        Wend
    End If
    rsAna.Close
    Set rsAna = Nothing
    VerificaPodeEliminarTubo_C = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarTubo_C: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarTubo_C"
    VerificaPodeEliminarTubo_C = False
    Exit Function
    Resume Next
    
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarTubo_P(CodTubo As String, analise As String) As Boolean
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    flg_Elimina = False
    
    sSql = "SELECT  cod_tubo from sl_perfis where cod_perfis = " & BL_TrataStringParaBD(analise)
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If Not rsAna.EOF Then
        If BL_HandleNull(rsAna!cod_tubo, "") <> "" Then
            If rsAna!cod_tubo <> CodTubo Then
                flg_Elimina = True
            End If
        Else
            Set rsAna = New ADODB.recordset
            sSql = "SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(analise)
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    If Mid(analise, 1, 1) = "C" Then
                        flg_Elimina = VerificaPodeEliminarTubo_C(CodTubo, rsAna!cod_analise)
                    ElseIf Mid(analise, 1, 1) = "S" Then
                        flg_Elimina = VerificaPodeEliminarTubo_S(CodTubo, rsAna!cod_analise)
                    End If
                    
                    If flg_Elimina = False Then
                        VerificaPodeEliminarTubo_P = flg_Elimina
                        rsAna.Close
                        Set rsAna = Nothing
                        Exit Function
                    End If
                    rsAna.MoveNext
                Wend
            End If
        End If
    End If
        

    
    rsAna.Close
    Set rsAna = Nothing
    VerificaPodeEliminarTubo_P = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarTubo_P: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarTubo_P"
    VerificaPodeEliminarTubo_P = False
    Exit Function
    Resume Next
    
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarTubo_S(CodTubo As String, analise As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    
    sSql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(analise)
    sSql = sSql & " AND cod_tubo = " & BL_TrataStringParaBD(CodTubo)
    
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        VerificaPodeEliminarTubo_S = False
    Else
        VerificaPodeEliminarTubo_S = True
    End If
    
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarTubo_S: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarTubo_S"
    VerificaPodeEliminarTubo_S = False
    Exit Function
    Resume Next
End Function





' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Sub EliminaProduto(cod_ana As String)
    Dim i  As Integer
    Dim sSql As String
    Dim sSqlP As String
    Dim sSqlC As String
    Dim rsAna As New ADODB.recordset
    Dim RsProd As New ADODB.recordset
    Dim CodProduto As String
    Dim flg_PodeEliminar As Boolean
    Dim flg_existeProd As Boolean
    
    On Error GoTo TrataErro
    
    
    ' SE FOR COMPLEXA CHAMA A MESMA FUNCAO PARA CADA SIMPLES
    If Mid(cod_ana, 1, 1) = "C" Then
        sSqlC = "SELECT cod_produto FROM sl_ana_C WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
        sSql = "SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSqlC
        rsAna.Open sSqlC, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
            GoTo continuap
        Else
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    EliminaProduto rsAna!cod_membro
                    rsAna.MoveNext
                Wend
            End If
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
    
    
    ' SE FOR PERFIL CHAMA A MESMA FUNCAO PARA CADA MEMBRO
    If Mid(cod_ana, 1, 1) = "P" Then
        sSql = "SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        sSqlP = "SELECT cod_produto FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSqlP
        rsAna.Open sSqlP, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
            GoTo continuap
        Else
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    EliminaProduto rsAna!cod_analise
                    rsAna.MoveNext
                Wend
            End If
        End If
    End If
    
    ' ENCONTRA O CODIGO DO PRODUTO DA ANALISE EM CAUSA
    If Mid(cod_ana, 1, 1) = "S" Then
        CodProduto = ""
        sSql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
continuap:
            ' VERIFICA SE EXISTE PRODUTO NA FLEX GRID
            flg_existeProd = False
            For i = 1 To RegistosP.Count - 1
                If RegistosP(i).CodProd = CodProduto Then
                    flg_existeProd = True
                    Exit For
                End If
            Next
            If flg_existeProd = False Then
                rsAna.Close
                Set rsAna = Nothing
                Exit Sub
            End If
            ' ---------------------------------------
        End If
        rsAna.Close
        Set rsAna = Nothing
        
        ' SE NAO TEM PRODUTO CODIFICADO...SAI
        If CodProduto = "" Then
            Exit Sub
        End If
        
        flg_PodeEliminar = VerificaPodeEliminarProduto(CodProduto)
        
        
        ' NAO ENCONTROU MAIS NENHUMA ANALISE COM ESSE TUBO LOGO PODE ELIMINAR
        If flg_PodeEliminar = True Then
            
            ' VERIFICA SE JA FOI DADO ENTRADA DO PRODUTO
            If EcNumReq <> "" Then
                sSql = "SELECT * FROM sl_req_prod WHERE n_req =" & EcNumReq
                sSql = sSql & " AND cod_prod = " & BL_TrataStringParaBD(CodProduto)
                RsProd.CursorLocation = adUseServer
                RsProd.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                RsProd.Open sSql, gConexao
                If RsProd.RecordCount > 0 Then
                    If BL_HandleNull(RsProd!dt_chega, "") <> "" Then
                        ' SE JA DEU ENTRADA NAO ELIMINA
                        RsProd.Close
                        Set RsProd = Nothing
                        Exit Sub
                    End If
                End If
                RsProd.Close
                Set RsProd = Nothing
                ' -----------------------------------------
            End If
            
            ' ELIMINA DA FLEX GRID E DA ESTRUTURA
            For i = 1 To RegistosP.Count - 1
                If RegistosP(i).CodProd = CodProduto Then
                    FgProd.row = 1
                    If i < FgProd.rows - 1 Then
                        RegistosP.Remove i
                        FgProd.RemoveItem i
                    Else
                        FgProd.TextMatrix(i, 0) = ""
                        FgProd.TextMatrix(i, 1) = ""
                        FgProd.TextMatrix(i, 2) = ""
                        FgProd.TextMatrix(i, 3) = ""
                        FgProd.TextMatrix(i, 4) = ""
                        FgProd.TextMatrix(i, 5) = ""
                        FgProd.TextMatrix(i, 6) = ""
                        FgProd.TextMatrix(i, 7) = ""
                        RegistosP(i).CodProd = ""
                        RegistosP(i).DescrProd = ""
                        RegistosP(i).EspecifObrig = ""
                        RegistosP(i).CodEspecif = ""
                        RegistosP(i).DescrEspecif = ""
                        RegistosP(i).DtPrev = ""
                        RegistosP(i).DtChega = ""
                        RegistosP(i).EstadoProd = ""
                        RegistosP(i).Volume = ""
                    End If
                    Exit For
                End If
            Next
        End If
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no FormGestaoRequisicaoVet -> Elimina��o de Produtos: " & Err.Description
    Exit Sub
    Resume Next
End Sub


Private Function VerificaPodeEliminarProduto(CodProduto As String) As Boolean
    Dim i As Integer
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    
    VerificaPodeEliminarProduto = True
    
    For i = 1 To RegistosA.Count - 1
        
        If Mid(RegistosA(i).codAna, 1, 1) = "C" Then
            flg_Elimina = VerificaPodeEliminarProduto_C(CodProduto, RegistosA(i).codAna)
        ElseIf Mid(RegistosA(i).codAna, 1, 1) = "P" Then
            flg_Elimina = VerificaPodeEliminarProduto_P(CodProduto, RegistosA(i).codAna)
        Else
            flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, RegistosA(i).codAna)
        End If
        
        If flg_Elimina = False Then
            VerificaPodeEliminarProduto = flg_Elimina
            Exit Function
        End If
    Next
    VerificaPodeEliminarProduto = True
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto"
    VerificaPodeEliminarProduto = False
    Exit Function
    Resume Next
End Function





' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarProduto_C(CodProduto As String, analise As String) As Boolean
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim flg_Elimina As Boolean
     On Error GoTo TrataErro
   
    flg_Elimina = False
    
    sSql = "SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(analise)
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        While Not rsAna.EOF
            flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, rsAna!cod_membro)
            If flg_Elimina = False Then
                VerificaPodeEliminarProduto_C = flg_Elimina
                rsAna.Close
                Set rsAna = Nothing
                Exit Function
            End If
            rsAna.MoveNext
        Wend
    End If
    rsAna.Close
    Set rsAna = Nothing
    VerificaPodeEliminarProduto_C = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_C: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_C"
    VerificaPodeEliminarProduto_C = False
    Exit Function
    Resume Next
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarProduto_P(CodProduto As String, analise As String) As Boolean
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    
    flg_Elimina = False
    
    sSql = "SELECT cod_produto from sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(analise)
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If Not rsAna.EOF Then
        If BL_HandleNull(rsAna!cod_produto, "") <> "" Then
            If rsAna!cod_produto <> CodProduto Then
                flg_Elimina = True
            End If
        Else
            sSql = "SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(analise)
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    If Mid(analise, 1, 1) = "C" Then
                        flg_Elimina = VerificaPodeEliminarProduto_C(CodProduto, rsAna!cod_analise)
                    ElseIf Mid(analise, 1, 1) = "S" Then
                        'flg_Elimina = VerificaPodeEliminarTubo_S(CodProduto, RsAna!cod_analise)
                        flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, rsAna!cod_analise)
                    End If
                    
                    If flg_Elimina = False Then
                        VerificaPodeEliminarProduto_P = flg_Elimina
                        rsAna.Close
                        Set rsAna = Nothing
                        Exit Function
                    End If
                    rsAna.MoveNext
                Wend
            End If
        End If
        
    End If
    
        
    rsAna.Close
    Set rsAna = Nothing
    VerificaPodeEliminarProduto_P = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_P: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_P"
    VerificaPodeEliminarProduto_P = False
    Exit Function
    Resume Next
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarProduto_S(CodProduto As String, analise As String) As Boolean
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    
    
    sSql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(analise)
    sSql = sSql & " AND cod_produto = " & BL_TrataStringParaBD(CodProduto)
    
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        VerificaPodeEliminarProduto_S = False
    Else
        VerificaPodeEliminarProduto_S = True
    End If
    
    rsAna.Close
    Set rsAna = Nothing

Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_S: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_S"
    VerificaPodeEliminarProduto_S = False
    Exit Function
    Resume Next
End Function


Sub Lista_GhostMembers(CodComplexa As String, complexa As String)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean
    Dim iRec As Long
    On Error GoTo TrataErro

    Dim i As Long
    Dim TotalElementosSel As Integer
    Dim indice As Long
    
    PesqRapida = False
    iRec = DevIndice(RegistosA(FGAna.row).codAna)
    ChavesPesq(1) = "cod_membro"
    CamposEcran(1) = "cod_membro"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "sl_membro, sl_ana_s "
    CampoPesquisa = "descr_ana_s"
    CWhere = " sl_membro.cod_membro = sl_ana_s.cod_ana_s and cod_ana_c = " & BL_TrataStringParaBD(CodComplexa)
    
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                        ChavesPesq, _
                                                                        CamposEcran, _
                                                                        CamposRetorno, _
                                                                        Tamanhos, _
                                                                        Headers, _
                                                                        CWhere, _
                                                                        CFrom, _
                                                                        "", _
                                                                        CampoPesquisa, _
                                                                        " ORDER BY sl_membro.ordem ", _
                                                                        " Membros da an�lise " & complexa)
    
    If PesqRapida = True Then

        FormPesqRapidaAvancadaMultiSel.Show vbModal
        
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
        If Not CancelouPesquisa Then
            For i = 1 To TotalElementosSel
                InsereGhostMember CStr(Resultados(i))
'                FormGestaoRequisicao.EcAuxAna.Enabled = True
'                FormGestaoRequisicao.EcAuxAna = Resultados(i)
'                FormGestaoRequisicao.EcAuxAna_KeyDown 13, 0
            Next i
            'SE COMPLEXA MARCADA PARA ENVIAR QUANTIDADE PARA FACTUS
            If IF_ContaMembros(CodComplexa, RegistosRM(iRec).ReciboEntidade) > 0 Then
                i = FGAna.row
                EliminaReciboManual iRec, RegistosA(i).codAna, False
                RegistosRM(iRec).ReciboQuantidade = TotalElementosSel
                RegistosRM(iRec).ReciboQuantidadeEFR = RegistosRM(iRec).ReciboQuantidade
                RegistosRM(iRec).ReciboTaxa = Replace(RegistosRM(iRec).ReciboTaxaUnitario, ".", ",") * TotalElementosSel
                FGAna.TextMatrix(i, lColFgAnaPreco) = RegistosRM(iRec).ReciboTaxa
                AdicionaAnaliseAoReciboManual iRec, False
            End If
            FormGestaoRequisicaoVet.FGAna.SetFocus
        Else
        i = FGAna.row
        Dim ana As String
        ana = RegistosA(i).codAna
        If EliminaReciboManual(iRec, ana, True) = True Then
            EliminaFgAnaEfr CLng(iRec)
            LimpaColeccao RegistosRM, iRec
            FGAna.row = i
            Elimina_Mareq RegistosA(i).codAna
            ana = RegistosA(i).codAna
            RegistosA.Remove i
            If RegistosA.Count = 0 Then
                CriaNovaClasse RegistosA, 1, "ANA"
            End If
            If FGAna.rows = 2 Then
                FGAna.AddItem ""
            End If
            FGAna.RemoveItem i
            EliminaTuboEstrut
            EliminaProduto ana
        End If
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem membros", vbExclamation, "ATEN��O"
        If gFormActivo.Name = "FormGestaoRequisicaoVet" Then
            FormGestaoRequisicaoVet.FGAna.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Lista_GhostMembers: " & Err.Number & " - " & Err.Description, Me.Name, "Lista_GhostMembers"
    Exit Sub
    Resume Next
End Sub


Private Function DevolveDataValidade(n_req As String) As String
    Dim sql As String
    Dim rs As New ADODB.recordset
    On Error GoTo TrataErro
    
    
    '----------------------------------
    ' DATA VALIDADE ACTUAL
    sql = "SELECT dt_conclusao FROM sl_requis WHERE n_req = " & n_req
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rs.Open sql, gConexao
    If rs.RecordCount > 0 Then
        DevolveDataValidade = BL_HandleNull(rs!dt_conclusao, CDate(1))
    End If
    rs.Close
    Set rs = Nothing
    '----------------------------------
Exit Function
TrataErro:
    BG_LogFile_Erros "DevolveDataValidade: " & Err.Number & " - " & Err.Description, Me.Name, "DevolveDataValidade"
    DevolveDataValidade = ""
    Exit Function
    Resume Next
End Function


' pferreira 2010.03.08
' TICKET GLINTTHS-263.
Public Sub RegistaEliminadas(Optional elimina_todas As Boolean)
    
    Dim i As Integer
    Dim total As Integer
    
    Dim sSql As String
    If (elimina_todas) Then
        total = FGAna.rows - 2
    Else
        total = TotalEliminadas
    End If
    If EcNumReq <> "" Then
        For i = 1 To total
            If (Not elimina_todas) Then
                BL_RegistaAnaEliminadas EcNumReq, Eliminadas(i).codAna, "", "", ""
            Else
                BL_RegistaAnaEliminadas EcNumReq, FGAna.TextMatrix(i, 0), "", "", ""
            End If
            
        Next i
    End If
    TotalEliminadas = 0
    ReDim Eliminadas(0)
    
End Sub

Sub RegistaAcrescentadas()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim t_urg As Integer
    
    If CbUrgencia.ListIndex = mediComboValorNull Then
        t_urg = mediComboValorNull
    Else
        t_urg = CbUrgencia.ItemData(CbUrgencia.ListIndex)
    End If
    If EcNumReq <> "" Then
        For i = 1 To TotalAcrescentadas
            BL_RegistaAnaAcrescentadas EcSeqUtente.text, EcNumReq, "", "", "", Acrescentadas(i).cod_agrup, Bg_DaData_ADO, Bg_DaHora_ADO, t_urg, mediComboValorNull
        Next i
    End If
    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "RegistaAcrescentadas: " & Err.Number & " - " & Err.Description, Me.Name, "RegistaAcrescentadas"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' IMPRIME FOLHA DE RESUMO ATRAV�S DO CRYSTAL REPORTS

' -----------------------------------------------------------------
Private Sub ImprimeResumoCrystal()
    Dim continua As Boolean
    Dim ObsUtente As String
    Dim codProfis As String
    Dim descrProfis As String
    Dim sSql As String
    On Error GoTo TrataErro
    
    continua = BL_IniciaReport("FolhaResumo", "Folha de Resumo", crptToPrinter)
    
    If continua = False Then Exit Sub
    BL_MudaCursorRato mediMP_Espera, Me
    
    sSql = "DELETE FROM sl_cr_folha_Resumo WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    
    ImprimeResumoProdutos
    ImprimeResumoAnalises
    ImprimeResumoTubos
    
    'soliveira terrugem
    Dim rsOBS As ADODB.recordset
    Set rsOBS = New ADODB.recordset
    rsOBS.CursorLocation = adUseServer
    rsOBS.CursorType = adOpenStatic
    rsOBS.Source = "select obs,cod_profis_ute from sl_identif where seq_utente = " & EcSeqUtente
    If gModoDebug = mediSim Then BG_LogFile_Erros rsOBS.Source
    rsOBS.Open , gConexao
    If rsOBS.RecordCount > 0 Then
        ObsUtente = BL_HandleNull(rsOBS!obs, "")
        codProfis = BL_HandleNull(rsOBS!cod_profis_ute, "")
        If codProfis <> "" Then
            descrProfis = BL_SelCodigo("sl_profis", "DESCR_PROFIS", "COD_PROFIS", codProfis, "V")
        End If
    End If
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = ""
    
    'F�rmulas do Report
    Report.formulas(0) = "NumReq=" & BL_TrataStringParaBD("" & EcNumReq)
    Report.formulas(1) = "NomeUtente=" & BL_TrataStringParaBD("" & EcNome)
    Report.formulas(2) = "DtNascUtente=" & BL_TrataStringParaBD("" & EcDataNasc)
    Report.formulas(3) = "DtChegada=" & BL_TrataStringParaBD("" & EcDataChegada)
    Report.formulas(4) = "Processo=" & BL_TrataStringParaBD("" & EcUtente)
    Report.formulas(5) = "DtValidade=" & BL_TrataStringParaBD("" & DevolveDataValidade(CStr(gRequisicaoActiva)))
    Report.formulas(6) = "NSC=" & BL_TrataStringParaBD("" & EcUtente)
    Report.formulas(7) = "Servico=''"
    Report.formulas(8) = "SeqUtente=" & BL_TrataStringParaBD("" & EcSeqUtente)
    If gListaMedicos = mediSim Then
        If EcNomeMedico = "" And EcCodMedico <> "" Then
            EcCodMedico_Validate False
        End If
        Report.formulas(9) = "Medico=" & BL_TrataStringParaBD("" & Trim(EcNomeMedico))
    Else
        Report.formulas(9) = "Medico=" & BL_TrataStringParaBD("" & Trim(EcDescrMedico))
    End If
    Report.formulas(10) = "Idade=" & BL_TrataStringParaBD("" & EcIdade)
    Report.formulas(11) = "EFR=" & BL_TrataStringParaBD("" & EcDescrEFR)
    Report.formulas(12) = "Sala=" & BL_TrataStringParaBD("" & EcDescrSala)
    Report.formulas(13) = "Prioridade=" & BL_TrataStringParaBD("" & CbUrgencia)
    'soliveira terrugem
    Report.formulas(14) = "ObsUtente=" & BL_TrataStringParaBD("" & ObsUtente)
    Report.formulas(15) = "ReqAssoc=" & BL_TrataStringParaBD("" & EcNumReqAssoc)
    Report.formulas(16) = "InfComplementar=" & BL_TrataStringParaBD("" & Replace(EcinfComplementar, vbCrLf, ""))
    Report.formulas(17) = "descrProfis=" & BL_TrataStringParaBD("" & descrProfis)
    Report.SubreportToChange = "analises"
    Report.SelectionFormula = "{sl_cr_folha_resumo.nome_computador} = '" & BG_SYS_GetComputerName & "' AND trim({sl_cr_folha_resumo.cod_ana})<>'' AND {sl_cr_folha_resumo.n_req} = " & gRequisicaoActiva
    Report.Connect = "DSN=" & gDSN
    Report.SubreportToChange = "SubRepTubos"
    Report.SelectionFormula = "{sl_cr_folha_resumo.nome_computador} = '" & BG_SYS_GetComputerName & "' AND trim({sl_cr_folha_resumo.tubo}) <> '' AND {sl_cr_folha_resumo.n_req} = " & gRequisicaoActiva
    Report.Connect = "DSN=" & gDSN
    Report.SubreportToChange = ""
    


    Call BL_ExecutaReport
    BL_MudaCursorRato mediMP_Activo, Me
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoCrystal: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoCrystal"
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARIA AS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub ImprimeResumoAnalises()
    Dim i As Integer
    Dim sSql As String
    On Error GoTo TrataErro
    If FGAna.rows > 2 Then
        BG_BeginTransaction
        gSQLError = 0
        For i = 1 To FGAna.rows - 2
            sSql = "INSERT INTO sl_cr_folha_resumo (nome_computador, n_req,cod_ana,descr_ana, ordem) VALUES("
            sSql = sSql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
            sSql = sSql & gRequisicaoActiva & ", "
            sSql = sSql & BL_TrataStringParaBD(FGAna.TextMatrix(i, lColFgAnaCodigo)) & ", "
            sSql = sSql & BL_TrataStringParaBD(FGAna.TextMatrix(i, lColFgAnaDescricao)) & "," & i & ")"
            
            BG_ExecutaQuery_ADO sSql
        Next
        If gSQLError = 0 Then
            BG_CommitTransaction
        Else
            BG_RollbackTransaction
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoAnalises: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoAnalises"
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARAIA OS PRODUTOS DAS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub ImprimeResumoProdutos()
    Dim i As Integer
    Dim sSql As String
    On Error GoTo TrataErro
        
    If FgProd.rows > 2 Then
        BG_BeginTransaction
        gSQLError = 0
        For i = 1 To FgProd.rows - 2
            sSql = "INSERT INTO sl_cr_folha_resumo (nome_computador, n_req,produto) VALUES("
            sSql = sSql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
            sSql = sSql & gRequisicaoActiva & ", "
            sSql = sSql & BL_TrataStringParaBD(FgProd.TextMatrix(i, 1)) & ")"
            
            BG_ExecutaQuery_ADO sSql
        Next
        If gSQLError = 0 Then
            BG_CommitTransaction
        Else
            BG_RollbackTransaction
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoProdutos: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoProdutos"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARAIA OS TUBOS DAS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub ImprimeResumoTubos()
    Dim i As Integer
    Dim sSql As String
    On Error GoTo TrataErro
        
    If FGTubos.rows > 2 Then
        BG_BeginTransaction
        gSQLError = 0
        For i = 1 To FGTubos.rows - 2
            sSql = "INSERT INTO sl_cr_folha_resumo (nome_computador, n_req,tubo, norma_colheita) VALUES("
            sSql = sSql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
            sSql = sSql & gRequisicaoActiva & ", "
            sSql = sSql & BL_TrataStringParaBD(gEstruturaTubos(i).Designacao_tubo) & ","
            sSql = sSql & BL_TrataStringParaBD(gEstruturaTubos(i).normaTubo) & ")"
            
            BG_ExecutaQuery_ADO sSql
        Next
        If gSQLError = 0 Then
            BG_CommitTransaction
        Else
            BG_RollbackTransaction
        End If
    End If
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "ImprimeResumoTubos: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoTubos"
    Exit Sub
    Resume Next
End Sub

Function ProdutoUnico(GrupoAna As String) As Boolean
    Dim RsProdU As ADODB.recordset
    Dim sql As String
    Dim ret As Boolean
    On Error GoTo TrataErro
    
    
    ret = True
    
    sql = "select flg_produnico from sl_gr_ana where cod_gr_ana = " & BL_TrataStringParaBD(GrupoAna)
    
    Set RsProdU = New ADODB.recordset
    RsProdU.CursorLocation = adUseServer
    RsProdU.CursorType = adOpenStatic
    RsProdU.Source = "select flg_produnico from sl_gr_ana where cod_gr_ana = " & BL_TrataStringParaBD(GrupoAna)
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsProdU.Open sql, gConexao
    If Not RsProdU.EOF Then
        If BL_HandleNull(RsProdU!flg_produnico, "") = "1" Then
            ret = True
        Else
            ret = False
        End If
    End If
    
    ProdutoUnico = ret
    
    RsProdU.Close
    Set RsProdU = Nothing
    
Exit Function
TrataErro:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : FormGestaoRequisicaoVet (ProdutoUnico) -> " & Err.Description)
            ProdutoUnico = False
            Exit Function
    End Select
End Function


' PFerreira 19.04.2007
' Activa ou desactiva controlos de morada
Private Sub EnableAddress(Enabled As Boolean)
    On Error GoTo TrataErro

    EcMorada.Enabled = Enabled
    EcCodPostal.Enabled = Enabled
    EcRuaPostal.Enabled = Enabled
    EcDescrPostal.Enabled = Enabled
    BtPesqCodPostal.Enabled = Enabled
Exit Sub
TrataErro:
    BG_LogFile_Erros "EnableAddress: " & Err.Number & " - " & Err.Description, Me.Name, "EnableAddress"
    Exit Sub
    Resume Next
End Sub


' PFerreira 19.04.2007
' Limpa controlos de morada
Private Sub LimpaMorada()
    On Error GoTo TrataErro

    EcMorada.text = ""
    EcCodPostal.text = ""
    EcRuaPostal.text = ""
    EcDescrPostal.text = ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaMorada: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaMorada"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesqCodPostal_Click()
    On Error GoTo TrataErro

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    
    If Trim(EcDescrPostal) = "" And EcCodPostal = "" Then
        BG_Mensagem mediMsgBox, "Preencha a descri��o com uma localidade (ou parte da descri��o de uma), para limitar a lista de c�digos postais.", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
        Exit Sub
    End If
    
    PesqRapida = False
    
    If EcDescrPostal <> "" Then
        ChavesPesq(1) = "descr_postal"
        CamposEcran(1) = "descr_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Descri��o"
        
        ChavesPesq(2) = "cod_postal"
        CamposEcran(2) = "cod_postal"
        Tamanhos(2) = 2000
        Headers(2) = "C�digo"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        ChavesPesq(1) = "cod_postal"
        CamposEcran(1) = "cod_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Codigo"
        
        ChavesPesq(2) = "descr_postal"
        CamposEcran(2) = "descr_postal"
        Tamanhos(2) = 2000
        Headers(2) = "Descr��o"
    End If
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_postal"
    If EcDescrPostal <> "" And EcCodPostal = "" Then
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        CWhere = "UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    Else
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
        CWhere = CWhere & " AND UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    End If
    CampoPesquisa = "descr_postal"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_postal ", " C�digos Postais")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            i = InStr(1, Resultados(2), "-") - 1
            If i = -1 Then
                i = InStr(1, Resultados(1), "-") - 1
            End If
            If EcDescrPostal <> "" Then
                If i = -1 Then
                    s1 = Resultados(2)
                    s2 = ""
                Else
                    s1 = Mid(Resultados(2), 1, i)
                    s2 = Mid(Resultados(2), InStr(1, Resultados(2), "-") + 1)
                End If
                EcCodPostal.text = s1
                EcRuaPostal.text = s2
                EcCodPostalAux.text = Trim(Resultados(2))
                EcDescrPostal.text = Resultados(1)
                EcDescrPostal.SetFocus
            ElseIf EcDescrPostal = "" Then
                If i = -1 Then
                    s1 = Resultados(1)
                    s2 = ""
                Else
                    s1 = Mid(Resultados(1), 1, i)
                    s2 = Mid(Resultados(1), InStr(1, Resultados(1), "-") + 1)
                End If
                EcCodPostal.text = s1
                EcRuaPostal.text = s2
                EcCodPostalAux.text = Trim(Resultados(1))
                EcDescrPostal.text = Resultados(2)
                EcDescrPostal.SetFocus
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "C�digo postal n�o encontrado!", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesqCodPostal_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesqCodPostal_Click"
    Exit Sub
    Resume Next
End Sub
Private Sub PesquisaEntFin()

    PA_PesquisaEFR EcCodEfr2, EcDescrEfr2, ""
End Sub


Sub PreencheCodigoPostal()
    On Error GoTo TrataErro
    
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostalAux.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal = " & BL_TrataStringParaBD(EcCodPostal & "-" & EcCodPostalAux)
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo do c�digo postal inexistente!", vbExclamation, App.ProductName
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.text = s1
            EcRuaPostal.text = s2
            EcDescrPostal.text = Trim(rsCodigo!descr_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheCodigoPostal: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheCodigoPostal"
    Exit Sub
    Resume Next
End Sub
Private Sub EcRuaPostal_Validate(cancel As Boolean)
    On Error GoTo TrataErro
    
    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset
    
    EcRuaPostal.text = UCase(EcRuaPostal.text)
    If Trim(EcCodPostal) <> "" Then
        If Trim(EcRuaPostal) <> "" Then
            Postal = Trim(EcCodPostal.text) & "-" & Trim(EcRuaPostal.text)
        Else
            Postal = EcCodPostal
        End If
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal=" & BL_TrataStringParaBD(Postal)
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        
        If rsCodigo.RecordCount <= 0 Then
            cancel = True
            'BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrPostal = ""
            EcRuaPostal = ""
            EcCodPostal = ""
            EcCodPostalAux = ""
        Else
            EcCodPostalAux.text = Postal
            EcDescrPostal.text = rsCodigo!descr_postal
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrPostal.text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcRuaPostal_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcRuaPostal_Validate"
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------------------------------------

' LIMPA FGRECIBOS

' -----------------------------------------------------------------------------------------------
Sub LimpaFgRecibos()
     On Error GoTo TrataErro
   Dim j As Long
    
    ExecutaCodigoR = False
    j = FGRecibos.rows - 1
    While j > 0
        If j > 1 Then
            FGRecibos.RemoveItem j
        Else
            FGRecibos.TextMatrix(j, 0) = ""
            FGRecibos.TextMatrix(j, 1) = ""
            FGRecibos.TextMatrix(j, 2) = ""
            FGRecibos.TextMatrix(j, 3) = ""
            FGRecibos.TextMatrix(j, 4) = ""
            FGRecibos.TextMatrix(j, 5) = ""
            FGRecibos.TextMatrix(j, 6) = ""
            BL_MudaCorFg FGRecibos, j, vbWhite
        End If
        j = j - 1
    Wend
    
    ExecutaCodigoR = True
    LastColR = 0
    LastRowR = 1
    CriaNovaClasse RegistosR, 1, "REC", True
    CriaNovaClasse RegistosRM, 1, "REC_MAN", True
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgRecibos: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgRecibos"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------

' QUANDO ADICIONA UMA NOVA ANALISE, ADICIONA TAMBEM RESPECTIVA TAXA NA ESTRUTURA DE RECIBOS

' -----------------------------------------------------------------------------------------------

Private Sub AdicionaAnaliseAoReciboManual(indice As Long, MudaEstadoPerdidas As Boolean)
    On Error GoTo TrataErro
    Dim i As Long
    Dim Flg_inseriuEntidade As Boolean
    Dim Flg_EntidadeJaEmitida As Boolean
    Flg_inseriuEntidade = False
    
    ' ------------------------------------------------------------------------------------------
    ' SE TAXA FOR 0 NAO E NECESSARIO ACRESCENTAR
    ' ------------------------------------------------------------------------------------------

    If RegistosRM(indice).ReciboCodFacturavel <> RegistosRM(indice).ReciboCodAna Then
        For i = 1 To indice
            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(indice).ReciboCodFacturavel Then
                If RegistosRM(i).ReciboCodAna <> RegistosRM(indice).ReciboCodAna Then
                    Exit Sub
                End If
            End If
        Next
    End If
    
    If RegistosR.Count = 0 Then
        CriaNovaClasse RegistosR, i + 1, "REC"
    End If
    Flg_EntidadeJaEmitida = False
    For i = 1 To RegistosR.Count
        ' ------------------------------------------------------------------------------------------
        ' ENTIDADE JA EXISTE NA ESTRUTURA
        ' ------------------------------------------------------------------------------------------
        If RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade Then
            If RegistosR(i).Estado = gEstadoReciboNaoEmitido Or RegistosR(i).Estado = gEstadoReciboNulo Or (RegistosR(i).Estado = gEstadoReciboPerdido And MudaEstadoPerdidas = True) Then
                RegistosR(i).ValorPagar = RegistosR(i).ValorPagar + Round(((RegistosRM(indice).ReciboTaxa * (100 - RegistosR(i).Desconto)) / 100), 2)
                RegistosR(i).ValorOriginal = RegistosR(i).ValorOriginal + RegistosRM(indice).ReciboTaxa
                RegistosR(i).NumAnalises = RegistosR(i).NumAnalises + 1
                FGRecibos.TextMatrix(i, lColRecibosValorPagar) = BG_CvDecimalParaCalculo(RegistosR(i).ValorPagar)
                If RegistosR(i).ValorPagar > 0 Then
                
                    RegistosR(i).Estado = gEstadoReciboNaoEmitido
                    FGRecibos.TextMatrix(i, lColRecibosEstado) = "N�o Emitido"
                ElseIf RegistosR(i).ValorPagar = 0 Then
                    RegistosR(i).Estado = gEstadoReciboNulo
                    FGRecibos.TextMatrix(i, lColRecibosEstado) = "Recibo Nulo"
                End If
                Flg_inseriuEntidade = True
                If RegistosR(i).ValorPagar > 0 Then
                    RegistosR(i).Estado = gEstadoReciboNaoEmitido
                Else
                    RegistosR(i).Estado = gEstadoReciboNulo
                End If
            ElseIf RegistosR(i).Estado = gEstadoReciboPerdido Then
                RegistosR(i).ValorPagar = (RegistosR(i).ValorPagar) + (RegistosRM(indice).ReciboTaxa)
                RegistosR(i).ValorOriginal = (RegistosR(i).ValorOriginal) + (RegistosRM(indice).ReciboTaxa)
                RegistosR(i).NumAnalises = RegistosR(i).NumAnalises + 1
                FGRecibos.TextMatrix(i, lColRecibosValorPagar) = RegistosR(i).ValorPagar
                Flg_inseriuEntidade = True
                Flg_EntidadeJaEmitida = True
            ElseIf RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca Then
                Flg_EntidadeJaEmitida = True
            End If
        End If
    Next
    i = i - 1
    ' ------------------------------------------------------------------------------------------
    ' ENTIDADE NAO EXISTE NA ESTRUTURA
    ' ------------------------------------------------------------------------------------------
    If Flg_inseriuEntidade = False Then
        RegistosR(i).SeqRecibo = RECIBO_RetornaSeqRecibo
        RegistosR(i).NumDoc = 0
        RegistosR(i).SerieDoc = "0"
        RegistosR(i).codEntidade = RegistosRM(indice).ReciboEntidade
        RegistosR(i).DescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(indice).ReciboEntidade)
        RegistosR(i).codEmpresa = BL_SelCodigo("SL_EFR", "cod_empresa", "COD_EFR", RegistosRM(indice).ReciboEntidade)
        RegistosR(i).ValorPagar = BG_CvDecimalParaCalculo(RegistosRM(indice).ReciboTaxa)
        RegistosR(i).ValorOriginal = BG_CvDecimalParaCalculo(RegistosRM(indice).ReciboTaxa)
        RegistosR(i).Caucao = 0
        RegistosR(i).UserEmi = ""
        RegistosR(i).DtEmi = ""
        RegistosR(i).HrEmi = ""
        RegistosR(i).flg_impressao = False
        RegistosR(i).DtPagamento = ""
        RegistosR(i).HrPagamento = ""
        If Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar > 0 Then
            RegistosR(i).Estado = gEstadoReciboNaoEmitido
        ElseIf Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar = 0 Then
            RegistosR(i).Estado = gEstadoReciboNulo
        ElseIf Flg_EntidadeJaEmitida = True Then
            RegistosR(i).Estado = gEstadoReciboPerdido
        End If
        RegistosR(i).NumAnalises = 1
        RegistosR(i).flg_DocCaixa = 0
        
        FGRecibos.TextMatrix(i, lColRecibosNumDoc) = RegistosR(i).NumDoc
        FGRecibos.TextMatrix(i, lColRecibosEntidade) = RegistosR(i).DescrEntidade
        FGRecibos.TextMatrix(i, lColRecibosValorPagar) = RegistosR(i).ValorPagar
        FGRecibos.TextMatrix(i, lColRecibosDtEmissao) = RegistosR(i).DtEmi & " " & RegistosR(i).HrEmi
        FGRecibos.TextMatrix(i, lColRecibosDtPagamento) = RegistosR(i).DtPagamento & " " & RegistosR(i).HrPagamento
        If Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar > 0 Then
            FGRecibos.TextMatrix(i, lColRecibosEstado) = "N�o Emitido"
        ElseIf Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar = 0 Then
            FGRecibos.TextMatrix(i, lColRecibosEstado) = "Recibo Nulo"
        ElseIf Flg_EntidadeJaEmitida = True Then
            FGRecibos.TextMatrix(i, lColRecibosEstado) = "An�lises Perdidas"
        End If
        
        FGRecibos.AddItem ""
        CriaNovaClasse RegistosR, i + 1, "REC"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "AdicionaAnaliseAoReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "AdicionaAnaliseAoReciboManual"
    Exit Sub
    Resume Next
End Sub




' -----------------------------------------------------------------------------------------------

' ELIMINA A TAXA DO RECIBO - APENAS SE O RECIBO AINDA NAO TIVER SIDO EMITIDO

' -----------------------------------------------------------------------------------------------
Private Function EliminaReciboManual(indice As Long, codAna As String, EliminaAnalise As Boolean) As Boolean
    On Error GoTo TrataErro
    Dim i As Long
    Dim sSql As String
    
    EliminaReciboManual = True
    For i = 1 To RegistosR.Count
        If Trim(RegistosRM(indice).ReciboNumDoc) = Trim(RegistosR(i).NumDoc) And Trim(RegistosRM(indice).ReciboSerieDoc) = Trim(RegistosR(i).SerieDoc) And RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade Then
            If RegistosR(i).Estado = gEstadoReciboNaoEmitido Or RegistosR(i).Estado = gEstadoReciboPerdido Or RegistosR(i).Estado = gEstadoReciboNulo Then
                If RegistosR(i).NumAnalises > 0 Then RegistosR(i).NumAnalises = RegistosR(i).NumAnalises - 1
                
                RegistosR(i).ValorPagar = BG_CvDecimalParaCalculo(RegistosR(i).ValorPagar) - BG_CvDecimalParaCalculo(Round(((RegistosRM(indice).ReciboTaxa * (100 - RegistosR(i).Desconto)) / 100), 2))
                RegistosR(i).ValorOriginal = BG_CvDecimalParaCalculo(RegistosR(i).ValorOriginal) - BG_CvDecimalParaCalculo(BL_HandleNull(RegistosRM(indice).ReciboTaxa, 0))
                If RegistosR(i).ValorPagar <= 0 And RegistosR(i).NumAnalises = 0 Then
                    FGRecibos.RemoveItem i
                    LimpaColeccao RegistosR, i
                    FgAnaRec.RemoveItem (i)
                    Exit For
                ElseIf RegistosR(i).NumAnalises = 0 Then
                    FGRecibos.RemoveItem i
                    LimpaColeccao RegistosR, i
                    Exit For
                Else
                    If RegistosR(i).ValorPagar <= 0 And RegistosR(i).NumAnalises > 0 Then
                        RegistosR(i).Estado = gEstadoReciboNulo
                    End If
                    FGRecibos.TextMatrix(i, lColRecibosValorPagar) = RegistosR(i).ValorPagar
                End If

                EliminaReciboManual = True
            ElseIf (RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca) And EliminaAnalise = True Then
                EliminaReciboManual = False
            ElseIf (RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca) And EliminaAnalise = False Then
                BG_Mensagem mediMsgBox, "J� foi emitido recibo para esta an�lise.N�o Pode alterar valor deste campo", vbExclamation, "Altera��o Recibo."
                EliminaReciboManual = False
            End If
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "EliminaReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "EliminaReciboManual"
    EliminaReciboManual = False
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA A ORDEM DE MARCACAO

' -----------------------------------------------------------------------------------------------
Private Function RetornaOrdemMarcacao(ByVal linha As Long) As Long
    On Error GoTo TrataErro
    Dim codEfr As String
    Dim p1 As String
    Dim i As Integer
    Dim Max As Long
    Max = 0
    For i = 1 To RegistosA.Count - 1
        If RegistosRM(linha).ReciboCodFacturavel = RegistosA(i).codAna Then
            RetornaOrdemMarcacao = i
            Exit Function
        End If
    Next
    
    codEfr = RegistosRM(linha).ReciboEntidade
    p1 = RegistosRM(linha).ReciboP1
    If codEfr <> "" And p1 <> "" Then
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboEntidade = codEfr And RegistosRM(i).ReciboP1 = p1 And linha <> i Then
                If RegistosRM(i).ReciboOrdemMarcacao > Max Then
                    Max = RegistosRM(i).ReciboOrdemMarcacao
                End If
            End If
        Next
    End If
    RetornaOrdemMarcacao = Max + 1
Exit Function
TrataErro:
    BG_LogFile_Erros "RetornaOrdemMarcacao: " & Err.Number & " - " & Err.Description, Me.Name, "RetornaOrdemMarcacao"
    RetornaOrdemMarcacao = 0
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA A ORDEM DE MARCACAO DENTRO DO P1

' -----------------------------------------------------------------------------------------------
Private Function RetornaOrdemP1(ByVal linha As Long) As Long
    On Error GoTo TrataErro
    Dim codEfr As String
    Dim p1 As String
    Dim i As Integer
    Dim Max As Long
    Max = 0
    codEfr = RegistosRM(linha).ReciboEntidade
    p1 = RegistosRM(linha).ReciboP1
    If codEfr <> "" And p1 <> "" Then
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboEntidade = codEfr And RegistosRM(i).ReciboP1 = p1 And linha <> i Then
                If RegistosRM(i).ReciboOrdemMarcacao > Max Then
                    Max = RegistosRM(i).ReciboOrdemMarcacao
                End If
            End If
        Next
    End If
    RetornaOrdemP1 = Max + 1
Exit Function
TrataErro:
    BG_LogFile_Erros "RetornaOrdemP1: " & Err.Number & " - " & Err.Description, Me.Name, "RetornaOrdemP1"
    RetornaOrdemP1 = 0
    Exit Function
    Resume Next
End Function
' --------------------------------------------------------------------------------------------

Sub Grava_ObsAnaReq()
    On Error GoTo TrataErro

    Dim i As Integer
    Dim sSql As String
    
    ' -----------------------------------------------------------------------------------------------
    ' INSERE OS DADOS DE CADA ANALISE NA TABELA SL_OBS_ANA_REQ
    ' -----------------------------------------------------------------------------------------------
    If RegistosA.Count > 0 And Trim(RegistosA(1).codAna) <> "" Then
        For i = 1 To RegistosA.Count - 1
            If RegistosA(i).codAna <> "" And EcNumReq <> "" And RegistosA(i).ObsAnaReq <> "" Then
                BL_GravaObsAnaReq CStr(EcNumReq), RegistosA(i).codAna, RegistosA(i).ObsAnaReq, CLng(RegistosA(i).seqObsAnaReq), _
                                   0, EcSeqUtente, mediComboValorNull
                PreencheNotas
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_ObsAnaReq: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_ObsAnaReq"
    Exit Sub
    Resume Next
End Sub


' --------------------------------------------------------

' VERIFICA SE EXISTEM NOTAS PARA REQUISI��O EM CAUSA

' --------------------------------------------------------
Private Sub PreencheNotas()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsNotas As New ADODB.recordset
    
    If EcNumReq = "" Then Exit Sub
    sSql = "SELECT count(*) total FROM sl_obs_ana_req WHERE n_req = " & EcNumReq

    RsNotas.CursorType = adOpenStatic
    RsNotas.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsNotas.Open sSql, gConexao
    If RsNotas.RecordCount > 0 Then
        If RsNotas!total > 0 Then
            BtNotas.Visible = False
            BtNotasVRM.Visible = True
            
        Else
            BtNotas.Visible = True
            BtNotasVRM.Visible = False
        End If
    End If
    RsNotas.Close
    Set RsNotas = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheNotas: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheNotas"
    Exit Sub
    Resume Next
End Sub

Private Sub ImprimeEtiqNovo()
    'Limpar parametros
        BG_LimpaPassaParams
    'Passar parametros necessarios o ao form de impress�o
        gPassaParams.Param(0) = EcNumReq.text
    'Abrir form de impress�o
    FormNEtiqNTubos.Show
    DoEvents
    'FormGestaoRequisicaoPrivado.SetFocus

End Sub

Sub PreencheDescMedico()
    On Error GoTo TrataErro
        
    Dim Tabela As ADODB.recordset
    Dim sql As String

    If EcCodMedico.text <> "" Then
        Set Tabela = New ADODB.recordset
        sql = "SELECT nome_med FROM sl_medicos WHERE cod_med='" & Trim(EcCodMedico.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "M�dico inexistente!", vbOKOnly + vbInformation, "M�dicos"
            EcCodMedico.text = ""
            EcDescrMedico.text = ""
            EcCodMedico.SetFocus
        Else
            EcDescrMedico.text = Tabela!nome_med
            EcNomeMedico.text = Tabela!nome_med
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDescMedico: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDescMedico"
    Exit Sub
    Resume Next
End Sub
Sub AcrescentaAnalise(linha As Integer)
    
    Dim i As Integer
    Dim k As Long
    Dim iRec As Integer
    Dim lastCodAgrup As String
    On Error GoTo Trata_Erro
    
    'Cria linha vazia
    FGAna.AddItem "", FGAna.row
    CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
    ExecutaCodigoA = True
    Fgana_RowColChange
    
    ReDim Preserve MaReq(UBound(MaReq) + 1)
    If UBound(MaReq) = 0 Then ReDim MaReq(1)
    
'    i = UBound(MaReq)
    LastRowA = linha
    LastColA = 0
    i = RegistosA.Count
    
    While i > linha
            
            RegistosA(i).codAna = RegistosA(i - 1).codAna
            RegistosA(i).descrAna = RegistosA(i - 1).descrAna
            RegistosA(i).Estado = RegistosA(i - 1).Estado
            RegistosA(i).ObsAnaReq = RegistosA(i - 1).ObsAnaReq
            RegistosA(i).seqObsAnaReq = RegistosA(i - 1).seqObsAnaReq
            RegistosA(i).NReqARS = RegistosA(i - 1).NReqARS
        i = i - 1
    Wend
    
    'Actualiza ord_marca da estrutura MaReq
    k = 1
    lastCodAgrup = ""
    While k <= UBound(MaReq)
        If MaReq(k).Ord_Marca >= linha Then
            MaReq(k).Ord_Marca = MaReq(k).Ord_Marca + 1
            If lastCodAgrup <> MaReq(k).cod_agrup Then
                iRec = DevIndice(MaReq(k).cod_agrup)
                If iRec > -1 Then
                    RegistosRM(iRec).ReciboOrdemMarcacao = RegistosRM(iRec).ReciboOrdemMarcacao + 1
                End If
                lastCodAgrup = MaReq(k).cod_agrup
            End If
        End If
        k = k + 1
    Wend
    
    
    RegistosA(linha).codAna = ""
    RegistosA(linha).descrAna = ""
    RegistosA(linha).Estado = "-1"
    RegistosA(linha).ObsAnaReq = ""
    RegistosA(linha).seqObsAnaReq = 0
    RegistosA(linha).NReqARS = ""
    RegistosA(linha).p1 = ""
    RegistosA(linha).NReqARS = ""
    

    FGAna.TextMatrix(LastRowA, 2) = RegistosA(linha).NReqARS
    FGAna.TextMatrix(LastRowA, 3) = RegistosA(linha).p1
    
    
    
    ExecutaCodigoA = True
    FGAna.RowSel = linha
    FGAna.ColSel = 0
    Call Fgana_RowColChange
    FGAna.row = linha
    LastRowA = linha
    LastColA = 0
    
    
    Exit Sub

Trata_Erro:
    If Err.Number <> 35601 Then
        BG_LogFile_Erros Me.Name & "AcrescentaAnalise: " & Err.Description
    End If
    
    Resume Next


End Sub


' --------------------------------------------------------

' ALTERA ENTIDADE DE UMA ANALISE NO RECIBO MANUAL

' --------------------------------------------------------
Private Sub AlteraEntidadeReciboManual(linha As Long, codEntidade As String, DescrEFR As String, CodPai As String, linhaAna As Long)
    On Error GoTo TrataErro
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    Dim taxa As String
    Dim taxaEfr As String
    Dim valorUnitEFR As Double
    Dim mens As String
    Dim codigo As String
    Dim flg_FdsValFixo As Integer
    'codigo = RegistosA(linhaAna).codAna
    
    
    If linha > 0 Then
        If RegistosRM(linha).flgCodBarras = 1 Then
            Exit Sub
        End If
        If EliminaReciboManual(linha, RegistosRM(linha).ReciboCodFacturavel, False) = False Then
            Exit Sub
        End If
    End If
    
    If codEntidade <> "" Then
        If RegistosRM.Count = 0 Or linha = 0 Then
            'linha = RegistosRM.Count
            CriaNovaClasse RegistosRM, linha, "REC_MAN"
        End If
        If RegistosRM(linha).ReciboEntidade = "" And linhaAna > mediComboValorNull Then
            RegistosRM(linha).ReciboEntidade = codEntidade
            RegistosRM(linha).ReciboCodEfrEnvio = codEntidade
            RegistosRM(linha).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", codEntidade)
            RegistosRM(linha).ReciboQuantidade = 1
            RegistosRM(linha).ReciboQuantidadeEFR = RegistosRM(linha).ReciboQuantidade
            RegistosRM(linha).ReciboNumDoc = "0"
            RegistosRM(linha).ReciboSerieDoc = "0"
            RegistosRM(linha).ReciboCodFacturavel = RegistosA(linhaAna).codAna
            RegistosRM(linha).ReciboFlgAdicionado = "0"
            RegistosRM(linha).ReciboOrdemMarcacao = 0
            If linha = 1 Then
                RegistosRM(linha).ReciboP1 = 1
                RegistosRM(linha).ReciboCodMedico = ""
            ElseIf linha > 1 Then
                RegistosRM(linha).ReciboP1 = 1
                RegistosRM(linha).ReciboCodMedico = ""
            End If
            RegistosRM(linha).ReciboFlgAdicionado = "0"
            RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento
        Else
            RegistosRM(linha).ReciboEntidade = codEntidade
            RegistosRM(linha).ReciboCodEfrEnvio = codEntidade
            RegistosRM(linha).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", codEntidade)
            If RegistosRM(linha).ReciboOrdemMarcacao = 0 Then
                RegistosRM(linha).ReciboOrdemMarcacao = 0
            End If
            
        End If
        ' RECALCULA A TAXA
        codAnaEfr = ""
        If RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento Then
            taxa = IF_RetornaTaxaAnalise(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, CbTipoUtente, EcUtente, BL_HandleNull(EcDataChegada, Bg_DaData_ADO), codAnaEfr, RegistosRM(linha).ReciboDescrAnaEFR, False)
            
            taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, _
                                         EcDataPrevista, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, RegistosRM(linha).ReciboQuantidadeEFR, "", False, descrAnaEfr, mediComboValorNull)
            RegistosRM(linha).ReciboValorUnitEFR = valorUnitEFR
            RegistosRM(linha).ReciboCodAnaEFR = codAnaEfr
            RegistosRM(linha).ReciboDescrAnaEFR = descrAnaEfr
            
        Else
            taxa = 0
        End If
        Select Case taxa
            Case -1
                mens = " An�lise n�o est� mapeada para o FACTUS!"
                taxa = 0
            Case -2
                mens = "Impossivel abrir conex�o com o FACTUS!"
                taxa = 0
            Case -3
                mens = "Impossivel seleccionar portaria activa para a entidade em causa!"
                taxa = 0
            Case -4
                mens = "N�o existe taxa codificada para a an�lise " & RegistosRM(linha).ReciboCodFacturavel & "!"
                taxa = 0
            Case Else
                mens = ""
        End Select
        taxa = BG_CvDecimalParaCalculo(taxa)
        
        RegistosRM(linha).ReciboCodAnaEFR = codAnaEfr
        If mens <> "" Then
            If codEntidade <> gCodEFRSemCredencial Then
                BG_Mensagem mediMsgBox, mens, vbOKOnly + vbInformation, "Taxas"
            End If
            RegistosRM(linha).ReciboTaxa = "0,0"
        Else
            RegistosRM(linha).ReciboTaxa = taxa
        End If
        
        RegistosRM(linha).ReciboQuantidade = BL_HandleNull(RetornaQtd(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade), 1)
        RegistosRM(linha).ReciboQuantidadeEFR = RegistosRM(linha).ReciboQuantidade
        RegistosRM(linha).ReciboFDS = BL_HandleNull(EcFimSemana, "0")
        RegistosRM(linha).ReciboCodigoPai = CodPai
        RegistosRM(linha).ReciboTaxaUnitario = RegistosRM(linha).ReciboTaxa
        RegistosRM(linha).ReciboTaxa = Replace(RegistosRM(linha).ReciboTaxaUnitario, ".", ",") * RegistosRM(linha).ReciboQuantidade
        RegistosRM(linha).ReciboTaxa = Round(RegistosRM(linha).ReciboTaxa, 2)
        
        RegistosRM(linha).ReciboValorEFR = Replace(RegistosRM(linha).ReciboValorUnitEFR, ".", ",") * RegistosRM(linha).ReciboQuantidadeEFR
        RegistosRM(linha).ReciboValorEFR = Round(RegistosRM(linha).ReciboValorEFR, 2)
        RegistosRM(linha).flgCodBarras = 0
        
        'REQUISI��ES DE FIM DE SEMANA DOBRA O PRE�O
        'SE ENTIDADE COM VALOR FIXO PARA FINS DE SEMANA - COLOCA A ZERO
        flg_FdsValFixo = VerificaFdsValFixo(linha)
        If RegistosRM(linha).ReciboFDS = mediSim And flg_FdsValFixo = 0 Then
            RegistosRM(linha).ReciboTaxa = RegistosRM(linha).ReciboTaxa * 2
        ElseIf RegistosRM(linha).ReciboFDS = mediSim And flg_FdsValFixo = 1 Then
            RegistosRM(linha).ReciboTaxa = "0"
        End If
        
        AdicionaAnaliseAoReciboManual linha, False
        If linhaAna > 0 Then
            FGAna.TextMatrix(linhaAna, lColFgAnaP1) = RegistosRM(linha).ReciboP1
            FGAna.TextMatrix(linhaAna, lColFgAnaCodEFR) = RegistosRM(linha).ReciboEntidade
            FGAna.TextMatrix(linhaAna, lColFgAnaDescrEFR) = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(linha).ReciboEntidade)
            FGAna.TextMatrix(linhaAna, lColFgAnaPreco) = RegistosRM(linha).ReciboTaxa
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "AlteraEntidadeReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "AlteraEntidadeReciboManual"
    Exit Sub
    Resume Next
End Sub

Private Function LerEtiqInI() As Boolean
    Dim aux As String
    On Error GoTo erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Etiq.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Etiq.ini"
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqInI = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While Mid(aux, 1, 1) <> "P"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqInI = True
Exit Function
erro:
    If Err.Number = 55 Then
        Close 1
        Open s For Input As 1
        Resume Next
    End If
    BG_LogFile_Erros "Erro ao abrir Ficheiro:" & s & " " & Err.Description, Me.Name, "LerEtiqIni", True
    Exit Function
    Resume Next
End Function
Private Function LerEtiqIni_fim() As Boolean
    Dim aux As String
    On Error GoTo erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Etiq_fim.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Etiq_fim.ini"
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqIni_fim = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While Mid(aux, 1, 1) <> "P"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqIni_fim = True
    Exit Function
erro:
End Function
Private Function TrocaBinarios(ByVal s As String) As String
    On Error GoTo TrataErro
    
    Dim p As Long
    Dim k As Byte
    
    Do
        p = InStr(s, "&H")
        If p = 0 Then Exit Do
        k = val(Mid(s, p, 4))
        s = left(s, p - 1) + Chr(k) + Mid(s, p + 4)
    Loop
    TrocaBinarios = s
Exit Function
TrataErro:
    BG_LogFile_Erros "TrocaBinarios: " & Err.Number & " - " & Err.Description, Me.Name, "TrocaBinarios"
    Exit Function
    Resume Next
End Function

Private Function EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    On Error GoTo TrataErro
    
    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function
    
    MyDocInfo.pDocName = "Etiquetas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function
    
    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    sWrittenData = TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    
    EtiqOpenPrinter = True
Exit Function
TrataErro:
    BG_LogFile_Erros "EtiqOpenPrinter: " & Err.Number & " - " & Err.Description, Me.Name, "EtiqOpenPrinter"
    Exit Function
    Resume Next
End Function

Private Function EtiqClosePrinter() As Boolean
    On Error GoTo TrataErro
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    EtiqClosePrinter = True
Exit Function
TrataErro:
    BG_LogFile_Erros "EtiqClosePrinter: " & Err.Number & " - " & Err.Description, Me.Name, "EtiqClosePrinter"
    Exit Function
    Resume Next
End Function

Private Sub EcNome_LostFocus()
     On Error GoTo TrataErro
   
    If gNomeCapitalizado = 1 Then
        EcNome.text = StrConv(EcNome.text, vbProperCase)
    Else
        EcNome.text = UCase(EcNome.text)
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNome_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcNome_LostFocus"
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------

' VERIFICA SE ANALISE JA FOI MARCADA NOUTRA REQUUISICAO NO PROPRIO DIA

' --------------------------------------------------------
Private Function VerificaAnaliseJaMarcada(n_req As String, codAgrup As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim data As Integer
    Dim Data2 As String
    data = -1
    sSql = "SELECT prazo_Val FROM slv_analises WHERE cod_ana = " & BL_TrataStringParaBD(codAgrup) & " AND prazo_val IS NOT NULL "
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        data = BL_HandleNull(rsAna!prazo_val, 0)
    End If
    rsAna.Close
    If data = -1 Then
        data = 0
    End If
    Data2 = CStr(CDate(Bg_DaData_ADO) - data)
    sSql = "SELECT distinct x2.n_Req FROM  sl_requis x2, sl_marcacoes x3 WHERE x2.dt_previ >= " & BL_TrataDataParaBD(Data2)
    sSql = sSql & " AND x2.dt_previ <= " & BL_TrataStringParaBD(Bg_DaData_ADO)
    sSql = sSql & " AND x2.seq_utente = " & EcSeqUtente & " AND x2.n_Req = x3.n_req   AND x3.cod_agrup = " & BL_TrataStringParaBD(codAgrup)
    If n_req <> "" Then
        sSql = sSql & " AND x2.n_req <> " & n_req
    End If
    sSql = sSql & " UNION SELECT distinct x2.n_Req FROM  sl_requis x2, sl_realiza x3 WHERE x2.dt_previ >= " & BL_TrataDataParaBD(Data2)
    sSql = sSql & " AND x2.dt_previ <= " & BL_TrataStringParaBD(Bg_DaData_ADO)
    sSql = sSql & " AND x2.seq_utente = " & EcSeqUtente & " AND x2.n_Req = x3.n_req AND x3.cod_agrup = " & BL_TrataStringParaBD(codAgrup)
    If n_req <> "" Then
        sSql = sSql & " AND x2.n_req <> " & n_req
    End If
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        VerificaAnaliseJaMarcada = True
        MsgBox "A an�lise j� foi marcada  para o Utente na requisi��o:" & rsAna!n_req & " !"
    Else
        VerificaAnaliseJaMarcada = False
    End If
    rsAna.Close
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaAnaliseJaMarcada: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaAnaliseJaMarcada"
    Exit Function
    Resume Next
End Function


' --------------------------------------------------------

' ADICIONA ANALISE AO RECIBO MANUAL

' --------------------------------------------------------
Private Function AdicionaReciboManual(ByVal flg_manual As Integer, ByVal codEfr As String, ByVal nRec As String, ByVal sRec As String, ByVal CodFacturavel As String, _
                                 ByVal quantidade As Integer, ByVal CodigoPai As String, ByVal codAna As String, ByVal codBarras As Boolean) As Integer
    On Error GoTo TrataErro
    Dim anaFacturar As String
    Dim taxa As String
    Dim taxaEfr As String
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    Dim mens As String
    Dim indice As Long
    Dim flg_FdsValFixo As Integer
    Dim AnaRegra As String
    Dim p1 As String
    Dim corP1 As String
    Dim valorUnitEFR As Double
    Dim iAux As Integer
    If CodFacturavel = "" Then Exit Function
    indice = BL_HandleNull(RegistosRM.Count, 0)
    If indice = 0 Then
        CriaNovaClasse RegistosRM, 1, "REC_MAN"
        indice = 1
    End If
    ' -----------------------------------------
    ' ENTIDADE E NUMERO DE DOCUMENTO
    ' -----------------------------------------
    RegistosRM(indice).ReciboEntidade = codEfr
    RegistosRM(indice).ReciboCodEfrEnvio = codEfr
    RegistosRM(indice).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", codEfr)
    RegistosRM(indice).ReciboNumDoc = nRec
    RegistosRM(indice).ReciboSerieDoc = sRec
    RegistosRM(indice).ReciboCodAna = BL_HandleNull(codAna, CodFacturavel)
    RegistosRM(indice).ReciboCodFacturavel = BL_HandleNull(UCase(CodFacturavel), "0")
    If VerificaCodFacturavel(RegistosRM(indice).ReciboCodFacturavel) = False Then
        RegistosRM(indice).ReciboCodFacturavel = codAna
    End If
    RegistosRM(indice).ReciboFlgAdicionado = "0"
    RegistosRM(indice).ReciboAnaNaoFact = IF_Verifica_Regra_PRIVADO(RegistosRM(indice).ReciboCodFacturavel, codEfr)
    anaFacturar = IF_VerificaFacturarNovaAnalise(RegistosRM(indice).ReciboCodFacturavel, codEfr)
    If anaFacturar <> "" Then
        gMsgTitulo = "Inserir"
        gMsgMsg = "Quer facturar a an�lise: " & BL_SelCodigo("SLV_ANALISES_FACTUS", "DESCR_ANA", "COD_ANA", anaFacturar) & "?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
        If gMsgResp = vbYes Then
            CriaNovaClasse RegistosRM, indice, "REC_MAN"
            AdicionaReciboManual 1, codEfr, "0", "0", anaFacturar, 1, RegistosRM(indice).ReciboCodAna, anaFacturar, codBarras
        End If
    End If

    
    ' -----------------------------------------
    ' TAXA
    ' -----------------------------------------
    codAnaEfr = ""
    descrAnaEfr = ""
    taxa = 0
    taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade, _
                                 EcDataPrevista, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, quantidade, "", False, descrAnaEfr, mediComboValorNull)
    RegistosRM(indice).ReciboValorUnitEFR = valorUnitEFR
    taxaEfr = Replace(taxaEfr, ".", ",")
    taxa = BG_CvDecimalParaCalculo(taxa)
    RegistosRM(indice).ReciboCodAnaEFR = codAnaEfr
    RegistosRM(indice).ReciboDescrAnaEFR = descrAnaEfr
    RegistosRM(indice).ReciboTaxa = taxa
    taxaEfr = BG_CvDecimalParaCalculo(taxaEfr)
    
    RegistosRM(indice).ReciboFDS = BL_HandleNull(EcFimSemana, "0")
    RegistosRM(indice).ReciboCodigoPai = BL_HandleNull(CodigoPai, "")
    If quantidade <= 1 Then
        RegistosRM(indice).ReciboQuantidade = BL_HandleNull(RetornaQtd(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade), 1)
    Else
        RegistosRM(indice).ReciboQuantidade = quantidade
    End If
    RegistosRM(indice).ReciboQuantidadeEFR = RegistosRM(indice).ReciboQuantidade
    RegistosRM(indice).ReciboTaxaUnitario = RegistosRM(indice).ReciboTaxa
    RegistosRM(indice).ReciboTaxa = RegistosRM(indice).ReciboTaxaUnitario * RegistosRM(indice).ReciboQuantidade
    RegistosRM(indice).ReciboValorEFR = RegistosRM(indice).ReciboValorUnitEFR * RegistosRM(indice).ReciboQuantidadeEFR
    
    RegistosRM(indice).ReciboTaxa = Round(RegistosRM(indice).ReciboTaxa, 2)
    RegistosRM(indice).ReciboFlgReciboManual = flg_manual
    RegistosRM(indice).ReciboFlgMarcacaoRetirada = "0"
    
    ' -----------------------------------------
    ' VERIFICA SE APLICA A REGRA
    ' -----------------------------------------
    AnaRegra = ""
    AnaRegra = VerificaRegra(indice)
    If AnaRegra <> "" Then
        RegistosRM(indice).ReciboTaxa = "0,0"
        RegistosRM(indice).ReciboFlgRetirado = "1"
        BG_Mensagem mediMsgBox, "An�lise gratuita. An�lise " & AnaRegra & " marcada.", vbInformation, "An�lise"
    End If
    
    ' -----------------------------------------
    ' P1
    ' -----------------------------------------
    If indice = 1 Then
        p1 = 1
        corP1 = "B"
        RegistosRM(indice).ReciboCodMedico = ""
        RegistosRM(indice).ReciboNrBenef = BL_HandleNull(EcNumBenef, "")
    ElseIf indice > 1 Then
        If flg_manual = 0 Then
            If FGAna.row > 1 Then
                iAux = -1
                iAux = DevIndice(FGAna.TextMatrix(FGAna.row - 1, 0))
                If iAux > -1 Then
                    p1 = BL_HandleNull(Trim(RegistosRM(iAux).ReciboP1), "-1")
                    If Flg_IncrementaP1 = True Then
                        If IsNumeric(p1) Then
                            p1 = p1 + 1
                            corP1 = "B"
                        End If
                    End If
                End If
            End If
        Else
            p1 = BL_HandleNull(Trim(RegistosRM(indice - 1).ReciboP1), "-1")
            corP1 = BL_HandleNull(RegistosRM(indice - 1).ReciboCorP1, "B")
        End If
        If p1 = "" Then
            If indice > 1 Then
                p1 = BL_HandleNull(Trim(RegistosRM(indice - 1).ReciboP1), "1")
            End If
        End If
                
                
        RegistosRM(indice).ReciboCodMedico = RegistosRM(indice - 1).ReciboCodMedico
        RegistosRM(indice).ReciboNrBenef = RegistosRM(indice - 1).ReciboNrBenef
    End If
    RegistosRM(indice).ReciboP1 = p1
    RegistosRM(indice).ReciboCorP1 = corP1
    If codBarras = True Then
        RegistosRM(indice).flgCodBarras = 1
        If EcCredAct <> "" Then
            RegistosRM(indice).ReciboP1 = EcCredAct
        End If
        If EcBenefAct <> "" Then
            RegistosRM(indice).ReciboNrBenef = EcBenefAct
        End If
        If EcMedAct <> "" Then
            RegistosRM(indice).ReciboCodMedico = EcMedAct
        End If
    Else
        RegistosRM(indice).flgCodBarras = 0
    End If
    'ORDEM DE MARCACAO
    RegistosRM(indice).ReciboOrdemMarcacao = RetornaOrdemP1(indice)
    ' -----------------------------------------
    ' PERFIL MARCACAO - GUARDADO NA TABELA SL_RECIBOS_DET
    ' -----------------------------------------
    RegistosRM(indice).ReciboFlgAdicionado = "0"
    'soliveira lacto **
    If BG_DaComboSel(CbTipoIsencao) = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "Deve indicar primeiro o tipo de isen��o!", vbInformation, "Tipo de Isen��o"
        SSTGestReq.Tab = 0
        Exit Function
    End If
    RegistosRM(indice).ReciboIsencao = BG_DaComboSel(CbTipoIsencao)
    'RegistosRM(indice).indiceAna = indiceAna
        
    ' -----------------------------------------
    ' ADICIONA ANALISE � ESTRUTURA DE RECIBOS SE NAO FOR ISENTO
    ' -----------------------------------------
    If RegistosRM(indice).ReciboEntidade <> mediComboValorNull Then
        AdicionaAnaliseAoReciboManual indice, False
    End If
    If CbEFR.ListIndex > mediComboValorNull Then
        PreencheFgAnaEFR indice, CbEFR.ItemData(CbEFR.ListIndex)
    End If
    AdicionaReciboManual = indice
    CriaNovaClasse RegistosRM, indice, "REC_MAN"
Exit Function
TrataErro:
    AdicionaReciboManual = -1
    BG_LogFile_Erros "AdicionaReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "AdicionaReciboManual"
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------

' ADICIONA ANALISE AO RECIBO MANUAL

' --------------------------------------------------------
Private Function VerificaRegra(indice As Long) As String
    On Error GoTo TrataErro
    Dim i As Long
    Dim iRec As Long
    
    VerificaRegra = ""
    
    For i = 1 To RegistosA.Count
        iRec = DevIndice(RegistosA(i).codAna)
        If RegistosA(i).codAna <> "" And iRec > mediComboValorNull Then
        
            If RegistosA(i).codAna = RegistosRM(indice).ReciboAnaNaoFact Then
                If RegistosRM(iRec).ReciboIsencao = gTipoIsencaoNaoIsento And RegistosRM(iRec).ReciboEntidade = RegistosRM(iRec).ReciboEntidade Then
                    VerificaRegra = RegistosA(i).descrAna
                End If
            End If
            
            If RegistosRM(indice).codAna = RegistosRM(iRec).ReciboAnaNaoFact Then
                If RegistosRM(iRec).ReciboIsencao = gTipoIsencaoNaoIsento And RegistosRM(iRec).ReciboEntidade = RegistosA(i).ReciboEntidade Then
                    If EliminaReciboManual(iRec, RegistosA(i).codAna, False) = True Then
                        RegistosRM(iRec).ReciboTaxa = "0,0"
                        RegistosRM(iRec).ReciboFlgRetirado = "1"
                        FGAna.TextMatrix(i, lColFgAnaPreco) = "0,0"
                        BG_Mensagem mediMsgBox, "An�lise gratuita. An�lise " & RegistosA(indice).descrAna & " marcada.", vbInformation, "An�lise"
                        AdicionaAnaliseAoReciboManual iRec, False
                    End If
                End If
            End If
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaRegra: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaRegra"
    VerificaRegra = ""
    Exit Function
    Resume Next
End Function


Private Function VerificaRegraSexoMarcar(codAgrup As String, seq_utente As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAux As New ADODB.recordset
    Dim Sexo As String
    
    If Mid(codAgrup, 1, 1) = "S" Then
        sSql = "SELECT flg_sexo FROM sl_ana_s where cod_ana_S = " & BL_TrataStringParaBD(codAgrup)
    ElseIf Mid(codAgrup, 1, 1) = "C" Then
        sSql = "SELECT flg_sexo FROM sl_ana_c where cod_ana_c = " & BL_TrataStringParaBD(codAgrup)
    ElseIf Mid(codAgrup, 1, 1) = "P" Then
        sSql = "SELECT flg_sexo FROM sl_perfis where cod_perfis = " & BL_TrataStringParaBD(codAgrup)
    End If
    rsAux.CursorType = adOpenStatic
    rsAux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAux.Open sSql, gConexao
    If rsAux.RecordCount > 0 Then
        If BL_HandleNull(rsAux!flg_sexo, "") <> "" Then
            Sexo = rsAux!flg_sexo
            rsAux.Close
            
            sSql = "SELECT sexo_ute FROM sl_identif where seq_utente = " & seq_utente
            rsAux.CursorType = adOpenStatic
            rsAux.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAux.Open sSql, gConexao
            If rsAux.RecordCount > 0 Then
                If BL_HandleNull(rsAux!sexo_ute, "") = Sexo Then
                    rsAux.Close
                    VerificaRegraSexoMarcar = True
                    Exit Function
                Else
                    rsAux.Close
                    VerificaRegraSexoMarcar = False
                    Exit Function
                End If
            Else
                rsAux.Close
                VerificaRegraSexoMarcar = False
                Exit Function
            End If
            
        Else
            rsAux.Close
            VerificaRegraSexoMarcar = True
            Exit Function
        End If
    
    End If
    Set rsAux = Nothing
 Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaRegraSexoMarcar: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaRegraSexoMarcar"
    VerificaRegraSexoMarcar = False
    Exit Function
    Resume Next
   
End Function

Public Sub ImprimeRecibo(descricao As String)
    On Error GoTo TrataErro
        RECIBO_ImprimeRecibo descricao, EcNumReq, RegistosR(FGRecibos.row).NumDoc, RegistosR(FGRecibos.row).SerieDoc, _
                             RegistosR(FGRecibos.row).codEntidade, EcPrinterRecibo, RegistosR(FGRecibos.row).SeqUtente
        
BL_MudaCursorRato mediMP_Activo, Me
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeRecibo: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeRecibo"
    Exit Sub
    Resume Next
End Sub

Sub VerificaUtentePagaActoMedico(codEfr As String, serieRec As String, NumDoc As String)
    On Error GoTo TrataErro
    Dim RsAM As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    Dim Flg_ExisteActoMed As Boolean
    
    Set RsAM = New ADODB.recordset
    RsAM.CursorLocation = adUseServer
    RsAM.CursorType = adOpenStatic
    sql = "select flg_acto_med_ute from sl_efr where cod_efr = " & Trim(codEfr)
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsAM.Open sql, gConexao
    If RsAM.RecordCount > 0 Then
        If BL_HandleNull(RsAM!flg_acto_med_ute, 0) = 1 Then
            For i = 1 To RegistosRM.Count
                If Trim(RegistosRM(i).ReciboCodFacturavel) <> "" Then
                    If Trim(RegistosRM(i).ReciboEntidade) = codEfr And Trim(RegistosRM(i).ReciboSerieDoc) = serieRec And Trim(RegistosRM(i).ReciboNumDoc) = NumDoc And RegistosRM(i).ReciboCodFacturavel = GCodAnaActMed Then
                        Flg_ExisteActoMed = True
                    End If
                End If
            Next i
            
            If Flg_ExisteActoMed = False Then
                AdicionaReciboManual 1, codEfr, NumDoc, serieRec, GCodAnaActMed, 1, "", GCodAnaActMed, False
                
            End If
        End If
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "VerificaUtentePagaActoMedico: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaUtentePagaActoMedico"
    Exit Sub
    Resume Next
End Sub


Private Function VerificaObrigaNrBenef(codEfr As String) As Boolean
    On Error GoTo TrataErro
    Dim RsAM As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    Dim Flg_ExisteActoMed As Boolean
    
    Set RsAM = New ADODB.recordset
    RsAM.CursorLocation = adUseServer
    RsAM.CursorType = adOpenStatic
    sql = "select FLG_OBRIGA_NR_BENEF from sl_efr where cod_efr = " & Trim(codEfr)
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsAM.Open sql, gConexao
    If RsAM.RecordCount > 0 Then
        If BL_HandleNull(RsAM!FLG_OBRIGA_NR_BENEF, 0) = 1 Then
            VerificaObrigaNrBenef = True
        Else
            VerificaObrigaNrBenef = False
        End If
    Else
        VerificaObrigaNrBenef = False
    End If
    RsAM.Close
 Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaObrigaNrBenef: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaObrigaNrBenef"
    VerificaObrigaNrBenef = False
    Exit Function
    Resume Next
End Function

Function VerificaCodFacturavel(CodFacturavel As String) As Boolean
    On Error GoTo TrataErro
    Dim RsVerf As ADODB.recordset
    Dim sql As String
    Set RsVerf = New ADODB.recordset
    RsVerf.CursorLocation = adUseServer
    RsVerf.CursorType = adOpenStatic
    sql = "select * from sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(CodFacturavel)
    sql = sql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, Bg_DaData_ADO)) & " BETWEEN "
    sql = sql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsVerf.Open sql, gConexao
    If RsVerf.RecordCount = 0 Then
        VerificaCodFacturavel = False
    Else
        VerificaCodFacturavel = True
    End If
    RsVerf.Close
    Set RsVerf = Nothing
 Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaCodFacturavel: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaCodFacturavel"
    VerificaCodFacturavel = False
    Exit Function
    Resume Next
End Function


' -------------------------------------------------------------------

' VERIFICA SE CODIGO � CODIGO PAI DE ALGUMA ANALISE DO RECIBO MANUAL

' -------------------------------------------------------------------
Private Sub VerificaEFRCodigoFilho(CodPai As String, cod_efr As String, descr_efr As String, linhaAna As Long)
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodigoPai = CodPai Then
            AlteraEntidadeReciboManual i, cod_efr, descr_efr, CodPai, linhaAna
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "erro ao verificar codigos pai: " & Err.Number & " - " & Err.Description, Me.Name, "VerificacodigoPai"
    Exit Sub
    Resume Next
End Sub


' -------------------------------------------------------------------

' VERIFICA SE CODIGO � CODIGO PAI DE ALGUMA ANALISE DO RECIBO MANUAL

' -------------------------------------------------------------------
Private Sub EliminaCodigoFilho(CodPai As String)
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodigoPai = CodPai Then
            EliminaReciboManual i, RegistosRM(i).ReciboCodFacturavel, True
            EliminaFgAnaEfr i
            LimpaColeccao RegistosRM, i
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "erro ao Eliminar codigos filhos: " & Err.Number & " - " & Err.Description, Me.Name, "EliminaCodigoFilho"
    Exit Sub
    Resume Next
End Sub


Sub ActualizaFlgAparTrans()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim RsTrans As ADODB.recordset
    Dim sql As String
    
    For i = 1 To UBound(MaReq)
        Set RsTrans = New ADODB.recordset
        RsTrans.CursorLocation = adUseServer
        RsTrans.CursorType = adOpenStatic
        sql = "SELECT flg_apar_trans,n_folha_trab FROM sl_marcacoes WHERE n_req = " & EcNumReq.text & " and cod_perfil = " & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & " And cod_ana_c = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & " and cod_ana_s= " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsTrans.Open sql, gConexao
        If Not RsTrans.EOF Then
            MaReq(i).Flg_Apar_Transf = BL_HandleNull(RsTrans!flg_apar_trans, 0)
            MaReq(i).N_Folha_Trab = BL_HandleNull(RsTrans!N_Folha_Trab, 0)
        Else
            MaReq(i).Flg_Apar_Transf = 0
            MaReq(i).N_Folha_Trab = 0
        End If
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "ActualizaFlgAparTrans: " & Err.Number & " - " & Err.Description, Me.Name, "ActualizaFlgAparTrans"
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------------------------------------

' RETORNA QUANTIDADE DE MAPEADA PARA FACTURACAO

' -----------------------------------------------------------------------------------------------

Public Function RetornaQtd(codAna As String, cod_efr As String) As String
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
        sSql = "SELECT qtd FROM sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(codAna) & " AND cod_efr = " & cod_efr
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, Bg_DaData_ADO)) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            RetornaQtd = BL_HandleNull(rsAna!qtd, "")
        Else
            rsAna.Close
            sSql = "SELECT qtd FROM sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(codAna) & " AND cod_efr IS NULL "
            sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, Bg_DaData_ADO)) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                RetornaQtd = BL_HandleNull(rsAna!qtd, "")
            End If
        End If
        rsAna.Close
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "RetornaQtd"
    RetornaQtd = ""
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' RETORNA A COLUNA ACTUAL

' ----------------------------------------------------------------------
Private Sub FgTubos_DevolveColunaActual(X As Single)
    Dim coluna As Integer
    For coluna = 0 To FGTubos.Cols - 1
        If FGTubos.ColPos(coluna) <= X And FGTubos.ColPos(coluna) + FGTubos.ColWidth(coluna) >= X Then
            TuboColunaActual = coluna
            Exit Sub
        End If
    Next
    TuboColunaActual = -1
End Sub


' ----------------------------------------------------------------------

' RETORNA A LINHA ACTUAL

' ----------------------------------------------------------------------
Private Sub FgTubos_DevolveLinhaActual(Y As Single)
    Dim linha As Integer
        For linha = 0 To FGTubos.rows - 1
            If FGTubos.RowPos(linha) <= Y And FGTubos.RowPos(linha) + FGTubos.RowHeight(linha) >= Y Then
                TuboLinhaActual = linha
                Exit Sub
            End If
        Next
        TuboLinhaActual = -1
End Sub

' ----------------------------------------------------------------------

' PARA CADA ANALISE MARCADA NO TOP, REGISTA-A!

' ----------------------------------------------------------------------
Public Sub PreencheTopAna()
    Dim CamposRetorno As New ClassPesqResultados
    Dim cancelou As Boolean
    Dim total As Integer
    Dim Resultados(100) As Variant
    Dim i As Integer
    On Error GoTo TrataErro
    
    CamposRetorno.InicializaResultados 1
    FormMarcAnaTop.Inicializa CamposRetorno
    FormMarcAnaTop.Show vbModal
    CamposRetorno.RetornaResultados Resultados(), cancelou, total
    For i = 1 To total
        EcAuxAna.Enabled = True
        EcAuxAna = Resultados(i)
        EcAuxAna_KeyDown 13, 0
    Next i
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheTopAna: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheTopAna"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' VERIFICA SE ENTIDADE TEM VALOR FIXO PARA FINS DE SEMANA

' ----------------------------------------------------------------------
Private Function VerificaFdsValFixo(indice As Long) As Long
    Dim i As Long
    Dim sSql As String
    Dim RsEFR As New ADODB.recordset
    On Error GoTo TrataErro
    
    For i = 1 To RegistosR.Count
        If RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade And RegistosRM(indice).ReciboNumDoc = RegistosR(i).NumDoc And _
           RegistosRM(indice).ReciboSerieDoc = RegistosR(i).SerieDoc Then
           VerificaFdsValFixo = RegistosR(i).flg_FdsFixo
           Exit Function
        End If
    Next
    
    sSql = "SELECT * FROM sl_efr where cod_efr = " & RegistosRM(indice).ReciboEntidade
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEFR.Open sSql, gConexao
    If RsEFR.RecordCount > 0 Then
        VerificaFdsValFixo = BL_HandleNull(RsEFR!flg_fds_fixo, 0)
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaFdsValFixo: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaFdsValFixo"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' PREENCHE ALGUNS DADOS DA ENTIDADE COM APENAS UM ACESSO � BD

' ----------------------------------------------------------------------
Private Sub PreencheDadosEntidade(ByRef RegistosR As Collection, indice As Long)
    Dim sSql As String
    Dim RsEFR As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_efr where cod_efr = " & RegistosR(indice).codEntidade
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEFR.Open sSql, gConexao
    If RsEFR.RecordCount > 0 Then
        RegistosR(indice).DescrEntidade = BL_HandleNull(RsEFR!descr_efr, "")
        RegistosR(indice).codEmpresa = BL_HandleNull(RsEFR!cod_empresa, "")
        RegistosR(indice).flg_FdsFixo = BL_HandleNull(RsEFR!flg_fds_fixo, 0)
        RegistosR(indice).NumCopias = BL_HandleNull(RsEFR!num_copias_rec, 0)
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDadosEntidade: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDadosEntidade"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' RECALCULA VALOR DO RECIBO

' ----------------------------------------------------------------------
Public Sub RecalculaRecibo(indice As Long)
    On Error GoTo TrataErro
    Dim i As Long
    Dim taxa As Double
    If RegistosR(indice).Estado = gEstadoReciboNaoEmitido Or RegistosR(indice).Estado = gEstadoReciboNulo Or _
        RegistosR(indice).Estado = gEstadoReciboPerdido Then
        taxa = 0
        For i = 1 To RegistosRM.Count
            If RegistosRM(i).ReciboEntidade = RegistosR(indice).codEntidade And RegistosRM(i).ReciboNumDoc = RegistosR(indice).NumDoc And RegistosRM(i).ReciboSerieDoc = RegistosR(indice).SerieDoc Then
                taxa = taxa + RegistosRM(i).ReciboTaxa
            End If
        Next
        RegistosR(indice).ValorPagar = taxa
        RegistosR(indice).ValorOriginal = taxa
        FGRecibos.TextMatrix(indice, lColRecibosValorPagar) = taxa
        If RegistosR(indice).ValorPagar = 0 Then
            RegistosR(indice).Estado = gEstadoReciboNulo
            FGRecibos.TextMatrix(indice, lColRecibosEstado) = "Recibo Nulo"
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "RecalculaRecibo: " & Err.Number & " - " & Err.Description, Me.Name, "RecalculaRecibo"
    Exit Sub
    Resume Next
End Sub


Private Sub RegistaAlteracoesRequis(num_requis As String)
    Dim i As Integer
    Dim ValorControloNovo As String
    Dim seq_alteracao As Integer
    seq_alteracao = BL_ContaAltRequis(num_requis) + 1
    For i = 0 To NumCampos - 1
        ValorControloNovo = ""
        'TextBox, Label, MaskEdBox, RichTextBox
        If TypeOf CamposEc(i) Is TextBox Or _
           TypeOf CamposEc(i) Is Label Or _
           TypeOf CamposEc(i) Is MaskEdBox Or _
           TypeOf CamposEc(i) Is RichTextBox Then
           
            ValorControloNovo = CamposEc(i)
        'DataCombo
        ElseIf TypeOf CamposEc(i) Is DataCombo Then
            ValorControloNovo = BG_DataComboSel(CamposEc(i))
            If ValorControloNovo = mediComboValorNull Then
                ValorControloNovo = ""
            End If
        'ComboBox
        ElseIf TypeOf CamposEc(i) Is ComboBox Then
            ValorControloNovo = BG_DaComboSel(CamposEc(i))
            If ValorControloNovo = mediComboValorNull Then
                ValorControloNovo = ""
            End If
        'CheckBox
        ElseIf TypeOf CamposEc(i) Is CheckBox Then
            If CamposEc(i).value = 0 Then
                ValorControloNovo = "0"
            ElseIf CamposEc(i).value = 1 Then
                ValorControloNovo = "1"
            Else
                ValorControloNovo = ""
            End If
        Else
        End If
        
        If CamposEcBckp(i) <> ValorControloNovo Then
            BL_RegistaAltRequis num_requis, CamposBD(i), CamposEcBckp(i), ValorControloNovo, seq_alteracao
        End If
    Next
End Sub
Private Function VerificaAnaliseJaExisteRealiza(n_req As Long, Cod_Perfil As String, cod_ana_c As String, cod_ana_s As String, flg_apaga As Boolean) As Long
    Dim sSql As String
    Dim rsReal As New ADODB.recordset
    Dim RsRes As New ADODB.recordset
    On Error GoTo TrataErro
    
    VerificaAnaliseJaExisteRealiza = 0
    
    sSql = "SELECT seq_realiza FROM sl_realiza WHERE n_req = " & n_req & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil)
    sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c) & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    sSql = sSql & " UNION SELECT seq_realiza FROM sl_realiza_h WHERE n_req = " & n_req & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil)
    sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c) & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    rsReal.CursorLocation = adUseServer
    rsReal.CursorType = adOpenStatic
    If gModoDebug = mediSim Then
        BG_LogFile_Erros sSql
    End If
    rsReal.Open sSql, gConexao
    If rsReal.RecordCount > 0 Then
        VerificaAnaliseJaExisteRealiza = rsReal!seq_realiza
        sSql = "SELECT seq_Realiza FROM sl_res_alfan where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_frase where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_micro where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_alfan_h where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_frase_h where seq_realiza = " & rsReal!seq_realiza
        sSql = sSql & " UNION SELECT seq_Realiza FROM sl_res_micro_h where seq_realiza = " & rsReal!seq_realiza
        RsRes.CursorLocation = adUseServer
        RsRes.CursorType = adOpenStatic
        If gModoDebug = mediSim Then
            BG_LogFile_Erros sSql
        End If
        RsRes.Open sSql, gConexao
        If RsRes.RecordCount > 0 Or cod_ana_s = "S99999" Then
            VerificaAnaliseJaExisteRealiza = rsReal!seq_realiza
        Else
            If flg_apaga = True Then
                sSql = "DELETE FROM sl_realiza WHERE seq_realiza = " & rsReal!seq_realiza
                BG_ExecutaQuery_ADO sSql
            End If
            VerificaAnaliseJaExisteRealiza = 0
        End If
        RsRes.Close
        Set RsRes = Nothing
    Else
        VerificaAnaliseJaExisteRealiza = 0
    End If
    rsReal.Close
    Set rsReal = Nothing
    
Exit Function
TrataErro:
    VerificaAnaliseJaExisteRealiza = 0
    BG_LogFile_Erros "Erro  ao VerificaAnaliseJaExisteRealiza: " & Err.Description, Me.Name, "VerificaAnaliseJaExisteRealiza", True
    Exit Function
    Resume Next
End Function


Public Sub PreencheFgRecibos()
    Dim i As Long
    Dim j As Integer
    Dim cor  As Long
    Dim txtEstado As String
    
    ExecutaCodigoR = False
    j = FGRecibos.rows - 1
    While j > 0
        If j > 1 Then
            FGRecibos.RemoveItem j
        Else
            FGRecibos.TextMatrix(j, 0) = ""
            FGRecibos.TextMatrix(j, 1) = ""
            FGRecibos.TextMatrix(j, 2) = ""
            FGRecibos.TextMatrix(j, 3) = ""
            FGRecibos.TextMatrix(j, 4) = ""
            FGRecibos.TextMatrix(j, 5) = ""
            FGRecibos.TextMatrix(j, 6) = ""
            BL_MudaCorFg FGRecibos, CLng(j), vbWhite
        End If
        j = j - 1
    Wend
    
    ExecutaCodigoR = True
    For i = 1 To RegistosR.Count - 1
        If RegistosR(i).Estado = gEstadoReciboAnulado Then
            cor = vermelho
            txtEstado = "Anulado"
        ElseIf RegistosR(i).Estado = gEstadoReciboPago Then
            cor = Verde
            txtEstado = "Pago"
        ElseIf RegistosR(i).Estado = gEstadoReciboCobranca Then
            cor = Amarelo
            txtEstado = "Cobran�a"
        ElseIf RegistosR(i).Estado = gEstadoReciboPerdido Then
            cor = vbWhite
            txtEstado = "An�lises Perdidas"
        ElseIf RegistosR(i).Estado = gEstadoReciboNulo Then
            cor = vbWhite
            txtEstado = "Recibo Nulo"
        Else
            cor = vbWhite
            txtEstado = "N�o Emitido"
        End If
        If RegistosR(i).NumDoc <> "" Then
        ' -----------------------------------------------------------------------------------------------
            ' PREENCHCE A FLEXGRID
            ' -----------------------------------------------------------------------------------------------
            If BL_HandleNull(RegistosR(i).NumDoc, "0") <> "0" Then
                FGRecibos.TextMatrix(i, lColRecibosNumDoc) = RegistosR(i).SerieDoc & "/" & RegistosR(i).NumDoc
            Else
                FGRecibos.TextMatrix(i, lColRecibosNumDoc) = RegistosR(i).NumDoc
                RegistosR(i).CodFormaPag = gModoPagNaoPago
            End If
            FGRecibos.TextMatrix(i, lColRecibosEntidade) = RegistosR(i).DescrEntidade
            FGRecibos.TextMatrix(i, lColRecibosValorPagar) = RegistosR(i).ValorPagar
            FGRecibos.TextMatrix(i, lColRecibosPercentagem) = CDbl(100 - RegistosR(i).Desconto)
            FGRecibos.TextMatrix(i, lColRecibosDtEmissao) = RegistosR(i).DtEmi & " " & RegistosR(i).HrEmi
            FGRecibos.TextMatrix(i, lColRecibosDtPagamento) = RegistosR(i).DtPagamento & " " & RegistosR(i).HrPagamento
            FGRecibos.TextMatrix(i, lColRecibosEstado) = txtEstado
            If RegistosR(i).CodFormaPag = gModoPagNaoPago Then
                FGRecibos.TextMatrix(i, lColRecibosModoPag) = ""
            Else
                FGRecibos.TextMatrix(i, lColRecibosModoPag) = BL_SelCodigo("sl_forma_pag", "DESCR_FORMA_PAG", "COD_FORMA_PAG", RegistosR(i).CodFormaPag)
            End If
            BL_MudaCorFg FGRecibos, i, cor
            FGRecibos.AddItem ""
        End If
    Next

End Sub
Public Sub PreencheFgAdiantamentos()
    Dim i As Long
    Dim j As Integer
    Dim cor  As Long
    Dim txtEstado As String
    
    j = FgAdiantamentos.rows - 1
    While j > 0
        If j > 1 Then
            FgAdiantamentos.RemoveItem j
        Else
            For i = 0 To FgAdiantamentos.Cols - 1
                FgAdiantamentos.TextMatrix(j, i) = ""
            Next
            BL_MudaCorFg FgAdiantamentos, CLng(j), vbWhite
        End If
        j = j - 1
    Wend
    
    For i = 1 To RegistosAD.Count - 1
        If RegistosAD(i).Estado = gEstadoReciboAnulado Then
            cor = vermelho
            txtEstado = "Anulado"
        ElseIf RegistosAD(i).Estado = gEstadoReciboPago Then
            cor = Verde
            txtEstado = "Pago"
        ElseIf RegistosAD(i).Estado = gEstadoReciboCobranca Then
            cor = Amarelo
            txtEstado = "Cobran�a"
        ElseIf RegistosAD(i).Estado = gEstadoReciboPerdido Then
            cor = vbWhite
            txtEstado = "An�lises Perdidas"
        ElseIf RegistosAD(i).Estado = gEstadoReciboNulo Then
            cor = vbWhite
            txtEstado = "Recibo Nulo"
        Else
            cor = vbWhite
            txtEstado = "N�o Emitido"
        End If
        ' -----------------------------------------------------------------------------------------------
        ' PREENCHCE A FLEXGRID
        ' -----------------------------------------------------------------------------------------------
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosNumDoc) = RegistosAD(i).NumAdiantamento
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosDtEmissao) = RegistosAD(i).DtEmi & " " & RegistosAD(i).HrEmi
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosDtAnulacao) = RegistosAD(i).DtAnul & " " & RegistosAD(i).HrAnul
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosEstado) = txtEstado
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosValor) = RegistosAD(i).valor
        BL_MudaCorFg FgAdiantamentos, i, cor
        FgAdiantamentos.AddItem ""
    Next

End Sub

Private Sub GereFrameAdiantamentos(flg_Aberto As Boolean)
    If flg_Aberto = True Then
        FrameAdiantamentos.top = 360
        FrameAdiantamentos.Visible = True
    ElseIf flg_Aberto = False Then
        FrameAdiantamentos.Visible = False
    End If
End Sub


Private Sub VerificaDoenteHemodialise()
    Dim codAna As String
    Dim sSql As String
    Dim rsHemo As New ADODB.recordset
    Dim i As Integer
    Dim serieRec As String
    Dim numRec As String
    serieRec = "0"
    numRec = "0"
    If EcHemodialise <> "" Then
    
        sSql = "SELECT cod_ana FROM sl_cod_hemodialise WHERE cod_hemodialise = " & BL_TrataStringParaBD(EcHemodialise)
        rsHemo.CursorLocation = adUseServer
        rsHemo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsHemo.Open sSql, gConexao
        If rsHemo.RecordCount > 0 Then
            If BL_HandleNull(rsHemo!cod_ana, "") <> "" Then
                If VerificaCodAnaJaMarcadoRM(rsHemo!cod_ana) = False Then
                    For i = 1 To RegistosR.Count - 1
                        If RegistosR(i).Estado = gEstadoReciboNulo And RegistosR(i).codEntidade = EcCodEFR Then
                            numRec = RegistosR(i).NumDoc
                            serieRec = RegistosR(i).SerieDoc
                            Exit For
                        End If
                    Next
                    AdicionaReciboManual 1, RegistosR(i).codEntidade, numRec, serieRec, rsHemo!cod_ana, 1, "", rsHemo!cod_ana, False
                    
                End If
            End If
        
        End If
        rsHemo.Close
        Set rsHemo = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao VerificaDoenteHemodialise: " & Err.Description, Me.Name, "VerificaDoenteHemodialise", False
    Exit Sub
    Resume Next
End Sub

Private Function VerificaCodAnaJaMarcadoRM(cod_ana As String) As Boolean
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodFacturavel = cod_ana Then
            VerificaCodAnaJaMarcadoRM = True
            Exit Function
        End If
    Next
    VerificaCodAnaJaMarcadoRM = False
Exit Function
TrataErro:
    VerificaCodAnaJaMarcadoRM = False
    BG_LogFile_Erros "Erro  ao VerificaCodAnaJaMarcadoRM: " & Err.Description, Me.Name, "VerificaCodAnaJaMarcadoRM", False
    Exit Function
    Resume Next
End Function

Private Function VerificaEfrHemodialise(cod_efr As String) As Boolean
    Dim sSql As String
    Dim RsEFR As New ADODB.recordset
    On Error GoTo TrataErro
    VerificaEfrHemodialise = False
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    sSql = "select flg_hemodialise from sl_efr where cod_efr = " & Trim(cod_efr)
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEFR.Open sSql, gConexao
    If RsEFR.RecordCount > 0 Then
        If BL_HandleNull(RsEFR!flg_hemodialise, 0) = 1 Then
            VerificaEfrHemodialise = True
        End If
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Function
TrataErro:
    VerificaEfrHemodialise = False
    BG_LogFile_Erros "Erro  ao VerificaEfrHemodialise: " & Err.Description, Me.Name, "VerificaEfrHemodialise", False
    Exit Function
    Resume Next
End Function

Private Sub InicializaFrameAnaRecibos()
    EcCodAnaAcresc.left = FgAnaRec.left
    EcCodAnaAcresc.Width = FgAnaRec.ColWidth(lColFgAnaRecCodAna)
    EcDescrAnaAcresc.left = EcCodAnaAcresc.left + EcCodAnaAcresc.Width
    EcDescrAnaAcresc.Width = FgAnaRec.ColWidth(lColFgAnaRecDescrAna)
    EcQtdAnaAcresc.left = EcDescrAnaAcresc.left + EcDescrAnaAcresc.Width
    EcQtdAnaAcresc.Width = FgAnaRec.ColWidth(lColFgAnaRecQtd)
    EcTaxaAnaAcresc.left = EcQtdAnaAcresc.left + EcQtdAnaAcresc.Width
    EcTaxaAnaAcresc.Width = FgAnaRec.ColWidth(lColFgAnaRecTaxa)
    If RegistosR(FGRecibos.row).Estado = gEstadoReciboAnulado Or RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca Or _
       RegistosR(FGRecibos.row).Estado = gEstadoReciboPago Then
            BtAnaAcresc.Enabled = False
    Else
            BtAnaAcresc.Enabled = True
    End If
End Sub

Private Function RetornaPrecoAnaRec(cod_ana As String, cod_efr As String, qtd As Integer) As String
    Dim taxa As String
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    Dim mens As String
    On Error GoTo TrataErro
    
    taxa = IF_RetornaTaxaAnalise(cod_ana, cod_efr, CbTipoUtente, EcUtente, EcDataPrevista, codAnaEfr, descrAnaEfr, False)
    taxa = Replace(taxa, ".", ",")
    If Mid(taxa, 1, 1) = "," Then
        taxa = "0" & taxa
    End If
            
    Select Case taxa
        Case "-1"
            mens = " An�lise n�o est� mapeada para o FACTUS!"
            taxa = 0
        Case "-2"
            mens = "Impossivel abrir conex�o com o FACTUS!"
            taxa = 0
        Case "-3"
            mens = "Impossivel seleccionar portaria activa!"
            taxa = 0
        Case "-4"
            mens = "An�lise n�o comparticipada. Pre�o PARTICULAR!"
            
            taxa = 0
        Case "-5"
            mens = "N�o existe tabela codificada para a entidade em causa!"
            taxa = 0
        Case Else
            mens = ""
    End Select
    
    If mens <> "" Then
        taxa = "0,0"
    End If
    taxa = BG_CvDecimalParaCalculo(taxa) * qtd
    RetornaPrecoAnaRec = taxa
Exit Function
TrataErro:
    RetornaPrecoAnaRec = "0"
    BG_LogFile_Erros "Erro  ao RetornaPrecoAnaRec: " & Err.Description, Me.Name, "RetornaPrecoAnaRec", False
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------------

' COLOCA UM RECIBO EM COBRANCA/PAGO

' -------------------------------------------------------------------------
    
Private Sub ColocaCobrancaPago(linha As Integer, serieRec As String, nRec As String, tipo As Integer)
    Dim sSql As String
    On Error GoTo TrataErro
    If tipo = 0 Then ' COLOCAR EM PAGAMENTO
        BG_BeginTransaction
        sSql = "UPDATE sl_recibos set user_emi = " & BL_TrataStringParaBD(RegistosR(linha).UserEmi) & ","
        sSql = sSql & " dt_pagamento = " & BL_TrataDataParaBD(RegistosR(linha).DtPagamento) & ", "
        sSql = sSql & " hr_pagamento = " & BL_TrataStringParaBD(RegistosR(linha).HrPagamento) & ", "
        sSql = sSql & " estado ='P' WHERE serie_rec = " & BL_TrataStringParaBD(serieRec) & " AND n_rec = " & nRec

    ElseIf tipo = 1 Then
        sSql = "UPDATE sl_recibos set dt_pagamento = null, hr_pagamento = null, estado = 'C' WHERE serie_rec = " & BL_TrataStringParaBD(serieRec) & " AND n_rec = " & nRec
    End If
    BG_ExecutaQuery_ADO sSql
    BG_CommitTransaction
Exit Sub
TrataErro:
BG_RollbackTransaction
    BG_LogFile_Erros "Erro  ao ColocaCobrancapago: " & Err.Description, Me.Name, "ColocaCobrancapago", False
    Exit Sub
    Resume Next
End Sub


Private Sub FlgIncrementa(Ind As Long, qtd As Integer)
    Dim i As Integer
    Dim flg_FdsValFixo As Long
    On Error GoTo TrataErro
    
    For i = 1 To RegistosRM.Count
        If RegistosA(Ind).codAna = RegistosRM(i).ReciboCodAna Then
            Exit For
        End If
    Next
    EliminaReciboManual CLng(i), RegistosA(Ind).codAna, False
    RegistosRM(i).ReciboQuantidade = qtd
    RegistosRM(i).ReciboQuantidadeEFR = RegistosRM(i).ReciboQuantidade
    RegistosRM(i).ReciboTaxa = Replace(RegistosRM(i).ReciboTaxaUnitario, ".", ",") * qtd
    
    'PARA FIM DE SEMANA DOBRA O PRE�O
    'SE ENTIDADE COM VALOR FIXO PARA FINS DE SEMANA - COLOCA A ZERO
    flg_FdsValFixo = VerificaFdsValFixo(Ind)
    If RegistosRM(i).ReciboFDS = mediSim And flg_FdsValFixo = 0 Then
        RegistosRM(i).ReciboTaxa = RegistosRM(i).ReciboTaxa * 2
    ElseIf RegistosRM(Ind).ReciboFDS = mediSim And flg_FdsValFixo = 1 Then
        RegistosRM(i).ReciboTaxa = "0"
    End If
    
    FGAna.TextMatrix(Ind, lColFgAnaPreco) = RegistosRM(i).ReciboTaxa
    AdicionaAnaliseAoReciboManual CLng(i), False
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  aFlgIncrementa: " & Err.Description, Me.Name, "FlgIncrementa", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' PREENCHE GRELHA DE ANALISES COM INFORMACAO DO PRE�O ASSOCIADO

' -------------------------------------------------------------------------
Private Sub ActualizaLinhasRecibos(linha As Integer)
    Dim i As Integer
    Dim j As Integer
    On Error GoTo TrataErro
    
    If linha > 0 Then
        
        FGAna.TextMatrix(linha, lColFgAnaP1) = ""
        FGAna.TextMatrix(linha, lColFgAnaMedico) = ""
        FGAna.TextMatrix(linha, lColFgAnaCodEFR) = ""
        FGAna.TextMatrix(linha, lColFgAnaDescrEFR) = ""
        FGAna.TextMatrix(linha, lColFgAnaNrBenef) = ""
        FGAna.TextMatrix(linha, lColFgAnaPreco) = ""
        
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboCodAna = RegistosA(linha).codAna Then
                If RegistosRM(i).ReciboCodAna <> "" And RegistosRM(i).ReciboFlgReciboManual = 0 And RegistosRM(i).flgCodBarras = 0 Then
                    If RegistosRM(i).ReciboCorP1 = "V" Then
                        FGAna.row = linha
                        FGAna.Col = 2
                        FGAna.CellBackColor = Verde
                        FGAna.Col = 0
                    Else
                        FGAna.row = linha
                        FGAna.Col = 2
                        FGAna.CellBackColor = vbWhite
                        FGAna.Col = 0
                    End If
        
                    FGAna.TextMatrix(linha, lColFgAnaP1) = RegistosRM(i).ReciboP1
                    FGAna.TextMatrix(linha, lColFgAnaMedico) = RegistosRM(i).ReciboCodMedico
                    FGAna.TextMatrix(linha, lColFgAnaCodEFR) = RegistosRM(i).ReciboEntidade
                    FGAna.TextMatrix(linha, lColFgAnaDescrEFR) = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(i).ReciboEntidade)
                    FGAna.TextMatrix(linha, lColFgAnaNrBenef) = RegistosRM(i).ReciboNrBenef
                    FGAna.TextMatrix(linha, lColFgAnaPreco) = RegistosRM(i).ReciboTaxa
                    If RegistosRM(i).ReciboIsencao = gTipoIsencaoBorla Then
                        FGAna.Col = lColFgAnaPreco
                        FGAna.CellBackColor = CorBorla
                    ElseIf RegistosRM(i).ReciboIsencao = gTipoIsencaoIsento Then
                        FGAna.Col = lColFgAnaPreco
                        FGAna.CellBackColor = CorIsento
                    Else
                        FGAna.Col = lColFgAnaPreco
                        FGAna.CellBackColor = vbWhite
                    End If
                    FGAna.Col = lColFgAnaCodigo
                    Exit For
                End If
            End If
        Next
    Else
        For i = 1 To RegistosA.Count - 1
            FGAna.TextMatrix(i, lColFgAnaP1) = ""
            FGAna.TextMatrix(i, lColFgAnaMedico) = ""
            FGAna.TextMatrix(i, lColFgAnaCodEFR) = ""
            FGAna.TextMatrix(i, lColFgAnaDescrEFR) = ""
            FGAna.TextMatrix(i, lColFgAnaNrBenef) = ""
            FGAna.TextMatrix(i, lColFgAnaPreco) = ""
        Next
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboCodAna <> "" And RegistosRM(i).ReciboFlgReciboManual = 0 And RegistosRM(i).flgCodBarras = 0 Then
                For j = 1 To RegistosA.Count - 1
                    If RegistosRM(i).ReciboCodAna = RegistosA(j).codAna Then
                
                        If RegistosRM(i).ReciboCorP1 = "V" Then
                            FGAna.row = j
                            FGAna.Col = 2
                            FGAna.CellBackColor = Verde
                            FGAna.Col = 0
                        Else
                            FGAna.row = j
                            FGAna.Col = 2
                            FGAna.CellBackColor = vbWhite
                            FGAna.Col = 0
                        End If
            
                        FGAna.TextMatrix(j, lColFgAnaP1) = RegistosRM(i).ReciboP1
                        FGAna.TextMatrix(j, lColFgAnaMedico) = RegistosRM(i).ReciboCodMedico
                        FGAna.TextMatrix(j, lColFgAnaCodEFR) = RegistosRM(i).ReciboEntidade
                        FGAna.TextMatrix(j, lColFgAnaDescrEFR) = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(i).ReciboEntidade)
                        FGAna.TextMatrix(j, lColFgAnaNrBenef) = RegistosRM(i).ReciboNrBenef
                        FGAna.TextMatrix(j, lColFgAnaPreco) = RegistosRM(i).ReciboTaxa
                        If RegistosRM(i).ReciboIsencao = gTipoIsencaoBorla Then
                            FGAna.Col = lColFgAnaPreco
                            FGAna.CellBackColor = CorBorla
                        ElseIf RegistosRM(i).ReciboIsencao = gTipoIsencaoIsento Then
                            FGAna.Col = lColFgAnaPreco
                            FGAna.CellBackColor = CorIsento
                        Else
                            FGAna.Col = lColFgAnaPreco
                            FGAna.CellBackColor = vbWhite
                        End If
                        FGAna.Col = lColFgAnaCodigo
                        Exit For
                    End If
                Next
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ActualizarLinhasRecibos: " & Err.Description, Me.Name, "ActualizarLinhasRecibos", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' RETORNA O INDICE DA ESTRUTURA DE RECIBOS PARA ANALISE EM CAUSA

' -------------------------------------------------------------------------

Public Function DevIndice(codAna As String) As Long
    Dim i As Integer
    On Error GoTo TrataErro
    DevIndice = -1
    If codAna = "" Then Exit Function
    For i = 1 To RegistosRM.Count - 1
        If RegistosRM(i).ReciboCodFacturavel = codAna And BL_HandleNull(RegistosRM(i).flgCodBarras, 0) = 0 Then
            DevIndice = i
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    DevIndice = -1
    BG_LogFile_Erros "Erro  DevIndice: " & Err.Description, Me.Name, "DevIndice", False
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------------

' PREENCHE LINHA DA GRELHA DE RECIBOS

' -------------------------------------------------------------------------

Private Sub PreencheFgAnaEFR(indice As Long, codEfr As Long)
    Dim i As Integer
    Dim linha As Integer
    On Error GoTo TrataErro
    linha = FgAnaEFR.rows - 1
    For i = 1 To FGAna.rows - 1
        If FgAnaEFR.TextMatrix(i, lColFgAnaEFRlinha) = "" Then
            linha = FgAnaEFR.rows - 1
            Exit For
        ElseIf FgAnaEFR.TextMatrix(i, lColFgAnaEFRlinha) = indice Then
            linha = i
            Exit For
        End If
    Next
    If RegistosRM(indice).ReciboEntidade = codEfr Or codEfr = -1 Then

        If RegistosRM(indice).ReciboCodAna <> "" Then
            FgAnaEFR.TextMatrix(linha, lColFgAnaEFRlinha) = indice
            FgAnaEFR.TextMatrix(linha, lColFgAnaEFRCodigo) = RegistosRM(indice).ReciboCodAnaEFR
            FgAnaEFR.TextMatrix(linha, lColFgAnaEFRDescricao) = RegistosRM(indice).ReciboDescrAnaEFR
            FgAnaEFR.TextMatrix(linha, lColFgAnaEFRMedico) = RegistosRM(indice).ReciboCodMedico
            FgAnaEFR.TextMatrix(linha, lColFgAnaEFRNrBenef) = RegistosRM(indice).ReciboNrBenef
            FgAnaEFR.TextMatrix(linha, lColFgAnaEFRP1) = RegistosRM(indice).ReciboP1
            FgAnaEFR.TextMatrix(linha, lColFgAnaEFROrdemP1) = RegistosRM(indice).ReciboOrdemMarcacao
            FgAnaEFR.TextMatrix(linha, lColFgAnaEFRRec) = RegistosRM(indice).ReciboSerieDoc & "/" & RegistosRM(indice).ReciboNumDoc
            FgAnaEFR.TextMatrix(linha, lColFgAnaEFRPreco) = RegistosRM(indice).ReciboTaxa
            FgAnaEFR.TextMatrix(linha, lColFgAnaEFRPrecoEFR) = RegistosRM(indice).ReciboValorEFR
            FgAnaEFR.TextMatrix(linha, lColFgAnaEFRQtd) = RegistosRM(indice).ReciboQuantidade
            If RegistosRM(indice).ReciboFlgFacturado = 1 Then
                FgAnaEFR.TextMatrix(linha, lColFgAnaEFRFact) = "S"
            Else
                FgAnaEFR.TextMatrix(linha, lColFgAnaEFRFact) = "N"
            End If
            If RegistosRM(indice).flgCodBarras = 1 Then
                FgAnaEFR.TextMatrix(linha, lColFgAnaEFRCB) = "S"
            Else
                FgAnaEFR.TextMatrix(linha, lColFgAnaEFRCB) = "N"
            End If
            
            If RegistosRM(indice).ReciboIsencao = gTipoIsencaoIsento Then
                FgAnaEFR.TextMatrix(linha, lColFgAnaEFRIsen) = "S"
            Else
                FgAnaEFR.TextMatrix(linha, lColFgAnaEFRIsen) = "N"
            End If
    
            If linha = FgAnaEFR.rows - 1 Then
                FgAnaEFR.AddItem ""
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheFgAnaEFR: " & Err.Description, Me.Name, "PreencheFgAnaEFR", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' PREENCHE GRELHA DE RECIBOS

' -------------------------------------------------------------------------

Private Sub PreencheFgAnaEFR_todos(codEfr As Long)
    Dim i As Long
    On Error GoTo TrataErro
    LimpaFGAnaEfr
    For i = 1 To RegistosRM.Count - 1
        If RegistosRM(i).ReciboEntidade = codEfr Then
            PreencheFgAnaEFR i, codEfr
        End If
    Next
    If i > 1 Then
        EcCredAct = RegistosRM(i - 1).ReciboP1
        EcMedAct = RegistosRM(i - 1).ReciboCodMedico
        EcBenefAct = RegistosRM(i - 1).ReciboNrBenef
        Select Case RegistosRM(i - 1).ReciboIsencao
            Case gTipoIsencaoIsento
                EcIsencaoAct = "Isento"
            Case gTipoIsencaoNaoIsento
                EcIsencaoAct = "N�o Isento"
            Case gTipoIsencaoBorla
                EcIsencaoAct = "Borla"
        End Select
    Else
        EcBenefAct = EcNumBenef
        EcMedAct = EcCodMedico
        EcCredAct = "1"
        If CbTipoIsencao.ListIndex > mediComboValorNull Then
            Select Case CbTipoIsencao.ItemData(CbTipoIsencao.ListIndex)
                Case gTipoIsencaoIsento
                    EcIsencaoAct = "Isento"
                Case gTipoIsencaoNaoIsento
                    EcIsencaoAct = "N�o Isento"
                Case gTipoIsencaoBorla
                    EcIsencaoAct = "Borla"
            End Select
        Else
            EcIsencaoAct = ""
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheFgAnaEFR_todos: " & Err.Description, Me.Name, "PreencheFgAnaEFR_todos", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' ELIMINA LINHA DA GRELHA DE RECIBOSRM

' -------------------------------------------------------------------------

Private Sub EliminaFgAnaEfr(indice As Long)
    On Error GoTo TrataErro
    If indice < FgAnaEFR.rows Then
        FgAnaEFR.RemoveItem indice
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheFgAnaEFR_todos: " & Err.Description, Me.Name, "PreencheFgAnaEFR_todos", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' ADICIONA ANALISE � ESTRUTURA DE ANALISES

' -------------------------------------------------------------------------

Public Sub AdicionaAnalise(ByVal analise As String, codBarras As Boolean, ultimaLinha As Boolean, multiAna As String)
    On Error GoTo TrataErro
    Dim AnaFacturavel As String
    Dim cod_Agrup_aux As String
    Dim d As String
    DoEvents
    If ultimaLinha = True Then
        FGAna.row = FGAna.rows - 1
        LastRowA = FGAna.row
    End If
    AnaFacturavel = ""
    analise = BL_RetornaCodigoMarcar(analise, AnaFacturavel)
    
    DoEvents
    
    If Verifica_Ana_ja_Existe(LastRowA, analise) = False Then
        If Trim(UCase(analise)) <> Trim(UCase(RegistosA(LastRowA).codAna)) Then
            DoEvents
            If Insere_Nova_Analise(LastRowA, analise, AnaFacturavel, codBarras, multiAna) = True Then
                cod_Agrup_aux = EcAuxAna
                d = SELECT_Descr_Ana(cod_Agrup_aux)
                If LastRowA < FGAna.rows And codBarras = False Then
                    VerificaAnaliseJaMarcada EcNumReq, cod_Agrup_aux
                End If
                'o estado deixa de estar "Sem Analises"
                If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Then
                    EcEstadoReq = gEstadoReqEsperaProduto
                End If
                EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
                
                If FGAna.row = FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
                    'Cria linha vazia
                    FGAna.AddItem ""
                    CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
                End If
             
                FGAna.topRow = LastRowA
            End If
        End If
    Else
        Beep
        
        BG_Mensagem mediMsgStatus, "An�lise j� indicada!"
    End If
    FGAna.Col = 0
    Flg_IncrementaP1 = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaAnalise: " & Err.Description, Me.Name, "AdicionaAnalise", False
    Exit Sub
    Resume Next

End Sub

' -------------------------------------------------------------------------

' ALTERA FLAG QUE INDICA SE ESTA OU NAO USAR CODIGOS DE BARRAS

' -------------------------------------------------------------------------

Private Sub AlteraFlgCodBarras(valor As Boolean)
    On Error GoTo TrataErro
    If valor = True Then
        Me.caption = Cabecalho & " Marca��o por C�digos de Barras"
        CkEnviaFactus.value = vbChecked
        flg_codBarras = True
    Else
        Me.caption = Cabecalho
        flg_codBarras = False
        CkEnviaFactus.value = vbUnchecked
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AlteraFlgCodBarras: " & Err.Description, Me.Name, "AlteraFlgCodBarras", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' PREENCHE COMBO DE ENTIDADES

' -------------------------------------------------------------------------
Private Sub PreencheEntidades()
    Dim i As Integer
    Dim j As Integer
    On Error GoTo TrataErro
    
    CbEFR.Clear
    For j = 1 To totalEFR
        If EcCodEFR = EstrutEFR(j).cod_efr Then
            EstrutEFR(j).flgActivo = True
        Else
            EstrutEFR(j).flgActivo = False
        End If
    Next
        
    For i = 1 To RegistosRM.Count - 1
        For j = 1 To totalEFR
            If RegistosRM(i).ReciboEntidade = EstrutEFR(j).cod_efr Then
                EstrutEFR(j).flgActivo = True
                Exit For
            End If
        Next
    Next
    
    For j = 1 To totalEFR
        If EstrutEFR(j).flgActivo = True Then
            CbEFR.AddItem EstrutEFR(j).descr_efr
            CbEFR.ItemData(CbEFR.NewIndex) = EstrutEFR(j).cod_efr
        End If
    Next
    For i = 0 To CbEFR.ListCount - 1
        If CbEFR.ItemData(i) = EcCodEFR Then
            CbEFR.ListIndex = i
            Exit For
        End If
    Next
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheEntidades: " & Err.Description, Me.Name, "PreencheEntidades", False
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheEntidadesDefeito()
    Dim sSql As String
    Dim RsEFR As New ADODB.recordset
    totalEFR = 0
    ReDim EstrutEFR(0)
    sSql = "SELECT  * FROM sl_efr"
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEFR.Open sSql, gConexao
    If RsEFR.RecordCount >= 1 Then
        While Not RsEFR.EOF
            totalEFR = totalEFR + 1
            ReDim Preserve EstrutEFR(totalEFR)
            EstrutEFR(totalEFR).cod_efr = BL_HandleNull(RsEFR!cod_efr, "")
            EstrutEFR(totalEFR).descr_efr = BL_HandleNull(RsEFR!descr_efr, "")
            EstrutEFR(totalEFR).cod_empresa = BL_HandleNull(RsEFR!cod_empresa, "")
            EstrutEFR(totalEFR).deducao_ute = BL_HandleNull(RsEFR!deducao_ute, "")
            EstrutEFR(totalEFR).flg_65anos = BL_HandleNull(RsEFR!flg_65anos, "0")
            EstrutEFR(totalEFR).flg_acto_med = BL_HandleNull(RsEFR!flg_acto_med_efr, "0")
            EstrutEFR(totalEFR).flg_compart_dom = BL_HandleNull(RsEFR!flg_compart_dom, "0")
            EstrutEFR(totalEFR).flg_controla_isencao = BL_HandleNull(RsEFR!flg_controla_isencao, "0")
            EstrutEFR(totalEFR).flg_dom_defeito = BL_HandleNull(RsEFR!flg_dom_defeito, "0")
            EstrutEFR(totalEFR).flg_insere_cod_rubr_efr = BL_HandleNull(RsEFR!flg_insere_cod_rubr_efr, "0")
            EstrutEFR(totalEFR).flg_nao_urbano = BL_HandleNull(RsEFR!flg_nao_urbano, "0")
            EstrutEFR(totalEFR).flg_obriga_recibo = BL_HandleNull(RsEFR!flg_obriga_recibo, "0")
            EstrutEFR(totalEFR).flg_perc_facturar = BL_HandleNull(RsEFR!flg_perc_facturar, "0")
            EstrutEFR(totalEFR).flg_separa_dom = BL_HandleNull(RsEFR!flg_separa_dom, "0")
            EstrutEFR(totalEFR).flg_novaArs = BL_HandleNull(RsEFR!flg_nova_ars, 0)
            EstrutEFR(totalEFR).flgActivo = False
            RsEFR.MoveNext
        Wend
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheEntidadesDefeito: " & Err.Description, Me.Name, "PreencheEntidadesDefeito", False
    Exit Sub
    Resume Next
End Sub



' -------------------------------------------------------------------------

' AP�S ANALISES TEREM SIDO APAGAAS, SE RECIBO CANCELADO, RETIRA ESSAS ANALISES

' -------------------------------------------------------------------------
Public Sub RetiraAnalisesApagadas()
    Dim i As Integer
    Dim j As Long
    Dim flg_apaga As Boolean
    On Error GoTo TrataErro
    For j = 1 To RegistosRM.Count - 1
        flg_apaga = True
        If RegistosRM(j).ReciboFlgReciboManual <> "1" Then
            For i = 1 To RegistosA.Count - 1
                If RegistosA(i).codAna = RegistosRM(j).ReciboCodAna Then
                    flg_apaga = False
                    Exit For
                End If
            Next
        Else
            flg_apaga = False
        End If
        If flg_apaga = True Then
            If EliminaReciboManual(j, RegistosRM(j).ReciboCodAna, True) = True Then
                LimpaColeccao RegistosRM, j
                j = j - 1
            End If

        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  RetiraAnalisesApagadas: " & Err.Description, Me.Name, "RetiraAnalisesApagadas", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' PREENCHE INFORMA��O ASSOCIADA �S ANALISES QUE FORAM REGISTADAS

' -------------------------------------------------------------------------
Private Sub PreencheInformacao(n_req)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsInfo As New ADODB.recordset
    totalInfo = 0
    ReDim EstrutInfo(0)
    
    sSql = "SELECT x2.descr_informacao, x2.cod_informacao, informacao FROM sl_req_informacao x1, sl_informacao x2 "
    sSql = sSql & " WHERE n_req = " & n_req & " and x1.cod_informacao = x2.cod_informacao "
    
    rsInfo.CursorLocation = adUseServer
    rsInfo.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsInfo.Open sSql, gConexao
    If rsInfo.RecordCount >= 1 Then
        While Not rsInfo.EOF
            totalInfo = totalInfo + 1
            ReDim Preserve EstrutInfo(totalInfo)
            EstrutInfo(totalInfo).cod_informacao = BL_HandleNull(rsInfo!cod_informacao, "")
            EstrutInfo(totalInfo).descr_informacao = BL_HandleNull(rsInfo!descr_informacao, "")
            EstrutInfo(totalInfo).informacao = BL_HandleNull(rsInfo!informacao, "")
            FgInformacao.TextMatrix(totalInfo, 0) = BL_HandleNull(rsInfo!descr_informacao, "")
            
            EcInfo.TextRTF = BL_HandleNull(rsInfo!informacao, "")
            FgInformacao.TextMatrix(totalInfo, 1) = EcInfo.text
            FgInformacao.AddItem ""
            rsInfo.MoveNext
        Wend
    End If
    rsInfo.Close
    Set rsInfo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheInformacao: " & Err.Description, Me.Name, "PreencheInformacao", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' PREENCHE Quest�es ASSOCIADA �S ANALISES QUE FORAM REGISTADAS

' -------------------------------------------------------------------------
Private Sub PreencheQuestao(n_req)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsInfo As New ADODB.recordset
    totalQuestao = 0
    ReDim EstrutQuestao(0)
    
    sSql = "SELECT x2.descr_frase, x2.cod_frase, x1.resposta FROM sl_req_questoes x1, sl_dicionario x2 "
    sSql = sSql & " WHERE n_req = " & n_req & " and x1.cod_frase = x2.cod_Frase "
    
    rsInfo.CursorLocation = adUseServer
    rsInfo.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsInfo.Open sSql, gConexao
    If rsInfo.RecordCount >= 1 Then
        While Not rsInfo.EOF
            totalQuestao = totalQuestao + 1
            ReDim Preserve EstrutQuestao(totalQuestao)
            EstrutQuestao(totalQuestao).cod_questao = BL_HandleNull(rsInfo!cod_frase, "")
            EstrutQuestao(totalQuestao).descr_questao = BL_HandleNull(rsInfo!descr_frase, "")
            EstrutQuestao(totalQuestao).Resposta = BL_HandleNull(rsInfo!Resposta, "")
            FgQuestoes.TextMatrix(totalQuestao, 0) = BL_HandleNull(rsInfo!descr_frase, "")
            FgQuestoes.TextMatrix(totalQuestao, 1) = BL_HandleNull(rsInfo!Resposta, "")
            FgQuestoes.AddItem ""
            rsInfo.MoveNext
        Wend
    End If
    rsInfo.Close
    Set rsInfo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheQuestao: " & Err.Description, Me.Name, "PreencheQuestao", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' AO ADICIONAR ANALISES, ADICIONA INFORMACAO DA ANALISE

' -------------------------------------------------------------------------
Private Sub AdicionaInformacaoAna(cod_ana As String)
    Dim sSql As String
    Dim flg_existe As Boolean
    Dim rsInfo As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    sSql = "SELECT x1.cod_informacao, x2.descr_informacao, x2.informacao FROM sl_ana_informacao x1, sl_informacao x2 WHERE x1.cod_informacao = x2.cod_informacao AND "
    sSql = sSql & " x1.cod_ana = " & BL_TrataStringParaBD(cod_ana)
    rsInfo.CursorLocation = adUseServer
    rsInfo.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsInfo.Open sSql, gConexao
    If rsInfo.RecordCount >= 1 Then
        While Not rsInfo.EOF
            flg_existe = False
            For i = 1 To totalInfo
                If BL_HandleNull(rsInfo!cod_informacao, "") = EstrutInfo(i).cod_informacao Then
                    flg_existe = True
                    Exit For
                End If
            Next
            If flg_existe = False Then
                totalInfo = totalInfo + 1
                ReDim Preserve EstrutInfo(totalInfo)
                EstrutInfo(totalInfo).cod_informacao = BL_HandleNull(rsInfo!cod_informacao, "")
                EstrutInfo(totalInfo).descr_informacao = BL_HandleNull(rsInfo!descr_informacao, "")
                EstrutInfo(totalInfo).informacao = BL_HandleNull(rsInfo!informacao, "")
                FgInformacao.TextMatrix(totalInfo, 0) = BL_HandleNull(rsInfo!descr_informacao, "")
                
                EcInfo.TextRTF = BL_HandleNull(rsInfo!informacao, "")
                FgInformacao.TextMatrix(totalInfo, 1) = EcInfo.text
                FgInformacao.AddItem ""
            End If
            rsInfo.MoveNext
        Wend
    End If
    rsInfo.Close
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaInformacaoAna: " & Err.Description, Me.Name, "AdicionaInformacaoAna", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' GRAVA INFORMACAO DA ANALISE

' -------------------------------------------------------------------------
Private Function GravaInfoAna(n_req As String) As Boolean
    Dim sSql As String
    Dim i As Integer
    Dim registos As Integer
    On Error GoTo TrataErro
    GravaInfoAna = False
    sSql = "DELETE FROM sl_req_informacao WHERE n_req = " & n_req
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To totalInfo
        sSql = "INSERT INTO sl_req_informacao (n_req, cod_informacao) VALUES("
        sSql = sSql & n_req & ", " & BL_TrataStringParaBD(EstrutInfo(i).cod_informacao) & ")"
        registos = BG_ExecutaQuery_ADO(sSql)
        If registos <= 0 Then
            GoTo TrataErro
        End If
    Next
    GravaInfoAna = True
Exit Function
TrataErro:
    GravaInfoAna = False
    BG_LogFile_Erros "Erro  GravaInfoAna: " & Err.Description, Me.Name, "GravaInfoAna", False
    Exit Function
    Resume Next
End Function


' -------------------------------------------------------------------------

' AO ADICIONAR ANALISES, ADICIONA Questoes DA ANALISE

' -------------------------------------------------------------------------
Private Sub AdicionaQuestaoAna(cod_ana As String)
    Dim sSql As String
    Dim flg_existe As Boolean
    Dim rsQuest As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    sSql = "SELECT x1.cod_frase, x2.descr_frase FROM sl_ana_questoes x1, sl_dicionario x2 WHERE x1.cod_frase = x2.cod_Frase AND "
    sSql = sSql & " x1.cod_ana = " & BL_TrataStringParaBD(cod_ana)
    rsQuest.CursorLocation = adUseServer
    rsQuest.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsQuest.Open sSql, gConexao
    If rsQuest.RecordCount >= 1 Then
        While Not rsQuest.EOF
            flg_existe = False
            For i = 1 To totalQuestao
                If BL_HandleNull(rsQuest!cod_frase, "") = EstrutQuestao(i).cod_questao Then
                    flg_existe = True
                    Exit For
                End If
            Next
            If flg_existe = False Then
                totalQuestao = totalQuestao + 1
                ReDim Preserve EstrutQuestao(totalQuestao)
                EstrutQuestao(totalQuestao).cod_questao = BL_HandleNull(rsQuest!cod_frase, "")
                EstrutQuestao(totalQuestao).descr_questao = BL_HandleNull(rsQuest!descr_frase, "")

                FgQuestoes.TextMatrix(totalQuestao, 0) = BL_HandleNull(rsQuest!descr_frase, "")
                FgQuestoes.AddItem ""
            End If
            rsQuest.MoveNext
        Wend
    End If
    rsQuest.Close
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaQuestaoAna: " & Err.Description, Me.Name, "AdicionaQuestaoAna", False
    Exit Sub
    Resume Next
End Sub


' -------------------------------------------------------------------------

' GRAVA Questoes DA ANALISE

' -------------------------------------------------------------------------
Private Function GravaQuestoesAna(n_req As String) As Boolean
    Dim sSql As String
    Dim i As Integer
    Dim registos As Integer
    On Error GoTo TrataErro
    GravaQuestoesAna = False
    sSql = "DELETE FROM sl_req_questoes WHERE n_req = " & n_req
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To totalQuestao
        sSql = "INSERT INTO sl_req_questoes (n_req, cod_frase, resposta) VALUES("
        sSql = sSql & n_req & ", " & BL_TrataStringParaBD(EstrutQuestao(i).cod_questao) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutQuestao(i).Resposta) & " )"
        registos = BG_ExecutaQuery_ADO(sSql)
        If registos <= 0 Then
            GoTo TrataErro
        End If
    Next
    GravaQuestoesAna = True
Exit Function
TrataErro:
    GravaQuestoesAna = False
    BG_LogFile_Erros "Erro  GravaQuestoesAna: " & Err.Description, Me.Name, "GravaQuestoesAna", False
    Exit Function
    Resume Next
End Function
Private Sub EcCodpais_Validate(cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodPais.text = UCase(EcCodPais.text)
    If EcCodPais.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_pais FROM sl_pais WHERE cod_pais='" & Trim(EcCodPais.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            cancel = True
            BG_Mensagem mediMsgBox, "Pa�s inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodPais.text = ""
            EcDescrPais.text = ""
        Else
            EcDescrPais.text = BL_HandleNull(Tabela!descr_pais)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrPais.text = ""
    End If
End Sub


Private Sub BtPesqPais_click()

    
    FormPesquisaRapida.InitPesquisaRapida "sl_pais", _
                        "descr_pais", "cod_pais", _
                        EcCodPais
    EcCodpais_Validate False
End Sub

' ----------------------------------------------------------------------------------

' VERIFICA SE DADOS ESTAO COERENTES PARA FACTURACAO ARS

' ----------------------------------------------------------------------------------

Private Function ValidaDadosFactARS() As Boolean
    On Error GoTo TrataErro
    ValidaDadosFactARS = False
    If CbCodUrbano.ListIndex > mediComboValorNull Then
        If EcCodPostal = "" Or EcRuaPostal = "" And EcDescrPostal = "" Then
            BG_Mensagem mediMsgBox, "Domicilio preenchido. C�digo Postal completo obrigat�rio.", vbExclamation + vbOKOnly, App.ProductName
            ValidaDadosFactARS = False
            Exit Function
        End If
    End If
    If CbConvencao.ItemData(CbConvencao.ListIndex) = gConvencaoInternacional Then
        If EcCodPais = "" Then
            BG_Mensagem mediMsgBox, "Conven��o Internacional. Pa�s obrigat�rio.", vbExclamation + vbOKOnly, App.ProductName
            ValidaDadosFactARS = False
            Exit Function
        End If
    End If
    If CbConvencao.ItemData(CbConvencao.ListIndex) = gConvencaoProfissional Then
        If EcCodPais = "" Then
            BG_Mensagem mediMsgBox, "Conven��o Profissional. N�mero de Doente Profissional obrigat�rio.", vbExclamation + vbOKOnly, App.ProductName
            ValidaDadosFactARS = False
            Exit Function
        End If
    End If
    ValidaDadosFactARS = True
Exit Function
TrataErro:
    ValidaDadosFactARS = False
    BG_LogFile_Erros "Erro  ValidaDadosFactARS: " & Err.Description, Me.Name, "ValidaDadosFactARS", False
    Exit Function
    Resume Next
End Function

Private Function FacturaReqVet() As Boolean
    Dim i As Integer
    Dim j As Integer
    Dim flg_Fact As Boolean
    On Error GoTo TrataErro
    FacturaReqVet = False
    For i = 1 To RegistosR.Count
        flg_Fact = False
        If BL_HandleNull(RegistosR(i).NumDoc, -1) <> -1 Then
            For j = 1 To RegistosRM.Count
                If RegistosR(i).NumDoc = RegistosRM(j).ReciboNumDoc And RegistosR(i).SerieDoc = RegistosRM(j).ReciboSerieDoc And RegistosR(i).codEntidade = RegistosRM(j).ReciboEntidade Then
                    If BL_HandleNull(RegistosRM(j).ReciboFlgFacturado, 0) = 0 Then
                        If flg_Fact = False Then
                            'IF_Factura_RequisicaoPrivado
                            flg_Fact = True
                        End If
                    End If
                End If
            Next
        End If
    Next
    FacturaReqVet = True
Exit Function
TrataErro:
    FacturaReqVet = False
    BG_LogFile_Erros "Erro  FacturaReqVet: " & Err.Description, Me.Name, "FacturaReqVet", False
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------------------------------

' VERIFICA SE JA EXISTE REGISTO PARA DETERMINADO PERFIL DE MARCACAO NA ESTRUTURA DE RECIBOS

' -------------------------------------------------------------------------------------------
Private Function VerificaPerfilMarcacaoJaMarcado(CodPerfil As String) As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodFacturavel = CodPerfil And RegistosRM(i).ReciboCodAna = CodPerfil Then
            VerificaPerfilMarcacaoJaMarcado = True
            Exit Function
        End If
    Next
    VerificaPerfilMarcacaoJaMarcado = False
    
Exit Function
TrataErro:
    VerificaPerfilMarcacaoJaMarcado = False
    BG_LogFile_Erros "Erro  VerificaPerfilMarcacaoJaMarcado: " & Err.Description, Me.Name, "VerificaPerfilMarcacaoJaMarcado", False
    Exit Function
    Resume Next
End Function



Private Sub RegistaTubosEliminados()
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To TotalTubosEliminados
        TB_ApagaTuboBD BL_HandleNull(tubosEliminados(i).n_req, EcNumReq), tubosEliminados(i).seq_req_tubo
    Next
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  RegistaTubosEliminados: " & Err.Description, Me.Name, "RegistaTubosEliminados", False
    Exit Sub
    Resume Next
End Sub
Private Sub AcrescentaTubosEliminados(n_req As String, seq_req_tubo As Long)
    On Error GoTo TrataErro
    TotalTubosEliminados = TotalTubosEliminados + 1
    ReDim Preserve tubosEliminados(TotalTubosEliminados)
    tubosEliminados(TotalTubosEliminados).n_req = n_req
    tubosEliminados(TotalTubosEliminados).seq_req_tubo = seq_req_tubo
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AcrescentaTubosEliminados: " & Err.Description, Me.Name, "AcrescentaTubosEliminados", False
    Exit Sub
    Resume Next
End Sub



Public Sub EliminaTuboEstrut()
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim flg_temAna As Boolean
    On Error GoTo TrataErro
    
    For i = 1 To gTotalTubos
        If i <= gTotalTubos Then
            flg_temAna = False
            For j = 1 To UBound(MaReq)
                If MaReq(j).indice_tubo = i Then
                    flg_temAna = True
                    Exit For
                End If
            Next j
            'BEDSIDE -> gEstruturaTubos(i).estado_tubo <> gEstadosTubo.cancelado
            If flg_temAna = False And gEstruturaTubos(i).estado_tubo <> gEstadosTubo.cancelado Then
                AcrescentaTubosEliminados EcNumReq.text, gEstruturaTubos(i).seq_req_tubo
                For k = i To gTotalTubos - 1
                    TB_MoveEstrutTubos k + 1, k
                Next
                gTotalTubos = gTotalTubos - 1
                ReDim Preserve gEstruturaTubos(gTotalTubos)
                FGTubos.RemoveItem i
                i = i - 1
            End If
        End If
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no FormGestaoRequisicaoPrivadoPrivado -> Elimina��o de Tubos: " & Err.Description
    Exit Sub
    Resume Next
End Sub

Private Function VerificaAnalisesJaMarcadasPerfil(CodPerfil As String) As Boolean
    Dim sSql As String
    Dim RsPerf As New ADODB.recordset
    Dim total As Integer
    Dim i As Integer
    On Error GoTo TrataErro
    sSql = "SELECT * FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(CodPerfil)
    RsPerf.CursorType = adOpenStatic
    RsPerf.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsPerf.Open sSql, gConexao
    total = 0
    If RsPerf.RecordCount > 0 Then
        While Not RsPerf.EOF
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_agrup = BL_HandleNull(RsPerf!cod_analise, "") Then
                    total = total + 1
                    Exit For
                End If
            Next i
            RsPerf.MoveNext
        Wend
        If total > 0 Then
            gMsgTitulo = "Marca��o Perfil"
            gMsgMsg = "O Perfil em causa j� tem " & total & " an�lises marcadas. Pretende continuar a marcar?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                VerificaAnalisesJaMarcadasPerfil = True
            Else
                VerificaAnalisesJaMarcadasPerfil = False
            End If
        Else
            VerificaAnalisesJaMarcadasPerfil = True
        End If
    End If
    RsPerf.Close
    Set RsPerf = Nothing
    
Exit Function
TrataErro:
    VerificaAnalisesJaMarcadasPerfil = False
    BG_LogFile_Erros "Erro no VerificaAnalisesJaMarcadasPerfil:" & Err.Description, Me.Name, "VerificaAnalisesJaMarcadasPerfil"
    Exit Function
    Resume Next
End Function

Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
    'rcoelho 20.05.2013 cedivet-22
    EcCodEFR_LostFocus
End Sub

'RGONCALVES 29.05.2013 Cedivet-30
Private Function ActualizaEntidadeIdentif(ByVal seq_utente As String, ByVal cod_efr As String) As Boolean
    On Error GoTo TrataErro
    Dim sql As String
    Dim rs As ADODB.recordset
    
    seq_utente = Trim(seq_utente)
    cod_efr = Trim(cod_efr)
    
    If seq_utente = "" Then
        ActualizaEntidadeIdentif = True
        Exit Function
    End If
    
    If cod_efr <> "" Then
        sql = "SELECT * FROM sl_efr WHERE cod_efr = " & BL_TrataStringParaBD(cod_efr)
        
        Set rs = New ADODB.recordset
        rs.CursorType = adOpenStatic
        rs.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rs.Open sql, gConexao
        
        If rs.RecordCount = 1 Then
            sql = "UPDATE sl_identif SET cod_efr_ute = " & BL_TrataStringParaBD(cod_efr) & _
                ", descr_mor_ute = " & BL_TrataStringParaBD(BL_HandleNull(rs!mor_efr, "")) & _
                ", cod_postal_ute = " & BL_TrataStringParaBD(BL_HandleNull(rs!cod_postal_efr, "")) & _
                " WHERE seq_utente = " & BL_TrataStringParaBD(seq_utente)
            BG_ExecutaQuery_ADO (sql)
        End If
    End If
    ActualizaEntidadeIdentif = True
Exit Function
TrataErro:
    ActualizaEntidadeIdentif = False
    BG_LogFile_Erros "Erro no ActualizaEntidadeIdentif:" & Err.Description, Me.Name, "ActualizaEntidadeIdentif"
    Exit Function
    Resume Next
End Function

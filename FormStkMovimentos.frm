VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormStkMovimentos 
   BackColor       =   &H00FFE0C7&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormStkMovimentos"
   ClientHeight    =   8160
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13815
   Icon            =   "FormStkMovimentos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8160
   ScaleWidth      =   13815
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameMovimento 
      BackColor       =   &H00FFE0C7&
      Caption         =   "Novo Movimento"
      Height          =   2055
      Left            =   1440
      TabIndex        =   41
      Top             =   2280
      Width           =   11175
      Begin VB.TextBox EcStockMin 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   10320
         Locked          =   -1  'True
         MaxLength       =   9
         TabIndex        =   59
         Top             =   840
         Width           =   735
      End
      Begin VB.CommandButton BtCancelarMovimento 
         BackColor       =   &H00FFC0C0&
         Height          =   495
         Left            =   9720
         Picture         =   "FormStkMovimentos.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   58
         ToolTipText     =   "Cancelar Movimento"
         Top             =   1440
         Width           =   615
      End
      Begin VB.TextBox EcStockActual 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   8640
         Locked          =   -1  'True
         MaxLength       =   9
         TabIndex        =   56
         Top             =   1320
         Width           =   735
      End
      Begin VB.CommandButton BtGravarMovimento 
         BackColor       =   &H00C0FFC0&
         Height          =   495
         Left            =   10440
         Picture         =   "FormStkMovimentos.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   55
         ToolTipText     =   "Gravar Movimento"
         Top             =   1440
         Width           =   615
      End
      Begin VB.TextBox EcQuantidade 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   8640
         MaxLength       =   9
         TabIndex        =   53
         Top             =   840
         Width           =   735
      End
      Begin VB.TextBox EcObservacao 
         Appearance      =   0  'Flat
         Height          =   495
         Left            =   1080
         TabIndex        =   51
         Top             =   1320
         Width           =   5535
      End
      Begin VB.ComboBox CbMovimento2 
         Height          =   315
         Left            =   8640
         Style           =   2  'Dropdown List
         TabIndex        =   49
         Top             =   360
         Width           =   2415
      End
      Begin VB.TextBox EcCodArtigo2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1110
         MaxLength       =   4
         TabIndex        =   46
         Top             =   840
         Width           =   735
      End
      Begin VB.TextBox EcDescrArtigo2 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1800
         Locked          =   -1  'True
         TabIndex        =   45
         Top             =   840
         Width           =   4815
      End
      Begin VB.TextBox EcCodLote2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   1110
         MaxLength       =   4
         TabIndex        =   44
         Top             =   360
         Width           =   735
      End
      Begin VB.TextBox EcDescrLote2 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1800
         Locked          =   -1  'True
         TabIndex        =   43
         Top             =   360
         Width           =   4815
      End
      Begin VB.CommandButton BtPesqLote2 
         Height          =   315
         Left            =   6600
         Picture         =   "FormStkMovimentos.frx":19A0
         Style           =   1  'Graphical
         TabIndex        =   42
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
         Top             =   360
         Width           =   375
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Stock Min"
         ForeColor       =   &H00000000&
         Height          =   255
         Index           =   12
         Left            =   9480
         TabIndex        =   60
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Stock Actual"
         Height          =   255
         Index           =   8
         Left            =   7560
         TabIndex        =   57
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Quantidade"
         Height          =   255
         Index           =   11
         Left            =   7560
         TabIndex        =   54
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Observa��o"
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   52
         Top             =   1320
         Width           =   975
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Movimento"
         Height          =   255
         Index           =   9
         Left            =   7560
         TabIndex        =   50
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Artigo"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   48
         Top             =   840
         Width           =   615
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Lote"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   47
         Top             =   360
         Width           =   615
      End
   End
   Begin VB.PictureBox PictureListColor 
      Height          =   255
      Left            =   0
      ScaleHeight     =   195
      ScaleWidth      =   555
      TabIndex        =   39
      Top             =   13560
      Width           =   615
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFE0C7&
      Caption         =   "Pesquisa"
      Height          =   2295
      Left            =   120
      TabIndex        =   20
      Top             =   120
      Width           =   13575
      Begin VB.CommandButton BtCodifLote 
         BackColor       =   &H00C0FFFF&
         Height          =   615
         Left            =   12120
         Picture         =   "FormStkMovimentos.frx":1F2A
         Style           =   1  'Graphical
         TabIndex        =   61
         ToolTipText     =   "Codifica��o Lote"
         Top             =   1440
         Width           =   615
      End
      Begin VB.CommandButton BtNovoMovimento 
         BackColor       =   &H00FFC0C0&
         Height          =   615
         Left            =   12840
         Picture         =   "FormStkMovimentos.frx":2BF4
         Style           =   1  'Graphical
         TabIndex        =   40
         ToolTipText     =   "Novo Movimento"
         Top             =   1440
         Width           =   615
      End
      Begin VB.ComboBox CbMovimento 
         Height          =   315
         Left            =   8040
         Style           =   2  'Dropdown List
         TabIndex        =   37
         Top             =   840
         Width           =   3135
      End
      Begin VB.CommandButton BtPesqLote 
         Height          =   315
         Left            =   6480
         Picture         =   "FormStkMovimentos.frx":38BE
         Style           =   1  'Graphical
         TabIndex        =   35
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
         Top             =   360
         Width           =   375
      End
      Begin VB.TextBox EcDescrLote 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   34
         Top             =   360
         Width           =   4815
      End
      Begin VB.TextBox EcCodLote 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   990
         MaxLength       =   4
         TabIndex        =   33
         Top             =   360
         Width           =   735
      End
      Begin VB.TextBox EcDtFim 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   10110
         TabIndex        =   31
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox EcDtIni 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   8070
         TabIndex        =   29
         Top             =   360
         Width           =   975
      End
      Begin VB.CommandButton BtPesqTipo 
         Height          =   315
         Left            =   6480
         Picture         =   "FormStkMovimentos.frx":3E48
         Style           =   1  'Graphical
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
         Top             =   1320
         Width           =   375
      End
      Begin VB.TextBox EcCodTipo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   990
         MaxLength       =   4
         TabIndex        =   27
         Top             =   1320
         Width           =   735
      End
      Begin VB.TextBox EcDescrTipo 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   1320
         Width           =   4935
      End
      Begin VB.CommandButton BtPesqArtigo 
         Height          =   315
         Left            =   6480
         Picture         =   "FormStkMovimentos.frx":43D2
         Style           =   1  'Graphical
         TabIndex        =   23
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
         Top             =   840
         Width           =   375
      End
      Begin VB.TextBox EcDescrArtigo 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   22
         Top             =   840
         Width           =   4815
      End
      Begin VB.TextBox EcCodArtigo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   990
         MaxLength       =   4
         TabIndex        =   21
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Movimento"
         Height          =   255
         Index           =   6
         Left            =   7080
         TabIndex        =   38
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Lote"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   36
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Data Final"
         Height          =   255
         Index           =   4
         Left            =   9240
         TabIndex        =   32
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Data Inicial"
         Height          =   255
         Index           =   3
         Left            =   7080
         TabIndex        =   30
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Tipo"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   28
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Artigo"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   24
         Top             =   840
         Width           =   615
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFE0C7&
      Height          =   795
      Left            =   120
      TabIndex        =   6
      Top             =   7080
      Width           =   13605
      Begin VB.Label Label8 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   195
         Width           =   675
      End
      Begin VB.Label Label5 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   480
         Width           =   705
      End
      Begin VB.Label LaUtilCriacao 
         BackColor       =   &H00FFE0C7&
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   10
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         BackColor       =   &H00FFE0C7&
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   9
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaDataCriacao 
         BackColor       =   &H00FFE0C7&
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   8
         Top             =   195
         Width           =   2055
      End
      Begin VB.Label LaDataAlteracao 
         BackColor       =   &H00FFE0C7&
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   7
         Top             =   480
         Width           =   2295
      End
   End
   Begin VB.TextBox EcHrAct 
      Enabled         =   0   'False
      Height          =   285
      Left            =   7080
      TabIndex        =   5
      Top             =   10080
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.TextBox EcHrCri 
      Enabled         =   0   'False
      Height          =   285
      Left            =   7080
      TabIndex        =   4
      Top             =   9720
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2400
      TabIndex        =   3
      Top             =   10080
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4800
      TabIndex        =   2
      Top             =   10080
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2400
      TabIndex        =   1
      Top             =   9720
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4800
      TabIndex        =   0
      Top             =   9720
      Visible         =   0   'False
      Width           =   285
   End
   Begin MSComctlLib.ListView LvMovimentos 
      Height          =   4335
      Left            =   120
      TabIndex        =   13
      Top             =   2640
      Width           =   13695
      _ExtentX        =   24156
      _ExtentY        =   7646
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   0
   End
   Begin VB.Label Label1 
      Caption         =   "EcHrAct"
      Height          =   255
      Index           =   17
      Left            =   5760
      TabIndex        =   19
      Top             =   10080
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "EcHrCri"
      Height          =   255
      Index           =   18
      Left            =   5760
      TabIndex        =   18
      Top             =   9720
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Index           =   2
      Left            =   720
      TabIndex        =   17
      Top             =   9720
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   720
      TabIndex        =   16
      Top             =   10080
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   3480
      TabIndex        =   15
      Top             =   9720
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   3360
      TabIndex        =   14
      Top             =   10080
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "FormStkMovimentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Const cLightGray = &H808080
Const cLightYellow = &H80000018
Const cWhite = &HFFFFFF
Const cLightBlue = &HFCE7D8
Const cYellow = &HFFFF&
Const cGreen = &HC000&
Const cRed = &HFF&

Private Type movimento
    seq_movimento As Long
    cod_lote As String
    descr_lote As String
    cod_artigo As String
    descr_artigo As String
    tipo_movimento As Integer
    ent_saida As Integer
    descr_tipo_mov As String
    transferencia As Integer
    quantidade As Double
    stock_disponivel As Double
    user_cri As String
    dt_cri As String
    hr_cri As String
    observacao As String
    cor As Long
End Type
Dim estrutMov() As movimento
Dim totalMovimento As Integer

Dim stock_antes As Double

Private Sub BtCancelarMovimento_Click()
    LimpaFrameMovimentos
    FrameMovimento.Visible = False
    LvMovimentos.SetFocus
End Sub

Private Sub BtCodifLote_Click()
    If EcCodLote <> "" Then
        FormStkLotes.Show
        FormStkLotes.EcCodigo = EcCodLote
        FormStkLotes.FuncaoProcurar
    End If
End Sub

Private Sub BtGravarMovimento_Click()
    On Error GoTo TrataErro
    
    If EcCodArtigo2 = "" Then
        BG_Mensagem mediMsgBox, "Deve indicar um Artigo.", vbInformation, "Movimento inv�lido"
        Exit Sub
    End If
    If EcCodLote2 = "" Then
        BG_Mensagem mediMsgBox, "Deve indicar um Lote.", vbInformation, "Movimento inv�lido"
        Exit Sub
    End If
    If CbMovimento2.ListIndex = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "Deve indicar um Tipo de Movimento.", vbInformation, "Movimento inv�lido"
        Exit Sub
    End If
    If EcQuantidade = "" Or EcStockActual = "" Then
    
        BG_Mensagem mediMsgBox, "Deve indicar Quantidade.", vbInformation, "Movimento inv�lido"
        Exit Sub
    Else
        If IsNumeric(EcQuantidade) = False Or IsNumeric(EcStockActual) = False Then
            BG_Mensagem mediMsgBox, "Deve indicar uma Quantidade v�lida.", vbInformation, "Movimento inv�lido"
            Exit Sub
        End If
    End If
    If EcStockMin > EcStockActual Then
        gMsgMsg = "Stock actual � menor que stock minimo. Tem a certeza que quer registar o movimento?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbNo Then
            Exit Sub
        End If
    End If
    If EcStockActual <= 0 Then
        gMsgMsg = "Stock actual � nulo. Tem a certeza que quer registar o movimento?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbNo Then
            Exit Sub
        End If
    End If
    
    
    BG_BeginTransaction
    If STK_InsereMovimento(EcCodLote2, EcCodArtigo2, CbMovimento2.ItemData(CbMovimento2.ListIndex), Replace(EcQuantidade, ".", ","), EcObservacao) = False Then
        GoTo TrataErro
    End If
    BG_CommitTransaction
    
    FrameMovimento.Visible = False
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "FormStkMovimentos: BtGravarMovimento_Click: " & Err.Description, Me.Name, "BtGravarMovimento_Click", True
    Exit Sub
    Resume Next
End Sub

Private Sub BtNovoMovimento_Click()
    FrameMovimento.left = 1440
    FrameMovimento.top = 2280
    FrameMovimento.Visible = True
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Movimentos"
    Me.left = 5
    Me.top = 5
    Me.Width = 13905
    Me.Height = 8580 ' Normal
    'Me.Height = 8330 ' Campos Extras
    Set CampoDeFocus = EcCodLote
    
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormStkMovimentos = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus
    LvMovimentos.ListItems.Clear
    EcCodArtigo = ""
    EcCodLote = ""
    EcDescrArtigo = ""
    EcDescrLote = ""
    EcDtFim = ""
    EcDtIni = ""
    CbMovimento.ListIndex = mediComboValorNull
    EcDescrTipo = ""
    EcCodTipo = ""
    
End Sub

Sub DefTipoCampos()
    InicializaTreeView
    FrameMovimento.Visible = False
    BG_PreencheComboBD_ADO "SL_STK_TIPOS_MOV", "codigo", "descricao", CbMovimento, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "SL_STK_TIPOS_MOV", "codigo", "descricao", CbMovimento2, mediAscComboDesignacao
    
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        PreencheLV
    End If
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    If EcCodArtigo = "" And EcCodLote = "" And (EcDtIni = "" Or EcDtFim = "") And CbMovimento.ListIndex = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "Deve indicar um Crit�rio.", vbInformation, "Pesquisa inv�lida"
        Exit Sub
    End If
    
    CriterioTabela = ConstroiCriterio
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        Call BG_Mensagem(mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, " Procurar")
        FuncaoLimpar
    Else
        estado = 2

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    
    If LvMovimentos.SelectedItem.Index = 1 Then
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LvMovimentos.ListItems(LvMovimentos.SelectedItem - 1).Selected = True
    
        BL_FimProcessamento Me
        'EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    
    If LvMovimentos.SelectedItem = totalMovimento Then
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LvMovimentos.ListItems(LvMovimentos.SelectedItem + 1).Selected = True
        BL_FimProcessamento Me
        'EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()

End Sub

Sub BD_Insert()
End Sub

Sub FuncaoModificar()
End Sub

Sub BD_Update()
End Sub

Sub FuncaoRemover()
End Sub

Sub BD_Delete()
End Sub




Private Sub ecCodArtigo_Validate(Cancel As Boolean)

    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset

    EcCodArtigo.Text = UCase(EcCodArtigo.Text)
    If Trim(EcCodArtigo) <> "" Then

        sql = "SELECT " & _
              "     descricao " & _
              "FROM " & _
              gBD_PREFIXO_TAB & "stk_artigo " & _
              "WHERE " & _
              "     codigo=" & (EcCodArtigo)

        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao

        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Artigo inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrArtigo = ""
            EcCodArtigo = ""
        Else
            EcDescrArtigo.Text = rsCodigo!descricao
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrArtigo.Text = ""
        EcCodArtigo = ""
    End If

End Sub


Private Sub BtPesqArtigo_Click()



    Dim ChavesPesq(1 To 2) As String

    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String

    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String

    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String

    'Classe
    Dim CamposRetorno As New ClassPesqResultados

    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long

    'Cabe�alhos
    Dim Headers(1 To 2) As String

    'Mensagem do resultado da pesquisa
    Dim mensagem As String

    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean

    Dim resultados(1 To 2) As Variant

    'Defini��o dos campos a retornar
    ChavesPesq(1) = "codigo"
    CamposEcran(1) = "codigo"
    Tamanhos(1) = 2000

    ChavesPesq(2) = "descricao"
    CamposEcran(2) = "descricao"
    Tamanhos(2) = 3000

    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"

    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_stk_Artigo"
    CampoPesquisa1 = "descricao"
    ClausulaWhere = "  "

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Artigos")

    mensagem = "N�o foi encontrada nenhum Artigo."

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodArtigo.Text = resultados(1)
            EcDescrArtigo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
End Sub


Sub Funcao_DataActual()
    On Error GoTo TrataErro

    If Me.ActiveControl.Name = "EcDtIni" Then
        BL_PreencheData EcDtIni, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtFim" Then
        BL_PreencheData EcDtFim, Me.ActiveControl
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_DataActual: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_DataActual"
    Exit Sub
    Resume Next
    
End Sub

Private Sub eccodlote_Validate(Cancel As Boolean)

    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset

    EcCodLote.Text = UCase(EcCodLote.Text)
    If Trim(EcCodLote) <> "" Then

        sql = "SELECT " & _
              "     descricao " & _
              "FROM " & _
              gBD_PREFIXO_TAB & "stk_lotes " & _
              "WHERE " & _
              "     codigo=" & (EcCodLote)

        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao

        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Lote inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrLote = ""
            EcCodLote = ""
        Else
            EcDescrLote.Text = rsCodigo!descricao
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrLote.Text = ""
        EcCodLote = ""
    End If

End Sub


Private Sub BtPesqLote_Click()



    Dim ChavesPesq(1 To 2) As String

    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String

    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String

    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String

    'Classe
    Dim CamposRetorno As New ClassPesqResultados

    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long

    'Cabe�alhos
    Dim Headers(1 To 2) As String

    'Mensagem do resultado da pesquisa
    Dim mensagem As String

    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean

    Dim resultados(1 To 2) As Variant

    'Defini��o dos campos a retornar
    ChavesPesq(1) = "codigo"
    CamposEcran(1) = "codigo"
    Tamanhos(1) = 2000

    ChavesPesq(2) = "descricao"
    CamposEcran(2) = "descricao"
    Tamanhos(2) = 3000

    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"

    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_stk_lotes"
    CampoPesquisa1 = "descricao"
    ClausulaWhere = "  "

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Lote")

    mensagem = "N�o foi encontrada nenhum Lote."

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodLote.Text = resultados(1)
            EcDescrLote.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
End Sub



Private Sub InicializaTreeView()

    With LvMovimentos
        .ColumnHeaders.Add(, , "Sequ�ncial", 1000, lvwColumnLeft).Key = "SEQ_MOVIMENTO"
        .ColumnHeaders.Add(, , "C�digo", 1000, lvwColumnCenter).Key = "COD_LOTE"
        .ColumnHeaders.Add(, , "Descri��o", 4700, lvwColumnCenter).Key = "DESCR_LOTE"
        .ColumnHeaders.Add(, , "Artigo", 1000, lvwColumnCenter).Key = "DESCR_ARTIGO"
        .ColumnHeaders.Add(, , "Tipo", 1000, lvwColumnCenter).Key = "DESCR_TIPO_MOV"
        .ColumnHeaders.Add(, , "Quantidade", 1000, lvwColumnCenter).Key = "QUANTIDADE"
        .ColumnHeaders.Add(, , "Stock Ap�s", 1000, lvwColumnCenter).Key = "STOCL_DISPONIVEL"
        .ColumnHeaders.Add(, , "Data", 1000, lvwColumnCenter).Key = "DT_CRI"
        .ColumnHeaders.Add(, , "Hora", 800, lvwColumnCenter).Key = "HR_CRI"
        .ColumnHeaders.Add(, , "Utilizador", 1000, lvwColumnCenter).Key = "USER_CRI"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .FullRowSelect = True
        .ForeColor = cLightGray
        .MultiSelect = False
    End With
    ColocaCores LvMovimentos, PictureListColor, cLightBlue, cWhite
    Exit Sub

End Sub

Public Sub ColocaCores(lv As ListView, pb As PictureBox, cor1 As Long, Optional cor2 As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo TrataErro
    
    lColor1 = cor1
    lColor2 = cor2
    lv.Picture = LoadPicture("")
    lv.Refresh
    pb.Cls
    pb.AutoRedraw = True
    pb.BorderStyle = vbBSNone
    pb.ScaleMode = vbTwips
    pb.Visible = False
    lv.PictureAlignment = lvwTile
    pb.Font = lv.Font
    pb.top = lv.top
    pb.Font = lv.Font
    With pb.Font
        .Size = lv.Font.Size + 1
        .Bold = lv.Font.Bold
        .Charset = lv.Font.Charset
        .Italic = lv.Font.Italic
        .Name = lv.Font.Name
        .Strikethrough = lv.Font.Strikethrough
        .Underline = lv.Font.Underline
        .Weight = lv.Font.Weight
    End With
    pb.Refresh
    lLineHeight = pb.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = lv.Width
    pb.Height = lBarHeight * 2
    pb.Width = lBarWidth
    pb.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    pb.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    pb.AutoSize = True
    lv.Picture = pb.Image
    lv.Refresh
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "Erro: " & Err.Description, Me.Name, "ColocaCores", False
    Exit Sub
End Sub

Private Sub PreencheLV()
    Dim i As Integer
    Dim j As Integer
    LvMovimentos.ListItems.Clear
    totalMovimento = 0
    ReDim estrutMov(0)
    MarcaInicial = rs.Bookmark
    While Not rs.EOF
        totalMovimento = totalMovimento + 1
        ReDim Preserve estrutMov(totalMovimento)
        estrutMov(totalMovimento).seq_movimento = BL_HandleNull(rs!seq_movimento, 0)
        estrutMov(totalMovimento).cod_lote = BL_HandleNull(rs!cod_lote, "")
        estrutMov(totalMovimento).descr_lote = BL_HandleNull(rs!descr_lote, "")
        estrutMov(totalMovimento).cod_artigo = BL_HandleNull(rs!cod_artigo, "")
        estrutMov(totalMovimento).descr_artigo = BL_HandleNull(rs!descr_artigo, "")
        estrutMov(totalMovimento).tipo_movimento = BL_HandleNull(rs!tipo_movimento, -1)
        estrutMov(totalMovimento).descr_tipo_mov = BL_HandleNull(rs!descr_tipo_mov, -1)
        estrutMov(totalMovimento).ent_saida = BL_HandleNull(rs!tipo, -1)
        estrutMov(totalMovimento).transferencia = BL_HandleNull(rs!transferencia, -1)
        estrutMov(totalMovimento).quantidade = BL_HandleNull(rs!quantidade, 0)
        estrutMov(totalMovimento).stock_disponivel = BL_HandleNull(rs!qtd_disponivel, 0)
        estrutMov(totalMovimento).user_cri = BL_HandleNull(rs!user_cri, "")
        estrutMov(totalMovimento).dt_cri = BL_HandleNull(rs!dt_cri, "")
        estrutMov(totalMovimento).hr_cri = BL_HandleNull(rs!hr_cri, "")
        estrutMov(totalMovimento).observacao = BL_HandleNull(rs!observacao, "")
        If estrutMov(totalMovimento).ent_saida = 2 Then
            estrutMov(totalMovimento).cor = cRed
        ElseIf estrutMov(totalMovimento).ent_saida = 1 Then
            If estrutMov(totalMovimento).transferencia = 1 Then
                estrutMov(totalMovimento).cor = cYellow
            Else
                estrutMov(totalMovimento).cor = cGreen
            End If
        End If
        
        With LvMovimentos.ListItems.Add(totalMovimento, "KEY_" & estrutMov(totalMovimento).seq_movimento, CStr(estrutMov(totalMovimento).seq_movimento))
            .ListSubItems.Add , , estrutMov(totalMovimento).cod_lote
            .ListSubItems.Add , , estrutMov(totalMovimento).descr_lote
            .ListSubItems.Add , , estrutMov(totalMovimento).descr_artigo
            .ListSubItems.Add , , estrutMov(totalMovimento).descr_tipo_mov
            .ListSubItems.Add , , estrutMov(totalMovimento).quantidade
            .ListSubItems.Add , , estrutMov(totalMovimento).stock_disponivel
            .ListSubItems.Add , , estrutMov(totalMovimento).dt_cri
            .ListSubItems.Add , , estrutMov(totalMovimento).hr_cri
            .ListSubItems.Add , , estrutMov(totalMovimento).user_cri
        End With
        For i = 1 To totalMovimento
            For j = 1 To LvMovimentos.ListItems(i).ListSubItems.Count
                LvMovimentos.ListItems(i).ListSubItems(j).ForeColor = estrutMov(i).cor
                Select Case j
                    Case 2: LvMovimentos.ListItems(i).ListSubItems(j).ToolTipText = estrutMov(i).descr_lote
                    Case 3: LvMovimentos.ListItems(i).ListSubItems(j).ToolTipText = estrutMov(i).descr_artigo
                    Case 4: LvMovimentos.ListItems(i).ListSubItems(j).ToolTipText = estrutMov(i).descr_tipo_mov
                End Select
            Next
        Next
        rs.MoveNext
    Wend
    rs.MoveFirst
    'LvMovimentos.SetFocus
End Sub


Private Sub LimpaFrameMovimentos()
    EcCodArtigo2 = ""
    EcDescrArtigo2 = ""
    EcCodLote2 = ""
    EcDescrLote2 = ""
    CbMovimento2.ListIndex = mediComboValorNull
    EcQuantidade = ""
    EcStockActual = ""
    EcStockMin = ""
End Sub
Private Sub ecCodLote2_Validate(Cancel As Boolean)

    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset

    EcCodLote2.Text = UCase(EcCodLote2.Text)
    If Trim(EcCodLote2) <> "" Then

        sql = "SELECT " & _
              "     descricao " & _
              "FROM " & _
              gBD_PREFIXO_TAB & "stk_lotes " & _
              "WHERE " & _
              "     codigo=" & (EcCodLote2)
        If EcCodArtigo2 <> "" Then
            sql = sql & "  AND cod_artigo = " & EcCodArtigo2
        End If
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao

        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Lote inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrLote2 = ""
            EcCodLote2 = ""
        Else
            EcDescrLote2.Text = rsCodigo!descricao
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrLote2.Text = ""
        EcCodLote2 = ""
    End If
    PreencheDadosLote EcCodLote2

End Sub



Private Sub BtPesqLote2_Click()



    Dim ChavesPesq(1 To 2) As String

    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String

    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String

    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String

    'Classe
    Dim CamposRetorno As New ClassPesqResultados

    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long

    'Cabe�alhos
    Dim Headers(1 To 2) As String

    'Mensagem do resultado da pesquisa
    Dim mensagem As String

    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean

    Dim resultados(1 To 2) As Variant

    'Defini��o dos campos a retornar
    ChavesPesq(1) = "codigo"
    CamposEcran(1) = "codigo"
    Tamanhos(1) = 2000

    ChavesPesq(2) = "descricao"
    CamposEcran(2) = "descricao"
    Tamanhos(2) = 3000

    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"

    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_stk_lotes"
    CampoPesquisa1 = "descricao"
    If EcCodArtigo2 <> "" Then
        ClausulaWhere = "  cod_artigo = " & EcCodArtigo2
    Else
        ClausulaWhere = ""
    End If

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Lote")

    mensagem = "N�o foi encontrada nenhum Lote."

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodLote2.Text = resultados(1)
            EcDescrLote2.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    PreencheDadosLote EcCodLote2
End Sub

Private Sub PreencheDadosLote(cod_lote As String)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsLote As New ADODB.recordset
    
    EcCodArtigo2 = ""
    EcDescrArtigo2 = ""
    EcQuantidade = "0"
    EcStockActual = "0"
    If cod_lote <> "" Then
        sSql = "SELECT x1.cod_artigo, x1.decremento,x1.stock_actual, x2.descricao,x1.stock_minimo FROM sl_stk_lotes x1, sl_stk_artigo x2 "
        sSql = sSql & " WHERE x1.codigo = " & cod_lote & " AND x1.cod_artigo = x2.codigo "
        rsLote.CursorLocation = adUseServer
        rsLote.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsLote.Open sSql, gConexao
        If rsLote.RecordCount = 1 Then
            EcCodArtigo2 = BL_HandleNull(rsLote!cod_artigo, "")
            EcDescrArtigo2 = BL_HandleNull(rsLote!descricao, "")
            EcQuantidade = BL_HandleNull(rsLote!decremento, 0)
            EcStockActual = BL_HandleNull(rsLote!stock_actual, 0)
            stock_antes = EcStockActual
            EcStockMin = BL_HandleNull(rsLote!stock_minimo, 0)
        End If
        rsLote.Close
        Set rsLote = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FormStkMovimentos: PreencheDadosLote: " & Err.Description, Me.Name, "PreencheDadosLote", True
    Exit Sub
    Resume Next
End Sub

    

Private Function ConstroiCriterio() As String
    Dim sSql As String
    On Error GoTo TrataErro
    sSql = "SELECT x1.seq_movimento, x1.cod_lote, x2.descricao descr_lote, x1.cod_artigo, x3.descricao descr_artigo, x1.tipo_movimento, x4.descricao descr_tipo_mov, "
    sSql = sSql & " x4.tipo, x4.transferencia, x1.quantidade, x1.qtd_disponivel, x1.user_cri, x1.dt_cri, x1.hr_Cri, "
    sSql = sSql & " x1.observacao "
    sSql = sSql & " FROM sl_stk_movimento x1, sl_stk_lotes x2, sl_stk_artigo x3, sl_stk_tipos_mov x4 "
    sSql = sSql & " WHERE x1.cod_lote = x2.codigo AND x1.cod_artigo = x3.codigo AND x1.tipo_movimento = x4.codigo "
    
    If EcCodLote <> "" Then
        sSql = sSql & " AND x2.codigo = " & EcCodLote
    End If
    If EcCodArtigo <> "" Then
        sSql = sSql & " AND x3.codigo = " & EcCodArtigo
    End If
    If EcCodTipo <> "" Then
        sSql = sSql & " AND x3.cod_tipo = " & EcCodTipo
    End If
    If CbMovimento.ListIndex > mediComboValorNull Then
        sSql = sSql & " AND x4.codigo= " & CbMovimento.ItemData(CbMovimento.ListIndex)
    End If
    If EcDtIni <> "" And EcDtFim <> "" Then
        sSql = sSql & " AND x1.dt_cri BETWEEN  " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    End If
    sSql = sSql & " ORDER BY x1.dt_cri, x1.hr_cri "
    ConstroiCriterio = sSql
Exit Function
TrataErro:
    ConstroiCriterio = ""
    BG_LogFile_Erros "FormStkMovimentos: ConstroiCriterio: " & Err.Description, Me.Name, "ConstroiCriterio", True
    Exit Function
    Resume Next
End Function

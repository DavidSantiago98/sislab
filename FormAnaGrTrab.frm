VERSION 5.00
Begin VB.Form FormAnaGrTrab 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAnaGrTrab"
   ClientHeight    =   5625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8880
   Icon            =   "FormAnaGrTrab.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5625
   ScaleWidth      =   8880
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPesquisaSel 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   5400
      TabIndex        =   23
      Top             =   1440
      Width           =   3345
   End
   Begin VB.CommandButton BtCopiaDadosAna 
      Height          =   360
      Left            =   8400
      Picture         =   "FormAnaGrTrab.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   22
      ToolTipText     =   "Copia An�lises de outro Grupo de Trabalho"
      Top             =   1080
      Width           =   375
   End
   Begin VB.CommandButton BtBaixo 
      Height          =   495
      Left            =   4800
      Style           =   1  'Graphical
      TabIndex        =   21
      ToolTipText     =   " Alterar ordem na Complexa "
      Top             =   4080
      Width           =   495
   End
   Begin VB.CommandButton BtCima 
      Height          =   495
      Left            =   4800
      Style           =   1  'Graphical
      TabIndex        =   20
      ToolTipText     =   " Alterar ordem na Complexa "
      Top             =   2040
      Width           =   495
   End
   Begin VB.ListBox EcCodAnalise 
      Height          =   1620
      Left            =   120
      TabIndex        =   19
      Top             =   3360
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.ListBox EcListaPerfis 
      Appearance      =   0  'Flat
      Height          =   3540
      Left            =   1560
      TabIndex        =   8
      Top             =   1800
      Width           =   3105
   End
   Begin VB.Frame Tipoanalise 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1515
      Left            =   120
      TabIndex        =   12
      ToolTipText     =   "Tipo de an�lise a pesquisar."
      Top             =   1680
      Width           =   1275
      Begin VB.OptionButton OpP 
         Caption         =   "Perfis"
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   720
         Width           =   975
      End
      Begin VB.PictureBox Lupa 
         BorderStyle     =   0  'None
         Height          =   495
         Left            =   360
         Picture         =   "FormAnaGrTrab.frx":0396
         ScaleHeight     =   495
         ScaleWidth      =   495
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   960
         Width           =   495
      End
      Begin VB.OptionButton opC 
         Caption         =   "Complexas"
         Height          =   195
         Left            =   120
         TabIndex        =   3
         Top             =   480
         Width           =   1125
      End
      Begin VB.OptionButton opS 
         Caption         =   "Simples"
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin VB.CommandButton BtRetira 
      Height          =   495
      Left            =   4780
      Picture         =   "FormAnaGrTrab.frx":11D8
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   3360
      Width           =   495
   End
   Begin VB.CommandButton BtInsere 
      Height          =   495
      Left            =   4780
      Picture         =   "FormAnaGrTrab.frx":14E2
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   2760
      Width           =   495
   End
   Begin VB.TextBox EcPesquisa 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1560
      TabIndex        =   5
      Top             =   1440
      Width           =   3105
   End
   Begin VB.Frame Frame1 
      Caption         =   "Grupo de Trabalho"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   120
      TabIndex        =   14
      Top             =   0
      Width           =   8625
      Begin VB.ComboBox EcDescricaoCombo 
         Height          =   315
         Left            =   3780
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   450
         Width           =   4755
      End
      Begin VB.TextBox EcCodigo 
         Height          =   285
         Left            =   1080
         TabIndex        =   0
         Top             =   450
         Width           =   1545
      End
      Begin VB.Label Label1 
         Caption         =   "Descri��o"
         Height          =   255
         Left            =   2880
         TabIndex        =   16
         Top             =   500
         Width           =   915
      End
      Begin VB.Label Label3 
         Caption         =   "C�digo"
         Height          =   255
         Left            =   210
         TabIndex        =   15
         Top             =   500
         Width           =   615
      End
   End
   Begin VB.ListBox EcListaSimples 
      Height          =   3570
      Left            =   1560
      TabIndex        =   7
      Top             =   1800
      Width           =   3105
   End
   Begin VB.ListBox EcListaComplexa 
      Height          =   3570
      Left            =   1560
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1800
      Width           =   3105
   End
   Begin VB.ListBox EcListaSC 
      Appearance      =   0  'Flat
      Height          =   3540
      Left            =   5400
      TabIndex        =   11
      Top             =   1800
      Width           =   3345
   End
   Begin VB.Label Label4 
      Caption         =   "An�lises do Grupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5450
      TabIndex        =   18
      Top             =   1200
      Width           =   2055
   End
   Begin VB.Label Label2 
      Caption         =   "Digite o texto que pretende pesquisar"
      Height          =   255
      Left            =   1560
      TabIndex        =   17
      Top             =   1200
      Width           =   2955
   End
End
Attribute VB_Name = "FormAnaGrTrab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 29/04/2003
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String
Dim CriterioTabelaD As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox As Variant
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim ListaOrigem As Control
Dim ListaDestino As Control
    
Public rs As ADODB.recordset
Public RSListaDestino As ADODB.recordset
'Public RSListaOrigemS As ADODB.Recordset
'Public RSListaOrigemC As ADODB.Recordset
'Public RSListaOrigemP As ADODB.Recordset

Dim disposicao As Integer


Sub RefinaPesquisa()
    
    Dim i As Integer
    
    Dim CampoCriterio As String
    Dim sql As String
    Dim SQLCriterioAux As String
    Dim RegAnalise As ADODB.recordset
    Dim NomeControl As Control
    Dim total As Integer
    
    
    On Error GoTo Trata_Erro
    

    If opS.value = True Then
        Set NomeControl = EcListaSimples
        CampoCriterio = "descr_ana_s"
        sql = "SELECT seq_ana_s Sequencial,cod_ana_s Codigo, descr_ana_s Descricao " & _
              "FROM sl_ana_s "
    Else
        If opC.value = True Then
            Set NomeControl = EcListaComplexa
            CampoCriterio = "descr_ana_c"
            sql = "SELECT seq_ana_c Sequencial,cod_ana_c Codigo, descr_ana_c Descricao " & _
                  "FROM sl_ana_c "
        Else
            If OpP.value = True Then
                Set NomeControl = EcListaPerfis
                CampoCriterio = "descr_perfis"
                sql = "SELECT seq_perfis Sequencial,cod_perfis Codigo, descr_perfis Descricao " & _
                      "FROM sl_perfis "
                SQLCriterioAux = " AND flg_activo=1"
            End If
        End If
    End If
    sql = sql & " WHERE UPPER(" & CampoCriterio & ") LIKE '" & Trim(UCase(BG_CvWilcard(EcPesquisa))) & IIf(BG_CvWilcard(EcPesquisa) <> "%", "%'", "") & _
                SQLCriterioAux & _
                " ORDER BY Descricao"
    
    Set RegAnalise = New ADODB.recordset
    RegAnalise.ActiveConnection = gConexao
    RegAnalise.CursorLocation = adUseServer
    RegAnalise.CursorType = adOpenStatic
    RegAnalise.LockType = adLockReadOnly
    RegAnalise.Open sql
    total = RegAnalise.RecordCount
    
    NomeControl.ListIndex = mediComboValorNull
    NomeControl.Clear
    EcCodAnalise.Clear
    
    For i = 0 To total - 1
        'soliveira terrugem
        NomeControl.AddItem Trim("" & RegAnalise!Codigo) & Space(8 - Len(Trim("" & RegAnalise!Codigo)) + 1) & Trim("" & RegAnalise!descricao) & Space(80 - Len(Trim("" & RegAnalise!descricao)) + 1)
        
        'A ser usado para as an�lises do tipo simples
        NomeControl.ItemData(i) = CInt(RegAnalise!sequencial)
        'A ser usado para as an�lises do tipo Perfis e Complexas
        EcCodAnalise.AddItem "" & RegAnalise!Codigo
        
        RegAnalise.MoveNext
    Next i
    
    RegAnalise.Close
    Set RegAnalise = Nothing
        
    Exit Sub
        
Trata_Erro:
    
    Err.Clear
    If Not RegAnalise Is Nothing Then
        If RegAnalise.state <> adStateClosed Then
            RegAnalise.Close
        End If
        Set RegAnalise = Nothing
    End If
    
End Sub




Private Sub BtBaixo_Click()
    Dim sdummy As String
    Dim ldummy As Long
    
    If EcListaSC.ListIndex < EcListaSC.ListCount - 1 Then
        sdummy = EcListaSC.List(EcListaSC.ListIndex + 1)
        ldummy = EcListaSC.ItemData(EcListaSC.ListIndex + 1)
        EcListaSC.List(EcListaSC.ListIndex + 1) = EcListaSC.List(EcListaSC.ListIndex)
        EcListaSC.ItemData(EcListaSC.ListIndex + 1) = EcListaSC.ItemData(EcListaSC.ListIndex)
        EcListaSC.List(EcListaSC.ListIndex) = sdummy
        EcListaSC.ItemData(EcListaSC.ListIndex) = ldummy
    End If
End Sub

Private Sub BtCima_Click()
    Dim sdummy As String
    Dim ldummy As Long
    
    If EcListaSC.ListIndex > 0 Then
        sdummy = EcListaSC.List(EcListaSC.ListIndex - 1)
        ldummy = EcListaSC.ItemData(EcListaSC.ListIndex - 1)
        EcListaSC.List(EcListaSC.ListIndex - 1) = EcListaSC.List(EcListaSC.ListIndex)
        EcListaSC.ItemData(EcListaSC.ListIndex - 1) = EcListaSC.ItemData(EcListaSC.ListIndex)
        EcListaSC.List(EcListaSC.ListIndex) = sdummy
        EcListaSC.ItemData(EcListaSC.ListIndex) = ldummy
        EcListaSC.ListIndex = (EcListaSC.ListIndex - 1)
    End If
End Sub

Private Sub BtCopiaDadosAna_Click()
    Dim sSql As String
    Dim rsTrab As New ADODB.recordset
    
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_trab"
    CamposEcran(1) = "cod_gr_trab"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_trab"
    CamposEcran(2) = "descr_gr_trab"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_trab"
    CampoPesquisa1 = "descr_gr_trab"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupo de Trabalho")
    
    mensagem = "N�o foi encontrada nenhum Grupo de Trabalho."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            sSql = "SELECT x1.cod_analise, x2.descr_ana, x2.seq_ana FROM sl_ana_Trab x1, slv_analises x2 WHERE x1.cod_analise =X2.cod_ana AND x1.cod_gr_trab = " & BL_TrataStringParaBD(CStr(resultados(1)))
            sSql = sSql & " ORDER by x1.ordem "
            rsTrab.ActiveConnection = gConexao
            rsTrab.CursorType = adOpenStatic
            rsTrab.CursorLocation = adUseServer
            rsTrab.LockType = adLockReadOnly
            rsTrab.Open sSql
            If rsTrab.RecordCount > 0 Then
                While Not rsTrab.EOF
                    EcListaSC.AddItem BL_HandleNull(rsTrab!cod_analise, "") & "   " & BL_HandleNull(rsTrab!descr_ana, "")
                    EcListaSC.ItemData(EcListaSC.NewIndex) = rsTrab!seq_ana
                    rsTrab.MoveNext
                Wend
            End If
            rsTrab.Close
            Set rsTrab = Nothing
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
End Sub

Private Sub BtInsere_Click()

    Dim CmdAnaSimples As ADODB.Command
    Dim CmdAnaComplexas As ADODB.Command
    Dim CmdAnaPerfis As ADODB.Command
    Dim RegCmdPerfis As ADODB.recordset
    Dim RegCmdComplexas As ADODB.recordset
    Dim RegCmdSimples As ADODB.recordset
                        
    
'    'Verifica se o n� de an�lises m�ximo foi atingido (13)
'    If Modelo <> gT_Livre And EcListaSC.ListCount = 13 Then
'        Call BG_Mensagem(mediMsgBox, "N� de an�lises m�ximo permitido para o modelo Horizontal\Vertical � 13!", vbExclamation + vbOKOnly, App.ProductName)
'        Exit Sub
'    End If
    
    
    'An�lises Simples
    If opS.value = True Then
        Set ListaOrigem = EcListaSimples
    Else
        If opC.value = True Then
            Set ListaOrigem = EcListaComplexa
        Else
            Set ListaOrigem = EcListaPerfis
        End If
        
        'Comando com as an�lises Simples
        Set CmdAnaSimples = New ADODB.Command
        CmdAnaSimples.ActiveConnection = gConexao
        CmdAnaSimples.CommandType = adCmdText
        CmdAnaSimples.CommandText = "SELECT seq_ana_s,cod_ana_s ,descr_ana_s " & _
                                    "FROM sl_ana_s " & _
                                    "WHERE " & _
                                    "sl_ana_s.cod_ana_s=?"
        CmdAnaSimples.Prepared = True
        CmdAnaSimples.Parameters.Append CmdAnaSimples.CreateParameter("COD_ANALISE", adVarChar, adParamInput, 8)
        
        'Comando com as an�lises Complexas
        Set CmdAnaComplexas = New ADODB.Command
        CmdAnaComplexas.ActiveConnection = gConexao
        CmdAnaComplexas.CommandType = adCmdText
        'soliveira altera��o
        'em vez de ir buscar os membros da complexa selecciona os dados da propria complexa
        CmdAnaComplexas.CommandText = "SELECT seq_ana_c,cod_ana_c,descr_ana_c " & _
                                      "FROM sl_ana_c " & _
                                      "WHERE " & _
                                      "sl_ana_c.cod_ana_c=?"
'        CmdAnaComplexas.CommandText = "SELECT cod_membro " & _
'                                      "FROM sl_membro " & _
'                                      "WHERE " & _
'                                      "sl_membro.cod_ana_c=? " & _
'                                      "ORDER BY sl_membro.ordem"
        CmdAnaComplexas.Prepared = True
        CmdAnaComplexas.Parameters.Append CmdAnaComplexas.CreateParameter("COD_ANALISE", adVarChar, adParamInput, 8)
        
        'Comando com as an�lises Perfis
        Set CmdAnaPerfis = New ADODB.Command
        CmdAnaPerfis.ActiveConnection = gConexao
        CmdAnaPerfis.CommandType = adCmdText
        CmdAnaPerfis.CommandText = "SELECT cod_analise " & _
                                   "FROM sl_ana_perfis " & _
                                   "WHERE " & _
                                   "sl_ana_perfis.cod_perfis=? " & _
                                   "ORDER BY sl_ana_perfis.ordem"
        CmdAnaPerfis.Prepared = True
        CmdAnaPerfis.Parameters.Append CmdAnaPerfis.CreateParameter("COD_ANALISE", adVarChar, adParamInput, 8)

    End If
    Set ListaDestino = EcListaSC
    
    
    'Verifica se j� escolheu o tipo de an�lise
    If ListaOrigem Is Nothing Then
        Exit Sub
    Else
        If ListaOrigem.ListIndex = -1 Then
            Exit Sub
        Else
            If ListaOrigem.Selected(ListaOrigem.ListIndex) = False Then
                Exit Sub
            End If
        End If
    End If
    
    
    
    'Passa os itens seleccionados entre as listas
    If opS.value = True Then
        'Passa o C�digo e descri��o da an�lise
        'Actualiza o ItemData da Lista de destino com o sequencial da an�lise
        Call BG_PassaElementoEntreListas(ListaOrigem, ListaDestino, ListaOrigem.ListIndex, False, False)
    Else
        'An�lises Perfis
        If OpP.value = True Then
            Call BG_PassaElementoEntreListas(ListaOrigem, ListaDestino, ListaOrigem.ListIndex, False, False)
            
'            CmdAnaPerfis.Parameters("COD_ANALISE").Value = Trim(EcCodAnalise.Text)
'            Set RegCmdPerfis = CmdAnaPerfis.Execute
'            While Not RegCmdPerfis.EOF
'                'An�lises complexas do Perfil
'                CmdAnaComplexas.Parameters("COD_ANALISE").Value = "" & RegCmdPerfis!cod_analise
'                Set RegCmdComplexas = CmdAnaComplexas.Execute
'                If Not RegCmdComplexas.EOF Then
'                    While Not RegCmdComplexas.EOF
'                        'An�lises simples da Complexa do Perfil
'                        CmdAnaSimples.Parameters("COD_ANALISE").Value = "" & RegCmdComplexas!cod_membro
'                        Set RegCmdSimples = CmdAnaSimples.Execute
'                        If Not RegCmdSimples.EOF Then
'                            Call PassaElementos("" & RegCmdSimples!cod_ana_s, "" & RegCmdSimples!Descr_Ana_S, RegCmdSimples!seq_ana_s)
'                        End If
'                        RegCmdSimples.Close
'                        Set RegCmdSimples = Nothing
'                        RegCmdComplexas.MoveNext
'                    Wend
'                Else
'                    'An�lises simples do Perfil
'                    CmdAnaSimples.Parameters("COD_ANALISE").Value = "" & RegCmdPerfis!cod_analise
'                    Set RegCmdSimples = CmdAnaSimples.Execute
'                    If Not RegCmdSimples.EOF Then
'                        Call PassaElementos("" & RegCmdSimples!cod_ana_s, "" & RegCmdSimples!Descr_Ana_S, RegCmdSimples!seq_ana_s)
'                    End If
'                    RegCmdSimples.Close
'                    Set RegCmdSimples = Nothing
'                End If
'                RegCmdComplexas.Close
'                Set RegCmdComplexas = Nothing
'                RegCmdPerfis.MoveNext
'            Wend
'            RegCmdPerfis.Close
'            Set RegCmdPerfis = Nothing
        Else
            'An�lises Complexas
            'soliveira altera��o
            'em vez de passar as simples da complexa passa directamente a complexa
            Call BG_PassaElementoEntreListas(ListaOrigem, ListaDestino, ListaOrigem.ListIndex, False, False)
'            CmdAnaComplexas.Parameters("COD_ANALISE").Value = Trim(EcCodAnalise.Text)
'            Set RegCmdComplexas = CmdAnaComplexas.Execute
'            While Not RegCmdComplexas.EOF
'                'An�lises simples da Complexa
'                CmdAnaSimples.Parameters("COD_ANALISE").Value = "" & RegCmdComplexas!cod_membro
'                Set RegCmdSimples = CmdAnaSimples.Execute
'                If Not RegCmdSimples.EOF Then
'                    'SIMPLES
'                    Call PassaElementos("" & RegCmdSimples!cod_ana_s, "" & RegCmdSimples!Descr_Ana_S, RegCmdSimples!seq_ana_s)
'                End If
'                RegCmdSimples.Close
'                Set RegCmdSimples = Nothing
'                RegCmdComplexas.MoveNext
'            Wend
'            RegCmdComplexas.Close
'            Set RegCmdComplexas = Nothing
        End If
    End If
    
    ListaOrigem.Selected(ListaOrigem.ListIndex) = False
        
    
    Set CmdAnaSimples = Nothing
    Set CmdAnaComplexas = Nothing
    Set CmdAnaPerfis = Nothing
    
End Sub

Private Sub BtRetira_Click()

    Set ListaDestino = EcListaSC
    If ListaDestino.ListIndex <> -1 Then
        ListaDestino.RemoveItem (ListaDestino.ListIndex)
    End If
    
End Sub

Private Sub EcCodigo_Change()
        
    opS.Enabled = True
    opC.Enabled = True
    OpP.Enabled = True
    
    opS.value = False
    opC.value = False
    OpP.value = False
    
    
End Sub

Private Sub EcCodigo_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodigo)
    
    If Cancel = False Then
        EcCodigo.Text = UCase(EcCodigo.Text)
        BL_ColocaTextoCombo "sl_gr_trab", "seq_gr_trab", "cod_gr_trab", EcCodigo, EcDescricaoCombo
    End If
    
End Sub

Private Sub EcDescricaoCombo_Click()

    BL_ColocaComboTexto "sl_gr_trab", "seq_gr_trab", "cod_gr_trab", EcCodigo, EcDescricaoCombo
    
End Sub

Private Sub EcDescricaoCombo_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then EcDescricaoCombo.ListIndex = -1
    
End Sub


Private Sub EcListaPerfis_Click()
    
    EcCodAnalise.ListIndex = EcListaPerfis.ListIndex
    EcListaPerfis.ToolTipText = Trim(EcListaPerfis.List(EcListaPerfis.ListIndex))
    
End Sub

Private Sub EcListaSC_Click()

    EcListaSC.ToolTipText = Trim(EcListaSC.List(EcListaSC.ListIndex))
    
End Sub
Private Sub EcListaComplexa_Click()
    
    EcCodAnalise.ListIndex = EcListaComplexa.ListIndex
    EcListaComplexa.ToolTipText = Trim(EcListaComplexa.List(EcListaComplexa.ListIndex))
    
End Sub

Private Sub EcListaSimples_Click()
    
    EcCodAnalise.ListIndex = EcListaSimples.ListIndex
    EcListaSimples.ToolTipText = Trim(EcListaSimples.List(EcListaSimples.ListIndex))
    
End Sub

Private Sub EcPesquisa_Change()
    
    Call RefinaPesquisa
    
End Sub

Private Sub EcPesquisaSel_Change()
    Dim i As Integer
    EcListaSC.ListIndex = mediComboValorNull
    EcListaSC.TopIndex = 0
    
    For i = 0 To EcListaSC.ListCount - 1
        If InStr(1, UCase(EcListaSC.List(i)), UCase(EcPesquisaSel)) > 0 Then
            EcListaSC.ListIndex = i
            EcListaSC.Selected(i) = True
            EcListaSC.TopIndex = i
            Exit For
        End If
    Next
End Sub

Sub Form_Load()

    Call EventoLoad
    
End Sub

Sub Form_Activate()

    Call EventoActivate
    
End Sub

Sub Form_Unload(Cancel As Integer)

    Call EventoUnload
    
End Sub

Sub Inicializacoes()

    Me.caption = " An�lises de um grupo de trabalho"
    Me.left = 540
    Me.top = 450
    Me.Width = 8975
    Me.Height = 6135 ' Normal
    'Me.Height = 8825 ' Campos Extras
    
    NomeTabela = "sl_ana_trab"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 1
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "cod_gr_trab"
       
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "C�digo do Grupo de Trabalho"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_gr_trab"
    Set ChaveEc = EcCodigo
    
    CamposBDparaListBox = Array("cod_analise", "descricao")

    opC.value = False
    opS.value = False
    OpP.value = False
    NumEspacos = Array(6, 40)
    
End Sub


Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    
    BG_ParametrizaPermissoes_ADO Me.Name
    
    opC.value = False
    opS.value = False
    OpP.value = False
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormAnaGrTrab = Nothing
    
End Sub

Sub LimpaCampos()

    Me.SetFocus
        
    BG_LimpaCampo_Todos CamposEc
    opS.Enabled = False
    opC.Enabled = False
    OpP.Enabled = False
    EcListaSC.Clear
    EcListaComplexa.Clear
    EcListaPerfis.Clear
    EcListaSimples.Clear
    EcCodAnalise.Clear
    EcDescricaoCombo.ListIndex = mediComboValorNull
    
End Sub

Sub DefTipoCampos()

    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
End Sub
Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_gr_trab", "seq_gr_trab", "descr_gr_trab", EcDescricaoCombo
    
    'Traz o c�digo do form de grupos de trabalho
    'caso a chamada a este form tenha sido efectuada de l�
    If gF_GRUPOSTRAB = 1 Then
        EcCodigo = Trim(FormGruposTrab.EcCodigo)
        If EcCodigo <> "" Then BL_ColocaTextoCombo "sl_gr_trab", "seq_gr_trab", "cod_gr_trab", EcCodigo, EcDescricaoCombo
    End If

End Sub
Sub PreencheCampos()

    Dim ii As Integer
    
    Me.SetFocus

    BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
    
    If Trim(EcCodigo) <> "" Then
        BL_ColocaTextoCombo "sl_gr_trab", "seq_gr_trab", "cod_gr_trab", EcCodigo, EcDescricaoCombo
    End If

    Set RSListaDestino = New ADODB.recordset
    RSListaDestino.CursorType = adOpenStatic
    RSListaDestino.CursorLocation = adUseServer
    
    'Selecciona as an�lises do Grupo de Trabalho (an�lises simples)
    CriterioTabelaD = " SELECT cod_analise, seq_ana_s as sequencia, descr_ana_s as descricao, sl_ana_trab.ordem " & _
                      " FROM sl_ana_trab, sl_ana_s " & _
                      " WHERE " & _
                      " cod_gr_trab = '" & Trim(EcCodigo) & "' AND " & _
                      " cod_ana_s = cod_analise " & _
                      " UNION " & _
                      " SELECT cod_analise, seq_ana_c as sequencia, descr_ana_c as descricao, sl_ana_trab.ordem " & _
                      " FROM sl_ana_trab, sl_ana_c " & _
                      " WHERE " & _
                      " cod_gr_trab = '" & Trim(EcCodigo) & "' AND " & _
                      " cod_ana_c = cod_analise " & _
                      " UNION " & _
                      " SELECT cod_analise, seq_perfis as sequencia, descr_perfis as descricao, sl_ana_trab.ordem " & _
                      " FROM sl_ana_trab, sl_perfis " & _
                      " WHERE " & _
                      " cod_gr_trab = '" & Trim(EcCodigo) & "' AND " & _
                      " cod_perfis = cod_analise "
    If gSGBD = gOracle Then
        CriterioTabelaD = CriterioTabelaD & " ORDER BY ORDEM"
    ElseIf gSGBD = gSqlServer Then
        CriterioTabelaD = CriterioTabelaD & " ORDER BY sl_ana_trab.ORDEM"
    End If
                      
                        
    RSListaDestino.Open CriterioTabelaD, gConexao

    If RSListaDestino.RecordCount > 0 Then
        estado = 2
        'Inicio do preenchimento de 'EcLista'=>An�lises simples
        'C�digo da an�lise e descri��o
        'ItemData � o sequencial da an�lise para saber qual o c�digo
        BL_PreencheListBoxMultipla_ADO EcListaSC, RSListaDestino, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT", , "sequencia"
    End If
    
    RSListaDestino.Close
    Set RSListaDestino = Nothing
    
End Sub
Sub FuncaoLimpar()

    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        BL_ToolbarEstadoN estado
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
    
End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcListaSC.Clear
        
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
        
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If
    
End Sub

Function ValidaCamposEc() As Integer

    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Public Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    If Trim(EcCodigo) = "" Then
        CriterioTabela = "SELECT " & _
                         "      cod_gr_trab " & _
                         "FROM " & _
                         "      sl_ana_trab " & _
                         "GROUP BY cod_gr_trab " & _
                         "ORDER BY cod_gr_trab ASC"
    Else
        CriterioTabela = "SELECT " & _
                         "  cod_gr_trab " & _
                         "FROM sl_ana_trab WHERE cod_gr_trab ='" & Trim(EcCodigo) & "'"
    End If
    
    rs.ActiveConnection = gConexao
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.LockType = adLockReadOnly
    rs.Open CriterioTabela
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub
Sub FuncaoAnterior()

    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub
Sub FuncaoSeguinte()

    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub


Sub FuncaoModificar()

    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim SQLAux As String
    Dim SQLAux2 As String
    Dim MarcaLocal As Variant
    Dim i As Integer
    Dim rsAux As ADODB.recordset
        
    SQLAux2 = "DELETE FROM " & NomeTabela & " WHERE cod_gr_trab='" & EcCodigo & "'"
    BG_ExecutaQuery_ADO SQLAux2, -1
    
    Set rsAux = New ADODB.recordset
    rsAux.CursorLocation = adUseServer
    
    For i = 0 To EcListaSC.ListCount - 1
        '---------------------------------------------------------------------------------------
        If left(EcListaSC.List(i), 1) = "P" Then
            SQLAux = "SELECT cod_perfis cod_ana FROM sl_perfis WHERE seq_perfis=" & Trim(EcListaSC.ItemData(i))
        ElseIf left(EcListaSC.List(i), 1) = "C" Then
            SQLAux = "SELECT cod_ana_c cod_ana FROM sl_ana_c WHERE seq_ana_c=" & Trim(EcListaSC.ItemData(i))
        Else
        '---------------------------------------------------------------------------------------
            SQLAux = "SELECT cod_ana_s cod_ana FROM sl_ana_s WHERE seq_ana_s=" & Trim(EcListaSC.ItemData(i))
        End If
        rsAux.Open SQLAux, gConexao, adOpenStatic, adLockReadOnly
            
        SQLQuery = "INSERT INTO " & NomeTabela & " (Cod_gr_trab,Cod_analise, ordem) " & _
                   " VALUES ('" & Trim(EcCodigo) & "','" & Trim("" & rsAux!cod_ana) & "'," & i & ")"
        BG_ExecutaQuery_ADO SQLQuery
        rsAux.Close
    Next i
    Set rsAux = Nothing
    
    'Permite que a FuncaoInserir utilize esta funcao
    If rs Is Nothing Then Exit Sub Else: If rs.RecordCount <= 0 Then Exit Sub
    
    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
   
    If MarcaLocal <= rs.RecordCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos
    
End Sub


Sub FuncaoInserir()

    Dim iRes As Integer
    Dim SQLQuery As String
    Dim rsInsAux As ADODB.recordset
    If EcListaSC.ListCount <= 0 Then
        Exit Sub
    End If
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        Set rsInsAux = New ADODB.recordset
    
        SQLQuery = "SELECT " & ChaveBD & " FROM " & NomeTabela & " WHERE " & ChaveBD & "='" & EcCodigo & "'"
    
        rsInsAux.CursorLocation = adUseServer
        rsInsAux.CursorType = adOpenStatic
        rsInsAux.Open SQLQuery, gConexao
    
        If rsInsAux.RecordCount = (-1) Then
            BG_Mensagem mediMsgBox, "Erro a verificar c�digo!", vbExclamation, "Inserir"
        ElseIf rsInsAux.RecordCount > 0 Then
            BG_Mensagem mediMsgBox, "O grupo j� tem an�lises codificadas. Opera��es possiveis: selec��o e altera��o !", vbExclamation, "Inserir"
        Else
            BL_InicioProcessamento Me, "A inserir registo."
            iRes = ValidaCamposEc
        
            If iRes = True Then
                BD_Update
                FuncaoProcurar
            End If
            BL_FimProcessamento Me
        End If
        
        rsInsAux.Close
        Set rsInsAux = Nothing
    End If
    
End Sub

Private Sub opP_Click()

    EcCodAnalise.Clear
    EcListaPerfis.Clear
    EcListaPerfis.Visible = True
    EcListaPerfis.TabStop = True
    EcListaSimples.Visible = False
    EcListaSimples.TabStop = False
    EcListaComplexa.Visible = False
    EcListaComplexa.TabStop = False
    
    Call RefinaPesquisa
        
End Sub

Private Sub opS_Click()
    
    EcCodAnalise.Clear
    EcListaSimples.Clear
    EcListaSimples.Visible = True
    EcListaSimples.TabStop = True
    EcListaComplexa.Visible = False
    EcListaComplexa.TabStop = False
    EcListaPerfis.Visible = False
    EcListaPerfis.TabStop = False
    
    Call RefinaPesquisa
    
End Sub

Private Sub opC_Click()
        
    EcCodAnalise.Clear
    EcListaComplexa.Visible = True
    EcListaComplexa.TabStop = True
    EcListaSimples.Visible = False
    EcListaSimples.TabStop = False
    EcListaPerfis.Visible = False
    EcListaPerfis.TabStop = False
    
    Call RefinaPesquisa
    
End Sub


Sub FuncaoRemover()

    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If MarcaLocal <= rs.RecordCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
        
    LimpaCampos
    PreencheCampos

End Sub


Private Sub PassaElementos(ByVal Codigo As String, ByVal descricao As String, ByVal sequencial As Long)
        
    Dim Repetido As Boolean
    Dim s As String
    Dim i As Integer
        
    'Verifica se o item a inserir j� existe na lista de destino
    s = Trim(Codigo) & Space(6 - Len(Trim(Codigo)) + 1) & Trim(descricao) & Space(40 - Len(Trim(descricao)) + 1)
    Repetido = False
    For i = 0 To ListaDestino.ListCount - 1
        If ListaDestino.List(i) = s Then
            If ListaDestino.ItemData(i) = sequencial Then
                Repetido = True
                Exit For
            End If
        End If
    Next i
    If Repetido = False Then
        ListaDestino.AddItem s
        ListaDestino.ItemData(ListaDestino.NewIndex) = sequencial
    End If
    
End Sub

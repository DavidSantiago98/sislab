Attribute VB_Name = "Stocks"
Option Explicit

' ------------------------------------------------------------------------------------------------

' REGISTA MOVIMENTO NA TABELA DE STOCKS.

' ------------------------------------------------------------------------------------------------
Public Function STK_InsereMovimento(cod_lote As String, cod_artigo As String, tipo_movimento As Integer, _
                                        quantidade As Double, observacao As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim transferencia As Boolean
    Dim Entrada As Boolean
    Dim rsTipoMov As New ADODB.recordset
    Dim stock_restante As Double
    
    STK_InsereMovimento = False
    
    sSql = "SELECT * FROM sl_stk_tipos_mov WHERE codigo = " & tipo_movimento
    rsTipoMov.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTipoMov.Open sSql, gConexao
    
    If rsTipoMov.RecordCount <= 0 Then
        rsTipoMov.Close
        Set rsTipoMov = Nothing
        Exit Function
    Else
        If BL_HandleNull(rsTipoMov!transferencia, 0) = 0 Then
            transferencia = False
        Else
            transferencia = True
        End If
        If BL_HandleNull(rsTipoMov!tipo, 1) = 1 Then
            Entrada = True
        Else
            Entrada = False
        End If
    End If
    rsTipoMov.Close
    Set rsTipoMov = Nothing
    
    ' SE MOVIMENTO FOR DE ABERTURA/FeCHO DE LOTE, VERIFICA SE O PODE ABRIR/FECHAR
    If tipo_movimento = gCodMovAberLote Then
        If STK_VerificaAberturaLote(cod_lote) = False Then
            BG_Mensagem mediMsgBox, "Imposs�vel abrir lote.", vbCritical, " Movimento"
            STK_InsereMovimento = False
            Exit Function
        End If
        quantidade = 0
        
        'REGISTA ABERTURA
        If STK_AbreLote(cod_lote, Bg_DaData_ADO) = False Then
            GoTo TrataErro
        End If
    ElseIf tipo_movimento = gCodMovFechoLote Then
        If STK_VerificaFechoLote(cod_lote) = False Then
            BG_Mensagem mediMsgBox, "Imposs�vel Fechar lote.", vbCritical, " Movimento"
            STK_InsereMovimento = False
            Exit Function
        End If
        quantidade = 0
        
        'REGISTA FECHO
        If STK_FechaLote(cod_lote, Bg_DaData_ADO) = False Then
            GoTo TrataErro
        End If
    Else
        If STK_VerificaLoteAberto(cod_lote, Bg_DaData_ADO) = False Then
            BG_Mensagem mediMsgBox, "Lote ainda n�o foi aberto.", vbCritical, " Movimento"
            STK_InsereMovimento = False
            Exit Function
        End If
    End If
    
    ' VERIFICA O STOCK QUE FICA DEPOIS DO MOVIMENTO
    stock_restante = STK_RetornaQuantidadeRestante(cod_lote, quantidade, Entrada, transferencia)
    If stock_restante > -1 Then
        If gSGBD = gOracle Then
            sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "stk_movimento(seq_movimento, cod_lote, cod_artigo, tipo_movimento,"
            sSql = sSql & " quantidade, qtd_disponivel, user_cri, dt_cri, hr_cri, observacao) VALUES("
            sSql = sSql & " seq_stk_movimento.nextval, " & cod_lote & "," & cod_artigo & ", " & tipo_movimento & ", "
            sSql = sSql & Replace(quantidade, ",", ".") & ", " & Replace(stock_restante, ",", ".") & " , " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ","
            sSql = sSql & BL_TrataStringParaBD(observacao) & ") "
        ElseIf gSGBD = gSqlServer Then
            sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "stk_movimento( cod_lote, cod_artigo, tipo_movimento,"
            sSql = sSql & " quantidade, qtd_disponivel, user_cri, dt_cri, hr_cri,observacao) VALUES("
            sSql = sSql & cod_lote & "," & cod_artigo & ", " & tipo_movimento & ", "
            sSql = sSql & Replace(quantidade, ",", ".") & ", " & Replace(stock_restante, ",", ".") & " , " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ","
            sSql = sSql & BL_TrataStringParaBD(observacao) & ") "
        End If
    End If
    
    If BG_ExecutaQuery_ADO(sSql) > -1 Then
        STK_InsereMovimento = True
    Else
        STK_InsereMovimento = False
    End If
Exit Function
TrataErro:
    STK_InsereMovimento = False
    BG_LogFile_Erros "BibliotecaLocal: STK_InsereMovimento: " & Err.Description, "BL", "STK_InsereMovimento", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' CALCULA STOCK_ACTUAL E STOCK_TOTAL. RETORNA O STOCK_ACTUAL.

' ------------------------------------------------------------------------------------------------
Public Function STK_RetornaQuantidadeRestante(cod_lote As String, quantidade As Double, _
                                                  Entrada As Boolean, transferencia As Boolean) As Double
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsStk As New ADODB.recordset
    Dim stock_act As Double
    Dim stock_tot As Double
    
    STK_RetornaQuantidadeRestante = -1
    sSql = "SELECT * FROM sl_stk_lotes WHERE codigo = " & cod_lote
    rsStk.CursorLocation = adUseServer
    rsStk.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsStk.Open sSql, gConexao
    
    If rsStk.RecordCount = 1 Then
        If BL_HandleNull(rsStk!data_inicio, "") = "" Then
            gMsgResp = BG_Mensagem(mediMsgBox, "Lote ainda n�o foi aberto", vbYesNo + vbDefaultButton2 + vbQuestion, "Lote")
            STK_RetornaQuantidadeRestante = -1
            rsStk.Close
            Set rsStk = Nothing
            Exit Function
        End If
        If BL_HandleNull(rsStk!data_fim, "") <> "" Then
            If CDate(BL_HandleNull(rsStk!data_fim, "")) < Bg_DaData_ADO Then
                gMsgResp = BG_Mensagem(mediMsgBox, "Lote j� fechado", vbYesNo + vbDefaultButton2 + vbQuestion, "Lote")
                STK_RetornaQuantidadeRestante = -1
                rsStk.Close
                Set rsStk = Nothing
                Exit Function
            End If
        End If
        
        stock_act = BL_HandleNull(rsStk!stock_actual, 0)
        stock_tot = BL_HandleNull(rsStk!stock_total, 0)
        
        If Entrada = True Then
            stock_act = stock_act + quantidade
            If transferencia = False Then
                stock_tot = stock_tot + quantidade
            End If
        Else
            stock_act = stock_act - quantidade
            If stock_act < 0 Then
                stock_act = 0
            End If
        End If
        sSql = "UPDATE sl_stk_lotes SET stock_actual = " & Replace(stock_act, ",", ".") & ", stock_total = " & Replace(stock_tot, ",", ".")
        sSql = sSql & " WHERE codigo = " & cod_lote
        If BG_ExecutaQuery_ADO(sSql) > -1 Then
            STK_RetornaQuantidadeRestante = stock_act
        Else
            GoTo TrataErro
        End If
    End If
Exit Function
TrataErro:
    STK_RetornaQuantidadeRestante = -1
    BG_LogFile_Erros "BibliotecaLocal: STK_RetornaQuantidadeRestante: " & Err.Description, "BL", "STK_RetornaQuantidadeRestante", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' VERIFICA SE PODE ABRIR UM LOTE

' ------------------------------------------------------------------------------------------------
Public Function STK_VerificaAberturaLote(cod_lote As String) As Boolean
    Dim sSql As String
    Dim rsLote As New ADODB.recordset
    On Error GoTo TrataErro
    
    STK_VerificaAberturaLote = False
    sSql = "SELECT * FROM sl_stk_lotes WHERE codigo = " & cod_lote & " AND data_inicio IS NULL "
    rsLote.CursorLocation = adUseServer
    rsLote.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsLote.Open sSql, gConexao
    If rsLote.RecordCount > 0 Then
        STK_VerificaAberturaLote = True
    Else
        STK_VerificaAberturaLote = False
    End If
    rsLote.Close
    Set rsLote = Nothing
    
Exit Function
TrataErro:
    STK_VerificaAberturaLote = False
    BG_LogFile_Erros "BibliotecaLocal: STK_VerificaAberturaLote: " & Err.Description, "BL", "STK_VerificaAberturaLote", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' VERIFICA SE PODE FECHAR UM LOTE

' ------------------------------------------------------------------------------------------------
Public Function STK_VerificaFechoLote(cod_lote As String) As Boolean
    Dim sSql As String
    Dim rsLote As New ADODB.recordset
    On Error GoTo TrataErro
    
    STK_VerificaFechoLote = False
    sSql = "SELECT * FROM sl_stk_lotes WHERE codigo = " & cod_lote & " AND data_inicio IS NOT NULL  AND data_fim IS NULL"
    rsLote.CursorLocation = adUseServer
    rsLote.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsLote.Open sSql, gConexao
    If rsLote.RecordCount > 0 Then
        STK_VerificaFechoLote = True
    Else
        STK_VerificaFechoLote = False
    End If
    rsLote.Close
    Set rsLote = Nothing
    
Exit Function
TrataErro:
    STK_VerificaFechoLote = False
    BG_LogFile_Erros "BibliotecaLocal: STK_VerificaFechoLote: " & Err.Description, "BL", "STK_VerificaFechoLote", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' VERIFICA SE DETERMINADO LOTE ESTA ACTIVO

' ------------------------------------------------------------------------------------------------
Public Function STK_VerificaLoteAberto(cod_lote As String, data As String) As Boolean
    Dim sSql As String
    Dim rsLote As New ADODB.recordset
    On Error GoTo TrataErro
    
    STK_VerificaLoteAberto = False
    sSql = "SELECT * FROM sl_stk_lotes WHERE codigo = " & cod_lote & " AND data_inicio IS NOT NULL  "
    sSql = sSql & " AND data_inicio <= " & BL_TrataDataParaBD(data) & " AND (data_fim IS NULL "
    sSql = sSql & " OR data_fim >" & BL_TrataDataParaBD(data) & ")"
    rsLote.CursorLocation = adUseServer
    rsLote.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsLote.Open sSql, gConexao
    If rsLote.RecordCount > 0 Then
        STK_VerificaLoteAberto = True
    Else
        STK_VerificaLoteAberto = False
    End If
    rsLote.Close
    Set rsLote = Nothing
    
Exit Function
TrataErro:
    STK_VerificaLoteAberto = False
    BG_LogFile_Erros "BibliotecaLocal: STK_VerificaLoteAberto: " & Err.Description, "BL", "STK_VerificaLoteAberto", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' ACTUALIZA DATA DE FECHO DO LOTE

' ------------------------------------------------------------------------------------------------
Public Function STK_FechaLote(cod_lote As String, data As String) As Boolean
    Dim sSql As String
    On Error GoTo TrataErro
    
    STK_FechaLote = False
    sSql = "UPDATE sl_stk_lotes SET data_fim = " & BL_TrataStringParaBD(data)
    If BG_ExecutaQuery_ADO(sSql) > -1 Then
        STK_FechaLote = True
    End If
Exit Function
TrataErro:
    STK_FechaLote = False
    BG_LogFile_Erros "BibliotecaLocal: STK_FechaLote: " & Err.Description, "BL", "STK_FechaLote", True
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' ACTUALIZA DATA DE INICO DO LOTE

' ------------------------------------------------------------------------------------------------
Public Function STK_AbreLote(cod_lote As String, data As String) As Boolean
    Dim sSql As String
    On Error GoTo TrataErro
    
    STK_AbreLote = False
    sSql = "UPDATE sl_stk_lotes SET data_inicio = " & BL_TrataStringParaBD(data) & ", stock_actual = nvl(num_unidades,0) * nvl(qtd_unidade,0), stock_total = nvl(num_unidades,0) * nvl(qtd_unidade,0) "
    If BG_ExecutaQuery_ADO(sSql) > -1 Then
        STK_AbreLote = True
    End If
Exit Function
TrataErro:
    STK_AbreLote = False
    BG_LogFile_Erros "BibliotecaLocal: STK_AbreLote: " & Err.Description, "BL", "STK_AbreLote", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' FUNCIONALIDADE QUE PERMITE DECREMENTAR AUTOMATICAMENTE STOCK ASSOCIADO A ANALISE

' ------------------------------------------------------------------------------------------------
Public Function STK_DecrementaAutoAna(ByVal n_req As String, ByVal Cod_Perfil As String, _
                                      ByVal cod_ana_c As String, ByVal cod_ana_s As String, _
                                      user_apar As String, data As String) As Boolean
    Dim sSql As String
    Dim rsLote As New ADODB.recordset
    Dim rsRepet As New ADODB.recordset
    Dim quantidade As Double
    STK_DecrementaAutoAna = False
    On Error GoTo TrataErro
    If cod_ana_s = gGHOSTMEMBER_S Then
        STK_DecrementaAutoAna = True
        Exit Function
    End If
    ' VERIFICA SE H� LOTES ACTIVOS PARA ANALISE EM CAUSA
    sSql = "select DISTINCT x2.codigo, x2.cod_artigo, x2.decremento, x2.stock_actual "
    sSql = sSql & " From sl_stk_ana_lotes x1, sl_stk_lotes x2 WHERE x1.cod_lote = x2.codigo"
    sSql = sSql & " AND x1.cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s) & " AND user_apar =" & BL_TrataStringParaBD(user_apar)
    sSql = sSql & " AND x2.data_inicio <= " & BL_TrataDataParaBD(data) & " AND (x2.data_fim IS NULL OR x2.data_fim >" & BL_TrataDataParaBD(data) & ")"
    rsLote.CursorLocation = adUseServer
    rsLote.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsLote.Open sSql, gConexao
    If rsLote.RecordCount > 0 Then
        While Not rsLote.EOF
            quantidade = 0
            quantidade = BL_HandleNull(rsLote!decremento, 0)
            STK_InsereMovimento BL_HandleNull(rsLote!Codigo, ""), BL_HandleNull(rsLote!cod_artigo, ""), _
                                2, quantidade, " Requisi��o: " & n_req & "-" & cod_ana_s
            
            ' CALCULA REPETICOES PARA ANALISE EM CAUSA
            sSql = "SELECT * FROM sl_repeticoes WHERE n_req = " & n_req
            sSql = sSql & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil)
            sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c)
            sSql = sSql & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
            rsRepet.CursorLocation = adUseServer
            rsRepet.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsRepet.Open sSql, gConexao
            If rsRepet.RecordCount > 0 Then
                quantidade = (rsRepet.RecordCount * BL_HandleNull(rsLote!decremento, 0))
                STK_InsereMovimento BL_HandleNull(rsLote!Codigo, ""), BL_HandleNull(rsLote!cod_artigo, ""), _
                                    2, quantidade, " Requisi��o (R): " & n_req & "-" & cod_ana_s
            End If
            rsRepet.Close
            rsLote.MoveNext
        Wend
        Set rsRepet = Nothing
        rsLote.Close
        Set rsLote = Nothing
    Else
        rsLote.Close
        Set rsLote = Nothing
        STK_DecrementaAutoAna = True
        Exit Function
    End If
     STK_DecrementaAutoAna = True
Exit Function
TrataErro:
    STK_DecrementaAutoAna = False
    BG_LogFile_Erros "STOCK: STK_ASTK_DecrementaAutoAnabreLote: " & Err.Description, "BL", "STK_DecrementaAutoAna", True
    Exit Function
    Resume Next
End Function


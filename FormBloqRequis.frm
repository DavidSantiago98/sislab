VERSION 5.00
Begin VB.Form FormBloqRequis 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormBloqRequis"
   ClientHeight    =   2280
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5535
   Icon            =   "FormBloqRequis.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2280
   ScaleWidth      =   5535
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtDesbloq 
      Height          =   495
      Left            =   4800
      Picture         =   "FormBloqRequis.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Desbloquear"
      Top             =   1680
      Width           =   495
   End
   Begin VB.CommandButton BtBloq 
      Height          =   495
      Left            =   4200
      Picture         =   "FormBloqRequis.frx":0776
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Bloquear"
      Top             =   1680
      Width           =   495
   End
   Begin VB.TextBox EcMotivo 
      Appearance      =   0  'Flat
      Height          =   885
      Left            =   960
      TabIndex        =   3
      Top             =   600
      Width           =   4335
   End
   Begin VB.TextBox EcEstado 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   1920
      TabIndex        =   2
      Top             =   120
      Width           =   3375
   End
   Begin VB.TextBox EcRequis 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   960
      TabIndex        =   0
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Motivo"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Requisi��o"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   975
   End
End
Attribute VB_Name = "FormBloqRequis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean


Private Sub BtBloq_Click()
    If EcRequis = "" Or EcMotivo = "" Or EcEstado = "" Then
        Exit Sub
    End If
    If BL_BloqueiaReq(EcRequis, EcMotivo.Text) = True Then
        BG_Mensagem mediMsgBox, "Requisi��o Bloqueada", vbExclamation, "Bloqueio"
        LimpaCampos
    End If
End Sub

Private Sub BtDesbloq_Click()
    If EcRequis = "" Or EcMotivo = "" Or EcEstado = "" Then
        Exit Sub
    End If
    If BL_DesbloqueiaReq(EcRequis) = True Then
        BG_Mensagem mediMsgBox, "Requisi��o Desbloqueada", vbExclamation, "Desbloqueio"
        LimpaCampos
    End If
End Sub

Private Sub EcRequis_Validate(Cancel As Boolean)
    Dim sSql As String
    Dim rsBl As New ADODB.recordset
    Dim rsBl2 As New ADODB.recordset
    
    BtBloq.Enabled = False
    BtDesbloq.Enabled = False
    If EcRequis <> "" Then
        If IsNumeric(EcRequis) Then
            sSql = "SELECT * FROM sl_requis WHERE n_req = " & EcRequis
            rsBl.CursorLocation = adUseServer
            rsBl.CursorType = adOpenStatic
            rsBl.Open sSql, gConexao
            If rsBl.RecordCount = 1 Then
                EcEstado = BL_DevolveEstadoReq(BL_HandleNull(rsBl!estado_req, " "))
                Select Case BL_HandleNull(rsBl!estado_req, " ")
                    Case gEstadoReqBloqueada
                        sSql = "SELECT * FROM Sl_req_bloq WHERE n_req = " & EcRequis
                        rsBl2.CursorLocation = adUseServer
                        rsBl2.CursorType = adOpenStatic
                        rsBl2.Open sSql, gConexao
                        If rsBl2.RecordCount = 1 Then
                            BtBloq.Enabled = False
                            BtDesbloq.Enabled = True
                            EcMotivo = BL_HandleNull(rsBl2!motivo_bloq, "")
                        Else
                            EcEstado = BL_DevolveEstadoReq(BL_MudaEstadoReq(EcRequis))
                            EcRequis_Validate False
                        End If
                        rsBl2.Close
                        Set rsBl2 = Nothing
                    Case gEstadoReqEsperaProduto, gEstadoReqEsperaResultados, gEstadoReqEsperaValidacao, gEstadoReqImpressaoParcial, gEstadoReqNaoPassarHistorico, gEstadoReqResultadosParciais, gEstadoReqResultadosParciais, gEstadoReqSemAnalises, gEstadoReqTodasImpressas, gEstadoReqValicacaoMedicaCompleta, gEstadoReqValidacaoMedicaParcial
                        BtBloq.Enabled = True
                        BtDesbloq.Enabled = False
                    Case Else
                        BtBloq.Enabled = False
                        BtDesbloq.Enabled = False
                End Select
            End If
        End If
    End If
    
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = " Bloquear/Desbloquear Requisi��o"
    Me.left = 5
    Me.top = 5
    Me.Width = 5625
    Me.Height = 2700 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormBloqRequis = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus
    EcRequis = ""
    EcMotivo = ""
    EcEstado = ""
    BtBloq.Enabled = False
    BtDesbloq.Enabled = False
End Sub

Sub DefTipoCampos()

End Sub

Sub PreencheCampos()
    
    Me.SetFocus

End Sub


Sub FuncaoLimpar()
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        
    End If
End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos

    Else
        Unload Me
    End If
End Sub



Sub FuncaoProcurar()
    'nada
End Sub



Sub FuncaoInserir()

End Sub

Sub FuncaoModificar()

End Sub

Sub FuncaoRemover()
    
End Sub


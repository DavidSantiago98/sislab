VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormFACTUSConfirmaNovo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFACTUSConfirmaNovo"
   ClientHeight    =   11910
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   20565
   Icon            =   "FormFACTUSConfirmaNovo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   11910
   ScaleWidth      =   20565
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtGestReq 
      Enabled         =   0   'False
      Height          =   855
      Left            =   6360
      Picture         =   "FormFACTUSConfirmaNovo.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   44
      ToolTipText     =   "Requisi��o"
      Top             =   8760
      Width           =   1335
   End
   Begin VB.CommandButton BtApagar 
      Caption         =   "Retirar"
      Enabled         =   0   'False
      Height          =   855
      Left            =   8400
      Picture         =   "FormFACTUSConfirmaNovo.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   37
      ToolTipText     =   "Apagar Registos do FACTUS"
      Top             =   8760
      Width           =   1335
   End
   Begin VB.Frame Frame2 
      Caption         =   "An�lises "
      Height          =   5295
      Left            =   1200
      TabIndex        =   1
      Top             =   3240
      Width           =   17055
      Begin VB.TextBox EcAux 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   0
         TabIndex        =   2
         Top             =   480
         Width           =   615
      End
      Begin MSFlexGridLib.MSFlexGrid FGAnalises 
         Height          =   4860
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   16695
         _ExtentX        =   29448
         _ExtentY        =   8573
         _Version        =   393216
         FixedCols       =   0
         ForeColor       =   0
         BackColorSel    =   -2147483643
         ForeColorSel    =   0
         BackColorBkg    =   -2147483633
         ScrollBars      =   2
         BorderStyle     =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Dados da Requisi��o"
      Height          =   3015
      Left            =   1200
      TabIndex        =   10
      Top             =   120
      Width           =   17055
      Begin VB.TextBox EcCodSala 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   42
         Top             =   1920
         Width           =   975
      End
      Begin VB.TextBox EcDescrSala 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   1920
         Width           =   4815
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   6840
         Picture         =   "FormFACTUSConfirmaNovo.frx":19A0
         Style           =   1  'Graphical
         TabIndex        =   40
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   1920
         Width           =   375
      End
      Begin VB.TextBox EcCredencial 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   8400
         TabIndex        =   38
         ToolTipText     =   "Credencial"
         Top             =   1920
         Width           =   3495
      End
      Begin VB.TextBox EcDataFinal 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   6000
         TabIndex        =   35
         Top             =   1080
         Width           =   1215
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2400
         TabIndex        =   32
         Top             =   240
         Width           =   1215
      End
      Begin VB.TextBox EcTipoUte 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   31
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   600
         Width           =   7335
      End
      Begin VB.TextBox EcDescrEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   1485
         Width           =   4815
      End
      Begin VB.TextBox EcCodEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   1080
         TabIndex        =   18
         Top             =   1485
         Width           =   975
      End
      Begin VB.TextBox EcKm 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11280
         TabIndex        =   17
         Top             =   1560
         Width           =   615
      End
      Begin VB.ComboBox CbCodUrbano 
         BackColor       =   &H80000018&
         Height          =   315
         Left            =   10920
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1080
         Width           =   1455
      End
      Begin VB.TextBox EcNumBenef 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   8400
         TabIndex        =   15
         Top             =   1530
         Width           =   1335
      End
      Begin VB.TextBox EcEstado 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   1080
         Width           =   2055
      End
      Begin VB.TextBox EcNumReq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   285
         Left            =   1080
         TabIndex        =   13
         Top             =   1080
         Width           =   975
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   6840
         Picture         =   "FormFACTUSConfirmaNovo.frx":1F2A
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   1460
         Width           =   375
      End
      Begin VB.TextBox EcDataInicial 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   4560
         TabIndex        =   11
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label lb_erro_efec 
         Caption         =   "Erro de Efetiva��o"
         ForeColor       =   &H00404080&
         Height          =   255
         Left            =   120
         TabIndex        =   45
         Top             =   2640
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Posto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   43
         Top             =   1920
         Width           =   1155
      End
      Begin VB.Label Label1 
         Caption         =   "Credencial"
         Height          =   255
         Index           =   19
         Left            =   7560
         TabIndex        =   39
         Top             =   1920
         Width           =   975
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "e"
         Height          =   195
         Index           =   2
         Left            =   5790
         TabIndex        =   36
         Top             =   1080
         Width           =   90
      End
      Begin VB.Label Label1 
         Caption         =   "Total Fac.Rec."
         Height          =   255
         Index           =   1
         Left            =   7680
         TabIndex        =   34
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label LbTotalRecibo 
         Height          =   255
         Left            =   8760
         TabIndex        =   33
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         Height          =   195
         Left            =   120
         TabIndex        =   30
         Top             =   600
         Width           =   420
      End
      Begin VB.Label Label10 
         Caption         =   "E.F.R."
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   1485
         Width           =   495
      End
      Begin VB.Label Label4 
         Caption         =   "Km"
         Height          =   255
         Left            =   10680
         TabIndex        =   26
         Top             =   1560
         Width           =   615
      End
      Begin VB.Label Label3 
         Caption         =   "C�digo Urbano"
         Height          =   255
         Left            =   9720
         TabIndex        =   25
         Top             =   1080
         Width           =   1455
      End
      Begin VB.Label Label11 
         Caption         =   "Nr. Benef."
         Height          =   255
         Left            =   7560
         TabIndex        =   24
         Top             =   1530
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "N� Req."
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   23
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Data"
         Height          =   195
         Index           =   14
         Left            =   4200
         TabIndex        =   22
         Top             =   1095
         Width           =   345
      End
      Begin VB.Label LbCredenciais 
         Height          =   255
         Left            =   6360
         TabIndex        =   21
         Top             =   1560
         Width           =   2415
      End
      Begin VB.Label LbLote 
         Height          =   255
         Left            =   9480
         TabIndex        =   20
         Top             =   1560
         Width           =   1695
      End
   End
   Begin VB.CommandButton BtCima 
      Enabled         =   0   'False
      Height          =   495
      Left            =   18840
      Picture         =   "FormFACTUSConfirmaNovo.frx":24B4
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Mudar Ordem"
      Top             =   5880
      Width           =   855
   End
   Begin VB.CommandButton BtBaixo 
      Enabled         =   0   'False
      Height          =   495
      Left            =   18840
      Picture         =   "FormFACTUSConfirmaNovo.frx":317E
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Mudar Ordem"
      Top             =   6480
      Width           =   855
   End
   Begin VB.CommandButton BtValida 
      Caption         =   "Confirmar"
      Enabled         =   0   'False
      Height          =   855
      Left            =   10440
      Picture         =   "FormFACTUSConfirmaNovo.frx":3E48
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Actualizar Factura��o "
      Top             =   8760
      Width           =   1355
   End
   Begin VB.CommandButton BtRetira 
      Height          =   615
      Left            =   18720
      Picture         =   "FormFACTUSConfirmaNovo.frx":4B12
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Retirar An�lise"
      Top             =   7680
      Width           =   1095
   End
   Begin VB.CommandButton BtAdiciona 
      Enabled         =   0   'False
      Height          =   615
      Left            =   18720
      Picture         =   "FormFACTUSConfirmaNovo.frx":57DC
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Adicionar An�lise"
      Top             =   4680
      Width           =   1095
   End
   Begin VB.TextBox EcCodAna 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   18720
      TabIndex        =   4
      Top             =   3840
      Visible         =   0   'False
      Width           =   1455
   End
   Begin MSFlexGridLib.MSFlexGrid FgReq 
      Height          =   7695
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   13573
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
   End
   Begin VB.Label LbTotalFactEFR 
      Height          =   255
      Left            =   3000
      TabIndex        =   28
      Top             =   8760
      Width           =   735
   End
End
Attribute VB_Name = "FormFACTUSConfirmaNovo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' Vari�veis Globais para este Form.

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

'**** Emanuel Sousa 15.05.2009 *****************
'**** Vars para armazenar intervalo datas pedidas no Report ******'
Dim dataInicialReport As String
Dim dataFinalReport As String
'***********************************************'

Const NumLinhas = 16
'Linha da grid selecionada.
Dim LINHA_SEL As Integer
Dim COLUNA_SEL As Integer

'Linha da grid selecionada.
Dim LINHA_EDITA As Integer
Dim COLUNA_EDITA As Integer


Public rs As adodb.recordset


' -------------------------------------------------------------------
' COLUNAS NA GRELHA ENVIO PARA FACTURACAO
' -------------------------------------------------------------------
Const lColFactP1 = 0
Const lColFactData = 1
Const lColFactCodRubr = 2
Const lColFactDescrRubr = 3
Const lColFactCodRubrEFR = 4
Const lColFactQTD = 5
Const lColFactValActo = 6
Const lColFactValEFR = 7
Const lColFactValDoe = 8
Const lColFactPercDoe = 9
Const lColFactIsencao = 10
Const lColFactConvencao = 11
'Const lColFactCor = 12
Const lColFact_SerieFact = 12
Const lColFact_NFact = 13
'UALIA-871
Const lColFact_SerieFactDoe = 14
Const lColFact_NFacDoe = 15
Const lColFact_SerieFactTX = 16
Const lColFact_NFacTX = 17
'

Const lCodDomicilio = 3
Const lCodInternacional = 2
Const lCodNormal = 1
Const lCod97 = 4

Dim lCredencialActiva  As Long
Dim lPosicaoActual As Long
Dim ReqActual As Long

Dim FLG_ExecutaFGAna As Boolean

'UALIA -871
Const vermelhoClaro = &HBDC8F4
Const Verde = &HC0FFC0
Const azul = &HFFC0C0
Const Amarelo = &HC0FFFF
Const rosa = &HFFC0FF
Const azul2 = &HFFFFC0

'UALIA-919
Private Type CamposPesquisa
    credencial As String
    NumReq As String
    TipoUte As String
    codEfr As String
    NumBenef As String
    dataInicial As String
    dataFinal As String
    codSala As String
    LinhaFgReq As String
    FGReqPrivado As Boolean
End Type

Dim PesqRequis As CamposPesquisa
'


Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    
    Call Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    Call PreencheValoresDefeito
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
    gF_FACTUSENVIO = 1
    
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    'Emanuel Sousa - 13.05.2009
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    DoEvents
    'UALIA-919
    If PesqRequis.FGReqPrivado = True Then
        EcCredencial = PesqRequis.credencial
        EcNumReq = PesqRequis.NumReq
        EcTipoUte = PesqRequis.TipoUte
        EcCodEFR = PesqRequis.codEfr
        EcNumBenef = PesqRequis.NumBenef
        EcDataInicial = PesqRequis.dataInicial
        EcDataFinal = PesqRequis.dataFinal
        EcCodSala = PesqRequis.codSala
        CbCodUrbano.ListIndex = mediComboValorNull
        PesqRequis.LinhaFgReq = FgReq.row
        Preenche_Dados_Requisicao
    End If
    
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    gF_FACTUSENVIO = 0
    
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    ' Fecha a conex�o secund�ria.
    BL_Fecha_Conexao_Secundaria
    
    Set FormFACTUSConfirmaNovo = Nothing
    
End Sub

Sub Inicializacoes()

    Me.caption = " Confirma��o da Factura��o"
    
    Me.left = 100
    Me.top = 20
    Me.Width = 15795
    Me.Height = 10095 ' Normal
    'Me.Height = 8700 ' Campos Extras
    
    Set CampoDeFocus = EcNumReq
    
    EcKm.Enabled = False
    
    LINHA_SEL = 1
    
    ' Abre a liga��o ao Factus.
    Call BL_Abre_Conexao_Secundaria(gSGBD_SECUNDARIA)
    ReqActual = 1
    DoEvents
    DoEvents
    DoEvents
    FLG_ExecutaFGAna = False
    'UALIA-871
    BtRetira.Enabled = False
    Frame2.Width = 14500
    Frame3.Width = 14500
    Me.Width = 17250
    BtAdiciona.left = 15850
    BtRetira.left = 15850
    BtCima.left = 16000
    BtBaixo.left = 16000
    '
    
    'edgar.parada Glintt-HS-21241 24.05.2019
    ReDim fa_movi_resp(0)
    lb_erro_efec.Visible = False
    lb_erro_efec.ForeColor = RGB(255, 99, 71)
    '
End Sub

 Sub DefTipoCampos()

    With FGAnalises
        ' pferreira 2011.09.06
        .Width = 14000
        .Cols = 18
        .rows = NumLinhas
        .FixedCols = 0
        .FixedRows = 1
        .Col = lColFactP1
        .ColWidth(lColFactP1) = 1900
        .TextMatrix(0, lColFactP1) = "Cred."
        
        .Col = lColFactData
        .ColWidth(lColFactData) = 900
        .TextMatrix(0, lColFactData) = "Data"
        
        .Col = lColFactCodRubr
        .ColWidth(lColFactCodRubr) = 700
        .TextMatrix(0, lColFactCodRubr) = "R�brica"
        
        .Col = lColFactDescrRubr
        .ColWidth(lColFactDescrRubr) = 2300
        .TextMatrix(0, lColFactDescrRubr) = "Descri��o da R�brica"
        
        .Col = lColFactCodRubrEFR
        .ColWidth(lColFactCodRubrEFR) = 1000
        .TextMatrix(0, lColFactCodRubrEFR) = "Rubr EFR"
        
        .Col = lColFactQTD
        .ColWidth(lColFactQTD) = 400
        .TextMatrix(0, lColFactQTD) = "Qtd."
        
        .Col = lColFactValActo
        .ColWidth(lColFactValActo) = 700
        .TextMatrix(0, lColFactValActo) = "Val.Acto"
        
        .Col = lColFactValEFR
        .ColWidth(lColFactValEFR) = 700
        .TextMatrix(0, lColFactValEFR) = "Val Ent."
        
        .Col = lColFactValDoe
        .ColWidth(lColFactValDoe) = 700
        .TextMatrix(0, lColFactValDoe) = "Val Doe."
        
        .Col = lColFactPercDoe
        .ColWidth(lColFactPercDoe) = 500
        .TextMatrix(0, lColFactPercDoe) = "%Doe"
        
        .Col = lColFactIsencao
        .ColWidth(lColFactIsencao) = 900
        .TextMatrix(0, lColFactIsencao) = "Isen��o"
        
        .Col = lColFactConvencao
        .ColWidth(lColFactConvencao) = 1000
        .TextMatrix(0, lColFactConvencao) = "Conven��o"
        
        '.Col = lColFactCor
        '.ColWidth(lColFactCor) = 700
       ' .TextMatrix(0, lColFactCor) = "Cor"
        
        'LJMANSO-312
        .Col = lColFact_SerieFact
        .ColWidth(lColFact_SerieFact) = 900
        .TextMatrix(0, lColFact_SerieFact) = "S�rie Fat."
        
        .Col = lColFact_NFact
        .ColWidth(lColFact_NFact) = 700
        .TextMatrix(0, lColFact_NFact) = "N. Fat."
        '
        'UALIA-871
        .Col = lColFact_SerieFactDoe
        .ColWidth(lColFact_SerieFactDoe) = 0
        .TextMatrix(0, lColFact_SerieFactDoe) = "S. Fat. Doe."
        
        .Col = lColFact_SerieFactDoe
        .ColWidth(lColFact_NFacDoe) = 0
        .TextMatrix(0, lColFact_NFacDoe) = "N. Fat. Doe."
        
        .Col = lColFact_SerieFactTX
        .ColWidth(lColFact_SerieFactTX) = 0
        .TextMatrix(0, lColFact_SerieFactTX) = "S. Fat. TX"
        
        .Col = lColFact_NFacTX
        .ColWidth(lColFact_NFacTX) = 0
        .TextMatrix(0, lColFact_NFacTX) = "N. Fat. TX"
        '
        
        .ColAlignment(0) = 4
        .ColAlignment(1) = flexAlignLeftCenter
        .ColAlignment(2) = 4
        .ColAlignment(3) = 4
        .ColAlignment(4) = 4
        .ColAlignment(5) = flexAlignLeftCenter
        .DragMode = 0
    End With
    
    With FgReq
        .Cols = 1
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .ColWidth(0) = 1000
        .TextMatrix(0, 0) = "Requisi��es"
    End With
    
    EcKm.Tag = adDouble
    EcKm.MaxLength = 6
    EcDataFinal.Tag = adDate
    EcDataInicial.Tag = adDate
    EcCodEFR.Tag = adNumeric
    
    
 End Sub
 
Private Sub BExpandir_Click()
    FGAnalises.Height = 7380
    Frame2.Height = 7695
    Frame2.top = 0

End Sub

Private Sub BtAdiciona_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean
    Dim ssql As String
    Dim i As Integer
    Dim TotalElementosSel As Integer
    Dim Portaria As String
    Dim seq_fact As Long
    Dim valEntidade As String
    Dim cod_rubr_efr As String
    Dim descr_rubr As String
    Dim j As String
    
    On Error GoTo TrataErro
    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "Descr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
        
    If EcCodEFR = "" Then
        Exit Sub
    End If
    CamposRetorno.InicializaResultados 1
    
    CFrom = "slv_analises_factus "
    CWhere = " cod_ana in (Select cod_ana from sl_ana_facturacao)"
    CampoPesquisa = "descr_ana"
           
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana ", _
                                                                           " An�lises")
        
    
    If PesqRapida = True Then
        FormPesqRapidaAvancadaMultiSel.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
        
        If Not CancelouPesquisa Then
        
            For i = 1 To TotalElementosSel
                
                If fa_movi_resp(FgReq.row).totalAna > 0 Then
                    FACTUS_AdicionaAnalise EcCodEFR.text, BL_HandleNull(UCase(Resultados(i)), ""), _
                                             fa_movi_resp(FgReq.row).analises(fa_movi_resp(FgReq.row).totalAna).dt_ini_real, 0, _
                                              mediComboValorNull, fa_movi_resp(FgReq.row).analises(fa_movi_resp(FgReq.row).totalAna).cod_isen_doe, _
                                              mediComboValorNull, fa_movi_resp(FgReq.row).n_benef_doe, fa_movi_resp(FgReq.row).t_doente, _
                                              fa_movi_resp(FgReq.row).doente, fa_movi_resp(FgReq.row).episodio, "", lCodNormal, _
                                              fa_movi_resp(FgReq.row).analises(fa_movi_resp(FgReq.row).totalAna).cod_medico, _
                                              fa_movi_resp(FgReq.row).analises(fa_movi_resp(FgReq.row).totalAna).cod_dom, _
                                              fa_movi_resp(FgReq.row).analises(fa_movi_resp(FgReq.row).totalAna).km_dom, _
                                              fa_movi_resp(FgReq.row).analises(fa_movi_resp(FgReq.row).totalAna).cod_post_dom, _
                                              fa_movi_resp(FgReq.row).analises(fa_movi_resp(FgReq.row).totalAna).localidade_dom, _
                                              fa_movi_resp(FgReq.row).analises(fa_movi_resp(FgReq.row).totalAna).cod_u_saude, mediComboValorNull
                End If
                
                ' ---------------------------------------------------------------------------
                ' AUMENTA GRELHA
                ' ---------------------------------------------------------------------------
                If fa_movi_resp(FgReq.row).totalAna >= (NumLinhas - 1) Then
                    FGAnalises.AddItem ""
                End If
                
            Next
            PreencheFGAnalises FgReq.row
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "BtAdiciona_click"
    Exit Sub
    Resume Next
End Sub


Private Sub BtApagar_Click()
    Dim ssql As String
    Dim RsFact As New adodb.recordset
    Dim i As Integer
    Dim linhasAfectadas As Integer
    Dim iAna As Integer
    On Error GoTo TrataErro
    

    If (BG_Mensagem(mediMsgBox, "Deseja n�o facturar a requisi��o em causa?" & vbCrLf & "Este procedimento ir� isentar as an�lises para a entidade.", _
                                vbQuestion + vbYesNo + vbDefaultButton2, _
                                " " & cAPLICACAO_NOME_CURTO) = vbNo) Then
        ' Sa�.
        Exit Sub
    End If
    ssql = "SELECT * FROM fa_movi_fact WHERE t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(fa_movi_resp(FgReq.row).episodio)
    ssql = ssql & " AND flg_estado = 3 " & " AND n_ord IN (select n_ord FROM fa_movi_resp WHERE t_episodio = 'SISLAB' AND episodio = "
    ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(FgReq.row).episodio) & " AND cod_efr = " & fa_movi_resp(FgReq.row).cod_efr & ")"
    RsFact.CursorLocation = adUseClient
    RsFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    RsFact.Open ssql, gConexaoSecundaria
    If RsFact.RecordCount > 0 Then
        BG_Mensagem mediMsgBox, "Existem registos j� facturados. N�o � poss�vel eliminar do FACTUS", vbExclamation, "EFR"
        Exit Sub
    End If
    RsFact.Close
    Set RsFact = Nothing
    For iAna = 1 To fa_movi_resp(FgReq.row).totalAna
        If FACTUS_IsencaoEntidade(FgReq.row, iAna) = False Then
            BG_Mensagem mediMsgBox, "Erro ao apagar atividade da an�lise!", vbExclamation
            Exit Sub
        End If
    Next iAna
    PreencheFGAnalises FgReq.row
Exit Sub
TrataErro:
    gConexaoSecundaria.RollbackTrans
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro ao apagar registos do FACTUS:" & Err.Number & " - " & Err.Description, Me.Name, "BtApagar_Click", True
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------

' TROCA A ORDEM COM A LINHA DE BAIXO

' ---------------------------------------------------------------------------
Private Sub BtBaixo_Click()
    
    On Error Resume Next
    
    Dim rv As Integer
    Dim l1 As Integer
    Dim l2 As Integer
    On Error GoTo TrataErro
    
        
    If (LINHA_SEL >= fa_movi_resp(FgReq.row).totalAna) Then
        Exit Sub
    End If
    
    l1 = LINHA_SEL
'    If EstrutAnalisesFact(l1).Flg_Estado <> 1 Or EstrutAnalisesFact(l1 + 1).Flg_Estado <> 1 Then
'        Exit Sub
'    End If
    FLG_ExecutaFGAna = False
    rv = Troca_Linha(l1 + 1, l1)
    If (rv = 1) Then
        LINHA_SEL = l1 + 1
    End If
    FLG_ExecutaFGAna = True
    FGAnalises.row = l1 + 1
    FGAnalises.Col = 0
    FGAnalises_click
    FGAnalises.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "BtBaixo_click"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------

' TROCA A ORDEM COM A LINHA DE CIMA

' ---------------------------------------------------------------------------
Private Sub BtCima_Click()

    On Error GoTo TrataErro
    
    Dim rv As Integer
    Dim l1 As Integer
    Dim l2 As Integer

    
    If LINHA_SEL <= 1 Or LINHA_SEL > fa_movi_resp(FgReq.row).totalAna Then Exit Sub
    l1 = LINHA_SEL
    'If EstrutAnalisesFact(l1).Flg_Estado <> 1 Or EstrutAnalisesFact(l1 - 1).Flg_Estado <> 1 Then
    '    Exit Sub
    'End If
    FLG_ExecutaFGAna = False
        
    rv = Troca_Linha(l1 - 1, l1)
    If (rv = 1) Then
        LINHA_SEL = l1 - 1
    End If
    
    
    FLG_ExecutaFGAna = True
    FGAnalises.row = l1 - 1
    FGAnalises.Col = 0
    FGAnalises_click
    FGAnalises.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "BtCima_click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtMinimizar_Click()
    FGAnalises.Height = 4260
    Frame2.Height = 4695
    Frame2.top = 3000
End Sub

'UALIA-871
Private Sub BtGestReq_Click()
    If EcNumReq.text <> "" Then
            FormGestaoRequisicaoPrivado.Show
            Set gFormActivo = FormGestaoRequisicaoPrivado
            FormGestaoRequisicaoPrivado.EcNumReq.text = EcNumReq.text
            FormGestaoRequisicaoPrivado.FuncaoProcurar
            'UALIA-919
            PesqRequis.FGReqPrivado = True
        End If
End Sub

Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
End Sub

Public Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, "")
End Sub


' ---------------------------------------------------------------------------

' RETIRA A AN�LISE DO ENVIO PARA FACTURACAO

' ---------------------------------------------------------------------------
Private Sub BtRetira_Click()
    
    On Error GoTo TrataErro
    
    If LINHA_SEL > fa_movi_resp(FgReq.row).totalAna Then Exit Sub
    'BRUNODSANTOS LJMANSO-306 - 10.08.2018
    'Adicionado If fa_movi_resp(FgReq.row).analises(LINHA_SEL).flg_estado <> 8 Then
    
    If fa_movi_resp(FgReq.row).analises(LINHA_SEL).flg_estado <> 1 Then
        If fa_movi_resp(FgReq.row).analises(LINHA_SEL).flg_estado <> 8 Then
            Exit Sub
        End If
    End If
     
    If fa_movi_resp(FgReq.row).analises(LINHA_SEL).flg_estado_doe = 3 Or fa_movi_resp(FgReq.row).analises(LINHA_SEL).flg_estado_tx = 3 Then
        gMsgTitulo = "Fatura��o"
        gMsgMsg = "An�lise j� tem fatura emitida ao doente. Apenas ser� isentada valor da entidade. Pretende continuar?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp <> vbYes Then
            Exit Sub
        End If
    End If

    If (BG_Mensagem(mediMsgBox, "Deseja retiar a an�lise " & _
                                FGAnalises.TextMatrix(LINHA_SEL, lColFactDescrRubr) & "?      ", _
                                vbQuestion + vbYesNo + vbDefaultButton2, _
                                " " & cAPLICACAO_NOME_CURTO) = vbNo) Then
        ' Sa�.
        Exit Sub
    Else
        If FACTUS_IsencaoEntidade(FgReq.row, LINHA_SEL) = False Then
            BG_Mensagem mediMsgBox, "Erro ao isentar atividade da an�lise!", vbExclamation
            Exit Sub
        End If
    End If
    PreencheFGAnalises FgReq.row
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "BtRetira_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub CbCodUrbano_Click()
    Dim iAna As Integer
    If ReqActual <= fa_movi_resp_tot Then
        For iAna = 1 To fa_movi_resp(FgReq.row).totalAna
            If CbCodUrbano.ListIndex <> mediComboValorNull Then
                EcKm.Enabled = True
                EcKm.SetFocus
            Else
                EcKm.text = ""
                EcKm.Enabled = False
                fa_movi_resp(FgReq.row).analises(iAna).km_dom = 0
            End If
            fa_movi_resp(FgReq.row).analises(iAna).cod_dom = BG_DaComboSel(CbCodUrbano)
        Next iAna
    End If
End Sub

Private Sub CbCodUrbano_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 Then
        BG_LimpaOpcao CbCodUrbano, KeyCode
        EcKm.text = ""
    End If
    
End Sub

Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        
        EcAux_LostFocus
    End If
End Sub

Private Sub EcAux_LostFocus()
    Dim i As Integer
    On Error GoTo TrataErro
    EcAux = UCase(EcAux)
    
    If COLUNA_EDITA = lColFactP1 Then
        For i = LINHA_EDITA To fa_movi_resp(FgReq.row).totalAna
            If EcAux <> "" Then
                AlteraP1 CLng(i), EcAux
            End If
        Next
        
    ElseIf COLUNA_EDITA = lColFactQTD Then
        If EcAux <> "" Then
            AlteraQTD CLng(LINHA_EDITA), EcAux
        End If
        
    ElseIf COLUNA_EDITA = lColFactCodRubrEFR Then
'            FGAnalises.TextMatrix(LINHA_EDITA, lColFactCodRubrEFR) = BL_HandleNull(EcAux, "0")
'            fa_movi_resp(FgReq.row).analises(LINHA_EDITA).cod_rubr_efr = BL_HandleNull(EcAux, "0")
            
    ElseIf COLUNA_EDITA = lColFactData Then
        If IsDate(EcAux.text) = True Then
            For i = FGAnalises.row To fa_movi_resp(FgReq.row).totalAna
                fa_movi_resp(FgReq.row).analises(i).dt_ini_real = EcAux.text
                fa_movi_resp(FgReq.row).analises(i).dt_fim_real = EcAux.text
                FGAnalises.TextMatrix(i, lColFactData) = EcAux.text
            Next i
        End If
    ElseIf COLUNA_EDITA = lColFactPercDoe Then
        If EcAux <> "" And CLng(EcAux) >= 0 And CLng(EcAux) <= 100 Then
            If (BG_Mensagem(mediMsgBox, "Deseja alterar para todas an�lises?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
                For i = 1 To fa_movi_resp(FgReq.row).totalAna
                    AlteraPercentDoente CLng(i), CDbl(EcAux)
                Next
            Else
                AlteraPercentDoente CLng(LINHA_EDITA), CDbl(EcAux)
            End If
        End If
    End If
    
    EcAux = ""
    EcAux.Visible = False
    FGAnalises.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "EcAux_LostFocus"
    Exit Sub
    Resume Next
End Sub



Public Sub EcCodsala_Validate(Cancel As Boolean)
    Cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
End Sub
Private Sub BtPesquisaSala_Click()
       PA_PesquisaSala EcCodSala, EcDescrSala, ""

End Sub

Private Sub EcKm_Validate(Cancel As Boolean)
    Dim iAna As Integer
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcKm)
    If EcKm.text = "" Then
        EcKm.text = "0"
    End If
    For iAna = 1 To fa_movi_resp(FgReq.row).totalAna
        fa_movi_resp(FgReq.row).analises(iAna).km_dom = IIf(EcKm.text = "", 0, EcKm.text)
    Next iAna
    
End Sub


Private Sub EcNumBenef_Validate(Cancel As Boolean)
    fa_movi_resp(FgReq.row).n_benef_doe = EcNumBenef.text
End Sub

Private Sub EcNumReq_Click()

    'EcNumReq.text = ""
    DoEvents
    
End Sub

Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    If EcNumReq <> "" And KeyCode = 13 Then
        FuncaoProcurar
    End If
End Sub

Private Sub EcNumReq_LostFocus()
    
    EcNumReq.Tag = adDecimal
    If Not BG_ValidaTipoCampo_ADO(Me, EcNumReq) Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub FgAnalises_dblClick()
    Dim i As Long
    'edgar.parada  LJMANSO-339 24.04.2019
    Dim credecialSelect As String
    '
    On Error GoTo TrataErro
    If FGAnalises.row <= fa_movi_resp(FgReq.row).totalAna Then
        If FGAnalises.Col = lColFactP1 Then
            If fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado = 1 Then
                'UALIA-943
                If gAtivaESP = mediSim Then
                    If CredencialESP(fa_movi_resp(FgReq.row).analises(FGAnalises.row).nr_req_ars) = True Then
                        Exit Sub
                    End If
                End If
                '
                EcAux = fa_movi_resp(FgReq.row).analises(FGAnalises.row).nr_req_ars
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        ElseIf FGAnalises.Col = lColFactQTD Then
            If fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado_doe = 3 Then
                BG_Mensagem mediMsgBox, "J� emitido documento para o doente. N�o pode alterar este dado", vbOKOnly + vbInformation, "Documento emitido"
                Exit Sub
            End If
            If FGAnalises.TextMatrix(FGAnalises.row, lColFactQTD) <> "" And fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado = 1 Then
                EcAux = fa_movi_resp(FgReq.row).analises(FGAnalises.row).qtd
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        ElseIf FGAnalises.Col = lColFactPercDoe Then
            If fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado_doe = 3 Then
                BG_Mensagem mediMsgBox, "J� emitido documento para o doente. N�o pode alterar este dado", vbOKOnly + vbInformation, "Documento emitido"
                Exit Sub
            End If
            If fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado = 1 Then
                EcAux = fa_movi_resp(FgReq.row).analises(FGAnalises.row).perc_pag_doe
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        ElseIf FGAnalises.Col = lColFactData Then
            If fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado = 3 Then
                BG_Mensagem mediMsgBox, "J� emitido documento para entidade. N�o pode alterar este dado", vbOKOnly + vbInformation, "Documento emitido"
                Exit Sub
            End If
            If fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado = 1 Then
                EcAux = fa_movi_resp(FgReq.row).analises(FGAnalises.row).dt_ini_real
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        ElseIf FGAnalises.Col = lColFactP1 Then
        
            If fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado_doe = 3 Then
                BG_Mensagem mediMsgBox, "J� emitido documento para o doente. N�o pode alterar este dado", vbOKOnly + vbInformation, "Documento emitido"
                Exit Sub
            End If
            If fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado = 1 Then
                EcAux = fa_movi_resp(FgReq.row).analises(FGAnalises.row).nr_req_ars
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        ElseIf FGAnalises.Col = lColFactIsencao Then
            ' edgar.parada  LJMANSO-339 24.04.2019
            If fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado_doe = 3 Or fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado_tx = 3 Then
                credecialSelect = FGAnalises.TextMatrix(FGAnalises.RowSel, lColFactP1)
                gMsgResp = BG_Mensagem(mediMsgBox, "J� emitido documento para o doente. Tem a certeza que pretende alterar para todas as an�lises da credencial " & credecialSelect & "?", vbQuestion + vbYesNo + vbDefaultButton2, "Altera��o de tipo isen��o!")
                If gMsgResp = vbYes Then
                   If fa_movi_resp(FgReq.row).analises(FGAnalises.row).cod_isen_doe = gTipoIsencaoIsento Then
                      For i = 1 To fa_movi_resp(FgReq.row).totalAna
                          If FGAnalises.TextMatrix(i, lColFactP1) = credecialSelect Then
                            fa_movi_resp(FgReq.row).analises(i).cod_isen_doe = gTipoIsencaoNaoIsento
                            FGAnalises.TextMatrix(i, lColFactIsencao) = "N�o Isento"
                          End If
                      Next
                   Else
                      For i = 1 To fa_movi_resp(FgReq.row).totalAna
                          If FGAnalises.TextMatrix(i, lColFactP1) = credecialSelect Then
                            fa_movi_resp(FgReq.row).analises(i).cod_isen_doe = gTipoIsencaoIsento
                            FGAnalises.TextMatrix(i, lColFactIsencao) = "Isento"
                          End If
                      Next
                   End If
                End If
            Else
                'LJMANSO-31
                'Adicionado fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado = 8
                If FGAnalises.row <= fa_movi_resp(FgReq.row).totalAna And (fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado = 1 Or fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado = 8) Then
                    If (BG_Mensagem(mediMsgBox, "Deseja alterar para todas an�lises?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
                        
                        If fa_movi_resp(FgReq.row).analises(FGAnalises.row).cod_isen_doe = gTipoIsencaoIsento Then
                            For i = 1 To fa_movi_resp(FgReq.row).totalAna
                                If FACTUS_AlteraIsencao(gTipoIsencaoNaoIsento, FgReq.row, i, "", _
                                                            1, mediComboValorNull, fa_movi_resp(FgReq.row).analises(i).cod_rubr) = True Then
                                    FGAnalises.TextMatrix(i, lColFactIsencao) = "N�o Isento"
                                End If
                            Next
                        Else
                            For i = 1 To fa_movi_resp(FgReq.row).totalAna
                                If FACTUS_AlteraIsencao(gTipoIsencaoIsento, FgReq.row, i, "", _
                                                            1, mediComboValorNull, fa_movi_resp(FgReq.row).analises(i).cod_rubr) = True Then
                                    FGAnalises.TextMatrix(i, lColFactIsencao) = "Isento"
                                End If
                            Next
                        End If
                        PreencheFGAnalises FgReq.row
                    Else
                        If fa_movi_resp(FgReq.row).analises(FGAnalises.row).cod_isen_doe = gTipoIsencaoIsento Then
                            If FACTUS_AlteraIsencao(gTipoIsencaoNaoIsento, FgReq.row, FGAnalises.row, "", _
                                                        1, mediComboValorNull, fa_movi_resp(FgReq.row).analises(FGAnalises.row).cod_rubr) = True Then
                                FGAnalises.TextMatrix(FGAnalises.row, lColFactIsencao) = "N�o Isento"
                            End If
                        Else
                            If FACTUS_AlteraIsencao(gTipoIsencaoIsento, FgReq.row, FGAnalises.row, "", _
                                                        1, mediComboValorNull, fa_movi_resp(FgReq.row).analises(FGAnalises.row).cod_rubr) = True Then
                                FGAnalises.TextMatrix(FGAnalises.row, lColFactIsencao) = "Isento"
                            End If
                        End If
                    End If
                  End If
                End If
        ElseIf FGAnalises.Col = lColFactConvencao Then
            If fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado_doe = 3 Then
                BG_Mensagem mediMsgBox, "J� emitido documento para o doente. N�o pode alterar este dado", vbOKOnly + vbInformation, "Documento emitido"
                Exit Sub
            End If
            If FGAnalises.row <= fa_movi_resp(FgReq.row).totalAna And fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_estado = 1 Then
                If (BG_Mensagem(mediMsgBox, "Deseja alterar para todas an�lises?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
                    Dim convencao As Integer
                    convencao = fa_movi_resp(FgReq.row).analises(FGAnalises.row).convencao
                    For i = 1 To fa_movi_resp(FgReq.row).totalAna
                        AlteraConvencao i, convencao
                    Next
                Else
                    AlteraConvencao FGAnalises.row, fa_movi_resp(FgReq.row).analises(FGAnalises.row).convencao
                End If
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "FGAnalises_DblClick"
    Exit Sub
    Resume Next
End Sub


Private Sub FGAnalises_RowColChange()
    If FLG_ExecutaFGAna = True Then
        FGAnalises_click
    End If
End Sub

Private Sub FgReq_Click()
    Dim i As Long
    Dim estado_req As String
    On Error GoTo TrataErro
        
    If FgReq.row > fa_movi_resp_tot Then
        Exit Sub
    End If
    
    LimpaCamposEcra
    LimpaFgAnalises
    
    'UALIA-919
    If PesqRequis.LinhaFgReq <> "" Then
        FgReq.row = PesqRequis.LinhaFgReq
        PesqRequis.LinhaFgReq = ""
    Else
        ReqActual = FgReq.row
    End If
    
    EcNumReq = fa_movi_resp(FgReq.row).episodio
    EcTipoUte = fa_movi_resp(FgReq.row).t_doente
    EcUtente = fa_movi_resp(FgReq.row).doente
    EcNome = NomeUte(FgReq.row)
    EcCodEFR = fa_movi_resp(FgReq.row).cod_efr
    EcDescrEFR = fa_movi_resp(FgReq.row).descr_efr
    EcDataInicial = fa_movi_resp(FgReq.row).dt_cri
    EcDataFinal = fa_movi_resp(FgReq.row).dt_cri
    EcNumBenef = fa_movi_resp(FgReq.row).n_benef_doe
    EcKm = fa_movi_resp(FgReq.row).qtd_dom
    LbLote.caption = fa_movi_resp(FgReq.row).n_fac & " " '& fa_movi_resp(FgReq.row).n_lote
    LbTotalRecibo.caption = TotalRecibos(FgReq.row)
    ' ---------
    ' URBANO
    ' ---------
    For i = 0 To CbCodUrbano.ListCount - 1
        If fa_movi_resp(FgReq.row).cod_dom = CbCodUrbano.ItemData(i) Then
            CbCodUrbano.ListIndex = i
            Exit For
        End If
    Next
    'LJMANSO-312
    'Call CarregaConvencao(fa_movi_resp(FgReq.row).episodio)
    '
    'NELSONPSILVA Glintt-HS-21031 * bloquear campos se for credencial importada
    CbCodUrbano.Enabled = True
    EcKm.Enabled = True
    If RequisicaoESP(EcNumReq.text) Then
        estado_req = BL_EstadoRequis(fa_movi_resp(FgReq.row).episodio)
        If (estado_req = gEstadoReqEsperaResultados Or estado_req = gEstadoReqResultadosParciais Or estado_req = gEstadoReqImpressaoParcial _
            Or estado_req = gEstadoReqValidacaoMedicaParcial Or estado_req = gEstadoReqTodasImpressas Or estado_req = gEstadoReqValicacaoMedicaCompleta Or estado_req = gEstadoReqEsperaValidacao) Then
            CbCodUrbano.Enabled = False
            EcKm.Enabled = False
        End If
    End If
    
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    
    DoEvents
    DoEvents
    DoEvents
    FuncaoProcurar_continua FgReq.row
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "FgReq_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub FuncaoProcurar_continua(linha As Long)
    FLG_ExecutaFGAna = False
    Dim StrAna As String
    PreencheEstrutAnalises fa_movi_resp(FgReq.row).episodio
    'NELSONPSILVA UALIA-857 01.02.2019
    If VerificaDuplicacaoRubricas(StrAna) Then
        BG_Mensagem mediMsgBox, "Foram encontradas rubricas repetidas para a requisi��o em causa para a(s) an�lise(s): " & vbNewLine & StrAna, vbExclamation + vbOKOnly
    End If
            
    BtRetira.Enabled = True
    BtAdiciona.Enabled = True
    BtCima.Enabled = True
    BtBaixo.Enabled = True
    BtValida.Enabled = True
    DoEvents
    
    lCredencialActiva = 1
    lPosicaoActual = 1
    FLG_ExecutaFGAna = True
    'UALIA-871
    BtGestReq.Enabled = True
    BtApagar.Enabled = True
    '
End Sub

Private Sub FgReq_RowColChange()
    'FgReq_Click
End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub FuncaoProcurar()
    
    On Error Resume Next
    
    Dim sRet As String
    
    Me.BtValida.Enabled = False
    DoEvents
    
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    DoEvents
    
    'UALIA-919
    PesqRequis.credencial = EcCredencial
    PesqRequis.NumReq = EcNumReq
    PesqRequis.TipoUte = EcTipoUte
    PesqRequis.codEfr = EcCodEFR
    PesqRequis.NumBenef = EcNumBenef
    PesqRequis.dataInicial = EcDataInicial
    PesqRequis.dataFinal = EcDataFinal
    PesqRequis.codSala = EcCodSala
            
    Preenche_Dados_Requisicao
    FLG_ExecutaFGAna = True
End Sub

' ---------------------------------------------------------------------------

' PREENCHE TODOS OS DADOS DA REQUISICAO E DO UTENTE EM CAUSA

' ---------------------------------------------------------------------------

Function Preenche_Dados_Requisicao() As Boolean
    
    On Error GoTo TrataErro
    Dim RsDet As New adodb.recordset
    
    Dim ssql As String
    Dim rsReq As adodb.recordset
    Dim rsUte As New adodb.recordset
    Dim i As Integer
    Dim flg_criterio As Boolean
    Dim iBD_Aberta As Long
    
    iBD_Aberta = BL_Abre_Conexao_Secundaria(gOracle)
    If iBD_Aberta = 0 Then
        Exit Function
    End If
    ' ---------------------------------------------------------------------------
    ' LIMPA ESTRUTURA DAS REQUISICOES A FACTURAR
    ' ---------------------------------------------------------------------------
    LimpaReqFact
    
    Preenche_Dados_Requisicao = True
    flg_criterio = False
    
    ssql = ""
    If EcCredencial.text <> "" Then
        flg_criterio = True
        ssql = ssql & " AND (t_episodio, episodio, n_ord) IN (select t_episodio, episodio, n_ord FROM  fa_movi_fact WHERE fa_movi_fact.nr_req_ars = " & BL_TrataStringParaBD(EcCredencial.text) & ")"
    End If
        
    If EcNumReq <> "" Then
        flg_criterio = True
        ssql = ssql & " AND t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(EcNumReq)
    End If
    
    If EcTipoUte <> "" And EcUtente <> "" Then
        flg_criterio = True
        ssql = ssql & " AND t_doente = " & BL_TrataStringParaBD(EcTipoUte) & " AND doente = " & BL_TrataStringParaBD(EcUtente)
    End If
    
        
    If EcCodEFR <> "" Then
        flg_criterio = True
        ssql = ssql & " AND x2.cod_Efr = " & EcCodEFR
    End If
        
    If EcNumBenef <> "" Then
        flg_criterio = True
        ssql = ssql & " AND n_benef_doe = " & BL_TrataStringParaBD(EcNumBenef)
    End If
            
    If EcDataInicial <> "" And EcDataFinal <> "" Then
        flg_criterio = True
        '***** Emanuel Sousa 15.05.2009 ********
        dataInicialReport = EcDataInicial.text
        dataFinalReport = EcDataFinal.text
        '***************************************
        ssql = ssql & " AND episodio in (SELECT n_req FROM sl_requis WHERE dt_chega between " & BL_TrataDataParaBD(EcDataInicial) & " AND " & BL_TrataDataParaBD(EcDataFinal) & ")"
    End If
    
    If EcCodSala.text <> "" Then
        flg_criterio = True
        ssql = ssql & " AND episodio in (SELECT n_req FROM sl_requis WHERE cod_sala = " & BL_TrataStringParaBD(EcCodSala.text) & ")"
    End If
    
        'edgar.parada  LJMANSO-339 24.04.2019
    If CbCodUrbano.ListIndex <> mediComboValorNull Then
       flg_criterio = True
       ssql = ssql & " AND cod_urbano = " & BG_DaComboSel(CbCodUrbano)
    End If
    '
    If flg_criterio = False Then
        BG_Mensagem mediMsgBox, "Tem que indicar um crit�rio.", vbOKOnly + vbInformation, "Crit�rios"
        Exit Function
    End If
        
        'edgar.parada  LJMANSO-339 24.04.2019 - parametro credencial
    FACTUS_PreencheRequis "", ssql, EcCredencial.text
    '
    If (fa_movi_resp_tot <= 0) Then
        Preenche_Dados_Requisicao = False
        MsgBox "Requisi��o inexistente.    ", vbExclamation, Me.caption
        rsReq.Close
        Set rsReq = Nothing
        Exit Function
    Else
        For i = 1 To fa_movi_resp_tot
            'FGONCALVES_UALIA
            ssql = "SELECT * FROM sl_movi_resp_det WHERE t_episodio ='SISLAB' and episodio = " & fa_movi_resp(i).episodio & " AND n_ord = " & fa_movi_resp(i).n_ord & " AND flg_confirmada = 1 "
            RsDet.CursorLocation = adUseClient
            RsDet.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros ssql
            RsDet.Open ssql, gConexao
            FgReq.row = i
            FgReq.Col = 0
            If RsDet.RecordCount > 0 Then
                FgReq.CellBackColor = vbGreen
            Else
                FgReq.CellBackColor = vbWhite
            End If
            RsDet.Close
            
            FgReq.TextMatrix(i, 0) = fa_movi_resp(i).episodio
            FgReq.AddItem ""
        Next i
        FgReq.row = 1
        FgReq_Click
    End If
    Preenche_Dados_Requisicao = True
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "Preenche_Dados_Requisicao"
    BG_Mensagem mediMsgBox, "Erro ao preencher Requisi��o: " & Err.Description, vbOKOnly + vbInformation, "Preencher Requisi��o"
    Preenche_Dados_Requisicao = False
    Exit Function
    Resume Next
End Function

Sub FuncaoLimpar()
    
    EcNumReq.text = ""
    EcCredencial.text = ""
    Me.EcNumReq.ForeColor = &H8000&
    LimpaCamposEcra
    EcNumReq.SetFocus
    
    LimpaFgAnalises
    'UALIA-919
    LimpaPesqRequis
    '
    
    Me.BtRetira.Enabled = False
    Me.BtAdiciona.Enabled = False
    Me.BtCima.Enabled = False
    Me.BtBaixo.Enabled = False
    Me.BtValida.Enabled = False
    LINHA_SEL = 0
    ReDim fa_movi_resp(0)
    fa_movi_resp_tot = 0
    
    DoEvents
    DoEvents
    'soliveira correccao lacto
    'EcNumReq.SetFocus
    LbTotalFactEFR = ""
    LimpaReqFact
    'EcCodEFR.Enabled = True
    'BtPesquisaEntFin.Enabled = True
    'UALIA-871
    BtGestReq.Enabled = False
    BtApagar.Enabled = False
    BtApagar.Enabled = False
    Call DefTipoCampos
    Call Inicializacoes
    '
End Sub

Sub FuncaoModificar()
    On Error GoTo TrataErro
    
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "FuncaoModificar"
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_tbf_t_urbano", "cod_t_urbano", "descr_t_urbano", CbCodUrbano
    EcAux.Visible = False
    'LJMANSO-312
    'BG_PreencheComboBD_ADO "sl_tbf_convencao", "cod_convencao", "descr_convencao", CbConvencao
End Sub

' Seleciona uma linha da grid.

Private Sub FGAnalises_click()

    On Error Resume Next
    Dim coluna As Integer
    Dim msf As Object
    Dim i As Integer
    Dim linha As Integer
    If FGAnalises.CellFontBold = True Then
        Exit Sub
    End If
    If FLG_ExecutaFGAna = True Then
        FLG_ExecutaFGAna = False
                
        'FGAnalises.Col = coluna
        LINHA_SEL = FGAnalises.row
        COLUNA_SEL = FGAnalises.Col
        FLG_ExecutaFGAna = True
    End If
    
    EcCredencial = fa_movi_resp(FgReq.row).analises(FGAnalises.row).nr_req_ars
    'edgar.parada Glintt-HS-21241 24.05.2019
    'If fa_movi_resp(FgReq.row).analises(FGAnalises.row).flg_erro_efect = 1 Then
    '   lb_erro_efec.Visible = True
    'Else
    '   lb_erro_efec.Visible = False
    'End If
    '
End Sub

' ---------------------------------------------------------------------------

' TROCA DUAS LINHAS NA ESTRUTURA E NA GRELHA

' ---------------------------------------------------------------------------

Private Function Troca_Linha(linha1 As Integer, linha2 As Integer) As Integer
    FACTUS_AlteraOrdemAnalises FgReq.row, linha1, linha2
    
    
    PreencheFGAnalises FgReq.row

    
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "Troca_Linha"
    Troca_Linha = -1
    Exit Function
End Function

' ---------------------------------------------------------------------------

' PREENCHE A ESTRUTURA COM AS ANALISES DA REQUISI��O

' ---------------------------------------------------------------------------

Public Function PreencheEstrutAnalises(requisicao As String, Optional NaoActualizaGrid As Boolean) As Integer
    Dim ssql As String
    Dim TotalFactEnt As Double
    Dim i As Integer
    Dim flg_encontrouP1 As Boolean
    On Error GoTo TrataErro
    
    TotalFactEnt = 0
    
    ' -------------- Emanuel Sousa 15.05.2009 -----------------------------------
    If (NaoActualizaGrid = False) Then
            PreencheFGAnalises FgReq.row
            DoEvents
            FLG_ExecutaFGAna = False
            FLG_ExecutaFGAna = True
            ' ---------------------------------------------------------------------------
            ' AUMENTA GRELHA E ESTRUTURA
            ' ---------------------------------------------------------------------------
        If fa_movi_resp(FgReq.row).totalAna >= (NumLinhas - 1) Then
            FGAnalises.AddItem ""
        End If
    End If
    FGAnalises.row = 1
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "PreencheEstrutAnalises"
    BG_Mensagem mediMsgBox, "Erro ao preencher An�lises: " & Err.Description, vbOKOnly + vbInformation, "Preencher An�lises"
    PreencheEstrutAnalises = -1
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------

' PREENCHE A ESTRUTURA COM AS ANALISES DA REQUISI��O

' ---------------------------------------------------------------------------

Private Sub BtValida_Click()
    Dim ssql As String
    Dim codDom As Integer
    Dim flg_insere_cod_rubr_efr As Boolean
    Dim i As Integer
    Dim actP1 As String
    Dim n_ord_ins As Integer
    Dim iReg As Integer
    
    On Error GoTo TrataErro
    
    'NELSONPSILVA UALIA-860 31.01.2019
    'If VerificaTotalAnaCred = False Then
    '    BG_Mensagem mediMsgBox, "Existem credenciais da ARS com mais que 6 an�lises", vbOKOnly + vbInformation, "Factura��o"
    '    Exit Sub
    'End If
    '
     
    If CbCodUrbano.ListIndex > mediComboValorNull Then
        codDom = CbCodUrbano.ItemData(CbCodUrbano.ListIndex)
    Else
        codDom = "0"
    End If
    
     FgReq.CellBackColor = vbGreen
     
     'LJMANSO-312
     'Call AlteraConvencaoReq
     '
     If FACTUS_GravaDados(CLng(fa_movi_resp(FgReq.row).episodio), FgReq.row) = True Then
        
        
        'FGONCALVES_UALIA
        ssql = "UPDATE sl_movi_resp_det SET FLG_CONFIRMADA = 1 WHERE t_episodio ='SISLAB' and episodio = " & fa_movi_resp(FgReq.row).episodio & " AND n_ord = " & fa_movi_resp(FgReq.row).n_ord
        iReg = BG_ExecutaQuery_ADO(ssql)
        If iReg = 0 Then
           ssql = "INSERT INTO SL_MOVI_RESP_DET (T_EPISODIO, EPISODIO, N_ORD, FLG_CONFIRMADA) VALUES("
           ssql = ssql & "'SISLAB'," & fa_movi_resp(FgReq.row).episodio & "," & fa_movi_resp(FgReq.row).n_ord & ",1)"
           BG_ExecutaQuery_ADO ssql
        End If
        'BRUNODSANTOS LJMANSO-301 06.08.2018
        FACTUS_OrdenaCredenciais (EcNumReq.text)
        '
        'UALIA-861
        FgReq.CellBackColor = vbGreen
        '
        BG_Mensagem mediMsgBox, "Requisi��o alterada no FACTUS. ", vbOKOnly + vbInformation, "Preencher Requisi��o"
     End If
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "BtValida_Click"
    BG_Mensagem mediMsgBox, "Requisi��o alterada no FACTUS. ", vbOKOnly + vbInformation, "Preencher Requisi��o"
    gConexaoSecundaria.RollbackTrans
    Exit Sub
    Resume Next
End Sub


' ---------------------------------------------------------------------------

' LIMPA A ESTRUTURA QUE CONTEM OS DADOS DA REQUISICAO A FACTURAR

' ---------------------------------------------------------------------------


Private Sub LimpaReqFact()
    FgReq.rows = 2
    FgReq.TextMatrix(1, 0) = ""
    FgReq.CellBackColor = vbWhite
End Sub




' ---------------------------------------------------------------------------

' LIMPA A GRELHA DAS ANALISES

' ---------------------------------------------------------------------------

Private Sub LimpaFgAnalises()
    Dim i As Integer
    For i = 1 To FGAnalises.rows - 2
        FGAnalises.RemoveItem 1
    Next
    FLG_ExecutaFGAna = False
    FGAnalises.rows = 1
    FGAnalises.rows = NumLinhas
    FLG_ExecutaFGAna = True
End Sub

Private Sub Ecdatareq_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub



' ---------------------------------------------------------------------------

' ALTERA NUMERO DO P1

' ---------------------------------------------------------------------------
Private Sub AlteraP1(linha As Long, NovoP1 As String)
    Dim ssql As String
    On Error GoTo TrataErro
    fa_movi_resp(FgReq.row).analises(linha).nr_req_ars = NovoP1
    fa_movi_resp(FgReq.row).analises(linha).chave_prog = Mid(fa_movi_resp(FgReq.row).analises(linha).episodio & fa_movi_resp(FgReq.row).analises(linha).nr_req_ars, 2, 15)
    FGAnalises.TextMatrix(linha, lColFactP1) = NovoP1
    
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "AlteraP1"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------

' ALTERA QUANTIDADE DO DOENTE

' ---------------------------------------------------------------------------
Private Sub AlteraQTD(linha As Integer, qtd As Long)
    On Error GoTo TrataErro

    fa_movi_resp(FgReq.row).analises(linha).qtd = qtd
    FACTUS_CalculaQuantidade FgReq.row, linha, False, 0, False, "", 0
    
    PreencheFGAnalises FgReq.row
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "AlteraQTD"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------

' ALTERA PERCENTAGEM DO DOENTE

' ---------------------------------------------------------------------------
Private Sub AlteraPercentDoente(linha As Long, percentagem As Double)
    On Error GoTo TrataErro

'    EstrutAnalisesFact(linha).perc_pag_doe = percentagem
'    EstrutAnalisesFact(linha).perc_pag_efr = CStr(100 - percentagem)
'    EstrutAnalisesFact(linha).val_pr_u_doe = Round(((CDbl(EstrutAnalisesFact(linha).val_total_acto) * percentagem) / 100), 2)
'    EstrutAnalisesFact(linha).val_pag_doe = Round(((CDbl((EstrutAnalisesFact(linha).val_total_acto) * percentagem) / 100)) * EstrutAnalisesFact(linha).qtd, 2)
'    EstrutAnalisesFact(linha).val_pr_unit = Round((CDbl(EstrutAnalisesFact(linha).val_total_acto) - EstrutAnalisesFact(linha).val_pr_u_doe), 2)
'    EstrutAnalisesFact(linha).val_total = Round((CDbl(EstrutAnalisesFact(linha).val_total_acto) - EstrutAnalisesFact(linha).val_pr_u_doe) * EstrutAnalisesFact(linha).qtd, 2)
'    PreencheFGAnalises FgReq.row
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "AlteraPercentDoente"
    Exit Sub
    Resume Next
End Sub


' ---------------------------------------------------------------------------

' ALTERA TIPO CONVENCAO

' ---------------------------------------------------------------------------
Private Sub AlteraConvencao(linha As Long, convencao As Integer)
    On Error GoTo TrataErro
    
    If convencao = lCodInternacional Then
        fa_movi_resp(FgReq).analises(linha).convencao = lCodNormal
        fa_movi_resp(FgReq).analises(linha).flg_tip_req = "NV"
        FGAnalises.TextMatrix(linha, lColFactConvencao) = "Normal"
    ElseIf convencao = lCodNormal Then
        fa_movi_resp(FgReq).analises(linha).convencao = lCodDomicilio
        fa_movi_resp(FgReq).analises(linha).flg_tip_req = "DV"
        FGAnalises.TextMatrix(linha, lColFactConvencao) = "Domicilio"
    Else
        fa_movi_resp(FgReq).analises(linha).convencao = lCodInternacional
        fa_movi_resp(FgReq).analises(linha).flg_tip_req = "IV"
        FGAnalises.TextMatrix(linha, lColFactConvencao) = "Internacional"
    End If
    
    'LJMANSO-312
'    fa_movi_resp(FgReq.RowSel).analises(linha).convencao = DevolveConvencao(convencao)
'    fa_movi_resp(FgReq.RowSel).analises(linha).flg_tip_req = DevolveFlgTipReq(convencao)
    
    
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "AlteraConvencao"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataInicial_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
   If Trim(EcDataInicial.text) = "" Then
        EcDataInicial.text = Bg_DaData_ADO
        'SendKeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDataInicial_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataInicial)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    Else
    End If
    
End Sub

Private Sub EcDataFinal_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
   If Trim(EcDataFinal.text) = "" Then
        EcDataFinal.text = Bg_DaData_ADO
        'SendKeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDataFinal_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataFinal)
    If Cancel Then
        'SendKeys ("{HOME}+{END}")
    Else
    End If
    
End Sub
Private Sub LimpaCamposEcra()
    EcNumReq = ""
    EcCredencial.text = ""
    EcTipoUte = ""
    EcUtente = ""
    EcNome = ""
    EcCodEFR = ""
    EcDescrEFR = ""
    EcNumBenef = ""
    EcDataFinal = ""
    EcDataInicial = ""
    CbCodUrbano.ListIndex = mediComboValorNull
    EcEstado = ""
    LbLote = ""
    EcKm.text = ""
    LbTotalRecibo.caption = ""
    EcCodSala.text = ""
    EcCodsala_Validate False
    'LJMANSO-312
    'CbConvencao.ListIndex = mediComboValorNull
End Sub


Private Sub PreencheLoteFac(linha As Long)
    Dim ssql As String
    Dim rsLote As New adodb.recordset
    On Error GoTo TrataErro
    
    ssql = "select serie_fac, n_fac, n_lote from fa_lin_fact where episodio = " & BL_TrataStringParaBD(fa_movi_resp(FgReq.row).episodio)
    rsLote.CursorLocation = adUseClient
    rsLote.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsLote.Open ssql, gConexaoSecundaria
    If rsLote.RecordCount > 0 Then
        fa_movi_resp(FgReq.row).n_fac = BL_HandleNull(rsLote!serie_fac, "") & "/" & BL_HandleNull(rsLote!n_fac, "")
        'ReqFact(linha).n_lote = BL_HandleNull(rsLote!n_lote, "")
    End If
    rsLote.Close
    Set rsLote = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "PreencheLoteFac"
    Exit Sub
    Resume Next
End Sub


' ---------------------------------------------------------------------------

' PREENCHE A GRELHA

' ---------------------------------------------------------------------------

Private Sub PreencheFGAnalises(iResp As Integer)
    On Error GoTo TrataErro
    Dim cor As Long
    Dim iMoviFact As Integer
    'edgar.parada Glintt-HS-21241 20.05.2019
    Dim lastCredencial As String
    lastCredencial = ""
    lb_erro_efec.Visible = False
    '
    FGAnalises.Visible = False
    LimpaFgAnalises
    For iMoviFact = 1 To fa_movi_resp(iResp).totalAna
'        UALIA-871
'        If fa_movi_resp(iResp).analises(iMoviFact).flg_estado = 8 Or fa_movi_resp(iResp).analises(iMoviFact).flg_estado = 2 Then
'            cor = vermelhoClaro
'        Else
'            cor = vbWhite
'        End If
        
        'edgar.parada Glintt-HS-21241 20.05.2019
        If fa_movi_resp(iResp).analises(iMoviFact).nr_req_ars <> lastCredencial Then
            lastCredencial = fa_movi_resp(iResp).analises(iMoviFact).nr_req_ars
            If lastCredencial <> "" And CredencialESP(lastCredencial) And verificaErroEfectivacao(lastCredencial) Then
                cor = RGB(255, 99, 71) 'vermelho
                lb_erro_efec.Visible = True
                'fa_movi_resp(iResp).analises(iMoviFact).flg_erro_efect = 1
            Else
                cor = DefineCorLinha(iResp, iMoviFact)
            End If
        ElseIf fa_movi_resp(iResp).analises(iMoviFact).nr_req_ars = "" Then
            cor = DefineCorLinha(iResp, iMoviFact)
        End If
        '
        FGAnalises.row = iMoviFact
        
        FGAnalises.TextMatrix(iMoviFact, lColFactP1) = fa_movi_resp(iResp).analises(iMoviFact).nr_req_ars
        FGAnalises.Col = lColFactP1
        FGAnalises.CellBackColor = cor
        FGAnalises.TextMatrix(iMoviFact, lColFactData) = fa_movi_resp(iResp).analises(iMoviFact).dt_ini_real
        FGAnalises.Col = lColFactData
        FGAnalises.CellBackColor = cor
        FGAnalises.TextMatrix(iMoviFact, lColFactCodRubr) = fa_movi_resp(iResp).analises(iMoviFact).cod_rubr
        FGAnalises.Col = lColFactCodRubr
        FGAnalises.CellBackColor = cor
        FGAnalises.TextMatrix(iMoviFact, lColFactDescrRubr) = fa_movi_resp(iResp).analises(iMoviFact).descr_rubr
        FGAnalises.Col = lColFactDescrRubr
        FGAnalises.CellBackColor = cor
        FGAnalises.TextMatrix(iMoviFact, lColFactCodRubrEFR) = fa_movi_resp(iResp).analises(iMoviFact).cod_rubr_efr
        FGAnalises.Col = lColFactCodRubrEFR
        FGAnalises.CellBackColor = cor
        FGAnalises.TextMatrix(iMoviFact, lColFactQTD) = fa_movi_resp(iResp).analises(iMoviFact).qtd
        FGAnalises.Col = lColFactQTD
        FGAnalises.CellBackColor = cor
        'FGAnalises.TextMatrix(iMoviFact, lColFactValActo) = Round(fa_movi_resp(iResp).analises(iMoviFact).qtd * (fa_movi_resp(iResp).analises(iMoviFact).val_pr_unit + fa_movi_resp(iResp).analises(iMoviFact).val_pr_u_doe + fa_movi_resp(iResp).analises(iMoviFact).val_Taxa), 2)
        FGAnalises.TextMatrix(iMoviFact, lColFactValActo) = Round((fa_movi_resp(iResp).analises(iMoviFact).qtd * (fa_movi_resp(iResp).analises(iMoviFact).val_pr_unit) + fa_movi_resp(iResp).analises(iMoviFact).val_pag_doe + fa_movi_resp(iResp).analises(iMoviFact).val_Taxa), 2)
        FGAnalises.Col = lColFactValActo
        FGAnalises.CellBackColor = cor
        FGAnalises.TextMatrix(iMoviFact, lColFactValEFR) = Round(fa_movi_resp(iResp).analises(iMoviFact).qtd * (fa_movi_resp(iResp).analises(iMoviFact).val_pr_unit), 2)
        FGAnalises.Col = lColFactValEFR
        FGAnalises.CellBackColor = cor
        'FGAnalises.TextMatrix(iMoviFact, lColFactValDoe) = Round(fa_movi_resp(iResp).analises(iMoviFact).qtd * (fa_movi_resp(iResp).analises(iMoviFact).val_pr_u_doe + fa_movi_resp(iResp).analises(iMoviFact).val_Taxa), 2)
        FGAnalises.TextMatrix(iMoviFact, lColFactValDoe) = Round((fa_movi_resp(iResp).analises(iMoviFact).qtd * (fa_movi_resp(iResp).analises(iMoviFact).val_pag_doe) + fa_movi_resp(iResp).analises(iMoviFact).val_Taxa), 2)
        FGAnalises.Col = lColFactValDoe
        FGAnalises.CellBackColor = cor
        FGAnalises.TextMatrix(iMoviFact, lColFactPercDoe) = fa_movi_resp(iResp).analises(iMoviFact).perc_pag_doe
        FGAnalises.Col = lColFactPercDoe
        FGAnalises.CellBackColor = cor
        'ISEN��O
        If fa_movi_resp(iResp).analises(iMoviFact).cod_isen_doe = gTipoIsencaoIsento Then
            FGAnalises.TextMatrix(iMoviFact, lColFactIsencao) = "Isento"
        Else
            FGAnalises.TextMatrix(iMoviFact, lColFactIsencao) = "N�o Isento"
        End If
        FGAnalises.Col = lColFactIsencao
        FGAnalises.CellBackColor = cor
        
        'CONVENCAO
        If fa_movi_resp(iResp).analises(iMoviFact).convencao = lCodInternacional Then
            FGAnalises.TextMatrix(iMoviFact, lColFactConvencao) = "Internacional"
        ElseIf fa_movi_resp(iResp).analises(iMoviFact).convencao = lCodDomicilio Then
            FGAnalises.TextMatrix(iMoviFact, lColFactConvencao) = "Domicilio"
        ElseIf fa_movi_resp(iResp).analises(iMoviFact).convencao = lCod97 Then
            FGAnalises.TextMatrix(iMoviFact, lColFactConvencao) = "97"
        Else
            FGAnalises.TextMatrix(iMoviFact, lColFactConvencao) = "Normal"
        End If
        FGAnalises.Col = lColFactConvencao
        FGAnalises.CellBackColor = cor
        'LJMANSO-312
        FGAnalises.TextMatrix(iMoviFact, lColFact_SerieFact) = IIf(fa_movi_resp(iResp).analises(iMoviFact).serie_fac = "-1", "", fa_movi_resp(iResp).analises(iMoviFact).serie_fac)
        FGAnalises.TextMatrix(iMoviFact, lColFact_NFact) = IIf(fa_movi_resp(iResp).analises(iMoviFact).n_fac = "-1", "", fa_movi_resp(iResp).analises(iMoviFact).n_fac)
        '
        'UALIA-871
       ' FGAnalises.CellBackColor = cor
        FGAnalises.Col = lColFact_NFact
        FGAnalises.CellBackColor = cor
        FGAnalises.Col = lColFact_SerieFact
        FGAnalises.CellBackColor = cor
        
        If fa_movi_resp(iResp).analises(iMoviFact).serie_fac_doe <> "" Or fa_movi_resp(iResp).analises(iMoviFact).n_Fac_doe <> -1 Then
            Me.Width = 18750
            Frame2.Width = 16200
            Frame3.Width = 16200
            FGAnalises.Width = 16000
            FGAnalises.Col = lColFact_NFacDoe
            FGAnalises.CellBackColor = cor
            FGAnalises.ColWidth(lColFact_NFacDoe) = 900
            FGAnalises.Col = lColFact_SerieFactDoe
            FGAnalises.CellBackColor = cor
            FGAnalises.ColWidth(lColFact_SerieFactDoe) = 970
            FGAnalises.TextMatrix(iMoviFact, lColFact_SerieFactDoe) = fa_movi_resp(iResp).analises(iMoviFact).serie_fac_doe
            FGAnalises.TextMatrix(iMoviFact, lColFact_NFacDoe) = fa_movi_resp(iResp).analises(iMoviFact).n_Fac_doe
            BtAdiciona.left = 17490
            BtRetira.left = 17490
            BtCima.left = 17610
            BtBaixo.left = 17610
            
            If FGAnalises.ColWidth(lColFact_SerieFactTX) <> 0 Or FGAnalises.ColWidth(lColFact_NFacTX) <> 0 Then
                Me.Width = 20500
                Frame2.Width = 17900
                Frame3.Width = 17900
                FGAnalises.Width = 17750
                BtAdiciona.left = 19200
                BtRetira.left = 19200
                BtCima.left = 19320
                BtBaixo.left = 19320
            End If
        End If
        
         If fa_movi_resp(iResp).analises(iMoviFact).serie_fac_tx <> "" Or fa_movi_resp(iResp).analises(iMoviFact).n_fac_tx <> -1 Then
            Frame2.Width = 17900
            Frame3.Width = 17900
            Me.Width = 20500
            FGAnalises.Width = 17750
            FGAnalises.Col = lColFact_NFacTX
            FGAnalises.CellBackColor = cor
            FGAnalises.ColWidth(lColFact_NFacTX) = 900
            FGAnalises.Col = lColFact_SerieFactTX
            FGAnalises.CellBackColor = cor
            FGAnalises.ColWidth(lColFact_SerieFactTX) = 970
            FGAnalises.TextMatrix(iMoviFact, lColFact_SerieFactTX) = fa_movi_resp(iResp).analises(iMoviFact).serie_fac_tx
            FGAnalises.TextMatrix(iMoviFact, lColFact_NFacTX) = fa_movi_resp(iResp).analises(iMoviFact).n_fac_tx
            BtAdiciona.left = 19200
            BtRetira.left = 19200
            BtCima.left = 19320
            BtBaixo.left = 19320
            
            If FGAnalises.ColWidth(lColFact_SerieFactDoe) = 0 Or FGAnalises.ColWidth(lColFact_NFacDoe) = 0 Then
                Me.Width = 18750
                Frame2.Width = 16200
                Frame3.Width = 16200
                FGAnalises.Width = 16000
                BtAdiciona.left = 17490
                BtRetira.left = 17490
                BtCima.left = 17610
                BtBaixo.left = 17610
            End If
        End If
        '
        If iMoviFact >= FGAnalises.rows - 1 Then
            FGAnalises.AddItem ""
        End If
    Next iMoviFact
    FGAnalises.Visible = True
    
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "PreencheFGAnalises"
    Exit Sub
    Resume Next
End Sub


' ---------------------------------------------------------------------------

' VERIFICA SE PODE MUDAR ENTIDADE PARA REQUISICAO - SE JA FOI FACTURADA

' ---------------------------------------------------------------------------
Private Function VeriricaPodeMudarEFR() As Boolean
    Dim ssql As String
    Dim rsLinFact As New adodb.recordset
    On Error GoTo TrataErro
    ssql = "SELECT * FROM fa_lin_Fact WHERE episodio = " & BL_TrataStringParaBD(fa_movi_resp(FgReq.row).episodio)
    rsLinFact.CursorLocation = adUseClient
    rsLinFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsLinFact.Open ssql, gConexaoSecundaria
    If rsLinFact.RecordCount > 0 Then
        VeriricaPodeMudarEFR = False
    Else
        VeriricaPodeMudarEFR = True
    End If
    rsLinFact.Close
    Set rsLinFact = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "VeriricaPodeMudarEFR"
    Exit Function
    Resume Next
End Function

Private Function TotalRecibos(iResp As Integer) As String

    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    Dim total
    On Error GoTo TrataErro
    
    ssql = "SELECT * FROM sl_recibos WHERE n_req = " & BL_TrataStringParaBD(fa_movi_resp(iResp).episodio)
    ssql = ssql & " AND cod_Efr = " & fa_movi_resp(iResp).cod_efr & " AND estado = 'P' "
    
    rsRecibos.CursorLocation = adUseClient
    rsRecibos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao
    If rsRecibos.RecordCount > 0 Then
        total = 0
        While Not rsRecibos.EOF
            total = total + CDbl(Replace(BL_HandleNull(rsRecibos!total_pagar, ""), ".", ","))
            rsRecibos.MoveNext
        Wend
        TotalRecibos = CStr(total)
    Else
        TotalRecibos = ""
    End If
    rsRecibos.Close
    Set rsRecibos = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "TotalRecibos"
    Exit Function
    Resume Next
End Function
Private Function TrataBool(Param As Boolean) As Integer
'Emanuel Sousa 05.2009
    Dim res As Integer
    res = 0

        If (Param = True) Then
            res = 1
        Else
            res = 0
        End If

End Function

Public Sub FuncaoImprimir()
'Emanuel Sousa 05.2009
    Dim ssql As String
    Dim i, j As Integer
    Dim continua As Boolean
    Dim NomeEFR As String
    Dim serie_fac_doe As String
    Dim n_Fac_doe As Long
    ssql = "DELETE FROM SL_CR_CONFIRMACAO_FACTURACAO WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO ssql
    BG_Trata_BDErro
    

    For i = 1 To fa_movi_resp_tot
    
        
        For j = 1 To fa_movi_resp(i).totalAna
            serie_fac_doe = ""
            n_Fac_doe = mediComboValorNull
            If fa_movi_resp(i).analises(j).serie_fac_doe <> "" Then
                serie_fac_doe = fa_movi_resp(i).analises(j).serie_fac_doe
                n_Fac_doe = fa_movi_resp(i).analises(j).n_Fac_doe
            Else
                serie_fac_doe = fa_movi_resp(i).analises(j).serie_fac_tx
                n_Fac_doe = fa_movi_resp(i).analises(j).n_fac_tx
 
            End If
            '********************* SQL campos a inserir *********************
    
            ssql = "INSERT INTO SL_CR_CONFIRMACAO_FACTURACAO (n_ord_req, t_episodio_req, "
            ssql = ssql & "cod_empresa_req, nreq_req, user_cri_req, hr_chega_req, n_benef_req, "
            ssql = ssql & "estadofact_req, codurbano_req, km_req, codefr_req, nomeutente_req, "
            ssql = ssql & "utente_req, tipoutente_req, dt_cri_req, n_lote_req, n_fac_req, "
            ssql = ssql & "totalrecibos_req, facturapercent_req, episodio_ana, n_seq_prog_ana, n_ord_ana, "
            ssql = ssql & "dt_ini_real_ana, cod_rubr_ana, cod_rubr_efr_ana, descr_rubr_ana, val_pr_unit_ana, "
            ssql = ssql & "val_pag_efr, qtd_ana, val_total_acto_ana, val_total_ana, perc_pag_efr_ana, perc_pag_doe_ana, "
            ssql = ssql & "val_pr_u_doe_ana, val_pag_doe_ana, cod_isen_doe_ana, flg_pagou_taxa_ana, "
            ssql = ssql & "nr_req_ars_ana, flg_estado_ana, chave_prog_ana, n_ord_ins_ana, "
            ssql = ssql & "flg_tip_req_ana, p1_ana, convencao_ana, cor_ana, "
            ssql = ssql & "cod_serv_exec_ana, flg_estado_doe_ana, serie_fac_doe_ana, n_fac_doe_ana, nome_computador, desc_efr, nr_c) VALUES ("
            
            '**************** SQL valores dos campos a inserir Requisicao ***************
            
            ssql = ssql & fa_movi_resp(i).analises(j).n_ord & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).t_episodio) & ", "
            ssql = ssql & BL_TrataStringParaBD("") & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).episodio) & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).user_cri) & ", "
            ssql = ssql & BL_TrataStringParaBD("") & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).n_benef_doe) & ", "
            ssql = ssql & fa_movi_resp(i).analises(j).flg_estado & ", "
            ssql = ssql & fa_movi_resp(i).analises(j).cod_dom & ", "
            ssql = ssql & fa_movi_resp(i).analises(j).km_dom & ", "
            ssql = ssql & BL_TrataStringParaBD(CStr(fa_movi_resp(i).cod_efr)) & ", "
            ssql = ssql & BL_TrataStringParaBD(NomeUte(FgReq.row)) & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).doente) & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).t_doente) & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).dt_cri) & ", "
            ssql = ssql & BL_TrataStringParaBD("") & ", "
            ssql = ssql & BL_TrataStringParaBD(CStr(fa_movi_resp(i).analises(j).n_fac)) & ", "
            ssql = ssql & BL_TrataStringParaBD("0") & ", "
            ssql = ssql & "null, "
            
            '**************** SQL valores dos campos a inserir Analises ***************
            
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).episodio) & ", "
            ssql = ssql & fa_movi_resp(i).analises(j).n_seq_prog & ", "
            ssql = ssql & fa_movi_resp(i).analises(j).n_ord & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).dt_ini_real) & ", "
            ssql = ssql & BL_TrataStringParaBD(CStr(fa_movi_resp(i).analises(j).cod_rubr)) & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).cod_rubr_efr) & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).descr_rubr) & ", "
            ssql = ssql & BL_HandleNull(Replace((fa_movi_resp(i).analises(j).val_pr_unit), ",", "."), 0) & ", "
            ssql = ssql & BL_HandleNull(Replace((fa_movi_resp(i).analises(j).val_total), ",", "."), 0) & ", "
            ssql = ssql & fa_movi_resp(i).analises(j).qtd & ", "
            ssql = ssql & BL_HandleNull(Replace(fa_movi_resp(i).analises(j).val_pag_doe, ",", "."), 0) & ", "
            ssql = ssql & BL_HandleNull(Replace(fa_movi_resp(i).analises(j).val_total, ",", "."), 0) & ", "
            ssql = ssql & BL_HandleNull(Replace(fa_movi_resp(i).analises(j).perc_pag_efr, ",", "."), 0) & ", "
            ssql = ssql & BL_HandleNull(Replace(fa_movi_resp(i).analises(j).perc_pag_doe, ",", "."), 0) & ", "
            ssql = ssql & BL_HandleNull(Replace(fa_movi_resp(i).analises(j).val_pr_u_doe, ",", "."), 0) & ", "
            ssql = ssql & BL_HandleNull(Replace(fa_movi_resp(i).analises(j).val_pag_doe, ",", "."), 0) & ", "
            ssql = ssql & fa_movi_resp(i).analises(j).cod_isen_doe & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).flg_pagou_taxa) & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).nr_req_ars) & ", "
            ssql = ssql & fa_movi_resp(i).analises(j).flg_estado & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).chave_prog) & ", "
            ssql = ssql & fa_movi_resp(i).analises(j).n_ord_ins & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).flg_tip_req) & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).analises(j).nr_req_ars) & ", "
            ssql = ssql & BL_TrataStringParaBD(CStr(fa_movi_resp(i).analises(j).convencao)) & ", "
            ssql = ssql & BL_TrataStringParaBD("") & ", "
            ssql = ssql & BL_TrataStringParaBD(CStr(fa_movi_resp(i).analises(j).cod_serv_exec)) & ", "
            ssql = ssql & fa_movi_resp(i).analises(j).flg_estado_doe & ", "
            ssql = ssql & BL_TrataStringParaBD(serie_fac_doe) & ", "
            ssql = ssql & BL_TrataNumberParaBD(CStr(n_Fac_doe)) & ", "
            
            '**************** SQL para gravar o computador q pediu a listagem ***************
            
            ssql = ssql & BL_TrataStringParaBD(gComputador) & ", "
            ssql = ssql & BL_TrataStringParaBD(fa_movi_resp(i).descr_efr) & ", "
            ssql = ssql & BL_HandleNull(Replace(fa_movi_resp(i).analises(j).nr_c, ",", "."), 0) & ") "
    
            BG_ExecutaQuery_ADO ssql
            BG_Trata_BDErro
        Next
        j = 0
    Next
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("ListagemConfirmacaoFacturacao", "Listagem de Confirma��o de Factura��o", crptToPrinter)
    Else
        continua = BL_IniciaReport("ListagemConfirmacaoFacturacao", "Listagem de Confirma��o de Factura��o", crptToWindow)
    End If
    
    If continua = False Then Exit Sub
        Dim ReportDetalhado As CrystalReport
        Set ReportDetalhado = forms(0).Controls("Report")
                
           ReportDetalhado.SQLQuery = "SELECT CODEFR_REQ," & _
                                      "     COD_EMPRESA_REQ, " & _
                                      "     NREQ_REQ, " & _
                                      "     N_BENEF_REQ, " & _
                                      "     NOMEUTENTE_REQ, " & _
                                      "     COD_RUBR_ANA, " & _
                                      "     DESCR_RUBR_ANA, " & _
                                      "     QTD_ANA, " & _
                                      "     VAL_TOTAL_ACTO_ANA, " & _
                                      "     VAL_PAG_DOE_ANA " & _
                                      "FROM " & _
                                      "     SL_CR_CONFIRMACAO_FACTURACAO " & _
                                      "WHERE " & _
                                      "     NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador) & _
                                      " ORDER BY CODEFR_REQ, NREQ_REQ,N_ORD_INS_ANA "
                                      
            'F�rmulas do Report
            ReportDetalhado.formulas(1) = "DataInicioListagem=" & BL_TrataDataParaBD(BL_HandleNull(dataInicialReport, EcDataInicial))
            ReportDetalhado.formulas(2) = "DataFimListagem=" & BL_TrataDataParaBD(BL_HandleNull(dataFinalReport, EcDataFinal))
                    
            'Pergunta se pretende imprimir o relat�rio detalhado ou resumido
            If BG_Mensagem(mediMsgBox, "Ser� impressa a listagem detalhada!" & vbNewLine & "Deseja imprimir apenas a resumida?", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
                ReportDetalhado.formulas(3) = "SuprimeDetalhe=" & BL_TrataStringParaBD("False")
            'Neste caso ir� imprimir o report resumido
            Else
                ReportDetalhado.formulas(3) = "SuprimeDetalhe=" & BL_TrataStringParaBD("True")
            End If
            
            Me.SetFocus
            
            Call BL_ExecutaReport
            '        'Report.PageShow Report.ReportLatestPage
            '        Report.PageZoom (100)

    ssql = "DELETE FROM SL_CR_CONFIRMACAO_FACTURACAO WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO ssql
    BG_Trata_BDErro
    
End Sub

' --------------------------------------------------------------------------------------------

' VERIFICA SE P1 tem mais que 6 analises

' --------------------------------------------------------------------------------------------
Private Function VerificaTotalAnaCred() As Boolean
    Dim i As Integer
    Dim credAct As String
    Dim totalAna As Integer
    Dim ssql As String
    Dim RsEFR As New adodb.recordset
    
    ssql = "SELECT * FROM SL_EFR WHERE COD_EFR = " & fa_movi_resp(FgReq.row).cod_efr
    RsEFR.CursorLocation = adUseClient
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    RsEFR.Open ssql, gConexao
    If RsEFR.RecordCount = 1 Then
        If BL_HandleNull(RsEFR!flg_nova_ars, 0) = 0 Then
            VerificaTotalAnaCred = True
            RsEFR.Close
            Exit Function
        End If
    End If
    RsEFR.Close
    VerificaTotalAnaCred = False
    totalAna = 0
    credAct = ""
    For i = 1 To fa_movi_resp(FgReq.row).totalAna
        If fa_movi_resp(FgReq.row).analises(i).flg_estado <> 8 And fa_movi_resp(FgReq.row).analises(i).flg_estado <> 2 Then
            If credAct <> fa_movi_resp(FgReq.row).analises(i).nr_req_ars Then
                credAct = fa_movi_resp(FgReq.row).analises(i).nr_req_ars
                totalAna = 0
            End If
            totalAna = totalAna + 1
            If totalAna > 6 Then
                VerificaTotalAnaCred = False
                Exit Function
            End If
        End If
    Next
    VerificaTotalAnaCred = True
Exit Function
TrataErro:
     VerificaTotalAnaCred = False
    BG_LogFile_Erros "Erro ao VerificaTotalAnaCred : " & " " & Err.Number & " - " & Err.Description, Me.Name, "VerificaTotalAnaCred", True
    Exit Function
    Resume Next
End Function



Private Function NomeUte(iResp As Integer) As String

    Dim ssql As String
    Dim rsRecibos As New adodb.recordset
    Dim total
    On Error GoTo TrataErro
    
    ssql = " SELECT * FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(fa_movi_resp(iResp).t_doente)
    ssql = ssql & " AND utente = " & BL_TrataStringParaBD(fa_movi_resp(iResp).doente)
    
    rsRecibos.CursorLocation = adUseClient
    rsRecibos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsRecibos.Open ssql, gConexao
    If rsRecibos.RecordCount = 1 Then
        NomeUte = BL_HandleNull(rsRecibos!nome_ute, "")
    Else
        NomeUte = ""
    End If
    rsRecibos.Close
    Set rsRecibos = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "NomeUte"
    Exit Function
    Resume Next
End Function

'NELSONPSILVA UALIA-857 01.02.2019
Private Function VerificaDuplicacaoRubricas(ByRef StrAna As String) As Boolean

    Dim ssql As String
    Dim rs As New adodb.recordset
    On Error GoTo TrataErro
    
    ssql = "SELECT COD_RUBR FROM fa_movi_fact WHERE episodio = " & BL_TrataStringParaBD(fa_movi_resp(FgReq.row).episodio) & " AND flg_estado = 1"
    ssql = ssql & " GROUP BY COD_RUBR"
    ssql = ssql & " HAVING COUNT(*) > 1"
    'If codRubr <> "" Then
    '    sSql = sSql & " AND cod_rubr_efr in (" & BL_TrataStringParaBD(codRubr) & ")"
    'End If
    
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    
    rs.Open ssql, gConexao
    
    If rs.RecordCount > 0 Then
        VerificaDuplicacaoRubricas = True
        While Not rs.EOF
           StrAna = StrAna & BL_HandleNull(rs.Fields("cod_rubr").value, "") & vbNewLine
           rs.MoveNext
        Wend
    End If
    
    Set rs = Nothing
    
    Exit Function
    
TrataErro:
    BG_LogFile_Erros "Erro: " & Err.Description, Me.Name, "VerificaDuplicacaoRubricas", False
    Exit Function
    
End Function



'UALIA-871
Private Function DefineCorLinha(ByVal iResp As Integer, ByVal iFact As Integer) As Long
    Select Case fa_movi_resp(iResp).analises(iFact).flg_estado
        Case EstadoFaturacao.Faturado
            DefineCorLinha = Verde
        Case EstadoFaturacao.NaoFaturavel
            DefineCorLinha = vermelhoClaro
        Case EstadoFaturacao.Rejeitado
            DefineCorLinha = vermelhoClaro
    End Select
End Function



'LJMANSO-312
'Private Sub CarregaConvencao(nReq As String)
'    Dim sSql As String
'    Dim Rs As New ADODB.recordset
'    On Error GoTo TrataErro
'
'    sSql = "SELECT DECODE(a.flg_tip_req,2," & lCodInternacional & ","
'    sSql = sSql & "0," & lCodNormal & ",1," & lCodProfissional & ",97," & lCod97 & ",3," & lCodNormalEspecial & ",5," & lCodInternacionalEspecial & ","
'    sSql = sSql & "4," & lCodProfissionalEspecial & ",0) convencao FROM fa_movi_fact a WHERE episodio = " & BL_TrataStringParaBD(nReq) & " AND ROWNUM = 1"
'
'    Rs.CursorType = adOpenStatic
'    Rs.CursorLocation = adUseClient
'
'    Rs.Open sSql, gConexao
'
'    If Rs.RecordCount > 0 Then
'        'CbConvencao.ListIndex = BL_HandleNull(Rs.Fields("convencao").value, mediComboValorNull)
'        'CbConvencao.ItemData = (BL_HandleNull(Rs.Fields("convencao").value, mediComboValorNull))
'        Dim i As Integer
'        i = 0
'        For i = 0 To CbConvencao.ListCount - 1
'            If CbConvencao.ItemData(i) = BL_HandleNull(Rs.Fields("convencao").value, mediComboValorNull) Then
'                CbConvencao.ListIndex = i
'                Exit For
'            End If
'        Next
'    End If
'
'    Set Rs = Nothing
'
'    Exit Sub
'TrataErro:
'    BG_LogFile_Erros "Erro: " & Err.Description, Me.Name, "CarregaConvencao", False
'    Exit Sub
'End Sub
'
''LJMANSO-312
'Private Sub AlteraConvencaoReq()
'    For i = 1 To UBound(fa_movi_resp)
'        For j = 1 To UBound(fa_movi_resp(i).analises)
'            fa_movi_resp(i).analises(j).convencao = DevolveConvencao
'            fa_movi_resp(i).analises(j).flg_tip_req = DevolveFlgTipReq
'       Next j
'    Next i
'End Sub
'
''LJMANSO-312
'Private Function DevolveConvencao(ByVal convencao As Integer) As Integer
'
'    Select Case convencao
'        Case lCodInternacional
'             DevolveConvencao = lCodInternacional
'        Case lCodNormal
'             DevolveConvencao = lCodNormal
'        Case lCodProfissional
'             DevolveConvencao = lCodProfissional
'         Case lCod97
'            DevolveConvencao = lCod97
'        Case lCodNormalEspecial
'             DevolveConvencao = lCodNormalEspecial
'        Case lCodInternacionalEspecial
'             DevolveConvencao = lCodInternacionalEspecial
'         Case lCodProfissionalEspecial
'             DevolveConvencao = lCodProfissionalEspecial
'    End Select
'
'End Function
'
''LJMANSO-312
'Private Function DevolveFlgTipReq(ByVal convencao As Integer) As String
'
'    Select Case convencao
'        Case lCodInternacional
'             DevolveFlgTipReq = "2"
'        Case lCodNormal
'             DevolveFlgTipReq = "0"
'        Case lCodProfissional
'             DevolveFlgTipReq = "1"
'         Case lCod97
'            DevolveFlgTipReq = "97"
'        Case lCodNormalEspecial
'             DevolveFlgTipReq = "3"
'        Case lCodInternacionalEspecial
'             DevolveFlgTipReq = "5"
'         Case lCodProfissionalEspecial
'             DevolveFlgTipReq = "5"
'    End Select
'
'End Function

'UALIA-919
Private Sub LimpaPesqRequis()
    PesqRequis.credencial = ""
    PesqRequis.NumReq = ""
    PesqRequis.TipoUte = ""
    PesqRequis.codEfr = ""
    PesqRequis.NumBenef = ""
    PesqRequis.dataInicial = ""
    PesqRequis.dataFinal = ""
    PesqRequis.codSala = ""
    PesqRequis.FGReqPrivado = False
End Sub

'edgar.parada Glintt-HS-21241 20.05.2019
Public Function verificaErroEfectivacao(ByVal credencial As String) As Boolean
    Dim ssql As String
    Dim rs As New adodb.recordset
    
    On Error GoTo TrataErro
    
    'ssql = "SELECT COUNT(distinct b.operation_id) erro_efect "
    'ssql = ssql & " FROM esp_requisition_det a, esp_operation b, esp_requisition c"
    'ssql = ssql & " WHERE b.req_number = " & BL_TrataStringParaBD(credencial)
    'ssql = ssql & " AND (b.operation_code = 'EFECTIVACAO' AND b.operation_state = 'ERRO')"
    'ssql = ssql & " AND (a.mcdt_state_code <> ' ARE' OR a.mcdt_state_code <> 'RES') "
    'ssql = ssql & " AND a.token_cancelamento IS NULL AND a.req_id = c.req_id AND b.req_number in (select req_number from esp_requisition)"
    
    ssql = "SELECT COUNT(distinct b.operation_id) erro_efect "
    ssql = ssql & " FROM esp_requisition_det a, esp_operation b, esp_requisition c"
    ssql = ssql & " WHERE b.req_number = " & BL_TrataStringParaBD(credencial)
    ssql = ssql & " AND (b.operation_code = 'EFECTIVACAO' AND a.MCDT_STATE_CODE = 'ARE')"
    'sSql = sSql & " AND (a.mcdt_state_code <> ' ARE' OR a.mcdt_state_code <> 'RES') "
    ssql = ssql & " AND a.req_id = c.req_id AND b.req_number = c.req_number"
    
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    
    rs.Open ssql, gConexao
    
    'Se n�o encontrou registo no estado ARE -  EFETIVADO � porque h� erros
    'If rs.RecordCount > 0 Then
        If rs!erro_efect = 0 Then
           verificaErroEfectivacao = True
        Else
           verificaErroEfectivacao = False
        End If
    'End If
    Exit Function
TrataErro:
    verificaErroEfectivacao = False
    BG_LogFile_Erros "Erro: " & Err.Description, Me.Name, "verificaErroEfectivacao", False
    Exit Function
End Function
'


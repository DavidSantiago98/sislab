VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form FormMessages 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   10035
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   14325
   Icon            =   "FormMessages.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10035
   ScaleWidth      =   14325
   ShowInTaskbar   =   0   'False
   Begin RichTextLib.RichTextBox RichTextBoxMessage 
      Height          =   2655
      Left            =   120
      TabIndex        =   17
      Top             =   6120
      Width           =   12735
      _ExtentX        =   22463
      _ExtentY        =   4683
      _Version        =   393217
      BackColor       =   16777215
      BorderStyle     =   0
      Enabled         =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"FormMessages.frx":000C
   End
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   120
      TabIndex        =   8
      Top             =   0
      Width           =   12735
      Begin VB.CommandButton CommandButtonConfigure 
         Height          =   495
         Left            =   11040
         Picture         =   "FormMessages.frx":008E
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "Configurar lista de destinat�rios"
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton CommandButtonRefresh 
         Height          =   495
         Left            =   9480
         Picture         =   "FormMessages.frx":051E
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Refrescar ecr�"
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton CommandButtonDelete 
         Height          =   495
         Left            =   7920
         Picture         =   "FormMessages.frx":09B7
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Eliminar mensagem"
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton CommandButtonFoward 
         Height          =   495
         Left            =   6360
         Picture         =   "FormMessages.frx":0E45
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Reencaminhar mensagem"
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton CommandButtonReplyAll 
         Height          =   495
         Left            =   4800
         Picture         =   "FormMessages.frx":12CC
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "Responder a todos"
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton CommandButtonReply 
         Height          =   495
         Left            =   3240
         Picture         =   "FormMessages.frx":1784
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   "Responder"
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton CommandButtonOpen 
         Height          =   495
         Left            =   1680
         Picture         =   "FormMessages.frx":1C0D
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Abrir mensagem"
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton CommandButtonEdit 
         Height          =   495
         Left            =   120
         Picture         =   "FormMessages.frx":212C
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Escrever nova mensagem"
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1455
      Left            =   120
      TabIndex        =   3
      Top             =   4560
      Width           =   12735
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Cc:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   600
         Width           =   735
      End
      Begin VB.Label LabelCc 
         Height          =   255
         Left            =   1080
         TabIndex        =   22
         Top             =   600
         Width           =   11535
      End
      Begin VB.Label LabelSubject 
         Height          =   255
         Left            =   1080
         TabIndex        =   21
         Top             =   1080
         Width           =   11535
      End
      Begin VB.Label LabelDate 
         Height          =   255
         Left            =   1080
         TabIndex        =   20
         Top             =   840
         Width           =   11535
      End
      Begin VB.Label LabelTo 
         Height          =   255
         Left            =   1080
         TabIndex        =   19
         Top             =   360
         Width           =   11535
      End
      Begin VB.Label LabelFrom 
         Height          =   255
         Left            =   1080
         TabIndex        =   18
         Top             =   120
         Width           =   11535
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Assunto:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Data:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Para:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "De:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   2040
      ScaleHeight     =   315
      ScaleWidth      =   555
      TabIndex        =   2
      Top             =   9240
      Width           =   615
   End
   Begin VB.Timer Timer1 
      Interval        =   65500
      Left            =   2760
      Top             =   9240
   End
   Begin MSComctlLib.ListView ListViewMessages 
      Height          =   3495
      Left            =   3000
      TabIndex        =   1
      Top             =   1080
      Width           =   9855
      _ExtentX        =   17383
      _ExtentY        =   6165
      LabelWrap       =   0   'False
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.TreeView TreeViewNavigation 
      Height          =   3495
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   2775
      _ExtentX        =   4895
      _ExtentY        =   6165
      _Version        =   393217
      Indentation     =   706
      LineStyle       =   1
      Style           =   7
      ImageList       =   "ImageListIcons"
      Appearance      =   1
      Enabled         =   0   'False
   End
   Begin MSComctlLib.ImageList ImageListIcons 
      Left            =   3720
      Top             =   9120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":25BB
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":2A16
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":2E71
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":32CC
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":3727
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   4560
      Top             =   9120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":3B82
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":3F1C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":42B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":4650
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":49EA
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":4D84
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":511E
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormMessages.frx":5438
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormMessages"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'     .................................................................
'    .            Form of the application messages system              .
'   .                                                                   .
'   .                    Paulo Ferreira 2009.08.07                      .
'    .                        � 2009 GLINTT-HS                         .
'     .................................................................

Option Explicit

' Declare system LockWindowUpdate function to lock a list view.
Private Declare Function LockWindowUpdate Lib "user32" (ByVal hWndLock As Long) As Long

Private Enum ListViewColumn
    
    cColumnIcon
    cColumnAttachment
    cColumnDate
    cColumnSubject
    cColumnSent

End Enum

' CommandButtonConfigure_Click.
Private Sub CommandButtonConfigure_Click()
    
    On Error GoTo ErrorHandler
    BG_AbreForm FormConfigureMessageLists, "FormConfigureMessageLists"
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Paulo Ferreira 2009.08.04
Private Sub CommandButtonFoward_Click()
    
    On Error GoTo ErrorHandler
    If (ListViewMessages.ListItems.Count = Empty) Then: Exit Sub
    ForwardMessage CStr(Split(ListViewMessages.SelectedItem.Key, "-")(2))
    FormEditMessage.Show
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' CommandButtonOpen_Click.
Private Sub CommandButtonOpen_Click()
    
    On Error GoTo ErrorHandler
    ListViewMessages_DblClick
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' CommandButtonRefresh_Click.
Public Sub CommandButtonRefresh_Click()

    On Error GoTo ErrorHandler
    RefreshForm
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' CommandButtonEdit_Click.
Private Sub CommandButtonEdit_Click()

    On Error GoTo ErrorHandler
    FormEditMessage.Show
    FillNavigation
    Exit Sub
    
ErrorHandler:
    Exit Sub
End Sub

Private Sub CommandButtonReply_Click()

    On Error GoTo ErrorHandler
    If (ListViewMessages.ListItems.Count = Empty) Then: Exit Sub
    ReplyMessage CStr(Split(ListViewMessages.SelectedItem.Key, "-")(2))
    FormEditMessage.Show
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub CommandButtonReplyAll_Click()

    On Error GoTo ErrorHandler
    If (ListViewMessages.ListItems.Count = Empty) Then: Exit Sub
    ReplyMessage CStr(Split(ListViewMessages.SelectedItem.Key, "-")(2)), True
    FormEditMessage.Show
    Exit Sub
    
ErrorHandler:
    Exit Sub

End Sub

' ListViewMessages_ItemClick.
Private Sub ListViewMessages_ItemClick(ByVal item As MSComctlLib.ListItem)
    
    On Error GoTo ErrorHandler
    Call WriteMessagePreview(CStr(Split(item.Key, "-")(2)), TreeViewNavigation.SelectedItem.Key)
    Exit Sub
    
ErrorHandler:
    Exit Sub

End Sub

' ListViewMessages_ColumnClick.
Private Sub ListViewMessages_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    
    On Error GoTo ErrorHandler
    
    ' Commence sorting
    With ListViewMessages
    
        ' Display the hourglass cursor whilst sorting
        
        Dim lngCursor As Long
        lngCursor = .MousePointer
        .MousePointer = vbHourglass
        
        ' Prevent the ListView control from updating on screen -
        ' this is to hide the changes being made to the listitems
        ' and also to speed up the sort
        
        LockWindowUpdate .hwnd
        
        ' Check the data type of the column being sorted,
        ' and act accordingly
        
        Dim l As Long
        Dim strFormat As String
        Dim strData() As String
        
        Dim lngIndex As Long
        lngIndex = ColumnHeader.Index - 1
    
        Select Case UCase$(ColumnHeader.Key)
        Case "DATE"
        
            ' Sort by date.
            
            strFormat = "YYYYMMDDHhNnSs"
        
            ' Loop through the values in this column. Re-format
            ' the dates so as they can be sorted alphabetically,
            ' having already stored their visible values in the
            ' tag, along with the tag's original value
        
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsDate(.Text) Then
                                .Text = Format(CDate(.Text), _
                                                    strFormat)
                            Else
                                .Text = ""
                            End If
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsDate(.Text) Then
                                .Text = Format(CDate(.Text), _
                                                    strFormat)
                            Else
                                .Text = ""
                            End If
                        End With
                    Next l
                End If
            End With
            
            ' Sort the list alphabetically by this column
            
            .sortOrder = (.sortOrder + 1) Mod 2
            .SortKey = ColumnHeader.Index - 1
            .Sorted = True
            
            ' Restore the previous values to the 'cells' in this
            ' column of the list from the tags, and also restore
            ' the tags to their original values
            
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                End If
            End With
            
        Case "NUMBER"
        
            ' Sort Numerically
        
            strFormat = String(30, "0") & "." & String(30, "0")
        
            ' Loop through the values in this column. Re-format the values so as they
            ' can be sorted alphabetically, having already stored their visible
            ' values in the tag, along with the tag's original value
        
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsNumeric(.Text) Then
                                If CDbl(.Text) >= 0 Then
                                    .Text = Format(CDbl(.Text), _
                                        strFormat)
                                Else
                                    .Text = "&" & InvertNumber( _
                                        Format(0 - CDbl(.Text), _
                                        strFormat))
                                End If
                            Else
                                .Text = ""
                            End If
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsNumeric(.Text) Then
                                If CDbl(.Text) >= 0 Then
                                    .Text = Format(CDbl(.Text), _
                                        strFormat)
                                Else
                                    .Text = "&" & InvertNumber( _
                                        Format(0 - CDbl(.Text), _
                                        strFormat))
                                End If
                            Else
                                .Text = ""
                            End If
                        End With
                    Next l
                End If
            End With
            
            ' Sort the list alphabetically by this column
            
            .sortOrder = (.sortOrder + 1) Mod 2
            .SortKey = ColumnHeader.Index - 1
            .Sorted = True
            
            ' Restore the previous values to the 'cells' in this
            ' column of the list from the tags, and also restore
            ' the tags to their original values
            
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                End If
            End With
       
        Case "ICON"
        
            ' Sort Numerically
        
            strFormat = String(30, "0") & "." & String(30, "0")
        
            ' Loop through the values in this column. Re-format the values so as they
            ' can be sorted alphabetically, having already stored their visible
            ' values in the tag, along with the tag's original value
        
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            .Tag = .ReportIcon & Chr$(0) & .Tag
                            If IsNumeric(.ReportIcon) Then
                                If .ReportIcon >= 0 Then
                                    .ReportIcon = .ReportIcon
                                Else
                                    .ReportIcon = "&" & InvertNumber(.ReportIcon)
                                End If
                            Else
                                .ReportIcon = ""
                            End If
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            .Tag = .SmallIcon & Chr$(0) & .Tag
                            If IsNumeric(.SmallIcon) Then
                                If .SmallIcon >= 0 Then
                                    .SmallIcon = .SmallIcon
                                Else
                                        .SmallIcon = "&" & InvertNumber(.SmallIcon)
                                            
                                End If
                            Else
                                .SmallIcon = ""
                            End If
                        End With
                    Next l
                End If
            End With
            
            ' Sort the list alphabetically by this column
            
            .sortOrder = (.sortOrder + 1) Mod 2
            .SortKey = ColumnHeader.Index - 1
            .Sorted = True
            
            ' Restore the previous values to the 'cells' in this
            ' column of the list from the tags, and also restore
            ' the tags to their original values
            
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                End If
            End With
             
        Case Else   ' Assume sort by string
            
            ' Sort alphabetically. This is the only sort provided
            ' by the MS ListView control (at this time), and as
            ' such we don't really need to do much here
        
            .sortOrder = (.sortOrder + 1) Mod 2
            .SortKey = ColumnHeader.Index - 1
            .Sorted = True
            
        End Select

        ' Unlock the list window so that the OCX can update it
        LockWindowUpdate 0&
        ' Restore the previous cursor
        .MousePointer = lngCursor
        
            
    End With
    Exit Sub
    ' Report time elapsed, in milliseconds
Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' ListViewMessages_DblClick.
Private Sub ListViewMessages_DblClick()
    
    On Error GoTo ErrorHandler
    If (ListViewMessages.ListItems.Count > 0) Then
        Load FormEditMessage
        FormEditMessage.FillMessage (Split(ListViewMessages.SelectedItem.Key, "-")(2))
        FormEditMessage.Show
        ListViewMessages.SelectedItem.Checked = True
        ChangeMessageState ModuleMessages.MessageStateRead
        ListViewMessages.SelectedItem.Checked = False
    End If
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' ListViewMessages_KeyDown.
Private Sub ListViewMessages_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo ErrorHandler
    If (KeyCode = vbKeyReturn) Then: ListViewMessages_DblClick
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' ListViewMessages_MouseDown
Private Sub ListViewMessages_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
     
    On Error GoTo ErrorHandler
    If (Button = MouseButtonConstants.vbRightButton) Then
        If (SetMenu) Then: Me.PopupMenu MDIFormInicio.IDM_MENU_FERRAMENTAS(6)
    End If
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' TreeViewNavigation_NodeClick.
Public Sub TreeViewNavigation_NodeClick(ByVal Node As MSComctlLib.Node)

    On Error GoTo ErrorHandler
    CleanMessagePreview
    MDIFormInicio.IDM_CORREIO_MARCAR(6).caption = "Seleccionar Todos"
    Select Case (UCase(Node.Key))
        Case ModuleMessages.MessageNavigationRoot: ListViewMessages.ListItems.Clear
        Case ModuleMessages.MessageNavigationSent: FillNode ModuleMessages.MessageNavigationSent
        Case ModuleMessages.MessageNavigationReceived: FillNode ModuleMessages.MessageNavigationReceived
        Case ModuleMessages.MessageNavigationArchived: FillNode ModuleMessages.MessageNavigationArchived
        Case ModuleMessages.MessageNavigationDeleted: FillNode ModuleMessages.MessageNavigationDeleted
    End Select
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Form_Load.
Sub Form_Load()
    
    On Error GoTo ErrorHandler
    LoadEvent
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Form_Activate.
Sub Form_Activate()

    On Error GoTo ErrorHandler
    ActivateEvent
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Form_Unload
Sub Form_Unload(Cancel As Integer)
    
    On Error GoTo ErrorHandler
    UnloadEvent
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Initialize navigation tree view.
Private Sub InitializeMessages()
    
    On Error GoTo ErrorHandler
    With ListViewMessages
        .SmallIcons = ImageList2
        .ColumnHeaderIcons = ImageList2
        .ColumnHeaders.Add , "ICON", "", 600, ListColumnAlignmentConstants.lvwColumnLeft, 6
        .ColumnHeaders.Add , "ATTACHMENT", Empty, 300, ListColumnAlignmentConstants.lvwColumnLeft
        .ColumnHeaders.Add , "DATE", "Data", 1700, ListColumnAlignmentConstants.lvwColumnLeft
        .ColumnHeaders.Add , "SUBJECT", "Assunto", 4500, ListColumnAlignmentConstants.lvwColumnLeft
        .ColumnHeaders.Add , "SENT", "Emissor", 2200, ListColumnAlignmentConstants.lvwColumnLeft
        .View = lvwReport
        .BorderStyle = ccNone
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .FullRowSelect = True
        .Checkboxes = True
        .ForeColor = &H808080
        .GridLines = False
    End With
    SetListViewColor ListViewMessages, Picture1, vbWhite, &H8000000F
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Initialize navigation tree view.
Private Sub InitializeNavigation()
    
    On Error GoTo ErrorHandler
    With TreeViewNavigation
        .Nodes.Clear
        .Nodes.Add , , ModuleMessages.MessageNavigationRoot, CR_BL_DevolveNomeUtilizadorActual, 5, 5
        .Nodes.Add ModuleMessages.MessageNavigationRoot, tvwChild, ModuleMessages.MessageNavigationReceived, "Recebidas", 2, 2
        .Nodes.Add ModuleMessages.MessageNavigationRoot, tvwChild, ModuleMessages.MessageNavigationSent, "Enviadas", 4, 4
        .Nodes.Add ModuleMessages.MessageNavigationRoot, tvwChild, ModuleMessages.MessageNavigationArchived, "Arquivo", 1, 1
        .Nodes.Add ModuleMessages.MessageNavigationRoot, tvwChild, ModuleMessages.MessageNavigationDeleted, "Eliminadas", 3, 3
        .Nodes.item(ModuleMessages.MessageNavigationRoot).Expanded = True
        .ImageList = ImageListIcons
        .LabelEdit = tvwManual
        .Enabled = True
    End With
    Exit Sub
    
ErrorHandler:
    Exit Sub

End Sub

' Change message state.
Public Sub ChangeMessageState(�iState� As Integer)
    
    Dim sSql As String
    Dim sMessageId As String
    Dim vItem As Variant
    Dim bChanged As Boolean
    
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me
    If (ExistsUnreadMessages(�iState�)) Then: BG_Mensagem cr_mediMsgBox, "N�o pode mover mensagens n�o lidas!", vbExclamation, " Aten��o": BL_FimProcessamento Me: Exit Sub
    For Each vItem In GetCheckedItems(ListViewMessages)
        sMessageId = Split(vItem.Key, "-")(2): bChanged = True
        Select Case (�iState�)
          Case ModuleMessages.MessageStateUnread
                sSql = "update " & gBD_PREFIXO_TAB & "mensagens_destinatarios set cod_estado = " & �iState� & ", flg_lida = 0 where id_mensagem = " & sMessageId & " and cod_util = " & gCodUtilizador
                CR_BG_ExecutaQuery_ADO sSql
            Case ModuleMessages.MessageStateRead
                sSql = "update " & gBD_PREFIXO_TAB & "mensagens_destinatarios set cod_estado = " & �iState� & ", flg_lida = 1 where id_mensagem = " & sMessageId & " and cod_util = " & gCodUtilizador
                CR_BG_ExecutaQuery_ADO sSql
            Case ModuleMessages.MessageStateArchive:
                sSql = "update " & gBD_PREFIXO_TAB & "mensagens_destinatarios set cod_estado = " & �iState� & " where id_mensagem = " & sMessageId & " and cod_util = " & gCodUtilizador
                CR_BG_ExecutaQuery_ADO sSql
            Case ModuleMessages.MessageStateDestroy:
                sSql = "update " & gBD_PREFIXO_TAB & "mensagens set flg_eliminada = 1 WHERE id = " & sMessageId
                CR_BG_ExecutaQuery_ADO sSql
            Case ModuleMessages.MessageStateDelete:
                Select Case (UCase(TreeViewNavigation.SelectedItem.Key))
                    Case MessageNavigationReceived
                        sSql = "update " & gBD_PREFIXO_TAB & "mensagens_destinatarios set cod_estado = " & �iState� & ", flg_lida = 1 where id_mensagem = " & sMessageId & " and cod_util = " & gCodUtilizador
                    Case MessageNavigationSent
                        sSql = "update " & gBD_PREFIXO_TAB & "mensagens set flg_eliminada = 1 where id = " & sMessageId & " and user_cri = " & gCodUtilizador
                End Select
                CR_BG_ExecutaQuery_ADO sSql
        End Select
    Next
    If (bChanged) Then
        RefreshForm
    End If
    BL_FimProcessamento Me
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Set options menu.
Private Function SetMenu() As Boolean
    
    On Error GoTo ErrorHandler
    If (ListViewMessages.ListItems.Count = 0) Then: Exit Function
    Select Case (TreeViewNavigation.SelectedItem.Key)
        Case "RECEIVED"
            MDIFormInicio.IDM_CORREIO_MARCAR(0).Enabled = True
            MDIFormInicio.IDM_CORREIO_MARCAR(1).Enabled = True
            MDIFormInicio.IDM_CORREIO_MARCAR(2).Enabled = True
            MDIFormInicio.IDM_CORREIO_MARCAR(3).Enabled = True
            MDIFormInicio.IDM_CORREIO_MARCAR(4).Enabled = True
            SetMenu = True
        Case "SENT"
            MDIFormInicio.IDM_CORREIO_MARCAR(0).Enabled = False
            MDIFormInicio.IDM_CORREIO_MARCAR(1).Enabled = False
            MDIFormInicio.IDM_CORREIO_MARCAR(2).Enabled = False
            MDIFormInicio.IDM_CORREIO_MARCAR(3).Enabled = False
            MDIFormInicio.IDM_CORREIO_MARCAR(4).Enabled = True
            SetMenu = True
        Case "ARCHIVE"
            MDIFormInicio.IDM_CORREIO_MARCAR(0).Enabled = True
            MDIFormInicio.IDM_CORREIO_MARCAR(1).Enabled = True
            MDIFormInicio.IDM_CORREIO_MARCAR(2).Enabled = False
            MDIFormInicio.IDM_CORREIO_MARCAR(3).Enabled = True
            MDIFormInicio.IDM_CORREIO_MARCAR(4).Enabled = True
            SetMenu = True
        Case "DELETED"
            MDIFormInicio.IDM_CORREIO_MARCAR(0).Enabled = False
            MDIFormInicio.IDM_CORREIO_MARCAR(1).Enabled = True
            MDIFormInicio.IDM_CORREIO_MARCAR(2).Enabled = True
            MDIFormInicio.IDM_CORREIO_MARCAR(3).Enabled = True
            MDIFormInicio.IDM_CORREIO_MARCAR(4).Enabled = False
            SetMenu = True
    End Select
    Exit Function
    
ErrorHandler:
    Exit Function
  
End Function

' Fill navigation tree view.
Private Function FillNavigation()

    Dim sSql As String
    
    On Error GoTo ErrorHandler
    ListViewMessages.ListItems.Clear
    TreeViewNavigation.Nodes.item(ModuleMessages.MessageNavigationReceived).Text = "Recebidas (" & GetMessagesNumberByNavigation(ModuleMessages.MessageNavigationReceived) & ")"
    TreeViewNavigation.Nodes.item(ModuleMessages.MessageNavigationSent).Text = "Enviadas (" & GetMessagesNumberByNavigation(ModuleMessages.MessageNavigationSent) & ")"
    TreeViewNavigation.Nodes.item(ModuleMessages.MessageNavigationArchived).Text = "Arquivadas (" & GetMessagesNumberByNavigation(ModuleMessages.MessageNavigationArchived) & ")"
    TreeViewNavigation.Nodes.item(ModuleMessages.MessageNavigationDeleted).Text = "Eliminadas (" & GetMessagesNumberByNavigation(ModuleMessages.MessageNavigationDeleted) & ")"
    TreeViewNavigation.Nodes.item(ModuleMessages.MessageNavigationRoot).Selected = True
    Exit Function

ErrorHandler:
    Exit Function
    
End Function

' Fill a node by message node state.
Private Sub FillNode(�sNavigation� As String)
    
    Dim rNode As ADODB.recordset
    
    On Error GoTo ErrorHandler
    ListViewMessages.ListItems.Clear
    ListViewMessages.Refresh
    Set rNode = New ADODB.recordset
    rNode.CursorLocation = adUseServer
    rNode.Open GetNodeQueryString(�sNavigation�), gConexao, adOpenStatic
    If (rNode.RecordCount > 0) Then
        While (Not rNode.EOF)
            With ListViewMessages.ListItems.Add(, "M-" & rNode!tipo & "-" & rNode!id, "", , GetIconByNavigationKey(�sNavigation�, BL_HandleNull(rNode!flg_lida, Empty)))
                .ListSubItems.Add , , BL_HandleNull(rNode!tipo, Empty)
                .ListSubItems.Add , , BL_HandleNull(rNode!dt_cri, Empty)
                .ListSubItems.Add , , BL_HandleNull(rNode!assunto, Empty)
                .ListSubItems.Add , , CR_BL_DevolveNomeUtilizadorActual(BL_HandleNull(rNode!user_cri, Empty))
            End With
            Call VerifyAttachments(BL_HandleNull(rNode!id, Empty))
            rNode.MoveNext
        Wend
        ListViewMessages.Refresh
    End If
    If (rNode.state = adStateOpen) Then: rNode.Close
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Form initializations.
Private Sub Initializations()
        
    On Error GoTo ErrorHandler
    Me.caption = " Sistema de Mensagens"
    Me.left = 100
    Me.top = 100
    Me.Width = 13065
    Me.Height = 9285
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Form load event.
Private Sub LoadEvent()

    On Error GoTo ErrorHandler
    Initializations
    CR_BG_ParametrizaPermissoes_ADO Me.Name
    SetDefaultValues
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Form activate event.
Private Sub ActivateEvent()
    
    On Error GoTo ErrorHandler
    Set gFormActivo = Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Form unload event.
Private Sub UnloadEvent()
    
    On Error GoTo ErrorHandler
    Set gFormActivo = MDIFormInicio
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Set default values.
Private Sub SetDefaultValues()

    On Error GoTo ErrorHandler
    InitializeNavigation
    InitializeMessages
    FillNavigation
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Paint a list view with colored bars.
Private Sub SetListViewColor(�lvCtrlListView� As ListView, �pCtrlPictureBox� As PictureBox, �lColorOne� As Long, Optional �lColorTwo� As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo ErrorHandler
    lColor1 = �lColorOne�
    lColor2 = �lColorTwo�
    �lvCtrlListView�.Picture = LoadPicture("")
    �lvCtrlListView�.Refresh
    �pCtrlPictureBox�.Cls
    �pCtrlPictureBox�.AutoRedraw = True
    �pCtrlPictureBox�.BorderStyle = vbBSNone
    �pCtrlPictureBox�.ScaleMode = vbTwips
    �pCtrlPictureBox�.Visible = False
    �lvCtrlListView�.PictureAlignment = lvwTile
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    �pCtrlPictureBox�.top = �lvCtrlListView�.top
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    With �pCtrlPictureBox�.Font
        .Size = �lvCtrlListView�.Font.Size + 2
        .Bold = �lvCtrlListView�.Font.Bold
        .Charset = �lvCtrlListView�.Font.Charset
        .Italic = �lvCtrlListView�.Font.Italic
        .Name = �lvCtrlListView�.Font.Name
        .Strikethrough = �lvCtrlListView�.Font.Strikethrough
        .Underline = �lvCtrlListView�.Font.Underline
        .Weight = �lvCtrlListView�.Font.Weight
    End With
    �pCtrlPictureBox�.Refresh
    lLineHeight = �pCtrlPictureBox�.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = �lvCtrlListView�.Width
    �pCtrlPictureBox�.Height = lBarHeight * 2
    �pCtrlPictureBox�.Width = lBarWidth
    �pCtrlPictureBox�.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    �pCtrlPictureBox�.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    �pCtrlPictureBox�.AutoSize = True
    �lvCtrlListView�.Picture = �pCtrlPictureBox�.Image
    �lvCtrlListView�.Refresh
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Invert number.
Private Function InvertNumber(ByVal Number As String) As String
    
    Static i As Integer
    
    On Error GoTo ErrorHandler
    For i = 1 To Len(Number)
        Select Case Mid$(Number, i, 1)
        Case "-": Mid$(Number, i, 1) = " "
        Case "0": Mid$(Number, i, 1) = "9"
        Case "1": Mid$(Number, i, 1) = "8"
        Case "2": Mid$(Number, i, 1) = "7"
        Case "3": Mid$(Number, i, 1) = "6"
        Case "4": Mid$(Number, i, 1) = "5"
        Case "5": Mid$(Number, i, 1) = "4"
        Case "6": Mid$(Number, i, 1) = "3"
        Case "7": Mid$(Number, i, 1) = "2"
        Case "8": Mid$(Number, i, 1) = "1"
        Case "9": Mid$(Number, i, 1) = "0"
        End Select
    Next
    InvertNumber = Number
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' Select all items.
Public Sub SelectAll()

    Dim bSelect As Boolean
    Dim oItem As Variant
    
    On Error GoTo ErrorHandler
    bSelect = True
    If (ListViewMessages.ListItems.Count > 0) Then
        For Each oItem In ListViewMessages.ListItems
           bSelect = bSelect And ListViewMessages.ListItems.item(1).Checked
        Next
    End If
    If (ListViewMessages.ListItems.Count > 0) Then
        For Each oItem In ListViewMessages.ListItems
            oItem.Checked = Not bSelect
        Next
    End If
    If (Not bSelect) Then: MDIFormInicio.IDM_CORREIO_MARCAR(6).caption = "Desseleccionar Todos"
    If (bSelect) Then: MDIFormInicio.IDM_CORREIO_MARCAR(6).caption = "Seleccionar Todos"
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Timer1_Timer.
Private Sub timer1_Timer()
    CommandButtonRefresh_Click
End Sub

' Check unread messages.
Private Function ExistsUnreadMessages(�iState� As Integer) As Boolean

    Dim vItem As Variant
    
    On Error GoTo ErrorHandler
    If (ListViewMessages.ListItems.Count = 0) Then: Exit Function
    Select Case (�iState�)
        Case ModuleMessages.MessageStateRead, ModuleMessages.MessageStateUnread: Exit Function
    End Select
    For Each vItem In ListViewMessages.ListItems
        
        If (vItem.Checked And vItem.SmallIcon = ModuleMessages.MessageStateUnread) Then: ExistsUnreadMessages = True: Exit Function
    Next
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' Get all checked items of a listview.
Private Function GetCheckedItems(�lvListView As ListView) As Collection

    Dim vItem As ListItem

    On Error GoTo ErrorHandler
    Set GetCheckedItems = New Collection
    For Each vItem In �lvListView.ListItems
        If (vItem.Checked) Then: GetCheckedItems.Add vItem
    Next
    Exit Function

ErrorHandler:
    Exit Function

End Function

' Get messages number by message node navigation.
Private Function GetMessagesNumberByNavigation(�sNavigation� As String) As Long
    
    Dim sSql As String
    Dim rMessages As ADODB.recordset
    
    On Error GoTo ErrorHandler
    Select Case (�sNavigation�)
        Case ModuleMessages.MessageNavigationSent:
            sSql = "select distinct id from " & gBD_PREFIXO_TAB & "mensagens where user_cri = " & gCodUtilizador & " and (flg_eliminada is null or flg_eliminada <> 1)"
        Case ModuleMessages.MessageNavigationArchived:
            sSql = "select distinct id from " & gBD_PREFIXO_TAB & "mensagens, " & gBD_PREFIXO_TAB & "mensagens_destinatarios where id = id_mensagem and (user_cri = " & gCodUtilizador & " or cod_util = " & gCodUtilizador & ") and cod_estado = " & ModuleMessages.MessageStateArchive
        Case ModuleMessages.MessageNavigationDeleted:
            sSql = "select distinct id, dt_cri, assunto, mensagem, flg_lida, user_cri,'R' tipo from " & gBD_PREFIXO_TAB & "mensagens, " & gBD_PREFIXO_TAB & "mensagens_destinatarios where id = id_mensagem and (cod_util = " & gCodUtilizador & ") and cod_estado = " & ModuleMessages.MessageStateDelete
            sSql = sSql & " UNION select distinct id, dt_cri, assunto, mensagem, flg_lida, user_cri, 'E' tipo from " & gBD_PREFIXO_TAB & "mensagens, " & gBD_PREFIXO_TAB & "mensagens_destinatarios where id = id_mensagem and (user_cri  = " & gCodUtilizador & ") and flg_eliminada = 1 "
        Case ModuleMessages.MessageNavigationReceived:
            sSql = "select distinct id_mensagem from " & gBD_PREFIXO_TAB & "mensagens_destinatarios where cod_util = " & gCodUtilizador & " and cod_estado in (" & ModuleMessages.MessageStateRead & "," & ModuleMessages.MessageStateUnread & ")"
    End Select
    If (sSql = Empty) Then: Exit Function
    Set rMessages = New ADODB.recordset
    rMessages.CursorLocation = adUseServer
    rMessages.Open sSql, gConexao, adOpenStatic
    GetMessagesNumberByNavigation = rMessages.RecordCount
    If (rMessages.state = adStateOpen) Then: rMessages.Close
    Exit Function

ErrorHandler:
    Exit Function

End Function

' Get node query string by message node navigation and message id.
Private Function GetNodeQueryString(�sNavigation� As String, Optional �sMessageId� As String) As String
    
    Dim sSql As String
    
    On Error GoTo ErrorHandler
    Select Case (�sNavigation�)
        Case ModuleMessages.MessageNavigationSent:
            GetNodeQueryString = "select distinct id, dt_cri, assunto, mensagem, user_cri, null flg_lida,'E' tipo from " & gBD_PREFIXO_TAB & "mensagens where user_cri = " & gCodUtilizador & " and (flg_eliminada is null or flg_eliminada <> 1)"
            If (�sMessageId� <> Empty) Then: GetNodeQueryString = GetNodeQueryString & " and id = " & �sMessageId�
        Case ModuleMessages.MessageNavigationArchived:
            GetNodeQueryString = "select distinct id, dt_cri, assunto, mensagem, flg_lida, user_cri, 'R' tipo from " & gBD_PREFIXO_TAB & "mensagens, " & gBD_PREFIXO_TAB & "mensagens_destinatarios where id = id_mensagem and (cod_util = " & gCodUtilizador & ") and cod_estado = " & ModuleMessages.MessageStateArchive
            If (�sMessageId� <> Empty) Then: GetNodeQueryString = GetNodeQueryString & " and id = " & �sMessageId�
        Case ModuleMessages.MessageNavigationDeleted:
            GetNodeQueryString = "select distinct id, dt_cri, assunto, mensagem, flg_lida, user_cri,'R' tipo from " & gBD_PREFIXO_TAB & "mensagens, " & gBD_PREFIXO_TAB & "mensagens_destinatarios where id = id_mensagem and (cod_util = " & gCodUtilizador & ") and cod_estado = " & ModuleMessages.MessageStateDelete
            GetNodeQueryString = GetNodeQueryString & " UNION select distinct id, dt_cri, assunto, mensagem, flg_lida, user_cri, 'E' tipo from " & gBD_PREFIXO_TAB & "mensagens, " & gBD_PREFIXO_TAB & "mensagens_destinatarios where id = id_mensagem and (user_cri  = " & gCodUtilizador & ") and flg_eliminada = 1 "
            If (�sMessageId� <> Empty) Then: GetNodeQueryString = GetNodeQueryString & " and id = " & �sMessageId�
        Case ModuleMessages.MessageNavigationReceived:
            GetNodeQueryString = "select distinct id_mensagem id, dt_cri, assunto, mensagem, flg_lida, user_cri, 'R' tipo from " & gBD_PREFIXO_TAB & "mensagens, " & gBD_PREFIXO_TAB & "mensagens_destinatarios where id = id_mensagem and cod_util = " & gCodUtilizador & " and cod_estado in (" & ModuleMessages.MessageStateRead & "," & ModuleMessages.MessageStateUnread & ")"
            If (�sMessageId� <> Empty) Then: GetNodeQueryString = GetNodeQueryString & " and id = " & �sMessageId�
    End Select
    Exit Function

ErrorHandler:
    Exit Function

End Function

' Writes a message preview fields.
Private Sub WriteMessagePreview(�sMessageId� As String, �sNavigation� As String)
    
    Dim rMessages As ADODB.recordset
    
    On Error GoTo ErrorHandler
    Set rMessages = New ADODB.recordset
    rMessages.CursorLocation = adUseServer
    rMessages.Open GetNodeQueryString(�sNavigation�, �sMessageId�), gConexao, adOpenStatic
    If (rMessages.RecordCount > 0) Then
        RichTextBoxMessage.TextRTF = BL_HandleNull(rMessages!mensagem, Empty)
        LabelFrom.caption = CR_BL_DevolveNomeUtilizadorActual(BL_HandleNull(rMessages!user_cri, Empty))
        LabelTo.caption = ModuleMessages.GetMessageRecipients(�sMessageId�)
        LabelCc.caption = GetMessageRecipients(�sMessageId�, True)
        LabelSubject.caption = BL_HandleNull(rMessages!assunto, Empty)
        LabelDate.caption = BL_HandleNull(rMessages!dt_cri, Empty)
    End If
    If (rMessages.state = adStateOpen) Then: rMessages.Close
    Exit Sub

ErrorHandler:
    Exit Sub

End Sub

' Reply message.
Private Sub ReplyMessage(�sMessageId� As String, Optional �bReplyAll� As Boolean)
    
    Dim rMessages As ADODB.recordset
    Dim sSql As String
    
    On Error GoTo ErrorHandler
    sSql = "select user_cri, cod_util, assunto, flg_cc, mensagem, dt_cri from " & gBD_PREFIXO_TAB & "mensagens, " & gBD_PREFIXO_TAB & "mensagens_destinatarios where id = id_mensagem and id_mensagem = " & �sMessageId�
    Set rMessages = New ADODB.recordset
    rMessages.CursorLocation = adUseServer
    rMessages.Open sSql, gConexao, adOpenStatic
    If (rMessages.RecordCount = Empty) Then: Exit Sub
    If (�bReplyAll�) Then
        While (Not rMessages.EOF)
            If (BL_HandleNull(rMessages!flg_cc, ModuleMessages.RecipientTO) = ModuleMessages.RecipientTO) Then
                FormEditMessage.TextBoxTo.Text = FormEditMessage.TextBoxTo.Text & CR_BL_DevolveNomeUtilizadorActual(rMessages!cod_util) & "; "
                FormEditMessage.TextBoxTo.Tag = FormEditMessage.TextBoxTo.Tag & rMessages!cod_util & "; "
            ElseIf (BL_HandleNull(rMessages!flg_cc, ModuleMessages.RecipientTO) = ModuleMessages.RecipientCC) Then
                FormEditMessage.TextBoxCC.Text = FormEditMessage.TextBoxCC.Text & CR_BL_DevolveNomeUtilizadorActual(rMessages!cod_util) & "; "
                FormEditMessage.TextBoxCC.Tag = FormEditMessage.TextBoxCC.Tag & rMessages!cod_util & "; "
            End If
            rMessages.MoveNext
        Wend
    End If
    rMessages.MoveFirst
    FormEditMessage.TextBoxTo.Text = CR_BL_DevolveNomeUtilizadorActual(rMessages!user_cri) & "; "
    FormEditMessage.TextBoxTo.Tag = rMessages!user_cri & "; "
    FormEditMessage.TextBoxSubject.Text = "RE: " & BL_HandleNull(rMessages!assunto, Empty)
    FormEditMessage.RichTextBoxMessage.TextRTF = BuildRTFMessage(rMessages!dt_cri, rMessages!mensagem)
    FormEditMessage.RichTextBoxMessage.SetFocus
    If (rMessages.state = adStateOpen) Then: rMessages.Close
    Exit Sub

ErrorHandler:
    Exit Sub

End Sub

' Forward message.
Private Sub ForwardMessage(�lMessageId� As String)
    
    Dim rMessages As ADODB.recordset
    Dim sSql As String
    
    On Error GoTo ErrorHandler
    sSql = "select assunto from " & gBD_PREFIXO_TAB & "mensagens where id = " & �lMessageId�
    Set rMessages = New ADODB.recordset
    rMessages.CursorLocation = adUseServer
    rMessages.Open sSql, gConexao, adOpenStatic
    If (rMessages.RecordCount > Empty) Then: FormEditMessage.TextBoxSubject.Text = "FW: " & BL_HandleNull(rMessages!assunto, Empty)
    If (rMessages.state = adStateOpen) Then: rMessages.Close
    Exit Sub

ErrorHandler:
    Exit Sub

End Sub

' Delete message.
Private Sub DeleteMessage(�lMessageId� As String)

    On Error GoTo ErrorHandler
    ChangeMessageState ModuleMessages.MessageStateDelete
    Exit Sub

ErrorHandler:
    Exit Sub
    
End Sub

' Clean message preview fields.
Private Sub CleanMessagePreview()

    On Error GoTo ErrorHandler
    LabelCc.caption = Empty
    LabelTo.caption = Empty
    LabelDate.caption = Empty
    LabelFrom.caption = Empty
    LabelSubject.caption = Empty
    RichTextBoxMessage.TextRTF = Empty
    Exit Sub

ErrorHandler:
    Exit Sub
    
End Sub

' Get icon by navigation key.
Private Function GetIconByNavigationKey(�sNavigation� As String, �sMessageState� As String) As Integer

    On Error GoTo ErrorHandler
    Select Case (�sNavigation�)
        Case ModuleMessages.MessageNavigationArchived: GetIconByNavigationKey = ModuleMessages.MessageStateArchive
        Case ModuleMessages.MessageNavigationSent: GetIconByNavigationKey = ModuleMessages.MessageStateSent
        Case ModuleMessages.MessageNavigationReceived: GetIconByNavigationKey = IIf(�sMessageState� = 1, ModuleMessages.MessageStateRead, ModuleMessages.MessageStateUnread)
        Case ModuleMessages.MessageNavigationDeleted: GetIconByNavigationKey = ModuleMessages.MessageStateDelete
        Case Else: GetIconByNavigationKey = Null
    End Select
    Exit Function

ErrorHandler:
    Exit Function
    
End Function

' Build RTF message.
Private Function BuildRTFMessage(�sDate� As String, �sMessageText� As String) As String

    On Error GoTo ErrorHandler
    BuildRTFMessage = Empty
    Exit Function

ErrorHandler:
    Exit Function
    
End Function

Private Sub RefreshForm()

    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me
    FillNavigation
    TreeViewNavigation_NodeClick TreeViewNavigation.Nodes.item(TreeViewNavigation.SelectedItem.Key)
    TreeViewNavigation.Nodes.item(ModuleMessages.MessageNavigationReceived).Selected = True
    TreeViewNavigation_NodeClick TreeViewNavigation.Nodes.item(ModuleMessages.MessageNavigationReceived)
    BL_FimProcessamento Me
    Exit Sub

ErrorHandler:
    Exit Sub
    
End Sub

' Verify Attachments.
Private Function VerifyAttachments(�sMessageId� As String) As Boolean
    
    Dim rMessages As ADODB.recordset
    Dim vSubItem As ListSubItem
    Dim sSql As String
    
    On Error GoTo ErrorHandler
    Set vSubItem = ListViewMessages.ListItems(ListViewMessages.ListItems.Count).ListSubItems(ListViewColumn.cColumnAttachment)
    vSubItem.ToolTipText = Empty
    sSql = "select anexo from " & gBD_PREFIXO_TAB & "mensagens_anexos where id_mensagem = " & �sMessageId�
    Set rMessages = New ADODB.recordset
    rMessages.CursorLocation = adUseServer
    rMessages.Open sSql, gConexao, adOpenStatic
    If (rMessages.RecordCount = Empty) Then: Exit Function
    vSubItem.ReportIcon = 8
    While (Not rMessages.EOF)
        vSubItem.ToolTipText = vSubItem.ToolTipText & BL_HandleNull(rMessages!anexo, Empty) & "; "
        rMessages.MoveNext
    Wend
    If (rMessages.state = adStateOpen) Then: rMessages.Close
    VerifyAttachments = True
    Exit Function

ErrorHandler:
    Exit Function

End Function

Attribute VB_Name = "SINAVE"
Private Type DadosLab
    nif As String
    codLabDependente As String
End Type
Dim Laboratorio As DadosLab
Private Type DadosValidador
    codValidador As String
    DtHrValExm As String
End Type
Dim Validador As DadosValidador
Private Type DadosRequis
    num_epis_inst_saude As String
    num_proc_inst_saude As String
    num_requis_int As String
    num_requis_nacional As String
End Type
Dim requisicao As DadosRequis
Private Type DadosAmbulatorio
    empresa_id As String
    cod_postal_prefixo As String
    cod_posta_sufixo As String
End Type
Dim Ambulatorio As DadosAmbulatorio
Private Type DadosPrescritor
    ent_prof_presc As String
    nome_clinico_prescr As String
    num_cedula_presc As String
    'brunosinave
    cod_local_presc As String
    inst_presc_nome As String
End Type
Dim Prescritor As DadosPrescritor
Private Type DadosEstabelecimento
     codLocalPrescricao As String
     codPostalPrefixo As String
     codPostalSufixo As String
     nif_inst_presc As String
     freguesia_inst_presc As String
     nome_inst_presc As String
End Type
Dim Estabelecimento As DadosEstabelecimento
Private Type DadosUtente
    RNU As String
    codEntFinRes As String
    codPostalBase As String
    codPostalSufixo As String
    infClinica As String
    nomeEntFin As String
    NomeUte As String
    nBenefEntFin As String
    numIdentifUte As String
    tipoDoc As String
    DataNascUte As String
    fregResUte As String
    paisNacioUte As String
    paisNaturUte As String
    paisResUte As String
    sexUtente As String
End Type
Dim Utente As DadosUtente

Private Type DadosAnalise
    codAnaSNV As String
    descrAna As String
    codProdSNV As String
    codMetodoSNV As String
    descrMetodo As String
    especifProd As String
    DescrProd As String
    TabelaProdutos As String
End Type
Dim analise As DadosAnalise

Public Type DadosAntiMicrobianos
    MIC As String
    codAntibiotico As String
    nomeAntibiotico As String
    obsAntibiotico As String
    resistencias As String
    resultadoTSA As String
    tabelaAntibioticos As String
End Type

Public Type DadosAgentes
    TRAM As String
    codAgente As String
    AntiMicrobianos() As DadosAntiMicrobianos
    nEstirpe As String
    nomeAgente As String
    obsAgente As String
    tabelaAgentes As String
    TotalAntibioticos As Integer
End Type

Dim Agentes() As DadosAgentes

Public Type RespostaXML
    idRegistoDestino As String
    idRegistoOrigem As String
    codMensagem As String
    flgEnviado As String
    MsgRetorno As String
End Type
Dim RespostaXML As RespostaXML
Dim erro As Boolean

' ----------------------------------------------------------------------------------------------

' CHAMADA AO WEBSERVICE SINAVE

' ----------------------------------------------------------------------------------------------
Public Sub SNV_EnviaNotificacaoLaboratorial(ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)

    Dim seq_log_sinave As Long
    Dim strSoapAction As String
    Dim strXml As String
    Dim RetornoXML As String
    Dim urleR As String
    Dim sRet As String
    
    On Error GoTo TrataErro
    
    'EDGARPARADA Glintt-HS-19848
    erro = False
    '
    
    urleR = BL_DevolveURLWebService("SINAVE")
    
    SNV_InicializaEstrut RespostaXML

    If urleR = "" Then
        Exit Sub
    End If
    
    strSoapAction = ""
    strXml = ""
    
    strXml = SNV_GeraXML(EstrutGrEpid, iGrEpid, iRegra, iDeta)
    
    If erro = True Then
       Exit Sub
    End If
    
    seq_log_sinave = SNV_InsereLog(strXml, urleR)
    
    If seq_log_sinave > mediComboValorNull Then
        RetornoXML = SVN_PostWebservice(urleR, strSoapAction, strXml)
        If RetornoXML <> "" Then
            SNV_ConstroiEstruturaRespostaXML RespostaXML, UCase(RetornoXML)
            SVN_ActualizaLog seq_log_sinave, RetornoXML, RespostaXML.idRegistoDestino, RespostaXML.flgEnviado
            SVN_Atualiza_VE EstrutGrEpid, iGrEpid, iRegra, iDeta, RespostaXML.idRegistoDestino, RespostaXML.flgEnviado
            'sRet = SNV_RetornaConteudoTag(RetornoXML, "CreateUserResult")
            If RespostaXML.flgEnviado <> "1" Then
            'BG_Mensagem mediMsgBox, "Dados Enviados Com Sucesso!", vbInformation, "SINAVE"
            'Else
                BG_Mensagem mediMsgBox, "Falhou envio de Dados", vbExclamation, "SINAVE"
            End If
        End If
    End If
   
Exit Sub
TrataErro:
    BG_LogFile_Erros "SNV_EnviaNotificacaoLaboratorial: " & Err.Description, "SINAVE", "SNV_EnviaNotificacaoLaboratorial", True
    Exit Sub
    Resume Next

End Sub



' ----------------------------------------------------------------------------------------------

' CHAMADA AO WEBSERVICE

' ----------------------------------------------------------------------------------------------
Public Function SVN_PostWebservice(ByVal AsmxUrl As String, ByVal SoapActionUrl As String, ByVal XmlBody As String) As String
    Dim objDom As New MSXML2.DOMDocument
    Dim objXmlHttp As New MSXML2.xmlhttp
    Dim strRet As String
    Dim intPos1 As Long
    Dim intPos2 As Long
    
    On Error GoTo TrataErro
    
    ' Create objects to DOMDocument and XMLHTTP
'    Set objDom = CreateObject("MSXML2.DOMDocument")
'    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
    
    ' Load XML
    objDom.async = False
    objDom.loadXML XmlBody

    ' Open the webservice
    objXmlHttp.Open "POST", AsmxUrl, False
    
    ' Create headings
    objXmlHttp.SetRequestHeader "Content-Type", "text/xml; charset=utf-8" '"text/xml
    objXmlHttp.SetRequestHeader "SOAPAction", SoapActionUrl
    
    ' Send XML command
    objXmlHttp.Send objDom.xml

    ' Get all response text from webservice
    strRet = objXmlHttp.ResponseText
    
    ' Close object
    Set objXmlHttp = Nothing
    Set objDom = Nothing
    ' Return result
    SVN_PostWebservice = strRet
    
Exit Function
TrataErro:
    SVN_PostWebservice = ""
    BG_LogFile_Erros "SVN_PostWebservice: " & Err.Description, "SINAVE", "SVN_PostWebservice", True
    erro = True
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------

' INSERE LOG NA TABELA DE LOG COM A CHAMADA AO WEBSERVICE

' ----------------------------------------------------------------------------------------------
Public Function SNV_InsereLog(xml_request As String, xml_url As String) As Long
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsLog As New adodb.recordset
    Dim seq_log As Long
    Dim iReg As Integer
    Dim qtd As Double
    Dim i As Integer
    Dim sArr() As String
    Dim carateres As Long
    Dim sAux As String
    
    SNV_InsereLog = mediComboValorNull
    ssql = "SELECT seq_vig_epid.nextval proximo FROM dual"
    rsLog.CursorLocation = adUseServer
    rsLog.CursorType = adOpenStatic
    rsLog.Open ssql, gConexao
    If rsLog.RecordCount = 1 Then
        seq_log = BL_HandleNull(rsLog!proximo, "")
    End If
    rsLog.Close
    Set rsLog = Nothing
    
    carateres = 1
    xml_request = Trim(xml_request)
    If Len(xml_request) > 4000 Then
        qtd = Len(xml_request) / 3990
        
        If qtd > Round(qtd) Then
            qtd = Round(qtd) + 1
        End If
        
        For i = 1 To qtd
            ReDim Preserve sArr(i)
            
            If i = qtd Then
                sArr(i) = Mid(xml_request, carateres + 1, Len(xml_request) - carateres + 1)
                carateres = carateres + Len(sArr(i))
            Else
                sArr(i) = Mid(xml_request, carateres, 3990)
                carateres = carateres + Len(sArr(i))
                If i = 1 Then
                    carateres = carateres - 1
                End If
            End If
        Next i
    
        sAux = ""
        For i = 1 To UBound(sArr)
           sAux = sAux & "to_clob(" & BL_TrataStringParaBD(sArr(i)) & ")||"
        Next i
        sAux = Mid(sAux, 1, Len(sAux) - 2)
    Else
        sAux = BL_TrataStringParaBD(xml_request)
    End If
    
    If seq_log > mediComboValorNull Then
        ssql = "INSERT INTO sl_log_sinave(seq_vig_epid, url, request, dt_request, user_request) VALUES("
        ssql = ssql & seq_log & "," & BL_TrataStringParaBD(xml_url) & "," & sAux & ","
        ssql = ssql & " sysdate," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ")"
        iReg = BG_ExecutaQuery_ADO(ssql)
        If iReg = 1 Then
            SNV_InsereLog = seq_log
        End If
    End If
Exit Function

TrataErro:
    SNV_InsereLog = mediComboValorNull
    BG_LogFile_Erros "SNV_InsereLog: " & Err.Description, "SINAVE", "SNV_InsereLog", True
    erro = True
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------

' ACTUALIZA A TABELA DE LOG COM A RESPOSTA DO WEBSERVICE

' ----------------------------------------------------------------------------------------------
Public Function SVN_ActualizaLog(seq_log As Long, xml_response As String, idSNV As String, ByVal flg_enviado As Integer) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsLog As New adodb.recordset
    Dim iReg As Integer
    
    SVN_ActualizaLog = False
    If seq_log > mediComboValorNull Then
        ssql = "UPDATE sl_log_sinave SET response = " & BL_TrataStringParaBD(xml_response) & ","
        ssql = ssql & " dt_response = sysdate, user_act = " & BL_TrataStringParaBD(CStr(gCodUtilizador))
        ssql = ssql & ", id_sinave = " & BL_TrataStringParaBD(idSNV)
        ssql = ssql & ", flg_enviado =  " & flg_enviado
        ssql = ssql & " WHERE seq_vig_epid = " & seq_log
        iReg = BG_ExecutaQuery_ADO(ssql)
        If iReg = 1 Then
            SVN_ActualizaLog = True
        End If
    End If
Exit Function
TrataErro:
    SVN_ActualizaLog = False
    BG_LogFile_Erros "SVN_ActualizaLog: " & Err.Description, "SINAVE", "SVN_ActualizaLog", True
    erro = True
    Exit Function
    Resume Next
End Function

Public Function SNV_GeraXML(ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer) As String
Dim xml_doc As New DOMDocument
Dim notificacao_lab_node As IXMLDOMElement
Dim id_log As String
Dim sXML As String

    SNV_GeraXML = ""
    
    ' Make the notificacao_lab_node root node.
    Set notificacao_lab_node = xml_doc.createElement("not:enviarNotificacaoLaboratorial")
    xml_doc.appendChild notificacao_lab_node
    notificacao_lab_node.appendChild xml_doc.createTextNode(vbCrLf)
    
    ' Make some Credenciais elements.
    MakeCredenciais notificacao_lab_node
    
    'Make some accao elements
    id_log = ""
    MakeAccao notificacao_lab_node, SNV_TipoAccao(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_realiza, EstrutGrEpid(iGrEpid).regras(iRegra).cod_regra, id_log), id_log
    
    ' Make campos elements.
    MakeCamposNode notificacao_lab_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
        
    
    SNV_GeraXML = "<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:not=""http://notlab.sinave.pt/"">" & _
                    "<soapenv:Header/><soapenv:Body>" & xml_doc.xml & "</soapenv:Body></soapenv:Envelope>"
         
    SNV_GeraXML = Replace(SNV_GeraXML, "&gt;", ">")
    SNV_GeraXML = Replace(SNV_GeraXML, "&lt;", "<")
    'Debug.Print SNV_GeraXML
    
End Function

Private Sub MakeCredenciais(ByVal parent_node As IXMLDOMElement)

Dim credenciais_node As IXMLDOMElement
Dim utilizador As IXMLDOMElement
Dim password As IXMLDOMElement
Dim sArr() As String
Dim sutilizador As String
Dim spassword As String

    sArr = Split(gAuntenticaSINAVE, ";")
    sutilizador = sArr(0)
    spassword = sArr(1)
    
    ' Make the credenciais_node element.
    Set credenciais_node = parent_node.ownerDocument.createElement("credenciais")
    parent_node.appendChild credenciais_node

    ' Add the utilizador and password elements.
    Set utilizador = parent_node.ownerDocument.createElement("utilizador")
    credenciais_node.appendChild utilizador
    utilizador.appendChild parent_node.ownerDocument.createTextNode(sutilizador)

    Set password = parent_node.ownerDocument.createElement("password")
    credenciais_node.appendChild password
    password.appendChild parent_node.ownerDocument.createTextNode(spassword)
    
End Sub

Private Sub MakeAccao(ByVal parent_node As IXMLDOMElement, ByVal stipo_dados As String, ByVal sid_log As String)

Dim accao_node As IXMLDOMElement
Dim tipo_dados As IXMLDOMElement
Dim id_log As IXMLDOMElement

    ' Make the credenciais_node element.
    Set accao_node = parent_node.ownerDocument.createElement("accao")
    parent_node.appendChild accao_node

    ' Add the tipo_dados e id_log elements.
    Set tipo_dados = parent_node.ownerDocument.createElement("tipo_dados")
    accao_node.appendChild tipo_dados
    tipo_dados.appendChild parent_node.ownerDocument.createTextNode(stipo_dados)

    Set id_log = parent_node.ownerDocument.createElement("id_log")
    accao_node.appendChild id_log
    id_log.appendChild parent_node.ownerDocument.createTextNode(sid_log)
    
End Sub

Private Sub MakeCamposNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)

Dim campos_node As IXMLDOMElement

    ' Make the campos_node element.
    Set campos_node = parent_node.ownerDocument.createElement("campos")
    parent_node.appendChild campos_node
    
    MakeLaboratorioNode campos_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    MakeValidadorNode campos_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    MakeRegistoEnvioNode campos_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    MakeRequisicaoNode campos_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    MakeUtenteNode campos_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    MakeDoencaNode campos_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    MakeAmbulatorioNode campos_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    MakeEstabelecimentoNode campos_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    MakePrescritorNode campos_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
End Sub

Private Sub MakeLaboratorioNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim laboratorio_node As IXMLDOMElement
    
    ' Make the laboratorio_node element.
    Set laboratorio_node = parent_node.ownerDocument.createElement("laboratorio")
    parent_node.appendChild laboratorio_node
    
    SNV_CarregaDadosLab
    MakeLaboratorioElements laboratorio_node, Laboratorio.nif, Laboratorio.codLabDependente
End Sub

Private Sub MakeLaboratorioElements(ByVal parent_node As IXMLDOMElement, ByVal snif_lab As String, ByVal slab_depenente As String)

Dim nif_lab As IXMLDOMElement
Dim cod_lab_dependente As IXMLDOMElement

    ' Add the snif_lab and slab_depenente elements.
    Set nif_lab = parent_node.ownerDocument.createElement("NIF_lab")
    parent_node.appendChild nif_lab
    nif_lab.appendChild parent_node.ownerDocument.createTextNode(snif_lab)

    Set cod_lab_dependente = parent_node.ownerDocument.createElement("cod_lab_dependente")
    parent_node.appendChild cod_lab_dependente
    cod_lab_dependente.appendChild parent_node.ownerDocument.createTextNode(slab_depenente)

End Sub

Private Sub MakeValidadorNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim validador_node As IXMLDOMElement
    
    ' Make the validador_node element.
    Set validador_node = parent_node.ownerDocument.createElement("validador")
    parent_node.appendChild validador_node
   
    SNV_CarregaDadosValidador EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_realiza
    MakeValidadorElements validador_node, Validador.codValidador, Validador.DtHrValExm
    
End Sub

Private Sub MakeValidadorElements(ByVal parent_node As IXMLDOMElement, ByVal scod_validador As String, ByVal sdata_hora_validacao_exame As String)

Dim cod_validador As IXMLDOMElement
Dim data_hora_validacao_exame As IXMLDOMElement

    ' Add the snif_lab and slab_depenente elements.
    Set cod_validador = parent_node.ownerDocument.createElement("cod_validador")
    parent_node.appendChild cod_validador
    cod_validador.appendChild parent_node.ownerDocument.createTextNode(scod_validador)

    Set data_hora_validacao_exame = parent_node.ownerDocument.createElement("data_hora_validacao_exame")
    parent_node.appendChild data_hora_validacao_exame
    data_hora_validacao_exame.appendChild parent_node.ownerDocument.createTextNode(sdata_hora_validacao_exame)

End Sub

Private Sub MakeRegistoEnvioNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim registo_envio_node As IXMLDOMElement
    
    ' Make the registo_envio_node element.
    Set registo_envio_node = parent_node.ownerDocument.createElement("registo_envio")
    parent_node.appendChild registo_envio_node
    
    MakeRegistoEnvioElements registo_envio_node, EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_realiza, CStr(Format(Bg_DaDataHora_ADO, "yyyy-mm-ddThh:mm:ss"))
    
End Sub

Private Sub MakeRegistoEnvioElements(ByVal parent_node As IXMLDOMElement, ByVal scod_exame_origem As String, ByVal sdata_hora_envio_exame As String)

Dim cod_exame_origem As IXMLDOMElement
Dim data_hora_envio_exame As IXMLDOMElement

    ' Add the cod_exame_origem and data_hora_envio_exame elements.
    Set cod_exame_origem = parent_node.ownerDocument.createElement("cod_exame_origem")
    parent_node.appendChild cod_exame_origem
    cod_exame_origem.appendChild parent_node.ownerDocument.createTextNode(scod_exame_origem)

    Set data_hora_envio_exame = parent_node.ownerDocument.createElement("data_hora_envio_exame")
    parent_node.appendChild data_hora_envio_exame
    data_hora_envio_exame.appendChild parent_node.ownerDocument.createTextNode(sdata_hora_envio_exame)

End Sub

Private Sub MakeRequisicaoNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim requisicao_node As IXMLDOMElement
    
    ' Make the requisicao_node element.
    Set requisicao_node = parent_node.ownerDocument.createElement("requisicao")
    parent_node.appendChild requisicao_node
    
    SNV_CarregaDadosRequis EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).n_req
    MakeRequisicaoElements requisicao_node, requisicao.num_epis_inst_saude, requisicao.num_proc_inst_saude, requisicao.num_requis_int, requisicao.num_requis_nacional
    
End Sub

Private Sub MakeRequisicaoElements(ByVal parent_node As IXMLDOMElement, ByVal snum_episodio_instituicao_saude As String, ByVal snum_processo_instituicao_saude As String, _
ByVal snum_requisicao_interna As String, ByVal snum_requisicao_nacional As String)

Dim num_episodio_instituicao_saude As IXMLDOMElement
Dim num_processo_instituicao_saude As IXMLDOMElement
Dim num_requisicao_interna As IXMLDOMElement
Dim num_requisicao_nacional As IXMLDOMElement

    Set num_episodio_instituicao_saude = parent_node.ownerDocument.createElement("num_episodio_instituicao_saude")
    parent_node.appendChild num_episodio_instituicao_saude
    num_episodio_instituicao_saude.appendChild parent_node.ownerDocument.createTextNode(snum_episodio_instituicao_saude)

    Set num_processo_instituicao_saude = parent_node.ownerDocument.createElement("num_processo_instituicao_saude")
    parent_node.appendChild num_processo_instituicao_saude
    num_processo_instituicao_saude.appendChild parent_node.ownerDocument.createTextNode(snum_processo_instituicao_saude)
    
    Set num_requisicao_interna = parent_node.ownerDocument.createElement("num_requisicao_interna")
    parent_node.appendChild num_requisicao_interna
    num_requisicao_interna.appendChild parent_node.ownerDocument.createTextNode(snum_requisicao_interna)
    
    Set num_requisicao_nacional = parent_node.ownerDocument.createElement("num_requisicao_nacional")
    parent_node.appendChild num_requisicao_nacional
    num_requisicao_nacional.appendChild parent_node.ownerDocument.createTextNode(dnum_requisicao_nacional)

End Sub

Private Sub MakeUtenteNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim utente_node As IXMLDOMElement
    
    ' Make the utente_node element.
    Set utente_node = parent_node.ownerDocument.createElement("utente")
    parent_node.appendChild utente_node
    
    SNV_CarregaDadosUtente (EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_realiza)
    MakeUtenteElements utente_node, Utente.RNU, Utente.codEntFinRes, Utente.codPostalBase, Utente.codPostalSufixo, Utente.infClinica, Utente.nomeEntFin, Utente.NomeUte, _
            Utente.nBenefEntFin, Utente.numIdentifUte, Utente.tipoDoc, Utente.DataNascUte, Utente.fregResUte, Utente.paisNacioUte, Utente.paisNaturUte, Utente.paisResUte, Utente.sexUtente
    
End Sub

Private Sub MakeUtenteElements(ByVal parent_node As IXMLDOMElement, ByVal sRNU As String, ByVal scod_entid_financ_responsavel As String, _
ByVal scod_postal_base As String, ByVal scod_postal_sufixo As String, ByVal sinf_clinica As String, ByVal snome_entid_financ_responsavel As String, ByVal snome_utente As String, _
ByVal snum_benef_entid_financ_responsavel As String, ByVal snum_identif_doente As String, ByVal stipo_doc_identif_doente As String, ByVal sutente_data_nasc As String, _
ByVal sutente_freg_resid As String, ByVal sutente_pais_nacio As String, ByVal sutente_pais_natur As String, ByVal sutente_pais_resid As String, ByVal sutente_sexo As String)

Dim RNU As IXMLDOMElement
Dim cod_entid_financ_responsavel As IXMLDOMElement
Dim cod_postal_base As IXMLDOMElement
Dim cod_postal_sufixo As IXMLDOMElement
Dim inf_clinica As IXMLDOMElement
Dim nome_entid_financ_responsavel As IXMLDOMElement
Dim nome_utente As IXMLDOMElement
Dim num_benef_entid_financ_responsavel As IXMLDOMElement
Dim num_identif_doente As IXMLDOMElement
Dim tipo_doc_identif_doente As IXMLDOMElement
Dim utente_data_nasc As IXMLDOMElement
Dim utente_freg_resid As IXMLDOMElement
Dim utente_pais_nacio As IXMLDOMElement
Dim utente_pais_natur As IXMLDOMElement
Dim utente_pais_resid As IXMLDOMElement
Dim utente_sexo As IXMLDOMElement

    Set RNU = parent_node.ownerDocument.createElement("RNU")
    parent_node.appendChild RNU
    RNU.appendChild parent_node.ownerDocument.createTextNode(sRNU)

    Set cod_entid_financ_responsavel = parent_node.ownerDocument.createElement("cod_entid_financ_responsavel")
    parent_node.appendChild cod_entid_financ_responsavel
    cod_entid_financ_responsavel.appendChild parent_node.ownerDocument.createTextNode(scod_entid_financ_responsavel)
    
    Set cod_postal_base = parent_node.ownerDocument.createElement("cod_postal_base")
    parent_node.appendChild cod_postal_base
    cod_postal_base.appendChild parent_node.ownerDocument.createTextNode(scod_postal_base)
    
    Set cod_postal_sufixo = parent_node.ownerDocument.createElement("cod_postal_sufixo")
    parent_node.appendChild cod_postal_sufixo
    cod_postal_sufixo.appendChild parent_node.ownerDocument.createTextNode(scod_postal_sufixo)
    
    Set inf_clinica = parent_node.ownerDocument.createElement("inf_clinica")
    parent_node.appendChild inf_clinica
    inf_clinica.appendChild parent_node.ownerDocument.createTextNode(sinf_clinica)
    
    Set nome_entid_financ_responsavel = parent_node.ownerDocument.createElement("nome_entid_financ_responsavel")
    parent_node.appendChild nome_entid_financ_responsavel
    nome_entid_financ_responsavel.appendChild parent_node.ownerDocument.createTextNode(snome_entid_financ_responsavel)
    
    Set nome_utente = parent_node.ownerDocument.createElement("nome_utente")
    parent_node.appendChild nome_utente
    nome_utente.appendChild parent_node.ownerDocument.createTextNode(snome_utente)
    
    Set num_benef_entid_financ_responsavel = parent_node.ownerDocument.createElement("num_benef_entid_financ_responsavel")
    parent_node.appendChild num_benef_entid_financ_responsavel
    num_benef_entid_financ_responsavel.appendChild parent_node.ownerDocument.createTextNode(snum_benef_entid_financ_responsavel)
    
    Set num_identif_doente = parent_node.ownerDocument.createElement("num_identif_doente")
    parent_node.appendChild num_identif_doente
    num_identif_doente.appendChild parent_node.ownerDocument.createTextNode(snum_identif_doente)
    
    Set tipo_doc_identif_doente = parent_node.ownerDocument.createElement("tipo_doc_identif_doente")
    parent_node.appendChild tipo_doc_identif_doente
    tipo_doc_identif_doente.appendChild parent_node.ownerDocument.createTextNode(stipo_doc_identif_doente)
    
    Set utente_data_nasc = parent_node.ownerDocument.createElement("utente_data_nasc")
    parent_node.appendChild utente_data_nasc
    utente_data_nasc.appendChild parent_node.ownerDocument.createTextNode(sutente_data_nasc)
    
    Set utente_freg_resid = parent_node.ownerDocument.createElement("utente_freg_resid")
    parent_node.appendChild utente_freg_resid
    utente_freg_resid.appendChild parent_node.ownerDocument.createTextNode(sutente_freg_resid)
    
    Set utente_pais_nacio = parent_node.ownerDocument.createElement("utente_pais_nacio")
    parent_node.appendChild utente_pais_nacio
    utente_pais_nacio.appendChild parent_node.ownerDocument.createTextNode(sutente_pais_nacio)
    
    Set utente_pais_natur = parent_node.ownerDocument.createElement("utente_pais_natur")
    parent_node.appendChild utente_pais_natur
    utente_pais_natur.appendChild parent_node.ownerDocument.createTextNode(sutente_pais_natur)
    
    Set utente_pais_resid = parent_node.ownerDocument.createElement("utente_pais_resid")
    parent_node.appendChild utente_pais_resid
    utente_pais_resid.appendChild parent_node.ownerDocument.createTextNode(sutente_pais_resid)
    
    Set utente_sexo = parent_node.ownerDocument.createElement("utente_sexo")
    parent_node.appendChild utente_sexo
    utente_sexo.appendChild parent_node.ownerDocument.createTextNode(sutente_sexo)

End Sub

Private Sub MakeDoencaNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim doenca_node As IXMLDOMElement
    
    ' Make the doenca_node element.
    Set doenca_node = parent_node.ownerDocument.createElement("doenca")
    parent_node.appendChild doenca_node
    
    
    
    MakeDoencaElements doenca_node, SNV_CarregaCodDoenca(EstrutGrEpid(iGrEpid).regras(iRegra).cod_regra), EstrutGrEpid, iGrEpid, iRegra, iDeta
    
End Sub

Private Sub MakeDoencaElements(ByVal parent_node As IXMLDOMElement, ByVal scod_doenca As String, _
ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)

Dim cod_doenca As IXMLDOMElement

    Set cod_doenca = parent_node.ownerDocument.createElement("cod_doenca")
    parent_node.appendChild cod_doenca
    cod_doenca.appendChild parent_node.ownerDocument.createTextNode(scod_doenca)

    MakeProdutosNode parent_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    
End Sub

Private Sub MakeProdutosNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim produtos_node As IXMLDOMElement
    
    ' Make the produtos_node element.
    Set produtos_node = parent_node.ownerDocument.createElement("produtos")
    parent_node.appendChild produtos_node
    
    SNV_CarregaDadosAnalise EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_ana, EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_realiza, EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).dt_chega
    MakeProdutosElements produtos_node, Format(CDate(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).dt_chega), "yyyy-mm-ddThh:mm:ss"), analise.codProdSNV, analise.especifProd, analise.DescrProd, _
                        "", analise.TabelaProdutos, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
End Sub

Private Sub MakeProdutosElements(ByVal parent_node As IXMLDOMElement, ByVal sdata_colheita As String, ByVal scod_produto As String, ByVal sdesc_adic_produto As String, _
ByVal snome_produto As String, ByVal sobs_produto As String, ByVal stabela_produtos As String, _
ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)

Dim data_colheita As IXMLDOMElement
Dim cod_produto As IXMLDOMElement
Dim desc_adic_produto As IXMLDOMElement
Dim nome_produto As IXMLDOMElement
Dim obs_produto As IXMLDOMElement
Dim tabela_produtos As IXMLDOMElement
    
    Set data_colheita = parent_node.ownerDocument.createElement("data_colheita")
    parent_node.appendChild data_colheita
    data_colheita.appendChild parent_node.ownerDocument.createTextNode(sdata_colheita)
    
    MakeAnalisesNode parent_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    Set cod_produto = parent_node.ownerDocument.createElement("cod_produto")
    parent_node.appendChild cod_produto
    cod_produto.appendChild parent_node.ownerDocument.createTextNode(scod_produto)
    
    Set desc_adic_produto = parent_node.ownerDocument.createElement("desc_adic_produto")
    parent_node.appendChild desc_adic_produto
    desc_adic_produto.appendChild parent_node.ownerDocument.createTextNode(sdesc_adic_produto)
    
    Set nome_produto = parent_node.ownerDocument.createElement("nome_produto")
    parent_node.appendChild nome_produto
    nome_produto.appendChild parent_node.ownerDocument.createTextNode(snome_produto)
    
    Set obs_produto = parent_node.ownerDocument.createElement("obs_produto")
    parent_node.appendChild obs_produto
    obs_produto.appendChild parent_node.ownerDocument.createTextNode(sobs_produto)
    
    Set tabela_produtos = parent_node.ownerDocument.createElement("tabela_produtos")
    parent_node.appendChild tabela_produtos
    tabela_produtos.appendChild parent_node.ownerDocument.createTextNode(stabela_produtos)
      
    
    
End Sub

Private Sub MakeAnalisesNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim analises_node As IXMLDOMElement
    
    ' Make analises_node element.
    Set analises_node = parent_node.ownerDocument.createElement("analises")
    parent_node.appendChild analises_node
    
    MakeAnalisesElements analises_node, analise.codAnaSNV, analise.descrAna, "", "", "", EstrutGrEpid, iGrEpid, iRegra, iDeta
    
End Sub

Private Sub MakeAnalisesElements(ByVal parent_node As IXMLDOMElement, ByVal scod_analise As String, ByVal snome_analise As String, ByVal scod_analise_faturacao As String, _
ByVal stabela_analises_faturacao As String, ByVal sobs_analise As String, _
ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)

Dim cod_analise As IXMLDOMElement
Dim nome_analise As IXMLDOMElement
Dim cod_analise_faturacao As IXMLDOMElement
Dim tabela_analises_faturacao As IXMLDOMElement
Dim obs_analise As IXMLDOMElement
    
    Set cod_analise = parent_node.ownerDocument.createElement("cod_analise")
    parent_node.appendChild cod_analise
    cod_analise.appendChild parent_node.ownerDocument.createTextNode(scod_analise)
    
    Set nome_analise = parent_node.ownerDocument.createElement("nome_analise")
    parent_node.appendChild nome_analise
    nome_analise.appendChild parent_node.ownerDocument.createTextNode(snome_analise)
    
    Set cod_analise_faturacao = parent_node.ownerDocument.createElement("cod_analise_faturacao")
    parent_node.appendChild cod_analise_faturacao
    cod_analise_faturacao.appendChild parent_node.ownerDocument.createTextNode(dcod_analise_faturacao)
    
    Set tabela_analises_faturacao = parent_node.ownerDocument.createElement("tabela_analises_faturacao")
    parent_node.appendChild tabela_analises_faturacao
    tabela_analises_faturacao.appendChild parent_node.ownerDocument.createTextNode(stabela_analises_faturacao)
    
    MakeTecnicasNode parent_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    Set obs_analise = parent_node.ownerDocument.createElement("obs_analise")
    parent_node.appendChild obs_analise
    obs_analise.appendChild parent_node.ownerDocument.createTextNode(sobs_analise)
    
      
    
    
End Sub

Private Sub MakeTecnicasNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim tecnicas_node As IXMLDOMElement
    
    ' Make tecnicas_node element.
    Set tecnicas_node = parent_node.ownerDocument.createElement("tecnicas")
    parent_node.appendChild tecnicas_node
    
    MakeTecnicasElements tecnicas_node, analise.codMetodoSNV, analise.descrMetodo, "", EstrutGrEpid, iGrEpid, iRegra, iDeta
    
End Sub

Private Sub MakeTecnicasElements(ByVal parent_node As IXMLDOMElement, ByVal scod_tecnica As String, ByVal snome_tecnica As String, ByVal sobs_tecnica As String, _
ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)

Dim cod_tecnica As IXMLDOMElement
Dim nome_tecnica As IXMLDOMElement
Dim obs_tecnica As IXMLDOMElement
    
    Set cod_tecnica = parent_node.ownerDocument.createElement("cod_tecnica")
    parent_node.appendChild cod_tecnica
    cod_tecnica.appendChild parent_node.ownerDocument.createTextNode(scod_tecnica)
    
    Set nome_tecnica = parent_node.ownerDocument.createElement("nome_tecnica")
    parent_node.appendChild nome_tecnica
    nome_tecnica.appendChild parent_node.ownerDocument.createTextNode(snome_tecnica)
    
    MakeResultadoNode parent_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    Set obs_tecnica = parent_node.ownerDocument.createElement("obs_tecnica")
    parent_node.appendChild obs_tecnica
    obs_tecnica.appendChild parent_node.ownerDocument.createTextNode(sobs_tecnica)
     
    
      
End Sub

Private Sub MakeResultadoNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim resultado_node As IXMLDOMElement
    Dim resultado As String
    Dim i As Integer
    ' Make resultado_node element.
    Set resultado_node = parent_node.ownerDocument.createElement("resultado")
    parent_node.appendChild resultado_node
    
    'For i = 1 To UBound(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).ResAna)
     '   If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_ana = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).ResAna(i).cod_ana Then
      '      resultado = EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).ResAna(i).resultado
       '     Exit For
        'End If
    'Next i
    
    'EDGARPARADA Glintt-HS-19848
    resultadoQuant = SNV_DevolveResultado(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_realiza, EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_ana)
    
    If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).enviarResultado2 = mediSim Then
      resultadoQuali = SNV_DevolveResultado2(EstrutGrEpid(iGrEpid).regras(iRegra).cod_regra, EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_ana)
    End If
    
    MakeResultadoElements resultado_node, "", "", resultadoQuali, resultadoQuant, "", EstrutGrEpid, iGrEpid, iRegra, iDeta
    '
    
End Sub

Private Sub MakeResultadoElements(ByVal parent_node As IXMLDOMElement, ByVal scod_CPAL_resultado As String, ByVal sobs_resultado As String, ByVal sresul_qualitativo As String, _
ByVal sresul_quantitativo As String, ByVal svalor_refer As String, _
ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)

Dim cod_CPAL_resultado As IXMLDOMElement
Dim obs_resultado As IXMLDOMElement
Dim resul_qualitativo As IXMLDOMElement
Dim resul_quantitativo As IXMLDOMElement
Dim valor_refer As IXMLDOMElement
    
    If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_micro <> "" Then
        SNV_CarregaDadosAgentes EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_realiza, EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_micro
        For i = 1 To UBound(Agentes)
            MakeAgentesNode parent_node, EstrutGrEpid, iGrEpid, iRegra, iDeta, i
        Next i
    End If
    
    Set cod_CPAL_resultado = parent_node.ownerDocument.createElement("cod_CPAL_resultado")
    parent_node.appendChild cod_CPAL_resultado
    cod_CPAL_resultado.appendChild parent_node.ownerDocument.createTextNode(scod_CPAL_resultado)
    
    Set obs_resultado = parent_node.ownerDocument.createElement("obs_resultado")
    parent_node.appendChild obs_resultado
    obs_resultado.appendChild parent_node.ownerDocument.createTextNode(sobs_resultado)
    
    
    Set resul_qualitativo = parent_node.ownerDocument.createElement("resul_qualitativo")
    parent_node.appendChild resul_qualitativo
    resul_qualitativo.appendChild parent_node.ownerDocument.createTextNode(sresul_qualitativo)
    
    Set resul_quantitativo = parent_node.ownerDocument.createElement("resul_quantitativo")
    parent_node.appendChild resul_quantitativo
    resul_quantitativo.appendChild parent_node.ownerDocument.createTextNode(sresul_quantitativo)
    
    'NAO USAMOS
    'MakeSubAgentesNode parent_node, EstrutGrEpid, iGrEpid, iRegra, iDeta
    
    Set valor_refer = parent_node.ownerDocument.createElement("valor_refer")
    parent_node.appendChild valor_refer
    valor_refer.appendChild parent_node.ownerDocument.createTextNode(svalor_refer)
        
End Sub


Private Sub MakeAgentesNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer, _
ByVal i As Integer)
    
    Dim agentes_node As IXMLDOMElement
    
    
    ' Make agentes_node element.
    Set agentes_node = parent_node.ownerDocument.createElement("agentes")
    parent_node.appendChild agentes_node
    
    
    'RESULTADO N�O � DE MICRORGANISMO
    If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_micro = "" Then
         MakeAgentesElements agentes_node, "", "", "", "", "", "", EstrutGrEpid, iGrEpid, iRegra, iDeta, i, 1
    Else
        'SNV_CarregaDadosAgentes EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_realiza, EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_micro
        'For i = 1 To UBound(Agentes)
            MakeAgentesElements agentes_node, Agentes(i).TRAM, Agentes(i).codAgente, Agentes(i).nEstirpe, Agentes(i).nomeAgente, Agentes(i).obsAgente, _
            Agentes(i).tabelaAgentes, EstrutGrEpid, iGrEpid, iRegra, iDeta, i, Agentes(i).TotalAntibioticos
        'Next i
    End If
   
    
End Sub

Private Sub MakeAgentesElements(ByVal parent_node As IXMLDOMElement, ByVal sTRAM As String, ByVal scod_agente As String, ByVal sn_estirpe As String, _
ByVal snome_agente As String, ByVal sobs_agente As String, ByVal stabela_agentes As String, _
ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer, ByVal i As Integer, ByVal totalAntib As Integer)

Dim TRAM As IXMLDOMElement
Dim cod_agente As IXMLDOMElement
Dim n_estirpe As IXMLDOMElement
Dim nome_agente As IXMLDOMElement
Dim obs_agente As IXMLDOMElement
Dim tabela_agentes As IXMLDOMElement

       
    Set TRAM = parent_node.ownerDocument.createElement("TRAM")
    parent_node.appendChild TRAM
    TRAM.appendChild parent_node.ownerDocument.createTextNode(sTRAM)
    
    
    For j = 1 To totalAntib
        MakeAntimicrobianosNode parent_node, EstrutGrEpid, iGrEpid, iRegra, iDeta, i, j
    Next j
   
    Set cod_agente = parent_node.ownerDocument.createElement("cod_agente")
    parent_node.appendChild cod_agente
    cod_agente.appendChild parent_node.ownerDocument.createTextNode(scod_agente)
    
    Set n_estirpe = parent_node.ownerDocument.createElement("n_estirpe")
    parent_node.appendChild n_estirpe
    n_estirpe.appendChild parent_node.ownerDocument.createTextNode(sn_estirpe)
    
    Set nome_agente = parent_node.ownerDocument.createElement("nome_agente")
    parent_node.appendChild nome_agente
    nome_agente.appendChild parent_node.ownerDocument.createTextNode(snome_agente)
    
    
    Set obs_agente = parent_node.ownerDocument.createElement("obs_agente")
    parent_node.appendChild obs_agente
    obs_agente.appendChild parent_node.ownerDocument.createTextNode(sobs_agente)
    
    Set tabela_agentes = parent_node.ownerDocument.createElement("tabela_agentes")
    parent_node.appendChild tabela_agentes
    tabela_agentes.appendChild parent_node.ownerDocument.createTextNode(stabela_agentes)
        
End Sub

Private Sub MakeAntimicrobianosNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer, ByVal i As Integer, ByVal j As Integer)
    
    Dim antimicrobianos_node As IXMLDOMElement
    
    ' Make antimicrobianos_node element.
    Set antimicrobianos_node = parent_node.ownerDocument.createElement("antimicrobianos")
    parent_node.appendChild antimicrobianos_node
    
    If EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).cod_micro = "" Then
        MakeAntimicrobianosElements antimicrobianos_node, "", "", "", "", "", "", ""
    Else
        
    
        If Agentes(i).TotalAntibioticos > 0 Then
            'For j = 1 To Agentes(i).TotalAntibioticos
                MakeAntimicrobianosElements antimicrobianos_node, Agentes(i).AntiMicrobianos(j).MIC, Agentes(i).AntiMicrobianos(j).codAntibiotico, Agentes(i).AntiMicrobianos(j).nomeAntibiotico, _
                Agentes(i).AntiMicrobianos(j).obsAntibiotico, Agentes(i).AntiMicrobianos(j).resistencias, Agentes(i).AntiMicrobianos(j).resultadoTSA, Agentes(i).AntiMicrobianos(j).tabelaAntibioticos
            'Next j
        Else
            MakeAntimicrobianosElements antimicrobianos_node, "", "", "", "", "", "", ""
        End If
   ' Next i
      
      'For i = 1 To Agentes.TotalAntibioticos
         'MakeAntimicrobianosElements antimicrobianos_node, Agentes.AntiMicrobianos(i).MIC, Agentes.AntiMicrobianos(i).codAntibiotico, Agentes.AntiMicrobianos(i).nomeAntibiotico, _
        'Agentes.AntiMicrobianos(i).obsAntibiotico, Agentes.AntiMicrobianos(i).resistencias, Agentes.AntiMicrobianos(i).resultadoTSA, Agentes.AntiMicrobianos(i).tabelaAntibioticos
      'Next i
       
    End If
    
End Sub

Private Sub MakeAntimicrobianosElements(ByVal parent_node As IXMLDOMElement, ByVal sMIC As String, ByVal scod_antibiotico As String, ByVal snome_antibiotico As String, _
ByVal sobs_antibiotico As String, ByVal sresistencias_moleculares As String, ByVal sresultado_TSA As String, ByVal stabela_antibioticos As String)

Dim MIC As IXMLDOMElement
Dim cod_antibiotico As IXMLDOMElement
Dim nome_antibiotico As IXMLDOMElement
Dim obs_antibiotico As IXMLDOMElement
Dim resistencias_moleculares As IXMLDOMElement
Dim resultado_TSA As IXMLDOMElement
Dim tabela_antibioticos As IXMLDOMElement

       
    Set MIC = parent_node.ownerDocument.createElement("MIC")
    parent_node.appendChild MIC
    MIC.appendChild parent_node.ownerDocument.createTextNode(sMIC)
    
    Set cod_antibiotico = parent_node.ownerDocument.createElement("cod_antibiotico")
    parent_node.appendChild cod_antibiotico
    cod_antibiotico.appendChild parent_node.ownerDocument.createTextNode(scod_antibiotico)
    
    Set nome_antibiotico = parent_node.ownerDocument.createElement("nome_antibiotico")
    parent_node.appendChild nome_antibiotico
    nome_antibiotico.appendChild parent_node.ownerDocument.createTextNode(snome_antibiotico)
    
    Set obs_antibiotico = parent_node.ownerDocument.createElement("obs_antibiotico")
    parent_node.appendChild obs_antibiotico
    obs_antibiotico.appendChild parent_node.ownerDocument.createTextNode(sobs_antibiotico)
    
    Set resistencias_moleculares = parent_node.ownerDocument.createElement("resistencias_moleculares")
    parent_node.appendChild resistencias_moleculares
    resistencias_moleculares.appendChild parent_node.ownerDocument.createTextNode(sresistencias_moleculares)
    
    Set resultado_TSA = parent_node.ownerDocument.createElement("resultado_TSA")
    parent_node.appendChild resultado_TSA
    resultado_TSA.appendChild parent_node.ownerDocument.createTextNode(sresultado_TSA)
    
    Set tabela_antibioticos = parent_node.ownerDocument.createElement("tabela_antibioticos")
    parent_node.appendChild tabela_antibioticos
    tabela_antibioticos.appendChild parent_node.ownerDocument.createTextNode(stabela_antibioticos)
        
End Sub

Private Sub MakeSubAgentesNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim subagentes_node As IXMLDOMElement
    
    ' Make subagentes_node element.
    Set subagentes_node = parent_node.ownerDocument.createElement("subagentes")
    parent_node.appendChild subagentes_node
    
    MakeSubAgentesElements subagentes_node, "", "", "", "", ""
    
End Sub

Private Sub MakeSubAgentesElements(ByVal parent_node As IXMLDOMElement, ByVal scodigo_especificidade_agente As String, ByVal sDescricao As String, _
ByVal snome_lista_especificidade_agente As String, ByVal sobs_especificidade_agente As String, ByVal stabela_especificidade_agente As String)

Dim codigo_especificidade_agente As IXMLDOMElement
Dim descricao As IXMLDOMElement
Dim nome_lista_especificidade_agente As IXMLDOMElement
Dim obs_especificidade_agente As IXMLDOMElement
Dim tabela_especificidade_agente As IXMLDOMElement


    
    Set codigo_especificidade_agente = parent_node.ownerDocument.createElement("codigo_especificidade_agente")
    parent_node.appendChild codigo_especificidade_agente
    codigo_especificidade_agente.appendChild parent_node.ownerDocument.createTextNode(scodigo_especificidade_agente)
    
    Set descricao = parent_node.ownerDocument.createElement("descricao")
    parent_node.appendChild descricao
    descricao.appendChild parent_node.ownerDocument.createTextNode(sDescricao)
    
    Set nome_lista_especificidade_agente = parent_node.ownerDocument.createElement("nome_lista_especificidade_agente")
    parent_node.appendChild nome_lista_especificidade_agente
    nome_lista_especificidade_agente.appendChild parent_node.ownerDocument.createTextNode(snome_lista_especificidade_agente)
    
    Set obs_especificidade_agente = parent_node.ownerDocument.createElement("obs_especificidade_agente")
    parent_node.appendChild obs_especificidade_agente
    obs_especificidade_agente.appendChild parent_node.ownerDocument.createTextNode(sobs_especificidade_agente)
    
    Set tabela_especificidade_agente = parent_node.ownerDocument.createElement("tabela_especificidade_agente")
    parent_node.appendChild tabela_especificidade_agente
    tabela_especificidade_agente.appendChild parent_node.ownerDocument.createTextNode(stabela_especificidade_agente)
        
End Sub

Private Sub MakeAmbulatorioNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim ambulatorio_node As IXMLDOMElement
    
    ' Make the ambulatorio_node element.
    Set ambulatorio_node = parent_node.ownerDocument.createElement("ambulatorio")
    parent_node.appendChild ambulatorio_node
    
    SNV_CarregaDadosAmbulatorio
    MakeAmbulatorioElements ambulatorio_node, Ambulatorio.empresa_id, Ambulatorio.cod_postal_prefixo, Ambulatorio.cod_posta_sufixo
End Sub

Private Sub MakeAmbulatorioElements(ByVal parent_node As IXMLDOMElement, ByVal scod_local_colheita As String, ByVal scod_postal_base As String, _
ByVal scod_postal_sufixo As String)

Dim cod_local_colheita As IXMLDOMElement
Dim cod_postal_base As IXMLDOMElement
Dim cod_postal_sufixo As IXMLDOMElement

    
    Set cod_local_colheita = parent_node.ownerDocument.createElement("cod_local_colheita")
    parent_node.appendChild cod_local_colheita
    cod_local_colheita.appendChild parent_node.ownerDocument.createTextNode(scod_local_colheita)
    
    Set cod_postal_base = parent_node.ownerDocument.createElement("cod_postal_base")
    parent_node.appendChild cod_postal_base
    cod_postal_base.appendChild parent_node.ownerDocument.createTextNode(scod_postal_base)
    
    Set cod_postal_sufixo = parent_node.ownerDocument.createElement("cod_postal_sufixo")
    parent_node.appendChild cod_postal_sufixo
    cod_postal_sufixo.appendChild parent_node.ownerDocument.createTextNode(scod_postal_sufixo)
    
        
End Sub

Private Sub MakeEstabelecimentoNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim estabelecimento_node As IXMLDOMElement
    
    ' Make the estabelecimento_node element.
    Set estabelecimento_node = parent_node.ownerDocument.createElement("estabelecimento")
    parent_node.appendChild estabelecimento_node
    
    SNV_CarregaDadosEstabelecimento EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_realiza
    MakeEstabelecimentoElements estabelecimento_node, Estabelecimento.codLocalPrescricao, Estabelecimento.codPostalPrefixo, Estabelecimento.codPostalSufixo, Estabelecimento.nif_inst_presc, Estabelecimento.freguesia_inst_presc, Estabelecimento.nome_inst_presc
    
End Sub

Private Sub MakeEstabelecimentoElements(ByVal parent_node As IXMLDOMElement, ByVal scod_local_presc As String, ByVal scod_postal_base As String, _
ByVal scod_postal_sufixo As String, ByVal sinst_presc_NIF As String, ByVal sinst_presc_freg As String, ByVal sinst_presc_nome As String)

Dim cod_local_presc As IXMLDOMElement
Dim cod_postal_base As IXMLDOMElement
Dim cod_postal_sufixo As IXMLDOMElement
Dim inst_presc_NIF As IXMLDOMElement
Dim inst_presc_freg As IXMLDOMElement
Dim inst_presc_nome As IXMLDOMElement
    
    Set cod_local_presc = parent_node.ownerDocument.createElement("cod_local_presc")
    parent_node.appendChild cod_local_presc
    cod_local_presc.appendChild parent_node.ownerDocument.createTextNode(scod_local_presc)
    
    Set cod_postal_base = parent_node.ownerDocument.createElement("cod_postal_base")
    parent_node.appendChild cod_postal_base
    cod_postal_base.appendChild parent_node.ownerDocument.createTextNode(scod_postal_base)
    
    Set cod_postal_sufixo = parent_node.ownerDocument.createElement("cod_postal_sufixo")
    parent_node.appendChild cod_postal_sufixo
    cod_postal_sufixo.appendChild parent_node.ownerDocument.createTextNode(scod_postal_sufixo)
     
    Set inst_presc_NIF = parent_node.ownerDocument.createElement("inst_presc_NIF")
    parent_node.appendChild inst_presc_NIF
    inst_presc_NIF.appendChild parent_node.ownerDocument.createTextNode(sinst_presc_NIF)
     
    Set inst_presc_freg = parent_node.ownerDocument.createElement("inst_presc_freg")
    parent_node.appendChild inst_presc_freg
    inst_presc_freg.appendChild parent_node.ownerDocument.createTextNode(sinst_presc_freg)
     
    Set inst_presc_nome = parent_node.ownerDocument.createElement("inst_presc_nome")
    parent_node.appendChild inst_presc_nome
    inst_presc_nome.appendChild parent_node.ownerDocument.createTextNode(sinst_presc_nome)
    
        
End Sub

Private Sub MakePrescritorNode(ByVal parent_node As IXMLDOMElement, ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer)
    
    Dim prescritor_node As IXMLDOMElement
    
    ' Make the prescritor_node element.
    Set prescritor_node = parent_node.ownerDocument.createElement("prescritor")
    parent_node.appendChild prescritor_node
    
    SNV_CarregaDadosPrescritor CStr(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).n_req)
    MakePrescritorElements prescritor_node, Prescritor.ent_prof_presc, Prescritor.nome_clinico_prescr, Prescritor.num_cedula_presc, Prescritor.cod_local_presc, Prescritor.inst_presc_nome
    
End Sub

Private Sub MakePrescritorElements(ByVal parent_node As IXMLDOMElement, ByVal sentid_prof_prescritor As String, ByVal snome_clinico_prescritor As String, _
ByVal snum_cedula_prescritor As String, ByVal scod_local_presc As String, ByVal sinst_presc_nome As String)

Dim entid_prof_prescritor As IXMLDOMElement
Dim nome_clinico_prescritor As IXMLDOMElement
Dim num_cedula_prescritor As IXMLDOMElement
Dim cod_local_presc As IXMLDOMElement
Dim inst_presc_nome As IXMLDOMElement

     
    Set entid_prof_prescritor = parent_node.ownerDocument.createElement("entid_prof_prescritor")
    parent_node.appendChild entid_prof_prescritor
    entid_prof_prescritor.appendChild parent_node.ownerDocument.createTextNode(sentid_prof_prescritor)
     
    Set nome_clinico_prescritor = parent_node.ownerDocument.createElement("nome_clinico_prescritor")
    parent_node.appendChild nome_clinico_prescritor
    nome_clinico_prescritor.appendChild parent_node.ownerDocument.createTextNode(snome_clinico_prescritor)
     
    Set num_cedula_prescritor = parent_node.ownerDocument.createElement("num_cedula_prescritor")
    parent_node.appendChild num_cedula_prescritor
    num_cedula_prescritor.appendChild parent_node.ownerDocument.createTextNode(snum_cedula_prescritor)
    
    Set cod_local_presc = parent_node.ownerDocument.createElement("cod_local_presc")
    parent_node.appendChild cod_local_presc
    cod_local_presc.appendChild parent_node.ownerDocument.createTextNode(scod_local_presc)
    
    Set inst_presc_nome = parent_node.ownerDocument.createElement("inst_presc_nome")
    parent_node.appendChild inst_presc_nome
    inst_presc_nome.appendChild parent_node.ownerDocument.createTextNode(sinst_presc_nome)
End Sub

Private Function SNV_TipoAccao(ByVal seqRealiza As String, ByVal codRegra As String, ByRef id_log As String) As String

Dim rs As New adodb.recordset
Dim ssql As String
Dim id_snv As String
Dim flg_enviado As Integer
On Error GoTo TrataErro

    ssql = "SELECT vg.id_notif_sinave, s.flg_enviado FROM SL_VIG_EPID vg, sl_log_sinave s WHERE vg.seq_realiza = " & BL_TrataStringParaBD(seqRealiza) & " AND vg.cod_regra_ve = " & BL_TrataStringParaBD(codRegra)
    ssql = ssql & " AND vg.id_notif_sinave = s.id_sinave"
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
    
    If rs.RecordCount > 0 Then
        id_snv = BL_HandleNull(rs.Fields("id_notif_sinave").value, "")
        flg_enviado = BL_HandleNull(rs.Fields("flg_enviado").value, "")
    End If
    
    If id_snv = "" Or flg_enviado = 0 Then
        SNV_TipoAccao = "I"
        id_log = ""
    ElseIf id_snv <> "" And flg_enviado = 1 Then
        SNV_TipoAccao = "S"
        id_log = id_snv
    End If

    rs.Close
    Set rs = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros "SNV_TipoAccao: " & Err.Description, "SINAVE", "SNV_TipoAccao", True
    erro = True
    Exit Function
    Resume Next

End Function

Private Sub SNV_CarregaDadosLab()

Dim rs As New adodb.recordset
Dim ssql As String
Dim id_snv As String
On Error GoTo TrataErro

    ssql = "SELECT nif, sigla FROM gr_empr_inst WHERE empresa_id = " & gCodLocal
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
    
    If rs.RecordCount > 0 Then
        Laboratorio.nif = CStr(BL_HandleNull(rs.Fields("nif").value, ""))
        Laboratorio.codLabDependente = BL_HandleNull(rs.Fields("sigla").value, "")
    End If
    
    rs.Close
    Set rs = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "SNV_DadosLab: " & Err.Description, "SINAVE", "SNV_DadosLab", True
    erro = True
    Exit Sub
    Resume Next

End Sub

Private Sub SNV_CarregaDadosValidador(ByVal seqRealiza As String)

Dim rs As New adodb.recordset
Dim ssql As String
Dim id_snv As String
On Error GoTo TrataErro

    'sSql = "SELECT user_val,dt_val,hr_val FROM sl_realiza WHERE seq_realiza = " & BL_TrataStringParaBD(SeqRealiza)
    ssql = "SELECT r.seq_realiza, u.cod_sinave, r.user_val,r.dt_val,r.hr_val FROM sl_realiza r, sl_idutilizador u WHERE  r.user_val = u.cod_utilizador and seq_realiza = " & BL_TrataStringParaBD(seqRealiza)
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
     
    
    If rs.RecordCount > 0 Then
        Validador.codValidador = CStr(BL_HandleNull(rs.Fields("cod_sinave").value, ""))
        Validador.DtHrValExm = Format(CDate(rs.Fields("dt_val").value & " " & rs.Fields("hr_val").value), "yyyy-mm-ddThh:mm:ss")
    End If
    
    rs.Close
    Set rs = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "SNV_CarregaDadosValidador: " & Err.Description, "SINAVE", "SNV_CarregaDadosValidador", True
    erro = True
    Exit Sub
    Resume Next

End Sub

Private Sub SNV_CarregaDadosRequis(ByVal NReq As String)

Dim rs As New adodb.recordset
Dim ssql As String
Dim id_snv As String
On Error GoTo TrataErro
    
    If gTipoInstituicao = "PRIVADA" Then
        ssql = "SELECT p1 FROM sl_recibos_det WHERE n_req= " & BL_TrataStringParaBD(NReq)
    Else
        ssql = "SELECT n_req_exame FROM sl_req_episodio WHERE n_req = " & BL_TrataStringParaBD(NReq)
    End If
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
     
    
    If rs.RecordCount > 0 Then
         If gTipoInstituicao = "PRIVADA" Then
            requisicao.num_requis_nacional = BL_HandleNull(rs.Fields("p1").value, "")
         Else
             requisicao.num_requis_nacional = BL_HandleNull(rs.Fields("n_req_exame").value, "")
         End If
    End If
    
    rs.Close
    
    ssql = "SELECT id.utente, id.n_proc_1, req.n_epis FROM sl_requis req, sl_identif id WHERE req.seq_utente = id.seq_utente and req.n_req = " & BL_TrataStringParaBD(NReq)
    rs.Open ssql, gConexao
    
    If rs.RecordCount > 0 Then
         If gUsaNProc1 = mediSim Then
            requisicao.num_proc_inst_saude = BL_HandleNull(rs.Fields("n_proc_1").value, "")
         Else
             requisicao.num_proc_inst_saude = BL_HandleNull(rs.Fields("utente").value, "")
         End If
         requisicao.num_epis_inst_saude = BL_HandleNull(rs.Fields("n_epis").value, "")
    End If
    
    requisicao.num_requis_int = NReq
    
    rs.Close
    Set rs = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "SNV_CarregaDadosRequis: " & Err.Description, "SINAVE", "SNV_CarregaDadosRequis", True
    erro = True
    Exit Sub
    Resume Next

End Sub

Private Sub SNV_CarregaDadosAmbulatorio()

Dim rs As New adodb.recordset
Dim ssql As String
Dim id_snv As String
Dim arrStr() As String
On Error GoTo TrataErro
   
   
    ssql = "SELECT cod_postal from gr_empr_inst WHERE empresa_id = " & gCodLocal
   
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
     
    
    If rs.RecordCount > 0 Then
        Ambulatorio.empresa_id = gCodLocal
        If BL_HandleNull(rs.Fields("cod_postal").value, "") <> "" Then
            arrStr = Split(rs.Fields("cod_postal").value, "-")
            Ambulatorio.cod_postal_prefixo = arrStr(0)
            Ambulatorio.cod_posta_sufixo = arrStr(1)
        Else
             Ambulatorio.cod_postal_prefixo = ""
            Ambulatorio.cod_posta_sufixo = ""
        End If
    End If
    
    rs.Close

    Set rs = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "SNV_CarregaDadosAmbulatorio: " & Err.Description, "SINAVE", "SNV_CarregaDadosAmbulatorio", True
    erro = True
    Exit Sub
    Resume Next

End Sub

Private Sub SNV_CarregaDadosPrescritor(ByVal NReq As String)

Dim rs As New adodb.recordset
Dim ssql As String
Dim id_snv As String
On Error GoTo TrataErro
   
   Prescritor.ent_prof_presc = "M"
   
    ssql = "select med.cod_ordem, med.nome_med from sl_requis req, sl_medicos med where req.n_req = " & BL_TrataStringParaBD(NReq) & " AND req.cod_med = med.cod_med"
    'sSql = "SELECT med.cod_ordem, med.nome_med FROM sl_medicos med, sl_medicos_req mr WHERE med.cod_med = mr.cod_med and mr.n_req = " & BL_TrataStringParaBD(NReq)
   
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
    
    If rs.RecordCount > 0 Then
        Prescritor.num_cedula_presc = BL_HandleNull(rs.Fields("cod_ordem").value, "")
        Prescritor.nome_clinico_prescr = BL_HandleNull(rs.Fields("nome_med").value, "")
    End If
    
    rs.Close

    Set rs = Nothing
    
        'Brunosinave
    ssql = "select req.cod_proven, req.obs_proven from sl_requis req where req.n_req = " & BL_TrataStringParaBD(NReq) & " AND req.cod_med = 'DG'"
    'sSql = "SELECT med.cod_ordem, med.nome_med FROM sl_medicos med, sl_medicos_req mr WHERE med.cod_med = mr.cod_med and mr.n_req = " & BL_TrataStringParaBD(NReq)
   
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
    
    If rs.RecordCount > 0 Then
        Prescritor.cod_local_presc = BL_HandleNull(rs.Fields("cod_proven").value, "")
        Prescritor.inst_presc_nome = BL_HandleNull(rs.Fields("obs_proven").value, "")
    End If
    
    rs.Close

    Set rs = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "SNV_CarregaDadosPrescritor: " & Err.Description, "SINAVE", "SNV_CarregaDadosPrescritor", True
    erro = True
    Exit Sub
    Resume Next

End Sub

Private Sub SNV_CarregaDadosEstabelecimento(ByVal seqRealiza As String)

Dim rs As New adodb.recordset
Dim ssql As String
Dim id_snv As String
On Error GoTo TrataErro
   
    ssql = " SELECT x5.cod_sinave, x5.descr_sinave, x5.nif, x5.inst_presc_freg, " & _
            " x5.cod_postal_base, x5.cod_postal_sufixo  FROM sl_identif x1, sl_requis x2  LEFT OUTER JOIN sl_proven x5 ON nvl(x2.cod_proven, " & gDefaultProven & ") = x5.cod_proven, sl_realiza x3 LEFT OUTER JOIN sl_perfis x4 ON x3.cod_perfil = x4.cod_perfis " & _
            " WHERE x1.seq_utente = x2.seq_utente  AND x3.n_req = x2.n_req  AND x3.seq_realiza = " & BL_TrataStringParaBD(seqRealiza)
   
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
    
    
    If rs.RecordCount > 0 Then
        Estabelecimento.codLocalPrescricao = BL_HandleNull(rs.Fields("cod_sinave").value, "")
        Estabelecimento.nome_inst_presc = BL_HandleNull(rs.Fields("descr_sinave").value, "")
        Estabelecimento.codPostalPrefixo = BL_HandleNull(rs.Fields("cod_postal_base").value, "")
        Estabelecimento.codPostalSufixo = BL_HandleNull(rs.Fields("cod_postal_sufixo").value, "")
        Estabelecimento.freguesia_inst_presc = BL_HandleNull(rs.Fields("inst_presc_freg").value, "")
        Estabelecimento.nif_inst_presc = BL_HandleNull(rs.Fields("nif").value, "")
    End If
    
    rs.Close

    Set rs = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "SNV_CarregaDadosEstabelecimento: " & Err.Description, "SINAVE", "SNV_CarregaDadosEstabelecimento", True
    erro = True
    Exit Sub
    Resume Next

End Sub


Private Sub SNV_CarregaDadosUtente(ByVal seqRealiza As String)

Dim rs As New adodb.recordset
Dim ssql As String
Dim identif As identifRNU
Dim arrStr() As String

On Error GoTo TrataErro
   
   
   
    ssql = " SELECT x1.n_benef_ute, x1.seq_utente, x1.nome_ute,  x2.n_req, x2.dt_chega, x4.cod_produto, x1.dt_nasc_ute, decode(x1.sexo_ute,1,'M',0,'F') sexo_ute, x1.n_cartao_ute, x2.t_sit, x2.n_epis, x1.n_proc_1, nvl(x6.cod_sinave," & BL_TrataStringParaBD(gDefaultPais) & ") cod_pais, " & _
            " x1.cod_postal_ute, x5.cod_sinave, x5.descr_sinave, x5.nif, x5.inst_presc_freg, x7.cod_efr, x7.descr_efr, " & _
            " x5.cod_postal_base, x5.cod_postal_sufixo, x8.cod_externo  FROM sl_identif x1, sl_requis x2 LEFT OUTER JOIN sl_proven x5 ON x2.cod_proven = x5.cod_proven, " & _
            " sl_realiza x3 LEFT OUTER JOIN sl_perfis x4 ON x3.cod_perfil = x4.cod_perfis, " & _
            " sl_pais x6, sl_efr x7, sl_efr_map x8 " & _
            " WHERE x1.seq_utente = x2.seq_utente  AND x3.n_req = x2.n_req  AND x1.cod_pais = x6.cod_pais AND x2.cod_efr = x7.cod_efr AND x7.cod_efr = x8.cod_efr AND " & _
            " x3.seq_realiza = " & BL_TrataStringParaBD(seqRealiza)
   
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
    
    
    If rs.RecordCount > 0 Then
        Utente.RNU = BL_HandleNull(rs.Fields("n_cartao_ute").value, "")
       
        If Utente.RNU <> "" Then
           identif = RNU_GetPatientBySNS(BL_HandleNull(rs.Fields("n_cartao_ute").value, ""))
        End If
        
        'SE RNU N�O RETORNAR DADOS
        If identif.identificacao.NomeCompleto = "" Then
            Utente.numIdentifUte = BL_HandleNull(rs.Fields("seq_utente").value, "")
            Utente.tipoDoc = "NSEQ"
            Utente.NomeUte = BL_HandleNull(rs.Fields("nome_ute").value, "")
            Utente.DataNascUte = CStr(Format(BL_HandleNull(rs.Fields("dt_nasc_ute").value, ""), "yyyy-mm-ddThh:mm:ss"))
            Utente.sexUtente = BL_HandleNull(rs.Fields("sexo_ute").value, "")
            Utente.paisNacioUte = BL_HandleNull(rs.Fields("cod_pais").value, "")
            Utente.paisNaturUte = BL_HandleNull(rs.Fields("cod_pais").value, "")
            Utente.paisResUte = BL_HandleNull(rs.Fields("cod_pais").value, "")
            
            If BL_HandleNull(rs.Fields("cod_postal_ute").value, "") <> "" Then
                arrStr = Split(BL_HandleNull(rs.Fields("cod_postal_ute").value, ""), "-")
                Utente.codPostalBase = arrStr(0)
                Utente.codPostalSufixo = arrStr(1)
            Else
                Utente.codPostalBase = ""
                Utente.codPostalSufixo = ""
            End If
           
            Utente.nBenefEntFin = BL_HandleNull(rs.Fields("n_benef_ute").value, "")
            Utente.codEntFinRes = BL_HandleNull(rs.Fields("cod_externo").value, "")
            Utente.nomeEntFin = BL_HandleNull(rs.Fields("descr_efr").value, "")
            Utente.infClinica = ""
        Else
            Utente.numIdentifUte = BL_HandleNull(rs.Fields("seq_utente").value, "")
            Utente.tipoDoc = "NSEQ"
            Utente.NomeUte = identif.identificacao.NomeCompleto
            Utente.DataNascUte = identif.identificacao.DataNascimento
            Utente.sexUtente = identif.identificacao.Sexo
            Utente.paisNacioUte = identif.identificacao.codPais
            Utente.paisNaturUte = identif.identificacao.codPais
            Utente.paisResUte = identif.identificacao.codPais
            If BL_HandleNull(rs.Fields("cod_postal_ute").value, "") <> "" Then
                arrStr = Split(BL_HandleNull(rs.Fields("cod_postal_ute").value, ""), "-")
                Utente.codPostalBase = arrStr(0)
                Utente.codPostalSufixo = arrStr(1)
            Else
                Utente.codPostalBase = ""
                Utente.codPostalSufixo = ""
            End If
            Utente.nBenefEntFin = BL_HandleNull(rs.Fields("n_benef_ute").value, "")
            Utente.codEntFinRes = BL_HandleNull(rs.Fields("cod_externo").value, "")
            Utente.nomeEntFin = BL_HandleNull(rs.Fields("descr_efr").value, "")

            Utente.infClinica = ""
        End If
    End If
    
    rs.Close

    Set rs = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "SNV_CarregaDadosUtente: " & Err.Description, "SINAVE", "SNV_CarregaDadosUtente", True
    erro = True
    Exit Sub
    Resume Next

End Sub

Private Function SNV_CarregaCodDoenca(ByVal codRegra As String) As String

Dim rs As New adodb.recordset
Dim ssql As String
Dim identif As identifRNU
Dim arrStr() As String

On Error GoTo TrataErro
   
      
    ssql = "SELECT cod_doenca_snv FROM sl_cod_regras_ve WHERE cod_regra_ve = " & BL_TrataStringParaBD(codRegra)
   
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
    
    
    If rs.RecordCount > 0 Then
        SNV_CarregaCodDoenca = BL_HandleNull(rs.Fields("cod_doenca_snv").value, "")
    End If
    
    rs.Close

    Set rs = Nothing

Exit Function
TrataErro:
    BG_LogFile_Erros "SNV_CarregaCodDoenca: " & Err.Description, "SINAVE", "SNV_CarregaCodDoenca", True
    erro = True
    Exit Function
    Resume Next

End Function

Private Sub SNV_CarregaDadosAnalise(ByVal codAna As String, ByVal seqRealiza As String, ByVal DtChega As String)

Dim rsMetodo As New adodb.recordset
Dim rs As New adodb.recordset
Dim ssql As String


On Error GoTo TrataErro
   
    rsMetodo.CursorLocation = adUseServer
    rsMetodo.CursorType = adOpenStatic
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    
    ssql = "SELECT * FROM sl_ana_ref af WHERE af.cod_ana_s = " & BL_TrataStringParaBD(codAna)
    
    rsMetodo.Open ssql, gConexao
    
    'SE TIVER METODO VAI BUSCAR O QUE ESTAVA EM VIGENCIA NA ALTURA DA REALIZACAO DA ANALISE
    If rsMetodo.RecordCount > 0 Then
        ssql = " SELECT ana.cod_ana_sinave, ana.descr_ana_s, m.descr_metodo, p.cod_sinave produto_sinave, m.cod_tec_sinave, esp.descr_especif, p.descr_produto FROM " & _
        " sl_ana_s ana, sl_ana_ref af, sl_produto p, sl_especif esp, sl_metodo m, sl_realiza r WHERE " & _
        " ana.cod_ana_s = af.cod_ana_s AND ana.cod_produto = p.cod_produto(+) AND m.cod_metodo(+) = af.cod_metod AND p.cod_especif = esp.cod_especif(+) AND ana.cod_ana_s = " & BL_TrataStringParaBD(codAna) & _
        " AND r.seq_realiza =  " & BL_TrataStringParaBD(seqRealiza) & " AND r.dt_chega BETWEEN af.dt_valid_inf AND NVL(af.dt_valid_sup, '12-12-9999') "
        
        rs.Open ssql, gConexao
        
        If rs.RecordCount = 1 Then
            analise.codAnaSNV = BL_HandleNull(rs.Fields("cod_ana_sinave").value, "")
            analise.descrAna = BL_HandleNull(rs.Fields("descr_ana_s").value, "")
            analise.codProdSNV = BL_HandleNull(rs.Fields("produto_sinave").value, "")
            analise.codMetodoSNV = BL_HandleNull(rs.Fields("cod_tec_sinave").value, "")
            analise.descrMetodo = BL_HandleNull(rs.Fields("descr_metodo").value, "")
            analise.especifProd = BL_HandleNull(rs.Fields("descr_especif").value, "")
            analise.DescrProd = BL_HandleNull(rs.Fields("descr_produto").value, "")
            analise.TabelaProdutos = "SINAVE"
        Else
            BG_Mensagem mediMsgBox, "Verificar valores de refer�ncia da an�lise!", vbExclamation, "Erro"
            BG_LogFile_Erros "SNV_CarregaDadosAnalise SQL: " & ssql, "SINAVE", "SNV_CarregaDadosAnalise"
            erro = True
        End If
        
    'SE NAO TIVER METODO NAO ENVIA O MESMO
    Else
        ssql = "SELECT ana.cod_ana_sinave, ana.descr_ana_s, p.cod_sinave produto_sinave, esp.descr_especif, p.descr_produto FROM sl_ana_s ana, sl_produto p, sl_especif esp " & _
        " WHERE  ana.cod_produto = p.cod_produto AND p.cod_especif = esp.cod_especif(+)  AND ana.cod_ana_s = " & BL_TrataStringParaBD(codAna)
    
        rs.Open ssql, gConexao
        
        If rs.RecordCount = 1 Then
            analise.codAnaSNV = BL_HandleNull(rs.Fields("cod_ana_sinave").value, "")
            analise.descrAna = BL_HandleNull(rs.Fields("descr_ana_s").value, "")
            analise.codProdSNV = BL_HandleNull(rs.Fields("produto_sinave").value, "")
            analise.codMetodoSNV = ""
            analise.descrMetodo = ""
            analise.especifProd = BL_HandleNull(rs.Fields("descr_especif").value, "")
            analise.DescrProd = BL_HandleNull(rs.Fields("descr_produto").value, "")
            analise.TabelaProdutos = ""
        Else
            BG_Mensagem mediMsgBox, "Verificar valores de refer�ncia da an�lise!", vbExclamation, "Erro"
            BG_LogFile_Erros "SNV_CarregaDadosAnalise SQL: " & ssql, "SINAVE", "SNV_CarregaDadosAnalise"
            erro = True
        End If
        
    End If
    
    
    rsMetodo.Close
    rs.Close
    
    Set rsMetodo = Nothing
    Set rs = Nothing

    
Exit Sub
TrataErro:
    BG_LogFile_Erros "SNV_CarregaDadosAnalise: " & Err.Description, "SINAVE", "SNV_CarregaDadosAnalise", True
    erro = True
    Exit Sub
    Resume Next

End Sub

Private Sub SNV_CarregaDadosAgentes(ByVal seqRealiza As String, ByVal CodMicro As String)

Dim rs As New adodb.recordset
Dim rsAnti As New adodb.recordset
Dim ssql As String
Dim i As Integer
i = 1
Dim j As Integer
j = 1


ReDim Agentes(i)


On Error GoTo TrataErro
   
    ssql = "select * from sl_res_micro rm, sl_microrg m where rm.cod_micro = m.cod_microrg and rm.seq_realiza = " & seqRealiza
    
    'sSql = " SELECT tsq.seq_realiza,  m.cod_microrg, m.descr_microrg, a.cod_sinave cod_antibio, a.descr_antibio, tsq.res_sensib FROM sl_res_tsq tsq, sl_antibio a, sl_microrg m " &
    '        " WHERE tsq.cod_antib = a.cod_antibio AND m.cod_microrg = tsq.cod_micro AND tsq.seq_realiza = " & BL_TrataStringParaBD(SeqRealiza) & " AND m.cod_microrg = " & _
    '        BL_TrataStringParaBD(CodMicro)
   
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
    
    
    If rs.RecordCount > 0 Then
       
       While Not rs.EOF
        
        ReDim Preserve Agentes(i)
        
        Agentes(i).codAgente = BL_HandleNull(rs.Fields("cod_sinave").value, "")
        Agentes(i).nomeAgente = BL_HandleNull(rs.Fields("descr_microrg").value, "")
        Agentes(i).tabelaAgentes = "SINAVE"
        Agentes(i).TRAM = "0"
        Agentes(i).obsAgente = ""
        Agentes(i).nEstirpe = ""
        Agentes(i).TotalAntibioticos = 0
        
        ssql = "select * from sl_res_tsq res, sl_antibio a where res.cod_micro = " & BL_TrataStringParaBD(rs.Fields("cod_micro").value) & " and res.seq_realiza = " & BL_TrataStringParaBD(seqRealiza)
        ssql = ssql & " AND a.cod_antibio = res.cod_antib"
        rsAnti.CursorLocation = adUseServer
        rsAnti.CursorType = adOpenStatic
        rsAnti.Open ssql, gConexao
        
        If rsAnti.RecordCount > 0 Then
            While Not rsAnti.EOF
                ReDim Preserve Agentes(i).AntiMicrobianos(rsAnti.RecordCount)
                Agentes(i).TRAM = "1"
                Agentes(i).AntiMicrobianos(j).codAntibiotico = BL_HandleNull(rsAnti.Fields("cod_sinave").value, "")
                If CodMicro <> "" Then
                    Agentes(i).AntiMicrobianos(j).tabelaAntibioticos = "SINAVE"
                Else
                     Agentes(i).AntiMicrobianos(j).tabelaAntibioticos = ""
                End If
                Agentes(i).AntiMicrobianos(j).nomeAntibiotico = BL_HandleNull(rsAnti.Fields("descr_antibio").value, "")
                Agentes(i).AntiMicrobianos(j).resultadoTSA = BL_HandleNull(rsAnti.Fields("res_sensib").value, "")
                Agentes(i).AntiMicrobianos(j).MIC = BL_HandleNull(rsAnti.Fields("cmi").value, "")
                Agentes(i).AntiMicrobianos(j).resistencias = ""
                Agentes(i).AntiMicrobianos(j).obsAntibiotico = ""
                
                j = j + 1
                rsAnti.MoveNext
            Wend
            Agentes(i).TotalAntibioticos = j - 1
        End If
        
         i = i + 1
        rs.MoveNext
       Wend
       
    End If
    
    
    If Not rs Is Nothing Then
        If rs.state = adStateOpen Then
             rs.Close
        End If
        Set rs = Nothing
    End If
   
    If Not rsAnti Is Nothing Then
        If rsAnti.state = adStateOpen Then
            rsAnti.Close
        End If
        Set rsAnti = Nothing
    End If
    
   

Exit Sub
TrataErro:
    BG_LogFile_Erros "SNV_CarregaDadosAgentes: " & Err.Description, "SINAVE", "SNV_CarregaDadosAgentes", True
    erro = True
    Exit Sub
    Resume Next

End Sub

Private Function SNV_ConstroiEstrutura(estrutXML As RespostaXML, textoXML As String) As Boolean
     On Error GoTo TrataErro
     SNV_ConstroiEstrutura = False
     If SNV_ConstroiEstrutura(estrutXML, textoXML) = False Then
        GoTo TrataErro
    End If

    SNV_ConstroiEstrutura = True
Exit Function
TrataErro:
    SNV_ConstroiEstrutura = False
    BG_LogFile_Erros "SNV_ConstroiEstrutura: " & Err.Description, "SINAVE", "SNV_ConstroiEstrutura", True
    erro = True
    Exit Function
    Resume Next
End Function

Private Function SNV_ConstroiEstruturaRespostaXML(estrutXML As RespostaXML, textoXML As String) As Boolean
    Dim varString As String
    On Error GoTo TrataErro
    SNV_ConstroiEstruturaRespostaXML = False
  
    'ID_registo_destino
    RespostaXML.idRegistoDestino = SNV_RetornaConteudoTag(textoXML, "ID_registo_destino")
    'ID_registo_origem
     RespostaXML.idRegistoOrigem = SNV_RetornaConteudoTag(textoXML, "ID_registo_origem")
    'codigo_mensagem
     RespostaXML.codMensagem = SNV_RetornaConteudoTag(textoXML, "codigo_mensagem")
    'enviado
     RespostaXML.flgEnviado = SNV_RetornaConteudoTag(textoXML, "enviado")
    'mensagem_retorno
     RespostaXML.MsgRetorno = SNV_RetornaConteudoTag(textoXML, "mensagem_retorno")
    
    SNV_ConstroiEstruturaRespostaXML = True
Exit Function
TrataErro:
    SNV_ConstroiEstruturaRespostaXML = False
    BG_LogFile_Erros "SNV_ConstroiEstruturaRespostaXML: " & Err.Description, "SINAVE", "SNV_ConstroiEstruturaRespostaXML", True
    erro = True
    Exit Function
    Resume Next
End Function


Public Function SNV_RetornaConteudoTag(textoXML As String, variavel As String) As String
    On Error GoTo TrataErro
    Dim iInicio As Long
    Dim iTamanho As Long
    
    variavel = UCase(variavel)
    textoXML = UCase(textoXML)
    
    If InStr(1, textoXML, "<" & variavel & ">") > 0 Then
       iInicio = InStr(1, textoXML, "<" & variavel & ">") + Len("<" & variavel & ">")
       iTamanho = InStr(1, textoXML, "</" & variavel & ">") - iInicio
       SNV_RetornaConteudoTag = Mid(textoXML, iInicio, iTamanho)
    End If
Exit Function
TrataErro:
    SNV_RetornaConteudoTag = ""
    BG_LogFile_Erros "SNV_RetornaConteudoTag: " & Err.Description, "SINAVE", "SNV_RetornaConteudoTag", True
    erro = True
    Exit Function
    Resume Next
End Function

Private Function SNV_InicializaEstrut(estrutXML As RespostaXML)
    
    estrutXML.codMensagem = ""
    estrutXML.flgEnviado = ""
    estrutXML.idRegistoDestino = ""
    estrutXML.idRegistoOrigem = ""
    estrutXML.MsgRetorno = ""
    
    Utente.RNU = ""
    Utente.codEntFinRes = ""
    Utente.codPostalBase = ""
    Utente.codPostalSufixo = ""
    Utente.infClinica = ""
    Utente.nomeEntFin = ""
    Utente.NomeUte = ""
    Utente.nBenefEntFin = ""
    Utente.numIdentifUte = ""
    Utente.tipoDoc = ""
    Utente.DataNascUte = ""
    Utente.fregResUte = ""
    Utente.paisNacioUte = ""
    Utente.paisNaturUte = ""
    Utente.paisResUte = ""
    Utente.sexUtente = ""
        
End Function

Private Sub SVN_Atualiza_VE(ByRef EstrutGrEpid() As GrEpid, ByVal iGrEpid As Integer, ByVal iRegra As Integer, ByVal iDeta As Integer, ByVal idSNV As String, ByVal flgEnviado As String)
    Dim ssql As String
    Dim strEnviado As String
    On Error GoTo TrataErro
        
    If flgEnviado = "0" Then
        strEnviado = CStr(gEstadoVePendente)
    Else
        strEnviado = CStr(gEstadoVeEnviado)
    End If
    
    ssql = "UPDATE sl_vig_epid set id_notif_sinave = " & idSNV & ", cod_estado_ve = " & BL_TrataStringParaBD(strEnviado) & " WHERE cod_regra_ve = " & _
    BL_TrataStringParaBD(EstrutGrEpid(iGrEpid).regras(iRegra).cod_regra) & " AND seq_realiza = " & BL_TrataStringParaBD(EstrutGrEpid(iGrEpid).regras(iRegra).detalhes(iDeta).seq_realiza)

     BG_ExecutaQuery_ADO ssql
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "SVN_Atualiza_VE: " & Err.Description, "SINAVE", "SVN_Atualiza_VE", True
    erro = True
    Exit Sub
    Resume Next
End Sub

Public Function SNV_DevolveRespostaFromID(ByVal idSNV As String) As String

Dim rs As New adodb.recordset
Dim ssql As String
Dim id_snv As String
On Error GoTo TrataErro
   
    ssql = "SELECT response FROM sl_log_sinave WHERE id_sinave = " & BL_TrataStringParaBD(idSNV)
   
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
    
    
    If rs.RecordCount > 0 Then
        SNV_DevolveRespostaFromID = BL_HandleNull(rs.Fields("response").value, "")
    End If
    
    rs.Close

    Set rs = Nothing

Exit Function
TrataErro:
    BG_LogFile_Erros "SNV_DevolveRespostaFromID: " & Err.Description, "SINAVE", "SNV_DevolveRespostaFromID", True
    erro = True
    Exit Function
    Resume Next

End Function

Private Sub LimpaEstruturas()
   
End Sub

'edgar.parada Glintt-HS-19848
Public Function SNV_DevolveResultado2(ByVal cod_regra_ve As String, ByVal codAna As String) As String

Dim rs As New adodb.recordset
Dim ssql As String
On Error GoTo TrataErro
SNV_DevolveResultado2 = ""
    
    ssql = "SELECT abrev_sinave FROM sl_cod_regras_ve_ana , sl_tbf_seg_res_sinave WHERE cod_regra_ve= " & cod_regra_ve & " and cod_ana_s = " & BL_TrataStringParaBD(codAna) & " and cod_sinave=resultado2"
   
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
    
    
    If rs.RecordCount > 0 Then
        SNV_DevolveResultado2 = BL_HandleNull(rs.Fields("abrev_sinave").value, "")
    Else
        SNV_DevolveResultado2 = ""
    End If
    
    rs.Close
    Set rs = Nothing

Exit Function
TrataErro:
    BG_LogFile_Erros "SNV_DevolveResultado2: " & Err.Description, "SINAVE", "SNV_DevolveResultado", True
    erro = True
    Exit Function
    Resume Next
End Function
'
Public Function SNV_DevolveResultado(ByVal seqRealiza As String, ByVal codAna As String) As String

Dim rs As New adodb.recordset
Dim ssql As String
Dim id_snv As String
On Error GoTo TrataErro
SNV_DevolveResultado = ""

    ssql = "SELECT * FROM sl_realiza r, sl_res_alfan ral where r.seq_realiza = ral.seq_realiza and r.cod_ana_s = " & BL_TrataStringParaBD(codAna) & " AND r.seq_realiza = " & _
            BL_TrataStringParaBD(seqRealiza)
   
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open ssql, gConexao
    
    
    If rs.RecordCount > 0 Then
        SNV_DevolveResultado = BL_HandleNull(rs.Fields("result").value, "")
    Else
        rs.Close
        Set rs = Nothing
        
        ssql = "SELECT d.descr_frase FROM sl_realiza r, sl_res_frase ral, sl_dicionario d WHERE ral.seq_realiza = " & BL_TrataStringParaBD(seqRealiza) & " AND r.seq_realiza = ral.seq_realiza AND ral.cod_frase = d.cod_frase"
        
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenStatic
        rs.Open ssql, gConexao
        
        If rs.RecordCount > 0 Then
            SNV_DevolveResultado = BL_HandleNull(rs.Fields("descr_frase").value, "")
        End If
        
    End If
    
    rs.Close
    Set rs = Nothing

Exit Function
TrataErro:
    BG_LogFile_Erros "SNV_DevolveResultado: " & Err.Description, "SINAVE", "SNV_DevolveResultado", True
    erro = True
    Exit Function
    Resume Next

End Function

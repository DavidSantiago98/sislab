VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form FormDiario 
   BackColor       =   &H00FFE0C7&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormDiario"
   ClientHeight    =   8985
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   15225
   Icon            =   "FormDiario.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8985
   ScaleWidth      =   15225
   ShowInTaskbar   =   0   'False
   Begin SISLAB.CabecalhoResultados EcCabecalho 
      Height          =   1095
      Left            =   120
      TabIndex        =   12
      Top             =   0
      Width           =   14895
      _ExtentX        =   26273
      _ExtentY        =   1931
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   360
      Top             =   1240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDiario.frx":000C
            Key             =   "TASK"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDiario.frx":03A6
            Key             =   "TASKG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDiario.frx":0740
            Key             =   "HIST"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDiario.frx":0ADA
            Key             =   "VERDE"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDiario.frx":0E74
            Key             =   "VERMELHO"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDiario.frx":120E
            Key             =   "AMARELO"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   0
      ScaleHeight     =   315
      ScaleWidth      =   555
      TabIndex        =   1
      Top             =   11160
      Width           =   615
   End
   Begin VB.Frame FrameGeral 
      BackColor       =   &H00FFE0C7&
      BorderStyle     =   0  'None
      Height          =   7695
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   14895
      Begin VB.TextBox EcPrinterEtiq 
         Height          =   285
         Left            =   0
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   0
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Frame FrameNovaActividade 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFE0C7&
         Caption         =   "Novo Detalhe"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   3495
         Left            =   3120
         TabIndex        =   4
         Top             =   3960
         Width           =   11655
         Begin VB.CommandButton BtPesquisaFrase 
            Height          =   315
            Left            =   11160
            Picture         =   "FormDiario.frx":15A8
            Style           =   1  'Graphical
            TabIndex        =   15
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
            Top             =   720
            Width           =   375
         End
         Begin VB.ListBox EcListaFrases 
            Appearance      =   0  'Flat
            Height          =   810
            Left            =   1320
            TabIndex        =   14
            Top             =   720
            Width           =   9855
         End
         Begin VB.TextBox EcDescricao 
            Appearance      =   0  'Flat
            Height          =   1335
            Left            =   1320
            TabIndex        =   13
            Top             =   1680
            Width           =   9855
         End
         Begin VB.ComboBox CbEstadoMeio 
            Height          =   315
            Left            =   7320
            Style           =   2  'Dropdown List
            TabIndex        =   9
            Top             =   240
            Width           =   4215
         End
         Begin VB.CommandButton BtPesquisaActividade 
            Height          =   315
            Left            =   5400
            Picture         =   "FormDiario.frx":1B32
            Style           =   1  'Graphical
            TabIndex        =   7
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Grupos de An�lises"
            Top             =   240
            Width           =   375
         End
         Begin VB.TextBox EcDescrActividade 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2040
            Locked          =   -1  'True
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   240
            Width           =   3375
         End
         Begin VB.TextBox EcCodActividade 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1320
            TabIndex        =   5
            Top             =   240
            Width           =   735
         End
         Begin VB.Label Label1 
            BackColor       =   &H00FFE0C7&
            Caption         =   "Frases"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   16
            Top             =   720
            Width           =   855
         End
         Begin VB.Image BtAdicionarDetalhe 
            Height          =   480
            Left            =   11160
            Picture         =   "FormDiario.frx":1EBC
            ToolTipText     =   "Adinionar Nova Actividade"
            Top             =   3000
            Width           =   480
         End
         Begin VB.Label Label1 
            BackColor       =   &H00FFE0C7&
            Caption         =   "Descri��o"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   11
            Top             =   1680
            Width           =   855
         End
         Begin VB.Label Label1 
            BackColor       =   &H00FFE0C7&
            Caption         =   "Estado"
            Height          =   255
            Index           =   13
            Left            =   6360
            TabIndex        =   10
            Top             =   240
            Width           =   855
         End
         Begin VB.Label Label1 
            BackColor       =   &H00FFE0C7&
            Caption         =   "Tipo"
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   8
            Top             =   240
            Width           =   855
         End
      End
      Begin MSComctlLib.ListView LvMeiosDet 
         Height          =   3255
         Left            =   3120
         TabIndex        =   3
         ToolTipText     =   "Actividades Associadas a Meios"
         Top             =   600
         Width           =   11655
         _ExtentX        =   20558
         _ExtentY        =   5741
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   0
      End
      Begin MSComctlLib.TreeView TvReq 
         Height          =   7095
         Left            =   120
         TabIndex        =   2
         ToolTipText     =   "Actividades Associadas a Exames"
         Top             =   600
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   12515
         _Version        =   393217
         Style           =   7
         SingleSel       =   -1  'True
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Image Image1 
         Height          =   360
         Left            =   14280
         Picture         =   "FormDiario.frx":2B86
         ToolTipText     =   "Adinionar Nova Actividade"
         Top             =   120
         Width           =   360
      End
      Begin VB.Image BtImprimeEtiqMeio 
         Height          =   480
         Left            =   3120
         Picture         =   "FormDiario.frx":32F0
         ToolTipText     =   "Imprimir Etiquetas Meios"
         Top             =   120
         Width           =   480
      End
      Begin VB.Image BtAcrescentaMeio 
         Height          =   480
         Left            =   2520
         Picture         =   "FormDiario.frx":3FBA
         ToolTipText     =   "Adinionar Nova Actividade"
         Top             =   120
         Width           =   480
      End
   End
End
Attribute VB_Name = "FormDiario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim ReqActiva As Long
Dim seqUtenteActivo As String
Dim CodAgrupActivo As String

Dim iDiario As Integer
Dim iMeio As Integer
Dim iActividade As Integer
Dim indiceTreeView As Integer

Dim estrutDiario() As tDiario
Dim totalDiario As Integer


Private Sub BtAcrescentaMeio_Click()
    Dim ChavesPesq(2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean
    Dim i As Integer
    Dim TotalElementosSel As Integer
    On Error GoTo TrataErro
    If totalDiario >= 1 Then
        If iDiario = mediComboValorNull Or iDiario <> 1 Then Exit Sub
    
        PesqRapida = False
        ChavesPesq(2) = "cod_meio"
        ChavesPesq(1) = "descr_meio"
        
        CamposEcran(1) = "cod_meio"
        Tamanhos(1) = 1000
        Headers(1) = "C�digo"
        
        CamposEcran(2) = "descr_meio"
        Tamanhos(2) = 3500
        Headers(2) = "Descri��o"
        
        CFrom = " sl_cod_meio "
        CWhere = "  flg_invisivel = 0 or flg_invisivel IS NULL "
        CampoPesquisa = "descr_meio"
        
        CamposRetorno.InicializaResultados 1
        PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                               ChavesPesq, _
                                                                               CamposEcran, _
                                                                               CamposRetorno, _
                                                                               Tamanhos, _
                                                                               Headers, _
                                                                               CWhere, _
                                                                               CFrom, _
                                                                               "", _
                                                                               CampoPesquisa, _
                                                                               " ORDER BY descr_meio ", _
                                                                               " Actividades")
        
        If PesqRapida = True Then
            FormPesqRapidaAvancadaMultiSel.Show vbModal
            CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
            If Not CancelouPesquisa Then
                For i = 1 To TotalElementosSel
                    If iMeio > mediComboValorNull Then
                        AdicionaMeio CLng(Resultados(i)), estrutDiario(iDiario).meios(iMeio).seq_diario_meio
                    Else
                        AdicionaMeio CLng(Resultados(i)), mediComboValorNull
                    End If
                Next i
            End If
        Else
            BG_Mensagem mediMsgBox, "N�o existem actividades codificadas.", vbExclamation, "ATEN��O"
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtAcrescentaMeio_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtAcrescentaMeio_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtAcrescentaMeio_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    BtAcrescentaMeio.BorderStyle = 1
End Sub

Private Sub BtAcrescentaMeio_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    BtAcrescentaMeio.BorderStyle = 0
End Sub

Private Sub BtAdicionarDetalhe_Click()
    On Error GoTo TrataErro
    
    If iDiario <= mediComboValorNull Or iMeio <= mediComboValorNull Then
        BG_Mensagem mediMsgBox, "Precisa de seleccionar uma actividade", vbExclamation, "ATEN��O"
        Exit Sub
    End If
    
    If EcCodActividade = "" Then
        BG_Mensagem mediMsgBox, "Tipo de Detalhe n�o preenchido.", vbExclamation, "ATEN��O"
        Exit Sub
    End If
    
    If CbEstadoMeio.ListIndex = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "Estado n�o preenchido", vbExclamation, "ATEN��O"
        Exit Sub
    End If
    
    
    DIA_NovoDiarioMeioDetalhe estrutDiario, totalDiario, iDiario, iMeio, EcCodActividade, CbEstadoMeio.ItemData(CbEstadoMeio.ListIndex), _
                              EcDescrActividade, CbEstadoMeio.text, mediComboValorNull, EcDescricao.text, "", _
                              mediComboValorNull, "", mediComboValorNull
    'PreencheLvMeiosDet
    PreencheTvReq
    TvReq.Nodes.Item(estrutDiario(iDiario).meios(iMeio).indiceTV).Selected = True
    TvReq_Click
    EcCodActividade = ""
    EcDescrActividade = ""
    CbEstadoMeio.ListIndex = mediComboValorNull
    EcDescricao.text = ""
    EcListaFrases.Clear
    gTotalFrases = 0
    ReDim gFrasesDiario(0)
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtAdicionarDetalhe_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtAdicionarDetalhe_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtAdicionarDetalhe_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    BtAdicionarDetalhe.BorderStyle = 1
End Sub

Private Sub BtAdicionarDetalhe_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    BtAdicionarDetalhe.BorderStyle = 0
End Sub

Private Sub BtGuardarDetalhe_Click()
' nada
End Sub




' ------------------------------------------------------------------------------------------------------

' IMPRIME DETERMINADA ETIQUETA SECUNDARIA

' ------------------------------------------------------------------------------------------------------

Private Sub BtImprimeEtiqMeio_Click()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim N_req_bar As String
    Dim idade As String
    
    
    N_req_bar = Right("0000000" & estrutDiario(iDiario).n_req, 7)
    
    
    ' ---------------------------------
    If BL_HandleNull(estrutDiario(iDiario).dt_nasc_ute, "") <> "" Then
        idade = CStr(BG_CalculaIdade(CDate(estrutDiario(iDiario).dt_nasc_ute), Bg_DaData_ADO))
    End If
        
    If Not BL_LerEtiqIni("etiq_sec.ini") Then
        MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
        Exit Sub
    End If
    If Not BL_EtiqOpenPrinter(EcPrinterEtiq) Then
        MsgBox "Imposs�vel abrir impressora etiquetas"
        Exit Sub
    End If
        
    Call BL_EtiqSecPrint(estrutDiario(iDiario).t_utente, estrutDiario(iDiario).Utente, estrutDiario(iDiario).n_req, _
                   estrutDiario(iDiario).nome_ute, N_req_bar, estrutDiario(iDiario).meios(iMeio).descr_meio, 1, "", "", "", "", "", "", "")
    BL_EtiqClosePrinter
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao imprimir meios: " & Err.Number & " - " & Err.Description, Me.Name, "BtImprimeEtiqMeio_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaFrase_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean
    Dim iRec As Long
    Dim encontrou As Boolean
    Dim j As Integer
    Dim i As Long
    Dim TotalElementosSel As Integer
    Dim indice As Long
    If iDiario = mediComboValorNull Or iMeio = mediComboValorNull Then
        Exit Sub
    End If
    PesqRapida = False
    
    ChavesPesq(1) = "sl_dicionario.cod_frase"
    CamposEcran(1) = "sl_dicionario.cod_Frase"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "Descr_frase"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "sl_ass_meio_frase, sl_dicionario "
    CampoPesquisa = "descr_frase"
    CWhere = " sl_ass_meio_frase.cod_frase = sl_dicionario.cod_frase "
    
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                        ChavesPesq, _
                                                                        CamposEcran, _
                                                                        CamposRetorno, _
                                                                        Tamanhos, _
                                                                        Headers, _
                                                                        CWhere, _
                                                                        CFrom, _
                                                                        "", _
                                                                        CampoPesquisa, _
                                                                        " ORDER BY descr_frase ", _
                                                                        " Frases Associadas")
    
    If PesqRapida = True Then

        FormPesqRapidaAvancadaMultiSel.Show vbModal
        
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
        If Not CancelouPesquisa Then
            For i = 1 To TotalElementosSel
                encontrou = False
                For j = 1 To gTotalFrases
                    If gFrasesDiario(j).cod_frase = Resultados(i) Then
                        encontrou = True
                        Exit For
                    End If
                Next
                If encontrou = False Then
                    gTotalFrases = gTotalFrases + 1
                    ReDim Preserve gFrasesDiario(gTotalFrases)
                    gFrasesDiario(gTotalFrases).cod_frase = Resultados(i)
                    gFrasesDiario(gTotalFrases).descr_frase = BL_SelCodigo("SL_DICIONARIO", "DESCR_FRASE", "COD_FRASE", Resultados(i))
                    gFrasesDiario(gTotalFrases).ordem = gTotalFrases
                    EcListaFrases.AddItem gFrasesDiario(gTotalFrases).descr_frase
                End If
            Next i
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem frases associadas", vbExclamation, "ATEN��O"
    End If
End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Di�rio - Estudo de Exames"
    Me.left = 50
    Me.top = 50
    
    'Me.Width = 15360
    Me.Width = 15315
    Me.Height = 9410
'    Me.Height = 8880 ' Normal
    FrameGeral.left = 0
    'Me.Height = 8330 ' Campos Extras
    
    Set CampoDeFocus = TvReq
    InicializaLvMeiosDet
    EcPrinterEtiq = BL_SelImpressora("Etiqueta.rpt")
End Sub
Private Sub EventoActivate()
    On Error GoTo TrataErro
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN 0
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"

    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
Exit Sub
TrataErro:
    BG_LogFile_Erros "EventoActivate: " & Err.Number & " - " & Err.Description, Me.Name, "EventoActivate"
    Exit Sub
    Resume Next

End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    On Error GoTo TrataErro
    
    BL_InicioProcessamento Me, "Inicializar �cran..."
    Set gFormActivo = Me
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    Set CampoActivo = Me.ActiveControl

    BG_ParametrizaPermissoes_ADO Me.Name

    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
Exit Sub
TrataErro:
    BG_LogFile_Erros "EventoLoad: " & Err.Number & " - " & Err.Description, Me.Name, "EventoLoad"
    Exit Sub
    Resume Next
End Sub


Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormDiario = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    EcCabecalho.LimpaCampos &HFFE0C7
    totalDiario = 0
    ReDim estrutDiario(0)
    TvReq.Nodes.Clear
    indiceTreeView = 0
    iDiario = mediComboValorNull
    iMeio = mediComboValorNull
    EcCodActividade = ""
    EcDescrActividade = ""
    CbEstadoMeio.ListIndex = mediComboValorNull
    EcDescricao.text = ""
    EcListaFrases.Clear
    gTotalFrases = 0
    ReDim gFrasesDiario(0)
End Sub

Sub DefTipoCampos()
    If gPermResUtil = gPermResUtil = cValTecRes Then
        BG_PreencheComboBD_ADO "SELECT cod_estado, descr_estado FROM sl_tbf_estado_meio WHERE flg_visivel_tec = 1", "cod_estado", "descr_estado", CbEstadoMeio
    Else
        BG_PreencheComboBD_ADO "SELECT cod_estado, descr_estado FROM sl_tbf_estado_meio WHERE flg_visivel_med = 1", "cod_estado", "descr_estado", CbEstadoMeio
    End If
    
    EcCabecalho.LimpaCampos &H8000000F
    EcCabecalho.Width = Me.Width - 200
    indiceTreeView = 0
    TvReq.ImageList = ImageList1
    TvReq.Scroll = False
    TvReq.LineStyle = tvwRootLines
    TvReq.LabelEdit = tvwAutomatic
    
End Sub

Sub PreencheValoresDefeito()
    '
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    FuncaoLimpar
    
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
'nada

End Sub

Function ValidaCamposEc() As Integer
    'nada
End Function

Sub FuncaoProcurar()
    totalDiario = 0
    ReDim estrutDiario(0)
    LimpaCampos
    DIA_PreencheDiario estrutDiario, totalDiario, ReqActiva, CodAgrupActivo
    If totalDiario >= 1 Then
        EcCabecalho.ActualizaDados CStr(estrutDiario(1).n_req), estrutDiario(1).Utente, estrutDiario(1).t_utente, _
                                   estrutDiario(1).nome_ute, estrutDiario(1).estado_req, estrutDiario(1).n_epis, _
                                   estrutDiario(1).t_sit, estrutDiario(1).descr_proven, "", estrutDiario(1).dt_chega, _
                                   estrutDiario(1).hr_chega, estrutDiario(1).dt_nasc_ute, CStr(estrutDiario(1).sexo_ute), _
                                   0, 0, estrutDiario(1).seq_utente, "", "", 0, 0, 0
        PreencheTvReq
    Else
        gMsgTitulo = "Novo Di�rio"
        gMsgMsg = "A an�lise em causa n�o tem di�rio associado. Deseja criar um novo associado � an�lise " & BL_SelCodigo("SLV_ANALISES", "DESCR_ANA", "COD_ANA", CodAgrupActivo) & "?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            DIA_RegistaDiario CStr(seqUtenteActivo), CStr(ReqActiva), CodAgrupActivo, True
            FuncaoProcurar
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar: " & Err.Description, Me.Name, "FuncaoProcurar"
    Exit Sub
End Sub

Sub FuncaoAnterior()
    
'nada
End Sub

Sub FuncaoSeguinte()
    'nada

End Sub

Sub FuncaoInserir()
    'nada
End Sub

Sub FuncaoModificar()
    'nada
End Sub

Sub FuncaoRemover()
    'nada
End Sub


Private Function RetornaIndiceDiario(seqDiario As Long) As Integer
    Dim i As Integer
    For i = 1 To totalDiario
        If estrutDiario(i).seq_diario = seqDiario Then
            RetornaIndiceDiario = i
            Exit Function
        End If
    Next
    RetornaIndiceDiario = mediComboValorNull
End Function

Public Sub InicializaDiario(n_req As Long, cod_agrup As String, seq_utente As Long)
    ReqActiva = n_req
    CodAgrupActivo = cod_agrup
    seqUtenteActivo = seq_utente
    FuncaoProcurar
End Sub

Private Sub InicializaLvMeiosDet()
    With LvMeiosDet
        .SmallIcons = ImageList1
        .ColumnHeaderIcons = ImageList1
        .ColumnHeaders.Add , "SEQ_MEIO_DET", "ID", 500, ListColumnAlignmentConstants.lvwColumnLeft
        .ColumnHeaders.Add , "DESCR_ACTIVIDADE", "Actividade", 1700, ListColumnAlignmentConstants.lvwColumnLeft
        .ColumnHeaders.Add , "DESCR_ESTADO", "Estado", 1700, ListColumnAlignmentConstants.lvwColumnLeft
        .ColumnHeaders.Add , "DESCRICAO", "Descri��o", 3500, ListColumnAlignmentConstants.lvwColumnLeft
        .ColumnHeaders.Add , "DT_CRI", "Data", 2000, ListColumnAlignmentConstants.lvwColumnLeft
        .ColumnHeaders.Add , "USER_CRI", "Utilizador", 2000, ListColumnAlignmentConstants.lvwColumnLeft
        .View = lvwReport
        .BorderStyle = ccFixedSingle
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .FullRowSelect = True
        .ForeColor = &H808080
        .GridLines = False
    End With
End Sub

Private Sub TvReq_Click()
    Dim i As Integer
    On Error GoTo TrataErro
    
    LvMeiosDet.ListItems.Clear
    iDiario = mediComboValorNull
    iMeio = mediComboValorNull
    iActividade = mediComboValorNull
    If Mid(TvReq.SelectedItem.Key, 1, 1) = "D" Then
        For i = 1 To totalDiario
            If Mid(TvReq.SelectedItem.Key, 2) = estrutDiario(i).seq_diario Then
                iDiario = i
                Exit For
            End If
        Next
    ElseIf Mid(TvReq.SelectedItem.Key, 1, 1) = "M" Then
        iDiario = DevolveIndiceDiario(TvReq.SelectedItem.Index)
        If iDiario > mediComboValorNull Then
            For i = 1 To estrutDiario(iDiario).totalMeios
                If Mid(TvReq.SelectedItem.Key, 2) = estrutDiario(iDiario).meios(i).seq_diario_meio Then
                    iMeio = i
                    Exit For
                End If
            Next
        End If
        PreencheLvMeiosDet
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "TvReq_Click: " & Err.Description, Me.Name, "TvReq_Click", True
    iDiario = mediComboValorNull
    iActividade = mediComboValorNull
    iMeio = mediComboValorNull
    Exit Sub
    Resume Next
End Sub
Private Sub PreencheLvMeiosDet()
    Dim k As Integer
    On Error GoTo TrataErro
    LvMeiosDet.ListItems.Clear
    If iDiario > mediComboValorNull And iMeio > mediComboValorNull Then
        For k = 1 To estrutDiario(iDiario).meios(iMeio).totalDetalhe
            With LvMeiosDet.ListItems.Add(, "A" & estrutDiario(iDiario).meios(iMeio).detalhe(k).seq_diario_meio_Det, estrutDiario(iDiario).meios(iMeio).detalhe(k).seq_diario_meio_Det)
                .ListSubItems.Add , , BL_HandleNull(estrutDiario(iDiario).meios(iMeio).detalhe(k).descR_actividade, Empty)
                .ListSubItems.Add , , BL_HandleNull(estrutDiario(iDiario).meios(iMeio).detalhe(k).descr_estado, Empty)
                .ListSubItems.Add , , BL_HandleNull(estrutDiario(iDiario).meios(iMeio).detalhe(k).descricao, Empty)
                .ListSubItems.Add , , BL_HandleNull(estrutDiario(iDiario).meios(iMeio).detalhe(k).dt_cri, Empty)
                .ListSubItems.Add , , BL_HandleNull(estrutDiario(iDiario).meios(iMeio).detalhe(k).nome_cri, Empty)
            End With
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheLvMeiosDet: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheLvMeiosDet"
    Exit Sub
    Resume Next
End Sub

Private Sub AdicionaMeio(ByVal cod_meio As Long, ByVal seq_diario_meio_pai As Long)
    Dim flgExiste As Boolean
    Dim i As Integer
    On Error GoTo TrataErro
    
    If totalDiario >= 1 Then
        flgExiste = False
        For i = 1 To estrutDiario(1).totalMeios
            If estrutDiario(1).meios(i).cod_meio = cod_meio Then
                gMsgTitulo = "Inserir Actividade"
                gMsgMsg = "Actividade j� existente. Pretende introduzir novamente"
                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                If gMsgResp = vbNo Then
                    flgExiste = True
                End If
                Exit For
            End If
        Next
        
        If flgExiste = True Then Exit Sub
        DoEvents
        DIA_AdicionaEstrutMeio estrutDiario, totalDiario, 1, mediComboValorNull, cod_meio, "", "", 0, mediComboValorNull, "", "", "", "", 0, seq_diario_meio_pai
        PreencheTvReq
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "AdicionaMeio: " & Err.Number & " - " & Err.Description, Me.Name, "AdicionaMeio"
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheTvReq()
    Dim i As Integer
    Dim k As Integer
    Dim j As Integer
    Dim iconMeio As String
    Dim indicePai As Integer
    On Error GoTo TrataErro
    
    TvReq.Nodes.Clear
    LvMeiosDet.ListItems.Clear
    indiceTreeView = 0
    For i = 1 To totalDiario
        Dim root As Node
        indiceTreeView = indiceTreeView + 1
        
        estrutDiario(i).indiceTV = indiceTreeView
        If i = 1 Then
            Set root = TvReq.Nodes.Add(, tvwChild, "D" & estrutDiario(i).seq_diario, estrutDiario(i).n_req & "-" & estrutDiario(i).descr_ana, "TASK")
            root.Expanded = True
            indiceTreeView = indiceTreeView + 1
            Set root = TvReq.Nodes.Add(, tvwChild, "HIST", "Hist�rico", "HIST")
            root.Expanded = False
        Else
            Set root = TvReq.Nodes.Add("HIST", tvwChild, "D" & estrutDiario(i).seq_diario, estrutDiario(i).n_req & "-" & estrutDiario(i).descr_ana, "TASKG")
        End If
        For j = 1 To estrutDiario(i).totalMeios
            indiceTreeView = indiceTreeView + 1
            estrutDiario(i).meios(j).indiceTV = indiceTreeView
            If estrutDiario(i).meios(j).seq_diario_medio_pai > mediComboValorNull Then
                For k = 1 To estrutDiario(i).totalMeios
                    If estrutDiario(i).meios(k).seq_diario_meio = estrutDiario(i).meios(j).seq_diario_medio_pai Then
                        indicePai = estrutDiario(i).meios(k).indiceTV
                        Exit For
                    End If
                Next k
            Else
                indicePai = estrutDiario(i).indiceTV
            End If
            
            If estrutDiario(i).meios(j).flg_estado_final = mediSim And gPermResUtil = cValMedRes Then
                iconMeio = "VERDE"
            ElseIf estrutDiario(i).meios(j).flg_estado_final_tec = mediSim And gPermResUtil = cValTecRes Then
                iconMeio = "VERDE"
            Else
                iconMeio = "VERMELHO"
            End If
            Set root = TvReq.Nodes.Add(indicePai, tvwChild, "M" & estrutDiario(i).meios(j).seq_diario_meio, estrutDiario(i).meios(j).descr_meio, iconMeio)
            root.Expanded = True
        Next
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheTvReq: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheTvReq"
    Exit Sub
    Resume Next
End Sub

Public Sub EcCodactividade_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodActividade.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_actividade FROM sl_cod_actividade WHERE cod_actividade=" & Trim(EcCodActividade.text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodActividade.text = ""
            EcDescrActividade.text = ""
        Else
            EcDescrActividade.text = Tabela!descR_actividade
        End If
       
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrActividade.text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodactividade_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodactividade_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaActividade_Click()
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False
    
    ChavesPesq(1) = "descR_actividade"
    CamposEcran(1) = "descR_actividade"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_actividade"
    CamposEcran(2) = "cod_actividade"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_actividade"
    CampoPesquisa = "descR_actividade"
    'CWhere = " (flg_invisivel IS NULL or flg_invisivel = 0) AND cod_sala IN (SELECT x.cod_sala FROM sl_ass_salas_locais x WHERE x.cod_local = " & BL_HandleNull(EcLocal, CStr(gCodLocal)) & ") "
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descR_actividade ", " Detalhe de Actividade")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodActividade.text = Resultados(2)
            EcDescrActividade.text = Resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Detalhes", vbExclamation, "Detalhes de Actividades"
        EcCodActividade.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaActividade_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaActividade_Click"
    Exit Sub
    Resume Next
End Sub
Private Sub EcListaFrases_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    If KeyCode = 46 And EcListaFrases.ListCount > 0 Then     'Delete
        If EcListaFrases.ListIndex > mediComboValorNull Then
            For i = EcListaFrases.ListIndex + 1 To gTotalFrases - 1
                gFrasesDiario(i) = gFrasesDiario(i + 1)
            Next i
            gTotalFrases = gTotalFrases - 1
            ReDim Preserve gFrasesDiario(gTotalFrases)
            EcListaFrases.RemoveItem (EcListaFrases.ListIndex)
        End If
    End If
End Sub


Private Function DevolveIndiceDiario(indice As Integer) As Integer
    Dim retorno As Integer
    Dim i As Integer
    If Mid(TvReq.Nodes.Item(indice).parent.Key, 1, 1) = "M" Then
        retorno = DevolveIndiceDiario(TvReq.Nodes.Item(indice).parent.Index)
    ElseIf Mid(TvReq.Nodes.Item(indice).parent.Key, 1, 1) = "D" Then
        For i = 1 To totalDiario
            If Mid(TvReq.Nodes.Item(indice).parent.Key, 2) = estrutDiario(i).seq_diario Then
                retorno = i
                Exit For
            End If
        Next
    End If
    DevolveIndiceDiario = retorno
End Function

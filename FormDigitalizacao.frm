VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FormDigitalizacao 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "picHidden"
   ClientHeight    =   9525
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13770
   Icon            =   "FormDigitalizacao.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9525
   ScaleWidth      =   13770
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameDigit 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   1695
      Left            =   11040
      TabIndex        =   16
      Top             =   480
      Width           =   2655
      Begin VB.ComboBox CbQual 
         Height          =   315
         Left            =   720
         TabIndex        =   21
         Text            =   "Combo1"
         Top             =   600
         Width           =   1815
      End
      Begin VB.CommandButton btCancel 
         Height          =   375
         Left            =   1560
         Picture         =   "FormDigitalizacao.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "Voltar"
         Top             =   1200
         Width           =   495
      End
      Begin VB.CommandButton BtOk 
         Height          =   375
         Left            =   480
         Picture         =   "FormDigitalizacao.frx":0396
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   1200
         Width           =   495
      End
      Begin VB.ComboBox CbGrDigital 
         Height          =   315
         Left            =   600
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   120
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Qualid."
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   22
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   18
         Top             =   120
         Width           =   855
      End
   End
   Begin VB.PictureBox PicAux 
      Height          =   375
      Left            =   1800
      ScaleHeight     =   315
      ScaleWidth      =   915
      TabIndex        =   15
      Top             =   9840
      Width           =   975
   End
   Begin VB.CommandButton BtZoomOut 
      Height          =   495
      Left            =   12720
      Picture         =   "FormDigitalizacao.frx":0720
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   0
      Width           =   495
   End
   Begin VB.CommandButton BtZommIn 
      Height          =   495
      Left            =   12120
      Picture         =   "FormDigitalizacao.frx":13EA
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   0
      Width           =   495
   End
   Begin VB.TextBox EcSeqUtente 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Height          =   285
      Left            =   120
      TabIndex        =   12
      Top             =   9960
      Width           =   1095
   End
   Begin VB.TextBox EcNome 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Height          =   285
      Left            =   5880
      Locked          =   -1  'True
      TabIndex        =   10
      Top             =   120
      Width           =   5175
   End
   Begin VB.CommandButton BtDigitalizacao 
      Height          =   480
      Left            =   11160
      Picture         =   "FormDigitalizacao.frx":20B4
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Digitalizar Documento"
      Top             =   0
      Width           =   495
   End
   Begin VB.PictureBox Picholder 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   8535
      Left            =   2880
      ScaleHeight     =   8505
      ScaleWidth      =   10665
      TabIndex        =   7
      Top             =   480
      Width           =   10695
      Begin VB.PictureBox pic 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         Height          =   8535
         Left            =   0
         ScaleHeight     =   8535
         ScaleWidth      =   10575
         TabIndex        =   8
         Top             =   0
         Width           =   10575
      End
   End
   Begin VB.HScrollBar HScroll1 
      Height          =   255
      Left            =   2640
      TabIndex        =   6
      Top             =   9120
      Width           =   10575
   End
   Begin VB.VScrollBar VScroll1 
      Height          =   5415
      Left            =   13560
      TabIndex        =   5
      Top             =   480
      Width           =   255
   End
   Begin VB.PictureBox picHidden 
      AutoRedraw      =   -1  'True
      Height          =   732
      Left            =   0
      ScaleHeight     =   675
      ScaleWidth      =   915
      TabIndex        =   4
      Top             =   10320
      Width           =   972
   End
   Begin VB.PictureBox Zoom_Ori 
      Height          =   375
      Left            =   0
      ScaleHeight     =   315
      ScaleWidth      =   675
      TabIndex        =   3
      Top             =   10320
      Visible         =   0   'False
      Width           =   735
   End
   Begin MSComctlLib.ImageList ImageListDoc 
      Left            =   4080
      Top             =   2040
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDigitalizacao.frx":281E
            Key             =   "Grupo"
            Object.Tag             =   "Grupo"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormDigitalizacao.frx":9080
            Key             =   "Elemento"
            Object.Tag             =   "Elemento"
         EndProperty
      EndProperty
   End
   Begin VB.TextBox EcNumReq 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Height          =   285
      Left            =   3960
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   120
      Width           =   1095
   End
   Begin MSComctlLib.TreeView TreeViewDoc 
      Height          =   9015
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   15901
      _Version        =   393217
      Style           =   7
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin VB.Label Label1 
      Caption         =   "Nome"
      Height          =   255
      Index           =   1
      Left            =   5400
      TabIndex        =   11
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Requisi��o"
      Height          =   255
      Index           =   0
      Left            =   2880
      TabIndex        =   2
      Top             =   120
      Width           =   855
   End
End
Attribute VB_Name = "FormDigitalizacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type documento
    cod_gr_digital As Integer
    descr_gr_digital As String
    seq_digital As Long
    seq_utente As Long
    nome_ute As String
    n_req As Long
    cod_agrup As String
    nome_documento As String
    dt_cri As String
    user_cri As String
    hr_cri As String
End Type
Dim estrutDoc() As documento
Dim totalDoc As Integer

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim Estado As Integer

Private Type RECT
    left As Long
    top As Long
    Width As Long
    Height As Long
End Type

Private Sup As Boolean, Sdn As Boolean, Slt As Boolean, Srt As Boolean
Private startX%, startY%, OldX%, OldY%, NowX%, NowY As Integer
Private OffSetX%, OffSetY As Integer
Private Selection As Boolean, SelMove As Boolean
Private SelResize As Boolean
Private SelRect As RECT
Private SelRect_S As Boolean
Const SelDrawType = vbDot ' other options :vbdashdot , vbDashDotdot
Private bX1 As Boolean, bX2 As Boolean
Private bY1 As Boolean, bY2 As Boolean
Private Ori_Pos As RECT, Tmp_Pos As RECT, Tmp_Sel As RECT, Ori_Sel As RECT

Private Const S_Timer = 20 ' alter this value to speed up and slow down the auto scroll..
Private Const S_Steps = 3 ' alter this value to speed up and slow down the auto scroll..
Private Const S_Check = S_Steps - 1
Dim zoom As Double
Private Sub DefTipoCampos()
    ' set the original image
    With Zoom_Ori
        .AutoRedraw = True
        .ScaleMode = vbPixels
        .Visible = False
        .Width = Pic.Width
        .Height = Pic.Height
        '.PaintPicture pic.Picture, 0, 0
        .Picture = .Image
    End With
    
    HScroll1.Visible = True
    VScroll1.Visible = True
    
    
    'hidden picbox
    With picHidden
        .AutoRedraw = True
        .ScaleMode = vbPixels
        .Visible = False
        .Width = Pic.Width
        .Height = Pic.Height
    End With
    
    'Additional Controls
    VScroll1.left = Picholder.left + Picholder.Width
    VScroll1.Height = Picholder.Height
    HScroll1.top = Picholder.top + Picholder.Height
    HScroll1.Width = Picholder.Width
    SelRect_S = False
    HScroll1_Change
    VScroll1_Change
    

  TreeViewDoc.LineStyle = tvwRootLines
  TreeViewDoc.ImageList = ImageListDoc
  FrameDigit.Visible = False
  
End Sub

Public Sub FuncaoLimpar()

    LimpaCampos

End Sub
Public Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsDig As New ADODB.recordset
    TreeViewDoc.Nodes.Clear
    If EcSeqUtente <> "" And EcNumReq <> "" Then
        sSql = "SELECT x1.seq_digital, x1.seq_utente, x1.n_req, x1.cod_agrup, x1.nome_documento, x1.dt_cri, "
        sSql = sSql & "x1.user_cri, x1.hr_Cri, x1.cod_gr_digital, x1.tamanho_ficheiro, x2.descr_gr_digital "
        sSql = sSql & " FROM sl_digital x1, sl_tbf_gr_digital x2 WHERE seQ_utente = " & EcSeqUtente
        sSql = sSql & " AND n_req = " & EcNumReq & " AND x1.cod_gr_digital = x2.cod_gr_digital "
        sSql = sSql & " ORDER by cod_gr_digital asc , dt_cri asc"
    Else
        Exit Sub
    End If
    rsDig.CursorType = adOpenStatic
    rsDig.LockType = adLockOptimistic
    rsDig.Open sSql, gConexao
    If rsDig.RecordCount > 0 Then
        Picholder.Visible = True
        totalDoc = 0
        ReDim estrutDoc(0)
        rsDig.MoveFirst
        PreencheEstrutura rsDig
        preencheGruposDigital
        preencheDocumentos
    End If
    rsDig.Close
    Set rsDig = Nothing
End Sub




Private Sub btCancel_Click()
    FrameDigit.Visible = False
End Sub

Private Sub BtOk_Click()
    Digitaliza
    FrameDigit.Visible = False
End Sub

Private Sub BtZommIn_Click()
If PicAux.Picture = Empty Then Exit Sub
zoom = zoom + 0.2
Set Pic.Picture = Nothing
Pic.PaintPicture PicAux.Picture, 0, 0, Pic.ScaleWidth * zoom, Pic.ScaleHeight * zoom
End Sub

Private Sub BtZoomOut_Click()
If PicAux.Picture = Empty Then Exit Sub
zoom = zoom - 0.2
Set Pic.Picture = Nothing
Pic.PaintPicture PicAux.Picture, 0, 0, Pic.ScaleWidth * zoom, Pic.ScaleHeight * zoom
End Sub


Private Sub Form_Activate()

    EventoActivate

End Sub

Private Sub Form_Deactivate()

    Call BL_FimProcessamento(Screen.ActiveForm, "")

End Sub

Private Sub Form_Load()

    EventoLoad

End Sub

Private Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN 0
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"

    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Private Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Set gFormActivo = Me
    Inicializacoes
    PreencheValoresDefeito
    DefTipoCampos
    Set CampoActivo = Me.ActiveControl

    BG_ParametrizaPermissoes_ADO Me.Name

    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Private Sub Inicializacoes()

    Me.caption = " Documentos Digitalizados"
    Me.left = 5
    Me.top = 5
    Me.Width = 13960
    Me.Height = 9855 ' Normal
    'Me.Height = 8330 ' Campos Extras

    Set CampoDeFocus = EcNumReq

End Sub

Private Sub LimpaCampos()

    Me.SetFocus
    EcSeqUtente = ""
    EcNumReq = ""
    Picholder.Visible = False
    FrameDigit.Visible = False
End Sub

Private Sub Form_Unload(Cancel As Integer)

    BG_StackJanelas_Pop

    Set gFormActivo = FormGestaoRequisicaoPrivado
    FormGestaoRequisicaoPrivado.Enabled = True
    BL_ToolbarEstadoN 0

    Set FormDigitalizacao = Nothing

End Sub

Private Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_tbf_gr_digital", "cod_gr_digital", "descr_gr_digital", CbGrDigital
    CbQual.AddItem "50"
    CbQual.AddItem "75"
    CbQual.AddItem "100"
    CbQual.AddItem "125"
    CbQual.AddItem "150"
    CbQual.AddItem "175"
    CbQual.AddItem "200"
    CbQual.ListIndex = 2
'nada
End Sub

Private Sub PreencheEstrutura(rsDig As ADODB.recordset)
    Dim sSql As String

    While Not rsDig.EOF
        totalDoc = totalDoc + 1
        ReDim Preserve estrutDoc(totalDoc)
        estrutDoc(totalDoc).cod_agrup = BL_HandleNull(rsDig!cod_agrup, "")
        estrutDoc(totalDoc).cod_gr_digital = BL_HandleNull(rsDig!cod_gr_digital, -1)
        estrutDoc(totalDoc).descr_gr_digital = BL_HandleNull(rsDig!descr_gr_digital, "")
        estrutDoc(totalDoc).dt_cri = BL_HandleNull(rsDig!dt_cri, "")
        estrutDoc(totalDoc).hr_cri = BL_HandleNull(rsDig!hr_cri, "")
        estrutDoc(totalDoc).n_req = BL_HandleNull(rsDig!n_req, -1)
        estrutDoc(totalDoc).nome_documento = BL_HandleNull(rsDig!nome_documento, "")
        estrutDoc(totalDoc).seq_digital = BL_HandleNull(rsDig!seq_digital, -1)
        estrutDoc(totalDoc).seq_utente = BL_HandleNull(rsDig!seq_utente, -1)
        estrutDoc(totalDoc).user_cri = BL_HandleNull(rsDig!user_cri, "")
        
        'sSql = "SELECT documento FROM sl_digital WHERE seq_digital = " & estrutDoc(totalDoc).seq_digital
        'IM_RetiraImagemBD2 gDirCliente & "\digital.bmp", sSql, estrutDoc(totalDoc).Pic, 500000
        'Kill gDirCliente & "\digital.bmp"
        rsDig.MoveNext
    Wend
End Sub


Private Sub preencheGruposDigital()
    Dim ultimo As Integer
    Dim i As Integer
    ultimo = -1
    For i = 1 To totalDoc
        If ultimo <> estrutDoc(i).cod_gr_digital Then
        Dim root As Node
        Set root = TreeViewDoc.Nodes.Add(, tvwChild, "KEY_" & (estrutDoc(i).cod_gr_digital), estrutDoc(i).descr_gr_digital, "Grupo")
        root.Expanded = False
        ultimo = estrutDoc(i).cod_gr_digital
        End If
    Next

End Sub
Private Sub preencheDocumentos()
    Dim i As Integer
    Dim root As Node
    For i = 1 To totalDoc
        Set root = TreeViewDoc.Nodes.Add(estrutDoc(i).cod_gr_digital, tvwChild, "KEYY_" & estrutDoc(i).cod_gr_digital & "_" & i, estrutDoc(i).seq_digital & " " & estrutDoc(i).dt_cri & " " & estrutDoc(i).hr_cri, "Elemento")
        root.Expanded = False
    Next

End Sub

Private Sub HScroll1_Change()
Pic.left = -HScroll1.Value
End Sub

Private Sub VScroll1_Change()
Pic.top = -VScroll1.Value
End Sub

Private Sub TreeViewDoc_Click()
    Dim sSql As String
    Dim aux As Integer
    Dim cod As String
    Set Pic = Nothing
    aux = InStr(1, CStr(TreeViewDoc.SelectedItem.Key), "KEYY_")
    If aux > 0 Then
        cod = Mid(TreeViewDoc.SelectedItem.Key, 8)
        sSql = "SELECT documento, tamanho_ficheiro FROM sl_digital WHERE seq_digital = " & estrutDoc(cod).seq_digital
        IM_RetiraImagemBD gDirCliente & "\digital.bmp", sSql, Pic
        Set PicAux.Picture = Pic.Picture
        With Pic
            .ScaleMode = vbPixels
            .AutoRedraw = True
            .AutoSize = True
            .DrawMode = vbInvert
            .DrawStyle = SelDrawType
            .BackColor = vbWhite
            ScrollAdjust
            Ori_Pos.top = .top
            Ori_Pos.left = .left
            Ori_Pos.Height = .Height
            Ori_Pos.Width = .Width
            zoom = 1
        End With
    End If
End Sub

Private Sub ScrollAdjust()
    With Pic
        '.Left = 0
        HScroll1.Min = 0
        HScroll1.Max = .Width - Picholder.Width
        HScroll1.SmallChange = 1
        HScroll1.LargeChange = 10
        'HScroll1.Value = 0
        HScroll1.Enabled = True
        '.Top = 0
        VScroll1.Min = 0
        VScroll1.Max = .Height - Picholder.Height
        VScroll1.SmallChange = 1
        VScroll1.LargeChange = 10
        'VScroll1.Value = 0
        VScroll1.Enabled = True
    End With
End Sub


Private Sub BtDigitalizacao_Click()
    FrameDigit.top = 480
    FrameDigit.left = 11040
    FrameDigit.Visible = True
End Sub
Private Sub Digitaliza()
Dim Res As String
Dim nomeFich As String
Dim nBarcodes As Long
Dim sBarcode As String
Dim i As Integer
Dim barLen As Integer
On Error GoTo TrataErro
    If EcNumReq <> "" Or CbGrDigital.ListIndex = mediComboValorNull Or CbQual.ListIndex = mediComboValorNull Then
        nomeFich = EcNumReq & "_" & Date & Replace(time, ":", "") & ".bmp"
        'nomeFich = "1474262_05-03-2011234748.bmp"
        Res = DG_TransferWithoutUI(CbQual.text, GREY, 0, 0, 0, 0, gDirCliente & "\" & nomeFich)
        If Res = 0 Then
            Set Pic.Picture = LoadPicture(gDirCliente & "\" & nomeFich)
            SetRegistrationCode ("HVFU5QQXAM9JYR738YL7")
            SetReadCode39 (1)
            SetReadCode128 (1)
            SetReadCode11 (1)
            
            SetReadCode39 (1)
            SetScanRight (1)
            SetScanUp (1)
            SetScanLeft (1)
            SetScanDown (1)
            nBarcodes = ReadBarcode(gDirCliente & "\" & nomeFich)
            For i = 0 To nBarcodes - 1
            
                ' determine the required size to store barcode string
                barLen = GetBarcodeString(i, sBarcode, 0)
                sBarcode = Space(barLen)
                Call GetBarcodeString(i, sBarcode, barLen)
                
                
                ' remove null-terminatiors
                sBarcode = Trim(Replace(sBarcode, Chr(0), ""))
                Res = Res & sBarcode & "|"
                sBarcode = ""
                DoEvents
            Next i
            
            
            DG_InsereBd EcSeqUtente, EcNumReq, Empty, nomeFich, Res, CbGrDigital.ItemData(CbGrDigital.ListIndex)
        End If
        Kill gDirCliente & "\" & nomeFich
    End If
    FuncaoProcurar
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Digitaliza: " & Err.Number & " - " & Err.Description, Me.Name, "Digitaliza"
    Exit Sub
    Resume Next
End Sub

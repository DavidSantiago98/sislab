VERSION 5.00
Begin VB.Form FormAgendaNovoEvento 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormAgendaNovoEvento"
   ClientHeight    =   3030
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   9825
   Icon            =   "FormAgendaNovoEvento.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3030
   ScaleWidth      =   9825
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkImprimirMarcacao 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Imprimir Marca��o"
      Height          =   255
      Left            =   1320
      TabIndex        =   13
      Top             =   2280
      Width           =   1815
   End
   Begin VB.TextBox EcObs 
      Appearance      =   0  'Flat
      Height          =   1005
      Left            =   1320
      TabIndex        =   11
      Top             =   1200
      Width           =   8175
   End
   Begin VB.TextBox EcDtFim 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1320
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   840
      Width           =   2775
   End
   Begin VB.CommandButton BtCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   8520
      TabIndex        =   8
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton BtConfirmar 
      Caption         =   "Confirmar"
      Height          =   375
      Left            =   6840
      TabIndex        =   7
      Top             =   2280
      Width           =   975
   End
   Begin VB.TextBox EcDtIni 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1320
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   480
      Width           =   2775
   End
   Begin VB.TextBox EcTitulo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1320
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   120
      Width           =   4695
   End
   Begin VB.ComboBox CbPrioridade 
      Height          =   315
      Left            =   7320
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   120
      Width           =   2175
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Observa��es"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   12
      Top             =   1200
      Width           =   1095
   End
   Begin VB.Label ctrlColor 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   9480
      TabIndex        =   10
      Top             =   120
      Width           =   255
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Fim"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   4
      Top             =   840
      Width           =   855
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Inicio"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Prioridade"
      Height          =   255
      Index           =   1
      Left            =   6480
      TabIndex        =   2
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Requisi��o"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   855
   End
End
Attribute VB_Name = "FormAgendaNovoEvento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim estado As Integer
Dim Evento As CalendarEvent
Dim CalAgenda As CalendarControl
Dim agenda As ClAgenda

Private Sub BtCancelar_Click()
    Unload Me

End Sub

Private Sub BtConfirmar_Click()
    If CbPrioridade.ListIndex <= mediComboValorNull Then
        BG_Mensagem mediMsgBox, "Ter� que indicar uma prioridade para a colheita", vbExclamation, "Prioridade"
        Exit Sub
    End If
    If Evento.ScheduleID <= 0 Then
        Evento.Body = EcObs.Text
        If agenda.AG_GravaEventoBD(Evento) = True Then
            CalAgenda.DataProvider.AddEvent Evento
            If gF_REQCONS = mediSim Then
                FormGesReqCons.EcDtMarcacao.Text = Format(Evento.StartTime, "dd-mm-yyyy")
            End If
            If CkImprimirMarcacao.Value = vbChecked Then
                ImprimeMarcacao
            End If
        End If
    Else
        CalAgenda.DataProvider.ChangeEvent Evento
    End If
    CalAgenda.Populate
    Unload Me
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Edi��o de Evento"
    Me.Width = 9915
    Me.Height = 3465 ' Normal
    Me.left = FormAgendaV2.Width / 2 - (Me.Width / 2)
    Me.top = FormAgendaV2.Height / 2 - (Me.Height / 2)
    
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    Set gFormActivo = FormAgendaV2
    FormAgendaV2.Enabled = True
    BL_ToolbarEstadoN 0

    Set FormAgendaNovoEvento = Nothing
End Sub



Sub DefTipoCampos()
End Sub

Sub PreencheCampos()
    

End Sub


Sub FuncaoLimpar()
    

End Sub

Sub FuncaoEstadoAnterior()

End Sub



Sub FuncaoProcurar()
    
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()


End Sub

Sub FuncaoModificar()


End Sub

Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub


Public Sub InicializaEvento(m_pEditingEvent As CalendarEvent, mCalAgenda As CalendarControl, mAgenda As ClAgenda)
    Dim i As Long
    Dim pLabel As CalendarEventLabel
    
    Set Evento = m_pEditingEvent
    Set CalAgenda = mCalAgenda
    Set agenda = mAgenda
    
    For Each pLabel In CalAgenda.DataProvider.LabelList
        CbPrioridade.AddItem pLabel.Name
        CbPrioridade.ItemData(CbPrioridade.NewIndex) = pLabel.LabelID
    Next
    
    EcTitulo.Text = m_pEditingEvent.Subject
    EcDtIni.Text = m_pEditingEvent.StartTime
    EcDtFim.Text = m_pEditingEvent.EndTime
    EcObs.Text = m_pEditingEvent.Body
    For i = 0 To CbPrioridade.ListCount - 1
        If CbPrioridade.ItemData(i) = m_pEditingEvent.label Then
            CbPrioridade.ListIndex = i
            Exit For
        End If
    Next

End Sub




Private Sub CbPrioridade_Click()
    Dim pLabel As CalendarEventLabel
    Dim nLabelID As Long
    
    nLabelID = CbPrioridade.ItemData(CbPrioridade.ListIndex)
    Evento.label = nLabelID

    Set pLabel = CalAgenda.DataProvider.LabelList.Find(nLabelID)
    If Not pLabel Is Nothing Then
        ctrlColor.BackColor = pLabel.color
    End If
    
End Sub

Private Sub ImprimeMarcacao()
    Dim continua As Boolean
    continua = BL_IniciaReport("AvisoMarcacao", "Avisos de Marca��o", crptToPrinter)
    
    If continua = False Then Exit Sub
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT sl_identif.utente,sl_identif.t_utente,sl_identif.n_proc_1,sl_identif.nome_ute,sl_identif.dt_nasc_ute,sl_identif.descr_mor_ute,"
    Report.SQLQuery = Report.SQLQuery & " sl_identif.cod_postal_ute,sl_cod_postal.descr_postal,sl_requis_consultas.n_req,sl_proven.descr_proven,"
    Report.SQLQuery = Report.SQLQuery & " sl_agenda.dt_marcacao, sl_agenda.dt_ini,sl_idutilizador.nome  FROM sl_identif JOIN "
    Report.SQLQuery = Report.SQLQuery & " sl_requis_consultas  ON sl_identif.seq_utente = sl_requis_consultas.seq_utente JOIN "
    Report.SQLQuery = Report.SQLQuery & " sl_cod_postal ON sl_identif.cod_postal_ute = sl_cod_postal.cod_postal JOIN "
    Report.SQLQuery = Report.SQLQuery & " sl_proven  ON sl_requis_consultas.cod_proven = sl_proven.cod_proven JOIN sl_agenda  "
    Report.SQLQuery = Report.SQLQuery & " ON sl_requis_consultas.n_req = sl_agenda.n_req LEFT OUTER JOIN sl_tbf_t_sit  ON "
    Report.SQLQuery = Report.SQLQuery & " sl_requis_consultas.t_sit = sl_tbf_t_sit.cod_t_sit, sl_idutilizador "
    Report.SQLQuery = Report.SQLQuery & " WHERE sl_agenda.seq_agenda = " & Evento.ScheduleID
    Report.SQLQuery = Report.SQLQuery & " AND sl_idutilizador.cod_utilizador = " & CStr(gCodUtilizador)
    Call BL_ExecutaReport
    Set Report = Nothing
End Sub

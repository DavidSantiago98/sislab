VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormConfirmacaoPorTubos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   8175
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9555
   Icon            =   "FormConfirmacaoPorTubos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8175
   ScaleWidth      =   9555
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameTubosCompletos 
      Height          =   8175
      Left            =   120
      TabIndex        =   23
      Top             =   0
      Visible         =   0   'False
      Width           =   9375
      Begin VB.CommandButton BtPesquisaTuboP 
         Height          =   315
         Left            =   9000
         Picture         =   "FormConfirmacaoPorTubos.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   38
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida Tubos Prim�rios"
         Top             =   720
         Width           =   375
      End
      Begin VB.TextBox EcDescrTuboP 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6600
         Locked          =   -1  'True
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   720
         Width           =   2415
      End
      Begin VB.TextBox EcCodTuboP 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5760
         TabIndex        =   36
         Top             =   720
         Width           =   855
      End
      Begin VB.CommandButton BtPesquisaProduto 
         Height          =   315
         Left            =   4440
         Picture         =   "FormConfirmacaoPorTubos.frx":0396
         Style           =   1  'Graphical
         TabIndex        =   34
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos"
         Top             =   720
         Width           =   375
      End
      Begin VB.TextBox EcDescrProduto 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   33
         TabStop         =   0   'False
         Top             =   720
         Width           =   2535
      End
      Begin VB.TextBox EcCodProd 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   32
         Top             =   720
         Width           =   735
      End
      Begin VB.TextBox EcReqPesq 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5760
         TabIndex        =   31
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton BtSairTubosComp 
         Height          =   495
         Left            =   8520
         Picture         =   "FormConfirmacaoPorTubos.frx":0720
         Style           =   1  'Graphical
         TabIndex        =   29
         ToolTipText     =   "Sair"
         Top             =   7560
         Width           =   735
      End
      Begin MSFlexGridLib.MSFlexGrid FgTubosComp 
         Height          =   6135
         Left            =   240
         TabIndex        =   28
         Top             =   1200
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   10821
         _Version        =   393216
         BackColorBkg    =   -2147483644
         BorderStyle     =   0
         Appearance      =   0
      End
      Begin MSComCtl2.DTPicker EcDtIni 
         Height          =   255
         Left            =   1200
         TabIndex        =   24
         Top             =   240
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   146014209
         CurrentDate     =   40802
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   3600
         TabIndex        =   25
         Top             =   240
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   146014209
         CurrentDate     =   40802
      End
      Begin VB.Label Label1 
         Caption         =   "Tubo "
         Height          =   255
         Index           =   8
         Left            =   4920
         TabIndex        =   39
         Top             =   720
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Produto"
         Height          =   255
         Index           =   7
         Left            =   240
         TabIndex        =   35
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label6 
         Caption         =   "Requisi��o"
         Height          =   255
         Left            =   4920
         TabIndex        =   30
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label5 
         Caption         =   "Data Final"
         Height          =   255
         Left            =   2760
         TabIndex        =   27
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "Data Inicial"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.CommandButton BtEliminaSemResultado 
      Height          =   555
      Left            =   4440
      Picture         =   "FormConfirmacaoPorTubos.frx":13EA
      Style           =   1  'Graphical
      TabIndex        =   21
      ToolTipText     =   "Elimina TODAS as Analises Sem Resultado"
      Top             =   7440
      Width           =   975
   End
   Begin VB.TextBox EcNumRequis 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Left            =   5040
      TabIndex        =   19
      Top             =   120
      Width           =   735
   End
   Begin VB.Frame FrTubos 
      Height          =   5775
      Left            =   120
      TabIndex        =   16
      Top             =   1560
      Width           =   9375
      Begin MSFlexGridLib.MSFlexGrid FGTubos 
         Height          =   5595
         Left            =   120
         TabIndex        =   17
         Top             =   120
         Width           =   8955
         _ExtentX        =   15796
         _ExtentY        =   9869
         _Version        =   393216
         BackColorBkg    =   -2147483644
         GridColor       =   -2147483644
         BorderStyle     =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.TextBox EcHoraChegada 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Left            =   8760
      TabIndex        =   15
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox EcDataChegada 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Left            =   6840
      TabIndex        =   13
      Top             =   120
      Width           =   975
   End
   Begin VB.Frame Frame2 
      Caption         =   "Dados do Utente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   975
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   9375
      Begin VB.CommandButton BtListaTubosConcluidos 
         Height          =   375
         Left            =   8400
         Picture         =   "FormConfirmacaoPorTubos.frx":1974
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   360
         Width           =   495
      End
      Begin VB.TextBox EcNumCartaoUte 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6360
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         ToolTipText     =   "N�mero de Cart�o de Utente"
         Top             =   600
         Width           =   1095
      End
      Begin VB.ComboBox CbTipoUtente 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   240
         Width           =   1215
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6360
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcUtente 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   240
         Width           =   3135
      End
      Begin VB.TextBox EcNome 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   600
         Width           =   4335
      End
      Begin VB.Label LbReqAssoci 
         AutoSize        =   -1  'True
         Caption         =   "Cart�o Ute."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5520
         TabIndex        =   10
         ToolTipText     =   "N�mero de Cart�o de Utente"
         Top             =   600
         Width           =   810
      End
      Begin VB.Label LbData 
         AutoSize        =   -1  'True
         Caption         =   "Data Nasc."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5520
         TabIndex        =   9
         ToolTipText     =   "Data de Nascimento"
         Top             =   240
         Width           =   795
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   8
         Top             =   240
         Width           =   465
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   7
         Top             =   600
         Width           =   405
      End
   End
   Begin VB.TextBox EcCodTubo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H002C7124&
      Height          =   315
      Left            =   600
      MaxLength       =   9
      TabIndex        =   1
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcDescrTubo 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H002C7124&
      Height          =   315
      Left            =   1560
      TabIndex        =   0
      Top             =   120
      Width           =   2295
   End
   Begin VB.Label Label3 
      Caption         =   "Requisi��o"
      Height          =   255
      Left            =   3960
      TabIndex        =   18
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Hr.Chega"
      Height          =   255
      Left            =   7920
      TabIndex        =   14
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Dt. Chega"
      Height          =   255
      Index           =   0
      Left            =   6000
      TabIndex        =   12
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label LbNumero 
      AutoSize        =   -1  'True
      Caption         =   "&Tubo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   420
   End
End
Attribute VB_Name = "FormConfirmacaoPorTubos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Vari�veis Globais para este Form.
Dim estado As Integer
Dim CamposBDparaListBox
Dim NumEspacos
Dim CampoDeFocus As Object

'Flag que evita os beep dos enters nas textbox
Dim Enter As Boolean
Const Cinza = &HE0E0E0
Const Verde = &H8000&
Const vermelho = &HC0&

Dim etiqueta As String
Private Type Analises_Tubos
    cod_ana_s As String
    cod_agrup As String
    descr_ana As String
    estado As String
    Cor_Estado As Long
    tipo As String
End Type
Dim EstrutAnalisesTubos() As Analises_Tubos
Dim TotalAnalisesTubos  As Integer

'Estrutura que guarda as an�lises eliminadas
Private Type Tipo_AnalisesEliminadas
    NReq As String
    CodSimples As String
    cod_agrup As String
End Type
Dim Eliminadas() As Tipo_AnalisesEliminadas
Dim TotalEliminadas As Integer

'Comando utilizados para seleccionar as an�lises da requisi��o mais os seu dados
Dim CmdReq As New ADODB.Command

Private Type tubo
    cod_tubo As String
    descR_tubo As String
    dt_chega As String
    estado As Integer
    cod_etiq As String
End Type
Private Type Requis
    n_req As String
    dt_chega As String
    estrutTubo() As tubo
    totalTubo As Integer
End Type
Dim EstrutReq() As Requis
Dim totalReq As Integer
Const vermelhoClaro = &H8080FF
Const Amarelo = &H80FFFF
Const Branco = &HFFFFFF
Const VerdeClaro = &HC0FFC0

Const cinzento = &HC0C0C0              '&HE0E0E0
Const azul = &HFEA381                          '&HFF8080
Const laranja = &H80FF&
Const AzulClaro = &HFF8080
Const tamanhoTubo = 1000
Private Sub BtListaTubosConcluidos_Click()
FrameTubosCompletos.top = 0
FrameTubosCompletos.left = 120
FrameTubosCompletos.Visible = True

End Sub

Private Sub BtPesquisaProduto_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_produto"
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_produto"
    CampoPesquisa1 = "descr_produto"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Produto")
    
    mensagem = "N�o foi encontrado nenhum produto."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProd.Text = resultados(1)
            EcDescrProduto.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


Private Sub BtPesquisaTubop_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_Tubo"
    CamposEcran(1) = "cod_Tubo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_Tubo"
    CamposEcran(2) = "descr_Tubo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_Tubo"
    CampoPesquisa1 = "descr_Tubo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tubo")
    
    mensagem = "N�o foi encontrado nenhum Tubo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodTuboP.Text = resultados(1)
            EcDescrTuboP.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub EcCodProd_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProd.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_produto, descr_produto FROM sl_produto WHERE upper(cod_produto)= " & BL_TrataStringParaBD(UCase(EcCodProd.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodProd.Text = "" & RsDescrGrupo!cod_produto
            EcDescrProduto.Text = "" & RsDescrGrupo!descr_produto
        Else
            Cancel = True
            EcDescrProduto.Text = ""
            BG_Mensagem mediMsgBox, "O Produto indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrProduto.Text = ""
    End If

End Sub

Private Sub EcCodtubop_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodTuboP.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_tubo, descr_tubo FROM sl_tubo WHERE upper(cod_tubo)= " & BL_TrataStringParaBD(UCase(EcCodTuboP.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodTuboP.Text = "" & RsDescrGrupo!cod_tubo
            EcDescrTuboP.Text = "" & RsDescrGrupo!descR_tubo
        Else
            Cancel = True
            EcDescrTuboP.Text = ""
            BG_Mensagem mediMsgBox, "O Tubo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrTuboP.Text = ""
    End If
End Sub
Private Sub BtSairTubosComp_Click()
    FrameTubosCompletos.Visible = False
End Sub

Private Sub EcCodTubo_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim CodEtiqTubo As String
    Dim RsDescrT As ADODB.recordset
    Dim sql As String
    Dim rsReq As New ADODB.recordset
    Enter = False
    If EcCodTubo.Text <> "" Then
        If (KeyCode = 13 And Shift = 0) Then
            Enter = True
            If BG_ValidaTipoCampo_ADO(Me, EcCodTubo) Then
                If Len(EcCodTubo.Text) = 9 Then
                    etiqueta = left(EcCodTubo.Text, 2)
                    gRequisicaoActiva = Right(EcCodTubo.Text, 7)
                ElseIf Len(EcCodTubo.Text) <= 7 Then
                    etiqueta = ""
                    gRequisicaoActiva = Right(EcCodTubo.Text, 7)
                End If
                        CmdReq.Parameters(0).value = gRequisicaoActiva
                        Set rsReq = CmdReq.Execute
                        If Not rsReq.EOF Then
                            EcNumRequis.Text = gRequisicaoActiva
                            EcDataChegada.Text = BL_HandleNull(rsReq!dt_chega, "01-01-1900")
                            EcHoraChegada.Text = BL_HandleNull(rsReq!hr_chega, "")
                            PreencheEstadoRequisicao (rsReq!estado_req)
                            If etiqueta <> "" Then
                                CodEtiqTubo = Mid(EcCodTubo.Text, 1, 2)
                                If Verifica_Tubo_ja_Chegou(CStr(CodEtiqTubo), gRequisicaoActiva) = True Then
                                    PreencheDadosUtente (BL_HandleNull(rsReq!seq_utente, ""))
                                    FuncaoProcurar_PreencheTubo gRequisicaoActiva, etiqueta
                                    Actualiza_Estado_Tubo
                                End If
                            Else
                                PreencheDadosUtente (BL_HandleNull(rsReq!seq_utente, ""))
                                FuncaoProcurar_PreencheTubo gRequisicaoActiva, etiqueta
                                Actualiza_Estado_Tubo
                            End If
                        Else
                            BG_Mensagem mediMsgBox, "Requisi��o inexistente!", vbExclamation, "Elimina��o  de Tubos"
                            LimpaCampos
                        End If
                        rsReq.Close
                        Set rsReq = Nothing
                        'Set CmdReq = Nothing
                        EcCodTubo.Text = ""
                        EcCodTubo.SetFocus
            Else
                Sendkeys ("{HOME}+{END}")
                EcCodTubo.SetFocus
            End If
        End If
    End If
 
End Sub

Private Sub EcEtiqTubo_KeyPress(KeyAscii As Integer)
    If Enter = True Then KeyAscii = 0
End Sub




Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormConfirmacaoPorTubos = Nothing

End Sub

Sub DefTipoCampos()
With FGTubos
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        .ColWidth(0) = 3000
        .Col = 0
        .TextMatrix(0, 0) = "An�lise"
        .ColWidth(1) = 2000
        .Col = 1
        .TextMatrix(0, 1) = "Estado"
        .row = 1
        .Col = 0
    End With
With FgTubosComp
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 1000
        .Col = 0
        .TextMatrix(0, 0) = "Requisi��o"
        .ColWidth(1) = tamanhoTubo
        .Col = 1
        .TextMatrix(0, 1) = "Tubo"
        .row = 1
        .Col = 0
    End With

End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    FrameTubosCompletos.Visible = False
    EcDtIni.value = Bg_DaData_ADO
    EcDtFim.value = EcDtIni
End Sub

Sub Inicializacoes()
    
    Me.caption = "Elimina��o de Tubos"
    Me.left = 440
    Me.top = 20
    Me.Width = 9645
    Me.Height = 8595 ' Normal
    
    EcCodTubo.Tag = 5
    Set CampoDeFocus = EcCodTubo
    EcDescrTubo.Locked = True
    
    'Inicializa o comando ADO para selec��o do estado do utente da requisi��o
    Set CmdReq.ActiveConnection = gConexao
    CmdReq.CommandType = adCmdText
    CmdReq.CommandText = "SELECT estado_req, seq_utente, dt_chega, hr_chega FROM sl_requis WHERE " & _
                            "n_req =?"
    CmdReq.Parameters.Append CmdReq.CreateParameter("N_REQ", adDouble, adParamInput, 20)
    CmdReq.Prepared = True
    
End Sub

Sub FuncaoProcurar()
    If FrameTubosCompletos.Visible = False Then
        If EcCodTubo.Text <> "" Then
            EcCodTubo_KeyDown 13, 0
        End If
    Else
        ProcuraTubos
    End If
    
End Sub

Sub LimpaCampos()
    etiqueta = ""
    EcCodTubo.Text = ""
    EcDescrTubo.Text = ""
    EcHoraChegada.Text = ""
    EcDataChegada.Text = ""
    EcNumRequis.Text = ""
    LimpaFgTubos
    EcNumCartaoUte.Text = ""
    EcDataNasc.Text = ""
    EcNome.Text = ""
    EcUtente.Text = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    EcCodProd = ""
    EcDescrProduto = ""
    EcCodTuboP = ""
    EcDescrTuboP = ""
End Sub

Sub FuncaoLimpar()
    LimpaCampos
    EcCodTubo.SetFocus
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub FuncaoProcurar_PreencheTubo(NumReq As Long, tubo As String)
    ' Selecciona todos os tubos da requisi��o e preenche a lista
    Dim i As Long
    Dim rsDescr As ADODB.recordset
    Dim rsTubo As ADODB.recordset
    
    Dim codAgrup As String
    Dim descCodAgrup As String
    
    Dim desCcodComplexa As String
    Dim descCodPerfil As String
    
    Set rsTubo = New ADODB.recordset
    With rsTubo
        If tubo = "" Then
            .Source = " SELECT sl_ana_s.cod_ana_s, sl_ana_s.descr_ana_s, sl_realiza.flg_estado, sl_realiza.cod_agrup, ord_ana, ord_ana_perf, ord_ana_compl, 'R' tipo,v.descr_ana " & _
                      " FROM sl_ana_s, sl_realiza, slv_analises_apenas v " & _
                      " WHERE sl_realiza.n_req =" & gRequisicaoActiva & _
                      " AND sl_ana_s.cod_ana_s = sl_realiza.cod_ana_s " & _
                      " AND sl_realiza.cod_agrup = v.cod_Ana " & _
                      " UNION " & _
                      " SELECT sl_ana_s.cod_ana_s, sl_ana_s.descr_ana_s, '-1' flg_estado, sl_marcacoes.cod_agrup, ord_ana, ord_ana_perf, ord_ana_compl, 'M' tipo, v.descR_ana " & _
                      " FROM sl_ana_s, sl_marcacoes, slv_analises_apenas v " & _
                      " WHERE sl_marcacoes.n_req =" & gRequisicaoActiva & _
                      " AND sl_ana_s.cod_ana_s = sl_marcacoes.cod_ana_s " & _
                      " AND sl_marcacoes.cod_agrup = v.cod_Ana "
            If gSGBD = gOracle Then
                 .Source = .Source & " order by flg_estado,ord_ana, ord_ana_perf, ord_ana_compl, flg_estado"
            Else
                .Source = .Source & " order by  flg_estado"
            End If
        Else
            .Source = " SELECT sl_ana_s.cod_ana_s, sl_ana_s.descr_ana_s, sl_realiza.flg_estado, sl_realiza.cod_agrup, ord_ana, ord_ana_perf, ord_ana_compl, 'R' tipo, v.descR_ana  " & _
                      " FROM sl_ana_s, sl_realiza, slv_analises_apenas v " & _
                      " WHERE sl_realiza.n_req =" & gRequisicaoActiva & _
                      " AND sl_ana_s.cod_tubo IN (SELECT cod_tubo FROM sl_tubo WHERE cod_etiq =  " & BL_TrataStringParaBD(tubo) & ")" & _
                      " AND sl_ana_s.cod_ana_s = sl_realiza.cod_ana_s " & _
                      " AND sl_realiza.cod_agrup = v.cod_Ana " & _
                      " UNION " & _
                      " SELECT sl_ana_s.cod_ana_s, sl_ana_s.descr_ana_s, '-1' flg_estado, sl_marcacoes.cod_agrup, ord_ana, ord_ana_perf, ord_ana_compl, 'M' tipo, v.descR_ana  " & _
                      " FROM sl_ana_s, sl_marcacoes, slv_analises_apenas v " & _
                      " WHERE sl_marcacoes.n_req =" & gRequisicaoActiva & _
                      " AND sl_ana_s.cod_tubo IN (SELECT cod_tubo FROM sl_tubo WHERE cod_etiq =  " & BL_TrataStringParaBD(tubo) & ")" & _
                      " AND sl_ana_s.cod_ana_s = sl_marcacoes.cod_ana_s " & _
                      " AND sl_marcacoes.cod_agrup = v.cod_Ana "
            If gSGBD = gOracle Then
                 .Source = .Source & " order by flg_estado,ord_ana, ord_ana_perf, ord_ana_compl, flg_estado"
            Else
                .Source = .Source & " order by  flg_estado"
            End If
        End If
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
        .ActiveConnection = gConexao
        .Open
    End With
     i = 1
     LimpaFgTubos
     codAgrup = ""
     While Not rsTubo.EOF
        If BL_HandleNull(rsTubo!cod_agrup, "") <> codAgrup And Mid(BL_HandleNull(rsTubo!cod_agrup, ""), 1, 1) <> "S" Then
            TotalAnalisesTubos = TotalAnalisesTubos + 1
            ReDim Preserve EstrutAnalisesTubos(TotalAnalisesTubos)
            EstrutAnalisesTubos(i).cod_agrup = BL_HandleNull(rsTubo!cod_agrup, "")
            EstrutAnalisesTubos(i).descr_ana = BL_HandleNull(rsTubo!descr_ana, "")
            EstrutAnalisesTubos(i).tipo = BL_HandleNull(rsTubo!tipo, "")
            FGTubos.row = i
            FGTubos.Col = 0
            FGTubos.CellBackColor = Cinza
            FGTubos.TextMatrix(i, 0) = EstrutAnalisesTubos(i).descr_ana
            FGTubos.Col = 1
            FGTubos.CellBackColor = Cinza
            FGTubos.TextMatrix(i, 1) = EstrutAnalisesTubos(i).descr_ana
            FGTubos.MergeRow(i) = True
            i = i + 1
            FGTubos.AddItem "", i
            FGTubos.row = i
        End If
        If BL_HandleNull(rsTubo!cod_agrup, "") <> codAgrup Then
            codAgrup = BL_HandleNull(rsTubo!cod_agrup, "")
        End If
        TotalAnalisesTubos = TotalAnalisesTubos + 1
        ReDim Preserve EstrutAnalisesTubos(TotalAnalisesTubos)
        EstrutAnalisesTubos(i).cod_agrup = codAgrup
        EstrutAnalisesTubos(i).cod_ana_s = BL_HandleNull(rsTubo!cod_ana_s, "")
        If EstrutAnalisesTubos(i).cod_ana_s <> EstrutAnalisesTubos(i).cod_agrup Then
            EstrutAnalisesTubos(i).descr_ana = "   "
        Else
            EstrutAnalisesTubos(i).descr_ana = ""
        End If
        EstrutAnalisesTubos(i).descr_ana = EstrutAnalisesTubos(i).descr_ana & BL_HandleNull(rsTubo!descr_ana_s, "")
        EstrutAnalisesTubos(i).estado = BL_DevolveEstadoAna(rsTubo!flg_estado)
        EstrutAnalisesTubos(i).tipo = BL_HandleNull(rsTubo!tipo, "")
        Select Case rsTubo!flg_estado
            Case gEstadoAnaSemResultado
                EstrutAnalisesTubos(i).Cor_Estado = vermelho
            Case gEstadoAnaComResultado
                EstrutAnalisesTubos(i).Cor_Estado = vermelho
            Case gEstadoAnaValidacaoMedica
                EstrutAnalisesTubos(i).Cor_Estado = Verde
            Case gEstadoAnaImpressa
                EstrutAnalisesTubos(i).Cor_Estado = Verde
            Case gEstadoAnaValidacaoTecnica
                EstrutAnalisesTubos(i).Cor_Estado = vermelho
            Case gEstadoAnaBloqueada
                EstrutAnalisesTubos(i).Cor_Estado = vermelho
            Case gEstadoAnaPendente
                EstrutAnalisesTubos(i).Cor_Estado = vermelho
        End Select
        
        FGTubos.row = i
        FGTubos.Col = 0
        FGTubos.TextMatrix(i, 0) = EstrutAnalisesTubos(i).descr_ana
        FGTubos.CellForeColor = EstrutAnalisesTubos(i).Cor_Estado
        FGTubos.Col = 1
        FGTubos.TextMatrix(i, 1) = EstrutAnalisesTubos(i).estado
        FGTubos.CellForeColor = EstrutAnalisesTubos(i).Cor_Estado
        rsTubo.MoveNext
        i = i + 1
        FGTubos.AddItem "", i
        FGTubos.row = i
     Wend
     Set rsTubo = Nothing
End Sub

Sub PreencheDadosUtente(SeqUtente As String)
    
    Dim Campos(5) As String
    Dim retorno() As String
    Dim iret As Integer
    
    Campos(0) = "utente"
    Campos(1) = "t_utente"
    Campos(2) = "nome_ute"
    Campos(3) = "n_cartao_ute"
    Campos(4) = "dt_nasc_ute"

    iret = BL_DaDadosUtente(Campos, retorno, SeqUtente)
    
    If iret = -1 Then
        BG_Mensagem mediMsgBox, "Erro a seleccionar utente!", vbCritical + vbOKOnly, App.ProductName
        CbTipoUtente.ListIndex = mediComboValorNull
        EcUtente.Text = ""
        Exit Sub
    End If
    
    EcUtente.Text = retorno(0)
    CbTipoUtente.Text = retorno(1)
    EcNome.Text = retorno(2)
    EcNumCartaoUte.Text = retorno(3)
    EcDataNasc.Text = retorno(4)
End Sub

Sub LimpaFgTubos()
    Dim j As Long
    j = FGTubos.rows - 1
    While j > 0
        If j > 1 Then
            FGTubos.RemoveItem j
        Else
            FGTubos.TextMatrix(j, 0) = ""
            FGTubos.TextMatrix(j, 1) = ""
        End If
        j = j - 1
    Wend
    TotalAnalisesTubos = 0
    ReDim EstrutAnalisesTubos(TotalAnalisesTubos)
End Sub

'VERIFICA SE O TUBO JA DEU ENTRADA
Function Verifica_Tubo_ja_Chegou(CodEtiqTubo As String, NumReq As Long) As Boolean
    Dim sql As String
    Dim rsTubo As ADODB.recordset
    Set rsTubo = New ADODB.recordset
    
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    
    sql = " select sl_req_tubo.cod_tubo cod_tubo, dt_chega, hr_chega, cod_prod, sl_tubo.descr_tubo " & _
          " from sl_req_tubo, sl_tubo where sl_req_tubo.cod_tubo = sl_tubo.cod_tubo " & _
          " and n_req = " & NumReq & "  and cod_etiq = " & BL_TrataStringParaBD(CodEtiqTubo) & _
          " AND  sl_Req_tubo.dt_eliminacao IS NULL"
          
          
    rsTubo.Open sql, gConexao
    If rsTubo.RecordCount > 0 Then
        If BL_HandleNull(rsTubo!dt_chega, "") = "" Then
            Verifica_Tubo_ja_Chegou = False
            BG_Mensagem mediMsgBox, "O Tubo " & BL_HandleNull(rsTubo!descR_tubo, "") & " da requisi��o " & NumReq & " n�o deu entrada!"
        Else
            Verifica_Tubo_ja_Chegou = True
            EcCodTubo.Text = BL_HandleNull(rsTubo!cod_tubo)
            EcDescrTubo.Text = BL_HandleNull(rsTubo!descR_tubo)
            EcHoraChegada = BL_HandleNull(rsTubo!hr_chega)
            EcDataChegada = BL_HandleNull(rsTubo!dt_chega)
        End If
    Else
        Verifica_Tubo_ja_Chegou = False
    End If
End Function

Private Sub Actualiza_Estado_Tubo()
    Dim i As Integer
    Dim sql As String
    
    On Error GoTo TrataErro
    
    For i = 1 To TotalAnalisesTubos
        If EstrutAnalisesTubos(i).Cor_Estado = vermelho Then
            BG_Mensagem mediMsgBeep, "O Tubo n�o pode ser enviado para o lixo!"
            Exit Sub
        End If
    Next
    BG_Mensagem mediMsgBeep, "O Tubo pode ser enviado para o LIXO!"
'    If etiqueta = "" Then
'        BG_Mensagem mediMsgBeep, "O Tubo pode ser enviado para o LIXO!"
'    Else
'        BG_Mensagem mediMsgBox, "O Tubo foi enviado para o LIXO!"
'        sql = "UPDATE sl_req_tubo set dt_eliminacao = " & BL_TrataDataParaBD(Bg_DaData_ADO) & _
'            " , hr_eliminacao = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & _
'            " WHERE n_req = " & gRequisicaoActiva & " AND cod_tubo = " & EcCodTubo.text
'        BG_ExecutaQuery_ADO sql
'    End If
    
    Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub
Function SelDescrPerfil(codAna As String) As String
    Dim rsDescr As ADODB.recordset
    Dim ret As String
    Dim CmdDesPerfil As New ADODB.Command
    Set CmdDesPerfil.ActiveConnection = gConexao
    CmdDesPerfil.CommandType = adCmdText
    CmdDesPerfil.CommandText = "SELECT descr_perfis FROM sl_perfis WHERE cod_perfis = ?"
    CmdDesPerfil.Prepared = True
    CmdDesPerfil.Parameters.Append CmdDesPerfil.CreateParameter("COD_ANA", adVarChar, adParamInput, 10)

    
    If Trim(codAna) = "" Or Trim(codAna) = "0" Then
        ret = ""
    Else
    
        CmdDesPerfil.Parameters(0).value = codAna
        Set rsDescr = CmdDesPerfil.Execute
        If Not rsDescr.EOF Then
            ret = BL_HandleNull(rsDescr!descr_perfis, codAna)
        Else
            ret = codAna & "(Erro a seleccionar descri��o)"
        End If
        
        rsDescr.Close
        Set rsDescr = Nothing
    
    End If
    
    SelDescrPerfil = ret
End Function
Function SelDescrComplexa(codAna As String) As String
    Dim rsDescr As ADODB.recordset
    Dim ret As String
    Dim CmdDesComplexa As New ADODB.Command
    Set CmdDesComplexa.ActiveConnection = gConexao
    CmdDesComplexa.CommandType = adCmdText
    CmdDesComplexa.CommandText = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c = ?"
    CmdDesComplexa.Prepared = True
    CmdDesComplexa.Parameters.Append CmdDesComplexa.CreateParameter("COD_ANA", adVarChar, adParamInput, 10)

    If Trim(codAna) = "" Or Trim(codAna) = "0" Then
        ret = ""
    Else
        CmdDesComplexa.Parameters(0).value = Trim(codAna)
        Set rsDescr = CmdDesComplexa.Execute
        If Not rsDescr.EOF Then
            ret = BL_HandleNull(rsDescr!Descr_Ana_C, codAna)
        Else
            ret = codAna & "(Erro a seleccionar descri��o)"
        End If
        
        rsDescr.Close
        Set rsDescr = Nothing
    
    End If
    
    SelDescrComplexa = ret
End Function

Private Sub FGTubos_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim CodTubo As String
    Dim cod_ana_s As String
    
    If KeyCode = 46 And Shift = 1 Then ' Shift+Delete = Apaga an�lise currente
        cod_ana_s = EstrutAnalisesTubos(FGTubos.row).cod_ana_s
        
        If EstrutAnalisesTubos(FGTubos.row).tipo = "M" Then
            If BG_Mensagem(mediMsgBox, "Deseja eliminar a an�lise ?", vbYesNo + vbQuestion, "Eliminar") = vbYes Then
                 CodTubo = etiqueta
                 ' ---------------
                 TotalEliminadas = TotalEliminadas + 1
                 ReDim Preserve Eliminadas(TotalEliminadas)
                 Eliminadas(TotalEliminadas).NReq = gRequisicaoActiva
                 Eliminadas(TotalEliminadas).CodSimples = EstrutAnalisesTubos(FGTubos.row).cod_ana_s
                 Eliminadas(TotalEliminadas).cod_agrup = EstrutAnalisesTubos(FGTubos.row).cod_agrup
                 ' ----------------
                 Elimina_Analise_SL_MARCACOES
                 
                 ' Limpa tudo e volta a procurar
                 FuncaoLimpar
                 EcCodTubo.Text = CodTubo
                 EcCodTubo_KeyDown 13, 0
            End If
        Else
            BG_Mensagem mediMsgBox, "N�o � Possivel Eliminar a An�lise."
        End If
    End If
 End Sub



Private Sub Elimina_Analise_SL_MARCACOES()
    Dim sql As String
    Dim Erro As Integer
    Dim contador As Integer
    
    BG_BeginTransaction
    For contador = 1 To TotalEliminadas
        sql = "DELETE FROM sl_marcacoes WHERE n_req = " & gRequisicaoActiva & _
            " AND cod_ana_s = " & BL_TrataStringParaBD(Eliminadas(contador).CodSimples) & _
            " AND cod_agrup = " & BL_TrataStringParaBD(Eliminadas(contador).cod_agrup)
        BG_ExecutaQuery_ADO sql
        Erro = BG_Trata_BDErro
        BL_RegistaAnaEliminadas CStr(gRequisicaoActiva), Eliminadas(contador).cod_agrup, "0", "0", Eliminadas(contador).CodSimples
    Next
    
    If Erro = 0 Then
        BG_CommitTransaction
        TotalEliminadas = 0
        ReDim Eliminadas(0)
    Else
        BG_RollbackTransaction
    End If
End Sub

Private Sub BtEliminaSemResultado_Click()
    Dim contador As Integer
    Dim cod_tubo As String
    
    On Error GoTo TrataErro
    
    cod_tubo = etiqueta
    If BG_Mensagem(mediMsgBox, "Deseja eliminar TODAS as an�lises ?", vbYesNo + vbQuestion, "Eliminar") = vbYes Then
        For contador = 1 To TotalAnalisesTubos
            If EstrutAnalisesTubos(contador).tipo = "M" Then
                ' ---------------
                TotalEliminadas = TotalEliminadas + 1
                ReDim Preserve Eliminadas(TotalEliminadas)
                Eliminadas(TotalEliminadas).NReq = gRequisicaoActiva
                Eliminadas(TotalEliminadas).CodSimples = EstrutAnalisesTubos(contador).cod_ana_s
                Eliminadas(TotalEliminadas).cod_agrup = EstrutAnalisesTubos(contador).cod_agrup
                ' ----------------
            End If
        Next
    End If
        If TotalEliminadas > 0 Then
            Elimina_Analise_SL_MARCACOES
            FuncaoLimpar
            EcCodTubo.Text = cod_tubo
            EcCodTubo_KeyDown 13, 0
        End If
    Exit Sub
    
TrataErro:
    Exit Sub
    Resume Next
End Sub

Sub PreencheEstadoRequisicao(EstadoReq As String)
    
    EcDescrTubo = BL_DevolveEstadoReq(EstadoReq)
    If EstadoReq = gEstadoReqBloqueada Then
        BtEliminaSemResultado.Enabled = False
    Else
        BtEliminaSemResultado.Enabled = True
    End If
    
End Sub

Private Sub ProcuraTubos()
    Dim sSql As String
    Dim RsTubos As New ADODB.recordset
    On Error GoTo TrataErro
    LimpaFgTubosComp
    totalReq = 0
    ReDim EstrutReq(0)
    sSql = "SELECT x1.n_req, x1.cod_tubo, x1.dt_chega, x2.descr_tubo, x3.dt_chega dt_req, x2.cod_etiq "
    sSql = sSql & " FROM sl_req_tubo x1, sl_tubo x2, sl_Requis x3 "
    sSql = sSql & " WHERE x1.n_Req = x3.n_req AND x1.cod_tubo =X2.cod_tubo "
    sSql = sSql & " AND x3.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
    If EcReqPesq <> "" Then
        sSql = sSql & " AND x3.n_Req = " & EcReqPesq
    End If
    If EcCodTuboP <> "" Then
        sSql = sSql & " AND x2.cod_tubo = " & BL_TrataStringParaBD(EcCodTuboP)
    End If
    If EcCodProd <> "" Then
        sSql = sSql & " AND x2.cod_prod = " & BL_TrataStringParaBD(EcCodProd)
    End If
    sSql = sSql & " ORDER by n_Req ASC, cod_tubo ASC "
    RsTubos.CursorLocation = adUseServer
    RsTubos.CursorType = adOpenStatic
    RsTubos.Open sSql, gConexao
    If RsTubos.RecordCount > 0 Then
        totalReq = 0
        ReDim EstrutReq(0)
        While Not RsTubos.EOF
            If EstrutReq(totalReq).n_req <> BL_HandleNull(RsTubos!n_req, "") Then
                totalReq = totalReq + 1
                ReDim Preserve EstrutReq(totalReq)
                EstrutReq(totalReq).n_req = BL_HandleNull(RsTubos!n_req, "")
                EstrutReq(totalReq).dt_chega = BL_HandleNull(RsTubos!dt_req, "")
                EstrutReq(totalReq).totalTubo = 0
                ReDim EstrutReq(totalReq).estrutTubo(EstrutReq(totalReq).totalTubo)
            End If
            EstrutReq(totalReq).totalTubo = EstrutReq(totalReq).totalTubo + 1
            ReDim Preserve EstrutReq(totalReq).estrutTubo(EstrutReq(totalReq).totalTubo)
            EstrutReq(totalReq).estrutTubo(EstrutReq(totalReq).totalTubo).cod_tubo = BL_HandleNull(RsTubos!cod_tubo, "")
            EstrutReq(totalReq).estrutTubo(EstrutReq(totalReq).totalTubo).descR_tubo = BL_HandleNull(RsTubos!descR_tubo, "")
            EstrutReq(totalReq).estrutTubo(EstrutReq(totalReq).totalTubo).dt_chega = BL_HandleNull(RsTubos!dt_chega, "")
            EstrutReq(totalReq).estrutTubo(EstrutReq(totalReq).totalTubo).cod_etiq = BL_HandleNull(RsTubos!cod_etiq, "")
            EstrutReq(totalReq).estrutTubo(EstrutReq(totalReq).totalTubo).estado = DevolveEstadoTubo(totalReq, EstrutReq(totalReq).totalTubo)
            RsTubos.MoveNext
        Wend
    End If
    RsTubos.Close
    Set RsTubos = Nothing
    PreencheFGTubosComp
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro:" & Err.Description & " " & sSql, Me.Name, "ProcuraTubos", False
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheFGTubosComp()
    Dim i As Integer
    Dim j As Integer
    On Error GoTo TrataErro
    For i = 1 To totalReq
        FgTubosComp.row = i
        FgTubosComp.TextMatrix(i, 0) = EstrutReq(i).n_req
        For j = 1 To EstrutReq(i).totalTubo
            FgTubosComp.Col = j
            If EstrutReq(i).estrutTubo(j).estado = -1 Then
                FgTubosComp.CellBackColor = vermelhoClaro
            ElseIf EstrutReq(i).estrutTubo(j).estado = 0 Then
                FgTubosComp.CellBackColor = vermelhoClaro
            ElseIf EstrutReq(i).estrutTubo(j).estado = 1 Then
                FgTubosComp.CellBackColor = VerdeClaro
            End If
            FgTubosComp.TextMatrix(i, j) = EstrutReq(i).estrutTubo(j).descR_tubo
            If j = FgTubosComp.Cols - 1 Then
                FgTubosComp.Cols = FgTubosComp.Cols + 1
                FgTubosComp.TextMatrix(0, FgTubosComp.Cols - 1) = "Tubo"
            End If
        Next
        FgTubosComp.AddItem ""
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro:" & Err.Description, Me.Name, "PreencheFGTubosComp", False
    Exit Sub
    Resume Next
End Sub
Private Function DevolveEstadoTubo(iReq As Integer, iTubo As Integer) As Integer
    Dim sSql As String
    Dim rsEst As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT '-1' flg_estado FROM sl_marcacoes x1, slv_analises_apenas x2, sl_req_tubo x3 WHERE x1.n_req = " & EstrutReq(iReq).n_req
    sSql = sSql & " AND x1.n_Req = x3.n_req AND x3.cod_tubo = " & BL_TrataStringParaBD(EstrutReq(iReq).estrutTubo(iTubo).cod_tubo)
    sSql = sSql & " AND x1.cod_Agrup = x2.cod_ana AND x2.cod_tubo = x3.cod_tubo "
    sSql = sSql & " UNION SELECT  flg_estado FROM sl_realiza x1, slv_analises_apenas x2, sl_req_tubo x3 WHERE x1.n_req = " & EstrutReq(iReq).n_req
    sSql = sSql & " AND x1.n_Req = x3.n_req AND x3.cod_tubo = " & BL_TrataStringParaBD(EstrutReq(iReq).estrutTubo(iTubo).cod_tubo)
    sSql = sSql & " AND x1.cod_Agrup = x2.cod_ana AND x2.cod_tubo = x3.cod_tubo "
    rsEst.CursorLocation = adUseServer
    rsEst.CursorType = adOpenStatic
    rsEst.Open sSql, gConexao
    If rsEst.RecordCount > 0 Then
        While Not rsEst.EOF
            If BL_HandleNull(rsEst!flg_estado) <> 3 And BL_HandleNull(rsEst!flg_estado) <> 4 Then
                DevolveEstadoTubo = 0
                rsEst.Close
                Set rsEst = Nothing
                Exit Function
            End If
            rsEst.MoveNext
        Wend
    End If
    rsEst.Close
    Set rsEst = Nothing
    DevolveEstadoTubo = 1
Exit Function
TrataErro:
    DevolveEstadoTubo = 0
    BG_LogFile_Erros "Erro:" & Err.Description, Me.Name, "DevolveEstadoTubo", False
    Exit Function
    Resume Next
End Function

Private Sub LimpaFgTubosComp()
    FgTubosComp.rows = 2
    FgTubosComp.Cols = 2
    FgTubosComp.TextMatrix(1, 0) = ""
    FgTubosComp.TextMatrix(1, 1) = ""
End Sub

VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormFACTUSFicheiros 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   6270
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   10800
   Icon            =   "FormFACTUSFicheiros.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6270
   ScaleWidth      =   10800
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtElimina 
      Height          =   585
      Left            =   8760
      Picture         =   "FormFACTUSFicheiros.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Elimina ficheiro seleccionado"
      Top             =   120
      Width           =   1935
   End
   Begin VB.CommandButton BtPesquisa 
      Height          =   585
      Left            =   6840
      Picture         =   "FormFACTUSFicheiros.frx":05F4
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Pesquisa ficheiro facturado"
      Top             =   120
      Width           =   1935
   End
   Begin VB.TextBox EcFicheiro 
      Appearance      =   0  'Flat
      ForeColor       =   &H00808080&
      Height          =   4455
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   7
      Top             =   1680
      Width           =   10575
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3240
      Top             =   8040
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormFACTUSFicheiros.frx":0C11
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      Height          =   495
      Left            =   2520
      ScaleHeight     =   435
      ScaleWidth      =   555
      TabIndex        =   4
      Top             =   8040
      Width           =   615
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   6615
      Begin VB.TextBox EcNFac 
         Height          =   285
         Left            =   4440
         TabIndex        =   3
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox EcSerieFac 
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "N�mero"
         Height          =   255
         Left            =   3480
         TabIndex        =   6
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "S�rie"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   615
      End
   End
   Begin MSComctlLib.ListView LvFicheiros 
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   10575
      _ExtentX        =   18653
      _ExtentY        =   1296
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   0
   End
End
Attribute VB_Name = "FormFACTUSFicheiros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'     .................................................................
'    .        Ecr� de gest�o de ficheiros enviados para o FACTUS       .
'   .                                                                   .
'   .                    Paulo Ferreira 2009.09.16                      .
'    .                        � 2009 GLINTT-HS                         .
'     .................................................................

Option Explicit

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim CriterioTabela As String

Public rs As ADODB.recordset

' ------------------------------------------------------------------------------------
' ESTRUTURA QUE GUARDA OS DADOS SOBRE O FICHEIRO
' ------------------------------------------------------------------------------------
Private Type ficheiro
    tipo As String
    NumSeq As Long
    serie As String
    Nfac As Long
    conteudo As String
    DtCri As String
    UserCri As String
    nome As String
    caminho As String
    Computador As String
End Type

Dim ficheiros() As ficheiro
Dim TotalFicheiros As Long

Public Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    
    Call Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Public Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Public Sub EventoUnload()
    
    BG_StackJanelas_Pop
  
    Set gFormActivo = MDIFormInicio
    
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        If (rs.state = adStateOpen) Then: rs.Close
        Set rs = Nothing
    End If
    
    ' Fecha a conex�o secund�ria.
    BL_Fecha_Conexao_Secundaria
    
    
End Sub

Private Sub Inicializacoes()

    Me.caption = " Elimina��o de ficheiros facturados"
    
    Me.left = 100
    Me.top = 20
    Me.Width = 10890
    Me.Height = 6660 ' Normal
    'Me.Height = 8700 ' Campos Extras
    
    Set CampoDeFocus = EcSerieFac
    
    ' -----------------------
    ' ABRE CONEXAO COM FACTUS
    ' -----------------------
    Call BL_Abre_Conexao_Secundaria(gSGBD_SECUNDARIA)
   
    InicializaEstrutura
    
End Sub

Private Sub DefTipoCampos()
    
    On Error GoTo TrataErro
    
    ' --------------------------
    ' INICIALIZA LISTA FICHEIROS
    ' --------------------------
    With LvFicheiros
        .ColumnHeaders.Add(, , "Tipo", 1800, lvwColumnLeft).Key = "TIPO"
        .ColumnHeaders.Add(, , "N�Sequencial", 1200, lvwColumnLeft).Key = "N_SEQ"
        .ColumnHeaders.Add(, , "S�rie", 800, lvwColumnLeft).Key = "SERIE"
        .ColumnHeaders.Add(, , "N�Factura��o", 1200, lvwColumnLeft).Key = "N_FAC"
        .ColumnHeaders.Add(, , "Data", 1250, lvwColumnLeft).Key = "DATA"
        .ColumnHeaders.Add(, , "Utilizador", 1500, lvwColumnLeft).Key = "UTIL"
        .ColumnHeaders.Add(, , "Nome", 1300, lvwColumnLeft).Key = "NAME"
        .ColumnHeaders.Add(, , "Direct�rio", 1520, lvwColumnLeft).Key = "DIR"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .SmallIcons = ImageList1
        .AllowColumnReorder = False
        .FullRowSelect = True
        .Checkboxes = False
        .ForeColor = &H808080
        .MultiSelect = True
    End With
    SetListViewColor LvFicheiros, Picture1, &H8000000F, &HFFFFFF
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "DefTipoCampos"
    Exit Sub
    Resume Next
End Sub
 
Private Sub SetListViewColor(�lvCtrlListView� As ListView, �pCtrlPictureBox� As PictureBox, �lColorOne� As Long, Optional �lColorTwo� As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo TrataErro
    lColor1 = �lColorOne�
    lColor2 = �lColorTwo�
    �lvCtrlListView�.Picture = LoadPicture("")
    �lvCtrlListView�.Refresh
    �pCtrlPictureBox�.Cls
    �pCtrlPictureBox�.AutoRedraw = True
    �pCtrlPictureBox�.BorderStyle = vbBSNone
    �pCtrlPictureBox�.ScaleMode = vbTwips
    �pCtrlPictureBox�.Visible = False
    �lvCtrlListView�.PictureAlignment = lvwTile
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    �pCtrlPictureBox�.top = �lvCtrlListView�.top
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    With �pCtrlPictureBox�.Font
        .Size = �lvCtrlListView�.Font.Size + 2
        .Bold = �lvCtrlListView�.Font.Bold
        .Charset = �lvCtrlListView�.Font.Charset
        .Italic = �lvCtrlListView�.Font.Italic
        .Name = �lvCtrlListView�.Font.Name
        .Strikethrough = �lvCtrlListView�.Font.Strikethrough
        .Underline = �lvCtrlListView�.Font.Underline
        .Weight = �lvCtrlListView�.Font.Weight
    End With
    �pCtrlPictureBox�.Refresh
    lLineHeight = �pCtrlPictureBox�.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = �lvCtrlListView�.Width
    �pCtrlPictureBox�.Height = lBarHeight * 2
    �pCtrlPictureBox�.Width = lBarWidth
    �pCtrlPictureBox�.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    �pCtrlPictureBox�.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    �pCtrlPictureBox�.AutoSize = True
    �lvCtrlListView�.Picture = �pCtrlPictureBox�.Image
    �lvCtrlListView�.Refresh
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "SetListViewColor"
    Exit Sub
    Resume Next
End Sub

Public Sub FuncaoProcurar()
    
    On Error GoTo TrataErro
    If (BL_Abre_Conexao_HIS(gConexaoSecundaria, gOracle) = 0) Then: Exit Sub
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
   If (EcSerieFac.Text = Empty And EcNFac.Text = Empty) Then: BG_Mensagem mediMsgBox, "� obrigat�rio indicar s�rie e n�mero de factura��o!", vbExclamation, " Procurar": Exit Sub
    
    CriterioTabela = "select x2.descricao, x1.num_sequencia, x1.serie_fac, x1.n_fac, dbms_lob.substr (ficheiro, 3997, 1) ficheiro, " & _
                     "x1.dt_cri, x4.Nome, x1.nome_ficheiro, x1.Path, x3.descr_computador " & _
                     "from fa_ficheiros x1, fa_tbf_t_ficheiro x2, fa_computador x3, fa_idutilizador x4 where " & _
                     "x1.n_fac = " & EcNFac.Text & " and x1.serie_fac = " & BL_TrataStringParaBD(EcSerieFac.Text) & " and " & _
                     "x1.user_cri = x4.cod_utilizador and x1.cod_computador = x3.cod_computador  and x1.tipo = x2.codigo"
  
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
 
    rs.Open CriterioTabela, gConexaoSecundaria
    
    If rs.RecordCount <= 0 Then
        estado = 0
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        PreencheCampos
        ActulizaLista
        BL_ToolbarEstadoN estado
        
        BL_FimProcessamento Me
    End If
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar"
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheCampos()

    On Error GoTo TrataErro
    
    If (rs.state = adStateClosed) Then: Exit Sub
    While (Not rs.EOF)
        
        PreencheEstrutura BL_HandleNull(rs!descricao, Empty), BL_HandleNull(rs!num_sequencia, Empty), _
                          BL_HandleNull(rs!serie_fac, Empty), BL_HandleNull(rs!n_fac, Empty), _
                          BL_HandleNull(rs!ficheiro, Empty), BL_HandleNull(rs!dt_cri, Empty), _
                          BL_HandleNull(rs!nome, Empty), BL_HandleNull(rs!nome_ficheiro, Empty), _
                          BL_HandleNull(rs!Path, Empty), BL_HandleNull(rs!descr_computador, Empty)
        rs.MoveNext
    Wend
    If (rs.state = adStateOpen) Then: rs.Close
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "PreencheCampos"
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheEstrutura(tipo As String, NumSeq As Long, serie As String, Nfac As Long, conteudo As String, DtCri As String, UserCri As String, nome As String, caminho As String, Computador As String)

    On Error GoTo TrataErro
    
    ReDim Preserve ficheiros(UBound(ficheiros) + 1)
    ficheiros(UBound(ficheiros)).tipo = tipo
    ficheiros(UBound(ficheiros)).NumSeq = NumSeq
    ficheiros(UBound(ficheiros)).serie = serie
    ficheiros(UBound(ficheiros)).Nfac = Nfac
    ficheiros(UBound(ficheiros)).conteudo = conteudo
    ficheiros(UBound(ficheiros)).DtCri = DtCri
    ficheiros(UBound(ficheiros)).UserCri = UserCri
    ficheiros(UBound(ficheiros)).nome = nome
    ficheiros(UBound(ficheiros)).caminho = caminho
    ficheiros(UBound(ficheiros)).Computador = Computador
    TotalFicheiros = UBound(ficheiros)
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "PreencheEstrutura"
    Exit Sub
    Resume Next
End Sub

Private Sub ActulizaLista()

    Dim item As Long
    Dim cursor As Long

    On Error GoTo TrataErro
    
    cursor = LvFicheiros.MousePointer
    LvFicheiros.MousePointer = vbHourglass
    LockWindowUpdate LvFicheiros.hwnd
    For item = 1 To UBound(ficheiros)
        With LvFicheiros.ListItems.Add(, , ficheiros(item).tipo, , 1)
            .ListSubItems.Add , , ficheiros(item).NumSeq, , ficheiros(item).NumSeq
            .ListSubItems.Add , , ficheiros(item).serie, , ficheiros(item).serie
            .ListSubItems.Add , , ficheiros(item).Nfac, , ficheiros(item).Nfac
            .ListSubItems.Add , , ficheiros(item).DtCri, , ficheiros(item).DtCri
            .ListSubItems.Add , , ficheiros(item).UserCri, , ficheiros(item).UserCri
            .ListSubItems.Add , , ficheiros(item).nome, , ficheiros(item).nome
            .ListSubItems.Add , , ficheiros(item).caminho, , ficheiros(item).caminho
        End With
    Next
    LockWindowUpdate 0&
    LvFicheiros.MousePointer = cursor
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "ActulizaLista"
    LockWindowUpdate 0&
    LvFicheiros.MousePointer = cursor
    Exit Sub
    Resume Next
End Sub

Public Sub FuncaoLimpar()

   LimpaCampos

End Sub

Private Sub LimpaCampos()
    
    On Error GoTo TrataErro
    
    EcSerieFac.Text = Empty
    EcNFac.Text = Empty
    LvFicheiros.ListItems.Clear
    EcFicheiro.Text = Empty
    InicializaEstrutura
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "LimpaCampos"
    Exit Sub
    Resume Next
End Sub

Private Sub BtElimina_Click()

    EliminaFicheiro
    
End Sub

Private Sub BtPesquisa_Click()

    FuncaoProcurar
    
End Sub

Private Sub EcSerieFac_LostFocus()

    If (EcSerieFac.Text <> Empty) Then: EcSerieFac.Text = UCase(EcSerieFac.Text)
    
End Sub

Public Sub Form_Load()
    
    EventoLoad
    
End Sub


Public Sub Form_Activate()
    
    EventoActivate
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    EventoUnload
    
End Sub

Private Sub InicializaEstrutura()

    On Error GoTo TrataErro
    ReDim ficheiros(0 To 0)
    ficheiros(0).tipo = Empty
    ficheiros(0).NumSeq = Empty
    ficheiros(0).serie = Empty
    ficheiros(0).Nfac = Empty
    ficheiros(0).conteudo = Empty
    ficheiros(0).DtCri = Empty
    ficheiros(0).nome = Empty
    ficheiros(0).caminho = Empty
    ficheiros(0).Computador = Empty
  
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "InicializaEstrutura"
    Exit Sub
    Resume Next
End Sub

Private Sub ApagaFicheiro(Index As Long)

    Dim sql As String
    
    On Error GoTo TrataErro
    sql = "delete from fa_ficheiros where serie_fac = " & BL_TrataStringParaBD(ficheiros(Index).serie) & _
          " and n_fac = " & ficheiros(Index).Nfac
    BL_ExecutaQuery_Secundaria sql
    If (gSQLError <> Empty) Then: BG_Mensagem mediMsgBox, "Erro ao apagar ficheiro!", vbExclamation, " Aten��o"
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "ActualizaFacturacao"
    Exit Sub
    Resume Next
End Sub

Private Sub ActualizaFacturacao(Index As Long)

    Dim sql As String
    
    On Error GoTo TrataErro
    sql = "update fa_fact set flg_fact_fx = null, nome_fx = null where serie_fac = " & _
          BL_TrataStringParaBD(ficheiros(Index).serie) & " and n_fac = " & ficheiros(Index).Nfac
    BL_ExecutaQuery_Secundaria sql
    If (gSQLError <> Empty) Then: BG_Mensagem mediMsgBox, "Erro ao apagar ficheiro!", vbExclamation, " Aten��o"
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "ActualizaFacturacao"
    Exit Sub
    Resume Next
End Sub

Private Sub RegistaFicheiroApagado(Index As Long)

    Dim sql As String
    
    On Error GoTo TrataErro
    sql = "insert into sl_eliminacoes_ficheiros (serie_fac,n_fac,user_cri,dt_cri,hr_cri) values (" & _
           BL_TrataStringParaBD(ficheiros(Index).serie) & "," & ficheiros(Index).Nfac & "," & _
           BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & _
           BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
    BG_ExecutaQuery_ADO sql
    If (gSQLError <> Empty) Then: BG_Mensagem mediMsgBox, "Erro ao registar ficheiro apagado!", vbExclamation, " Aten��o"
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "RegistaFicheiroApagado"
    Exit Sub
    Resume Next
End Sub

Private Sub LvFicheiros_ItemClick(ByVal item As MSComctlLib.ListItem)

    EcFicheiro.Text = ficheiros(item.Index).conteudo

End Sub

Private Sub EliminaFicheiro()

    On Error GoTo TrataErro
    If (LvFicheiros.ListItems.Count = Empty) Then: Exit Sub
    If (BG_Mensagem(mediMsgBox, "Deseja eliminar o ficheiro " & ficheiros(LvFicheiros.SelectedItem.Index).serie & "?", vbYesNo + vbDefaultButton2 + vbQuestion, " Elimina��o") <> vbYes) Then: Exit Sub
    ActualizaFacturacao LvFicheiros.SelectedItem.Index
    If (gSQLError = Empty) Then: ApagaFicheiro LvFicheiros.SelectedItem.Index
    If (gSQLError = Empty) Then: RegistaFicheiroApagado LvFicheiros.SelectedItem.Index
    If (gSQLError = Empty) Then: BG_Mensagem mediMsgBox, "Ficheiro eliminado.", vbInformation, " Elimina��o"
    If (gSQLError = Empty) Then: LimpaCampos
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "EliminaFicheiro"
    Exit Sub
    Resume Next
End Sub

Private Sub LvFicheiros_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo TrataErro
    If (LvFicheiros.ListItems.Count = Empty) Then: Exit Sub
    If (KeyCode = vbKeyDelete) Then: EliminaFicheiro
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "EliminaFicheiro"
    Exit Sub
    Resume Next
End Sub

VERSION 5.00
Begin VB.Form FormStkFornecedores 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormStkFornecedores"
   ClientHeight    =   4395
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   6360
   Icon            =   "FormStkFornecedores.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4395
   ScaleWidth      =   6360
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcRuaPostal 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1680
      MaxLength       =   3
      TabIndex        =   5
      Top             =   1440
      Width           =   490
   End
   Begin VB.TextBox EcCodPostal 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1080
      MaxLength       =   4
      TabIndex        =   4
      Top             =   1440
      Width           =   615
   End
   Begin VB.TextBox EcDescrPostal 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   2160
      TabIndex        =   38
      Top             =   1440
      Width           =   3615
   End
   Begin VB.CommandButton BtPesqCodPostal 
      Height          =   315
      Left            =   5760
      Picture         =   "FormStkFornecedores.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   37
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
      Top             =   1440
      Width           =   375
   End
   Begin VB.Frame Frame1 
      Height          =   795
      Left            =   0
      TabIndex        =   24
      Top             =   3360
      Width           =   6165
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   30
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   29
         Top             =   195
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   28
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   27
         Top             =   120
         Width           =   1695
      End
      Begin VB.Label Label7 
         Caption         =   "Altera��o"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   26
         Top             =   480
         Width           =   765
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   25
         Top             =   195
         Width           =   615
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4080
      TabIndex        =   23
      Top             =   5760
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   22
      Top             =   5760
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4080
      TabIndex        =   21
      Top             =   6120
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   20
      Top             =   6150
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox EcHrCri 
      Enabled         =   0   'False
      Height          =   285
      Left            =   6120
      TabIndex        =   19
      Top             =   5760
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcHrAct 
      Enabled         =   0   'False
      Height          =   285
      Left            =   6120
      TabIndex        =   18
      Top             =   6120
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcPrazoPag 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   5160
      TabIndex        =   1
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcObservacao 
      Appearance      =   0  'Flat
      Height          =   855
      Left            =   1080
      TabIndex        =   9
      Top             =   2280
      Width           =   5055
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcNome 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      TabIndex        =   2
      Top             =   480
      Width           =   5055
   End
   Begin VB.TextBox EcMorada 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      TabIndex        =   3
      Top             =   960
      Width           =   5055
   End
   Begin VB.TextBox EcCodigoPostal 
      Height          =   285
      Left            =   1080
      TabIndex        =   7
      Top             =   4920
      Width           =   1095
   End
   Begin VB.TextBox EcTelefone 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      TabIndex        =   6
      Top             =   1920
      Width           =   2535
   End
   Begin VB.TextBox EcFax 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4440
      TabIndex        =   8
      Top             =   1920
      Width           =   1695
   End
   Begin VB.Label Label11 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   120
      TabIndex        =   36
      Top             =   5760
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label12 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   120
      TabIndex        =   35
      Top             =   6120
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label13 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2880
      TabIndex        =   34
      Top             =   5760
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label14 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2760
      TabIndex        =   33
      Top             =   6120
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label16 
      Caption         =   "EcHrCriacao"
      Height          =   255
      Left            =   4920
      TabIndex        =   32
      Top             =   5760
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label17 
      Caption         =   "EcHrAlteracao"
      Height          =   255
      Left            =   4800
      TabIndex        =   31
      Top             =   6120
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label8 
      Caption         =   "Prazo Pagam."
      Height          =   255
      Index           =   0
      Left            =   3960
      TabIndex        =   17
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label7 
      Caption         =   "Observa��o"
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   16
      Top             =   2280
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Nome"
      Height          =   255
      Left            =   0
      TabIndex        =   15
      Top             =   480
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   0
      TabIndex        =   14
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "Morada"
      Height          =   255
      Left            =   0
      TabIndex        =   13
      Top             =   960
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "C�digo Postal"
      Height          =   255
      Left            =   0
      TabIndex        =   12
      Top             =   1440
      Width           =   975
   End
   Begin VB.Label Label5 
      Caption         =   "Telefone(s)"
      Height          =   255
      Left            =   0
      TabIndex        =   11
      Top             =   1920
      Width           =   975
   End
   Begin VB.Label Label6 
      Caption         =   "Fax"
      Height          =   255
      Left            =   3840
      TabIndex        =   10
      Top             =   1920
      Width           =   495
   End
End
Attribute VB_Name = "FormStkFornecedores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim rsTabela As ADODB.recordset
Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object



Private Sub EcNome_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub


Private Sub EcNome_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub


Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = "Fornecedores"
    Me.left = 5
    Me.top = 5
    Me.Width = 6450
    Me.Height = 4815
    
    NomeTabela = gBD_PREFIXO_TAB & "stk_fornecedores"
    Set CampoDeFocus = EcNome
    
    NumCampos = 14
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "codigo"
    CamposBD(1) = "nome"
    CamposBD(2) = "morada"
    CamposBD(3) = "cod_postal"
    CamposBD(4) = "telefone"
    CamposBD(5) = "fax"
    CamposBD(6) = "observacao"
    CamposBD(7) = "prazo_pag"
    CamposBD(8) = "user_cri"
    CamposBD(9) = "dt_cri"
    CamposBD(10) = "hr_cri"
    CamposBD(11) = "user_act"
    CamposBD(12) = "dt_act"
    CamposBD(13) = "hr_Act"
    
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcNome
    Set CamposEc(2) = EcMorada
    Set CamposEc(3) = EcCodigoPostal
    Set CamposEc(4) = EcTelefone
    Set CamposEc(5) = EcFax
    Set CamposEc(6) = EcObservacao
    Set CamposEc(7) = EcPrazoPag
    Set CamposEc(8) = EcUtilizadorCriacao
    Set CamposEc(9) = EcDataCriacao
    Set CamposEc(10) = EcHrCri
    Set CamposEc(11) = EcUtilizadorAlteracao
    Set CamposEc(12) = EcDataAlteracao
    Set CamposEc(13) = EcHrAct
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    'TextoCamposObrigatorios(0) = "C�digo"
    TextoCamposObrigatorios(1) = "Nome"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "codigo"
    Set ChaveEc = EcCodigo
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    
    estado = 1
    BG_StackJanelas_Push Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rsTabela Is Nothing Then
        rsTabela.Close
        Set rsTabela = Nothing
    End If
    Set FormStkFornecedores = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    BG_LimpaCampo_Todos CamposEc
    
    EcCodPostal = ""
    EcDescrPostal = ""
    EcRuaPostal = ""
    LaDataAlteracao = ""
    LaDataCriacao = ""
    LaUtilAlteracao = ""
    LaUtilCriacao = ""
    
End Sub

Sub DefTipoCampos()
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

Sub PreencheValoresDefeito()
End Sub

Sub PreencheCampos()
    Me.SetFocus
    
    If rsTabela.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rsTabela, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao & " " & EcHrCri
        LaDataAlteracao = EcDataAlteracao & " " & EcHrAct
        PreencheCodigoPostal
    End If
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rsTabela Is Nothing Then
            rsTabela.Close
            Set rsTabela = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
    Dim SelTotal As Boolean

    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    
    Set rsTabela = New ADODB.recordset
    rsTabela.CursorLocation = adUseServer
    rsTabela.Open CriterioTabela, gConexao, adOpenStatic
    
    If rsTabela.RecordCount = 0 Then
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos
        PreencheCampos
        BL_ToolbarEstadoN estado
    End If
End Sub

Sub FuncaoAnterior()
    rsTabela.MovePrevious
    
    If rsTabela.BOF Then
        rsTabela.MoveNext
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
    End If
End Sub

Sub FuncaoSeguinte()
    rsTabela.MoveNext
    
    If rsTabela.EOF Then
        rsTabela.MovePrevious
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    Dim SQLMax As String
    Dim rsTabelaMax As ADODB.recordset
    

    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        
        iRes = ValidaCamposEc
        
        If iRes = True Then
            ' Inicio - Obtem n�mero sequencial
            SQLMax = "SELECT Max(codigo) AS max_codigo FROM " & NomeTabela
            Set rsTabelaMax = New ADODB.recordset
            rsTabelaMax.CursorLocation = adUseServer
            rsTabelaMax.Open SQLMax, gConexao, adOpenStatic
            If IsNull(rsTabelaMax("max_codigo")) Then
                EcCodigo = 1
            Else
                EcCodigo = rsTabelaMax("max_codigo") + 1
            End If
            rsTabelaMax.Close
            Set rsTabelaMax = Nothing
            EcUtilizadorCriacao = gCodUtilizador
            EcDataCriacao = Bg_DaData_ADO
            EcHrCri = Bg_DaHora_ADO
            ' Fim - Obtem numero sequencial
            BD_Insert
        End If
    
    End If
End Sub

Sub BD_Insert()
    Dim SQLQuery As String

    SQLQuery = "SELECT " & ChaveBD & " FROM " & NomeTabela & " WHERE " & ChaveBD & " = " & ChaveEc
    Set rsTabela = New ADODB.recordset
    rsTabela.CursorLocation = adUseServer
    rsTabela.Open SQLQuery, gConexao, adOpenStatic
    If rsTabela.RecordCount <> 0 Then
        BG_Mensagem mediMsgBox, "J� existe registo com esse c�digo !", vbExclamation, "Inserir"
    Else
        SQLQuery = BG_ConstroiCriterio_INSERT(NomeTabela, CamposBD, CamposEc)
        BG_ExecutaQuery_ADO SQLQuery
    End If
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        
        iRes = ValidaCamposEc
        
        If iRes = True Then
            EcUtilizadorAlteracao = gCodUtilizador
            EcDataAlteracao = Bg_DaData_ADO
            EcHrAct = Bg_DaHora_ADO
            BD_Update
        End If
    
    End If

End Sub

Sub BD_Update()
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    MarcaLocal = rsTabela.Bookmark

    condicao = ChaveBD & " = " & rsTabela(ChaveBD)
    SQLQuery = BG_ConstroiCriterio_UPDATE(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery

    rsTabela.Requery
    If Not (rsTabela.EOF) Or Not (rsTabela.BOF) Then
        rsTabela.Move MarcaLocal - 1
    End If
    LimpaCampos
    PreencheCampos
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer remover este registo ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BD_Delete
    End If
    
End Sub

Sub BD_Delete()
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    MarcaLocal = rsTabela.Bookmark

    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & ChaveBD & " = " & rsTabela(ChaveBD)
    BG_ExecutaQuery_ADO SQLQuery

    rsTabela.Requery
    If rsTabela.BOF And rsTabela.EOF Then
        BG_Mensagem mediMsgBox, "O registo apagado era �nico nesta pesquisa!", vbExclamation, "Remover"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    rsTabela.Move MarcaLocal
    If rsTabela.EOF Then rsTabela.MovePrevious
    
    LimpaCampos
    PreencheCampos
End Sub

Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub


Private Sub EcRuaPostal_Validate(Cancel As Boolean)
    
    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset
    
    EcRuaPostal.Text = UCase(EcRuaPostal.Text)
    If Trim(EcCodPostal) <> "" Then
        If Trim(EcRuaPostal) <> "" Then
            Postal = Trim(EcCodPostal.Text) & "-" & Trim(EcRuaPostal.Text)
        Else
            Postal = EcCodPostal
        End If
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              gBD_PREFIXO_TAB & "cod_postal " & _
              "WHERE " & _
              "     cod_postal=" & BL_TrataStringParaBD(Postal)
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        
        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrPostal = ""
            EcRuaPostal = ""
            EcCodPostal = ""
        Else
            EcCodigoPostal.Text = Postal
            EcDescrPostal.Text = rsCodigo!descr_postal
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrPostal.Text = ""
        EcCodigoPostal.Text = ""
        EcCodPostal.Text = ""
    End If
    
End Sub
Private Sub EcCodPostal_Change()
    
    EcCodPostal.Text = UCase(EcCodPostal.Text)

End Sub
Private Sub BtPesqCodPostal_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    
    If Trim(EcDescrPostal) = "" And EcCodPostal = "" Then
        BG_Mensagem mediMsgBox, "Preencha a descri��o com uma localidade (ou parte da descri��o de uma), para limitar a lista de c�digos postais.", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
        Exit Sub
    End If
    
    PesqRapida = False
    
    If EcDescrPostal <> "" Then
        ChavesPesq(1) = "descr_postal"
        CamposEcran(1) = "descr_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Descri��o"
        
        ChavesPesq(2) = "cod_postal"
        CamposEcran(2) = "cod_postal"
        Tamanhos(2) = 2000
        Headers(2) = "C�digo"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        ChavesPesq(1) = "cod_postal"
        CamposEcran(1) = "cod_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Codigo"
        
        ChavesPesq(2) = "descr_postal"
        CamposEcran(2) = "descr_postal"
        Tamanhos(2) = 2000
        Headers(2) = "Descr��o"
    End If
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = gBD_PREFIXO_TAB & "cod_postal"
    If EcDescrPostal <> "" And EcCodPostal = "" Then
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        CWhere = "UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    Else
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
        CWhere = CWhere & " AND UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    End If
    CampoPesquisa = "descr_postal"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " C�digos Postais")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            i = InStr(1, resultados(2), "-") - 1
            If i = -1 Then
                i = InStr(1, resultados(1), "-") - 1
            End If
            If EcDescrPostal <> "" Then
                If i = -1 Then
                    s1 = resultados(2)
                    s2 = ""
                Else
                    s1 = Mid(resultados(2), 1, i)
                    s2 = Mid(resultados(2), InStr(1, resultados(2), "-") + 1)
                End If
                EcCodPostal.Text = s1
                EcRuaPostal.Text = s2
                EcCodigoPostal.Text = Trim(resultados(2))
                EcDescrPostal.Text = resultados(1)
                EcDescrPostal.SetFocus
            ElseIf EcDescrPostal = "" Then
                If i = -1 Then
                    s1 = resultados(1)
                    s2 = ""
                Else
                    s1 = Mid(resultados(1), 1, i)
                    s2 = Mid(resultados(1), InStr(1, resultados(1), "-") + 1)
                End If
                EcCodPostal.Text = s1
                EcRuaPostal.Text = s2
                EcCodigoPostal.Text = Trim(resultados(1))
                EcDescrPostal.Text = resultados(2)
                EcDescrPostal.SetFocus
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "C�digo postal n�o encontrado!", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
    End If

    
End Sub



Sub PreencheCodigoPostal()
    
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodigoPostal.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal = " & BL_TrataStringParaBD(EcCodigoPostal)
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo do c�digo postal inexistente!", vbExclamation, App.ProductName
            EcCodigoPostal.Text = ""
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.Text = s1
            EcRuaPostal.Text = s2
            EcDescrPostal.Text = Trim(rsCodigo!descr_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub



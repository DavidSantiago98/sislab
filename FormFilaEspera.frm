VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormFilaEspera 
   BackColor       =   &H00FFE0C7&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Fila de espera para colheita"
   ClientHeight    =   10425
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15210
   ForeColor       =   &H8000000F&
   Icon            =   "FormFilaEspera.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10425
   ScaleWidth      =   15210
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFE0C7&
      Caption         =   " Fila de Espera "
      Height          =   7095
      Left            =   120
      TabIndex        =   2
      Top             =   1560
      Width           =   12975
      Begin MSComctlLib.ListView LvUtente 
         Height          =   6735
         Left            =   120
         TabIndex        =   48
         Top             =   240
         Width           =   12615
         _ExtentX        =   22251
         _ExtentY        =   11880
         View            =   3
         Arrange         =   1
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   0
      End
   End
   Begin VB.CommandButton BtPesquisaSala 
      Height          =   315
      Left            =   12720
      Picture         =   "FormFilaEspera.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   52
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
      Top             =   1200
      Width           =   375
   End
   Begin VB.TextBox EcDescrSalaPesq 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFE0C7&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2040
      Locked          =   -1  'True
      TabIndex        =   51
      TabStop         =   0   'False
      Top             =   1200
      Width           =   10695
   End
   Begin VB.TextBox EcCodSalaPesq 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1320
      TabIndex        =   50
      Top             =   1200
      Width           =   735
   End
   Begin SISLAB.CabecalhoResultados CabecalhoResultados1 
      Height          =   975
      Left            =   240
      TabIndex        =   49
      Top             =   120
      Width           =   14895
      _ExtentX        =   22251
      _ExtentY        =   1720
   End
   Begin VB.TextBox EcEtqNCopias 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   41
      Top             =   10080
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNEsp 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   40
      Top             =   10320
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqTipo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   39
      Top             =   11040
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPrinterEtiq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   38
      Top             =   9840
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNTubos 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   37
      Top             =   10560
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNAdm 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   36
      Top             =   10800
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcRegisto 
      Height          =   285
      Left            =   6840
      TabIndex        =   35
      Top             =   9960
      Width           =   855
   End
   Begin VB.TextBox EcOrdem 
      Height          =   285
      Left            =   6840
      TabIndex        =   34
      Top             =   9600
      Width           =   855
   End
   Begin VB.TextBox EcProcessed 
      Height          =   285
      Left            =   5880
      TabIndex        =   33
      Top             =   9480
      Width           =   855
   End
   Begin VB.TextBox EcMorada 
      Height          =   285
      Left            =   5880
      TabIndex        =   32
      Top             =   9600
      Width           =   855
   End
   Begin VB.TextBox EcMedico 
      Height          =   285
      Left            =   5880
      TabIndex        =   31
      Top             =   9960
      Width           =   855
   End
   Begin VB.TextBox EcEntidade 
      Height          =   285
      Left            =   4920
      TabIndex        =   30
      Top             =   9480
      Width           =   855
   End
   Begin VB.TextBox EcSexo 
      Height          =   285
      Left            =   4920
      TabIndex        =   29
      Top             =   9600
      Width           =   855
   End
   Begin VB.TextBox EcDestino 
      Height          =   285
      Left            =   4920
      TabIndex        =   28
      Top             =   9960
      Width           =   855
   End
   Begin VB.TextBox EcIdade 
      Height          =   285
      Left            =   3960
      TabIndex        =   27
      Top             =   9480
      Width           =   855
   End
   Begin VB.TextBox EcDtNasc 
      Height          =   285
      Left            =   3960
      TabIndex        =   26
      Top             =   9960
      Width           =   855
   End
   Begin VB.Timer Timer2 
      Left            =   7320
      Top             =   9960
   End
   Begin VB.TextBox EcLocked 
      Height          =   285
      Left            =   3960
      TabIndex        =   25
      Top             =   9600
      Width           =   855
   End
   Begin VB.Frame Frame4 
      BackColor       =   &H00FFE0C7&
      Height          =   1695
      Left            =   13200
      TabIndex        =   24
      Top             =   6960
      Width           =   1935
      Begin VB.CommandButton BtColheita 
         Enabled         =   0   'False
         Height          =   600
         Left            =   1080
         Picture         =   "FormFilaEspera.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   56
         ToolTipText     =   "Colheita"
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton BtIdentif 
         Enabled         =   0   'False
         Height          =   600
         Left            =   360
         Picture         =   "FormFilaEspera.frx":1260
         Style           =   1  'Graphical
         TabIndex        =   55
         ToolTipText     =   "Identifica��o"
         Top             =   960
         Width           =   615
      End
      Begin VB.CommandButton BtGestReq 
         Enabled         =   0   'False
         Height          =   600
         Left            =   1080
         Picture         =   "FormFilaEspera.frx":1F2A
         Style           =   1  'Graphical
         TabIndex        =   54
         ToolTipText     =   "Requisi��o"
         Top             =   960
         Width           =   615
      End
      Begin VB.CommandButton BtListaChamar 
         Height          =   600
         Left            =   360
         Picture         =   "FormFilaEspera.frx":2BF4
         Style           =   1  'Graphical
         TabIndex        =   0
         ToolTipText     =   "Visualiza lista de espera"
         Top             =   240
         Width           =   600
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00FFE0C7&
      Caption         =   "Prioridades"
      Height          =   3495
      Left            =   13200
      TabIndex        =   15
      Top             =   1080
      Width           =   1935
      Begin VB.PictureBox Picture1 
         BackColor       =   &H00FFE0C7&
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   120
         Picture         =   "FormFilaEspera.frx":38BE
         ScaleHeight     =   615
         ScaleWidth      =   495
         TabIndex        =   19
         Top             =   390
         Width           =   495
      End
      Begin VB.PictureBox Picture4 
         BackColor       =   &H00FFE0C7&
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   120
         Picture         =   "FormFilaEspera.frx":4588
         ScaleHeight     =   615
         ScaleWidth      =   495
         TabIndex        =   18
         Top             =   2685
         Width           =   495
      End
      Begin VB.PictureBox Picture3 
         BackColor       =   &H00FFE0C7&
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   120
         Picture         =   "FormFilaEspera.frx":5252
         ScaleHeight     =   615
         ScaleWidth      =   495
         TabIndex        =   17
         Top             =   1965
         Width           =   495
      End
      Begin VB.PictureBox Picture2 
         BackColor       =   &H00FFE0C7&
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   120
         Picture         =   "FormFilaEspera.frx":5F1C
         ScaleHeight     =   615
         ScaleWidth      =   495
         TabIndex        =   16
         Top             =   1125
         Width           =   495
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   " Desconhecida"
         Height          =   615
         Index           =   0
         Left            =   600
         TabIndex        =   23
         Top             =   390
         Width           =   1215
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Crian�a"
         Height          =   495
         Index           =   3
         Left            =   600
         TabIndex        =   22
         Top             =   2760
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Priorit�ria"
         Height          =   615
         Index           =   2
         Left            =   600
         TabIndex        =   21
         Top             =   1965
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Normal"
         Height          =   615
         Index           =   1
         Left            =   600
         TabIndex        =   20
         Top             =   1125
         Width           =   1095
      End
   End
   Begin VB.TextBox EcUserAct 
      Height          =   285
      Left            =   2040
      TabIndex        =   14
      Top             =   9480
      Width           =   855
   End
   Begin VB.TextBox EcUserCri 
      Height          =   285
      Left            =   1080
      TabIndex        =   13
      Top             =   9480
      Width           =   855
   End
   Begin VB.TextBox EcPrioridade 
      Height          =   285
      Left            =   120
      TabIndex        =   12
      Top             =   9480
      Width           =   855
   End
   Begin MSComctlLib.ImageList imlSmallIcons 
      Left            =   7800
      Top             =   9960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormFilaEspera.frx":6BE6
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormFilaEspera.frx":6F80
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormFilaEspera.frx":731A
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormFilaEspera.frx":76B4
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.TextBox EcNome 
      Height          =   285
      Left            =   3000
      TabIndex        =   11
      Top             =   9480
      Width           =   855
   End
   Begin VB.PictureBox PictureList 
      Height          =   375
      Left            =   7800
      ScaleHeight     =   315
      ScaleWidth      =   435
      TabIndex        =   10
      Top             =   9600
      Width           =   495
   End
   Begin VB.TextBox EcHrCri 
      Height          =   285
      Left            =   2040
      TabIndex        =   9
      Top             =   9960
      Width           =   855
   End
   Begin VB.TextBox EcDtCri 
      Height          =   285
      Left            =   1080
      TabIndex        =   8
      Top             =   9960
      Width           =   855
   End
   Begin VB.Timer Timer1 
      Left            =   6840
      Top             =   9960
   End
   Begin VB.TextBox EcSeqUtente 
      Height          =   285
      Left            =   3000
      TabIndex        =   7
      Top             =   9960
      Width           =   855
   End
   Begin VB.TextBox EcHrAct 
      Height          =   285
      Left            =   3000
      TabIndex        =   6
      Top             =   9600
      Width           =   855
   End
   Begin VB.TextBox EcDtAct 
      Height          =   285
      Left            =   2040
      TabIndex        =   5
      Top             =   9600
      Width           =   855
   End
   Begin VB.TextBox EcCodSala 
      Height          =   285
      Left            =   1080
      TabIndex        =   4
      Top             =   9600
      Width           =   855
   End
   Begin VB.TextBox EcCodLocal 
      Height          =   285
      Left            =   120
      TabIndex        =   3
      Top             =   9600
      Width           =   855
   End
   Begin VB.TextBox EcReq 
      Height          =   285
      Left            =   120
      TabIndex        =   1
      Top             =   9960
      Width           =   855
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFE0C7&
      Caption         =   "Sala"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   14
      Left            =   240
      TabIndex        =   53
      Top             =   1200
      Width           =   1155
   End
   Begin VB.Label Labe41 
      Caption         =   "EcEtqNCopias"
      Enabled         =   0   'False
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   47
      Top             =   10080
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Labe41 
      Caption         =   "EcEtqNEsp"
      Enabled         =   0   'False
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   46
      Top             =   10320
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqTipo"
      Enabled         =   0   'False
      Height          =   255
      Index           =   15
      Left            =   120
      TabIndex        =   45
      Top             =   11040
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcPrinterEtiq"
      Enabled         =   0   'False
      Height          =   255
      Index           =   12
      Left            =   120
      TabIndex        =   44
      Top             =   9840
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqNTubos"
      Enabled         =   0   'False
      Height          =   255
      Index           =   16
      Left            =   120
      TabIndex        =   43
      Top             =   10560
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqNAdm"
      Enabled         =   0   'False
      Height          =   255
      Index           =   18
      Left            =   120
      TabIndex        =   42
      Top             =   10800
      Visible         =   0   'False
      Width           =   1095
   End
End
Attribute VB_Name = "FormFilaEspera"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

''''''''''''''''''''''''''''''''''''''''''''
' Actualiza��o: 30/03/2007 '''''''''''''''''
' T�cnico: Paulo Ferreira ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''

''''''''''''''''''''''''''''''''''''''''''''
' Constantes de cor ''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Const CC_GRAY = &HE0E0E0
Private Const CC_FORMGRAY = &H8000000F
Private Const CC_LIGHTBLUE = &HFFD3A4
Private Const CC_LIGHTYELLOW = &H80000018



''''''''''''''''''''''''''''''''''''''''''''
' Constantes de processamento ''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Const CP_UNPROCESSED = 0
Private Const CP_PROCESSED = 1

''''''''''''''''''''''''''''''''''''''''''''
' Constantes de bloqueio '''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Const CL_UNLOCKED = 0
Private Const CL_LOCKED = 1


''''''''''''''''''''''''''''''''''''''''''''
' Constantes de posicao ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Const CPOS_HIGH = 6
Private Const CPOS_HIGHEST = 3


''''''''''''''''''''''''''''''''''''''''''''
' Constantes de chamada ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Const CA_CALLIN = 0
Private Const CA_CALLBACK = 1


''''''''''''''''''''''''''''''''''''''''''''
' Variaveis para acesso a BD '''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Dim NumCampos As Integer
Dim CamposBD() As String
Dim CamposEc() As Object
Dim ChaveBD As String
Dim ChaveEc As Object
Dim NomeTabela As String
Dim CriterioTabela As String
Public rs As ADODB.recordset



''''''''''''''''''''''''''''''''''''''''''''
' Descricao da sala corrente '''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Dim DescrSala As String

''''''''''''''''''''''''''''''''''''''''''''
' Modo de chamada ''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Dim CALL_MODE As Integer

''''''''''''''''''''''''''''''''''''''''''''
' Numero corrente de elementos das lista '''
''''''''''''''''''''''''''''''''''''''''''''
Dim N_LIST As Integer


''''''''''''''''''''''''''''''''''''''''''''
' Evento de botao actulizar ''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BtActualiza_Click()
    
    LimpaCampos
    
    ' Procura novamente (consoante o modo de chamada) para refrescar a lista
    Select Case CALL_MODE
        Case CA_CALLIN: FuncaoProcurar
        Case CA_CALLBACK: FuncaoProcurar
    End Select
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de botao chamar '''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BtChamar_Click()
    
    ' Efectua chamada se existir seleccao do utente
    If (LvUtente.ListItems.Count > 0) Then
        CallUtente LvUtente.SelectedItem.Index, CA_CALLIN
        BtActualiza_Click
    End If
End Sub


Private Sub BtColheita_Click()
    If LvUtente.ListItems.Count > 0 Then
        gColheitaChegada = gEnumColheitaChegada.ColheitaTubo
        FormChegadaPorTubosV2.Show
        Set gFormActivo = FormChegadaPorTubosV2
        FormChegadaPorTubosV2.EcNumReq = gEstrutUteFilaEspera(LvUtente.SelectedItem.Index).n_req
        FormChegadaPorTubosV2.FuncaoProcurar
    End If
End Sub

Private Sub BtGestReq_Click()
    If LvUtente.SelectedItem.Index <= UBound(gEstrutUteFilaEspera) Then
        If gTipoInstituicao = "PRIVADA" Then
            FormGestaoRequisicaoPrivado.Show
            Set gFormActivo = FormGestaoRequisicaoPrivado
            FormGestaoRequisicaoPrivado.EcNumReq = gEstrutUteFilaEspera(LvUtente.SelectedItem.Index).n_req
            FormGestaoRequisicaoPrivado.FuncaoProcurar
        ElseIf gTipoInstituicao = "HOSPITALAR" Then
            FormGestaoRequisicao.Show
            Set gFormActivo = FormGestaoRequisicao
            FormGestaoRequisicao.EcNumReq = gEstrutUteFilaEspera(LvUtente.SelectedItem.Index).n_req
            FormGestaoRequisicao.FuncaoProcurar
        ElseIf gTipoInstituicao = gTipoInstituicaoVet Then
            FormGestaoRequisicaoVet.Show
            Set gFormActivo = FormGestaoRequisicaoVet
            FormGestaoRequisicaoVet.EcNumReq = gEstrutUteFilaEspera(LvUtente.SelectedItem.Index).n_req
            FormGestaoRequisicaoVet.FuncaoProcurar
        End If
    End If
End Sub


Private Sub BtIdentif_Click()
    Dim i As Long
    If LvUtente.SelectedItem.Index <= UBound(gEstrutUteFilaEspera) Then
        If gTipoInstituicao = gTipoInstituicaoVet Then
            FormIdentificaVet.Show
            Set gFormActivo = FormIdentificaVet
            FormIdentificaVet.EcCodSequencial = gEstrutUteFilaEspera(LvUtente.SelectedItem.Index).seq_utente
            FormIdentificaVet.FuncaoProcurarPesquisa
        Else
            FormIdentificaUtente.Show
            Set gFormActivo = FormIdentificaUtente
            FormIdentificaUtente.EcCodSequencial = gEstrutUteFilaEspera(LvUtente.SelectedItem.Index).seq_utente
            FormIdentificaUtente.FuncaoProcurar
        End If
    End If
End Sub



''''''''''''''''''''''''''''''''''''''''''''
' Visualiza utentes por chamar '''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BtListaChamar_Click()
    
    ' Se o modo de procura for o de chamada sai da funcao
    If (CALL_MODE = CA_CALLIN) Then
        FuncaoProcurar
    Else
        ' Processamento do form
        ResetTimer Timer1
        Timer2.Enabled = True
        CALL_MODE = CA_CALLIN
        
        ' Limpa a estrutura e lista e procura utentes por chamar
        FuncaoLimpar
        FuncaoProcurar
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Visualiza utentes chamados '''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BtListaRetroceder_Click()
    
    ' Se o modo de procura for o de retrocesso sai da funcao
    If (CALL_MODE = CA_CALLBACK) Then: Exit Sub
    
    ' Processamento do form
    ResetTimer Timer1
    Timer2.Enabled = False
    CALL_MODE = CA_CALLBACK
    
    ' Limpa a estrutura e lista e procura utentes chamados
    FuncaoLimpar
    FuncaoProcurar
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento do botao retroceder chamada '''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BtRetroceder_Click()
    
    ' Efectua retrocesso se existir seleccao do utente
    If (LvUtente.ListItems.Count > 0) Then
        CallUtente LvUtente.SelectedItem.Index, CA_CALLBACK
        BtActualiza_Click
    End If
End Sub





''''''''''''''''''''''''''''''''''''''''''''
' Entrada neste form '''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Sub Form_Load()
    EventoLoad
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Form activo ''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Sub Form_Activate()
    EventoActivate
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Saida neste form '''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Inicializacoes deste form ''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Sub Inicializacoes()

    ' Propriedades do form
    Me.caption = " Fila de espera para colheita"
    Me.left = 50
    Me.top = 50
    Me.Width = 15300
    Me.Height = 9180
    
    
    ' Activa recordset
    Set rs = New ADODB.recordset
    
    ' Inicia lista
    InitiateList
    
    ' Parametros do timer 1
    Timer1.Enabled = True
    Timer1.Interval = 65000
    
    ' Parametros do timer 2
    Timer2.Enabled = True
    Timer2.Interval = 10000
    
    ' Definicao por defeito
    CALL_MODE = CA_CALLIN
    
    ' Gere acesso dos controlos do ecra
    ManageControlsPermissions
    
    
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Define propriedades da lista '''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub InitiateList()
    
    With LvUtente
        .ColumnHeaders.Add(, , "Requisi��o", 1000).Tag = "REQUISICAO"
        .ColumnHeaders.Add(, , "Nome Utente", 5250).Tag = "NOME"
        .ColumnHeaders.Add(, , "Data", 2000).Tag = "DATA"
        .ColumnHeaders.Add(, , "Operador", 2000).Tag = "OPERADOR"
        .ColumnHeaders.Add(, , "Nova Colheita", 2000).Tag = "NOVACOLHEITA"
        .ColumnHeaders(1).Alignment = lvwColumnLeft
        .ColumnHeaders(2).Alignment = lvwColumnLeft
        .ColumnHeaders(3).Alignment = lvwColumnLeft
        .ColumnHeaders(4).Alignment = lvwColumnLeft
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .SmallIcons = imlSmallIcons
        .AllowColumnReorder = False
        .FullRowSelect = True
    End With
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de entrada do form ''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    ' Verifica se a sala ja esta definida
    VerifySala
        
    ' Padrao
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
    ' Procura utentes
    FuncaoProcurar
    
    ' Carrega labels da legenda
    LoadLegendLabels
    gF_FILA_ESPERA = mediSim
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento form activo '''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Sub EventoActivate()

    ' Padrao
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de saida do form ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Sub EventoUnload()
    
    ' Padrao
    LimpaCampos
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    If rs.state = adStateOpen Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormFilaEspera = Nothing
    Timer1.Enabled = False
    Timer2.Enabled = False
    
    ' Coloca codigo da sala a nulo
    DescrSala = ""
    gCodSala = Empty
    gF_FILA_ESPERA = mediNao

End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Limpa campos do form '''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Sub LimpaCampos()
    
    ' Padrao
    Dim campo As Variant
    Me.SetFocus

    ' Limpa estrutura e lista de utentes
    gTotalFilaEspera = 0
    ReDim gEstrutUteFilaEspera(0)
    LvUtente.ListItems.Clear
    
    ' Limpa numero corrente de elementos
    N_LIST = 0
    CabecalhoResultados1.LimpaCampos &HFFE0C7
    PreencheSalaDefeito
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Define campos do form ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Sub DefTipoCampos()
    

    
    EcDtCri.Tag = adDate
    EcDtAct.Tag = adDate
    EcDtNasc.Tag = adDate
    CabecalhoResultados1.LimpaCampos &HFFE0C7
    PreencheSalaDefeito
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Procura utentes em fila de espera ''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub FuncaoProcurar()
    Dim SQLQuery As String
    On Error GoTo Trata_Erro
    N_LIST = 0
    gSQLError = 0
    If EcCodSalaPesq.Text = "" Then
'        BG_Mensagem mediMsgBox, "Ter� que estar associado a uma sala .", vbCritical + vbOKOnly, App.ProductName
        EcCodSalaPesq.Locked = False
        BtPesquisaSala.Enabled = True
        Exit Sub
    Else
        EcCodSalaPesq.Locked = True
        BtPesquisaSala.Enabled = False
    End If
    ' Suspende timer
    Timer2.Enabled = False
    ' Altera o cursor so rato (espera)
    Me.MousePointer = vbHourglass
    ' Selecciona todos os utentes em fila de espera
    SQLQuery = FE_DevolveQueryFilaEspera(CALL_MODE, EcCodSalaPesq.Text)
    LvUtente.ListItems.Clear
    ' Executa procura
    If SQLQuery <> "" Then
        rs.Open SQLQuery, gConexao, adOpenStatic, adLockOptimistic
    Else
        Exit Sub
    End If
    ' Se ocorrer algum erro trata o erro
    If gSQLError <> 0 Then
        GoTo Trata_Erro
    
    ' Senao preenche a lista de utentes
    Else
        ' Actualiza numero de utentes na lista
        N_LIST = N_LIST + rs.RecordCount
        ' Preenche a estrutura/lista
        If (rs.RecordCount > 0) Then: FillList rs
        ' Pinta a lista
        ' Coloca texto na frame da lista
        SetFrameCaption N_LIST
        ' Actualiza ordem na bd
        
    End If
    
    If (Me.Visible = True) Then: LvUtente.SetFocus
    ' Fecha conexao
    If (rs.state = adStateOpen) Then: rs.Close
    ' Altera o cursor so rato (normal) e sai da funcao
    Me.MousePointer = vbArrow
    ' Activa timer
    If (CALL_MODE = CA_CALLIN) Then: Timer2.Enabled = True
        
    Exit Sub
       
Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro ao procurar requisi��es!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormFilaEspera: BD_Select -> " & Err.Description
    BG_RollbackTransaction
    If (rs.state = adStateOpen) Then: rs.Close
    Me.MousePointer = vbArrow
    Exit Sub
    Resume Next
End Sub


''''''''''''''''''''''''''''''''''''''''''''
' Define o texto da frame da lista '''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub SetFrameCaption(nList As Integer)
    
    Dim Text As String
    
    ' Verifica qual o modo de chamada corrente
    If (CALL_MODE = CA_CALLIN) Then: Text = " Fila de espera " & "(" & nList & ")"
    If (CALL_MODE = CA_CALLBACK) Then: Text = "Fila de utentes chamados " & "(" & nList & ")"
    
    ' Associa o texto a frame
    Me.Frame2.caption = Text
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Verifica se existe nova entrada na fila ''
''''''''''''''''''''''''''''''''''''''''''''
Private Function IsToRefresh() As Boolean
    
    Dim sql As String
    Dim RsFila As ADODB.recordset
    Set RsFila = New ADODB.recordset
    
    ' Procura utentes em espera
'    sql = "SELECT * FROM sl_fila_espera WHERE " & CamposBD(CI_DTACT) & " is null AND " & CamposBD(CI_HRACT) & " is null"
'    RsFila.Open sql, gConexao, adOpenStatic, adLockOptimistic
'
'    ' Se o numero de utentes em espera for maior do que a lista actual entao retorna true
'    If (RsFila.RecordCount > UBound(gEstrutUteFilaEspera) - 1) Then: IsToRefresh = True
'
'    ' Fecha conexao
'    If (RsFila.state = adStateOpen) Then: RsFila.Close
End Function




Private Sub LvUtente_Click()
    If LvUtente.ListItems.Count = 0 Then Exit Sub
    If LvUtente.SelectedItem.Index <= UBound(gEstrutUteFilaEspera) Then
        CabecalhoResultados1.ProcuraDados gEstrutUteFilaEspera(LvUtente.SelectedItem.Index).n_req
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento que recebe duplo-click na lista '''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub LvUtente_DblClick()
    If LvUtente.ListItems.Count = 0 Then Exit Sub
    ' Condiciona acesso consoante utilizador
    If (CALL_MODE = CA_CALLIN And gCodGrupo = cTecnico And LvUtente.SelectedItem.Index <> 1) Then: Exit Sub
    'BtActualiza_Click
    ' Efectua chamada do utente seleccionado
    If (LvUtente.ListItems.Count > 0) Then
        If (CALL_MODE = CA_CALLIN) Then: CallUtente LvUtente.SelectedItem.Index, CA_CALLIN
        If (CALL_MODE = CA_CALLBACK) Then: CallUtente LvUtente.SelectedItem.Index, CA_CALLBACK
        'FuncaoLimpar
        'FuncaoProcurar
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que procede a chamada do utente '''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub CallUtente(Index As Integer, Optional modo As Integer)
 
    Dim iRes As Integer
    Dim sala As String
    
    ' Define a caixa de mensagem de acordo com o modo de chamada
    If (modo = CA_CALLIN) Then
        
        BG_BeginTransaction
        ' Verifica se o utente esta bloqueado
        If (VerifyLock(Index) = True) Then
            BG_RollbackTransaction
            Exit Sub
        End If
        BloqueiaRegisto Index
        BG_CommitTransaction
        gMsgTitulo = " Chamada de utente"
        gMsgMsg = "Tem a certeza que deseja dar chamada do utente " & vbCrLf & _
            UCase(gEstrutUteFilaEspera(Index).nome & " [" & BG_CalculaIdade(CDate(gEstrutUteFilaEspera(Index).dt_nasc))) & "]?"
    
    ElseIf (modo = CA_CALLBACK) Then
        gMsgTitulo = " Retroceder a chamada de utente"
        gMsgMsg = "Tem a certeza que deseja retroceder a chamada do utente " & gEstrutUteFilaEspera(Index).nome & "?"
    End If
    
    ' Confirma a resposta do utilizador
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton1 + vbQuestion, gMsgTitulo)

    ' Se confirmado chama ou retrocede a chamada
    If gMsgResp = vbYes Then
            
        ' Suspende timer
        Timer1.Enabled = False
        ' Processa chamada
        If (modo = CA_CALLIN) Then
            ChamaUtente Index
            
        ElseIf (modo = CA_CALLBACK) Then
            'BD_Update Index, , modo
        End If
        ' Retoma timer
        Timer1.Enabled = True
    Else
        If (modo = CA_CALLIN) Then
            DesBloqueiaRegisto Index
        End If
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento que recebe o enter sobre a lista ''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub LvUtente_KeyDown(KeyCode As Integer, Shift As Integer)
    
    ' Se 'Enter' efectua chamada do utente seleccionado
    If (KeyCode = 13 And Shift = 0 And CALL_MODE = CA_CALLIN) Then
        If (LvUtente.ListItems.Count > 0) Then
            ' Condiciona acesso consoante utilizador
            If (CALL_MODE = CA_CALLIN And gCodGrupo = cTecnico And LvUtente.SelectedItem.Index <> 1) Then: Exit Sub
            CallUtente LvUtente.SelectedItem.Index, CA_CALLIN
        End If
    
    ' Se 'Delete' retrocede a chamda do utente
    ElseIf (KeyCode = 46 And Shift = 0 And CALL_MODE = CA_CALLBACK) Then
        If (LvUtente.ListItems.Count > 0) Then: CallUtente LvUtente.SelectedItem.Index, CA_CALLBACK
        BtActualiza_Click
    End If
End Sub



Private Sub Text1_Change()

End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento do intervalo do timer '''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub timer1_Timer()
    
    ' Se existe nova entrada efectua procura
    If gCodSala > 0 Then
        If (NewListArrive) Then: Call FuncaoProcurar
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao limpar do form ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoLimpar()
    LimpaCampos
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Preenche a lista de utentes em espera ''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub FillList(RsStack As ADODB.recordset)
    
    ' Preenche estrutura
    Call FE_PopulateEstrutura(RsStack, EcCodSalaPesq.Text)
    
    LvUtente.ListItems.Clear
    PopulatetList
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Efectua reset do timer1 ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub ResetTimer(MyTimer As Timer)

    MyTimer.Enabled = False
    MyTimer.Enabled = True
End Sub

Private Sub ChamaUtente(indice As Integer)
    Dim res As Boolean
    res = FE_AtualizaEstadoFilaEspera(indice, gEstadoFilaEspera.concluido, EcCodSalaPesq.Text)
    If res = True Then
        
        If gRegistaColheitaAtiva = mediSim Then
            BtColheita_Click
        End If
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Verifica se a sala foi definida no pc ''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub VerifySala()

    Dim rsSala As ADODB.recordset
    Set rsSala = New ADODB.recordset
    DescrSala = "{Sala Desconhecida}"
    
    
    ' Procura descricao da sala para o codigo gCodSala
    rsSala.Open "SELECT descr_sala FROM sl_cod_salas WHERE seq_sala=" & gCodSala, gConexao, adOpenStatic, adLockReadOnly
    
    ' Se encontrou guarda a descricao
    If (rsSala.RecordCount > 0) Then: DescrSala = BL_HandleNull(rsSala!descr_sala, "{Sala Desconhecida}")
    
    ' Se o recordset estiver aberto fecha-o
    If (rsSala.state = adStateOpen) Then: rsSala.Close
End Sub



''''''''''''''''''''''''''''''''''''''''''''
' Efectua leitura do codigo da sala ''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Timer2_Timer()
    
    Dim RsLock As ADODB.recordset
    Set RsLock = New ADODB.recordset
    
    ' Se a lista estiver vazia sai da funcao
    If (Not LvUtente.ListItems.Count > 0) Then: Exit Sub
    
    ' Procura utentes bloqueados
    RsLock.Open "SELECT* FROM sl_fila_espera WHERE trunc(dt_cri) = " & BL_TrataDataParaBD(Bg_DaData_ADO), gConexao, adOpenStatic, adLockReadOnly
    
    ' Se existir actualiza locks
    If (RsLock.RecordCount > 0) Then: UpdateLocks RsLock
    
    ' Se a conexao estiver aberta fecha-a
    If (RsLock.state = adStateOpen) Then: RsLock.Close
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Actualiza locks dos utentes ''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub UpdateLocks(RsLock As ADODB.recordset)

    ' Actualiza locks para todos os utentes retornados
    While (Not RsLock.EOF)
        UnOrLockUtente BL_HandleNull(RsLock!n_req, ""), _
                       BL_HandleNull(RsLock!seq_utente, ""), _
                       BL_HandleNull(RsLock!flg_estado, CL_UNLOCKED)
        RsLock.MoveNext
    Wend
    
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Efectua lock/unlock ao utente'''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub UnOrLockUtente(NReq As String, SeqUtente As String, CI_LOCK As Integer)
    
    Dim i As Integer
    
    ' Se encontrar o utente faz o respectivo lock/ unlock
    For i = 1 To UBound(gEstrutUteFilaEspera)
    
        ' Se encontrar utente
        If (gEstrutUteFilaEspera(i).n_req = NReq And _
            gEstrutUteFilaEspera(i).seq_utente = SeqUtente) Then
            
            ' Efectua lock/unlock
            If (CI_LOCK = gEstadoFilaEspera.bloqueado) Then: LvUtente.ListItems(i).ForeColor = vbRed
            If (CI_LOCK = gEstadoFilaEspera.concluido) Then: LvUtente.ListItems(i).ForeColor = vbRed
            If (CI_LOCK = gEstadoFilaEspera.cancelado) Then: LvUtente.ListItems(i).ForeColor = vbRed
            If (CI_LOCK = gEstadoFilaEspera.espera) Then: LvUtente.ListItems(i).ForeColor = vbBlack
            
            ' Actualiza campo da estrutura
            'gEstrutUteFilaEspera(i).locked = CI_LOCK
            Exit Sub
        End If
    Next
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Verifica se o utente esta bloqueado ''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Function VerifyLock(Index As Integer) As Boolean
    Dim sSql As String
    Dim rsLocked As New ADODB.recordset
    
    sSql = "SELECT flg_estado FROM sl_fila_espera where n_req = " & gEstrutUteFilaEspera(Index).n_req & " FOR UPDATE OF sl_fila_espera.flg_estado "
    rsLocked.CursorLocation = adUseServer
    rsLocked.CursorType = adOpenForwardOnly
    rsLocked.LockType = adLockReadOnly
    rsLocked.Open sSql, gConexao
    
    ' Se o utentes estiver bloqueado ent�o avisa o utilizador
    If (BL_HandleNull(rsLocked!flg_estado, 0) = gEstadoFilaEspera.bloqueado) Then
        BG_Mensagem mediMsgBox, "Este utente encontra-se bloqueado!", vbExclamation, "Chamada de utente"
        VerifyLock = True
        Exit Function
    ElseIf (BL_HandleNull(rsLocked!flg_estado, 0) = gEstadoFilaEspera.concluido) Then
        BG_Mensagem mediMsgBox, "Utente j� atendido!", vbExclamation, "Chamada de utente"
        VerifyLock = True
        Exit Function
    ElseIf (BL_HandleNull(rsLocked!flg_estado, 0) = gEstadoFilaEspera.cancelado) Then
        BG_Mensagem mediMsgBox, "Utente cancelado!", vbExclamation, "Chamada de utente"
        VerifyLock = True
        Exit Function

    End If
    
    VerifyLock = False
End Function

''''''''''''''''''''''''''''''''''''''''''''
' Escreve as descricoes das prioridades ''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub LoadLegendLabels()

    Dim RsPrio As ADODB.recordset
    Set RsPrio = New ADODB.recordset
    Dim rows As Variant
    
    ' Selecciona as descricoes das prioriades e extrai para um vector
    With RsPrio
        .Open "SELECT nome_prio FROM sl_cod_prioridades ORDER BY cod_prio", _
                gConexao, adOpenStatic, adLockReadOnly
        rows = .GetRows
    End With
    
    ' Escreve os valores nas labels
    If (RsPrio.RecordCount > 3) Then: Label1(3).caption = rows(0, 3)
    If (RsPrio.RecordCount > 2) Then: Label1(2).caption = rows(0, 2)
    If (RsPrio.RecordCount > 1) Then: Label1(1).caption = rows(0, 1)
    If (RsPrio.RecordCount > 0) Then: Label1(0).caption = rows(0, 0)
    
    ' Fecha recordset
    If (RsPrio.state = adStateOpen) Then: RsPrio.Close
End Sub


''''''''''''''''''''''''''''''''''''''''''''
' Insere utentes na lista de espera ''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub PopulatetList()

    Dim Index As Integer
    
    ' Percorre a estrutura
    For Index = 1 To UBound(gEstrutUteFilaEspera)
        
        ' Adiciona o utente a lista
        With LvUtente.ListItems.Add(, , gEstrutUteFilaEspera(Index).n_req, , CInt(gEstrutUteFilaEspera(Index).prioridade))
            .ListSubItems.Add , , gEstrutUteFilaEspera(Index).nome
            .ListSubItems.Add , , gEstrutUteFilaEspera(Index).dt_estado
            .ListSubItems.Add , , BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", gEstrutUteFilaEspera(Index).user_estado, "V")
            .ListSubItems.Add , , gEstrutUteFilaEspera(Index).hora_nova_colheita
        End With
        
        ' Insere tooltip em cada linha da lista
        LvUtente.ListItems.item(Index).ToolTipText = gEstrutUteFilaEspera(Index).seq_utente
        If LvUtente.ListItems.Count >= 1 Then
            LvUtente.ListItems.item(1).Selected = True
            LvUtente_Click
        End If
    Next
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Define posicao do utente na lista ''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Function GetListPosition(prioridade As Integer) As Integer
        
    Dim position As Integer
    Dim currentLen As Integer
    
    ' Inicializacao de variaveis auxiliares
    currentLen = UBound(gEstrutUteFilaEspera)
    position = currentLen + 1
    
    ' Processamento difere para cada caso
    Select Case prioridade
            
        Case FE_UNKNOWN, FE_NORMAL
        
            ' Retorna ultima posicao
            position = currentLen + 1
            
        Case FE_HIGH
            
            ' Decide a posicao do prioritario CR_HIGH
            position = DecidePriotityPosition(FE_HIGH, CPOS_HIGH, currentLen)
            
        Case FE_HIGHEST
            
            ' Decide a posicao do prioritario CR_HIGHEST
            position = DecidePriotityPosition(FE_HIGHEST, CPOS_HIGHEST, currentLen)
    End Select
    
    ' Retorna posicao
    GetListPosition = position
End Function

''''''''''''''''''''''''''''''''''''''''''''
' Decide qual a posicao do utente na lista '
''''''''''''''''''''''''''''''''''''''''''''
Private Function DecidePriotityPosition(prioritiy As Integer, reference, currentLen As Integer) As Integer

    Dim Index As Integer
    Dim position As Integer
        
    ' Procura ultima posicao do utente com mesma prioridade
    For Index = currentLen To 0 Step -1
        If (gEstrutUteFilaEspera(Index).prioridade = CStr(prioritiy)) Then: Exit For
    Next
            
    ' Se nao encontrar retorna a primeira posicao definida para a prioridade
    If (Index = -1) Then
        If (currentLen >= reference) Then: position = reference
    
    ' Se encontrar numa posicao anterior a referencia retorna a propria referencia
    ElseIf (Index < reference And reference < currentLen) Then
        position = reference
    
    ' Senao retorna a ultima posicao encontrada mais 1
    Else
        position = Index + 1
    End If
    
    ' Se a posicao nao estiver definida retorna a ultima posicao da lista
    DecidePriotityPosition = IIf(position = 0, currentLen + 1, position)
End Function


''''''''''''''''''''''''''''''''''''''''''''
' Gere as permissoes de controlo do ecra '''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub ManageControlsPermissions()

    Dim Control As Control
    Dim bPermission As Boolean
    
    ' Por defeito
    bPermission = True
    
    ' Define o valor da propriedade 'Enabled'
    'If (gCodGrupo = cTecnico) Then: bPermission = False
    
    ' Procura os controlos do tipo botao e define a propriedade 'Enabled'
    For Each Control In Me.Controls
        If (TypeOf Control Is CommandButton) Then: Control.Enabled = bPermission
    Next
End Sub






''''''''''''''''''''''''''''''''''''''''''''
' Verifica se existe nova entrada na fila ''
''''''''''''''''''''''''''''''''''''''''''''
Private Function NewListArrive() As Boolean

    Dim sql As String
    Dim RsFila As ADODB.recordset
    Set RsFila = New ADODB.recordset
    
    ' String de procura
    sql = "select * from sl_fila_espera where flg_proc = " & CP_UNPROCESSED
    
    With RsFila
        ' Executa procura
        .Open sql, gConexao, adOpenStatic, adLockOptimistic
        ' Retorna true || false
        NewListArrive = .RecordCount > 0
        ' Fecha conexao
        If (.state = adStateOpen) Then: .Close
    End With
End Function



Private Sub BloqueiaRegisto(indice As Integer)
    FE_AtualizaEstadoFilaEspera indice, gEstadoFilaEspera.bloqueado, EcCodSalaPesq.Text
End Sub
Private Sub DesBloqueiaRegisto(indice As Integer)
    FE_AtualizaEstadoFilaEspera indice, gEstadoFilaEspera.espera, EcCodSalaPesq.Text
End Sub

Private Sub BtPesquisaSala_Click()
    PA_PesquisaSala EcCodSalaPesq, EcDescrSalaPesq, ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaSala_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaSala_Click"
    Exit Sub
    Resume Next
End Sub



Public Sub EcCodsalaPEsq_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    Cancel = PA_ValidateSala(EcCodSalaPesq, EcDescrSalaPesq, "")
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodsalaPEsq_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodsalaPEsq_Validate"
    Exit Sub
    Resume Next
End Sub



' --------------------------------------------------------

' UTILIZADORES DOS POSTOS PREENCHE POR DEFEITO A SALA

' --------------------------------------------------------
Private Sub PreencheSalaDefeito()
     On Error GoTo TrataErro
   EcCodSalaPesq.Enabled = True
    BtPesquisaSala.Enabled = True
    If gCodSalaAssocUser > 0 Then
        EcCodSalaPesq = gCodSalaAssocUser
        EcCodsalaPEsq_Validate True
        EcCodSalaPesq.Enabled = False
        BtPesquisaSala.Enabled = False
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheSalaDefeito: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheSalaDefeito"
    Exit Sub
    Resume Next
End Sub



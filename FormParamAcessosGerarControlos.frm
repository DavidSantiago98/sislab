VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form FormParamAcessosGerarControlos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormParamAcessosGerarControlos"
   ClientHeight    =   1020
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8625
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1020
   ScaleWidth      =   8625
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtAnalisar 
      Caption         =   "Analisar"
      Height          =   615
      Left            =   3600
      TabIndex        =   3
      Top             =   120
      Width           =   1215
   End
   Begin VB.CommandButton BtGerarForms 
      Caption         =   "GerarForms"
      Height          =   615
      Left            =   240
      TabIndex        =   2
      Top             =   120
      Width           =   1215
   End
   Begin VB.CommandButton BtGerarMenus 
      Caption         =   "Gerar Menus"
      Height          =   615
      Left            =   1560
      TabIndex        =   1
      Top             =   120
      Width           =   1455
   End
   Begin RichTextLib.RichTextBox FRM 
      Height          =   855
      Left            =   2520
      TabIndex        =   0
      Top             =   7320
      Visible         =   0   'False
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   1508
      _Version        =   393217
      TextRTF         =   $"FormParamAcessosGerarControlos.frx":0000
   End
   Begin MSComDlg.CommonDialog ProcuraDir 
      Left            =   2400
      Top             =   1080
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Pesquisar Forms"
      FileName        =   "*.frm"
      Filter          =   "*.frm"
      InitDir         =   "C:\"
   End
End
Attribute VB_Name = "FormParamAcessosGerarControlos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim forms() As String
Dim TotalForms As Integer

Private Type Controlos
    Form As String
    tipo As String
    nome As String
    caption As String
    Index As Integer
End Type
Dim ordem As Long
Dim EstrutControlos() As Controlos
Dim totalEstrutControlos As Long

Sub TrataFrm(FichFRM As String, Form As String)
    Dim aux As String
    Dim pos As Single
    Dim pos2 As Single
    Dim pos3 As Single
    Dim pos4 As Long
    Dim pos5 As Long
    Dim pos6 As Long
    Dim PosCodigo As Single
    Dim nomeForm As Boolean
    Dim indice As Integer
    
    Dim Objecto As String
    Dim caption As String
    
    On Error GoTo Trata_Erro
    
    nomeForm = False
    
    FRM.text = ""
    FRM.LoadFile FichFRM, rtfText
    
    aux = FRM.text
    
    If InStr(1, aux, "Begin VB.MDIForm ") > 0 Then
        pos = InStr(1, aux, "Begin VB.MDIForm ")
        pos2 = InStr(pos + 17, aux, vbCrLf)
        Form = UCase(Trim(Mid(aux, pos + 17, pos2 - (pos + 17))))
    Else
        pos = InStr(1, aux, "Begin VB.Form")
        pos2 = InStr(pos + 13, aux, vbCrLf)
        Form = UCase(Trim(Mid(aux, pos + 13, pos2 - (pos + 13))))
    End If
    pos = InStr(1, aux, "Begin ")
    While pos <> 0
        PosCodigo = InStr(1, aux, "Attribute VB_Name")
        
        '"Begin " N�o se encontra no codigo ?
        If PosCodigo > pos Then
            pos2 = InStr(pos, aux, " ")
            pos2 = InStr(pos2 + 1, aux, " ")
            pos3 = InStr(pos2 + 1, aux, vbNewLine)
            Objecto = UCase(Trim(Mid(aux, pos2, pos3 - pos2)))
            pos4 = InStr(pos2 + 1, aux, "End")
            caption = ""
            indice = -1
            If InStr(pos2 + 1, aux, "Caption") < pos4 Then
                pos5 = InStr(pos2 + 1, aux, """")
                pos6 = InStr(pos5 + 1, aux, """")
                caption = Mid(aux, pos5 + 1, pos6 - (pos5 + 1))
            End If
            If InStr(pos2 + 1, aux, " Index") < pos4 And InStr(pos2 + 1, aux, " Index") <> 0 Then
                pos5 = InStr(InStr(pos2 + 1, aux, "Index") + 1, aux, "=")
                pos6 = InStr(pos5 + 1, aux, vbCrLf)
                indice = CInt(Trim(Mid(aux, pos5 + 1, pos6 - (pos5 + 1))))
            End If
            totalEstrutControlos = totalEstrutControlos + 1
            ReDim Preserve EstrutControlos(totalEstrutControlos)
            EstrutControlos(totalEstrutControlos).Form = Form
            EstrutControlos(totalEstrutControlos).nome = Objecto
            EstrutControlos(totalEstrutControlos).caption = caption
            EstrutControlos(totalEstrutControlos).Index = indice
        End If
        aux = Mid(aux, pos + 1)
        pos = InStr(1, aux, "Begin ")
    Wend

    
Exit Sub
Trata_Erro:
    Resume Next
    
End Sub




Private Sub Grava_BD()
    Dim sSql As String
    Dim i As Integer
    sSql = "DELETE FROM SL_PARAM_ACESS_OBJECTOS"
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To totalEstrutControlos
        Me.caption = i
        DoEvents
        If gSGBD = gOracle Then
            sSql = "INSERT INTO sl_param_acess_objectos (SEQ_OBJECTO,FORM, OBJECTO, CAPTION, INDICE) VALUES( seq_param_acess_objecto.nextval, "
            sSql = sSql & BL_TrataStringParaBD(EstrutControlos(i).Form) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutControlos(i).nome) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutControlos(i).caption) & ", "
            sSql = sSql & EstrutControlos(i).Index & ") "
        Else
            sSql = "INSERT INTO sl_param_acess_objectos (FORM, OBJECTO, CAPTION, INDICE) VALUES(  "
            sSql = sSql & BL_TrataStringParaBD(EstrutControlos(i).Form) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutControlos(i).nome) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutControlos(i).caption) & ", "
            sSql = sSql & EstrutControlos(i).Index & ") "
        End If
        BG_ExecutaQuery_ADO sSql
    Next
End Sub

Private Sub BtAnalisar_Click()
    Dim origem As String
    Dim destino As String
    On Error GoTo TrataErro
    ProcuraDir.FileName = ""
    ProcuraDir.Filter = "Visual Basic Project|*.vpb"
    ProcuraDir.DialogTitle = "Seleccionar Projecto (*.vbp)"
    ProcuraDir.flags = cdlOFNExplorer
    ProcuraDir.MaxFileSize = 10000
    ProcuraDir.InitDir = "C:\"
    ProcuraDir.ShowOpen
    origem = ProcuraDir.FileName
    
    ProcuraDir.FileName = ""
    ProcuraDir.Filter = "*.xlsx|*.xls"
    ProcuraDir.DialogTitle = "Ficheiro Excel Destino"
    ProcuraDir.flags = cdlOFNExplorer
    ProcuraDir.MaxFileSize = 10000
    ProcuraDir.InitDir = "C:\"
    ProcuraDir.ShowOpen
    destino = ProcuraDir.FileName
    If Dir(destino) <> "" Then
        gMsgResp = MsgBox("A informa��o contida no ficheiro actual ser� perdida!" & vbCrLf & "Deseja proseguir?", vbQuestion + vbYesNo)
        If gMsgResp = vbYes Then
            Kill destino
        Else
            Exit Sub
        End If
    End If
    
    Analisa origem, destino
    
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' GERA MENUS PARA BD

' ----------------------------------------------------------------------------------

Private Sub BtGerarMenus_Click()
    On Error GoTo TrataErro
    ProcuraDir.FileName = "MDIFInic.frm"
    ProcuraDir.Filter = "Visual Basic Form|*.frm"
    ProcuraDir.DialogTitle = "Seleccionar Projecto (*.frm)"
    ProcuraDir.flags = cdlOFNExplorer
    ProcuraDir.MaxFileSize = 10000
    ProcuraDir.InitDir = "C:\"
    
    ProcuraDir.ShowOpen
    If ProcuraDir.FileName <> "*.FRM" Then
        TrataMENU ProcuraDir.FileName
    End If
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' GERA FORMS E CONTROLOS PARA BD

' ----------------------------------------------------------------------------------

Private Sub BtGerarForms_Click()
    On Error GoTo TrataErro
    ProcuraDir.FileName = "SISLAB.vbp"
    ProcuraDir.Filter = "Visual Basic Project|*.vpb"
    ProcuraDir.DialogTitle = "Seleccionar Projecto (*.vbp)"
    ProcuraDir.flags = cdlOFNExplorer
    ProcuraDir.MaxFileSize = 10000
    ProcuraDir.InitDir = "C:\"
    
    ProcuraDir.ShowOpen
    If ProcuraDir.FileName <> "*.VBP" Then
        TrataVBP ProcuraDir.FileName
    End If
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub


' ----------------------------------------------------------------------------------

' PARSE DO FICHEIRO VBP PARA RETORNAR OS FORMS

' ----------------------------------------------------------------------------------

Sub TrataVBP(FichVBP As String)
    Dim linhas() As String
    Dim iFile As Long
    Dim i As Long
    Dim sSql As String
    On Error GoTo TrataErro
    
    sSql = "DELETE FROM " & gBD_PREFIXO_TAB & "forms"
    BG_ExecutaQuery_ADO sSql
    
    sSql = "DELETE FROM " & gBD_PREFIXO_TAB & "objectos"
    BG_ExecutaQuery_ADO sSql
    ReDim linhas(0)
    iFile = FreeFile
    Open FichVBP For Input As #iFile
        Do While Not EOF(iFile)
            ReDim Preserve linhas(UBound(linhas) + 1)
            Line Input #iFile, linhas(UBound(linhas))
        Loop
    Close #iFile
    ordem = 0
    For i = 1 To UBound(linhas)
        If InStr(1, linhas(i), "Form=") = 1 Then
            InsereControlosForms Replace(FichVBP, "sislab.vbp", Replace(linhas(i), "Form=", ""))
            InsereForm linhas(i), Replace(FichVBP, "sislab.vbp", Replace(linhas(i), "Form=", ""))
        ElseIf InStr(1, linhas(i), "Module=") >= 1 Then
            InsereMensagensModule Mid(linhas(i), InStr(1, linhas(i), ";") + 2)
        End If
    Next
Exit Sub

TrataErro:
    Exit Sub
    Resume Next
    
End Sub

' ----------------------------------------------------------------------------------

' PARSE DO FICHEIRO MDIFORMINI PARA TRATAR OS MENUS

' ----------------------------------------------------------------------------------

Sub TrataMENU(FichFRM As String)
    Dim linhas() As String
    Dim iFile As Long
    Dim i As Long
    Dim sSql As String
    On Error GoTo TrataErro
    
    sSql = "DELETE FROM " & gBD_PREFIXO_TAB & "menus"
    BG_ExecutaQuery_ADO sSql
    ReDim linhas(0)
    iFile = FreeFile
    Open FichFRM For Input As #iFile
        Do While Not EOF(iFile)
            ReDim Preserve linhas(UBound(linhas) + 1)
            Line Input #1, linhas(UBound(linhas))
        Loop
    Close #iFile
    ordem = 0
    For i = 1 To UBound(linhas)
        While Not InStr(1, linhas(i), "Begin VB.Menu") = 0
            i = InsereMenu("", -1, linhas, i)

        Wend
    Next
Exit Sub

TrataErro:
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' INSERE O MENU NA BD

' ----------------------------------------------------------------------------------

Private Function InsereMenu(CodPai As String, indicePai As Long, linhas() As String, i As Long) As Long
    Dim sSql As String
    Dim j As Long
    Dim nome As String
    Dim indice As Long
    Dim caption As String
    Dim ord As Integer
    Dim id_texto As Long
    On Error GoTo TrataErro
    
    ord = ordem
    ordem = ordem + 1
    nome = Trim(Replace(linhas(i), "Begin VB.Menu", ""))
    indice = -1
    For j = i + 1 To UBound(linhas)
        id_texto = mediComboValorNull
        If InStr(1, linhas(j), "Caption         =") > 0 Then
            caption = Trim(Replace(linhas(j), "Caption         =", ""))
        ElseIf InStr(1, linhas(j), "Index           =") > 0 Then
            indice = CLng(Trim(Replace(linhas(j), "Index           =", "")))
        ElseIf InStr(1, linhas(j), "Begin VB.Menu") > 0 Then
            j = InsereMenu(nome, indice, linhas, j)
        ElseIf Trim(linhas(j)) = "End" Then
            InsereMenu = j
            id_texto = DIC_DevolveIndiceTexto(Replace(caption, """", ""))
            sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "menus (cod_pai,indice_cod_pai,codigo,indice,descricao, ordem, id_texto) VALUES("
            sSql = sSql & BL_TrataStringParaBD(CodPai) & ", "
            sSql = sSql & BL_TrataStringParaBD(CStr(indicePai)) & ", "
            sSql = sSql & BL_TrataStringParaBD(Replace(nome, """", "")) & ", "
            sSql = sSql & indice & ", "
            sSql = sSql & BL_TrataStringParaBD(Replace(caption, """", "")) & ", "
            sSql = sSql & ord & ", "
            If id_texto = mediComboValorNull Then
                sSql = sSql & "Null)"
            Else
                sSql = sSql & id_texto & ") "
            End If
            BG_ExecutaQuery_ADO sSql
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' INSERE O FORM NA BD

' ----------------------------------------------------------------------------------

Private Sub InsereForm(linha As String, caminhoForm As String)
    Dim sSql As String
    Dim j As Long
    Dim nome As String
    Dim iFile As Long
    Dim linhas2() As String
    Dim i As Long
    On Error GoTo TrataErro
    
    ReDim linhas2(0)
    iFile = FreeFile
    Open caminhoForm For Input As #iFile
        Do While Not EOF(iFile)
            ReDim Preserve linhas2(UBound(linhas2) + 1)
            Line Input #iFile, linhas2(UBound(linhas2))
        Loop
    Close #iFile
    
    linha = ""
    For i = 1 To UBound(linhas2)
        If InStr(1, linhas2(i), "Begin VB.Form ") > 0 Then
            linha = Trim(Replace(linhas2(i), "Begin VB.Form ", ""))
            Exit For
        End If
    Next
    If linha <> "" Then
        sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "forms (cod_form) VALUES("
        sSql = sSql & BL_TrataStringParaBD(Replace(linha, """", "")) & ") "
        BG_ExecutaQuery_ADO sSql
    End If
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' INSERE TODOS CONTROLOS DO FORM NA BD (PARSE DE CADA FORM)

' ----------------------------------------------------------------------------------


Private Sub InsereControlosForms(Form As String)
    Dim sSql As String
    Dim i As Long
    Dim iFile As Long
    Dim linhas() As String
    Dim nomeForm As String
    Dim ProcActual As String
    Dim inicio As Integer
    Dim fim As Integer
    Dim flgInsereControlo As Boolean
    ReDim linhas(0)
    On Error GoTo TrataErro
    
    If InStr(1, Form, "MDIFInic") > 0 Or InStr(1, Form, "MDIFormInicio") > 0 Then
        Exit Sub
    End If
    iFile = FreeFile
    
    Open Form For Input As #iFile
        Do While Not EOF(iFile)
            ReDim Preserve linhas(UBound(linhas) + 1)
            Line Input #iFile, linhas(UBound(linhas))
        Loop
    Close #iFile
    i = 1
    While InStr(1, linhas(i), "Begin VB.Form") = 0
        
        i = i + 1
    Wend
    ' APAGA TODOS OS OBJECTOS DO FORM
    nomeForm = Trim(Replace(linhas(i), "Begin VB.Form ", ""))
    sSql = "DELETE FROM " & gBD_PREFIXO_TAB & "objectos WHERE cod_form =" & BL_TrataStringParaBD(nomeForm)
    BG_ExecutaQuery_ADO sSql
    
    'ApagaTextoAux nomeForm
    flgInsereControlo = True
    For i = i + 1 To UBound(linhas)
        inicio = 0
        fim = 0
        If InStr(1, linhas(i), "Begin ") > 0 And flgInsereControlo = True Then
            i = InsereControlos2(nomeForm, linhas, i)
        ElseIf InStr(1, linhas(i), "Attribute VB_Name =") > 0 Then
            flgInsereControlo = False
        ElseIf InStr(1, linhas(i), "Sub ") > 0 And InStr(1, linhas(i), "(") > 0 And InStr(1, linhas(i), ")") > 0 Then
            inicio = InStr(1, linhas(i), "Sub ") + 4
            fim = InStr(1, linhas(i), "(")
            ProcActual = Trim(Mid(linhas(i), inicio, fim - inicio))
        ElseIf InStr(1, linhas(i), "Function ") > 0 And InStr(1, linhas(i), "(") > 0 And InStr(1, linhas(i), ")") > 0 Then
            inicio = InStr(1, linhas(i), "Function ") + 9
            fim = InStr(1, linhas(i), "(")
            ProcActual = Trim(Mid(linhas(i), inicio, fim - inicio))
        'MENSAGEM - BG_MENSAGEM
        ElseIf InStr(1, linhas(i), "BG_Mensagem") > 0 Then
            If InStr(1, Replace(linhas(i), """...""", ""), """") > 0 Then
                'InsereTextoAux nomeForm, ProcActual, "Mensagem", i, Linhas(i)
            End If
        ' MENSAGEM - MSGBOX
        ElseIf InStr(1, linhas(i), " MsgBox") > 0 Then
                'InsereTextoAux nomeForm, ProcActual, "MsgBox", i, Linhas(i)
        ' MENSAGEM -gMsgTitulo
        ElseIf InStr(1, linhas(i), "gMsgTitulo =") > 0 Then
                'InsereTextoAux nomeForm, ProcActual, "gMsgTitulo", i, Linhas(i)
        ' MENSAGEM -gMsgMsg
        ElseIf InStr(1, linhas(i), "gMsgMsg =") > 0 Then
                'InsereTextoAux nomeForm, ProcActual, "gMsgMsg", i, Linhas(i)
        ' FlexGrid - TextMatrix
        ElseIf InStr(1, UCase(linhas(i)), ".TEXTMATRIX(") > 0 Then
            If InStr(1, linhas(i), """") > 0 Then
                'InsereTextoAux nomeForm, ProcActual, "FlexGrid", i, Linhas(i)
            End If
        ' Treeviews - Nodes.add
        ElseIf InStr(1, UCase(linhas(i)), "NODES.ADD") > 0 Then
            If InStr(1, linhas(i), """") > 0 Then
                'InsereTextoAux nomeForm, ProcActual, "Nodes.Add", i, Linhas(i)
            End If
        ' ListView - ColumnHeaders.Add
        ElseIf InStr(1, UCase(linhas(i)), "COLUMNHEADERS.ADD") > 0 Then
            If InStr(1, linhas(i), """") > 0 Then
                'InsereTextoAux nomeForm, ProcActual, "ColumnHeaders.Add", i, Linhas(i)
            End If
        'Campos Obrigatorios
        ElseIf InStr(1, linhas(i), "TextoCamposObrigatorios(") > 0 Then
            If InStr(1, linhas(i), """") > 0 Then
                'InsereTextoAux nomeForm, ProcActual, "CampoObrigatorio", i, Linhas(i)
            End If
        'Caption dos Forms
        ElseIf InStr(1, linhas(i), "Me.caption =") > 0 Then
            If InStr(1, linhas(i), """") > 0 Then
                'InsereTextoAux nomeForm, ProcActual, "Me.caption", i, Linhas(i)
            End If
        End If
    Next
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

Private Sub InsereMensagensModule(modulo As String)
    Dim i As Long
    Dim iFile As Long
    Dim linhas() As String
    Dim ProcActual As String
    Dim inicio As Integer
    Dim fim As Integer
    Dim flgInsereControlo As Boolean
    
    On Error GoTo TrataErro
    iFile = FreeFile
    ReDim linhas(0)
    Open modulo For Input As #iFile
        Do While Not EOF(iFile)
            ReDim Preserve linhas(UBound(linhas) + 1)
            Line Input #iFile, linhas(UBound(linhas))
        Loop
    Close #iFile
    i = 1
    
    For i = i + 1 To UBound(linhas)
        inicio = 0
        fim = 0
        If InStr(1, linhas(i), "Sub ") > 0 And InStr(1, linhas(i), "(") > 0 And InStr(1, linhas(i), ")") > 0 Then
            inicio = InStr(1, linhas(i), "Sub ") + 4
            fim = InStr(1, linhas(i), "(")
            ProcActual = Trim(Mid(linhas(i), inicio, fim - inicio))
        ElseIf InStr(1, linhas(i), "Function ") > 0 And InStr(1, linhas(i), "(") > 0 And InStr(1, linhas(i), ")") > 0 Then
            inicio = InStr(1, linhas(i), "Function ") + 9
            fim = InStr(1, linhas(i), "(")
            ProcActual = Trim(Mid(linhas(i), inicio, fim - inicio))
        'MENSAGEM - BG_MENSAGEM
        ElseIf InStr(1, linhas(i), "BG_Mensagem") > 0 Then
            If InStr(1, Replace(linhas(i), """...""", ""), """") > 0 Then
                'InsereTextoAux Modulo, ProcActual, "Mensagem", i, Linhas(i)
            End If
        ' MENSAGEM - MSGBOX
        ElseIf InStr(1, linhas(i), " MsgBox") > 0 Then
                'InsereTextoAux Modulo, ProcActual, "MsgBox", i, Linhas(i)
        ' MENSAGEM -gMsgTitulo
        ElseIf InStr(1, linhas(i), "gMsgTitulo =") > 0 Then
                'InsereTextoAux Modulo, ProcActual, "gMsgTitulo", i, Linhas(i)
        ' MENSAGEM -gMsgMsg
        ElseIf InStr(1, linhas(i), "gMsgMsg =") > 0 Then
                'InsereTextoAux Modulo, ProcActual, "gMsgMsg", i, Linhas(i)
        ' CONSTANTES
        ElseIf InStr(1, linhas(i), "Const ") > 0 And InStr(1, linhas(i), """") > 0 Then
                'InsereTextoAux Modulo, ProcActual, "CONST", i, Linhas(i)
        End If
    Next
    
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' INSERE CADA CONTROLO NA BD

' ----------------------------------------------------------------------------------
Private Function InsereControlos2(nomeForm As String, linhas() As String, i As Long) As Long
    Dim sSql As String
    Dim j As Long
    Dim nome As String
    Dim tipoObj As String
    Dim indice As Long
    Dim caption As String
    Dim id_texto_caption As Long
    Dim toolTip As String
    Dim id_texto_tooltip As Long
    Dim colunas() As String
    On Error GoTo TrataErro
    
    ReDim colunas(0)
    nome = ""
    indice = -1
    tipoObj = ""
    caption = ""
    For j = i To UBound(linhas)
        id_texto_caption = mediComboValorNull
        id_texto_tooltip = mediComboValorNull
        ReDim colunas(0)
        If InStr(1, Trim(linhas(j)), "Begin ") > 0 And nome = "" Then
            BL_Tokenize linhas(j), " ", colunas
            nome = colunas(2)
            indice = -1
            tipoObj = Replace(colunas(1), "VB.", "")
        ElseIf InStr(1, Trim(linhas(j)), "Caption ") > 0 And nome <> "" And caption = "" Then
            caption = Trim(Replace(Mid(linhas(j), InStr(1, linhas(j), "=", vbTextCompare) + 1), """", ""))
        ElseIf InStr(1, Trim(linhas(j)), "ToolTipText ") > 0 And nome <> "" And caption = "" Then
            toolTip = Trim(Replace(Mid(linhas(j), InStr(1, linhas(j), "=", vbTextCompare) + 1), """", ""))
        ElseIf InStr(1, Trim(linhas(j)), " Text ") > 0 And nome <> "" And caption = "" Then
            caption = Trim(Replace(Mid(linhas(j), InStr(1, linhas(j), "=", vbTextCompare) + 1), """", ""))
        ElseIf InStr(1, Trim(linhas(j)), "Begin ") > 0 And nome <> "" Then
            j = InsereControlos2(nomeForm, linhas, j)
        ElseIf InStr(1, Trim(linhas(j)), "Index ") = 1 Then
            indice = Trim(Replace(linhas(j), "Index           =   ", ""))
        ElseIf Trim(linhas(j)) = "End" Then
            id_texto_caption = DIC_DevolveIndiceTexto(caption)
            id_texto_tooltip = DIC_DevolveIndiceTexto(toolTip)
            sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "Objectos (cod_form,tipo,cod_objecto,indice_objecto,id_texto_caption,id_texto_tooltip) VALUES("
            sSql = sSql & BL_TrataStringParaBD(nomeForm) & ", "
            sSql = sSql & BL_TrataStringParaBD(tipoObj) & ", "
            sSql = sSql & BL_TrataStringParaBD(nome) & ", "
            sSql = sSql & indice & ", "
            sSql = sSql & Replace(id_texto_caption, mediComboValorNull, "NULL") & ","
            sSql = sSql & Replace(id_texto_tooltip, mediComboValorNull, "NULL") & ")"
            BG_ExecutaQuery_ADO sSql
            nome = ""
            indice = -1
            tipoObj = ""
            InsereControlos2 = j
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    Exit Function
    Resume Next
End Function

Private Sub Analisa(fichOrigem As String, fichDestino As String)
    On Error GoTo TrataErro
    Dim FolhaExcel As Object
    Dim fFicheiroExcel As Object
    Dim linhas() As String
    Dim iFile As Long
    Dim i As Long
    Dim sSql As String
    
    ReDim linhas(0)
    iFile = FreeFile
    
    Open fichOrigem For Input As #iFile

    
    With CreateObject("Excel.application")
        On Error GoTo TrataErro
        With .Workbooks.Add
            Set fFicheiroExcel = .Worksheets(1)
            fFicheiroExcel.Activate
            ProcessaVBP iFile, linhas, fFicheiroExcel
            Set FolhaExcel = Nothing
            Set fFicheiroExcel = Nothing

            .SaveAs fichDestino, , , , , , , , False
        End With
        .Quit
    End With
    Close #iFile
    
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

Private Sub ProcessaVBP(iFile As Long, linhas() As String, fFicheiroExcel As Object)
    Dim tokens() As String
    On Error GoTo TrataErro
    
    fFicheiroExcel.cells(1, 1) = "Descri��o"
    fFicheiroExcel.cells(1, 2) = "Valor"
    
    Do While Not EOF(iFile)
        ReDim Preserve linhas(UBound(linhas) + 1)
        Line Input #iFile, linhas(UBound(linhas))
        BL_Tokenize linhas(UBound(linhas)), "=", tokens()
        fFicheiroExcel.cells(UBound(linhas) + 1, 1) = tokens(0)
        fFicheiroExcel.cells(UBound(linhas) + 1, 2) = tokens(1)
    Loop
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

Private Sub InsereTextoAux(nomeForm As String, metodo As String, tipo As String, linha As Long, descr_linha As String)
    On Error GoTo TrataErro
    Dim sSql As String
    sSql = "INSERT INTO sl_texto_aux (nome_form, metodo, tipo, linha, descr_linha) VALUES( "
    sSql = sSql & BL_TrataStringParaBD(nomeForm) & ","
    sSql = sSql & BL_TrataStringParaBD(metodo) & ","
    sSql = sSql & BL_TrataStringParaBD(tipo) & ","
    sSql = sSql & linha & ","
    sSql = sSql & BL_TrataStringParaBD(descr_linha) & ")"
    BG_ExecutaQuery_ADO sSql
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

Private Sub ApagaTextoAux(nomeForm As String)
    On Error GoTo TrataErro
    Dim sSql As String
    sSql = "DELETE FROM  sl_texto_aux WHERE nome_form= " & BL_TrataStringParaBD(nomeForm)
    BG_ExecutaQuery_ADO sSql
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub


Attribute VB_Name = "VariaveisGlobais"
Option Explicit

' Actualiza��o : 27/03/2003
' T�cnico Paulo Costa

'*-------------------*
'| Vari�veis Globais |
'*-------------------*

' Constantes -----------------------------------------------

' Skin.
Global Const cANASIBAS = "ANASIBAS"
Global Const cSISLAB = "SISLAB"

' Sistema de factura��o.
Global Const cFACTURACAO_FACTUS = "FACTUS"
Global Const cFACTURACAO_GH = "GH"
Global Const cFACTURACAO_SONHO = "SONHO"
Global Const cFACTURACAO_NOVAHIS = "NOVAHIS"

' ----------------------------------------------------------

Global gLAB As String


' Laborat�rios

Global Const cHSMARTA = "HSMARTA"
Global Const cLABJM = "LABJM"
Global Const cHUCs = "HUCS"
Global Const cHPPs = "HPPS"
Global Const cIGM = "IGM"
Global Const cHPOVOA = "HPOVOA"
Global Const cCHAM_PL = "CHAM_PL" ' Centro Hospitalar do Alto Minho - Ponte de Lima
Global Const cCHVNG = "CHVNG"

' ----------------------------------------------------------

Global cAPLICACAO_NOME_CURTO As String
Global cAPLICACAO_NOME As String
Global cAPLICACAO_AUTOR As String

Global Const cREGISTRY_NOME = "SISLAB"
Global Const cINI_ODBC = "odbc.ini"

Global Const cMAX_JANELAS_ABERTAS = 20

' As constantes come�adas por `medi� n�o s�o precedidas de `c�
' assumindo que o `medi� indica uma constante
Global Const mediTipoDefeito = 0
Global Const mediTipoMaiusculas = 101
Global Const mediTipoMinusculas = 102
Global Const mediTipoCapitalizado = 103
Global Const mediTipoData = 104
Global Const mediTipoHora = 105

Global Const mediComboValorNull = -1

Global Const mediMsgBox = 0
Global Const mediMsgStatus = 1

Global Const mediMsgPermanece = 500
Global Const mediMsgBeep = 1000

Global Const mediSim = 1
Global Const mediNao = 0

Global Const mediOn = 1
Global Const mediOff = 0

Global Const mediAsc = 1
Global Const mediDesc = 0

Global Const mediAscComboCodigo = 1
Global Const mediDescComboCodigo = 0
Global Const mediAscComboDesignacao = 3
Global Const mediDescComboDesignacao = 2
'edgar.parada Glintt-HS-19949 *LOTE 97
Global Const mediCodConvencao = 1
'
Global Const mediMP_Activo = vbArrow
Global Const mediMP_Espera = vbHourglass

Global Const mediAmbitoGeral = 0      ' Para aceder � tabela parametriza��es
Global Const mediAmbitoComputador = 1 ' Para aceder � tabela parametriza��es
Global Const mediAmbitoUtilizador = 2 ' Para aceder � tabela parametriza��es

Global Const mediValorNulo = "NULL"

' Factura��o : Estados de requisi��o.
Global Const factPorEnviarFact = 0
Global Const factEnviadaFact = 1

'Variaveis Globais para Parametrizacao
Global Const cParamAmbiente = "sl_paramet"

'Variaveis Globais para Parametrizacao de Acesso
Global Const cParamAcessos = "sl_param_acess"

' for�ar a defini��o de data a usar nos acessos � BD
Global Const cFormatoDataBD = "dd-MM-yyyy"
' for�ar a defini��o de separador decimal a usar nos acessos � BD
Global Const cFormatoDecBD = "."

'Outros Erros
Global Const cItemNotFoundInThisCollection = 3265

' Vari�veis ------------------------------------------------

'Vers�o da aplica��o (carregada com as defeni��es de compila��o do VB)
Global gAPLICACAO_VERSAO As String

'identifica��o do sistema gestor de base de dados
Global gSGBD As String
'identifica��o do sistema gestor de base de dados SECUNDARIA
Global gSGBD_SECUNDARIA As String

Global gVersaoSGBD As String
'password da base de dados
Global gBDPwd As String
'flag de activ��o da vers�o demo
Global gDEMO As String
' Indica��o da directoria da aplica��o no Servidor.
Global gDirServidor As String
' Indica��o da directoria da aplica��o no Cliente.
Global gDirCliente As String
Global gBD_LocalOuRemota As String
'variavel de gConexao
Global gConexao As adodb.Connection
Global gConexaoLOGS As adodb.Connection

'data source
Global gProvider As String
Global gOracleServer As String
Global gOracleServerSec As String
Global gOracleUID As String
Global gOraclePWD As String

Global gDSN As String
Global gFileDSN As String

Global gFormActivo As Form
Global gModoDebug As Integer

Global gFormatoData As String
Global gFormatoHora As String
Global gSimboloDecimal As String

Global gIdUtilizador As String
Global gCodUtilizador As Long
Global gSenhaUtilizador As String
Global gNomeUtilizador As String
Global gTituloUtilizador As String
Global gCodGrupo As Integer
Global gCodGrAnaUtilizador As String
Global gCodGrTrabUtilizador As String
Global gProfileUtilizador As String
Global gCodUtilizadorColheita As String
Global gCodTAgendaDefeito As String

Global gMsgTitulo As String
Global gMsgMsg As String
Global gMsgResp As String

' Apenas para indicar o c�digo para a tabela de Registo de acessos
Global gCodAcesso As Long

' 1 - Parametriza��es Gerais; 2 - Parametriza��es Locais
Global gOpParamAmbiente As Integer

Global gImprimirDestino As Integer

' Stack de Janelas (s� � utilizada na BG - mantem-se a nomenclatura)
Global StackJanelas(0 To cMAX_JANELAS_ABERTAS) As Form
Global pStackJanelas As Integer ' 'Pointer' ou 'Posi��o' actual da Stack de Janelas

'Variaveis que v�o conter os erros SQL e ISAM (ISAM = informa��o adicional do erro SQL)
'V�o ser actualizadas no BG_TrataErro
Global gSQLError
Global gSQLISAM

' Skin do SISLAB.
' Ex. : SISLAB/MINISISLAB
Global gSKIN As String

' Sistema de factura��o usado.
Global gSISTEMA_FACTURACAO As String

' EFR que cont�m os pre�os ARS e serve para as outras.
Global gGrupoARS As String

' EFR que cont�m os pre�os ADSE e serve para as outras.
Global gGrupoADSE As String

' Forma como os resultados s�o inseridos.
Global Const cFormaInsercao_NORMAL = "NORMAL"
Global Const cFormaInsercao_SEGUNDO = "SEGUNDO"
Global gFormaInsercao As String

' Permite apenas valida��o M�dica.
'Global gSoValMedica As Boolean

'Arrays que v�o conter os erros nativos do Gestor de Base de Dados
Public gInsRegistoDuplo
Public gUpdRegistoDuplo
Public gInsRegistoNulo
Public gValoresInsuf
Public gChaveViolada
Public gErroSyntax

'variavel que determina o estado da transa��o
'se estiver a 1 : transa��o aberta, se estiver a 0 : transa��o fechada
Public gEstadoTransacao As Boolean

'Variavel que guarda o numero de cada sess�o aberta
Global gNumeroSessao As Long

'Estrutura que retorna todas as propriedades de um ou v�rios campos da Base de Dados
Public Type Tipo_FieldObjectProperties
    tipo As String
    Precisao As String
    EscalaNumerica As String
    TamanhoConteudo As String
    TamanhoCampo As String
    nome As String
End Type

Public gFieldObjectProperties() As Tipo_FieldObjectProperties

Public Type locais
    cod_local As String
    descr_local As String
End Type
Public gEstruturaLocais() As locais
Public gTotalLocais As Integer

'Comando e parametro utilizados para aceder � tabela de parametrizacao de ambiente
'em BG_DaParamAmbiente_ADO
Public gCmdParamAmbiente_G As New adodb.Command
Public gCmdParamAmbiente_C As New adodb.Command
Public gCmdParamAmbiente_U As New adodb.Command
Public gPmtChave As adodb.Parameter
Public gPmtAmbito As adodb.Parameter
Public gPmtComputador As adodb.Parameter
Public gPmtUtilizador As adodb.Parameter
    
'Comando e parametros utilizados para aceder � tabela de parametrizacao de acessos
'em BG_ParametrizaPermissoes_ADO
Public gCmdParamAcessos As New adodb.Command
Public gPmtForm As adodb.Parameter
Public gPmtGrupoUtil As adodb.Parameter
Public gPmtCodUtil As adodb.Parameter

' Indica se a aplica��o escreve as requisic��es na tabela do GESCOM 'gc_requisicoes'.
Public gReqGESCOM As Boolean

' Indica se o login � case sensitive.
Public gLoginCaseSensitive As Boolean

' Sistema operativo.
Public gSistemaOperativo As String

' Indica se se pode introduzir c�digos de an�lise do SONHO na gest�o de Requisi��es.
Public gMapeiaSONHO As Boolean

' ----------------------------------------------------------------------
' ERESULTS
' ----------------------------------------------------------------------

' DATA SOURCE PARA LIGAR AO ERESULTS
Public geResultsDSN As String
' UTILIZADOR BASE DADOS ERESULTS
Public geResultsUTIL As String
' PASSWORD PARA LIGAR AO ERESULTS
Public geResultsPWD As String
' CODIGO DO DOCUMENTO ORIGEM DO SISLAB
Public geResultsCodOrigem As String
' TIPO DO DOCUMENTO ORIGEM DO SISLAB
Public geResultsTipoDoc As String
' Indica se o SISLAB envia documentos para o eResults.
Public geResults As Boolean

' Indica a pasta onde s�o enviados os documentos para o eResults.
Public geResults_Pasta As String

' Impressora para PDF, para o eResults.
Public geResults_Impr_PDF As String

' Pasta local dos ficheiros para o eResults.
Public geResults_Local As String

' Pasta temp para os ficheiros do eResults.
Public geResults_Temp As String

' C�digo de origem desta aplica��o, para o eResults.
Public gER_ORIGEM As String

' Mapa de resultados simples (Relat�rio de Ensaios).
Public gER_MAPA_RES_SIMPLES As String

' Encripta��o de documentos para o eResults.
Public gENCRIPTA_DOC As Boolean

' Encripta��o de ficheiros de Indexa��o para o eResults.
Public gENCRIPTA_F_IND As Boolean

' ----------------------------------------------------------------------

' Categorias profissionais.
Global Const cMedico = "101"
Global Const cTecnico = "103"

' ----------------------------------------------------------------------

' Indica se o epis�sio fica igual ao n�mero de requisi��o por defeito.
Public gEpisodioIgualNumReq As Boolean

' N�mero de etiquetas administrativas X grupo, por defeito.
Global gNumEtiqAdmin As Integer

' Indica se, ao registar, se procura  a requisi��o mais recente com esta an�lise pendente, para este utente.
Global gProcAnPendentesAoRegistar As Integer

' Indica se FormGesReqCons est� aberto.
' Utilizado quando se sai de FormGestaoRequisicao e se volta para FormGesReqCons.

Global gFormGesReqCons_Aberto As Boolean

' Indica o esquema do GESCOM.
' Ex. : GESCOM2002.
Global gEsquemaGescom As String

' Indica o tipo de pesquisa em FormPesqRapidaAvancadaMultiSel :
' True  : LIKE 'XXXX%'
' False : LIKE '%XXXX%'
Global gPesquisaDentroCampo As Boolean

' Indica se as etiquetas v�o para a impressora ou ou preview.
Global gEtiqPreview As Boolean

' -----------------------------------------------
' Interface para os campos do tipo C�digo.
' -----------------------------------------------
Global Const cEcCampo_Alignment = vbAlignLeft
Global Const cEcCampo_BackColor = "&H00D9FFFF&"
Global Const cEcCampo_FontBold = True

' ToolTiText utilizado nos campos onde se pode utilizar * e ? para a pesquisa.
Global Const cMsgWilcards = " Pode utilizar * e ? na pesquisa "

'Indica se se pretende colocar na gest�o de requisi��es a EFR do utente
Global gPreencheEFRUtente As Integer

'Indica se se pretende colocar na gest�o de requisi��es a EFR para o episodio da GH
Global gPreencheEFREpisodioGH As Integer

' Na gest�o de requisi��es, obriga a ter um produto ao inserir ou modificar, se
' a requisi��o tiver an�lises.
Global gObrigaProduto As String

' Na gest�o de requisi��es : Bot�o para alterar o n�mero de epis�dio.
Global gBtAlteraEpisodio As Boolean

' Indica, caso preenchida, o c�digo da an�lise a inserir por defeito no SONHO em cada requisi��o
Global gCodAnaSONHO As String

' Indica se preenchem o spisodio para fins de facturacao
Global gPreencheEpis As String

'Indica se utiliza o form de chegada de tubos para dar entrada do produto e do tubo
Global gRegChegaTubos As String

' Indica o codigo da analise que N�O entra para a estatistica de tempos
Global gCodAnaLownstein As String
 
' Indica o Codigo do grupo de Microbiologia que n�o entra para a estatistica de tempos
Global gCodGrupoMicrobiologia As String
Global gCodGrupoMicobacteriologia As String
' Indica o C�digo de proveniencia do servi�o de URGENCIA
Global gCodProvenUrgencia As Long

' Indica se utiliza o novo registo de frases no form de resultados
Global gNovoRegFrases As String

' Chave cujo conte�do indica a apresenta��o ou n�o da caixa de registo de produtos e tubos associados a an�lise
Global gIgnoraPerguntaProdutoTubo As String

'Chave cujo conteudo indica o local de acesso no Sislab
Global gCodLocal As Integer

'Chave cujo conteudo indica o local de acesso no Sislab
Global gEmpresaIdFactDefault As String

'Chave cujo conteudo indica se utiliza a gest�o de familias na informa��o clinica do utente
Global gUsaFamilias As Integer

'Chave que indica se a factura��o � efectuada apenas para requisi��es fechadas
Global gFacturaFechadas As Integer

'Cheva que indica o c�digo do utilizador HL7
Global gCodUtilizadorHL7 As String

'Indica a op��o escolhida para imprimir documentos
Global gTipoDoc As Integer

'Indica se quer apenas o ultimo utilizador de valida��o por grupo no relat�rio
Global gValidacaoUnicaPorGrupo As Integer

'Indica se epis�dio na gest�o de requisi��es � um campo obrigat�rio
Global gEpisObrigatorio As Integer

'Indica se mostra o separador de colheita na gest�o de requisi��o
Global gMostraColheita As Integer

' Indica se envia para a factura��o por n�mero de processo
Global gFacturaPorProcesso As Integer


' PFerreira 23.02.2007
' Indica que o form FormInfoRes se encontra aberto
Global gFormInfoRes_Open


Global Const gEstadoReciboNaoEmitido = "N"
Global Const gEstadoReciboPerdido = "PE"
Global Const gEstadoReciboCobranca = "C"
Global Const gEstadoReciboPago = "P"
Global Const gEstadoReciboAnulado = "A"
Global Const gEstadoReciboNulo = "NL"

Global Const gEstadoFactFaturado = "R"
Global Const gEstadoFactNaoFaturado = "NF"
Global Const gEstadoFactCancelado = "C"
Global Const gEstadoFactCreditado = "NC"



' -------------------------------------------------------------------
' ESTADOS PARA FACTURACAO DA REQUISICAO
' -------------------------------------------------------------------
Global Const gEstadoFactRequisNaoFacturado = 0
Global Const gEstadoFactRequisFacturado = 1
Global Const gEstadoFactRequisRejeitado = 2

' -------------------------------------------------------------------
' ESTADOS SEROTECA
' -------------------------------------------------------------------
Global Const gEstadoSerotecaAtribuido = 0
Global Const gEstadoSerotecaCongelado = 1
Global Const gEstadoSerotecaDescongelado = 2
Global Const gEstadoSerotecaLixo = 3

' -------------------------------------------------------------------
' CODIGOS PARA TIPOS DE ISENCAO
' -------------------------------------------------------------------
Global gTipoIsencaoIsento As Integer
Global gTipoIsencaoNaoIsento As Integer
Global gTipoIsencaoBorla As Integer
Global gTipoIsencaoNaoFacturar As Integer

' -------------------------------------------------------------------
' CODIGO PARA TIPO NAO URBANO
' -------------------------------------------------------------------
Global Const gCodNaoUrbano = 3

' -------------------------------------------------------------------
' ESTADOS PARA ANALISE
' -------------------------------------------------------------------
Global Const gEstadoAnaSemResultado = -1
Global Const gEstadoAnaComResultado = 1
Global Const gEstadoAnaBloqueada = 2
Global Const gEstadoAnaValidacaoTecnica = 5
Global Const gEstadoAnaPendente = 6
Global Const gEstadoAnaValidacaoMedica = 3
Global Const gEstadoAnaImpressa = 4

' -------------------------------------------------------------------
' ESTADOS PARA REQUISICAO
' -------------------------------------------------------------------
Global Const gEstadoReqEsperaResultados = "A"
Global Const gEstadoReqEsperaValidacao = "B"
Global Const gEstadoReqCancelada = "C"
Global Const gEstadoReqValicacaoMedicaCompleta = "D"
Global Const gEstadoReqNaoPassarHistorico = "E"
Global Const gEstadoReqTodasImpressas = "F"
Global Const gEstadoReqHistorico = "H"
Global Const gEstadoReqEsperaProduto = "I"
Global Const gEstadoReqResultadosParciais = "1"
Global Const gEstadoReqValidacaoMedicaParcial = "2"
Global Const gEstadoReqImpressaoParcial = "3"
Global Const gEstadoReqBloqueada = "4"
Global Const gEstadoReqSemAnalises = " "

' -------------------------------------------------------------------
' ESTADOS PARA ANALISE
' -------------------------------------------------------------------
Global Const gEstadoEtiqSemTubo = 0
Global Const gEstadoEtiqTodasImpressas = 1
Global Const gEstadoEtiqNenhumaImpressa = 2
Global Const gEstadoEtiqAlgumasImpressas = 3

' -------------------------------------------------------------------
' ESTADOS PARA VIGILANCIA EPIDEMIOL�GICA
' -------------------------------------------------------------------
Global Const gEstadoVePendente = 0
Global Const gEstadoVeEnviado = 1
Global Const gEstadoVeRejeitado = 2

Global gCodEFRSemCredencial As String

Global gUsaMarcacaoARS As String


' pferreira 2007.11.13
' M�dulo Correio -------------------------------------------------------------------------------------

' pferreira 2007.11.13
Global gActivarCorreio As Integer
' pferreira 2007.11.13
Global gTimerCorreio As Long
' pferreira 2007.11.15
Global gPathAnexos As String
' pferreira 2007.12.14
Global gTabelaUtilizadores As String
' pferreira 2007.12.14
Global gCampoCodUtilizador As String
' pferreira 2007.12.14
Global gCampoNomeUtilizador As String
' pferreira 2007.12.14
Global gTabelaCorreio As String
' pferreira 2007.12.14
Global gTabelaCorreioUtil As String
' pferreira 2007.12.14
Global gTabelaCorreioAnexos As String
' pferreira 2007.12.14
Global gTabelaCodListas As String
' pferreira 2007.12.14
Global gTabelaCodListasMembros As String
' pferreira 2007.12.19
Global gTabelaCrDestinatarios As String
' pferreira 2007.12.19
Global gTabelaCrAnexos As String
' pferreira 2007.12.19
Global gTabelaCrMensagem As String
' -----------------------------------------------------------------------------------------------------

Global Const gBD_PREFIXO_TAB = "SL_"

Global gComputador As String
Global gTerminal As String
Global gCodSalaAssocComp As Integer
Global gCodSalaAssocUser As Integer

Global gMostraMgmPagaRecibo As Integer


Global Const gCodDestinoLab = 0
Global Const gCodDestinoPosto = 1
Global Const gCodDestinoDomic = 2
Global Const gCodDestinoEmailUte = 3
Global Const gCodDestinoEmailPosto = 4
Global Const gCodDestinoEmailProven = 5
Global Const gCodDestinoEmailMedico = 6
Global Const gCodDestinoSMSUte = 7
Global Const gCodDestinoSMSPosto = 8
Global Const gCodDestinoSMSProven = 9
Global Const gCodDestinoSMSMedico = 10
Global Const gCodDestinoEmailLocalEntrega = 11

Global Const gDCNaoFazerNada = 1
Global Const gDCValidar = 2
Global Const gDCNaoValidar = 3
Global Const gDCAvisar = 4
Global Const gDCNaoImprimir = 5

Global Const gINTERFBloquear = 0
Global Const gINTERFAvisar = 1
Global Const gINTERFDesBloquear = 2

Global Const gDataDias = 0
Global Const gDataMeses = 1
Global Const gDataAnos = 2
Global Const gDataCrianca = 3
Global Const gDataAdulto = 4



Global Const gArgumentoObsAutoAviso = 0
Global Const gArgumentoDescrRelatorio = 1
Global Const gArgumentoCodPerfil = 2
Global Const gArgumentoCodAnaC = 3
Global Const gArgumentoCodAnaS = 4
Global Const gArgumentoNaoAvaliaRes = 5
Global Const gArgumentoObsRef = 6
Global Const gArgumentoUserCri = 7
Global Const gArgumentoOrdemARS = 8
Global Const gArgumentoCaracMetodo1 = 9
Global Const gArgumentoCaracMetodo2 = 10
Global Const gArgumentoCaracMetodoComp = 11
Global Const gArgumentoCaracMetodoPerf = 12
Global Const gArgumentoRespValMedMed = 13
Global Const gArgumentoRespValMedTec = 14
Global Const gNumeroArgumentos = 15

Global Const gProfileInvisivel = 0
Global Const gProfileVisivel = 1
Global Const gProfileDefeito = 2

'Estrutura que guarda dados da Folha de Trabalho
Public Type FolhaTrabalho
    codFolhaTrab As String
    seqFolhaTrab As Long
    DescrFolhaTrab As String
    ordem As Long
    Estado As Integer
End Type
Public gFolhasTrab() As FolhaTrabalho
    

' -------------------------------------------------------------------
' ESTADO DA REQUISI��O (FACTURA��O)
' -------------------------------------------------------------------
Global Const gEstadoReqFactFacturada = 0
Global Const gEstadoReqFactNaoFacturada = 1
Global Const gEstadoReqFactTodas = 2


Public Type Paramet
    acesso As Integer
    ambito As Integer
    cod_computador As Long
    cod_utilizador As String
    chave As String
    conteudo As String
    cod_local As Integer
End Type
Public gEstrutParamet() As Paramet
Public gTotalParamet As Long

' -------------------------------------------------------------------
' CONFIGURA��O PARA ENVIO DE SMS
' -------------------------------------------------------------------
Public gSMSEnviaTodasReq As Integer
Public gSMSEnviaReqAssinaladas As Integer
Public gSMSEnviaReqProntasAntesTempo As Integer

Public Type MenuItem
    Objecto As String
    indice As Integer
    visivel As Boolean
End Type
Public gEstrutMenus() As MenuItem
Public gTotalMenus As Long
Public gMudouUtilizador As Boolean


Global Const gRegraWestgard12s = 1
Global Const gRegraWestgard13s = 2
Global Const gRegraWestgard22s = 3
Global Const gRegraWestgardR4s = 4
Global Const gRegraWestgard41s = 5
Global Const gRegraWestgard10x = 6
Global Const gRegraWestgard8x = 7
Global Const gRegraWestgard12x = 8

' --------------------------------------
' grupos de notas com a��o
' --------------------------------------
Global Const gGrupoNotasImprRel = 1
Global Const gGrupoNotasGeral = 1
Global Const gGrupoNotasUtente = 2
Global Const gGrupoNotasSegVia = 3
Global Const gGrupoNotasTubo = 4

Public Enum TimersEnum
    e_timer_correio
    e_timer_mensagens
    e_timer_resultados_Criticos
    e_timer_systray
End Enum
' --------------------------------------
' t_urg
' --------------------------------------
Public Type t_urg
    codigo As String
    descricao As String
    cor As Long
End Type
Global gEstrutTUrg() As t_urg
' --------------------------------------
' ESTADOS DA REQUISI��O
' --------------------------------------
Public Type estadosReq
    codigo As String
    descricao As String
    cor As Long
End Type
Global gEstrutEstadosReq() As estadosReq
' --------------------------------------
' ESTADOS DA ANALISE
' --------------------------------------
Public Type estadosAna
    codigo As String
    descricao As String
    cor As Long
End Type
Global gEstrutEstadosAna() As estadosAna

' --------------------------------------
' CHAVES PARA MODOS DE PAGAMENTO
' --------------------------------------
Global Const gModoPagNaoPago = 0
Global Const gModoPagDinheiro = 1
Global Const gModoPagCheque = 2
Global Const gModoPagMultibanco = 3

' --------------------------------------
' CHAVES PARA CONVENCAO
' --------------------------------------
Global Const gConvencaoProfissional = 3
Global Const gConvencaoInternacional = 2
Global Const gconvencaoNormal = 1


' --------------------------------------
' TIPOS DE GRAFICO
' --------------------------------------
Global Const gTGraficoBanda = 0

' --------------------------------------
' TIPO DE MENU DE RESULTADOS
' --------------------------------------
Global gTipoMenuResultados As Integer
Global Const gTipoMenuGeral = 0
Global Const gTipoMenuDC = 1
Global Const gTipoMenuIF = 2
Global Const gTipoMenuAnexo = 3
Global Const gTipoMenuVigEpid = 4

' --------------------------------------
' INDICA RELEASE EM USO
' --------------------------------------
Global Const gMajorRelease = "21R06"
Global Const cSortTypeDate = "DATE"

'R.G.
Global Const cSortTypeString = "STRING"
Global Const cSortTypeLong = "LONG"
Global Const cSortTypeDouble = "DOUBLE"

Global Const cSortOrderAsc = "ASC"
Global Const cSortOrderDesc = "DESC"


' --------------------------------------
' PERFIS MARCADOS
' --------------------------------------
Public Type PerfisMarcados
    seq_marcacao_perfil As Long
    n_req As Long
    Cod_Perfil As String
    cod_agrup As String
    user_cri As String
    dt_cri As String
    hr_cri As String
End Type
Global gEstrutPerfisMarcados() As PerfisMarcados
Global gTotalPerfisMarcados As Integer

Public Enum gEnumColheitaChegada
    ColheitaTubo
    ChegadaTubo
End Enum
Global gColheitaChegada As Integer

Public Enum gEstadosTubo
    Pendente
    colhido
    chegado
    cancelado
End Enum


'RGONCALVES 07.07.2015 CHSJ-2082
Global gUsaAutenticacaoActiveDirectory As Integer
Global gDominioActiveDirectory As String
'
    
Global Const gOracle = "ORACLE"
Global Const gSqlServer = "SQL_SERVER"
Global Const gPostGres = "POSTGRES"
Global Const gInformix = "INFORMIX"

'BRUNDSANTOS 21.06.2016
Global gUsaVersaoOffice As Integer
'

'BRUNODSANTOS 01.12.2015
Global gInformacaoParaUtente As Integer
'
'BRUNODSANTOS CHSJ-2670 - 05.05.2016
Global gValidaGeraVersaoReport As Integer
'
'RGONCALVES 19.05.2016
Global gImprimeEtiqResumoEcraChegadaTubos As Integer
'
'BRUNODSANTOS 23.02.2017 - Glintt-HS-14510
Public Enum tipo_notificacao
    email = 0
    ws_sinave = 1
End Enum

Public gAuntenticaSINAVE As String

'
'BRUNODSANTOS 09.03.2017 - Cedivet-206
Public gParamPathAnexo As String
'
'BRUNODSANTOS 30.03.2017 - Glintt-HS-14510
Public gDefaultProven As String
Public gDefaultPais As String
'
'BRUNODSANTOS GLINTT-HS-15750
Public gUsaEnvioPDS As Integer

'BRUNODSANTOS Glintt-HS-17949 10.01.2018
Public gVerificaContaFechada As Integer
'

'NELSONPSILVA 16.01.2018 UnimedVitoria-1152
Public gMostraMisDataDiscovery As Integer
'
'NELSONPSILVA 08.03.2018 Glintt-HS-18730
Public gGestao_Tubos_Default As Integer
'
'NELSONPSILVA 04.04.2018 Glintt-HS-18011
Public gATIVA_LOGS_RGPD As Integer
'
'edgar.parada Glintt-HS-17250 LOGIN GA_ACCESS
Public gUsa_Autenticacao_GA_ACCESS As Integer
'
'BRUNODSANTOS glintt-hs-18011 - 21.06.2018
Public Enum ModoLOG
    NONE = 0
    REQUEST = 1
    response = 2
End Enum
'
'BRUNODSANTOS ULSNE-1871
Public gMudaLocalAnaliseTubo As Integer

Public gAtivaESP As Integer
'

'edgar.parada Glintt-HS-19165
Public gCompanyDb As String
'
'NELSONPSILVA CHVNG-7461 29.10.2018
Public gAtiva_Reutilizacao_Colheitas As Integer
Public gAtiva_Nova_Numeracao_Tubo As Integer
'

'edgar.parada BACKLOG-11940 (COLHEITAS) 24.01.2019
Public gAtiva_Alertas As Integer
Public gAtivarAlertasTimer As Integer
'
'edgar.parada ENTIDADE ESP
Public gEntidadeESP As String
'
'NELSONPSILVA URL MONITOR
Public gMonitorESP As String
'
Public gFactQrCodeDate As Date

VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormPrescricoesAssociadas 
   BackColor       =   &H00FFE0C7&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormPrescricoesAssociadas"
   ClientHeight    =   5400
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   12570
   Icon            =   "FormPrescricoesAssociadas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5400
   ScaleWidth      =   12570
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcNumReq 
      Height          =   285
      Left            =   360
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   6360
      Width           =   1095
   End
   Begin MSComctlLib.TreeView TvPrescr 
      Height          =   3855
      Left            =   240
      TabIndex        =   1
      Top             =   1320
      Width           =   12135
      _ExtentX        =   21405
      _ExtentY        =   6800
      _Version        =   393217
      Style           =   7
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin SISLAB.CabecalhoResultados Cabecalho 
      Height          =   975
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   12495
      _ExtentX        =   22040
      _ExtentY        =   1720
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   5160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   16777215
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormPrescricoesAssociadas.frx":000C
            Key             =   "ANA"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormPrescricoesAssociadas.frx":03A6
            Key             =   "PRESC"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormPrescricoesAssociadas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Const KeyPrescricao = "KeyP"
Const KeyAnalise = "KeyA"
Const KeyProfile = "KeyP"
Private Type analises
    cod_agrup As String
    descr_ana As String
    indiceTV As Integer
End Type
Private Type prescricoes
    n_req_exame As String
    n_prescricao As String
    dt_cri As String
    estrutAna() As analises
    totalAna As Integer
    indiceTV As Integer
End Type
Dim estrutPrescricoes() As prescricoes
Dim totalPrescricoes As Integer



Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = "Prescri��es Associadas"
    Me.left = 5
    Me.top = 5
    Me.Width = 12660
    Me.Height = 6120
    Set CampoDeFocus = TvPrescr
    
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    If gF_REQUIS = mediSim Then
        Set gFormActivo = FormGestaoRequisicao
        FormGestaoRequisicao.Enabled = True
    End If
    Set FormPrescricoesAssociadas = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    
    
End Sub

Sub DefTipoCampos()
    Cabecalho.LimpaCampos &HFFE0C7
End Sub

Sub PreencheCampos()
    ' nada
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    

End Function

Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim root As Node
    Dim i As Integer
    Dim j As Integer
    ReDim estrutPrescricoes(0)
    Dim indice As Integer
    totalPrescricoes = 0
    TvPrescr.Nodes.Clear
    indice = 0
    Cabecalho.ProcuraDados EcNumReq
    sSql = "SELECT distinct x1.n_Req, x1.cod_agrup, x2.descr_ana, x1.dt_cri, x1.n_prescricao, x1.n_req_exame "
    sSql = sSql & " FROM sl_req_episodio x1, slv_analises_apenas x2 WHERE x1.cod_Agrup = x2.cod_ana"
    sSql = sSql & " AND x1.n_Req = " & EcNumReq
    sSql = sSql & " ORDER BY n_prescricao ASC "
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount >= 1 Then
        While Not rsAna.EOF
            If estrutPrescricoes(totalPrescricoes).n_prescricao <> BL_HandleNull(rsAna!n_prescricao, "") Then
                totalPrescricoes = totalPrescricoes + 1
                ReDim Preserve estrutPrescricoes(totalPrescricoes)
                estrutPrescricoes(totalPrescricoes).n_req_exame = BL_HandleNull(rsAna!n_req_exame, "")
                estrutPrescricoes(totalPrescricoes).n_prescricao = BL_HandleNull(rsAna!n_prescricao, "")
                estrutPrescricoes(totalPrescricoes).dt_cri = BL_HandleNull(rsAna!dt_cri, "")
                estrutPrescricoes(totalPrescricoes).totalAna = 0
                ReDim estrutPrescricoes(totalPrescricoes).estrutAna(0)
            End If
            estrutPrescricoes(totalPrescricoes).totalAna = estrutPrescricoes(totalPrescricoes).totalAna + 1
            ReDim Preserve estrutPrescricoes(totalPrescricoes).estrutAna(estrutPrescricoes(totalPrescricoes).totalAna)
            estrutPrescricoes(totalPrescricoes).estrutAna(estrutPrescricoes(totalPrescricoes).totalAna).cod_agrup = BL_HandleNull(rsAna!cod_agrup, "")
            estrutPrescricoes(totalPrescricoes).estrutAna(estrutPrescricoes(totalPrescricoes).totalAna).descr_ana = BL_HandleNull(rsAna!descr_ana, "")
            rsAna.MoveNext
        Wend
    End If
    rsAna.Close
    Set rsAna = Nothing
    For i = 1 To totalPrescricoes
        indice = indice + 1
        estrutPrescricoes(i).indiceTV = indice
        Set root = TvPrescr.Nodes.Add(, tvwChild, KeyPrescricao & estrutPrescricoes(i).n_prescricao, estrutPrescricoes(i).n_prescricao & " " & estrutPrescricoes(i).dt_cri, "PRESC")
        root.Expanded = True
        For j = 1 To estrutPrescricoes(i).totalAna
            indice = indice + 1
            estrutPrescricoes(i).estrutAna(j).indiceTV = indice
            Set root = TvPrescr.Nodes.Add(estrutPrescricoes(i).indiceTV, tvwChild, KeyAnalise & estrutPrescricoes(i).estrutAna(j).cod_agrup, estrutPrescricoes(i).estrutAna(j).descr_ana, "ANA")
        Next j
    Next
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()
    

End Sub

Sub FuncaoInserir()
    
End Sub

Sub BD_Insert()
    
    
End Sub

Sub FuncaoModificar()
    

End Sub

Sub BD_Update()


End Sub

Sub FuncaoRemover()
    
    
End Sub

Sub BD_Delete()
    

End Sub


Sub PreencheValoresDefeito()
    InicializaTreeView
End Sub

Private Sub InicializaTreeView()
  TvPrescr.LineStyle = tvwRootLines
  TvPrescr.ImageList = ImageList1
End Sub

Private Sub TvPrescr_DblClick()
    Dim i As Integer
    Dim j As Integer
    For i = 1 To totalPrescricoes
        If estrutPrescricoes(i).indiceTV = TvPrescr.SelectedItem.Index Then
            'rcoelho 21.05.2013 chvng-4118
            BL_GeraTokenFichasDinamicas BL_HandleNull(estrutPrescricoes(i).n_req_exame, estrutPrescricoes(i).n_prescricao)
            Exit Sub
        End If
        For j = 1 To estrutPrescricoes(i).totalAna
            If estrutPrescricoes(i).estrutAna(j).indiceTV = TvPrescr.SelectedItem.Index Then
                BL_GeraTokenFichasDinamicas BL_HandleNull(estrutPrescricoes(i).n_req_exame, estrutPrescricoes(i).n_prescricao)
                Exit Sub
            End If
        Next j
    Next i
End Sub

VERSION 5.00
Begin VB.Form FormContacProv 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormContacProv"
   ClientHeight    =   5160
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7230
   Icon            =   "FormContacProv.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5160
   ScaleWidth      =   7230
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcDescricao 
      Height          =   315
      Left            =   1380
      TabIndex        =   3
      Top             =   1020
      Width           =   4755
   End
   Begin VB.ComboBox EcTipoContacCombo 
      Height          =   315
      Left            =   1380
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   570
      Width           =   2145
   End
   Begin VB.ComboBox EcProvenCombo 
      Height          =   315
      Left            =   2800
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   120
      Width           =   3285
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2040
      TabIndex        =   10
      Top             =   4560
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4440
      TabIndex        =   9
      Top             =   4560
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2040
      TabIndex        =   8
      Top             =   4200
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4440
      TabIndex        =   7
      Top             =   4200
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Left            =   120
      TabIndex        =   5
      Top             =   1830
      Width           =   6885
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6600
      TabIndex        =   6
      Top             =   4200
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcCodigo 
      Height          =   315
      Left            =   1380
      TabIndex        =   0
      Top             =   120
      Width           =   1425
   End
   Begin VB.ComboBox CmbPrinters 
      Height          =   315
      Left            =   1380
      TabIndex        =   4
      Top             =   1020
      Width           =   4935
   End
   Begin VB.Label lbltitnome 
      Caption         =   "&Nome"
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   1080
      Width           =   855
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   3000
      TabIndex        =   20
      Top             =   4560
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   3120
      TabIndex        =   19
      Top             =   4200
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   360
      TabIndex        =   18
      Top             =   4560
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   360
      TabIndex        =   17
      Top             =   4200
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "Descri��o : "
      Height          =   255
      Left            =   150
      TabIndex        =   16
      Top             =   1050
      Width           =   1185
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   5040
      TabIndex        =   15
      Top             =   4200
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.Label Label6 
      Caption         =   "Tipo Contacto "
      Height          =   255
      Left            =   150
      TabIndex        =   14
      Top             =   600
      Width           =   1185
   End
   Begin VB.Label Label3 
      Caption         =   "Proveni�ncia "
      Height          =   255
      Left            =   150
      TabIndex        =   13
      Top             =   180
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   1620
      Width           =   645
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   3030
      TabIndex        =   11
      Top             =   1590
      Width           =   1065
   End
End
Attribute VB_Name = "FormContacProv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 07/02/2003
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub

Private Sub EcCodigo_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodigo_LostFocus()
    
    EcCodigo.Text = UCase(EcCodigo.Text)
    BL_ColocaTextoCombo "sl_proven", "seq_proven", "cod_proven", EcCodigo, EcProvenCombo

End Sub

Private Sub EcDescricao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDescricao_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Private Sub EcProvenCombo_Click()
    
    BL_ColocaComboTexto "sl_proven", "seq_proven", "cod_proven", EcCodigo, EcProvenCombo

End Sub

Private Sub EcProvenCombo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcProvenCombo.ListIndex = -1

End Sub

Private Sub EcTipoContacCombo_Click()
    
    EcDescricao.Text = ""
    CmbPrinters.ListIndex = -1
    CmbPrinters.Text = ""
    If EcTipoContacCombo.ListIndex <> -1 Then
        If EcTipoContacCombo.ItemData(EcTipoContacCombo.ListIndex) = gT_ImpressoraRede Then
            lbltitnome.Visible = True
            CmbPrinters.Visible = True
            EcDescricao.Visible = False
        Else
            lbltitnome.Visible = False
            CmbPrinters.Visible = False
            EcDescricao.Visible = True
        End If
    Else
        lbltitnome.Visible = False
        CmbPrinters.Visible = False
        EcDescricao.Visible = True
    End If
    
End Sub

Private Sub EcTipoContacCombo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcTipoContacCombo.ListIndex = -1

End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Contactos de proveni�ncia"
    Me.left = 540
    Me.top = 450
    Me.Width = 7275
    Me.Height = 4600 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_contac_prov"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 4
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_contac"
    CamposBD(1) = "cod_proven"
    CamposBD(2) = "t_contac"
    CamposBD(3) = "conteudo_cont"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcTipoContacCombo
    Set CamposEc(3) = EcDescricao
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo identificador da proveni�ncia"
    TextoCamposObrigatorios(2) = "Tipo de contacto"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_contac"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_proven", "t_contac", "conteudo_cont")
    NumEspacos = Array(10, 22, 43)
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormContacProv = Nothing

End Sub

Sub LimpaCampos()

    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    EcProvenCombo.ListIndex = mediComboValorNull
    EcTipoContacCombo.ListIndex = mediComboValorNull
    CmbPrinters.ListIndex = mediComboValorNull
    CmbPrinters.Text = ""
    
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheValoresDefeito()
    
    Dim i As Integer
    
    BG_PreencheComboBD_ADO "sl_proven", "seq_proven", "descr_proven", EcProvenCombo, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "sl_tbf_t_contac", "cod_t_contac", "descr_t_contac", EcTipoContacCombo, mediAscComboDesignacao
    
    For i = 0 To Printers.Count - 1
        CmbPrinters.AddItem Printers(i).DeviceName
    Next i
    
    'trazer codigo do form de proveniencias
    'caso a chamada a este form tenha sido efectuada de l�
    If gF_PROVENIENCIA = 1 Then
        EcCodigo = FormProveniencia.EcCodigo
        BL_ColocaTextoCombo "sl_proven", "seq_proven", "cod_proven", EcCodigo, EcProvenCombo
    End If
      
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_ColocaTextoCombo "sl_proven", "seq_proven", "cod_proven", EcCodigo, EcProvenCombo
        CmbPrinters = EcDescricao.Text
    End If
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_proven ASC, t_contac ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_contac") + 1
    
    If EcTipoContacCombo.ListIndex <> -1 Then
        If EcTipoContacCombo.ItemData(EcTipoContacCombo.ListIndex) = gT_ImpressoraRede Then
            EcDescricao.Text = CmbPrinters.Text
        End If
    End If
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    
    If EcTipoContacCombo.ListIndex <> -1 Then
        If EcTipoContacCombo.ItemData(EcTipoContacCombo.ListIndex) = gT_ImpressoraRede Then
            EcDescricao.Text = CmbPrinters.Text
        End If
    End If
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal

End Sub

Sub FuncaoRemover()
    
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
   
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub





VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormConsRequis 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormConsRequis"
   ClientHeight    =   6960
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9900
   Icon            =   "FormConsRequis.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6960
   ScaleWidth      =   9900
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrDadosAdicionais 
      Caption         =   "Dados adicionais"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   600
      TabIndex        =   61
      Top             =   1920
      Visible         =   0   'False
      Width           =   5655
      Begin VB.TextBox EcDataImpressao 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   69
         TabStop         =   0   'False
         Top             =   1200
         Width           =   975
      End
      Begin VB.TextBox EcHora2Via 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   4680
         Locked          =   -1  'True
         TabIndex        =   68
         TabStop         =   0   'False
         Top             =   1560
         Width           =   735
      End
      Begin VB.TextBox EcUtilImpressao 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   67
         TabStop         =   0   'False
         Top             =   1200
         Width           =   2295
      End
      Begin VB.TextBox EcData2Via 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   66
         TabStop         =   0   'False
         Top             =   1560
         Width           =   975
      End
      Begin VB.TextBox EcHoraImpressao 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   4680
         Locked          =   -1  'True
         TabIndex        =   65
         TabStop         =   0   'False
         Top             =   1200
         Width           =   735
      End
      Begin VB.TextBox EcUtil2Via 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   64
         TabStop         =   0   'False
         Top             =   1560
         Width           =   2295
      End
      Begin VB.TextBox EcDataPrevista 
         Height          =   285
         Left            =   3720
         TabIndex        =   63
         Tag             =   "7"
         Top             =   720
         Width           =   975
      End
      Begin VB.TextBox EcReqAssociada 
         Height          =   285
         Left            =   1560
         TabIndex        =   62
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label Label11 
         Caption         =   "Emiss�o"
         Height          =   255
         Left            =   240
         TabIndex        =   73
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label18 
         Caption         =   "Ultima 2� Via"
         Height          =   255
         Left            =   240
         TabIndex        =   72
         Top             =   1560
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Data &Prevista para a chegada do(s) produto(s)"
         Height          =   255
         Left            =   240
         TabIndex        =   71
         Top             =   720
         Width           =   3495
      End
      Begin VB.Label Label1 
         Caption         =   "Req. &Associada"
         Height          =   255
         Left            =   240
         TabIndex        =   70
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.TextBox EcReqAux 
      Height          =   285
      Left            =   6120
      TabIndex        =   59
      Top             =   2040
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox EcSeqRel 
      Height          =   285
      Left            =   8880
      TabIndex        =   57
      Top             =   8160
      Width           =   855
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   255
      Left            =   2160
      TabIndex        =   56
      Top             =   9240
      Width           =   975
   End
   Begin VB.CommandButton BtBaixo 
      Height          =   195
      Left            =   240
      Picture         =   "FormConsRequis.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   49
      ToolTipText     =   "Dados Adicionais"
      Top             =   1680
      Width           =   375
   End
   Begin VB.CommandButton BtCima 
      Height          =   195
      Left            =   240
      Picture         =   "FormConsRequis.frx":0156
      Style           =   1  'Graphical
      TabIndex        =   50
      ToolTipText     =   "Fechar Dados Adicionais"
      Top             =   1680
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Frame Frame2 
      Height          =   1935
      Left            =   120
      TabIndex        =   17
      Top             =   0
      Width           =   9615
      Begin VB.TextBox EcEpisodio 
         Height          =   285
         Left            =   8040
         TabIndex        =   2
         Tag             =   "200"
         Top             =   1080
         Width           =   1335
      End
      Begin VB.TextBox EcNumReq 
         Height          =   315
         Left            =   1320
         TabIndex        =   0
         Tag             =   "131 "
         Top             =   1080
         Width           =   855
      End
      Begin VB.ComboBox CbEstadoReq 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         Style           =   1  'Simple Combo
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   1080
         Width           =   2775
      End
      Begin VB.TextBox EcDataChega 
         Height          =   285
         Left            =   5880
         TabIndex        =   1
         Tag             =   "7"
         Top             =   1080
         Width           =   1335
      End
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   5880
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1440
         Width           =   1335
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   8040
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1440
         Width           =   1335
      End
      Begin VB.TextBox EcDescrProveniencia 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   1440
         Width           =   3615
      End
      Begin VB.TextBox EcTipoUtente 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   720
         Locked          =   -1  'True
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcProcHosp2 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   7200
         Locked          =   -1  'True
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   240
         Width           =   2175
      End
      Begin VB.TextBox EcDataNasc 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   6240
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcNome 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   720
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   600
         Width           =   4215
      End
      Begin VB.TextBox EcProcHosp1 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   5040
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   240
         Width           =   2175
      End
      Begin VB.TextBox EcUtente 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   240
         Width           =   2175
      End
      Begin VB.TextBox EcDataInscr 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   8400
         Locked          =   -1  'True
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   600
         Width           =   975
      End
      Begin VB.Label LbAdmin 
         AutoSize        =   -1  'True
         Caption         =   "&Epis�dio"
         Height          =   195
         Left            =   7320
         TabIndex        =   48
         Top             =   1080
         Width           =   600
      End
      Begin VB.Label LbNumero 
         AutoSize        =   -1  'True
         Caption         =   "&Nr. Requisi��o"
         Height          =   195
         Left            =   120
         TabIndex        =   47
         Top             =   1080
         Width           =   1050
      End
      Begin VB.Label Label2 
         Caption         =   "&Chegada"
         Height          =   255
         Left            =   5040
         TabIndex        =   46
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   5040
         TabIndex        =   45
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label8 
         Caption         =   "&Urg�ncia"
         Height          =   255
         Left            =   7320
         TabIndex        =   44
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label9 
         Caption         =   "Proveni�ncia"
         Height          =   255
         Left            =   120
         TabIndex        =   43
         Top             =   1440
         Width           =   1455
      End
      Begin VB.Label LbDataNasc 
         AutoSize        =   -1  'True
         Caption         =   "Data nasc."
         Height          =   195
         Left            =   5400
         TabIndex        =   22
         Top             =   600
         Width           =   780
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   600
         Width           =   420
      End
      Begin VB.Label LbProcesso 
         AutoSize        =   -1  'True
         Caption         =   "Proc. Hosp."
         Height          =   195
         Left            =   4080
         TabIndex        =   20
         Top             =   240
         Width           =   840
      End
      Begin VB.Label LbUtente 
         AutoSize        =   -1  'True
         Caption         =   "Utente"
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   480
      End
      Begin VB.Label Label5 
         Caption         =   "Data inscr."
         Height          =   255
         Left            =   7560
         TabIndex        =   18
         Top             =   600
         Width           =   855
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000005&
         X1              =   0
         X2              =   9590
         Y1              =   975
         Y2              =   975
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000003&
         BorderWidth     =   2
         X1              =   0
         X2              =   9590
         Y1              =   960
         Y2              =   975
      End
   End
   Begin RichTextLib.RichTextBox EcComFinal 
      Height          =   495
      Left            =   120
      TabIndex        =   8
      Top             =   6360
      Width           =   9615
      _ExtentX        =   16960
      _ExtentY        =   873
      _Version        =   393217
      BackColor       =   -2147483624
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"FormConsRequis.frx":02A0
   End
   Begin RichTextLib.RichTextBox EcObsAna 
      Height          =   495
      Left            =   120
      TabIndex        =   7
      Top             =   5880
      Width           =   9615
      _ExtentX        =   16960
      _ExtentY        =   873
      _Version        =   393217
      BackColor       =   -2147483624
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"FormConsRequis.frx":0322
   End
   Begin VB.TextBox EcInfo 
      BackColor       =   &H00E0E0E0&
      Height          =   20000
      Left            =   4800
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   6
      Top             =   9240
      Visible         =   0   'False
      Width           =   4215
   End
   Begin VB.CommandButton BtMostraRes 
      Height          =   195
      Left            =   6840
      Picture         =   "FormConsRequis.frx":03A4
      Style           =   1  'Graphical
      TabIndex        =   52
      ToolTipText     =   "Mostrar resultados Frase, Microrganismos e TSQ."
      Top             =   7920
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton BTEscondeRes 
      Height          =   195
      Left            =   6840
      Picture         =   "FormConsRequis.frx":04EE
      Style           =   1  'Graphical
      TabIndex        =   51
      ToolTipText     =   "Esconder resultados Frase, Microrganismos e TSQ."
      Top             =   8160
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ComboBox CbGrupo 
      Height          =   315
      Left            =   1560
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   2040
      Width           =   3615
   End
   Begin VB.TextBox EcCodGrupo 
      Height          =   285
      Left            =   5160
      TabIndex        =   39
      Top             =   2040
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox NrReqCopia 
      Height          =   285
      Left            =   7560
      TabIndex        =   35
      TabStop         =   0   'False
      Top             =   8760
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcUrgencia 
      Height          =   285
      Left            =   3480
      TabIndex        =   33
      TabStop         =   0   'False
      Top             =   8760
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox EcCodUtilImp2 
      Height          =   285
      Left            =   5640
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   8760
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcCodUtilImp 
      Height          =   285
      Left            =   5640
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   8280
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcCodProv 
      Height          =   285
      Left            =   3480
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   8280
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcCodEstadoReq 
      Height          =   285
      Left            =   1560
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   8280
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcCodUteSeq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   8760
      Visible         =   0   'False
      Width           =   375
   End
   Begin RichTextLib.RichTextBox RText 
      Height          =   255
      Left            =   1560
      TabIndex        =   37
      TabStop         =   0   'False
      Top             =   7920
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   450
      _Version        =   393217
      BorderStyle     =   0
      Appearance      =   0
      TextRTF         =   $"FormConsRequis.frx":0638
   End
   Begin MSFlexGridLib.MSFlexGrid FGAna 
      Height          =   3135
      Left            =   120
      TabIndex        =   24
      Top             =   2760
      Width           =   9615
      _ExtentX        =   16960
      _ExtentY        =   5530
      _Version        =   393216
      Rows            =   1
      Cols            =   1
      FixedRows       =   0
      FixedCols       =   0
      BackColor       =   16777215
      BackColorBkg    =   -2147483636
      GridColorFixed  =   8388608
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      TextStyle       =   1
      FocusRect       =   2
      HighLight       =   2
      GridLines       =   3
      SelectionMode   =   1
   End
   Begin VB.Frame Frame1 
      Height          =   495
      Left            =   7560
      TabIndex        =   53
      Top             =   1890
      Width           =   2175
      Begin VB.CommandButton BtImpRes 
         Enabled         =   0   'False
         Height          =   375
         Left            =   1560
         Picture         =   "FormConsRequis.frx":06BA
         Style           =   1  'Graphical
         TabIndex        =   54
         ToolTipText     =   "Imprimir Resultados Com Valida��o M�dica"
         Top             =   120
         Width           =   600
      End
      Begin VB.Label Label12 
         Caption         =   "&Imprimir Resultados"
         Height          =   255
         Left            =   120
         TabIndex        =   55
         Top             =   180
         Width           =   1815
      End
   End
   Begin VB.Label LbeResults 
      BackColor       =   &H80000004&
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   5760
      TabIndex        =   74
      Top             =   2400
      Width           =   3855
   End
   Begin VB.Label LaReqAux 
      Caption         =   "Nr. An�&lise"
      Height          =   255
      Left            =   5280
      TabIndex        =   60
      Top             =   2040
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label13 
      Caption         =   "EcSeqRel"
      Height          =   375
      Left            =   8040
      TabIndex        =   58
      Top             =   8160
      Width           =   735
   End
   Begin VB.Label Label10 
      Caption         =   "Grupo de An�lises"
      Height          =   255
      Left            =   120
      TabIndex        =   40
      Top             =   2040
      Width           =   1455
   End
   Begin VB.Label Label6 
      Caption         =   "RText"
      Height          =   255
      Left            =   960
      TabIndex        =   38
      Top             =   7920
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label21 
      Caption         =   "NrReqCopia"
      Enabled         =   0   'False
      Height          =   255
      Left            =   6480
      TabIndex        =   36
      Top             =   8760
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label22 
      Caption         =   "EcUrgencia"
      Height          =   255
      Left            =   2520
      TabIndex        =   34
      Top             =   8760
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label19 
      Caption         =   "EcCodUtiImp2"
      Enabled         =   0   'False
      Height          =   255
      Left            =   4560
      TabIndex        =   32
      Top             =   8760
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label17 
      Caption         =   "EcCodProv"
      Enabled         =   0   'False
      Height          =   255
      Left            =   2520
      TabIndex        =   30
      Top             =   8280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label16 
      Caption         =   "EcCodUtiImp"
      Enabled         =   0   'False
      Height          =   255
      Left            =   4560
      TabIndex        =   29
      Top             =   8280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label15 
      Caption         =   "EcCodEstadoReq"
      Enabled         =   0   'False
      Height          =   255
      Left            =   240
      TabIndex        =   26
      Top             =   8280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label7 
      Caption         =   "EcCodUteSeq"
      Enabled         =   0   'False
      Height          =   255
      Left            =   240
      TabIndex        =   16
      Top             =   8760
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "FormConsRequis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim VectorCamposBD

Dim NomeTabela As String
Dim NumCampos As Integer
Dim CamposBD() As String
Dim CamposEc() As Object

Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

'Comando e parametro utilizados para seleccionar dados dos utentes
Dim CmdUtente As New ADODB.Command

'Comando utilizado para seleccionar dados do historico
Dim CmdHist As New ADODB.Command

'RecordSet utilizado para seleccionar dados das realizacoes e marcacoes
Dim RegMarcadas As ADODB.recordset

'Comando e parametro utilizados para seleccionar designacoes de perfis
Dim CmdDesPerfil As New ADODB.Command
'Comando e parametro utilizados para seleccionar designacoes de Complexas
Dim CmdDesComplexa As New ADODB.Command
'Comando e parametro utilizados para seleccionar designacoes de Simples
Dim CmdDesSimples As New ADODB.Command
'Comando e parametro utilizados para seleccionar os tipo de resultados da an�lise simples
Dim CmdTres As New ADODB.Command

'Comandos utilizados para seleccionar os resultados
Dim CmdSelRes As New ADODB.Command
Dim CmdResAlf As New ADODB.Command
Dim CmdResFr As New ADODB.Command
Dim CmdResFrDescr As New ADODB.Command
Dim CmdResMicro As New ADODB.Command
Dim CmdResMicroDescr As New ADODB.Command
Dim CmdResMiTSQ As New ADODB.Command
Dim CmdResAntibiotico As New ADODB.Command
Dim CmdResAntib As New ADODB.Command

Dim CmdSelRes_H As New ADODB.Command
Dim CmdResAlf_H As New ADODB.Command
Dim CmdResFr_H As New ADODB.Command
Dim CmdResMicro_H As New ADODB.Command
Dim CmdResMiTSQ_H As New ADODB.Command
Dim CmdResAntib_H As New ADODB.Command

Dim CmdObsAna As New ADODB.Command
Dim CmdComFinal As New ADODB.Command

' Estruturas que guardam os dados dos resultados
Dim resultados As New Collection

Private Type analises
    CodPerfil As String
    DescrPerfil As String
    CodComplexa As String
    DescrComplexa As String
    CodSimples As String
    DescrSimples As String
    GridEstado As Integer 'Indica se a an�lise j� foi inserida na grid ou n�o
    GridIndice As Long
    AnaEstado As Integer
    TemProduto As Boolean
    OrdemFrase As Integer
    CodMicro As String
End Type
Dim AnalisesReq() As analises
Dim AnaIndex As Integer

'Definicoes das cores das an�lises
Const SemProduto = &HC0&
Const SemResultado = &HC0&
Const PorValidar = &H40C0&
Const Validada = &H4000&
Const ValidadaTec = &H8080&
Const Impressa = &H800000
Const Historico = &H400000
Const Cancelada = &HFF&
Const azul = &H8000000D

Dim APreencherFgAna As Boolean
Dim EcInfoVisivel As Boolean

Dim IndiceFrase As Long
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()
    If gFormInfoRes_Open = True Then
        FormInfoRes.Visible = False
    End If

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    If gF_REQUTE = 1 Or gF_RESULT = 1 Or gF_REQUIS = 1 Or gF_RESMICRO = 1 Or gF_RESULT_NOVO = 1 Then
        If gF_RESULT = 1 Or gF_REQUIS = 1 Or gF_RESMICRO = 1 Or gF_RESULT_NOVO = 1 Then
            EcCodGrupo = ""
        End If
        EcNumReq = gRequisicaoActiva
        FuncaoProcurar
    End If
        
    Select Case gLAB
        Case "CITO", "BIO"
            LaReqAux.Visible = True
            EcReqAux.Visible = True
            LaReqAux.caption = "Nr. An�lise"
        Case "GM"
            LaReqAux.Visible = True
            EcReqAux.Visible = True
            LaReqAux.caption = "Nr. Caso"
    End Select
    
End Sub
Sub Funcao_CopiaReq()
    If gRequisicaoActiva <> 0 Then
        EcNumReq = gRequisicaoActiva
        FuncaoProcurar
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe requisi��o activa!", , "Copiar requisi��o"
    End If
End Sub

Function Funcao_DataActual()
    
    Select Case UCase(CampoActivo.Name)
        Case "ECDATACHEGA"
            EcDataChega = Bg_DaData_ADO
        Case "ECDATAPREVISTA"
            EcDataPrevista = Bg_DaData_ADO
    End Select
    
End Function


Sub FuncaoSeguinte()

    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

Sub EventoUnload()

    ' Faz unload do form FormInfoRes
    If (gFormInfoRes_Open = True) Then
        Unload FormInfoRes
    End If
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    
    BL_ToolbarEstadoN 0
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
        
    If Not RegMarcadas Is Nothing Then
        If RegMarcadas.state <> adStateClosed Then
            RegMarcadas.Close
        End If
        Set RegMarcadas = Nothing
    End If
        
    Set CmdUtente = Nothing
    Set CmdTres = Nothing
    Set CmdHist = Nothing
    Set CmdDesPerfil = Nothing
    Set CmdDesComplexa = Nothing
    Set CmdDesSimples = Nothing
    Set CmdResAlf = Nothing
    Set CmdResFr = Nothing
    Set CmdResFrDescr = Nothing
    Set CmdResMicro = Nothing
    Set CmdResMicroDescr = Nothing
    Set CmdResMiTSQ = Nothing
    Set CmdResAntibiotico = Nothing
    Set CmdResAntib = Nothing
    
    Set CmdSelRes_H = Nothing
    Set CmdResAlf_H = Nothing
    Set CmdResFr_H = Nothing
    Set CmdResMicro_H = Nothing
    Set CmdResMiTSQ_H = Nothing
    Set CmdResAntib_H = Nothing
    
    LimpaColeccao resultados, -1
            
    If gF_REQUTE = 1 Then
        FormReqUte.Enabled = True
    ElseIf gF_RESULT = 1 Then
        FormResultados.Enabled = True
    ElseIf gF_RESULT_NOVO = 1 Then
        FormResultadosNovo.Enabled = True
    ElseIf gF_RESMICRO = 1 Then
        FormResMicro.Enabled = True
    ElseIf gF_REQUIS = 1 Then
        FormGestaoRequisicao.Enabled = True
    End If
    
    
    
    Set FormConsRequis = Nothing
    
End Sub

Sub MudaCorFlexGrid(linha As Long, EstadoAna As Integer, TemProduto As Boolean, Optional Selected As Boolean = False)
    
    If Selected = False Then
        FGAna.CellBackColor = vbWhite
    Else
        FGAna.CellBackColor = azul
        FGAna.CellForeColor = vbWhite
        Exit Sub
    End If
    
    If Trim(EcCodEstadoReq.Text) = gEstadoReqHistorico Then
        ' Hist�rico
        FGAna.TextMatrix(linha, 3) = BL_DevolveEstadoAna(CStr(EstadoAna))
        FGAna.CellForeColor = Historico
    Else
        FGAna.TextMatrix(linha, 3) = BL_DevolveEstadoAna(CStr(EstadoAna))
        
        Select Case EstadoAna
            ' Sem Resultado
            Case gEstadoAnaSemResultado
                If Trim(EcCodEstadoReq.Text) = gEstadoReqEsperaProduto Then
                    FGAna.TextMatrix(linha, 3) = "Sem Produto"
                    FGAna.CellForeColor = SemProduto
                ElseIf Trim(EcCodEstadoReq.Text) <> gEstadoReqEsperaProduto Then
                    If TemProduto = True Then
                        FGAna.TextMatrix(linha, 3) = cSemResultado
                        FGAna.CellForeColor = SemResultado
                    Else
                        FGAna.TextMatrix(linha, 3) = "Sem Produto"
                        FGAna.CellForeColor = SemProduto
                    End If
                End If
            ' A espera de validacao
            Case gEstadoAnaComResultado
                FGAna.CellForeColor = PorValidar
            ' Cancelada
            Case gEstadoAnaBloqueada
                FGAna.CellForeColor = Cancelada
            ' Validada
            Case gEstadoAnaValidacaoMedica
                FGAna.CellForeColor = Validada
            ' Impressa
            Case gEstadoAnaImpressa
                FGAna.CellForeColor = Impressa
            ' Impressa
            Case gEstadoAnaValidacaoTecnica
                FGAna.CellForeColor = ValidadaTec
            Case gEstadoAnaPendente
                FGAna.CellForeColor = ValidadaTec
        End Select
    End If
    
End Sub

Sub PreencheValoresDefeito()

    'Preenche combo situa��o da requisi��o
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia
    
    'Preenche Combo de Grupos de An�lises
    BG_PreencheComboBD_ADO "sl_gr_ana", "cod_gr_ana", "descr_gr_ana", CbGrupo
    If gCodGrAnaUtilizador <> "" And BL_SeleccionaGrupoAnalises = True Then
        BG_MostraComboSel gCodGrAnaUtilizador, CbGrupo
    Else
        CbGrupo.ListIndex = -1
    End If

    
    'Preenche combo estado da requisi��o
    CbEstadoReq.AddItem "Sem an�lises"
    CbEstadoReq.AddItem "Espera de produtos"
    CbEstadoReq.AddItem "Espera de resultados"
    CbEstadoReq.AddItem "Espera de valida��o m�dica"
    CbEstadoReq.AddItem "Cancelada"
    CbEstadoReq.AddItem "Valida��o m�dica completa"
    CbEstadoReq.AddItem "N�o passar a hist�rico"
    CbEstadoReq.AddItem "Todas impressas"
    CbEstadoReq.AddItem "Hist�rico"
    CbEstadoReq.AddItem "Resultados parciais"
    CbEstadoReq.AddItem "Valida��o m�dica parcial"
    CbEstadoReq.AddItem "Impress�o parcial"
    CbEstadoReq.AddItem "Bloqueada"

    EcObsAna.ToolTipText = "OBSERVA��ES DA AN�LISE: Duplo click para aumentar."
    EcComFinal.ToolTipText = "COMENT�RIO FINAL: Duplo click para aumentar."
    
    EcInfoVisivel = EcInfo.Visible
End Sub
Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_previ", EcDataPrevista, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_chega", EcDataChega, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_imp", EcDataImpressao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_imp2", EcData2Via, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
        
    With FGAna
        .rows = 2
        .FixedRows = 1
        .Cols = 4
        .FixedCols = 0
        .AllowBigSelection = True
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 0
        .ColWidth(0) = 3440
        .ColWidth(1) = 1900
        .ColWidth(2) = 1900
        .ColWidth(3) = 2000
        .row = 0
        .Col = 0
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 0) = "An�lises da Requisi��o"
        .row = 0
        .Col = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 1) = "1� Resultado"
        .row = 0
        .Col = 2
        .CellAlignment = flexAlignCenterCenter
        If gLAB = "CITO" Then
            .TextMatrix(0, 2) = "Data de Sa�da"
        Else
            .TextMatrix(0, 2) = "2� Resultado"
        End If
        .row = 0
        .Col = 3
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 3) = "Estado"
        .WordWrap = False
        .ColAlignment(1) = flexAlignLeftCenter
    End With
    
    EcNumReq.Tag = adInteger
    If gCodGrupo = gGrupoAdministradores Or gCodGrupo = gGrupoMedicos Or gCodGrupo = gGrupoTecnicos Or gCodGrupo = gGrupoFarmaceuticos Or gTipoInstituicao = "HOSPITALAR" Then
        BtImpRes.Enabled = True
    Else
        BtImpRes.Enabled = False
    End If
End Sub

Sub Inicializacoes()

    Me.caption = " Consultar Requisi��es"
    Me.left = 440
    Me.top = 10
    Me.Width = 9975
    Me.Height = 7350 ' Normal
    'Me.Height = 8500 ' Campos Extras
    
    Set CampoDeFocus = EcNumReq
    
    NomeTabela = "sl_requis"
    NumCampos = 17
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "n_req"
    CamposBD(1) = "dt_previ"
    CamposBD(2) = "dt_chega"
    CamposBD(3) = "n_req_assoc"
    CamposBD(4) = "N_epis"
    CamposBD(5) = "estado_req"
    CamposBD(6) = "cod_proven"
    CamposBD(7) = "seq_utente"
    CamposBD(8) = "user_imp"
    CamposBD(9) = "user_imp2"
    CamposBD(10) = "t_urg"
    CamposBD(11) = "t_sit"
    CamposBD(12) = "dt_imp"
    CamposBD(13) = "dt_imp2"
    CamposBD(14) = "hr_imp"
    CamposBD(15) = "hr_imp2"
    CamposBD(16) = "req_aux"

    ' Campos do Ecr�
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcDataPrevista
    Set CamposEc(2) = EcDataChega
    Set CamposEc(3) = EcReqAssociada
    Set CamposEc(4) = EcEpisodio
    Set CamposEc(5) = EcCodEstadoReq
    Set CamposEc(6) = EcCodProv
    Set CamposEc(7) = EcCodUteSeq
    Set CamposEc(8) = EcCodUtilImp
    Set CamposEc(9) = EcCodUtilImp2
    Set CamposEc(10) = CbUrgencia
    Set CamposEc(11) = CbSituacao
    Set CamposEc(12) = EcDataImpressao
    Set CamposEc(13) = EcData2Via
    Set CamposEc(14) = EcHoraImpressao
    Set CamposEc(15) = EcHora2Via
    Set CamposEc(16) = EcReqAux

    'inicializacao do comando ADO para seleccao de dados do utente
    Set CmdUtente.ActiveConnection = gConexao
    CmdUtente.CommandType = adCmdText
    CmdUtente.CommandText = "SELECT utente, t_utente, n_proc_1, n_proc_2, nome_ute, " & _
                "dt_nasc_ute, dt_inscr FROM sl_identif WHERE seq_utente = ?"
    CmdUtente.Prepared = True
    CmdUtente.Parameters.Append CmdUtente.CreateParameter("SEQ_UTENTE", adInteger, adParamInput, 20)
     
    'Inicializacao do comando ADO para seleccao de dados do historico
    Set CmdHist.ActiveConnection = gConexao
    CmdHist.CommandType = adCmdText
    CmdHist.CommandText = "SELECT seq_realiza, cod_perfil, cod_ana_c, " & _
                                 "cod_ana_s,ord_ana_perf,ord_ana_compl " & _
                          "FROM sl_realiza_h " & _
                          "WHERE n_req = ? " & _
                          "ORDER BY Cod_perfil,ord_ana_perf,cod_ana_c,ord_ana_compl,cod_ana_s"
    CmdHist.Prepared = True
    CmdHist.Parameters.Append CmdHist.CreateParameter("NUM_REQ", adInteger, adParamInput, 7)
    
    'Inicializacao do RecordSet do ADO para seleccao de dados das realizacoes e marcacoes
    Set RegMarcadas = New ADODB.recordset
    'NELSONPSILVA 19.01.2018 CHVNG-9007 (adUseServer -> adUseClient)
    RegMarcadas.CursorLocation = adUseClient
    RegMarcadas.CursorType = adOpenForwardOnly
    RegMarcadas.LockType = adLockReadOnly
    RegMarcadas.ActiveConnection = gConexao
     
    'Inicializacao dos comandos ADO para seleccao de designacoes das analises
    Set CmdDesPerfil.ActiveConnection = gConexao
    Set CmdDesComplexa.ActiveConnection = gConexao
    Set CmdDesSimples.ActiveConnection = gConexao
    CmdDesPerfil.CommandType = adCmdText
    CmdDesComplexa.CommandType = adCmdText
    CmdDesSimples.CommandType = adCmdText
    CmdDesPerfil.CommandText = "SELECT descr_perfis FROM sl_perfis WHERE cod_perfis = ?"
    CmdDesComplexa.CommandText = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c = ?"
    CmdDesSimples.CommandText = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s = ?"
    CmdDesPerfil.Prepared = True
    CmdDesComplexa.Prepared = True
    CmdDesSimples.Prepared = True
    CmdDesPerfil.Parameters.Append CmdDesPerfil.CreateParameter("COD_ANA", adVarChar, adParamInput, 10)
    CmdDesComplexa.Parameters.Append CmdDesPerfil.CreateParameter("COD_ANA", adVarChar, adParamInput, 10)
    CmdDesSimples.Parameters.Append CmdDesPerfil.CreateParameter("COD_ANA", adVarChar, adParamInput, 10)
     
    'Inicializa��es dos comandos para ver se existem resultados para uma an�lise
    CmdSelRes.ActiveConnection = gConexao
    CmdSelRes.CommandType = adCmdText
    CmdSelRes.CommandText = " SELECT a.seq_realiza FROM sl_res_alfan a Where A.seq_realiza = ? " & _
                            " UNION " & _
                            " SELECT f.seq_realiza FROM sl_res_frase f Where F.seq_realiza = ? " & _
                            " UNION " & _
                            " SELECT m.seq_realiza FROM sl_res_micro m Where m.seq_realiza = ? " & _
                            " UNION " & _
                            " SELECT t.seq_realiza FROM sl_res_tsq t Where t.seq_realiza = ? "
    CmdSelRes.Prepared = True
    CmdSelRes.Parameters.Append CmdSelRes.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdSelRes.Parameters.Append CmdSelRes.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdSelRes.Parameters.Append CmdSelRes.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdSelRes.Parameters.Append CmdSelRes.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    
    CmdSelRes_H.ActiveConnection = gConexao
    CmdSelRes_H.CommandType = adCmdText
    CmdSelRes_H.CommandText = " SELECT a.seq_realiza FROM sl_res_alfan_h a Where A.seq_realiza = ? " & _
                            " UNION " & _
                            " SELECT f.seq_realiza FROM sl_res_frase_h f Where F.seq_realiza = ? " & _
                            " UNION " & _
                            " SELECT m.seq_realiza FROM sl_res_micro_h m Where m.seq_realiza = ? " & _
                            " UNION " & _
                            " SELECT t.seq_realiza FROM sl_res_tsq_h t Where t.seq_realiza = ? "
    CmdSelRes_H.Prepared = True
    CmdSelRes_H.Parameters.Append CmdSelRes.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdSelRes_H.Parameters.Append CmdSelRes.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdSelRes_H.Parameters.Append CmdSelRes.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdSelRes_H.Parameters.Append CmdSelRes.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    
    'Inicilaliza��es dos comandos para selec��o de resultados tipo numericos,alfanumericos
    CmdResAlf.ActiveConnection = gConexao
    CmdResAlf.CommandType = adCmdText
    CmdResAlf.CommandText = "SELECT * FROM sl_res_alfan WHERE seq_realiza=? order by n_res"
    CmdResAlf.Prepared = True
    CmdResAlf.Parameters.Append CmdResAlf.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
     
    CmdResAlf_H.ActiveConnection = gConexao
    CmdResAlf_H.CommandType = adCmdText
    CmdResAlf_H.CommandText = "SELECT * FROM sl_res_alfan_h WHERE seq_realiza=? order by n_res"
    CmdResAlf_H.Prepared = True
    CmdResAlf_H.Parameters.Append CmdResAlf.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
     
    'Inicializa��es dos comandos para selec��o de resultados tipo frase
    CmdResFr.ActiveConnection = gConexao
    CmdResFr.CommandType = adCmdText
    CmdResFr.CommandText = "SELECT * FROM sl_res_frase WHERE seq_realiza=? and ord_frase =? ORDER BY n_res ASC, ord_frase DESC "
    CmdResFr.Prepared = True
    CmdResFr.Parameters.Append CmdResFr.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdResFr.Parameters.Append CmdResFr.CreateParameter("ORD_FRASE", adInteger, adParamInput, 9)
     
    CmdResFr_H.ActiveConnection = gConexao
    CmdResFr_H.CommandType = adCmdText
    CmdResFr_H.CommandText = "SELECT * FROM sl_res_frase WHERE seq_realiza=? and ord_frase =? ORDER BY n_res ASC, ord_frase DESC "
    CmdResFr_H.Prepared = True
    CmdResFr_H.Parameters.Append CmdResFr_H.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdResFr_H.Parameters.Append CmdResFr_H.CreateParameter("ORD_FRASE", adInteger, adParamInput, 9)
     
    'Inicializa��o do comando para ir buscar a descri��o dos resultados tipo frase
    Set CmdResFrDescr.ActiveConnection = gConexao
    CmdResFrDescr.CommandType = adCmdText
    CmdResFrDescr.CommandText = "SELECT descr_frase FROM sl_dicionario WHERE cod_frase=?"
    CmdResFrDescr.Prepared = True
    CmdResFrDescr.Parameters.Append CmdResFrDescr.CreateParameter("COD_FRASE", adVarChar, adParamInput, 10)
     
    'Inicializa��es dos comandos para selec��o de resultados tipo microrganismo
    CmdResMicro.ActiveConnection = gConexao
    CmdResMicro.CommandType = adCmdText
    CmdResMicro.CommandText = "SELECT * FROM sl_res_micro WHERE seq_realiza=? and cod_micro = ? order by n_res"
    CmdResMicro.Prepared = True
    CmdResMicro.Parameters.Append CmdResMicro.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdResMicro.Parameters.Append CmdResMicro.CreateParameter("COD_MICRO", adVarChar, adParamInput, 15)
     
    CmdResMicro_H.ActiveConnection = gConexao
    CmdResMicro_H.CommandType = adCmdText
    CmdResMicro_H.CommandText = "SELECT * FROM sl_res_micro WHERE seq_realiza=? and cod_micro = ? order by n_res"
    CmdResMicro_H.Prepared = True
    CmdResMicro_H.Parameters.Append CmdResMicro_H.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdResMicro_H.Parameters.Append CmdResMicro_H.CreateParameter("COD_MICRO", adVarChar, adParamInput, 15)
     
    'Inicializa��o do comando para ir buscar a descri��o dos resultados tipo microrganismo
    CmdResMicroDescr.ActiveConnection = gConexao
    CmdResMicroDescr.CommandType = adCmdText
    CmdResMicroDescr.CommandText = "SELECT descr_microrg FROM sl_microrg WHERE cod_microrg=?"
    CmdResMicroDescr.Prepared = True
    CmdResMicroDescr.Parameters.Append CmdResMicroDescr.CreateParameter("COD_MICRORG", adVarChar, adParamInput, 15)
    
    'Inicializa��o do comando para ir buscar os TSQ's de um resultado tipo microrganismo
    CmdResMiTSQ.ActiveConnection = gConexao
    CmdResMiTSQ.CommandType = adCmdText
    CmdResMiTSQ.CommandText = "SELECT n_res,cod_antib,res_sensib,cmi,flg_imp FROM sl_res_tsq, sl_rel_grantib WHERE seq_realiza=? AND n_res = ? AND cod_micro=? AND cod_antib = cod_antibio AND cod_gr_antibio in ? and flg_imp = 'S' order by n_res,ordem"
    CmdResMiTSQ.Prepared = True
    CmdResMiTSQ.Parameters.Append CmdResMiTSQ.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 12)
    CmdResMiTSQ.Parameters.Append CmdResMiTSQ.CreateParameter("N_RES", adInteger, adParamInput, 1)
    CmdResMiTSQ.Parameters.Append CmdResMiTSQ.CreateParameter("COD_MICRO", adVarChar, adParamInput, 15)
    CmdResMiTSQ.Parameters.Append CmdResMiTSQ.CreateParameter("COD_GR_ANTIBIO", adBSTR, adParamInput, 15)
    
    CmdResMiTSQ_H.ActiveConnection = gConexao
    CmdResMiTSQ_H.CommandType = adCmdText
    CmdResMiTSQ_H.CommandText = "SELECT n_res,cod_antib,res_sensib,cmi,flg_imp FROM sl_res_tsq, sl_rel_grantib WHERE seq_realiza=? AND n_res = ? AND cod_micro=? AND cod_antib = cod_antibio AND cod_gr_antibio in ? and flg_imp = 'S' order by n_res,ordem"
    CmdResMiTSQ_H.Prepared = True
    CmdResMiTSQ_H.Parameters.Append CmdResMiTSQ_H.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 12)
    CmdResMiTSQ_H.Parameters.Append CmdResMiTSQ_H.CreateParameter("N_RES", adInteger, adParamInput, 1)
    CmdResMiTSQ_H.Parameters.Append CmdResMiTSQ_H.CreateParameter("COD_MICRO", adVarChar, adParamInput, 15)
    CmdResMiTSQ_H.Parameters.Append CmdResMiTSQ_H.CreateParameter("COD_GR_ANTIBIO", adBSTR, adParamInput, 15)
    
    'Inicializa��o do comando para ir buscar a descri��o e grupo dos antibioticos
    CmdResAntibiotico.ActiveConnection = gConexao
    CmdResAntibiotico.CommandType = adCmdText
    Select Case gSGBD
        Case gOracle
            CmdResAntibiotico.CommandText = "SELECT descr_antibio,cod_gr_antibio FROM sl_antibio,sl_rel_grantib WHERE " & _
                "sl_antibio.cod_antibio=? AND sl_rel_grantib.cod_antibio(+)=sl_antibio.cod_antibio"
        Case gInformix
            CmdResAntibiotico.CommandText = "SELECT descr_antibio,cod_gr_antibio FROM sl_antibio OUTER sl_rel_grantib WHERE " & _
                "sl_antibio.cod_antibio=? AND sl_rel_grantib.cod_antibio=sl_antibio.cod_antibio"
        Case gSqlServer
            CmdResAntibiotico.CommandText = "SELECT descr_antibio,cod_gr_antibio FROM sl_antibio,sl_rel_grantib WHERE " & _
                "sl_antibio.cod_antibio=? AND sl_rel_grantib.cod_antibio=*sl_antibio.cod_antibio"
    End Select
    CmdResAntibiotico.Prepared = True
    CmdResAntibiotico.Parameters.Append CmdResAntibiotico.CreateParameter("COD_ANTIBIO", adVarChar, adParamInput, 15)
     
    'Inicializa��es dos comandos para selec��o de resultados tipo antibiograma
    CmdResAntib.ActiveConnection = gConexao
    CmdResAntib.CommandType = adCmdText
    CmdResAntib.CommandText = "SELECT * FROM sl_res_tsq WHERE seq_realiza=?  and cod_micro=? order by n_res"
    CmdResAntib.Prepared = True
    CmdResAntib.Parameters.Append CmdResAntib.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdResAntib.Parameters.Append CmdResAntib.CreateParameter("COD_MICRO", adChar, adParamInput, 15)
     
    CmdResAntib_H.ActiveConnection = gConexao
    CmdResAntib_H.CommandType = adCmdText
    CmdResAntib_H.CommandText = "SELECT * FROM sl_res_tsq_h WHERE seq_realiza=?  and cod_micro=? order by n_res"
    CmdResAntib_H.Prepared = True
    CmdResAntib_H.Parameters.Append CmdResAntib.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdResAntib_H.Parameters.Append CmdResAntib.CreateParameter("COD_MICRO", adChar, adParamInput, 15)
     
    'Inicializa��es do comando para selec��o de tipos resultados
    CmdTres.ActiveConnection = gConexao
    CmdTres.CommandType = adCmdText
    CmdTres.CommandText = "SELECT sl_ana_s.t_result as t_result1, sl_segundo_res.t_result as t_result2 FROM sl_ana_s LEFT OUTER JOIN sl_segundo_res ON sl_ana_s.cod_ana_s = sl_segundo_res.cod_ana_s WHERE sl_ana_s.cod_ana_s=? "
    CmdTres.Prepared = True
    CmdTres.Parameters.Append CmdTres.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
     
    'Inicializa��o do camando para seleccionar os dados da tabela sl_obs_abna
    CmdObsAna.ActiveConnection = gConexao
    CmdObsAna.CommandType = adCmdText
    CmdObsAna.CommandText = "SELECT seq_obs_ana,descr_obs_ana FROM sl_obs_ana WHERE seq_realiza=?"
    CmdObsAna.Prepared = True
    CmdObsAna.Parameters.Append CmdObsAna.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 12)

    'Inicializa��es do comando para seleccionar dados da tabela sl_cm_fin
    CmdComFinal.ActiveConnection = gConexao
    CmdComFinal.CommandType = adCmdText
    CmdComFinal.CommandText = "SELECT seq_cm_fin,n_req,descr_cm_fin FROM sl_cm_fin WHERE n_req=?"
    CmdComFinal.Parameters.Append CmdComFinal.CreateParameter("N_REQ", adInteger, adParamInput)
     
    AnaIndex = -1
    ReDim AnalisesReq(0)
     
End Sub

Sub FuncaoProcurar()

    Dim SelTotal As Boolean
    Dim sql As String
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    EcReqAux.Text = UCase(EcReqAux.Text)
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
'    If SelTotal = True Then
        CriterioTabela = CriterioTabela & " Order by n_req desc"
'    End If
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer

    rs.Open CriterioTabela, gConexao

    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        'N�o Limpa o C�digo do Grupo
        LimpaCampos
        PreencheCampos
        BL_Toolbar_BotaoEstado "Anterior", "Activo"
        BL_Toolbar_BotaoEstado "Seguinte", "Activo"
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoLimpar()

        LimpaCampos
        'CbGrupo.ListIndex = mediComboValorNull
        'EcCodGrupo.Text = ""
        CampoDeFocus.SetFocus
        
End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        
        LimpaCampos
        CampoDeFocus.SetFocus
                
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
        
        Set CmdUtente = Nothing
        Set CmdHist = Nothing
        Set CmdDesPerfil = Nothing
        Set CmdDesComplexa = Nothing
        Set CmdDesSimples = Nothing

    Else
        Unload Me
    End If
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
     
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        PreencheEnvioEResults
        PreencheFlexGrid
        PreencheEstadoRequisicao
    End If
    
End Sub

Sub PreencheEstadoRequisicao()
    
    Select Case (EcCodEstadoReq.Text)
        Case gEstadoReqSemAnalises
            CbEstadoReq.ListIndex = 0
        Case gEstadoReqEsperaProduto
            CbEstadoReq.ListIndex = 1
        Case gEstadoReqEsperaResultados
            CbEstadoReq.ListIndex = 2
        Case gEstadoReqEsperaValidacao
            CbEstadoReq.ListIndex = 3
        Case gEstadoReqCancelada
            CbEstadoReq.ListIndex = 4
        Case gEstadoReqValicacaoMedicaCompleta
            CbEstadoReq.ListIndex = 5
        Case gEstadoReqNaoPassarHistorico
            CbEstadoReq.ListIndex = 6
        Case gEstadoReqTodasImpressas
            CbEstadoReq.ListIndex = 7
        Case gEstadoReqHistorico
            CbEstadoReq.ListIndex = 8
        Case gEstadoReqResultadosParciais
            CbEstadoReq.ListIndex = 9
        Case gEstadoReqValidacaoMedicaParcial
            CbEstadoReq.ListIndex = 10
        Case gEstadoReqImpressaoParcial
            CbEstadoReq.ListIndex = 11
        Case gEstadoReqBloqueada
            CbEstadoReq.ListIndex = 12
        Case Else
            CbEstadoReq.ListIndex = mediComboValorNull
    End Select
    
End Sub

Sub PreencheFlexGrid()

    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim LastPerfil As String
    Dim LastComplexa As String
    Dim Flg_Insere As Boolean
    Dim Inc As Integer
    Dim t As Integer
    Dim encontrou As Boolean
    Dim RsComFinal As ADODB.recordset
    
    APreencherFgAna = True
    
    Select Case Trim(EcCodEstadoReq.Text)
        Case gEstadoReqEsperaProduto
            SelMarcadas
        Case gEstadoReqEsperaResultados
            AnaIndex = -1
            SelMarcadas
        Case gEstadoReqNaoPassarHistorico
            SelHistorico
        Case Else
            AnaIndex = -1
            SelMarcadas
    End Select
    
    LastPerfil = ""
    LastComplexa = ""
    If AnaIndex <> -1 Then
        If gCodGrupo = gGrupoAdministradores Or gCodGrupo = gGrupoMedicos Or gCodGrupo = gGrupoTecnicos Or gCodGrupo = gGrupoFarmaceuticos Or gTipoInstituicao = "HOSPITALAR" Then
            BtImpRes.Enabled = True
        Else
            BtImpRes.Enabled = False
        End If
        i = 0
            j = 1
        While i < AnaIndex
            'Verificar se esta an�lise � igual � anterior;se sim insere na grid sen�o procura at� ao fim para ver se existe uma igual, caso exista insere a seguir
            Inc = i
            If Inc > 0 Then
                If AnalisesReq(Inc).CodPerfil = AnalisesReq(Inc - 1).CodPerfil And _
                   AnalisesReq(Inc).CodComplexa = AnalisesReq(Inc - 1).CodComplexa And _
                   AnalisesReq(Inc).GridEstado = 0 Then
                   
                    Inc = i
                Else
                    'Verificar na restante estrutura
                    encontrou = False
                    t = Inc
                    While t < AnaIndex And encontrou = False
                        If AnalisesReq(t).CodPerfil = AnalisesReq(Inc - 1).CodPerfil And _
                           AnalisesReq(t).CodComplexa = AnalisesReq(Inc - 1).CodComplexa And _
                           AnalisesReq(t).GridEstado = 0 And _
                           LastComplexa <> "" And LastComplexa <> "0" And _
                           LastPerfil <> "" And LastPerfil <> "0" Then
                            encontrou = True
                            Inc = t
                        End If
                        t = t + 1
                    Wend
                    If encontrou = False Then Inc = i
                End If
            End If
            
            
            If AnalisesReq(Inc).GridEstado = 0 Then
                ' Perfil
                If AnalisesReq(Inc).CodPerfil <> "0" Then
                    If LastPerfil <> AnalisesReq(Inc).CodPerfil Then
                        FGAna.row = j
                        FGAna.Col = 0
                        FGAna.CellBackColor = &HE0E0E0
                        FGAna.Col = 1
                        FGAna.CellBackColor = &HE0E0E0
                        FGAna.Col = 2
                        FGAna.CellBackColor = &HE0E0E0
                        FGAna.Col = 3
                        FGAna.CellBackColor = &HE0E0E0
                        FGAna.Col = 0
    
                        FGAna.TextMatrix(j, 0) = AnalisesReq(Inc).DescrPerfil
                        AnalisesReq(Inc).GridIndice = j
                        j = j + 1
                        FGAna.AddItem "", j
                        LastPerfil = AnalisesReq(Inc).CodPerfil
                    End If
                Else
                    LastPerfil = ""
                End If
                
                'Complexa
                If AnalisesReq(Inc).CodComplexa <> "0" And AnalisesReq(Inc).CodComplexa <> "C99999" Then
                    If LastComplexa <> AnalisesReq(Inc).CodComplexa Then
                        FGAna.row = j
                        FGAna.Col = 0
                        FGAna.CellBackColor = &HE0E0E0
                        FGAna.Col = 1
                        FGAna.CellBackColor = &HE0E0E0
                        FGAna.Col = 2
                        FGAna.CellBackColor = &HE0E0E0
                        FGAna.Col = 3
                        FGAna.CellBackColor = &HE0E0E0
                        FGAna.Col = 0
    
                        FGAna.TextMatrix(j, 0) = IIf(AnalisesReq(Inc).CodPerfil <> "0", "   ", "") & AnalisesReq(Inc).DescrComplexa
                        AnalisesReq(Inc).GridIndice = j
                        j = j + 1
                        LastComplexa = AnalisesReq(Inc).CodComplexa
                        FGAna.AddItem "", j
                    End If
                Else
                    LastComplexa = ""
                End If
                
                'Simples
                If AnalisesReq(Inc).CodSimples <> "S99999" Then
                    AnalisesReq(Inc).GridIndice = j
                    FGAna.row = j
                    FGAna.Col = 0
                    MudaCorFlexGrid FGAna.row, AnalisesReq(Inc).AnaEstado, AnalisesReq(Inc).TemProduto
                    FGAna.Col = 1
                    MudaCorFlexGrid FGAna.row, AnalisesReq(Inc).AnaEstado, AnalisesReq(Inc).TemProduto
                    FGAna.Col = 2
                    MudaCorFlexGrid FGAna.row, AnalisesReq(Inc).AnaEstado, AnalisesReq(Inc).TemProduto
                    FGAna.Col = 3
                    MudaCorFlexGrid FGAna.row, AnalisesReq(Inc).AnaEstado, AnalisesReq(Inc).TemProduto
                    FGAna.Col = 0
                    FGAna.TextMatrix(j, 0) = IIf(AnalisesReq(Inc).CodPerfil <> "0", "   ", "") & IIf(AnalisesReq(Inc).CodComplexa <> "0", "   ", "") & AnalisesReq(Inc).DescrSimples
                    FGAna.ColAlignment(0) = flexAlignLeftCenter
                    FGAna.MergeCol(0) = True
                    j = j + 1
                    FGAna.AddItem "", j
                    'Resultado da an�lise
                    For k = 1 To resultados.Count
                        If AnalisesReq(Inc).CodSimples = resultados(k).DadosAna.codAnaS And _
                           AnalisesReq(Inc).CodComplexa = resultados(k).DadosAna.CodAnaC And _
                           AnalisesReq(Inc).CodPerfil = resultados(k).DadosAna.CodPerfil And _
                           AnalisesReq(Inc).OrdemFrase = resultados(k).DadosAna.OrdemFrase And AnalisesReq(Inc).CodMicro = resultados(k).DadosAna.ResMicro(1).CodMicro Then
                            'O campo "estado" neste caso vai servir para guardar o ROW actual da GRID
                            resultados(k).DadosAna.estado = j - 1
                            If gCodGrupo = gGrupoAdministradores Or gCodGrupo = gGrupoMedicos Or gCodGrupo = gGrupoTecnicos Or gTipoInstituicao = "HOSPITALAR" Or gCodGrupo = gGrupoFarmaceuticos Then
                                If (AnalisesReq(Inc).AnaEstado = gEstadoAnaImpressa Or AnalisesReq(Inc).AnaEstado = gEstadoAnaValidacaoMedica) And gCodGrupo = gGrupoSecretariado Then
                                    FGAna.TextMatrix(j - 1, 1) = resultados(k).DadosAna.res
                                    FGAna.TextMatrix(j - 1, 2) = resultados(k).DadosAna.Dados2Res.res
                                ElseIf gCodGrupo = gGrupoAdministradores Or gCodGrupo = gGrupoMedicos Or gCodGrupo = gGrupoTecnicos Or gCodGrupo = gGrupoFarmaceuticos Then
                                    FGAna.TextMatrix(j - 1, 1) = resultados(k).DadosAna.res
                                    FGAna.TextMatrix(j - 1, 2) = resultados(k).DadosAna.Dados2Res.res
                                End If
                            End If
                            Exit For
                        End If
                    Next k
                End If
            End If
            i = i + 1
            AnalisesReq(Inc).GridEstado = 1
        Wend
        If j <> 1 Then
            FGAna.RemoveItem j
        End If
    Else
        BtImpRes.Enabled = False
        FGAna.Clear
        FGAna.row = 0
        FGAna.Col = 0
        FGAna.CellAlignment = flexAlignCenterCenter
        FGAna.TextMatrix(0, 0) = "An�lises da Requisi��o"
        FGAna.Col = 1
        FGAna.CellAlignment = flexAlignCenterCenter
        FGAna.TextMatrix(0, 1) = "1� Resultados"
        FGAna.Col = 2
        FGAna.CellAlignment = flexAlignCenterCenter
        If gLAB = "CITO" Then
            FGAna.TextMatrix(0, 2) = "Data de Sa�da"
        Else
            FGAna.TextMatrix(0, 2) = "2� Resultados"
        End If
        FGAna.Col = 3
        FGAna.CellAlignment = flexAlignCenterCenter
        FGAna.TextMatrix(0, 3) = "Estado"
        FGAna.WordWrap = False
    End If
    
    'Coment�rios Finais da requisi��o
    EcComFinal = ""
    CmdComFinal.Parameters(0).value = Trim(EcNumReq.Text)
    Set RsComFinal = CmdComFinal.Execute
    If Not RsComFinal.EOF Then
        EcComFinal.TextRTF = "" & RsComFinal!descr_cm_fin
    End If
    RsComFinal.Close
    Set RsComFinal = Nothing
        
    APreencherFgAna = False
    
End Sub

Sub LimpaCampos()

    Dim i As Integer
    
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    CbSituacao.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    
    APreencherFgAna = True
    
    i = FGAna.rows - 2
    While i > 0
        FGAna.RemoveItem i
        i = i - 1
    Wend
    
    'Limpar as classes
    LimpaColeccao resultados, -1
    
    FGAna.Clear
    FGAna.row = 0
    FGAna.Col = 0
    FGAna.CellAlignment = flexAlignCenterCenter
    FGAna.TextMatrix(0, 0) = "An�lises da Requisi��o"
    FGAna.Col = 1
    FGAna.CellAlignment = flexAlignCenterCenter
    FGAna.TextMatrix(0, 1) = "1� Resultados"
    FGAna.Col = 2
    FGAna.CellAlignment = flexAlignCenterCenter
    If gLAB = "CITO" Then
        FGAna.TextMatrix(0, 2) = "Data de Sa�da"
    Else
        FGAna.TextMatrix(0, 2) = "2� Resultados"
    End If
    FGAna.Col = 3
    FGAna.CellAlignment = flexAlignCenterCenter
    FGAna.TextMatrix(0, 3) = "Estado"
    FGAna.WordWrap = False
    
    CbEstadoReq.ListIndex = mediComboValorNull
    EcDescrProveniencia = ""
    EcDataInscr = ""
    EcDataNasc = ""
    EcNome = ""
    EcProcHosp1 = ""
    EcProcHosp2 = ""
    EcTipoUtente = ""
    EcUtente = ""
    EcUtil2Via = ""
    EcUtilImpressao = ""
    EcInfo.Text = ""
    EcObsAna.Text = ""
    EcComFinal.Text = ""
    BtImpRes.Enabled = False
    EcSeqRel = ""
    
    AnaIndex = -1
    ReDim AnalisesReq(0)
    
    APreencherFgAna = False
    LbeResults = ""
End Sub


Function SelDescrPerfil(codAna As String) As String
    Dim rsDescr As ADODB.recordset
    Dim ret As String
    
    If Trim(codAna) = "" Or Trim(codAna) = "0" Then
        ret = ""
    Else
    
        CmdDesPerfil.Parameters(0).value = codAna
        Set rsDescr = CmdDesPerfil.Execute
        If Not rsDescr.EOF Then
            ret = BL_HandleNull(rsDescr!descr_perfis, codAna)
        Else
            ret = codAna & "(Erro a seleccionar descri��o)"
        End If
        
        rsDescr.Close
        Set rsDescr = Nothing
    
    End If
    
    SelDescrPerfil = ret
End Function
Function SelDescrComplexa(codAna As String) As String
    Dim rsDescr As ADODB.recordset
    Dim ret As String
    
    If Trim(codAna) = "" Or Trim(codAna) = "0" Then
        ret = ""
    Else
        CmdDesComplexa.Parameters(0).value = Trim(codAna)
        Set rsDescr = CmdDesComplexa.Execute
        If Not rsDescr.EOF Then
            ret = BL_HandleNull(rsDescr!Descr_Ana_C, codAna)
        Else
            ret = codAna & "(Erro a seleccionar descri��o)"
        End If
        
        rsDescr.Close
        Set rsDescr = Nothing
    
    End If
    
    SelDescrComplexa = ret
End Function

Function SelDescrSimples(codAna As String) As String
    Dim rsDescr As ADODB.recordset
    Dim ret As String
    
    If Trim(codAna) = "" Or Trim(codAna) = "0" Or Trim(codAna) = "S99999" Then
        ret = ""
    Else
        
        CmdDesSimples.Parameters(0).value = Trim(codAna)
        Set rsDescr = CmdDesSimples.Execute
        If Not rsDescr.EOF Then
            ret = BL_HandleNull(rsDescr!descr_ana_s, codAna)
        Else
            ret = codAna & "(Erro a seleccionar descri��o)"
        End If
        
        rsDescr.Close
        Set rsDescr = Nothing
    
    End If
    
    SelDescrSimples = ret
End Function

Sub SelHistorico()

    Dim RsHistorico As New ADODB.recordset
    Dim RsSelRes As ADODB.recordset
    Dim RsTres  As ADODB.recordset
    Dim iRes As Long
    Dim sSql As String
    
'    Set CmdProdAna.ActiveConnection = gConexao
'    CmdProdAna.CommandType = adCmdText
'    CmdProdAna.CommandText = " SELECT cod_produto " & _
'                             " FROM sl_ana_s " & _
'                             " WHERE cod_ana_s = ?"
'    CmdProdAna.Prepared = True
'    CmdProdAna.Parameters.Append CmdProdAna.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 8)
    
    
    'AN�LISES DA REQUISI��O

    'Verifica se h� restri��o de an�lises
    If Trim(EcCodGrupo.Text) <> "" Then
        'An�lises do Grupo seleccionado
        If gSGBD = gOracle Then
            RsHistorico.Source = " SELECT R.seq_realiza,R.ord_ana,R.cod_agrup,R.cod_perfil,R.ord_ana_perf,R.cod_ana_c,R.ord_ana_compl,R.cod_ana_s,R.flg_estado, F.ord_frase, M.cod_micro " & _
                                 " FROM sl_realiza_h R,  sl_res_frase_h F , sl_res_micro_h M, sl_ana_s " & _
                                 " WHERE " & _
                                 " R.n_req =" & Trim(EcNumReq.Text) & _
                                 " AND sl_ana_s.cod_ana_s=R.cod_ana_s " & _
                                 " AND sl_ana_s.gr_ana=" & BL_TrataStringParaBD(Trim(EcCodGrupo.Text)) & _
                                 " AND R.seq_realiza = F.seq_realiza(+) and F.n_res(+) = 1 " & _
                                 " AND R.seq_realiza = M.seq_realiza(+) and M.n_res(+) = 1 " & _
                                 " ORDER BY 2,3,4,5,6,7,8,10 "
        ElseIf gSGBD = gSqlServer Then
            RsHistorico.Source = " SELECT R.seq_realiza,R.ord_ana,R.cod_agrup,R.cod_perfil,R.ord_ana_perf,R.cod_ana_c,R.ord_ana_compl,R.cod_ana_s,R.flg_estado, F.ord_frase, M.cod_micro " & _
                                 " FROM sl_realiza_h R LEFT OUTER JOIN sl_res_frase_h F ON (R.seq_realiza = F.seq_realiza AND F.n_res = 1 ) " & _
                                 " LEFT OUTER JOIN sl_res_micro_h M ON (m.seq_realiza = r.seq_realiza AND M.n_res = 1) ,sl_ana_s " & _
                                 " WHERE " & _
                                 " R.n_req =" & Trim(EcNumReq.Text) & _
                                 " AND sl_ana_s.cod_ana_s=R.cod_ana_s " & _
                                 " AND sl_ana_s.gr_ana=" & BL_TrataStringParaBD(Trim(EcCodGrupo.Text)) & _
                                 " ORDER BY 2,3,4,5,6,7,8,10 "
        End If
        
    Else
        'An�lises de todos os Grupos
        If gSGBD = gOracle Then
            RsHistorico.Source = "( select  r.seq_realiza, r.ord_ana, r.cod_agrup,r.cod_perfil, r.ord_ana_perf, r.cod_ana_c, r.ord_ana_compl, " & _
                                 " r.cod_ana_s, r.flg_estado, s.gr_ana, f.ord_frase, M.cod_micro " & _
                                 " FROM sl_realiza_h r, sl_ana_s s, sl_res_frase_h f, sl_res_micro_h m " & _
                                 " WHERE r.n_req = " & Trim(EcNumReq.Text) & _
                                 " and s.cod_ana_s(+) = r.cod_ana_s " & _
                                 " AND R.seq_realiza = F.seq_realiza(+) and F.n_res(+) = 1 " & _
                                 " AND R.seq_realiza = M.seq_realiza(+) AND M.n_res(+) = 1) " & _
                                 " ORDER BY 2,3,4,5,6,7,8,11 "
        ElseIf gSGBD = gSqlServer Then
            RsHistorico.Source = "( select  r.seq_realiza, r.ord_ana, r.cod_agrup,r.cod_perfil, r.ord_ana_perf, r.cod_ana_c, r.ord_ana_compl, " & _
                                 " r.cod_ana_s, r.flg_estado, s.gr_ana, f.ord_frase, M.cod_micro " & _
                                 " FROM sl_realiza_h r LEFT OUTER JOIN sl_res_frase_h f ON (R.seq_realiza = F.seq_realiza AND F.n_res = 1 )" & _
                                 " LEFT OUTER JOIN sl_res_micro_h m ON (R.seq_realiza = M.seq_realiza AND M.n_res = 1) " & _
                                 " LEFT OUTER JOIN sl_ana_s s on r.cod_ana_s = s.cod_ana_S " & _
                                 " WHERE r.n_req = " & Trim(EcNumReq.Text) & ") " & _
                                 " ORDER BY 2,3,4,5,6,7,8,11 "
        End If

    End If
    
    'CmdHist.Parameters(0).Value = Trim(EcNumReq.text)
    'Set RsHistorico = CmdHist.Execute

    RsHistorico.CursorLocation = adUseServer
    RsHistorico.CursorType = adOpenForwardOnly
    RsHistorico.LockType = adLockReadOnly
    RsHistorico.ActiveConnection = gConexao
    RsHistorico.Open
    iRes = 1
    CriaNovaClasse resultados, "RES", 1, True
    If RsHistorico.RecordCount > 0 Then
        If AnaIndex = -1 Then AnaIndex = 0
        While Not RsHistorico.EOF
            AnalisesReq(AnaIndex).CodPerfil = BL_HandleNull(RsHistorico!Cod_Perfil, "0")
            AnalisesReq(AnaIndex).DescrPerfil = SelDescrPerfil(AnalisesReq(AnaIndex).CodPerfil)
            AnalisesReq(AnaIndex).CodComplexa = BL_HandleNull(RsHistorico!cod_ana_c, "0")
            AnalisesReq(AnaIndex).DescrComplexa = SelDescrComplexa(AnalisesReq(AnaIndex).CodComplexa)
            AnalisesReq(AnaIndex).CodSimples = BL_HandleNull(RsHistorico!cod_ana_s, "0")
            AnalisesReq(AnaIndex).DescrSimples = SelDescrSimples(AnalisesReq(AnaIndex).CodSimples)
            AnalisesReq(AnaIndex).OrdemFrase = BL_HandleNull(RsHistorico!ord_frase, 0)
            AnalisesReq(AnaIndex).AnaEstado = 0
                       
            'verificar resultados
            sSql = " SELECT a.seq_realiza FROM sl_res_alfan_h a Where A.seq_realiza =  " & RsHistorico!seq_realiza & _
                        " UNION " & _
                        " SELECT f.seq_realiza FROM sl_res_frase_h f Where F.seq_realiza =  " & RsHistorico!seq_realiza & _
                        " UNION " & _
                        " SELECT m.seq_realiza FROM sl_res_micro_h m Where m.seq_realiza =  " & RsHistorico!seq_realiza & _
                        " UNION " & _
                        " SELECT t.seq_realiza FROM sl_res_tsq_h t Where t.seq_realiza =  " & RsHistorico!seq_realiza
            
            Set RsSelRes = New ADODB.recordset
            RsSelRes.CursorType = adOpenStatic
            RsSelRes.CursorLocation = adUseServer
            RsSelRes.Open sSql, gConexao

            If Not RsSelRes.EOF Then
                resultados.item(iRes).DadosAna.CodPerfil = AnalisesReq(AnaIndex).CodPerfil
                resultados.item(iRes).DadosAna.DescrPerfil = AnalisesReq(AnaIndex).DescrPerfil
                resultados.item(iRes).DadosAna.CodAnaC = AnalisesReq(AnaIndex).CodComplexa
                resultados.item(iRes).DadosAna.DescrAnaC = AnalisesReq(AnaIndex).DescrComplexa
                resultados.item(iRes).DadosAna.codAnaS = AnalisesReq(AnaIndex).CodSimples
                resultados.item(iRes).DadosAna.DescrAnaS = AnalisesReq(AnaIndex).DescrSimples
                resultados.item(iRes).DadosAna.OrdemFrase = AnalisesReq(AnaIndex).OrdemFrase
                CmdTres.Parameters(0).value = AnalisesReq(AnaIndex).CodSimples
                Set RsTres = CmdTres.Execute
                If Not RsTres.EOF Then
                    resultados.item(iRes).DadosAna.TRes = BL_HandleNull(RsTres!t_result1, 0)
                    resultados.item(iRes).DadosAna.Dados2Res.TRes = BL_HandleNull(RsTres!t_result2, 0)
                End If
                RsTres.Close
                Set RsTres = Nothing
                SeleccionaResultado_h iRes, RsHistorico!seq_realiza, BL_HandleNull(RsHistorico!cod_micro, ""), resultados.item(iRes).DadosAna.OrdemFrase
                
                If resultados(iRes).DadosAna.ObsAna <> "" Then
                    AnalisesReq(AnaIndex).DescrSimples = "(Ver Obs.) " & AnalisesReq(AnaIndex).DescrSimples
                End If

                iRes = iRes + 1
                CriaNovaClasse resultados, "RES", iRes
            End If
                        
            RsSelRes.Close
            Set RsSelRes = Nothing
            
            AnaIndex = AnaIndex + 1
            
            RsHistorico.MoveNext
            ReDim Preserve AnalisesReq(AnaIndex)
        Wend
    Else
        AnaIndex = -1
    End If

    RsHistorico.Close
    Set RsHistorico = Nothing

End Sub

Sub SelMarcadas()
    
    Dim RsProdAna As ADODB.recordset
    Dim RsSelRes As ADODB.recordset
    Dim RsDataProd As ADODB.recordset
    Dim RsTres  As ADODB.recordset
    Dim CmdProdAna As New ADODB.Command
    Dim ProdutoAna As String
    Dim sSql As String
    Dim iRes As Long
    Dim i As Integer
    Dim j As Integer
    
    iRes = 1
    CriaNovaClasse resultados, "RES", 1, True
    
    Set CmdProdAna.ActiveConnection = gConexao
    CmdProdAna.CommandType = adCmdText
    CmdProdAna.CommandText = " SELECT cod_produto " & _
                             " FROM sl_ana_s " & _
                             " WHERE cod_ana_s = ?"
    CmdProdAna.Prepared = True
    CmdProdAna.Parameters.Append CmdProdAna.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 8)
    
    
    'AN�LISES DA REQUISI��O

    'Verifica se h� restri��o de an�lises
    If Trim(EcCodGrupo.Text) <> "" Then
        'An�lises do Grupo seleccionado
        RegMarcadas.Source = " SELECT R.seq_realiza,R.ord_ana,R.cod_agrup,R.cod_perfil,R.ord_ana_perf,R.cod_ana_c,R.ord_ana_compl,R.cod_ana_s,R.flg_estado, F.ord_frase, M.cod_micro " & _
                             " FROM sl_realiza R LEFT OUTER JOIN sl_res_frase F ON (R.seq_realiza = F.seq_realiza AND F.n_res = 1 ) " & _
                             " LEFT OUTER JOIN sl_res_micro M ON (m.seq_realiza = r.seq_realiza AND M.n_res = 1) ,sl_ana_s " & _
                             " WHERE " & _
                             " R.n_req =" & Trim(EcNumReq.Text) & _
                             " AND sl_ana_s.cod_ana_s=R.cod_ana_s " & _
                             " AND sl_ana_s.gr_ana=" & BL_TrataStringParaBD(Trim(EcCodGrupo.Text)) & _
                             " UNION " & _
                             " SELECT  -1 as seq_realiza,M.ord_ana,M.cod_agrup,M.cod_perfil,M.ord_ana_perf,M.cod_ana_c,M.ord_ana_compl,M.cod_ana_s, '-1' as flg_estado, 0, null " & _
                             " FROM sl_marcacoes M,sl_ana_s " & _
                             " WHERE " & _
                             " M.n_req =" & Trim(EcNumReq.Text) & _
                             " AND sl_ana_s.cod_ana_s=M.cod_ana_s " & _
                             " AND sl_ana_s.gr_ana=" & BL_TrataStringParaBD(Trim(EcCodGrupo.Text)) & _
                             " ORDER BY 2,3,4,5,6,7,8,10 "
        
    Else
        RegMarcadas.Source = "( select  r.seq_realiza, r.ord_ana, r.cod_agrup,r.cod_perfil, r.ord_ana_perf, r.cod_ana_c, r.ord_ana_compl, " & _
                             " r.cod_ana_s, r.flg_estado, s.gr_ana, f.ord_frase, M.cod_micro " & _
                             " FROM sl_realiza r LEFT OUTER JOIN sl_res_frase f ON (R.seq_realiza = F.seq_realiza AND F.n_res = 1 )" & _
                             " LEFT OUTER JOIN sl_res_micro m ON (R.seq_realiza = M.seq_realiza AND M.n_res = 1) " & _
                             " LEFT OUTER JOIN sl_ana_s s on r.cod_ana_s = s.cod_ana_S " & _
                             " WHERE r.n_req = " & Trim(EcNumReq.Text) & ") " & _
                             "  Union ALL" & _
                             "( select  -1 as seq_realiza,m.ord_ana,m.cod_agrup,m.cod_perfil, m.ord_ana_perf, m.cod_ana_c, " & _
                             " m.ord_ana_compl ,  m.cod_ana_s, '-1' as flg_estado, s.gr_ana, 0, null " & _
                             " FROM sl_marcacoes m LEFT OUTER JOIN sl_ana_s s ON  s.cod_ana_s = m.cod_ana_s " & _
                             " WHERE m.n_req =" & Trim(EcNumReq.Text) & _
                             "  )" & _
                             " ORDER BY 2,3,4,5,6,7,8,11 "

    End If
    
    
    'Abre o RecordSet
    RegMarcadas.Open
    
    If RegMarcadas.RecordCount <> 0 Then
        If AnaIndex = -1 Then AnaIndex = 0
        For i = 1 To RegMarcadas.RecordCount
            'Bruno 08-01-2003
'            If Trim(RegMarcadas!cod_ana_s) = "S99999" And Len(RegMarcadas!Cod_Perfil) <> 0 Then
'               AnalisesReq(AnaIndex).CodPerfil = BL_HandleNull(RegMarcadas!Cod_Perfil, "0")
'               AnalisesReq(AnaIndex).DescrPerfil = SelDescrPerfil(AnalisesReq(AnaIndex).CodPerfil)
'               j = j + 1
'
'               If j > 1 Then
'                  GoTo TrataGhost
'               End If
'
'            Else
        
                AnalisesReq(AnaIndex).CodPerfil = BL_HandleNull(RegMarcadas!Cod_Perfil, "0")
                AnalisesReq(AnaIndex).DescrPerfil = SelDescrPerfil(AnalisesReq(AnaIndex).CodPerfil)
                AnalisesReq(AnaIndex).CodComplexa = BL_HandleNull(RegMarcadas!cod_ana_c, "0")
                AnalisesReq(AnaIndex).DescrComplexa = SelDescrComplexa(AnalisesReq(AnaIndex).CodComplexa)
                AnalisesReq(AnaIndex).CodSimples = BL_HandleNull(RegMarcadas!cod_ana_s, "0")
                AnalisesReq(AnaIndex).DescrSimples = SelDescrSimples(AnalisesReq(AnaIndex).CodSimples)
                AnalisesReq(AnaIndex).OrdemFrase = BL_HandleNull(RegMarcadas!ord_frase, 0)
                AnalisesReq(AnaIndex).CodMicro = BL_HandleNull(RegMarcadas!cod_micro, "")
'            End If
            AnalisesReq(AnaIndex).GridEstado = 0
            AnalisesReq(AnaIndex).AnaEstado = -1
                        
            If BL_HandleNull(RegMarcadas!seq_realiza, -1) <> -1 Then
                'Vem da tabela de realizacoes -> verificar resultados
                sSql = " SELECT a.seq_realiza FROM sl_res_alfan a Where A.seq_realiza =  " & RegMarcadas!seq_realiza & _
                            " UNION " & _
                            " SELECT f.seq_realiza FROM sl_res_frase f Where F.seq_realiza =  " & RegMarcadas!seq_realiza & _
                            " UNION " & _
                            " SELECT m.seq_realiza FROM sl_res_micro m Where m.seq_realiza =  " & RegMarcadas!seq_realiza & _
                            " UNION " & _
                            " SELECT t.seq_realiza FROM sl_res_tsq t Where t.seq_realiza =  " & RegMarcadas!seq_realiza
                
                CmdSelRes.Parameters(0).value = RegMarcadas!seq_realiza
                CmdSelRes.Parameters(1).value = RegMarcadas!seq_realiza
                CmdSelRes.Parameters(2).value = RegMarcadas!seq_realiza
                CmdSelRes.Parameters(3).value = RegMarcadas!seq_realiza
                Set RsSelRes = CmdSelRes.Execute
'                Set RsSelRes = New ADODB.recordset
'                RsSelRes.CursorType = adOpenStatic
'                RsSelRes.CursorLocation = adUseServer
'                RsSelRes.Open sSql, gConexao
                
                If Not RsSelRes.EOF Then
                    resultados.item(iRes).DadosAna.CodPerfil = AnalisesReq(AnaIndex).CodPerfil
                    resultados.item(iRes).DadosAna.DescrPerfil = AnalisesReq(AnaIndex).DescrPerfil
                    resultados.item(iRes).DadosAna.CodAnaC = AnalisesReq(AnaIndex).CodComplexa
                    resultados.item(iRes).DadosAna.DescrAnaC = AnalisesReq(AnaIndex).DescrComplexa
                    resultados.item(iRes).DadosAna.codAnaS = AnalisesReq(AnaIndex).CodSimples
                    resultados.item(iRes).DadosAna.DescrAnaS = AnalisesReq(AnaIndex).DescrSimples
                    resultados.item(iRes).DadosAna.OrdemFrase = AnalisesReq(AnaIndex).OrdemFrase
                    CmdTres.Parameters(0).value = AnalisesReq(AnaIndex).CodSimples
                    Set RsTres = CmdTres.Execute
                    If Not RsTres.EOF Then
                        resultados.item(iRes).DadosAna.TRes = BL_HandleNull(RsTres!t_result1, 0)
                        resultados.item(iRes).DadosAna.Dados2Res.TRes = BL_HandleNull(RsTres!t_result2, 0)
                    End If
                    RsTres.Close
                    Set RsTres = Nothing
                    
                    If gCodGrupo = gGrupoAdministradores Or gCodGrupo = gGrupoMedicos Or gCodGrupo = gGrupoTecnicos Or gCodGrupo = gGrupoFarmaceuticos Or gTipoInstituicao = "HOSPITALAR" Then
                        SeleccionaResultado iRes, RegMarcadas!seq_realiza, BL_HandleNull(RegMarcadas!cod_micro, ""), BL_HandleNull(RegMarcadas!ord_frase, 0)
                    Else
                        If BL_HandleNull(RegMarcadas!flg_estado, 0) = 3 Or BL_HandleNull(RegMarcadas!flg_estado, 0) = 4 Then
                            SeleccionaResultado iRes, RegMarcadas!seq_realiza, BL_HandleNull(RegMarcadas!cod_micro, ""), BL_HandleNull(RegMarcadas!ord_frase, 0)
                        End If
                    End If
                    
                    If resultados(iRes).DadosAna.ObsAna <> "" Then
                        AnalisesReq(AnaIndex).DescrSimples = "(Ver Obs.) " & AnalisesReq(AnaIndex).DescrSimples
                    End If
                    
                    iRes = iRes + 1
                    CriaNovaClasse resultados, "RES", iRes
                End If
                RsSelRes.Close
                Set RsSelRes = Nothing
                AnalisesReq(AnaIndex).AnaEstado = BL_HandleNull(RegMarcadas!flg_estado, -1)
            End If
            
            If Trim(EcCodEstadoReq.Text) <> gEstadoReqEsperaProduto Then
                'Verificar se an�lise j� tem produto
                AnalisesReq(AnaIndex).TemProduto = False
                ProdutoAna = ""
                CmdProdAna.Parameters(0).value = AnalisesReq(AnaIndex).CodSimples
                Set RsProdAna = CmdProdAna.Execute
                
                If Not RsProdAna.EOF Then
                    If Trim(BL_HandleNull(RsProdAna!cod_produto, "")) <> "" Then
                        ProdutoAna = BL_TrataStringParaBD(Trim(RsProdAna!cod_produto))
                    End If
                End If
                
                RsProdAna.Close
                Set RsProdAna = Nothing
                
                Set RsDataProd = New ADODB.recordset
                
                If Trim(ProdutoAna) <> "" Then
                    'Se a analise tem produto verifica se j� chegou
                    RsDataProd.Source = "SELECT COUNT(*) AS Conta FROM sl_req_prod WHERE " & _
                        "n_req = " & Trim(EcNumReq.Text) & " AND " & _
                        " cod_prod = " & ProdutoAna & " AND " & _
                        "dt_chega IS NOT NULL"
                Else
                    'Se n�o verifica a chegada de qualquer um
                    RsDataProd.Source = _
                        "SELECT COUNT(*) AS Conta FROM sl_req_prod WHERE " & _
                        "n_req = " & Trim(EcNumReq.Text) & " AND " & _
                        "dt_chega IS NOT NULL"
                End If
                
                With RsDataProd
                    .CursorLocation = adUseServer
                    .CursorType = adOpenStatic
                    .ActiveConnection = gConexao
                    .Open
                End With

                If RsDataProd.RecordCount > 0 Then
                    If BL_HandleNull(RsDataProd!conta, 0) <> 0 Then
                        AnalisesReq(AnaIndex).TemProduto = True
                    Else
                        AnalisesReq(AnaIndex).TemProduto = False
                    End If
                Else
                    AnalisesReq(AnaIndex).TemProduto = False
                End If
                
                RsDataProd.Close
                Set RsDataProd = Nothing
            End If
            
            AnaIndex = AnaIndex + 1
            ReDim Preserve AnalisesReq(AnaIndex)
TrataGhost:
            RegMarcadas.MoveNext
            
        Next i
    Else
        AnaIndex = -1
    End If
    
    'Fecha o RecordSet para poder mudar a propriedade Source mais tarde
    If RegMarcadas.state <> adStateClosed Then
        RegMarcadas.Close
    End If

    Set CmdProdAna = Nothing
    
End Sub



Private Sub BtBaixo_Click()
    BtBaixo.Visible = False
    BtCima.Visible = True
    FrDadosAdicionais.Visible = True
End Sub

Private Sub BtCima_Click()
    BtBaixo.Visible = True
    BtCima.Visible = False
    FrDadosAdicionais.Visible = False
End Sub


Private Sub BTEscondeRes_Click()
    BtMostraRes.Visible = True
    BTEscondeRes.Visible = False
    EcInfo.Visible = False
    EcInfoVisivel = False
    FGAna.ScrollBars = flexScrollBarVertical
    FGAna.SetFocus
End Sub

Private Sub BtImpRes_Click()
    gImprimirDestino = 0
    ImpRes
End Sub

Private Sub BtMostraRes_Click()
    BtMostraRes.Visible = False
    BTEscondeRes.Visible = True
'    EcInfo.Visible = True
'    EcInfoVisivel = True
'    FGAna.ScrollBars = flexScrollBarNone
'    FGAna.SetFocus
    
    If FGAna.RowSel >= 1 And FGAna.TextMatrix(FGAna.row, FGAna.Col) <> "" Then
    'If Resultados(FGAna.Row - 1).DadosAna.TRes = gT_Microrganismo Then
'     Abre o form FormInfoRes
    FormInfoRes.Show
    FormInfoRes.CarregaInfoConsulta BtMostraRes.left + 800, _
                            BtMostraRes.top + 420, 7500, EcInfo.Text
'    Else
'        EcInfo.Visible = True
'        EcInfoVisivel = True
'        FGAna.ScrollBars = flexScrollBarNone
'        FGAna.SetFocus
'    End If
    Else
        BtMostraRes.Visible = False
        BTEscondeRes.Visible = False
    End If
                            
                            
'    FormInfoRes.CarregaInfoRes BtMostraRes.Left + 620, _
'                            BtMostraRes.Top + 420, _
'                            Resultados(FGRes.Row).DadosAna.ResMicro(1).CodMicro, _
'                            Resultados(FGRes.Row).DadosAna.ResMicro(1).CodGrAntib, _
'                            Resultados(FGRes.Row).DadosAna.SeqRealiza, _
'                            IIf(Resultados(FGRes.Row).DadosAna.TRes = gT_Microrganismo And FGRes.Col = ColRes1, _
'                            Trim(Resultados(FGRes.Row).DadosAna.ResMicro(1).DescrMicro), Replace(EcInfo.Text, vbCrLf, "")), _
'                            Resultados(FGRes.Row).DadosAna.ResMicro(1).FlgImp, _
'                            Resultados(FGRes.Row).DadosAna.ResMicro(1).FlgTSQ, FGRes.Col

End Sub

Private Sub CbGrupo_Click()
    
    BL_ColocaComboTexto "sl_gr_ana", "seq_gr_ana", "cod_gr_ana", EcCodGrupo, CbGrupo
    
End Sub


Private Sub CbGrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbGrupo.ListIndex = -1
    
End Sub


Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then CbSituacao.ListIndex = -1
End Sub


Private Sub CbUrgencia_Click()
    Select Case CbUrgencia.ListIndex
        Case 0
            EcUrgencia.Text = "N"
        Case 1
            EcUrgencia.Text = "U"
        Case Else
            EcUrgencia.Text = ""
    End Select
End Sub


Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1
End Sub


Private Sub cmdOK_Click()
    FuncaoProcurar
End Sub

Private Sub EcCodProv_Change()
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodProv.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_proven FROM sl_proven WHERE cod_proven='" & Trim(EcCodProv.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "N�o existe proveni�ncia com esse c�digo !", vbInformation, App.ProductName
            EcCodProv.Text = ""
            EcDescrProveniencia.Text = ""
        Else
            EcDescrProveniencia.Text = "" & Tabela!descr_proven
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If
End Sub

Private Sub EcCodUteSeq_Change()
    Dim rsUte As ADODB.recordset
    
    If Trim(EcCodUteSeq.Text) <> "" Then
    
        CmdUtente.Parameters(0).value = Trim(EcCodUteSeq.Text)
        Set rsUte = CmdUtente.Execute

        If Not rsUte.EOF Then
            EcUtente.Text = Trim(BL_HandleNull(rsUte!Utente, ""))
            EcTipoUtente.Text = Trim(BL_HandleNull(rsUte!t_utente, ""))
            EcProcHosp1.Text = Trim(BL_HandleNull(rsUte!n_proc_1, ""))
            EcProcHosp2.Text = Trim(BL_HandleNull(rsUte!n_proc_2, ""))
            EcNome.Text = Trim(BL_HandleNull(rsUte!nome_ute, ""))
            EcDataNasc.Text = Trim(BL_HandleNull(rsUte!dt_nasc_ute, ""))
            EcDataInscr.Text = Trim(BL_HandleNull(rsUte!dt_inscr, ""))
        End If
        
        rsUte.Close
        Set rsUte = Nothing
    
    End If

End Sub

Private Sub EcCodUtilImp_Change()
    EcUtilImpressao.Text = BL_SelNomeUtil(Trim(EcCodUtilImp.Text))
End Sub

Private Sub EcCodUtilImp2_Change()
    EcUtil2Via.Text = BL_SelNomeUtil(Trim(EcCodUtilImp2.Text))
End Sub


Private Sub EcComFinal_DblClick()
    If EcComFinal.top = FGAna.top Then
        EcComFinal.ToolTipText = "COMENT�RIO FINAL: Duplo click para aumentar."
        EcComFinal.top = 6000
        EcComFinal.Height = 495
    Else
        EcComFinal.ToolTipText = "COMENT�RIO FINAL: Duplo click para diminuir."
        EcComFinal.top = FGAna.top
        EcComFinal.Height = 4100
    End If
End Sub


Private Sub EcDataChega_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub


Private Sub EcDataChega_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDataPrevista_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub




Private Sub EcDataPrevista_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub


Private Sub EcEpisodio_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub





Private Sub Ecepisodio_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub







Private Sub EcNumReq_GotFocus()
    Set CampoActivo = Me.ActiveControl
    cmdOK.Default = True
End Sub

Private Sub EcNumReq_LostFocus()
    cmdOK.Default = False
End Sub

Private Sub EcNumReq_Validate(Cancel As Boolean)
    
    If Len(EcNumReq) > 7 Then
        EcNumReq = Right(EcNumReq, 7)
    End If
End Sub
Private Sub EcReqAux_GotFocus()
    Set CampoActivo = Me.ActiveControl
    cmdOK.Default = True
End Sub

Private Sub EcReqAux_LostFocus()
    cmdOK.Default = False
End Sub

Private Sub EcObsAna_DblClick()
    If EcObsAna.top = FGAna.top Then
        EcObsAna.ToolTipText = "OBSERVA��ES DA AN�LISE: Duplo click para aumentar."
        EcObsAna.top = 5520
        EcObsAna.Height = 495
    Else
        EcObsAna.ToolTipText = "OBSERVA��ES DA AN�LISE: Duplo click para diminuir."
        EcObsAna.top = FGAna.top
        EcObsAna.Height = 3620
    End If
End Sub


Private Sub EcReqAssociada_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcReqAssociada)
    
End Sub


Private Sub EcUrgencia_Change()
    Select Case Trim(EcUrgencia.Text)
        Case "N"
            CbUrgencia.ListIndex = 0
        Case "U"
            CbUrgencia.ListIndex = 1
        Case Else
            CbUrgencia.ListIndex = mediComboValorNull
    End Select
End Sub


Private Sub FGAna_EnterCell()
    Dim i As Long
    Dim EncontrouAnalise As Boolean
    
    On Error Resume Next
    
    If FGAna.row = 0 Or APreencherFgAna = True Then Exit Sub
    
    If Trim(FGAna.TextMatrix(FGAna.row, 0)) <> "" And Trim(FGAna.TextMatrix(FGAna.row, 3)) = "" Then
        'Perfil ou Complexa
        FGAna.CellBackColor = &H404040
    Else
        EncontrouAnalise = False
        For i = 0 To UBound(AnalisesReq) - 1
            If AnalisesReq(i).GridIndice = FGAna.row Then
                EncontrouAnalise = True
                Exit For
            End If
        Next i
        If EncontrouAnalise = True Then
            MudaCorFlexGrid FGAna.row, AnalisesReq(i).AnaEstado, AnalisesReq(i).TemProduto, True
        End If
        
        EncontrouAnalise = False
        For i = 1 To resultados.Count - 1
            If resultados(i).DadosAna.estado = FGAna.row Then
                EncontrouAnalise = True
                Exit For
            End If
        Next i
        If EncontrouAnalise = True Then
            EcObsAna.TextRTF = resultados(i).DadosAna.ObsAna
        End If
                
        'Resultados
        If FGAna.TextMatrix(FGAna.row, FGAna.Col) <> "" Then
            
            If FGAna.Col = 1 Then
                If resultados.item(i).DadosAna.res = "Click para ver resultado" Or _
                   InStr(1, resultados.item(i).DadosAna.res, "\\") <> 0 Or resultados.item(i).DadosAna.TRes = gT_Frase Or resultados.item(i).DadosAna.TRes = gT_Microrganismo Then
                        BtMostraRes.left = FGAna.CellLeft + FGAna.CellWidth - BtMostraRes.Width + 120
                        BtMostraRes.top = FGAna.CellTop + FGAna.top + 10
                        BTEscondeRes.left = FGAna.CellLeft + FGAna.CellWidth - BTEscondeRes.Width + 120
                        BTEscondeRes.top = FGAna.CellTop + FGAna.top + 10
                        EcInfo.top = FGAna.top + 280
                        EcInfo.Height = FGAna.Height - 280
                        EcInfo.left = BTEscondeRes.left + BTEscondeRes.Width
                        EcInfo.Width = FGAna.Width - BTEscondeRes.left - 280
                        If EcInfoVisivel = True Then
                            BTEscondeRes.Visible = True
                            BtMostraRes.Visible = False
                            EcInfo.Visible = True
                            FGAna.ScrollBars = flexScrollBarNone
                        Else
                            BTEscondeRes.Visible = False
                            BtMostraRes.Visible = True
                            EcInfo.Visible = False
                            FGAna.ScrollBars = flexScrollBarVertical
                        End If
                End If
            Else
                If resultados.item(i).DadosAna.Dados2Res.res = "Click para ver resultado" Or _
                    InStr(1, resultados.item(i).DadosAna.Dados2Res.res, "\\") <> 0 Or resultados.item(i).DadosAna.Dados2Res.TRes = gT_Frase Or resultados.item(i).DadosAna.Dados2Res.TRes = gT_Microrganismo Then
                        BtMostraRes.left = FGAna.CellLeft + FGAna.CellWidth - BtMostraRes.Width + 120
                        BtMostraRes.top = FGAna.CellTop + FGAna.top + 10
                        BTEscondeRes.left = FGAna.CellLeft + FGAna.CellWidth - BTEscondeRes.Width + 120
                        BTEscondeRes.top = FGAna.CellTop + FGAna.top + 10
                        EcInfo.top = FGAna.top + 280
                        EcInfo.Height = FGAna.Height - 280
                        EcInfo.left = BTEscondeRes.left + BTEscondeRes.Width
                        EcInfo.Width = FGAna.Width - BTEscondeRes.left - 280
                        If EcInfoVisivel = True Then
                            BTEscondeRes.Visible = True
                            BtMostraRes.Visible = False
                            EcInfo.Visible = True
                            FGAna.ScrollBars = flexScrollBarNone
                        Else
                            BTEscondeRes.Visible = False
                            BtMostraRes.Visible = True
                            EcInfo.Visible = False
                            FGAna.ScrollBars = flexScrollBarVertical
                        End If
                End If
            End If
            
            TrataLinhaRes
            If EncontrouAnalise = True And EcInfo.Text = "" Then
                If resultados(i).DadosAna.DescrFrAlf <> "" Then
                    EcInfo.Text = resultados(i).DadosAna.DescrFrAlf
                End If
                If resultados(i).DadosAna.Dados2Res.DescrFrAlf <> "" Then
                    EcInfo.Text = resultados(i).DadosAna.Dados2Res.DescrFrAlf
                End If
            End If

        End If
    End If
End Sub

Private Sub FGAna_LeaveCell()
    Dim i As Long
    Dim EncontrouAnalise As Boolean
    
    On Error Resume Next
    
    If APreencherFgAna = False Then
        BtMostraRes.Visible = False
        BTEscondeRes.Visible = False
        EcInfo.Visible = False
        FGAna.ScrollBars = flexScrollBarVertical
    End If
    If FGAna.row = 0 Or APreencherFgAna = True Then Exit Sub

    If Trim(FGAna.TextMatrix(FGAna.row, 0)) <> "" And Trim(FGAna.TextMatrix(FGAna.row, 3)) = "" Then
        'Perfil ou Complexa
        FGAna.CellBackColor = &HE0E0E0
    Else
        EncontrouAnalise = False
        For i = 0 To UBound(AnalisesReq) - 1
            If AnalisesReq(i).GridIndice = FGAna.row Then
                EncontrouAnalise = True
                Exit For
            End If
        Next i
        If EncontrouAnalise = True Then
            MudaCorFlexGrid FGAna.row, AnalisesReq(i).AnaEstado, AnalisesReq(i).TemProduto
        End If
    End If
    
    EcObsAna.Text = ""
    EcInfo.Text = ""
End Sub

Private Sub FGAna_LostFocus()
    Dim i As Long
    Dim EncontrouAnalise As Boolean
    
    On Error Resume Next
    
    If FGAna.row = 0 Or APreencherFgAna = True Then Exit Sub

    If Trim(FGAna.TextMatrix(FGAna.row, 0)) <> "" And Trim(FGAna.TextMatrix(FGAna.row, 3)) = "" Then
        'Perfil ou Complexa
        FGAna.CellBackColor = &HE0E0E0
    Else
        EncontrouAnalise = False
        For i = 0 To UBound(AnalisesReq) - 1
            If AnalisesReq(i).GridIndice = FGAna.row Then
                EncontrouAnalise = True
                Exit For
            End If
        Next i
        If EncontrouAnalise = True Then
            MudaCorFlexGrid FGAna.row, AnalisesReq(i).AnaEstado, AnalisesReq(i).TemProduto
        End If
    End If
    
End Sub


Private Sub Form_Activate()

    EventoActivate
    
End Sub

Private Sub Form_Load()

    EventoLoad
    
End Sub


Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Private Sub LimpaColeccao(Coleccao As Collection, Index As Long)
' Index = -1 : Apaga todos elementos
    Dim i As Long
    Dim Tot As Long

    Tot = Coleccao.Count - 1
    If Index = -1 Then
        i = Tot
        While i > 0
            Coleccao.Remove i
            i = i - 1
        Wend
    Else
        Coleccao.Remove Index
    End If

End Sub

Sub SeleccionaResultado(indice As Long, seqRealiza As Double, CodMicro As String, OrdemFrase As Integer)
'Rotina que dependendo do tipo de resultado cria e preenche as classes
    Dim SqlRes As String
    Dim res As Integer
    Dim i As Long
    Dim k As Long
    Dim l As Long
    Dim j As Long
    Dim RsResAlf As ADODB.recordset
    Dim RsResFr As ADODB.recordset
    Dim RsResFrDescr As ADODB.recordset
    Dim RsResMicro As ADODB.recordset
    Dim RsResMicroDescr As ADODB.recordset
    Dim RsResMiTSQ As ADODB.recordset
    Dim RsResAntibiotico As ADODB.recordset
    Dim RsResAntib As ADODB.recordset
    Dim RsObsAna As ADODB.recordset
    Dim Str1 As String
    Dim Str2 As String
    Dim RsFr As ADODB.recordset
    Dim sql As String
    Dim sSql As String
    
    resultados.item(indice).DadosAna.seqRealiza = seqRealiza
    
    'Alfanum�ricos / num�ricos / auxiliares
    sSql = "SELECT * FROM sl_res_alfan WHERE seq_realiza=" & seqRealiza & " order by n_res"
    CmdResAlf.Parameters(0).value = seqRealiza
    'Set RsResAlf = CmdResAlf.Execute
    Set RsResAlf = New ADODB.recordset
    RsResAlf.CursorLocation = adUseServer
    RsResAlf.CursorType = adOpenStatic
    RsResAlf.Open sSql, gConexao
    
    While Not RsResAlf.EOF
        Select Case RsResAlf!N_Res
            Case 1
                resultados.item(indice).DadosAna.res = "" & RsResAlf!result
        
                If resultados.item(indice).DadosAna.res <> "" Then
                    Str1 = Mid(resultados.item(indice).DadosAna.res, 1, 2)
                    If Len(Trim(resultados.item(indice).DadosAna.res)) > 2 Then
                        Str2 = Mid(resultados.item(indice).DadosAna.res, 3, Len(Trim(resultados.item(indice).DadosAna.res)) - 2)
                    End If
                End If
                If Str1 = "\\" Then
                    'C�digo de uma frase
                    sql = "SELECT cod_frase,descr_frase FROM sl_dicionario WHERE cod_frase=" & BL_TrataStringParaBD(UCase(Str2))
                    Set RsFr = New ADODB.recordset
                    RsFr.CursorLocation = adUseServer
                    RsFr.CursorType = adOpenStatic
                    RsFr.Open sql, gConexao
                    If RsFr.RecordCount > 0 Then
                        resultados(indice).DadosAna.DescrFrAlf = "" & RsFr!descr_frase
                    Else
                        resultados(indice).DadosAna.DescrFrAlf = ""
                    End If
                    RsFr.Close
                    Set RsFr = Nothing
                Else
                    resultados(indice).DadosAna.DescrFrAlf = ""
                End If
                
                res = 1
            Case 2
                resultados.item(indice).DadosAna.Dados2Res.res = RsResAlf!result
                
                If resultados.item(indice).DadosAna.Dados2Res.res <> "" Then
                    Str1 = Mid(resultados.item(indice).DadosAna.Dados2Res.res, 1, 2)
                    If Len(Trim(resultados.item(indice).DadosAna.Dados2Res.res)) > 2 Then
                        Str2 = Mid(resultados.item(indice).DadosAna.Dados2Res.res, 3, Len(Trim(resultados.item(indice).DadosAna.Dados2Res.res)) - 2)
                    End If
                End If
                If Str1 = "\\" Then
                    'C�digo de uma frase
                    sql = "SELECT cod_frase,descr_frase FROM sl_dicionario WHERE cod_frase=" & BL_TrataStringParaBD(UCase(Str2))
                    Set RsFr = New ADODB.recordset
                    RsFr.CursorLocation = adUseServer
                    RsFr.CursorType = adOpenStatic
                    RsFr.Open sql, gConexao
                    If RsFr.RecordCount > 0 Then
                        resultados(indice).DadosAna.Dados2Res.DescrFrAlf = "" & RsFr!descr_frase
                    Else
                        resultados(indice).DadosAna.Dados2Res.DescrFrAlf = ""
                    End If
                    RsFr.Close
                    Set RsFr = Nothing
                Else
                    resultados(indice).DadosAna.Dados2Res.DescrFrAlf = ""
                End If
                
                res = 2
        End Select
        RsResAlf.MoveNext
    Wend
        
    'Frases
'    If indice > 1 Then
'        If Resultados(indice).DadosAna.CodAnaS = Resultados(indice - 1).DadosAna.CodAnaS Then
'            IndiceFrase = IndiceFrase + 1
'        Else
'            IndiceFrase = 0
'        End If
'    Else
'        IndiceFrase = 0
'    End If
    resultados.item(indice).DadosAna.FrasesIndex = -1
    resultados.item(indice).DadosAna.Dados2Res.FrasesIndex = -1
    CmdResFr.Parameters(0).value = seqRealiza
    CmdResFr.Parameters(1).value = OrdemFrase
    Set RsResFr = CmdResFr.Execute
    If Not RsResFr.EOF Then
        i = 1
        k = 1
        While Not RsResFr.EOF

            'Selecciona a descri��o
            CmdResFrDescr.Parameters(0).value = RsResFr!cod_frase
            Set RsResFrDescr = CmdResFrDescr.Execute
            If Not RsResFrDescr.EOF Then
                Select Case RsResFr!N_Res
                    Case 1
                        If resultados.item(indice).DadosAna.FrasesIndex = -1 Then
                            CriaNovaClasse resultados(indice).DadosAna.ResFrases, "FRA", 1, True
                            resultados.item(indice).DadosAna.res = RTrim(RsResFrDescr!descr_frase)
'                            Resultados.Item(indice).DadosAna.Res = "Click para ver resultado"
                            resultados.item(indice).DadosAna.FrasesIndex = 0
                        End If
                        
                        resultados.item(indice).DadosAna.ResFrases(i).CodFr = RTrim(RsResFr!cod_frase)
                        resultados.item(indice).DadosAna.ResFrases(i).DescrFr = RTrim(RsResFrDescr!descr_frase)
                        resultados.item(indice).DadosAna.ResFrases(i).ordem = RsResFr!ord_frase
                        
                        CriaNovaClasse resultados(indice).DadosAna.ResFrases, "FRA", i + 1, False
                        resultados.item(indice).DadosAna.FrasesIndex = resultados.item(indice).DadosAna.FrasesIndex + 1
                        i = i + 1
                    Case 2
                        If resultados.item(indice).DadosAna.Dados2Res.FrasesIndex = -1 Then
                            CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResFrases, "FRA", 1, True
                            resultados.item(indice).DadosAna.Dados2Res.res = RTrim(RsResFrDescr!descr_frase)
'                            Resultados.Item(indice).DadosAna.Dados2Res.Res = "Click para ver resultado"
                            resultados.item(indice).DadosAna.Dados2Res.FrasesIndex = 0
                        End If
                        
                        resultados.item(indice).DadosAna.Dados2Res.ResFrases(k).CodFr = RTrim(RsResFr!cod_frase)
                        resultados.item(indice).DadosAna.Dados2Res.ResFrases(k).DescrFr = RTrim(RsResFrDescr!descr_frase)
                        resultados.item(indice).DadosAna.Dados2Res.ResFrases(k).ordem = RsResFr!ord_frase
                        
                        CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResFrases, "FRA", k + 1, False
                        resultados.item(indice).DadosAna.Dados2Res.FrasesIndex = resultados.item(indice).DadosAna.Dados2Res.FrasesIndex + 1
                        k = k + 1
                    End Select
            End If

            RsResFr.MoveNext
           
        Wend
    End If
    
    'Microrganismos
    CmdResMicro.Parameters(0).value = seqRealiza
    CmdResMicro.Parameters(1).value = CodMicro
    Set RsResMicro = CmdResMicro.Execute
    resultados.item(indice).DadosAna.MicroIndex = -1
    resultados.item(indice).DadosAna.Dados2Res.MicroIndex = -1
    If Not RsResMicro.EOF Then
        i = 1
        k = 1
        While Not RsResMicro.EOF
            'Selecciona a descri��o
            CmdResMicroDescr.Parameters(0).value = RsResMicro!cod_micro
            Set RsResMicroDescr = CmdResMicroDescr.Execute
            j = 1
            l = 1
            If Not RsResMicroDescr.EOF Then
                'Preencher classes com os microrganismos
                Select Case RsResMicro!N_Res
                    Case 1
                        If resultados.item(indice).DadosAna.MicroIndex = -1 Then
                            'Resultados.Item(indice).DadosAna.Res = "Click para ver resultado"
                            resultados.item(indice).DadosAna.res = RTrim(RsResMicroDescr!descr_microrg)
                            CriaNovaClasse resultados(indice).DadosAna.ResMicro, "MIC", 1, True
                            resultados.item(indice).DadosAna.MicroIndex = 0
                        End If

                        resultados.item(indice).DadosAna.ResMicro(i).CodMicro = RTrim(RsResMicro!cod_micro)
                        resultados.item(indice).DadosAna.ResMicro(i).DescrMicro = RTrim(RsResMicroDescr!descr_microrg)
                        resultados.item(indice).DadosAna.ResMicro(i).Quantif = BL_HandleNull(RTrim(RsResMicro!Quantif), "")
                        resultados.item(indice).DadosAna.ResMicro(i).FlgImp = IIf(RTrim(RsResMicro!flg_imp) = "S", "Sim", "N�o")
                        resultados.item(indice).DadosAna.ResMicro(i).FlgTSQ = IIf(RTrim(RsResMicro!flg_tsq) = "S", "Sim", "N�o")
                        
                        CriaNovaClasse resultados(indice).DadosAna.ResMicro, "MIC", i + 1, False
                        resultados.item(indice).DadosAna.MicroIndex = resultados.item(indice).DadosAna.MicroIndex + 1
                    Case 2
                        If resultados.item(indice).DadosAna.Dados2Res.MicroIndex = -1 Then
                            'Resultados.Item(indice).DadosAna.Dados2Res.Res = "Click para ver resultado"
                            resultados.item(indice).DadosAna.Dados2Res.res = RTrim(RsResMicroDescr!descr_microrg)
                            CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResMicro, "MIC", 1, True
                            resultados.item(indice).DadosAna.Dados2Res.MicroIndex = 0
                            res = 2
                        End If
                       
                        resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).CodMicro = RTrim(RsResMicro!cod_micro)
                        resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).DescrMicro = RTrim(RsResMicroDescr!descr_microrg)
                        resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).Quantif = BL_HandleNull(RTrim(RsResMicro!Quantif), "")
                        resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).FlgImp = IIf(RTrim(RsResMicro!flg_imp) = "S", "Sim", "N�o")
                        resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).FlgTSQ = IIf(RTrim(RsResMicro!flg_tsq) = "S", "Sim", "N�o")
                        
                        CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResMicro, "MIC", k + 1, False
                        resultados.item(indice).DadosAna.Dados2Res.MicroIndex = resultados.item(indice).DadosAna.Dados2Res.MicroIndex + 1
                End Select
                
                Dim A As Integer
                Dim b As String
                Dim c As String
                Dim val As Variant
            
'                A = InStr(1, BL_HandleNull(RsResMicro!cod_gr_antibio, ""), ";")
'                If (A > 0) Then
'                    b = Mid(RsResMicro!cod_gr_antibio, 1, A - 1)
'                    c = Mid(RsResMicro!cod_gr_antibio, A + 1)
'                Else
'                    b = BL_HandleNull(RsResMicro!cod_gr_antibio, "")
'                End If
                
                c = BL_HandleNull(RsResMicro!Cod_Gr_Antibio, "")
                If c <> "" Then
                For Each val In Split(c, ";")
                    b = b & "'" & val & "'" & ","
                Next
                b = "(" & Mid(b, 1, InStrRev(b, ",") - 1) & ")"
                Else
                    b = ""
                End If
'
'                'Se existirem TSQ criar as respectivas instancias
'                CmdResMiTSQ.Parameters(0).Value = SeqRealiza
'                CmdResMiTSQ.Parameters(1).Value = RsResMicro!N_Res
'                CmdResMiTSQ.Parameters(2).Value = RsResMicro!cod_micro
'                CmdResMiTSQ.Parameters(3).Value = b
                
                Set RsResMiTSQ = New ADODB.recordset
                RsResMiTSQ.CursorLocation = adUseServer
                RsResMiTSQ.CursorType = adOpenStatic
                If b = "" Then
                RsResMiTSQ.Source = "SELECT n_res,cod_antib,res_sensib,cmi,flg_imp " & _
                                    " FROM sl_res_tsq WHERE seq_realiza= " & seqRealiza & _
                                    " AND n_res = " & RsResMicro!N_Res & _
                                    " AND cod_micro= " & BL_TrataStringParaBD(RsResMicro!cod_micro) & _
                                    " order by n_res "
                Else
                RsResMiTSQ.Source = " SELECT n_res,cod_antib,res_sensib,cmi,flg_imp " & _
                                    " FROM sl_res_tsq, sl_rel_grantib " & _
                                    " WHERE seq_realiza=" & seqRealiza & _
                                    " AND n_res = " & RsResMicro!N_Res & _
                                    " AND cod_micro= " & BL_TrataStringParaBD(RsResMicro!cod_micro) & _
                                    " AND cod_antib = cod_antibio AND cod_gr_antibio in " & b & " and flg_imp = 'S' order by n_res,ordem "
                End If
                RsResMiTSQ.Open , gConexao


                'Set RsResMiTSQ = CmdResMiTSQ.Execute
                If resultados(indice).DadosAna.MicroIndex >= 0 Then
                    resultados(indice).DadosAna.ResMicro(i).TSQIndex = -1
                End If
                If resultados(indice).DadosAna.Dados2Res.MicroIndex >= 0 Then
                    resultados(indice).DadosAna.Dados2Res.ResMicro(k).TSQIndex = -1
                End If

                While Not RsResMiTSQ.EOF
                    'seleccionar a descri��o e grupo do antibiotico
                    CmdResAntibiotico.Parameters(0).value = RsResMiTSQ!cod_antib
                    Set RsResAntibiotico = CmdResAntibiotico.Execute
                    Select Case RsResMiTSQ!N_Res
                        Case 1
                            If resultados(indice).DadosAna.ResMicro(i).TSQIndex = -1 Then
                                CriaNovaClasse resultados(indice).DadosAna.ResMicro(i).ResTSQ, "TSQ", 1, True
                                resultados(indice).DadosAna.ResMicro(i).TSQIndex = 0
                            End If

                            resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).codAntib = RsResMiTSQ!cod_antib
                            resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).DescrAntib = RTrim(RsResAntibiotico!descr_antibio)
                            resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).CodGrAntib = BL_HandleNull(RsResAntibiotico!Cod_Gr_Antibio, "")
                            resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).Sensib = BL_HandleNull(RsResMiTSQ!res_sensib, "")
                            resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).CMI = BL_HandleNull(RsResMiTSQ!CMI, "")
                            resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).FlgImp = IIf(RTrim(RsResMiTSQ!flg_imp) = "S", "Sim", "N�o")
                            
                            CriaNovaClasse resultados(indice).DadosAna.ResMicro(i).ResTSQ, "TSQ", j + 1, False
                            resultados(indice).DadosAna.ResMicro(i).TSQIndex = resultados(indice).DadosAna.ResMicro(i).TSQIndex + 1
                        Case 2
                            If resultados(indice).DadosAna.Dados2Res.ResMicro(k).TSQIndex = -1 Then
                                CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ, "TSQ", 1, True
                                resultados(indice).DadosAna.Dados2Res.ResMicro(k).TSQIndex = 0
                            End If
                            
                            resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(l).codAntib = RsResMiTSQ!cod_antib
                            resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(l).DescrAntib = RTrim(RsResAntibiotico!descr_antibio)
                            resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(l).CodGrAntib = BL_HandleNull(RsResAntibiotico!Cod_Gr_Antibio, "")
                            resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(l).Sensib = BL_HandleNull(RsResMiTSQ!res_sensib, "")
                            resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(l).CMI = BL_HandleNull(RsResMiTSQ!CMI, "")
                            resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(l).FlgImp = IIf(RTrim(RsResMiTSQ!flg_imp) = "S", "Sim", "N�o")
                            
                            CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ, "TSQ", l + 1, False
                            resultados(indice).DadosAna.Dados2Res.ResMicro(k).TSQIndex = resultados(indice).DadosAna.Dados2Res.ResMicro(k).TSQIndex + 1
                    End Select
                    
                    Select Case RsResMiTSQ!N_Res
                        Case 1
                            j = j + 1
                        Case 2
                            l = l + 1
                    End Select
                    
                    RsResMiTSQ.MoveNext
                Wend
                RsResMiTSQ.Close
                Set RsResMiTSQ = Nothing
                
            End If

            Select Case RsResMicro!N_Res
                Case 1
                    i = i + 1
                Case 2
                    k = k + 1
            End Select
            RsResMicro.MoveNext
        Wend
    End If
    RsResMicro.Close
    Set RsResMicro = Nothing
   
    'Antibiogramas
    CmdResAntib.Parameters(0) = seqRealiza
    CmdResAntib.Parameters(1) = CodMicro
    Set RsResAntib = CmdResAntib.Execute
    If Not RsResAntib.EOF Then
        i = 1
        k = 1
        resultados.item(indice).DadosAna.AntibIndex = -1
        resultados.item(indice).DadosAna.Dados2Res.AntibIndex = -1
        While Not RsResAntib.EOF
            'Selecciona a descri��o do antibiotico
            CmdResAntibiotico.Parameters(0).value = RsResAntib!cod_antib
            Set RsResAntibiotico = CmdResAntibiotico.Execute
            If Not RsResAntibiotico.EOF Then
                Select Case RsResAntib!N_Res
                    Case 1
                        If resultados.item(indice).DadosAna.AntibIndex = -1 Then
                            'Resultados.Item(indice).DadosAna.Res = "Click para ver resultado"
                            CriaNovaClasse resultados(indice).DadosAna.ResAntib, "TSQ", 1, True
                            resultados.item(indice).DadosAna.AntibIndex = 0
                        End If
                    
                        resultados.item(indice).DadosAna.ResAntib(i).codAntib = RsResAntib!cod_antib
                        resultados.item(indice).DadosAna.ResAntib(i).DescrAntib = RTrim(RsResAntibiotico!descr_antibio)
                        resultados.item(indice).DadosAna.ResAntib(i).Sensib = BL_HandleNull(RsResAntib!res_sensib, "")
                        resultados.item(indice).DadosAna.ResAntib(i).CMI = BL_HandleNull(RsResAntib!CMI, "")
                        resultados.item(indice).DadosAna.ResAntib(i).FlgImp = BL_HandleNull(RsResAntib!flg_imp, "N")
                        
                        CriaNovaClasse resultados(indice).DadosAna.ResAntib, "TSQ", i + 1, False
                        resultados.item(indice).DadosAna.AntibIndex = resultados.item(indice).DadosAna.AntibIndex + 1
                        res = 1
                        i = i + 1
                    Case 2
                        If resultados.item(indice).DadosAna.Dados2Res.AntibIndex = -1 Then
                            'Resultados.Item(indice).DadosAna.Dados2Res.Res = "Click para ver resultado"
                            CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResAntib, "TSQ", 1, True
                            resultados.item(indice).DadosAna.Dados2Res.AntibIndex = 0
                        End If
                    
                        resultados.item(indice).DadosAna.Dados2Res.ResAntib(k).codAntib = RsResAntib!cod_antib
                        resultados.item(indice).DadosAna.Dados2Res.ResAntib(k).DescrAntib = RTrim(RsResAntibiotico!descr_antibio)
                        resultados.item(indice).DadosAna.Dados2Res.ResAntib(k).Sensib = BL_HandleNull(RsResAntib!res_sensib, "")
                        resultados.item(indice).DadosAna.Dados2Res.ResAntib(k).CMI = BL_HandleNull(RsResAntib!CMI, "")
                        resultados.item(indice).DadosAna.Dados2Res.ResAntib(k).FlgImp = BL_HandleNull(RsResAntib!flg_imp, "N")
                        
                        CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResAntib, "TSQ", k + 1, False
                        resultados.item(indice).DadosAna.Dados2Res.AntibIndex = resultados.item(indice).DadosAna.Dados2Res.AntibIndex + 1
                        res = 2
                        k = k + 1
                End Select
            End If
            RsResAntib.MoveNext
        Wend
    End If
    
    'Observa��es da an�lise
    CmdObsAna.Parameters(0).value = seqRealiza
    resultados(indice).DadosAna.ObsAna = ""
    Set RsObsAna = CmdObsAna.Execute
    If Not RsObsAna.EOF Then
        resultados(indice).DadosAna.SeqObsAna = "" & RsObsAna!seq_obs_ana
        resultados(indice).DadosAna.ObsAna = "" & RsObsAna!descr_obs_ana
    End If
    RsObsAna.Close
    Set RsObsAna = Nothing
        
    If Not RsResAlf Is Nothing Then
        RsResAlf.Close
        Set RsResAlf = Nothing
    End If
    If Not RsResFr Is Nothing Then
        RsResFr.Close
        Set RsResFr = Nothing
    End If
    If Not RsResFrDescr Is Nothing Then
        RsResFrDescr.Close
        Set RsResFrDescr = Nothing
    End If
    If Not RsResMicro Is Nothing Then
        RsResMicro.Close
        Set RsResMicro = Nothing
    End If
    If Not RsResMicroDescr Is Nothing Then
        RsResMicroDescr.Close
        Set RsResMicroDescr = Nothing
    End If
    If Not RsResMiTSQ Is Nothing Then
        RsResMiTSQ.Close
        Set RsResMiTSQ = Nothing
    End If
    If Not RsResAntibiotico Is Nothing Then
        RsResAntibiotico.Close
        Set RsResAntibiotico = Nothing
    End If
End Sub
Sub SeleccionaResultado_h(indice As Long, seqRealiza As Double, CodMicro As String, OrdemFrase As Integer)
'Rotina que dependendo do tipo de resultado cria e preenche as classes
    Dim SqlRes As String
    Dim res As Integer
    Dim i As Long
    Dim k As Long
    Dim l As Long
    Dim j As Long
    Dim RsResAlf As ADODB.recordset
    Dim RsResFr As ADODB.recordset
    Dim RsResFrDescr As ADODB.recordset
    Dim RsResMicro As ADODB.recordset
    Dim RsResMicroDescr As ADODB.recordset
    Dim RsResMiTSQ As ADODB.recordset
    Dim RsResAntibiotico As ADODB.recordset
    Dim RsResAntib As ADODB.recordset
    Dim RsObsAna As ADODB.recordset
    Dim Str1 As String
    Dim Str2 As String
    Dim RsFr As ADODB.recordset
    Dim sql As String
    Dim sSql As String
    
    resultados.item(indice).DadosAna.seqRealiza = seqRealiza
    
    'Alfanum�ricos / num�ricos / auxiliares
    CmdResAlf_H.Parameters(0).value = seqRealiza
    Set RsResAlf = CmdResAlf_H.Execute
    
    While Not RsResAlf.EOF
        Select Case RsResAlf!N_Res
            Case 1
                resultados.item(indice).DadosAna.res = "" & RsResAlf!result
        
                If resultados.item(indice).DadosAna.res <> "" Then
                    Str1 = Mid(resultados.item(indice).DadosAna.res, 1, 2)
                    If Len(Trim(resultados.item(indice).DadosAna.res)) > 2 Then
                        Str2 = Mid(resultados.item(indice).DadosAna.res, 3, Len(Trim(resultados.item(indice).DadosAna.res)) - 2)
                    End If
                End If
                If Str1 = "\\" Then
                    'C�digo de uma frase
                    sql = "SELECT cod_frase,descr_frase FROM sl_dicionario WHERE cod_frase=" & BL_TrataStringParaBD(UCase(Str2))
                    Set RsFr = New ADODB.recordset
                    RsFr.CursorLocation = adUseServer
                    RsFr.CursorType = adOpenStatic
                    RsFr.Open sql, gConexao
                    If RsFr.RecordCount > 0 Then
                        resultados(indice).DadosAna.DescrFrAlf = "" & RsFr!descr_frase
                    Else
                        resultados(indice).DadosAna.DescrFrAlf = ""
                    End If
                    RsFr.Close
                    Set RsFr = Nothing
                Else
                    resultados(indice).DadosAna.DescrFrAlf = ""
                End If
                
                res = 1
            Case 2
                resultados.item(indice).DadosAna.Dados2Res.res = RsResAlf!result
                
                If resultados.item(indice).DadosAna.Dados2Res.res <> "" Then
                    Str1 = Mid(resultados.item(indice).DadosAna.Dados2Res.res, 1, 2)
                    If Len(Trim(resultados.item(indice).DadosAna.Dados2Res.res)) > 2 Then
                        Str2 = Mid(resultados.item(indice).DadosAna.Dados2Res.res, 3, Len(Trim(resultados.item(indice).DadosAna.Dados2Res.res)) - 2)
                    End If
                End If
                If Str1 = "\\" Then
                    'C�digo de uma frase
                    sql = "SELECT cod_frase,descr_frase FROM sl_dicionario WHERE cod_frase=" & BL_TrataStringParaBD(UCase(Str2))
                    Set RsFr = New ADODB.recordset
                    RsFr.CursorLocation = adUseServer
                    RsFr.CursorType = adOpenStatic
                    RsFr.Open sql, gConexao
                    If RsFr.RecordCount > 0 Then
                        resultados(indice).DadosAna.Dados2Res.DescrFrAlf = "" & RsFr!descr_frase
                    Else
                        resultados(indice).DadosAna.Dados2Res.DescrFrAlf = ""
                    End If
                    RsFr.Close
                    Set RsFr = Nothing
                Else
                    resultados(indice).DadosAna.Dados2Res.DescrFrAlf = ""
                End If
                
                res = 2
        End Select
        RsResAlf.MoveNext
    Wend
        
    resultados.item(indice).DadosAna.FrasesIndex = -1
    resultados.item(indice).DadosAna.Dados2Res.FrasesIndex = -1
    CmdResFr_H.Parameters(0).value = seqRealiza
    CmdResFr_H.Parameters(1).value = OrdemFrase
    Set RsResFr = CmdResFr_H.Execute
    If Not RsResFr.EOF Then
        i = 1
        k = 1
        While Not RsResFr.EOF

            'Selecciona a descri��o
            CmdResFrDescr.Parameters(0).value = RsResFr!cod_frase
            Set RsResFrDescr = CmdResFrDescr.Execute
            If Not RsResFrDescr.EOF Then
                Select Case RsResFr!N_Res
                    Case 1
                        If resultados.item(indice).DadosAna.FrasesIndex = -1 Then
                            CriaNovaClasse resultados(indice).DadosAna.ResFrases, "FRA", 1, True
                            resultados.item(indice).DadosAna.res = RTrim(RsResFrDescr!descr_frase)
'                            Resultados.Item(indice).DadosAna.Res = "Click para ver resultado"
                            resultados.item(indice).DadosAna.FrasesIndex = 0
                        End If
                        
                        resultados.item(indice).DadosAna.ResFrases(i).CodFr = RTrim(RsResFr!cod_frase)
                        resultados.item(indice).DadosAna.ResFrases(i).DescrFr = RTrim(RsResFrDescr!descr_frase)
                        resultados.item(indice).DadosAna.ResFrases(i).ordem = RsResFr!ord_frase
                        
                        CriaNovaClasse resultados(indice).DadosAna.ResFrases, "FRA", i + 1, False
                        resultados.item(indice).DadosAna.FrasesIndex = resultados.item(indice).DadosAna.FrasesIndex + 1
                        i = i + 1
                    Case 2
                        If resultados.item(indice).DadosAna.Dados2Res.FrasesIndex = -1 Then
                            CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResFrases, "FRA", 1, True
                            resultados.item(indice).DadosAna.Dados2Res.res = RTrim(RsResFrDescr!descr_frase)
'                            Resultados.Item(indice).DadosAna.Dados2Res.Res = "Click para ver resultado"
                            resultados.item(indice).DadosAna.Dados2Res.FrasesIndex = 0
                        End If
                        
                        resultados.item(indice).DadosAna.Dados2Res.ResFrases(k).CodFr = RTrim(RsResFr!cod_frase)
                        resultados.item(indice).DadosAna.Dados2Res.ResFrases(k).DescrFr = RTrim(RsResFrDescr!descr_frase)
                        resultados.item(indice).DadosAna.Dados2Res.ResFrases(k).ordem = RsResFr!ord_frase
                        
                        CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResFrases, "FRA", k + 1, False
                        resultados.item(indice).DadosAna.Dados2Res.FrasesIndex = resultados.item(indice).DadosAna.Dados2Res.FrasesIndex + 1
                        k = k + 1
                    End Select
            End If

            RsResFr.MoveNext
           
        Wend
    End If
    
    'Microrganismos
    CmdResMicro_H.Parameters(0).value = seqRealiza
    CmdResMicro_H.Parameters(1).value = CodMicro
    Set RsResMicro = CmdResMicro_H.Execute
    resultados.item(indice).DadosAna.MicroIndex = -1
    resultados.item(indice).DadosAna.Dados2Res.MicroIndex = -1
    If Not RsResMicro.EOF Then
        i = 1
        k = 1
        While Not RsResMicro.EOF
            'Selecciona a descri��o
            CmdResMicroDescr.Parameters(0).value = RsResMicro!cod_micro
            Set RsResMicroDescr = CmdResMicroDescr.Execute
            j = 1
            l = 1
            If Not RsResMicroDescr.EOF Then
                'Preencher classes com os microrganismos
                Select Case RsResMicro!N_Res
                    Case 1
                        If resultados.item(indice).DadosAna.MicroIndex = -1 Then
                            'Resultados.Item(indice).DadosAna.Res = "Click para ver resultado"
                            resultados.item(indice).DadosAna.res = RTrim(RsResMicroDescr!descr_microrg)
                            CriaNovaClasse resultados(indice).DadosAna.ResMicro, "MIC", 1, True
                            resultados.item(indice).DadosAna.MicroIndex = 0
                        End If

                        resultados.item(indice).DadosAna.ResMicro(i).CodMicro = RTrim(RsResMicro!cod_micro)
                        resultados.item(indice).DadosAna.ResMicro(i).DescrMicro = RTrim(RsResMicroDescr!descr_microrg)
                        resultados.item(indice).DadosAna.ResMicro(i).Quantif = BL_HandleNull(RTrim(RsResMicro!Quantif), "")
                        resultados.item(indice).DadosAna.ResMicro(i).FlgImp = IIf(RTrim(RsResMicro!flg_imp) = "S", "Sim", "N�o")
                        resultados.item(indice).DadosAna.ResMicro(i).FlgTSQ = IIf(RTrim(RsResMicro!flg_tsq) = "S", "Sim", "N�o")
                        
                        CriaNovaClasse resultados(indice).DadosAna.ResMicro, "MIC", i + 1, False
                        resultados.item(indice).DadosAna.MicroIndex = resultados.item(indice).DadosAna.MicroIndex + 1
                    Case 2
                        If resultados.item(indice).DadosAna.Dados2Res.MicroIndex = -1 Then
                            'Resultados.Item(indice).DadosAna.Dados2Res.Res = "Click para ver resultado"
                            resultados.item(indice).DadosAna.Dados2Res.res = RTrim(RsResMicroDescr!descr_microrg)
                            CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResMicro, "MIC", 1, True
                            resultados.item(indice).DadosAna.Dados2Res.MicroIndex = 0
                            res = 2
                        End If
                       
                        resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).CodMicro = RTrim(RsResMicro!cod_micro)
                        resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).DescrMicro = RTrim(RsResMicroDescr!descr_microrg)
                        resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).Quantif = BL_HandleNull(RTrim(RsResMicro!Quantif), "")
                        resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).FlgImp = IIf(RTrim(RsResMicro!flg_imp) = "S", "Sim", "N�o")
                        resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).FlgTSQ = IIf(RTrim(RsResMicro!flg_tsq) = "S", "Sim", "N�o")
                        
                        CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResMicro, "MIC", k + 1, False
                        resultados.item(indice).DadosAna.Dados2Res.MicroIndex = resultados.item(indice).DadosAna.Dados2Res.MicroIndex + 1
                End Select
                
                Dim A As Integer
                Dim b As String
                Dim c As String
                Dim val As Variant
            
'                A = InStr(1, BL_HandleNull(RsResMicro!cod_gr_antibio, ""), ";")
'                If (A > 0) Then
'                    b = Mid(RsResMicro!cod_gr_antibio, 1, A - 1)
'                    c = Mid(RsResMicro!cod_gr_antibio, A + 1)
'                Else
'                    b = BL_HandleNull(RsResMicro!cod_gr_antibio, "")
'                End If
                
                c = BL_HandleNull(RsResMicro!Cod_Gr_Antibio, "")
                If c <> "" Then
                For Each val In Split(c, ";")
                    b = b & "'" & val & "'" & ","
                Next
                b = "(" & Mid(b, 1, InStrRev(b, ",") - 1) & ")"
                Else
                    b = ""
                End If
'
'                'Se existirem TSQ criar as respectivas instancias
'                CmdResMiTSQ.Parameters(0).Value = SeqRealiza
'                CmdResMiTSQ.Parameters(1).Value = RsResMicro!N_Res
'                CmdResMiTSQ.Parameters(2).Value = RsResMicro!cod_micro
'                CmdResMiTSQ.Parameters(3).Value = b
                
                Set RsResMiTSQ = New ADODB.recordset
                RsResMiTSQ.CursorLocation = adUseServer
                RsResMiTSQ.CursorType = adOpenStatic
                If b = "" Then
                RsResMiTSQ.Source = "SELECT n_res,cod_antib,res_sensib,cmi,flg_imp " & _
                                    " FROM sl_res_tsq_h WHERE seq_realiza= " & seqRealiza & _
                                    " AND n_res = " & RsResMicro!N_Res & _
                                    " AND cod_micro= " & BL_TrataStringParaBD(RsResMicro!cod_micro) & _
                                    " order by n_res "
                Else
                RsResMiTSQ.Source = " SELECT n_res,cod_antib,res_sensib,cmi,flg_imp " & _
                                    " FROM sl_res_tsq_h, sl_rel_grantib " & _
                                    " WHERE seq_realiza=" & seqRealiza & _
                                    " AND n_res = " & RsResMicro!N_Res & _
                                    " AND cod_micro= " & BL_TrataStringParaBD(RsResMicro!cod_micro) & _
                                    " AND cod_antib = cod_antibio AND cod_gr_antibio in " & b & " and flg_imp = 'S' order by n_res,ordem "
                End If
                RsResMiTSQ.Open , gConexao


                'Set RsResMiTSQ = CmdResMiTSQ.Execute
                If resultados(indice).DadosAna.MicroIndex >= 0 Then
                    resultados(indice).DadosAna.ResMicro(i).TSQIndex = -1
                End If
                If resultados(indice).DadosAna.Dados2Res.MicroIndex >= 0 Then
                    resultados(indice).DadosAna.Dados2Res.ResMicro(k).TSQIndex = -1
                End If

                While Not RsResMiTSQ.EOF
                    'seleccionar a descri��o e grupo do antibiotico
                    CmdResAntibiotico.Parameters(0).value = RsResMiTSQ!cod_antib
                    Set RsResAntibiotico = CmdResAntibiotico.Execute
                    Select Case RsResMiTSQ!N_Res
                        Case 1
                            If resultados(indice).DadosAna.ResMicro(i).TSQIndex = -1 Then
                                CriaNovaClasse resultados(indice).DadosAna.ResMicro(i).ResTSQ, "TSQ", 1, True
                                resultados(indice).DadosAna.ResMicro(i).TSQIndex = 0
                            End If

                            resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).codAntib = RsResMiTSQ!cod_antib
                            resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).DescrAntib = RTrim(RsResAntibiotico!descr_antibio)
                            resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).CodGrAntib = BL_HandleNull(RsResAntibiotico!Cod_Gr_Antibio, "")
                            resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).Sensib = BL_HandleNull(RsResMiTSQ!res_sensib, "")
                            resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).CMI = BL_HandleNull(RsResMiTSQ!CMI, "")
                            resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).FlgImp = IIf(RTrim(RsResMiTSQ!flg_imp) = "S", "Sim", "N�o")
                            
                            CriaNovaClasse resultados(indice).DadosAna.ResMicro(i).ResTSQ, "TSQ", j + 1, False
                            resultados(indice).DadosAna.ResMicro(i).TSQIndex = resultados(indice).DadosAna.ResMicro(i).TSQIndex + 1
                        Case 2
                            If resultados(indice).DadosAna.Dados2Res.ResMicro(k).TSQIndex = -1 Then
                                CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ, "TSQ", 1, True
                                resultados(indice).DadosAna.Dados2Res.ResMicro(k).TSQIndex = 0
                            End If
                            
                            resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(l).codAntib = RsResMiTSQ!cod_antib
                            resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(l).DescrAntib = RTrim(RsResAntibiotico!descr_antibio)
                            resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(l).CodGrAntib = BL_HandleNull(RsResAntibiotico!Cod_Gr_Antibio, "")
                            resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(l).Sensib = BL_HandleNull(RsResMiTSQ!res_sensib, "")
                            resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(l).CMI = BL_HandleNull(RsResMiTSQ!CMI, "")
                            resultados.item(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(l).FlgImp = IIf(RTrim(RsResMiTSQ!flg_imp) = "S", "Sim", "N�o")
                            
                            CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ, "TSQ", l + 1, False
                            resultados(indice).DadosAna.Dados2Res.ResMicro(k).TSQIndex = resultados(indice).DadosAna.Dados2Res.ResMicro(k).TSQIndex + 1
                    End Select
                    
                    Select Case RsResMiTSQ!N_Res
                        Case 1
                            j = j + 1
                        Case 2
                            l = l + 1
                    End Select
                    
                    RsResMiTSQ.MoveNext
                Wend
                RsResMiTSQ.Close
                Set RsResMiTSQ = Nothing
                
            End If

            Select Case RsResMicro!N_Res
                Case 1
                    i = i + 1
                Case 2
                    k = k + 1
            End Select
            RsResMicro.MoveNext
        Wend
    End If
    RsResMicro.Close
    Set RsResMicro = Nothing
   
    'Antibiogramas
    CmdResAntib_H.Parameters(0) = seqRealiza
    CmdResAntib_H.Parameters(1) = CodMicro
    Set RsResAntib = CmdResAntib_H.Execute
    If Not RsResAntib.EOF Then
        i = 1
        k = 1
        resultados.item(indice).DadosAna.AntibIndex = -1
        resultados.item(indice).DadosAna.Dados2Res.AntibIndex = -1
        While Not RsResAntib.EOF
            'Selecciona a descri��o do antibiotico
            CmdResAntibiotico.Parameters(0).value = RsResAntib!cod_antib
            Set RsResAntibiotico = CmdResAntibiotico.Execute
            If Not RsResAntibiotico.EOF Then
                Select Case RsResAntib!N_Res
                    Case 1
                        If resultados.item(indice).DadosAna.AntibIndex = -1 Then
                            'Resultados.Item(indice).DadosAna.Res = "Click para ver resultado"
                            CriaNovaClasse resultados(indice).DadosAna.ResAntib, "TSQ", 1, True
                            resultados.item(indice).DadosAna.AntibIndex = 0
                        End If
                    
                        resultados.item(indice).DadosAna.ResAntib(i).codAntib = RsResAntib!cod_antib
                        resultados.item(indice).DadosAna.ResAntib(i).DescrAntib = RTrim(RsResAntibiotico!descr_antibio)
                        resultados.item(indice).DadosAna.ResAntib(i).Sensib = BL_HandleNull(RsResAntib!res_sensib, "")
                        resultados.item(indice).DadosAna.ResAntib(i).CMI = BL_HandleNull(RsResAntib!CMI, "")
                        resultados.item(indice).DadosAna.ResAntib(i).FlgImp = BL_HandleNull(RsResAntib!flg_imp, "N")
                        
                        CriaNovaClasse resultados(indice).DadosAna.ResAntib, "TSQ", i + 1, False
                        resultados.item(indice).DadosAna.AntibIndex = resultados.item(indice).DadosAna.AntibIndex + 1
                        res = 1
                        i = i + 1
                    Case 2
                        If resultados.item(indice).DadosAna.Dados2Res.AntibIndex = -1 Then
                            'Resultados.Item(indice).DadosAna.Dados2Res.Res = "Click para ver resultado"
                            CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResAntib, "TSQ", 1, True
                            resultados.item(indice).DadosAna.Dados2Res.AntibIndex = 0
                        End If
                    
                        resultados.item(indice).DadosAna.Dados2Res.ResAntib(k).codAntib = RsResAntib!cod_antib
                        resultados.item(indice).DadosAna.Dados2Res.ResAntib(k).DescrAntib = RTrim(RsResAntibiotico!descr_antibio)
                        resultados.item(indice).DadosAna.Dados2Res.ResAntib(k).Sensib = BL_HandleNull(RsResAntib!res_sensib, "")
                        resultados.item(indice).DadosAna.Dados2Res.ResAntib(k).CMI = BL_HandleNull(RsResAntib!CMI, "")
                        resultados.item(indice).DadosAna.Dados2Res.ResAntib(k).FlgImp = BL_HandleNull(RsResAntib!flg_imp, "N")
                        
                        CriaNovaClasse resultados(indice).DadosAna.Dados2Res.ResAntib, "TSQ", k + 1, False
                        resultados.item(indice).DadosAna.Dados2Res.AntibIndex = resultados.item(indice).DadosAna.Dados2Res.AntibIndex + 1
                        res = 2
                        k = k + 1
                End Select
            End If
            RsResAntib.MoveNext
        Wend
    End If
    
    'Observa��es da an�lise
    CmdObsAna.Parameters(0).value = seqRealiza
    resultados(indice).DadosAna.ObsAna = ""
    Set RsObsAna = CmdObsAna.Execute
    If Not RsObsAna.EOF Then
        resultados(indice).DadosAna.SeqObsAna = "" & RsObsAna!seq_obs_ana
        resultados(indice).DadosAna.ObsAna = "" & RsObsAna!descr_obs_ana
    End If
    RsObsAna.Close
    Set RsObsAna = Nothing
        
    If Not RsResAlf Is Nothing Then
        RsResAlf.Close
        Set RsResAlf = Nothing
    End If
    If Not RsResFr Is Nothing Then
        RsResFr.Close
        Set RsResFr = Nothing
    End If
    If Not RsResFrDescr Is Nothing Then
        RsResFrDescr.Close
        Set RsResFrDescr = Nothing
    End If
    If Not RsResMicro Is Nothing Then
        RsResMicro.Close
        Set RsResMicro = Nothing
    End If
    If Not RsResMicroDescr Is Nothing Then
        RsResMicroDescr.Close
        Set RsResMicroDescr = Nothing
    End If
    If Not RsResMiTSQ Is Nothing Then
        RsResMiTSQ.Close
        Set RsResMiTSQ = Nothing
    End If
    If Not RsResAntibiotico Is Nothing Then
        RsResAntibiotico.Close
        Set RsResAntibiotico = Nothing
    End If
End Sub

'Sub SeleccionaResultado_H(indice As Long, SeqRealiza As Double, CodMicro As String, Optional OrdemFrase As Integer)
'
'    'Rotina que dependendo do tipo de resultado cria e preenche as classes
'
'    Dim SqlRes As String
'    Dim Res As Integer
'    Dim i As Long
'    Dim j As Long
'    Dim RsResAlf As ADODB.recordset
'    Dim RsResFr As ADODB.recordset
'    Dim RsResFrDescr As ADODB.recordset
'    Dim RsResMicro As ADODB.recordset
'    Dim RsResMicroDescr As ADODB.recordset
'    Dim RsResMiTSQ As ADODB.recordset
'    Dim RsResAntibiotico As ADODB.recordset
'    Dim RsResAntib As ADODB.recordset
'    Dim RsObsAna As ADODB.recordset
'
'    Resultados.Item(indice).DadosAna.SeqRealiza = SeqRealiza
'
'    'Alfanum�ricos / num�ricos / auxiliares
'    CmdResAlf_H.Parameters(0).Value = SeqRealiza
'    Set RsResAlf = CmdResAlf_H.Execute
'    While Not RsResAlf.EOF
'        Select Case RsResAlf!N_Res
'            Case 1
'                Resultados.Item(indice).DadosAna.Res = RsResAlf!Result
'                Res = 1
'            Case 2
'                Resultados.Item(indice).DadosAna.Dados2Res.Res = RsResAlf!Result
'                Res = 2
'        End Select
'        RsResAlf.MoveNext
'    Wend
'
'    'Frases
'    Resultados.Item(indice).DadosAna.FrasesIndex = -1
'    Resultados.Item(indice).DadosAna.Dados2Res.FrasesIndex = -1
'    CmdResFr_H.Parameters(0).Value = SeqRealiza
'    Set RsResFr = CmdResFr_H.Execute
'    If Not RsResFr.EOF Then
'        i = 1
'        While Not RsResFr.EOF
'            'Selecciona a descri��o
'            CmdResFrDescr.Parameters(0).Value = RsResFr!cod_frase
'            Set RsResFrDescr = CmdResFrDescr.Execute
'            If Not RsResFrDescr.EOF Then
'                Select Case RsResFr!N_Res
'                    Case 1
'                        If Resultados.Item(indice).DadosAna.FrasesIndex = -1 Then
'                            CriaNovaClasse Resultados(indice).DadosAna.ResFrases, "FRA", 1, True
''                            Resultados.Item(indice).DadosAna.Res = "Click para ver resultado"
'                            Resultados.Item(indice).DadosAna.Res = RTrim(RsResFrDescr!descr_frase)
'                            Resultados.Item(indice).DadosAna.FrasesIndex = 0
'                        End If
'
'                        Resultados.Item(indice).DadosAna.ResFrases(i).CodFr = RTrim(RsResFr!cod_frase)
'                        Resultados.Item(indice).DadosAna.ResFrases(i).DescrFr = RTrim(RsResFrDescr!descr_frase)
'                        Resultados.Item(indice).DadosAna.ResFrases(i).Ordem = RsResFr!ord_frase
'
'                        CriaNovaClasse Resultados(indice).DadosAna.ResFrases, "FRA", i + 1, False
'                        Resultados.Item(indice).DadosAna.FrasesIndex = Resultados.Item(indice).DadosAna.FrasesIndex + 1
'                    Case 2
'                        If Resultados.Item(indice).DadosAna.Dados2Res.FrasesIndex = -1 Then
'                            CriaNovaClasse Resultados(indice).DadosAna.Dados2Res.ResFrases, "FRA", 1, True
'                            Resultados.Item(indice).DadosAna.Dados2Res.Res = RTrim(RsResFrDescr!descr_frase)
''                            Resultados.Item(indice).DadosAna.Dados2Res.Res = "Click para ver resultado"
'                            Resultados.Item(indice).DadosAna.Dados2Res.FrasesIndex = 0
'                        End If
'
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResFrases(i).CodFr = RTrim(RsResFr!cod_frase)
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResFrases(i).DescrFr = RTrim(RsResFrDescr!descr_frase)
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResFrases(i).Ordem = RsResFr!ord_frase
'
'                        CriaNovaClasse Resultados(indice).DadosAna.Dados2Res.ResFrases, "FRA", i + 1, False
'                        Resultados.Item(indice).DadosAna.Dados2Res.FrasesIndex = Resultados.Item(indice).DadosAna.Dados2Res.FrasesIndex + 1
'                    End Select
'            End If
'            RsResFr.MoveNext
'            i = i + 1
'        Wend
'    End If
'
'    'Microrganismos
'    CmdResMicro_H.Parameters(0).Value = SeqRealiza
'    CmdResMicro_H.Parameters(1).Value = CodMicro
'    Set RsResMicro = CmdResMicro_H.Execute
'    Resultados.Item(indice).DadosAna.MicroIndex = -1
'    Resultados.Item(indice).DadosAna.Dados2Res.MicroIndex = -1
'    If Not RsResMicro.EOF Then
'        i = 1
'        While Not RsResMicro.EOF
'            'Selecciona a descri��o
'            CmdResMicroDescr.Parameters(0).Value = RsResMicro!cod_micro
'            Set RsResMicroDescr = CmdResMicroDescr.Execute
'            j = 1
'            If Not RsResMicroDescr.EOF Then
'                'Preencher classes com os microrganismos
'                Select Case RsResMicro!N_Res
'                    Case 1
'                        If Resultados.Item(indice).DadosAna.MicroIndex = -1 Then
'                            'Resultados.Item(indice).DadosAna.Res = "Click para ver resultado"
'                            Resultados.Item(indice).DadosAna.Res = RTrim(RsResMicroDescr!Descr_Microrg)
'                            CriaNovaClasse Resultados(indice).DadosAna.ResMicro, "MIC", 1, True
'                            Resultados.Item(indice).DadosAna.MicroIndex = 0
'                        End If
'
'                        Resultados.Item(indice).DadosAna.ResMicro(i).CodMicro = RTrim(RsResMicro!cod_micro)
'                        Resultados.Item(indice).DadosAna.ResMicro(i).DescrMicro = RTrim(RsResMicroDescr!Descr_Microrg)
'                        Resultados.Item(indice).DadosAna.ResMicro(i).Quantif = BL_HandleNull(RTrim(RsResMicro!Quantif), "")
'                        Resultados.Item(indice).DadosAna.ResMicro(i).FlgImp = IIf(RTrim(RsResMicro!flg_imp) = "S", "Sim", "N�o")
'                        Resultados.Item(indice).DadosAna.ResMicro(i).FlgTSQ = IIf(RTrim(RsResMicro!Flg_Tsq) = "S", "Sim", "N�o")
'
'                        CriaNovaClasse Resultados(indice).DadosAna.ResMicro, "MIC", i + 1, False
'                        Resultados.Item(indice).DadosAna.MicroIndex = Resultados.Item(indice).DadosAna.MicroIndex + 1
'                    Case 2
'                        If Resultados.Item(indice).DadosAna.Dados2Res.MicroIndex = -1 Then
'                            'Resultados.Item(indice).DadosAna.Dados2Res.Res = "Click para ver resultado"
'                            Resultados.Item(indice).DadosAna.Dados2Res.Res = RTrim(RsResMicroDescr!Descr_Microrg)
'                            CriaNovaClasse Resultados(indice).DadosAna.Dados2Res.ResMicro, "MIC", 1, True
'                            Resultados.Item(indice).DadosAna.Dados2Res.MicroIndex = 0
'                            Res = 2
'                        End If
'
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResMicro(i).CodMicro = RTrim(RsResMicro!cod_micro)
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResMicro(i).DescrMicro = RTrim(RsResMicroDescr!Descr_Microrg)
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResMicro(i).Quantif = BL_HandleNull(RTrim(RsResMicro!Quantif), "")
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResMicro(i).FlgImp = IIf(RTrim(RsResMicro!flg_imp) = "S", "Sim", "N�o")
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResMicro(i).FlgTSQ = IIf(RTrim(RsResMicro!Flg_Tsq) = "S", "Sim", "N�o")
'
'                        CriaNovaClasse Resultados(indice).DadosAna.Dados2Res.ResMicro, "MIC", i + 1, False
'                        Resultados.Item(indice).DadosAna.Dados2Res.MicroIndex = Resultados.Item(indice).DadosAna.Dados2Res.MicroIndex + 1
'                End Select
'
'                'Se existirem TSQ criar as respectivas instancias
'                CmdResMiTSQ_H.Parameters(0).Value = SeqRealiza
'                CmdResMiTSQ_H.Parameters(1).Value = RsResMicro!cod_micro
'                Set RsResMiTSQ = CmdResMiTSQ_H.Execute
'                If Resultados(indice).DadosAna.MicroIndex > 0 Then
'                    Resultados(indice).DadosAna.ResMicro(i).TSQIndex = -1
'                End If
'                If Resultados(indice).DadosAna.Dados2Res.MicroIndex > 0 Then
'                    Resultados(indice).DadosAna.Dados2Res.ResMicro(i).TSQIndex = -1
'                End If
'                If Not RsResMiTSQ.EOF Then
'                    While Not RsResMiTSQ.EOF
'                        'seleccionar a descri��o e grupo do antibiotico
'                        CmdResAntibiotico.Parameters(0).Value = RsResMiTSQ!cod_antib
'                        Set RsResAntibiotico = CmdResAntibiotico.Execute
'                        Select Case RsResMiTSQ!N_Res
'                            Case 1
'                                If Resultados(indice).DadosAna.ResMicro(i).TSQIndex = -1 Then
'                                    CriaNovaClasse Resultados(indice).DadosAna.ResMicro(i).ResTSQ, "TSQ", 1, True
'                                    Resultados(indice).DadosAna.ResMicro(i).TSQIndex = 0
'                                End If
'
'                                Resultados.Item(indice).DadosAna.ResMicro(i).ResTSQ(j).CodAntib = RsResMiTSQ!cod_antib
'                                Resultados.Item(indice).DadosAna.ResMicro(i).ResTSQ(j).DescrAntib = RTrim(RsResAntibiotico!descr_antibio)
'                                Resultados.Item(indice).DadosAna.ResMicro(i).ResTSQ(j).CodGrAntib = BL_HandleNull(RsResAntibiotico!Cod_Gr_Antibio, "")
'                                Resultados.Item(indice).DadosAna.ResMicro(i).ResTSQ(j).Sensib = BL_HandleNull(RsResMiTSQ!res_sensib, "")
'                                Resultados.Item(indice).DadosAna.ResMicro(i).ResTSQ(j).CMI = BL_HandleNull(RsResMiTSQ!CMI, "")
'                                Resultados.Item(indice).DadosAna.ResMicro(i).ResTSQ(j).FlgImp = IIf(RTrim(RsResMiTSQ!flg_imp) = "S", "Sim", "N�o")
'
'                                CriaNovaClasse Resultados(indice).DadosAna.ResMicro(i).ResTSQ, "TSQ", j + 1, False
'                                Resultados(indice).DadosAna.ResMicro(i).TSQIndex = Resultados(indice).DadosAna.ResMicro(i).TSQIndex + 1
'                            Case 2
'                                If Resultados(indice).DadosAna.Dados2Res.ResMicro(i).TSQIndex = -1 Then
'                                    CriaNovaClasse Resultados(indice).DadosAna.Dados2Res.ResMicro(i).ResTSQ, "TSQ", 1, True
'                                    Resultados(indice).DadosAna.Dados2Res.ResMicro(i).TSQIndex = 0
'                                End If
'
'                                Resultados.Item(indice).DadosAna.Dados2Res.ResMicro(i).ResTSQ(j).CodAntib = RsResMiTSQ!cod_antib
'                                Resultados.Item(indice).DadosAna.Dados2Res.ResMicro(i).ResTSQ(j).DescrAntib = RTrim(RsResAntibiotico!descr_antibio)
'                                Resultados.Item(indice).DadosAna.Dados2Res.ResMicro(i).ResTSQ(j).CodGrAntib = BL_HandleNull(RsResAntibiotico!Cod_Gr_Antibio, "")
'                                Resultados.Item(indice).DadosAna.Dados2Res.ResMicro(i).ResTSQ(j).Sensib = BL_HandleNull(RsResMiTSQ!res_sensib, "")
'                                Resultados.Item(indice).DadosAna.Dados2Res.ResMicro(i).ResTSQ(j).CMI = BL_HandleNull(RsResMiTSQ!CMI, "")
'                                Resultados.Item(indice).DadosAna.Dados2Res.ResMicro(i).ResTSQ(j).FlgImp = IIf(RTrim(RsResMiTSQ!flg_imp) = "S", "Sim", "N�o")
'
'                                CriaNovaClasse Resultados(indice).DadosAna.Dados2Res.ResMicro(i).ResTSQ, "TSQ", j + 1, False
'                                Resultados(indice).DadosAna.Dados2Res.ResMicro(i).TSQIndex = Resultados(indice).DadosAna.Dados2Res.ResMicro(i).TSQIndex + 1
'                        End Select
'                        j = j + 1
'                        RsResMiTSQ.MoveNext
'                    Wend
'                End If
'
'            End If
'
'            i = i + 1
'            RsResMicro.MoveNext
'        Wend
'    End If
'
'    'Antibiogramas
'    CmdResAntib_H.Parameters(0) = SeqRealiza
'    CmdResAntib_H.Parameters(1) = CodMicro
'    Set RsResAntib = CmdResAntib_H.Execute
'    If Not RsResAntib.EOF Then
'        i = 1
'        Resultados.Item(indice).DadosAna.AntibIndex = -1
'        Resultados.Item(indice).DadosAna.Dados2Res.AntibIndex = -1
'        While Not RsResAntib.EOF
'            'Selecciona a descri��o do antibiotico
'            CmdResAntibiotico.Parameters(0).Value = RsResAntib!cod_antib
'            Set RsResAntibiotico = CmdResAntibiotico.Execute
'            If Not RsResAntibiotico.EOF Then
'                Select Case RsResAntib!N_Res
'                    Case 1
'                        If Resultados.Item(indice).DadosAna.AntibIndex = -1 Then
'                            'Resultados.Item(indice).DadosAna.Res = "Click para ver resultado"
'                            CriaNovaClasse Resultados(indice).DadosAna.ResAntib, "TSQ", 1, True
'                            Resultados.Item(indice).DadosAna.AntibIndex = 0
'                        End If
'
'                        Resultados.Item(indice).DadosAna.ResAntib(i).CodAntib = RsResAntib!cod_antib
'                        Resultados.Item(indice).DadosAna.ResAntib(i).DescrAntib = RTrim(RsResAntibiotico!descr_antibio)
'                        Resultados.Item(indice).DadosAna.ResAntib(i).Sensib = BL_HandleNull(RsResAntib!res_sensib, "")
'                        Resultados.Item(indice).DadosAna.ResAntib(i).CMI = BL_HandleNull(RsResAntib!CMI, "")
'                        Resultados.Item(indice).DadosAna.ResAntib(i).FlgImp = BL_HandleNull(RsResAntib!flg_imp, "N")
'
'                        CriaNovaClasse Resultados(indice).DadosAna.ResAntib, "TSQ", i + 1, False
'                        Resultados.Item(indice).DadosAna.AntibIndex = Resultados.Item(indice).DadosAna.AntibIndex + 1
'                        Res = 1
'                    Case 2
'                        If Resultados.Item(indice).DadosAna.Dados2Res.AntibIndex = -1 Then
'                            'Resultados.Item(indice).DadosAna.Dados2Res.Res = "Click para ver resultado"
'                            CriaNovaClasse Resultados(indice).DadosAna.Dados2Res.ResAntib, "TSQ", 1, True
'                            Resultados.Item(indice).DadosAna.Dados2Res.AntibIndex = 0
'                        End If
'
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResAntib(i).CodAntib = RsResAntib!cod_antib
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResAntib(i).DescrAntib = RTrim(RsResAntibiotico!descr_antibio)
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResAntib(i).Sensib = BL_HandleNull(RsResAntib!res_sensib, "")
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResAntib(i).CMI = BL_HandleNull(RsResAntib!CMI, "")
'                        Resultados.Item(indice).DadosAna.Dados2Res.ResAntib(i).FlgImp = BL_HandleNull(RsResAntib!flg_imp, "N")
'
'                        CriaNovaClasse Resultados(indice).DadosAna.Dados2Res.ResAntib, "TSQ", i + 1, False
'                        Resultados.Item(indice).DadosAna.Dados2Res.AntibIndex = Resultados.Item(indice).DadosAna.Dados2Res.AntibIndex + 1
'                        Res = 2
'                End Select
'            End If
'            RsResAntib.MoveNext
'            i = i + 1
'        Wend
'    End If
'
'    'Observa��es da an�lise
'    CmdObsAna.Parameters(0).Value = SeqRealiza
'    Resultados(indice).DadosAna.ObsAna = ""
'    Set RsObsAna = CmdObsAna.Execute
'    If Not RsObsAna.EOF Then
'        Resultados(indice).DadosAna.SeqObsAna = "" & RsObsAna!seq_obs_ana
'        Resultados(indice).DadosAna.ObsAna = "" & RsObsAna!descr_obs_ana
'    End If
'
'    RsObsAna.Close
'    Set RsObsAna = Nothing
'
'    If Not RsResAlf Is Nothing Then
'        RsResAlf.Close
'        Set RsResAlf = Nothing
'    End If
'    If Not RsResFr Is Nothing Then
'        RsResFr.Close
'        Set RsResFr = Nothing
'    End If
'    If Not RsResFrDescr Is Nothing Then
'        RsResFrDescr.Close
'        Set RsResFrDescr = Nothing
'    End If
'    If Not RsResMicro Is Nothing Then
'        RsResMicro.Close
'        Set RsResMicro = Nothing
'    End If
'    If Not RsResMicroDescr Is Nothing Then
'        RsResMicroDescr.Close
'        Set RsResMicroDescr = Nothing
'    End If
'    If Not RsResMiTSQ Is Nothing Then
'        RsResMiTSQ.Close
'        Set RsResMiTSQ = Nothing
'    End If
'    If Not RsResAntibiotico Is Nothing Then
'        RsResAntibiotico.Close
'        Set RsResAntibiotico = Nothing
'    End If
'End Sub
'
'
Private Sub CriaNovaClasse(Coleccao As Collection, tipo As String, Index As Long, Optional EmCascata As Variant)
' Tipo = RES, MIC, FRA, TSQ : Indica qual o tipo de elemento se pretende criar
' EmCascata = True : Cria todos elementos at� o Index
' EmCascata = False: Cria apenas o elemento Index

    Dim Cascata As Boolean
    Dim i As Long
    Dim AuxRes As New ClResultados
    Dim AuxMic As New ClDadosMicro
    Dim AuxTsq As New ClDadosTsq
    Dim AuxFra As New ClDadosFrases
    Dim AuxAntib As New ClDadosTsq
    
    If IsMissing(EmCascata) Then
        Cascata = False
    Else
        Cascata = EmCascata
    End If
    
    If tipo = "RES" Then
        AuxMic.TSQIndex = 0
        AuxMic.ResTSQ.Add AuxTsq
        
        AuxRes.DadosAna.MicroIndex = 0
        AuxRes.DadosAna.ResMicro.Add AuxMic
        
        AuxRes.DadosAna.FrasesIndex = 0
        AuxRes.DadosAna.ResFrases.Add AuxFra
        
        AuxRes.DadosAna.AntibIndex = 0
        AuxRes.DadosAna.ResAntib.Add AuxAntib
        
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRes
            Next i
        Else
            Coleccao.Add AuxRes
        End If
    ElseIf tipo = "MIC" Then
        AuxMic.TSQIndex = 0
        AuxMic.ResTSQ.Add AuxTsq
    
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxMic
            Next i
        Else
            Coleccao.Add AuxMic
        End If
    ElseIf tipo = "FRA" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxFra
            Next i
        Else
            Coleccao.Add AuxFra
        End If
    ElseIf tipo = "TSQ" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxTsq
            Next i
        Else
            Coleccao.Add AuxTsq
        End If
    End If
    
    Set AuxFra = Nothing
    Set AuxMic = Nothing
    Set AuxRes = Nothing
    Set AuxTsq = Nothing
    Set AuxAntib = Nothing
    
End Sub

Sub CarregaInfoFrases(indice As Integer)
    Dim j As Long
    Dim Max As Long
    
    
    If FGAna.Col = 2 Then
        Max = resultados(indice).DadosAna.Dados2Res.FrasesIndex
    Else
        Max = resultados(indice).DadosAna.FrasesIndex
    End If
    
    EcInfo.Text = ""
    
    j = Max
    While j > 0
        If FGAna.Col = 2 Then
            EcInfo.Text = EcInfo.Text & resultados(indice).DadosAna.Dados2Res.ResFrases(j).DescrFr & Chr(13) & vbLf
        Else
            EcInfo.Text = EcInfo.Text & resultados(indice).DadosAna.ResFrases(j).DescrFr & Chr(13) & vbLf
        End If
        j = j - 1
    Wend
End Sub
Sub CarregaInfoMicro(indice As Integer)
    Dim j As Long
    Dim k As Long
    Dim MaxMicro As Long
    Dim MaxTsq As Long
    
    If FGAna.Col = 1 Then
        MaxMicro = resultados(indice).DadosAna.MicroIndex
    Else
        MaxMicro = resultados(indice).DadosAna.Dados2Res.MicroIndex
    End If
    
    EcInfo.Text = ""
    EcInfo.Font = "Courier new"
    For k = 1 To MaxMicro
    
        If FGAna.Col = 1 Then
            MaxTsq = resultados(indice).DadosAna.ResMicro(k).TSQIndex
            EcInfo.Text = EcInfo.Text & Trim(resultados(indice).DadosAna.ResMicro(k).DescrMicro) & IIf(VerificaResMicro(FGAna.Col, indice) = True, ": ", "") & Chr(13) & vbLf
            If resultados(indice).DadosAna.ResMicro(k).FlgImp <> "N�o" Then
                EcInfo.Text = EcInfo.Text & "     (Imprimir)" & Chr(13) & vbLf
            Else
                EcInfo.Text = EcInfo.Text & "     (N�o Imprimir)" & Chr(13) & vbLf
            End If
            If Trim(resultados(indice).DadosAna.ResMicro(k).Quantif) <> "" Then EcInfo.Text = EcInfo.Text & "     Quantifica��o:  " & Trim(resultados(indice).DadosAna.ResMicro(k).Quantif) & Chr(13) & vbLf
        Else
            MaxTsq = resultados(indice).DadosAna.Dados2Res.ResMicro(k).TSQIndex
            EcInfo.Text = EcInfo.Text & Trim(resultados(indice).DadosAna.Dados2Res.ResMicro(k).DescrMicro) & IIf(VerificaResMicro(FGAna.Col, indice) = True, ": ", "") & Chr(13) & vbLf
            If resultados(indice).DadosAna.Dados2Res.ResMicro(k).FlgImp <> "N�o" Then
                EcInfo.Text = EcInfo.Text & "     (Imprimir)" & Chr(13) & vbLf
            Else
                EcInfo.Text = EcInfo.Text & "     (N�o Imprimir)" & Chr(13) & vbLf
            End If
            If Trim(resultados(indice).DadosAna.Dados2Res.ResMicro(k).Quantif) <> "" Then EcInfo.Text = EcInfo.Text & "     Quantifica��o:  " & Trim(resultados(indice).DadosAna.Dados2Res.ResMicro(k).Quantif) & Chr(13) & vbLf
        End If
    
        For j = 1 To MaxTsq
            If FGAna.Col = 1 Then
                If Trim(resultados(indice).DadosAna.ResMicro(k).ResTSQ(j).CMI) <> "" Or _
                   Trim(resultados(indice).DadosAna.ResMicro(k).ResTSQ(j).Sensib) <> "" Or _
                   Trim(resultados(indice).DadosAna.ResMicro(k).ResTSQ(j).FlgImp) <> "N�o" Then
                    EcInfo.Text = EcInfo.Text & "     " & _
                    resultados(indice).DadosAna.ResMicro(k).ResTSQ(j).DescrAntib & IIf(Trim(resultados(indice).DadosAna.ResMicro(k).ResTSQ(j).Sensib) <> "", ":     ", "") & _
                    Space(30 - Len(resultados(indice).DadosAna.ResMicro(k).ResTSQ(j).DescrAntib)) & IIf(Trim(resultados(indice).DadosAna.ResMicro(k).ResTSQ(j).Sensib) <> "", "", "") & _
                    resultados(indice).DadosAna.ResMicro(k).ResTSQ(j).Sensib & _
                    IIf((resultados(indice).DadosAna.ResMicro(k).ResTSQ(j).FlgImp = "Sim"), "      (Imprimir)", "      (N�o Imprimir)") & Chr(13) & vbLf
                End If
            Else
                If Trim(resultados(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(j).CMI) <> "" Or _
                   Trim(resultados(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(j).Sensib) <> "" Or _
                   Trim(resultados(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(j).FlgImp) <> "N�o" Then
                    EcInfo.Text = EcInfo.Text & "     " & _
                    resultados(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(j).DescrAntib & IIf(Trim(resultados(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(j).Sensib) <> "", ":     ", "") & _
                    IIf(Trim(resultados(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(j).Sensib) <> "", "", "") & _
                    resultados(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(j).Sensib & _
                    IIf((resultados(indice).DadosAna.Dados2Res.ResMicro(k).ResTSQ(j).FlgImp = "Sim"), "      (Imprimir)", "      (N�o Imprimir)") & Chr(13) & vbLf
                End If
            End If
        Next j
    Next k
End Sub
Function VerificaResMicro(Col As Variant, indice As Integer) As Boolean
'Verifica se foram introduzidos resultados
    Dim i As Integer
    Dim Existe As Boolean
    Dim j As Integer
    
    i = 1
    VerificaResMicro = False
    If Col = 1 Then
        While i <= resultados(indice).DadosAna.MicroIndex
            If resultados(indice).DadosAna.ResMicro(i).TSQIndex <> -1 Then
                Existe = False
                j = 1
                While j <= resultados(indice).DadosAna.ResMicro(i).TSQIndex And Existe = False
                    If resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).Sensib <> "" Or resultados.item(indice).DadosAna.ResMicro(i).ResTSQ(j).FlgImp = "Sim" Then Existe = True
                    j = j + 1
                Wend
                If Existe Or resultados(indice).DadosAna.ResMicro(i).Quantif <> "" Then
                    VerificaResMicro = True
                    i = resultados(indice).DadosAna.MicroIndex
                End If
            End If
            i = i + 1
        Wend
    Else
        While i <= resultados(indice).DadosAna.Dados2Res.MicroIndex
            If resultados(indice).DadosAna.Dados2Res.ResMicro(i).TSQIndex <> -1 Then
                Existe = False
                j = 1
                While j <= resultados(indice).DadosAna.Dados2Res.ResMicro(i).TSQIndex And Existe = False
                    If resultados.item(indice).DadosAna.Dados2Res.ResMicro(i).ResTSQ(j).Sensib <> "" Or resultados.item(indice).DadosAna.Dados2Res.ResMicro(i).ResTSQ(j).FlgImp = "Sim" Then Existe = True
                    j = j + 1
                Wend
                If Existe Or resultados(indice).DadosAna.Dados2Res.ResMicro(i).Quantif <> "" Then
                    VerificaResMicro = True
                    i = resultados(indice).DadosAna.Dados2Res.MicroIndex
                End If
            End If
            i = i + 1
        Wend
    End If
End Function
Sub CarregaInfoAntib(indice As Integer)
    Dim j As Long
    Dim Max As Long
    
    If FGAna.Col = 1 Then
        If resultados(indice).DadosAna.AntibIndex <> 0 Then
            Max = resultados(indice).DadosAna.AntibIndex
        End If
    Else
        If resultados(indice).DadosAna.Dados2Res.AntibIndex <> 0 Then
            Max = resultados(indice).DadosAna.Dados2Res.AntibIndex
        End If
    End If
    
    EcInfo.Text = ""
    
    For j = 1 To Max
        If FGAna.Col = 1 Then
            If Trim(resultados(indice).DadosAna.ResAntib(j).CMI) <> "" Or _
               Trim(resultados(indice).DadosAna.ResAntib(j).Sensib) <> "" Or _
               Trim(resultados(indice).DadosAna.ResAntib(j).FlgImp) <> "N�o" Then
                EcInfo.Text = EcInfo.Text & _
                resultados(indice).DadosAna.ResAntib(j).DescrAntib & IIf(Trim(resultados(indice).DadosAna.ResAntib(j).Sensib) <> "", ":     ", "") & _
                IIf(Trim(resultados(indice).DadosAna.ResAntib(j).Sensib) <> "", ",  ", "") & _
                resultados(indice).DadosAna.ResAntib(j).Sensib & _
                IIf((resultados(indice).DadosAna.ResAntib(j).FlgImp = "Sim"), "      (Imprimir)", "      (N�o Imprimir)") & Chr(13) & vbLf
            End If
        Else
            If Trim(resultados(indice).DadosAna.Dados2Res.ResAntib(j).CMI) <> "" Or _
               Trim(resultados(indice).DadosAna.Dados2Res.ResAntib(j).Sensib) <> "" Or _
               Trim(resultados(indice).DadosAna.Dados2Res.ResAntib(j).FlgImp) <> "N�o" Then
                EcInfo.Text = EcInfo.Text & _
                resultados(indice).DadosAna.Dados2Res.ResAntib(j).DescrAntib & IIf(Trim(resultados(indice).DadosAna.Dados2Res.ResAntib(j).CMI) <> "" Or Trim(resultados(indice).DadosAna.Dados2Res.ResAntib(j).Sensib) <> "", ":     ", "") & _
                IIf(Trim(resultados(indice).DadosAna.Dados2Res.ResAntib(j).CMI) <> "" And Trim(resultados(indice).DadosAna.Dados2Res.ResAntib(j).Sensib) <> "", ",  ", "") & _
                resultados(indice).DadosAna.Dados2Res.ResAntib(j).Sensib & _
                IIf((resultados(indice).DadosAna.Dados2Res.ResAntib(j).FlgImp = "Sim"), "      (Imprimir)", "      (N�o Imprimir)") & Chr(13) & vbLf
            End If
        End If
    Next j
    
End Sub

Sub TrataLinhaRes()
    Dim tipo As String
    Dim Tipo2 As String
    Dim indice As Integer
            
    EcInfo.Text = ""
    indice = 1
    While resultados(indice).DadosAna.estado <> FGAna.row And indice < resultados.Count
        indice = indice + 1
    Wend
    If indice < resultados.Count Then
        tipo = resultados(indice).DadosAna.TRes
        Tipo2 = resultados(indice).DadosAna.Dados2Res.TRes
        
        '1� resultado
        If FGAna.Col = 1 Then
            Select Case tipo
                Case gT_Frase
                    CarregaInfoFrases indice
                Case gT_Microrganismo
                    CarregaInfoMicro indice
                Case gT_Antibiograma
                    CarregaInfoAntib indice
            End Select
        '2� Resultado
        ElseIf FGAna.Col = 2 Then
            Select Case Tipo2
                Case gT_Frase
                    CarregaInfoFrases indice
                Case gT_Microrganismo
                    CarregaInfoMicro indice
                Case gT_Antibiograma
                    CarregaInfoAntib indice
            End Select
        End If
    End If
End Sub

Sub PesquisaRelatorio()
    'Campos do RecordSet
    Dim ChavesPesq(1) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    Dim resultados(1) As Variant
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    'Query
    ClausulaFrom = "sl_relatorios, sl_relatorios_req"
    ClausulaWhere = "sl_relatorios.seq_relatorio = sl_relatorios_req.seq_relatorio and n_req = " & EcNumReq.Text
    'Crit�rio
    CampoPesquisa1 = "descr_relatorio"
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "sl_relatorios.seq_relatorio"
    
    Headers(1) = "Relat�rio"
    CamposEcran(1) = "descr_relatorio"
    Tamanhos(1) = 3500
    
    Headers(2) = "Data de Sa�da "
    CamposEcran(2) = "dt_saida"
    Tamanhos(2) = 2000
    
    
    'N� de Campos a pesquisar na Classe=>Resultados
    CamposRetorno.InicializaResultados 1

        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Incompatibilidade de Resultados")
    
    mensagem = "N�o foi encontrada nenhuma incompatibilidade de resultados."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcSeqRel.Text = resultados(1) & " "
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
End Sub

Public Sub FuncaoImprimir()

    If BL_PreviewAberto("Mapa de Resultados") = True Then
        If gImprimirDestino = 1 Then
            BL_FechaPreview ("Mapa de Resultados")
        Else
            Exit Sub
        End If
    End If
    'Call BtImpRes_Click
    ImpRes
End Sub

Private Sub ImpRes()
    Dim RsDoc As ADODB.recordset
    Dim RsRel As ADODB.recordset
    Dim SeqRel As String
    Dim n As Integer
    Dim sql As String
    Dim sURL As String
    Dim IE As New InternetExplorer
    Dim Criterio As String
    Dim i As Integer
    Dim sIDDoc As String

    If gMultiReport <> 1 Then
        Call IR_ImprimeResultados(False, False, EcNumReq.Text, EcCodEstadoReq.Text, EcCodUteSeq.Text, , , , False, , , , , , , , , , , False, False, False)
        BL_RegistaImprimeReq EcNumReq, -1
    End If
    Exit Sub

ErrorGesdoc:
    BG_Mensagem mediMsgBox, "Erro: " & Err.Description, vbOKOnly + vbCritical, "Erro"
    Call BG_LogFile_Erros("Erro Inesperado : BtImpRes_Click (FormConsRequis) -> " & Err.Description)
    Exit Sub
End Sub



' -----------------------------------------------------------------

' PREENCHE ESTADO DA REQUISI��O NO ERESULTS

' -----------------------------------------------------------------
Private Sub PreencheEnvioEResults()
    Dim estado As Integer
    On Error GoTo TrataErro
    If geResults <> True Then
        Exit Sub
    End If
    estado = BL_RetornaEstadoEnvioEresults(EcNumReq)
    If estado = -1 Then
        LbeResults.caption = "Requisi��o n�o disponibilizada online"
    ElseIf estado = 0 Then
        LbeResults.caption = "Requisi��o � espera de ser disponibilizada online"
    ElseIf estado = 1 Then
        LbeResults.caption = "Requisi��o disponivel online " & BL_RetornaDataEnvioEresults(EcNumReq)
    Else
        LbeResults.caption = "Erro ao disponibilizar online"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheEnvioEResults: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheEnvioEResults"
    Exit Sub
    Resume Next
End Sub






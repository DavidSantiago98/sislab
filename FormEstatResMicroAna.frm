VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormEstatResMicroAna 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEstatResMicroAna"
   ClientHeight    =   8445
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7410
   Icon            =   "FormEstatResMicroAna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8445
   ScaleWidth      =   7410
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodAntib 
      Height          =   285
      Left            =   240
      TabIndex        =   46
      Top             =   6240
      Width           =   735
   End
   Begin VB.TextBox EcDescrAntib 
      Enabled         =   0   'False
      Height          =   285
      Left            =   960
      TabIndex        =   45
      Top             =   6240
      Width           =   1935
   End
   Begin VB.CommandButton BtPesquisaAntib 
      Height          =   315
      Left            =   2880
      Picture         =   "FormEstatResMicroAna.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   44
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Exames"
      Top             =   6240
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapFrase 
      Height          =   285
      Left            =   1440
      TabIndex        =   43
      Top             =   8880
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapProduto 
      Height          =   285
      Left            =   0
      TabIndex        =   42
      Top             =   8880
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapGrupo 
      Height          =   285
      Left            =   2640
      TabIndex        =   41
      Top             =   8880
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapEspecif 
      Height          =   285
      Left            =   3840
      TabIndex        =   40
      Top             =   8880
      Width           =   975
   End
   Begin VB.CheckBox Flg_OrderProd 
      Caption         =   "Agrupar por Produto"
      Height          =   255
      Left            =   240
      TabIndex        =   36
      Top             =   8160
      Width           =   1815
   End
   Begin VB.CheckBox Flg_OrderProven 
      Caption         =   "Agrupar por Proveni�ncia"
      Height          =   255
      Left            =   4920
      TabIndex        =   35
      Top             =   8160
      Width           =   2295
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Width           =   7215
      Begin VB.OptionButton Option1 
         Caption         =   "Dt. Chegada"
         Height          =   255
         Index           =   0
         Left            =   4680
         TabIndex        =   49
         Top             =   240
         Width           =   1335
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Dt. Valida��o"
         Height          =   255
         Index           =   1
         Left            =   4680
         TabIndex        =   48
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   1560
         TabIndex        =   0
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   3120
         TabIndex        =   1
         Top             =   360
         Width           =   1095
      End
      Begin VB.CommandButton BtReportLista 
         Caption         =   "Lista"
         Height          =   725
         Left            =   6480
         Picture         =   "FormEstatResMicroAna.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   120
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2850
         TabIndex        =   34
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   720
         TabIndex        =   33
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Condi��es de Pesquisa"
      Height          =   7335
      Left            =   0
      TabIndex        =   15
      Top             =   840
      Width           =   7215
      Begin VB.ComboBox CbSensib 
         Height          =   315
         Left            =   3480
         Style           =   2  'Dropdown List
         TabIndex        =   47
         Top             =   5400
         Width           =   2535
      End
      Begin VB.CommandButton BtAdicionar 
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6480
         TabIndex        =   14
         Top             =   5040
         Width           =   495
      End
      Begin VB.CommandButton BtPesquisaFrase 
         Height          =   315
         Left            =   6000
         Picture         =   "FormEstatResMicroAna.frx":06E0
         Style           =   1  'Graphical
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Exames"
         Top             =   5040
         Width           =   375
      End
      Begin VB.TextBox EcDescrFrase 
         Enabled         =   0   'False
         Height          =   285
         Left            =   4080
         TabIndex        =   39
         Top             =   5040
         Width           =   1935
      End
      Begin VB.TextBox EcCodFrase 
         Height          =   285
         Left            =   3480
         TabIndex        =   12
         Top             =   5040
         Width           =   615
      End
      Begin VB.CommandButton BtPesquisaAnaS 
         Height          =   315
         Left            =   2880
         Picture         =   "FormEstatResMicroAna.frx":0C6A
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Exames"
         Top             =   5040
         Width           =   375
      End
      Begin VB.TextBox EcDescrAnaS 
         Enabled         =   0   'False
         Height          =   285
         Left            =   960
         TabIndex        =   38
         Top             =   5040
         Width           =   1935
      End
      Begin VB.TextBox EcCodAnaS 
         Height          =   285
         Left            =   240
         TabIndex        =   10
         Top             =   5040
         Width           =   735
      End
      Begin MSFlexGridLib.MSFlexGrid FgAna 
         Height          =   1455
         Left            =   240
         TabIndex        =   37
         Top             =   5760
         Width           =   6855
         _ExtentX        =   12091
         _ExtentY        =   2566
         _Version        =   393216
         FixedCols       =   0
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
      End
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   4680
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   240
         Width           =   1455
      End
      Begin VB.ComboBox EcDescrSexo 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   240
         Width           =   1455
      End
      Begin VB.CommandButton BtPesquisaGrupo 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicroAna.frx":11F4
         Style           =   1  'Graphical
         TabIndex        =   22
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos "
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox EcDescrGrupo 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   600
         Width           =   4095
      End
      Begin VB.TextBox EcCodGrupo 
         Height          =   315
         Left            =   1560
         TabIndex        =   4
         Top             =   600
         Width           =   735
      End
      Begin VB.ListBox ListaPerfis 
         Height          =   645
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   20
         Top             =   4080
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaPerfis 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicroAna.frx":177E
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Exames"
         Top             =   4080
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaGrPerfis 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicroAna.frx":1D08
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Grupos de Exames"
         Top             =   3240
         Width           =   375
      End
      Begin VB.ListBox ListaGrPerfis 
         Height          =   645
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   19
         Top             =   3240
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaProven 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicroAna.frx":2292
         Style           =   1  'Graphical
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   2520
         Width           =   375
      End
      Begin VB.ListBox ListaProven 
         Height          =   645
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   18
         Top             =   2520
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaProdutos 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicroAna.frx":281C
         Style           =   1  'Graphical
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   1080
         Width           =   375
      End
      Begin VB.ListBox ListaProdutos 
         Height          =   645
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   17
         Top             =   1080
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaEspecif 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicroAna.frx":2DA6
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   1800
         Width           =   375
      End
      Begin VB.ListBox ListaEspecif 
         Height          =   645
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   16
         Top             =   1800
         Width           =   4800
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Left            =   360
         TabIndex        =   30
         Top             =   2565
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   3480
         TabIndex        =   29
         Top             =   240
         Width           =   735
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Se&xo"
         Height          =   195
         Left            =   360
         TabIndex        =   28
         Top             =   360
         Width           =   360
      End
      Begin VB.Label Label10 
         Caption         =   "&Grupo An�lises"
         Height          =   255
         Left            =   360
         TabIndex        =   27
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label7 
         Caption         =   "&Exames"
         Height          =   255
         Left            =   360
         TabIndex        =   26
         Top             =   4200
         Width           =   1215
      End
      Begin VB.Label Label8 
         Caption         =   "&Grupos Exames"
         Height          =   255
         Left            =   360
         TabIndex        =   25
         Top             =   3240
         Width           =   1215
      End
      Begin VB.Label Label11 
         Caption         =   "Especifica��o"
         Height          =   255
         Left            =   360
         TabIndex        =   24
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Pro&duto"
         Height          =   255
         Left            =   360
         TabIndex        =   23
         Top             =   1080
         Width           =   975
      End
   End
End
Attribute VB_Name = "FormEstatResMicroAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim ListaOrigem As Control
Dim ListaDestino As Control
Public rs As ADODB.recordset
Public RSListaDestino As ADODB.recordset
Public RSListaOrigem As ADODB.recordset

'Estrutura que guarda as an�lises e frases
Private Type EstrutAnaFrase
    codAnaS As String
    DescrAnaS As String
    CodFrase As String
    cod_antib As String
    Cod_t_sensib As Integer
    tipo As String
    seqRealiza As Long
End Type
Dim AnaFrase() As EstrutAnaFrase
Dim TotalAnaFrase As Integer

'Estrutura que guarda as requisicoes
Private Type EstrutReq
    NReq As String
    nome As String
    Utente As String
    descr_proven As String
    descr_produto As String
    analises() As EstrutAnaFrase
    totalAnalises As Long
    analisesExistentes As Long
End Type
Dim Requis() As EstrutReq
Dim totalRequis As Integer
'NELSONPSILVA Glintt-HS-18011 09.02.2018
Dim tabela_aux As String
'

Sub Preenche_Estatistica()
    
    Dim sql As String
    Dim continua As Boolean
    Dim Produtos As String
    Dim especificacoes As String
    Dim proveniencias As String
    Dim frase As String
    Dim i As Integer
    Dim grPerfis As String
    Dim perfis As String
    
    If TotalAnaFrase = 0 Then Exit Sub
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Estat�stica Resultados Microbiologia") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("EstatisticaResMicroAna", "Estat�stica Resultados Microbiologia", crptToPrinter)
    Else
        continua = BL_IniciaReport("EstatisticaResMicroAna", "Estat�stica Resultados Microbiologia", crptToWindow)
    End If
    If continua = False Then Exit Sub
        
    'Call Cria_TmpRec_Estatistica
    
    PreencheTabelaTemporaria
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
'    Report.SQLQuery = "SELECT SL_CR_ESTATRESMICRO.DESCR_MICRORG, SL_CR_ESTATMICRORG.DESCR_ANTIBIO, SL_CR_ESTATMICRORG.RES_SENSIB, SL_CR_ESTATMICRORG.DESCR_PROVEN, SL_CR_ESTATMICRORG.DESCR_PRODUTO " & _
'        " FROM SL_CR_ESTATMICRORG" & gNumeroSessao & " SL_CR_ESTATMICRORG ORDER BY DESCR_PROVEN,DESCR_PRODUTO,DESCR_MICRORG, DESCR_ANTIBIO, RES_SENSIB "
    Report.SelectionFormula = "{SL_CR_ESTATRESMICROANA.nome_computador} = '" & BG_SYS_GetComputerName & "'"
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    
    Report.formulas(2) = "Frase=" & BL_TrataStringParaBD("" & frase)
    
    If ListaProven.ListCount = 0 Then
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("Todas")
    Else
        proveniencias = ""
        For i = 0 To ListaProven.ListCount - 1
            proveniencias = proveniencias & ListaProven.List(i) & "; "
        Next
        If Len(proveniencias) > 200 Then
            proveniencias = left(proveniencias, 200) & "..."
        End If
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("" & proveniencias)
    End If
    
    If ListaProdutos.ListCount = 0 Then
        Report.formulas(4) = "Produto=" & BL_TrataStringParaBD("Todos")
    Else
        Produtos = ""
        For i = 0 To ListaProdutos.ListCount - 1
           Produtos = Produtos & ListaProdutos.List(i) & "; "
        Next
        If Len(Produtos) > 200 Then
            Produtos = left(Produtos, 200) & "..."
        End If
        Report.formulas(4) = "Produto=" & BL_TrataStringParaBD("" & Produtos)
    End If
    If CbSituacao.ListIndex = -1 Then
        Report.formulas(5) = "Situacao=" & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(5) = "Situacao=" & BL_TrataStringParaBD("" & CbSituacao.Text)
    End If
    If EcDescrSexo.ListIndex = -1 Then
        Report.formulas(6) = "Sexo=" & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(6) = "Sexo=" & BL_TrataStringParaBD("" & EcDescrSexo.Text)
    End If
    If Flg_OrderProd.value = 1 Then
        Report.formulas(7) = "OrderProd=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(7) = "OrderProd=" & BL_TrataStringParaBD("N")
    End If
    If Flg_OrderProven.value = 1 Then
        Report.formulas(8) = "OrderProven=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(8) = "OrderProven=" & BL_TrataStringParaBD("N")
    End If
    
    If ListaEspecif.ListCount = 0 Then
        Report.formulas(9) = "Especif=" & BL_TrataStringParaBD("Todas")
    Else
        especificacoes = ""
        For i = 0 To ListaEspecif.ListCount - 1
            especificacoes = especificacoes & ListaEspecif.List(i) & "; "
        Next
        If Len(especificacoes) > 200 Then
            especificacoes = left(especificacoes, 200) & "..."
        End If
        Report.formulas(9) = "Especif=" & BL_TrataStringParaBD("" & especificacoes)
    End If
    
    If ListaGrPerfis.ListCount = 0 Then
        Report.formulas(10) = "GrPerfis=" & BL_TrataStringParaBD("Todos")
    Else
        especificacoes = ""
        For i = 0 To ListaGrPerfis.ListCount - 1
            grPerfis = grPerfis & ListaGrPerfis.List(i) & "; "
        Next
        If Len(grPerfis) > 200 Then
            grPerfis = left(grPerfis, 200) & "..."
        End If
        Report.formulas(10) = "GrPerfis=" & BL_TrataStringParaBD("" & grPerfis)
    End If
    
    If ListaPerfis.ListCount = 0 Then
        Report.formulas(11) = "Perfis=" & BL_TrataStringParaBD("Todos")
    Else
        perfis = ""
        For i = 0 To ListaPerfis.ListCount - 1
            perfis = perfis & ListaPerfis.List(i) & "; "
        Next
        Report.formulas(11) = "Perfis=" & BL_TrataStringParaBD("" & perfis)
    End If
    
    If EcCodGrupo.Text = "" Then
        Report.formulas(12) = "GrAnalises=" & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(12) = "GrAnalises=" & BL_TrataStringParaBD("" & EcDescrGrupo)
    End If
    
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
 
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    sql = "delete from sl_cr_estatresmicroana where nome_computador = '" & BG_SYS_GetComputerName & "'"
    BG_ExecutaQuery_ADO sql
    
End Sub

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function


Private Sub BtAdicionar_Click()
    If EcCodAnaS = "" Or EcCodFrase = "" Then Exit Sub
    TotalAnaFrase = TotalAnaFrase + 1
    ReDim Preserve AnaFrase(TotalAnaFrase)
    AnaFrase(TotalAnaFrase).seqRealiza = -1
    AnaFrase(TotalAnaFrase).codAnaS = EcCodAnaS
    AnaFrase(TotalAnaFrase).DescrAnaS = EcDescrAnaS
    AnaFrase(TotalAnaFrase).CodFrase = EcCodFrase
    AnaFrase(TotalAnaFrase).tipo = BL_HandleNull(BL_SelCodigo("SL_ANA_S", "t_result", "cod_ana_s", EcCodAnaS), 0)
    AnaFrase(TotalAnaFrase).cod_antib = BL_HandleNull(EcCodAntib)
    AnaFrase(TotalAnaFrase).Cod_t_sensib = BG_DaComboSel(CbSensib)
    FGAna.TextMatrix(FGAna.rows - 1, 0) = EcDescrAnaS
    FGAna.TextMatrix(FGAna.rows - 1, 1) = EcDescrFrase
    FGAna.AddItem ""
    EcCodAnaS = ""
    EcCodFrase = ""
    EcDescrAnaS = ""
    EcDescrFrase = ""
    EcCodAntib.Text = ""
    EcDescrAntib.Text = ""
    CbSensib.ListIndex = mediComboValorNull
    
End Sub

Private Sub BtPesquisaPerfis_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    

    ChavesPesq(1) = "seq_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    
    If ListaGrPerfis.ListCount > 0 Then
        CFrom = "sl_perfis, sl_gr_perfis "
    Else
        CFrom = "sl_perfis "
    End If
    If ListaProdutos.ListCount > 0 Then
        CFrom = CFrom & ", sl_produto "
    End If
    
    CWhere = " flg_activo = 1 "
    If ListaProdutos.ListCount > 0 Then
        CWhere = CWhere & " AND sl_produto.cod_produto = sl_perfis.cod_produto "
        CWhere = CWhere & " AND sl_produto.seq_produto IN ("
        For i = 0 To ListaProdutos.ListCount - 1
            CWhere = CWhere & ListaProdutos.ItemData(i) & ", "
        Next i
        CWhere = Mid(CWhere, 1, Len(CWhere) - 2) & ") "
    End If
    If ListaGrPerfis.ListCount > 0 Then
        CWhere = CWhere & " AND sl_gr_perfis.cod_gr_perfis = sl_perfis.cod_gr_perfis "
        CWhere = CWhere & " AND sl_gr_perfis.seq_gr_perfis IN ("
        For i = 0 To ListaGrPerfis.ListCount - 1
            CWhere = CWhere & ListaGrPerfis.ItemData(i) & ", "
        Next i
        CWhere = Mid(CWhere, 1, Len(CWhere) - 2) & ") "
    End If
    CampoPesquisa = "descr_perfis"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_perfis ", _
                                                                           " Exames")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaPerfis.ListCount = 0 Then
                ListaPerfis.AddItem BL_SelCodigo("sl_perfis", "descr_perfis", "seq_perfis", resultados(i))
                ListaPerfis.ItemData(0) = resultados(i)
            Else
                ListaPerfis.AddItem BL_SelCodigo("sl_perfis", "descr_perfis", "seq_perfis", resultados(i))
                ListaPerfis.ItemData(ListaPerfis.NewIndex) = resultados(i)
            End If
        Next i
        

    End If
End Sub
Private Sub BtPesquisaGrPerfis_click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_gr_perfis"
    CamposEcran(1) = "cod_gr_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_gr_perfis"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_gr_perfis "
    CWhere = ""
    CampoPesquisa = "descr_gr_perfis"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_gr_perfis ", _
                                                                           " Grupos de Exames")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaGrPerfis.ListCount = 0 Then
                ListaGrPerfis.AddItem BL_SelCodigo("sl_gr_perfis", "descr_gr_perfis", "seq_gr_perfis", resultados(i))
                ListaGrPerfis.ItemData(0) = resultados(i)
            Else
                ListaGrPerfis.AddItem BL_SelCodigo("sl_gr_perfis", "descr_gr_perfis", "seq_gr_perfis", resultados(i))
                ListaGrPerfis.ItemData(ListaGrPerfis.NewIndex) = resultados(i)
            End If
        Next i
        

    End If
End Sub






Private Sub BtPesquisaProdutos_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_produto "
    CWhere = ""
    CampoPesquisa = "descr_produto"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_produto ", _
                                                                           " Produtos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaProdutos.ListCount = 0 Then
                ListaProdutos.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                ListaProdutos.ItemData(0) = resultados(i)
            Else
                ListaProdutos.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                ListaProdutos.ItemData(ListaProdutos.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub


Private Sub BtReportLista_Click()
    'MsgBox "Queria...mas s� amanh�!! " & vbCrLf & "Para j� tem que usar o velho bot�o..."
    Call Preenche_Estatistica
    
End Sub

      
Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub






Private Sub FgAna_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    If KeyCode = 46 Then ' DELETE
        If FGAna.row > 0 And FGAna.row < (FGAna.rows - 1) And FGAna.TextMatrix(FGAna.row, 0) <> "" Then
            For i = FGAna.row To FGAna.rows - 2
                If i < TotalAnaFrase Then
                    AnaFrase(i).codAnaS = AnaFrase(i + 1).codAnaS
                    AnaFrase(i).CodFrase = AnaFrase(i + 1).CodFrase
                    AnaFrase(i).DescrAnaS = AnaFrase(i + 1).DescrAnaS
                    AnaFrase(i).seqRealiza = AnaFrase(i + 1).seqRealiza
                    AnaFrase(i).tipo = AnaFrase(i + 1).tipo
                End If
            Next
            TotalAnaFrase = TotalAnaFrase - 1
            FGAna.RemoveItem (FGAna.row)
        End If
    End If
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Estat�stica de Resultados da Microbiologia"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 7590
    Me.Height = 8820 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtIni
    TotalAnaFrase = 0
    ReDim AnaFrase(0)
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de Microrganismos")
    
    Set FormEstatMicrorg = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    CbSituacao.ListIndex = mediComboValorNull
    EcDescrSexo.ListIndex = mediComboValorNull
    'CbGrupo.ListIndex = mediComboValorNull
    ListaProven.Clear
    ListaProdutos.Clear
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    Flg_OrderProd.value = 0
    Flg_OrderProven.value = 0
    EcCodGrupo.Text = ""
    EcDescrGrupo.Text = ""
    ListaEspecif.Clear
    ListaPerfis.Clear
    ListaGrPerfis.Clear
    EcCodAnaS = ""
    EcCodFrase = ""
    EcDescrAnaS = ""
    EcDescrFrase = ""
    EcDescrAntib = ""
    CbSensib.ListIndex = mediComboValorNull
    TotalAnaFrase = 0
    ReDim AnaFrase(0)
    LimpaFGAna
    
    ReDim Requis(0)
    totalRequis = 0
    Option1(0).value = True
End Sub

Sub DefTipoCampos()


    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    EcCodGrupo.Tag = adVarChar
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
'    EcNumFolhaTrab.MaxLength = 10
    With FGAna
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = (FGAna.Width / 4) - 50
        .Col = 0
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 0) = "An�lise"
        .ColWidth(1) = (FGAna.Width / 4) * 3 - 50
        .Col = 1
        .CellAlignment = flexAlignLeftCenter
        .TextMatrix(0, 1) = "Frase"
        .row = 1
        .Col = 0
    End With
    EcCodFrase.Tag = adVarChar
    EcCodAnaS.Tag = adVarChar
    EcCodAntib.Enabled = False
    BtPesquisaAntib.Enabled = False
    CbSensib.Enabled = False
    Option1(0).value = True
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    BG_PreencheComboBD_ADO "sl_tbf_t_sensib", "cod_t_sensib", "descr_t_sensib", CbSensib

    EcCodGrupo.Text = gCodGrupoMicrobiologia
    EcCodGrupo_Validate False
    
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub


Sub PreencheTabelaTemporaria()
    Dim sql As String
    Dim i As Integer
    Dim j As Integer
    Dim SqlIns As String
    Dim SqlSel As String
    Dim sqlFrom As String
    Dim sqlWhere As String
    Dim SqlC As String
    Dim sqlOrder As String
    Dim rs As New ADODB.recordset
    Dim flg_frase As Boolean
    Dim flg_micro As Boolean
    
    On Error GoTo TrataErro
    flg_frase = False
    flg_micro = False
    
    sql = "delete from sl_cr_estatresmicroANA where nome_computador = '" & BG_SYS_GetComputerName & "'"
    BG_ExecutaQuery_ADO sql
    
    ReDim Requis(0)
    totalRequis = 0
    
    SeleccionaRequisicoes
    'VerificaCadaRequisicao
    Exit Sub
TrataErro:
    BG_LogFile_Erros " FormEstatResMicroAna -> PreencheTabelaTemporaria "
    BG_LogFile_Erros Err.Number & " - " & Err.Description
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaGrupo_Click()

    FormPesquisaRapida.InitPesquisaRapida "sl_gr_ana", _
                        "descr_gr_ana", "seq_gr_ana", _
                         EcPesqRapGrupo
End Sub


Private Sub Listaperfis_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaPerfis.ListCount > 0 Then     'Delete
        ListaPerfis.RemoveItem (ListaPerfis.ListIndex)
    End If
End Sub
Private Sub ListaGrPerfis_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaGrPerfis.ListCount > 0 Then     'Delete
        ListaGrPerfis.RemoveItem (ListaGrPerfis.ListIndex)
    End If
End Sub

Private Sub BtPesquisaProven_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven "
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Provini�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaProven.ListCount = 0 Then
                ListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                ListaProven.ItemData(0) = resultados(i)
            Else
                ListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                ListaProven.ItemData(ListaProven.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub

Private Sub BtPesquisaEspecif_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_especif"
    CamposEcran(1) = "cod_especif"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_especif"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_especif "
    CWhere = ""
    CampoPesquisa = "descr_especif"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_especif ", _
                                                                           " Especifica��es")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaEspecif.ListCount = 0 Then
                ListaEspecif.AddItem BL_SelCodigo("sl_especif", "descr_especif", "seq_especif", resultados(i))
                ListaEspecif.ItemData(0) = resultados(i)
            Else
                ListaEspecif.AddItem BL_SelCodigo("sl_especif", "descr_especif", "seq_especif", resultados(i))
                ListaEspecif.ItemData(ListaEspecif.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub


Private Sub ListaProven_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaProven.ListCount > 0 Then     'Delete
        ListaProven.RemoveItem (ListaProven.ListIndex)
    End If
End Sub
Private Sub Listaespecif_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaEspecif.ListCount > 0 Then     'Delete
        ListaEspecif.RemoveItem (ListaEspecif.ListIndex)
    End If
End Sub
Private Sub ListaProdutos_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaProdutos.ListCount > 0 Then     'Delete
        ListaProdutos.RemoveItem (ListaProdutos.ListIndex)
    End If
End Sub
Private Sub EcCodGrupo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    'Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    'If Cancel = True Then
    '    Exit Sub
    'End If
    
    If Trim(EcCodGrupo.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_gr_ana, descr_gr_ana FROM sl_gr_ana WHERE upper(cod_gr_ana)= " & BL_TrataStringParaBD(UCase(EcCodGrupo.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodGrupo.Text = "" & RsDescrGrupo!cod_gr_ana
            EcDescrGrupo.Text = "" & RsDescrGrupo!descr_gr_ana
        Else
            Cancel = True
            EcDescrGrupo.Text = ""
            BG_Mensagem mediMsgBox, "O grupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrGrupo.Text = ""
    End If

End Sub





Private Sub BtPesquisaAnaS_Click()
    Dim i As Integer
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim CWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_ana_s"
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    If ListaPerfis.ListCount > 0 Then
        ClausulaFrom = "sl_ana_s, sl_ana_perfis,sl_perfis, sl_membro "
    ElseIf ListaGrPerfis.ListCount > 0 Then
        ClausulaFrom = "sl_ana_s, sl_ana_perfis,sl_perfis,sl_gr_perfis, sl_membro "
    Else
    ClausulaFrom = "sl_ana_s"
    End If
    CampoPesquisa1 = "descr_ana_s"
    
    If ListaPerfis.ListCount > 0 Then
        CWhere = " sl_membro.cod_ana_c = sl_ana_perfis.cod_analise AND sl_membro.cod_membro = sl_ana_s.cod_ana_s "
        CWhere = CWhere & " AND sl_ana_perfis.cod_perfis = sl_perfis.cod_perfis "
        CWhere = CWhere & " AND sl_perfis.seq_perfis IN ("
        For i = 0 To ListaPerfis.ListCount - 1
            CWhere = CWhere & ListaPerfis.ItemData(i) & ", "
        Next i
        CWhere = Mid(CWhere, 1, Len(CWhere) - 2) & ") "
    ElseIf ListaGrPerfis.ListCount > 0 Then
        CWhere = " sl_membro.cod_ana_c = sl_ana_perfis.cod_analise AND sl_membro.cod_membro = sl_ana_s.cod_ana_s "
        CWhere = CWhere & " AND sl_ana_perfis.cod_perfis = sl_perfis.cod_perfis "
        CWhere = CWhere & " AND sl_perfis.cod_gr_perfis = sl_gr_perfis.cod_gr_perfis "
        CWhere = CWhere & " AND sl_gr_perfis.seq_gr_perfis IN ("
        For i = 0 To ListaGrPerfis.ListCount - 1
            CWhere = CWhere & ListaGrPerfis.ItemData(i) & ", "
        Next i
        CWhere = Mid(CWhere, 1, Len(CWhere) - 2) & ") GROUP BY cod_ana_s, descr_ana_s"
    ElseIf EcCodGrupo <> "" Then
        CWhere = " (flg_invisivel IS NULL OR flg_invisivel ='0') AND sl_ana_s.gr_ana = " & BL_TrataStringParaBD(EcCodGrupo)
    End If
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, CWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar An�lises")
    
    mensagem = "N�o foi encontrada nenhuma An�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAnaS.Text = resultados(1)
            EcDescrAnaS.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub



Private Sub BtPesquisaFrase_Click()
    Dim i As Integer
    Dim tipo As String
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim CWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    tipo = BL_HandleNull(BL_SelCodigo("SL_ANA_S", "t_result", "cod_ana_s", EcCodAnaS), 0)
    
    'Defini��o dos campos a retornar
    If tipo = gT_Frase Then
        ChavesPesq(1) = "distinct sl_dicionario.cod_frase"
        CamposEcran(1) = "distinct sl_dicionario.cod_frase"
    ElseIf tipo = gT_Microrganismo Then
        ChavesPesq(1) = "cod_microrg"
        CamposEcran(1) = "cod_microrg"
    End If
    Tamanhos(1) = 2000
    
    EcCodAntib.Enabled = False
    BtPesquisaAntib.Enabled = False
    CbSensib.Enabled = False
    EcCodAntib = ""
    CbSensib.ListIndex = mediComboValorNull
    
    If tipo = gT_Frase Then
        ChavesPesq(2) = "descr_frase"
        CamposEcran(2) = "descr_frase"
    ElseIf tipo = gT_Microrganismo Then
        ChavesPesq(2) = "descr_microrg"
        CamposEcran(2) = "descr_microrg"
    End If
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    If tipo = gT_Frase Then
        ClausulaFrom = "sl_dicionario"
        
        If ListaPerfis.ListCount > 0 Then
            ClausulaFrom = ClausulaFrom & ", sl_ana_frase, sl_perfis"
        End If
        
        If EcCodAnaS <> "" And ListaPerfis.ListCount = 0 And ListaGrPerfis.ListCount = 0 Then
            ClausulaFrom = ClausulaFrom & ", sl_ana_frase"
        End If
        CampoPesquisa1 = "descr_frase"
        CWhere = ""
        If ListaPerfis.ListCount > 0 Then
            CWhere = " sl_ana_frase.cod_frase = sl_dicionario.cod_frase AND sl_ana_frase.cod_perfil = sl_perfis.cod_perfis "
            CWhere = CWhere & " AND sl_perfis.seq_perfis IN ("
            For i = 0 To ListaPerfis.ListCount - 1
                CWhere = CWhere & ListaPerfis.ItemData(i) & ", "
            Next i
            CWhere = Mid(CWhere, 1, Len(CWhere) - 2) & ") "
        End If
        If EcCodAnaS <> "" Then
            If CWhere <> "" Then
                CWhere = CWhere & " AND sl_ana_frase.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS)
            Else
                CWhere = CWhere & "  sl_ana_frase.cod_frase = sl_dicionario.cod_frase AND sl_ana_frase.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS)
            End If
        End If
    ElseIf tipo = gT_Microrganismo Then
        ClausulaFrom = "sl_microrg"
        CampoPesquisa1 = "descr_microrg"
    End If
    If tipo = gT_Frase Then
        PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, CWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Frases")
        mensagem = "N�o foi encontrada nenhuma Frase."
    ElseIf tipo = gT_Microrganismo Then
        PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, CWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Microrganismos")
        mensagem = "N�o foi encontrada nenhum Microrganismo."
    End If
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodFrase.Text = resultados(1)
            EcDescrFrase.Text = resultados(2)
            If tipo = gT_Microrganismo = True Then
                EcCodAntib.Enabled = True
                BtPesquisaAntib.Enabled = True
                CbSensib.Enabled = True
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, " N�o existem frases associadas a an�lise!"
    End If
    
End Sub
Private Sub BtPesquisaantib_Click()
    Dim i As Integer
    Dim tipo As String
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim CWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    
    ChavesPesq(1) = " sl_antibio.cod_antibio"
    CamposEcran(1) = " sl_antibio.cod_antibio"
    Tamanhos(1) = 2000
    
    
    ChavesPesq(2) = "descr_antibio"
    CamposEcran(2) = "descr_antibio"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = " sl_antibio "
    CampoPesquisa1 = " descr_antibio "
    CWhere = ""
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, CWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Antibioticos")
    mensagem = "N�o foi encontrada nenhum Antibi�tico."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAntib.Text = resultados(1)
            EcDescrAntib.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, " N�o existem Antobioticos!"
    End If
    
End Sub

Private Sub EcCodAnaS_Validate(Cancel As Boolean)
    
    Dim RsDescrAnaS As ADODB.recordset
    EcCodAnaS = UCase(EcCodAnaS)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodAnaS.Text) <> "" Then
        Set RsDescrAnaS = New ADODB.recordset
        
        With RsDescrAnaS
            .Source = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s= " & BL_TrataStringParaBD(UCase(EcCodAnaS.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaS.RecordCount > 0 Then
            EcDescrAnaS.Text = "" & RsDescrAnaS!descr_ana_s
        Else
            Cancel = True
            EcDescrAnaS.Text = ""
            BG_Mensagem mediMsgBox, "A An�lise indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrAnaS.Close
        Set RsDescrAnaS = Nothing
    Else
        EcDescrAnaS.Text = ""
    End If
    
End Sub


Private Sub EcCodAntib_Validate(Cancel As Boolean)
    
    Dim RsDescrAnaS As ADODB.recordset
    EcCodAnaS = UCase(EcCodAnaS)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodAnaS.Text) <> "" Then
        Set RsDescrAnaS = New ADODB.recordset
        
        With RsDescrAnaS
            .Source = "SELECT descr_antibio FROM sl_antibio WHERE cod_antibio = " & BL_TrataStringParaBD(UCase(EcCodAntib.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaS.RecordCount > 0 Then
            EcDescrAntib.Text = "" & RsDescrAnaS!descr_antibio
        Else
            Cancel = True
            EcDescrAntib.Text = ""
            BG_Mensagem mediMsgBox, "O Antibi�tico indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrAnaS.Close
        Set RsDescrAnaS = Nothing
    Else
        EcDescrAntib.Text = ""
    End If
    
End Sub


Private Sub EcCodfrase_Validate(Cancel As Boolean)
    Dim sSql As String
    Dim RsDescrFrase As ADODB.recordset
    Dim flg_mostraAntib As Boolean
    
    If EcCodAnaS = "" Then
        Cancel = True
        EcDescrFrase.Text = ""
        EcCodFrase = ""
        BG_Mensagem mediMsgBox, "Tem de indicar uma an�lise!", vbOKOnly + vbExclamation, App.ProductName
        EcCodAnaS.SetFocus
        Sendkeys ("{HOME}+{END}")
    End If
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    flg_mostraAntib = False
    EcCodAntib.Enabled = False
    BtPesquisaAntib.Enabled = False
    CbSensib.Enabled = False
    EcCodAntib = ""
    CbSensib.ListIndex = mediComboValorNull
    
    If Trim(EcCodFrase.Text) <> "" Then
        If EcCodFrase.Text = "*" Then
            EcDescrFrase = "QUALQUER"
            Exit Sub
        End If
        Set RsDescrFrase = New ADODB.recordset
        If BL_HandleNull(BL_SelCodigo("SL_ANA_S", "t_result", "cod_ana_s", EcCodAnaS), 0) = gT_Frase Then
            sSql = "SELECT descr_frase descricao FROM sl_dicionario WHERE cod_frase= " & BL_TrataStringParaBD(EcCodFrase.Text)
        ElseIf BL_HandleNull(BL_SelCodigo("SL_ANA_S", "t_result", "cod_ana_s", EcCodAnaS), 0) = gT_Microrganismo Then
            sSql = "SELECT descr_microrg descricao FROM sl_microrg WHERE cod_microrg= " & BL_TrataStringParaBD(EcCodFrase.Text)
            flg_mostraAntib = True
        End If
        With RsDescrFrase
            .Source = sSql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrFrase.RecordCount > 0 Then
            If flg_mostraAntib = True Then
                EcCodAntib.Enabled = True
                BtPesquisaAntib.Enabled = True
                CbSensib.Enabled = True
            End If
            EcDescrFrase.Text = "" & RsDescrFrase!descricao
        Else
            Cancel = True
            EcDescrFrase.Text = ""
            BG_Mensagem mediMsgBox, "O C�digo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrFrase.Close
        Set RsDescrFrase = Nothing
    Else
        EcDescrFrase.Text = ""
    End If
    
End Sub
Private Sub EcCodanas_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub
Private Sub EcCodFrase_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub
Sub LimpaFGAna()
    Dim j As Long
    
    
    j = FGAna.rows - 1
    While j > 0
        If j > 1 Then
            FGAna.RemoveItem j
        Else
            FGAna.TextMatrix(j, 0) = ""
            FGAna.TextMatrix(j, 1) = ""
        End If
        
        j = j - 1
    Wend
    
End Sub

Private Function RetornaTotalAnalise(codAnaS As String) As Long
    Dim SqlSel As String
    Dim SqlSel2 As String
    Dim SqlC As String
    Dim i As Integer
    Dim rs As New ADODB.recordset
    
    On Error GoTo TrataErro
    
    SqlSel = " SELECT  count(*) total"
        
    SqlSel = SqlSel & " From sl_requis, sl_realiza, sl_res_frase, sl_dicionario, sl_proven, sl_req_prod, sl_produto, " & tabela_aux & ", sl_ana_s, sl_ana_c, sl_perfis, sl_gr_perfis, sl_gr_ana, sl_especif " & _
                " where sl_requis.n_req = sl_realiza.n_req and " & _
                " sl_requis.seq_utente = " & tabela_aux & ".seq_utente and " & _
                " sl_requis.cod_proven = sl_proven.cod_proven (+) and " & _
                " sl_requis.n_req = sl_req_prod.n_req(+) and " & _
                " sl_req_prod.cod_prod = sl_produto.cod_produto(+) and " & _
                " sl_req_prod.cod_especif = sl_especif.cod_especif (+) AND " & _
                " sl_realiza.seq_realiza = sl_res_frase.seq_realiza and " & _
                " sl_realiza.cod_perfil = sl_perfis.cod_perfis(+) and " & _
                " sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c(+) and " & _
                " sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s(+) and " & _
                " sl_res_frase.cod_frase = sl_dicionario.cod_frase and " & _
                " sl_ana_s.gr_ana = sl_gr_ana.cod_gr_ana and " & _
                " sl_perfis.cod_produto = sl_produto.cod_produto AND " & _
                " sl_perfis.cod_gr_perfis = sl_gr_perfis.cod_gr_perfis(+) AND " & _
                " sl_realiza.cod_ana_s = " & BL_TrataStringParaBD(codAnaS)

    'verifica os campos preenchidos
    
    'se sexo preenchido
    If (EcDescrSexo.ListIndex <> -1) Then
        SqlC = SqlC & " AND " & tabela_aux & ".sexo_ute = " & EcDescrSexo.ListIndex
    End If
        
    'se especificacao preenchida
    If ListaEspecif.ListCount > 0 Then
        SqlC = SqlC & " AND sl_especif.seq_especif IN ( "
        For i = 0 To ListaEspecif.ListCount - 1
            SqlC = SqlC & ListaEspecif.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
        
    ' se tem grupo de analises preenchido
    If (EcCodGrupo <> "") Then
        SqlC = SqlC & " AND sl_perfis.gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.Text)
    End If
    
    'Data preenchida
    SqlC = SqlC & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    
    'Situa��o preenchida?
    If (CbSituacao.ListIndex <> -1) Then
        SqlC = SqlC & " AND sl_requis.T_sit= " & CbSituacao.ListIndex
    End If
    
    
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    If ListaProven.ListCount > 0 Then
        SqlC = SqlC & " AND sl_proven.seq_proven IN ("
        For i = 0 To ListaProven.ListCount - 1
            SqlC = SqlC & ListaProven.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    'C�digo do produto preenchido?
    If ListaProdutos.ListCount > 0 Then
        SqlC = SqlC & " AND sl_produto.seq_produto IN ("
        For i = 0 To ListaProdutos.ListCount - 1
           SqlC = SqlC & ListaProdutos.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
        
    
    If ListaPerfis.ListCount > 0 Then
        SqlC = SqlC & " AND sl_perfis.seq_perfis IN ("
        For i = 0 To ListaPerfis.ListCount - 1
            SqlC = SqlC & ListaPerfis.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    If ListaGrPerfis.ListCount > 0 Then
        SqlC = SqlC & " AND sl_gr_perfis.seq_gr_perfis IN ("
        For i = 0 To ListaGrPerfis.ListCount - 1
            SqlC = SqlC & ListaGrPerfis.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.LockType = adLockReadOnly
    rs.Open SqlSel & SqlC, gConexao
    
    RetornaTotalAnalise = rs.RecordCount
    
    rs.Close
    Set rs = Nothing
    Exit Function
TrataErro:
    BG_LogFile_Erros " FormEstatResMicroAna -> RetornaTotalAnalise "
    BG_LogFile_Erros Err.Number & " - " & Err.Description
    RetornaTotalAnalise = -1
    Exit Function
    Resume Next
End Function


' --------------------------------------------------------------------------------------------------------

' SELECCIONA REQUISICOES QUE OBEDECEM AS CONDICOES INDICADAS (SEXO,DATAS, PROVEN,PRODUTOS,GR_EXAMES, ETC.)

' --------------------------------------------------------------------------------------------------------
Private Sub SeleccionaRequisicoes()
    Dim SqlSel As String
    Dim sqlFrom As String
    Dim sqlWhere As String
    Dim SqlC As String
    Dim sqlOrder As String
    Dim sSql As String
    Dim i As Integer
    Dim rs As New ADODB.recordset
        
    ' CONSTROI CLAUSULA SELECT
    ' ---------------------------------------------------------------------------------------------------
    SqlSel = " SELECT "
    SqlSel = SqlSel & " DESCR_PRODUTO,DESCR_PROVEN,"
    SqlSel = SqlSel & " '" & BG_SYS_GetComputerName & "', sl_requis.n_req, realiza.cod_perfil, descr_perfis, realiza.cod_ana_c,descr_ana_c, realiza.cod_ana_s,descr_ana_s "
    SqlSel = SqlSel & ", sl_gr_perfis.cod_gr_perfis, sl_gr_perfis.descr_gr_perfis, " & tabela_aux & ".nome_ute, " & tabela_aux & ".utente "
    SqlSel = SqlSel & ", realiza.seq_realiza,realiza.ord_ana, realiza.cod_agrup,realiza.ord_ana_perf,realiza.ord_ana_compl "
    
    ' CONSTROI CLAUSULA FROM
    ' ---------------------------------------------------------------------------------------------------
    sqlFrom = " FROM " & tabela_aux & ", sl_requis LEFT OUTER JOIN sl_proven ON sl_requis.cod_proven = sl_proven.cod_proven, "
    sqlFrom = sqlFrom & "  sl_realiza realiza LEFT OUTER JOIN sl_ana_c ON realiza.cod_ana_c = sl_ana_c.cod_ana_c  "
    sqlFrom = sqlFrom & ", sl_produto, SL_REQ_PROD LEFT OUTER JOIN sl_especif ON  sl_req_prod.cod_especif = sl_especif.cod_especif "
    sqlFrom = sqlFrom & ", sl_ana_s, sl_perfis LEFT OUTER JOIN sl_gr_perfis ON sl_perfis.cod_gr_perfis = sl_gr_perfis.cod_gr_perfis , sl_gr_ana                         "
    ' CONSTROI CLAUSULA WHERE
    ' ---------------------------------------------------------------------------------------------------
    If Option1(1).value = True Then
        sqlWhere = " WHERE sl_realiza.dt_val BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    Else
        sqlWhere = " WHERE sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    End If
    sqlWhere = sqlWhere & " AND sl_requis.seq_utente = " & tabela_aux & ".seq_utente "
    sqlWhere = sqlWhere & " AND realiza.n_req = sl_requis.n_req "
    sqlWhere = sqlWhere & " AND sl_requis.n_req = sl_req_prod.n_req "
    sqlWhere = sqlWhere & " AND sl_req_prod.cod_prod = sl_produto.cod_produto "
    sqlWhere = sqlWhere & " AND sl_ana_s.gr_ana = sl_gr_ana.cod_gr_ana  "
    sqlWhere = sqlWhere & " AND sl_perfis.cod_produto = sl_produto.cod_produto  "
    sqlWhere = sqlWhere & " AND realiza.cod_perfil = sl_perfis.cod_perfis "
    sqlWhere = sqlWhere & " AND realiza.cod_ana_s = sl_ana_s.cod_ana_s "
    sqlWhere = sqlWhere & " AND realiza.cod_ana_s IN( "
    For i = 1 To TotalAnaFrase
        If i = 1 Then
            sqlWhere = sqlWhere & BL_TrataStringParaBD(AnaFrase(i).codAnaS)
        Else
            sqlWhere = sqlWhere & ", " & BL_TrataStringParaBD(AnaFrase(i).codAnaS)
        End If
    Next
    sqlWhere = sqlWhere & ") "
    
    'verifica os campos preenchidos
    SqlC = ""
    'se sexo preenchido
    If (EcDescrSexo.ListIndex <> -1) Then
        SqlC = SqlC & " AND " & tabela_aux & ".sexo_ute = " & EcDescrSexo.ListIndex
    End If
    
    'se especificacao preenchida
    If ListaEspecif.ListCount > 0 Then
        SqlC = SqlC & " AND sl_especif.seq_especif IN ( "
        For i = 0 To ListaEspecif.ListCount - 1
            SqlC = SqlC & ListaEspecif.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    ' se tem grupo de analises preenchido
    If (EcCodGrupo <> "") Then
        SqlC = SqlC & " AND sl_perfis.gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.Text)
    End If
    
    
    
    'Situa��o preenchida?
    If (CbSituacao.ListIndex <> -1) Then
        SqlC = SqlC & " AND sl_requis.T_sit= " & CbSituacao.ListIndex
    End If
    
    
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    If ListaProven.ListCount > 0 Then
        SqlC = SqlC & " AND sl_proven.seq_proven IN ("
        For i = 0 To ListaProven.ListCount - 1
            SqlC = SqlC & ListaProven.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    'C�digo do produto preenchido?
    If ListaProdutos.ListCount > 0 Then
        SqlC = SqlC & " AND sl_produto.seq_produto IN ("
        For i = 0 To ListaProdutos.ListCount - 1
           SqlC = SqlC & ListaProdutos.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    

    If ListaPerfis.ListCount > 0 Then
        SqlC = SqlC & " AND sl_perfis.seq_perfis IN ("
        For i = 0 To ListaPerfis.ListCount - 1
            SqlC = SqlC & ListaPerfis.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    If ListaGrPerfis.ListCount > 0 Then
        SqlC = SqlC & " AND sl_gr_perfis.seq_gr_perfis IN ("
        For i = 0 To ListaGrPerfis.ListCount - 1
            SqlC = SqlC & ListaGrPerfis.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    ' CONSTROI ORDER BY
    ' ---------------------------------------------------------------------------------------------------
    sqlOrder = " ORDER BY  sl_requis.n_req, realiza.ord_ana, realiza.cod_agrup, realiza.cod_perfil, "
    sqlOrder = sqlOrder & " realiza.ord_ana_perf, realiza.cod_ana_c, realiza.ord_ana_compl, realiza.cod_ana_s "
    
    sSql = SqlSel & sqlFrom & sqlWhere & SqlC & sqlOrder
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.LockType = adLockReadOnly
    rs.Open sSql, gConexao
    If rs.RecordCount > 0 Then
        While Not rs.EOF
            AcrescentaRequis rs!n_req, rs!seq_realiza, rs!cod_ana_s, rs!nome_ute, rs!Utente, BL_HandleNull(rs!descr_proven, ""), BL_HandleNull(rs!descr_produto, "")
            rs.MoveNext
        Wend
        VerificaCadaRequisicao
    End If
    rs.Close
    Set rs = Nothing
End Sub


Private Sub AcrescentaRequis(n_req As String, seq_realiza As String, cod_ana_s As String, nome As String, _
                             Utente As String, descr_proven As String, descr_produto As String)
    Dim i As Long
    Dim j As Long
    Dim ReqExiste As Boolean
    On Error GoTo TrataErro
    
    ReqExiste = False
    'VERIFICA SE REQUISICAO JA ESTA NA ESTRUTURA
    For i = 1 To totalRequis
        If Requis(i).NReq = n_req Then
            ReqExiste = True
            Exit For
        End If
    Next
    
    If ReqExiste = False Then
        totalRequis = totalRequis + 1
        ReDim Preserve Requis(totalRequis)
        Requis(totalRequis).analises = AnaFrase
        Requis(totalRequis).totalAnalises = TotalAnaFrase
        i = totalRequis
        Requis(totalRequis).analisesExistentes = 0
    End If
    
    Requis(i).NReq = n_req
    Requis(i).nome = nome
    Requis(i).Utente = Utente
    Requis(i).descr_produto = descr_produto
    Requis(i).descr_proven = descr_proven
    For j = 1 To Requis(i).totalAnalises
        If cod_ana_s = Requis(i).analises(j).codAnaS Then
            If Requis(i).analises(j).seqRealiza = -1 Then
                Requis(i).analises(j).seqRealiza = CLng(seq_realiza)
                Requis(i).analisesExistentes = Requis(i).analisesExistentes + 1
                Exit For
            End If
        End If
    Next
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

Private Sub VerificaCadaRequisicao()
    Dim i As Integer
    Dim j As Integer
    Dim reqValida As Boolean
    Dim sSql As String
    Dim erroTmp As Long
    
    For i = 1 To totalRequis
        If Requis(i).analisesExistentes = TotalAnaFrase Then
            reqValida = True
            For j = 1 To TotalAnaFrase
                If VerificaResultadoAnalise(Requis(i).analises(j).CodFrase, Requis(i).analises(j).tipo, Requis(i).analises(j).seqRealiza, _
                                            Requis(i).analises(j).cod_antib, Requis(i).analises(j).Cod_t_sensib) = False Then
                    reqValida = False
                    Exit For
                End If
            Next
            If reqValida = True Then
                BG_BeginTransaction
                erroTmp = 0
                For j = 1 To TotalAnaFrase
                    sSql = "INSERT INTO sl_cr_estatresmicroana(nome_computador, descr_proven, descr_produto,n_req,cod_res,descr_res,cod_ana_s,descr_Ana_s,nome_ute,utente) VALUES("
                    sSql = sSql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ","
                    sSql = sSql & BL_TrataStringParaBD(Requis(i).descr_proven) & ","
                    sSql = sSql & BL_TrataStringParaBD(Requis(i).descr_produto) & ","
                    sSql = sSql & Requis(i).NReq & ","
                    sSql = sSql & BL_TrataStringParaBD(Requis(i).analises(j).CodFrase) & ","
                    If Requis(i).analises(j).tipo = gT_Microrganismo Then
                        sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("sl_microrg", "descr_microrg", "cod_microrg", Requis(i).analises(j).CodFrase)) & ","
                    ElseIf Requis(i).analises(j).tipo = gT_Frase Then
                        sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("sl_dicionario", "descr_frase", "cod_frase", Requis(i).analises(j).CodFrase)) & ","
                    End If
                    sSql = sSql & BL_TrataStringParaBD(Requis(i).analises(j).codAnaS) & ","
                    sSql = sSql & BL_TrataStringParaBD(Requis(i).analises(j).DescrAnaS) & ","
                    sSql = sSql & BL_TrataStringParaBD(Requis(i).nome) & ","
                    sSql = sSql & BL_TrataStringParaBD(Requis(i).Utente) & ")"
                    BG_ExecutaQuery_ADO sSql
                    erroTmp = erroTmp + gSQLError
                Next
                If erroTmp <> 0 Then
                    BG_RollbackTransaction
                Else
                    BG_CommitTransaction
                End If
            End If
        End If
    Next
End Sub


Private Function VerificaResultadoAnalise(cod_res As String, tipo As String, seq_realiza As Long, codAntib As String, Cod_t_sensib As Integer) As Boolean
    Dim sSql As String
    Dim rs As New ADODB.recordset
    If tipo = gT_Microrganismo Then
        sSql = "SELECT cod_micro codigo FROM SL_RES_MICRO WHERE seq_realiza = " & seq_realiza
    ElseIf tipo = gT_Frase Then
        sSql = "SELECT cod_frase codigo FROM SL_RES_frase WHERE seq_realiza = " & seq_realiza
    End If
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.LockType = adLockReadOnly
    rs.Open sSql, gConexao
    If rs.RecordCount > 0 Then
        If BL_HandleNull(rs!Codigo, "") = cod_res Or cod_res = "*" Then
            rs.Close
            Set rs = Nothing
            
            If tipo = gT_Microrganismo And codAntib <> "" And UCase(codAntib) <> "NULL" Then
                sSql = "SELECT * FROM sl_res_tsq WHERE seq_realiza = " & seq_realiza & " AND cod_antib = " & BL_TrataStringParaBD(codAntib)
                rs.CursorLocation = adUseServer
                rs.CursorType = adOpenStatic
                rs.LockType = adLockReadOnly
                rs.Open sSql, gConexao
                If rs.RecordCount > 0 Then
                    ' ANTIBIOTICO EXISTE
                    
                    If Cod_t_sensib <> mediComboValorNull Then
                        If BL_HandleNull(rs!res_sensib, "") = UCase(Mid(BL_SelCodigo("SL_TBF_T_SENSIB", "DESCR_T_SENSIB", "COD_T_SENSIB", Cod_t_sensib), 1, 1)) Then
                            ' SENSIBILIDADE E IGUAL A INDICADA
                            VerificaResultadoAnalise = True
                        Else
                            ' SENSIBILIDADE E DIFERENTE
                            VerificaResultadoAnalise = False
                        End If
                    Else
                        ' NAO FOI INDICADO NENHUMA SENSIBILIDADE
                        VerificaResultadoAnalise = True
                    End If
                Else
                    'ANTIBIOTICO INDICADO NAO EXISTE PARA O MICRORGANIMSO EM CAUSA
                    VerificaResultadoAnalise = False
                End If
            Else
                VerificaResultadoAnalise = True
            End If
            
        Else
            VerificaResultadoAnalise = False
        End If
    Else
        rs.Close
        Set rs = Nothing
        VerificaResultadoAnalise = False
    End If
End Function



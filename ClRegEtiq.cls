VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClRegEtiq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

Public NumReq As Variant
Public CodProd As Variant
Public DescrProd As Variant
Public DtPrev As Variant
Public DtChega As Variant
' esta classe serve para guardar os dados em mem�ria

Public Sub PreencheRegisto(ObjControlos, Posicao As Integer)
    ' esta sub preenche um registo no ecra
    ObjControlos.CtlEcNumReq(Posicao) = BL_FormataCampo(NumReq, ObjControlos.CtlEcNumReq(Posicao).Tag)
    ObjControlos.CtlEcCodProd(Posicao) = BL_FormataCampo(CodProd, ObjControlos.CtlEcCodProd(Posicao).Tag)
    ObjControlos.CtlEcDescrProd(Posicao) = BL_FormataCampo(DescrProd, ObjControlos.CtlEcDescrProd(Posicao).Tag)
    ObjControlos.CtlEcDtPrev(Posicao) = BL_FormataCampo(DtPrev, ObjControlos.CtlEcDtPrev(Posicao).Tag)
    ObjControlos.CtlEcDtChega(Posicao) = BL_FormataCampo(DtChega, ObjControlos.CtlEcDtChega(Posicao).Tag)
End Sub
Public Sub AlteraRegisto(ObjControlos, Posicao As Integer)
    ' esta sub altera um registo
    NumReq = BL_RetiraFormato(ObjControlos.CtlEcNumReq(Posicao), ObjControlos.CtlEcNumReq(Posicao).Tag)
    CodProd = BL_RetiraFormato(ObjControlos.CtlEcCodProd(Posicao), ObjControlos.CtlEcCodProd(Posicao).Tag)
    DescrProd = BL_RetiraFormato(ObjControlos.CtlEcDescrProd(Posicao), ObjControlos.CtlEcDescrProd(Posicao).Tag)
    DtPrev = BL_RetiraFormato(ObjControlos.CtlEcDtPrev(Posicao), ObjControlos.CtlEcDtPrev(Posicao).Tag)
    DtChega = BL_RetiraFormato(ObjControlos.CtlEcDtChega(Posicao), ObjControlos.CtlEcDtChega(Posicao).Tag)
End Sub
Public Sub AlteraValores(ObjRegisto As Object)
    NumReq = ObjRegisto.NumReq
    CodProd = ObjRegisto.CodProd
    DescrProd = ObjRegisto.DescrProd
    DtPrev = ObjRegisto.DtPrev
    DtChega = ObjRegisto.DtChega
End Sub

Public Function CriaInstanciaClasse(ObjControlos, Indice) As Object
    Dim ObjReg As New ClRegEtiq
    ObjReg.AlteraRegisto ObjControlos, (Indice)
    Set CriaInstanciaClasse = ObjReg
End Function




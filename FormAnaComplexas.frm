VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormAnaComplexas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionAnaComplexas"
   ClientHeight    =   11100
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   38745
   Icon            =   "FormAnaComplexas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   11100
   ScaleWidth      =   38745
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcLinhaPreco 
      Height          =   285
      Left            =   8040
      TabIndex        =   89
      Top             =   8640
      Width           =   615
   End
   Begin VB.TextBox EcColunaPreco 
      Height          =   285
      Left            =   8040
      TabIndex        =   88
      Top             =   9000
      Width           =   615
   End
   Begin VB.Frame Frame7 
      Height          =   975
      Left            =   0
      TabIndex        =   32
      Top             =   5520
      Width           =   12855
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   3060
         TabIndex        =   33
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   3060
         TabIndex        =   34
         Top             =   525
         Width           =   4335
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   38
         Top             =   240
         Width           =   675
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   37
         Top             =   525
         Width           =   705
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   36
         Top             =   240
         Width           =   1935
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   35
         Top             =   525
         Width           =   2055
      End
   End
   Begin VB.ComboBox EcLocal 
      Height          =   315
      Left            =   7200
      TabIndex        =   59
      Text            =   "Combo1"
      Top             =   9600
      Width           =   2535
   End
   Begin VB.CommandButton BtPesqRap2 
      Height          =   375
      Left            =   9600
      Picture         =   "FormAnaComplexas.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   50
      ToolTipText     =   " Pesquisa R�pida de  An�lises Complexas "
      Top             =   120
      Width           =   375
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   6960
      TabIndex        =   49
      Top             =   8040
      Width           =   975
   End
   Begin VB.TextBox EcOrdem 
      Height          =   285
      Left            =   5760
      TabIndex        =   47
      Top             =   7800
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.ListBox ListaAF 
      Height          =   645
      Left            =   8880
      TabIndex        =   46
      Top             =   7800
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   6720
      TabIndex        =   22
      Top             =   7440
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3960
      TabIndex        =   26
      Top             =   7440
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   6720
      TabIndex        =   25
      Top             =   7080
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3960
      TabIndex        =   27
      Top             =   7080
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   1800
      TabIndex        =   28
      Top             =   7080
      Visible         =   0   'False
      Width           =   615
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4575
      Left            =   120
      TabIndex        =   23
      Top             =   840
      Width           =   12825
      _ExtentX        =   22622
      _ExtentY        =   8070
      _Version        =   393216
      Tabs            =   4
      Tab             =   3
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "An�lises"
      TabPicture(0)   =   "FormAnaComplexas.frx":0596
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Label55(1)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label22"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame3"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "BtSinonimos"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "EcAnaLocaisExec"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "EcAnaLocais"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "FrameSinon"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).ControlCount=   7
      TabCaption(1)   =   "Membros"
      TabPicture(1)   =   "FormAnaComplexas.frx":05B2
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "BtCima"
      Tab(1).Control(1)=   "BtBaixo"
      Tab(1).Control(2)=   "BtRetira"
      Tab(1).Control(3)=   "BtInsere"
      Tab(1).Control(4)=   "Frame1"
      Tab(1).Control(5)=   "EcPesquisa"
      Tab(1).Control(6)=   "ListaMembros"
      Tab(1).Control(7)=   "ListaAnaSimples"
      Tab(1).Control(8)=   "ListaFrases"
      Tab(1).Control(9)=   "Label4"
      Tab(1).ControlCount=   10
      TabCaption(2)   =   "Factura��o"
      TabPicture(2)   =   "FormAnaComplexas.frx":05CE
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "BtAdicionarRubrica"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "FrameRubrica"
      Tab(2).Control(2)=   "EcQtd"
      Tab(2).Control(3)=   "EcRubrSeqAna"
      Tab(2).Control(4)=   "EcCodRubrica"
      Tab(2).Control(5)=   "EcDescrRubrica"
      Tab(2).Control(6)=   "BtPesquisaRubrica"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "BtGravarAnaFact"
      Tab(2).Control(8)=   "BtCopiaPrecario"
      Tab(2).Control(9)=   "EcCodAnaFacturar"
      Tab(2).Control(10)=   "CkFlgfacturarAna"
      Tab(2).Control(11)=   "CkFlgRegra"
      Tab(2).Control(12)=   "EcCodAnaRegra"
      Tab(2).Control(13)=   "CkContaMembros"
      Tab(2).Control(14)=   "EcAuxPreco"
      Tab(2).Control(15)=   "FgPrecos"
      Tab(2).Control(16)=   "Label100"
      Tab(2).Control(17)=   "Label1(46)"
      Tab(2).ControlCount=   18
      TabCaption(3)   =   "Dados Complementares"
      TabPicture(3)   =   "FormAnaComplexas.frx":05EA
      Tab(3).ControlEnabled=   -1  'True
      Tab(3).Control(0)=   "Label1(54)"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "Label57(1)"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).Control(2)=   "Label21(2)"
      Tab(3).Control(2).Enabled=   0   'False
      Tab(3).Control(3)=   "Label21(1)"
      Tab(3).Control(3).Enabled=   0   'False
      Tab(3).Control(4)=   "Label15"
      Tab(3).Control(4).Enabled=   0   'False
      Tab(3).Control(5)=   "Label21(0)"
      Tab(3).Control(5).Enabled=   0   'False
      Tab(3).Control(6)=   "Label1(58)"
      Tab(3).Control(6).Enabled=   0   'False
      Tab(3).Control(7)=   "Label1(40)"
      Tab(3).Control(7).Enabled=   0   'False
      Tab(3).Control(8)=   "Label1(59)"
      Tab(3).Control(8).Enabled=   0   'False
      Tab(3).Control(9)=   "Label1(1)"
      Tab(3).Control(9).Enabled=   0   'False
      Tab(3).Control(10)=   "CkGrafico"
      Tab(3).Control(10).Enabled=   0   'False
      Tab(3).Control(11)=   "EcListaFolhasTrab"
      Tab(3).Control(11).Enabled=   0   'False
      Tab(3).Control(12)=   "CbTipoGrafico"
      Tab(3).Control(12).Enabled=   0   'False
      Tab(3).Control(13)=   "CbSexo"
      Tab(3).Control(13).Enabled=   0   'False
      Tab(3).Control(14)=   "EcPrazoVal"
      Tab(3).Control(14).Enabled=   0   'False
      Tab(3).Control(15)=   "EcOrdemARS"
      Tab(3).Control(15).Enabled=   0   'False
      Tab(3).Control(16)=   "EcCodEstatistica"
      Tab(3).Control(16).Enabled=   0   'False
      Tab(3).Control(17)=   "EcPeso"
      Tab(3).Control(17).Enabled=   0   'False
      Tab(3).Control(18)=   "CkAbrEtiq"
      Tab(3).Control(18).Enabled=   0   'False
      Tab(3).Control(19)=   "CkFacturar"
      Tab(3).Control(19).Enabled=   0   'False
      Tab(3).Control(20)=   "CkEtiqueta"
      Tab(3).Control(20).Enabled=   0   'False
      Tab(3).Control(21)=   "EcFlagMarcacao"
      Tab(3).Control(21).Enabled=   0   'False
      Tab(3).Control(22)=   "EcInformacao"
      Tab(3).Control(22).Enabled=   0   'False
      Tab(3).Control(23)=   "EcPrazoConc"
      Tab(3).Control(23).Enabled=   0   'False
      Tab(3).Control(24)=   "EcQuestoes"
      Tab(3).Control(24).Enabled=   0   'False
      Tab(3).Control(25)=   "CkInibeDescricao"
      Tab(3).Control(25).Enabled=   0   'False
      Tab(3).Control(26)=   "EcDescrEtiq"
      Tab(3).Control(26).Enabled=   0   'False
      Tab(3).Control(27)=   "CkObrigaInfClin"
      Tab(3).Control(27).Enabled=   0   'False
      Tab(3).Control(28)=   "CkObrigaTerap"
      Tab(3).Control(28).Enabled=   0   'False
      Tab(3).Control(29)=   "CkAcreditacao"
      Tab(3).Control(29).Enabled=   0   'False
      Tab(3).ControlCount=   30
      Begin VB.CheckBox CkAcreditacao 
         Caption         =   "An�lise Acreditada"
         Height          =   255
         Left            =   4440
         TabIndex        =   152
         Top             =   2700
         Width           =   2415
      End
      Begin VB.CheckBox CkObrigaTerap 
         Caption         =   "ObrigaTerap�utica "
         Height          =   255
         Left            =   4440
         TabIndex        =   151
         ToolTipText     =   "An�lise sujeita a contagem de marcadores para fatura��o"
         Top             =   2400
         Width           =   3615
      End
      Begin VB.CheckBox CkObrigaInfClin 
         Caption         =   "Obriga Inf. Clinica"
         Height          =   255
         Left            =   4440
         TabIndex        =   150
         ToolTipText     =   "An�lise sujeita a contagem de marcadores para fatura��o"
         Top             =   2100
         Width           =   3615
      End
      Begin VB.TextBox EcDescrEtiq 
         Height          =   285
         Left            =   1800
         TabIndex        =   148
         Top             =   3480
         Width           =   735
      End
      Begin VB.CommandButton BtAdicionarRubrica 
         Height          =   315
         Left            =   -74760
         Picture         =   "FormAnaComplexas.frx":0606
         Style           =   1  'Graphical
         TabIndex        =   147
         TabStop         =   0   'False
         ToolTipText     =   "Adicionar R�brica ao FACTUS"
         Top             =   1080
         Width           =   375
      End
      Begin VB.Frame FrameSinon 
         Caption         =   "Sin�nimos"
         Height          =   4065
         Left            =   -74880
         TabIndex        =   54
         Top             =   4200
         Width           =   12615
         Begin VB.CommandButton BtAdicSinonimo 
            Height          =   375
            Left            =   10680
            Picture         =   "FormAnaComplexas.frx":0990
            Style           =   1  'Graphical
            TabIndex        =   137
            ToolTipText     =   "Adicionar Sinonimo"
            Top             =   135
            Width           =   375
         End
         Begin VB.ListBox EcListaSinonimos 
            Height          =   3180
            Left            =   960
            TabIndex        =   57
            Top             =   600
            Width           =   10215
         End
         Begin VB.CommandButton BtFecharSinonimos 
            Height          =   555
            Left            =   11400
            Picture         =   "FormAnaComplexas.frx":0D1A
            Style           =   1  'Graphical
            TabIndex        =   56
            ToolTipText     =   "Fechar Sin�nimos"
            Top             =   3240
            Width           =   915
         End
         Begin VB.TextBox EcSinonimo 
            Height          =   285
            Left            =   960
            TabIndex        =   55
            Top             =   240
            Width           =   9735
         End
      End
      Begin VB.CheckBox CkInibeDescricao 
         Caption         =   "Inibe designa��o no relat�rio"
         Height          =   255
         Left            =   4440
         TabIndex        =   146
         Top             =   1800
         Width           =   2415
      End
      Begin VB.ListBox EcQuestoes 
         Appearance      =   0  'Flat
         Height          =   1155
         Left            =   4320
         Style           =   1  'Checkbox
         TabIndex        =   144
         Top             =   3240
         Width           =   3615
      End
      Begin VB.TextBox EcPrazoConc 
         Height          =   285
         Left            =   1800
         TabIndex        =   138
         Top             =   3120
         Width           =   735
      End
      Begin VB.ListBox EcInformacao 
         Appearance      =   0  'Flat
         Height          =   1830
         Left            =   9360
         Style           =   1  'Checkbox
         TabIndex        =   135
         Top             =   2640
         Width           =   3135
      End
      Begin VB.CheckBox EcFlagMarcacao 
         Caption         =   "Marca��o autom�tica dos membros"
         Height          =   195
         Left            =   4440
         TabIndex        =   134
         Top             =   900
         Width           =   3135
      End
      Begin VB.CheckBox CkEtiqueta 
         Caption         =   "Etiqueta para cada membro complexa"
         Height          =   255
         Left            =   4440
         TabIndex        =   133
         Top             =   600
         Width           =   3135
      End
      Begin VB.CheckBox CkFacturar 
         Caption         =   "An�lise a facturar"
         Height          =   255
         Left            =   4440
         TabIndex        =   132
         Top             =   1500
         Width           =   3135
      End
      Begin VB.CheckBox CkAbrEtiq 
         Caption         =   "Imprimir abreviatura na etiqueta"
         Height          =   195
         Left            =   4440
         TabIndex        =   131
         Top             =   1200
         Width           =   3135
      End
      Begin VB.TextBox EcPeso 
         Height          =   285
         Left            =   1800
         TabIndex        =   126
         Top             =   1905
         Width           =   735
      End
      Begin VB.TextBox EcCodEstatistica 
         BackColor       =   &H0080FFFF&
         Height          =   285
         Left            =   1800
         TabIndex        =   125
         Top             =   1560
         Width           =   735
      End
      Begin VB.TextBox EcOrdemARS 
         Height          =   285
         Left            =   1800
         TabIndex        =   124
         Top             =   2340
         Width           =   735
      End
      Begin VB.TextBox EcPrazoVal 
         Height          =   285
         Left            =   1800
         TabIndex        =   123
         Top             =   2700
         Width           =   735
      End
      Begin VB.ComboBox CbSexo 
         Height          =   315
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   121
         Top             =   600
         Width           =   1455
      End
      Begin VB.ComboBox CbTipoGrafico 
         Height          =   315
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   119
         Top             =   1110
         Width           =   1455
      End
      Begin VB.ListBox EcListaFolhasTrab 
         Appearance      =   0  'Flat
         Height          =   1605
         Left            =   9240
         Style           =   1  'Checkbox
         TabIndex        =   117
         Top             =   600
         Width           =   3135
      End
      Begin VB.Frame FrameRubrica 
         Caption         =   "Cria��o de R�brica"
         Height          =   3975
         Left            =   -74880
         TabIndex        =   63
         Top             =   2280
         Width           =   12615
         Begin VB.TextBox EcRubrDescr 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   3120
            TabIndex        =   71
            Top             =   480
            Width           =   3735
         End
         Begin VB.TextBox EcRubrCodigo 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   960
            TabIndex        =   70
            Top             =   480
            Width           =   855
         End
         Begin VB.ComboBox CbRubrGrupo 
            Height          =   315
            Left            =   960
            Style           =   2  'Dropdown List
            TabIndex        =   69
            Top             =   1200
            Width           =   5895
         End
         Begin VB.CommandButton BtGravaRubr 
            Height          =   495
            Left            =   10440
            Picture         =   "FormAnaComplexas.frx":19E4
            Style           =   1  'Graphical
            TabIndex        =   68
            ToolTipText     =   "Insere R�brica"
            Top             =   1800
            Width           =   735
         End
         Begin VB.CommandButton BtSairRubr 
            Height          =   495
            Left            =   11400
            Picture         =   "FormAnaComplexas.frx":26AE
            Style           =   1  'Graphical
            TabIndex        =   67
            ToolTipText     =   "Sair Sem Gravar"
            Top             =   1800
            Width           =   735
         End
         Begin VB.TextBox EcRubrDtCri 
            Height          =   285
            Left            =   960
            TabIndex        =   66
            Top             =   1800
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox EcRubrUserCri 
            Height          =   285
            Left            =   1800
            TabIndex        =   65
            Top             =   1800
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox EcRubrFlgExecHosp 
            Height          =   285
            Left            =   2760
            TabIndex        =   64
            Top             =   1800
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo"
            Height          =   255
            Index           =   47
            Left            =   240
            TabIndex        =   74
            Top             =   480
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Descri��o"
            Height          =   255
            Index           =   48
            Left            =   2280
            TabIndex        =   73
            Top             =   480
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo"
            Height          =   255
            Index           =   49
            Left            =   240
            TabIndex        =   72
            Top             =   1200
            Width           =   615
         End
      End
      Begin VB.TextBox EcQtd 
         Height          =   285
         Left            =   -63720
         TabIndex        =   94
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox EcRubrSeqAna 
         Height          =   285
         Left            =   -65880
         TabIndex        =   92
         Top             =   1800
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.TextBox EcCodRubrica 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -73920
         TabIndex        =   86
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox EcDescrRubrica 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -73080
         TabIndex        =   85
         Top             =   600
         Width           =   3735
      End
      Begin VB.CommandButton BtPesquisaRubrica 
         Height          =   315
         Left            =   -69360
         Picture         =   "FormAnaComplexas.frx":3378
         Style           =   1  'Graphical
         TabIndex        =   84
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida R�bricas"
         Top             =   600
         Width           =   375
      End
      Begin VB.CommandButton BtGravarAnaFact 
         Height          =   315
         Left            =   -73680
         Picture         =   "FormAnaComplexas.frx":3702
         Style           =   1  'Graphical
         TabIndex        =   83
         ToolTipText     =   "Gravar Mapeamento Factura��o"
         Top             =   1080
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.CommandButton BtCopiaPrecario 
         Height          =   315
         Left            =   -74400
         Picture         =   "FormAnaComplexas.frx":3A8C
         Style           =   1  'Graphical
         TabIndex        =   82
         ToolTipText     =   "Copiar Tabela de Outra An�lise"
         Top             =   1080
         Width           =   375
      End
      Begin VB.TextBox EcCodAnaFacturar 
         Height          =   285
         Left            =   -65760
         TabIndex        =   81
         Top             =   615
         Width           =   735
      End
      Begin VB.CheckBox CkFlgfacturarAna 
         Caption         =   "Pergunta se pretende facturar an�lise:"
         Height          =   195
         Left            =   -68760
         TabIndex        =   80
         Top             =   615
         Width           =   3015
      End
      Begin VB.CheckBox CkFlgRegra 
         Caption         =   "S� factura se n�o marcada a an�lise:"
         Height          =   195
         Left            =   -68760
         TabIndex        =   79
         Top             =   1095
         Width           =   3015
      End
      Begin VB.TextBox EcCodAnaRegra 
         Height          =   285
         Left            =   -65760
         TabIndex        =   78
         Top             =   1095
         Width           =   735
      End
      Begin VB.CheckBox CkContaMembros 
         Caption         =   "Envia Membros para FACTUS"
         Height          =   255
         Left            =   -64920
         TabIndex        =   77
         Top             =   1080
         Width           =   2535
      End
      Begin VB.TextBox EcAuxPreco 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -72720
         TabIndex        =   75
         Top             =   2040
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.ListBox EcAnaLocais 
         Height          =   960
         Left            =   -67080
         Style           =   1  'Checkbox
         TabIndex        =   10
         Top             =   1020
         Width           =   4695
      End
      Begin VB.ListBox EcAnaLocaisExec 
         Height          =   960
         Left            =   -67080
         Style           =   1  'Checkbox
         TabIndex        =   11
         Top             =   2520
         Width           =   4695
      End
      Begin VB.CommandButton BtSinonimos 
         Height          =   550
         Left            =   -63240
         Picture         =   "FormAnaComplexas.frx":3E16
         Style           =   1  'Graphical
         TabIndex        =   53
         ToolTipText     =   "Sin�nimos"
         Top             =   3720
         Width           =   765
      End
      Begin VB.CommandButton BtCima 
         Height          =   495
         Left            =   -69470
         Picture         =   "FormAnaComplexas.frx":4AE0
         Style           =   1  'Graphical
         TabIndex        =   19
         ToolTipText     =   " Alterar ordem na Complexa "
         Top             =   960
         Width           =   720
      End
      Begin VB.CommandButton BtBaixo 
         Height          =   495
         Left            =   -69470
         Picture         =   "FormAnaComplexas.frx":4E6A
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   " Alterar ordem na Complexa "
         Top             =   3720
         Width           =   735
      End
      Begin VB.CommandButton BtRetira 
         Height          =   495
         Left            =   -69360
         Picture         =   "FormAnaComplexas.frx":51F4
         Style           =   1  'Graphical
         TabIndex        =   18
         ToolTipText     =   " Retirar da Complexa "
         Top             =   2640
         Width           =   495
      End
      Begin VB.CommandButton BtInsere 
         Height          =   495
         Left            =   -69360
         Picture         =   "FormAnaComplexas.frx":557E
         Style           =   1  'Graphical
         TabIndex        =   17
         ToolTipText     =   " Inserir na Complexa "
         Top             =   2040
         Width           =   495
      End
      Begin VB.Frame Frame1 
         Height          =   3810
         Left            =   -74880
         TabIndex        =   14
         Top             =   465
         Width           =   1215
         Begin VB.PictureBox Lupa 
            BorderStyle     =   0  'None
            Height          =   495
            Left            =   390
            Picture         =   "FormAnaComplexas.frx":5908
            ScaleHeight     =   495
            ScaleWidth      =   495
            TabIndex        =   45
            TabStop         =   0   'False
            Top             =   1440
            Width           =   495
         End
         Begin VB.OptionButton OptAnaSimples 
            Caption         =   "An�lises "
            Height          =   255
            Left            =   210
            TabIndex        =   12
            Top             =   600
            Value           =   -1  'True
            Width           =   945
         End
         Begin VB.OptionButton OptFrases 
            Caption         =   "Frases"
            Height          =   255
            Left            =   215
            TabIndex        =   13
            Top             =   960
            Width           =   855
         End
      End
      Begin VB.TextBox EcPesquisa 
         Height          =   285
         Left            =   -73560
         TabIndex        =   15
         Top             =   720
         Width           =   3495
      End
      Begin VB.ListBox ListaMembros 
         Height          =   3570
         Left            =   -67920
         TabIndex        =   21
         Top             =   600
         Width           =   4335
      End
      Begin VB.ListBox ListaAnaSimples 
         Height          =   3180
         Left            =   -73560
         TabIndex        =   24
         Top             =   1020
         Width           =   3495
      End
      Begin VB.ListBox ListaFrases 
         Height          =   2985
         Left            =   -73560
         TabIndex        =   16
         Top             =   1140
         Width           =   3375
      End
      Begin MSFlexGridLib.MSFlexGrid FgPrecos 
         Height          =   2895
         Left            =   -74880
         TabIndex        =   76
         Top             =   1560
         Width           =   12615
         _ExtentX        =   22251
         _ExtentY        =   5106
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
      End
      Begin VB.CheckBox CkGrafico 
         Caption         =   "An�lise tem Gr�fico"
         Height          =   255
         Left            =   120
         TabIndex        =   120
         Top             =   1125
         Width           =   1815
      End
      Begin VB.Frame Frame3 
         Height          =   3975
         Left            =   -74760
         TabIndex        =   95
         Top             =   480
         Width           =   6135
         Begin VB.CommandButton BtPesquisaTuboS 
            Height          =   315
            Left            =   5520
            Picture         =   "FormAnaComplexas.frx":674A
            Style           =   1  'Graphical
            TabIndex        =   142
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Tubos Secund�rios"
            Top             =   3060
            Width           =   375
         End
         Begin VB.TextBox EcDescrTuboS 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   141
            TabStop         =   0   'False
            Top             =   3060
            Width           =   3375
         End
         Begin VB.TextBox EcCodTuboS 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   140
            Top             =   3060
            Width           =   735
         End
         Begin VB.TextBox EcCodMetodo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   9
            Top             =   3480
            Width           =   735
         End
         Begin VB.TextBox EcDescrMetodo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   115
            TabStop         =   0   'False
            Top             =   3480
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaMetodo 
            Height          =   315
            Left            =   5520
            Picture         =   "FormAnaComplexas.frx":6AD4
            Style           =   1  'Graphical
            TabIndex        =   114
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida Tubos Prim�rios"
            Top             =   3480
            Width           =   375
         End
         Begin VB.CommandButton BtPesquisaClasse 
            Height          =   315
            Left            =   5520
            Picture         =   "FormAnaComplexas.frx":6E5E
            Style           =   1  'Graphical
            TabIndex        =   107
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Classes"
            Top             =   1200
            Width           =   375
         End
         Begin VB.TextBox EcDescrClasse 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   106
            TabStop         =   0   'False
            Top             =   1200
            Width           =   3375
         End
         Begin VB.TextBox EcCodClasse 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   5
            Top             =   1200
            Width           =   735
         End
         Begin VB.CommandButton BtPesquisaSubGrupo 
            Height          =   315
            Left            =   5520
            Picture         =   "FormAnaComplexas.frx":71E8
            Style           =   1  'Graphical
            TabIndex        =   105
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Sub Grupos"
            Top             =   720
            Width           =   375
         End
         Begin VB.TextBox EcDescrSubGrupo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   104
            TabStop         =   0   'False
            Top             =   720
            Width           =   3375
         End
         Begin VB.TextBox EcCodSubGrupo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   4
            Top             =   720
            Width           =   735
         End
         Begin VB.CommandButton BtPesquisaGrupo 
            Height          =   315
            Left            =   5520
            Picture         =   "FormAnaComplexas.frx":7572
            Style           =   1  'Graphical
            TabIndex        =   103
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Grupos de An�lises"
            Top             =   240
            Width           =   375
         End
         Begin VB.TextBox EcDescrGrupo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   102
            TabStop         =   0   'False
            Top             =   240
            Width           =   3375
         End
         Begin VB.TextBox EcCodGrupo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   3
            Top             =   240
            Width           =   735
         End
         Begin VB.CommandButton BtPesquisaTuboP 
            Height          =   315
            Left            =   5520
            Picture         =   "FormAnaComplexas.frx":78FC
            Style           =   1  'Graphical
            TabIndex        =   101
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida Tubos Prim�rios"
            Top             =   2640
            Width           =   375
         End
         Begin VB.TextBox EcDescrTuboP 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   100
            TabStop         =   0   'False
            Top             =   2640
            Width           =   3375
         End
         Begin VB.TextBox EcCodTuboP 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   8
            Top             =   2640
            Width           =   735
         End
         Begin VB.CommandButton BtPesquisaProduto 
            Height          =   315
            Left            =   5520
            Picture         =   "FormAnaComplexas.frx":7C86
            Style           =   1  'Graphical
            TabIndex        =   99
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Produtos"
            Top             =   2160
            Width           =   375
         End
         Begin VB.TextBox EcDescrProduto 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   98
            TabStop         =   0   'False
            Top             =   2160
            Width           =   3375
         End
         Begin VB.TextBox EcCodProd 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   7
            Top             =   2160
            Width           =   735
         End
         Begin VB.TextBox EcCodGrImpr 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   6
            Top             =   1635
            Width           =   735
         End
         Begin VB.TextBox EcDescrGrImpr 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   97
            TabStop         =   0   'False
            Top             =   1635
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaGrImpr 
            Height          =   315
            Left            =   5520
            Picture         =   "FormAnaComplexas.frx":8010
            Style           =   1  'Graphical
            TabIndex        =   96
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Grupos de An�lises"
            Top             =   1635
            Width           =   375
         End
         Begin VB.Label Label1 
            Caption         =   "Tubo Aliquota"
            Height          =   255
            Index           =   9
            Left            =   240
            TabIndex        =   143
            Top             =   3060
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "M�todo"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   116
            Top             =   3480
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "Classe"
            Height          =   255
            Index           =   6
            Left            =   240
            TabIndex        =   113
            Top             =   1200
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "SubGrupo"
            Height          =   255
            Index           =   5
            Left            =   240
            TabIndex        =   112
            Top             =   720
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo"
            Height          =   255
            Index           =   4
            Left            =   240
            TabIndex        =   111
            Top             =   240
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Tubo Prim�rio"
            Height          =   255
            Index           =   8
            Left            =   240
            TabIndex        =   110
            Top             =   2640
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "Produto"
            Height          =   255
            Index           =   7
            Left            =   240
            TabIndex        =   109
            Top             =   2160
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo Impress."
            Height          =   255
            Index           =   62
            Left            =   240
            TabIndex        =   108
            Top             =   1680
            Width           =   1215
         End
      End
      Begin VB.Label Label1 
         Caption         =   "Design. Etiqueta"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   149
         Top             =   3480
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Quest�es"
         Height          =   255
         Index           =   59
         Left            =   3360
         TabIndex        =   145
         Top             =   3240
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Tempo Conclus�o"
         Height          =   255
         Index           =   40
         Left            =   120
         TabIndex        =   139
         Top             =   3120
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Informa��es"
         Height          =   255
         Index           =   58
         Left            =   8160
         TabIndex        =   136
         Top             =   2640
         Width           =   1095
      End
      Begin VB.Label Label21 
         Caption         =   "Peso Estat�stico"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   130
         Top             =   1920
         Width           =   1335
      End
      Begin VB.Label Label15 
         Caption         =   "C�d. Estat�stico"
         Height          =   255
         Left            =   120
         TabIndex        =   129
         Top             =   1575
         Width           =   1335
      End
      Begin VB.Label Label21 
         Caption         =   "Ordem ARS"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   128
         Top             =   2355
         Width           =   1335
      End
      Begin VB.Label Label21 
         Caption         =   "Validade"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   127
         Top             =   2715
         Width           =   1335
      End
      Begin VB.Label Label57 
         Caption         =   "An�lise S� marcada em:"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   122
         Top             =   645
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Folhas Trab."
         Height          =   255
         Index           =   54
         Left            =   8280
         TabIndex        =   118
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label100 
         Caption         =   "Quantidade:"
         Height          =   255
         Left            =   -64680
         TabIndex        =   93
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Rubrica"
         Height          =   255
         Index           =   46
         Left            =   -74880
         TabIndex        =   87
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label22 
         Caption         =   "Locais de marca��o"
         Height          =   255
         Left            =   -68040
         TabIndex        =   61
         Top             =   660
         Width           =   3495
      End
      Begin VB.Label Label55 
         Caption         =   "Locais de Execu��o"
         Height          =   255
         Index           =   1
         Left            =   -67920
         TabIndex        =   60
         Top             =   2160
         Width           =   3255
      End
      Begin VB.Label Label4 
         Caption         =   "Pesquisa R�pida"
         Height          =   225
         Left            =   -73560
         TabIndex        =   44
         Top             =   540
         Width           =   2715
      End
   End
   Begin VB.TextBox EcAbreviatura 
      BackColor       =   &H0080FFFF&
      Height          =   285
      Left            =   11280
      TabIndex        =   2
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox EcCodigo 
      BackColor       =   &H0080FFFF&
      Height          =   285
      Left            =   960
      TabIndex        =   0
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox EcDesignacao 
      BackColor       =   &H0080FFFF&
      Height          =   285
      Left            =   3480
      TabIndex        =   1
      Top             =   120
      Width           =   6015
   End
   Begin VB.CheckBox CkInvisivel 
      Caption         =   "Cancelada"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   375
      Left            =   2400
      TabIndex        =   51
      Top             =   480
      Width           =   2535
   End
   Begin VB.Label Label1 
      Caption         =   "EcLinhaPreco"
      Height          =   375
      Index           =   50
      Left            =   7080
      TabIndex        =   91
      Top             =   8640
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcColunaPreco"
      Height          =   375
      Index           =   51
      Left            =   7080
      TabIndex        =   90
      Top             =   9000
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Abreviatura"
      Height          =   255
      Index           =   1
      Left            =   10320
      TabIndex        =   62
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label20 
      Caption         =   "EcCodMetodo"
      Height          =   255
      Left            =   240
      TabIndex        =   58
      Top             =   7200
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label17 
      Caption         =   "EcCodProduto"
      Height          =   255
      Left            =   2760
      TabIndex        =   52
      Top             =   7320
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label52 
      Caption         =   "EcOrdem"
      Height          =   255
      Left            =   5040
      TabIndex        =   48
      Top             =   7800
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label24 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2640
      TabIndex        =   43
      Top             =   7440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label33 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   5040
      TabIndex        =   42
      Top             =   7080
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label34 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   5040
      TabIndex        =   41
      Top             =   7440
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label23 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2640
      TabIndex        =   40
      Top             =   7080
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "EcCodSequencial"
      Height          =   255
      Left            =   360
      TabIndex        =   39
      Top             =   7080
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "Designa��o"
      Height          =   255
      Index           =   0
      Left            =   2400
      TabIndex        =   31
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   30
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label3 
      Caption         =   "Abreviatura"
      Height          =   255
      Index           =   0
      Left            =   2400
      TabIndex        =   29
      Top             =   480
      Width           =   975
   End
End
Attribute VB_Name = "FormAnaComplexas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim NomeTabelaFactus As String
Dim NumCamposFACTUS As Integer
Dim CamposFactusBD() As String
Dim CamposFactusEc() As Object
Dim ChaveBDFactus As String
Dim ChaveEcFactus As Object

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim ListaOrigem As Control
Dim ListaDestino As Control

Dim CamposBDparaListBox
Dim CamposBDparaListBoxF
Dim CamposBDparaListBoxS
Dim NumEspacos
Dim ListarRemovidos As Boolean

'Variavel para guardar a posicao na lista de Membros
Dim PosicaoActualListaMembros As Integer
    
Public rs As ADODB.recordset
' --------------------------------------
' Estrutura de Sinonimos
' --------------------------------------
Private Type Sinonimos
    seq_sinonimo As Long
    descr_sinonimo As String
    flg_original As Integer
End Type
Dim EstrutSinonimos() As Sinonimos
Dim TotalEstrutSinonimos As Integer
Private Type Precarios
    cod_rubr As String
    Portaria As String
    descr_Portaria As String
    cod_efr As String
    cod_rubr_efr As String
    descr_rubr_efr As String
    val_Taxa As String
    t_fac As String
    nr_k As String
    nr_c As String
    val_pag_ent As String
    perc_pag_doe As String
    val_pag_doe As String
    user_cri As String
    dt_cri As String
    user_act As String
    dt_act As String
    empresa_id As String
    valor_c As String
    flg_modificado As Boolean
End Type
Dim EstrutPrec() As Precarios
Dim TotalPrec As Long
Const lColPrecosCodPrecario = 0
Const lColPrecosDescrPrecario = 1
Const lColPrecosCodRubrEFR = 2
Const lColPrecosDescrRubrEFR = 3
Const lColPrecosTxModeradora = 4
Const lColPrecosTipoFact = 5
Const lColPrecosNumK = 6
Const lColPrecosNumC = 7
Const lColPrecosValPagEFR = 8
Const lColPrecosValPagDoe = 9
Const lColPrecosPercDoente = 10
Sub insereMembros()
    
    Dim i As Integer
    Dim sql, insere As String
    Dim Tabela As ADODB.recordset
    Dim sequencial As Integer
    
    For i = 0 To ListaMembros.ListCount - 1
        Set Tabela = New ADODB.recordset
        sequencial = BG_DaMAX("sl_membro", "seq_membro") + 1
        ListaAF.ListIndex = i
        If ListaAF.List(i) = "A" Then
            sql = "SELECT cod_ana_s FROM sl_ana_s WHERE seq_ana_s=" & ListaMembros.ItemData(i)
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            Tabela.Open sql, gConexao
            insere = "INSERT INTO sl_membro(seq_membro,cod_ana_c,cod_membro,t_membro,ordem)"
            insere = insere & "VALUES (" & sequencial & ",'" & Trim(EcCodigo.Text) & "','" & Tabela!cod_ana_s & "',"
            insere = insere & "'" & ListaAF.List(i) & "'," & i + 1 & ")"
            BG_ExecutaQuery_ADO insere
        Else
            sql = "SELECT cod_frase FROM sl_dicionario WHERE seq_frase=" & ListaMembros.ItemData(i)
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            Tabela.Open sql, gConexao
            insere = "INSERT INTO sl_membro(seq_membro,cod_ana_c,cod_membro,t_membro,ordem)"
            insere = insere & "VALUES (" & sequencial & ",'" & Trim(EcCodigo.Text) & "','" & Tabela!cod_frase & "',"
            insere = insere & "'" & ListaAF.List(i) & "'," & i + 1 & ")"
            BG_ExecutaQuery_ADO insere
        End If
        Tabela.Close
        Set Tabela = Nothing
    Next

End Sub

Sub ModificaMembros()
    
    Dim sql As String
    Dim condicao As String
    Dim SQLQuery As String
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    If gSQLError = 0 Then
        sql = "DELETE FROM sl_membro WHERE cod_ana_c='" & Trim(EcCodigo.Text) & "'"
        BG_ExecutaQuery_ADO sql
        insereMembros
    End If

End Sub

Sub PreencheListaMembros()
    
    Dim ii As Integer
    Dim RSListaDestino As ADODB.recordset
    Dim CriterioTabelaD As String
    
    Me.SetFocus
    
    Set RSListaDestino = New ADODB.recordset
    RSListaDestino.CursorType = adOpenStatic
    RSListaDestino.CursorLocation = adUseServer

    CriterioTabelaD = "( SELECT sl_membro.ordem, cod_membro, seq_ana_s as sequencia, descr_ana_s as descricao, t_membro " & _
                      "FROM sl_membro, sl_ana_s " & _
                      "WHERE cod_ana_s = cod_membro AND t_membro = 'A' AND cod_ana_c = '" & Trim(EcCodigo) & "') " & _
                      " UNION " & _
                      "( SELECT sl_membro.ordem, cod_membro, seq_frase as sequencia, descr_frase as descricao, t_membro" & _
                      " FROM sl_membro, sl_dicionario " & _
                      " WHERE cod_frase = cod_membro and t_membro = 'F' AND cod_ana_c = '" & Trim(EcCodigo) & "')" & _
                      " ORDER BY 1"
    
    RSListaDestino.Open CriterioTabelaD, gConexao

    If RSListaDestino.RecordCount > 0 Then
        estado = 2
        ' Inicio do preenchimento de 'EcLista'

        BL_PreencheListBoxMultipla_ADO ListaMembros, RSListaDestino, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT", , "sequencia"
        
        ListaAF.Clear
        RSListaDestino.MoveFirst
        For ii = 0 To RSListaDestino.RecordCount - 1
            ListaAF.AddItem Trim(RSListaDestino!t_membro), ii
            RSListaDestino.MoveNext
        Next ii
        
    End If
    
    RSListaDestino.Close
    Set RSListaDestino = Nothing
      
End Sub

Private Sub BtBaixo_Click()
    
    Dim sdummy As String
    Dim ldummy As Long
    
    If ListaMembros.ListIndex < ListaMembros.ListCount - 1 Then
        sdummy = ListaMembros.List(ListaMembros.ListIndex + 1)
        ldummy = ListaMembros.ItemData(ListaMembros.ListIndex + 1)
        ListaMembros.List(ListaMembros.ListIndex + 1) = ListaMembros.List(ListaMembros.ListIndex)
        ListaMembros.ItemData(ListaMembros.ListIndex + 1) = ListaMembros.ItemData(ListaMembros.ListIndex)
        ListaMembros.List(ListaMembros.ListIndex) = sdummy
        ListaMembros.ItemData(ListaMembros.ListIndex) = ldummy
        
        sdummy = ListaAF.List(ListaMembros.ListIndex + 1)
        ldummy = ListaAF.ItemData(ListaMembros.ListIndex + 1)
        ListaAF.List(ListaMembros.ListIndex + 1) = ListaAF.List(ListaMembros.ListIndex)
        ListaAF.ItemData(ListaMembros.ListIndex + 1) = ListaAF.ItemData(ListaMembros.ListIndex)
        ListaAF.List(ListaMembros.ListIndex) = sdummy
        ListaAF.ItemData(ListaMembros.ListIndex) = ldummy
        ListaMembros.ListIndex = ListaMembros.ListIndex + 1
    End If

End Sub

Private Sub BtCima_Click()
    
    Dim sdummy As String
    Dim ldummy As Long
    
    If ListaMembros.ListIndex > 0 Then
        sdummy = ListaMembros.List(ListaMembros.ListIndex - 1)
        ldummy = ListaMembros.ItemData(ListaMembros.ListIndex - 1)
        ListaMembros.List(ListaMembros.ListIndex - 1) = ListaMembros.List(ListaMembros.ListIndex)
        ListaMembros.ItemData(ListaMembros.ListIndex - 1) = ListaMembros.ItemData(ListaMembros.ListIndex)
        ListaMembros.List(ListaMembros.ListIndex) = sdummy
        ListaMembros.ItemData(ListaMembros.ListIndex) = ldummy
        
        sdummy = ListaAF.List(ListaMembros.ListIndex - 1)
        ldummy = ListaAF.ItemData(ListaMembros.ListIndex - 1)
        ListaAF.List(ListaMembros.ListIndex - 1) = ListaAF.List(ListaMembros.ListIndex)
        ListaAF.ItemData(ListaMembros.ListIndex - 1) = ListaAF.ItemData(ListaMembros.ListIndex)
        ListaAF.List(ListaMembros.ListIndex) = sdummy
        ListaAF.ItemData(ListaMembros.ListIndex) = ldummy
        ListaMembros.ListIndex = ListaMembros.ListIndex - 1
    End If

End Sub

Private Sub BtInsere_Click()
    
    Dim i As Integer
    Dim CountAntes As Integer
    Dim CountDepois As Integer
            
    If OptAnaSimples.value = True Then
        Set ListaOrigem = ListaAnaSimples
    ElseIf OptFrases.value = True Then
        Set ListaOrigem = ListaFrases
    End If
    
    If Not ListaOrigem Is Nothing Then
        Set ListaDestino = ListaMembros
           
        For i = 0 To ListaOrigem.ListCount - 1
            If ListaOrigem.Selected(i) Then
                CountAntes = ListaMembros.ListCount
                BG_PassaElementoEntreListas ListaOrigem, ListaDestino, i, False, False
                CountDepois = ListaMembros.ListCount
                If CountAntes < CountDepois Then
                    If OptAnaSimples.value = True Then
                        ListaAF.AddItem "A", CountDepois - 1
                    Else
                        ListaAF.AddItem "F", CountDepois - 1
                    End If
                End If
                ListaOrigem.Selected(i) = False
            End If
        Next i
    End If
       
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Private Sub BtPesqRap2_Click()
    
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_c"
    CamposEcran(1) = "cod_ana_c"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_c"
    Tamanhos(2) = 3900
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "sl_ana_c"
    CampoPesquisa = "descr_ana_c"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, _
                                                                   ChavesPesq, _
                                                                   CamposEcran, _
                                                                   CamposRetorno, _
                                                                   Tamanhos, _
                                                                   Headers, _
                                                                   CWhere, _
                                                                   CFrom, _
                                                                   "", _
                                                                   CampoPesquisa, _
                                                                   " ORDER BY descr_ana_c ", _
                                                                   "  Pesquisa R�pida de An�lises Complexas")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            Call LimpaCampos
            EcCodigo.Text = resultados(1)
            EcCodigo.SetFocus
            Call FuncaoProcurar
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises Complexas", vbExclamation, "ATEN��O"
        EcCodigo.SetFocus
    End If

End Sub

Private Sub BtRetira_Click()

    If ListaMembros.ListIndex <> -1 Then
        ListaAF.ListIndex = PosicaoActualListaMembros
        ListaMembros.RemoveItem (ListaMembros.ListIndex)
        PosicaoActualListaMembros = ListaMembros.ListIndex
        ListaAF.RemoveItem (ListaAF.ListIndex)
    End If

End Sub


Private Sub CbSexo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbSexo.ListIndex = mediComboValorNull
    End If
End Sub



Private Sub CkGrafico_Click()
    If CkGrafico.value = vbChecked Then
        CbTipoGrafico.Enabled = True
    Else
        CbTipoGrafico.Enabled = False
        CbTipoGrafico.ListIndex = mediComboValorNull
    End If
End Sub

Private Sub cmdOK_Click()

    EcCodigo.Text = UCase(Trim(EcCodigo.Text))
    If (Mid(EcCodigo.Text, 1, 1) <> "C") Then
        EcCodigo.Text = "C" & EcCodigo.Text
    End If
    
    Call FuncaoProcurar

End Sub


Private Sub EcCodigo_GotFocus()
    
    cmdOK.Default = True

End Sub

Private Sub EcCodigo_LostFocus()

    cmdOK.Default = False

End Sub

Private Sub EcCodigo_Validate(Cancel As Boolean)
    
    EcCodigo.Text = UCase(EcCodigo.Text)
    If Trim(EcCodigo.Text) = "C" Then
        BG_Mensagem mediMsgBox, "O [C] por si s�, n�o pode ser c�digo da an�lise, pois � identificativo do tipo de an�lise !   ", vbExclamation, " C�digo da Complexa"
        Cancel = True
    ElseIf left(Trim(EcCodigo.Text), 1) <> "C" And Trim(EcCodigo.Text) <> "" Then
        EcCodigo.Text = "C" & EcCodigo.Text
    ElseIf Trim(EcCodigo.Text) <> "" Then
        EcCodigo.Text = left(Trim(EcCodigo.Text), 1) & Mid(EcCodigo.Text, 2, Len(EcCodigo.Text) - 1)
    End If
    
End Sub

Private Sub EcFlagMarcacao_Click()
    
    If EcFlagMarcacao.value = Checked Then
        CkEtiqueta.Enabled = True
    Else
        CkEtiqueta.value = vbGrayed
        CkEtiqueta.Enabled = False
    End If

End Sub



Private Sub EcLocal_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcLocal.ListIndex = -1
End Sub

Private Sub EcPeso_Change()
    
    BG_ValidaTipoCampo_ADO Me, EcPeso

End Sub

Private Sub EcPesquisa_Change()
    
    RefinaPesquisa

End Sub

Sub RefinaPesquisa()
    
    Dim i As Integer
    Dim k As Integer
    Dim TSQLQueryAux As String
    Dim TCampoChave As String
    Dim TCampoPesquisa As String
    Dim TCamposSel As String
    Dim TNomeTabela As String
    Dim TClausulaWhereAuxiliar As String
    Dim TTabelaAux As ADODB.recordset
    Dim NomeControl As Control
    
    On Error GoTo Trata_Erro
    Set TTabelaAux = New ADODB.recordset
    
    
    If OptAnaSimples.value = True Then
        Set NomeControl = ListaAnaSimples
        TCampoChave = "seq_ana_s"
        TCamposSel = "cod_ana_s, descr_ana_s"
        TCampoPesquisa = "descr_ana_s"
        TNomeTabela = "sl_ana_s"
    Else
        Set NomeControl = ListaFrases
        TCampoChave = "seq_frase"
        TCamposSel = "cod_frase, descr_frase"
        TCampoPesquisa = "descr_frase"
        TNomeTabela = "sl_dicionario"
    End If
    
    TSQLQueryAux = "SELECT " & TCampoChave & " ," & (TCamposSel) & " FROM " & TNomeTabela
    
    If (gPesquisaDentroCampo) Then
        TSQLQueryAux = TSQLQueryAux & " WHERE UPPER(" & TCampoPesquisa & ") " & _
                                              "LIKE '%" & Trim(UCase(BG_CvWilcard(EcPesquisa))) & IIf(BG_CvWilcard(EcPesquisa) <> "%", "%'", "")
    Else
        TSQLQueryAux = TSQLQueryAux & " WHERE UPPER(" & TCampoPesquisa & ") " & _
                                              "LIKE '" & Trim(UCase(BG_CvWilcard(EcPesquisa))) & IIf(BG_CvWilcard(EcPesquisa) <> "%", "%'", "")
    End If
    
    If TClausulaWhereAuxiliar <> "" Then
        TSQLQueryAux = TSQLQueryAux & " AND " & TClausulaWhereAuxiliar
    End If
    TSQLQueryAux = TSQLQueryAux & " ORDER BY " & TCampoPesquisa
    
    TTabelaAux.CursorLocation = adUseServer
    TTabelaAux.CursorType = adOpenStatic
    TTabelaAux.Open TSQLQueryAux, gConexao
    NomeControl.ListIndex = mediComboValorNull
    NomeControl.Clear
    
    i = 0
    Do Until TTabelaAux.EOF
        k = InStr(1, TCamposSel, ",")
        NomeControl.AddItem Trim(TTabelaAux(Trim(Mid(TCamposSel, 1, k - 1)))) & Space(8 - Len(Trim(TTabelaAux(Trim(Mid(TCamposSel, 1, k - 1))))) + 1) & Trim(TTabelaAux(Trim(Mid(TCamposSel, k + 1)))) & Space(60 - Len(Mid(Trim(TTabelaAux(Trim(Mid(TCamposSel, k + 1)))), 1, 40)) + 1)
        NomeControl.ItemData(i) = CInt(TTabelaAux(TCampoChave))
        TTabelaAux.MoveNext
        i = i + 1
    Loop
    TTabelaAux.Close
    Set TTabelaAux = Nothing
    
Trata_Erro:
End Sub

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    
    Set rs = New ADODB.recordset
        
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_ana_c ASC"
    Else
    End If
  
    CriterioTabela = BL_Upper_Campo(CriterioTabela, _
                                    "descr_ana_c", _
                                    gPesquisaDentroCampo)
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        Me.EcCodigo.Locked = True
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    Dim fraseSQL
        
    gMsgTitulo = " Modificar"
    gMsgMsg = "Tem a certeza que deseja validar as altera��es efectuadas ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = BG_CvDataParaWhere_ADO(Bg_DaData_ADO)
        EcUtilizadorAlteracao = gCodUtilizador
        If EcFlagMarcacao.value = 2 Then EcFlagMarcacao.value = 0
        If CkEtiqueta.value = 2 Then CkEtiqueta.value = 0
        If CkGrafico.value = 2 Then CkGrafico.value = 0
        If CkAbrEtiq.value = 2 Then CkAbrEtiq.value = 0
        iRes = ValidaCamposEc
        If iRes = True Then
            ModificaMembros
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        GravaFacturacao
        GravaPrecarios
    End If
    GravaSinonimo
    BL_GravaLocaisAna EcCodigo, Me
    BL_GravaLocaisAnaExec EcCodigo, Me
    BL_GravaFolhasTrab EcCodigo, Me
    BL_GravaDadosInfAna Me, EcCodigo
    BL_GravaDadosQuestoes Me, EcCodigo
    
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
   
    LimpaCampos
    PreencheCampos

End Sub
    
Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        EcCodclasse_Validate False
        EcCodgrImpr_Validate False
        EcCodGrupo_Validate False
        EcCodsubGrupo_Validate False
        EcCodProd_Validate False
        EcCodtubop_Validate False
        EcCodMetodo_Validate False
        EcCodtuboS_Validate False
        PreencheListaMembros
        If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
            PreencheFacturacao
            PreenchePrecarios
        End If
        BL_CarregaLocaisAna EcCodigo, Me
        BL_CarregaLocaisAnaExec EcCodigo, Me
        BL_CarregaFolhasTrab EcCodigo, Me
        BL_PreencheDadosInfAna Me, EcCodigo
        BL_PreencheDadosQuestoes Me, EcCodigo
        If CkInvisivel.value = 1 Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If

    End If

End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    SSTab1.Tab = 0
    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Sub LimpaCampos()
    Dim i As Integer
    
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    LimpaLabels
    ListaMembros.Clear
    ListaAF.Clear
    CkFacturar.value = 2
    LimpaPrecos
    For i = 0 To EcAnaLocais.ListCount - 1
        EcAnaLocais.Selected(i) = False
    Next
    For i = 0 To EcAnaLocaisExec.ListCount - 1
        EcAnaLocaisExec.Selected(i) = False
    Next
    For i = 0 To EcListaFolhasTrab.ListCount - 1
        EcListaFolhasTrab.Selected(i) = False
    Next
    EcDescrClasse = ""
    EcDescrGrImpr = ""
    EcDescrGrupo = ""
    EcDescrProduto = ""
    EcDescrSubGrupo = ""
    EcDescrTuboP = ""
    EcDescrMetodo = ""
    EcDescrTuboS = ""
    CkFacturar.value = vbGrayed
    CkContaMembros.value = vbGrayed
    CkFlgRegra.value = vbGrayed
    EcCodAnaRegra = ""
    CkFlgfacturarAna.value = vbGrayed
    EcCodAnaFacturar = ""
    EcCodRubrica = ""
    EcCodRubrica_Validate False
    EcRubrSeqAna = ""
    EcAuxPreco = ""
    EcAuxPreco.Visible = False
    EcColunaPreco = ""
    EcLinhaPreco = ""
    EcQtd = ""
    BL_LimpaDadosInfAna Me
    BL_LimpaDadosQuestoes Me
    SSTab1.Tab = 0
    CkInibeDescricao.value = vbGrayed
    CkObrigaInfClin.value = vbGrayed
    CkObrigaTerap.value = vbGrayed
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
    
    Me.EcCodigo.Locked = False
    CkInvisivel.Visible = False
    CkInvisivel.value = vbGrayed
    
End Sub

Sub FuncaoRemover()
    
    Dim sql As String
    
    gMsgTitulo = " Remover"
    gMsgMsg = "Tem a certeza que deseja desactivar o registo ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        'Sql = "DELETE FROM sl_membro WHERE cod_ana_c=" & BL_TrataStringParaBD(EcCodigo.Text)
        'BG_ExecutaQuery_ADO Sql
        BD_Delete
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
           
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = "UPDATE " & NomeTabela & " SET flg_invisivel = 1 WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub

Sub FuncaoInserir()
    Dim Codigo As String
    Dim iRes As Integer
    
    gMsgTitulo = " Inserir"
    gMsgMsg = "Tem a certeza que deseja Inserir estes dados ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        If EcCodigo = "" Then
            gMsgTitulo = " C�digo"
            gMsgMsg = "Quer gerar um novo c�digo?   "
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            
            If gMsgResp = vbYes Then
                Codigo = BL_GeraCodigoAnalise
                If Codigo <> "" Then
                    gMsgTitulo = " C�digo"
                    gMsgMsg = "Foi gerado o c�digo: C" & Codigo & ". Aceitar?"
                    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                    If gMsgResp = vbYes Then
                        EcCodigo = "C" & Codigo
                        EcCodEstatistica = EcCodigo
                    Else
                        Exit Sub
                    End If
                End If
            End If
        Else
            If BL_VerificaCodigoExiste(EcCodigo) = True Then
                gMsgTitulo = " C�digo"
                gMsgMsg = "J� existe uma an�lise com esse c�digo. Quer continuar? "
                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                If gMsgResp <> vbYes Then
                    Exit Sub
                End If
            End If
        End If
        
        BL_InicioProcessamento Me, "A inserir registo."
        EcDataCriacao = BG_CvDataParaWhere_ADO(Bg_DaData_ADO)
        EcUtilizadorCriacao = gCodUtilizador
        If EcFlagMarcacao.value = 2 Then EcFlagMarcacao.value = 0
        If CkEtiqueta.value = 2 Then CkEtiqueta.value = 0
        If CkGrafico.value = 2 Then CkGrafico.value = 0
        If CkAbrEtiq.value = 2 Then CkAbrEtiq.value = 0
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
            BL_FimProcessamento Me
            Call FuncaoProcurar
        Else
            BL_FimProcessamento Me
        End If
    End If
   
    
End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    
    EcOrdem.Text = BG_DaMAX(NomeTabela, "ordem") + 1
    
    EcCodSequencial = BG_DaMAX("SLV_ANALISES_APENAS", "seq_ana") + 1
        
    If EcFlagMarcacao.value = 2 Then EcFlagMarcacao.value = 0
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    If gSQLError = 0 Then
        insereMembros
        If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
            GravaFacturacao
            GravaPrecarios
        End If
        GravaSinonimo
        BL_GravaLocaisAna EcCodigo, Me
        BL_GravaLocaisAnaExec EcCodigo, Me
        BL_GravaFolhasTrab EcCodigo, Me
        BL_GravaDadosInfAna Me, EcCodigo
        BL_GravaDadosQuestoes Me, EcCodigo
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Private Sub Form_Load()
    
    EventoLoad

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."

    Set CampoDeFocus = EcCodigo
    
    Inicializacoes
    DefTipoCampos
    LimpaLabels
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    
    PreencheValoresDefeito
    BL_FimProcessamento Me

End Sub

Sub PreencheValoresDefeito()

    BL_InicioProcessamento Me, "Carregar valores de defeito..."
    
    EcFlagMarcacao.value = 2
    CkEtiqueta.value = 2
    CkGrafico.value = 2
    CkFacturar.value = 2
    CkAbrEtiq.value = 2
    
    ' Locais
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcAnaLocais, mediAscComboCodigo
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcAnaLocaisExec, mediAscComboCodigo
    
    BL_IniFolhasTrab Me
    BL_Informacao Me
    BL_Questoes Me
    
    ' Locais
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocal, mediAscComboCodigo

    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", CbSexo, mediAscComboCodigo

    BG_PreencheComboBD_ADO "sl_t_grafico", "cod_t_grafico", "descr_t_grafico", CbTipoGrafico, mediAscComboDesignacao

    BL_FimProcessamento Me
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        BG_PreencheComboBDSecundaria_ADO "FA_GRUP_RUBR", "cod_grupo", "descr_grupo", CbRubrGrupo
    End If
    FrameRubrica.Visible = False

End Sub

Sub LimpaLabels()
    
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
    With FgPrecos
       .rows = 2
       .Cols = 11
       .FixedRows = 1
       .FixedCols = 0
       .AllowBigSelection = False
       .HighLight = flexHighlightAlways
       .FocusRect = flexFocusHeavy
       .MousePointer = flexDefault
       .FillStyle = flexFillSingle
       .SelectionMode = flexSelectionFree
       .AllowUserResizing = flexResizeColumns
       .GridLinesFixed = flexGridInset
       .GridColorFixed = flexTextInsetLight
       .MergeCells = flexMergeFree
       .PictureType = flexPictureColor
       .RowHeightMin = 280
       .row = 0
       
       .ColWidth(lColPrecosCodPrecario) = 500
       .Col = lColPrecosCodPrecario
       .Text = "C�d"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosDescrPrecario) = 2800
       .Col = lColPrecosDescrPrecario
       .Text = "Pre��rio"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosCodRubrEFR) = 1000
       .Col = lColPrecosCodRubrEFR
       .Text = "C�d.Rubr."
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosDescrRubrEFR) = 2800
       .Col = lColPrecosDescrRubrEFR
       .Text = "Descr. Rubrica"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosTxModeradora) = 800
       .Col = lColPrecosTxModeradora
       .Text = "Tx.Modr."
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosTipoFact) = 800
       .Col = lColPrecosTipoFact
       .Text = "Tipo Fact"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosNumC) = 700
       .Col = lColPrecosNumC
       .Text = "N�Cs"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosNumK) = 700
       .Col = lColPrecosNumK
       .Text = "N�Ks"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosValPagEFR) = 700
       .Col = lColPrecosValPagEFR
       .Text = "Val.EFR"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosValPagDoe) = 700
       .Col = lColPrecosValPagDoe
       .Text = "Val.Doe."
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosPercDoente) = 700
       .Col = lColPrecosPercDoente
       .Text = "%Doe"
       .CellAlignment = flexAlignLeftCenter
       
       .Col = 0
    End With
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        SSTab1.TabEnabled(2) = True
    ElseIf gTipoInstituicao = "HOSPITALAR" Then
        SSTab1.TabEnabled(2) = False
    End If
    CbRubrGrupo.Tag = adNumeric
    EcRubrDtCri.Tag = adDate
    EcRubrUserCri.Tag = adVarChar
    EcRubrCodigo.Tag = adNumeric
    EcRubrDescr.Tag = adVarChar
    CkInibeDescricao.value = vbGrayed
    CkObrigaInfClin.value = vbGrayed
    CkObrigaTerap.value = vbGrayed
    'RGONCALVES 10.12.2015 CHSJ-2511
    CkAcreditacao.value = vbGrayed
    '
End Sub

Sub Inicializacoes()
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        BL_Abre_Conexao_Secundaria gSGBD_SECUNDARIA
    End If
    Me.caption = " An�lises Complexas"
    Me.left = 5
    Me.top = 5
    Me.Width = 13170
    Me.Height = 7110
    'Me.Height = 9000 'Campos extras
    
    NomeTabela = "sl_ana_c"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 36
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "seq_ana_c"
    CamposBD(1) = "cod_ana_c"
    CamposBD(2) = "abr_ana_c"
    CamposBD(3) = "descr_ana_c"
    CamposBD(4) = "gr_ana"
    CamposBD(5) = "sgr_ana"
    CamposBD(6) = "flg_etiq_ord"
    CamposBD(7) = "flg_marc_memb"
    CamposBD(8) = "peso"
    CamposBD(9) = "user_cri"
    CamposBD(10) = "dt_cri"
    CamposBD(11) = "user_act"
    CamposBD(12) = "dt_act"
    CamposBD(13) = "classe_ana"
    CamposBD(14) = "ordem"
    CamposBD(15) = "cod_tubo"
    CamposBD(16) = "cod_estatistica"
    CamposBD(17) = "flg_invisivel"
    CamposBD(18) = "cod_produto"
    CamposBD(19) = "flg_grafico"
    CamposBD(20) = "flg_facturar"
    CamposBD(21) = "metodo"
    CamposBD(22) = "cod_local"
    CamposBD(23) = "flg_sexo"
    CamposBD(24) = "cod_t_grafico"
    CamposBD(25) = "flg_etiq_abr"
    CamposBD(26) = "ordem_ars"
    CamposBD(27) = "prazo_Val"
    CamposBD(28) = "gr_impr"
    CamposBD(29) = "prazo_conc"
    CamposBD(30) = "cod_tubo_sec"
    CamposBD(31) = "flg_inibe_descr"
    CamposBD(32) = "descr_etiq"
    CamposBD(33) = "flg_obriga_infocli"
    CamposBD(34) = "flg_obriga_terap"
    'RGONCALVES 10.12.2015 CHSJ-2511
    CamposBD(35) = "ana_acreditada"
    '
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcAbreviatura
    Set CamposEc(3) = EcDesignacao
    Set CamposEc(4) = EcCodGrupo
    Set CamposEc(5) = EcCodSubGrupo
    Set CamposEc(6) = CkEtiqueta
    Set CamposEc(7) = EcFlagMarcacao
    Set CamposEc(8) = EcPeso
    Set CamposEc(9) = EcUtilizadorCriacao
    Set CamposEc(10) = EcDataCriacao
    Set CamposEc(11) = EcUtilizadorAlteracao
    Set CamposEc(12) = EcDataAlteracao
    Set CamposEc(13) = EcCodClasse
    Set CamposEc(14) = EcOrdem
    Set CamposEc(15) = EcCodTuboP
    Set CamposEc(16) = EcCodEstatistica
    Set CamposEc(17) = CkInvisivel
    Set CamposEc(18) = EcCodProd
    Set CamposEc(19) = CkGrafico
    Set CamposEc(20) = CkFacturar
    Set CamposEc(21) = EcCodMetodo
    Set CamposEc(22) = EcLocal
    Set CamposEc(23) = CbSexo
    Set CamposEc(24) = CbTipoGrafico
    Set CamposEc(25) = CkAbrEtiq
    Set CamposEc(26) = EcOrdemARS
    Set CamposEc(27) = EcPrazoVal
    Set CamposEc(28) = EcCodGrImpr
    Set CamposEc(29) = EcPrazoConc
    Set CamposEc(30) = EcCodTuboS
    Set CamposEc(31) = CkInibeDescricao
    Set CamposEc(32) = EcDescrEtiq
    Set CamposEc(33) = CkObrigaInfClin
    Set CamposEc(34) = CkObrigaTerap
    'RGONCALVES 10.12.2015 CHSJ-2511
    Set CamposEc(35) = CkAcreditacao
    '
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo An�lise"
    TextoCamposObrigatorios(3) = "Designa��o da An�lise"
    TextoCamposObrigatorios(6) = "Etiqueta para cada membro da complexa"
    TextoCamposObrigatorios(16) = "C�digo Estat�stico"
    TextoCamposObrigatorios(20) = "An�lise a Facturar"

    
    ChaveBD = "seq_ana_c"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBoxS = Array("descr_ana_s")
    CamposBDparaListBoxF = Array("descr_frase")
    CamposBDparaListBox = Array("cod_membro", "descricao")
    NumEspacos = Array(8, 40)
    PosicaoActualListaMembros = 0

    EcPesquisa.ToolTipText = cMsgWilcards
    EcDesignacao.ToolTipText = cMsgWilcards
    Call BL_FormataCodigo(EcCodigo)
    
    CkInvisivel.Visible = False
    CkInvisivel.value = vbGrayed
    FrameSinon.Visible = False
    
    NumCamposFACTUS = 6
    NomeTabelaFactus = "FA_RUBR"
    ReDim CamposFactusBD(0 To NumCamposFACTUS - 1)
    ReDim CamposFactusEc(0 To NumCamposFACTUS - 1)
    ReDim TextoCamposObrigatoriosFactus(0 To NumCamposFACTUS - 1)
    
    ' Campos da Base de Dados
    CamposFactusBD(0) = "cod_grupo"
    CamposFactusBD(1) = "cod_rubr"
    CamposFactusBD(2) = "descr_rubr"
    CamposFactusBD(3) = "user_cri"
    CamposFactusBD(4) = "dt_cri"
    CamposFactusBD(5) = "flg_exec_hosp"

    ' Campos do Ecr�
    Set CamposFactusEc(0) = CbRubrGrupo
    Set CamposFactusEc(1) = EcRubrCodigo
    Set CamposFactusEc(2) = EcRubrDescr
    Set CamposFactusEc(3) = EcRubrUserCri
    Set CamposFactusEc(4) = EcRubrDtCri
    Set CamposFactusEc(5) = EcRubrFlgExecHosp
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormAnaComplexas = Nothing

End Sub






Private Sub ListaMembros_Click()
    
    PosicaoActualListaMembros = ListaMembros.ListIndex

End Sub

Private Sub ListaMembros_DblClick()
    Dim pos As Integer
    If Mid(Trim(ListaMembros.List(ListaMembros.ListIndex)), 1, 1) = "S" Then
        pos = InStr(1, Trim(ListaMembros.List(ListaMembros.ListIndex)), " ", vbTextCompare)
        FormCodAna.Show
        FormCodAna.EcCodigo = left(Trim(ListaMembros.List(ListaMembros.ListIndex)), pos - 1)
        FormCodAna.FuncaoProcurar
        Set gFormActivo = FormCodAna
    End If
End Sub

Private Sub ListaMembros_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim sdummy As String
    Dim ldummy As Long
    
    If KeyCode = 38 And Shift = 2 Then 'Ctrl + Up: mover para cima
    
        If ListaMembros.ListIndex > 0 Then
            sdummy = ListaMembros.List(ListaMembros.ListIndex - 1)
            ldummy = ListaMembros.ItemData(ListaMembros.ListIndex - 1)
            ListaMembros.List(ListaMembros.ListIndex - 1) = ListaMembros.List(ListaMembros.ListIndex)
            ListaMembros.ItemData(ListaMembros.ListIndex - 1) = ListaMembros.ItemData(ListaMembros.ListIndex)
            ListaMembros.List(ListaMembros.ListIndex) = sdummy
            ListaMembros.ItemData(ListaMembros.ListIndex) = ldummy
            
            sdummy = ListaAF.List(ListaMembros.ListIndex - 1)
            ldummy = ListaAF.ItemData(ListaMembros.ListIndex - 1)
            ListaAF.List(ListaMembros.ListIndex - 1) = ListaAF.List(ListaMembros.ListIndex)
            ListaAF.ItemData(ListaMembros.ListIndex - 1) = ListaAF.ItemData(ListaMembros.ListIndex)
            ListaAF.List(ListaMembros.ListIndex) = sdummy
            ListaAF.ItemData(ListaMembros.ListIndex) = ldummy
        End If
    
    ElseIf KeyCode = 40 And Shift = 2 Then 'Ctrl + Down: mover para baixo
        
        If ListaMembros.ListIndex < ListaMembros.ListCount - 1 Then
            sdummy = ListaMembros.List(ListaMembros.ListIndex + 1)
            ldummy = ListaMembros.ItemData(ListaMembros.ListIndex + 1)
            ListaMembros.List(ListaMembros.ListIndex + 1) = ListaMembros.List(ListaMembros.ListIndex)
            ListaMembros.ItemData(ListaMembros.ListIndex + 1) = ListaMembros.ItemData(ListaMembros.ListIndex)
            ListaMembros.List(ListaMembros.ListIndex) = sdummy
            ListaMembros.ItemData(ListaMembros.ListIndex) = ldummy
            
            sdummy = ListaAF.List(ListaMembros.ListIndex + 1)
            ldummy = ListaAF.ItemData(ListaMembros.ListIndex + 1)
            ListaAF.List(ListaMembros.ListIndex + 1) = ListaAF.List(ListaMembros.ListIndex)
            ListaAF.ItemData(ListaMembros.ListIndex + 1) = ListaAF.ItemData(ListaMembros.ListIndex)
            ListaAF.List(ListaMembros.ListIndex) = sdummy
            ListaAF.ItemData(ListaMembros.ListIndex) = ldummy
        End If
    
    End If
    
End Sub

Private Sub OptAnaSimples_Click()
    
    ListaAnaSimples.Visible = True
    ListaFrases.Visible = False
    RefinaPesquisa

End Sub

Private Sub OptFrases_Click()
    
    ListaAnaSimples.Visible = False
    ListaFrases.Visible = True
    RefinaPesquisa

End Sub



Private Sub BtFecharSinonimos_Click()
    EcListaSinonimos.Clear
    FrameSinon.Visible = False
End Sub

Private Sub BtSinonimos_Click()
    FrameSinon.top = 400
    FrameSinon.left = 50
    FrameSinon.Visible = True
    FuncaoProcurarSinonimos
End Sub

Private Sub FuncaoProcurarSinonimos()
    Dim sSql As String
    Dim rsSin As New ADODB.recordset
    sSql = "SELECT * FROM sl_ana_sinonimos WHERE cod_ana = " & BL_TrataStringParaBD(EcCodigo)
    sSql = sSql & " AND (flg_original IS NULL or flg_original = 0) "
    rsSin.CursorType = adOpenStatic
    rsSin.CursorLocation = adUseServer
    rsSin.Open sSql, gConexao
    TotalEstrutSinonimos = 0
    ReDim EstrutSinonimos(0)
    EcListaSinonimos.Clear
    If rsSin.RecordCount > 0 Then
        While Not rsSin.EOF
            TotalEstrutSinonimos = TotalEstrutSinonimos + 1
            ReDim Preserve EstrutSinonimos(TotalEstrutSinonimos)
            
            EstrutSinonimos(TotalEstrutSinonimos).seq_sinonimo = rsSin!seq_sinonimo
            EstrutSinonimos(TotalEstrutSinonimos).descr_sinonimo = BL_HandleNull(rsSin!descr_sinonimo, "")
            EstrutSinonimos(TotalEstrutSinonimos).flg_original = 0
            EcListaSinonimos.AddItem EstrutSinonimos(TotalEstrutSinonimos).descr_sinonimo
            rsSin.MoveNext
        Wend
    End If
    rsSin.Close
    Set rsSin = Nothing
End Sub



Private Sub EcListaSinonimos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        ApagaSinonimo
    End If
End Sub



Private Sub EcSinonimo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        BtAdicSinonimo_Click
    End If
End Sub

Private Sub ApagaSinonimo()
    Dim sSql As String
    sSql = "DELETE FROM sl_ana_sinonimos WHERE seq_sinonimo =  " & EstrutSinonimos(EcListaSinonimos.ListIndex + 1).seq_sinonimo
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    
    FuncaoProcurarSinonimos
    EcSinonimo = ""
    EcSinonimo.SetFocus

End Sub

Private Sub BtAdicSinonimo_Click()
    Dim sSql As String
    On Error GoTo TrataErro
    
    If gSGBD = gOracle Then
        sSql = "INSERT INTO sl_ana_sinonimos (seq_sinonimo, cod_ana, descr_sinonimo, flg_original) VALUES (seq_sinonimo.nextval, "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", " & BL_TrataStringParaBD(EcSinonimo) & ",0)"
    ElseIf gSGBD = gSqlServer Then
        sSql = "INSERT INTO sl_ana_sinonimos ( cod_ana, descr_sinonimo, flg_original) VALUES ( "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", " & BL_TrataStringParaBD(EcSinonimo) & ",0)"
    End If
    
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    FuncaoProcurarSinonimos
    EcSinonimo = ""
    EcSinonimo.SetFocus
TrataErro:
    BL_LogFile_BD Me.Name, "BtAdicSinonimo_click", Err.Number, Err.Description, ""
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------

' PREENCHE DADOS DE FACTURACAO

' ------------------------------------------------------
Private Sub PreencheFacturacao()
    Dim sSql As String
    Dim RsFact As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT * FROM sl_ana_facturacao WHERE cod_ana = " & BL_TrataStringParaBD(EcCodigo) & " AND cod_efr IS NULL "
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    RsFact.CursorType = adOpenStatic
    RsFact.CursorLocation = adUseServer
    RsFact.Open sSql, gConexao
    If RsFact.RecordCount >= 1 Then
        EcRubrSeqAna = BL_HandleNull(RsFact!seq_ana, "")
        EcCodRubrica = BL_HandleNull(RsFact!cod_ana_gh, "")
        EcCodRubrica_Validate False
        
        If BL_HandleNull(RsFact!flg_conta_membros, "0") = mediSim Then
            CkContaMembros.value = vbChecked
        Else
            CkContaMembros.value = vbUnchecked
        End If
        
        If BL_HandleNull(RsFact!flg_ana_facturar, "0") = mediSim Then
            CkFlgfacturarAna.value = vbChecked
        Else
            CkFlgfacturarAna.value = vbUnchecked
        End If
        EcCodAnaFacturar = BL_HandleNull(RsFact!cod_ana_facturar, "")
        
        If BL_HandleNull(RsFact!Flg_Regra, "0") = mediSim Then
            CkFlgRegra.value = vbChecked
        Else
            CkFlgRegra.value = vbUnchecked
        End If
        EcCodAnaRegra = BL_HandleNull(RsFact!Cod_Ana_Regra, "")
        EcQtd = BL_HandleNull(RsFact!qtd, "")
    End If
    RsFact.Close
    Set RsFact = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a  Preencher Factura��o ", Me.Name, "PreencheFacturacao"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------

' CARREGA PRECARIOS

' ------------------------------------------------------
Private Sub PreenchePrecarios()
    Dim sSql As String
    Dim RsFact As New ADODB.recordset
    Dim rsPrRubr As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM fa_portarias where DT_FIM IS NULL ORDER BY cod_efr, dt_ini"
    RsFact.CursorType = adOpenStatic
    RsFact.CursorLocation = adUseServer
    RsFact.Open sSql, gConexaoSecundaria
    If RsFact.RecordCount >= 1 Then
        While Not RsFact.EOF
            TotalPrec = TotalPrec + 1
            ReDim Preserve EstrutPrec(TotalPrec)
            
            EstrutPrec(TotalPrec).cod_rubr = EcCodRubrica
            EstrutPrec(TotalPrec).cod_efr = BL_HandleNull(RsFact!cod_efr, "")
            EstrutPrec(TotalPrec).Portaria = BL_HandleNull(RsFact!Portaria, "")
            EstrutPrec(TotalPrec).descr_Portaria = BL_HandleNull(RsFact!descr_Port, "")
            EstrutPrec(TotalPrec).empresa_id = BL_HandleNull(RsFact!empresa_id, "")
            EstrutPrec(TotalPrec).flg_modificado = False
            
            FgPrecos.TextMatrix(TotalPrec, lColPrecosCodPrecario) = EstrutPrec(TotalPrec).Portaria
            FgPrecos.TextMatrix(TotalPrec, lColPrecosDescrPrecario) = EstrutPrec(TotalPrec).descr_Portaria
            
            If EcCodRubrica <> "" Then
                sSql = "SELECT * FROM fa_pr_rubr WHERE cod_rubr = " & EcCodRubrica & " AND portaria = " & EstrutPrec(TotalPrec).Portaria
                rsPrRubr.CursorType = adOpenStatic
                rsPrRubr.CursorLocation = adUseServer
                rsPrRubr.Open sSql, gConexaoSecundaria
                If rsPrRubr.RecordCount = 1 Then
                    EstrutPrec(TotalPrec).cod_rubr_efr = BL_HandleNull(rsPrRubr!cod_rubr_efr, "")
                    EstrutPrec(TotalPrec).descr_rubr_efr = BL_HandleNull(rsPrRubr!descr_rubr_efr, "")
                    EstrutPrec(TotalPrec).nr_c = BL_HandleNull(rsPrRubr!nr_c, "")
                    EstrutPrec(TotalPrec).nr_k = BL_HandleNull(rsPrRubr!nr_k, "")
                    EstrutPrec(TotalPrec).perc_pag_doe = BL_HandleNull(rsPrRubr!perc_pag_doe, "")
                    EstrutPrec(TotalPrec).t_fac = BL_HandleNull(rsPrRubr!t_fac, "")
                    EstrutPrec(TotalPrec).val_pag_doe = BL_HandleNull(rsPrRubr!val_pag_doe, "")
                    EstrutPrec(TotalPrec).val_pag_ent = BL_HandleNull(rsPrRubr!val_pag_ent, "")
                    EstrutPrec(TotalPrec).val_Taxa = BL_HandleNull(rsPrRubr!val_Taxa, "")
                    EstrutPrec(TotalPrec).user_cri = BL_HandleNull(rsPrRubr!user_cri, "")
                    EstrutPrec(TotalPrec).user_act = BL_HandleNull(rsPrRubr!user_act, "")
                    EstrutPrec(TotalPrec).dt_cri = BL_HandleNull(rsPrRubr!dt_cri, "")
                    EstrutPrec(TotalPrec).dt_act = BL_HandleNull(rsPrRubr!dt_act, "")
                    EstrutPrec(TotalPrec).valor_c = IF_RetornaValorC(EstrutPrec(TotalPrec).Portaria)
                     
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosCodRubrEFR) = EstrutPrec(TotalPrec).cod_rubr_efr
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosDescrRubrEFR) = EstrutPrec(TotalPrec).descr_rubr_efr
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosNumC) = EstrutPrec(TotalPrec).nr_c
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosNumK) = EstrutPrec(TotalPrec).nr_k
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosPercDoente) = EstrutPrec(TotalPrec).perc_pag_doe
                    
                    If EstrutPrec(TotalPrec).t_fac = "1" Then
                        FgPrecos.TextMatrix(TotalPrec, lColPrecosTipoFact) = "KS E CS"
                    ElseIf EstrutPrec(TotalPrec).t_fac = "2" Then
                        FgPrecos.TextMatrix(TotalPrec, lColPrecosTipoFact) = "EUROS"
                    End If
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosValPagDoe) = EstrutPrec(TotalPrec).val_pag_doe
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosValPagEFR) = EstrutPrec(TotalPrec).val_pag_ent
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosTxModeradora) = EstrutPrec(TotalPrec).val_Taxa
                End If
                rsPrRubr.Close
            End If
            FgPrecos.AddItem ""
            RsFact.MoveNext
        Wend
        RsFact.Close
        Set RsFact = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Carregar Precarios", Me.Name, "PreenchePrecarios"
    Exit Sub
    Resume Next
End Sub


Private Sub EcCodRubrica_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodRubrica.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_rubr, descr_rubr FROM fa_rubr WHERE upper(cod_rubr)= " & BL_TrataStringParaBD(UCase(EcCodRubrica.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexaoSecundaria
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodRubrica.Text = "" & RsDescrGrupo!cod_rubr
            EcDescrRubrica.Text = "" & RsDescrGrupo!descr_rubr
        
        Else
            Cancel = True
            EcDescrRubrica.Text = ""
            BG_Mensagem mediMsgBox, "A R�brica indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            'SendKeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrRubrica.Text = ""
    End If

End Sub


' ------------------------------------------------------

' LIMPA TODOS CAMPOS RELATIVOS AOS VALORES DE REFERENCIA

' ------------------------------------------------------
Private Sub LimpaPrecos()
    Dim i  As Long
    On Error GoTo TrataErro
    
    FgPrecos.rows = 2
    FgPrecos.row = 1
    For i = 0 To FgPrecos.Cols - 1
        FgPrecos.TextMatrix(1, i) = ""
    Next
    TotalPrec = 0
    ReDim EstrutPrec(0)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a limpar FgPrecos ", Me.Name, "LimpaPrecos"
    Exit Sub
    Resume Next
End Sub

Private Sub BtAdicionarRubrica_Click()
    FrameRubrica.Visible = True
    FrameRubrica.top = 360
    FrameRubrica.left = 50
End Sub

Private Sub BtCopiaPrecario_Click()
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_rubr"
    CamposEcran(1) = "cod_rubr"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_rubr"
    CamposEcran(2) = "descr_rubr"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "fa_rubr"
    CampoPesquisa1 = "descr_rubr"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexaoSecundaria, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " descr_rubr", " Pesquisar R�bricas")
    
    mensagem = "N�o foi encontrada nenhuma R�brica"
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            gMsgTitulo = " Remover"
            gMsgMsg = "Tem a certeza que deseja copiar os pre�os da r�brica " & resultados(1) & " - " & resultados(2) & "?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
            If gMsgResp = vbYes Then
                CopiaRubrica CStr(resultados(1))
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


Private Sub BtPesquisaRubrica_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_rubr"
    CamposEcran(1) = "cod_rubr"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_rubr"
    CamposEcran(2) = "descr_rubr"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "fa_rubr"
    CampoPesquisa1 = "descr_rubr"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexaoSecundaria, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " descr_rubr", " Pesquisar R�bricas")
    
    mensagem = "N�o foi encontrada nenhuma R�brica"
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodRubrica.Text = resultados(1)
            EcDescrRubrica.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


' ----------------------------------------------------------------------

' COPIA CODIFICA��ES DE OUTRA AN�LISE

' ----------------------------------------------------------------------
Private Sub CopiaRubrica(rubrica As String)
    Dim sSql As String
    Dim rsPrRubr As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    
    If EcCodRubrica <> "" Then
    
        ' APAGA AS PARAMETRIZACOES EXISTENTES
        For i = 1 To TotalPrec
           EstrutPrec(i).cod_rubr_efr = -1
           EstrutPrec(i).descr_rubr_efr = ""
           EstrutPrec(i).nr_c = ""
           EstrutPrec(i).nr_k = ""
           EstrutPrec(i).perc_pag_doe = ""
           EstrutPrec(i).t_fac = ""
           EstrutPrec(i).val_pag_doe = ""
           EstrutPrec(i).val_pag_ent = ""
           EstrutPrec(i).val_Taxa = ""
           EstrutPrec(i).user_cri = ""
           EstrutPrec(i).user_act = ""
           EstrutPrec(i).dt_cri = ""
           EstrutPrec(i).dt_act = ""
           EstrutPrec(i).flg_modificado = True
    
           FgPrecos.TextMatrix(i, lColPrecosCodRubrEFR) = EstrutPrec(i).cod_rubr_efr
           FgPrecos.TextMatrix(i, lColPrecosDescrRubrEFR) = EstrutPrec(i).descr_rubr_efr
           FgPrecos.TextMatrix(i, lColPrecosNumC) = EstrutPrec(i).nr_c
           FgPrecos.TextMatrix(i, lColPrecosNumK) = EstrutPrec(i).nr_k
           FgPrecos.TextMatrix(i, lColPrecosPercDoente) = EstrutPrec(i).perc_pag_doe
           FgPrecos.TextMatrix(i, lColPrecosTipoFact) = ""
           FgPrecos.TextMatrix(i, lColPrecosValPagDoe) = EstrutPrec(i).val_pag_doe
           FgPrecos.TextMatrix(i, lColPrecosValPagEFR) = EstrutPrec(i).val_pag_ent
           FgPrecos.TextMatrix(i, lColPrecosTxModeradora) = EstrutPrec(i).val_Taxa
           Exit For
        Next
        sSql = "SELECT * FROM fa_pr_rubr WHERE cod_rubr = " & rubrica
        rsPrRubr.CursorType = adOpenStatic
        rsPrRubr.CursorLocation = adUseServer
        rsPrRubr.Open sSql, gConexaoSecundaria
        If rsPrRubr.RecordCount >= 1 Then
            While Not rsPrRubr.EOF
                For i = 1 To TotalPrec
                    If BL_HandleNull(rsPrRubr!Portaria, "") = EstrutPrec(i).Portaria Then
                        EstrutPrec(i).cod_rubr_efr = BL_HandleNull(rsPrRubr!cod_rubr_efr, "")
                        EstrutPrec(i).descr_rubr_efr = EcDescrRubrica
                        EstrutPrec(i).nr_c = BL_HandleNull(rsPrRubr!nr_c, "")
                        EstrutPrec(i).nr_k = BL_HandleNull(rsPrRubr!nr_k, "")
                        EstrutPrec(i).perc_pag_doe = BL_HandleNull(rsPrRubr!perc_pag_doe, "")
                        EstrutPrec(i).t_fac = BL_HandleNull(rsPrRubr!t_fac, "")
                        EstrutPrec(i).val_pag_doe = BL_HandleNull(rsPrRubr!val_pag_doe, "")
                        EstrutPrec(i).val_pag_ent = BL_HandleNull(rsPrRubr!val_pag_ent, "")
                        EstrutPrec(i).val_Taxa = BL_HandleNull(rsPrRubr!val_Taxa, "")
                        EstrutPrec(i).user_cri = BL_HandleNull(rsPrRubr!user_cri, "")
                        EstrutPrec(i).user_act = BL_HandleNull(rsPrRubr!user_act, "")
                        EstrutPrec(i).dt_cri = BL_HandleNull(rsPrRubr!dt_cri, "")
                        EstrutPrec(i).dt_act = BL_HandleNull(rsPrRubr!dt_act, "")
                        EstrutPrec(i).flg_modificado = True
                 
                        FgPrecos.TextMatrix(i, lColPrecosCodRubrEFR) = EstrutPrec(i).cod_rubr_efr
                        FgPrecos.TextMatrix(i, lColPrecosDescrRubrEFR) = EstrutPrec(i).descr_rubr_efr
                        FgPrecos.TextMatrix(i, lColPrecosNumC) = EstrutPrec(i).nr_c
                        FgPrecos.TextMatrix(i, lColPrecosNumK) = EstrutPrec(i).nr_k
                        FgPrecos.TextMatrix(i, lColPrecosPercDoente) = EstrutPrec(i).perc_pag_doe
                        If BL_HandleNull(rsPrRubr!t_fac, "") = "1" Then
                            FgPrecos.TextMatrix(i, lColPrecosTipoFact) = "KS E CS"
                        ElseIf BL_HandleNull(rsPrRubr!t_fac, "") = "2" Then
                            FgPrecos.TextMatrix(i, lColPrecosTipoFact) = "EUROS"
                        End If
                        FgPrecos.TextMatrix(i, lColPrecosValPagDoe) = EstrutPrec(i).val_pag_doe
                        FgPrecos.TextMatrix(i, lColPrecosValPagEFR) = EstrutPrec(i).val_pag_ent
                        FgPrecos.TextMatrix(i, lColPrecosTxModeradora) = EstrutPrec(i).val_Taxa
                        Exit For
                    End If
                Next
                rsPrRubr.MoveNext
            Wend
        End If
        rsPrRubr.Close
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Copiar R�brica", Me.Name, "CopiaRubrica"
    Exit Sub
    Resume Next
End Sub


Private Sub BtGravaRubr_Click()
    Dim SQLQuery As String
    Dim RsFact As New ADODB.recordset
    On Error GoTo TrataErro
    
    SQLQuery = "SELECT * from fa_rubr WHERE cod_rubr = " & EcRubrCodigo
    RsFact.CursorType = adOpenStatic
    RsFact.CursorLocation = adUseServer
    RsFact.Open SQLQuery, gConexaoSecundaria
    If RsFact.RecordCount >= 1 Then
        BG_Mensagem mediMsgBox, "R�brica indicada j� existe.", vbExclamation, "ATEN��O"
        Exit Sub
    End If
    On Error GoTo TrataErro
    EcRubrDtCri = Bg_DaData_ADO
    EcRubrUserCri = gCodUtilizador
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabelaFactus, CamposFactusBD, CamposFactusEc)
    gConexaoSecundaria.Execute SQLQuery
    BG_Mensagem mediMsgBox, "R�brica Inserida.", vbExclamation, "ATEN��O"

    FrameRubrica.Visible = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Gravar Nova Rubrica ", Me.Name, "BtGravaRubr_Click"
    BG_RollbackTransaction
    Exit Sub
    Resume Next
End Sub

Private Sub BtSairRubr_Click()
    FrameRubrica.Visible = False
End Sub

' ------------------------------------------------------

' GRAVA DADOS DE FACTURA��O

' ------------------------------------------------------

Private Function GravaFacturacao() As Boolean
    Dim sSql As String
    Dim FlgfacturarAna As Integer
    Dim flgRegra As Integer
    Dim ContaMembros As Integer
    Dim rsPortaria As New ADODB.recordset
    Dim portariaAtiba As Integer
    On Error GoTo TrataErro
    
    GravaFacturacao = False
    If CkFlgfacturarAna.value = vbChecked Then
        FlgfacturarAna = 1
    Else
        FlgfacturarAna = 0
    End If
    If CkFlgRegra.value = vbChecked Then
        flgRegra = 1
    Else
        flgRegra = 0
    End If
    If CkContaMembros.value = vbChecked Then
        ContaMembros = 1
    Else
        ContaMembros = 0
    End If
    
    If EcRubrSeqAna <> "" And EcCodRubrica.Text = "" Then
        sSql = "DELETE FROM sl_ana_facturacao WHERE seq_ana = " & EcRubrSeqAna
        BG_ExecutaQuery_ADO sSql
    ElseIf EcCodRubrica <> "" Then
        sSql = " SELECT x2.cod_portaria FROM sl_portarias x2 Where " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN dt_ini"
        sSql = sSql & " AND NVL (dt_fim,'31-12-2099')"
        rsPortaria.CursorType = adOpenStatic
        rsPortaria.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsPortaria.Open sSql, gConexao
        If rsPortaria.RecordCount <> 1 Then
            GravaFacturacao = False
            Exit Function
        Else
            portariaAtiba = BL_HandleNull(rsPortaria!cod_portaria, mediComboValorNull)
        End If
        rsPortaria.Close
        Set rsPortaria = Nothing
        If EcRubrSeqAna.Text = "" Then
            EcRubrSeqAna = BG_DaMAX("sl_ana_facturacao", "seq_ana") + 1
            sSql = "INSERT INTO Sl_ana_facturacao (seq_ana, cod_ana, descr_ana,cod_ana_gh, descr_ana_Facturacao,flg_regra, "
            sSql = sSql & " cod_ana_regra, flg_conta_membros, flg_ana_facturar, cod_ana_facturar,qtd, cod_portaria, user_cri, dt_Cri, hr_cri  ) VALUES( "
            sSql = sSql & EcRubrSeqAna & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", "
            sSql = sSql & BL_TrataStringParaBD(EcDesignacao) & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodRubrica) & ", "
            sSql = sSql & BL_TrataStringParaBD(EcDescrRubrica) & ", "
            sSql = sSql & flgRegra & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodAnaRegra) & ", "
            sSql = sSql & ContaMembros & ", "
            sSql = sSql & FlgfacturarAna & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodAnaFacturar) & ","
            sSql = sSql & BL_HandleNull(EcQtd, "NULL") & ", "
            sSql = sSql & portariaAtiba & ","
            sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ") "
            BG_ExecutaQuery_ADO sSql
        Else
            sSql = "UPDATE Sl_ana_facturacao SET cod_ana_gh  = " & BL_TrataStringParaBD(EcCodRubrica) & ","
            sSql = sSql & " flg_regra  = " & flgRegra & ", "
            sSql = sSql & " cod_ana_regra  = " & BL_TrataStringParaBD(EcCodAnaRegra) & ", "
            sSql = sSql & " flg_conta_membros  = " & ContaMembros & ", "
            sSql = sSql & " flg_ana_facturar  = " & FlgfacturarAna & ", "
            sSql = sSql & " cod_ana_facturar  = " & BL_TrataStringParaBD(EcCodAnaFacturar.Text) & ", "
            sSql = sSql & " qtd  = " & BL_HandleNull(EcQtd, "NULL") & ", "
            sSql = sSql & " cod_portaria  = " & portariaAtiba & ", "
            sSql = sSql & " user_act  = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & " dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & " hr_act = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & " WHERE  seq_ana = " & EcRubrSeqAna.Text
            BG_ExecutaQuery_ADO sSql
        End If
    End If
    GravaFacturacao = True
Exit Function
TrataErro:
    GravaFacturacao = False
    BG_LogFile_Erros "Erro a Gravar Factura��o ", Me.Name, "GravaFacturacao"
    Exit Function
    Resume Next
End Function



' ------------------------------------------------------

' GRAVA DADOS DA TABELA DE PRECOS

' ------------------------------------------------------

Private Sub GravaPrecarios()
    Dim sSql As String
    Dim i As Long
    On Error GoTo TrataErro
    
    gConexaoSecundaria.BeginTrans
    
    For i = 1 To TotalPrec
    
        If EstrutPrec(i).flg_modificado = True And EstrutPrec(i).cod_rubr <> "" Then
            sSql = "DELETE FROM fa_pr_rubr WHERE portaria = " & EstrutPrec(i).Portaria & " AND cod_rubr = " & EstrutPrec(i).cod_rubr
            gConexaoSecundaria.Execute sSql
        
            If EstrutPrec(i).cod_rubr_efr <> "" Or EstrutPrec(i).nr_c <> "" Or EstrutPrec(i).nr_k <> "" Or EstrutPrec(i).perc_pag_doe <> "" _
                    Or EstrutPrec(i).val_pag_doe <> "" Or EstrutPrec(i).val_pag_ent <> "" Or EstrutPrec(i).val_pag_doe <> "" _
                    Or EstrutPrec(i).val_Taxa <> "" Or EstrutPrec(i).perc_pag_doe <> "" Then
                    
                sSql = "INSERT INTO fa_pr_rubr (cod_rubr, portaria, cod_Efr, cod_rubr_efr, descr_rubr_efr, val_taxa, t_fac, nr_c, nr_k, "
                sSql = sSql & " val_pag_ent, perc_pag_doe, val_pag_doe,user_cri, dt_cri, user_act, dt_act ) VALUES ("
                sSql = sSql & EstrutPrec(i).cod_rubr & ", "
                sSql = sSql & EstrutPrec(i).Portaria & ", "
                sSql = sSql & EstrutPrec(i).cod_efr & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).cod_rubr_efr) & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).descr_rubr_efr) & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).val_Taxa, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).t_fac, ",", "."), 2) & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).nr_c, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).nr_k, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).val_pag_ent, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).perc_pag_doe, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).val_pag_doe, ",", "."), "NULL") & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).user_cri) & ", "
                sSql = sSql & BL_TrataDataParaBD(EstrutPrec(i).dt_cri) & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).user_act) & ", "
                sSql = sSql & BL_TrataDataParaBD(EstrutPrec(i).dt_act) & ") "
                gConexaoSecundaria.Execute sSql
            End If
            
        End If
    Next
    gConexaoSecundaria.CommitTrans
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Gravar Pre�arios ", Me.Name, "GravaPrecarios"
    gConexaoSecundaria.RollbackTrans
    Exit Sub
    Resume Next
End Sub


Private Sub FgPrecos_Click()
    EcLinhaPreco = FgPrecos.row
    EcColunaPreco = FgPrecos.Col
End Sub

Private Sub FgPrecos_DblClick()
    EcLinhaPreco = ""
    EcColunaPreco = ""
    If FgPrecos.row > 0 And FgPrecos.row <= TotalPrec Then
        EcLinhaPreco = FgPrecos.row
        EcColunaPreco = FgPrecos.Col
        If FgPrecos.Col <> lColPrecosCodPrecario And FgPrecos.Col <> lColPrecosDescrPrecario And FgPrecos.Col <> lColPrecosTipoFact Then
            EcAuxPreco.Visible = True
            EcAuxPreco.left = FgPrecos.CellLeft + FgPrecos.left
            EcAuxPreco.top = FgPrecos.CellTop + FgPrecos.top
            EcAuxPreco.Width = FgPrecos.CellWidth + 10
            EcAuxPreco.SetFocus
        ElseIf FgPrecos.Col = lColPrecosTipoFact Then
            If EstrutPrec(EcLinhaPreco).t_fac = "1" Then
                EstrutPrec(EcLinhaPreco).t_fac = "2"
            Else
                EstrutPrec(EcLinhaPreco).t_fac = "1"
            End If
            If EstrutPrec(EcLinhaPreco).t_fac = "1" Then
                FgPrecos.TextMatrix(EcLinhaPreco, lColPrecosTipoFact) = "KS E CS"
            ElseIf EstrutPrec(EcLinhaPreco).t_fac = "2" Then
                FgPrecos.TextMatrix(EcLinhaPreco, lColPrecosTipoFact) = "EUROS"
            End If
    
        Else
            Exit Sub
        End If
        
        EstrutPrec(EcLinhaPreco).flg_modificado = True
        If EstrutPrec(EcLinhaPreco).user_cri = "" Then
            EstrutPrec(EcLinhaPreco).user_cri = gCodUtilizador
            EstrutPrec(EcLinhaPreco).dt_cri = Bg_DaData_ADO
        Else
            EstrutPrec(EcLinhaPreco).user_act = gCodUtilizador
            EstrutPrec(EcLinhaPreco).dt_act = Bg_DaData_ADO
        End If
        
        If FgPrecos.Col = lColPrecosCodRubrEFR Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).cod_rubr_efr
        ElseIf FgPrecos.Col = lColPrecosDescrRubrEFR Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).descr_rubr_efr
        ElseIf FgPrecos.Col = lColPrecosTxModeradora Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).val_Taxa
        ElseIf FgPrecos.Col = lColPrecosNumC Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).nr_c
        ElseIf FgPrecos.Col = lColPrecosNumK Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).nr_k
        ElseIf FgPrecos.Col = lColPrecosValPagEFR Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).val_pag_ent
        ElseIf FgPrecos.Col = lColPrecosValPagDoe Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).val_pag_doe
        ElseIf FgPrecos.Col = lColPrecosPercDoente Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).perc_pag_doe
        End If
    End If
End Sub

Private Sub FgPrecos_GotFocus()
    EcAuxPreco.Visible = False
End Sub

Private Sub FgPrecos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        FgPrecos_DblClick
    End If
End Sub

Private Sub EcAuxPreco_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        If EcLinhaPreco <> "" And EcColunaPreco <> "" Then
            If EcColunaPreco = lColPrecosCodRubrEFR Then
                EstrutPrec(EcLinhaPreco).cod_rubr_efr = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosDescrRubrEFR Then
                EstrutPrec(EcLinhaPreco).descr_rubr_efr = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosTxModeradora Then
                EstrutPrec(EcLinhaPreco).val_Taxa = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosNumC Then
                EstrutPrec(EcLinhaPreco).nr_c = EcAuxPreco
                EstrutPrec(EcLinhaPreco).val_pag_ent = Round(CDbl(EstrutPrec(EcLinhaPreco).nr_c) * CDbl(EstrutPrec(EcLinhaPreco).valor_c), 2)
                FgPrecos.TextMatrix(EcLinhaPreco, lColPrecosValPagEFR) = EstrutPrec(EcLinhaPreco).val_pag_ent
            ElseIf EcColunaPreco = lColPrecosNumK Then
                EstrutPrec(EcLinhaPreco).nr_k = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosPercDoente Then
                EstrutPrec(EcLinhaPreco).perc_pag_doe = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosValPagDoe Then
                EstrutPrec(EcLinhaPreco).val_pag_doe = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosValPagEFR Then
                EstrutPrec(EcLinhaPreco).val_pag_ent = EcAuxPreco
            End If
            FgPrecos.TextMatrix(EcLinhaPreco, EcColunaPreco) = EcAuxPreco
            EcAuxPreco = ""
            EcAuxPreco.Visible = False
            FgPrecos.SetFocus
        End If
    End If
End Sub

Private Sub EcAuxPreco_LostFocus()
    EcAuxPreco.Visible = False
    EcAuxPreco = ""
End Sub

Private Sub EcAuxPreco_Validate(Cancel As Boolean)
    EcAuxPreco.Visible = False
    EcAuxPreco = ""
End Sub

Private Sub EcOrdemARS_Validate(Cancel As Boolean)
    If EcOrdemARS = "" Then Exit Sub
    If Not IsNumeric(EcOrdemARS) Then
        BG_Mensagem mediMsgBox, "Valor tem que ser num�rico.", vbExclamation + vbOKOnly, "Ordem ARS."
        EcOrdemARS.SetFocus
    End If
End Sub


' ----------------------------------------------------------------------

' GRAVA SINONIMO IGUAL A DESCRICAO

' ----------------------------------------------------------------------
Private Sub GravaSinonimo()
    Dim sSql As String
    
    sSql = "DELETE FROM sl_ana_sinonimos WHERE cod_ana = " & BL_TrataStringParaBD(EcCodigo) & " AND flg_original = 1 "
    BG_ExecutaQuery_ADO sSql
    
    If gSGBD = gOracle Then
        sSql = "INSERT INTO sl_ana_sinonimos (seq_sinonimo, cod_ana, descr_sinonimo, flg_original) VALUES (seq_sinonimo.nextval, "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", " & BL_TrataStringParaBD(EcDesignacao) & ",1)"
    ElseIf gSGBD = gSqlServer Then
        sSql = "INSERT INTO sl_ana_sinonimos ( cod_ana, descr_sinonimo, flg_original) VALUES ( "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", " & BL_TrataStringParaBD(EcDesignacao) & ",1)"
    End If
    
    BG_ExecutaQuery_ADO sSql
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Gravar Sinonimos", Me.Name, "GravaSinonimo"
    Exit Sub
    Resume Next
End Sub

Private Sub EcCodgrImpr_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodGrImpr)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrImpr.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_gr_impr, descr_gr_impr FROM sl_gr_impr WHERE upper(cod_gr_impr)= " & BL_TrataStringParaBD(UCase(EcCodGrImpr.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodGrImpr.Text = "" & RsDescrGrupo!cod_gr_impr
            EcDescrGrImpr.Text = "" & RsDescrGrupo!descr_gr_impr
        Else
            Cancel = True
            EcDescrGrImpr.Text = ""
            BG_Mensagem mediMsgBox, "O grupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrGrImpr.Text = ""
    End If

End Sub

Private Sub EcCodGrupo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodGrupo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrupo.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_gr_ana, descr_gr_ana FROM sl_gr_ana WHERE upper(cod_gr_ana)= " & BL_TrataStringParaBD(UCase(EcCodGrupo.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodGrupo.Text = "" & RsDescrGrupo!cod_gr_ana
            EcDescrGrupo.Text = "" & RsDescrGrupo!descr_gr_ana
        Else
            Cancel = True
            EcDescrGrupo.Text = ""
            BG_Mensagem mediMsgBox, "O grupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrGrupo.Text = ""
    End If

End Sub

Private Sub EcCodsubGrupo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodGrupo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodSubGrupo.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_sgr_ana, descr_sgr_ana FROM sl_sgr_ana WHERE upper(cod_sgr_ana)= " & BL_TrataStringParaBD(UCase(EcCodSubGrupo.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodSubGrupo.Text = "" & RsDescrGrupo!cod_sgr_ana
            EcDescrSubGrupo.Text = "" & RsDescrGrupo!descr_sgr_ana
        Else
            Cancel = True
            EcDescrSubGrupo.Text = ""
            BG_Mensagem mediMsgBox, "O Subgrupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrSubGrupo.Text = ""
    End If

End Sub

Private Sub EcCodclasse_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodClasse.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_classe_ana, descr_classe_ana FROM sl_classe_ana WHERE upper(cod_classe_ana)= " & BL_TrataStringParaBD(UCase(EcCodClasse.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodClasse.Text = "" & RsDescrGrupo!cod_classe_ana
            EcDescrClasse.Text = "" & RsDescrGrupo!descr_classe_ana
        Else
            Cancel = True
            EcDescrClasse.Text = ""
            BG_Mensagem mediMsgBox, "A Classe indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrClasse.Text = ""
    End If

End Sub

Private Sub EcCodProd_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProd.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_produto, descr_produto FROM sl_produto WHERE upper(cod_produto)= " & BL_TrataStringParaBD(UCase(EcCodProd.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodProd.Text = "" & RsDescrGrupo!cod_produto
            EcDescrProduto.Text = "" & RsDescrGrupo!descr_produto
        Else
            Cancel = True
            EcDescrProduto.Text = ""
            BG_Mensagem mediMsgBox, "O Produto indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrProduto.Text = ""
    End If

End Sub

Private Sub EcCodtubop_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodTuboP.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_tubo, descr_tubo FROM sl_tubo WHERE upper(cod_tubo)= " & BL_TrataStringParaBD(UCase(EcCodTuboP.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodTuboP.Text = "" & RsDescrGrupo!cod_tubo
            EcDescrTuboP.Text = "" & RsDescrGrupo!descR_tubo
        Else
            Cancel = True
            EcDescrTuboP.Text = ""
            BG_Mensagem mediMsgBox, "O Tubo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrTuboP.Text = ""
    End If
End Sub

Private Sub BtPesquisaGrupo_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupo de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrupo.Text = resultados(1)
            EcDescrGrupo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub
Private Sub BtPesquisaGrImpr_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_impr"
    CamposEcran(1) = "seq_gr_impr"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_impr"
    CamposEcran(2) = "descr_gr_impr"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_impr"
    CampoPesquisa1 = "descr_gr_impr"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupo de Impress�o")
    
    mensagem = "N�o foi encontrada nenhum Grupo de Impress�o."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrImpr.Text = resultados(1)
            EcDescrGrImpr.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesquisasubGrupo_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_sgr_ana"
    CamposEcran(1) = "cod_sgr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_sgr_ana"
    CamposEcran(2) = "descr_sgr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_sgr_ana"
    CampoPesquisa1 = "descr_sgr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar SubGrupo de An�lises")
    
    mensagem = "N�o foi encontrada nenhum SubGrupo de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodSubGrupo.Text = resultados(1)
            EcDescrSubGrupo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub



Private Sub BtPesquisaclasse_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_classe_ana"
    CamposEcran(1) = "cod_classe_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_classe_ana"
    CamposEcran(2) = "descr_classe_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_classe_ana"
    CampoPesquisa1 = "descr_classe_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Classe de An�lises")
    
    mensagem = "N�o foi encontrada nenhuma classe de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodClasse.Text = resultados(1)
            EcDescrClasse.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesquisaProduto_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_produto"
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_produto"
    CampoPesquisa1 = "descr_produto"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Produto")
    
    mensagem = "N�o foi encontrado nenhum produto."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProd.Text = resultados(1)
            EcDescrProduto.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


Private Sub BtPesquisaTubop_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_Tubo"
    CamposEcran(1) = "cod_Tubo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_Tubo"
    CamposEcran(2) = "descr_Tubo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_Tubo"
    CampoPesquisa1 = "descr_Tubo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tubo")
    
    mensagem = "N�o foi encontrado nenhum Tubo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodTuboP.Text = resultados(1)
            EcDescrTuboP.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub






Private Sub BtPesquisametodo_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_metodo"
    CamposEcran(1) = "seq_metodo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_metodo"
    CamposEcran(2) = "descr_metodo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_metodo"
    CampoPesquisa1 = "descr_metodo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Metodos")
    
    mensagem = "N�o foi encontrado nenhum Metodo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMetodo.Text = resultados(1)
            EcDescrMetodo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub EcCodMetodo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodMetodo.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_metodo, descr_metodo FROM sl_metodo WHERE upper(cod_metodo)= " & BL_TrataStringParaBD(UCase(EcCodMetodo.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodMetodo.Text = "" & RsDescrGrupo!cod_metodo
            EcDescrMetodo.Text = "" & RsDescrGrupo!descr_metodo
        Else
            Cancel = True
            EcDescrMetodo.Text = ""
            BG_Mensagem mediMsgBox, "O grupo de exames indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrMetodo.Text = ""
    End If
End Sub

Private Sub EcCodTuboS_LostFocus()
    
    EcCodTuboS.Text = UCase(EcCodTuboS.Text)
    

End Sub
Private Sub EcCodtuboS_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodTuboS.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_tubo, descr_tubo FROM sl_Tubo_sec WHERE upper(cod_tubo)= " & BL_TrataStringParaBD(UCase(EcCodTuboS.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodTuboS.Text = "" & RsDescrGrupo!cod_tubo
            EcDescrTuboS.Text = "" & RsDescrGrupo!descR_tubo
        Else
            Cancel = True
            EcDescrTuboS.Text = ""
            BG_Mensagem mediMsgBox, "O Tubo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrTuboS.Text = ""
    End If

End Sub

Private Sub BtPesquisaTubos_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_Tubo"
    CamposEcran(1) = "cod_Tubo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_Tubo"
    CamposEcran(2) = "descr_Tubo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_Tubo_sec"
    CampoPesquisa1 = "descr_Tubo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tubo")
    
    mensagem = "N�o foi encontrado nenhum Tubo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodTuboS.Text = resultados(1)
            EcDescrTuboS.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


Attribute VB_Name = "ParamAmbiente"
Option Explicit

'Indica que o doente deve ser sempre pesquisado na gestao hospitalar
    'independentemente se existe ou nao no SISLAB (HPP)
Global gPesquisaSempreUtenteNaGH As Integer

'Indica se a EFR � para passar para o boletim de resultados ou n�o
Global gImprimeEFRRes As Integer

'Indica o estado da caixa de texto (Info) no ecra de resultados
Global gEstadoInfoResultados As Integer

'Indica se a impressao remota est� activa ou desactiva qd se entra na aplica��o
Global gImpressaoRemota As Integer

'N� de linhas de uma p�gina no boletim de resultados
Global gLinhasPagina As Integer

'Modelo do report de resultados
Global gModeloMapaRes As Integer

'Indica se � para copiar dados anteiores do utente em vez da requisi��o
Global gCopiarAnteriorUtente As Integer

'Indica o c�digo da an�lise simples onde se soma/subtrai a diferen�a relativa � formula leucocit�ria
Global gCodAnaSFLeuc As String

'Permite preencher a data prevista
        'e data de chegada por defeito na requisi��o, no inserir
Global gPreencheDatasReq As Integer

'Indica se se pretende enviar an�lises para o sonho (factura��o)
Global gEnviaSONHO As Integer

'Indica se se pretende enviar an�lises para o sonho na gest�o de requisi��es
Global gEnviaFact_GesReq As Integer

'Indica se se pretende enviar an�lises para o sonho na valida��o de resultados
Global gEnviaFact_ValRes As Integer

'CHVNG-Espinho
'Indica se se pretende enviar an�lises para o sonho na valida��o de resultados
Global gEnviaFact_ChegaTubo As Integer

'Indica se se pretende abrir o ecr� de marcac�es de consultas provis�rias
Global gMarcacoesPrevias As Integer

Global gDestinoFicheiroBeep As String

'Indica se estamos a usar o novo formato de relatorio para a microbiologia
Global gNovoRelatorioMicro As Integer

'Indica o caminho onde s�o gravados os relat�rios
Global gGesDocPathRelatorios As String

' INDICAM O TEMPO MAXIMO PARA CADA UMA DAS SITUACOES (ESTAT. TEMPOS)
Global gTempoMaximoURG As String
Global gTempoMaximoCON As String
Global gTempoMaximoINT As String

' INDICA SE IMPRIME PARA IMPRESSORAS DO TIPO EPL(1) OU ZPL(0)
' H MILITAR -> 1
Global gImprimeEPL As Integer

' NUMERO MECANOGRAFICO DO MEDICO EXECUTANTE
' HMILITAR
Global gNumMecanMedExec As String

' N�mero de etiquetas administrativas por defeito
Global gNumEtiqAdminDefeito As Integer

' INTEGRA��O COM DESKTOP DO M�DICO
' HMILITAR
Global gIntegracaoDesktopMedico As Integer

' CODIGO DA FOLHA DE TRABALHO DAS ELECTROFORESES
' HMILITAR
Global gFolhaTrabElectroforese As String

' CODIGO DA ANALISE PROTEINAS PERTENCENTE A ELECTROFORESE
' HMILITAR
Global gCodAnaProteinas As String

' (1/0) UTILIZA NUM SEQUENCIAL NO SONHO
' CHVNG
Global gUtilizaNumSeqSONHO As Integer

'Chave cujo conteudo indica a sala de acesso no Sislab
Global gCodSala As Integer


Global gLinhaAzul As Integer
Global gMostraAnalisesImpressas As Integer
Global gMostraAnalisesSemTubo As Integer

' INDICA SE CONCEITO DE ASSINATURA DA REQUISICAO ESTA ACTIVO OU NAO
' FGONCALVES - ALTEREI - 2007.10.09
Global gAssinaturaActivo As Integer

' PERMITE IMPRIMIR ETIQUETAS PARA TUBOS DIRECTAMETNE DO ECRA DE PENDENTES
' FGONCALVES - ALTEREI - 2007.10.30
Global gImprimeEtiqPendentes As Integer


' CODIGOS DAS ENTIDADES PT E GNR
' FGONCALVES - ALTEREI - 2007.11.14
Global gCodEfrPT As Integer
Global gCodEfrGNR As Integer

Global gBloqueiaAplicacao As Integer
Global gTimeOutActivo As Boolean
Global gTimeOutMinutos As Integer
Global gTimeOutActivoTerminaAplicacao As Integer


Global gEfrParticular As String

Global gPermiteMudarComputador As String

' NUMERO DE ETIQUETAS DE TUBOS POR FICHA CRUZADA
Global gNumeroEtiquetasFichaCruzada As Integer

' OBRIGA PREENCHIMENTO DE COMPUTADOR NA ENTRADA
Global gObrigaPreencherComputadorEntrada As Integer

'Indica se utiliza lista de m�dicos no form de gestao de requisi��es privado
Global gListaMedicos As Integer

'Indica se utiliza s�rie unica do n�mero de requisi��o na marca��o de c0lheitas
Global gSerieUnicaMarcacao As Integer

'Indica se emite etiquetas para os tubos na marca��o de colheitas
Global gEtiqTubosMarcacao As Integer

'Indica se utiliza ficha cruzada
Global gFichaCruz As Integer

' INDICA SE USA NOVO ECRA DE RESULTADOS
Global gNovoEcraResultados As Integer

' INDICA TABELA USADA PELA ARS
Global gTabelaARS As Integer
    
Global gPedidoElectronico As Integer

Global gReciboCrystal As Integer

'Indica o c�digo da sala a colocar por defeito no ecr� de gest�o de requisi��o

Global gCodSalaDefeito As String

'INDICA SE USA OU NAO DOCUMENTOS DE CAIXA (TERRUGEM COLOCAR A 1)

Global gDocCaixa As Integer

'Indica se a op��o de Resultados Geral � apenas usada para consulta
Global gConsultaResGeral As Integer

'Indica se utiliza etiquetas pr�-impressas com reserva do n�mero de requisi��o
'N�o � possivel com esta chave, gerar n�meros de requisi��es
Global gNReqPreImpressa As Integer

Global gEtiqFim As Integer

' INDICA SE PERMITE IMPRESSAO DE COPIAS E SEGUNDAS VIAS DE RECIBOS
Global gUsaCopiaRecibo As Integer
Global gNumCopiasRecibo As Integer

'Indica se coloca por defeito a op��o de agrupamento de requisi��es activa no ecr� de resultados novo
Global gOptAgrupaRequis As Integer

'INDICA SE USA SAIDA DE TUBOS. USADO NO CHAM PARA REGISTAR SAIDA DE TUBOS DE STIRSO PARA FAMALICAO (1/0)
Global gUsaSaidaTubos As Integer

'SOLIVEIRA
Global GCodAnaActMed As String

'INDICA SE USAMOS NOVA VERS�O DAS FOLHAS DE TRABALHO
Global gUsaNovaFolhaTrab As Integer

'INDICA SE DESTINGUIMOS REQUISI��ES FIM SEMANA
Global gUsaFimSemana As Integer

'INDICA SE ENVIA POR EMAIL
Global gEnvioEmail As Integer

' INDICA SE PRETENDE IMPRIMIR DOC DE CAIXA
Global gImprimeDocCaixa As Integer

' ANALISE SE NAO TIVER COD_LOCAL, PERTENCE A TODOS LOCAIS
Global gAnaliseMultiLocal As Integer

' INDICA SE USA OU N�O DELTA CHECK
Global gUsaDeltaCheck As Integer

' INDICA SE USA  UM PRODUTO UNICO POR EXAME
Global gUsaProdutoPorExame As Integer

' INDICA SE USA  Interferencias
Global gUsaInterferencia As Integer


' INDICA SE IMPRIME GRUPO MICROBIOLOGIA NAS ANALISES PENDENTES NO RELATORIO FINAL. DR PAULO ACHA QUE NAO FAZ SENTIDO.
' HCVP QUER QUE SAIA.
Global gImprimeMicroAnaPendentes As Integer

' INDICA SE MARCA AS ANA�LISES SEM RESULTADO COMO IMPRESSAS QUANDO SE VALIDA.
Global gMarcaSemResultadoImpressa As Integer

' INDICA SE POR DEFEITO RETIRA O VISTO DAS CHECKBOX 'VALOR REF' E 'UNIDADES'
Global gNaoSeleccionaValRefUnid As Integer

' INDICA SE INIBE A CODIFICACAO DE ANALISES ATRAVES DO ECR� DE RESULTADOS
Global gInibeCodifAnalises  As Integer

' INDICA SE USA REQUISI��O ASSOCIADA
Global gUsaReqAssoc  As Integer

' INDICA SE IMPRESSAO DE PRE IMPRESSAS USA ZPL (0/1)
Global gImprimePreImpressasZPL As Integer

' INDICA SE USA NOVO ECRA DE RESULTADOS PARA MICROBIOLOGIA
Global gNovoEcraResultadosMicro As Integer

' INDICA SE VALIDA EPISODIO NO SONHO/GH AO INSERIR REQUISICAO
Global gValidaEpisodioInserir As Integer

' USA NOVO ECRA DE CODIFICACACOES ANALISES SIMPLES
Global gUsaNovoEcraCodAnaSimples As Integer

' GUARDA CODIGO DA ANALISE DOMICILIO NAO URBANO
Global gCodAnaDomNaoUrbano As String

' GUARDA CODIGO DA ANALISE DOMICILIO URBANO
Global gCodAnaDomUrbano As String

' IGNORA O PROCEDIMENTO DO FACTUS PARA RETORNAR TAXAS AOS UTENTES
Global gNaoUsaProcFactusTaxas As Integer

' IGNORA SE UTENTE TEM GRUPO SANGUINEO OU NAO. IMPRIME CARTOES DOS UTENTES  INSCRITOS
Global gImprCartoesInscritos As Integer

' CODIGO QUE COLOCA POR DEFEITO O GENERO DO UTENTE
Global gCodGeneroDefeito As Integer

' GRUPO DE RUBRICAS ANALISES CLINICAS NO FACTUS
Global gGrupoRubricaFactus As Long

' INDICA SE ENCRIPTA E DESENCRIPTA CORREIO
Global gEncriptaCorreio As Integer

Global gImprEtiqParametroQTD As Integer

Global gConjuntoAnalisesRequisicao As String

' INDICA SE USA NOVA FUNCIONALIDADE DE ASSOCIACAO DE PROFILES
Global gUsaProfiles As String

' INDICA SE PODE ENTRAR EM CONTA COM A HORA NO ECRA DE PENDENTES
Global gUsaHoraPendentes As String

' CODIGO DA ANALISE OBSERVACOES
Global gCodAnaObs As String

' CODIGO DA RUBRICA PARA FINS DE SEMANA FIXOS
Global gCodRubrFDS As String

' INDICA SE AO VALIDAR NAOOOO ENVIA AUTOMATICAMENTE PARA ERESULTS
Global gNaoEnviaEresultsValidacao As String

' USA NOVA IMPRESSAO DE ETIQUETAS
Global gUsaNovaImpEtiquetas As String

'INDICA SE IMPORTA SEMPRE UTENTE DA GESTADO DE DOENTES
Global gImportaSempreUtente As Integer

'Na impress�o de etiquetas tira uma etiqueta com todas as analises pedidas
Global gImprimirEtiqResumo As Integer

'IMPRIME FOLHA DE RESUMO PARA IMPRESSORA RECIBOS (CORDEIRO)
Global gImprFolhaResumoParaRecibos As Integer

'INDICA SE ORDENA A PENDENTES DETALHADA POR NUM REQUIS OU DATA/HORA
Global gOrdenaPendentesNumReq As Integer

'INDICA SE NO FIM DA IMPRESSAO DOS GRUPOS SE IMPRIME FOLHA DIVERSOS
Global gImprimeFolhaTrabDiversos As Integer

'INDICA SE CONTROLA A DATA DO ENVIO PELA DATA DO PRECARIO
Global gControlaDataPrecario As Integer

'INDICA SE USA CONTROLO QUALIDADE
Global gUsaControloQualidade As Integer

'INDICA SE USA FILA DE ESPERA
Global gUsaFilaEspera As Integer

'INDICA SE ENVIA CAMPO LABORATORIO PARA TABELA ANALISES_IN
Global gEnviaLocalSONHO As Integer

'INDICA SE OBRIGA QUE CAMPO UTILIZADOR EXISTA NA GESTAO HOSPITALAR
Global gConfirmaUtilizadorGH As Integer

'INDICA SE CONFIRMA QUE O CAMPO RUBRICA EXISTE NO FACTUS/GH
Global gConfirmaRubrica As Integer

' INDICA SE IMPRIME AUTOMATICAMENTE AS ETIQUETAS
Global gImprAutoEtiq As Integer

' INDICA CAMINHO PARA FICHEIROS FACTURACAO (SISBIT - HLEIRIA)
Global gCaminhoFacturacaoSISBIT As String

' MOSTRA AVISO AO IMPRIMIR ANALISE INCOMPLETA
Global gMostraAvisoIncompletas As Integer

    
' INDICA A VERSAO DO MODULO DE CORREIO A SER UTILIZADO
Global gVersaoModuloCorreio As String

' INDICA SE ENVIO DE SMS ESTA ACTIVO
Global gEnvioSMSActivo As String

' INDICA SE CONTROLO STOCKS ESTA ACTIVO
Global gUsaStocks As String

' INDICA SE CONTROLO STOCKS VERSAO 2 ESTA ACTIVO
Global gUsaStocksV2 As Integer

' INDICA SE USA SEROTECA
Global gUsaSeroteca As String

' INDICA SE ABRE O FORM DE ENVIO PARA FACTURACAO PRIVADO - ICIL
Global gAbreEnvioFactusPrivado As Integer

' INDICA SE PERMITE IMPRIMIR ETIQUETAS ARS NO ECRA DE ENVIO
Global gImprimeEtiqARS As Integer


Global gPathIE As String

Global gEresults_Link As String

' NAO DEIXA IMPRIMIR SEM ESTAR ASSINADA
Global gApenasImprimeAssinadas As Integer

' VERSAO DE CHEGADA DE TUBOS
Global gVersaoChegadaTubos As String

' INDICA O CODIGO DE CANCELAMENTO DE TUBOS - TUBO SEM ANALISES
Global gMotivoCancelamentoTuboSemAnalises As Integer

Global gUsaUtilizadoresColheitas As Integer

' PERMITE ESCOLHER PASTA PARA ONDE COPIAR PDF NO ENVIO DE EMAIL
Global gEscolhePastaEmail As Integer

' IMPRIME RELATORIO ORDENADOPELA ORDEM DO GRUPO DE IMPRESSAO
Global gImprimeRelOrderGrImp As Integer

' INDICA SE EXPANDE TREE VIEW AUTO - TUBOS V2
Global gExpandeAutoTubosV2 As Integer

' INDICA SE PERMITE PESQUISA POR CODIGO
Global gPermitePesquisaCodigo As Integer

' INDICA SE SINAVE ESTA ACTIVO
Global gSinave As Integer

' CAMINHO PARA CHAMAR SINAVE
Global gCaminhoSinave As String

' CODIGO DO TIPO DE MOVIMENTO PARA ABERTURA E FECHO DE LOTE
Global gCodMovFechoLote As Integer
Global gCodMovAberLote As Integer

' INDICA SE USA N_REQ PARA GERAR ETIQUETAS PRE_IMPRESSAS
Global gUsaNReqPreImpressa As Integer

' INDICA SE USA GEST�O DOCUMENTAL
Global gUsaGestaoDocumental As Integer

' INDICA SE PREENCHE A SALA AO INTRODUZIR NUM REQ PRE IMPRESSO
Global gPreencheSalaDefeitoPreImpressa As Integer

' 0/1 AO ABRIR ECRA RESULTADOS BLOQUEIA ANALISES
Global gUsaBloqueioAnalises As Integer

Global gIdioma As String

Global gVERSAO_PARAM_AMB As String

' APENAS ENVIA POR EMAIL REQUISICOES IMPRESSAS (MANSO)
Global gEnviaEmailApenasImpressas As Integer

' INDICA SE USA SERIE ANUAL PARA RECIBOS
Global gUsaSerieAnualRec As Integer

' INDICA SE IMPRIME (INDVIDUALMENTE) REQUISICOES APENAS COM VALIDA��O TECNICA
Global gImprimeRequisValTec As Integer

' INDICA QUAL O TIPO DE ACTIVIDADE QUE EST� ACTIVA NO LABORAT�RIO
Global gTipoActividade As String

' INDICA SE USA OU N�O TABELAS DE HISTORICO PARA CARREGAR RESULTADOS ANTERIORES
Global gUsaTabelaHistorico

' MARGEM CORREC��O DA FORMULA LEUCOCITARIA (CASA DECIMAL = .)
Global gMargemFormulaLeucocitaria As Double

' INDICA SE PERMITE VALIDAR AUTOMATICAMENTE OU NAO (0/1)
Global gUsaValidacaoAutomatica As Integer

' INDICA SE USA MARCACAO DE ANALISES PELO CODIGO DA ENTIDADE
Global gUsaMarcacaoCodAnaEFR As Integer

' INDICA SE O MULTILOCAL FUNCIONA COMO AMBITO E NAO LOCAL FISICO
Global gMultiLocalAmbito As Integer

' INDICA SE O NA IDENTIFICA��O DE UTENTES PRETENDE INFORMA�AO DO UTENTE COMO OUVIU FALAR DO LABORATORIO LCC (0/1)
Global gFeedBackUtente As Integer

' INDICA SE USA NOVA VERS�O PARA IMPRESSAO DE RESULTADOS
Global gUsaNovaVersaoImprResultados As Integer

' NAO DEIXA EDITAR OBSERVACOES DAS ANALISES QUE J� ESTEJAM VALIDADAS
Global gNaoEditaObsAnaValidadas As Integer

' INDICA SE OPCAO ESTA DISPONIVEL OU NAO PARA BLOQUEAR REQUISICOES
Global gBloqueioRequisicoes As Integer

' INDICA NUMERO DE DIAS QUE APARECE POR DEFEITO NO ECRA DE PENDENTES DETALHADO
Global gDiasReqPendAna As Integer

' AMGIENTE HOSPITALAR - INDICA SE VAI BUSCAR PRECOS AO GH
Global gPrecosAmbienteHospitalar As Integer


' INDICA NUMERO DE MINUTOS QUE DEMORA A FAZER REFRESH. SE <= 0 NAO FAZ REFRESH
Global gMinutosRefreshPendentes As Integer

' INDICA SE IMPRIME ETIQUETA DE TUBOS PRIMARIOS NA CHEGADA DE TUBOS, AUTOMATICAMENTE
Global gImprimeEtiqTuboPrimChegadaTubo As Integer

' SE ACTIVO NAO REFRESCA ECRA DE REQUISICOES PENDENTES ELECTRONICAS
Global gNaoRefrescaReqPedElectr As Integer

' INDICA SE CQ TRABALHA SOBRE A MEDIA OBTIDA OU MEDIA ALVO
Global gCQUsaMediaObtida As Integer

' INDICA SE IMPRIME OU NAO VALOR DAS ANALISES NOS RECIBOS COM DESCONTO > 0
Global gControlaDesconto As Integer

' VERIFICA SE UTENTE TEM MAIS REQUISICOES NAS ULTIMAS 24HORAS
Global gVerificaReq24Horas As Integer

'DISPONIBILIZA OU NAO O BOTAO PARA ENVIAR RESULTADOS POR EMAIL.
Global gEnviaEmailEcraResultados As Integer

' INDICA SE APARECE FRAME OBSERVACOES MAXIMIZADAS
Global gFrameObsMaximizadas As Integer

' INDICA SE USA O MESMO RELATORIO PARA EMAIL E  IMPRESSAO
Global gUsaMesmoRelatorioEmail As Integer

' INDICA SE ENVIO DE RESULTADOS POR SMS ESTA ACTIVO
'RGONCALVES 16.06.2013 ICIL-470
Global gEnvioResultadosSMSActivo As String
Global gEnvioSMSAvulsoActivo As String
Global gInibeDataChegada As Integer

' INDICA SE IMPRIME ANALISES SEM RESULTADO COM INDICA��O DE ESTAR EM CURSO.
Global gImprimeAnalisesSemResultado As Integer

' INDICA Os utilizadores para enviar mensagem automatica quando do pedido de nova amostra. separados por ;
Global gUtilEnvioMensagemPedNovaAmostra As String

' INDICA SE ENVIA RELATORIO PARA ERESULTS QUANDO DA CHEGADA DO PRIMEIRO TUBO(0/1)
Global gEnviaRelatorioEresultsChegada As Integer


' INDICA SE MODULO DE VIGILANCIA EPIDEMIOLOGICA ESTA ATIVO (0/1)
Global gUsaVigilanciaEpidemiologica As Integer

Global gAIDAWebService As String

'INDICA O INTERVALO PARA ANALISE DE UM MICRORGANISMO DUPLICADO
Global gIntervaloTempoRepetidosMicro  As Integer

'INDICA O NUMERO MAXIMO DE ERROS MINOR ADMISSIVEIS.
Global gNumeroErrosMinorMicro As Integer

'INDICA O CODIGO DA ENTIDADE A SER GERADA NAS REFERENCIAS MULTIBANCO
Global gEntidadeMultibanco As Long

' INDICA SE A COLHEITA � EFETUADA NO ECRA DE CHEGADA/COLHEITA DE TUBOS
Global gRegistaColheitaAtiva As Integer

'INDICA SE USA A DATA DO TUBO PARA MOSTRAR POR DEFAULT PARA MOSTRAR AS AN�LISES NO ECR� DE ANALISES PENDENTES
Global gUsaDataTuboPendentes As Integer

Global gModuloAgendaV2 As Integer

'SE ATIVO INDICA QUE O LOCAL UTILIZADO NAS FOLHAS TRABALHO � REFERENTE AO LOCAL DA REQUISI��O E NAO LOCAL DE EXECU��O DAS ANALISES
Global gFolhaTrabLocalReq As Integer

'SE ACTIVO INDICA QUE CHEGADA DE TUBO PASSA A SER EFETUADA POR SEQ_REQ_TUBO_LOCAL E N�O POR COD_ETIQ
Global gSeqReqTuboLocal As Integer

' VARIAVEIS PARA IMPLEMENTACAO DO MODULO DE COLHEITAS
Global gGestaoColheita As Integer
Global gMaxTempoColheita As Long

Global gPassaRecFactus As Integer

Global GSerieNotaCred As String

Sub CarregaParamAmbiente()
    
    On Error Resume Next
    
    Dim aux As String
    BG_CarregaParamAmbiente
    
    gSGBD = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SGBD")
    gSGBD_SECUNDARIA = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SGBD_SECUNDARIA")
    gVersaoSGBD = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "VERSAO_gSGBD")
    
    
    ' Sistema operativo.
    gSistemaOperativo = UCase(Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SISTEMA_OPERATIVO")))

    gInstituicao = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "INSTITUICAO")
    gTipoInstituicao = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TIPO_INSTITUICAO")
    
    'Liga��o � HIS
    gHIS_Import = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "HIS_IMPORT")
    HIS.nome = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "HIS_Nome")
    HIS.DataSource = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "HIS_DataSource")
    HIS.uID = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "HIS_UID")
    HIS.PWD = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "HIS_PWD")
    HIS.LoginTimout = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "HIS_LoginTimeout")
    
    'coloca valores na variaveis dos tipos de resultado
    ' Carrega os c�digos "constantes" codificados na tabela Paramet.

    'Tipo de disposi��es de folhas de trabalho
    gT_Horizontal = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_HORIZONTAL"))
    gT_Vertical = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_VERTICAL"))
    gT_Livre = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_LIVRE"))
    'Estado civil
    gT_Solteiro = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_SOLTEIRO"))
    gT_Casado = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_CASADO"))
    gT_Viuvo = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_VIUVO"))
    gT_Divorciado = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_DIVORCIADO"))
    gT_Sep_Judicial = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_SEP_JUDICIAL"))
    gT_Uniao_Facto = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_UNIAO_FACTO"))
    gT_Outro = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_OUTRO"))
    'Sexo
    gT_Feminino = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_FEMININO"))
    gT_Masculino = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_MASCULINO"))
    gT_Indeterminado = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_INDETERMINADO"))
    'Resposta Sim/N�o
    gT_Sim = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_SIM"))
    gT_Nao = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_NAO"))
    'Utilizadas na parametriza�ao de ambiente
    'Permiss�o
    gT_NaoVisualizar = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_NAOVISUALIZAR"))
    gT_ApenasVisualizar = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_APENASVISUALIZAR"))
    gT_VisualizarAlterar = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_VISUALIZARALTERAR"))
    'Ambito
    gT_Geral = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_GERAL"))
    gT_Computador = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_COMPUTADOR"))
    gT_Utilizador = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_UTILIZADOR"))
    'Tipos de contactos
    gT_Telefone = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_TELEFONE"))
    gT_Email = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_EMAIL"))
    gT_Fax = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_FAX"))
    gT_ImpressoraRede = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_IMPRESSORAREDE"))
    'Def. utilizadas na gest�o de datas/idades
    gT_Dias = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_DIAS"))
    gT_Meses = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_MESES"))
    gT_Anos = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_ANOS"))
    gT_Crianca = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_CRIANCA"))
    gT_Adulto = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_ADULTO"))
    
    'Tipo de resultados de an�lises
    gT_Alfanumerico = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_ALFANUMERICO"))
    gT_Numerico = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_NUMERICO"))
    gT_Frase = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_FRASE"))
    gT_Microrganismo = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_MICRORGANISMO"))
    gT_Antibiograma = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_ANTIBIOGRAMA"))
    gT_Auxiliar = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_AUXILIAR"))
    gT_Relatorio = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_RELATORIO"))
    'Estado da requisi��o
    
    gT_Acidente = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_ACIDENTE"))
    gT_Ambulatorio = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_AMBULATORIO"))
    gT_Cons_Inter = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_CONS_INTER"))
    gT_Cons_Telef = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_CONS_TELEF"))
    gT_Consulta = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_CONSULTA"))
    gT_Consumos = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_CONSUMOS"))
    gT_Credenciais = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_CREDENCIAIS"))
    gT_Diagnosticos = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_DIAGNOSTICOS"))
    gT_Exame = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_EXAME"))
    gT_Ficha_ID = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_FICHA_ID"))
    gT_Fisio = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_FISIO"))
    gT_HDI = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_HDI"))
    gT_Internamento = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_INTERNAMENTO"))
    gT_Intervencao = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_INTERVENCAO"))
    gT_MCDT = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_MCDT"))
    gT_Plano_Oper = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_PLANO_OPER"))
    gT_Pre_Intern = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_PRE_INTERN"))
    gT_Prescricoes = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_PRESCRICOES"))
    gT_Prog_Cirugico = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_PROG_CIRUGICO"))
    gT_Protoc = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_PROTOC"))
    gT_Referenciacao = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_REFERENCIACAO"))
    gT_Reg_Oper = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_REG_OPER"))
    gT_Tratamentos = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_TRATAMENTOS"))
    gT_Urgencia = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_URGENCIA"))
    
    gT_Externo = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_EXTERNO"))
    gT_LAB = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_LAB"))
    gT_RAD = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_RAD"))
    gT_Bloco = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "T_BLOCO"))
    
    'Tipos de EFR
    gEFRCodificada = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "EFR_CODIFICADA"))
    gEFRNaoCodificada = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "EFR_N_CODIFICADA"))
    gEFRIndependente = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "EFR_INDEPENDENTE"))
    gEFRBancos = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "EFR_BANCOS"))
    'Serie da requisi��o
    gSerieRec = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SERIE_REC_ACTIVA"))

    
    'carrega o vector de entidades gARS
    EFR_CarregaVector_gARS gARS, "ARS"
    EFR_CarregaVector_gARS gADSE, "ADSE"


    ' verificar se � vers�o de demonstra��o
    gDEMO = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "DEMO")

    'Verificar o modo de debug
    gModoDebug = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ModoDebug")
    
    'coloca valores do grupos de utilizadores
    gGrupoAdministradores = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GrupoAdministradores")
    gGrupoMedicos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GrupoMedicos")
    gGrupoEnfermeiros = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GrupoEnfermeiros")
    gGrupoTecnicos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GrupoTecnicos")
    gGrupoSecretariado = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GrupoSecretariado")
    gGrupoChefeSecretariado = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GrupoChefeSecretariado")
    gGrupoExternos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GrupoUtilizadorExterno")
    gGrupoFarmaceuticos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GRUPOFARMACEUTICOS")
    'rcoelho 22.08.2013 chvng-4412
    gGrupoTecnicosSuperiores = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GrupoTecnicosSuperiores")
    gGrupoTecSupValidacao = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GrupoTecSupValidacao")
    
    'Convers�o do Euro (utilizado nos �crans do factus)
    gConvEur = BL_String2Double(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CONV_EUR"))
    'Moeda Activa
    gMoedaActiva = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MOEDA_ACTIVA"))
    
    'Indica se � obrigat�rio o grupo na selec��o de resultados
    gObrigaGrupo = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "OBRIGA_GRUPO_RES"))

    'Vari�vel que indica a utiliza��o ou n�o de recibos na aplica��o
    gRecibos = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "UTILIZAR_RECIBOS"))
    
    'Tamanho da letra no Report Resultados
    gResTamLetra = CInt(Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "RES_TAM_LETRA")))
    If gResTamLetra = -1 Then
        gResTamLetra = 8
    End If
    
    gResTipoLetra = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "RES_TIPO_LETRA"))
    If gResTipoLetra = "" Or gResTipoLetra = "-1" Then
        gResTipoLetra = "Microsoft Sans Serif"
    End If
    
    gNomeCapitalizado = CInt(Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NOME_CAPITALIZADO")))
    If gNomeCapitalizado = -1 Then gNomeCapitalizado = 0
    
    'Variaveis usadas na factura��o
    gCodGrupoRubr = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_GRUPO_RUBR")
    gCodTipRubr = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_TIP_RUBR")
    gCodServExec = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_SERV_EXEC")
    gCodServExecFACTUS = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_SERV_EXEC_FACTUS")
    gCodProg = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_PROG")
    
    'conexao secundaria (usada na facturacao)
    gConSec = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CONEXAO_SECUNDARIA")
    
    'conexao secundaria (usada na facturacao)
    gConInterfaces = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CONEXAO_INTERFACES")
    
    'Codigo da isen��o a colocar por defeito
    gCodIsencaoDefeito = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_ISENCAO_DEFEITO")
    
    'Indica se � para imprimir a morada no report de resultados
    gImprimeMoradaRes = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ImprimirMoradaRes")
    
    'Indica o c�digo das an�lises simples a inserir por bot�o (usada no FormResultados)
    gCodAnaSimples1 = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_ANA_SIMPLES1")
    gCodAnaSimples2 = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_ANA_SIMPLES2")
    
    'Indica se � para abrir ecr� de reports
    gMultiReport = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MULTI_REPORT")
    
    'conexao a GesDoc
    gDSNGesDoc = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "DSN_GESDOC")
    
    'Url GesDoc
    gPutServlet = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GD_PutServlet")
    
    'Variaveis usadas na liga��o � GesDoc
    gGDServerPort = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GD_ServerPort")
    gGDServerAddr = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GD_ServerAddr")
    gGDServerPath = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GD_ServerPath")
    
    'Url GesDoc - editar doc
    gGDEditDoc = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GD_EditDoc")
    
    'id da pasta default (GesDoc)
    gGDDefFolder = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GD_DefFolder")
    
    'id do tipo de documento (GesDoc)
    gGDDocType = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GD_DocType")

    
    ' Indica se a aplica��o escreve as requisic��es na tabela do GESCOM 'gc_requisicoes'.
    ' 0 : N�o
    ' 1 : Sim
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "REQ_GESCOM"))
    If (aux = "1") Then
        gReqGESCOM = True
    Else
        gReqGESCOM = False
    End If

    ' Indica se o login � case sensitive.
    ' 0 : N�o
    ' 1 : Sim
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "LOGIN_CASE_SENSITIVE"))
    If (aux = "1") Then
        gLoginCaseSensitive = True
    Else
        gLoginCaseSensitive = False
    End If
    
    ' Indica se o SISLAB envia documentos para o eResults.
    ' 0 : N�o
    ' 1 : Sim
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS"))
    If (aux = "1") Then
        geResults = True
    Else
        geResults = False
    End If
    
    ' --------------------------------------------------------------------
    '  eResults
    ' --------------------------------------------------------------------
    
    ' Indica se o SISLAB envia documentos para o eResults.
    ' 0 : N�o
    ' 1 : Sim
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS"))
    If (aux = "1") Then
        geResults = True
    Else
        geResults = False
    End If
    
    ' Indica a pasta onde s�o enviados os documentos para o eResults.
    ' Ex. : C:\exp\
    geResults_Pasta = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_PASTA"))
    
    ' Impressora para PDF para o eResults.
    ' Ex. : Acrobat Distiller
    geResults_Impr_PDF = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_IMPR_PDF"))
    
    ' Pasta local dos ficheiros para o eResults.
    ' Ex. : C:\Projectos\SISLAB\Bin\pdf\
    geResults_Local = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_LOCAL"))
    
    ' Pasta temp para os ficheiros do eResults.
    geResults_Temp = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_TEMP"))
    
    ' C�digo de origem desta aplica��o, para o eResults.
    gER_ORIGEM = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_ORIGEM"))

    ' Mapa de resultados simples (Relat�rio de Ensaios).
    gER_MAPA_RES_SIMPLES = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_MAPA_RES_SIMPLES"))

    ' Encripta��o de documentos para o eResults.
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_ENCRIPTA_DOC"))
    If (aux = "1") Then
        gENCRIPTA_DOC = True
    Else
        gENCRIPTA_DOC = False
    End If

    ' Encripta��o de ficheiros de Indexa��o para o eResults.
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_ENCRIPTA_F_IND"))
    If (aux = "1") Then
        gENCRIPTA_F_IND = True
    Else
        gENCRIPTA_F_IND = False
    End If
    
    ' --------------------------------------------------------------------
        
    'Indica se se pode imprimir o boletim de resultados apenas com valida��o tecnica
    gImprimirComValTec = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIMIR_COM_VAL_TEC")
    
    'Indica as analises complexas cujas as analises simples podem
    'ser feitas em 2 aparelhos diferentes e cada aparelho tem
    'valores de referencia diferentes
    gAnaComp2Aparelhos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ANA_COMP_2_APARELHOS")
    
    'Indica o laborat�rio
    gLAB = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "LAB")
    
    gValidarReqAssociada = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "VALIDAR_REQ_ASSOCIADA")
    
    gPesquisaSempreUtenteNaGH = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PESQUISA_SEMPRE_UTENTE_NA_GH")
    
    gImprimeEFRRes = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIMIR_EFR_RES")
    
    gTextoValidacaoTecnicaBoletim = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TEXTO_VAL_TEC_BOLETIM")
    
    gEstadoInfoResultados = 1 'Minimizado
    
    gImpressaoRemota = 0
    gImpressaoRemota = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRESSAO_REMOTA")
    
    'Indica se � para o botao "CopiarAnterior" copiar dados anteriores do utente e n�o da requisi��o
    gCopiarAnteriorUtente = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CopiarAnterior_utente")
    
    gModeloMapaRes = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MODELO_MAPA_RES")
    gLinhasPagina = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "LINHAS_PAGINA")
    
    'Indica o c�digo da an�lise simples onde se soma/subtrai a diferen�a relativa � formula leucocit�ria
    gCodAnaSFLeuc = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_ANAS_FLEUC")
    
    ' Indica se se pode introduzir c�digos de an�lise do SONHO na gest�o de Requisi��es.    Aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MAPEIA_COD_SONHO"))
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MAPEIA_CODS_SONHO"))
    If (aux = "1") Then
        gMapeiaSONHO = True
    Else
        gMapeiaSONHO = False
    End If

    ' Indica se se permite a valida��o tecnica.
'    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SO_VAL_MEDICA"))
'    If (aux = "1") Then
'        gSoValMedica = True
'    Else
'        gSoValMedica = False
'    End If
    
    ' Indica se o epis�dio fica igual ao n�mero de requisi��o por defeito.
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "EPISODIO_IGUAL_NUM_REQ"))
    If (aux = "1") Then
        gEpisodioIgualNumReq = True
    Else
        gEpisodioIgualNumReq = False
    End If

    ' N�mero de etiquetas administrativas X n.�grupos, por defeito.
    gNumEtiqAdmin = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NumEtiqAdmin"))

    ' Numero de etiquetas administrativas por defeito (indepentente das etiquetas para grupos)
    gNumEtiqAdminDefeito = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NumEtiqAdminDefeito"))
    
    ' Indica se, ao registar, se procura  a requisi��o mais recente com esta an�lise pendente, para este utente.
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PROCANPENDENTES"))
    If (aux = "1") Then
        gProcAnPendentesAoRegistar = mediSim
    Else
        gProcAnPendentesAoRegistar = mediNao
    End If

    gPreencheDatasReq = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PREENCHE_DATAS_REQ")
    
    'Indica para onde se pretende enviar as an�lises para factura��o
    gSISTEMA_FACTURACAO = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SISTEMA_FACTURACAO"))

    'Indica se se pretende enviar an�lises para o sonho (factura��o)
    gEnviaSONHO = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENVIA_SONHO")
    
    'Indica se se pretende enviar an�lises para o sonho na gest�o de requisi��es
    gEnviaFact_GesReq = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENVIA_FACT_GESREQ")
    
    'Indica se se pretende enviar an�lises para o sonho na valida��o de resultados
    gEnviaFact_ValRes = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENVIA_FACT_VALRES")
    
    'CHVNG-Espinho
    'Indica se se pretende enviar an�lises para o sonho na valida��o de resultados
    gEnviaFact_ChegaTubo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENVIA_FACT_CHEGATUBO")
    
    'Indica se se pretende abrir o ecr� de marca��es de consultas provis�rias
    gMarcacoesPrevias = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MARCACOES_PREVIAS")
    
    ' Indica o esquema do GESCOM.
    ' Ex. : GESCOM2002.
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ESQUEMA_GESCOM"))
    If (aux = "-1") Then
        gEsquemaGescom = "GESCOM."
    Else
        gEsquemaGescom = UCase(Trim(aux))
    End If
    
    ' Indica o tipo de pesquisa em FormPesqRapidaAvancadaMultiSel :
    ' True  : LIKE 'XXXX%'
    ' False : LIKE '%XXXX%'
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PESQUISA_DENTRO_CAMPO"))
    If (aux = "1") Then
        gPesquisaDentroCampo = True
    Else
        gPesquisaDentroCampo = False
    End If
    
    ' Destino das etiquetas Crystal.
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ETIQ_PREVIEW"))
    If (aux = "1") Then
        ' Preview.
        gEtiqPreview = True
    Else
        ' Impressora.
        gEtiqPreview = False
    End If
    
    'Preenche EFR do Utente para a gest�o de requisi��es
    gPreencheEFRUtente = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PREENCHE_EFR_UTENTE"))
    
    'Preenche EFR do episodio da GH preenchido na gest�o de requisi��es
    gPreencheEFREpisodioGH = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PREENCHE_EFR_EPISODIOGH"))
    
    ' Na gest�o de requisi��es, obriga a ter um produto ao inserir ou modificar, se
    ' a requisi��o tiver an�lises.
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "OBRIGA_PRODUTO"))
    If (aux = "1") Then
        gObrigaProduto = True
    Else
        gObrigaProduto = False
    End If

    ' Na gest�o de requisi��es : Bot�o para alterar o n�mero de epis�dio.
    aux = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "BT_ALTERA_EPISODIO"))
    If (aux = "1") Then
        gBtAlteraEpisodio = True
    Else
        gBtAlteraEpisodio = False
    End If

    gNovoRelatorioMicro = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NOVO_RELATORIO_MICRO"))
    
    gGesDocPathRelatorios = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GesDocPathRelatorios"))
    
    'carrega o vector de an�lises do tipo microrganismos
    ANALISE_CarregaVector_gMicro gMicro, "MICRO"
    
    'Carrega o vector de analises a nao facturar na gestao de requisicoes
    ANALISE_CarregaVector_gMicro gAnaNaoFacturaGESREQ, "ANA_NAO_FACT_GESREQ"
    
    'carrega, caso preenchido, o c�digo da an�lise a inserir por defeito no SONHO
    gCodAnaSONHO = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_ANA_SONHO")
    
    'indica se preenchem o episodio para fins de factura��o ou nao
    gPreencheEpis = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PREENCHE_EPIS")
    
    'Indica se utiliza o form de chegada de tubos para dar entrada do produto e dos tubos
    gRegChegaTubos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "REGISTA_CHEGADA_TUBOS")
    
    ' Indica o codigo da analise que N�O entra para a estatistica de tempos
    gCodAnaLownstein = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_ANA_LOWNSTEIN")
       
    ' Indica o Codigo do grupo de Microbiologia que n�o entra para a estatistica de tempos
    gCodGrupoMicrobiologia = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_GRUPO_MICROBIOLOGIA")
    gCodGrupoMicobacteriologia = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_GRUPO_MICOBACTERIOLOGIA")
    ' Indica o C�digo de proveniencia do servi�o de URGENCIA
    gCodProvenUrgencia = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_PROVEN_URGENCIA")

    'Indica se utiliza o novo registo de frases no form de Resultados
    gNovoRegFrases = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NOVO_REG_FRASES")
    
    'Chave cujo conte�do indica a apresenta��o ou n�o da caixa de registo de produtos e tubos associados a an�lise
    gIgnoraPerguntaProdutoTubo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IGNORA_PERGUNTA_PRODUTO_TUBO")
    
        
    'Chave cujo conteudo indica de fam�lias na informa��o clinica
    gUsaFamilias = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_FAMILIAS")
    
    ' INDICAM O TEMPO MAXIMO PARA CADA UMA DAS SITUACOES (ESTAT. TEMPOS)
    gTempoMaximoURG = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TEMPO_MAX_URG")
    gTempoMaximoCON = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TEMPO_MAX_CON")
    gTempoMaximoINT = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TEMPO_MAX_INT")

    gImprimeEPL = BG_DaParamAmbiente_NEW(mediAmbitoComputador, "IMPRIME_EPL")
    If gImprimeEPL = -1 Then
        gImprimeEPL = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIME_EPL")
    End If
    
    ' NUMERO MECANOGRAFICO DO MEDICO EXECUTANTE PARA ENVIAR PARA FACTURACAO
    gNumMecanMedExec = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NUM_MECAN_MED_EXEC")
    
    'Indica se a factura��o � efectuada apenas para requisi��es fechadas
    gFacturaFechadas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "FACTURA_FECHADAS")
    
    'Indica o c�digo do utilizador HL7 no Sislab
    gCodUtilizadorHL7 = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_UTILIZADOR_HL7")
    
    'Indica se quer apenas o ultimo utilizador de valida��o por grupo no relat�rio
    gValidacaoUnicaPorGrupo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "VALIDACAO_UNICA_POR_GRUPO")
    
    'Indica se epis�dio obrigat�rio na gest�o de requisi��es
    gEpisObrigatorio = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "EPISODIO_OBRIGATORIO")
    
    ' indica se existe ou n�o integra��o com o desktop m�dico
    gIntegracaoDesktopMedico = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "INTEGRACAO_DESKTOP_MEDICO")
    
    gFolhaTrabElectroforese = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "FOLHA_TRAB_ELECTROFORESE")
    
    gCodAnaProteinas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_ANA_PROTEINAS")
    
    gMostraColheita = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MOSTRA_COLHEITA")
    
    'Indica se envia para a factura��o por processo do utente
    gFacturaPorProcesso = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "FACTURA_POR_PROCESSO")

    gUtilizaNumSeqSONHO = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "UTILIZA_NUM_SEQ_SONHO")
    
    
    gCodEFRSemCredencial = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_EFR_SEM_CREDENCIAL")
    
    gUsaMarcacaoARS = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_MARCACAO_ARS")

    gMostraAnalisesImpressas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MOSTRA_ANALISES_IMPRESSAS")
    gMostraAnalisesSemTubo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MOSTRA_ANALISES_SEM_TUBO")
    gLinhaAzul = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "LINHA_AZUL")

    ' FGONCALVES - ALTEREI - 2007.10.09
     gAssinaturaActivo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ASSINATURA_ACTIVO")
     
     gImprimeEtiqPendentes = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIME_ETIQ_PENDENTES")
     
    ' pferreira 2007.11.13
    ' M�dulo Correio -------------------------------------------------------------------------------------
    
    ' pferreira 2007.11.13
    gActivarCorreio = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ACTIVAR_CORREIO")
    ' pferreira 2007.11.13
    gTimerCorreio = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TIMER_CORREIO")
    ' pferreira 2007.11.15
    gPathAnexos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PATH_ANEXOS")
    ' pferreira 2007.12.14
    gTabelaUtilizadores = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TABELA_UTILIZADORES")
    ' pferreira 2007.12.14
    gCampoCodUtilizador = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CAMPO_COD_UTILIZADORES")
    ' pferreira 2007.12.14
    gCampoNomeUtilizador = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CAMPO_NOME_UTILIZADORES")
    ' pferreira 2007.12.14
    gTabelaCorreio = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TABELA_CORREIO")
    ' pferreira 2007.12.14
    gTabelaCorreioUtil = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TABELA_CORREIO_UTIL")
    ' pferreira 2007.12.14
    gTabelaCorreioAnexos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TABELA_CORREIO_ANEXOS")
    ' pferreira 2007.12.14
    gTabelaCodListas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TABELA_COD_LISTAS")
    ' pferreira 2007.12.14
    gTabelaCodListasMembros = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TABELA_COD_LISTAS_MEMBROS")
    ' pferreira 2007.12.19
    gTabelaCrAnexos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TABELA_CR_CORREIO_ANEXOS")
    ' pferreira 2007.12.19
    gTabelaCrDestinatarios = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TABELA_CR_CORREIO_DESTINATARIOS")
    ' pferreira 2007.12.19
    gTabelaCrMensagem = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TABELA_CR_CORREIO_MENSAGEM")
    
    ' ----------------------------------------------------------------------------------------------------
    
     gCodEfrPT = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_EFR_PT")
     gCodEfrGNR = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_EFR_GNR")
     
    gBloqueiaAplicacao = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "BLOQUEIA_APLICACAO")
    'gTimeOutActivo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TIME_OUT_ACTIVO")
    gTimeOutMinutos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TIME_OUT_MINUTOS")
    gTimeOutActivoTerminaAplicacao = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TIME_OUT_ACTIVO_TERMINA_APLICACAO")
    gEfrParticular = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "EFR_PARTICULAR")
    gPermiteMudarComputador = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PERMITE_MUDAR_COMPUTADOR")
    gNumeroEtiquetasFichaCruzada = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NUMERO_ETIQUETAS_FICHA_CRUZADA")
    gObrigaPreencherComputadorEntrada = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "OBRIGA_PREENHCER_COMPUTADOR_ENTRADA")
    
    gListaMedicos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "LISTA_MEDICOS")
    
    gSerieUnicaMarcacao = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SERIE_UNICA_MARCACAO")
    
    gEtiqTubosMarcacao = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ETIQ_TUBOS_MARCACAO")
    
    'soliveira lacto
    gFichaCruz = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "FICHA_CRUZADA")
    
    'FGONCALVES - TERRUGEM
    gMostraMgmPagaRecibo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MOSTRA_MGM_PAGA_RECIBO")
    
    'FGONCALVES - LHL
    gNovoEcraResultados = BG_DaParamAmbiente_NEW(mediAmbitoComputador, "USA_NOVO_ECRA_RESULTADOS")
    If gNovoEcraResultados = mediComboValorNull Then
        gNovoEcraResultados = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_NOVO_ECRA_RESULTADOS")
    End If

    'FGONCALVES - LHL
    gTabelaARS = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_NOVO_ECRA_RESULTADOS")
    
    'Indica se existe algum tipo de pedido electr�nico de an�lises
    gPedidoElectronico = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PEDIDO_ELECTRONICO")

    gReciboCrystal = BG_DaParamAmbiente_NEW(mediAmbitoComputador, "RECIBO_CRYSTAL")
    
    If gReciboCrystal = mediComboValorNull Then
        gReciboCrystal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "RECIBO_CRYSTAL")
    End If
    
    'indica o c�digo da sala a colocar por defeito no ecr� de gest�o de requisi��es
     gCodSalaDefeito = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_SALA_DEFEITO")
    
    'INDICA SE USA OU NAO DOCUMENTO DE CAIXA
     gDocCaixa = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "DOC_CAIXA")
     
     'Indica se a op��o de Resultados Geral � apenas usada para consulta
     gConsultaResGeral = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CONSULTA_RES_GERAL")
     
     'Indica se utiliza etiquetas pr�-impressas com reserva do n�mero de requisi��o
     'N�o � possivel com esta chave, gerar n�meros de requisi��es
     gNReqPreImpressa = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NREQ_PRE_IMPRESSA")
     
     'Indica se utilizam etiqueta de fim
     gEtiqFim = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ETIQ_FIM")
      
     
     gUsaCopiaRecibo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_COPIA_RECIBO")
     gNumCopiasRecibo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NUM_COPIAS_RECIBO")
     
     'Indica se coloca por defeito a op��o de agrupamento de requisi��es activa no ecr� de resultados novo
     gOptAgrupaRequis = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "OPT_AGRUPA_REQUIS")
     
     'INDICA SE USA SAIDA DE TUBOS. USADO NO CHAM PARA REGISTAR SAIDA DE TUBOS DE STIRSO PARA FAMALICAO (1/0)
     gUsaSaidaTubos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_SAIDA_TUBOS")
     
     GCodAnaActMed = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_ANA_ACT_MED")
     
    'INDICA SE USAMOS NOVA VERS�O DAS FOLHAS DE TRABALHO
    gUsaNovaFolhaTrab = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_NOVA_FOLHA_TRAB")

    'INDICA SE DESTINGUIMOS REQUISI��ES FIM SEMANA
    gUsaFimSemana = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_FIM_SEMANA")

    'INDICA SE  ENVIA RESULTADOS POR EMAIL
    gEnvioEmail = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENVIO_EMAIL")

    'INDICA SE  IMPRIME DOCUMENTO DE CAIXA
    gImprimeDocCaixa = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIME_DOC_CAIXA")

    ' SE PERMITE QUE UMA ANALISE PERTEN�A MAIS QUE UM LOCAL (NAO TER COD_LOCAL PREENCHIDO)
    gAnaliseMultiLocal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ANALISE_MULTI_LOCAL")
    
    ' INDICA SE USA OU NAO DELTA CHECK
    gUsaDeltaCheck = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_DELTA_CHECK")

    gUsaProdutoPorExame = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_PRODUTO_POR_EXAME")
    
    ' INDICA SE USA OU NAO INTERFERENCIAS
    gUsaInterferencia = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_INTERFERENCIA")
    
    ' INDICA SE IMPRIME GRUPO MICROBIOLOGIA NAS ANALISES PENDENTES NO RELATORIO FINAL.
    gImprimeMicroAnaPendentes = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIME_MICRO_ANA_PENDENTES")
    
    ' INDICA SE MARCA AS ANA�LISES SEM RESULTADO COMO IMPRESSAS QUANDO SE VALIDA.(0/1)
    gMarcaSemResultadoImpressa = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MARCA_SEM_RESULTADO_IMPRESSA")

    ' INDICA SE POR DEFEITO RETIRA O VISTO DAS CHECKBOX 'VALOR REF' E 'UNIDADES' (0/1)
    gNaoSeleccionaValRefUnid = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NAO_SELECCIONA_VAL_REF_UNID")
    
    ' INDICA SE INIBE A CODIFICACAO DE ANALISES ATRAVES DO ECR� DE RESULTADOS(0/1)
    gInibeCodifAnalises = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "INIBE_CODIF_ANALISES")

    gUsaReqAssoc = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_REQ_ASSOC")
    
     ' INDICA SE IMPRESSAO DE PRE IMPRESSAS USA ZPL (0/1)
    gImprimePreImpressasZPL = BG_DaParamAmbiente_NEW(mediAmbitoComputador, "IMPRIME_PRE_IMPRESSAS_ZPL")
    If gImprimePreImpressasZPL = mediComboValorNull Then
        gImprimePreImpressasZPL = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIME_PRE_IMPRESSAS_ZPL")
    End If
    
    ' INDICA SE USA NOVO ECRA DE RESULTADOS PARA MICROBIOLOGIA
    gNovoEcraResultadosMicro = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NOVO_ECRA_RESULTADOS_MICRO")


    gValidaEpisodioInserir = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "VALIDA_EPISODIO_INSERIR")

    ' USA NOVO ECRA DE CODIFICACACOES ANALISES SIMPLES
    gUsaNovoEcraCodAnaSimples = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_NOVO_ECRA_COD_ANA_SIMPLES")
    
    ' GUARDA CODIGO DA ANALISE DOMICILIO NAO URBANO
    gCodAnaDomNaoUrbano = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_ANA_DOM_NAO_URBANO")

    ' GUARDA CODIGO DA ANALISE DOMICILIO  URBANO
    gCodAnaDomUrbano = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_ANA_DOM_URBANO")

    gNaoUsaProcFactusTaxas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NAO_USA_PROC_FACTUS_TAXAS")
    
    gImprCartoesInscritos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPR_CARTOES_INSCRITOS")
    
    gCodGeneroDefeito = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_GENERO_DEFEITO")

    gGrupoRubricaFactus = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GRUPO_RUBRICA_FACTUS")

    gEncriptaCorreio = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENCRIPTA_CORREIO")
    
    gImprEtiqParametroQTD = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPR_ETIQ_PARAMETRO_QTD")

    gConjuntoAnalisesRequisicao = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CONJUNTO_ANALISES_REQUISICAO")

    ' INDICA SE USA NOVA FUNCIONALIDADE DE ASSOCIACAO DE PROFILES
    gUsaProfiles = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_PROFILES")
    
    ' INDICA SE PODE ENTRAR EM CONTA COM A HORA NO ECRA DE PENDENTES
    gUsaHoraPendentes = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_HORA_PENDENTES")
    
    ' INDICA CODIGO DA ANALISE OBSERVACOES
    gCodAnaObs = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_ANA_OBS")
    
    ' CODIGO DA RUBRICA PARA FINS DE SEMANA FIXOS
    gCodRubrFDS = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_RUBR_FDS")
    
    ' INDICA SE AO VALIDAR NAOOOO ENVIA AUTOMATICAMENTE PARA ERESULTS
    gNaoEnviaEresultsValidacao = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NAO_ENVIA_ERESULTS_VALIDACAO")
    
    ' USA NOVA IMPRESSAO DE ETIQUETAS
    gUsaNovaImpEtiquetas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_NOVA_IMP_ETIQUETAS")
    
    'INDICA SE IMPORTA SEMPRE UTENTE DA GESTADO DE DOENTES
    gImportaSempreUtente = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPORTA_SEMPRE_UTENTE")
    
    'Na impress�o de etiquetas tira uma etiqueta com todas as analises pedidas
    gImprimirEtiqResumo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMP_ETIQ_RESUMO")
    
    ' DATA SOURCE PARA LIGAR AO ERESULTS
    geResultsDSN = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_DSN")
    
    ' UTILIZADOR BASE DADOS ERESULTS
    geResultsUTIL = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_UTIL")
    
    ' PASSWORD PARA LIGAR AO ERESULTS
    geResultsPWD = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_PWD")
    
    ' CODIGO DO DOCUMENTO ORIGEM DO SISLAB
    geResultsCodOrigem = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_COD_ORIGEM")
    
    ' TIPO DO DOCUMENTO ORIGEM DO SISLAB
    geResultsTipoDoc = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_TIPO_DOCUMENTO")
    
    'IMPRIME FOLHA DE RESUMO PARA IMPRESSORA RECIBOS (CORDEIRO)
    gImprFolhaResumoParaRecibos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPR_FOLHA_RESUMO_PARA_RECIBOS")
    
    ' ORDENA PENDENTES DETALHADA POR NUMERO DE REQUISI��O E N�O POR DATA HORA(CORDEIRO)
    gOrdenaPendentesNumReq = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ORDENA_PENDENTES_NUM_REQ")
    
    'INDICA SE NO FIM DA IMPRESSAO DOS GRUPOS SE IMPRIME FOLHA DIVERSOS
    gImprimeFolhaTrabDiversos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIME_FOLHA_TRAB_DIVERSOS")

    'INDICA SE CONTROLA A DATA DO ENVIO PELA DATA DO PRECARIO
    gControlaDataPrecario = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CONTROLA_DATA_PRECARIO")
    
    'INDICA SE USA CONTROLO QUALIDADE
    gUsaControloQualidade = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_CONTROLO_QUALIDADE")
    
    'INDICA SE USA FILA ESPERA
    gUsaFilaEspera = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_FILA_ESPERA")
    
    'INDICA SE ENVIA PARA SONHO O CODIGO DO LOCAL
    gEnviaLocalSONHO = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENVIA_LOCAL_SONHO")
    
    'INDICA SE OBRIGA QUE CAMPO UTILIZADOR EXISTA NA GESTAO HOSPITALAR
    gConfirmaUtilizadorGH = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CONFIRMA_UTILIZADOR_GH")

    'INDICA SE CONFIRMA QUE O CAMPO RUBRICA EXISTE NO FACTUS/GH
    gConfirmaRubrica = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CONFIRMA_RUBRICA")

    'INDICA SE IMPRIME AUTOMATICAMENTE AS ETIQUETAS
    gImprAutoEtiq = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPR_AUTO_ETIQ")
    
    ' INDICA CAMINHO PARA FICHEIROS FACTURACAO (SISBIT - HLEIRIA)
    gCaminhoFacturacaoSISBIT = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CAMINHO_FACTURACAO_SISBIT"))

    ' MOSTRA AVISO AO IMPRIMIR ANALISE INCOMPLETA
    gMostraAvisoIncompletas = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MOSTRA_AVISO_INCOMPLETAS"))

   
    ' INDICA A VERSAO DO MODULO DE CORREIO A SER UTILIZADO
    gVersaoModuloCorreio = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "VERSAO_MODULO_CORREIO")
    
    ' INDICA SE ENVIO DE SMS ESTA ACTIVO
    gEnvioSMSActivo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENVIO_SMS_ACTIVO")
    
    ' INDICA SE USA CONTROLO STOCKS
    gUsaStocks = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_STOCKS")
    gUsaStocksV2 = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_STOCKS_V2")
    
    ' INDICA SE USA SEROTECA
    gUsaSeroteca = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_SEROTECA")
    
    ' INDICA SE ABRE FORM DE ENVIO PARA FACTUS PRIVADO - ICIL
    gAbreEnvioFactusPrivado = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ABRE_ENVIO_FACTUS_PRIVADO")
    
    ' INDICA SE PERMITE IMPRIMIR ETIQUETAS ARS NO ECRA DE ENVIO
    gImprimeEtiqARS = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIME_ETIQ_ARS")


    ' PATH DO INTERNET EXPLORER
    gPathIE = BG_DaParamAmbiente_NEW(mediAmbitoComputador, "PATH_IE")
    If gPathIE = "-1" Or gPathIE = "" Then
        gPathIE = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PATH_IE")
    End If
    
    ' LINK PARA ERESULTS
    gEresults_Link = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ERESULTS_LINK")

    ' NAO DEIXA IMPRIMIR SEM ESTAR ASSINADA
    gApenasImprimeAssinadas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "APENAS_IMPRIME_ASSINADAS")

    ' VERSAO DE CHEGADA DE TUBOS
    gVersaoChegadaTubos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "VERSAO_CHEGADA_TUBOS")

    ' INDICA O CODIGO DE CANCELAMENTO DE TUBOS - TUBO SEM ANALISES
    gMotivoCancelamentoTuboSemAnalises = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TUBO_SEM_ANALISES")

    ' INDICA SE USA TABELA DE UTILIZADORES DE COLHEITAS CODIFICADO.
    gUsaUtilizadoresColheitas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_UTILIZADORES_COLHEITAS")

    ' PERMITE ESCOLHER PASTA PARA ONDE COPIAR PDF NO ENVIO DE EMAIL
    gEscolhePastaEmail = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ESCOLHE_PASTA_EMAIL")

    ' IMPRIME RELATORIO ORDENADOPELA ORDEM DO GRUPO DE IMPRESSAO
    gImprimeRelOrderGrImp = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIME_REL_ORDER_GR_IMP")
    
    ' INDICA A ANALISE (TIPO FRASE) PARA CANCELAMENTO DA AMOSTRA (CHEGADA DE TUBOS V2)
    gAnaliseCancelamentoAmostra = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ANALISE_CANCELAMENTO_AMOSTRA")
    
     ' INDICA SE EXPANDE TREE VIEW AUTO - TUBOS V2
    gExpandeAutoTubosV2 = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "EXPANDE_AUTO_TUBOS_V2")
     
     ' INDICA SE PERMITE PESQUISA DE CODIGO
    gPermitePesquisaCodigo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PERMITE_PESQUISA_CODIGO")
    
     ' SINAVE
    gSinave = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SINAVE")
     
     ' CAMINHO PARA EXECUTAVEL SINAVE
    gCaminhoSinave = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CAMINHO_SINAVE")

    ' CODIGO DO TIPO DE MOVIMENTO PARA ABERTURA E FECHO DE LOTE
    gCodMovFechoLote = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_MOV_FECHO_LOTE")
    
    gCodMovAberLote = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COD_MOV_ABER_LOTE")
    
    ' INDICA SE USA N_REQ PARA GERAR ETIQUETAS PRE_IMPRESSAS
    gUsaNReqPreImpressa = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_NREQ_PRE_IMPRESSA")
    
    ' INDICA SE USA GEST�O DOCUMENTAL
    gUsaGestaoDocumental = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_GESTAO_DOCUMENTAL")

    ' INDICA SE PREENCHE A SALA AO INTRODUZIR NUM REQ PRE IMPRESSO
    gPreencheSalaDefeitoPreImpressa = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PREENCHE_SALA_DEFEITO_PRE_IMPRESSA")

    ' 0/1 AO ABRIR ECRA RESULTADOS BLOQUEIA ANALISES
    gUsaBloqueioAnalises = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_BLOQUEIO_ANALISES")
    

    ' APENAS ENVIA POR EMAIL REQUISICOES IMPRESSAS (MANSO)
    gEnviaEmailApenasImpressas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENVIA_EMAIL_APENAS_IMPRESSAS")

    ' INDICA SE USA SERIE ANUAL PARA RECIBOS
    gUsaSerieAnualRec = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_SERIE_ANUAL_REC")

    ' INDICA SE IMPRIME (INDVIDUALMENTE) REQUISICOES APENAS COM VALIDA��O TECNICA
    gImprimeRequisValTec = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIME_REQUIS_VAL_TEC")
    
    ' INDICA QUAL O TIPO DE ACTIVIDADE QUE EST� ACTIVA NO LABORAT�RIO
    gTipoActividade = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TIPO_ACTIVIDADE")
    
    ' INDICA SE USA OU N�O TABELAS DE HISTORICO PARA CARREGAR RESULTADOS ANTERIORES
    gUsaTabelaHistorico = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_TABELA_HISTORICO")
    
    ' MARGEM CORREC��O DA FORMULA LEUCOCITARIA
    gMargemFormulaLeucocitaria = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MARGEM_FORMULA_LEUCOCITARIA")
    
    ' INDICA SE PERMITE VALIDAR AUTOMATICAMENTE OU NAO (0/1)
    gUsaValidacaoAutomatica = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_VALIDACAO_ATOMATICA")

    ' INDICA SE USA MARCACAO DE ANALISES PELO CODIGO DA ENTIDADE
    gUsaMarcacaoCodAnaEFR = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_MARCACAO_COD_ANA_EFR")

    ' INDICA SE O MULTILOCAL FUNCIONA COMO AMBITO E NAO LOCAL FISICO - CHVNG
    gMultiLocalAmbito = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MULTILOCAL_AMBITO")
    
    ' INDICA SE O NA IDENTIFICA��O DE UTENTES PRETENDE INFORMA�AO DO UTENTE COMO OUVIU FALAR DO LABORATORIO LCC (0/1)
    gFeedBackUtente = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "FEEDBACK_UTENTE")
    
    ' INDICA SE USA NOVA VERS�O PARA IMPRESSAO DE RESULTADOS
    gUsaNovaVersaoImprResultados = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_NOVA_VERSAO_IMPR_RESULTADOS")
    
    ' NAO DEIXA EDITAR OBSERVACOES DAS ANALISES QUE J� ESTEJAM VALIDADAS
    gNaoEditaObsAnaValidadas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NAO_EDITA_OBS_ANA_VALIDADAS")

    ' INDICA SE OPCAO ESTA DISPONIVEL OU NAO PARA BLOQUEAR REQUISICOES
    gBloqueioRequisicoes = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "BLOQUEIO_REQUISICOES")

    ' INDICA NUMERO DE DIAS QUE APARECE POR DEFEITO NO ECRA DE PENDENTES DETALHADO
    gDiasReqPendAna = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "DIAS_REQ_PEND_ANA")
    
    ' AMGIENTE HOSPITALAR - INDICA SE VAI BUSCAR PRECOS AO GH
    gPrecosAmbienteHospitalar = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PRECOS_AMBIENTE_HOSPITALAR")
    
    ' INDICA NUMERO DE MINUTOS QUE DEMORA A FAZER REFRESH. SE <= 0 NAO FAZ REFRESH
    gMinutosRefreshPendentes = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MINUTOS_REFRESH_PENDENTES")

    ' INDICA SE IMPRIME ETIQUETA DE TUBOS PRIMARIOS NA CHEGADA DE TUBOS, AUTOMATICAMENTE
    gImprimeEtiqTuboPrimChegadaTubo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIME_ETIQ_TUBO_PRIMARIO_CHEGADA_TUBO")

    gIdioma = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IDIOMA")

    ' SE ACTIVO NAO REFRESCA ECRA DE REQUISICOES PENDENTES ELECTRONICAS
    gNaoRefrescaReqPedElectr = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NAO_REFRESCA_REQ_PED_ELECTR")

    ' INDICA SE CQ TRABALHA SOBRE A MEDIA OBTIDA OU MEDIA ALVO
    gCQUsaMediaObtida = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CQ_USA_MEDIA_OBTIDA")

    ' INDICA SE IMPRIME OU NAO VALOR DAS ANALISES NOS RECIBOS COM DESCONTO > 0
    gControlaDesconto = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "CONTROLA_DESCONTO")

    ' VERIFICA SE UTENTE TEM MAIS REQUISICOES NAS ULTIMAS 24HORAS
    gVerificaReq24Horas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "VERIFICA_REQ_24_HORAS")

    'DISPONIBILIZA OU NAO O BOTAO PARA ENVIAR RESULTADOS POR EMAIL.
    gEnviaEmailEcraResultados = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENVIA_EMAIL_ECRA_RESULTADOS")

    ' INDICA SE APARECE FRAME OBSERVACOES MAXIMIZADAS
    gFrameObsMaximizadas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "FRAME_OBS_MAXIMIZADAS")
    
    ' INDICA SE USA O MESMO RELATORIO PARA EMAIL E  IMPRESSAO
    gUsaMesmoRelatorioEmail = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_MESMO_RELATORIO_EMAIL")
    
    ' INDICA SE ENVIO DE RESULTADOS POR SMS ESTA ACTIVO
    'RGONCALVES 16.06.2013 ICIL-470
    gEnvioResultadosSMSActivo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENVIO_RESULTADOS_SMS_ACTIVO")
    
    ' INDICA SE ENVIO DE RESULTADOS POR SMS ESTA ACTIVO
    'RGONCALVES 16.06.2013 ICIL-470
    gEnvioSMSAvulsoActivo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENVIO_SMS_AVULSO_ACTIVO")
    
    ' FGONCALVES
    ' INIBE O PREENCHIMENTO DO CAMPO DATA DE CHEGADA NO FORMGESTAOREQUISICAO
    gInibeDataChegada = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "INIBE_DATA_CHEGADA")

    ' INDICA SE IMPRIME ANALISES SEM RESULTADO COM INDICA��O DE ESTAR EM CURSO.
    gImprimeAnalisesSemResultado = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIME_ANALISES_SEM_RESULTADOS")

    ' INDICA Os utilizadores para enviar mensagem automatica quando do pedido de nova amostra. separados por ;
    gUtilEnvioMensagemPedNovaAmostra = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "UTIL_ENVIO_MENSAGEM_PED_NOVA_AMOSTRA")

    ' INDICA SE ENVIA RELATORIO PARA ERESULTS QUANDO DA CHEGADA DO PRIMEIRO TUBO(0/1)
    gEnviaRelatorioEresultsChegada = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENVIA_RELATORIO_ERESULTS_CHEGADA")

    ' INDICA SE MODULO DE VIGILANCIA EPIDEMIOLOGICA ESTA ATIVO (0/1)
    gUsaVigilanciaEpidemiologica = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_VIGILANCIA_EPIDEMIOLOGICA")

    'INDICA O INTERVALO PARA ANALISE DE UM MICRORGANISMO DUPLICADO
    gIntervaloTempoRepetidosMicro = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "INTERVALO_TEMPO_REPETIDOS_MICRO")

    'INDICA O NUMERO MAXIMO DE ERROS MINOR ADMISSIVEIS.
    gNumeroErrosMinorMicro = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NUMERO_ERROS_MINOR_MICRO")
    
    gEntidadeMultibanco = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENTIDADE_MULTIBANCO")
    
    'BRUNODSANTOS 21.06.2016
    gUsaVersaoOffice = BG_DaParamAmbiente_NEW(mediAmbitoComputador, "USA_VERSAO_OFFICE_MANUAL")
    If gUsaVersaoOffice = -1 Then
        gUsaVersaoOffice = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_VERSAO_OFFICE_MANUAL")
    End If
    '
    
    'RGONCALVES 07.07.2015 CHSJ-2082
    gUsaAutenticacaoActiveDirectory = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_AUTENTICACAO_ACTIVE_DIRECTORY")
    gDominioActiveDirectory = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "DOMINIO_ACTIVE_DIRECTORY")
    '
    
    ' INDICA SE A COLHEITA � EFETUADA NO ECRA DE CHEGADA/COLHEITA DE TUBOS
    gRegistaColheitaAtiva = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "REGISTA_COLHEITA_ATIVA")
    

    'INDICA SE USA A DATA DO TUBO PARA MOSTRAR POR DEFAULT PARA MOSTRAR AS AN�LISES NO ECR� DE ANALISES PENDENTES
    gUsaDataTuboPendentes = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_DATA_TUBO_PENDENTES")

    gModuloAgendaV2 = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MODULO_AGENDA_V2")
    
    'BRUNODSANTOS 01.12.2015
    gInformacaoParaUtente = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "INFORMACAO_PARA_UTENTE")
    If gInformacaoParaUtente < 0 Then
        gInformacaoParaUtente = 1
    End If
    'BRUNODSANTOS CHSJ-2670 - 05.05.2016
    gValidaGeraVersaoReport = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "VALIDA_GERA_VERSAO_RELATORIO")
    '
    'RGONCALVES 19.05.2016
    gImprimeEtiqResumoEcraChegadaTubos = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMPRIME_ETIQ_RESUMO_ECRA_CHEGADA_TUBOS")
    
    'SE ATIVO INDICA QUE O LOCAL UTILIZADO NAS FOLHAS TRABALHO � REFERENTE AO LOCAL DA REQUISI��O E NAO LOCAL DE EXECU��O DAS ANALISES
    gFolhaTrabLocalReq = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "FOLHA_TRAB_LOCAL_REQ")
    
    'BRUNODSANTOS 24.02.2017 - Glintt-HS-14510
    gAuntenticaSINAVE = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AUTENTICACAO_SINAVE_WS")
    
    '
    'BRUNODSANTOS 09.03.2017 - Cedivet-206
    gParamPathAnexo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_CAMINHO_ANEXO")
    '
    'BRUNODSANTOS 30.03.2017 - Glintt-HS-14510
    gDefaultProven = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PROVEN_DEFAULT")
    gDefaultPais = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PAIS_DEFAULT")
    '

    'SE ACTIVO INDICA QUE CHEGADA DE TUBO PASSA A SER EFETUADA POR SEQ_REQ_TUBO_LOCAL E N�O POR COD_ETIQ
     gSeqReqTuboLocal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SEQ_REQ_TUBO_LOCAL")

    ' VARIAVEIS PARA IMPLEMENTACAO DO MODULO DE COLHEITAS
    gGestaoColheita = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GESTAO_COLHEITA")
    gMaxTempoColheita = BL_HandleNull(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MAX_TEMPO_COLHEITA"), 0)

    'INDICA SE FATURAǺAO J� E ESCRITA NO FACTUS.
    gPassaRecFactus = BL_HandleNull(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "PASSA_REC_FACTUS"), 0)
    
    'INDICA QUAL A SERIE DEFINIDA PARA EMISSAO DE NOTAS DE CREDITO
    GSerieNotaCred = BL_HandleNull(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SERIE_NOTA_CRED"), "")


    gTipoIsencaoIsento = BL_HandleNull(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TIPO_ISENCAO_ISENTO"), 1)
    gTipoIsencaoNaoIsento = BL_HandleNull(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TIPO_ISENCAO_NAO_ISENTO"), 2)
    gTipoIsencaoBorla = BL_HandleNull(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TIPO_ISENCAO_BORLA"), 4)
    gTipoIsencaoNaoFacturar = BL_HandleNull(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "TIPO_ISENCAO_NAO_FATURAR"), 4)
    
    'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
    gUsaEnvioPDS = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_ENVIO_PDS")
    '
    'BRUNODSANTOS Glintt-HS-17949 10.01.2018
    gVerificaContaFechada = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "VERIFICA_CONTA_FECHADA")
    '
    'NELSONPSILVA 16.01.2018 UnimedVitoria-1152
    gMostraMisDataDiscovery = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MOSTRA_MIS_DATA_DISCOVERY")
    '
    'NELSONPSILVA 08.03.2018 Glintt-HS-18730
    gGestao_Tubos_Default = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GESTAO_TUBOS_DEFAULT")
    '
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    gATIVA_LOGS_RGPD = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ATIVA_LOGS_RGPD")
    '
    'edgar.parada Glintt-HS-17250 LOGIN GA_ACCESS
    gUsa_Autenticacao_GA_ACCESS = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "USA_AUTENTICACAO_GA_ACCESS")
    
    'BRUNODSANTOS ULSNE-1871
    gMudaLocalAnaliseTubo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MUDA_LOCAL_ANA_TUBO")
       
    'edgar.parada Glintt-HS-19165
    gAtivaESP = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ATIVA_ESP")
    
    'NELSONPSILVA 23.10.2018
    gCompanyDb = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "COMPANY_DB")
    '
    'NELOSNPSILVA CHVNG-47793 29.10.2018
    gAtiva_Reutilizacao_Colheitas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ATIVA_REUTILIZACAO_COLHEITA")
    gAtiva_Nova_Numeracao_Tubo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ATIVA_NOVA_NUMERACAO_TUBO")
    'edgar.parada BACKLOG-11940 (COLHEITAS) 24.01.2019
    gAtiva_Alertas = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ATIVAR_ALERTAS")
    gAtivarAlertasTimer = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ATIVAR_ALERTAS_TIMER")
    '
    'edgar.parada ENTIDADE_ESP
     gEntidadeESP = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ENTIDADE_ESP")
     
    'NELSONPSILVA URL MONITOR
    gMonitorESP = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "MONITOR_ESP")
    
    gFactQrCodeDate = Format(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "GERA_QR_CODE_FACT"), gFormatoData)

End Sub




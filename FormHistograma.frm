VERSION 5.00
Begin VB.Form FormHistograma 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormHistograma"
   ClientHeight    =   9480
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   14490
   Icon            =   "FormHistograma.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9480
   ScaleWidth      =   14490
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodAna 
      Height          =   285
      Left            =   2400
      TabIndex        =   1
      Top             =   8160
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox EcNumReq 
      Height          =   285
      Left            =   840
      TabIndex        =   0
      Top             =   8160
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "FormHistograma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 30/03/2003
' T�cnico : Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String
Dim CriterioTabelaD As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim picbox(10) As PictureBox



Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " "
    Me.left = 5
    Me.top = 5
    Me.Width = 13395
    Me.Height = 9840 ' Normal
    Me.BorderStyle = 0
    
    NomeTabela = ""
    Set CampoDeFocus = Nothing
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

   
    
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"

'    Me.opC.Value = True
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    Dim i As Integer
    BG_StackJanelas_Pop
    Set gFormActivo = FormResultadosNovo
    BL_ToolbarEstadoN 0
    For i = 1 To UBound(picbox)
        Set picbox(i) = Nothing
    Next
    Set FormHistograma = Nothing
End Sub

Sub LimpaCampos()
    EcNumReq = ""
    EcCodAna = ""


End Sub

Sub DefTipoCampos()
    
    

End Sub

Sub PreencheValoresDefeito()
    'nada

End Sub

Sub PreencheCampos()
    
'nada
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
    End If


End Sub

Sub FuncaoEstadoAnterior()
'nada
End Sub



Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsGrafico As New ADODB.recordset
    Dim i As Integer
    Dim c As Integer
    Dim colunas As Integer
    On Error GoTo TrataErro
    c = 3000
    If EcNumReq = "" Or EcCodAna = "" Then Exit Sub
    
    sSql = "SELECT * FROM sl_res_grafico_banda WHERE n_Req = " & EcNumReq
    sSql = sSql & " AND cod_ana = " & BL_TrataStringParaBD(EcCodAna)
    sSql = sSql & " AND (flg_invisivel IS NULL OR flg_invisivel = 0)"
    sSql = sSql & " ORDER BY ordem ASC "
    rsGrafico.CursorLocation = adUseServer
    rsGrafico.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsGrafico.Open sSql, gConexao
    If rsGrafico.RecordCount > 0 Then
        colunas = CInt(Sqr(rsGrafico.RecordCount) + 0.5)
        i = 1
        While Not rsGrafico.EOF
            Set picbox(i) = Me.Controls.Add("VB.PictureBox", "Picture" & i)
            With picbox(i)
                IM_RetiraImagemBD gDirCliente & "\hemog.jpg", "SELECT grafico FROM sl_res_grafico_banda WHERE seq_grafico_banda = " & BL_HandleNull(rsGrafico!seq_grafico_banda, ""), picbox(i)
            
                .Visible = True
                .Appearance = 0
                .Height = PixelsToTwips(BL_HandleNull(rsGrafico!Height, 200))
                .Width = PixelsToTwips(BL_HandleNull(rsGrafico!Width, 200))
                
                If (i - 1) \ colunas = 0 Then
                    If i = 1 Then
                        .left = 5
                    Else
                        .left = picbox(i - 1).left + picbox(i - 1).Width
                    End If
                Else
                    .left = picbox((i) - colunas).left
                
                End If
                If (i - 1) \ colunas = 0 Then
                    .top = 5
                Else
                    .top = picbox(i - colunas).top + picbox(i - colunas).Height
                End If
            End With
            i = i + 1
            rsGrafico.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar", True
    Exit Sub
    Resume Next
End Sub

Sub FuncaoAnterior()
    
'nada
End Sub

Sub FuncaoSeguinte()
    
    'nada
End Sub

Sub FuncaoModificar()
    'nada
End Sub

Sub FuncaoInserir()
'nada
End Sub



Sub FuncaoRemover()
'nada
End Sub


Private Function PixelsToTwips(pixels As Integer)
PixelsToTwips = pixels * Screen.TwipsPerPixelX
End Function

Private Function TwipsToPixels(twips As Integer)
TwipsToPixels = twips / Screen.TwipsPerPixelX
End Function

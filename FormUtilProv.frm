VERSION 5.00
Begin VB.Form FormUtilProv 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Associa��o Utilizadores\Proveni�ncia"
   ClientHeight    =   2205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6030
   Icon            =   "FormUtilProv.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2205
   ScaleWidth      =   6030
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcSeqUtilProv 
      Height          =   285
      Left            =   720
      TabIndex        =   9
      Top             =   1680
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcCodProven 
      Height          =   285
      Left            =   3840
      TabIndex        =   7
      Top             =   1680
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcCodUtilizador 
      Height          =   285
      Left            =   2160
      TabIndex        =   4
      Top             =   1680
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ComboBox CbUtilizador 
      Height          =   315
      Left            =   1200
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   240
      Width           =   4695
   End
   Begin VB.ComboBox CbProv 
      Height          =   315
      Left            =   1200
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   720
      Width           =   4695
   End
   Begin VB.Label Label4 
      Caption         =   "EcSeqUtilProv"
      Height          =   255
      Left            =   720
      TabIndex        =   8
      Top             =   1440
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "EcCodProven"
      Height          =   255
      Left            =   3840
      TabIndex        =   6
      Top             =   1440
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "EcCodUtilizador"
      Height          =   255
      Left            =   2160
      TabIndex        =   5
      Top             =   1440
      Width           =   1215
   End
   Begin VB.Label Lbutilizador 
      Caption         =   "Utilizador"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   240
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Proveni�ncia"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   975
   End
End
Attribute VB_Name = "FormUtilProv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Private Sub Form_Activate()
    
    Call EventoActivate
    
End Sub

Private Sub Form_Load()
    
    Call EventoLoad
    
End Sub



Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    Call BL_InicioProcessamento(Me, "Inicializar �cran.")
    
    'Preenche Combos
    Call PreencheValoresDefeito
    
    'Define Campos do ecran e das tabelas
    Call Inicializacoes
    
    'Define o tipo de campos
    Call DefTipoCampos
    
    Call BG_ParametrizaPermissoes_ADO(Me.Name)
    
    estado = 1
    Call BG_StackJanelas_Push(Me)
    Call BL_FimProcessamento(Me)
    
End Sub

Private Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormUtilProv = Nothing

End Sub

Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub

Private Sub Inicializacoes()
        
    Me.caption = " Associa��o Utilizadores\Proveni�ncia"
    
    Me.left = 800
    Me.top = 800
    Me.Width = 6120
    Me.Height = 1620
    
    
    Set CampoDeFocus = CbUtilizador
    
    NomeTabela = "sl_util_prov"
    
    NumCampos = 3
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_util_prov"
    CamposBD(1) = "cod_util"
    CamposBD(2) = "cod_proven"
    
    'Campos do Ecr�
    Set CamposEc(0) = EcSeqUtilProv
    Set CamposEc(1) = CbUtilizador
    Set CamposEc(2) = CbProv
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Utilizador"
    TextoCamposObrigatorios(2) = "Proveni�ncia"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_util_prov"
    Set ChaveEc = EcSeqUtilProv
        
        
End Sub
Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
End Sub

Public Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
    
End Sub

Public Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcSeqUtilProv.Text = BG_DaMAX("sl_util_prov", "seq_util_prov") + 1
        
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        
        BL_FimProcessamento Me
    End If
    
End Sub
Private Function ValidaCamposEc() As Integer

    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function
Public Sub FuncaoProcurar()
    
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_util ASC, cod_proven ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."

        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoRemover()
    
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
    
End Sub

Public Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoEstadoAnterior()
    
     If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Private Sub PreencheValoresDefeito()
    
    Dim i As Integer
    Dim s As String
    
    'Carrega as ComboBox:
    
    'Utilizadores
    BG_PreencheComboBD_ADO "SELECT nome,Cod_Utilizador FROM sl_idutilizador WHERE flg_removido=0", "Cod_Utilizador", "Nome", CbUtilizador
    
    'Proveni�ncias
    BG_PreencheComboBD_ADO "sl_proven", "cod_proven", "descr_proven", CbProv
    
End Sub

Private Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
            
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    
End Sub

Private Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    rs.Requery
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    LimpaCampos
    PreencheCampos
    
End Sub

Private Sub BD_Update()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    rs.Requery
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
End Sub

Private Sub LimpaCampos()
    
    CbUtilizador.ListIndex = -1
    CbProv.ListIndex = -1
    
    EcSeqUtilProv.Text = ""
    EcCodUtilizador.Text = ""
    EcCodProven.Text = ""
    
End Sub

Private Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
    End If
    
End Sub


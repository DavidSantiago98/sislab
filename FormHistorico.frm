VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormHistorico 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Passagem a Hist�rico"
   ClientHeight    =   2145
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6960
   Icon            =   "FormHistorico.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2145
   ScaleWidth      =   6960
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame3 
      Height          =   615
      Left            =   120
      TabIndex        =   13
      Top             =   1440
      Width           =   6735
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   220
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   0
         Min             =   1e-4
         Max             =   13
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1455
      Left            =   1920
      TabIndex        =   1
      Top             =   0
      Width           =   4935
      Begin VB.TextBox EcReq 
         Height          =   285
         Left            =   960
         TabIndex        =   5
         Top             =   960
         Width           =   1215
      End
      Begin VB.CommandButton BtPassagem 
         Caption         =   "&Transferir"
         CausesValidation=   0   'False
         Height          =   855
         Left            =   3960
         Picture         =   "FormHistorico.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Efectuar Passagem"
         Top             =   480
         Width           =   855
      End
      Begin MSComCtl2.DTPicker DTPicker_Inicio 
         Height          =   300
         Left            =   960
         TabIndex        =   12
         Top             =   450
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   144965633
         CurrentDate     =   37447
      End
      Begin MSComCtl2.DTPicker DTPicker_Fim 
         Height          =   300
         Left            =   2640
         TabIndex        =   11
         Top             =   450
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   144965633
         CurrentDate     =   37447
      End
      Begin VB.TextBox EcDataFim 
         Height          =   285
         Left            =   2760
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   480
         Width           =   975
      End
      Begin VB.TextBox EcDataInicio 
         Height          =   285
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   480
         Width           =   975
      End
      Begin VB.Label LaDe 
         Caption         =   "De"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Laa 
         Caption         =   "a"
         Height          =   255
         Left            =   2280
         TabIndex        =   9
         Top             =   480
         Width           =   255
      End
      Begin VB.Label LaReq 
         Caption         =   "Requisi��o"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   960
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1455
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   1815
      Begin VB.OptionButton Option1 
         Caption         =   "Por Datas"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   3
         Top             =   480
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Por Requisi��o"
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   2
         Top             =   840
         Width           =   1455
      End
   End
End
Attribute VB_Name = "FormHistorico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 25/02/2002
' T�cnico Paulo Costa

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Private Sub DefTipoCampos()

    'Tipo Inteiro
    EcReq.Tag = adInteger
   
    'Tipo Data
    EcDataInicio.Tag = adDBTimeStamp
    EcDataFim.Tag = adDBTimeStamp
    
    EcDataInicio.MaxLength = 10
    EcDataFim.MaxLength = 10
    
End Sub

Private Sub BtPassagem_Click()
    
    'Efectua as transfer�ncias
    
    ProgressBar1.value = ProgressBar1.Min
    DoEvents
    
    If MDIFormInicio.Tag = "HISTORICO" Then
        'Passagem a Hist�rico:
        Call TransfereRes(True)
    Else
        Call TransfereRes(False)
   End If
        
    ProgressBar1.value = ProgressBar1.Min
    DoEvents
    
End Sub


Private Sub DTPicker_Fim_Change()

    EcDataFim.Text = BG_CvData(str(DTPicker_Fim.value))
    'DTPicker_Inicio.Value = DateAdd("d", -6, DTPicker_Fim.Value)

End Sub

Private Sub DTPicker_Inicio_Change()

    EcDataInicio.Text = BG_CvData(str(DTPicker_Inicio.value))
    'DTPicker_Fim.Value = DateAdd("d", 6, DTPicker_Inicio.Value)

End Sub

Private Sub EcDataFim_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDataFim_Validate(Cancel As Boolean)
    
    If BG_ValidaTipoCampo_ADO(Me, CampoActivo) = False Then
        Cancel = True
    End If
        
End Sub

Private Sub EcDataInicio_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDataInicio_Validate(Cancel As Boolean)
    
    If BG_ValidaTipoCampo_ADO(Me, CampoActivo) = False Then
        Cancel = True
    End If
    
End Sub

Private Sub EcReq_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcReq_KeyPress(KeyAscii As Integer)
    
    If Not IsNumeric(Chr(KeyAscii)) And KeyAscii <> 8 Then KeyAscii = 0
    
End Sub

Private Sub EcReq_Validate(Cancel As Boolean)
    
    Dim RsRequis As ADODB.recordset
    Dim i As Integer
    Dim n As Integer
    
    If Trim(EcReq.Text) <> "" Then
    
        Set RsRequis = New ADODB.recordset
        
        With RsRequis
            
            If MDIFormInicio.Tag = "HISTORICO" Then
                'Passagem a Hist�rico
                .Source = "SELECT n_req FROM sl_requis WHERE n_req= " & EcReq.Text & " AND estado_req='F'"
            Else
                'Passagem a Activo
                .Source = "SELECT n_req FROM sl_requis WHERE n_req= " & EcReq.Text & " AND estado_req='H'"
            End If
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        n = RsRequis.RecordCount
        If n = 0 Then
            RsRequis.Close
            Set RsRequis = Nothing
            BG_Mensagem mediMsgBox, "A Requisi��o indicada n�o existe!", vbOKOnly + vbExclamation, "Aten��o"
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    End If
    
End Sub

Private Sub Form_Activate()
    
    Call EventoActivate
    
End Sub

Private Sub Form_Load()
    
    Call EventoLoad
    
End Sub

Public Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
        
    BL_InicioProcessamento Me, "Inicializar �cran..."
    
    Call Inicializacoes
    Call DefTipoCampos
    
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    
    BL_FimProcessamento Me
        
End Sub

Public Sub LimpaCampos()

    Me.SetFocus
    
    EcReq.Text = ""
    DTPicker_Fim.value = Bg_DaData_ADO
    DTPicker_Inicio.value = Bg_DaData_ADO
    EcDataFim.Text = DTPicker_Fim.value
    EcDataInicio.Text = DTPicker_Inicio.value
End Sub

Public Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Public Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If
    
End Sub

Public Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDATAFIM"
            EcDataFim = Bg_DaData_ADO
        Case "ECDATAINICIO"
            EcDataInicio = Bg_DaData_ADO
    End Select
    
End Function

Public Sub EventoUnload()
       
    MDIFormInicio.Tag = ""
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormHistorico = Nothing
    
End Sub

Public Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub

Private Sub Inicializacoes()
        
    If MDIFormInicio.Tag = "HISTORICO" Then
        Me.caption = " Passagem a Hist�rico"
    Else
        'POR VALIDA��O
        Me.caption = " Passagem a Activo"
    End If
    
    Me.left = 600
    Me.top = 400
    Me.Width = 7050
    Me.Height = 2520
   
    Set CampoDeFocus = EcDataInicio
    Set CampoActivo = Me.ActiveControl
    
    ProgressBar1.Min = 0
    ProgressBar1.Max = 12
    ProgressBar1.Visible = True
    
    Option1(0).value = True
    
    Dim date_aux As Date
    date_aux = DateAdd("yyyy", -1, Date)
    
    EcDataFim.Text = BG_CvData(str(date_aux))
    DTPicker_Fim.value = date_aux
    
    EcDataInicio.Text = BG_CvData(str(DateAdd("d", -6, date_aux)))
    DTPicker_Inicio.value = DateAdd("d", -6, date_aux)
    
End Sub

Private Sub Option1_Click(Index As Integer)
    
    'Por Datas
    If Index = 0 Then
        LaDe.Enabled = True
        Laa.Enabled = True
        EcDataInicio.Enabled = True
        EcDataFim.Enabled = True
        LaReq.Enabled = False
        EcReq.Enabled = False
        EcReq.Text = ""
    End If
    
    'Pelo N� da Requisi��o
    If Index = 1 Then
        LaReq.Enabled = True
        EcReq.Enabled = True
        LaDe.Enabled = False
        Laa.Enabled = False
        EcDataInicio.Enabled = False
        EcDataFim.Enabled = False
        EcDataInicio.Text = ""
        EcDataFim.Text = ""
    End If
    
End Sub

Private Sub TransfereRes(ByVal PassaHistorico As Boolean)
        
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim Origem As String
    Dim destino As String
    Dim EstadoReqDe As String
    Dim EstadoReqPara As String
    Dim Criterio As String
    
    '****************************************************************************************************************************************
    
    Dim aux_long As Long
    aux_long = gConexao.CommandTimeout
    gConexao.CommandTimeout = 300
    
    '****************************************************************************************************************************************
    
    If PassaHistorico = True Then
        Origem = ""
        destino = "_h"
        EstadoReqDe = "'F'"
        EstadoReqPara = "'H'"
    Else
        Origem = "_h"
        destino = ""
        EstadoReqDe = "'H'"
        EstadoReqPara = "'F'"
    End If
        
    '****************************************************************************************************************************************
    
    ProgressBar1.value = ProgressBar1.Min
    
    If (Trim(EcDataInicio.Text) = "") Then
      EcDataInicio.Text = BG_CvData(str(DTPicker_Inicio.value))
    End If
    If (Trim(EcDataFim.Text) = "") Then
      EcDataFim.Text = BG_CvData(str(DTPicker_Fim.value))
    End If
    
    DoEvents
    DoEvents
    
    'Verifica se os campos est�o todos preenchidos
    'Por Data
    If Option1(0).value = True Then

        If Trim(EcDataInicio.Text) = "" Then
            BG_Mensagem mediMsgBox, "Campo Data Inicio obrigat�rio!", vbOKOnly + vbExclamation
            EcDataInicio.SetFocus
            Exit Sub
        End If
        If Trim(EcDataFim.Text) = "" Then
            BG_Mensagem mediMsgBox, "Campo Data Fim obrigat�rio!", vbOKOnly + vbExclamation
            EcDataFim.SetFocus
            Exit Sub
        End If
        
        Criterio = " AND sl_requis.dt_previ BETWEEN " & BL_TrataDataParaBD(EcDataInicio) & " AND " & BL_TrataDataParaBD(EcDataFim) & " "
    End If
    
    'Por Requisi��o
    If Option1(1).value = True Then
    
        If Trim(EcReq.Text) = "" Then
            BG_Mensagem mediMsgBox, "Campo N� Requisi��o obrigat�rio!", vbOKOnly + vbExclamation
            EcReq.SetFocus
            Exit Sub
        End If
        
        Criterio = " AND sl_requis.N_req=" & EcReq.Text & " "
    End If
        
        
    '****************************************************************************************************************************************

    'Valida
    Dim Cancel As Boolean
    
    If EcReq.Text <> "" Then
        Call EcReq_Validate(Cancel)
        If Cancel = True Then
            EcReq.SetFocus
            Exit Sub
        End If
    End If
    
    If EcDataInicio.Text <> "" Then
        Call EcDataInicio_Validate(Cancel)
        If Cancel = True Then
            EcDataInicio.SetFocus
            Exit Sub
        End If
    End If
    
    If EcDataFim.Text <> "" Then
        Call EcDataFim_Validate(Cancel)
        If Cancel = True Then
            EcDataFim.SetFocus
            Exit Sub
        End If
    End If
        
    '****************************************************************************************************************************************
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    'Efectua as transfer�ncias
    
    BG_BeginTransaction
        
    '________________________________________________________________________________________________________________________________________
        
    DoEvents
    DoEvents
    DoEvents
    
    'Insere SL_REALIZA
    sql = "INSERT INTO sl_realiza" & destino & vbCrLf & _
          " SELECT sl_realiza" & Origem & ".* FROM sl_requis,sl_realiza" & Origem & vbCrLf & _
          " WHERE sl_requis.n_req=sl_realiza" & Origem & ".n_req AND sl_requis.estado_req=" & EstadoReqDe & Criterio
    
    gSQLError = 0
    gSQLISAM = 0
    Call BG_ExecutaQuery_ADO(sql)
    
    If (gSQLError <> 0) Then
        gConexao.CommandTimeout = aux_long
        ProgressBar1.value = ProgressBar1.Min
        BG_RollbackTransaction
        BG_Mensagem mediMsgBox, "Passagem de registos anulada devido a erro!      ", vbOKOnly + vbCritical
        Exit Sub
    End If
        
    DoEvents
    DoEvents
    DoEvents
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    'Insere SL_RES_ALFAN:
    sql = "INSERT INTO sl_res_alfan" & destino & " (" & vbCrLf & _
        " seq_realiza, n_res, result, res_ant1, res_ant2, " & _
        " res_ant3, dt_res_ant1, dt_res_ant2, dt_res_ant3, " & _
        " local_ant1, local_ant2, local_ant3, flg_apar, " & _
        " flg_res_ant1, flg_res_ant2, flg_res_ant3, flg_imprimir) " & _
        " SELECT a.seq_realiza, a.n_res, a.result, a.res_ant1, a.res_ant2," & _
        " a.res_ant3, a.dt_res_ant1, a.dt_res_ant2, a.dt_res_ant3, " & _
        " a.local_ant1, a.local_ant2, a.local_ant3, a.flg_apar, " & _
        " a.flg_res_ant1, a.flg_res_ant2, a.flg_res_ant3, a.flg_imprimir " & _
        " FROM sl_requis,sl_realiza" & Origem & ",sl_res_alfan" & Origem & " a " & vbCrLf & _
        " WHERE sl_requis.n_req=sl_realiza" & Origem & ".n_req AND sl_realiza" & Origem & ".seq_realiza=a.seq_realiza AND sl_requis.estado_req=" & EstadoReqDe & Criterio
          
    gSQLError = 0
    gSQLISAM = 0
    Call BG_ExecutaQuery_ADO(sql)
    If gSQLError <> 0 Then
        gConexao.CommandTimeout = aux_long
        ProgressBar1.value = ProgressBar1.Min
        BG_RollbackTransaction
        BG_Mensagem mediMsgBox, "Passagem de registos anulada devido a erro!      ", vbOKOnly + vbCritical
        Exit Sub
    End If
    
    DoEvents
    DoEvents
    DoEvents
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    'Insere SL_RES_FRASE:
    sql = "INSERT INTO sl_res_frase" & destino & "(seq_realiza, n_res, cod_frase,ord_frase, flg_imprimir," & _
          " flg_apar) " & vbCrLf & _
          " SELECT a.seq_realiza, a.n_res, a.cod_frase, a.ord_frase, a.flg_imprimir," & _
          " A.flg_apar " & _
          " FROM sl_requis,sl_realiza" & Origem & ",sl_res_frase" & Origem & " a " & vbCrLf & _
          " WHERE sl_requis.n_req=sl_realiza" & Origem & ".n_req AND sl_realiza" & Origem & ".seq_realiza=a.seq_realiza AND sl_requis.estado_req=" & EstadoReqDe & Criterio
    
    gSQLError = 0
    gSQLISAM = 0
    Call BG_ExecutaQuery_ADO(sql)
    If gSQLError <> 0 Then
        gConexao.CommandTimeout = aux_long
        ProgressBar1.value = ProgressBar1.Min
        BG_RollbackTransaction
        BG_Mensagem mediMsgBox, "Passagem de registos anulada devido a erro      !", vbOKOnly + vbCritical
        Exit Sub
    End If
    
    DoEvents
    DoEvents
    DoEvents
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    'Insere SL_RES_MICRO:
    sql = "INSERT INTO sl_res_micro" & destino & " ( seq_realiza, n_res, cod_micro, quantif, flg_imp," & _
          " flg_tsq, cod_gr_antibio, prova, flg_testes, res_ant1," & _
          " dt_res_ant1, res_ant2, dt_res_ant2, res_ant3," & _
          " dt_res_ant3, datainsc, ncontrolo, analise, elemento," & _
          " ele_antib, seq, flg_imprimir, flg_apar )" & vbCrLf & _
          " SELECT a.seq_realiza, a.n_res, a.cod_micro, a.quantif, a.flg_imp," & _
          " a.flg_tsq, a.cod_gr_antibio, a.prova, a.flg_testes, a.res_ant1," & _
          " a.dt_res_ant1, a.res_ant2, a.dt_res_ant2, a.res_ant3," & _
          " a.dt_res_ant3, a.datainsc, a.ncontrolo, a.analise, a.elemento," & _
          " a.ele_antib, a.seq, a.flg_imprimir, a.flg_apar " & vbCrLf & _
          " FROM sl_requis,sl_realiza" & Origem & ",sl_res_micro" & Origem & " a " & vbCrLf & _
          " WHERE sl_requis.n_req=sl_realiza" & Origem & ".n_req AND sl_realiza" & Origem & ".seq_realiza=a.seq_realiza AND sl_requis.estado_req=" & EstadoReqDe & Criterio
    
    gSQLError = 0
    gSQLISAM = 0
    Call BG_ExecutaQuery_ADO(sql)
    If gSQLError <> 0 Then
        gConexao.CommandTimeout = aux_long
        ProgressBar1.value = ProgressBar1.Min
        BG_RollbackTransaction
        BG_Mensagem mediMsgBox, "Passagem de registos anulada devido a erro      !", vbOKOnly + vbCritical
        Exit Sub
    End If
    
    DoEvents
    DoEvents
    DoEvents
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    'Insere SL_RES_TSQ:
    sql = "INSERT INTO sl_res_tsq" & destino & "( seq_realiza, n_res, cod_antib, cod_micro, res_sensib, " & _
          " cmi, flg_imp )" & vbCrLf & _
          " SELECT a.seq_realiza, a.n_res, a.cod_antib, a.cod_micro, a.res_sensib," & _
          " A.CMI , A.flg_imp " & vbCrLf & _
          " FROM sl_requis,sl_realiza" & Origem & ",sl_res_tsq" & Origem & " a " & vbCrLf & _
          " WHERE sl_requis.n_req=sl_realiza" & Origem & ".n_req AND sl_realiza" & Origem & ".seq_realiza=a.seq_realiza AND sl_requis.estado_req=" & EstadoReqDe & Criterio
    
    gSQLError = 0
    gSQLISAM = 0
    Call BG_ExecutaQuery_ADO(sql)
    If gSQLError <> 0 Then
        gConexao.CommandTimeout = aux_long
        ProgressBar1.value = ProgressBar1.Min
        BG_RollbackTransaction
        BG_Mensagem mediMsgBox, "Passagem de registos anulada devido a erro!      ", vbOKOnly + vbCritical
        Exit Sub
    End If
    
    '________________________________________________________________________________________________________________________________________
    
    DoEvents
    DoEvents
    DoEvents
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    'Apaga SL_RES_ALFAN
    sql = "DELETE sl_res_alfan" & Origem & vbCrLf & _
          " WHERE sl_res_alfan" & Origem & ".seq_realiza IN " & vbCrLf & _
          "(SELECT sl_realiza" & Origem & ".seq_realiza " & vbCrLf & _
          " FROM sl_requis,sl_realiza" & Origem & vbCrLf & _
          " WHERE sl_requis.n_req=sl_realiza" & Origem & ".n_req AND sl_requis.estado_req=" & EstadoReqDe & Criterio & ")"
    
    gSQLError = 0
    gSQLISAM = 0
    Call BG_ExecutaQuery_ADO(sql)
    If gSQLError <> 0 Then
        gConexao.CommandTimeout = aux_long
        ProgressBar1.value = ProgressBar1.Min
        BG_RollbackTransaction
        BG_Mensagem mediMsgBox, "Passagem de registos anulada devido a erro!      ", vbOKOnly + vbCritical
        Exit Sub
    End If
    
    DoEvents
    DoEvents
    DoEvents
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    'Apaga SL_RES_FRASE
    sql = "DELETE sl_res_frase" & Origem & vbCrLf & _
          " WHERE sl_res_frase" & Origem & ".seq_realiza IN " & vbCrLf & _
          "(SELECT sl_realiza" & Origem & ".seq_realiza " & vbCrLf & _
          " FROM sl_requis,sl_realiza" & Origem & vbCrLf & _
          " WHERE sl_requis.n_req=sl_realiza" & Origem & ".n_req AND sl_requis.estado_req=" & EstadoReqDe & Criterio & ")"
    
    gSQLError = 0
    gSQLISAM = 0
    Call BG_ExecutaQuery_ADO(sql)
    If gSQLError <> 0 Then
        gConexao.CommandTimeout = aux_long
        ProgressBar1.value = ProgressBar1.Min
        BG_RollbackTransaction
        BG_Mensagem mediMsgBox, "Passagem de registos anulada devido a erro!      ", vbOKOnly + vbCritical
        Exit Sub
    End If
    
    DoEvents
    DoEvents
    DoEvents
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    'Apaga SL_RES_MICRO
    sql = "DELETE sl_res_micro" & Origem & vbCrLf & _
          " WHERE sl_res_micro" & Origem & ".seq_realiza IN " & vbCrLf & _
          "(SELECT sl_realiza" & Origem & ".seq_realiza " & vbCrLf & _
          " FROM sl_requis,sl_realiza" & Origem & vbCrLf & _
          " WHERE sl_requis.n_req=sl_realiza" & Origem & ".n_req AND sl_requis.estado_req=" & EstadoReqDe & Criterio & ")"
    
    gSQLError = 0
    gSQLISAM = 0
    Call BG_ExecutaQuery_ADO(sql)
    If gSQLError <> 0 Then
        gConexao.CommandTimeout = aux_long
        ProgressBar1.value = ProgressBar1.Min
        BG_RollbackTransaction
        BG_Mensagem mediMsgBox, "Passagem de registos anulada devido a erro!      ", vbOKOnly + vbCritical
        Exit Sub
    End If
    
    DoEvents
    DoEvents
    DoEvents
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    'Apaga SL_RES_TSQ
    sql = "DELETE sl_res_tsq" & Origem & vbCrLf & _
          " WHERE sl_res_tsq" & Origem & ".seq_realiza IN " & vbCrLf & _
          "(SELECT sl_realiza" & Origem & ".seq_realiza " & vbCrLf & _
          " FROM sl_requis,sl_realiza" & Origem & vbCrLf & _
          " WHERE sl_requis.n_req=sl_realiza" & Origem & ".n_req AND sl_requis.estado_req=" & EstadoReqDe & Criterio & ")"
    
    gSQLError = 0
    gSQLISAM = 0
    Call BG_ExecutaQuery_ADO(sql)
    If gSQLError <> 0 Then
        gConexao.CommandTimeout = aux_long
        ProgressBar1.value = ProgressBar1.Min
        BG_RollbackTransaction
        BG_Mensagem mediMsgBox, "Passagem de registos anulada devido a erro!      ", vbOKOnly + vbCritical
        Exit Sub
    End If
    
    DoEvents
    DoEvents
    DoEvents
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    'Apaga SL_REALIZA (� a �ltima tabela a ser apagada)
    sql = "DELETE sl_realiza" & Origem & vbCrLf & _
          " WHERE sl_realiza" & Origem & ".n_req IN " & vbCrLf & _
          " (SELECT sl_requis.n_req FROM sl_requis" & vbCrLf & _
          " WHERE sl_requis.estado_req=" & EstadoReqDe & Criterio & ")"
    
    gSQLError = 0
    gSQLISAM = 0
    Call BG_ExecutaQuery_ADO(sql)
    If gSQLError <> 0 Then
        gConexao.CommandTimeout = aux_long
        ProgressBar1.value = ProgressBar1.Min
        BG_RollbackTransaction
        BG_Mensagem mediMsgBox, "Passagem de registos anulada devido a erro!      ", vbOKOnly + vbCritical
        Exit Sub
    End If
    
    DoEvents
    DoEvents
    DoEvents
    
    '________________________________________________________________________________________________________________________________________
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    'Actualiza o Estado_Req na Tabela SL_REQUIS
    sql = "UPDATE sl_requis" & vbCrLf & _
          " SET estado_req=" & EstadoReqPara & vbCrLf & _
          " WHERE sl_requis.estado_req=" & EstadoReqDe & Criterio
    
    gSQLError = 0
    gSQLISAM = 0
    Call BG_ExecutaQuery_ADO(sql)
    If gSQLError <> 0 Then
        gConexao.CommandTimeout = aux_long
        ProgressBar1.value = ProgressBar1.Min
        BG_RollbackTransaction
        ProgressBar1.value = ProgressBar1.Min
        BG_Mensagem mediMsgBox, "Passagem de registos anulada devido a erro!      ", vbOKOnly + vbCritical
        Exit Sub
    End If
    
    '________________________________________________________________________________________________________________________________________
    
    ProgressBar1.value = ProgressBar1.value + 1
    DoEvents
    
    BG_CommitTransaction
    BG_Mensagem mediMsgBox, "Passagem de registos efectuada com sucesso!      ", vbOKOnly + vbInformation

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            gConexao.CommandTimeout = aux_long
            ProgressBar1.value = ProgressBar1.Min
            BG_RollbackTransaction
            BG_Mensagem mediMsgBox, "Passagem de registos anulada devido a erro!      ", vbOKOnly + vbCritical
            Call BG_LogFile_Erros("Erro Inesperado : TransfereRes (FormHistorico) -> " & Err.Description)
            Exit Sub
    End Select
End Sub


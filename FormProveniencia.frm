VERSION 5.00
Begin VB.Form FormProveniencia 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormProveniencia"
   ClientHeight    =   6840
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7260
   Icon            =   "FormProveniencia.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6840
   ScaleWidth      =   7260
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodSinave 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   6120
      TabIndex        =   49
      Top             =   1680
      Width           =   975
   End
   Begin VB.TextBox EcDescrEFR 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1920
      Locked          =   -1  'True
      TabIndex        =   47
      TabStop         =   0   'False
      Top             =   2040
      Width           =   3495
   End
   Begin VB.CommandButton BtPesquisaEntFin 
      Height          =   315
      Left            =   5400
      Picture         =   "FormProveniencia.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   46
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
      Top             =   2040
      Width           =   375
   End
   Begin VB.TextBox EcCodEFR 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1140
      TabIndex        =   45
      Top             =   2040
      Width           =   735
   End
   Begin VB.TextBox EcCodExt 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4350
      TabIndex        =   43
      Top             =   90
      Width           =   1425
   End
   Begin VB.ListBox EcLocais 
      Appearance      =   0  'Flat
      Height          =   705
      Left            =   1140
      Style           =   1  'Checkbox
      TabIndex        =   42
      Top             =   2400
      Width           =   4575
   End
   Begin VB.CheckBox CkNaoEnviaER 
      Caption         =   "N�o enviar para eResults"
      Height          =   255
      Left            =   1080
      TabIndex        =   41
      Top             =   3360
      Width           =   2415
   End
   Begin VB.CheckBox CkInvisivel 
      Caption         =   "Cancelada"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   375
      Left            =   5880
      TabIndex        =   40
      Top             =   0
      Width           =   1335
   End
   Begin VB.CheckBox CkImprimeParcial 
      Caption         =   "Imprimir requisi��es parciais (Consulta) na impress�o de resultados prontos"
      Height          =   255
      Left            =   1080
      TabIndex        =   39
      Top             =   3120
      Width           =   5775
   End
   Begin VB.TextBox EcEmail 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1140
      TabIndex        =   36
      Top             =   840
      Width           =   4635
   End
   Begin VB.TextBox EcSeqImpressao 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Left            =   3600
      TabIndex        =   34
      Top             =   1680
      Width           =   1335
   End
   Begin VB.TextBox EcPrefixo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1140
      TabIndex        =   32
      Top             =   1680
      Width           =   1305
   End
   Begin VB.CheckBox CkNaoImprimir 
      Caption         =   "N�o imprimir na impress�o de completos"
      Height          =   255
      Left            =   3600
      TabIndex        =   31
      Top             =   3360
      Width           =   3135
   End
   Begin VB.TextBox EcUrgencia 
      Enabled         =   0   'False
      Height          =   285
      Left            =   6360
      TabIndex        =   29
      Top             =   7680
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.ComboBox CbUrgencia 
      Height          =   315
      Left            =   4425
      Style           =   2  'Dropdown List
      TabIndex        =   27
      Top             =   1260
      Width           =   1335
   End
   Begin VB.ComboBox CbSituacao 
      Height          =   315
      Left            =   1150
      Style           =   2  'Dropdown List
      TabIndex        =   26
      Top             =   1260
      Width           =   1335
   End
   Begin VB.CommandButton BtContac 
      Caption         =   "Contactos"
      Height          =   855
      Left            =   6120
      Picture         =   "FormProveniencia.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   480
      Width           =   975
   End
   Begin VB.Frame Frame2 
      Height          =   795
      Left            =   240
      TabIndex        =   8
      Top             =   6000
      Width           =   6885
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   14
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   13
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   12
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   11
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4080
      TabIndex        =   7
      Top             =   7320
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   6
      Top             =   7320
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4080
      TabIndex        =   5
      Top             =   7680
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   4
      Top             =   7680
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1140
      TabIndex        =   1
      Top             =   90
      Width           =   1425
   End
   Begin VB.TextBox EcDescricao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1140
      TabIndex        =   2
      Top             =   480
      Width           =   4635
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6360
      TabIndex        =   0
      Top             =   7320
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1920
      Left            =   240
      TabIndex        =   3
      Top             =   3840
      Width           =   6855
   End
   Begin VB.Label Label13 
      Caption         =   "Cod. SINAVE"
      Height          =   255
      Index           =   3
      Left            =   5040
      TabIndex        =   50
      Top             =   1680
      Width           =   975
   End
   Begin VB.Label Label10 
      Caption         =   "E.&F.R."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   48
      Top             =   2040
      Width           =   495
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo Ext."
      Height          =   225
      Index           =   2
      Left            =   3360
      TabIndex        =   44
      Top             =   90
      Width           =   855
   End
   Begin VB.Label LbLocal 
      AutoSize        =   -1  'True
      Caption         =   "Locais"
      Height          =   195
      Index           =   0
      Left            =   240
      TabIndex        =   38
      Top             =   2445
      Width           =   465
   End
   Begin VB.Label Label6 
      Caption         =   "Email"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   37
      Top             =   960
      Width           =   855
   End
   Begin VB.Label Label15 
      Caption         =   "Sequ�ncia"
      Height          =   225
      Left            =   2640
      TabIndex        =   35
      Top             =   1680
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Prefixo"
      Height          =   225
      Index           =   1
      Left            =   240
      TabIndex        =   33
      Top             =   1710
      Width           =   615
   End
   Begin VB.Label Label14 
      Caption         =   "EcUrgencia"
      Enabled         =   0   'False
      Height          =   255
      Left            =   5280
      TabIndex        =   30
      Top             =   7680
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label13 
      Caption         =   "Prioridade"
      Height          =   255
      Index           =   0
      Left            =   3480
      TabIndex        =   28
      Top             =   1305
      Width           =   795
   End
   Begin VB.Label Label12 
      Caption         =   "Situa��o"
      Height          =   255
      Left            =   210
      TabIndex        =   25
      Top             =   1305
      Width           =   795
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2760
      TabIndex        =   23
      Top             =   7680
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Index           =   0
      Left            =   2760
      TabIndex        =   22
      Top             =   7320
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   240
      TabIndex        =   21
      Top             =   7680
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   240
      TabIndex        =   20
      Top             =   7320
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   2880
      TabIndex        =   19
      Top             =   3600
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   240
      TabIndex        =   18
      Top             =   3600
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo"
      Height          =   225
      Index           =   0
      Left            =   210
      TabIndex        =   17
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o"
      Height          =   255
      Index           =   0
      Left            =   210
      TabIndex        =   16
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   4800
      TabIndex        =   15
      Top             =   7320
      Visible         =   0   'False
      Width           =   1485
   End
End
Attribute VB_Name = "FormProveniencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 05/02/2002
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind, Max  As Integer
Dim flg_grava As Boolean

Private Sub BtContac_Click()
    
    If EcCodigo <> "" And EcDescricao <> "" Then
        Max = UBound(gFieldObjectProperties)
    
        ' Guardar as propriedades dos campos do form
        For Ind = 0 To Max
            ReDim Preserve FOPropertiesTemp(Ind)
            FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
            FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
            FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
            FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
            FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
            FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
        Next Ind

        FormContacProv.Show
    Else
        BG_Mensagem mediMsgBox, "Os contactos n�o podem ser abertos pois ainda n�o escolheu uma proveni�ncia."
    End If
End Sub

Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEfr, EcDescrEfr, ""
End Sub


Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbSituacao, KeyCode

End Sub







Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 And flg_grava = False Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub

Private Sub EcCodigo_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodigo_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)

End Sub

Private Sub EcDescricao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDescricao_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Proveni�ncias dos pedidos de an�lises"
    Me.left = 540
    Me.top = 450
    Me.Width = 7450
    Me.Height = 7260 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_proven"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 19
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_proven"
    CamposBD(1) = "cod_proven"
    CamposBD(2) = "descr_proven"
    CamposBD(3) = "user_cri"
    CamposBD(4) = "dt_cri"
    CamposBD(5) = "user_act"
    CamposBD(6) = "dt_act"
    CamposBD(7) = "t_sit"
    CamposBD(8) = "t_urg"
    CamposBD(9) = "flg_nao_imprimir"
    CamposBD(10) = "prefixo_impressao"
    CamposBD(11) = "seq_impressao"
    CamposBD(12) = "email"
    CamposBD(13) = "flg_imprime_prontos"
    CamposBD(14) = "FLG_INVISIVEL"
    CamposBD(15) = "FLG_NAO_ENVIA_ER"
    CamposBD(16) = "cod_ext"
    CamposBD(17) = "COD_EFR"
    'BRUNODSANTOS 22.02.2017 - Glintt-HS-14510
    CamposBD(18) = "cod_sinave"
    '
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcUtilizadorCriacao
    Set CamposEc(4) = EcDataCriacao
    Set CamposEc(5) = EcUtilizadorAlteracao
    Set CamposEc(6) = EcDataAlteracao
    Set CamposEc(7) = CbSituacao
    Set CamposEc(8) = CbUrgencia
    Set CamposEc(9) = CkNaoImprimir
    Set CamposEc(10) = EcPrefixo
    Set CamposEc(11) = EcSeqImpressao
    Set CamposEc(12) = EcEmail
    Set CamposEc(13) = CkImprimeParcial
    Set CamposEc(14) = CkInvisivel
    Set CamposEc(15) = CkNaoEnviaER
    Set CamposEc(16) = EcCodExt
    Set CamposEc(17) = EcCodEfr
    'BRUNODSANTOS 22.02.2017 - Glintt-HS-14510
    Set CamposEc(18) = EcCodSinave
    '
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo da Proveni�ncia"
    TextoCamposObrigatorios(2) = "Descri��o da Proveni�ncia"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_proven"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_proven", "descr_proven")
    NumEspacos = Array(22, 32)
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    gF_PROVENIENCIA = 1
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    
    Max = -1
    ReDim FOPropertiesTemp(0)
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    gF_PROVENIENCIA = 0
    Set FormProveniencia = Nothing

End Sub

Sub LimpaCampos()
    Dim i As Integer
    Me.SetFocus
    flg_grava = False
    
    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    CbUrgencia.ListIndex = mediComboValorNull
    CkNaoImprimir.value = vbGrayed
    CkImprimeParcial.value = vbGrayed
    CkInvisivel.value = vbGrayed
    CkNaoEnviaER.value = vbGrayed
    CkInvisivel.Visible = False
    For i = 0 To EcLocais.ListCount - 1
        EcLocais.Selected(i) = False
    Next
    EcDescrEfr = ""
    
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocais, mediAscComboCodigo
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia
    CkNaoImprimir.value = vbGrayed
    CkImprimeParcial.value = vbGrayed
    CkInvisivel.value = vbGrayed
    CkInvisivel.Visible = False
    CkNaoEnviaER.value = vbGrayed

End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        PreencheUrgencia
        CarregaLocaisProven EcCodigo
        EcCodEFR_Validate False
        If CkInvisivel.value = vbChecked Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
    End If

End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_proven ASC,descr_proven ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        flg_grava = True
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        
        If iRes = True Then
            EcUrgencia.Text = Mid(CbUrgencia.Text, 1, 1)
            BD_Insert
            GravaLocaisProven EcCodigo
        End If
        BL_FimProcessamento Me
        flg_grava = False
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_proven") + 1
    
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
 
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        flg_grava = True
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            EcUrgencia.Text = Mid(CbUrgencia.Text, 1, 1)
            BD_Update
            GravaLocaisProven EcCodigo
        End If
        BL_FimProcessamento Me
        flg_grava = False
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal

End Sub

Sub FuncaoRemover()
    
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        CkInvisivel.value = vbChecked
        BD_Update
        BL_FimProcessamento Me
    End If
    
End Sub


Private Sub CbUrgencia_Click()
    EcUrgencia.Text = Mid(CbUrgencia.Text, 1, 1)
End Sub
Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbUrgencia, KeyCode

End Sub

Sub PreencheUrgencia()
    
    If rs!t_urg = "N" Then
        CbUrgencia.ListIndex = 0
    ElseIf rs!t_urg = "U" Then
        CbUrgencia.ListIndex = 1
    ElseIf rs!t_urg = "L" Then
        CbUrgencia.ListIndex = 2
    End If

End Sub


Private Sub EcPrefixo_Change()
    If Len(EcPrefixo) > 2 Then
        EcPrefixo = Mid(EcPrefixo, 1, 2)
    End If
End Sub


' ------------------------------------------------------------------------------------------------

' CARREGA LOCAIS ASSOCIADOS A PROVENIENCIA

' ------------------------------------------------------------------------------------------------
Public Sub CarregaLocaisProven(cod_proven As String)
    Dim sSql As String
    Dim rsAnaLocais As New ADODB.recordset
    On Error GoTo TrataErro
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_ass_proven_locais WHERE cod_proven = " & BL_TrataStringParaBD(cod_proven)
    Set rsAnaLocais = BG_ExecutaSELECT(sSql)
    If rsAnaLocais.RecordCount > 0 Then
        While Not rsAnaLocais.EOF
            For i = 0 To EcLocais.ListCount - 1
                If EcLocais.ItemData(i) = BL_HandleNull(rsAnaLocais!cod_local, "") Then
                    EcLocais.Selected(i) = True
                End If
            Next
            rsAnaLocais.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao carregar locais: " & sSql & " " & Err.Description, Me.Name, "CarregaLocaisProven", False
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------------------------

' GRAVA LOCAIS ASSOCIADOS A PROVENIENCIA

' ------------------------------------------------------------------------------------------------

Public Sub GravaLocaisProven(cod_proven As String)
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    sSql = "DELETE FROM sl_ass_proven_locais WHERE cod_proven = " & BL_TrataStringParaBD(cod_proven)
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To EcLocais.ListCount - 1
        If EcLocais.Selected(i) = True Then
            sSql = "INSERT into sl_ass_proven_locais (cod_proven, cod_local) VALUES("
            sSql = sSql & BL_TrataStringParaBD(cod_proven) & ","
            sSql = sSql & EcLocais.ItemData(i) & ")"
            BG_ExecutaQuery_ADO sSql
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Gravar locais: " & sSql & " " & Err.Description, Me.Name, "GravaLocaisProven", False
    Exit Sub
    Resume Next
End Sub

Public Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEfr, EcDescrEfr, "")
End Sub


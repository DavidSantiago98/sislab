VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormEtiqPorTubo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   4785
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6615
   Icon            =   "FormEtiqPorTubo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4785
   ScaleWidth      =   6615
   ShowInTaskbar   =   0   'False
   Begin VB.ListBox ListaT 
      Height          =   645
      Left            =   480
      TabIndex        =   13
      Top             =   4920
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox EcPrinterEtiq 
      Height          =   285
      Left            =   120
      TabIndex        =   12
      Top             =   4920
      Width           =   255
   End
   Begin VB.Frame Frame1 
      Caption         =   "Intervalo de Datas"
      Height          =   1455
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   6375
      Begin VB.CheckBox CkNaoImpressos 
         Caption         =   "Apenas tubos n�o impressos"
         Height          =   195
         Left            =   480
         TabIndex        =   20
         Top             =   1200
         Width           =   4335
      End
      Begin VB.TextBox EcNumReqFinal 
         Height          =   285
         Left            =   3600
         TabIndex        =   17
         Top             =   840
         Width           =   1095
      End
      Begin VB.TextBox EcNumReqIni 
         Height          =   285
         Left            =   960
         TabIndex        =   16
         Top             =   840
         Width           =   1095
      End
      Begin VB.CommandButton BtEtiq 
         Height          =   480
         Left            =   5640
         Picture         =   "FormEtiqPorTubo.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Imprimir Etiquetas de Tubos"
         Top             =   240
         Width           =   495
      End
      Begin MSComCtl2.DTPicker EcDtIni 
         Height          =   255
         Left            =   960
         TabIndex        =   7
         Top             =   360
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   199622657
         CurrentDate     =   39573
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   3600
         TabIndex        =   8
         Top             =   360
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   450
         _Version        =   393216
         Format          =   199622657
         CurrentDate     =   39573
      End
      Begin VB.Label Label1 
         Caption         =   "Req. Final"
         Height          =   255
         Index           =   2
         Left            =   2760
         TabIndex        =   19
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Req. Inicial"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   18
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Data Inicial"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Data Final"
         Height          =   255
         Left            =   2760
         TabIndex        =   9
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Etiquetas para Tubos a Imprimir:"
      Height          =   2895
      Left            =   120
      TabIndex        =   0
      Top             =   1680
      Width           =   6375
      Begin VB.CommandButton BtBaixo 
         Height          =   495
         Left            =   2955
         Picture         =   "FormEtiqPorTubo.frx":044E
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   " Alterar ordem na Complexa "
         Top             =   2325
         Width           =   495
      End
      Begin VB.CommandButton BtCima 
         Height          =   495
         Left            =   2955
         Picture         =   "FormEtiqPorTubo.frx":0758
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   " Alterar ordem na Complexa "
         Top             =   1800
         Width           =   495
      End
      Begin VB.CheckBox Ck 
         Caption         =   "Todas as etiquetas especiais"
         Height          =   255
         Left            =   3720
         TabIndex        =   5
         Top             =   240
         Visible         =   0   'False
         Width           =   2415
      End
      Begin VB.ListBox ListaTodosTubos 
         Height          =   2205
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   2655
      End
      Begin VB.ListBox ListaTubos 
         Height          =   2205
         Left            =   3720
         TabIndex        =   3
         Top             =   600
         Width           =   2415
      End
      Begin VB.CommandButton BtInsere 
         Height          =   495
         Left            =   2955
         Picture         =   "FormEtiqPorTubo.frx":0A62
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   " Inserir na Tubo para impress�o"
         Top             =   720
         Width           =   495
      End
      Begin VB.CommandButton BtRetira 
         Height          =   495
         Left            =   2955
         Picture         =   "FormEtiqPorTubo.frx":0D6C
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   " Retirar Tubo de Impress�o"
         Top             =   1260
         Width           =   495
      End
   End
End
Attribute VB_Name = "FormEtiqPorTubo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 24/07/2006
' T�cnico

' Vari�veis Globais para este Form.

Private Type DOCINFO
    pDocName As String
    pOutputFile As String
    pDatatype As String
End Type

Private Declare Function ClosePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndDocPrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function OpenPrinter Lib "winspool.drv" Alias "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, ByVal pDefault As Long) As Long
Private Declare Function StartDocPrinter Lib "winspool.drv" Alias "StartDocPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, pDocInfo As DOCINFO) As Long
Private Declare Function StartPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function WritePrinter Lib "winspool.drv" (ByVal hPrinter As Long, pBuf As Any, ByVal cdBuf As Long, pcWritten As Long) As Long

Public CampoActivo As Object
Dim NomeTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim EtiqStartJob As String
Dim EtiqJob As String
Dim EtiqEndJob As String
Dim EtiqHPrinter As Long
Dim PrinterX As Long

'Variavel para guardar a posicao na lista de Tubos
Dim PosicaoActualListaTubos As Integer

Dim ListaOrigem As Control
Dim ListaDestino As Control
    

Private Sub BtEtiq_Click()
    ImprimeEtiq
End Sub

Private Sub EcDtFim_Change()
    PreencheNumRequis
End Sub
Private Sub EcDtIni_Change()
    PreencheNumRequis
End Sub

Sub Form_Load()
    
    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Impress�o de Etiquetas por Tubo"
    Me.left = 440
    Me.top = 20
    Me.Width = 6780
    Me.Height = 5175 ' Normal
    
    
    ' Pode ser a mesma impressora...da seroteca
    EcPrinterEtiq = BL_SelImpressora("Etiqueta.rpt")
    
    EcDtIni.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    PreencheNumRequis
    EcNumReqIni.Tag = adNumeric
    EcNumReqFinal.Tag = adNumeric
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    PesquisaTubos
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

    
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormEtiqPorTubo = Nothing
End Sub

Sub FuncaoLimpar()

End Sub

Sub FuncaoEstadoAnterior()

End Sub

Sub FuncaoProcurar()
    
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()
    
End Sub

Sub FuncaoInserir()
 
End Sub

Sub FuncaoModificar()

End Sub

Sub FuncaoRemover()
    
End Sub

Sub ImprimeEtiq()
    Dim i As Integer
    Dim j As Integer
    Dim valor As String
    Dim prefixo As String
    Dim qtd As Integer
    Dim descricao As String
    Dim EtqCrystal As String
    On Error GoTo ErrorHandler
    Dim NumReqBar As String
    Dim sql As String
    Dim Tabela As ADODB.recordset
    Dim CodEtiq As String
    Dim numEtiq As Integer
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    
    EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ETIQ_CRYSTAL")
                
    If EtqCrystal = "-1" Or EtqCrystal = "" Then EtqCrystal = "1"
                
    If EtqCrystal = "1" Then
    Else
        For i = 0 To ListaTubos.ListCount - 1
'-----------------------------------------
            Set Tabela = New ADODB.recordset
            sql = "select r.n_req,r.dt_previ, r.dt_chega,i.t_utente, i.utente,i.nome_ute,t.cod_prod, t.cod_etiq, t.cod_tubo, " & _
                    " t.capaci_util , t.descr_tubo, t.inf_complementar, t.num_copias, u.descr_t_urg, t.num_max_ana, i.abrev_ute " & _
                    " from sl_requis r, " & tabela_aux & " i, sl_req_tubo rt, sl_tubo t, sl_tbf_t_urg u " & _
                    " Where r.seq_utente = i.seq_utente and r.t_urg = u.cod_t_urg(+) and r.n_req = rt.n_req and rt.cod_tubo = t.cod_tubo "
            If CkNaoImpressos.value = vbChecked Then
                sql = sql & " And rt.dt_imp is null "
            End If
            sql = sql & " and t.seq_tubo = " & ListaTubos.ItemData(i)
            sql = sql & " and rt.dt_chega between " & BL_TrataDataParaBD(EcDtIni) & " and " & BL_TrataDataParaBD(EcDtFim) & ""
            sql = sql & " and r.n_req >= " & EcNumReqIni & " AND r.n_req <= " & EcNumReqFinal
            sql = sql & " order by n_req "
            
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            Tabela.Open sql, gConexao
            
            While Not Tabela.EOF
            
                'O n�mero de requisi��o tem de ter 7 digitos para as rotinas dos aparelhos
                NumReqBar = ""
                For j = 1 To (7 - Len(Tabela!n_req))
                    NumReqBar = NumReqBar & "0"
                Next j
                NumReqBar = NumReqBar & Tabela!n_req
                
                'concatena n�mero de requisi��o com cod_etiq caso exista
                CodEtiq = ""
                If BL_HandleNull(Tabela!cod_etiq, "") <> "" Then
                    For j = 1 To (2 - Len(Tabela!cod_etiq))
                        CodEtiq = CodEtiq & "0"
                    Next j
                    CodEtiq = CodEtiq & Tabela!cod_etiq
                End If
                
                NumReqBar = CodEtiq & NumReqBar
     

                If Not LerEtiqInI Then
                    MsgBox "Ficheiro de inicializa��o de etiquetas de Tubos ausente!"
                    Exit Sub
                End If
                If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                    MsgBox "Imposs�vel abrir impressora etiquetas."
                    Exit Sub
                End If
                
                'NumEtiq = BL_ArredondaCima(BL_HandleNull(Tabela!num_ana, 1) / BL_HandleNull(Tabela!num_max_ana, 99)) * BL_HandleNull(Tabela!num_copias, 1)
                Call EtiqPrint("", "", _
                                                   BL_HandleNull(Tabela!t_utente, ""), BL_HandleNull(Tabela!Utente, ""), _
                                                   "", Tabela!n_req, Mid(Tabela!descr_t_urg, 1, 3), _
                                                   Tabela!dt_chega, _
                                                   BL_HandleNull(Tabela!cod_prod, ""), _
                                                   Tabela!nome_ute, NumReqBar, _
                                                   BL_HandleNull(Tabela!descR_tubo, ""), _
                                                   BL_HandleNull(Tabela!inf_complementar, ""), 1, "", BL_HandleNull(Tabela!abrev_ute, ""))
                
                EtiqClosePrinter

                Tabela.MoveNext
            Wend
            Tabela.Close
            Set Tabela = Nothing
        Next i
        LimpaCampos
    End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro : ImprimeEtiq (FormEtiqSeroteca) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
    End Select
End Sub


Private Function LerEtiqInI() As Boolean
    Dim aux As String
    On Error GoTo Erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Etiq.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Etiq.ini"
    End If
    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqInI = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While aux <> "P1"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqInI = True
    Exit Function
Erro:

End Function

Private Function EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    
    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function
    
    MyDocInfo.pDocName = "Etiquetas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function
    
    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    sWrittenData = TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    
    EtiqOpenPrinter = True

End Function

Private Function EtiqClosePrinter() As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    EtiqClosePrinter = True

End Function

Private Function EtiqPrint( _
    ByVal gr_ana As String, _
    ByVal abr_ana As String, _
    ByVal t_utente As String, _
    ByVal Utente As String, _
    ByVal n_proc As String, _
    ByVal n_req As String, _
    ByVal t_urg As String, _
    ByVal dt_req As String, _
    ByVal Produto As String, _
    ByVal nome_ute As String, _
    ByVal N_req_bar As String, _
    ByVal Designacao_tubo As String, _
    ByVal inf_complementar As String, _
    ByVal num_copias As Integer, _
    ByVal Descr_etiq_ana As String, _
    ByVal abrev_ute As String, _
    Optional ByVal N_REQ_ARS As String, _
    Optional ByVal n_Req_assoc As String _
    ) As Boolean
    
    'soliveira terrugem n_req_assoc
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    If (Trim(UCase(Designacao_tubo)) = "ADMINISTRATIVA") Then
        N_req_bar = n_req
    End If

    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{GR_ANA}", gr_ana)
    sWrittenData = Replace(sWrittenData, "{ABR_ANA}", abr_ana)
    sWrittenData = Replace(sWrittenData, "{T_UTENTE}", t_utente)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{N_PROC}", n_proc)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{T_URG}", t_urg)
    sWrittenData = Replace(sWrittenData, "{DT_REQ}", dt_req)
    sWrittenData = Replace(sWrittenData, "{PRODUTO}", Produto)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", BL_RemovePortuguese(StrConv(nome_ute, vbProperCase)))
    sWrittenData = Replace(sWrittenData, "{ABREV_UTE}", BL_RemovePortuguese(StrConv(abrev_ute, vbProperCase)))
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", N_req_bar)
    sWrittenData = Replace(sWrittenData, "{DS_TUBO}", Designacao_tubo)
    sWrittenData = Replace(sWrittenData, "{N_REQ_ARS}", N_REQ_ARS)
    sWrittenData = Replace(sWrittenData, "{INF_COMPLEMENTAR}", inf_complementar)
    sWrittenData = Replace(sWrittenData, "{DESCR_ETIQ_ANA}", Descr_etiq_ana)
    sWrittenData = Replace(sWrittenData, "{N_REQ_ASSOC}", n_Req_assoc)
    sWrittenData = Replace(sWrittenData, "{NUM_COPIAS}", num_copias)
    sWrittenData = Replace(sWrittenData, "{QUANTIDADE}", BL_HandleNull(num_copias, "1"))

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True
    If gImprimeEPL = mediSim Then
        EtiqEndJob = Replace(EtiqEndJob, "{NUM_COPIAS}", num_copias)
        EtiqEndJob = Replace(EtiqEndJob, "{QUANTIDADE}", num_copias)
    End If
End Function



Private Function RemovePortuguese(ByVal s As String) As String
    
    Dim r As String
    
    Do While s <> ""
        Select Case left(s, 1)
            Case "�", "�", "�", "�"
                r = r + "a"
            Case "�", "�", "�", "�"
                r = r + "A"
            Case "�", "�", "�"
                r = r + "e"
            Case "�", "�", "�"
                r = r + "E"
            Case "�", "�", "�"
                r = r + "i"
            Case "�", "�", "�", "�"
                r = r + "o"
            Case "�", "�", "�", "�"
                r = r + "O"
            Case "�", "�", "�"
                r = r + "u"
            Case "�", "�", "�"
                r = r + "U"
            Case "�"
                r = r + "c"
            Case "�"
                r = r + "C"
            Case Else
                r = r + left(s, 1)
        End Select
        s = Mid(s, 2)
    Loop
    
    RemovePortuguese = r

End Function


Private Sub LimpaCampos()

End Sub
Private Function TrocaBinarios(ByVal s As String) As String
    
    Dim p As Long
    Dim k As Byte
    
    Do
        p = InStr(s, "&H")
        If p = 0 Then Exit Do
        k = val(Mid(s, p, 4))
        s = left(s, p - 1) + Chr(k) + Mid(s, p + 4)
    Loop
    TrocaBinarios = s

End Function


Private Sub BtInsere_Click()
    
    Dim i As Integer
    Dim CountAntes As Integer
    Dim CountDepois As Integer
        
    Set ListaOrigem = ListaTodosTubos
    
    If Not ListaOrigem Is Nothing Then
        Set ListaDestino = ListaTubos
           
        For i = 0 To ListaOrigem.ListCount - 1
            If ListaOrigem.Selected(i) Then
                CountAntes = ListaTubos.ListCount
                BG_PassaElementoEntreListas ListaOrigem, ListaDestino, i, False, False
                CountDepois = ListaTubos.ListCount
                If CountAntes < CountDepois Then
                    ListaT.AddItem CountDepois - 1
                End If
                ListaOrigem.Selected(i) = False
            End If
        Next i
    End If
       
End Sub

Private Sub BtRetira_Click()

    If ListaTubos.ListIndex <> -1 Then
        ListaT.ListIndex = PosicaoActualListaTubos
        ListaTubos.RemoveItem (ListaTubos.ListIndex)
        PosicaoActualListaTubos = ListaTubos.ListIndex
        ListaT.RemoveItem (ListaT.ListIndex)
    End If

End Sub

Private Sub BtBaixo_Click()
    
    Dim sdummy As String
    Dim ldummy As Long
    
    If ListaTubos.ListIndex < ListaTubos.ListCount - 1 Then
        sdummy = ListaTubos.List(ListaTubos.ListIndex + 1)
        ldummy = ListaTubos.ItemData(ListaTubos.ListIndex + 1)
        ListaTubos.List(ListaTubos.ListIndex + 1) = ListaTubos.List(ListaTubos.ListIndex)
        ListaTubos.ItemData(ListaTubos.ListIndex + 1) = ListaTubos.ItemData(ListaTubos.ListIndex)
        ListaTubos.List(ListaTubos.ListIndex) = sdummy
        ListaTubos.ItemData(ListaTubos.ListIndex) = ldummy
        
        sdummy = ListaT.List(ListaTubos.ListIndex + 1)
        ldummy = ListaT.ItemData(ListaTubos.ListIndex + 1)
        ListaT.List(ListaTubos.ListIndex + 1) = ListaT.List(ListaTubos.ListIndex)
        ListaT.ItemData(ListaTubos.ListIndex + 1) = ListaT.ItemData(ListaTubos.ListIndex)
        ListaT.List(ListaTubos.ListIndex) = sdummy
        ListaT.ItemData(ListaTubos.ListIndex) = ldummy
        ListaTubos.ListIndex = ListaTubos.ListIndex + 1
    End If

End Sub

Private Sub BtCima_Click()
    
    Dim sdummy As String
    Dim ldummy As Long
    
    If ListaTubos.ListIndex > 0 Then
        sdummy = ListaTubos.List(ListaTubos.ListIndex - 1)
        ldummy = ListaTubos.ItemData(ListaTubos.ListIndex - 1)
        ListaTubos.List(ListaTubos.ListIndex - 1) = ListaTubos.List(ListaTubos.ListIndex)
        ListaTubos.ItemData(ListaTubos.ListIndex - 1) = ListaTubos.ItemData(ListaTubos.ListIndex)
        ListaTubos.List(ListaTubos.ListIndex) = sdummy
        ListaTubos.ItemData(ListaTubos.ListIndex) = ldummy
        
        sdummy = ListaT.List(ListaTubos.ListIndex - 1)
        ldummy = ListaT.ItemData(ListaTubos.ListIndex - 1)
        ListaT.List(ListaTubos.ListIndex - 1) = ListaT.List(ListaTubos.ListIndex)
        ListaT.ItemData(ListaTubos.ListIndex - 1) = ListaT.ItemData(ListaTubos.ListIndex)
        ListaT.List(ListaTubos.ListIndex) = sdummy
        ListaT.ItemData(ListaTubos.ListIndex) = ldummy
        ListaTubos.ListIndex = ListaTubos.ListIndex - 1
    End If

End Sub

Sub PesquisaTubos()
    
    Dim i As Integer
    Dim k As Integer
    Dim TSQLQueryAux As String
    Dim TCampoChave As String
    Dim TCampoPesquisa As String
    Dim TCamposSel As String
    Dim TNomeTabela As String
    Dim TClausulaWhereAuxiliar As String
    Dim TTabelaAux As ADODB.recordset
    Dim NomeControl As Control
    
    On Error GoTo Trata_Erro
    Set TTabelaAux = New ADODB.recordset
    

    
    Set NomeControl = ListaTodosTubos
        TCampoChave = "seq_tubo"
        TCamposSel = "cod_tubo, descr_tubo"
        TCampoPesquisa = "descr_tubo"
        TNomeTabela = "sl_tubo"
    
    TSQLQueryAux = "SELECT seq_tubo, cod_tubo, descr_tubo " & _
                    " FROM sl_tubo order by descr_tubo "
    
    TTabelaAux.CursorLocation = adUseServer
    TTabelaAux.CursorType = adOpenStatic
    TTabelaAux.Open TSQLQueryAux, gConexao
    NomeControl.ListIndex = mediComboValorNull
    NomeControl.Clear
    
    i = 0
    Do Until TTabelaAux.EOF
        k = InStr(1, TCamposSel, ",")
        NomeControl.AddItem Trim(TTabelaAux(Trim(Mid(TCamposSel, 1, k - 1)))) & Space(8 - Len(Trim(TTabelaAux(Trim(Mid(TCamposSel, 1, k - 1))))) + 1) & Trim(TTabelaAux(Trim(Mid(TCamposSel, k + 1)))) & Space(60 - Len(Mid(Trim(TTabelaAux(Trim(Mid(TCamposSel, k + 1)))), 1, 40)) + 1)
        NomeControl.ItemData(i) = CInt(TTabelaAux(TCampoChave))
        TTabelaAux.MoveNext
        i = i + 1
    Loop
    TTabelaAux.Close
    Set TTabelaAux = Nothing
Exit Sub
Trata_Erro:
End Sub



Private Sub PreencheNumRequis()
    Dim sSql As String
    Dim rsReq As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT min(n_req) minimo, max (n_req) maximo FROM sl_requis where dt_chega BETWEEN "
    sSql = sSql & BL_TrataDataParaBD(EcDtIni.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    rsReq.Open sSql, gConexao
    If rsReq.RecordCount = 1 Then
        EcNumReqIni = BL_HandleNull(rsReq!minimo, 0)
        EcNumReqFinal = BL_HandleNull(rsReq!maximo, 0)
    End If
    rsReq.Close
    Set rsReq = Nothing
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  Preencheer Requisicoes " & Err.Description, Me.Name, "PreencheNumRequis"
    Exit Sub
    Resume Next
End Sub

Attribute VB_Name = "EnvioFact"
Option Explicit

Private Type Recibos
    serie_rec As String
    n_rec As String
    total As String
    cod_efr As String
    Estado As String
End Type

' ------------------------------------------------------------------------------------
' ESTRUTURA QUE GUARDA AS ANALISES A FACTURAR PARA UMA REQUISICAO
' ------------------------------------------------------------------------------------
Private Type AnalisesFacturar
    SeqRecibo As Long
    NReq As String
    codAna As String
    CodFacturavel As String
    descrAna As String
    CodAnaGH As String
    descrAnaGH As String
    ordem As Integer
    p1 As String
    preco As Double
    PrecoUnitario As Double
    
    precoUte As Double
    precoUnitarioUte As Double
    nRec As String
    serieRec As String
    estadoRec As String
    Flg_Facturada As Integer
    Flg_Retirada As Boolean
    Flg_adicionada As Boolean
    isencao As String
    cor_p1 As String
    cod_efr As String
    cod_efr_envio As String
    cod_rubr_efr As String
    descr_rubr_efr As String
    nr_c As String
    Peso As Integer
    Qtd_Ana As Integer
    qtd_ana_envio As Integer
    cod_medico As String
    nr_benef As String
    fds As String
    flg_ObrigaRecibo As Boolean
    tratado As Boolean
    
    flg_percentagem As Boolean
    PercDescCodif As String
    PercDescUtente As String
    PercDescEnvio As String
    flg_controlaIsencao As Boolean
    
    deducaoUtente As String
    cod_empresa As String
    flg_FdsValFixo As Integer
    Portaria As String
    data As String
    flg_recibo_manual As Integer
    flg_novaArs As Integer
End Type



' ------------------------------------------------------------------------------------
' ESTRUTURA QUE GUARDA OS DADOS SOBRE A REQUISICAO A FACTURAR
' ------------------------------------------------------------------------------------
Private Type RequisicaoFacturar
    NReq As String
    user_cri As String
    dt_chega As String
    hr_chega As String
    n_benef As String
    codIsencao As String
    flg_isenta As Boolean
    estadoFact As Integer
    EstadoReq As String
    codUrbano As Long
    km As Long
    codEfr As String
    DescrEFR As String
    SeqUtente As Long
    NomeUtente As String
    NomeDono As String
    dt_nasc_ute As String
    Sexo As String
    numCartaoUtente As String
    hemodialise As Integer
    cod_ana_hemodialise As String
    convencao As Integer
    fimSemana As Integer
    cod_sala As String
    Utente As String
    TipoUtente As String
    DataNasc As String
    NReqAssoc As String
    codMed As String
    codPostal As String
    localidade As String
    codPais As String
    numDoeProf As String
    cod_proven As String
    descr_proven As String
    cod_proven_efr As String
    
    n_lote As String
    n_fac As String
    
    val_pag_ent As String
    Desconto As Double
    SeqUtenteFact As Long
    
    dt_envio As String
    user_envio As String
    
    flg_perc_facturar As String
    flg_controla_isencao As String
    flg_obriga_recibo As String
    deducao_ute As String
    flg_acto_med As String
    
    cod_empresa As String
    flg_compart_dom As String
    flg_dom_defeito As String
    flg_nao_urbano As String
    flg_65anos As String
    flg_novaArs As Integer
    codDom As String
    flg_separa_dom As String
    flg_insere_cod_rubr_efr As String
    flg_adse As Integer
    portariaDefeito As String
    
    AnalisesFact() As AnalisesFacturar
    TotalAnalisesFact As Integer
    
    EstrutRecibos() As Recibos
    TotalRecibos As Long
    
End Type
Global EF_ReqFact() As RequisicaoFacturar
Global EF_TotalRequisicoes As Long

Const lCodUrbano = 2
Const lCodNaoUrbano = 3
Const lCodUrbanoPortoLisboa = 1
Const lCodUrbanoBiDiario = 4

Public Function EF_PreencheRequisicao(ByVal n_req As String, ByVal dt_chega As String, ByVal user_cri As String, ByVal hr_chega As String, ByVal n_benef As String, _
                                      ByVal cod_isencao As String, ByVal Flg_Facturado As Integer, ByVal cod_urbano As Integer, ByVal km As Integer, ByVal cod_efr As String, _
                                      ByVal cod_med As String, ByVal seq_utente As String, ByVal t_utente As String, ByVal Utente As String, ByVal nome_ute As String, _
                                      ByVal dt_nasc_ute As String, ByVal sexo_ute As String, ByVal n_cartao_ute As String, _
                                      ByVal convencao As Integer, ByVal hemodialise As Integer, ByVal fim_semana As Integer, ByVal cod_sala As String, _
                                      ByVal seq_utente_fact As Long, ByVal descr_efr As String, ByVal flg_perc_facturar As Integer, ByVal flg_controla_isencao As Integer, _
                                      ByVal flg_obriga_recibo As Integer, ByVal deducao_ute As String, ByVal flg_acto_med_efr As Integer, ByVal cod_empresa As String, _
                                      ByVal flg_compart_dom As Integer, ByVal flg_dom_defeito As String, ByVal flg_nao_urbano As String, ByVal flg_65anos As String, _
                                      ByVal flg_nova_ars As Integer, ByVal flg_separa_dom As Integer, ByVal flg_insere_cod_rubr_efr As Integer, ByVal flg_adse As Integer, _
                                      ByVal n_Req_assoc As String, ByVal Estado_Req As String, ByVal cod_pais As String, ByVal num_doe_prof As String, ByVal cod_postal As String, _
                                      ByVal descr_postal As String, ByVal cod_proven As String, ByVal cod_proven_efr As String, ByVal descr_proven, ByVal nome_dono As String) As Boolean
    EF_TotalRequisicoes = EF_TotalRequisicoes + 1
    ReDim Preserve EF_ReqFact(EF_TotalRequisicoes)
    
    ' ---------------------------------------------------------------------------
    ' PREENCHE ESTRUTURA COM DADOS DA REQUISICAO
    ' ---------------------------------------------------------------------------
    EF_ReqFact(EF_TotalRequisicoes).NReq = n_req
    EF_ReqFact(EF_TotalRequisicoes).dt_chega = dt_chega
    EF_ReqFact(EF_TotalRequisicoes).user_cri = user_cri
    EF_ReqFact(EF_TotalRequisicoes).hr_chega = hr_chega
    EF_ReqFact(EF_TotalRequisicoes).n_benef = n_benef
    EF_ReqFact(EF_TotalRequisicoes).flg_isenta = EF_RequisicaoIsenta(n_req)
    EF_ReqFact(EF_TotalRequisicoes).codIsencao = cod_isencao
    EF_ReqFact(EF_TotalRequisicoes).estadoFact = Flg_Facturado
    EF_ReqFact(EF_TotalRequisicoes).codUrbano = cod_urbano
    EF_ReqFact(EF_TotalRequisicoes).km = km
    EF_ReqFact(EF_TotalRequisicoes).codEfr = cod_efr
    EF_ReqFact(EF_TotalRequisicoes).codMed = cod_med
    EF_ReqFact(EF_TotalRequisicoes).SeqUtente = seq_utente
    EF_ReqFact(EF_TotalRequisicoes).TipoUtente = t_utente
    EF_ReqFact(EF_TotalRequisicoes).Utente = Utente
    EF_ReqFact(EF_TotalRequisicoes).NomeUtente = nome_ute
    EF_ReqFact(EF_TotalRequisicoes).NomeDono = nome_dono
    EF_ReqFact(EF_TotalRequisicoes).dt_nasc_ute = dt_nasc_ute
    EF_ReqFact(EF_TotalRequisicoes).Sexo = sexo_ute
    EF_ReqFact(EF_TotalRequisicoes).numCartaoUtente = n_cartao_ute
    EF_ReqFact(EF_TotalRequisicoes).estadoFact = BL_HandleNull(Flg_Facturado, gEstadoFactRequisNaoFacturado)
    If EF_ReqFact(EF_TotalRequisicoes).estadoFact = gEstadoFactRequisFacturado Then
        If IF_ConfirmaFacturado(EF_ReqFact(EF_TotalRequisicoes).NReq) = False Then
            EF_ReqFact(EF_TotalRequisicoes).estadoFact = gEstadoFactRequisNaoFacturado
        End If
    End If
    EF_ReqFact(EF_TotalRequisicoes).dt_chega = BL_HandleNull(dt_chega, Bg_DaData_ADO)
    EF_ReqFact(EF_TotalRequisicoes).convencao = convencao
    EF_ReqFact(EF_TotalRequisicoes).hemodialise = hemodialise
    If EF_ReqFact(EF_TotalRequisicoes).hemodialise > 0 Then
        EF_ReqFact(EF_TotalRequisicoes).cod_ana_hemodialise = BL_SelCodigo("sl_cod_hemodialise", "cod_ana", "cod_hemodialise", EF_ReqFact(EF_TotalRequisicoes).hemodialise)
    Else
        EF_ReqFact(EF_TotalRequisicoes).cod_ana_hemodialise = ""
    End If
    EF_ReqFact(EF_TotalRequisicoes).fimSemana = fim_semana
    ' --------------------------------------------
    ' SE FOR FIM DE SEMANA - POE DATA ULT DOMINGO
    ' --------------------------------------------
    If EF_ReqFact(EF_TotalRequisicoes).fimSemana = mediSim Then
        EF_ReqFact(EF_TotalRequisicoes).dt_chega = CDate(CDate(EF_ReqFact(EF_TotalRequisicoes).dt_chega) - CDate(Weekday(EF_ReqFact(EF_TotalRequisicoes).dt_chega)) + 1)
    Else
        If gControlaDataPrecario = mediSim Then
            EF_ReqFact(EF_TotalRequisicoes).dt_chega = IF_RetornaDataPrecario(EF_ReqFact(EF_TotalRequisicoes).codEfr, EF_ReqFact(EF_TotalRequisicoes).dt_chega)
        End If
    End If
    EF_ReqFact(EF_TotalRequisicoes).cod_sala = cod_sala
    EF_ReqFact(EF_TotalRequisicoes).SeqUtenteFact = seq_utente_fact
    If EF_ReqFact(EF_TotalRequisicoes).SeqUtenteFact > -1 Then
        EF_AlteraUtenteFact EF_ReqFact(EF_TotalRequisicoes).SeqUtenteFact, EF_TotalRequisicoes, False
    End If
    EF_ReqFact(EF_TotalRequisicoes).DescrEFR = descr_efr
    EF_ReqFact(EF_TotalRequisicoes).flg_perc_facturar = flg_perc_facturar
    EF_ReqFact(EF_TotalRequisicoes).flg_controla_isencao = flg_controla_isencao
    EF_ReqFact(EF_TotalRequisicoes).flg_obriga_recibo = flg_obriga_recibo
    EF_ReqFact(EF_TotalRequisicoes).deducao_ute = deducao_ute
    EF_ReqFact(EF_TotalRequisicoes).flg_acto_med = flg_acto_med_efr
    EF_ReqFact(EF_TotalRequisicoes).cod_empresa = cod_empresa
    EF_ReqFact(EF_TotalRequisicoes).flg_compart_dom = flg_compart_dom
    EF_ReqFact(EF_TotalRequisicoes).flg_dom_defeito = flg_dom_defeito
    EF_ReqFact(EF_TotalRequisicoes).flg_nao_urbano = flg_nao_urbano
    EF_ReqFact(EF_TotalRequisicoes).flg_65anos = flg_65anos
    EF_ReqFact(EF_TotalRequisicoes).flg_novaArs = flg_nova_ars
    EF_ReqFact(EF_TotalRequisicoes).codDom = BL_SelCodigo("SL_TBF_T_URBANO", "DOMICILIO", "COD_T_URBANO", EF_ReqFact(EF_TotalRequisicoes).codUrbano)
    EF_ReqFact(EF_TotalRequisicoes).flg_separa_dom = flg_separa_dom
    EF_ReqFact(EF_TotalRequisicoes).flg_insere_cod_rubr_efr = flg_insere_cod_rubr_efr
    EF_ReqFact(EF_TotalRequisicoes).flg_adse = flg_adse
    If EF_ReqFact(EF_TotalRequisicoes).hemodialise <> mediComboValorNull Then
        EF_ReqFact(EF_TotalRequisicoes).flg_controla_isencao = 1
    End If
    ' ---------------------------------------------------------------------------
    ' PREENCHE CAMPOS DO ECRA
    ' ---------------------------------------------------------------------------
    EF_ReqFact(EF_TotalRequisicoes).Utente = EF_ReqFact(EF_TotalRequisicoes).Utente
    EF_ReqFact(EF_TotalRequisicoes).TipoUtente = EF_ReqFact(EF_TotalRequisicoes).TipoUtente
    EF_ReqFact(EF_TotalRequisicoes).NomeUtente = EF_ReqFact(EF_TotalRequisicoes).NomeUtente
    EF_ReqFact(EF_TotalRequisicoes).DataNasc = dt_nasc_ute
    EF_ReqFact(EF_TotalRequisicoes).NReqAssoc = n_Req_assoc
    EF_ReqFact(EF_TotalRequisicoes).EstadoReq = BL_HandleNull(Estado_Req, gEstadoReqEsperaResultados)
    EF_ReqFact(EF_TotalRequisicoes).codPais = cod_pais
    EF_ReqFact(EF_TotalRequisicoes).numDoeProf = num_doe_prof
    EF_ReqFact(EF_TotalRequisicoes).codPostal = cod_postal
    EF_ReqFact(EF_TotalRequisicoes).localidade = descr_postal
    EF_ReqFact(EF_TotalRequisicoes).cod_proven = cod_proven
    EF_ReqFact(EF_TotalRequisicoes).cod_proven_efr = cod_proven_efr
    EF_ReqFact(EF_TotalRequisicoes).descr_proven = descr_proven
    EF_ReqFact(EF_TotalRequisicoes).portariaDefeito = IF_RetornaPortariaData(EF_ReqFact(EF_TotalRequisicoes).codEfr, Bg_DaData_ADO)
    If gTipoInstituicao = "PRIVADA" Then
        EF_PreencheLoteFac EF_TotalRequisicoes
        EF_PreencheUserFac EF_TotalRequisicoes
    End If

End Function

Private Sub EF_PreencheLoteFac(linha As Long)
    Dim ssql As String
    Dim rsLote As New adodb.recordset
    Dim iBD_Aberta As Long
    
    ssql = "select distinct serie_fac, n_fac, n_lote from fa_lin_fact where t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(EF_ReqFact(linha).NReq)
    rsLote.CursorLocation = adUseServer
    rsLote.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsLote.Open ssql, gConexaoSecundaria
    
    EF_ReqFact(linha).n_fac = ""
    EF_ReqFact(linha).n_lote = ""
    
    If rsLote.RecordCount > 0 Then
        While Not rsLote.EOF
            EF_ReqFact(linha).n_fac = EF_ReqFact(linha).n_fac & BL_HandleNull(rsLote!serie_fac, "") & "/" & BL_HandleNull(rsLote!n_fac, "") & ";"
            EF_ReqFact(linha).n_lote = EF_ReqFact(linha).n_lote & BL_HandleNull(rsLote!n_lote, "") & ";"
            rsLote.MoveNext
        Wend
    Else
        EF_ReqFact(linha).n_fac = ""
        EF_ReqFact(linha).n_lote = ""
    End If
    rsLote.Close
    Set rsLote = Nothing
End Sub

Private Sub EF_PreencheUserFac(linha As Long)
    Dim ssql As String
    Dim rsLote As New adodb.recordset
    Dim iBD_Aberta As Long
    'edgar.parada LJMANSO-258 13.04.2018 mudei fa_movi_resp por sl_movi_fact
    ssql = "select dt_cri, user_cri from sl_movi_resp where  t_episodio = 'SISLAB' AND  episodio = " & BL_TrataStringParaBD(EF_ReqFact(linha).NReq) & " AND T_EPISODIO = 'SISLAB'"
    '
    rsLote.CursorLocation = adUseServer
    rsLote.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsLote.Open ssql, gConexao
    If rsLote.RecordCount > 0 Then
        EF_ReqFact(linha).user_envio = BL_HandleNull(rsLote!user_cri, "")
        EF_ReqFact(linha).dt_envio = BL_HandleNull(rsLote!dt_cri, "")
    Else
    EF_ReqFact(linha).user_envio = ""
    EF_ReqFact(linha).dt_envio = ""
    End If
    rsLote.Close
    Set rsLote = Nothing
End Sub



' ---------------------------------------------------------------------------

' VERIFICA SE UTENTE PAGOU TODAS AS TAXAS

' ---------------------------------------------------------------------------
Public Function EF_RequisicaoIsenta(n_req As String) As Boolean
        
    On Error GoTo TrataErro
    
    Dim ssql As String
    Dim RsTaxa As adodb.recordset
    Dim ret As Boolean
    
    n_req = Trim(n_req)
    
    If (Len(n_req) = 0) Then
        EF_RequisicaoIsenta = False
        Exit Function
    End If
        
    ssql = "SELECT cod_isencao FROM sl_requis WHERE n_req = " & n_req
    Set RsTaxa = New adodb.recordset
    RsTaxa.CursorLocation = adUseServer
    RsTaxa.CursorType = adOpenForwardOnly
    RsTaxa.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    RsTaxa.Open ssql, gConexao
    
    
    If (RsTaxa.RecordCount > 0) Then
        If BL_HandleNull(RsTaxa!cod_isencao, "2") <> "2" Then
            ret = False
        Else
            ret = True
        End If
    End If
    RsTaxa.Close
    Set RsTaxa = Nothing

    EF_RequisicaoIsenta = ret

Exit Function
TrataErro:
    Call BL_LogFile_BD("EnvioFact", "RequisicaoIsenta", Err.Number, Err.Description, "")
    EF_RequisicaoIsenta = False
    Exit Function
    Resume Next
End Function




Sub EF_AlteraUtenteFact(SeqUtente As Long, row As Long, ActualizaReq As Boolean)
    'Altera o utente a enviar para o FACTUS
    Dim RsIdentif As adodb.recordset
    If SeqUtente <> -1 Then
        Set RsIdentif = New adodb.recordset
        RsIdentif.CursorLocation = adUseServer
        RsIdentif.CursorType = adOpenStatic
        RsIdentif.Source = "select seq_utente, t_utente, utente, dt_nasc_ute, nome_ute, n_cartao_ute from sl_identif where seq_utente = " & SeqUtente
        If gModoDebug = mediSim Then BG_LogFile_Erros RsIdentif.Source
        RsIdentif.Open , gConexao
        If RsIdentif.RecordCount > 0 Then
            EF_ReqFact(row).SeqUtente = RsIdentif!seq_utente
            EF_ReqFact(row).TipoUtente = RsIdentif!t_utente
            EF_ReqFact(row).Utente = RsIdentif!Utente
            EF_ReqFact(row).DataNasc = RsIdentif!dt_nasc_ute
            EF_ReqFact(row).NomeUtente = BL_HandleNull(RsIdentif!nome_ute, "")
            EF_ReqFact(row).numCartaoUtente = BL_HandleNull(RsIdentif!n_cartao_ute, "")
            EF_ReqFact(row).SeqUtenteFact = EF_ReqFact(row).SeqUtente
            FormFactusEnvio.FgReq_Click
        Else
            BG_Mensagem mediMsgBox, "Erro a alterar o utente para a factura��o!", vbInformation
        End If
    End If
End Sub


' ---------------------------------------------------------------------------

' PREENCHE A ESTRUTURA COM AS ANALISES DA REQUISI��O

' ---------------------------------------------------------------------------

Public Function EF_PreencheAnalises(linhaR As Long) As Integer
    Dim ssql As String
    Dim rsAnalises As New adodb.recordset
    Dim rsAnalises2 As New adodb.recordset
    Dim rsAnalises3 As New adodb.recordset
    Dim CodFactActual As String
    Dim CodEfrActual As String
    Dim TotalFactEnt As Double
    Dim linhaA As Long
    
    On Error GoTo TrataErro
    
    EF_ReqFact(linhaR).TotalAnalisesFact = 0
    ReDim EF_ReqFact(linhaR).AnalisesFact(0)
    
    TotalFactEnt = 0
    
    
    ' ---------------------------------------------------------------------------
    ' ANALISES EXISTENTES NA TABELA SL_RECIBOS_DET
    ' ---------------------------------------------------------------------------
    ssql = "SELECT   x1.cod_facturavel, x1.p1,x1.flg_facturado, x1.ordem_marcacao,x1.flg_retirado, "
    ssql = ssql & " x1.flg_adicionada, x1.ordem_marcacao, x1.isencao,x1.cor_p1, x1.cod_efr, X1.quantidade, "
    ssql = ssql & " x1.cod_medico, "
    ssql = ssql & " x1.n_benef, x1.fds, x2.cod_empresa, x1.n_rec, x1.serie_rec, x3.flg_fds_fixo, "
    ssql = ssql & " x1.flg_recibo_manual, x2.estado estado_Rec, x1.cod_efr_envio, x2.dt_cri, X1.quantidade_envio, x1.seq_recibo "
    ssql = ssql & " FROM SL_RECIBOS_DET x1, sl_Recibos x2, sl_efr x3 WHERE  x1.n_req = " & EF_ReqFact(linhaR).NReq
    ssql = ssql & " AND x1.n_rec = x2.n_rec AND x1.serie_rec = x2.serie_rec "
    ssql = ssql & " AND x2.n_req = " & BL_TrataStringParaBD(EF_ReqFact(linhaR).NReq)
    ssql = ssql & " AND x1.cod_efr = x2.cod_efr AND x1.cod_Efr = x3.cod_Efr "
    ssql = ssql & " AND x1.cod_efr = " & EF_ReqFact(linhaR).codEfr
    ssql = ssql & " GROUP BY   cod_facturavel, p1,flg_facturado, "
    ssql = ssql & " ordem_marcacao,flg_retirado, flg_adicionada, ordem_marcacao, isencao, cor_p1, x1.cod_efr, quantidade, cod_medico, "
    ssql = ssql & " n_benef, fds, x2.cod_empresa, x1.n_rec, x1.serie_rec, x3.flg_fds_fixo, x1.flg_recibo_manual,x2.estado, x1.cod_efr_envio,"
    ssql = ssql & " x2.dt_cri, x1.quantidade_envio, x1.seq_recibo "
    ssql = ssql & " ORDER BY cod_efr ASC, p1 ASC, x1.ordem_marcacao ASC "
    
    
    rsAnalises.CursorLocation = adUseServer
    rsAnalises.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAnalises.Open ssql, gConexao
    
    
    If rsAnalises.RecordCount > 0 Then
        CodFactActual = ""
        CodEfrActual = ""
        EF_PreencheRecibosReq linhaR
        
        While Not rsAnalises.EOF
        
                CodEfrActual = BL_HandleNull(rsAnalises!cod_efr, "")
                CodFactActual = BL_HandleNull(rsAnalises!cod_facturavel, "0")
                ssql = "SELECT x2.cod_ana, x2.descr_ana, x3.cod_ana_gh, x3.descr_ana_facturacao, x2.peso_estatistico, x3.flg_conta_membros, x3.qtd "
                ssql = ssql & " FROM   slv_analises_factus x2, sl_ana_facturacao x3 "
                ssql = ssql & " WHERE x2.cod_ana = x3.cod_ana AND x2.cod_ana = " & BL_TrataStringParaBD(rsAnalises!cod_facturavel) & " AND x3.cod_efr = " & BL_HandleNull(rsAnalises!cod_efr, "")
                ssql = ssql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(rsAnalises!dt_cri, Bg_DaData_ADO)) & " BETWEEN "
                ssql = ssql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
                
                rsAnalises2.CursorLocation = adUseServer
                rsAnalises2.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros ssql
                rsAnalises2.Open ssql, gConexao
                If rsAnalises2.RecordCount = 0 Then
                    rsAnalises2.Close
                    ssql = "SELECT x2.cod_ana, x2.descr_ana, x3.cod_ana_gh, x3.descr_ana_facturacao, x2.peso_estatistico, x3.flg_conta_membros, x3.qtd "
                    ssql = ssql & " FROM   slv_analises_factus x2, sl_ana_facturacao x3 "
                    ssql = ssql & " WHERE x2.cod_ana = x3.cod_ana AND x2.cod_ana = " & BL_TrataStringParaBD(rsAnalises!cod_facturavel) & " AND x3.cod_efr IS NULL "
                    ssql = ssql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(rsAnalises!dt_cri, Bg_DaData_ADO)) & " BETWEEN "
                    ssql = ssql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
                    rsAnalises2.CursorLocation = adUseServer
                    rsAnalises2.CursorType = adOpenStatic
                    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
                    rsAnalises2.Open ssql, gConexao
                 End If
                 
                 If rsAnalises2.RecordCount > 0 Then
                
                    While Not (rsAnalises2.EOF)
                        
                        linhaA = EF_PreencheEstrutAnalises(linhaR, mediComboValorNull, BL_HandleNull(rsAnalises!cod_facturavel, "0"), BL_HandleNull(rsAnalises2!cod_ana, "0"), BL_HandleNull(rsAnalises2!descr_ana, ""), _
                                                  BL_HandleNull(rsAnalises2!cod_ana_gh, ""), BL_HandleNull(rsAnalises2!descr_ana_facturacao, ""), BL_HandleNull(rsAnalises!p1, "0"), _
                                                   BL_HandleNull(rsAnalises!Flg_Facturado, gEstadoFactRequisNaoFacturado), BL_HandleNull(rsAnalises!flg_retirado, "0"), _
                                                   BL_HandleNull(rsAnalises!Flg_adicionada, "0"), BL_HandleNull(rsAnalises!isencao, "0"), BL_HandleNull(rsAnalises!n_rec, "0"), _
                                                   BL_HandleNull(rsAnalises!serie_rec, "0"), BL_HandleNull(rsAnalises!estado_rec, ""), BL_HandleNull(rsAnalises!ordem_marcacao, "0"), _
                                                   BL_HandleNull(rsAnalises!cor_p1, "B"), BL_HandleNull(rsAnalises!cod_efr, ""), BL_HandleNull(rsAnalises2!peso_estatistico, 0), _
                                                   BL_HandleNull(rsAnalises2!qtd, BL_HandleNull(rsAnalises!quantidade, 1)), BL_HandleNull(rsAnalises!cod_medico, ""), _
                                                   BL_HandleNull(rsAnalises!n_benef, ""), BL_HandleNull(rsAnalises!fds, ""), BL_HandleNull(rsAnalises!cod_empresa, ""), _
                                                   BL_HandleNull(rsAnalises!flg_fds_fixo, 0), BL_HandleNull(rsAnalises!flg_recibo_manual, "0"), BL_HandleNull(rsAnalises!cod_efr_envio, ""), _
                                                   BL_HandleNull(rsAnalises!quantidade_envio, BL_HandleNull(rsAnalises!quantidade, 1)), BL_HandleNull(rsAnalises!seq_Recibo, mediComboValorNull))
                        TotalFactEnt = TotalFactEnt + EF_ReqFact(linhaR).AnalisesFact(linhaA).preco

                        rsAnalises2.MoveNext
                    Wend
                End If
                rsAnalises2.Close
                Set rsAnalises2 = Nothing
            rsAnalises.MoveNext
        Wend
    End If
    rsAnalises.Close
    Set rsAnalises = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "EnvioFact" & "EF_PreencheAnalises :" & Err.Description
    BG_Mensagem mediMsgBox, "Erro ao preencher An�lises: " & Err.Description, vbOKOnly + vbInformation, "EF_PreencheAnalises"
    EF_PreencheAnalises = -1
    Exit Function
    Resume Next
End Function

Public Function EF_PreencheEstrutAnalises(ByVal linhaR As Long, ByVal linhaA As Long, ByVal cod_facturavel As String, ByVal cod_ana As String, ByVal descr_ana As String, ByVal cod_ana_gh As String, ByVal _
                                          descr_ana_facturacao As String, ByVal p1 As String, ByVal Flg_Facturado As Integer, ByVal flg_retirado As Integer, ByVal Flg_adicionada As Integer, ByVal _
                                          isencao As Integer, ByVal n_rec As Long, ByVal serie_rec As String, ByVal estado_rec As String, ByVal ordem_marcacao As Integer, ByVal _
                                          cor_p1 As String, ByVal cod_efr As String, ByVal peso_estatistico As Integer, ByVal quantidade As Integer, ByVal cod_medico As String, ByVal _
                                          n_benef As String, ByVal fds As Integer, ByVal cod_empresa As String, ByVal flg_fds_fixo As Integer, ByVal flg_recibo_manual As Integer, _
                                          cod_efr_envio As String, quantidade_envio As Integer, seq_Recibo As Long) As Long
    Dim i As Integer
    Dim taxa As String
    Dim mens As String
    Dim PercUtente As String
    On Error GoTo TrataErro
    If linhaA = mediComboValorNull Then
        EF_ReqFact(linhaR).TotalAnalisesFact = EF_ReqFact(linhaR).TotalAnalisesFact + 1
        linhaA = EF_ReqFact(linhaR).TotalAnalisesFact
        ReDim Preserve EF_ReqFact(linhaR).AnalisesFact(linhaA)
    End If
    For i = linhaA + 1 To EF_ReqFact(linhaR).TotalAnalisesFact
        EF_ReqFact(linhaR).AnalisesFact(i).ordem = EF_ReqFact(linhaR).AnalisesFact(i).ordem + 1
    Next i
    ' ---------------------------------------------------------------------------
    ' PREENCHE A ESTRUTURA
    ' ---------------------------------------------------------------------------
    EF_ReqFact(linhaR).AnalisesFact(linhaA).SeqRecibo = seq_Recibo
    EF_ReqFact(linhaR).AnalisesFact(linhaA).NReq = EF_ReqFact(linhaR).NReq
    EF_ReqFact(linhaR).AnalisesFact(linhaA).CodFacturavel = cod_facturavel
    EF_ReqFact(linhaR).AnalisesFact(linhaA).codAna = cod_ana
    EF_ReqFact(linhaR).AnalisesFact(linhaA).descrAna = descr_ana
    EF_ReqFact(linhaR).AnalisesFact(linhaA).CodAnaGH = cod_ana_gh
    EF_ReqFact(linhaR).AnalisesFact(linhaA).descrAnaGH = descr_ana_facturacao
    EF_ReqFact(linhaR).AnalisesFact(linhaA).p1 = p1
    EF_ReqFact(linhaR).AnalisesFact(linhaA).Flg_Facturada = Flg_Facturado
    EF_ReqFact(linhaR).AnalisesFact(linhaA).Flg_Retirada = flg_retirado
    EF_ReqFact(linhaR).AnalisesFact(linhaA).Flg_adicionada = Flg_adicionada
    EF_ReqFact(linhaR).AnalisesFact(linhaA).isencao = isencao
    EF_ReqFact(linhaR).AnalisesFact(linhaA).nRec = n_rec
    EF_ReqFact(linhaR).AnalisesFact(linhaA).serieRec = serie_rec
    EF_ReqFact(linhaR).AnalisesFact(linhaA).estadoRec = estado_rec
    EF_ReqFact(linhaR).AnalisesFact(linhaA).ordem = ordem_marcacao
    If ordem_marcacao <= 0 Then
        EF_ReqFact(linhaR).AnalisesFact(linhaA).ordem = linhaA
    End If
    EF_ReqFact(linhaR).AnalisesFact(linhaA).cor_p1 = cor_p1
    EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_efr = cod_efr
    EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_efr_envio = cod_efr_envio
    EF_ReqFact(linhaR).AnalisesFact(linhaA).Peso = peso_estatistico
    EF_ReqFact(linhaR).AnalisesFact(linhaA).tratado = False
    EF_ReqFact(linhaR).AnalisesFact(linhaA).Qtd_Ana = quantidade
    EF_ReqFact(linhaR).AnalisesFact(linhaA).qtd_ana_envio = BL_HandleNull(quantidade_envio, quantidade)
    EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_medico = cod_medico
    EF_ReqFact(linhaR).AnalisesFact(linhaA).nr_benef = n_benef
    EF_ReqFact(linhaR).AnalisesFact(linhaA).fds = fds
    EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_empresa = cod_empresa
    EF_ReqFact(linhaR).AnalisesFact(linhaA).flg_FdsValFixo = flg_fds_fixo
    EF_ReqFact(linhaR).AnalisesFact(linhaA).Portaria = EF_ReqFact(linhaR).portariaDefeito
    EF_ReqFact(linhaR).AnalisesFact(linhaA).data = EF_ReqFact(linhaR).dt_chega
    
    EF_ReqFact(linhaR).AnalisesFact(linhaA).flg_percentagem = EF_ReqFact(linhaR).flg_perc_facturar
    EF_ReqFact(linhaR).AnalisesFact(linhaA).flg_controlaIsencao = EF_ReqFact(linhaR).flg_controla_isencao
    EF_ReqFact(linhaR).AnalisesFact(linhaA).flg_recibo_manual = flg_recibo_manual
    EF_ReqFact(linhaR).AnalisesFact(linhaA).flg_novaArs = EF_ReqFact(linhaR).flg_novaArs
    
    If EF_ReqFact(linhaR).AnalisesFact(linhaA).flg_percentagem = False Then
        EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescEnvio = ""
        EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescCodif = ""
        EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescUtente = ""
    Else
        EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescEnvio = EF_RetornaDescontoEnvio(EF_ReqFact(linhaR).AnalisesFact(linhaA).NReq, EF_ReqFact(linhaR).AnalisesFact(linhaA).nRec, EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_efr)
        EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescCodif = IF_RetornaPercentPagEFR(EF_ReqFact(linhaR).codEfr, EF_ReqFact(linhaR).AnalisesFact(linhaA).CodAnaGH, EF_ReqFact(linhaR).dt_chega)
        EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescUtente = EF_RetornaDescontoUtente(linhaR, EF_ReqFact(linhaR).AnalisesFact(linhaA).serieRec, EF_ReqFact(linhaR).AnalisesFact(linhaA).nRec, EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_efr)
    End If
    ' APENAS ENVIA REQUISICOES SE TIVER RECIBO EMITIDO (PARA ENTIDADES COM ESTA FLAG)
    If EF_ReqFact(linhaR).flg_obriga_recibo = 1 Then
        EF_ReqFact(linhaR).AnalisesFact(linhaA).flg_ObrigaRecibo = True
    Else
        EF_ReqFact(linhaR).AnalisesFact(linhaA).flg_ObrigaRecibo = False
    End If
    
    'PREENCHE CODIGO DA RUBRICA PARA DEDUZIR A ENTIDADE PORQUE UTENTE JA PAGOU
    ' LHL - COMP. SEGUROS ACOREANA
    EF_ReqFact(linhaR).AnalisesFact(linhaA).deducaoUtente = EF_ReqFact(linhaR).deducao_ute
    
    'PARA ANALISES ASSOCIADAS A HEMODIALISE NAO CALCULA PRECO PELA TABELA, SOMA PRECOS DOS UTENTES
    If EF_ReqFact(linhaR).hemodialise > mediComboValorNull And EF_ReqFact(linhaR).cod_ana_hemodialise = EF_ReqFact(linhaR).AnalisesFact(linhaA).CodFacturavel Then
        EF_ReqFact(linhaR).AnalisesFact(linhaA).preco = 0
        For i = 1 To linhaA
            If EF_ReqFact(linhaR).cod_ana_hemodialise <> EF_ReqFact(linhaR).AnalisesFact(i).CodFacturavel Then
                EF_ReqFact(linhaR).AnalisesFact(linhaA).preco = EF_ReqFact(linhaR).AnalisesFact(linhaA).preco + EF_ReqFact(linhaR).AnalisesFact(i).precoUte
                EF_ReqFact(linhaR).AnalisesFact(linhaA).PrecoUnitario = EF_ReqFact(linhaR).AnalisesFact(linhaA).preco
            End If
        Next
    Else
        EF_PreencheTaxaUtente linhaR, linhaA
        taxa = 0
        EF_PreencheTaxaEFR linhaR, linhaA
    End If
    
    ' ---------------------------------------------------------------------------
    ' VERIFICA SE ENVIA PARA FACTUS PERCENTAGEM DESCONTO DO UTENTE
    ' ---------------------------------------------------------------------------
    If EF_ReqFact(linhaR).AnalisesFact(linhaA).flg_percentagem = True Then
        If CDbl(EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescCodif) < 100 Then
            EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescEnvio = CDbl(EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescCodif)
        Else
            EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescEnvio = CDbl(EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescUtente)
        End If
    End If
    EF_PreencheEstrutAnalises = linhaA
Exit Function
TrataErro:
    EF_PreencheEstrutAnalises = mediComboValorNull
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "EnvioFactus", "EF_PreencheEstrutAnalises"
    Exit Function
    Resume Next
End Function

Private Sub EF_PreencheRecibosReq(ByVal linhaR As Long)
    Dim ssql As String
    Dim rsRec As New adodb.recordset
    On Error GoTo TrataErro
    
    If EF_ReqFact(linhaR).NReq <> "0" Then
        ssql = "SELECT n_rec ,total_pagar, serie_rec, cod_efr,estado from sl_recibos where  n_req= " & BL_TrataStringParaBD(EF_ReqFact(linhaR).NReq)
        rsRec.CursorLocation = adUseServer
        rsRec.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsRec.Open ssql, gConexao
        If rsRec.RecordCount > 0 Then
            EF_ReqFact(linhaR).TotalRecibos = 0
            ReDim EF_ReqFact(linhaR).EstrutRecibos(0)
            While Not rsRec.EOF
                EF_ReqFact(linhaR).TotalRecibos = EF_ReqFact(linhaR).TotalRecibos + 1
                ReDim Preserve EF_ReqFact(linhaR).EstrutRecibos(EF_ReqFact(linhaR).TotalRecibos)
                EF_ReqFact(linhaR).EstrutRecibos(EF_ReqFact(linhaR).TotalRecibos).serie_rec = BL_HandleNull(rsRec!serie_rec, "0")
                EF_ReqFact(linhaR).EstrutRecibos(EF_ReqFact(linhaR).TotalRecibos).n_rec = BL_HandleNull(rsRec!n_rec, "0")
                EF_ReqFact(linhaR).EstrutRecibos(EF_ReqFact(linhaR).TotalRecibos).total = BL_HandleNull(rsRec!total_pagar, "0")
                EF_ReqFact(linhaR).EstrutRecibos(EF_ReqFact(linhaR).TotalRecibos).cod_efr = BL_HandleNull(rsRec!cod_efr, "0")
                EF_ReqFact(linhaR).EstrutRecibos(EF_ReqFact(linhaR).TotalRecibos).Estado = BL_HandleNull(rsRec!Estado, "")
                rsRec.MoveNext
            Wend
        End If
        rsRec.Close
        Set rsRec = Nothing
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "EnvioFactus", "PreencheRecibosReq"
    Exit Sub
    Resume Next
End Sub

Public Function EF_RetornaDescontoEnvio(n_req As String, n_rec As String, cod_efr As String) As String
    Dim ssql As String
    Dim rsDesc As New adodb.recordset
    On Error GoTo TrataErro
    
    EF_RetornaDescontoEnvio = ""
    ssql = " SELECT desconto_envio FROM sl_recibos where n_req = " & BL_TrataStringParaBD(n_req) & " AND cod_efr = " & cod_efr
    If n_rec <> "0" Then
        ssql = ssql & " AND n_rec = " & n_rec
    End If
    
    rsDesc.CursorLocation = adUseServer
    rsDesc.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsDesc.Open ssql, gConexao
    If rsDesc.RecordCount > 0 Then
        EF_RetornaDescontoEnvio = BL_HandleNull(rsDesc!desconto_envio, "0")
    End If
    rsDesc.Close
    Set rsDesc = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "EnvioFactus", "EF_RetornaDescontoEnvio"
    EF_RetornaDescontoEnvio = ""
    Exit Function
    Resume Next
End Function

Public Function EF_RetornaDeducaoUtente(cod_efr As String) As String
    Dim ssql As String
    Dim rsDesc As New adodb.recordset
    EF_RetornaDeducaoUtente = "0"
    ssql = " SELECT deducao_ute FROM sl_efr where cod_efr = " & cod_efr
    rsDesc.CursorLocation = adUseServer
    rsDesc.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsDesc.Open ssql, gConexao
    If rsDesc.RecordCount > 0 Then
        EF_RetornaDeducaoUtente = BL_HandleNull(rsDesc!deducao_ute, "0")
    End If
    rsDesc.Close
    Set rsDesc = Nothing
End Function




Public Function EF_RetornaDescontoUtente(linhaR As Long, serie_rec As String, n_rec As String, cod_efr As String) As String
    Dim ssql As String
    Dim rsDesc As New adodb.recordset
    EF_RetornaDescontoUtente = "0"
    ssql = " SELECT desconto, estado  FROM sl_recibos where n_req = " & BL_TrataStringParaBD(EF_ReqFact(linhaR).NReq) & " AND cod_efr = " & cod_efr
    If n_rec <> "0" Then
        ssql = ssql & " AND n_rec = " & n_rec
    End If
    
    rsDesc.CursorLocation = adUseServer
    rsDesc.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsDesc.Open ssql, gConexao
    If rsDesc.RecordCount > 0 Then
        If BL_HandleNull(rsDesc!Estado, "") = "NL" Then
            EF_RetornaDescontoUtente = 0
        Else
            EF_RetornaDescontoUtente = 100 - BL_HandleNull(rsDesc!Desconto, "100")
        End If
    ElseIf rsDesc.RecordCount = 0 Then
        rsDesc.Close
        ssql = " SELECT desconto,estado FROM sl_recibos where n_req = " & BL_TrataStringParaBD(EF_ReqFact(linhaR).NReq) & " AND cod_efr = " & cod_efr
        ssql = ssql & " AND n_rec = " & n_rec
        rsDesc.CursorLocation = adUseServer
        rsDesc.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsDesc.Open ssql, gConexao
        If rsDesc.RecordCount > 0 Then
            If BL_HandleNull(rsDesc!Estado, "") = "NL" Then
                EF_RetornaDescontoUtente = 0
            Else
                EF_RetornaDescontoUtente = 100 - BL_HandleNull(rsDesc!Desconto, "100")
            End If
        End If
    End If
    rsDesc.Close
    Set rsDesc = Nothing
End Function


' ---------------------------------------------------------------------------

' ENVIA DADOS PARA FACTUS

' ---------------------------------------------------------------------------

Public Function EF_EnviaDados(ByVal linhaR As Long) As Boolean

    On Error GoTo TrataErro
    Dim n_seq_prog As Long
    Dim n_ord_ins As Integer
    Dim rv As Integer
    Dim i As Integer
    Dim j As Integer
    Dim ssql As String
    Dim n_ord As Long
    Dim flg_pagouTaxa As String
    Dim cod_isen_doe As Integer
    Dim flg_tip_req As String
    Dim LastEFR As String
    Dim LastCodEmpresa As String
    Dim LastNrBENEF As String
    Dim LastRecibo As String
    Dim lastP1 As String
    Dim codDomicilio As Integer
    Dim flg_estado_doe As Integer
    Dim serie_fac_doe As String
    Dim n_Fac_doe As Long
    Dim FLG_MostraAviso As Boolean
    Dim PercUtente As String
    Dim episodio As String
    Dim flg_ignorarMsg As Boolean
    Dim flg_insere_cod_rubr_efr As Boolean
    Dim nr_req_ars As String
    Dim chave_prog As String
    Dim codServExec As String
    Dim flgValidaNrReqARS  As Boolean
    Dim rsResp As adodb.recordset
    
    
    If EF_ReqFact(linhaR).cod_proven_efr <> "" Then
        If EF_ReqFact(linhaR).cod_proven_efr <> EF_ReqFact(linhaR).codEfr Then
            If BG_Mensagem(mediMsgBox, "A proveni�ncia: " & EF_ReqFact(linhaR).descr_proven & " N�o pertence � entidade: " & EF_ReqFact(linhaR).DescrEFR & ". Pretende enviar mesmo assim?", vbOKOnly + vbInformation, "Factura��o") = vbNo Then
                EF_EnviaDados = False
                Exit Function
            End If
        End If
    End If
    
    
    
    If EF_VerificaRecADSE(linhaR) = False Then
        BG_Mensagem mediMsgBox, "Existem diferen�as entre o(s) recibo(s) emitidos e as an�lises enviadas da ADSE.", vbOKOnly + vbInformation, "Factura��o"
        EF_EnviaDados = False
        Exit Function
    End If
    
    If EF_VerificaMedicoPreenchidoADSE(linhaR) = False Then
        BG_Mensagem mediMsgBox, "Existem an�lises sem M�dico requisitante definido.", vbOKOnly + vbInformation, "Factura��o"
        EF_EnviaDados = False
        Exit Function
    End If
    
    lastP1 = ""
    For i = 1 To EF_ReqFact(linhaR).TotalAnalisesFact
        If Len(EF_ReqFact(linhaR).AnalisesFact(i).p1) = 19 And lastP1 <> EF_ReqFact(linhaR).AnalisesFact(i).p1 Then
            lastP1 = EF_ReqFact(linhaR).AnalisesFact(i).p1
            If IF_ValidaReqArs(EF_ReqFact(linhaR).AnalisesFact(i).p1, True) = False Then
                BG_Mensagem mediMsgBox, "Requisi��o ARS(P1) n�o � v�lido. Linha:" & i, vbOKOnly + vbInformation, "Factura��o"
                EF_EnviaDados = False
                Exit Function
            End If
            
            If EF_VerificaCredJaExistente(EF_ReqFact(linhaR).AnalisesFact(i).p1, linhaR) = True Then
                BG_Mensagem mediMsgBox, "Credencial j� existente anteriormente", vbOKOnly + vbInformation, "Factura��o"
                EF_EnviaDados = False
                Exit Function
            End If
            
            If EF_ReqFact(linhaR).flg_novaArs = mediSim Then
                If EF_VerificaTotalAnaCred(EF_ReqFact(linhaR).AnalisesFact(i).p1, linhaR) = False Then
                    BG_Mensagem mediMsgBox, "Existem credenciais da ARS com mais que 6 an�lises", vbOKOnly + vbInformation, "Factura��o"
                    EF_EnviaDados = False
                    Exit Function
                End If
            End If
        End If
    Next
    ' ------------------------------------------------------------------------------------
    ' VERIFICA O ESTADO DAS REQUISICOES. SE JA ESTA FACTURADO NAO PERMITE REENVIAR.
    ' -------------------------------------------------------------------------------------
    If EF_ReqFact(linhaR).estadoFact = gEstadoFactRequisFacturado Then
        If (BG_Mensagem(mediMsgBox, "Requisi��o j� foi enviada para o FACTUS. Deseja enviar an�lises n�o enviadas?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbNo) Then
            EF_EnviaDados = False
            Exit Function
        End If
    End If
    
    ' ------------------------------------------------------------------------------------
    ' VERIFICA DATA DA REQUISICAO
    ' -------------------------------------------------------------------------------------
    If EF_ReqFact(linhaR).dt_chega = "" Then
        BG_Mensagem mediMsgBox, "A Data da Requisi��o n�o pode estar vazia.", vbOKOnly + vbInformation, "Factura��o"
        EF_EnviaDados = False
        Exit Function
    End If
    
    gSQLError = 0
    BG_BeginTransaction
    
    n_ord = 0
    LastEFR = ""
    lastP1 = ""
    LastCodEmpresa = ""
    FLG_MostraAviso = False
    
    '----------------------------------------
    'ACTUALIZA DADOS DA REQUISI��O ALTERADOS
    '----------------------------------------
    ssql = "UPDATE sl_requis SET Seq_utente_fact = " & IIf(EF_ReqFact(linhaR).SeqUtenteFact > -1, EF_ReqFact(linhaR).SeqUtenteFact, "Null") & _
        ", cod_med = " & BL_TrataStringParaBD(EF_ReqFact(linhaR).codMed) & _
        ", cod_sala = " & BL_HandleNull(EF_ReqFact(linhaR).cod_sala, "null") & _
        ", cod_efr = " & EF_ReqFact(linhaR).codEfr & _
        ", n_benef = " & BL_TrataStringParaBD(EF_ReqFact(linhaR).n_benef) & _
        ", cod_urbano = " & EF_ReqFact(linhaR).codUrbano & _
        ", km = " & EF_ReqFact(linhaR).km & " " & _
        ", convencao = " & EF_ReqFact(linhaR).convencao & " Where n_req = " & EF_ReqFact(linhaR).NReq
    BG_ExecutaQuery_ADO ssql
    

    
    ' -------------------------------------------------------------------------------------------------
    ' TRATA LINHA A LINHA. INSERE NA FA_MOVI_FACT. ACTUALIZA NA ESTRUTURA. E GRAVA NA SL_RECIBOS_DET
    ' -------------------------------------------------------------------------------------------------
    flg_ignorarMsg = False
    For i = 1 To EF_ReqFact(linhaR).TotalAnalisesFact
        If (EF_ReqFact(linhaR).AnalisesFact(i).Flg_Facturada = gEstadoFactRequisNaoFacturado Or EF_ReqFact(linhaR).AnalisesFact(i).Flg_Facturada = gEstadoFactRequisRejeitado) _
            And EF_ReqFact(linhaR).AnalisesFact(i).Flg_Retirada = False And EF_ReqFact(linhaR).AnalisesFact(i).cod_efr_envio <> gEfrParticular Then
            
            ' ------------------------------------------------------------------------------------------------------------------
            'APENAS ENVIA REQUISICOES QUE NAO SEJA OBRIGATORIO RECIBO, OU ENTAO QUE SEJA OBRIGATORIO E TENHA NUMERO RECIBO
            ' ------------------------------------------------------------------------------------------------------------------
            If EF_ReqFact(linhaR).AnalisesFact(i).flg_ObrigaRecibo <> True Or EF_ReqFact(linhaR).AnalisesFact(i).nRec <> 0 Or EF_ReqFact(linhaR).AnalisesFact(i).estadoRec = gEstadoReciboNulo Then
            
                If flg_ignorarMsg = False Then
                    If EF_ReqFact(linhaR).flg_perc_facturar = "1" And EF_ReqFact(linhaR).AnalisesFact(i).PercDescEnvio = "100" Then
                        If (BG_Mensagem(mediMsgBox, "Aten��o.Requisi��o em causa vai ser enviada com valor 0 para entidade. Deseja Continuar?", _
                                                    vbQuestion + vbYesNo + vbDefaultButton2, _
                                                    " " & cAPLICACAO_NOME_CURTO) = vbNo) Then
                            Exit Function
                        Else
                            flg_ignorarMsg = True
                        End If
                    End If
                End If
                ' --------------------------------------
                ' SE ENTIDADE DIF ENTAO INCREMENTA N_ORD
                ' --------------------------------------
                If EF_ReqFact(linhaR).AnalisesFact(i).cod_efr_envio <> LastEFR Then
                    'soliveira ljjno 16.12.2008
                    'selecciona n_ord da entidade em quest�o, caso ja exista registo na fa_movi_resp para esse episodio
                    ssql = "select n_ord from fa_movi_resp where t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(EF_ReqFact(linhaR).NReq) & " and cod_efr = " & EF_ReqFact(linhaR).AnalisesFact(i).cod_efr_envio
                    Set rsResp = New adodb.recordset
                    rsResp.CursorLocation = adUseServer
                    rsResp.CursorType = adOpenStatic
                    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
                    rsResp.Open ssql, gConexaoSecundaria
                    If rsResp.RecordCount > 0 Then
                        n_ord = BL_HandleNull(rsResp!n_ord, 1)
                    Else
                        ssql = "select nvl(max(n_ord),0) conta_n_ord from fa_movi_resp where t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(EF_ReqFact(linhaR).NReq)
                        Set rsResp = New adodb.recordset
                        rsResp.CursorLocation = adUseServer
                        rsResp.CursorType = adOpenStatic
                        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
                        rsResp.Open ssql, gConexaoSecundaria
                        If rsResp.RecordCount > 0 Then
                            n_ord = rsResp!conta_n_ord + 1
                        End If
                    End If
                    rsResp.Close
                    Set rsResp = Nothing
                    'N_Ord = N_Ord + 1
                End If
                If EF_ReqFact(linhaR).AnalisesFact(i).p1 <> lastP1 And EF_ReqFact(linhaR).AnalisesFact(i).CodAnaGH <> "" Then
                    n_ord_ins = 1
                Else
                    n_ord_ins = n_ord_ins + 1
                End If
                

                
                ' ------------------------------------------------------------------------------------
                ' DADOS DOS RECIBOS
                ' -------------------------------------------------------------------------------------
                If EF_ReqFact(linhaR).AnalisesFact(i).nRec <> 0 Then
                    flg_estado_doe = 3
                    n_Fac_doe = EF_ReqFact(linhaR).AnalisesFact(i).nRec
                    serie_fac_doe = EF_ReqFact(linhaR).AnalisesFact(i).serieRec
                Else
                    flg_estado_doe = -1
                    n_Fac_doe = -1
                    serie_fac_doe = ""
                End If
                
                ' ------------------------------------------------------------------------------------
                ' ISENCAO
                ' -------------------------------------------------------------------------------------
                If EF_ReqFact(linhaR).AnalisesFact(i).isencao = gTipoIsencaoIsento Or EF_ReqFact(linhaR).hemodialise <> mediComboValorNull Then
                    flg_pagouTaxa = "N"
                    cod_isen_doe = "1"
                ElseIf EF_ReqFact(linhaR).AnalisesFact(i).isencao = gTipoIsencaoBorla Or EF_ReqFact(linhaR).AnalisesFact(i).isencao = gTipoIsencaoNaoIsento Then
                    flg_pagouTaxa = "S"
                    cod_isen_doe = "0"
                End If
                
                'SE ARS FOR DAS NOVAS, VERIFICA SE � DAS CREDENCIAIS NOVAS OU ANTIGAS
                If EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 Then
                    flgValidaNrReqARS = IF_ValidaReqArs(EF_ReqFact(linhaR).AnalisesFact(i).p1, False)
                End If
                    
                ' ------------------------------------------------------------------------------------
                ' FLG_TIP_REQ - TIPO DE DOMICILIO(N,D,I,C) CONCATENADO COM TIPO DE P1(B,V)
                ' HA ENTIDADES EM QUE DOMICILIO NAO E COMPARTICIPADO PELO QUE NAO PODE IR PARA FACTUS
                ' -------------------------------------------------------------------------------------
                If EF_ReqFact(linhaR).flg_compart_dom = "1" Then
                    ' VERIFICA SE PARA EFR CAUSA SE ENVIA SEMPRE DOMICILIO
                    If EF_ReqFact(linhaR).flg_dom_defeito <> "1" Then
                        If EF_ReqFact(linhaR).codUrbano <> 0 Then
                            If Len(EF_ReqFact(linhaR).codPostal) <> 8 Then
                                BG_Mensagem mediMsgBox, "O c�digo postal deve ser no formato XXXX-XXX. Requisi��o n�o enviada.", vbOKOnly + vbInformation, "Factura��o"
                                EF_EnviaDados = False
                                Exit Function
                            End If
                            'Verifica se EFR faz separa��o de lotes por domicilio
                            If EF_ReqFact(linhaR).flg_separa_dom = "1" Then
                                If EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = True Then
                                    flg_tip_req = "0"
                                ElseIf EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = False Then
                                    flg_tip_req = "3"
                                Else
                                    flg_tip_req = "D" & EF_ReqFact(linhaR).AnalisesFact(i).cor_p1
                                End If
                            ' VERIFICA SE ESTAO SOB REGRA DOS 65 ANOS
                            ElseIf EF_ReqFact(linhaR).AnalisesFact(i).isencao = gTipoIsencaoBorla Or EF_ReqFact(linhaR).AnalisesFact(i).isencao = gTipoIsencaoNaoIsento And _
                                   EF_VerificaRecADSE(linhaR) = True Then
                                If EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = True Then
                                    flg_tip_req = "0"
                                ElseIf EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = False Then
                                    flg_tip_req = "3"
                                Else
                                    flg_tip_req = "C" & EF_ReqFact(linhaR).AnalisesFact(i).cor_p1
                                End If
                            Else
                                If EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = True Then
                                    flg_tip_req = "0"
                                ElseIf EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = False Then
                                    flg_tip_req = "3"
                                Else
                                    flg_tip_req = "N" & EF_ReqFact(linhaR).AnalisesFact(i).cor_p1
                                End If
                            End If
                            codDomicilio = 0
                            codDomicilio = EF_ReqFact(linhaR).codDom
                            ' VERIFICA SE ENTIDADE PODE FACTURAR NAO URBANOS
                            If EF_ReqFact(linhaR).flg_compart_dom = "0" Then
                                codDomicilio = lCodUrbano
                            End If
                            If codDomicilio = lCodUrbano Then
                                EF_ReqFact(linhaR).km = 1
                            ElseIf codDomicilio = lCodUrbanoBiDiario Then
                                EF_ReqFact(linhaR).km = 2
                            End If
                        End If
                    Else
                        If EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = True Then
                            flg_tip_req = "0"
                        ElseIf EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = False Then
                            flg_tip_req = "3"
                        Else
                            flg_tip_req = "D" & EF_ReqFact(linhaR).AnalisesFact(i).cor_p1
                        End If
                        codDomicilio = lCodUrbano
                        EF_ReqFact(linhaR).km = 1
                    End If
                Else
                    If EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = True Then
                        flg_tip_req = "0"
                    ElseIf EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = False Then
                        flg_tip_req = "3"
                    Else
                        flg_tip_req = "N" & EF_ReqFact(linhaR).AnalisesFact(i).cor_p1
                    End If
                    codDomicilio = 0
                    EF_ReqFact(linhaR).km = 0
                    
                End If
                    
                If EF_ReqFact(linhaR).convencao = gConvencaoInternacional Then
                    If EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = True Then
                        flg_tip_req = "2"
                    ElseIf EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = False Then
                        flg_tip_req = "5"
                    Else
                        flg_tip_req = "I" & EF_ReqFact(linhaR).AnalisesFact(i).cor_p1
                    End If
               ElseIf EF_ReqFact(linhaR).convencao = gConvencaoProfissional Then
                    If EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = True Then
                        flg_tip_req = "1"
                    ElseIf EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = False Then
                        flg_tip_req = "4"
                    Else
                        flg_tip_req = "P" & EF_ReqFact(linhaR).AnalisesFact(i).cor_p1
                    End If
                ' VERIFICA SE ESTAO SOB REGRA DOS 65 ANOS
                ElseIf EF_ReqFact(linhaR).AnalisesFact(i).isencao = gTipoIsencaoBorla Or EF_ReqFact(linhaR).AnalisesFact(i).isencao = gTipoIsencaoNaoIsento And _
                        EF_VerificaRegraArs(linhaR) = True Then
                    If EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = True Then
                        flg_tip_req = "0"
                    ElseIf EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = False Then
                        flg_tip_req = "3"
                    Else
                        flg_tip_req = "C" & EF_ReqFact(linhaR).AnalisesFact(i).cor_p1
                    End If
                ElseIf EF_ReqFact(linhaR).convencao = gconvencaoNormal Then
                    If EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = True Then
                        flg_tip_req = "0"
                    ElseIf EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = 1 And flgValidaNrReqARS = False Then
                        flg_tip_req = "3"
                    Else
                        flg_tip_req = "N" & EF_ReqFact(linhaR).AnalisesFact(i).cor_p1
                    End If
                End If
                
                ' ------------------------------------------------------------------------------------
                ' FIM SEMANA - SE NAO ESTIVER COMO FDS MAS DIA FOR DE SEMANA ENVIA ULTIMO DOMINGO
                ' MARTELADA E DAS GRANDES....
                ' -------------------------------------------------------------------------------------
                If EF_ReqFact(linhaR).fimSemana = 1 Then
                  If Weekday(EF_ReqFact(linhaR).AnalisesFact(i).data) <> 1 Then
                    Dim k As Integer
                    For k = 1 To 7
                        If Weekday(CDate(EF_ReqFact(linhaR).AnalisesFact(i).data) - CDate(k)) = 1 Then
                            EF_ReqFact(linhaR).AnalisesFact(i).data = CDate(CDate(EF_ReqFact(linhaR).AnalisesFact(i).data) - CDate(k))
                            Exit For
                        End If
                    Next
                  End If
                End If
                
                ' QUE MARTELAN�O..........
                episodio = ""
                episodio = IIf(gLAB = "LJM" And EF_ReqFact(linhaR).NReq < 650000, Trim(EF_ReqFact(linhaR).NReqAssoc), Trim(EF_ReqFact(linhaR).NReq))
                
                'soliveira correccao lacto
                EF_ReqFact(linhaR).AnalisesFact(i).ordem = n_ord_ins
                
                If (EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = mediSim And Len(EF_ReqFact(linhaR).AnalisesFact(i).p1) >= 4) Or Len(EF_ReqFact(linhaR).AnalisesFact(i).p1) = 19 Then
                    ' NOVA ARS
                    nr_req_ars = EF_ReqFact(linhaR).AnalisesFact(i).p1
                    chave_prog = episodio
                ElseIf EFR_Verifica_ADSE(EF_ReqFact(linhaR).codEfr) = True Then
                    ' nOVA ADSE
                    nr_req_ars = EF_ReqFact(linhaR).AnalisesFact(i).p1
                    chave_prog = episodio
                Else
                    nr_req_ars = episodio & EF_ReqFact(linhaR).AnalisesFact(i).p1
                    chave_prog = episodio
                End If
                
                If EF_ReqFact(linhaR).AnalisesFact(i).CodAnaGH <> "" Then
                
                    ' VERIFICA SE ENVIA PARA FACTUS PERCENTAGEM DESCONTO DO UTENTE
                    If EF_ReqFact(linhaR).AnalisesFact(i).PercDescEnvio = "" Then
                        EF_ReqFact(linhaR).AnalisesFact(i).PercDescEnvio = "0"
                    End If
                    
                    'VERIFICA SE INSERE OU NAO O CODIGO DA RUBRICA DA ENTIDADE
                    If EF_ReqFact(linhaR).flg_insere_cod_rubr_efr = "1" Then
                        flg_insere_cod_rubr_efr = True
                    Else
                        flg_insere_cod_rubr_efr = False
                    End If
                    'VERIFICA QUAL A SALA ASSOCIADA. SALA VAI NO CAMPO SERVICO EXECUTANTE
                    codServExec = BL_HandleNull(EF_ReqFact(linhaR).cod_sala, gCodServExec)
                    
                    ' ------------------------------------------------------------------------------------------------------------------
                    ' N�O ENVIA AN�LISES DE FIM DE SEMANA EM QUE ENTIDADES TEM VALOR FIXO
                    ' ------------------------------------------------------------------------------------------------------------------
                    If EF_ReqFact(linhaR).fimSemana = 0 Or (EF_ReqFact(linhaR).fimSemana = 1 And EF_ReqFact(linhaR).AnalisesFact(i).flg_FdsValFixo = 0) Then
                        n_seq_prog = IF_RetornaNSeqProg
                        IF_Factura_AnalisePrivado gCodProg, n_seq_prog, n_ord, "SISLAB", episodio, EF_ReqFact(linhaR).TipoUtente, EF_ReqFact(linhaR).Utente, BL_HandleNull(EF_ReqFact(linhaR).AnalisesFact(i).data, Bg_DaData_ADO), _
                                                 EF_ReqFact(linhaR).AnalisesFact(i).CodAnaGH, nr_req_ars, flg_pagouTaxa, EF_ReqFact(linhaR).AnalisesFact(i).qtd_ana_envio, _
                                                 CStr(codServExec), 1, chave_prog, CStr(gCodUtilizador), _
                                                 Bg_DaData_ADO, cod_isen_doe, EF_ReqFact(linhaR).AnalisesFact(i).cor_p1, flg_tip_req, EF_ReqFact(linhaR).AnalisesFact(i).ordem, _
                                                 flg_estado_doe, serie_fac_doe, n_Fac_doe, EF_ReqFact(linhaR).AnalisesFact(i).cod_rubr_efr, CStr(EF_ReqFact(linhaR).AnalisesFact(i).PrecoUnitario), _
                                                 CDbl(EF_ReqFact(linhaR).AnalisesFact(i).PercDescEnvio), EF_ReqFact(linhaR).AnalisesFact(i).cod_empresa, EF_ReqFact(linhaR).AnalisesFact(i).flg_controlaIsencao, _
                                                 EF_ReqFact(linhaR).AnalisesFact(i).flg_percentagem, flg_insere_cod_rubr_efr
                        
                        ' ------------------------------------------------------------------------------------------------------------------
                        ' PARA AS ANALISES DA ADSE INSERE NUMA TABELA A PARTE PARA FAZER JOIN COM MOVI_FACT ATRAVES DO N_SEQ_PROG
                        ' ------------------------------------------------------------------------------------------------------------------
                        ' para mais tarde n�o haver desmentidos: BM disse para escrever na tabela caso p1 fosse diferente de 1
                        ' agora ja quer enviar tudo....
                        If EFR_Verifica_ADSE(EF_ReqFact(linhaR).codEfr) = True Then
                            Call IF_InsereAnaliseADSE(n_seq_prog, nr_req_ars, BL_HandleNull(EF_ReqFact(linhaR).AnalisesFact(i).cod_medico, EF_ReqFact(linhaR).codMed), EF_ReqFact(linhaR).cod_proven)
                        End If
                        
                        EF_ReqFact(linhaR).AnalisesFact(i).Flg_Facturada = gEstadoFactRequisFacturado
                        
                    ElseIf EF_ReqFact(linhaR).fimSemana = 1 And EF_ReqFact(linhaR).AnalisesFact(i).flg_FdsValFixo = 1 Then
                        EF_ReqFact(linhaR).AnalisesFact(i).Flg_Facturada = gEstadoFactRequisNaoFacturado
                        EF_ReqFact(linhaR).AnalisesFact(i).Flg_Retirada = True
                    End If
                End If
                
                ' ------------------------------------------------------------------------------------
                ' SE FOR UM NOVO P1, ENTAO ENVIA CABE�ALHO PARA TABELA SL_REQ_ARS
                ' -------------------------------------------------------------------------------------
                If EF_ReqFact(linhaR).AnalisesFact(i).p1 <> lastP1 And EF_ReqFact(linhaR).AnalisesFact(i).CodAnaGH <> "" Then
                    lastP1 = EF_ReqFact(linhaR).AnalisesFact(i).p1
                    If EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = mediSim Then
                        IF_InsereReqARS EF_ReqFact(linhaR).NReq, lastP1, "A", CStr(codDomicilio), CStr(EF_ReqFact(linhaR).km), EF_ReqFact(linhaR).codPostal, _
                                        EF_ReqFact(linhaR).localidade, "", "", EF_ReqFact(linhaR).TipoUtente, EF_ReqFact(linhaR).Utente, _
                                         EF_ReqFact(linhaR).convencao, flg_tip_req
                    End If
                End If
                ' ------------------------------------------------------------------------------------
                ' SE FOR UMA NOVA ENTIDADE, ENTAO ENVIA CABE�ALHO PARA TABELA FA_MOVI_RESP
                ' -------------------------------------------------------------------------------------
                If EF_ReqFact(linhaR).AnalisesFact(i).cod_efr_envio <> LastEFR And EF_ReqFact(linhaR).AnalisesFact(i).CodAnaGH <> "" Then
                    LastEFR = EF_ReqFact(linhaR).AnalisesFact(i).cod_efr_envio
                    LastCodEmpresa = BL_HandleNull(EF_ReqFact(linhaR).AnalisesFact(i).cod_empresa, EF_ReqFact(linhaR).cod_empresa)
                    LastNrBENEF = BL_HandleNull(EF_ReqFact(linhaR).AnalisesFact(i).nr_benef, "")
                    IF_Factura_RequisicaoPrivado CInt(n_ord), "SISLAB", episodio, BL_HandleNull(LastNrBENEF, EF_ReqFact(linhaR).n_benef), LastEFR, _
                                                CStr(gCodUtilizador), codDomicilio, EF_ReqFact(linhaR).km, EF_ReqFact(linhaR).TipoUtente, _
                                                EF_ReqFact(linhaR).Utente, LastCodEmpresa
                    EF_ReqFact(linhaR).estadoFact = gEstadoFactRequisFacturado
                    
                    ' ------------------------------------------------------------------------------------
                    ' SE ENTIDADE TIVER CODIFICADO UMA RUBRICA PARA ENVIAR SEMPRE (DEDUCAO) DEDUCAO_UTE
                    ' -------------------------------------------------------------------------------------
                    If EF_ReqFact(linhaR).AnalisesFact(i).deducaoUtente <> "" And EF_ReqFact(linhaR).AnalisesFact(i).deducaoUtente <> "0" Then
                        n_seq_prog = IF_RetornaNSeqProg
                        IF_Factura_AnaliseExtra gCodProg, n_seq_prog, n_ord, "SISLAB", episodio, EF_ReqFact(linhaR).TipoUtente, _
                                                 EF_ReqFact(linhaR).Utente, BL_HandleNull(EF_ReqFact(linhaR).AnalisesFact(i).data, Bg_DaData_ADO), EF_ReqFact(linhaR).AnalisesFact(i).deducaoUtente, _
                                                 nr_req_ars, flg_pagouTaxa, EF_ReqFact(linhaR).AnalisesFact(i).qtd_ana_envio, _
                                                 CStr(codServExec), 1, chave_prog, CStr(gCodUtilizador), _
                                                 Bg_DaData_ADO, cod_isen_doe, EF_ReqFact(linhaR).AnalisesFact(i).cor_p1, flg_tip_req, EF_ReqFact(linhaR).AnalisesFact(i).ordem, _
                                                 flg_estado_doe, serie_fac_doe, n_Fac_doe, EF_ReqFact(linhaR).AnalisesFact(i).cod_rubr_efr, _
                                                 EF_RetornaTaxaRecibo(linhaR, EF_ReqFact(linhaR).AnalisesFact(i).serieRec, _
                                                 EF_ReqFact(linhaR).AnalisesFact(i).nRec, EF_ReqFact(linhaR).AnalisesFact(i).cod_efr_envio, True), LastCodEmpresa
                
                    End If
                    
                    ' ------------------------------------------------------------------------------------
                    ' SE ENTIDADE TIVER CODIFICADO PARA ENVIAR RUBRICA DE FIM DE SEMANA
                    ' -------------------------------------------------------------------------------------
                    If EF_ReqFact(linhaR).fimSemana = 1 And EF_ReqFact(linhaR).AnalisesFact(i).flg_FdsValFixo = 1 Then
                        n_seq_prog = IF_RetornaNSeqProg
                        IF_Factura_AnaliseExtra gCodProg, n_seq_prog, n_ord, "SISLAB", episodio, EF_ReqFact(linhaR).TipoUtente, _
                                                 EF_ReqFact(linhaR).Utente, EF_ReqFact(linhaR).AnalisesFact(i).data, gCodRubrFDS, _
                                                 nr_req_ars, flg_pagouTaxa, EF_ReqFact(linhaR).AnalisesFact(i).qtd_ana_envio, _
                                                 CStr(codServExec), 1, chave_prog, CStr(gCodUtilizador), _
                                                 Bg_DaData_ADO, cod_isen_doe, EF_ReqFact(linhaR).AnalisesFact(i).cor_p1, flg_tip_req, EF_ReqFact(linhaR).AnalisesFact(i).ordem, _
                                                 flg_estado_doe, serie_fac_doe, n_Fac_doe, EF_ReqFact(linhaR).AnalisesFact(i).cod_rubr_efr, _
                                                 "", LastCodEmpresa
                    
                    End If
                End If
                
                ' ------------------------------------------------------------------------------------
                ' SE RECIBO TIVER DESCONTO E ENTIDADE ESTIVER CODIFICADA PARA ENVIAR DESCONTO,
                ' -------------------------------------------------------------------------------------
                If EF_ReqFact(linhaR).AnalisesFact(i).nRec <> LastRecibo Then
                    LastRecibo = EF_ReqFact(linhaR).AnalisesFact(i).nRec
                    EF_AlteraPercDescontoEnvio linhaR, CLng(i)
                End If
            
        
        
                ssql = "UPDATE sl_recibos_det SET "
                ssql = ssql & " p1 = " & BL_TrataStringParaBD(EF_ReqFact(linhaR).AnalisesFact(i).p1)
                ssql = ssql & ", cor_p1 = " & BL_TrataStringParaBD(EF_ReqFact(linhaR).AnalisesFact(i).cor_p1)
                ssql = ssql & ", flg_facturado =  " & EF_ReqFact(linhaR).AnalisesFact(i).Flg_Facturada
                ssql = ssql & ", flg_retirado = " & IIf(EF_ReqFact(linhaR).AnalisesFact(i).Flg_Retirada = True, 1, 0)
                ssql = ssql & ", ordem_marcacao = " & i
                ssql = ssql & " WHERE n_req = " & BL_TrataStringParaBD(EF_ReqFact(linhaR).NReq) & " AND cod_facturavel = " & BL_TrataStringParaBD(EF_ReqFact(linhaR).AnalisesFact(i).codAna)
                BG_ExecutaQuery_ADO ssql
                
                If gSQLError <> 0 Then
                    BL_LogFile_BD "Envio_FACTUS", "EF_Enviadados", "", "Erro update sl_recibos_det", ssql
                    BG_RollbackTransaction
                    Exit Function
                End If
                
                 'NELSONPSILVA 04.04.2018 Glintt-HS-18011
                If gATIVA_LOGS_RGPD = mediSim Then
                    Dim requestJson As String
                    Dim responseJson As String
                                      
                    requestJson = "{" & Chr(34) & "p1" & Chr(34) & ":" & Chr(34) & BL_TrataStringParaBD(EF_ReqFact(linhaR).AnalisesFact(i).p1) & ", " & Chr(34) & "cor_p1" & Chr(34) & ":" & Chr(34) & BL_TrataStringParaBD(EF_ReqFact(linhaR).AnalisesFact(i).cor_p1) & Chr(34) & _
                    ", " & Chr(34) & "flg_facturado" & Chr(34) & ":" & Chr(34) & EF_ReqFact(linhaR).AnalisesFact(i).Flg_Facturada & Chr(34) & ", " & Chr(34) & "flg_retirado" & Chr(34) & ":" & Chr(34) & IIf(EF_ReqFact(linhaR).AnalisesFact(i).Flg_Retirada = True, 1, 0) & Chr(34) & _
                    ", " & Chr(34) & "ordem_marcacao" & Chr(34) & ":" & Chr(34) & i & " " & Bg_DaHora_ADO & Chr(34) & ", " & Chr(34) & "n_req" & Chr(34) & ":" & Chr(34) & BL_TrataStringParaBD(EF_ReqFact(linhaR).NReq) & Chr(34) & _
                    ", " & Chr(34) & "cod_facturavel" & Chr(34) & ":" & Chr(34) & BL_TrataStringParaBD(EF_ReqFact(linhaR).AnalisesFact(i).codAna) & Chr(34) & "}"
                    

                    BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Alteracao) & " - " & gFormActivo.Name, _
                    Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
                End If
                
            Else
                If FLG_MostraAviso = False Then
                    BG_Mensagem mediMsgBox, "A Requisi��o n�o pode ser enviada porque tem an�lises sem recibo.", vbOKOnly + vbInformation, "Factura��o"
                    FLG_MostraAviso = True
                End If
            End If
        End If
    Next
    
    ' ------------------------------------------
    ' ALTERA O ESTADO NO ECRA
    ' -------------------------------------------
    If gSQLError = 0 Then
        BG_CommitTransaction
    Else
    BG_LogFile_Erros Err.Number & Err.Description & gSQLError, "Envio_FACTUS", "EF_EnviaDados", True
        BG_RollbackTransaction
        Exit Function
    End If
    
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & Err.Description, "Envio_FACTUS", "EF_EnviaDados", True
    BG_RollbackTransaction
    Exit Function
    Resume Next
End Function


' --------------------------------------------------------------------------------------------

' VERIFICA SE CREDENCIAL FOI ENVIADA NOUTRA REQUISICAO

' --------------------------------------------------------------------------------------------
Private Function EF_VerificaCredJaExistente(ByVal credencial As String, linhaR As Long) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsCred As New adodb.recordset
    
    ssql = "SELECT * FROM fa_movi_fact WHERE nr_req_ars = " & BL_TrataStringParaBD(credencial)
    ssql = ssql & " AND episodio <> " & BL_TrataStringParaBD(EF_ReqFact(linhaR).NReq)
    rsCred.CursorLocation = adUseServer
    rsCred.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsCred.Open ssql, gConexaoSecundaria
    If rsCred.RecordCount > 0 Then
        EF_VerificaCredJaExistente = True
    Else
        EF_VerificaCredJaExistente = False
    End If
    rsCred.Close
    Set rsCred = Nothing
Exit Function
TrataErro:
     EF_VerificaCredJaExistente = False
    BG_LogFile_Erros "Erro ao EF_VerificaCredJaExistente : " & " " & Err.Number & " - " & Err.Description, "Envio_Factus", "EF_VerificaCredJaExistente", True
    Exit Function
    Resume Next
End Function


' --------------------------------------------------------------------------------------------

' VERIFICA SE P1 tem mais que 6 analises

' --------------------------------------------------------------------------------------------
Private Function EF_VerificaTotalAnaCred(ByVal credencial As String, linhaR As Long) As Boolean
    Dim i As Integer
    Dim totalAna As Integer
    On Error GoTo TrataErro
    
    EF_VerificaTotalAnaCred = False
    totalAna = 0
    
    For i = 1 To EF_ReqFact(linhaR).TotalAnalisesFact
        If EF_ReqFact(linhaR).AnalisesFact(i).p1 = credencial And EF_ReqFact(linhaR).AnalisesFact(i).Flg_Retirada = False Then
            totalAna = totalAna + 1
            If totalAna > 6 Then
                EF_VerificaTotalAnaCred = False
                Exit Function
            End If
        End If
    Next
    EF_VerificaTotalAnaCred = True
Exit Function
TrataErro:
     EF_VerificaTotalAnaCred = False
    BG_LogFile_Erros "Erro ao EF_VerificaTotalAnaCred : " & " " & Err.Number & " - " & Err.Description, "Envio_Factus", "EF_VerificaTotalAnaCred", True
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------------------------------------------

' VERIFICA se ENTIDADE FOR ADSE, SE TODAS AS ANALISES NO RECIBO ESTAO SER ENVIADAS E SE TODAS QUE ESTAO SER ENVIADAS ESTAO NO RECIBO

' ----------------------------------------------------------------------------------------------------------------------------------
Private Function EF_VerificaRecADSE(ByVal linhaR As Long) As Boolean
    Dim i As Integer
    On Error GoTo TrataErro
    EF_VerificaRecADSE = False
    If EF_ReqFact(linhaR).flg_adse = mediSim Then
        For i = 1 To EF_ReqFact(linhaR).TotalAnalisesFact
            If EF_ReqFact(linhaR).AnalisesFact(i).Flg_Retirada = False And EF_ReqFact(linhaR).AnalisesFact(i).isencao <> gTipoIsencaoIsento Then
                If EF_ReqFact(linhaR).AnalisesFact(i).nRec = "0" Then
                    EF_VerificaRecADSE = False
                    Exit Function
                End If
            End If
        Next
        For i = 1 To EF_ReqFact(linhaR).TotalAnalisesFact
            If EF_ReqFact(linhaR).AnalisesFact(i).nRec <> "0" Then
                If EF_ReqFact(linhaR).AnalisesFact(i).Flg_Retirada = True Then
                    EF_VerificaRecADSE = False
                    Exit Function
                End If
            End If
        Next
        
    End If
    EF_VerificaRecADSE = True
Exit Function
TrataErro:
     EF_VerificaRecADSE = False
    BG_LogFile_Erros "Erro ao EF_VerificaRecADSE Etiquetas: " & " " & Err.Number & " - " & Err.Description, "Envio_Factus", "EF_VerificaRecADSE", True
    Exit Function
    Resume Next
End Function



' -----------------------------------------------------------------------------------------------

' VERIFICA SE UTENTE TEM DIREITO A DESCONTO POR TER MAIS DE 65 ANOS

' -----------------------------------------------------------------------------------------------
Public Function EF_VerificaRegraArs(ByVal linhaR As Long) As Boolean
    Dim idade As String
    On Error GoTo TrataErro
    
    If BL_HandleNull(EF_ReqFact(linhaR).flg_65anos, 0) = 0 Then
        EF_VerificaRegraArs = False
    Else
        idade = BG_CalculaIdade(BL_HandleNull(EF_ReqFact(linhaR).dt_nasc_ute, Bg_DaData_ADO), CDate(EF_ReqFact(linhaR).dt_chega))
        If CLng(Mid(idade, 1, InStr(1, idade, " "))) >= 65 Then
            EF_VerificaRegraArs = True
        Else
            EF_VerificaRegraArs = False
        End If
    End If

Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Envio_fACTUS", "EF_VerificaRegraArs"
    EF_VerificaRegraArs = False
    Exit Function
    Resume Next
End Function


Public Function EF_RetornaTaxaRecibo(linhaR As Long, serie_rec As String, n_rec As String, cod_efr As String, deducao As Boolean) As String
    Dim i As Long
    Dim valorTotal As Double
    EF_RetornaTaxaRecibo = ""
    If n_rec = "" Then
        EF_RetornaTaxaRecibo = "0"
        Exit Function
    End If
    valorTotal = 0
    For i = 1 To EF_ReqFact(linhaR).TotalRecibos
        If deducao = True Then
            If EF_ReqFact(linhaR).EstrutRecibos(i).Estado <> "A" And EF_ReqFact(linhaR).EstrutRecibos(i).Estado <> "NL" Then
                valorTotal = valorTotal + CDbl(EF_ReqFact(linhaR).EstrutRecibos(i).total)
            End If
            EF_RetornaTaxaRecibo = " " & CStr(valorTotal)
        Else
            If cod_efr = EF_ReqFact(linhaR).EstrutRecibos(i).cod_efr Then
                If serie_rec = EF_ReqFact(linhaR).EstrutRecibos(i).serie_rec And n_rec = EF_ReqFact(linhaR).EstrutRecibos(i).n_rec Then
                    
                    EF_RetornaTaxaRecibo = "  " & EF_ReqFact(linhaR).EstrutRecibos(i).total
                    Exit Function
                End If
            End If
        End If
    Next
    
End Function


' ---------------------------------------------------------------------------

' ALTERA PERCENTAGEM DE DESCONTO A ENVIAR PARA FACTUS

' ---------------------------------------------------------------------------
Private Sub EF_AlteraPercDescontoEnvio(ByVal linhaR As Long, ByVal linhaA As Long)
    Dim ssql As String
    On Error GoTo TrataErro
    
    ssql = "UPDATE sl_recibos SET desconto_envio = " & Replace(EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescEnvio, ",", ".") & " WHERE n_req = " & EF_ReqFact(linhaR).NReq
    ssql = ssql & " AND cod_efr = " & EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_efr
    ssql = ssql & " AND serie_rec = " & BL_TrataStringParaBD(EF_ReqFact(linhaR).AnalisesFact(linhaA).serieRec) & " and n_rec = " & EF_ReqFact(linhaR).AnalisesFact(linhaA).nRec
    BG_ExecutaQuery_ADO (ssql)

Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Envio_FACTUS", "EF_AlteraPercDescontoEnvio"
    Exit Sub
    Resume Next
End Sub




Public Function EF_MoveEstrutAnalises(ByVal linhaR As Long, ByVal linhaA As Long) As Long
    On Error GoTo TrataErro
    Dim i As Long
    
    EF_ReqFact(linhaR).TotalAnalisesFact = EF_ReqFact(linhaR).TotalAnalisesFact + 1
    ReDim Preserve EF_ReqFact(linhaR).AnalisesFact(EF_ReqFact(linhaR).TotalAnalisesFact)
    
    i = EF_ReqFact(linhaR).TotalAnalisesFact
    
    While i > linhaA

            EF_ReqFact(linhaR).AnalisesFact(i).NReq = EF_ReqFact(linhaR).AnalisesFact(i - 1).NReq
            EF_ReqFact(linhaR).AnalisesFact(i).codAna = EF_ReqFact(linhaR).AnalisesFact(i - 1).codAna
            EF_ReqFact(linhaR).AnalisesFact(i).CodFacturavel = EF_ReqFact(linhaR).AnalisesFact(i - 1).CodFacturavel
            EF_ReqFact(linhaR).AnalisesFact(i).descrAna = EF_ReqFact(linhaR).AnalisesFact(i - 1).descrAna
            EF_ReqFact(linhaR).AnalisesFact(i).CodAnaGH = EF_ReqFact(linhaR).AnalisesFact(i - 1).CodAnaGH
            EF_ReqFact(linhaR).AnalisesFact(i).descrAnaGH = EF_ReqFact(linhaR).AnalisesFact(i - 1).descrAnaGH
            EF_ReqFact(linhaR).AnalisesFact(i).p1 = EF_ReqFact(linhaR).AnalisesFact(i - 1).p1
            EF_ReqFact(linhaR).AnalisesFact(i).preco = EF_ReqFact(linhaR).AnalisesFact(i - 1).preco
            EF_ReqFact(linhaR).AnalisesFact(i).nRec = EF_ReqFact(linhaR).AnalisesFact(i - 1).nRec
            EF_ReqFact(linhaR).AnalisesFact(i).serieRec = EF_ReqFact(linhaR).AnalisesFact(i - 1).serieRec
            EF_ReqFact(linhaR).AnalisesFact(i).estadoRec = EF_ReqFact(linhaR).AnalisesFact(i - 1).estadoRec
            EF_ReqFact(linhaR).AnalisesFact(i).Flg_Facturada = EF_ReqFact(linhaR).AnalisesFact(i - 1).Flg_Facturada
            EF_ReqFact(linhaR).AnalisesFact(i).Flg_Retirada = EF_ReqFact(linhaR).AnalisesFact(i - 1).Flg_Retirada
            EF_ReqFact(linhaR).AnalisesFact(i).Flg_adicionada = EF_ReqFact(linhaR).AnalisesFact(i - 1).Flg_adicionada
            EF_ReqFact(linhaR).AnalisesFact(i).cod_rubr_efr = EF_ReqFact(linhaR).AnalisesFact(i - 1).cod_rubr_efr
            EF_ReqFact(linhaR).AnalisesFact(i).descr_rubr_efr = EF_ReqFact(linhaR).AnalisesFact(i - 1).descr_rubr_efr
            EF_ReqFact(linhaR).AnalisesFact(i).nr_c = EF_ReqFact(linhaR).AnalisesFact(i - 1).nr_c
            EF_ReqFact(linhaR).AnalisesFact(i).ordem = i
            EF_ReqFact(linhaR).AnalisesFact(i).tratado = EF_ReqFact(linhaR).AnalisesFact(i - 1).tratado
            EF_ReqFact(linhaR).AnalisesFact(i).codAna = EF_ReqFact(linhaR).AnalisesFact(i - 1).codAna
            EF_ReqFact(linhaR).AnalisesFact(i).Qtd_Ana = EF_ReqFact(linhaR).AnalisesFact(i - 1).Qtd_Ana
            EF_ReqFact(linhaR).AnalisesFact(i).qtd_ana_envio = EF_ReqFact(linhaR).AnalisesFact(i - 1).qtd_ana_envio
            EF_ReqFact(linhaR).AnalisesFact(i).nr_benef = EF_ReqFact(linhaR).AnalisesFact(i - 1).nr_benef
            EF_ReqFact(linhaR).AnalisesFact(i).cod_medico = EF_ReqFact(linhaR).AnalisesFact(i - 1).cod_medico
            EF_ReqFact(linhaR).AnalisesFact(i).cor_p1 = EF_ReqFact(linhaR).AnalisesFact(i - 1).cor_p1
            EF_ReqFact(linhaR).AnalisesFact(i).flg_ObrigaRecibo = EF_ReqFact(linhaR).AnalisesFact(i - 1).flg_ObrigaRecibo
            EF_ReqFact(linhaR).AnalisesFact(i).isencao = EF_ReqFact(linhaR).AnalisesFact(i - 1).isencao
            EF_ReqFact(linhaR).AnalisesFact(i).PrecoUnitario = EF_ReqFact(linhaR).AnalisesFact(i - 1).PrecoUnitario
            EF_ReqFact(linhaR).AnalisesFact(i).precoUte = EF_ReqFact(linhaR).AnalisesFact(i - 1).precoUte
            EF_ReqFact(linhaR).AnalisesFact(i).precoUnitarioUte = EF_ReqFact(linhaR).AnalisesFact(i - 1).precoUnitarioUte
            EF_ReqFact(linhaR).AnalisesFact(i).cod_efr = EF_ReqFact(linhaR).AnalisesFact(i - 1).cod_efr
            EF_ReqFact(linhaR).AnalisesFact(i).cod_empresa = EF_ReqFact(linhaR).AnalisesFact(i - 1).cod_empresa
            EF_ReqFact(linhaR).AnalisesFact(i).fds = EF_ReqFact(linhaR).AnalisesFact(i - 1).fds
            EF_ReqFact(linhaR).AnalisesFact(i).Portaria = EF_ReqFact(linhaR).AnalisesFact(i - 1).Portaria
            EF_ReqFact(linhaR).AnalisesFact(i).data = EF_ReqFact(linhaR).AnalisesFact(i - 1).data
            
            EF_ReqFact(linhaR).AnalisesFact(i).flg_percentagem = EF_ReqFact(linhaR).AnalisesFact(i - 1).flg_percentagem
            EF_ReqFact(linhaR).AnalisesFact(i).PercDescEnvio = EF_ReqFact(linhaR).AnalisesFact(i - 1).PercDescEnvio
            EF_ReqFact(linhaR).AnalisesFact(i).PercDescCodif = EF_ReqFact(linhaR).AnalisesFact(i - 1).PercDescCodif
            EF_ReqFact(linhaR).AnalisesFact(i).PercDescUtente = EF_ReqFact(linhaR).AnalisesFact(i - 1).PercDescUtente
            EF_ReqFact(linhaR).AnalisesFact(i).flg_controlaIsencao = EF_ReqFact(linhaR).AnalisesFact(i - 1).flg_controlaIsencao
            EF_ReqFact(linhaR).AnalisesFact(i).flg_novaArs = EF_ReqFact(linhaR).AnalisesFact(i - 1).flg_novaArs
            'rcoelho 3.06.2013 lrs-156
            EF_ReqFact(linhaR).AnalisesFact(i).cod_efr_envio = EF_ReqFact(linhaR).AnalisesFact(i - 1).cod_efr_envio
        i = i - 1
    Wend
    EF_MoveEstrutAnalises = linhaA
Exit Function
TrataErro:
    EF_MoveEstrutAnalises = mediComboValorNull
    BG_LogFile_Erros Err.Number & " - " & Err.Description, "Envio_FACTUS", "EF_MoveEstrutAnalises"
    Exit Function
    Resume Next
End Function

Public Sub EF_TrocaLinhas(ByVal linhaR As Long, ByVal linhaA1 As Long, ByVal linhaA2 As Long)
    Dim NReq As String
    Dim codAna As String
    Dim CodFacturavel As String
    Dim descrAna As String
    Dim CodAnaGH As String
    Dim descrAnaGH As String
    Dim ordem As Integer
    Dim p1 As String
    Dim preco As Double
    Dim nRec As String
    Dim serieRec As String
    Dim Flg_Facturada As Integer
    Dim Flg_Retirada As Boolean
    Dim Flg_adicionada As Boolean
    Dim cod_rubr_efr As String
    Dim descr_rubr_efr As String
    Dim tratado As Boolean
    Dim ssql As String
    Dim nr_c As String
    'Dim codAna As String
    Dim qtd As String
    Dim qtd_envio As Integer
    Dim cod_efr As String
    Dim n_benef As String
    Dim cod_empresa As String
    Dim Portaria As String
    Dim data As String
    Dim estadoRec As String
    Dim cod_efr_envio As String
    
    NReq = EF_ReqFact(linhaR).AnalisesFact(linhaA1).NReq
    codAna = EF_ReqFact(linhaR).AnalisesFact(linhaA1).codAna
    CodFacturavel = EF_ReqFact(linhaR).AnalisesFact(linhaA1).CodFacturavel
    descrAna = EF_ReqFact(linhaR).AnalisesFact(linhaA1).descrAna
    CodAnaGH = EF_ReqFact(linhaR).AnalisesFact(linhaA1).CodAnaGH
    descrAnaGH = EF_ReqFact(linhaR).AnalisesFact(linhaA1).descrAnaGH
    p1 = EF_ReqFact(linhaR).AnalisesFact(linhaA1).p1
    preco = EF_ReqFact(linhaR).AnalisesFact(linhaA1).preco
    nRec = EF_ReqFact(linhaR).AnalisesFact(linhaA1).nRec
    serieRec = EF_ReqFact(linhaR).AnalisesFact(linhaA1).serieRec
    estadoRec = EF_ReqFact(linhaR).AnalisesFact(linhaA1).estadoRec
    Flg_Facturada = EF_ReqFact(linhaR).AnalisesFact(linhaA1).Flg_Facturada
    Flg_Retirada = EF_ReqFact(linhaR).AnalisesFact(linhaA1).Flg_Retirada
    Flg_adicionada = EF_ReqFact(linhaR).AnalisesFact(linhaA1).Flg_adicionada
    ordem = EF_ReqFact(linhaR).AnalisesFact(linhaA1).ordem
    cod_rubr_efr = EF_ReqFact(linhaR).AnalisesFact(linhaA1).cod_rubr_efr
    descr_rubr_efr = EF_ReqFact(linhaR).AnalisesFact(linhaA1).descr_rubr_efr
    nr_c = EF_ReqFact(linhaR).AnalisesFact(linhaA1).nr_c
    tratado = EF_ReqFact(linhaR).AnalisesFact(linhaA1).tratado
    codAna = EF_ReqFact(linhaR).AnalisesFact(linhaA1).codAna
    qtd = EF_ReqFact(linhaR).AnalisesFact(linhaA1).Qtd_Ana
    qtd_envio = EF_ReqFact(linhaR).AnalisesFact(linhaA1).qtd_ana_envio
    cod_efr = EF_ReqFact(linhaR).AnalisesFact(linhaA1).cod_efr
    n_benef = EF_ReqFact(linhaR).AnalisesFact(linhaA1).nr_benef
    cod_empresa = EF_ReqFact(linhaR).AnalisesFact(linhaA1).cod_empresa
    Portaria = EF_ReqFact(linhaR).AnalisesFact(linhaA1).Portaria
    data = EF_ReqFact(linhaR).AnalisesFact(linhaA1).data
    'rcoelho 3.06.2013 lrs-156
    cod_efr_envio = EF_ReqFact(linhaR).AnalisesFact(linhaA1).cod_efr_envio
    
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).NReq = EF_ReqFact(linhaR).AnalisesFact(linhaA2).NReq
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).codAna = EF_ReqFact(linhaR).AnalisesFact(linhaA2).codAna
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).CodFacturavel = EF_ReqFact(linhaR).AnalisesFact(linhaA2).CodFacturavel
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).descrAna = EF_ReqFact(linhaR).AnalisesFact(linhaA2).descrAna
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).CodAnaGH = EF_ReqFact(linhaR).AnalisesFact(linhaA2).CodAnaGH
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).descrAnaGH = EF_ReqFact(linhaR).AnalisesFact(linhaA2).descrAnaGH
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).p1 = EF_ReqFact(linhaR).AnalisesFact(linhaA2).p1
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).preco = EF_ReqFact(linhaR).AnalisesFact(linhaA2).preco
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).nRec = EF_ReqFact(linhaR).AnalisesFact(linhaA2).nRec
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).serieRec = EF_ReqFact(linhaR).AnalisesFact(linhaA2).serieRec
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).estadoRec = EF_ReqFact(linhaR).AnalisesFact(linhaA2).estadoRec
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).Flg_Facturada = EF_ReqFact(linhaR).AnalisesFact(linhaA2).Flg_Facturada
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).Flg_Retirada = EF_ReqFact(linhaR).AnalisesFact(linhaA2).Flg_Retirada
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).Flg_adicionada = EF_ReqFact(linhaR).AnalisesFact(linhaA2).Flg_adicionada
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).cod_rubr_efr = EF_ReqFact(linhaR).AnalisesFact(linhaA2).cod_rubr_efr
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).descr_rubr_efr = EF_ReqFact(linhaR).AnalisesFact(linhaA2).descr_rubr_efr
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).nr_c = EF_ReqFact(linhaR).AnalisesFact(linhaA2).nr_c
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).ordem = linhaA1
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).tratado = EF_ReqFact(linhaR).AnalisesFact(linhaA2).tratado
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).codAna = EF_ReqFact(linhaR).AnalisesFact(linhaA2).codAna
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).Qtd_Ana = EF_ReqFact(linhaR).AnalisesFact(linhaA2).Qtd_Ana
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).qtd_ana_envio = EF_ReqFact(linhaR).AnalisesFact(linhaA2).qtd_ana_envio
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).cod_efr = EF_ReqFact(linhaR).AnalisesFact(linhaA2).cod_efr
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).nr_benef = EF_ReqFact(linhaR).AnalisesFact(linhaA2).nr_benef
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).cod_empresa = EF_ReqFact(linhaR).AnalisesFact(linhaA2).cod_empresa
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).Portaria = EF_ReqFact(linhaR).AnalisesFact(linhaA2).Portaria
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).data = EF_ReqFact(linhaR).AnalisesFact(linhaA2).data
    'rcoelho 3.06.2013 lrs-156
    EF_ReqFact(linhaR).AnalisesFact(linhaA1).cod_efr_envio = EF_ReqFact(linhaR).AnalisesFact(linhaA2).cod_efr_envio
    
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).NReq = NReq
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).codAna = codAna
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).CodFacturavel = CodFacturavel
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).descrAna = descrAna
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).CodAnaGH = CodAnaGH
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).descrAnaGH = descrAnaGH
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).p1 = p1
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).preco = preco
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).nRec = nRec
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).serieRec = serieRec
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).estadoRec = estadoRec
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).Flg_Facturada = Flg_Facturada
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).Flg_Retirada = Flg_Retirada
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).Flg_adicionada = Flg_adicionada
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).ordem = linhaA2
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).cod_rubr_efr = cod_rubr_efr
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).descr_rubr_efr = descr_rubr_efr
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).nr_c = nr_c
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).tratado = tratado
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).Qtd_Ana = qtd
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).qtd_ana_envio = qtd_envio
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).codAna = codAna
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).cod_efr = cod_efr
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).nr_benef = n_benef
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).cod_empresa = cod_empresa
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).Portaria = Portaria
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).data = data
    'rcoelho 3.06.2013 lrs-156
    EF_ReqFact(linhaR).AnalisesFact(linhaA2).cod_efr_envio = cod_efr_envio

End Sub

Public Sub EF_PreencheTaxaUtente(ByVal linhaR As Long, ByVal linhaA As Long)
    Dim taxa As String
    Dim mens As String
    Dim i As Integer
    'PREENCHE TAXA UTENTE
    If EF_ReqFact(linhaR).AnalisesFact(linhaA).isencao = gTipoIsencaoIsento And EF_ReqFact(linhaR).hemodialise = mediComboValorNull Then
        EF_ReqFact(linhaR).AnalisesFact(linhaA).precoUnitarioUte = 0
        EF_ReqFact(linhaR).AnalisesFact(linhaA).precoUte = 0
    Else
        
        taxa = IF_RetornaTaxaAnalise(EF_ReqFact(linhaR).AnalisesFact(linhaA).codAna, EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_efr_envio, _
                EF_ReqFact(linhaR).TipoUtente, EF_ReqFact(linhaR).Utente, EF_ReqFact(linhaR).dt_chega, Empty, Empty, False)
        If CDbl(Replace(taxa, ".", ",")) <= 0 Then
            taxa = 0
        End If
        EF_ReqFact(linhaR).AnalisesFact(linhaA).precoUnitarioUte = Replace(taxa, ".", ",")
        EF_ReqFact(linhaR).AnalisesFact(linhaA).precoUte = EF_ReqFact(linhaR).AnalisesFact(linhaA).precoUnitarioUte * EF_ReqFact(linhaR).AnalisesFact(linhaA).Qtd_Ana
        
        'SE DOENTE DE HEMODIALISE ENTAO ACRESCENTA TAXA AO PRECO DA ANALISE HEMODIALISE
        If EF_ReqFact(linhaR).hemodialise > mediComboValorNull And EF_ReqFact(linhaR).cod_ana_hemodialise <> "" Then
            For i = 1 To linhaA
                If EF_ReqFact(linhaR).AnalisesFact(i).CodFacturavel = EF_ReqFact(linhaR).cod_ana_hemodialise Then
                    EF_ReqFact(linhaR).AnalisesFact(i).preco = EF_ReqFact(linhaR).AnalisesFact(i).preco + EF_ReqFact(linhaR).AnalisesFact(linhaA).precoUte
                    EF_ReqFact(linhaR).AnalisesFact(i).PrecoUnitario = EF_ReqFact(linhaR).AnalisesFact(i).preco
                End If
            Next
        End If
    End If

End Sub

Public Sub EF_PreencheTaxaEFR(ByVal linhaR As Long, ByVal linhaA As Long)
    'rcoelho 5.06.2013 lhss-124
    'Dim taxa As Double
    Dim taxa As String
    Dim mens As String
    If EF_ReqFact(linhaR).AnalisesFact(linhaA).codAna <> "" Then
        taxa = IF_RetornaTaxaAnaliseEntidade(EF_ReqFact(linhaR).AnalisesFact(linhaA).codAna, EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_efr_envio, BL_HandleNull(EF_ReqFact(linhaR).dt_chega, ""), _
                            EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_rubr_efr, EF_ReqFact(linhaR).TipoUtente, EF_ReqFact(linhaR).Utente, EF_ReqFact(linhaR).AnalisesFact(linhaA).PrecoUnitario, EF_ReqFact(linhaR).AnalisesFact(linhaA).qtd_ana_envio, _
                            EF_ReqFact(linhaR).AnalisesFact(linhaA).nr_c, EF_ReqFact(linhaR).AnalisesFact(linhaA).flg_controlaIsencao, EF_ReqFact(linhaR).AnalisesFact(linhaA).descr_rubr_efr, EF_ReqFact(linhaR).hemodialise)
        'rcoelho 5.06.2013 lhss-124
        'Select Case taxa
        Select Case CDbl(taxa)
            Case -1
                mens = " An�lise n�o est� mapeada para o FACTUS!"
            Case -2
                mens = "Impossivel abrir conex�o com o FACTUS!"
            Case -3
                mens = "Impossivel seleccionar portaria activa para a entidade em causa!"
            Case -4
                mens = "N�o existe taxa codificada para a an�lise em causa!"
            Case Else
                mens = ""
        End Select
        If mens <> "" Then
            If gTipoInstituicao = "PRIVADA" Then
                BG_Mensagem mediMsgBox, mens, vbOKOnly + vbInformation, "Taxas"
            End If
            EF_ReqFact(linhaR).AnalisesFact(linhaA).preco = 0
        Else
            'rcoelho 5.06.2013 lhss-124
'            taxa = Replace(taxa, ".", ",")
'            If Mid(taxa, 1, 1) = "," Then
'                taxa = "0" & taxa
'            End If
'            EF_ReqFact(linhaR).AnalisesFact(linhaA).preco = taxa
            EF_ReqFact(linhaR).AnalisesFact(linhaA).preco = Replace(taxa, ".", ",")
        End If
    Else
        EF_ReqFact(linhaR).AnalisesFact(linhaA).preco = 0
    End If

End Sub


' ----------------------------------------------------------------------------------------------------------------------------------

' VERIFICA ENTIDADE FOR ADSE, SE TODAS AS ANALISES T�M MEDICO DEFINIDO

' ----------------------------------------------------------------------------------------------------------------------------------
Private Function EF_VerificaMedicoPreenchidoADSE(ByVal linhaR As Long) As Boolean
    Dim i As Integer
    On Error GoTo TrataErro
    EF_VerificaMedicoPreenchidoADSE = False
    
    If EF_ReqFact(linhaR).flg_adse = mediSim Then
        For i = 1 To EF_ReqFact(linhaR).TotalAnalisesFact
            'NELSONPSILVA LRV-393 26.07.2017'
            If EF_ReqFact(linhaR).AnalisesFact(i).Flg_Retirada = False Then
                If EF_ReqFact(linhaR).AnalisesFact(i).cod_medico = "" Then
                    EF_VerificaMedicoPreenchidoADSE = False
                    Exit Function
                End If
            End If
        Next i
    End If
    EF_VerificaMedicoPreenchidoADSE = True
Exit Function
TrataErro:
     EF_VerificaMedicoPreenchidoADSE = False
    BG_LogFile_Erros "Erro ao EF_VerificaMedicoPreenchidoADSE Etiquetas: " & " " & Err.Number & " - " & Err.Description, "Envio_Factus", "EF_VerificaMedicoPreenchidoADSE", True
    Exit Function
    Resume Next
End Function

Attribute VB_Name = "PesquisaAvancada"
Option Explicit

Public Sub PA_PesquisaSala(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_sala"
    CamposEcran(1) = "descr_sala"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_sala"
    CamposEcran(2) = "cod_sala"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_cod_salas"
    CampoPesquisa = "descr_sala"
    CWhere = " (flg_invisivel IS NULL or flg_invisivel = 0) AND cod_sala IN (SELECT x.cod_sala FROM sl_ass_salas_locais x WHERE x.cod_local = " & BL_HandleNull(codLocal, CStr(gCodLocal)) & ") "

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_sala ", " Salas/Postos de colheita")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Salas/postos", vbExclamation, "Salas/Postos"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaSala: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaSala"
    Exit Sub
    Resume Next
End Sub

Public Sub PA_PesquisaGen(cod_bd As String, descr_bd As String, Tabela As String, campoCodigo As TextBox, _
                         campoDescr As TextBox, descricao As String, CWhere As String)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = descr_bd
    CamposEcran(1) = descr_bd
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = cod_bd
    CamposEcran(2) = cod_bd
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = Tabela
    CampoPesquisa = descr_bd

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY " & descr_bd, descricao)

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem " & descricao, vbExclamation, descricao
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaGen: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaGen"
    Exit Sub
    Resume Next
End Sub

Public Sub PA_PesquisaTipoAmostra(campoCodigo As TextBox, campoDescr As TextBox)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_tipo_amostra"
    CamposEcran(1) = "descr_tipo_amostra"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_tipo_amostra"
    CamposEcran(2) = "cod_tipo_amostra"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_cod_tipo_amostra"
    CampoPesquisa = "descr_tipo_amostra"
    CWhere = " (flg_invisivel IS NULL or flg_invisivel = 0)  "

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_tipo_amostra ", " Tipo de Amostra")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem tipos de amostra", vbExclamation, "Tipos de Amostra"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaTipoAmostra: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaTipoAmostra"
    Exit Sub
    Resume Next
End Sub

Public Sub PA_PesquisaOrigemAmostra(campoCodigo As TextBox, campoDescr As TextBox)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_origem_amostra"
    CamposEcran(1) = "descr_origem_amostra"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_origem_amostra"
    CamposEcran(2) = "cod_origem_amostra"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_cod_origem_amostra"
    CampoPesquisa = "descr_origem_amostra"
    CWhere = " (flg_invisivel IS NULL or flg_invisivel = 0) "

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_origem_amostra ", " Origem da Amostra")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem origens da amostra", vbExclamation, "Origem da Amostra"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaOrigemAmostra: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaOrigemAmostra"
    Exit Sub
    Resume Next
End Sub

Public Sub PA_PesquisaPontosColheita(campoCodigo As TextBox, campoDescr As TextBox)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_ponto_colheita"
    CamposEcran(1) = "descr_ponto_colheita"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_ponto_colheita"
    CamposEcran(2) = "cod_ponto_colheita"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_cod_ponto_colheita"
    CampoPesquisa = "descr_ponto_colheita"
    CWhere = " (flg_invisivel IS NULL or flg_invisivel = 0) "

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_ponto_colheita ", " Ponto de Colheita")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem pontos de colheita", vbExclamation, "Pontos de Colheita"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaPontosColheita: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaPontosColheita"
    Exit Sub
    Resume Next
End Sub

Public Sub PA_PesquisaSalaMultiSel(lista As ListBox)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_sala"
    CamposEcran(1) = "cod_sala"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_sala"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_salas"
    CWhere = ""
    CampoPesquisa = "descr_sala"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_sala ", _
                                                                           " Salas")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If lista.ListCount = 0 Then
                lista.AddItem BL_SelCodigo("sl_cod_salas", "descr_sala", "seq_sala", resultados(i))
                lista.ItemData(0) = resultados(i)
            Else
                lista.AddItem BL_SelCodigo("sl_cod_salas", "descr_sala", "seq_sala", resultados(i))
                lista.ItemData(lista.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Public Function PA_ValidateLocalEntrega(campoCodigo As TextBox, campoDescr As TextBox) As Boolean

    On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    PA_ValidateLocalEntrega = False
    If campoCodigo.Text <> "" Then
        If Not IsNumeric(campoCodigo.Text) Then
            BG_Mensagem mediMsgBox, "Valor tem que ser num�rico", vbInformation, ""
            campoCodigo.Text = ""
            campoDescr.Text = ""
            Exit Function
        End If
        Set Tabela = New ADODB.recordset

        sql = "SELECT descr_local_entrega FROM sl_cod_local_entrega WHERE cod_local_entrega=" & Trim(campoCodigo.Text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount <= 0 Then
            PA_ValidateLocalEntrega = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela!descr_local_entrega
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateLocalEntrega: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateLocalEntrega"
    Exit Function
    Resume Next
End Function



Public Function PA_ValidateSala(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String) As Boolean
    On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    PA_ValidateSala = False
    If campoCodigo.Text <> "" Then
        Set Tabela = New ADODB.recordset

        sql = "SELECT descr_sala FROM sl_cod_salas WHERE cod_sala='" & Trim(campoCodigo.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount <= 0 Then
            PA_ValidateSala = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela!descr_sala
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateSala: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateSala"
    Exit Function
    Resume Next
End Function

Public Function PA_ValidateGen(cod_bd As String, descr_bd As String, tabe As String, campoCodigo As TextBox, _
                         campoDescr As TextBox, descricao As String) As Boolean
    On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    PA_ValidateGen = False
    If campoCodigo.Text <> "" Then
        Set Tabela = New ADODB.recordset

        sql = "SELECT " & descr_bd & " FROM " & tabe & " WHERE " & cod_bd & "='" & UCase(Trim(campoCodigo.Text)) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount <= 0 Then
            PA_ValidateGen = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela(0)
            campoCodigo.Text = UCase(Trim(campoCodigo.Text))
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateSala: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateSala"
    Exit Function
    Resume Next
End Function

Public Function PA_ValidateLocal(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String) As Boolean
    On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    PA_ValidateLocal = False
    If campoCodigo.Text <> "" Then
        Set Tabela = New ADODB.recordset

        sql = "SELECT nome_empr FROM gr_empr_inst WHERE empresa_id ='" & Trim(campoCodigo.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount <= 0 Then
            PA_ValidateLocal = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela!nome_empr
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateLocal: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateLocal"
    Exit Function
    Resume Next
End Function


Public Function PA_ValidateProven(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String) As Boolean
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If campoCodigo.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_proven FROM sl_proven WHERE cod_proven='" & Trim(campoCodigo.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela!descr_proven
        
        End If
     
        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
    End If
    
End Function


Public Function PA_ValidateTipoAmostra(campoCodigo As TextBox, campoDescr As TextBox) As Boolean
    On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    PA_ValidateTipoAmostra = False
    If campoCodigo.Text <> "" Then
        Set Tabela = New ADODB.recordset

        sql = "SELECT descr_tipo_amostra FROM sl_cod_tipo_amostra WHERE cod_tipo_amostra='" & Trim(campoCodigo.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount <= 0 Then
            PA_ValidateTipoAmostra = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela!descr_tipo_amostra
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateTipoAmostra: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateTipoAmostra"
    Exit Function
    Resume Next
End Function
Public Function PA_ValidatePontoColheita(campoCodigo As TextBox, campoDescr As TextBox) As Boolean
    On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    PA_ValidatePontoColheita = False
    If campoCodigo.Text <> "" Then
        Set Tabela = New ADODB.recordset

        sql = "SELECT descr_ponto_colheita FROM sl_cod_ponto_colheita WHERE cod_ponto_colheita='" & Trim(campoCodigo.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount <= 0 Then
            PA_ValidatePontoColheita = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela!descr_ponto_colheita
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidatePontoColheita: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidatePontoColheita"
    Exit Function
    Resume Next
End Function

Public Function PA_ValidateOrigemAmostra(campoCodigo As TextBox, campoDescr As TextBox) As Boolean
    On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    PA_ValidateOrigemAmostra = False
    If campoCodigo.Text <> "" Then
        Set Tabela = New ADODB.recordset

        sql = "SELECT descr_origem_amostra FROM sl_cod_origem_amostra WHERE cod_origem_amostra='" & Trim(campoCodigo.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount <= 0 Then
            PA_ValidateOrigemAmostra = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela!descr_origem_amostra
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateOrigemAmostra: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateOrigemAmostra"
    Exit Function
    Resume Next
End Function

Public Sub PA_ApagaItemListaMultiSel(lista As ListBox, KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And lista.ListCount > 0 Then     'Delete
        If lista.ListIndex > mediComboValorNull Then
            lista.RemoveItem (lista.ListIndex)
        End If
    End If
End Sub
Public Sub PA_PesquisaEFR(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 3) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 3)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_efr"
    CamposEcran(1) = "descr_efr"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_efr"
    CamposEcran(2) = "cod_efr"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"

    ChavesPesq(3) = "flg_nova_ars"
    CamposEcran(3) = "flg_nova_ars"
    Tamanhos(3) = 0
    Headers(3) = ""

    CamposRetorno.InicializaResultados 2

    CFrom = "sl_efr"
    CampoPesquisa = "descr_efr"
    CWhere = " (flg_invisivel = 0 or flg_invisivel is null) AND cod_efr IN (SELECT x.cod_efr FROM sl_ass_efr_locais x "
    If codLocal <> "" Then
        CWhere = CWhere & " WHERE x.cod_local = " & BL_HandleNull(codLocal, "")
    End If
    CWhere = CWhere & ") "
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_EFR ", " Entidades Finaceiras")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem entidades financeiras", vbExclamation, "EFR"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaEFR: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaEFR"
    Exit Sub
    Resume Next
End Sub

Public Sub PA_PesquisaAnaFact(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_ana"
    CamposEcran(1) = "descr_ana"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_ana"
    CamposEcran(2) = "cod_ana"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "slv_analises_factus"
    CampoPesquisa = "descr_ana"
    CWhere = " "
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_ana ", " An�lises para Factura��o")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises", vbExclamation, "An�lises para Factura��o"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaAnaFact: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaAnaFact"
    Exit Sub
    Resume Next
End Sub

Public Sub PA_PesquisaRubr(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_rubr"
    CamposEcran(1) = "descr_rubr"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_rubr"
    CamposEcran(2) = "cod_rubr"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "fa_rubr"
    CampoPesquisa = "descr_rubr"
    CWhere = " "
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_rubr ", " R�bricas")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem r�bricas", vbExclamation, "R�bricas"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaRubr: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaRubr"
    Exit Sub
    Resume Next
End Sub
Public Sub PA_PesquisaPortaria(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_portaria"
    CamposEcran(1) = "descr_portaria"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_portaria"
    CamposEcran(2) = "cod_portaria"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_portarias "
    CampoPesquisa = "descr_portaria"
    CWhere = " "
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_portaria ", " Portarias")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem portarias", vbExclamation, "Portarias"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaPortaria: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaPortaria"
    Exit Sub
    Resume Next
End Sub




Public Sub PA_PesquisaGruposEpid(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_gr_epid"
    CamposEcran(1) = "descr_gr_epid"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_gr_epid"
    CamposEcran(2) = "cod_gr_epid"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_cod_gr_epid"
    CampoPesquisa = "descr_gr_epid"
    CWhere = " (flg_invisivel = 0 or flg_invisivel is null) "
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_gr_epid ", " Grupos Epidemiol�gicos")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Grupos Epidemiol�gicos", vbExclamation, "Grupos Epidemiol�gicos"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaGruposEpid: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaGruposEpid"
    Exit Sub
    Resume Next
End Sub

Public Sub PA_PesquisaSubClasseAntibio(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_subclasse_antibio"
    CamposEcran(1) = "descr_subclasse_antibio"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_subclasse_antibio"
    CamposEcran(2) = "cod_subclasse_antibio"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_subclasse_antibio"
    CampoPesquisa = "descr_subclasse_antibio"
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_subclasse_antibio ", " SubClasses Antibi�ticos")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem SubClasses de Antibi�ticos", vbExclamation, "SubClasses Antibi�ticos"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaSubClasseAntibio: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaSubClasseAntibio"
    Exit Sub
    Resume Next
End Sub

Public Sub PA_PesquisaAntibio(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_antibio"
    CamposEcran(1) = "descr_antibio"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_antibio"
    CamposEcran(2) = "cod_antibio"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_antibio"
    CampoPesquisa = "descr_antibio"
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_antibio ", " Antibi�ticos")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Antibi�ticos", vbExclamation, "Antibi�ticos"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaAntibio: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaAntibio"
    Exit Sub
    Resume Next
End Sub




Public Sub PA_PesquisaGruposMicro(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_gr_microrg"
    CamposEcran(1) = "descr_gr_microrg"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_gr_microrg"
    CamposEcran(2) = "cod_gr_microrg"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_gr_microrg"
    CampoPesquisa = "descr_gr_microrg"
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_gr_microrg ", " Grupos Microrganismo")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Grupos Microrganismo", vbExclamation, "Grupos Microrganismo"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaGruposMicro: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaGruposMicro"
    Exit Sub
    Resume Next
End Sub




Public Sub PA_PesquisaProven(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_proven"
    CamposEcran(1) = "descr_proven"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_proven"
    CamposEcran(2) = "cod_proven"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_proven"
    CampoPesquisa = "descr_proven"
    CWhere = " (flg_invisivel = 0 or flg_invisivel is null) AND cod_proven IN (SELECT x.cod_proven FROM sl_ass_proven_locais x "
    If codLocal <> "" Then
        CWhere = CWhere & " WHERE x.cod_local = " & BL_HandleNull(codLocal, "")
    End If
    CWhere = CWhere & ") "
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_proven ", " Proveni�ncias")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem  Proveni�ncias", vbExclamation, "EFR"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaProven: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaProven"
    Exit Sub
    Resume Next
End Sub



Public Function PA_ValidateEFR(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String) As Boolean
     On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim ValidaEFR As Boolean
    Dim AntDescrEFR As String


    'se o TEXT do campo � num�rico
    If campoCodigo.Text <> "" Then
        If IsNumeric(campoCodigo) Then
            AntDescrEFR = campoDescr.Text
            Set Tabela = New ADODB.recordset
    
            sql = "SELECT t_efr,descr_efr, flg_nova_ars FROM sl_efr WHERE cod_efr=" & Trim(campoCodigo.Text)
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            Tabela.Open sql, gConexao
    
            If Tabela.RecordCount = 0 Then
                PA_ValidateEFR = True
                BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
                DoEvents
                campoCodigo.Text = ""
                campoDescr.Text = ""
            Else
                campoDescr.Text = Tabela!descr_efr
            End If
            Tabela.Close
            Set Tabela = Nothing
        Else
            campoDescr.Text = ""
        End If
    Else
        campoDescr.Text = ""
        campoCodigo.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateEFR: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateEFR"
    Exit Function
    Resume Next
End Function

Public Function PA_ValidateAnaFact(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String) As Boolean
     On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim ValidaEFR As Boolean
    Dim AntDescrEFR As String


    'se o TEXT do campo � num�rico
    If campoCodigo.Text <> "" Then
        AntDescrEFR = campoDescr.Text
        Set Tabela = New ADODB.recordset

        sql = "SELECT descr_ana FROM slv_analises_factus WHERE cod_ana=" & BL_TrataStringParaBD(Trim(campoCodigo.Text))
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount = 0 Then
            PA_ValidateAnaFact = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            DoEvents
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela!descr_ana
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
        campoCodigo.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateAnaFact: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateAnaFact"
    Exit Function
    Resume Next
End Function


Public Function PA_ValidatePortaria(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String) As Boolean
     On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim ValidaEFR As Boolean
    Dim AntDescrEFR As String


    'se o TEXT do campo � num�rico
    If campoCodigo.Text <> "" Then
        AntDescrEFR = campoDescr.Text
        Set Tabela = New ADODB.recordset

        sql = "SELECT descr_portaria FROM sl_portarias  WHERE cod_portaria=" & Trim(campoCodigo.Text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount = 0 Then
            PA_ValidatePortaria = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            DoEvents
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela!descr_Portaria
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
        campoCodigo.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidatePortaria: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidatePortaria"
    Exit Function
    Resume Next
End Function



Public Function PA_ValidateRubr(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String) As Boolean
     On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim ValidaEFR As Boolean
    Dim AntDescrEFR As String


    'se o TEXT do campo � num�rico
    If campoCodigo.Text <> "" Then
        If IsNumeric(campoCodigo) Then
            AntDescrEFR = campoDescr.Text
            Set Tabela = New ADODB.recordset
    
            sql = "SELECT descr_rubr FROM fa_rubr WHERE cod_rubr=" & BL_TrataStringParaBD(Trim(campoCodigo.Text))
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            Tabela.Open sql, gConexao
    
            If Tabela.RecordCount = 0 Then
                PA_ValidateRubr = True
                BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
                DoEvents
                campoCodigo.Text = ""
                campoDescr.Text = ""
            Else
                campoDescr.Text = Tabela!descr_rubr
            End If
            Tabela.Close
            Set Tabela = Nothing
        Else
            campoDescr.Text = ""
        End If
    Else
        campoDescr.Text = ""
        campoCodigo.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateRubr: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateRubr"
    Exit Function
    Resume Next
End Function


Public Function PA_ValidateGrupoEpid(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String) As Boolean
     On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim ValidaEFR As Boolean
    Dim AntDescrEFR As String


    'se o TEXT do campo � num�rico
    If campoCodigo.Text <> "" Then
        If IsNumeric(campoCodigo) Then
            AntDescrEFR = campoDescr.Text
            Set Tabela = New ADODB.recordset
    
            sql = "SELECT cod_gr_epid, descr_gr_epid FROM sl_cod_gr_epid WHERE cod_gr_epid=" & Trim(campoCodigo.Text)
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            Tabela.Open sql, gConexao
    
            If Tabela.RecordCount = 0 Then
                PA_ValidateGrupoEpid = True
                BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
                DoEvents
                campoCodigo.Text = ""
                campoDescr.Text = ""
            Else
                campoDescr.Text = Tabela!descr_gr_epid
            End If
            Tabela.Close
            Set Tabela = Nothing
        Else
            campoDescr.Text = ""
        End If
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateGrupoEpid: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateGrupoEpid"
    Exit Function
    Resume Next
End Function

Public Function PA_ValidateGrupoMicro(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String) As Boolean
     On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim ValidaEFR As Boolean
    Dim AntDescrEFR As String


    'se o TEXT do campo � num�rico
    If campoCodigo.Text <> "" Then
        If IsNumeric(campoCodigo) Then
            AntDescrEFR = campoDescr.Text
            Set Tabela = New ADODB.recordset
    
            sql = "SELECT cod_gr_microrg, descr_gr_microrg FROM sl_cod_gr_microrg WHERE cod_gr_microrg=" & Trim(campoCodigo.Text)
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            Tabela.Open sql, gConexao
    
            If Tabela.RecordCount = 0 Then
                PA_ValidateGrupoMicro = True
                BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
                DoEvents
                campoCodigo.Text = ""
                campoDescr.Text = ""
            Else
                campoDescr.Text = Tabela!descr_gr_microrg
            End If
            Tabela.Close
            Set Tabela = Nothing
        Else
            campoDescr.Text = ""
        End If
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateGrupoMicro: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateGrupoMicro"
    Exit Function
    Resume Next
End Function

Public Function PA_ValidateSubClasseAntibio(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String) As Boolean
     On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim ValidaEFR As Boolean
    Dim AntDescrEFR As String


    'se o TEXT do campo � num�rico
    If campoCodigo.Text <> "" Then
        If IsNumeric(campoCodigo) Then
            AntDescrEFR = campoDescr.Text
            Set Tabela = New ADODB.recordset
    
            sql = "SELECT cod_subclasse_antibio, descr_subclasse_antibio FROM sl_subclasse_antibio WHERE cod_subclasse_antibio=" & Trim(campoCodigo.Text)
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            Tabela.Open sql, gConexao
    
            If Tabela.RecordCount = 0 Then
                PA_ValidateSubClasseAntibio = True
                BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
                DoEvents
                campoCodigo.Text = ""
                campoDescr.Text = ""
            Else
                campoDescr.Text = Tabela!descr_subclasse_antibio
            End If
            Tabela.Close
            Set Tabela = Nothing
        Else
            campoDescr.Text = ""
        End If
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateSubClasseAntibio: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateSubClasseAntibio"
    Exit Function
    Resume Next
End Function

Public Function PA_ValidateAntibio(campoCodigo As TextBox, campoDescr As TextBox, codLocal As String) As Boolean
     On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    Dim ValidaEFR As Boolean
    Dim AntDescrEFR As String


    'se o TEXT do campo � num�rico
    If campoCodigo.Text <> "" Then
        AntDescrEFR = campoDescr.Text
        Set Tabela = New ADODB.recordset

        sql = "SELECT cod_antibio, descr_antibio FROM sl_antibio WHERE cod_antibio=" & BL_TrataStringParaBD(Trim(campoCodigo.Text))
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount = 0 Then
            PA_ValidateAntibio = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            DoEvents
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela!descr_antibio
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateAntibio: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateAntibio"
    Exit Function
    Resume Next
End Function





Public Sub PA_PesquisaEFRMultiSel(lista As ListBox)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_efr"
    CamposEcran(1) = "cod_efr"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_efr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_efr"
    CWhere = ""
    CampoPesquisa = "descr_efr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_efr ", _
                                                                           " Entidades")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If resultados(i) <> "" Then
                If lista.ListCount = 0 Then
                    lista.AddItem BL_SelCodigo("sl_Efr", "descr_efr", "seq_efr", resultados(i))
                    lista.ItemData(0) = resultados(i)
                Else
                    lista.AddItem BL_SelCodigo("sl_Efr", "descr_efr", "seq_efr", resultados(i))
                    lista.ItemData(lista.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If

End Sub

Public Sub PA_PesquisaLocalEntregaMultiSel(lista As ListBox)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_local_entrega"
    CamposEcran(1) = "cod_local_entrega"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_local_entrega"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_local_entrega"
    CWhere = ""
    CampoPesquisa = "descr_local_entrega"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_local_entrega ", _
                                                                           " Local De Entrega")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If resultados(i) <> "" Then
                If lista.ListCount = 0 Then
                    lista.AddItem BL_SelCodigo("sl_cod_local_entrega", "descr_local_entrega", "cod_local_entrega", resultados(i))
                    lista.ItemData(0) = resultados(i)
                Else
                    lista.AddItem BL_SelCodigo("sl_cod_local_entrega", "descr_local_entrega", "cod_local_entrega", resultados(i))
                    lista.ItemData(lista.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If

End Sub
Public Sub PA_PesquisaAnaMultiSel(lista As ListBox)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "slv_analises"
    CWhere = ""
    CampoPesquisa = "descr_ana"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana ", _
                                                                           " An�lises")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If resultados(i) <> "" Then
                If lista.ListCount = 0 Then
                    lista.AddItem BL_SelCodigo("slv_analises", "descr_ana", "seq_ana", resultados(i))
                    lista.ItemData(0) = resultados(i)
                Else
                    lista.AddItem BL_SelCodigo("slv_analises", "descr_ana", "seq_ana", resultados(i))
                    lista.ItemData(lista.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If

End Sub





Public Sub PA_PesquisaMicrorgMultiSel(lista As ListBox)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_microrg"
    CamposEcran(1) = "cod_microrg"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_microrg"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_microrg"
    CWhere = ""
    CampoPesquisa = "descr_microrg"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_microrg ", _
                                                                           " Microrganismos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If resultados(i) <> "" Then
                If lista.ListCount = 0 Then
                    lista.AddItem BL_SelCodigo("sl_microrg", "descr_microrg", "seq_microrg", resultados(i))
                    lista.ItemData(0) = resultados(i)
                Else
                    lista.AddItem BL_SelCodigo("sl_microrg", "descr_microrg", "seq_microrg", resultados(i))
                    lista.ItemData(lista.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If

End Sub



Public Sub PA_PesquisaLocalEntrega(campoCodigo As TextBox, campoDescr As TextBox)

    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_local_entrega"
    CamposEcran(1) = "descr_local_entrega"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_local_entrega"
    CamposEcran(2) = "cod_local_entrega"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_cod_local_entrega"
    CampoPesquisa = "descr_sala"
    CWhere = " (flg_invisivel IS NULL or flg_invisivel = 0)"

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_local_entrega ", " Locais de Entrega")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem locais de entrega codificados", vbExclamation, "Locais de Entrega"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaLocalEntrega: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaLocalEntrega"
    Exit Sub
    Resume Next
End Sub
Public Sub PA_PesquisaLocal(campoCodigo As TextBox, campoDescr As TextBox)

    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "nome_empr"
    CamposEcran(1) = "nome_empr"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "empresA_id"
    CamposEcran(2) = "empresA_id"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "gr_empr_inst"
    CampoPesquisa = "nome_empr"
    CWhere = ""

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY nome_empr ", " Locais")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem locais  codificados", vbExclamation, "Locais "
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaLocal: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaLocal"
    Exit Sub
    Resume Next
End Sub


Public Sub PA_PesquisaAparelhoMultiSel(lista As ListBox)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_apar"
    CamposEcran(1) = "seq_apar"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_apar"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "gc_apar"
    CWhere = ""
    CampoPesquisa = "descr_apar"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_apar ", _
                                                                           " Aparelho")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If resultados(i) <> "" Then
                If lista.ListCount = 0 Then
                    lista.AddItem BL_SelCodigo("gc_apar", "descr_apar", "seq_apar", resultados(i))
                    lista.ItemData(0) = resultados(i)
                Else
                    lista.AddItem BL_SelCodigo("gc_apar", "descr_apar", "seq_apar", resultados(i))
                    lista.ItemData(lista.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If

End Sub

Public Sub PA_PesquisaAparelho(campoCodigo As TextBox, campoDescr As TextBox)

    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_apar"
    CamposEcran(1) = "descr_apar"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "seq_apar"
    CamposEcran(2) = "seq_apar"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "gescom.gc_apar"
    CampoPesquisa = "descr_apar"
    CWhere = " (flg_invisivel IS NULL or flg_invisivel = 0)"

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_apar ", " Aparelho")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem aparelhos codificados", vbExclamation, "Aparelhos"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaAparelho: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaAparelho"
    Exit Sub
    Resume Next
End Sub





Public Sub PA_PesquisaGrAna(campoCodigo As TextBox, campoDescr As TextBox)

    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_gr_ana"
    CamposEcran(1) = "descr_gr_ana"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_Gr_ana"
    CamposEcran(2) = "cod_Gr_ana"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_gr_ana"
    CampoPesquisa = "descr_gr_ana"

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_gr_ana ", " Grupos de An�lise")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem grupos de an�lises codificados", vbExclamation, "Grupos de An�lises"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaGrAna: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaGrAna"
    Exit Sub
    Resume Next
End Sub

Public Function PA_ValidateGrAna(campoCodigo As TextBox, campoDescr As TextBox) As Boolean
    On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    PA_ValidateGrAna = False
    If campoCodigo.Text <> "" Then
        Set Tabela = New ADODB.recordset

        sql = "SELECT descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana='" & Trim(campoCodigo.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount <= 0 Then
            PA_ValidateGrAna = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela!descr_gr_ana
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateGrAna: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateGrAna"
    Exit Function
    Resume Next
End Function

Public Sub PA_PesquisaProdutos(campoCodigo As TextBox, campoDescr As TextBox)

    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_produto"
    CamposEcran(1) = "descr_produto"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_produto"
    CamposEcran(2) = "cod_produto"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_produto"
    CampoPesquisa = "descr_produto"

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_produto ", " Produtos")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Produtos codificados", vbExclamation, "Produtos"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaProdutos: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaProdutos"
    Exit Sub
    Resume Next
End Sub




Public Function PA_ValidateProduto(campoCodigo As TextBox, campoDescr As TextBox) As Boolean
    On Error GoTo TrataErro

    Dim Tabela As ADODB.recordset
    Dim sql As String
    PA_ValidateProduto = False
    If campoCodigo.Text <> "" Then
        Set Tabela = New ADODB.recordset

        sql = "SELECT descr_produto FROM sl_produto WHERE cod_produto='" & Trim(campoCodigo.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao

        If Tabela.RecordCount <= 0 Then
            PA_ValidateProduto = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            campoCodigo.Text = ""
            campoDescr.Text = ""
        Else
            campoDescr.Text = Tabela!descr_produto
        End If

        Tabela.Close
        Set Tabela = Nothing
    Else
        campoDescr.Text = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "PA_ValidateProduto: " & Err.Number & " - " & Err.Description, "PA", "PA_ValidateProduto"
    Exit Function
    Resume Next
End Function



Public Sub PA_PesquisaSinal(campoCodigo As TextBox, campoDescr As TextBox)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False

    ChavesPesq(1) = "descr_sinal"
    CamposEcran(1) = "descr_sinal"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"

    ChavesPesq(2) = "cod_sinal"
    CamposEcran(2) = "cod_sinal"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"


    CamposRetorno.InicializaResultados 2

    CFrom = "sl_tbf_sinais"
    CampoPesquisa = "descr_sinal"
    CWhere = " "
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_sinal ", " Sinais")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            campoCodigo.Text = resultados(2)
            campoDescr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Sinais", vbExclamation, "Sinais"
        campoCodigo.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PA_PesquisaSinal: " & Err.Number & " - " & Err.Description, "PA", "PA_PesquisaSinal"
    Exit Sub
    Resume Next
End Sub



Public Sub PA_PesquisaUtiliMultiSel(lista As ListBox)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_utilizador"
    CamposEcran(1) = "cod_utilizador"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "Nome"
    Tamanhos(2) = 3500
    Headers(2) = "Nome"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_idutilizador"
    CWhere = ""
    CampoPesquisa = "Nome"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY Nome ", _
                                                                           " Utilizadores")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If lista.ListCount = 0 Then
                lista.AddItem BL_SelCodigo("sl_idutilizador", "Nome", "cod_utilizador", resultados(i))
                lista.ItemData(0) = resultados(i)
            Else
                lista.AddItem BL_SelCodigo("sl_idutilizador", "Nome", "cod_utilizador", resultados(i))
                lista.ItemData(lista.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub



Public Sub PA_PesquisaMbMotivosMultiSel(lista As ListBox)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_motivo_conc"
    CamposEcran(1) = "cod_motivo_conc"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "Descr_motivo_conc"
    Tamanhos(2) = 3500
    Headers(2) = "Nome"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_mb_motivos_conc"
    CWhere = ""
    CampoPesquisa = "Descri��o"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY Descr_motivo_conc ", _
                                                                           " Motivos Conclus�o")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If lista.ListCount = 0 Then
                lista.AddItem BL_SelCodigo("sl_cod_mb_motivos_conc", "Descr_motivo_conc", "cod_motivo_conc", resultados(i))
                lista.ItemData(0) = resultados(i)
            Else
                lista.AddItem BL_SelCodigo("sl_cod_mb_motivos_conc", "Descr_motivo_conc", "cod_motivo_conc", resultados(i))
                lista.ItemData(lista.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub





Public Sub PA_PesquisaCaracMicroMultiSel(lista As ListBox)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_carac_micro"
    CamposEcran(1) = "cod_carac_micro"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_carac_micro"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_tbf_carac_micro"
    CWhere = ""
    CampoPesquisa = "descr_carac_micro"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_carac_micro ", _
                                                                           " Caracteristicas Microrganismos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If lista.ListCount = 0 Then
                lista.AddItem BL_SelCodigo("sl_tbf_carac_micro", "descr_carac_micro", "cod_carac_micro", resultados(i))
                lista.ItemData(0) = resultados(i)
            Else
                lista.AddItem BL_SelCodigo("sl_tbf_carac_micro", "descr_carac_micro", "cod_carac_micro", resultados(i))
                lista.ItemData(lista.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub




Public Sub PA_RefinaPesquisa(Origem As Control, NomeTabela As String, CampoPesquisa As String, CampoChave As String, CampoControlDestino As Control)

    Dim i As Integer
    Dim TSQLQueryAux As String
    Dim Str1 As String
    Dim TTabelaAux As ADODB.recordset
        
    If Origem.Text <> "" Then
        TSQLQueryAux = "SELECT " & CampoChave & "," & CampoPesquisa & " FROM " & NomeTabela & _
                      " WHERE UPPER(" & CampoPesquisa & ") LIKE '" & UCase(BG_CvWilcard(Origem.Text)) & "%'"
        TSQLQueryAux = TSQLQueryAux & " ORDER BY " & CampoPesquisa
        
        Set TTabelaAux = New ADODB.recordset
        TTabelaAux.CursorType = adOpenStatic
        TTabelaAux.CursorLocation = adUseServer
        TTabelaAux.Open TSQLQueryAux, gConexao
    
        CampoControlDestino.ListIndex = mediComboValorNull
        CampoControlDestino.Clear
        i = 0
        Do Until TTabelaAux.EOF
            Str1 = ""
            Str1 = Str1 & TTabelaAux(CampoPesquisa)
            CampoControlDestino.AddItem Str1
            CampoControlDestino.ItemData(i) = TTabelaAux(CampoChave)
            TTabelaAux.MoveNext
            i = i + 1
        Loop
        TTabelaAux.Close
        Set TTabelaAux = Nothing
    ElseIf Origem.Text = "" Then
        CampoControlDestino.Clear
    End If

End Sub



Public Sub PA_PesquisaGrAnaMultiSel(lista As ListBox)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "Descr_gr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_gr_ana"
    CWhere = ""
    CampoPesquisa = "descr_gr_ana"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_gr_ana ", _
                                                                           " Grupos de An�lises")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If resultados(i) <> "" Then
                If lista.ListCount = 0 Then
                    lista.AddItem BL_SelCodigo("sl_gr_ana", "descr_gr_ana", "cod_gr_ana", resultados(i))
                    lista.ItemData(0) = resultados(i)
                Else
                    lista.AddItem BL_SelCodigo("sl_gr_ana", "descr_gr_ana", "cod_gr_ana", resultados(i))
                    lista.ItemData(lista.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If

End Sub






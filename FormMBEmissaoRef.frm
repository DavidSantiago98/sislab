VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormMBEmissaoRef 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormMBEmissaoRef"
   ClientHeight    =   6555
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   13125
   Icon            =   "FormMBEmissaoRef.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6555
   ScaleWidth      =   13125
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrTextoReport 
      Caption         =   "Texto Aviso Pagamento"
      Height          =   4080
      Left            =   6600
      TabIndex        =   47
      Top             =   1200
      Width           =   6375
      Begin VB.CommandButton BtTextoOk 
         Height          =   495
         Left            =   5400
         Picture         =   "FormMBEmissaoRef.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   49
         ToolTipText     =   "Ok"
         Top             =   3240
         Width           =   615
      End
      Begin VB.TextBox EctextoReport 
         Appearance      =   0  'Flat
         Height          =   2655
         Left            =   240
         MaxLength       =   220
         MultiLine       =   -1  'True
         TabIndex        =   48
         Top             =   360
         Width           =   5895
      End
   End
   Begin VB.Frame FrameDetalhe 
      Caption         =   "Detalhe Refer�ncia"
      Height          =   5295
      Left            =   240
      TabIndex        =   11
      Top             =   1200
      Width           =   12735
      Begin VB.CommandButton BtFecharRef 
         Height          =   495
         Left            =   11400
         Picture         =   "FormMBEmissaoRef.frx":0776
         Style           =   1  'Graphical
         TabIndex        =   43
         ToolTipText     =   "Fechar Refer�ncia"
         Top             =   3360
         Width           =   495
      End
      Begin VB.TextBox EcIndice 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   38
         TabStop         =   0   'False
         ToolTipText     =   "Entidade"
         Top             =   360
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.CommandButton BtEmissaoDoc2 
         Height          =   495
         Left            =   10800
         Picture         =   "FormMBEmissaoRef.frx":0EE0
         Style           =   1  'Graphical
         TabIndex        =   37
         ToolTipText     =   "Impress�o de Confirma��o de Aviso Pagamento"
         Top             =   3360
         Width           =   495
      End
      Begin VB.CommandButton BtEmissaoDoc1 
         Height          =   495
         Left            =   10200
         Picture         =   "FormMBEmissaoRef.frx":164A
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "Impress�o Aviso Pagamento"
         Top             =   3360
         Width           =   495
      End
      Begin VB.TextBox EcObservacao 
         Appearance      =   0  'Flat
         Height          =   1335
         Left            =   1320
         MultiLine       =   -1  'True
         TabIndex        =   34
         Top             =   2160
         Width           =   4935
      End
      Begin VB.Frame Frame1 
         Height          =   1215
         Left            =   0
         TabIndex        =   28
         Top             =   3960
         Width           =   12735
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Aviso Pagamento"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   7
            Left            =   120
            TabIndex        =   42
            Top             =   720
            Width           =   1260
         End
         Begin VB.Label LbAvisoPagamento 
            Height          =   255
            Left            =   1560
            TabIndex        =   41
            Top             =   720
            Width           =   4215
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Conf. Aviso Pagamento"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   6
            Left            =   6360
            TabIndex        =   40
            Top             =   720
            Width           =   1695
         End
         Begin VB.Label LbAvisoPagamento2 
            Height          =   255
            Left            =   8160
            TabIndex        =   39
            Top             =   720
            Width           =   3975
         End
         Begin VB.Label LbConclusao 
            Height          =   255
            Left            =   8160
            TabIndex        =   32
            Top             =   240
            Width           =   3855
         End
         Begin VB.Label LbCriacao 
            Height          =   255
            Left            =   1560
            TabIndex        =   31
            Top             =   240
            Width           =   4095
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Conclus�o"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   4
            Left            =   6360
            TabIndex        =   30
            Top             =   240
            Width           =   765
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Cria��o"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   3
            Left            =   120
            TabIndex        =   29
            Top             =   240
            Width           =   555
         End
      End
      Begin VB.CommandButton BtGerarNovaRef 
         Height          =   495
         Left            =   12000
         Picture         =   "FormMBEmissaoRef.frx":1DB4
         Style           =   1  'Graphical
         TabIndex        =   27
         ToolTipText     =   "Gerar Nova Refer�ncia"
         Top             =   3360
         Width           =   495
      End
      Begin VB.CommandButton BtFecharDetalhe 
         Height          =   495
         Left            =   9600
         Picture         =   "FormMBEmissaoRef.frx":8606
         Style           =   1  'Graphical
         TabIndex        =   26
         ToolTipText     =   "Voltar"
         Top             =   3360
         Width           =   495
      End
      Begin VB.TextBox EcValor 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "#�##0,00 ""�"";(#�##0,00 ""�"")"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   2
         EndProperty
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Valor"
         Top             =   1680
         Width           =   975
      End
      Begin VB.TextBox EcReferencia 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   24
         TabStop         =   0   'False
         ToolTipText     =   "Refer�ncia"
         Top             =   1200
         Width           =   1695
      End
      Begin VB.TextBox EcEntidade 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   23
         TabStop         =   0   'False
         ToolTipText     =   "Entidade"
         Top             =   720
         Width           =   975
      End
      Begin VB.Frame FrameDocumentos 
         BorderStyle     =   0  'None
         Height          =   3495
         Left            =   6120
         TabIndex        =   12
         Top             =   240
         Width           =   6495
         Begin VB.TextBox EcValorDoc 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5280
            TabIndex        =   18
            ToolTipText     =   "Data do Documento"
            Top             =   360
            Width           =   855
         End
         Begin VB.TextBox EcDataDocumento 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4080
            TabIndex        =   17
            ToolTipText     =   "Data do Documento"
            Top             =   360
            Width           =   1095
         End
         Begin VB.TextBox EcNumDoc 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2880
            TabIndex        =   16
            ToolTipText     =   "N�mero de Documento"
            Top             =   360
            Width           =   1095
         End
         Begin VB.CommandButton BtAdicionar 
            Height          =   375
            Left            =   6120
            Picture         =   "FormMBEmissaoRef.frx":92D0
            Style           =   1  'Graphical
            TabIndex        =   19
            ToolTipText     =   "Importar Conclus�es do tipo RAST"
            Top             =   300
            Width           =   375
         End
         Begin VB.TextBox EcSerieDoc 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1680
            TabIndex        =   15
            ToolTipText     =   "Serie de Documento"
            Top             =   360
            Width           =   1095
         End
         Begin VB.ComboBox CbTipoDoc 
            Height          =   315
            Left            =   240
            Style           =   2  'Dropdown List
            TabIndex        =   14
            ToolTipText     =   "Tipo de Documento"
            Top             =   360
            Width           =   1455
         End
         Begin MSFlexGridLib.MSFlexGrid FgDocumentos 
            Height          =   2415
            Left            =   240
            TabIndex        =   13
            Top             =   720
            Width           =   6255
            _ExtentX        =   11033
            _ExtentY        =   4260
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
            Appearance      =   0
         End
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(9999.99 �)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   8
         Left            =   2400
         TabIndex        =   50
         Top             =   1680
         Width           =   840
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Observa��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   5
         Left            =   240
         TabIndex        =   35
         Top             =   2160
         Width           =   900
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Valor"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   2
         Left            =   240
         TabIndex        =   22
         Top             =   1680
         Width           =   390
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Refer�ncia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   1
         Left            =   240
         TabIndex        =   21
         Top             =   1200
         Width           =   795
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Entidade"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   240
         TabIndex        =   20
         Top             =   720
         Width           =   615
      End
   End
   Begin VB.CommandButton BtNovaRef 
      Height          =   615
      Left            =   12360
      Picture         =   "FormMBEmissaoRef.frx":9F9A
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   5760
      Width           =   615
   End
   Begin VB.Frame FrameDadosUtente 
      BorderStyle     =   0  'None
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   12855
      Begin VB.CheckBox CkRepresentante 
         Caption         =   "Incluir Representante"
         Height          =   255
         Left            =   10800
         TabIndex        =   46
         Top             =   120
         Width           =   1935
      End
      Begin VB.ComboBox CbEstado 
         Height          =   315
         Left            =   9120
         Style           =   2  'Dropdown List
         TabIndex        =   44
         Top             =   600
         Width           =   1455
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Nome do Utente"
         Top             =   600
         Width           =   5415
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3480
         TabIndex        =   2
         ToolTipText     =   "N� Utente"
         Top             =   120
         Width           =   4215
      End
      Begin VB.TextBox EcNumCartaoContrib 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   11640
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "N�mero de Cart�o de Utente"
         Top             =   600
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9120
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   120
         Width           =   1455
      End
      Begin VB.ComboBox CbTipoUtente 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2280
         Style           =   2  'Dropdown List
         TabIndex        =   1
         ToolTipText     =   "Tipo de Utente"
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label LbData 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   1
         Left            =   8160
         TabIndex        =   45
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   495
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1200
         TabIndex        =   9
         Top             =   600
         Width           =   405
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1200
         TabIndex        =   8
         Top             =   120
         Width           =   465
      End
      Begin VB.Label LbData 
         AutoSize        =   -1  'True
         Caption         =   "Data Nasc."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   8160
         TabIndex        =   7
         ToolTipText     =   "Data de Nascimento"
         Top             =   120
         Width           =   795
      End
      Begin VB.Label LbReqAssoci 
         AutoSize        =   -1  'True
         Caption         =   "N�Contrib."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   10800
         TabIndex        =   6
         ToolTipText     =   "N�mero de Cart�o de Utente"
         Top             =   600
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Image Image1 
         Height          =   1065
         Left            =   120
         Picture         =   "FormMBEmissaoRef.frx":AC64
         Top             =   120
         Width           =   975
      End
   End
   Begin MSFlexGridLib.MSFlexGrid FgRef 
      Height          =   4335
      Left            =   240
      TabIndex        =   33
      Top             =   1320
      Width           =   12735
      _ExtentX        =   22463
      _ExtentY        =   7646
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
      Appearance      =   0
   End
End
Attribute VB_Name = "FormMBEmissaoRef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim estado As Integer
Dim referenciaMB As mbUtente
Dim documentosMB() As mbDocumentos
Dim totalDoc As Integer
Const vermelhoClaro = &H8080FF
Const Verde = &HC0FFC0
Dim num_carta As Integer


Private Sub BtAdicionar_Click()
    On Error GoTo TrataErro
    Dim i As Integer
    i = 0
    If CbTipoDoc.ListIndex > mediComboValorNull And EcDataDocumento.Text <> "" Then
    
    Else
        BG_Mensagem mediMsgBox, "Informa��o n�o est� completa", vbCritical, "Documentos"
        Exit Sub
    End If
    i = 1
    MB_AdicionaDocumento documentosMB, totalDoc, CbTipoDoc.ItemData(CbTipoDoc.ListIndex), CbTipoDoc.Text, EcNumDoc.Text, _
                         mediComboValorNull, EcSerieDoc.Text, EcDataDocumento.Text, BL_HandleNull(EcValorDoc.Text, 0)
    i = 2
    FgDocumentos.TextMatrix(totalDoc, 0) = documentosMB(totalDoc).descR_tipo_doc
    i = 3
    FgDocumentos.TextMatrix(totalDoc, 1) = documentosMB(totalDoc).serie_doc
    i = 4
    FgDocumentos.TextMatrix(totalDoc, 2) = documentosMB(totalDoc).n_doc
    i = 5
    FgDocumentos.TextMatrix(totalDoc, 3) = documentosMB(totalDoc).data_doc
    i = 6
    FgDocumentos.TextMatrix(totalDoc, 4) = documentosMB(totalDoc).valor
    i = 7
    FgDocumentos.AddItem ""
    i = 8
    CbTipoDoc.ListIndex = mediComboValorNull
    i = 9
    EcSerieDoc.Text = ""
    i = 10
    EcNumDoc.Text = ""
    i = 11
    EcDataDocumento.Text = ""
    i = 12
    EcValorDoc.Text = ""
Exit Sub
TrataErro:
    BG_LogFile_Erros i & "Erro ao adicionar Documento: " & Err.Number & " - " & Err.Description, Me.Name, "BtAdicionar_Click", True
    Exit Sub
    Resume Next
End Sub

Private Sub BtEmissaoDoc1_Click()
    EctextoReport.Text = "� data de emiss�o desta carta, encontrava-se a pagamento documentos no valor de " & referenciaMB.referencias(EcIndice.Text).val_pag & "�, relativa a servi�os prestados no Hospital da Arr�bida."
    FrTextoReport.Visible = True
    FrTextoReport.top = 1200
    FrTextoReport.left = 6600
    EctextoReport.SetFocus
    num_carta = 1
End Sub
Private Sub BtEmissaoDoc2_Click()
    EctextoReport.Text = "Na sequ�ncia da nossa anterior comunica��o, solicita-se, mais uma vez, o pagamento de " & referenciaMB.referencias(EcIndice.Text).val_pag & "�, relativa a servi�os prestados no Hospital da Arr�bida."
    FrTextoReport.Visible = True
    FrTextoReport.top = 1200
    FrTextoReport.left = 6600
    EctextoReport.SetFocus
    num_carta = 2
End Sub

Private Sub BtTextoOk_Click()
    If num_carta = 1 Then
        Carta1
    ElseIf num_carta = 2 Then
        Carta2
    End If
End Sub
Private Sub Carta1()
  Dim sSql As String
    Dim sql As String
    Dim continua As Boolean
    Dim StrTemp  As String
    Dim i As Integer
    Dim seq_emissao_doc As Long
    If EcIndice.Text <> "" Then
        If IsNumeric(EcIndice.Text) Then
            seq_emissao_doc = MB_RegistaImpressaoDocumento(1, referenciaMB.referencias(EcIndice.Text).seq_ref_emitida, EctextoReport.Text)
            If seq_emissao_doc > 0 Then
                'Printer Common Dialog
                continua = BL_IniciaReport("AvisoPagamento", "Avisos de Pagamento", crptToPrinter)
                
                If continua = False Then Exit Sub
                Dim Report As CrystalReport
                Set Report = forms(0).Controls("Report")
                
                Report.SQLQuery = "select sl_identif.utente, sl_identif.nome_ute, sl_identif.descr_mor_ute, sl_identif.cod_postal_ute, sl_cod_postal.descr_postal, "
                Report.SQLQuery = Report.SQLQuery & " sl_mb_ref_emitida.cod_ent_ref,sl_mb_ref_emitida.cod_ref_pag,sl_mb_ref_emitida.val_pag, sl_idutilizador.nome,sl_idutilizador.departamento, "
                Report.SQLQuery = Report.SQLQuery & " sl_idutilizador.funcao FROM sl_identif LEFT OUTER JOIN sl_cod_postal ON sl_identif.cod_postal_ute = sl_cod_postal.cod_postal,"
                Report.SQLQuery = Report.SQLQuery & " sl_mb_ref_emitida , sl_mb_emissao_doc, sl_idutilizador"
                Report.SQLQuery = Report.SQLQuery & " WHERE sl_identif.seq_utente = sl_mb_ref_emitida.seq_utente And sl_mb_ref_emitida.seq_ref_emitida = sl_mb_emissao_doc.seq_ref_emitida"
                Report.SQLQuery = Report.SQLQuery & " and sl_mb_emissao_doc.user_cri = sl_idutilizador.cod_utilizador"
                Report.SQLQuery = Report.SQLQuery & " AND sl_mb_ref_emitida.seq_ref_emitida = " & referenciaMB.referencias(EcIndice.Text).seq_ref_emitida
                Report.SQLQuery = Report.SQLQuery & " AND sl_mb_emissao_doc.SEQ_EMISSAO_DOC = " & seq_emissao_doc
                If CkRepresentante.value = vbChecked Then
                    Report.formulas(0) = "Representante='S'"
                Else
                    Report.formulas(0) = "Representante='N'"
                End If
                
                Call BL_ExecutaReport
            Else
                BG_Mensagem mediMsgBox, "Erro ao imprimir documento!", vbExclamation, "Refer�ncia MB"
            End If
        Else
            BG_Mensagem mediMsgBox, "Erro ao imprimir documento!", vbExclamation, "Refer�ncia MB"
        End If
    Else
        BG_Mensagem mediMsgBox, "Erro ao imprimir documento!", vbExclamation, "Refer�ncia MB"
    End If
    EctextoReport.Text = ""
    FrTextoReport.Visible = False
End Sub

Private Sub Carta2()
  Dim sSql As String
    Dim sql As String
    Dim continua As Boolean
    Dim StrTemp  As String
    Dim i As Integer
    Dim seq_emissao_doc As Long
    
    Dim nomeReport As String
    If EcIndice.Text <> "" Then
        If IsNumeric(EcIndice.Text) Then
            seq_emissao_doc = MB_RegistaImpressaoDocumento(2, referenciaMB.referencias(EcIndice.Text).seq_ref_emitida, EctextoReport.Text)
            If seq_emissao_doc > 0 Then
                'Printer Common Dialog
                continua = BL_IniciaReport("AvisoPagamentoConfirmacao", "Avisos de Pagamento", crptToPrinter, , , , , , , , , "AvisoPagamento")
                
                If continua = False Then Exit Sub
                Dim Report As CrystalReport
                Set Report = forms(0).Controls("Report")
                
                Report.SQLQuery = "select sl_identif.utente, sl_identif.nome_ute, sl_identif.descr_mor_ute, sl_identif.cod_postal_ute, sl_cod_postal.descr_postal, "
                Report.SQLQuery = Report.SQLQuery & " sl_mb_ref_emitida.cod_ent_ref,sl_mb_ref_emitida.cod_ref_pag,sl_mb_ref_emitida.val_pag, sl_idutilizador.nome,sl_idutilizador.departamento, "
                Report.SQLQuery = Report.SQLQuery & " sl_idutilizador.funcao FROM sl_identif LEFT OUTER JOIN sl_cod_postal ON sl_identif.cod_postal_ute = sl_cod_postal.cod_postal,"
                Report.SQLQuery = Report.SQLQuery & " sl_mb_ref_emitida , sl_mb_emissao_doc, sl_idutilizador"
                Report.SQLQuery = Report.SQLQuery & " WHERE sl_identif.seq_utente = sl_mb_ref_emitida.seq_utente And sl_mb_ref_emitida.seq_ref_emitida = sl_mb_emissao_doc.seq_ref_emitida"
                Report.SQLQuery = Report.SQLQuery & " and sl_mb_emissao_doc.user_cri = sl_idutilizador.cod_utilizador"
                Report.SQLQuery = Report.SQLQuery & " AND sl_mb_ref_emitida.seq_ref_emitida = " & referenciaMB.referencias(EcIndice.Text).seq_ref_emitida
                Report.SQLQuery = Report.SQLQuery & " AND sl_mb_emissao_doc.SEQ_EMISSAO_DOC = " & seq_emissao_doc
                If CkRepresentante.value = vbChecked Then
                    Report.formulas(0) = "Representante='S'"
                Else
                    Report.formulas(0) = "Representante='N'"
                End If
            
                Call BL_ExecutaReport
            Else
                BG_Mensagem mediMsgBox, "Erro ao imprimir documento!", vbExclamation, "Refer�ncia MB"
            End If
        Else
            BG_Mensagem mediMsgBox, "Erro ao imprimir documento!", vbExclamation, "Refer�ncia MB"
        End If
    Else
        BG_Mensagem mediMsgBox, "Erro ao imprimir documento!", vbExclamation, "Refer�ncia MB"
    End If
End Sub


Private Sub BtFecharDetalhe_Click()
    FrameDetalhe.Visible = False
    totalDoc = 0
    ReDim documentosMB(totalDoc)
    FuncaoProcurar
End Sub


Private Sub BtFecharRef_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim res(100) As Variant
    Dim PesqRapida As Boolean
    Dim RsRelacao As ADODB.recordset
    Dim TotalElementosSel As Integer
    
    On Error GoTo TrataErro
   
        
    FormPesqRapidaAvancada.Width = 10000
    FormPesqRapidaAvancada.LwPesquisa.Width = 9500
    PesqRapida = False
    
    ChavesPesq(1) = "descr_motivo_conc"
    CamposEcran(1) = "descr_motivo_conc"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_motivo_conc"
    CamposEcran(2) = "cod_motivo_conc"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_mb_motivos_conc"
    CampoPesquisa = "descr_motivo_conc"
    CWhere = " cod_motivo_conc <> 0 "
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_motivo_conc ", " Motivo de Fecho")
    
    If (PesqRapida) Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados res, CancelouPesquisa, TotalElementosSel
        If res(2) <> "" Then
            If referenciaMB.referencias(EcIndice.Text).seq_ref_emitida > 0 Then
                If referenciaMB.referencias(EcIndice.Text).flg_estado = 0 Then
                    If MB_FechoReferenciaManual(CInt(res(2)), referenciaMB.referencias(EcIndice.Text).seq_ref_emitida) = True Then
                        BtFecharDetalhe_Click
                    End If
                End If
            End If
        Else
            BG_Mensagem mediMsgBox, "Fecho cancelado.", vbExclamation, "ATEN��O"
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Motivos codificados!", vbExclamation, "ATEN��O"
    End If
    
         
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtFecharRef_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtFecharRef_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtGerarNovaRef_Click()
    Dim seq_ref As Long
    Dim cod_estado As String
    On Error GoTo TrataErro
    
    If CbEstado.ListIndex > mediComboValorNull Then
        cod_estado = CbEstado.ItemData(CbEstado.ListIndex)
    Else
        cod_estado = ""
    End If
    
    BG_BeginTransaction
    If EcEntidade.Text <> "" Then
        If EcValor.Text <> "" Then
            If IsNumeric(EcValor.Text) = True Then
                seq_ref = MB_GeraReferencia(referenciaMB.seq_utente, CStr(gCodLocal), EcValor.Text, "", EcObservacao.Text)
                If seq_ref > 0 Then
                    If MB_PreencheReferencia(referenciaMB, seq_ref, cod_estado) = True Then
                        referenciaMB.referencias(referenciaMB.totalRef).totalDoc = totalDoc
                        referenciaMB.referencias(referenciaMB.totalRef).documentos = documentosMB
                        If MB_GravaDocumentos(referenciaMB.referencias, referenciaMB.totalRef) = True Then
                            PreencheDadosReferencia referenciaMB.totalRef
                            BG_Mensagem mediMsgBox, "Refer�ncia Multibanco criada com sucesso!", vbExclamation, "Refer�ncia MB"
                            EcIndice.Text = referenciaMB.totalRef
                        Else
                            BG_RollbackTransaction
                            
                        End If
                    End If
                Else
                    BG_Mensagem mediMsgBox, "Erro ao gerar refer�ncia", vbCritical, "Refer�ncia MB"
                    BG_RollbackTransaction
                End If
            Else
                BG_Mensagem mediMsgBox, "Valor tem que ser num�rico.", vbCritical, "Refer�ncia MB"
            End If
        Else
            BG_Mensagem mediMsgBox, "Tem que indicar um valor.", vbCritical, "Refer�ncia MB"
        End If
    Else
        BG_Mensagem mediMsgBox, "Tem que indicar uma entidade.", vbCritical, "Refer�ncia MB"
    End If
    BG_CommitTransaction
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtGerarNovaRef_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtGerarNovaRef_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtImp_Click()
    Dim sSql As String
    Dim rsUte As New ADODB.recordset
    If EcUtente.Text <> "" And CbTipoUtente.ListIndex <> mediComboValorNull Then
        If referenciaMB.seq_utente <= 0 Then
            If IN_TransfereUtente(EcUtente.Text, mediComboValorNull, True) = True Then
                AtualizaUtente
            End If
            
        Else
            BG_Mensagem mediMsgBox, "Utente j� seleccionado.", vbCritical, "Refer�ncia MB"
        End If
    Else
        BG_Mensagem mediMsgBox, "Tem que indicar tipo de utente e utente", vbCritical, "Refer�ncia MB"
    End If
End Sub

Private Sub BtNovaRef_Click()
    If FgRef.row <= 0 Then FgRef.row = 1
    If CbEstado.ListIndex > mediComboValorNull Then
        CbEstado.ListIndex = mediComboValorNull
    End If
    If referenciaMB.seq_utente > mediComboValorNull Then
        MostraDetalhe mediComboValorNull
    Else
        BG_Mensagem mediMsgBox, "Tem que seleccionar um Utente", vbCritical, "Refer�ncia MB"
     
    End If
End Sub

Private Sub MostraDetalhe(indice As Integer)
    If BL_HandleNull(gEntidadeMultibanco, mediComboValorNull) <= 0 Then
        BG_Mensagem mediMsgBox, "Entidade Multibanco n�o parametrizada. N�o � poss�vel gerar refer�ncias.", vbCritical, "Refer�ncia MB"
        Exit Sub
    End If
    
    LimpaFgDocumentos
    PreencheDadosReferencia indice
    FrameDetalhe.top = 1320
    FrameDetalhe.left = 240
    FrameDetalhe.Visible = True
End Sub



Private Sub CbEstado_Click()
    FuncaoProcurar
End Sub

Private Sub CbEstado_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbEstado.ListIndex = mediComboValorNull
    End If
End Sub

Private Sub CbTipoDoc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcNumDoc.SetFocus
    End If
End Sub


Private Sub CbTipoUtente_Click()
    If EcUtente.Text <> "" And CbTipoUtente.ListIndex <> mediComboValorNull Then
        AtualizaUtente
    End If
End Sub

Private Sub EcDataDocumento_GotFocus()
    If EcDataDocumento.Text = "" Then
        EcDataDocumento.Text = Bg_DaData_ADO
    End If
End Sub

Private Sub EcNumDoc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcSerieDoc.SetFocus
    End If
End Sub

Private Sub EcSerieDoc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcDataDocumento.SetFocus
    End If

End Sub
Private Sub Ecdatadocumento_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn And BtAdicionar.Enabled = True Then
        EcValorDoc.SetFocus
    End If
End Sub

Private Sub EcUtente_Validate(Cancel As Boolean)
    If EcUtente.Text <> "" And CbTipoUtente.ListIndex <> mediComboValorNull Then
        AtualizaUtente
    End If
End Sub

Private Sub EcValor_Validate(Cancel As Boolean)
    EcValor.Text = Replace(EcValor.Text, ".", ",")
    If EcValor.Text <> "" Then
        If Not IsNumeric(EcValor.Text) Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Valor tem que ser num�rico.", vbCritical, "Refer�ncia MB"
        Else
            EcValor.Text = Round(EcValor.Text, 2)
            'EcValor.Text = Right(Int(EcValor.Text), 6) & "," & Right("00" & Round(EcValor.Text - Int(EcValor.Text), 2), 2)
        End If
    End If
End Sub


Private Sub EcValorDoc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        BtAdicionar_Click
    End If
End Sub

Private Sub EcValorDoc_Validate(Cancel As Boolean)
    EcValorDoc.Text = Replace(EcValorDoc.Text, ".", ",")
    If EcValorDoc.Text <> "" Then
        If Not IsNumeric(EcValorDoc.Text) Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Valor tem que ser num�rico.", vbCritical, "Refer�ncia MB"
        Else
            EcValorDoc.Text = Round(EcValorDoc.Text, 2)
            'EcValor.Text = Right(Int(EcValor.Text), 6) & "," & Right("00" & Round(EcValor.Text - Int(EcValor.Text), 2), 2)
        End If
    End If
    
End Sub

Private Sub FgDocumentos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        If BL_HandleNull(EcIndice.Text, mediComboValorNull) = mediComboValorNull Then
            If FgDocumentos.row > 0 And FgDocumentos.row <= totalDoc Then
                EliminaDocumento FgDocumentos.row
            End If
        End If
    End If
End Sub

Private Sub EliminaDocumento(ByVal iDoc As Integer)
    Dim i As Integer
    For i = iDoc To totalDoc - 1
        documentosMB(i).cod_tipo_doc = documentosMB(i + 1).cod_tipo_doc
        documentosMB(i).data_doc = documentosMB(i + 1).data_doc
        documentosMB(i).descR_tipo_doc = documentosMB(i + 1).descR_tipo_doc
        documentosMB(i).n_doc = documentosMB(i + 1).n_doc
        documentosMB(i).seq_ref_emitida_fact = documentosMB(i + 1).seq_ref_emitida_fact
        documentosMB(i).serie_doc = documentosMB(i + 1).serie_doc
        documentosMB(i).valor = documentosMB(i + 1).valor
    Next i
    totalDoc = totalDoc - 1
    ReDim Preserve documentosMB(totalDoc)
    FgDocumentos.RemoveItem iDoc
End Sub
Private Sub FgRef_DblClick()
    If FgRef.row > 0 And FgRef.row <= referenciaMB.totalRef Then
        MostraDetalhe FgRef.row
    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Emiss�o de Refer�ncias Multibanco"
    Me.left = 50
    Me.top = 50
    Me.Width = 13140
    Me.Height = 6945 ' Normal
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    Dim i As Integer
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    For i = 0 To CbTipoUtente.ListCount - 1
        If gCodLocal = 1 Then
            If CbTipoUtente.ItemData(i) = 1 Then
                CbTipoUtente.ListIndex = i
                Exit For
            End If
        ElseIf gCodLocal = 2 Then
            If CbTipoUtente.ItemData(i) = 2 Then
                CbTipoUtente.ListIndex = i
                Exit For
            End If
        End If
    Next i
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormScriptFactus = Nothing
End Sub



Sub DefTipoCampos()
    EcDataDocumento.Tag = adDate
    FrTextoReport.Visible = False
    MB_InicializaEstrut referenciaMB
    FrameDetalhe.Visible = False
    EcNome.Locked = True
    EcNumCartaoContrib.Locked = True
    EcDataNasc.Locked = True
    With FgDocumentos
        .rows = 2
        .FixedRows = 1
        .Cols = 5
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        
        .ColWidth(0) = 1400
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, 0) = "Tipo Doc."
        
        .ColWidth(1) = 1200
        .Col = 1
        .ColAlignment(1) = flexAlignLeftCenter
        .TextMatrix(0, 1) = "Serie Doc."
        
        .ColWidth(2) = 1200
        .Col = 2
        .ColAlignment(2) = flexAlignLeftCenter
        .TextMatrix(0, 2) = "Num Doc."
        
        .ColWidth(3) = 1300
        .Col = 3
        .ColAlignment(3) = flexAlignLeftCenter
        .TextMatrix(0, 3) = "Data Doc."
        
        .ColWidth(4) = 900
        .Col = 4
        .ColAlignment(4) = flexAlignLeftCenter
        .TextMatrix(0, 4) = "Valor"
        
        .WordWrap = False
        .row = 0
        .Col = 0
    End With
    
    With FgRef
        .rows = 2
        .FixedRows = 1
        .Cols = 9
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        
        
        .ColWidth(0) = 1200
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, 0) = "Refer�ncia"
        
        .ColWidth(1) = 1300
        .Col = 1
        .ColAlignment(1) = flexAlignLeftCenter
        .TextMatrix(0, 1) = "Valor"
                
        .ColWidth(2) = 1350
        .Col = 2
        .ColAlignment(2) = flexAlignLeftCenter
        .TextMatrix(0, 2) = "Data Emiss�o"
        
        .ColWidth(3) = 1500
        .Col = 3
        .ColAlignment(3) = flexAlignLeftCenter
        .TextMatrix(0, 3) = "Util.Emiss�o"
        
        .ColWidth(4) = 1300
        .Col = 4
        .ColAlignment(4) = flexAlignLeftCenter
        .TextMatrix(0, 4) = "Estado"
        
        .ColWidth(5) = 1350
        .Col = 5
        .ColAlignment(5) = flexAlignLeftCenter
        .TextMatrix(0, 5) = "Data Conclus�o"
        
        .ColWidth(6) = 1500
        .Col = 6
        .ColAlignment(6) = flexAlignLeftCenter
        .TextMatrix(0, 6) = "Util Conclus�o"
        
        .ColWidth(7) = 1300
        .Col = 7
        .ColAlignment(7) = flexAlignLeftCenter
        .TextMatrix(0, 7) = "Motivo Conc"
        
        .ColWidth(8) = 1300
        .Col = 8
        .ColAlignment(8) = flexAlignLeftCenter
        .TextMatrix(0, 8) = "Data Pagamento"
        
        .WordWrap = False
        .row = 0
        .Col = 0
    End With
    If BL_HandleNull(gEntidadeMultibanco, mediComboValorNull) <= 0 Then
        BG_Mensagem mediMsgBox, "Entidade Multibanco n�o parametrizada. N�o � poss�vel gerar refer�ncias.", vbCritical, "Refer�ncia MB"
    End If
    totalDoc = 0
    ReDim documentosMB(totalDoc)
End Sub

Sub PreencheCampos()
    Dim i As Integer
    EcUtente.Locked = True
    CbTipoUtente.Locked = True
    EcNome.Text = referenciaMB.nome_ute
    EcDataNasc.Text = referenciaMB.dt_nasc_ute
    EcNumCartaoContrib.Text = referenciaMB.num_cartao_contrib
    EcUtente.Text = referenciaMB.Utente
    If BL_Da_Idade(Day(referenciaMB.dt_nasc_ute), Month(referenciaMB.dt_nasc_ute), Year(referenciaMB.dt_nasc_ute)) < 18 Then
        CkRepresentante.value = vbChecked
    Else
        CkRepresentante.value = vbUnchecked
    End If
    For i = 0 To CbTipoUtente.ListCount - 1
        If CbTipoUtente.List(i) = referenciaMB.t_utente Then
            CbTipoUtente.ListIndex = i
            Exit For
        End If
    Next i
    FuncaoProcurar
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    BG_PreencheComboBD_ADO "sl_tbf_tipo_doc", "cod_t_doc", "descr_t_doc", CbTipoDoc
    BG_PreencheComboBD_ADO "sl_tbf_estado_mb", "cod_estado", "descr_estado", CbEstado

End Sub

Sub FuncaoLimpar()
    Dim i As Integer
    LimpaCamposUtente
    MB_InicializaEstrut referenciaMB
    totalDoc = 0
    ReDim documentosMB(totalDoc)
    FrameDetalhe.Visible = False
    GereBotoes mediComboValorNull
    LimpaFgDocumentos
    LimpaFgRef
    For i = 0 To CbTipoUtente.ListCount - 1
        If gCodLocal = 1 Then
            If CbTipoUtente.ItemData(i) = 1 Then
                CbTipoUtente.ListIndex = i
                Exit For
            End If
        ElseIf gCodLocal = 2 Then
            If CbTipoUtente.ItemData(i) = 2 Then
                CbTipoUtente.ListIndex = i
                Exit For
            End If
        End If
    Next i
End Sub
Private Sub LimpaCamposUtente()
    EcUtente.Locked = False
    CbTipoUtente.Locked = False
    EcUtente.Text = ""
    EcNome.Text = ""
    EcDataNasc.Text = ""
    EcNumCartaoContrib.Text = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    CbEstado.ListIndex = mediComboValorNull
    CkRepresentante.value = vbUnchecked
End Sub

Sub FuncaoEstadoAnterior()

End Sub

Sub FuncaoProcurar()
    Dim sSql As String
    Dim RsRef As New ADODB.recordset
    Dim i As Integer
    Dim cod_estado As String
    LimpaFgRef
    referenciaMB.totalRef = 0
    ReDim referenciaMB.referencias(0)
    If CbEstado.ListIndex > mediComboValorNull Then
        cod_estado = CbEstado.ItemData(CbEstado.ListIndex)
    Else
        cod_estado = ""
    End If
    If referenciaMB.seq_utente > 0 Then
        MB_PreencheReferencia referenciaMB, mediComboValorNull, cod_estado
        
        For i = 1 To referenciaMB.totalRef
            FgRef.TextMatrix(i, 0) = Right("000000000" & referenciaMB.referencias(i).cod_ref_pag, 9)
            FgRef.TextMatrix(i, 1) = referenciaMB.referencias(i).val_pag & " �"
            FgRef.TextMatrix(i, 2) = referenciaMB.referencias(i).dt_cri
            FgRef.TextMatrix(i, 3) = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", referenciaMB.referencias(i).user_cri, "V")
            FgRef.row = i
            FgRef.Col = 4
            If referenciaMB.referencias(i).flg_estado = "0" Then
                FgRef.TextMatrix(i, 4) = BL_SelCodigo("SL_TBF_ESTADO_MB", "DESCR_ESTADO", "COD_ESTADO", 0)
                FgRef.CellBackColor = vermelhoClaro
            ElseIf referenciaMB.referencias(i).flg_estado = "1" Then
                FgRef.TextMatrix(i, 4) = BL_SelCodigo("SL_TBF_ESTADO_MB", "DESCR_ESTADO", "COD_ESTADO", 1)
                FgRef.CellBackColor = Verde
            Else
                FgRef.TextMatrix(i, 4) = ""
                FgRef.CellBackColor = vbWhite
            End If
            FgRef.TextMatrix(i, 5) = referenciaMB.referencias(i).dt_conc
            If referenciaMB.referencias(i).user_conc <> "" Then
                FgRef.TextMatrix(i, 6) = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", referenciaMB.referencias(i).user_conc, "V")
            End If
            If referenciaMB.referencias(i).cod_motivo_conc <> "" Then
                FgRef.TextMatrix(i, 7) = BL_SelCodigo("SL_COD_MB_MOTIVOS_CONC", "DESCR_MOTIVO_CONC", "COD_MOTIVO_CONC", referenciaMB.referencias(i).cod_motivo_conc)
            End If
            FgRef.TextMatrix(i, 8) = referenciaMB.referencias(i).dt_pagamento
            FgRef.AddItem ""
        Next i
    
    End If
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()


End Sub



Sub FuncaoModificar()


End Sub



Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub


Private Sub AtualizaUtente()
    Dim sSql As String
    Dim rsUte As New ADODB.recordset
    On Error GoTo TrataErro
    If referenciaMB.seq_utente > mediComboValorNull Then
        Exit Sub
    End If
    If EcUtente.Text <> "" And CbTipoUtente.ListIndex <> mediComboValorNull Then
        If referenciaMB.seq_utente <= 0 Then
            If IN_TransfereUtente(EcUtente.Text, mediComboValorNull, True) = True Then
                sSql = "SELECT * FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(CbTipoUtente.Text)
                sSql = sSql & " AND utente = " & BL_TrataStringParaBD(EcUtente.Text)
                rsUte.CursorType = adOpenStatic
                rsUte.CursorLocation = adUseServer
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsUte.Open sSql, gConexao
                If rsUte.RecordCount = 1 Then
                    LimpaCamposUtente
                    MB_PreencheDadosUtente referenciaMB, BL_HandleNull(rsUte!seq_utente, mediComboValorNull), BL_HandleNull(rsUte!dt_nasc_ute, ""), _
                        BL_HandleNull(rsUte!t_utente, ""), BL_HandleNull(rsUte!Utente, ""), BL_HandleNull(rsUte!nome_ute, ""), _
                        BL_HandleNull(rsUte!n_cartao_ute, ""), BL_HandleNull(rsUte!n_contrib_ute, "")
                        
                    PreencheCampos
                End If
            Else
                BG_Mensagem mediMsgBox, "Erro ao Importar o Utente.", vbCritical, "Refer�ncia MB"
            End If
        End If
    End If
    rsUte.Close
    Set rsUte = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "AtualizaUtente: " & Err.Number & " - " & Err.Description, Me.Name, "AtualizaUtente"
    Exit Sub
    Resume Next
End Sub



Private Sub PreencheDadosReferencia(indice As Integer)
    Dim i As Integer
    On Error GoTo TrataErro
    If indice > mediComboValorNull Then
        EcIndice.Text = indice
        EcEntidade.Text = referenciaMB.referencias(indice).cod_ent_ref
        EcReferencia.Text = Right("000000000" & referenciaMB.referencias(indice).cod_ref_pag, 9)
        EcValor.Text = referenciaMB.referencias(indice).val_pag
        LbCriacao.caption = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", referenciaMB.referencias(indice).user_cri, "V") & " " & referenciaMB.referencias(indice).dt_cri
        LbConclusao.caption = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", referenciaMB.referencias(indice).user_conc, "V") & " " & referenciaMB.referencias(indice).dt_conc
        LbAvisoPagamento.caption = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", referenciaMB.referencias(indice).user_doc, "V") & " " & referenciaMB.referencias(indice).dt_doc
        LbAvisoPagamento2.caption = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", referenciaMB.referencias(indice).user_doc2, "V") & " " & referenciaMB.referencias(indice).dt_doc2
        EcObservacao.Text = referenciaMB.referencias(indice).observacao
        EcObservacao.Enabled = False
    Else
        EcIndice.Text = mediComboValorNull
        EcEntidade.Text = gEntidadeMultibanco
        EcReferencia.Text = ""
        EcValor.Text = ""
        LbCriacao.caption = ""
        LbConclusao.caption = ""
        LbAvisoPagamento.caption = ""
        LbAvisoPagamento2.caption = ""
        EcObservacao.Text = ""
        EcObservacao.Enabled = True
    End If
    CbTipoDoc.ListIndex = mediComboValorNull
    EcSerieDoc.Text = ""
    EcNumDoc.Text = ""
    EcDataDocumento.Text = ""
    EcValorDoc.Text = ""
    
    preencheDocumentos indice
    If indice > mediComboValorNull Then
        For i = 1 To referenciaMB.referencias(indice).totalDoc
            FgDocumentos.TextMatrix(i, 0) = referenciaMB.referencias(indice).documentos(i).descR_tipo_doc
            FgDocumentos.TextMatrix(i, 1) = referenciaMB.referencias(indice).documentos(i).serie_doc
            FgDocumentos.TextMatrix(i, 2) = referenciaMB.referencias(indice).documentos(i).n_doc
            FgDocumentos.TextMatrix(i, 3) = referenciaMB.referencias(indice).documentos(i).data_doc
            FgDocumentos.TextMatrix(i, 4) = referenciaMB.referencias(indice).documentos(i).valor
            FgDocumentos.AddItem ""
        Next i
    End If
    GereBotoes indice
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDadosReferencia: " & Err.Number & " - " & Err.Description, Me.Name, "AtualizaUtente"
    Exit Sub
    Resume Next
End Sub

Private Sub GereBotoes(indice As Integer)
    EcEntidade.Locked = True
    If indice > mediComboValorNull Then
        If referenciaMB.referencias(indice).seq_ref_emitida > mediComboValorNull Then
            BtGerarNovaRef.Enabled = False
            BtAdicionar.Enabled = False
        End If
        If referenciaMB.referencias(indice).dt_conc = "" Then
            BtFecharRef.Enabled = True
            BtEmissaoDoc1.Enabled = True
            BtEmissaoDoc2.Enabled = True
        Else
            BtFecharRef.Enabled = False
            BtEmissaoDoc1.Enabled = False
            BtEmissaoDoc2.Enabled = False
        End If
    Else
        BtAdicionar.Enabled = True
        BtGerarNovaRef.Enabled = True
        BtFecharRef.Enabled = False
        BtEmissaoDoc1.Enabled = False
        BtEmissaoDoc2.Enabled = False
    End If
End Sub

Private Sub preencheDocumentos(indice As Integer)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsDoc As New ADODB.recordset
    If indice > mediComboValorNull Then
        If referenciaMB.referencias(indice).totalDoc <= 0 Then
            LimpaFgDocumentos
            sSql = "SELECT * FROM sl_mb_ref_emitida_fact WHERE seq_ref_emitida = " & referenciaMB.referencias(indice).seq_ref_emitida
            RsDoc.CursorType = adOpenStatic
            RsDoc.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            RsDoc.Open sSql, gConexao
            If RsDoc.RecordCount >= 1 Then
                While Not RsDoc.EOF
                        MB_PreencheDocumentos referenciaMB.referencias, indice, BL_HandleNull(RsDoc!seq_ref_emitida_fact, mediComboValorNull), _
                                              BL_HandleNull(RsDoc!cod_t_doc, mediComboValorNull), BL_HandleNull(RsDoc!serie_doc, ""), _
                                              BL_HandleNull(RsDoc!n_doc, ""), BL_HandleNull(RsDoc!dt_documento, ""), BL_HandleNull(RsDoc!valor, 0)
                    RsDoc.MoveNext
                Wend
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDocumentos: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDocumentos"
    Exit Sub
    Resume Next
End Sub

Private Sub LimpaFgDocumentos()
    Dim i As Integer
    FgDocumentos.rows = 2
    For i = 0 To FgDocumentos.Cols - 1
        FgDocumentos.TextMatrix(1, i) = ""
        
    Next i
End Sub
Private Sub LimpaFgRef()
    Dim i As Integer
    FgRef.rows = 2
    For i = 0 To FgRef.Cols - 1
        FgRef.TextMatrix(1, i) = ""
        FgRef.Col = i
        FgRef.CellBackColor = vbWhite
    Next i
End Sub
Sub Funcao_DataActual()

    If Me.ActiveControl.Name = "EcDataDocumento" Then
        BL_PreencheData EcDataDocumento, Me.ActiveControl
    End If
    
End Sub


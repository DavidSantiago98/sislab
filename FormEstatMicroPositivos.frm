VERSION 5.00
Begin VB.Form FormEstatMicroPositivos 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormEstatMicroPositivos"
   ClientHeight    =   7410
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   7995
   Icon            =   "FormEstatMicroPositivos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7410
   ScaleWidth      =   7995
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkContaminantes 
      Caption         =   "DescriminarContaminantes"
      Height          =   195
      Left            =   5280
      TabIndex        =   34
      Top             =   6840
      Width           =   2415
   End
   Begin VB.CheckBox CkImproprias 
      Caption         =   "Descriminar Impr�prias"
      Height          =   195
      Left            =   3120
      TabIndex        =   33
      Top             =   6840
      Width           =   1935
   End
   Begin VB.TextBox EcUtente 
      Height          =   285
      Left            =   0
      TabIndex        =   28
      Top             =   7080
      Width           =   735
   End
   Begin VB.CheckBox Flg_DescrRequis 
      Caption         =   "Descriminar Requisi��es e Utentes"
      Height          =   195
      Left            =   0
      TabIndex        =   27
      Top             =   6840
      Width           =   2895
   End
   Begin VB.Frame Frame1 
      Caption         =   "Condi��es de Pesquisa"
      Height          =   5775
      Left            =   120
      TabIndex        =   6
      Top             =   960
      Width           =   7695
      Begin VB.ListBox ListaSituacao 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   36
         Top             =   840
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaSituacao 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicroPositivos.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   35
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos "
         Top             =   840
         Width           =   375
      End
      Begin VB.ListBox ListaGrPerfis 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   31
         Top             =   3840
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaGrPerfis 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicroPositivos.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   30
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   3840
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaGrupo 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicroPositivos.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos "
         Top             =   1560
         Width           =   375
      End
      Begin VB.TextBox EcDescrGrupo 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   1560
         Width           =   4095
      End
      Begin VB.TextBox EcCodGrupo 
         Height          =   315
         Left            =   1560
         TabIndex        =   17
         Top             =   1560
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaProduto 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicroPositivos.frx":10AA
         Style           =   1  'Graphical
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos "
         Top             =   2040
         Width           =   375
      End
      Begin VB.ComboBox EcDescrSexo 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   405
         Width           =   1455
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   5280
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   405
         Width           =   1455
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicroPositivos.frx":1634
         Style           =   1  'Graphical
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncias "
         Top             =   2640
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaMicro 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicroPositivos.frx":1BBE
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Microrganismos"
         Top             =   3240
         Width           =   375
      End
      Begin VB.ListBox ListaProduto 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   11
         Top             =   2040
         Width           =   4800
      End
      Begin VB.ListBox ListaProven 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   10
         Top             =   2640
         Width           =   4800
      End
      Begin VB.ListBox ListaMicrorg 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   9
         Top             =   3240
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaPerfis 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatMicroPositivos.frx":2148
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   4440
         Width           =   375
      End
      Begin VB.ListBox ListaPerfis 
         Height          =   1230
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   7
         Top             =   4440
         Width           =   4800
      End
      Begin VB.Label Label11 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   360
         TabIndex        =   37
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label5 
         Caption         =   "&Grupo Exames"
         Height          =   255
         Index           =   1
         Left            =   360
         TabIndex        =   32
         Top             =   3840
         Width           =   1215
      End
      Begin VB.Label Label10 
         Caption         =   "&Grupo An�lises"
         Height          =   255
         Left            =   360
         TabIndex        =   26
         Top             =   1560
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Pro&duto"
         Height          =   255
         Left            =   360
         TabIndex        =   25
         Top             =   2040
         Width           =   975
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Se&xo"
         Height          =   195
         Left            =   360
         TabIndex        =   24
         Top             =   405
         Width           =   360
      End
      Begin VB.Label Label6 
         Caption         =   "Prio&ridade"
         Height          =   255
         Left            =   4320
         TabIndex        =   23
         Top             =   405
         Width           =   735
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Left            =   360
         TabIndex        =   22
         Top             =   2640
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "&Microrganismo"
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   21
         Top             =   3240
         Width           =   1215
      End
      Begin VB.Label Label7 
         Caption         =   "&Exames"
         Height          =   255
         Left            =   360
         TabIndex        =   20
         Top             =   4440
         Width           =   1215
      End
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   7695
      Begin VB.OptionButton Option1 
         Caption         =   "Dt. Valida��o"
         Height          =   255
         Index           =   1
         Left            =   5280
         TabIndex        =   39
         Top             =   480
         Width           =   1335
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Dt. Chegada"
         Height          =   255
         Index           =   0
         Left            =   5280
         TabIndex        =   38
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton BtReportLista 
         Caption         =   "Lista"
         Height          =   725
         Left            =   6960
         Picture         =   "FormEstatMicroPositivos.frx":26D2
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   120
         Width           =   735
      End
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   2400
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   3960
         TabIndex        =   1
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   3690
         TabIndex        =   5
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   1440
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Label Label8 
      Caption         =   "Utente"
      Height          =   255
      Index           =   1
      Left            =   840
      TabIndex        =   29
      Top             =   7080
      Width           =   1215
   End
End
Attribute VB_Name = "FormEstatMicroPositivos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim ListaOrigem As Control
Dim ListaDestino As Control

Public rs As ADODB.recordset

' -----------------------------------------------------------------------
' ESTRUTURAS USADAS PARA COMPARAR MICRORGANISMOS
' -----------------------------------------------------------------------
Private Type MicroUtente
    cod_micro As String
    numMicro As Integer
    numInfecoes As Integer
End Type

Private Type Antibiotico
    cod_antib As String
    descr_antib As String
    abrev_antib As String
    res_sensib As String
End Type

Private Type Microrganismo
    seq_realiza As String
    cod_micro As String
    descr_micro As String
    id_prova As String
    N_Res As String
    flg_tsq As String
    descr_produto As String
    
    TotalAntibioticos As Long
    EstrutAntib() As Antibiotico
End Type

Private Type requisicao
    n_req As String
    dt_chega As String
    descr_proven As String
    
    totalMicro As Long
    EstrutMicro() As Microrganismo
End Type

Private Type Utente
    seq_utente As String
    nome_ute As String
    Utente As String
    
    totalReq As Long
    EstrutReq() As requisicao
    
    TotalMiUte As Long
    EstrutMicroUte() As MicroUtente
End Type

Dim EstrutUte() As Utente
Dim TotalUte As Long
' -----------------------------------------------------------------------

'Estruturas utilizadas para gerar as etiquetas
Private Type Antibiograma
    seq_utente As String
    cod_micro As String
    cod_antib As String
    Sensib As String
End Type
Dim antib() As Antibiograma

Sub Preenche_Estatistica(TipoReport As String)
    Dim GrupoPerfis As String
    Dim sql As String
    Dim continua As Boolean
    Dim i As Integer
    Dim totalMicro As Long
    Dim TotalDupl As Long
    Dim TotalNaoDupl As Long
    Dim idxU As Long
    Dim idxM As Long
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Estat�stica de Positivos - Microbiologia") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    'Printer Common Dialog
    gImprimirDestino = 0
    continua = BL_IniciaReport("EstatisticaMicrorgPositiovs", "Estat�stica de Positivos - Microbiologia", gImprimirDestino)
     
    If continua = False Then Exit Sub
        
    
    PreencheTabelaTemporariaNEW
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT SL_CR_ESTATMICRORGPOSITIVOS.N_REQ,SL_CR_ESTATMICRORGPOSITIVOS.NOME_UTE, SL_CR_ESTATMICRORGPOSITIVOS.COD_PERFIL, SL_CR_ESTATMICRORGPOSITIVOS.DESCR_PERFIL,"
    Report.SQLQuery = Report.SQLQuery & " SL_CR_ESTATMICRORGPOSITIVOS.DT_CHEGA, SL_CR_ESTATMICRORGPOSITIVOS.RESULTADO,SL_CR_ESTATMICRORGPOSITIVOS.contaminante, SL_CR_ESTATMICRORGPOSITIVOS.descr_gr_PERFIS, sl_cr_estatmicrorgpositivos.impropria "
    Report.SQLQuery = Report.SQLQuery & " FROM SL_CR_ESTATMICRORGPOSITIVOS SL_CR_ESTATMICRORGPOSITIVOS WHERE nome_computador =  " & BL_TrataStringParaBD(BG_SYS_GetComputerName)
    Report.SQLQuery = Report.SQLQuery & " ORDER BY SL_CR_ESTATMICRORGPOSITIVOS.descr_gr_PERFIs, SL_CR_ESTATMICRORGPOSITIVOS.COD_PERFIL, SL_CR_ESTATMICRORGPOSITIVOS.RESULTADO,SL_CR_ESTATMICRORGPOSITIVOS.contaminante,SL_CR_ESTATMICRORGPOSITIVOS.impropria, SL_CR_ESTATMICRORGPOSITIVOS.N_REQ "

    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    
    Dim StrProven As String
    For i = 0 To ListaProven.ListCount - 1
        StrProven = StrProven & ListaProven.List(i) & "; "
    Next i
    If StrProven <> "" Then
        StrProven = Mid(StrProven, 1, Len(StrProven) - 2)
        If Len(StrProven) > 250 Then StrProven = Mid(StrProven, 1, 245) & " ..."
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("" & StrProven)
    Else
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("Todas")
    End If
    
    Dim STRProduto As String
    For i = 0 To ListaProduto.ListCount - 1
        STRProduto = STRProduto & ListaProduto.List(i) & "; "
    Next i
    If STRProduto <> "" Then
        STRProduto = Mid(STRProduto, 1, Len(STRProduto) - 2)
        If Len(STRProduto) > 250 Then STRProduto = Mid(STRProduto, 1, 245) & " ..."
        Report.formulas(4) = "Produto=" & BL_TrataStringParaBD("" & STRProduto)
    Else
        Report.formulas(4) = "Produto=" & BL_TrataStringParaBD("Todos")
    End If
    
    If CbUrgencia.ListIndex = -1 Then
        Report.formulas(5) = "Urgencia=" & BL_TrataStringParaBD("-")
    Else
        Report.formulas(5) = "Urgencia=" & BL_TrataStringParaBD("" & CbUrgencia.Text)
    End If
    
    Dim strSituacao As String
    If strSituacao <> "" Then
        strSituacao = Mid(strSituacao, 1, Len(strSituacao) - 2)
        If Len(strSituacao) > 250 Then strSituacao = Mid(strSituacao, 1, 245) & " ..."
        Report.formulas(6) = "Situacao=" & BL_TrataStringParaBD("" & strSituacao)
    Else
        Report.formulas(6) = "Situacao=" & BL_TrataStringParaBD("Todos")
    End If
    
    If EcDescrSexo.ListIndex = -1 Then
        Report.formulas(7) = "Sexo=" & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(7) = "Sexo=" & BL_TrataStringParaBD("" & EcDescrSexo.Text)
    End If
    
    Dim StrMicrorg As String
    For i = 0 To ListaMicrorg.ListCount - 1
        StrMicrorg = StrMicrorg & ListaMicrorg.List(i) & "; "
    Next i
    If StrMicrorg <> "" Then
        StrMicrorg = Mid(StrMicrorg, 1, Len(StrMicrorg) - 2)
        If Len(StrMicrorg) > 250 Then StrMicrorg = Mid(StrMicrorg, 1, 245) & " ..."
        Report.formulas(8) = "Microrganismo=" & BL_TrataStringParaBD("" & StrMicrorg)
    Else
        Report.formulas(8) = "Microrganismo=" & BL_TrataStringParaBD("Todos")
    End If
    
    If Flg_DescrRequis.value = 1 Then
        Report.formulas(9) = "DescrRequis=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(9) = "DescrRequis=" & BL_TrataStringParaBD("N")
    End If
    
    If EcCodGrupo.Text <> "" Then
        Report.formulas(10) = "GrupoAnalises=" & BL_TrataStringParaBD("" & EcDescrGrupo.Text)
    Else
        Report.formulas(10) = "GrupoAnalises=" & BL_TrataStringParaBD("Todos")
    End If
    
    Dim StrExames As String
    For i = 0 To ListaPerfis.ListCount - 1
        StrExames = StrExames & ListaPerfis.List(i) & "; "
    Next i
    If StrExames <> "" Then
        StrExames = Mid(StrExames, 1, Len(StrExames) - 2)
        If Len(StrExames) > 250 Then StrExames = Mid(StrExames, 1, 245) & " ..."
        Report.formulas(11) = "Exame=" & BL_TrataStringParaBD("" & StrExames)
    Else
        Report.formulas(11) = "Exame=" & BL_TrataStringParaBD("Todos")
    End If
    Report.formulas(12) = "Utente=" & BL_TrataStringParaBD(EcUtente)
    
    If ListaGrPerfis.ListCount = 0 Then
        Report.formulas(13) = "GrupoPerfis=" & BL_TrataStringParaBD("Todos")
    Else
        GrupoPerfis = ""
        For i = 0 To ListaGrPerfis.ListCount - 1
            GrupoPerfis = GrupoPerfis & ListaGrPerfis.List(i) & "; "
        Next
        If Len(GrupoPerfis) > 200 Then
            GrupoPerfis = left(GrupoPerfis, 200) & "..."
        End If
        Report.formulas(13) = "GrupoPerfis=" & BL_TrataStringParaBD("" & GrupoPerfis)
    End If
    
    If CkContaminantes.value = 1 Then
        Report.formulas(14) = "Flg_Contaminantes=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(14) = "Flg_Contaminantes=" & BL_TrataStringParaBD("N")
    End If
    
    If CkImproprias.value = 1 Then
        Report.formulas(15) = "Flg_Improprias=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(15) = "Flg_Improprias=" & BL_TrataStringParaBD("N")
    End If
    
    Call BL_ExecutaReport
    
    sql = "DELETE FROM sl_cr_estatmicrorgpositivos WHERE nome_computador = " & BL_TrataStringParaBD(BG_SYS_GetComputerName)
    BG_ExecutaQuery_ADO sql

End Sub

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function


Private Sub BtPesquisaGrupo_Click()

'    FormPesquisaRapida.InitPesquisaRapida "sl_gr_ana", _
'                        "descr_gr_ana", "seq_gr_ana", _
'                         EcPesqRapGrupo, , "cod_local = " & gCodLocal

    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    ClausulaWhere = "cod_local = " & gCodLocal
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupo de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrupo.Text = resultados(1)
            EcDescrGrupo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesquisaMicro_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_microrg", _
'                        "descr_microrg", "seq_microrg", _
'                         EcPesqRapMicro

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_microrg"
    CamposEcran(1) = "cod_microrg"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_microrg"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_microrg"
    If EcCodGrupo.Text <> "" Then
        CWhere = " gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.Text)
    End If
    CampoPesquisa = "descr_microrg"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_microrg ", _
                                                                           " Microrganismos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaMicrorg.ListCount = 0 Then
                ListaMicrorg.AddItem BL_SelCodigo("sl_microrg", "descr_microrg", "seq_microrg", resultados(i))
                ListaMicrorg.ItemData(0) = resultados(i)
            Else
                ListaMicrorg.AddItem BL_SelCodigo("sl_microrg", "descr_microrg", "seq_microrg", resultados(i))
                ListaMicrorg.ItemData(ListaMicrorg.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub BtPesquisaPerfis_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    If ListaGrPerfis.ListCount > 0 Then
        CFrom = "sl_perfis, sl_gr_perfis "
    Else
        CFrom = "sl_perfis "
    End If
    If ListaGrPerfis.ListCount > 0 Then
        CWhere = ""
        If EcCodGrupo.Text <> "" Then
            CWhere = CWhere & " and gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.Text)
        End If
        CWhere = CWhere & " flg_activo = 1 AND sl_gr_perfis.cod_gr_perfis = sl_perfis.cod_gr_perfis "
        CWhere = CWhere & " AND sl_gr_perfis.seq_gr_perfis IN ("
        For i = 0 To ListaGrPerfis.ListCount - 1
            CWhere = CWhere & ListaGrPerfis.ItemData(i) & ", "
        Next i
        CWhere = Mid(CWhere, 1, Len(CWhere) - 2) & ") "
    Else
        If EcCodGrupo.Text <> "" Then
            CWhere = CWhere & " and gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.Text)
        End If
        CWhere = " flg_activo = 1  "
    End If
    CampoPesquisa = "descr_perfis"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_perfis ", _
                                                                           " Exames")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaPerfis.ListCount = 0 Then
                ListaPerfis.AddItem BL_SelCodigo("sl_perfis", "descr_perfis", "seq_perfis", resultados(i))
                ListaPerfis.ItemData(0) = resultados(i)
            Else
                ListaPerfis.AddItem BL_SelCodigo("sl_perfis", "descr_perfis", "seq_perfis", resultados(i))
                ListaPerfis.ItemData(ListaPerfis.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub

Private Sub BtPesquisaProduto_click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_produto"
    CWhere = ""
    CampoPesquisa = "descr_produto"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_produto ", _
                                                                           " Produtos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaProduto.ListCount = 0 Then
                ListaProduto.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                ListaProduto.ItemData(0) = resultados(i)
            Else
                ListaProduto.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                ListaProduto.ItemData(ListaProduto.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub

Private Sub BtPesquisaProveniencia_Click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaProven.ListCount = 0 Then
                ListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                ListaProven.ItemData(0) = resultados(i)
            Else
                ListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                ListaProven.ItemData(ListaProven.NewIndex) = resultados(i)
            End If
        Next i
        

    End If
        
End Sub

Private Sub BtPesquisaSituacao_Click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_t_sit"
    CamposEcran(1) = "cod_t_sit"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_t_sit"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_tbf_t_sit"
    CWhere = ""
    CampoPesquisa = "descr_t_sit"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_t_sit ", _
                                                                           " Situa��o")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaSituacao.ListCount = 0 Then
                ListaSituacao.AddItem BL_SelCodigo("sl_tbf_t_sit", "descr_t_sit", "cod_t_sit", resultados(i))
                ListaSituacao.ItemData(0) = resultados(i)
            Else
                ListaSituacao.AddItem BL_SelCodigo("sl_tbf_t_sit", "descr_t_sit", "cod_t_sit", resultados(i))
                ListaSituacao.ItemData(ListaSituacao.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub



Private Sub BtReportLista_Click()
    Call Preenche_Estatistica("Lista")
End Sub

Private Sub CbUrgencia_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1
    
End Sub

Private Sub EcCodGrupo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrupo.Text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_gr_ana, descr_gr_ana FROM sl_gr_ana WHERE upper(cod_gr_ana)= " & BL_TrataStringParaBD(UCase(EcCodGrupo.Text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodGrupo.Text = "" & RsDescrGrupo!cod_gr_ana
            EcDescrGrupo.Text = "" & RsDescrGrupo!descr_gr_ana
        Else
            Cancel = True
            EcDescrGrupo.Text = ""
            BG_Mensagem mediMsgBox, "O grupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrGrupo.Text = ""
    End If
End Sub

Private Sub EcDescrSexo_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDescrSexo_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then EcDescrSexo.ListIndex = -1
    
End Sub

Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub





Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Estat�stica de Positivos - Microbiologia"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 7950
    Me.Height = 7740 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtIni
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
'    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
'    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de Positivos - Microbiologia")
    
    Set FormEstatMicroPositivos = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    'CbAnalises.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    EcDescrSexo.ListIndex = mediComboValorNull
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    Flg_DescrRequis.value = 0
    ListaProduto.Clear
    ListaProven.Clear
    ListaMicrorg.Clear
    ListaPerfis.Clear
    ListaSituacao.Clear
    Option1(0).value = True
End Sub

Sub DefTipoCampos()


    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
'    EcNumFolhaTrab.MaxLength = 10
    Option1(0).value = True
        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica("Lista")
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica("Lista")

End Sub

Private Function PreencheTabelaTemporariaNEW() As Long
    Dim sSql As String
    Dim rsSel As New ADODB.recordset
    Dim rsMicro As New ADODB.recordset
    Dim resultado As String
    Dim contaminante As Integer
    Dim improprias As Integer
    
    sSql = "DELETE FROM SL_CR_ESTATMICRORGPOSITIVOS WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    sSql = sSql & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql

    ReDim EstrutUte(0)
    TotalUte = 0
    ' -----------------------------------------------
    ' SELECCIONA E PREENCHE AS ESTRUTURAS
    ' -----------------------------------------------
    sSql = RetornaQuery
    rsSel.CursorLocation = adUseServer
    rsSel.CursorType = adOpenStatic
    rsSel.Open sSql, gConexao
    If rsSel.RecordCount > 0 Then
        While Not rsSel.EOF
            sSql = RetornaQueryEXAME(rsSel!n_req, rsSel!Cod_Perfil)
            rsMicro.CursorLocation = adUseServer
            rsMicro.CursorType = adOpenStatic
            rsMicro.Open sSql, gConexao
            If rsMicro.RecordCount > 0 Then
                If rsMicro!conta > 0 Then
                    resultado = "1"
                    contaminante = VerificaContaminante(rsSel!n_req)
                    improprias = 0
                Else
                    resultado = "0"
                    contaminante = 0
                    improprias = VerificaImproprias(rsSel!n_req)
                End If
                rsMicro.Close
            End If
            sSql = "INSERT INTO SL_CR_ESTATMICRORGPOSITIVOS ( nome_computador, num_sessao, nome_ute, n_req, cod_perfil,"
            sSql = sSql & " descr_perfil, dt_chega, resultado, contaminante, impropria, cod_gr_perfis, descr_gr_perfis, utente) VALUES( "
            sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
            sSql = sSql & gNumeroSessao & " ,"
            sSql = sSql & BL_TrataStringParaBD(rsSel!nome_ute) & ", "
            sSql = sSql & rsSel!n_req & ", "
            sSql = sSql & BL_TrataStringParaBD(rsSel!Cod_Perfil) & ", "
            sSql = sSql & BL_TrataStringParaBD(rsSel!descr_perfis) & ", "
            sSql = sSql & BL_TrataDataParaBD(rsSel!dt_chega) & ", "
            sSql = sSql & BL_TrataStringParaBD(resultado) & ","
            sSql = sSql & contaminante & ","
            sSql = sSql & improprias & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSel!cod_gr_perfis, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSel!descr_gr_perfis, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSel!Utente, "")) & ") "
            BG_ExecutaQuery_ADO sSql
            rsSel.MoveNext
        Wend
    End If
    rsSel.Close
    Set rsSel = Nothing

End Function

Private Sub ListaMicrorg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaMicrorg.ListCount > 0 Then     'Delete
        ListaMicrorg.RemoveItem (ListaMicrorg.ListIndex)
    End If
End Sub
Private Sub Listaperfis_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaPerfis.ListCount > 0 Then     'Delete
        ListaPerfis.RemoveItem (ListaPerfis.ListIndex)
    End If
End Sub
Private Sub ListaProduto_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaProduto.ListCount > 0 Then     'Delete
        ListaProduto.RemoveItem (ListaProduto.ListIndex)
    End If
End Sub

Private Sub ListaProven_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaProven.ListCount > 0 Then     'Delete
        ListaProven.RemoveItem (ListaProven.ListIndex)
    End If
End Sub


' -----------------------------------------------------------------------------------

' RETORNA O SELECT INICIAL PARA SABER OS MICRORGANISMOS EXISTENTES NO INTERVALO DATAS

' -----------------------------------------------------------------------------------
Private Function RetornaQuery() As String
    Dim sSql As String
    Dim i As Long
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
    
    sSql = "SELECT  DISTINCT sl_requis.n_req, " & tabela_aux & ".utente, nome_ute, sl_requis.dt_chega, "
    sSql = sSql & " sl_requis.seq_utente, descr_proven, " & tabela_aux & ".utente, sl_realiza.cod_perfil, sl_perfis.descr_perfis, "
    sSql = sSql & " sl_gr_perfis.cod_gr_perfis, sl_gr_perfis.descr_gr_perfis "
    
    If gSGBD = gOracle Then
        sSql = sSql & " FROM " & tabela_aux & ", sl_requis,sl_produto,sl_proven, sl_realiza, sl_perfis, sl_gr_perfis "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " FROM " & tabela_aux & ", sl_requis  "
        sSql = sSql & " LEFT OUTER JOIN sl_proven ON sl_requis.cod_proven = sl_proven.cod_proven,  "
        sSql = sSql & " sl_realiza, sl_perfis "
        sSql = sSql & " LEFT OUTER JOIN sl_produto ON sl_perfis.cod_produto = sl_produto.cod_produto "
        sSql = sSql & " LEFT OUTER JOIN sl_gr_perfis ON sl_perfis.cod_gr_perfis = sl_gr_perfis.cod_gr_perfis "
    End If
        
    sSql = sSql & " WHERE "
    sSql = sSql & " " & tabela_aux & ".seq_utente = sl_requis.seq_utente AND sl_requis.n_req = sl_realiza.n_req AND "
    If gSGBD = gOracle Then
        sSql = sSql & " sl_perfis.cod_produto = sl_produto.cod_produto(+) and "
        sSql = sSql & " sl_perfis.cod_gr_perfis = sl_gr_perfis.cod_gr_perfis(+) and "
        sSql = sSql & " sl_requis.cod_proven = sl_proven.cod_proven (+) AND "
    End If
    sSql = sSql & " sl_Realiza.cod_perfil= sl_perfis.cod_perfis and  sl_realiza.dt_val is not null "
    sSql = sSql & " AND sl_requis.n_req NOT IN (SELECT n_req FROM sl_marcacoes) "
    sSql = sSql & " AND sl_gr_perfis.flg_estat (+) = 1 "
    
    If Trim(EcUtente) <> "" Then
        sSql = sSql & " AND " & tabela_aux & ".utente = " & BL_TrataStringParaBD(EcUtente)
    End If
    
    'se GRUPO DE perfis preenchidos
    If ListaGrPerfis.ListCount > 0 Then
        sSql = sSql & " AND sl_gr_perfis.cod_gr_perfis = sl_perfis.cod_gr_perfis AND sl_gr_perfis.seq_gr_perfis IN ( "
        For i = 0 To ListaGrPerfis.ListCount - 1
            sSql = sSql & ListaGrPerfis.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    'se perfis preenchidos
    If ListaPerfis.ListCount > 0 Then
        sSql = sSql & " AND sl_realiza.cod_perfil = sl_perfis.cod_perfis AND sl_perfis.seq_perfis IN ( "
        For i = 0 To ListaPerfis.ListCount - 1
            sSql = sSql & ListaPerfis.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    'se sexo preenchido
    If (EcDescrSexo.ListIndex <> -1) Then
        sSql = sSql & " AND " & tabela_aux & ".sexo_ute = " & EcDescrSexo.ListIndex
    End If
    
    'Data preenchida
    If Option1(0).value = True Then
        sSql = sSql & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    ElseIf Option1(1).value = True Then
        sSql = sSql & " AND sl_realiza.dt_val BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    End If
    
    sSql = sSql & " AND sl_realiza.flg_estado in(" & gEstadoAnaImpressa & "," & gEstadoAnaValidacaoMedica & ")"
    
    'Situa��o preenchida?
    If ListaSituacao.ListCount > 0 Then
        sSql = sSql & " AND sl_requis.T_sit IN ("
        For i = 0 To ListaSituacao.ListCount - 1
            sSql = sSql & ListaSituacao.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> -1) Then
        sSql = sSql & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(BG_DaComboSel(CbUrgencia))
    End If
    
    
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    Dim StrProven As String
    StrProven = ""
    For i = 0 To ListaProven.ListCount - 1
        StrProven = StrProven & BL_TrataStringParaBD(BL_SelCodigo("sl_proven", "cod_proven", "seq_proven", ListaProven.ItemData(i))) & ","
    Next i
    If StrProven <> "" Then
        'Remove a virgula do final da express�o
        StrProven = Mid(StrProven, 1, Len(StrProven) - 1)
        sSql = sSql & " AND sl_requis.cod_proven in ( " & StrProven & " )"
    End If
    
    'C�digo do produto preenchido?
    Dim STRProduto As String
    STRProduto = ""
    For i = 0 To ListaProduto.ListCount - 1
        STRProduto = STRProduto & BL_TrataStringParaBD(BL_SelCodigo("sl_produto", "cod_produto", "seq_produto", ListaProduto.ItemData(i))) & ","
    Next i
    If STRProduto <> "" Then
        'Remove a virgula do final da express�o
        STRProduto = Mid(STRProduto, 1, Len(STRProduto) - 1)
        sSql = sSql & " AND sl_perfis.cod_produto in ( " & STRProduto & " )"
    End If
    
    
    'Se Grupo preenchido
    If EcCodGrupo.Text <> "" Then
        sSql = sSql & " and sl_perfis.gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.Text)
    End If
    If gSGBD = gOracle Then
        RetornaQuery = sSql & " ORDER BY seq_utente, dt_chega, n_req "
    ElseIf gSGBD = gSqlServer Then
        RetornaQuery = sSql & " ORDER BY sl_Requis.seq_utente, sl_requis.dt_chega, sl_requis.n_req "
    End If
End Function




' -----------------------------------------------------------------------------------

' RETORNA O SELECT INICIAL PARA SABER OS MICRORGANISMOS EXISTENTES NO INTERVALO DATAS

' -----------------------------------------------------------------------------------
Private Function RetornaQueryEXAME(n_req As String, Cod_Perfil As String) As String
    Dim sSql As String
    Dim i As Long
    
    sSql = "SELECT  count(*) conta "
    sSql = sSql & " FROM sl_realiza, sl_Res_micro "
    sSql = sSql & " WHERE "
    sSql = sSql & " sl_realiza.n_req = " & BL_TrataStringParaBD(n_req)
    sSql = sSql & " AND sl_realiza.cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil)
    sSql = sSql & " AND sl_realiza.seq_realiza =  sl_res_micro.seq_realiza "
    
    'C�digo do microrganismo preenchido?
    Dim StrMicrorg As String
    StrMicrorg = ""
    For i = 0 To ListaMicrorg.ListCount - 1
        StrMicrorg = StrMicrorg & BL_TrataStringParaBD(BL_SelCodigo("sl_microrg", "cod_microrg", "seq_microrg", ListaMicrorg.ItemData(i))) & ","
    Next i
    If StrMicrorg <> "" Then
        'Remove a virgula do final da express�o
        StrMicrorg = Mid(StrMicrorg, 1, Len(StrMicrorg) - 1)
        sSql = sSql & " AND sl_res_micro.cod_micro in ( " & StrMicrorg & " )"
    End If
    
    
    RetornaQueryEXAME = sSql
End Function


' -----------------------------------------------------------------------------------

' VERIFICA SE REQUISICAO, QUE TEM MICRO, � CONTAMINATE, SE TEM FRASES ASSOCIADAS A
' CONTAMINANTE
    
' -----------------------------------------------------------------------------------
Private Function VerificaContaminante(n_req As String) As Integer
    Dim sSql As String
    Dim rsCont As New ADODB.recordset
    sSql = "SELECT  count(*) conta "
    sSql = sSql & " FROM sl_realiza, sl_res_frase, sl_dicionario "
    sSql = sSql & " WHERE "
    sSql = sSql & " sl_realiza.n_req = " & BL_TrataStringParaBD(n_req)
    sSql = sSql & " AND sl_realiza.seq_realiza =  sl_res_frase.seq_realiza "
    sSql = sSql & " AND sl_res_frase.cod_frase  =  sl_dicionario.cod_frase "
    sSql = sSql & " AND sl_dicionario.flg_contaminante = 1 "
    
    rsCont.CursorLocation = adUseServer
    rsCont.CursorType = adOpenStatic
    rsCont.Open sSql, gConexao
    If rsCont.RecordCount > 0 Then
        If rsCont!conta > 0 Then
            VerificaContaminante = 1
        Else
            VerificaContaminante = 0
        End If
    Else
        VerificaContaminante = 0
    End If
    rsCont.Close
    Set rsCont = Nothing
End Function

' -----------------------------------------------------------------------------------

' VERIFICA SE REQUISICAO, QUE TEM MICRO, � IMPROPRIA, SE TEM FRASES ASSOCIADAS A
' IMPROPRIA
    
' -----------------------------------------------------------------------------------
Private Function VerificaImproprias(n_req As String) As Integer
    Dim sSql As String
    Dim rsCont As New ADODB.recordset
    sSql = "SELECT  count(*) conta "
    sSql = sSql & " FROM sl_realiza, sl_res_frase, sl_dicionario "
    sSql = sSql & " WHERE "
    sSql = sSql & " sl_realiza.n_req = " & BL_TrataStringParaBD(n_req)
    sSql = sSql & " AND sl_realiza.seq_realiza =  sl_res_frase.seq_realiza "
    sSql = sSql & " AND sl_res_frase.cod_frase  =  sl_dicionario.cod_frase "
    sSql = sSql & " AND sl_dicionario.flg_impropria = 1 "
    
    rsCont.CursorLocation = adUseServer
    rsCont.CursorType = adOpenStatic
    rsCont.Open sSql, gConexao
    If rsCont.RecordCount > 0 Then
        If rsCont!conta > 0 Then
            VerificaImproprias = 1
        Else
            VerificaImproprias = 0
        End If
    Else
        VerificaImproprias = 0
    End If
    rsCont.Close
    Set rsCont = Nothing
End Function


Private Sub BtPesquisaGrPerfis_click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_gr_perfis"
    CamposEcran(1) = "cod_gr_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_gr_perfis"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_gr_perfis "
    CWhere = ""
    CampoPesquisa = "descr_gr_perfis"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_gr_perfis ", _
                                                                           " Exames")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaGrPerfis.ListCount = 0 Then
                ListaGrPerfis.AddItem BL_SelCodigo("sl_gr_perfis", "descr_gr_perfis", "seq_gr_perfis", resultados(i))
                ListaGrPerfis.ItemData(0) = resultados(i)
            Else
                ListaGrPerfis.AddItem BL_SelCodigo("sl_gr_perfis", "descr_gr_perfis", "seq_gr_perfis", resultados(i))
                ListaGrPerfis.ItemData(ListaGrPerfis.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub
Private Sub ListaGrPerfis_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaGrPerfis.ListCount > 0 Then     'Delete
        ListaGrPerfis.RemoveItem (ListaGrPerfis.ListIndex)
    End If
End Sub


Private Sub ListaSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaSituacao.ListCount > 0 Then     'Delete
        ListaSituacao.RemoveItem (ListaSituacao.ListIndex)
    End If
End Sub


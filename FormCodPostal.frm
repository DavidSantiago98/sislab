VERSION 5.00
Begin VB.Form FormCodPostal 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCodPostal"
   ClientHeight    =   6615
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8085
   Icon            =   "FormCodPostal.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6615
   ScaleWidth      =   8085
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodSinave 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   6720
      TabIndex        =   29
      Top             =   480
      Width           =   975
   End
   Begin VB.ComboBox EcDistritoCombo 
      Height          =   315
      Left            =   2520
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   840
      Width           =   2805
   End
   Begin VB.TextBox EcAreaGeo 
      Height          =   315
      Left            =   1320
      TabIndex        =   5
      Top             =   1200
      Width           =   6435
   End
   Begin VB.TextBox EcCodDistrito 
      Height          =   315
      Left            =   1320
      TabIndex        =   3
      Top             =   840
      Width           =   1185
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   1320
      TabIndex        =   1
      Top             =   120
      Width           =   1215
   End
   Begin VB.TextBox EcDescricao 
      Height          =   285
      Left            =   1320
      TabIndex        =   2
      Top             =   480
      Width           =   4035
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   7320
      TabIndex        =   17
      Top             =   5040
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ListBox EcLista 
      Height          =   2010
      Left            =   180
      TabIndex        =   6
      Top             =   1920
      Width           =   7605
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   200
      TabIndex        =   10
      Top             =   4080
      Width           =   7605
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   16
         Top             =   480
         Width           =   2175
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   15
         Top             =   195
         Width           =   2175
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   14
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   13
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5040
      TabIndex        =   9
      Top             =   5040
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2280
      TabIndex        =   8
      Top             =   5040
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5040
      TabIndex        =   7
      Top             =   5400
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2280
      TabIndex        =   0
      Top             =   5520
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.Label Label13 
      Caption         =   "Cod. SINAVE"
      Height          =   255
      Index           =   3
      Left            =   5640
      TabIndex        =   30
      Top             =   480
      Width           =   1575
   End
   Begin VB.Label Label13 
      Caption         =   "EcDataAlteracao"
      Height          =   375
      Index           =   0
      Left            =   3600
      TabIndex        =   28
      Top             =   5400
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataCriacao"
      Height          =   375
      Left            =   3720
      TabIndex        =   27
      Top             =   5040
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   375
      Left            =   600
      TabIndex        =   26
      Top             =   5520
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   375
      Left            =   720
      TabIndex        =   25
      Top             =   5040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   1680
      TabIndex        =   24
      Top             =   1680
      Width           =   1065
   End
   Begin VB.Label Label7 
      Caption         =   "C�digo Postal "
      Height          =   255
      Left            =   240
      TabIndex        =   23
      Top             =   1680
      Width           =   1335
   End
   Begin VB.Label Label4 
      Caption         =   "Localidade"
      Height          =   285
      Left            =   240
      TabIndex        =   22
      Top             =   1200
      Width           =   1245
   End
   Begin VB.Label Label2 
      Caption         =   "Distrito"
      Height          =   285
      Left            =   240
      TabIndex        =   21
      Top             =   840
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   240
      TabIndex        =   20
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   240
      TabIndex        =   19
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   465
      Left            =   5760
      TabIndex        =   18
      Top             =   5040
      Visible         =   0   'False
      Width           =   1485
   End
End
Attribute VB_Name = "FormCodPostal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 24/04/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos

Public rs As ADODB.recordset

Private Sub EcCoddistrito_LostFocus()
    
    Dim SQLQuery As String

    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodDistrito.Text = UCase(EcCodDistrito.Text)
    BL_ColocaTextoCombo "sl_distritos", "seq_distrito", "cod_distrito", EcCodDistrito, EcDistritoCombo

End Sub

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
        BL_ColocaTextoCombo "sl_distritos", "seq_distrito", "cod_distrito", EcCodDistrito, EcDistritoCombo
    End If

End Sub

Private Sub EcCodigo_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodigo_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)

End Sub

Private Sub EcDescricao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDescricao_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Private Sub EcdistritoCombo_Click()
    
    BL_ColocaComboTexto "sl_distritos", "seq_distrito", "cod_distrito", EcCodDistrito, EcDistritoCombo
    
End Sub

Private Sub EcdistritoCombo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcDistritoCombo.ListIndex = -1

End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " C�digos Postais"
    Me.left = 540
    Me.top = 450
    Me.Width = 8085
    Me.Height = 5500 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_cod_postal"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 10
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_postal"
    CamposBD(1) = "cod_postal"
    CamposBD(2) = "descr_postal"
    CamposBD(3) = "cod_pais"
    CamposBD(4) = "area_geografica"
    CamposBD(5) = "user_cri"
    CamposBD(6) = "dt_cri"
    CamposBD(7) = "user_act"
    CamposBD(8) = "dt_act"
    'BRUNODSANTOS 22.02.2017 - Glintt-HS-14510
    CamposBD(9) = "cod_sinave"
    '
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcCodDistrito
    Set CamposEc(4) = EcAreaGeo
    Set CamposEc(5) = EcUtilizadorCriacao
    Set CamposEc(6) = EcDataCriacao
    Set CamposEc(7) = EcUtilizadorAlteracao
    Set CamposEc(8) = EcDataAlteracao
    'BRUNODSANTOS 22.02.2017 - Glintt-HS-14510
    Set CamposEc(9) = EcCodSinave
    '
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo Postal"
    TextoCamposObrigatorios(2) = "Descri��o do C�digo"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_postal"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_postal", "descr_postal")
    'CamposBDparaListBox = Array("cod_postal", "area_geografica")
    NumEspacos = Array(13, 100)

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormCodPostal = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    EcDistritoCombo.ListIndex = mediComboValorNull
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_distritos", "seq_distrito", "descr_distrito", EcDistritoCombo, mediAscComboDesignacao
    
'    EcCodDistrito.Text = "15"
    BL_ColocaTextoCombo "sl_distritos", "seq_distrito", "cod_distrito", EcCodDistrito, EcDistritoCombo
    DoEvents

End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BG_MostraComboSel EcCodDistrito, EcDistritoCombo
        
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
    End If
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    Dim str_aux1 As String
    Dim str_aux2 As String
    Dim str_aux3 As String
    
    If ((Len(Trim(EcCodigo.Text)) = 0) And _
        (Len(Trim(EcDescricao.Text)) = 0) And _
        (Len(Trim(EcCodDistrito.Text)) = 0) And _
        (Len(Trim(EcAreaGeo.Text)) = 0)) Then
        
        MsgBox "� necess�rio colocar um crit�rio de pesquisa.        ", vbExclamation
        Exit Sub
        
    End If
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
           
    str_aux1 = EcDescricao.Text
    str_aux2 = EcAreaGeo.Text
    str_aux3 = EcCodigo.Text
    If EcDescricao.Text <> "" Then
        EcDescricao.Text = "*" & UCase(EcDescricao.Text) & "*"
    End If
    If EcAreaGeo.Text <> "" Then
        EcAreaGeo.Text = "*" & UCase(EcAreaGeo) & "*"
    End If
    If EcCodigo.Text <> "" Then
        EcCodigo.Text = "*" & UCase(EcCodigo) & "*"
    End If
    DoEvents
    CamposBD(2) = "UPPER(descr_postal)"
    CamposBD(4) = "UPPER(area_geografica)"
    CamposBD(1) = "UPPER(cod_postal)"
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY UPPER(descr_postal) ASC, cod_postal ASC "
    Else
    End If
    
    CamposBD(2) = "descr_postal"
    CamposBD(4) = "area_geografica"
    CamposBD(1) = "cod_postal"
    EcDescricao.Text = str_aux1
    EcAreaGeo.Text = str_aux2
    EcCodigo.Text = str_aux3
    DoEvents
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        
        BG_PreencheListBoxMultipla_ADO EcLista, _
                                       rs, _
                                       CamposBDparaListBox, _
                                       NumEspacos, _
                                       CamposBD, _
                                       CamposEc, _
                                       "SELECT"
        
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_postal") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal

End Sub

Sub FuncaoRemover()
    
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE seq_postal=" & EcCodSequencial
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
    
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub






VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form FormEstTempoMediaAna 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEstTempoMediaAna"
   ClientHeight    =   5100
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8280
   Icon            =   "FormEstTempoMediaAna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5100
   ScaleWidth      =   8280
   ShowInTaskbar   =   0   'False
   Begin MSFlexGridLib.MSFlexGrid FgUltrap 
      Height          =   2535
      Left            =   120
      TabIndex        =   27
      Top             =   2520
      Width           =   8055
      _ExtentX        =   14208
      _ExtentY        =   4471
      _Version        =   393216
      ForeColor       =   0
      BackColorSel    =   -2147483643
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
      Appearance      =   0
   End
   Begin VB.Frame Frame3 
      Height          =   1455
      Left            =   120
      TabIndex        =   2
      Top             =   0
      Width           =   8055
      Begin VB.CheckBox CkApenasUrg 
         Caption         =   "Apenas Urgentes"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   1080
         Width           =   1935
      End
      Begin VB.ComboBox CbGrupo 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   3960
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   240
         Width           =   3495
      End
      Begin VB.TextBox EcCodAnaS 
         Height          =   315
         Left            =   3960
         TabIndex        =   8
         Top             =   960
         Width           =   735
      End
      Begin VB.TextBox EcDescrAnaS 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   4680
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   960
         Width           =   2775
      End
      Begin VB.CommandButton BtPesqRapS 
         Height          =   315
         Left            =   7440
         Picture         =   "FormEstTempoMediaAna.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de An�lises "
         Top             =   960
         Width           =   375
      End
      Begin VB.ComboBox CbProveniencia 
         Height          =   315
         Left            =   3960
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   600
         Width           =   3495
      End
      Begin VB.TextBox EcDataInicial 
         Height          =   285
         Left            =   1080
         TabIndex        =   0
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcDataFinal 
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label LaGrAnalises 
         Caption         =   "Grupo de An�lises"
         Height          =   255
         Left            =   2640
         TabIndex        =   12
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label LaAnalises 
         Caption         =   "An�lise Simples"
         Height          =   255
         Left            =   2640
         TabIndex        =   11
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "Proveni�ncia"
         Height          =   255
         Index           =   0
         Left            =   2640
         TabIndex        =   10
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label LaDataFinal 
         Caption         =   "Data Final"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   600
         Width           =   855
      End
      Begin VB.Label LaDataInicial 
         Caption         =   "Data Inicial"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
   End
   Begin Crystal.CrystalReport CrDadivas 
      Left            =   7680
      Top             =   120
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label LaTempoMax 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6720
      TabIndex        =   25
      Top             =   2160
      Width           =   1335
   End
   Begin VB.Label LaReqMax 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6720
      TabIndex        =   24
      Top             =   1920
      Width           =   1335
   End
   Begin VB.Label LaTempoMed 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3840
      TabIndex        =   23
      Top             =   1920
      Width           =   1215
   End
   Begin VB.Label LaTempoMin 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1320
      TabIndex        =   22
      Top             =   2160
      Width           =   1455
   End
   Begin VB.Label LaReqMin 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1320
      TabIndex        =   21
      Top             =   1920
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Tempo"
      Height          =   255
      Index           =   8
      Left            =   5760
      TabIndex        =   20
      Top             =   2160
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Requisi��o"
      Height          =   255
      Index           =   7
      Left            =   5760
      TabIndex        =   19
      Top             =   1920
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "M�ximo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   5400
      TabIndex        =   18
      Top             =   1560
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Tempo"
      Height          =   255
      Index           =   5
      Left            =   3120
      TabIndex        =   17
      Top             =   1920
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "M�dia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   2880
      TabIndex        =   16
      Top             =   1560
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Tempo"
      Height          =   255
      Index           =   3
      Left            =   360
      TabIndex        =   15
      Top             =   2160
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Requisi��o"
      Height          =   255
      Index           =   2
      Left            =   360
      TabIndex        =   14
      Top             =   1920
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Minimo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   13
      Top             =   1560
      Width           =   615
   End
End
Attribute VB_Name = "FormEstTempoMediaAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim CriterioBase As String 'usado para os botoes de ordenacao
Dim CriterioAnterior As String 'usado para os botoes de ordenacao

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Private Type DadosTempoAna
    n_req As String
    tempo As String          ' em minutos
    analise As String
    flg_Ultrapassa As Boolean
    tempo_ultrapassa As String
    tempo_previsto As String
End Type

Dim estrutAna() As DadosTempoAna
Dim TamEstrutAna As Long

Public obTabela As ADODB.recordset

Dim Flg_DataInicialValida As Boolean
Dim Flg_DataFinalValida As Boolean

Dim tempoMin As String
Dim tempoMax As String
Dim tempo As String

Dim requisMinimo As String
Dim requisMaximo As String

Dim totalReq As Long


Private Sub BtPesqRapS_Click()
    
    Dim ChavesPesq(1 To 3) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 3) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_s"
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    ChavesPesq(3) = "descr_gr_ana"
    CamposEcran(3) = "descr_gr_ana"
    Tamanhos(3) = 2000
    Headers(3) = "Grupo de An�lises"
    
    CamposRetorno.InicializaResultados 3
    
    CFrom = "sl_ana_s, sl_gr_ana "
    CWhere = "sl_ana_s.gr_ana = sl_gr_ana.cod_gr_ana "
    If CbGrupo.ListIndex <> mediComboValorNull Then
        'CWhere = CWhere & "and seq_gr_ana=" & CbGrupo.ItemData(CbGrupo.ListIndex) & " and t_result = 0"
        CWhere = CWhere & "and seq_gr_ana=" & CbGrupo.ItemData(CbGrupo.ListIndex) & ""
    Else
        'CWhere = CWhere & "and t_result = 0"
        'CWhere = ""
    End If
    CampoPesquisa = "descr_ana_s"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Simples")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaS.Text = Resultados(2)
            EcCodAnaS.Text = Resultados(1)
            CbGrupo.Text = Resultados(3)
            EcCodAnaS.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises simples", vbExclamation, "ATEN��O"
        EcCodAnaS.SetFocus
    End If
End Sub

Private Sub CbGrupo_KeyDown(KeyCode As Integer, Shift As Integer)

    BG_LimpaOpcao CbGrupo, KeyCode

End Sub

Private Sub EcDataFinal_Validate(Cancel As Boolean)
    
    Flg_DataFinalValida = False
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataFinal)
    Flg_DataFinalValida = Not Cancel
    If EcDataFinal = "" Then
        Flg_DataFinalValida = False
    ElseIf Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub EcDataInicial_Validate(Cancel As Boolean)
    
    Flg_DataInicialValida = False
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataInicial)
    Flg_DataInicialValida = Not Cancel
    If EcDataInicial = "" Then
        Flg_DataInicialValida = False
    ElseIf Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If

    
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = "Tempo M�dio Realiza��o de An�lises"
    Me.left = 500
    Me.top = 400
    Me.Width = 8460
    Me.Height = 5640
    
    TamEstrutAna = 0
    ReDim estrutAna(TamEstrutAna)
    
    Set CampoDeFocus = EcDataInicial
    
    Flg_DataInicialValida = False
    Flg_DataFinalValida = False
    
    CkApenasUrg.value = vbUnchecked
End Sub
Sub EventoLoad()
    Inicializacoes
    
    '
    'BT_DefineIdioma

    DefTipoCampos
    PreencheValoresDefeito
    
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not obTabela Is Nothing Then
        obTabela.Close
        Set obTabela = Nothing
    End If
    
    Set FormEstTempoMediaAna = Nothing
    
End Sub
Private Sub LimpaCamposProcura()
    totalReq = 0
    tempo = ""
    tempoMax = ""
    tempoMin = ""
    requisMaximo = ""
    requisMinimo = ""
    
    LaReqMax = ""
    LaReqMin = ""
    LaTempoMax = ""
    LaTempoMin = ""
    LaTempoMed = ""
        
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    CampoDeFocus.SetFocus
    TamEstrutAna = 0
    ReDim estrutAna(TamEstrutAna)
    
    LimpaFgUltrap

End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    EcDataInicial.Text = ""
    EcDataFinal.Text = ""
    CbGrupo.ListIndex = mediComboValorNull
    EcCodAnaS.Text = ""
    EcDescrAnaS.Text = ""
    Flg_DataInicialValida = False
    Flg_DataFinalValida = False
    CkApenasUrg.value = vbUnchecked
    LimpaCamposProcura
    CbProveniencia.ListIndex = mediComboValorNull
End Sub

Sub DefTipoCampos()

    EcDataInicial.MaxLength = 10
    EcDataFinal.MaxLength = 10

    EcDataInicial.Tag = adDate
    EcDataFinal.Tag = adDate
    With FgUltrap
        .rows = 2
        .FixedRows = 1
        .Cols = 5
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridRaised
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 1000
        .Col = 0
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 0) = "Req."
        .ColWidth(1) = 2200
        .Col = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 1) = "An�lise"
        .ColWidth(2) = 1500
        .Col = 2
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 2) = "Tempo"
        .ColWidth(3) = 1500
        .Col = 3
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 3) = "Tempo M�x."
        .ColWidth(4) = 1500
        .Col = 4
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 4) = "Tempo Excesso"
        
        .row = 1
        .Col = 0
    End With
 
End Sub

Sub PreencheCampos()
    'nada
End Sub

Sub FuncaoLimpar()
    LimpaCampos
End Sub

Sub FuncaoEstadoAnterior()
    'nada
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
    
    LimpaCamposProcura
    
    If EcDataInicial.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial.", vbOKOnly + vbExclamation, App.ProductName)
        EcDataInicial.SetFocus
        Exit Sub
    End If
    If EcDataFinal.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final.", vbOKOnly + vbExclamation, App.ProductName)
        EcDataFinal.SetFocus
        Exit Sub
    End If
    
    PreencheEstrutura
    
    If totalReq > 0 Then
        PreencheFgUltrap
        
        LaReqMax = requisMaximo
        LaReqMin = requisMinimo
        LaTempoMax = tempoMax
        LaTempoMin = tempoMin
        LaTempoMed = tempo
        
        
        BL_Toolbar_BotaoEstado "Imprimir", "Activo"
        BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    Else
        BG_Mensagem mediMsgBox, "N�o existem registos para abrir a estat�stica!", vbInformation, "Estat�stica de Tempo M�dio de Realiza��o de An�lises"
        BL_FimProcessamento Me
        Exit Sub
    End If
    
End Sub

Sub FuncaoAnterior()
    'nada
End Sub

Sub FuncaoSeguinte()
    'nada
End Sub

Sub FuncaoInserir()
    'nada
End Sub

Sub BD_Insert()
    'nada
End Sub

Sub FuncaoModificar()
    'nada
End Sub

Sub BD_Update()
    'nada
End Sub

Sub FuncaoRemover()
    'nada
End Sub

Sub BD_Delete()
    'nada
End Sub

Sub Funcao_DataActual()
    If Me.ActiveControl.Name = "EcDataInicial" Then
        BL_PreencheData EcDataInicial, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataFinal" Then
        BL_PreencheData EcDataFinal, Me.ActiveControl
    End If
End Sub

Sub FuncaoImprimir()

    Dim sql As String
    Dim continua As Boolean
    Dim i As Integer
    Dim MediaGrupo As String
    '1.Verifica se a Form Preview j� estava aberta!!
    
    On Error GoTo TrataErro
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    
    If BL_PreviewAberto("Estatistica Tempo M�dia Analise") = True Then Exit Sub
        
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("TempoMediaRequisicao", "Estatistica Tempo M�dia Por Requisicao", crptToPrinter)
    Else
        continua = BL_IniciaReport("TempoMediaRequisicao", "Estatistica Tempo M�dia Por Requisicao", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    
    
    
    Call Cria_TmpRec_Estatistica
    Call PreencheTabelaTemporaria
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    Report.SQLQuery = "SELECT " & _
                      "     SL_CR_TEMPOS.N_REQ, " & _
                      "     SL_CR_TEMPOS.TEMPO, " & _
                      "FROM " & _
                      "     SL_CR_TEMPOS" & gNumeroSessao & " SL_CR_TEMPOS " & _
                      "ORDER BY " & _
                      "     N_REQ "
    Report.formulas(0) = "DataInicial=" & BL_TrataDataParaBD(EcDataInicial.Text)
    Report.formulas(1) = "DataFinal=" & BL_TrataDataParaBD(EcDataFinal.Text)
    Report.formulas(2) = "TempoMedio=" & BL_TrataStringParaBD(CStr(tempo))
    Report.formulas(3) = "TempoMinimo=" & BL_TrataStringParaBD(CStr(tempoMin))
    Report.formulas(4) = "TempoMaximo=" & BL_TrataStringParaBD(CStr(tempoMax))
    Report.formulas(5) = "RequisMinimo=" & BL_TrataStringParaBD(CStr(requisMinimo))
    Report.formulas(6) = "RequisMaximo=" & BL_TrataStringParaBD(CStr(requisMaximo))
    Report.formulas(7) = "Grupo=" & BL_TrataStringParaBD(CbGrupo.List(CbGrupo.ListIndex))
    Report.formulas(8) = "Analise=" & BL_TrataStringParaBD(EcDescrAnaS.Text)
    Report.formulas(9) = "TotalReq=" & BL_TrataStringParaBD(CStr(totalReq))
    If CkApenasUrg.value = vbChecked Then
        Report.formulas(10) = "Tipo=" & BL_TrataStringParaBD("Apenas Urgentes")
    End If
    
    Me.SetFocus
    Call BL_ExecutaReport
    
    gSQLError = 0
    gSQLISAM = 0
    sql = "DELETE FROM " & "SL_CR_TEMPOS" & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
        
    gSQLError = 0
    gSQLISAM = 0
    sql = "DROP TABLE " & "SL_CR_TEMPOS" & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    
    BL_FimProcessamento Me
    Exit Sub
            
TrataErro:
    BL_FimProcessamento Me
    BG_Mensagem mediMsgBox, "Erro ao Imprimir." & vbCrLf & Err.Description, vbCritical, App.ProductName
    Exit Sub
    Resume Next
End Sub

Sub PreencheEstrutura()
    Dim sSql As String
    Dim sqlGrAna As String
    
    Dim rs As ADODB.recordset
    Dim rsGrAna As ADODB.recordset
    
    Dim codAna As String
    Dim i As Integer
    Dim flag As Integer
    Dim tempoTotal As Long
    
    Dim TempoMedio As Long
    Dim tempoMaximo As Long
    Dim TempoMinimo As Long
    
    Dim NumeroTotalAnalises As Long
    Dim MediaGrupo As String
    
    Dim condicao As String
    
    totalReq = 0
    codAna = ""
    
    'CRIA O SQL
    sSql = DevolveCriterioPesquisa
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenStatic
    rs.Open sSql, gConexao
    
    If rs.RecordCount <= 0 Then
        rs.Close
        Set rs = Nothing
        Exit Sub
    Else
        totalReq = rs.RecordCount
        
        TamEstrutAna = 0
        ReDim estrutAna(TamEstrutAna)
        While Not rs.EOF
            CalculaTempoSemTubos rs!n_req, BL_HandleNull(rs!cod_proven, "")
            rs.MoveNext
        Wend
    End If
    rs.Close
    Set rs = Nothing
    
    
    TempoMinimo = estrutAna(1).tempo
    requisMinimo = estrutAna(1).n_req
    tempoMaximo = 0
    tempoTotal = 0
    tempoMax = ""
    tempoMin = ""
    
    For i = 1 To TamEstrutAna
        tempoTotal = tempoTotal + estrutAna(i).tempo
        
        ' CALCULA O MINiMO
        If estrutAna(i).tempo < TempoMinimo Then
            TempoMinimo = estrutAna(i).tempo
            requisMinimo = estrutAna(i).n_req
        End If
        
        ' CALCULA O MAXIMO
        If estrutAna(i).tempo > tempoMaximo Then
            tempoMaximo = estrutAna(i).tempo
            requisMaximo = estrutAna(i).n_req
        End If
    Next
    
    TempoMedio = Format(tempoTotal / TamEstrutAna, "0")
    
    
    ' CALCULA A STRING PARA O TEMPO MEDIO
    tempo = DevolveTempo(CStr(TempoMedio))
    
    ' CALCULA A STRING PARA O TEMPO Minimo
    tempoMin = DevolveTempo(CStr(TempoMinimo))
    
    ' CALCULA A STRING PARA O TEMPO MAXIMO
    tempoMax = DevolveTempo(CStr(tempoMaximo))
End Sub
Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", CbGrupo
    BG_PreencheComboBD_ADO "sl_proven", "seq_proven", "descr_proven", CbProveniencia
    
End Sub

Private Sub EcCodAnaS_LostFocus()

    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    If EcCodAnaS.Text <> "" Then
        sql = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s=" & BL_TrataStringParaBD(UCase(EcCodAnaS.Text))
        Set rsAna = New ADODB.recordset
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        rsAna.Open sql, gConexao
        If rsAna.RecordCount > 0 Then
            EcDescrAnaS.Text = BL_HandleNull(rsAna!descr_ana_s, "")
            EcCodAnaS.Text = UCase(EcCodAnaS)
        Else
            BG_Mensagem mediMsgBox, "A an�lise n�o existe", vbExclamation, "SISLAB"
            EcCodAnaS.Text = ""
            EcCodAnaS.SetFocus
        End If
        rsAna.Close
        Set rsAna = Nothing
    Else
        EcCodAnaS.Text = ""
        EcDescrAnaS.Text = ""
    End If
    
End Sub


' APENAS PARA OS CASOS QUE NAO DAO ENTRADA DOS TUBOS

Private Sub CalculaTempoSemTubos(n_req As String, cod_proven As String)
    Dim sql As String
    Dim rs As New ADODB.recordset
    Dim sqlGrAna As String
    Dim rsGrAna As New ADODB.recordset
    Dim codAna As String

    Dim dtaChegada As String
    Dim hrChegada As String
    Dim dtaValidacao As String
    Dim HrValidacao As String
    Dim tipoSituacao As String
    Dim descrAna As String
    
    ' SE Proveniencia da requisicao � da Urg,n�o entra em conta com as analises da Microbiologia
    If gCodProvenUrgencia <> -1 And gCodGrupoMicrobiologia <> "-1" And cod_proven = CStr(gCodProvenUrgencia) Then
        sqlGrAna = "SELECT COD_ANA_S FROM SL_ANA_S,SL_GR_ANA WHERE " & _
            "gr_ana=cod_gr_ana and cod_gr_ana='" & gCodGrupoMicrobiologia & "'"
        codAna = "("
        
        Set rsGrAna = New ADODB.recordset
        rsGrAna.CursorLocation = adUseClient
        rsGrAna.CursorType = adOpenStatic
        rsGrAna.Open sqlGrAna, gConexao
        If rsGrAna.RecordCount > 0 Then
            While Not rsGrAna.EOF
                If codAna = "(" Then
                    codAna = codAna & "'" & rsGrAna!cod_ana_s & "'"
                Else
                    codAna = codAna & ",'" & rsGrAna!cod_ana_s & "'"
                End If
                rsGrAna.MoveNext
            Wend
        End If
            codAna = codAna & ")"
        rsGrAna.Close
        Set rsGrAna = Nothing
    End If
    
    sql = "SELECT x1.t_sit,x2.cod_ana_s, x1.dt_chega, x1.hr_chega, x2.dt_val, x2.hr_val " & _
        " FROM sl_requis x1, sl_realiza x2 " & _
        " WHERE x1.n_req = x2.n_req AND x2.dt_val IS NOT NULL AND x2.dt_val IS NOT NULL and x2.hr_val IS NOT NULL " & _
        " AND x1.dt_chega IS NOT NULL AND x1.hr_chega IS NOT NULL " & _
        " AND x1.n_req = " & n_req & _
        " AND x2.cod_ana_s <>'S99999' "
    
    ' SE FOI INDICADO UMA ANALISE ESPECIFICA
    If EcCodAnaS.Text <> "" Then
        sql = sql & " AND x2.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS)
    End If
       
    ' SE FOI INDICADO UM GRUPO ESPECIFICO
    If CbGrupo.ListIndex <> mediComboValorNull Then
        sql = sql & " AND x2.cod_ana_s IN (SELECT x3.cod_ana_s "
        sql = sql & " FROM sl_ana_s x3"
        sql = sql & " WHERE x3.gr_ana = " & BL_TrataStringParaBD(CbGrupo.ItemData(CbGrupo.ListIndex)) & ")"
    End If
        
    
    
    ' FEITO ESPECIFICAMENTE PARA FAMALICAO
    ' REQUISICOES DA URGENCIA NAO ENTRAM EM CONTA COM MICROBIOLOGIA
    ' TODAS AS OUTRAS NAO ENTRA EM CONTA COM A LOWNSTEIN
    ' --------------------------------------------------------------
    If gLAB = "HFM" Then
        
        If gCodGrupoMicrobiologia <> "-1" And codAna <> "" And cod_proven = CStr(gCodProvenUrgencia) Then
            sql = sql & " AND x2.cod_ana_s not in " & codAna
        ElseIf gCodAnaLownstein <> "-1" Then
            sql = sql & " AND x2.cod_ana_s <>  " & BL_TrataStringParaBD(gCodAnaLownstein)
        End If
    End If
    ' ---------------------------------------------------------------
    sql = sql & " ORDER BY dt_val desc, hr_val desc"

    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    If rs.RecordCount <= 0 Then
        Set rs = Nothing
        Exit Sub
    Else
        dtaChegada = BL_HandleNull(rs!dt_chega, "")
        hrChegada = BL_HandleNull(rs!hr_chega, "")
        dtaValidacao = BL_HandleNull(rs!dt_val, "")
        HrValidacao = BL_HandleNull(rs!hr_val, "")
        tipoSituacao = BL_HandleNull(rs!t_sit, "")
        If BL_HandleNull(rs!cod_ana_s, "") <> "" Then
            descrAna = BL_SelCodigo("sl_ana_S", "Descr_Ana_S", "cod_ana_s", rs!cod_ana_s)
        End If
        rs.Close
        Set rs = Nothing
    End If
    
    If dtaChegada <> "" And dtaValidacao <> "" And hrChegada <> "" And HrValidacao <> "" Then
        TamEstrutAna = TamEstrutAna + 1
        ReDim Preserve estrutAna(TamEstrutAna)
        estrutAna(TamEstrutAna).tempo = CLng(DateDiff("n", dtaChegada & " " & hrChegada, dtaValidacao & " " & HrValidacao))
        estrutAna(TamEstrutAna).n_req = n_req
        estrutAna(TamEstrutAna).flg_Ultrapassa = False
        
        ' GUARDA INFORMACAO DAS REQUISICOES QUE ULTRAPASSAM O TEMPO PREVISTO
        '---------------------------------------------------------------------
        If BL_HandleNull(tipoSituacao, "") = "0" Then  'CON
            If CLng(gTempoMaximoCON) < CLng(estrutAna(TamEstrutAna).tempo) Then
                estrutAna(TamEstrutAna).flg_Ultrapassa = True
                estrutAna(TamEstrutAna).analise = descrAna
                estrutAna(TamEstrutAna).tempo_previsto = gTempoMaximoCON
                estrutAna(TamEstrutAna).tempo_ultrapassa = CLng(estrutAna(TamEstrutAna).tempo) - CLng(gTempoMaximoCON)
            End If
        ElseIf BL_HandleNull(tipoSituacao, "") = "1" Then  'INT
            If CLng(gTempoMaximoINT) < CLng(estrutAna(TamEstrutAna).tempo) Then
                estrutAna(TamEstrutAna).flg_Ultrapassa = True
                estrutAna(TamEstrutAna).analise = descrAna
                estrutAna(TamEstrutAna).tempo_previsto = gTempoMaximoINT
                estrutAna(TamEstrutAna).tempo_ultrapassa = CLng(estrutAna(TamEstrutAna).tempo) - CLng(gTempoMaximoINT)
            End If
        ElseIf BL_HandleNull(tipoSituacao, "") = "2" Then  'URG
            If CLng(gTempoMaximoURG) < CLng(estrutAna(TamEstrutAna).tempo) Then
                estrutAna(TamEstrutAna).flg_Ultrapassa = True
                estrutAna(TamEstrutAna).analise = descrAna
                estrutAna(TamEstrutAna).tempo_previsto = gTempoMaximoURG
                estrutAna(TamEstrutAna).tempo_ultrapassa = CLng(estrutAna(TamEstrutAna).tempo) - CLng(gTempoMaximoURG)
            End If
        End If
        '---------------------------------------------------------------------
        
    End If
    
End Sub

' APENAS PARA OS CASOS QUE EXISTE ENTRADA DOS TUBOS

Private Sub CalculaTempoComTubos(n_req As String, cod_proven As Long)
    Dim sql As String
    Dim rs As New ADODB.recordset
    Dim sqlGrAna As String
    Dim rsGrAna As New ADODB.recordset
    Dim codAna As String

    Dim dtaChegada As String
    Dim hrChegada As String
    Dim dtaValidacao As String
    Dim HrValidacao As String
    Dim tipoSituacao As String
    Dim descrAna As String
    Dim tempoMaxReq As Long
    
    sql = "SELECT x1.t_sit, x2.cod_ana_s, x3.descr_ana_s, x1.dt_chega, x1.hr_chega, x2.dt_val, x2.hr_val "
    sql = sql & " FROM sl_requis x1, sl_realiza x2, sl_ana_s x3, sl_req_tubo x4 "
    sql = sql & " WHERE x1.n_req = x2.n_req AND x2.dt_val IS NOT NULL "
    sql = sql & " AND x2.hr_val IS NOT NULL "
    sql = sql & " AND x1.n_req = " & n_req & " "
    sql = sql & " AND x2.cod_ana_s = x3.cod_ana_s AND x2.cod_ana_s = x4.cod_ana_s "
    sql = sql & " AND x3.cod_tubo = x4.cod_tubo "
    sql = sql & " AND x1.n_req = x4.n_req "
    sql = sql & " AND x4.dt_chega IS NOT NULL AND x4.hr_chega IS NOT NULL "
    sql = sql & " AND x2.cod_ana_s <>'S99999' "
    
    ' SE FOI INDICADO UMA ANALISE ESPECIFICA
    If EcCodAnaS.Text <> "" Then
        sql = sql & " AND x2.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS)
    End If
       
    ' SE FOI INDICADO UM GRUPO ESPECIFICO
    If CbGrupo.ListIndex <> mediComboValorNull Then
        sql = sql & " AND  x3.gr_ana = " & BL_TrataStringParaBD(CbGrupo.ItemData(CbGrupo.ListIndex))
    End If
    


    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    If rs.RecordCount <= 0 Then
        Set rs = Nothing
        Exit Sub
    Else
        
        ' CALCULA A ANALISE QUE DEMOROU MAIS TEMPO A SER CONCLUIDA
        '---------------------------------------------------------
        rs.MoveFirst
        tempoMaxReq = 0
        While Not rs.EOF
            If CLng(DateDiff("n", BL_HandleNull(rs!dt_chega, "01-01-1900") & " " & BL_HandleNull(rs!hr_chega, "00:00"), BL_HandleNull(rs!dt_val, "01-01-1900") & " " & BL_HandleNull(rs!hr_val, "00:00"))) > tempoMaxReq Then
                dtaChegada = BL_HandleNull(rs!dt_chega, "01-01-1900")
                hrChegada = BL_HandleNull(rs!hr_chega, "00:00")
                dtaValidacao = BL_HandleNull(rs!dt_val, "01-01-1900")
                HrValidacao = BL_HandleNull(rs!hr_val, "00:00")
                tipoSituacao = BL_HandleNull(rs!t_sit, "")
                descrAna = BL_HandleNull(rs!descr_ana_s, "")
                tempoMaxReq = CLng(DateDiff("n", BL_HandleNull(rs!dt_chega, "01-01-1900") & " " & BL_HandleNull(rs!hr_chega, "00:00"), BL_HandleNull(rs!dt_val, "01-01-1900") & " " & BL_HandleNull(rs!hr_val, "00:00")))
            End If
            rs.MoveNext
        Wend
        '---------------------------------------------------------
        rs.Close
        Set rs = Nothing
    End If
    
    If dtaChegada <> "" And dtaValidacao <> "" And hrChegada <> "" And HrValidacao <> "" Then
        TamEstrutAna = TamEstrutAna + 1
        ReDim Preserve estrutAna(TamEstrutAna)
        estrutAna(TamEstrutAna).tempo = tempoMaxReq
        estrutAna(TamEstrutAna).n_req = n_req
        estrutAna(TamEstrutAna).flg_Ultrapassa = False
        
        ' GUARDA INFORMACAO DAS REQUISICOES QUE ULTRAPASSAM O TEMPO PREVISTO
        '---------------------------------------------------------------------
        If BL_HandleNull(tipoSituacao, "") = "0" Then  'CON
            If CLng(gTempoMaximoCON) < CLng(estrutAna(TamEstrutAna).tempo) Then
                estrutAna(TamEstrutAna).flg_Ultrapassa = True
                estrutAna(TamEstrutAna).analise = descrAna
                estrutAna(TamEstrutAna).tempo_previsto = gTempoMaximoCON
                estrutAna(TamEstrutAna).tempo_ultrapassa = CLng(estrutAna(TamEstrutAna).tempo) - CLng(gTempoMaximoCON)
            End If
        ElseIf BL_HandleNull(tipoSituacao, "") = "1" Then  'INT
            If CLng(gTempoMaximoINT) < CLng(estrutAna(TamEstrutAna).tempo) Then
                estrutAna(TamEstrutAna).flg_Ultrapassa = True
                estrutAna(TamEstrutAna).analise = descrAna
                estrutAna(TamEstrutAna).tempo_previsto = gTempoMaximoINT
                estrutAna(TamEstrutAna).tempo_ultrapassa = CLng(estrutAna(TamEstrutAna).tempo) - CLng(gTempoMaximoINT)
            End If
        ElseIf BL_HandleNull(tipoSituacao, "") = "2" Then  'URG
            If CLng(gTempoMaximoURG) < CLng(estrutAna(TamEstrutAna).tempo) Then
                estrutAna(TamEstrutAna).flg_Ultrapassa = True
                estrutAna(TamEstrutAna).analise = descrAna
                estrutAna(TamEstrutAna).tempo_previsto = gTempoMaximoURG
                estrutAna(TamEstrutAna).tempo_ultrapassa = CLng(estrutAna(TamEstrutAna).tempo) - CLng(gTempoMaximoURG)
            End If
        End If
        '---------------------------------------------------------------------
        
    End If
    
End Sub

' CRIA TABELA TEMPORARIA
Sub Cria_TmpRec_Estatistica()

    Dim TmpRec(2) As DefTable
    
    TmpRec(1).NomeCampo = "N_REQ"
    TmpRec(1).tipo = "INTEGER"
    TmpRec(1).tamanho = 9
    
    TmpRec(2).NomeCampo = "TEMPO"
    TmpRec(2).tipo = "STRING"
    TmpRec(2).tamanho = 20
    
    Call BL_CriaTabela("SL_CR_TEMPOS" & gNumeroSessao, TmpRec)
    
End Sub



' PREENCHE TABELA TEMPORARIA PARA REPORT

Sub PreencheTabelaTemporaria()
    Dim i As Integer
    Dim tempoUlt As String
    Dim sSql As String
    On Error GoTo TrataErro
    
    For i = 1 To totalReq
        If estrutAna(i).flg_Ultrapassa = True Then
            tempoUlt = DevolveTempo(estrutAna(i).tempo)
            sSql = "INSERT INTO sl_cr_tempos" & gNumeroSessao
            sSql = sSql & " (n_req, tempo) VALUES ("
            sSql = sSql & estrutAna(i).n_req & ", "
            sSql = sSql & Round(BL_TrataStringParaBD(tempoUlt), 2) & ")"
            
            BG_ExecutaQuery_ADO sSql
            BG_Trata_BDErro

        End If
    Next
Exit Sub
TrataErro:
    Call BG_LogFile_Erros("Erro PreencheTabelaTemporaria (FormEstatTempos) -> " & Err.Number & " : " & Err.Description)
    Exit Sub
    Resume Next
End Sub


' DADO DETERMINADO TEMPO EM MINUTOS TRANSFORMA ESSE TEMPO NUMA STRING

Private Function DevolveTempo(minutos As String) As String
    Dim tempo As String
    Dim aux As String
    
    ' CALCULA STRING  TEMPOS
    If minutos < 60 Then
        tempo = minutos & " m"
    ElseIf minutos < 24 * 60 Then
        tempo = minutos \ 60 & " h "
        tempo = tempo & minutos Mod 60 & " m"
    Else
        ' VERIFICA SE TEM CASAS DECIMAIS
        If InStr(1, minutos / (24 * 60), gSimboloDecimal) <> 0 Then
            tempo = minutos \ (24 * 60) & " d "
        Else
            tempo = minutos \ (24 * 60) & " d "
        End If
        
        minutos = minutos Mod (24 * 60)
            tempo = tempo & minutos \ 60 & " h "
            tempo = tempo & minutos Mod 60 & " m"
    End If
    DevolveTempo = tempo
End Function


' CONSTROI CRITERIO DE PESQUISA DAS REQUISICOES

Private Function DevolveCriterioPesquisa() As String
    Dim sSql As String
    
    sSql = "SELECT  x1.n_req n_req , x1.cod_proven "
    sSql = sSql & " FROM sl_requis x1, sl_realiza x2 WHERE x1.n_req = x2.n_req "
    
    ' REQUISICOES IMPRESSAS OU VALIDADAS
    If gLAB <> "HSJ" Then
        sSql = sSql & " AND x1.estado_req in ('F', 'D') "
    End If
    
    ' REQUICISOES QUE O dt_chega ESTA NO INTERVALO DE DATAS
    sSql = sSql & "  AND  (x1.dt_chega between " & BL_TrataDataParaBD(EcDataInicial) & " and "
    sSql = sSql & BL_TrataDataParaBD(EcDataFinal.Text) & ") "
    
    
    ' APENAS REQUISICOES URGENTES
    If CkApenasUrg.value = vbChecked Then
        sSql = sSql & " AND x1.T_URG ='1' "
    End If
    
    'SE FOI SELECCIONADA PROVENIENCIA
    If CbProveniencia.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND cod_proven =(select sp.cod_proven  FROM sl_proven sp WHERE seq_proven = " & BL_TrataStringParaBD(CStr(CbProveniencia.ItemData(CbProveniencia.ListIndex))) & ")"
    End If
    
    ' SE FOI INDICADO UMA ANALISE ESPECIFICA
    If EcCodAnaS.Text <> "" Then
        sSql = sSql & " AND x2.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS)
    End If
       
    ' SE FOI INDICADO UM GRUPO ESPECIFICO
    If CbGrupo.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND x2.cod_ana_s IN (SELECT x3.cod_ana_s "
        sSql = sSql & " FROM sl_ana_s x3"
        sSql = sSql & " WHERE x3.gr_ana = " & BL_TrataStringParaBD(CbGrupo.ItemData(CbGrupo.ListIndex)) & ")"
    End If
    
    sSql = sSql & " GROUP BY x1.n_req, x1.cod_proven"
    DevolveCriterioPesquisa = sSql
End Function



'LIMPA FLEXGRID

Sub LimpaFgUltrap()
    Dim j As Long
    j = FgUltrap.rows - 1
    While j > 0
        If j > 1 Then
            FgUltrap.RemoveItem j
        Else
            FgUltrap.TextMatrix(j, 0) = ""
            FgUltrap.TextMatrix(j, 1) = ""
            FgUltrap.TextMatrix(j, 2) = ""
            FgUltrap.TextMatrix(j, 3) = ""
            FgUltrap.TextMatrix(j, 4) = ""
        End If
        j = j - 1
    Wend
End Sub


' PREENCHE A FLEXGRID

Private Sub PreencheFgUltrap()
    Dim j As Integer
    Dim linha As Integer
    linha = 1
    For j = 1 To TamEstrutAna
        If estrutAna(j).flg_Ultrapassa = True Then
            FgUltrap.TextMatrix(linha, 0) = estrutAna(j).n_req
            FgUltrap.TextMatrix(linha, 1) = estrutAna(j).analise
            FgUltrap.TextMatrix(linha, 2) = DevolveTempo(estrutAna(j).tempo)
            FgUltrap.TextMatrix(linha, 3) = DevolveTempo(estrutAna(j).tempo_previsto)
            FgUltrap.TextMatrix(linha, 4) = "+" & DevolveTempo(estrutAna(j).tempo_ultrapassa)
            linha = linha + 1
            FgUltrap.AddItem ""
        End If
    Next j
End Sub



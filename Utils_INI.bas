Attribute VB_Name = "Utils_INI"
Option Explicit

' Actualiza��o : 19/02/2002
' T�cnico Paulo Costa

Declare Function getprivateprofilestring Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal grpnm$, ByVal parnm As Any, ByVal deflt$, ByVal parvl$, ByVal parlen%, ByVal inipath$) As Integer
Declare Function writeprivateprofilestring Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal grpnm$, ByVal parnm As Any, ByVal parvl As Any, ByVal inipath$) As Integer
 
Private Const GWL_EXSTYLE = (-20)
Private Const WS_EX_LAYERED = &H80000
Private Const LWA_ALPHA = &H2

Private Declare Function GetWindowLong Lib "user32" _
Alias "GetWindowLongA" (ByVal hWnd As Long, _
ByVal nIndex As Long) As Long

Private Declare Function SetWindowLong Lib "user32" _
Alias "SetWindowLongA" (ByVal hWnd As Long, _
ByVal nIndex As Long, ByVal dwNewLong As Long) _
As Long

Private Declare Function SetLayeredWindowAttributes Lib _
"user32" (ByVal hWnd As Long, ByVal crKey As Long, _
ByVal bAlpha As Byte, ByVal dwFlags As Long) As Long

Function iniReadValue(inipath$, grpnm$, parnm$, deflt$) As String
   
    ' get line from .INI file
    ' input: inpath=path to .INI file, grpnm=group header (no [])
    '        parnm=parameter name, deflt=default value
    ' output: value from file or default if not found
    
    On Error GoTo ErrorHandler
   
    Dim X As Integer
   
    Dim iodata As String * 2048
   
    X = getprivateprofilestring(grpnm, parnm, deflt, iodata, Len(iodata), inipath)
   
    iniReadValue = Left$(iodata, X)
   
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            
            Call BG_LogFile_Erros("iniReadValue (Utils_INI) -> " & Err.Description)

            iniReadValue = ""
            Exit Function
    End Select
End Function

 Sub iniWriteValue(inipath$, grpnm$, parnm$, parvl$)
   
    ' update single line of .INI file
    ' NOTE: group and/or parameter line will be added if not present
    ' input: inipath=path to .INI file, grpnm=group header (no [])
    '        parnm=parameter name, parvl=new parameter value
    ' output: TRUE=ok, FALSE=not done
   
    On Error GoTo ErrorHandler
   
    Dim X As Long
   
    X = writeprivateprofilestring(grpnm, parnm, parvl, inipath)
 
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            
            Call BG_LogFile_Erros("iniWriteValue (Utils_INI) -> " & Err.Description)

            Exit Sub
    End Select
End Sub

' Sub inidelvalue% (inipath$, grpnm$, parnm$)
'   ' delete line from .INI file (physically)
'   ' input: inipath=path to .INI file, grpnm=group header (no []),
'   '     parnm=parameter name
'   ' output: 1=OK, 0=error
'   Dim x As Long
'   x = writeprivateprofilestring(grpnm, parnm, 0&, inipath)
' End Sub

' Sub inidelgroup% (inipath$, grpnm$)
'   ' delete entire group from .INI file (physically)
'   ' input: inipath=path to .INI file, grpnm=group to delete
'   ' output:  1=OK, 0=error
'   Dim x As Long
'   x = writeprivateprofilestring(grpnm, 0&, 0&, inipath)
' End Function

' Function inireadentries(inipath$, grpnm$) As String
'
'   ' get all parameter names for a group in a .INI file
'   ' input: inipath=path to .INI file, grpnm=group header (no [])
'   ' output: string of null-delimited parameters
'
'   Dim X As Long
'   Dim iodata As String * 2048
'
'   X = getprivateprofilestring(grpnm, 0&, "", iodata, Len(iodata), inifind(inipath))
'   inireadentries = Left$(iodata, X)
'
' End Function


' RET :  1 : OK.
'       -1 : Erro inesperado.

Public Function Get_INI() As Integer

    On Error GoTo ErrorHandler
    
    Dim f_ini As String
    Dim valor As String
    
    f_ini = App.Path & "\SISLAB.INI"
    
    '........................................................
    ' Skin do SISLAB
    '........................................................
    valor = iniReadValue(f_ini, "Skin", "skin", "SISLAB")
    valor = UCase(Trim(valor))
    gSKIN = valor
    
    '........................................................
    ' Sistema de Factura��o
    '........................................................
    'soliveira 03-12-2003 em vez de usar o fx de inicializa��oes, usar a chave "SISTEMA_FACT"
'    Valor = iniReadValue(f_ini, "Facturacao", "facturacao", "")
'    Valor = UCase(Trim(Valor))
'    gSISTEMA_FACTURACAO = Valor
    
    '........................................................
    ' Forma como os resultados s�o inseridos.
    '........................................................
    valor = iniReadValue(f_ini, "Parametrizacoes", "FormaIntroducaoResultados", cFormaInsercao_NORMAL)
    valor = UCase(Trim(valor))
    gFormaInsercao = valor
    
    
    Get_INI = 1

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            
            Call BG_LogFile_Erros("Get_INI (Utils_INI) -> " & Err.Description)

            Get_INI = -1
            Exit Function
    End Select
End Function


Public Function DevolveValorINI(sFicheiro As String, sSeccao As String, sChave As String) As String

    On Error GoTo Trata_Erro
    Dim sRetorno As String
    
    If Dir(sFicheiro) <> "" Then
        sRetorno = iniReadValue(sFicheiro, sSeccao, sChave, "")
    End If

    DevolveValorINI = sRetorno

    Exit Function
    
Trata_Erro:
    DevolveValorINI = "Erro"
End Function






Public Function TransForm(Form As Form, TransLevel As Byte) As Boolean
SetWindowLong Form.hWnd, GWL_EXSTYLE, WS_EX_LAYERED
SetLayeredWindowAttributes Form.hWnd, 0, TransLevel, LWA_ALPHA
TransForm = Err.LastDllError = 0
End Function



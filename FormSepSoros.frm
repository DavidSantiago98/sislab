VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form FormSepSoros 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormProdutos"
   ClientHeight    =   6285
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6765
   Icon            =   "FormSepSoros.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6285
   ScaleWidth      =   6765
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton CmdOKPri 
      Caption         =   "OK"
      Height          =   375
      Left            =   3360
      TabIndex        =   13
      Top             =   5520
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Caption         =   "Tubos Prim�rios"
      Height          =   3015
      Left            =   3480
      TabIndex        =   8
      Top             =   0
      Width           =   3255
      Begin VB.CheckBox CkConsultas 
         Alignment       =   1  'Right Justify
         Caption         =   "Incluir Marca��es"
         Height          =   255
         Left            =   840
         TabIndex        =   14
         Top             =   720
         Width           =   1575
      End
      Begin VB.CommandButton BtEtiquetasPri 
         Height          =   530
         Left            =   2540
         Picture         =   "FormSepSoros.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   " Etiquetas de Tubos Prim�rios"
         Top             =   240
         Width           =   550
      End
      Begin VB.TextBox EcNumReqPri 
         Appearance      =   0  'Flat
         Height          =   300
         Left            =   1200
         TabIndex        =   10
         Top             =   360
         Width           =   1215
      End
      Begin VB.ListBox Lista_TubosPri 
         Appearance      =   0  'Flat
         Height          =   1785
         Left            =   200
         TabIndex        =   9
         Top             =   1080
         Width           =   2895
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Requisi��o"
         Height          =   195
         Left            =   240
         TabIndex        =   12
         Top             =   390
         Width           =   795
      End
   End
   Begin VB.TextBox EcPrinterEtiq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1080
      TabIndex        =   5
      Top             =   5520
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   1560
      TabIndex        =   4
      Top             =   5520
      Width           =   1095
   End
   Begin Crystal.CrystalReport Report1 
      Left            =   2760
      Top             =   5520
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tubos Secund�rios"
      Height          =   3015
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   3255
      Begin VB.ListBox Lista_Tubos 
         Appearance      =   0  'Flat
         Height          =   1980
         Left            =   195
         TabIndex        =   7
         Top             =   840
         Width           =   2895
      End
      Begin VB.TextBox EcNumReq 
         Appearance      =   0  'Flat
         Height          =   300
         Left            =   1200
         TabIndex        =   3
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton BtEtiquetas 
         Height          =   530
         Left            =   2520
         Picture         =   "FormSepSoros.frx":0BEE
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   " Etiquetas de Tubos Secund�rios "
         Top             =   240
         Width           =   550
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Requisi��o"
         Height          =   195
         Left            =   240
         TabIndex        =   2
         Top             =   390
         Width           =   795
      End
   End
   Begin VB.Label Label32 
      Caption         =   "EcPrinterEtiq"
      Enabled         =   0   'False
      Height          =   255
      Left            =   0
      TabIndex        =   6
      Top             =   5520
      Visible         =   0   'False
      Width           =   1095
   End
End
Attribute VB_Name = "FormSepSoros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 10/12/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Private Type DOCINFO
    pDocName As String
    pOutputFile As String
    pDatatype As String
End Type

Private Declare Function ClosePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndDocPrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function EndPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function OpenPrinter Lib "winspool.drv" Alias "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, ByVal pDefault As Long) As Long
Private Declare Function StartDocPrinter Lib "winspool.drv" Alias "StartDocPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, pDocInfo As DOCINFO) As Long
Private Declare Function StartPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Private Declare Function WritePrinter Lib "winspool.drv" (ByVal hPrinter As Long, pBuf As Any, ByVal cdBuf As Long, pcWritten As Long) As Long

Public CampoActivo As Object
Dim NomeTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

'Lista que vai conter os dados necess�rios � marca��o das an�lises
Private MaReq() As MarcacaoAnalises

'Estruturas utilizadas para gerar as etiquetas
Private Type Tipo_Tubo
    Especial As Boolean
    Cheio As Boolean
    abrAna As String
    GrAna As String
    CodProd As String
    CodProdBar As String
    OrdAna As Integer
    CodTubo As String
    QtMax As Double
    QtOcup As Double
    Designacao_tubo As String
    inf_complementar As String
    dt_chega As String
End Type

Dim Tubos() As Tipo_Tubo

'Comando utilizados para seleccionar as an�lises da requisi��o mais os seu dados
Dim CmdAnaReq As New ADODB.Command

Dim EtiqStartJob As String
Dim EtiqJob As String
Dim EtiqEndJob As String
Dim EtiqHPrinter As Long
Dim PrinterX As Long

Private Sub BtEtiquetas_Click()
    Dim i As Integer
    If EcNumReq = "" Then Exit Sub
    Carrega_Grid
    DoEvents
    If Lista_Tubos.ListCount = 0 Then Exit Sub
    For i = 0 To Lista_Tubos.ListCount - 1
        Lista_Tubos.ListIndex = i
    Next
    
End Sub

Private Sub BtEtiquetasPri_Click()
     
    'Call ImprimeEtiq(EcNumReqPri.Text, "", True)
    'BRUNODSANTOS 28.11.2017
    Call ImprimeEtiq(Right(EcNumReqPri.Text, 7), "", True)
End Sub

Private Sub cmdOK_Click()
    
    Call Carrega_Grid

End Sub

Private Sub CmdOKPri_Click()

    Call Carrega_Grid_Pri
    
End Sub

Private Sub EcNumReq_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    cmdOK.Default = True

End Sub

Private Sub EcNumReq_LostFocus()

    cmdOK.Default = False

End Sub

Private Sub EcNumReq_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcNumReq)

End Sub
Private Sub EcNumReqPri_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    CmdOKPri.Default = True

End Sub

Private Sub EcNumReqPri_LostFocus()

    CmdOKPri.Default = False

End Sub

Private Sub EcNumReqPri_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcNumReqPri)

End Sub

Sub Form_Load()
    
    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Etiquetas para Tubos "
    Me.left = 1000
    Me.top = 800
    Me.Width = 6855 '3555
    Me.Height = 3480 ' Normal
    
    NomeTabela = "sl_requis"

    'Define o tipo de campos
    Call DefTipoCampos
    

    EcPrinterEtiq.Text = BL_SelImpressora("Etiqueta.rpt")

    End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

    EcNumReq.SetFocus
    
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

End Sub

Sub FuncaoLimpar()

End Sub

Sub FuncaoEstadoAnterior()

End Sub

Sub FuncaoProcurar()
    
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()
    
End Sub

Sub FuncaoInserir()
 
End Sub

Sub FuncaoModificar()

End Sub

Sub FuncaoRemover()
    
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "n_req", EcNumReq, adDouble
    BG_DefTipoCampoEc_ADO NomeTabela, "n_req", EcNumReqPri, adDouble

End Sub

Sub Cria_TmpEtq()

    On Error GoTo ErrorHandler
    
    Dim TmpEtq(1 To 18) As DefTable
    Dim sql As String
    Dim rs As ADODB.recordset
    
    TmpEtq(1).NomeCampo = "t_utente"
    TmpEtq(1).tipo = "STRING"
    TmpEtq(1).tamanho = 5
    
    TmpEtq(2).NomeCampo = "utente"
    TmpEtq(2).tipo = "STRING"
    TmpEtq(2).tamanho = 20
    
    TmpEtq(3).NomeCampo = "n_proc"
    TmpEtq(3).tipo = "STRING"
    TmpEtq(3).tamanho = 100
    
    TmpEtq(4).NomeCampo = "n_req"
    TmpEtq(4).tipo = "STRING"
    TmpEtq(4).tamanho = 9
    
    TmpEtq(5).NomeCampo = "t_urg"
    TmpEtq(5).tipo = "STRING"
    TmpEtq(5).tamanho = 3
    
    TmpEtq(6).NomeCampo = "dt_req"
    TmpEtq(6).tipo = "DATE"
   
    TmpEtq(7).NomeCampo = "ordem"
    TmpEtq(7).tipo = "STRING"
    TmpEtq(7).tamanho = 3
    
    TmpEtq(8).NomeCampo = "produto"
    TmpEtq(8).tipo = "STRING"
    TmpEtq(8).tamanho = 25
    
    TmpEtq(9).NomeCampo = "nome_ute"
    TmpEtq(9).tipo = "STRING"
    TmpEtq(9).tamanho = 80
    
    TmpEtq(10).NomeCampo = "n_req_bar"
    TmpEtq(10).tipo = "STRING"
    TmpEtq(10).tamanho = 20

    TmpEtq(11).NomeCampo = "gr_ana"
    TmpEtq(11).tipo = "STRING"
    TmpEtq(11).tamanho = 40

    TmpEtq(12).NomeCampo = "abr_ana"
    TmpEtq(12).tipo = "STRING"
    TmpEtq(12).tamanho = 10
    
    TmpEtq(13).NomeCampo = "descr_tubo"
    TmpEtq(13).tipo = "STRING"
    TmpEtq(13).tamanho = 40

    TmpEtq(14).NomeCampo = "situacao"
    TmpEtq(14).tipo = "STRING"
    TmpEtq(14).tamanho = 7
    
    TmpEtq(15).NomeCampo = "episodio"
    TmpEtq(15).tipo = "STRING"
    TmpEtq(15).tamanho = 15
    
    TmpEtq(16).NomeCampo = "req_aux"
    TmpEtq(16).tipo = "STRING"
    TmpEtq(16).tamanho = 15
    
    TmpEtq(17).NomeCampo = "dt_nasc"
    TmpEtq(17).tipo = "DATE"
    
    TmpEtq(18).NomeCampo = "idade"
    TmpEtq(18).tipo = "STRING"
    TmpEtq(18).tamanho = 20

    Call BL_CriaTabela("sl_cr_etiq" & gNumeroSessao, TmpEtq)
    
    sql = "CREATE UNIQUE INDEX sl_cr_etiq" & gNumeroSessao & "_uk " & _
          "ON sl_cr_etiq" & gNumeroSessao & " " & _
          " ( descr_tubo  )"
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseClient
    rs.Open sql, gConexao
    Set rs = Nothing
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro : Cria_TmpEtq (FormSepSoros) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
    End Select
End Sub

Sub Carrega_Grid()
              
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim rs As ADODB.recordset
        
    'EcNumReq.Text = Trim(EcNumReq.Text)
    'BRUNODSANTOS 28.11.2017
    EcNumReq.Text = Right(EcNumReq.Text, 7)
    Lista_Tubos.Clear
    DoEvents
    DoEvents
    
    ' -----------------------------------------------
    
    ' Preenche a grid.
    
    sql = "SELECT DISTINCT " & vbCrLf & _
          "    T.descr_tubo " & vbCrLf & _
          "FROM " & vbCrLf & _
          "    sl_marcacoes M, " & vbCrLf & _
          "    slv_analises A, " & vbCrLf & _
          "    sl_tubo_sec  T " & vbCrLf & _
          "WHERE " & vbCrLf & _
          "    M.n_req        = " & EcNumReq.Text & " AND " & vbCrLf & _
          "    (A.cod_ana    = M.cod_ana_s or A.cod_ana    = M.cod_ana_c or A.cod_ana = M.cod_perfil)" & _
          "     AND A.cod_tubo_sec = T.cod_tubo "
    
    Set rs = New ADODB.recordset
    
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    rs.Open sql, gConexao
    
    If (rs.RecordCount < 1) Then
        MsgBox "N�o existem etiquetas secund�rias para a requisi��o " & Me.EcNumReq & "     ", vbInformation, " Etiquetas Secund�rias"
    End If

    While (Not rs.EOF)
        Lista_Tubos.AddItem BL_HandleNull(rs!descR_tubo, "")
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
     
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro : Carrega_Grid (FormSepSoros) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
    End Select
End Sub

Sub Carrega_Grid_Pri()
              
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim rs As ADODB.recordset
        
    EcNumReqPri.Text = Trim(EcNumReqPri.Text)
    
    Lista_TubosPri.Clear
    DoEvents
    DoEvents
    
    ' -----------------------------------------------
    
    ' Preenche a grid.
    
    sql = "SELECT DISTINCT " & vbCrLf & _
          "    T.descr_tubo " & vbCrLf & _
          "FROM " & vbCrLf & _
          "    sl_req_tubo RT, " & vbCrLf & _
          "    sl_tubo  T " & vbCrLf & _
          "WHERE " & vbCrLf & _
          "    rt.n_req        = " & EcNumReqPri.Text & " AND " & vbCrLf & _
          "    RT.cod_tubo = T.cod_tubo "
    
    If CkConsultas.value = vbChecked Then
        
        sql = sql & " UNION SELECT DISTINCT " & vbCrLf & _
          "    T.descr_tubo " & vbCrLf & _
          "FROM " & vbCrLf & _
          "    sl_req_tubo_consultas RT, " & vbCrLf & _
          "    sl_tubo  T " & vbCrLf & _
          "WHERE " & vbCrLf & _
          "    rt.n_req        = " & EcNumReqPri.Text & " AND " & vbCrLf & _
          "    RT.cod_tubo = T.cod_tubo "
    End If
    
    Set rs = New ADODB.recordset
    
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    rs.Open sql, gConexao
    
    If (rs.RecordCount < 1) Then
        MsgBox "N�o existem etiquetas prim�rias para a requisi��o " & Me.EcNumReq & "     ", vbInformation, " Etiquetas Prim�rias"
    End If

    While (Not rs.EOF)
        Lista_TubosPri.AddItem BL_HandleNull(rs!descR_tubo, "")
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
     
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro : Carrega_Grid_Pri (FormSepSoros) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
    End Select
End Sub

Sub ImprimeEtiq(NReq As String, descR_tubo As String, Optional Tubo_pri As Boolean)
              
    On Error GoTo ErrorHandler
    Dim t_urg As String
    Dim dataImpressao As String
    Dim abrAna As String
    Dim i As Integer
    Dim Report As CrystalReport
    Dim continua As Boolean
    Dim sql As String
    Dim Sql2 As String
    Dim rs As ADODB.recordset
    Dim RS2 As ADODB.recordset
    Dim n_req As String
    Dim N_req_bar As String
    Dim situacao As String
    Dim episodio As String
    Dim Criterio As String
    Dim idade As String
    Dim EtqCrystal As String
    Dim descr_etiq As String
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
    If Trim(NReq) = "" Then
        Exit Sub
    End If
    
    NReq = Trim(NReq)
    N_req_bar = Right("0000000" & NReq, 7)
    
    EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoComputador, "ETIQ_CRYSTAL")
    If EtqCrystal = "-1" Then
        EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ETIQ_CRYSTAL")
    End If
                
    If EtqCrystal = "-1" Or EtqCrystal = "" Then EtqCrystal = "1"
                
    If EtqCrystal = "1" Then
    
        If BL_PreviewAberto("Etiquetas") = True Then Exit Sub
                
        continua = BL_IniciaReport("Etiqueta", "Etiquetas", crptToPrinter, False)
        If (continua = False) Then
            Exit Sub
        End If
                
        Call Cria_TmpEtq
    End If
                
    ' -----------------------------------------------
    
    Criterio = ""
    
    If (Len(descR_tubo) > 0) Then
        Criterio = " AND T.descr_tubo = '" & descR_tubo & "'"
    End If
    
    ' Preenche a tabela secund�ria.
    

    
    sql = "SELECT DISTINCT " & vbCrLf & _
          "    U.t_utente, U.utente, U.n_proc_1, R.n_req, " & vbCrLf & _
          "    R.t_urg, R.dt_chega, 'ORDEM', U.nome_ute, " & vbCrLf & _
          "    'N_REQ_BAR', " & vbCrLf & _
          "    T.descr_tubo,R.t_sit, R.n_epis, R.req_aux, U.dt_nasc_ute, R.dt_previ, T.cod_etiq, T.inf_complementar, T.cod_tubo,  " & vbCrLf & _
          "    U.abrev_ute " & _
          "FROM " & vbCrLf & _
          "    sl_requis    R, " & vbCrLf & _
          "    " & tabela_aux & "   U, " & vbCrLf

    If Tubo_pri = True Then
        sql = sql & "   sl_tubo  T, sl_req_tubo RT " & vbCrLf & ""
    Else
        sql = sql & "   sl_tubo_sec  T " & vbCrLf & ""
    End If
    
    sql = sql & _
          "WHERE " & vbCrLf & _
          "    R.n_req        = " & NReq & "          AND " & vbCrLf & _
          "    R.seq_utente   = U.seq_utente  " & vbCrLf
    If Tubo_pri = True Then
        sql = sql & " AND r.n_Req = rt.n_req AND t.cod_tubo = rt.cod_tubo "
    End If
    sql = sql & Criterio
    
    Set rs = New ADODB.recordset
    
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    rs.Open sql, gConexao
    rs.MoveFirst
    While (Not rs.EOF)
        If BL_HandleNull(rs!t_urg, "0") = "0" Then
            t_urg = "N"
        ElseIf BL_HandleNull(rs!t_urg, "0") = "1" Then
            t_urg = "U"
        ElseIf BL_HandleNull(rs!t_urg, "0") = "2" Then
            t_urg = "L"
        Else
            t_urg = "N"
        End If
        
        ' ---------------------------------
        If BL_HandleNull(rs!dt_nasc_ute, "") <> "" And BL_HandleNull(rs!dt_previ, "") <> "" And BL_TrataDataParaBD(BL_HandleNull(rs!dt_nasc_ute, "")) <> BL_TrataDataParaBD("01/01/1800") Then
            idade = CStr(BG_CalculaIdade(rs!dt_nasc_ute, rs!dt_previ))
        End If
        
        ' Se o epis�dio for igual ao n�mero de requisi��o, n�o sai na etiqueta.
        n_req = Trim(BL_HandleNull(rs!n_req, ""))
        situacao = Trim(BL_HandleNull(rs!t_sit, ""))
        episodio = Trim(BL_HandleNull(rs!n_epis, ""))
        If Tubo_pri = True Then
            descr_etiq = BL_RetornaAnalisesTubos(n_req, BL_HandleNull(rs!cod_tubo, ""))
        Else
            descr_etiq = ""
        End If
        
        If (n_req = episodio) Then
            situacao = ""
            episodio = ""
        End If
        
        ' ---------------------------------
            
        Select Case situacao
        
            Case gT_Urgencia
                situacao = "URG"
            Case gT_Consulta
                situacao = "CON"
            Case gT_Internamento
                situacao = "INT"
            Case gT_Externo
                situacao = "HDI"
            Case gT_LAB
                situacao = "LAB"
            Case gT_RAD
                situacao = "RAD"
            Case gT_Bloco
                situacao = "BLO"
            Case Else
                situacao = ""
        
        End Select
        
        ' ---------------------------------
        If EtqCrystal = "1" Then
        
            dataImpressao = BL_HandleNull(BL_RetornaDtChegaTubo(BL_HandleNull(rs!n_req, ""), BL_HandleNull(rs!cod_tubo, "")), BL_HandleNull(rs!dt_chega, rs!dt_previ))
            Sql2 = "INSERT INTO sl_cr_etiq" & gNumeroSessao & " " & _
                   "(" & _
                   "  t_utente, utente, n_proc, n_req, " & _
                   "  t_urg, dt_req, ordem, produto, " & _
                   "  nome_ute, n_req_bar, gr_ana, abr_ana, " & _
                   "  descr_tubo, situacao, episodio, req_aux,dt_nasc,idade " & _
                   ") " & _
                   "VALUES " & _
                   "( " & _
                   "'" & BL_HandleNull(rs!t_utente, "") & "', " & vbCrLf & _
                   "'" & BL_HandleNull(rs!Utente, "") & "', " & vbCrLf & _
                   "'" & BL_HandleNull(rs!n_proc_1, "") & "', " & vbCrLf & _
                   "'" & BL_HandleNull(rs!n_req, "") & "', " & vbCrLf & _
                   "'" & BL_HandleNull(rs!t_urg, "") & "', " & vbCrLf & _
                   "'" & BL_HandleNull(rs!dt_chega, "") & "', " & vbCrLf & _
                   "'', " & vbCrLf & _
                   "'', " & vbCrLf & _
                   "'" & BL_HandleNull(rs!nome_ute, "") & "', " & vbCrLf & _
                   "'" & BL_HandleNull(rs!cod_etiq, "") & N_req_bar & "', " & vbCrLf & _
                   "'', " & vbCrLf & _
                   "'', " & vbCrLf & _
                   "'" & BL_HandleNull(rs!descR_tubo, "") & "', " & vbCrLf & _
                   "'" & situacao & "', " & vbCrLf & _
                   "'" & episodio & "', " & vbCrLf & _
                   "'" & BL_HandleNull(rs!req_aux, "") & "', " & vbCrLf & "'" & BL_HandleNull(rs!dt_nasc_ute, "") & "','" & idade & "' )"
            
            '   "'" & BL_HandleNull(Rs!descr_gr_ana, "") & "', " & vbCrLf & _

    '        Set RS2 = New ADODB.Recordset
    '        RS2.CursorLocation = adUseServer
    '
            On Error Resume Next
    '        RS2.Open Sql2, gConexao
            BG_ExecutaQuery_ADO Sql2, gConexao
            
            On Error GoTo ErrorHandler
            
            Set RS2 = Nothing
        Else
            If Not LerEtiqInI(Tubo_pri) Then
                MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                Exit Sub
            End If
            If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                MsgBox "Imposs�vel abrir impressora etiquetas"
                Exit Sub
            End If
            abrAna = BL_RetornaAbrAnaTubo(BL_HandleNull(rs!n_req, ""), BL_HandleNull(rs!cod_tubo, ""), Tubo_pri)
            dataImpressao = BL_HandleNull(BL_RetornaDtChegaTubo(BL_HandleNull(rs!n_req, ""), BL_HandleNull(rs!cod_tubo, "")), BL_HandleNull(rs!dt_chega, rs!dt_previ))
            Call EtiqPrint("", abrAna, _
                    BL_HandleNull(rs!t_utente, ""), BL_HandleNull(rs!Utente, ""), _
                    BL_HandleNull(rs!n_proc_1, ""), BL_HandleNull(rs!n_req, ""), _
                    t_urg, dataImpressao, "", _
                    BL_HandleNull(rs!nome_ute, ""), BL_HandleNull(rs!cod_etiq, "") & N_req_bar, BL_HandleNull(rs!descR_tubo, ""), BL_HandleNull(rs!inf_complementar, ""), BL_HandleNull(rs!abrev_ute, ""), BL_HandleNull(rs!cod_etiq, ""), descr_etiq)
            
            EtiqClosePrinter
            
            ' REGISTA A IMPRESS�O DO TUBO PRIM�RIO.
            If Tubo_pri = True Then
                BL_RegistaImprEtiq BL_HandleNull(rs!n_req, ""), BL_HandleNull(rs!cod_tubo, "")
            End If
        End If
        
        rs.MoveNext
        
    Wend
    
    rs.Close
    Set rs = Nothing
    
    If EtqCrystal = "1" Then
    
        ' -----------------------------------------------
        
        'Imprime o relat�rio no Crystal Reports
                
        Set Report = forms(0).Controls("Report")
                
        Report.SQLQuery = "SELECT " & _
                          "     T_UTENTE, " & _
                          "     UTENTE, " & _
                          "     N_PROC, " & _
                          "     N_REQ, " & _
                          "     T_URG, " & _
                          "     DT_REQ, " & _
                          "     ORDEM, " & _
                          "     PRODUTO, " & _
                          "     NOME_UTE, " & _
                          "     N_REQ_BAR " & _
                          "FROM " & _
                          "     SL_CR_ETIQ" & gNumeroSessao & " SL_CR_ETIQ"
                                            
        For i = 0 To Printers.Count - 1
        
            If Printers(i).DeviceName = EcPrinterEtiq Then
            
                Report.PrinterName = Printers(i).DeviceName
                Report.PrinterDriver = Printers(i).DriverName
                Report.PrinterPort = Printers(i).Port
                                
            End If
            
        Next i
        
        If Trim(gEtiqPreview) Then
            Report.Destination = crptToWindow
            Report.WindowState = crptMaximized
            Report.PageShow Report.ReportStartPage
        Else
            Report.Destination = crptToPrinter
        End If
                        
        Call BL_ExecutaReport
                        
        If Trim(gEtiqPreview) Then
            Report.PageZoom (200)
        End If
                
        'Elimina as Tabelas criadas para a impress�o dos relat�rios
        Call BL_RemoveTabela("sl_cr_etiq" & gNumeroSessao)
        
    End If
     
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro : ImprimeEtiq (FormSepSoros) -> " & Err.Number & " : " & Err.Description & vbCrLf & sql, Me.Name, "ImprimeEtiq", True)
            Exit Sub
            Resume Next
    End Select
End Sub

Private Sub Lista_Tubos_Click()

    Call ImprimeEtiq(EcNumReq.Text, Me.Lista_Tubos.List(Lista_Tubos.ListIndex))

End Sub

Private Sub Lista_TubosPri_Click()
    Call ImprimeEtiq(EcNumReqPri.Text, Me.Lista_TubosPri.List(Lista_TubosPri.ListIndex), True)
End Sub

Private Function LerEtiqInI(Tubo_pri As Boolean) As Boolean
    Dim aux As String
    Dim nome As String
    On Error GoTo Erro
    If Tubo_pri = False Then
        nome = "Etiq_sec.ini"
    Else
        nome = "Etiq.ini"
    End If
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\" & nome
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\" & nome
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqInI = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While Mid(aux, 1, 1) <> "P"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqInI = True
    Exit Function
Erro:
End Function
Private Function EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    
    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function
    
    MyDocInfo.pDocName = "Etiquetas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function
    
    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    sWrittenData = TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    
    EtiqOpenPrinter = True

End Function

Private Function EtiqClosePrinter() As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    EtiqClosePrinter = True

End Function

Private Function EtiqPrint( _
    ByVal gr_ana As String, _
    ByVal abr_ana As String, _
    ByVal t_utente As String, _
    ByVal Utente As String, _
    ByVal n_proc As String, _
    ByVal n_req As String, _
    ByVal t_urg As String, _
    ByVal dt_req As String, _
    ByVal Produto As String, _
    ByVal nome_ute As String, _
    ByVal N_req_bar As String, _
    ByVal Designacao_tubo As String, _
    ByVal inf_complementar As String, _
    ByVal abrev_ute As String, _
    ByVal cod_etiq As String, _
    ByVal Descr_etiq_ana As String) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    If (Trim(UCase(Designacao_tubo)) = "ADMINISTRATIVA") Then
        N_req_bar = n_req
    End If

    sWrittenData = TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{GR_ANA}", gr_ana)
    sWrittenData = Replace(sWrittenData, "{ABR_ANA}", abr_ana)
    sWrittenData = Replace(sWrittenData, "{T_UTENTE}", t_utente)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{N_PROC}", n_proc)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{T_URG}", t_urg)
    sWrittenData = Replace(sWrittenData, "{DT_REQ}", dt_req)
    sWrittenData = Replace(sWrittenData, "{PRODUTO}", Produto)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", RemovePortuguese(StrConv(nome_ute, vbProperCase)))
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", N_req_bar)
    sWrittenData = Replace(sWrittenData, "{DS_TUBO}", Designacao_tubo)
    sWrittenData = Replace(sWrittenData, "{INF_COMPLEMENTAR}", inf_complementar)
    sWrittenData = Replace(sWrittenData, "{N_REQ_ARS}", "")
    sWrittenData = Replace(sWrittenData, "{ABREV_UTE}", abrev_ute)
    sWrittenData = Replace(sWrittenData, "{COD_ETIQ}", cod_etiq)
    sWrittenData = Replace(sWrittenData, "{DESCR_ETIQ_ANA}", Descr_etiq_ana)
    sWrittenData = Replace(sWrittenData, "{DT_IMP}", Bg_DaData_ADO)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True
    If gImprimeEPL = mediSim Then
        EtiqEndJob = Replace(EtiqEndJob, "{NUM_COPIAS}", 1)
        EtiqEndJob = Replace(EtiqEndJob, "{QUANTIDADE}", 1)
    End If

End Function


Private Function TrocaBinarios(ByVal s As String) As String
    
    Dim p As Long
    Dim k As Byte
    
    Do
        p = InStr(s, "&H")
        If p = 0 Then Exit Do
        k = val(Mid(s, p, 4))
        s = left(s, p - 1) + Chr(k) + Mid(s, p + 4)
    Loop
    TrocaBinarios = s

End Function

Private Function RemovePortuguese(ByVal s As String) As String
    
    Dim r As String
    
    Do While s <> ""
        Select Case left(s, 1)
            Case "�", "�", "�", "�"
                r = r + "a"
            Case "�", "�", "�", "�"
                r = r + "A"
            Case "�", "�", "�"
                r = r + "e"
            Case "�", "�", "�"
                r = r + "E"
            Case "�", "�", "�"
                r = r + "i"
            Case "�", "�", "�", "�"
                r = r + "o"
            Case "�", "�", "�", "�"
                r = r + "O"
            Case "�", "�", "�"
                r = r + "u"
            Case "�", "�", "�"
                r = r + "U"
            Case "�"
                r = r + "c"
            Case "�"
                r = r + "C"
            Case Else
                r = r + left(s, 1)
        End Select
        s = Mid(s, 2)
    Loop
    
    RemovePortuguese = r

End Function




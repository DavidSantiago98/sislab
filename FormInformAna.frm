VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form FormInformAna 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormInformAna"
   ClientHeight    =   6780
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7980
   Icon            =   "FormInformAna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6780
   ScaleWidth      =   7980
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.ImageList ImageListFormat 
      Left            =   360
      Top             =   3000
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormInformAna.frx":000C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormInformAna.frx":686E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormInformAna.frx":D0D0
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormInformAna.frx":13932
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormInformAna.frx":1A194
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormInformAna.frx":209F6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.TextBox EcLocal 
      Height          =   285
      Left            =   6120
      TabIndex        =   27
      Top             =   8160
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      TabIndex        =   26
      Top             =   7800
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1320
      TabIndex        =   25
      Top             =   7800
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      TabIndex        =   24
      Top             =   8160
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1320
      TabIndex        =   23
      Top             =   8160
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6120
      TabIndex        =   22
      Top             =   7800
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   240
      TabIndex        =   19
      Top             =   3990
      Width           =   7455
   End
   Begin VB.Frame Frame2 
      Height          =   795
      Left            =   240
      TabIndex        =   11
      Top             =   5760
      Width           =   7485
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   17
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   16
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   15
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   14
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   195
         Width           =   675
      End
   End
   Begin RichTextLib.RichTextBox EcTexto 
      Height          =   2055
      Left            =   1080
      TabIndex        =   10
      Top             =   1680
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   3625
      _Version        =   393217
      TextRTF         =   $"FormInformAna.frx":27258
   End
   Begin VB.ComboBox CbTipoInformacao 
      Height          =   315
      Left            =   5040
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   120
      Width           =   2655
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   1080
      TabIndex        =   5
      Top             =   150
      Width           =   1425
   End
   Begin VB.TextBox EcDescricao 
      Height          =   285
      Left            =   1080
      TabIndex        =   4
      Top             =   660
      Width           =   6555
   End
   Begin VB.Frame Frame1 
      Height          =   555
      Left            =   1080
      TabIndex        =   0
      Top             =   1080
      Width           =   6615
      Begin VB.ComboBox EcTamanho 
         Height          =   315
         Left            =   3240
         TabIndex        =   2
         Top             =   180
         Width           =   855
      End
      Begin VB.ComboBox EcFonte 
         Height          =   315
         ItemData        =   "FormInformAna.frx":272DA
         Left            =   120
         List            =   "FormInformAna.frx":272DC
         Sorted          =   -1  'True
         TabIndex        =   1
         Top             =   180
         Width           =   3015
      End
      Begin MSComctlLib.Toolbar ToolbarFormatacao 
         Height          =   390
         Left            =   4200
         TabIndex        =   3
         Top             =   120
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         ImageList       =   "ImageListFormat"
         DisabledImageList=   "ImageListFormat"
         HotImageList    =   "ImageListFormat"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   8
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "bold"
               ImageIndex      =   4
               Style           =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "italic"
               ImageIndex      =   5
               Style           =   1
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "underline"
               ImageIndex      =   6
               Style           =   1
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "left"
               ImageIndex      =   2
               Style           =   2
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "center"
               ImageIndex      =   1
               Style           =   2
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "right"
               ImageIndex      =   3
               Style           =   2
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
         EndProperty
      End
   End
   Begin VB.Label Label13 
      Caption         =   "Local"
      Height          =   225
      Index           =   0
      Left            =   4560
      TabIndex        =   33
      Top             =   8160
      Visible         =   0   'False
      Width           =   1605
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   -240
      TabIndex        =   32
      Top             =   7800
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   -240
      TabIndex        =   31
      Top             =   8160
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2640
      TabIndex        =   30
      Top             =   7800
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2400
      TabIndex        =   29
      Top             =   8160
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   4560
      TabIndex        =   28
      Top             =   7800
      Visible         =   0   'False
      Width           =   1605
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   3030
      TabIndex        =   21
      Top             =   3720
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   270
      TabIndex        =   20
      Top             =   3720
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Informa��o"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   18
      Top             =   1800
      Width           =   615
   End
   Begin VB.Label Label3 
      Caption         =   "Tipo de Informa��o"
      Height          =   255
      Index           =   1
      Left            =   3360
      TabIndex        =   8
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo "
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o "
      Height          =   255
      Left            =   150
      TabIndex        =   6
      Top             =   690
      Width           =   855
   End
End
Attribute VB_Name = "FormInformAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 06/08/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset
Dim Objecto As Control


Private Sub EcInformacao_Change()

End Sub

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub

Private Sub EcCodigo_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodigo_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)

End Sub

Private Sub EcDescricao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDescricao_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Informa��es "
    Me.left = 540
    Me.top = 450
    Me.Width = 8070
    Me.Height = 7200 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_informacao"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 9
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_informacao"
    CamposBD(1) = "cod_informacao"
    CamposBD(2) = "descr_informacao"
    CamposBD(3) = "user_cri"
    CamposBD(4) = "dt_cri"
    CamposBD(5) = "user_act"
    CamposBD(6) = "dt_act"
    CamposBD(7) = "informacao"
    CamposBD(8) = "cod_t_informacao"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcUtilizadorCriacao
    Set CamposEc(4) = EcDataCriacao
    Set CamposEc(5) = EcUtilizadorAlteracao
    Set CamposEc(6) = EcDataAlteracao
    Set CamposEc(7) = EcTexto
    Set CamposEc(8) = CbTipoInformacao
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo da Informa��o"
    TextoCamposObrigatorios(2) = "Descri��o da Informa��o"
    TextoCamposObrigatorios(7) = "Informa��o"
    TextoCamposObrigatorios(8) = "Tipo da Informa��o"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_informacao"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_informacao", "descr_informacao")
    NumEspacos = Array(22, 32)
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormInformAna = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    
    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    

End Sub

Sub PreencheCampos()
    Dim i As Integer
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
    End If

End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY descr_informacao ASC, cod_informacao ASC "
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
        
    gSQLError = 0
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_informacao") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
          
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
    
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal

End Sub

Sub FuncaoRemover()
    ' NADA
End Sub

Sub BD_Delete()
    ' NADA
End Sub


Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_tbf_t_informacao", "cod_t_informacao", "descr_t_informacao", CbTipoInformacao, mediAscComboDesignacao
    Dim i As Integer
     For i = 0 To Screen.FontCount - 1
        EcFonte.AddItem Screen.Fonts(i)
    Next
    
    For i = 8 To 12
        EcTamanho.AddItem i
    Next i
    
    For i = 14 To 28 Step 2
        EcTamanho.AddItem i
    Next i
    
    EcTamanho.AddItem 36
    EcTamanho.AddItem 48
    EcTamanho.AddItem 72
    EcFonte = "Times New Roman"
    EcTamanho = "10"
    
End Sub



Private Sub ToolbarFormatacao_ButtonClick(ByVal Button As MSComctlLib.Button)
        On Error GoTo TrataErro

    If Me.ActiveControl.Name <> "EcTexto" And Me.ActiveControl.Name <> "EcConclusao" Then
        Exit Sub
    Else
        Set Objecto = Me.ActiveControl
    End If
    
    Select Case Button.Key
        Case "bold"
            If Button.value = tbrUnpressed Then
                If Not IsNull(Objecto.SelBold) Then Objecto.SelBold = False
            Else
                Objecto.SelBold = True
            End If
        Case "italic"
            If Button.value = tbrUnpressed Then
                If Not IsNull(Objecto.SelItalic) Then Objecto.SelItalic = False
            Else
                Objecto.SelItalic = True
            End If
        Case "underline"
            If Button.value = tbrUnpressed Then
                If Not IsNull(Objecto.SelUnderline) Then Objecto.SelUnderline = False
            Else
                Objecto.SelUnderline = True
            End If
        Case "left"
            If Button.value <> tbrUnpressed Then
                Objecto.SelAlignment = rtfLeft
            End If
        Case "center"
            If Button.value <> tbrUnpressed Then
                Objecto.SelAlignment = rtfCenter
            End If
        Case "right"
            If Button.value <> tbrUnpressed Then
                Objecto.SelAlignment = rtfRight
            End If
        Case Else
            'Nada
    End Select
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub


Sub EcTexto_GotFocus()
    Dim Control As Control
    
    Set CampoActivo = Me.ActiveControl
    
    On Error Resume Next
    
    For Each Control In Controls
        Control.TabStop = False
    Next Control
    
    Set Objecto = Me.ActiveControl
End Sub





Sub EcTexto_SelChange()
    
    
    
    If Not IsNull(EcTexto.SelFontName) Then
        EcFonte.Text = EcTexto.SelFontName
    End If
    
    If Not IsNull(EcTexto.SelFontSize) Then
        EcTamanho.Text = Int(EcTexto.SelFontSize)
    End If
    
    If EcTexto.SelBold Then
        ToolbarFormatacao.Buttons.item("bold").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("bold").value = tbrUnpressed
    End If
    
    If EcTexto.SelItalic Then
        ToolbarFormatacao.Buttons.item("italic").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("italic").value = tbrUnpressed
    End If
    
    If EcTexto.SelUnderline Then
        ToolbarFormatacao.Buttons.item("underline").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("underline").value = tbrUnpressed
    End If
    
    If IsNull(EcTexto.SelAlignment) Then
        ToolbarFormatacao.Buttons.item("left").value = tbrUnpressed
        ToolbarFormatacao.Buttons.item("center").value = tbrUnpressed
        ToolbarFormatacao.Buttons.item("right").value = tbrUnpressed
    ElseIf EcTexto.SelAlignment = rtfLeft Then
        ToolbarFormatacao.Buttons.item("left").value = tbrPressed
    ElseIf EcTexto.SelAlignment = rtfCenter Then
        ToolbarFormatacao.Buttons.item("center").value = tbrPressed
    ElseIf EcTexto.SelAlignment = rtfRight Then
        ToolbarFormatacao.Buttons.item("right").value = tbrPressed
    End If
End Sub
Private Sub EcFonte_Change()
    On Error Resume Next
        
    Objecto.SelFontName = EcFonte.Text
End Sub

Private Sub EcFonte_Click()
    On Error Resume Next
        
    Objecto.SelFontName = EcFonte.Text
    EcTexto.SetFocus
End Sub



Private Sub EcTamanho_Change()
      On Error Resume Next
    
    
    Objecto.SelFontSize = EcTamanho.Text
End Sub

Private Sub EcTamanho_Click()
      On Error Resume Next
    
    
    Objecto.SelFontSize = EcTamanho.Text
    EcTexto.SetFocus
End Sub




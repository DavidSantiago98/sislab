VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormAlteraEpisFacturado 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAlteraEpisFacturado"
   ClientHeight    =   3405
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9345
   Icon            =   "FormAlteraEpisFacturado.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3405
   ScaleWidth      =   9345
   ShowInTaskbar   =   0   'False
   Begin VB.OptionButton OptReq 
      Caption         =   "Marca��o"
      Height          =   255
      Index           =   1
      Left            =   3000
      TabIndex        =   25
      Top             =   120
      Width           =   1335
   End
   Begin VB.OptionButton OptReq 
      Caption         =   "Requisi��o"
      Height          =   255
      Index           =   0
      Left            =   1320
      TabIndex        =   24
      Top             =   120
      Width           =   1335
   End
   Begin VB.CheckBox CkReenviaFact 
      Caption         =   "Permitir Reenviar para factura��o"
      Height          =   195
      Left            =   240
      TabIndex        =   23
      Top             =   2400
      Width           =   2775
   End
   Begin VB.TextBox EcEstado 
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   315
      Left            =   2640
      Locked          =   -1  'True
      TabIndex        =   20
      Top             =   480
      Width           =   4935
   End
   Begin VB.TextBox EcDataChegada 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1440
      TabIndex        =   17
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton BtValida 
      Enabled         =   0   'False
      Height          =   615
      Left            =   8160
      Picture         =   "FormAlteraEpisFacturado.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   16
      ToolTipText     =   "Actualizar situa��o e epis�dio"
      Top             =   600
      Width           =   855
   End
   Begin VB.ComboBox CbSituacao 
      Height          =   315
      Left            =   6000
      Style           =   2  'Dropdown List
      TabIndex        =   14
      Top             =   960
      Width           =   1575
   End
   Begin VB.TextBox EcNumReq 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   1440
      TabIndex        =   11
      Top             =   480
      Width           =   1215
   End
   Begin VB.TextBox EcEpisodio 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3600
      TabIndex        =   10
      Top             =   960
      Width           =   1335
   End
   Begin VB.Frame Frame2 
      Caption         =   "Dados do Utente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   975
      Left            =   120
      TabIndex        =   0
      Top             =   1320
      Width           =   9135
      Begin VB.ComboBox CbTipoUtente 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   240
         Width           =   1215
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6720
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   1095
      End
      Begin VB.TextBox EcUtente 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   240
         Width           =   3015
      End
      Begin VB.TextBox EcNome 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   600
         Width           =   4215
      End
      Begin VB.TextBox EcProcHosp1 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7440
         Locked          =   -1  'True
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Data de Entrada"
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label LbData 
         AutoSize        =   -1  'True
         Caption         =   "Data Nasc."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5760
         TabIndex        =   9
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   795
      End
      Begin VB.Label LbProcesso 
         AutoSize        =   -1  'True
         Caption         =   "Processo Hospitalar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5760
         TabIndex        =   8
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   7
         Top             =   240
         Width           =   465
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   6
         Top             =   600
         Width           =   405
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   22
      Top             =   3030
      Width           =   9345
      _ExtentX        =   16484
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label2 
      Caption         =   "Registo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   21
      Top             =   2760
      Width           =   1095
   End
   Begin VB.Label LaCriacao 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   1440
      TabIndex        =   19
      Top             =   2760
      Width           =   7695
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "&Data Chegada"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   360
      TabIndex        =   18
      Top             =   960
      Width           =   1020
   End
   Begin VB.Label Label4 
      Caption         =   "&Situa��o"
      Height          =   255
      Left            =   5160
      TabIndex        =   15
      Top             =   960
      Width           =   975
   End
   Begin VB.Label LbNumero 
      AutoSize        =   -1  'True
      Caption         =   "&Requisi��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   360
      TabIndex        =   13
      Top             =   480
      Width           =   900
   End
   Begin VB.Label LbAdmin 
      AutoSize        =   -1  'True
      Caption         =   "&Epis�dio"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   2880
      TabIndex        =   12
      Top             =   960
      Width           =   840
   End
End
Attribute VB_Name = "FormAlteraEpisFacturado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' soliveira hcvp
' Actualiza��o : 10/04/2008
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset
Dim t_sit_old As String
Dim n_epis_old As String


Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    OptReq(0).value = True
    BL_InicioProcessamento Me, "Inicializar �cran."
    Call Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    Call PreencheValoresDefeito
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow


End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    
    Set FormAlteraEpisFacturado = Nothing
    
End Sub

Sub Inicializacoes()

    Me.caption = " Altera��o de epis�dio e tipo de epis�dio"
    Me.left = 400
    Me.top = 20
    Me.Width = 9435
    Me.Height = 3315 ' Normal
    'Me.Height = 8700 ' Campos Extras
    
    Set CampoDeFocus = EcNumReq
    

End Sub

 Sub DefTipoCampos()
    
    EcNumReq.Tag = adDecimal
    EcEpisodio.Tag = adDecimal
    
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    ' Inicializa a status bar.
    StatusBar1.Panels(1).Width = Width
    StatusBar1.Panels(1).Alignment = sbrLeft
    StatusBar1.Panels(1).Style = sbrText
    StatusBar1.Panels(1).Text = " " & caption

 End Sub
 

Private Sub BtValida_Click()
    FuncaoModificar
End Sub

Private Sub EcNumReq_Click()

    EcNumReq.Text = ""
    DoEvents
    
End Sub

Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    If EcNumReq <> "" And KeyCode = 13 Then
        FuncaoProcurar
    End If
End Sub

Private Sub EcNumReq_LostFocus()
    
    EcNumReq.Tag = adDecimal
    If Not BG_ValidaTipoCampo_ADO(Me, EcNumReq) Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub


Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub FuncaoProcurar()
    
    On Error Resume Next
    
    Me.BtValida.Enabled = False
    DoEvents
    
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    DoEvents
    
    If (Trim(EcNumReq.Text) <> "") Then
                
        If (Not Preenche_Dados_Requisicao) Then
            Call FuncaoLimpar
            Exit Sub
        End If
                
        BtValida.Enabled = True
        
        DoEvents
    
    Else
        MsgBox "O n�mero de requisi��o � obrigat�rio.     ", vbExclamation, Me.caption
        Me.EcNumReq.SetFocus
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    End If
    

End Sub
    
' ---------------------------------------------------------------------------

' PREENCHE TODOS OS DADOS DA REQUISICAO E DO UTENTE EM CAUSA

' ---------------------------------------------------------------------------

Function Preenche_Dados_Requisicao() As Boolean
    
    On Error GoTo TrataErro
    
    Dim sSql As String
    Dim rsReq As ADODB.recordset
    Dim RsFact As ADODB.recordset
    Dim SelTotal As Boolean
    Dim i As Integer
    Preenche_Dados_Requisicao = True
    
    
    If EcNumReq.Text <> "" Then
        
        sSql = "SELECT  x2.n_proc_1, x1.estado_req, x1.flg_facturado, "
        sSql = sSql & " x1.dt_chega, x1.user_cri cod_user_cri, x1.dt_cri, x1.hr_cri, x1.n_epis, x1.t_sit, "
        sSql = sSql & " x2.utente, x2.t_utente, x2.dt_nasc_ute, x2.nome_ute, x3.nome nome_user_cri "
        If OptReq(0).value = True Then
            sSql = sSql & " FROM sl_requis x1, sl_identif x2, sl_idutilizador x3 "
        Else
            sSql = sSql & " FROM sl_requis_consultas x1, sl_identif x2, sl_idutilizador x3 "
        End If
        sSql = sSql & " WHERE x1.seq_utente = x2.seq_utente AND x1.user_cri = x3.cod_utilizador and x1.n_req = " & EcNumReq

        Set rsReq = New ADODB.recordset
        rsReq.CursorLocation = adUseServer
        rsReq.CursorType = adOpenStatic
        rsReq.Open sSql, gConexao
        
        If (rsReq.RecordCount <= 0) Then
            Preenche_Dados_Requisicao = False
            MsgBox "Requisi��o inexistente.    ", vbExclamation, Me.caption
            rsReq.Close
            Set rsReq = Nothing
            Exit Function
        Else
            EcNumReq = EcNumReq
            EcDataChegada = BL_HandleNull(rsReq!dt_chega, "")
            EcEpisodio = BL_HandleNull(rsReq!n_epis, "")
            For i = 0 To CbSituacao.ListCount - 1
                If CbSituacao.ItemData(i) = BL_HandleNull(rsReq!t_sit, -1) Then
                    CbSituacao.ListIndex = i
                    Exit For
                End If
            Next
            t_sit_old = BL_HandleNull(rsReq!t_sit, "")
            n_epis_old = BL_HandleNull(rsReq!n_epis, "")
            
            CbTipoUtente.Text = BL_HandleNull(rsReq!t_utente, "")
            EcUtente.Text = BL_HandleNull(rsReq!Utente, "")
            EcProcHosp1 = BL_HandleNull(rsReq!n_proc_1, "")
            EcNome.Text = BL_HandleNull(rsReq!nome_ute, "")
            EcDataNasc = BL_HandleNull(rsReq!dt_nasc_ute, "")
            LaCriacao = BL_HandleNull(rsReq!nome_user_cri, "") & " " & BL_HandleNull(rsReq!dt_cri, "") & " " & BL_HandleNull(rsReq!hr_cri, "")

            If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Then
                sSql = "select * from sl_dados_fact where n_req = " & EcNumReq
            ElseIf UCase(HIS.nome) = UCase("SONHO") Then
                sSql = "select * from sl_analises_in where n_req = " & EcNumReq
            End If
            Set RsFact = New ADODB.recordset
            RsFact.CursorLocation = adUseServer
            RsFact.CursorType = adOpenStatic
            RsFact.Open sSql, gConexao
            If RsFact.RecordCount = 0 Then
            'If BL_HandleNull(rsReq!Flg_Facturado, "0") = "0" Then
                StatusBar1.Panels(1).Text = "Requisi��o n�o facturada!"
            Else
                StatusBar1.Panels(1).Text = "Requisi��o j� facturada com esta situac��o e epis�dio! N�o � poss�vel voltar a enviar os dados para a factura��o"
            End If
            RsFact.Close
            Set RsFact = Nothing


            Coloca_Estado_Req rsReq!estado_req
                                                        
            BL_Toolbar_BotaoEstado "Inserir", "InActivo"
            
            DoEvents
            DoEvents
            DoEvents
        
        End If
        rsReq.Close
        Set rsReq = Nothing
    End If
    Preenche_Dados_Requisicao = True

Exit Function
TrataErro:
    Call BL_LogFile_BD(Me.Name, "Preenche_Dados_Requisicao", Err.Number, Err.Description, "")
    Preenche_Dados_Requisicao = False
    Exit Function
    Resume Next
End Function

Sub Coloca_Estado_Req(estado As String)

    EcEstado = BL_DevolveEstadoReq(estado)
    
End Sub

Sub FuncaoLimpar()
    
    EcNumReq.Text = ""
    Me.EcNumReq.ForeColor = &H8000&
    EcEstado.Text = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    EcNome.Text = ""
    EcUtente.Text = ""
    EcDataNasc.Text = ""
    EcProcHosp1.Text = ""

    EcDataChegada.Text = ""
    EcEpisodio.Text = ""
    CbSituacao.ListIndex = mediComboValorNull
    
    

    StatusBar1.Panels(1).Text = " " & Me.caption
    
    Me.BtValida.Enabled = False

    
    DoEvents
    DoEvents

    EcNumReq.SetFocus

End Sub

Sub FuncaoModificar()
    
    Dim Erro As String
    Dim sql As String
    Dim RsFact As ADODB.recordset
    Dim cod_modulo As String
    
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0
    BG_BeginTransaction
    If CbSituacao.ListIndex <> mediComboValorNull And EcEpisodio.Text <> "" Then
        
        If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Then
            Select Case CbSituacao.ListIndex
                Case gT_Urgencia
                    cod_modulo = "Urgencias"
                Case gT_Consulta
                    cod_modulo = "Consultas"
                Case gT_Internamento
                    cod_modulo = "Internamentos"
                Case gT_Externo
                    cod_modulo = "Consultas"
                Case gT_Ficha_ID
                    cod_modulo = "Ficha-ID"
                Case gT_Prescricoes
                    cod_modulo = "Prescricoes"
                Case gT_LAB
                    cod_modulo = "Consultas"
                Case gT_Ambulatorio
                    cod_modulo = "Ambulatorio"
                Case gT_Acidente
                    cod_modulo = "Acidente"
                Case gT_Ambulatorio
                    cod_modulo = "Ambulatorio"
                Case gT_Cons_Inter
                    cod_modulo = "Cons-Inter"
                Case gT_Cons_Telef
                    cod_modulo = "Cons-Telef"
                Case gT_Consumos
                    cod_modulo = "Consumos"
                Case gT_Credenciais
                    cod_modulo = "Credenciais"
                Case gT_Diagnosticos
                    cod_modulo = "Diagnosticos"
                Case gT_Exame
                    cod_modulo = "Exame"
                Case gT_Fisio
                    cod_modulo = "Fisio"
                Case gT_HDI
                    cod_modulo = "Hosp-Dia"
                Case gT_Intervencao
                    cod_modulo = "Intervencao"
                Case gT_MCDT
                    cod_modulo = "Mcdt"
                Case gT_Plano_Oper
                    cod_modulo = "Plano-Oper"
                Case gT_Pre_Intern
                    cod_modulo = "Pre-Intern"
                Case gT_Prog_Cirugico
                    cod_modulo = "Prog-Cirurgico"
                Case gT_Protoc
                    cod_modulo = "Protoc"
                Case gT_Referenciacao
                    cod_modulo = "Referenciacao"
                Case gT_Reg_Oper
                    cod_modulo = "Reg-Oper"
                Case gT_Tratamentos
                    cod_modulo = "Tratamentos"
                Case Else
                    cod_modulo = ""
            End Select
            'Verifica se o novo t_episodio + episodio diz respeito ao mesmo doente, s� deixa actualiza se sim
            BL_Abre_Conexao_HIS gConnHIS, gOracle
            sql = "select e.t_doente, e.doente " & _
                    " from  sd_episodio e " & _
                    " where e.episodio = " & BL_TrataStringParaBD(EcEpisodio) & _
                    " and  e.t_episodio = " & BL_TrataStringParaBD(cod_modulo)
            Set RsFact = New ADODB.recordset
            RsFact.CursorLocation = adUseServer
            RsFact.CursorType = adOpenStatic
            RsFact.Open sql, gConnHIS
            If RsFact.RecordCount > 0 Then
                If RsFact!t_doente <> CbTipoUtente Or RsFact!doente <> EcUtente Then
                    BG_Mensagem mediMsgBox, "Novo doente: " & RsFact!t_doente & "/" & RsFact!doente & "! " & vbCrLf & " Doente seleccionado n�o corresponde ao doente da requisi��o!" & vbCrLf & "Actualiza��o n�o efectuada! "
                    Exit Sub
                End If
            Else
                BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum doente para essa situa��o e epis�dio! " & vbCrLf & " Actualiza��o n�o efectuada! "
                Exit Sub
            End If
            RsFact.Close
            Set RsFact = Nothing
            BL_Fecha_conexao_HIS
            
            If CkReenviaFact.value = vbChecked Then
                If GH_EnviaParaHistorico(EcNumReq) = False Then
                    GoTo Trata_Erro
                End If
            End If
            
            'Sql = "UPDATE sl_dados_fact set t_episodio = " & _
            '        BL_TrataStringParaBD(cod_modulo) & ", episodio = " & _
            '        BL_TrataStringParaBD(EcEpisodio) & " WHERE n_req = " & EcNumReq
            'BG_ExecutaQuery_ADO Sql
            
        ElseIf UCase(HIS.nome) = UCase("SONHO") Then
            ' Mapeia a situa��o do SISLAB para o SONHO.
            Select Case CbSituacao.ItemData(CbSituacao.ListIndex)
                Case gT_Urgencia
                    cod_modulo = "URG"
                Case gT_Consulta
                    cod_modulo = "CON"
                Case gT_Internamento
                    cod_modulo = "INT"
                Case gT_Externo
                    cod_modulo = "HDI"
                Case gT_LAB
                    cod_modulo = "LAB"
                Case gT_RAD
                    cod_modulo = "RAD"
                Case gT_Bloco
                    cod_modulo = "BLO"
                Case Else
                    cod_modulo = ""
            End Select
            
            If CkReenviaFact.value = vbChecked And OptReq(0).value = True Then
                If SONHO_EnviaParaHistorico(EcNumReq) = False Then
                    GoTo Trata_Erro
                End If
            End If
            
'            Sql = "UPDATE sl_analises_in SET cod_modulo = " & _
'                    BL_TrataStringParaBD(cod_modulo) & " num_episodio = " & _
'                    BL_TrataStringParaBD(EcEpisodio) & " WHERE n_req = " & EcNumReq
'            BG_ExecutaQuery_ADO Sql

        End If
        
        If gSQLError = 0 Then
            If OptReq(0).value = True Then
                sql = "UPDATE sl_requis " & _
                      "SET    n_epis = " & EcEpisodio.Text & ", " & _
                      "       t_sit = " & CbSituacao.ItemData(CbSituacao.ListIndex) & _
                      "WHERE  n_req =" & EcNumReq.Text
            Else
                sql = "UPDATE sl_requis_consultas " & _
                      "SET    n_epis = " & EcEpisodio.Text & ", " & _
                      "       t_sit = " & CbSituacao.ItemData(CbSituacao.ListIndex) & _
                      "WHERE  n_req =" & EcNumReq.Text
            End If
            BG_ExecutaQuery_ADO sql

            
            StatusBar1.Panels(1).Text = "Requisi��o alterada com sucesso"
        Else
            GoTo Trata_Erro
        End If
        
        ' ---------------------------------------------------------------------------------------------------------------------------
        ' INSERE REGISTO NA TABELA SL_REQUIS_MODIF
        ' ---------------------------------------------------------------------------------------------------------------------------
        sql = "INSERT INTO sl_requis_modif (n_req, t_sit_old, t_sit_new, n_epis_old, n_epis_new, user_cri, dt_cri, hr_cri) VALUES("
        sql = sql & EcNumReq & ", "
        sql = sql & BL_TrataStringParaBD(t_sit_old) & ", "
        sql = sql & BL_TrataStringParaBD(CbSituacao.ItemData(CbSituacao.ListIndex)) & ", "
        sql = sql & BL_TrataStringParaBD(EcEpisodio) & ", "
        sql = sql & BL_TrataStringParaBD(n_epis_old) & ", "
        sql = sql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        sql = sql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
        sql = sql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ") "
        BG_ExecutaQuery_ADO (sql)
    End If
    
    BG_CommitTransaction
    Exit Sub
Trata_Erro:
    BG_RollbackTransaction
    BG_LogFile_Erros "FuncaoModificar (FormAlteraEpisFacturado):" & Err.Number & "-" & Err.Description & "(" & Erro & ")", Me.Name, "FuncaoModificar", True
    Exit Sub
    Resume Next
End Sub

Sub PreencheValoresDefeito()

     BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao, "1"
     BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente, "1"
End Sub

Private Sub Ecepisodio_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcEpisodio)

End Sub

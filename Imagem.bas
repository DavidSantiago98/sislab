Attribute VB_Name = "Imagem"
   Option Explicit
   Dim strSQL As String
   Dim FileLength As Long  'Used in Command1 and Command2 procedures.
   Dim Numblocks As Integer
   Dim LeftOver As Long
   Dim i As Integer
   Const BlockSize = 100000
Public Sub IM_InserirImagemBD(OrigemFicheiro As String, NomeTabela As String, _
                            campoImagem As String, campoCondicao1 As String, valorCondicao1 As String, campoCondicao2 As String, valorCondicao2 As String)
                            
      Dim ByteData() As Byte   'Byte array for Blob data.
      Dim sourceFile As Integer
      Dim strSQL As String
      Dim rs As New ADODB.recordset
      Dim v_count As Integer
      On Error GoTo TrataErro
      
      v_count = 0
      While IM_InseriuImagem(v_count, valorCondicao1, valorCondicao2) = False And v_count < 20
          strSQL = "DELETE FROM " & NomeTabela & " WHERE nome_computador = " & BL_TrataStringParaBD(BG_SYS_GetComputerName)
          strSQL = strSQL & " AND " & campoCondicao2 & " = " & valorCondicao2
          BG_ExecutaQuery_ADO strSQL
          BG_Trata_BDErro
          
          strSQL = "INSERT INTO " & NomeTabela & " ( " & campoCondicao1 & "," & campoCondicao2 & ", NOME_COMPUTADOR, NUM_SESSAO) VALUES(" & valorCondicao1 & "," & valorCondicao2 & ", " & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", " & gNumeroSessao & " )"
          BG_ExecutaQuery_ADO strSQL
          BG_Trata_BDErro
          
          ' Open the BlobTable table.
          strSQL = "Select grafico FROM " & NomeTabela & _
                    " WHERE NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador) & " and " & campoCondicao1 & " = " & valorCondicao1 & _
                    " AND " & campoCondicao2 & " = " & (valorCondicao2)
          Set rs = New ADODB.recordset
          rs.CursorType = adOpenKeyset
          rs.LockType = adLockOptimistic
          rs.Open strSQL, gConexao
          If rs.RecordCount > 0 Then
            rs.MoveFirst
        
            If gSGBD = gOracle Then
                ' Save Picture image to the table column.
                sourceFile = FreeFile
                Open OrigemFicheiro For Binary As sourceFile
                FileLength = LOF(sourceFile)  ' Get the length of the file.
                'Debug.Print "Filelength is " & FileLength
            
                If FileLength = 0 Then
                    Close sourceFile
                    Exit Sub
                Else
            
                    Numblocks = FileLength / BlockSize
                    LeftOver = FileLength Mod BlockSize
            
                    ReDim ByteData(LeftOver)
                    Get sourceFile, , ByteData()
                    rs(0).AppendChunk ByteData()
            
                    ReDim ByteData(BlockSize)
                    For i = 1 To Numblocks
                        Get sourceFile, , ByteData()
                        rs(0).AppendChunk ByteData()
                    Next i
            
                    rs.Update   'Commit the new data.
            
                   Close sourceFile
                End If
            ElseIf gSGBD = gSqlServer Then
                Dim mstream As New ADODB.Stream
                ReDim ByteData(FileLen(OrigemFicheiro))
                mstream.Type = adTypeBinary
                mstream.Open
                mstream.LoadFromFile OrigemFicheiro
                rs.Fields("grafico").value = mstream.Read
                rs.Update
'                Get SourceFile, , ByteData
'                Close SourceFile
'
'                ' Save to db
'                RS.AddNew
'                RS("grafico").AppendChunk ByteData
'                RS.Update
            
            End If
        End If
        rs.Close
        v_count = v_count + 1
    Wend
    Exit Sub
    
TrataErro:
    rs.Close
    Exit Sub
    Resume Next
End Sub
Public Sub IM_RetiraImagemBD(origemImagem As String, sSql As String, Picture1 As PictureBox)
      
      Dim ByteData() As Byte   'Byte array for picture file.
      Dim DestFileNum As Integer
      Dim DiskFile As String
      Dim rs As New ADODB.recordset
      Dim sql As String
      On Error GoTo TrataErro
      

      Set rs = New ADODB.recordset
      rs.CursorType = adOpenKeyset
      rs.LockType = adLockOptimistic
      rs.Open sSql, gConexao
      If rs.RecordCount >= 1 Then
          ' Remove any existing destination file.
          If Len(Dir$(origemImagem)) > 0 Then
             Kill origemImagem
          End If
    
          DestFileNum = FreeFile
          Open origemImagem For Binary As DestFileNum
    
          Numblocks = FileLength / BlockSize
          LeftOver = FileLength Mod BlockSize
          ReDim ByteData(BlockSize)
          
          ByteData() = rs(0).GetChunk(rs(0).ActualSize)
          Put DestFileNum, , ByteData()
    
'          For i = 1 To Numblocks
'              ByteData() = rs(0).GetChunk(BlockSize)
'              Put DestFileNum, , ByteData()
'          Next i
    
          Close DestFileNum
          DestFileNum = FreeFile
          Picture1.Visible = True
          Picture1.Picture = LoadPicture(origemImagem)
      End If
      rs.Close
      Kill origemImagem
      DoEvents
      Debug.Print "Complete"
Exit Sub
TrataErro:
    If Err.Number = 55 Then
        Close DestFileNum
        DestFileNum = FreeFile
        On Error Resume Next
        Kill origemImagem
        On Error GoTo TrataErro
        Resume Next
    ElseIf Err.Number = 53 Then
        Exit Sub
    End If
    BG_LogFile_Erros "IM_RetiraImagemBD: " & Err.Number & " - " & Err.Description, "IM", "IM_RetiraImagemBD"
    Exit Sub
    Resume Next
End Sub


Private Function IM_InseriuImagem(contador As Integer, NReq As String, cod_ana_c As String) As Boolean
    Dim sSql As String
    Dim rs As New ADODB.recordset
    If InStr(1, cod_ana_c, "'") = 0 Then
        cod_ana_c = BL_TrataStringParaBD(cod_ana_c)
    End If
    sSql = "SELECT * FROM sl_res_grafico_im WHERE grafico is not null and n_req = " & NReq & " AND complexa = " & (cod_ana_c)
    sSql = sSql & " AND nome_computador = " & BL_TrataStringParaBD(BG_SYS_GetComputerName)
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sSql, gConexao
    If rs.RecordCount >= 1 Then
        IM_InseriuImagem = True
    Else
        If contador > 0 Then
            BL_LogFile_BD "", "IM_InseriuImagem", "0", "", "NAO EXISTE NA BD  EF: - " & NReq
        End If
        IM_InseriuImagem = False
    End If
    rs.Close
End Function

Public Sub IM_RetiraImagemBDParaFicheiro(origemImagem As String, sSql As String)
      
      Dim ByteData() As Byte   'Byte array for picture file.
      Dim DestFileNum As Integer
      Dim DiskFile As String
      Dim rs As New ADODB.recordset
      Dim sql As String
      On Error GoTo TrataErro
      

      Set rs = New ADODB.recordset
      rs.CursorType = adOpenKeyset
      rs.LockType = adLockOptimistic
      rs.Open sSql, gConexao
      If rs.RecordCount >= 1 Then
          ' Remove any existing destination file.
          If Len(Dir$(origemImagem)) > 0 Then
             Kill origemImagem
          End If
    
          DestFileNum = FreeFile
          Open origemImagem For Binary As DestFileNum
    
          Numblocks = FileLength / BlockSize
          LeftOver = FileLength Mod BlockSize
          ReDim ByteData(BlockSize)
          
          ByteData() = rs(0).GetChunk(rs(0).ActualSize)
          Put DestFileNum, , ByteData()
    
    
          Close DestFileNum
          DestFileNum = FreeFile
      End If
      rs.Close
      DoEvents
      Debug.Print "Complete"
Exit Sub
TrataErro:
    If Err.Number = 55 Then
        Close DestFileNum
        DestFileNum = FreeFile
        On Error Resume Next
        Kill origemImagem
        On Error GoTo TrataErro
        Resume Next
    ElseIf Err.Number = 53 Then
        Exit Sub
    End If
    BG_LogFile_Erros "IM_RetiraImagemBD: " & Err.Number & " - " & Err.Description, "IM", "IM_RetiraImagemBDParaFicheiro"
    Exit Sub
    Resume Next
End Sub

Public Sub IM_ApagaResGraficoBanda(n_req As Long, cod_ana As String)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iReg As Integer
    
    sSql = "UPDATE sl_reS_grafico_banda SET flg_invisivel = 1 WHERE n_req = " & n_req & " AND cod_ana = " & BL_TrataStringParaBD(cod_ana)
    sSql = sSql & " AND (flg_invisivel = 0 OR flg_invisivel IS NULL)"
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg > 0 Then
        sSql = "update sl_realiza set flg_grafico = 0 where n_req = " & n_req & " and cod_agrup = " & BL_TrataStringParaBD(cod_ana)
        BG_ExecutaQuery_ADO sSql
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "IM_ApagaResGraficoBanda: " & Err.Number & " - " & Err.Description, "IM", "IM_ApagaResGraficoBanda"
    Exit Sub
    Resume Next
End Sub
Public Sub IM_InsereResGraficoBanda(origemImagem As String, n_req As Long, cod_ana As String, cod_t_grafico As Integer, Width As Long, Height As Long)
    Dim sSql As String
    Dim rsGra As New ADODB.recordset
    Dim sourceFile As Integer
    Dim ByteData() As Byte
    On Error GoTo TrataErro


    sSql = "INSERT INTO sl_res_grafico_banda (n_req,cod_ana,cod_t_grafico,ordem,seq_grafico_banda,height,width) "
    sSql = sSql & " VALUES ('" & n_req & "','" & cod_ana & "','0','0',seq_grafico_banda.nextval," & Height & "," & Width & ")"
    BG_ExecutaQuery_ADO sSql
    
    sSql = "update sl_realiza set flg_grafico = '1' where n_req = '" & n_req & "' and cod_agrup = " & BL_TrataStringParaBD(cod_ana)
    BG_ExecutaQuery_ADO sSql
    
    strSQL = "SELECT * FROM  sl_res_grafico_banda  WHERE n_req  = '" & n_req & "'  AND cod_ana = '" & cod_ana & "' AND cod_t_grafico = '0' "
    Set rsGra = New ADODB.recordset
    rsGra.CursorType = adOpenKeyset
    rsGra.LockType = adLockOptimistic
    rsGra.Open strSQL, gConexao

    If rsGra.RecordCount > 0 Then

        rsGra.MoveFirst

            sourceFile = FreeFile
            Open origemImagem For Binary As sourceFile
            FileLength = LOF(sourceFile)  ' Get the length of the file.
            'Debug.Print "Filelength is " & FileLength
            If FileLength = 0 Then
                Call BG_LogFile_Erros(Now & " -> Ficheiro de banda com tamanho 0 : " & origemImagem)
                Close sourceFile
                'Kill origemficheiro
                Exit Sub
            Else

                Numblocks = FileLength / BlockSize
                LeftOver = FileLength Mod BlockSize

                If LeftOver > 0 Then
                    ReDim ByteData(LeftOver - 1)
                    Get sourceFile, , ByteData()
                    rsGra.Fields("grafico").AppendChunk ByteData()
                End If
                ReDim ByteData(BlockSize - 1)

                For i = 1 To Numblocks

                    Get sourceFile, , ByteData()

                    rsGra.Fields("grafico").AppendChunk ByteData()

                Next i
                rsGra.Update   'Commit the new data.
            End If
            Close sourceFile

    End If
    rsGra.Close
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub


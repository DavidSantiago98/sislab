VERSION 5.00
Begin VB.Form FormIdentificaVet 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormIdentificaVet"
   ClientHeight    =   10365
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   15270
   Icon            =   "FormIdentificaVet.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10365
   ScaleWidth      =   15270
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcFlgAnexo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   11880
      Locked          =   -1  'True
      TabIndex        =   88
      Top             =   9960
      Width           =   375
   End
   Begin VB.TextBox EcFlgDiagPrim 
      Height          =   285
      Left            =   2040
      TabIndex        =   86
      Top             =   10800
      Width           =   375
   End
   Begin VB.TextBox EcTipoUtente 
      Height          =   285
      Left            =   13920
      TabIndex        =   81
      Top             =   9600
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcSexo 
      Height          =   285
      Left            =   11880
      TabIndex        =   79
      Top             =   9600
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcCodPostalCompleto 
      Height          =   285
      Left            =   8280
      TabIndex        =   77
      Top             =   9840
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox EcCodPostalAux 
      Height          =   285
      Left            =   8280
      TabIndex        =   75
      Top             =   10080
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4560
      TabIndex        =   74
      Top             =   9480
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   8280
      TabIndex        =   72
      Top             =   9480
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4560
      TabIndex        =   70
      Top             =   9720
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2160
      TabIndex        =   66
      Top             =   9480
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2160
      TabIndex        =   65
      Top             =   9750
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Frame Frame7 
      Height          =   855
      Left            =   120
      TabIndex        =   54
      Top             =   7200
      Width           =   15015
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   3000
         TabIndex        =   60
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   3000
         TabIndex        =   59
         Top             =   480
         Width           =   2175
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   960
         TabIndex        =   58
         Top             =   480
         Width           =   2055
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   57
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   56
         Top             =   240
         Width           =   675
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   960
         TabIndex        =   55
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   3615
      Index           =   1
      Left            =   14160
      TabIndex        =   32
      Top             =   3600
      Width           =   975
      Begin VB.CommandButton BtGravar 
         Height          =   525
         Left            =   240
         Picture         =   "FormIdentificaVet.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   85
         ToolTipText     =   "Gravar"
         Top             =   3000
         Width           =   495
      End
      Begin VB.CommandButton BtConsulta 
         Height          =   495
         Left            =   240
         Picture         =   "FormIdentificaVet.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   84
         ToolTipText     =   "Consulta de Requisi��es"
         Top             =   1200
         Width           =   495
      End
      Begin VB.CommandButton BtAdicionar 
         Height          =   495
         Left            =   240
         MaskColor       =   &H8000000F&
         Picture         =   "FormIdentificaVet.frx":15A0
         Style           =   1  'Graphical
         TabIndex        =   83
         ToolTipText     =   "Adicionar Novo Animal"
         Top             =   0
         Width           =   495
      End
      Begin VB.CommandButton BtNotas 
         Enabled         =   0   'False
         Height          =   480
         Left            =   240
         Picture         =   "FormIdentificaVet.frx":226A
         Style           =   1  'Graphical
         TabIndex        =   63
         ToolTipText     =   "Notas"
         Top             =   2400
         Width           =   495
      End
      Begin VB.CommandButton BtAnexos 
         Height          =   495
         Left            =   240
         Picture         =   "FormIdentificaVet.frx":29D4
         Style           =   1  'Graphical
         TabIndex        =   62
         ToolTipText     =   "Anexos Associados ao Utente"
         Top             =   1800
         Width           =   495
      End
      Begin VB.CommandButton BtRequisicoes 
         Height          =   495
         Left            =   240
         MaskColor       =   &H8000000F&
         Picture         =   "FormIdentificaVet.frx":313E
         Style           =   1  'Graphical
         TabIndex        =   61
         ToolTipText     =   "Gest�o de Requisi��es"
         Top             =   600
         Width           =   495
      End
      Begin VB.CommandButton BtNotasVRM 
         Enabled         =   0   'False
         Height          =   480
         Left            =   240
         Picture         =   "FormIdentificaVet.frx":3AB0
         Style           =   1  'Graphical
         TabIndex        =   64
         ToolTipText     =   "Notas"
         Top             =   2400
         Width           =   495
      End
   End
   Begin VB.Frame FrameUtente 
      BorderStyle     =   0  'None
      Height          =   3375
      Left            =   120
      TabIndex        =   31
      Top             =   3720
      Width           =   13935
      Begin VB.TextBox EcNumExt 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4920
         TabIndex        =   10
         Top             =   240
         Width           =   1215
      End
      Begin VB.TextBox EcDescrEntFinR 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         Height          =   315
         Left            =   2160
         TabIndex        =   46
         TabStop         =   0   'False
         Top             =   795
         Width           =   3615
      End
      Begin VB.TextBox EcCodEntFinR 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   1080
         TabIndex        =   11
         Top             =   795
         Width           =   1095
      End
      Begin VB.CommandButton BtPesqEntFinR 
         Height          =   315
         Left            =   5760
         Picture         =   "FormIdentificaVet.frx":421A
         Style           =   1  'Graphical
         TabIndex        =   45
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras Respons�veis"
         Top             =   795
         Width           =   375
      End
      Begin VB.TextBox EcTelefone 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   12000
         MultiLine       =   -1  'True
         TabIndex        =   20
         Top             =   720
         Width           =   1215
      End
      Begin VB.TextBox EcRuaPostal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   8400
         MaxLength       =   3
         TabIndex        =   23
         Top             =   2280
         Width           =   490
      End
      Begin VB.TextBox EcCodPostal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   7800
         MaxLength       =   4
         TabIndex        =   22
         Top             =   2280
         Width           =   615
      End
      Begin VB.TextBox EcDescrPostal 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   8880
         TabIndex        =   44
         Top             =   2280
         Width           =   3975
      End
      Begin VB.CommandButton BtPesqCodPostal 
         Height          =   315
         Left            =   12840
         Picture         =   "FormIdentificaVet.frx":47A4
         Style           =   1  'Graphical
         TabIndex        =   43
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
         Top             =   2280
         Width           =   375
      End
      Begin VB.TextBox EcMorada 
         Appearance      =   0  'Flat
         Height          =   885
         Left            =   7800
         MultiLine       =   -1  'True
         TabIndex        =   21
         Top             =   1200
         Width           =   5415
      End
      Begin VB.TextBox EcEmail 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7800
         TabIndex        =   24
         Top             =   2715
         Width           =   5415
      End
      Begin VB.TextBox EcDescrEspecie 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   2235
         Width           =   3615
      End
      Begin VB.TextBox EcCodEspecie 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   14
         Top             =   2235
         Width           =   1095
      End
      Begin VB.CommandButton BtPesqRapEspecie 
         Height          =   315
         Left            =   5760
         Picture         =   "FormIdentificaVet.frx":4D2E
         Style           =   1  'Graphical
         TabIndex        =   41
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras Respons�veis"
         Top             =   2235
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapRaca 
         Height          =   315
         Left            =   5760
         Picture         =   "FormIdentificaVet.frx":52B8
         Style           =   1  'Graphical
         TabIndex        =   40
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras Respons�veis"
         Top             =   2715
         Width           =   375
      End
      Begin VB.TextBox EcCodRaca 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   15
         Top             =   2715
         Width           =   1095
      End
      Begin VB.TextBox EcDescrRaca 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   2715
         Width           =   3615
      End
      Begin VB.TextBox EcDataInscr 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   12000
         TabIndex        =   17
         Top             =   240
         Width           =   1215
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "d/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   3
         EndProperty
         Height          =   285
         Left            =   7800
         TabIndex        =   18
         Top             =   720
         Width           =   1095
      End
      Begin VB.TextBox EcIdade 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9000
         TabIndex        =   19
         Top             =   720
         Width           =   495
      End
      Begin VB.ComboBox EcDescrSexo 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   7800
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   240
         Width           =   1695
      End
      Begin VB.ComboBox EcDescrTipoUtente 
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   12
         Top             =   1320
         Width           =   5055
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2040
         TabIndex        =   9
         Top             =   240
         Width           =   1935
      End
      Begin VB.TextBox EcNomeDono 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   13
         Top             =   1800
         Width           =   5055
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Ext."
         Height          =   195
         Left            =   4200
         TabIndex        =   90
         Top             =   240
         Width           =   645
      End
      Begin VB.Label LbEntFinResp 
         AutoSize        =   -1  'True
         Caption         =   "Entidade"
         Height          =   195
         Left            =   120
         TabIndex        =   53
         Top             =   795
         Width           =   630
      End
      Begin VB.Label LbTelefone 
         AutoSize        =   -1  'True
         Caption         =   "Telefone :"
         Height          =   195
         Index           =   0
         Left            =   11160
         TabIndex        =   52
         Top             =   720
         Width           =   720
      End
      Begin VB.Label LbCodPostal 
         AutoSize        =   -1  'True
         Caption         =   "C. Postal"
         Height          =   195
         Left            =   6720
         TabIndex        =   51
         Top             =   2280
         Width           =   630
      End
      Begin VB.Label LbMorada 
         AutoSize        =   -1  'True
         Caption         =   "Morada"
         Height          =   195
         Left            =   6720
         TabIndex        =   50
         Top             =   1200
         Width           =   540
      End
      Begin VB.Label Label20 
         AutoSize        =   -1  'True
         Caption         =   "Email"
         Height          =   195
         Left            =   6720
         TabIndex        =   49
         Top             =   2760
         Width           =   375
      End
      Begin VB.Label LbEspecie 
         Caption         =   "Esp�cie"
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   2235
         Width           =   735
      End
      Begin VB.Label LbRaca 
         Caption         =   "Ra�a"
         Height          =   255
         Left            =   120
         TabIndex        =   47
         Top             =   2715
         Width           =   735
      End
      Begin VB.Label Label12 
         Caption         =   "Dt Inscr."
         Height          =   255
         Left            =   11160
         TabIndex        =   38
         ToolTipText     =   "Data de Inscri��o"
         Top             =   240
         Width           =   735
      End
      Begin VB.Label LbDataNasc 
         AutoSize        =   -1  'True
         Caption         =   "Data Nasc."
         Height          =   195
         Left            =   6720
         TabIndex        =   37
         ToolTipText     =   "Data Nascimento"
         Top             =   720
         Width           =   810
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "G�nero"
         Height          =   195
         Left            =   6720
         TabIndex        =   36
         Top             =   240
         Width           =   525
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Animal"
         Height          =   195
         Left            =   120
         TabIndex        =   35
         Top             =   1320
         Width           =   465
      End
      Begin VB.Label LbUtente 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         Height          =   195
         Left            =   120
         TabIndex        =   34
         Top             =   240
         Width           =   480
      End
      Begin VB.Label LbNomeDono 
         AutoSize        =   -1  'True
         Caption         =   " Dono"
         Height          =   195
         Left            =   120
         TabIndex        =   33
         Top             =   1800
         Width           =   435
      End
   End
   Begin VB.Frame FramePesquisa 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   120
      TabIndex        =   25
      Top             =   0
      Width           =   15015
      Begin VB.TextBox EcDescrGeneroPesq 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   13080
         TabIndex        =   6
         Top             =   200
         Width           =   1935
      End
      Begin VB.TextBox EcCodGeneroPesq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   12600
         TabIndex        =   5
         Top             =   200
         Width           =   495
      End
      Begin VB.TextBox EcDonoPesq 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   9405
         TabIndex        =   4
         Top             =   200
         Width           =   2535
      End
      Begin VB.TextBox EcNomePesq 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   6405
         TabIndex        =   3
         Top             =   200
         Width           =   2535
      End
      Begin VB.TextBox EcDescrEfrPesq 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   3000
         TabIndex        =   2
         Top             =   200
         Width           =   2775
      End
      Begin VB.TextBox EcCodEFRPesq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   2445
         TabIndex        =   1
         Top             =   200
         Width           =   735
      End
      Begin VB.TextBox EcUtentePesq 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   640
         TabIndex        =   0
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Esp�cie"
         Height          =   195
         Index           =   4
         Left            =   12000
         TabIndex        =   30
         Top             =   225
         Width           =   570
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Dono"
         Height          =   195
         Index           =   3
         Left            =   9000
         TabIndex        =   29
         Top             =   225
         Width           =   390
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Animal"
         Height          =   195
         Index           =   2
         Left            =   5880
         TabIndex        =   28
         Top             =   225
         Width           =   465
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Entidade"
         Height          =   195
         Index           =   1
         Left            =   1800
         TabIndex        =   27
         Top             =   225
         Width           =   630
      End
      Begin VB.Label Label1 
         Caption         =   "Utente"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   26
         Top             =   220
         Width           =   615
      End
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2760
      Left            =   120
      TabIndex        =   7
      Top             =   720
      Width           =   15015
   End
   Begin VB.Label Label9 
      Caption         =   "EcFlgSexo"
      Height          =   255
      Index           =   1
      Left            =   10920
      TabIndex        =   89
      Top             =   9960
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcFlgDiagPrim"
      Enabled         =   0   'False
      Height          =   255
      Index           =   29
      Left            =   720
      TabIndex        =   87
      Top             =   10800
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label6 
      Caption         =   "EctipoUtente"
      Height          =   255
      Left            =   12840
      TabIndex        =   82
      Top             =   9600
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label9 
      Caption         =   "EcSexo"
      Height          =   255
      Index           =   0
      Left            =   10920
      TabIndex        =   80
      Top             =   9600
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label4 
      Caption         =   "EcCodPostalCompleto"
      Height          =   255
      Left            =   6480
      TabIndex        =   78
      Top             =   9840
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label7 
      Caption         =   "EcCodPostalAux"
      Height          =   255
      Left            =   6840
      TabIndex        =   76
      Top             =   10080
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label11 
      Caption         =   "EcCodSequencial"
      Height          =   255
      Left            =   6840
      TabIndex        =   73
      Top             =   9480
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label16 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   3360
      TabIndex        =   71
      Top             =   9720
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label15 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   3360
      TabIndex        =   69
      Top             =   9480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label18 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   600
      TabIndex        =   68
      Top             =   9720
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label17 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   480
      TabIndex        =   67
      Top             =   9480
      Visible         =   0   'False
      Width           =   1575
   End
End
Attribute VB_Name = "FormIdentificaVet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim CamposEcBckp() As String
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

'vari�veis que guardam o formato do n�mero de beneficiario que est� na BD
Dim Formato1 As String
Dim Formato2 As String

Public rs As ADODB.recordset

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind As Integer
Dim Max  As Integer
Dim EcUtentePesqIni As String
Dim EcCodEfrPesqIni As String
Dim EcDescrEfrPesqIni As String
Dim EcNomePesqIni As String
Dim EcDonoPesqIni As String
Dim EcCodGeneroPesqIni As String
Dim EcDescrGeneroPesqIni As String
Dim seqUtentes() As Long
Public Flg_PesqUtente As Boolean
Dim Pesquisa As Boolean

Const vermelhoClaro = &HBDC8F4
Sub FuncaoInserir()
    
    Dim iRes As Integer
    Dim msgRespAux As String
    Dim processo As Boolean
    Dim Flg_abortar As Boolean
    Dim i As Long
    On Error GoTo TrataErro
    If EcDescrSexo.ListIndex > mediComboValorNull Then
        If EcDescrSexo.ItemData(EcDescrSexo.ListIndex) <> gT_Masculino And EcDescrSexo.ItemData(EcDescrSexo.ListIndex) <> gT_Feminino Then
            If MsgBox("Sexo do Utente indetermindado. Deseja gravar?   ", vbYesNo + vbQuestion, "Sislab") <> vbYes Then
                Exit Sub
            End If
        End If
    End If
    
    If EcUtente.Visible = True And EcUtente.Enabled = True Then
        EcUtente.SetFocus
    Else
    End If
    
    Flg_abortar = False
    
    EcUtilizadorCriacao = gCodUtilizador
    EcDataCriacao = Bg_DaData_ADO
    
    
    'se a data de inscri��o estiver vazia
    If EcDataInscr.Text = "" Then
        EcDataInscr.Text = Format(Bg_DaData_ADO, gFormatoData)
    End If
   
    If Trim(EcDataNasc.Text) = "" Then
        'EcDataNasc.text = "01-01-1900"
    End If
   
   ' caso o tipo de utente nao esteja preenchido
    If EcDescrTipoUtente.ListIndex = -1 Then
        EcDescrTipoUtente.Text = gInstituicao
    End If
    

    
    'o numero do utente e o tipo do utente foram preenchidos e os processos validados
    If Flg_abortar = False Then
        gMsgTitulo = "Inserir"
        gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            BL_InicioProcessamento Me, "A inserir registo."
            iRes = ValidaCamposEc
            If iRes = True Then
                BD_Insert
                FuncaoProcurarPesquisa
                If BtRequisicoes.Enabled = True Then
                    BtRequisicoes.SetFocus
                End If
'                RefinaPesquisa EcCodSequencial.text
            End If
            BL_FimProcessamento Me
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoInserir: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoInserir"
    Exit Sub
    Resume Next
End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer
    On Error GoTo TrataErro
        
    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    ValidaCamposEc = True
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaCamposEc: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaCamposEc"
    Exit Function
    Resume Next
End Function

Sub FuncaoProcurarPesquisa(Optional SuprimeMsg As Boolean)
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    On Error GoTo TrataErro
    
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        If BG_Mensagem(mediMsgBox, "N�o colocou crit�rios de pesquisa, esta opera��o pode demorar algum tempo." & vbNewLine & "Confirma a selec��o de todos os utentes ?", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
            Set rs = Nothing
            Exit Sub
        End If
        CriterioTabela = CriterioTabela & "  ORDER BY " & ChaveBD & " DESC"
    Else
    End If
              
    ' Permite que a pesquisa n�o seja case sensitive.
    CriterioTabela = BL_Upper_Campo(CriterioTabela, _
                                    "nome_ute", _
                                    True)
    
    BL_InicioProcessamento Me, "A pesquisar registos."
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.LockType = adLockReadOnly
    
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioTabela
    rs.Open CriterioTabela, gConexao
    
    If rs.EOF Then
        If SuprimeMsg = True Then Exit Sub
        BL_FimProcessamento Me
        'BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        LimpaCampos
    Else
        estado = 2
        EcDescrTipoUtente.Enabled = False
        EcUtente.Enabled = False
        If BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AlterarDoente") <> "1" Then
            EcNome.Enabled = False
        End If
        EcDataInscr.Enabled = False
        
        LimpaCampos
        PreencheCampos
        gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    FrameUtente.Enabled = True
    BtGravar.Enabled = True
    Set gFormActivo = Me
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar"
    Exit Sub
    Resume Next
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    On Error GoTo TrataErro
    
    If rs.EOF Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheVectorBackup NumCampos, CamposEc, CamposEcBckp
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        BL_ColocaTextoCombo "sl_tbf_sexo", "cod_sexo", "cod_sexo", EcSexo, EcDescrSexo
        BL_ColocaTextoCombo "sl_t_utente", "cod_t_utente", "descr_t_utente", EcTipoUtente, EcDescrTipoUtente
        EcCodraca_Validate False
        EcCodespecie_Validate False
        PreencheDescEntFinanceira
        PreencheCodigoPostal
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        BL_VerificaAnexosUtente Me, BL_HandleNull(EcFlgAnexo.Text, mediComboValorNull)
        BtRequisicoes.Enabled = True
        BtConsulta.Enabled = True
        BtRequisicoes.SetFocus
        If EcDataNasc <> "" Then
            PreencheIdade
        End If
        BtNotas.Enabled = True
        BtNotasVRM.Enabled = True
        PreencheNotas
        BtGravar.Enabled = False
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheCampos: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheCampos"
    Exit Sub
    Resume Next
End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    Dim RsNUtente As ADODB.recordset
    Dim Query As String
    Dim i As Integer
    Dim seq As Long
    
    On Error GoTo Trata_Erro
        
    If Trim(EcUtente) = "" Then
        
        'Determinar o novo numero para o Tipo Utente (TENTA 10 VEZES - LOCK!)
        i = 0
        seq = -1
        While seq = -1 And i <= 10
            seq = BL_GeraUtente(EcDescrTipoUtente)
            i = i + 1
        Wend
        
        If seq = -1 Then
            BG_Mensagem mediMsgBox, "Erro a gerar n�mero do utente!", vbCritical + vbOKOnly, "ERRO"
            Exit Sub
        End If
        
        EcUtente = seq
    Else
        Set RsNUtente = New ADODB.recordset
        RsNUtente.CursorLocation = adUseServer
        RsNUtente.Open "SELECT nome_ute, dt_nasc_ute FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(EcDescrTipoUtente) & " AND utente = " & BL_TrataStringParaBD(EcUtente), gConexao, adOpenStatic, adLockReadOnly
        
        If RsNUtente.RecordCount > 0 Then
            
            If BG_Mensagem(mediMsgBox, "J� existe um utente do tipo " & EcDescrTipoUtente.Text & " com o n�mero " & EcUtente & "!" & Chr(13) & "Nome : " & BL_HandleNull(RsNUtente!nome_ute) & Chr(13) & "Dt. Nasc : " & BL_HandleNull(RsNUtente!dt_nasc_ute) & Chr(13) & "Deseja actualizar o utente?", vbYesNo + vbExclamation, "Inserir Utente") = vbYes Then
                Dim TUtenteAux As String
                Dim UtenteAux As String
                TUtenteAux = EcDescrTipoUtente.Text
                UtenteAux = EcUtente.Text
                LimpaCampos
                EcDescrTipoUtente.Text = TUtenteAux
                EcUtente.Text = UtenteAux
                FuncaoProcurarPesquisa (True)
            End If
            RsNUtente.Close
            Set RsNUtente = Nothing
            Exit Sub
        Else
            RsNUtente.Close
            Set RsNUtente = Nothing
        End If
    End If
        
    'Determinar o novo numero sequencial (TENTA 10 VEZES - LOCK!)
    i = 0
    seq = -1
    While seq = -1 And i <= 10
        seq = BL_GeraNumero("SEQ_UTENTE")
        i = i + 1
    Wend
    
    If seq = -1 Then
        BG_Mensagem mediMsgBox, "Erro a gerar n�mero de sequencial interno do utente!", vbCritical + vbOKOnly, "ERRO"
        Exit Sub
    End If
    
    EcCodSequencial = seq
    
    gSQLError = 0
    gSQLISAM = 0
    
    If gNomeCapitalizado = 1 Then
        EcNome.Text = StrConv(EcNome.Text, vbProperCase)
    Else
        EcNome.Text = Trim(EcNome.Text)
    End If
    
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    If gSQLError <> 0 Then
        'Erro a inserir utente
        
        GoTo Trata_Erro
    End If
    
    BtRequisicoes.Enabled = True
    BtConsulta.Enabled = True
    gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
    
    BG_CommitTransaction
    
    
Exit Sub
Trata_Erro:
    BG_LogFile_Erros "FormIdentificaVet: BD_Insert -> " & Err.Description
    BG_RollbackTransaction
    
    LimpaCampos

End Sub

Sub FuncaoModificar(Optional supress_msg As Boolean)
    
    Dim iRes As Integer
    Dim processo As Boolean
    Dim Flg_abortar As Boolean
    On Error GoTo TrataErro
    
    Flg_abortar = False
    If EcDescrSexo.ListIndex > mediComboValorNull Then
        If EcDescrSexo.ItemData(EcDescrSexo.ListIndex) <> gT_Masculino And EcDescrSexo.ItemData(EcDescrSexo.ListIndex) <> gT_Feminino Then
            If MsgBox("Sexo do Utente indetermindado. Deseja gravar?   ", vbYesNo + vbQuestion, "Sislab") <> vbYes Then
                Exit Sub
            End If
        End If
    End If
        
    If EcUtente.Visible = True And EcUtente.Enabled = True Then
        EcUtente.SetFocus
    Else
    End If
    
    EcUtilizadorAlteracao = gCodUtilizador
    EcDataAlteracao = Bg_DaData_ADO
    
    'se a data de inscri��o estiver vazia
    If EcDataInscr.Text = "" Then
        EcDataInscr.Text = Format(Bg_DaData_ADO, gFormatoData)
    End If
   
    If Trim(EcDataNasc.Text) = "" Then
        EcDataNasc.Text = "01-01-1900"
    End If
   
   ' caso o tipo de utente nao esteja preenchido
    If EcDescrTipoUtente.ListIndex = -1 Then
        EcDescrTipoUtente.ListIndex = 0
    End If
    
    
    
    
    'o numero do utente e o tipo do utente foram preenchidos e os processos validados
    If Flg_abortar = False Then
        If supress_msg = False Then
            gMsgTitulo = "Modificar"
            gMsgMsg = "Tem a certeza que quer Modificar estes dados ?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        Else
            gMsgResp = vbYes
        End If
        If gMsgResp = vbYes Then
            BL_InicioProcessamento Me, "A modificar registo."
            iRes = ValidaCamposEc
            If iRes = True Then
                BD_Update
            End If
            RefinaPesquisa EcCodSequencial.Text
            BL_FimProcessamento Me
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoModificar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoModificar"
    Exit Sub
    Resume Next
End Sub

Sub BD_Update()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim i As Long
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0
    BG_BeginTransaction
    
    If gNomeCapitalizado = 1 Then
        EcNome.Text = StrConv(EcNome.Text, vbProperCase)
    Else
        EcNome.Text = Trim(EcNome.Text)
    End If
    
    
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    If gSQLError <> 0 Then
        
        GoTo Trata_Erro
    End If
        
    BG_CommitTransaction
    
    MarcaLocal = rs.Bookmark
    rs.Requery
    
    If rs.BOF And rs.EOF Then
        EcDescrTipoUtente.Enabled = True
        EcUtente.Enabled = True
        EcNome.Enabled = True
        EcDataNasc.Enabled = True
        EcDataInscr.Enabled = True
        FuncaoEstadoAnterior
        Exit Sub
    End If
    rs.Bookmark = MarcaLocal
    
    LimpaCampos
    PreencheCampos
    
    
    Exit Sub

Trata_Erro:
    BG_LogFile_Erros "FormIdentificaVet: BD_update -> " & Err.Description & " " & SQLQuery
    BG_RollbackTransaction

End Sub

Sub FuncaoLimpar()
    On Error GoTo TrataErro
    
    Set CampoDeFocus = EcUtentePesq
    EcDescrTipoUtente.Enabled = True
    EcUtente.Enabled = True
    EcNome.Enabled = True
    EcDataNasc.Enabled = True
    EcDataInscr.Enabled = True
        
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
    End If
    LimpaCamposPesquisa
    Set CampoDeFocus = EcUtentePesq
    CampoDeFocus.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoLimpar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoLimpar"
    'Exit Sub
    Resume Next
End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        'CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Sub FuncaoRemover()
    On Error GoTo TrataErro
    
    If EcCodSequencial <> "" Then
        If BL_VfUteResultados(EcCodSequencial) = True Then
            gMsgTitulo = "Remo��o n�o autorizada."
            gMsgMsg = "Utente " & EcDescrTipoUtente.List(EcDescrTipoUtente.ListIndex) & "/" & EcUtente & " j� tem resultados!"
            BG_Mensagem mediMsgBox, gMsgMsg, vbOKOnly + vbInformation, gMsgTitulo
            Exit Sub
        End If
    End If
    
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?" & Chr(13) & "Todos os dados pertencentes ao utente ser�o eliminados!"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoRemover: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoRemover"
    Exit Sub
    Resume Next
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim SQLRmRel As String
    Dim RsRmRel As ADODB.recordset
    Dim ReqsUte As String
    Dim MarcaLocal As Variant
           
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0
    BG_BeginTransaction
    
    'Seleccionar as requisi��es do utente sem resultado
    Set RsRmRel = New ADODB.recordset
    SQLRmRel = "( SELECT n_req FROM sl_requis WHERE seq_utente = " & rs(ChaveBD) & " )" & _
            "UNION (SELECT n_req FROM sl_falt_req WHERE seq_utente = " & rs(ChaveBD) & " )"
    With RsRmRel
        .Source = SQLRmRel
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLRmRel
        .Open , gConexao
    End With
    If RsRmRel.EOF Then
        ReqsUte = ""
    Else
        ReqsUte = " ( "
        While Not RsRmRel.EOF
            ReqsUte = ReqsUte & BL_HandleNull(RsRmRel!n_req, 0) & ","
            RsRmRel.MoveNext
        Wend
        ReqsUte = Mid(ReqsUte, 1, Len(ReqsUte) - 1) & ")"
    End If
    
    RsRmRel.Close
    Set RsRmRel = Nothing
    
    gSQLError = 0
    If ReqsUte <> "" Then
        'Remover requisi��es do utente
        SQLRmRel = "DELETE FROM sl_requis WHERE seq_utente = " & rs(ChaveBD)
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
    
        'Remover diagn�sticos principais do utente
        SQLRmRel = "DELETE FROM sl_diag_pri WHERE seq_utente = " & rs(ChaveBD)
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        'Remover requisi��es em que o utente faltou
        SQLRmRel = "DELETE FROM sl_falt_req WHERE seq_utente = " & rs(ChaveBD)
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro

        'Remover marca��es do utente
        SQLRmRel = "DELETE FROM sl_marcacoes WHERE n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        'Remover diagn�sticos secund�rios do utente
        SQLRmRel = "DELETE FROM sl_diag_sec WHERE n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        'Remover produtos de requisi��es em que o utente faltou
        SQLRmRel = "DELETE FROM sl_falt_prod WHERE n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        'Remover recibos do utente
        SQLRmRel = "DELETE FROM " & _
                   "        sl_recibos " & _
                   "WHERE " & _
                   "        n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        
        'Remover requisi��es canceladas do utente
        SQLRmRel = "DELETE FROM sl_req_canc WHERE n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        'Remover produtos das requisi��es do utente
        SQLRmRel = "DELETE FROM sl_req_prod WHERE n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
        
        'Remover terap�uticas e medica��o do utente
        SQLRmRel = "DELETE FROM sl_tm_ute WHERE n_req IN " & ReqsUte
        BG_ExecutaQuery_ADO SQLRmRel
        If gSQLError <> 0 Then GoTo Trata_Erro
    End If
        
    condicao = ChaveBD & " = " & rs(ChaveBD)
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    If gSQLError <> 0 Then GoTo Trata_Erro

    BG_CommitTransaction
    
    'temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        EcDescrTipoUtente.Enabled = True
        EcUtente.Enabled = True
        EcNome.Enabled = True
        EcDataNasc.Enabled = True
        EcDataInscr.Enabled = True
        Set CampoDeFocus = EcUtentePesq
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

    Exit Sub
    
Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro a eliminar utente!", vbOKOnly + vbCritical, App.ProductName
    BG_LogFile_Erros "FormIdentificaVet: BD_Delete -> SQL(" & gSQLError & ") " & Err.Description
    BG_RollbackTransaction
    
    LimpaCampos

End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
        BL_FimProcessamento Me
    End If

End Sub

Sub LimpaCampos()
    On Error GoTo TrataErro
    Me.SetFocus
    
    Set CampoDeFocus = EcUtentePesq
    BG_LimpaCampo_Todos CamposEc
    EcDescrSexo.ListIndex = -1
    EcDescrTipoUtente.ListIndex = -1
    
    EcCodSequencial.Text = ""
    EcCodPostal.Text = ""
    EcRuaPostal.Text = ""
    EcDescrPostal.Text = ""
    EcDescrEntFinR.Text = ""
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    EcCodPostalCompleto.Text = ""
    EcCodPostalAux.Text = ""
    Formato1 = ""
    Formato2 = ""
    
    BtRequisicoes.Enabled = False
    BtConsulta.Enabled = False
    EcUtente.Text = ""
    gEpisodioActivo = ""
    gEpisodioActivoAux = ""
    EcIdade.Text = ""
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    BtNotas.Enabled = False
    BtNotasVRM.Enabled = False
    EcDescrEspecie = ""
    EcDescrRaca = ""
    BtGravar.Enabled = False
    BL_VerificaAnexosUtente Me, mediComboValorNull
 Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaCampos: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaCampos"
    Exit Sub
    Resume Next
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    Max = -1
    ReDim FOPropertiesTemp(0)
    gF_IDENTIF_VET = 1
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
        
    estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    BL_FimProcessamento Me
    
    Flg_PesqUtente = False
    
End Sub

Sub EventoActivate()
    Dim RsCodEFR As ADODB.recordset
    
    On Error GoTo Trata_Erro
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    If CampoDeFocus.Enabled = True Then
        CampoDeFocus.SetFocus
    End If
    
    If FormIdentificaVet.Enabled = False Then
        FormIdentificaVet.Enabled = True
    End If
    
    
    'Foi pesquisado um utente na form requisicoes mas n�o foi encontrado
    If gDUtente.seq_utente = "" And Flg_PesqUtente = True Then
        If gDUtente.descr_sexo_ute <> "" Or gDUtente.dt_nasc_ute <> "" Or _
            gDUtente.n_cartao_ute <> "" Or gDUtente.n_proc_1 <> "" Or _
            gDUtente.n_proc_2 <> "" Or gDUtente.nome_ute <> "" Or _
            gDUtente.sexo_ute <> "" Or gDUtente.t_utente <> "" Or gDUtente.Utente <> "" Then
            
            'Pesquisa de utente falhada
            FuncaoLimpar
            EcDataNasc = gDUtente.dt_nasc_ute
            EcNome = gDUtente.nome_ute
            EcSexo = gDUtente.sexo_ute
            BL_ColocaTextoCombo "sl_tbf_sexo", "cod_sexo", "cod_sexo", EcSexo, EcDescrSexo
            EcTipoUtente = gDUtente.t_utente
            EcDescrTipoUtente = gDUtente.t_utente
            EcUtente = gDUtente.Utente
            EcEmail = gDUtente.email
            EcCodEspecie = gDUtente.cod_genero
            EcCodespecie_Validate False
            Flg_PesqUtente = False
        End If
    ElseIf gDUtente.seq_utente <> "" And Flg_PesqUtente = True Then
        If gDUtente.seq_utente = "-1" Then
            ' UTENTE HIS
            FuncaoLimpar
            EcDataNasc = gDUtente.dt_nasc_ute
            EcNome = gDUtente.nome_ute
            EcSexo = gDUtente.sexo_ute
            BL_ColocaTextoCombo "sl_tbf_sexo", "cod_sexo", "cod_sexo", EcSexo, EcDescrSexo
            EcTipoUtente = gDUtente.t_utente
            EcDescrTipoUtente = gDUtente.t_utente
            EcUtente = gDUtente.Utente
            FuncaoProcurarPesquisa (True)     'soliveira Procurar o doente mas deixar inserir se n existir
            EcNome = Trim(gDUtente.nome_ute)
            EcEmail = gDUtente.email
            EcCodEspecie = gDUtente.cod_genero
            EcCodespecie_Validate False

            If gHIS_Import = 1 And UCase(HIS.uID) = UCase("GH") And gDUtente.cod_efr = "" Then
                Set RsCodEFR = New ADODB.recordset
                With RsCodEFR
                    If gDUtente.situacao <> "" And gDUtente.situacao <> "-1" And gDUtente.episodio <> "" Then
                        .Source = "SELECT obter_cod_resp('" & gDUtente.t_utente & "','" & gDUtente.Utente & "','" & GH_RetornaDescrEpisodio(gDUtente.situacao) & "','" & gDUtente.episodio & "') resultado FROM dual"
                    Else
                        .Source = "SELECT obter_cod_resp('" & gDUtente.t_utente & "','" & gDUtente.Utente & "','Ficha-ID','" & gDUtente.Utente & "') resultado FROM dual"
                    End If
                    .CursorType = adOpenStatic
                    .CursorLocation = adUseServer
                    If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                    .Open , gConnHIS
                End With
                If RsCodEFR.RecordCount > 0 Then
                    EcCodEntFinR = BL_HandleNull(RsCodEFR!resultado, 0)
                Else
                    EcCodEntFinR = gDUtente.cod_efr
                End If
                RsCodEFR.Close
                Set RsCodEFR = Nothing
            Else
                EcCodEntFinR = gDUtente.cod_efr
            End If
            EcCodEntFinR_Validate False
            'EcCodEntFinR = gDUtente.cod_efr
            EcMorada = gDUtente.morada
            EcCodPostalAux = gDUtente.cod_postal
            PreencheCodigoPostal
            EcTelefone = gDUtente.telefone
            EcEmail = gDUtente.email
            EcCodEspecie = gDUtente.cod_genero
            EcCodespecie_Validate False
            
        Else
            FuncaoLimpar
            EcCodSequencial = CLng(gDUtente.seq_utente)
            FuncaoProcurarPesquisa
            If gHIS_Import = 1 Then
                If BL_Abre_Conexao_HIS(gConnHIS, gOracle) = 1 And UCase(HIS.uID) = UCase("GH") And EcTipoUtente.Text <> "" And EcUtente.Text <> "" Then
                    Set RsCodEFR = New ADODB.recordset
                    With RsCodEFR
                        .Source = "SELECT obter_cod_resp('" & EcTipoUtente.Text & "','" & EcUtente.Text & "','Ficha-ID','" & EcUtente.Text & "') resultado FROM dual"
                        .CursorType = adOpenStatic
                        .CursorLocation = adUseServer
                        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                        .Open , gConnHIS
                    End With
                    If RsCodEFR.RecordCount > 0 Then
                        EcCodEntFinR = BL_HandleNull(RsCodEFR!resultado, 0)
                        PreencheDescEntFinanceira
                    End If
                    RsCodEFR.Close
                    Set RsCodEFR = Nothing
                End If
            End If
        End If
        Flg_PesqUtente = False
    End If
    
    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    If EcCodSequencial <> "" Then
        PreencheNotas
    End If
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    
    Exit Sub
Trata_Erro:
    BG_LogFile_Erros "FormIdentificaVet: EventoActivate - " & Err.Description
    Resume Next

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    BL_ToolbarEstadoN 0
    
    'Hoje
    gEpisodioActivo = ""
    gSituacaoActiva = mediComboValorNull
    gReqAssocActiva = ""
    
    If Not rs Is Nothing Then
        If rs.state <> adStateClosed Then
            rs.Close
        End If
        Set rs = Nothing
    End If
    
    Set FormIdentificaVet = Nothing
    
    gF_IDENTIF_VET = 0
    If gF_FILA_ESPERA = mediSim Then
        FormFilaEspera.Enabled = True
        Set gFormActivo = FormFilaEspera
    End If

End Sub

Sub DefTipoCampos()
    ReDim seqUtentes(0)
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_nasc_ute", EcDataNasc, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_inscr", EcDataInscr, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    FrameUtente.Enabled = False
    BL_VerificaAnexosUtente Me, mediComboValorNull

End Sub

Sub PreencheCodigoPostal()
    On Error GoTo TrataErro
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostalAux.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal = " & BL_TrataStringParaBD(EcCodPostalAux)
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo do c�digo postal inexistente!", vbExclamation, App.ProductName
            EcCodPostalAux.Text = ""
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.Text = s1
            EcRuaPostal.Text = s2
            EcDescrPostal.Text = Trim(rsCodigo!descr_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheCodigoPostal: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheCodigoPostal"
    Exit Sub
    Resume Next
End Sub

Sub PreencheDescEntFinanceira()
    On Error GoTo TrataErro
    Dim Tabela As ADODB.recordset
    Dim sql As String
    If EcCodEntFinR.Text <> "" Then
        Set Tabela = New ADODB.recordset
        sql = "SELECT descr_efr FROM sl_efr WHERE cod_efr=" & Trim(EcCodEntFinR.Text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Entidade financeira inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodEntFinR.Text = ""
            EcDescrEntFinR.Text = ""
            
            If (gLAB = cHSMARTA) Then
                EcCodEntFinR.Text = "0"
                Call EcCodEntFinR_Validate(True)
            End If
            
            EcCodEntFinR.SetFocus
        Else
            EcDescrEntFinR.Text = Tabela!descr_efr
        End If
        Tabela.Close
        Set Tabela = Nothing

    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDescEntFinanceira: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDescEntFinanceira"
    Exit Sub
    Resume Next
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", EcDescrTipoUtente
    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    BtNotas.Enabled = False
    BtNotasVRM.Enabled = False
    BtGravar.Enabled = False

End Sub

Private Sub BtAdicionar_Click()
    FuncaoLimpar
    FramePesquisa.Enabled = False
    FrameUtente.Enabled = True
    BtGravar.Enabled = True
    EcCodEntFinR.SetFocus
End Sub

Private Sub BtAnexos_Click()
    FormCommonDialog.Show
    FormCommonDialog.EcSeqUtente = EcCodSequencial
    FormCommonDialog.FuncaoProcurar
    Set gFormActivo = FormCommonDialog
End Sub

Private Sub BtConsulta_Click()
    BL_LimpaDadosUtente
    gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
    gDUtente.t_utente = EcTipoUtente.Text
    gDUtente.Utente = EcUtente
    FormIdentificaVet.Enabled = False
    FormConsNovo.Show
End Sub


Private Sub BtGravar_Click()
    If EcCodSequencial <> "" Then
        FuncaoModificar
    Else
        FuncaoInserir
    End If
End Sub

Private Sub BtPesqCodPostal_Click()
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    
    If Trim(EcDescrPostal) = "" And EcCodPostal = "" Then
        BG_Mensagem mediMsgBox, "Preencha a descri��o com uma localidade (ou parte da descri��o de uma), para limitar a lista de c�digos postais.", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
        Exit Sub
    End If
    
    PesqRapida = False
    
    If EcDescrPostal <> "" Then
        ChavesPesq(1) = "descr_postal"
        CamposEcran(1) = "descr_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Descri��o"
        
        ChavesPesq(2) = "cod_postal"
        CamposEcran(2) = "cod_postal"
        Tamanhos(2) = 2000
        Headers(2) = "C�digo"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        ChavesPesq(1) = "cod_postal"
        CamposEcran(1) = "cod_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Codigo"
        
        ChavesPesq(2) = "descr_postal"
        CamposEcran(2) = "descr_postal"
        Tamanhos(2) = 2000
        Headers(2) = "Descr��o"
    End If
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_postal"
    If EcDescrPostal <> "" And EcCodPostal = "" Then
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        CWhere = "UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    Else
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
        CWhere = CWhere & " AND UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    End If
    CampoPesquisa = "descr_postal"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " C�digos Postais")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            i = InStr(1, resultados(2), "-") - 1
            If i = -1 Then
                i = InStr(1, resultados(1), "-") - 1
            End If
            If EcDescrPostal <> "" Then
                If i = -1 Then
                    s1 = resultados(2)
                    s2 = ""
                Else
                    s1 = Mid(resultados(2), 1, i)
                    s2 = Mid(resultados(2), InStr(1, resultados(2), "-") + 1)
                End If
                EcCodPostal.Text = s1
                EcRuaPostal.Text = s2
                EcCodPostalAux.Text = Trim(resultados(2))
                EcDescrPostal.Text = resultados(1)
                EcDescrPostal.SetFocus
            ElseIf EcDescrPostal = "" Then
                If i = -1 Then
                    s1 = resultados(1)
                    s2 = ""
                Else
                    s1 = Mid(resultados(1), 1, i)
                    s2 = Mid(resultados(1), InStr(1, resultados(1), "-") + 1)
                End If
                EcCodPostal.Text = s1
                EcRuaPostal.Text = s2
                EcCodPostalAux.Text = Trim(resultados(1))
                EcDescrPostal.Text = resultados(2)
                EcDescrPostal.SetFocus
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "C�digo postal n�o encontrado!", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
    End If

 Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesqCodPostal_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesqCodPostal_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesqEntFinR_Click()
    PA_PesquisaEFR EcCodEntFinR, EcDescrEntFinR, ""
End Sub

Private Sub BtPesqRapEspecie_Click()
    


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_genero"
    CamposEcran(1) = "cod_genero"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_genero"
    CamposEcran(2) = "descr_genero"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_genero"
    CampoPesquisa1 = "descr_genero"
    ClausulaWhere = " (flg_invisivel = 0 or flg_invisivel is null) "
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Esp�cies")
    
    mensagem = "N�o foi encontrada nenhuma Esp�cie."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodEspecie.Text = resultados(1)
            EcDescrEspecie.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesqRapEspecie_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesqRapEspecie_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesqRapraca_Click()
    


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_raca"
    CamposEcran(1) = "cod_raca"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_raca"
    CamposEcran(2) = "descr_raca"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_racas"
    CampoPesquisa1 = "descr_raca"
    ClausulaWhere = " (flg_invisivel = 0 or flg_invisivel is null) "
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Ra�as")
    
    mensagem = "N�o foi encontrada nenhuma Ra�a."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodRaca.Text = resultados(1)
            EcDescrRaca.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesqRapraca_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesqRapraca_Click"
    Exit Sub
    Resume Next
End Sub


Private Sub BtRequisicoes_Click()
    
    If (1 = 1) Then
    'Hoje
        'gEpisodioActivo = ""
    End If
    
    Max = UBound(gFieldObjectProperties)

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind
    If VerificaAlteracoes = True Then
        gMsgTitulo = "Gravar"
        gMsgMsg = "Foram alterados dados. Pretende Gravar?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            FuncaoModificar (True)
        End If
    End If
    BL_LimpaDadosUtente
    gDUtente.seq_utente = BL_String2Double(EcCodSequencial)
    FormIdentificaVet.Enabled = False
    FormGestaoRequisicaoVet.Show
    
End Sub

Private Sub EcCodEFRPesq_GotFocus()
    EcCodEfrPesqIni = EcCodEFRPesq.Text
End Sub

Private Sub EcCodEFRPesq_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcCodEFRPesq_Validate False
        EcDescrEfrPesq.SetFocus
    End If

End Sub

Private Sub EcCodEFRPesq_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEFRPesq, EcDescrEfrPesq, "")
End Sub

Private Sub EcCodEntFinR_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Public Sub EcCodEntFinR_Validate(Cancel As Boolean)
    
    Cancel = PA_ValidateEFR(EcCodEntFinR, EcDescrEntFinR, "")
    PreencheDadosEFR EcCodEntFinR.Text
End Sub

Private Sub EcCodGeneroPesq_GotFocus()
    EcCodGeneroPesqIni = EcCodGeneroPesq.Text
End Sub

Private Sub EcCodGeneroPesq_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcDescrGeneroPesq.SetFocus
    End If
End Sub

Private Sub EcCodGeneroPesq_Validate(Cancel As Boolean)
    If EcCodGeneroPesqIni <> EcCodGeneroPesq.Text Then
        EcCodGeneroPesqIni = EcCodGeneroPesq.Text
        EcDescrGeneroPesq = BL_SelCodigo("sl_genero", "descr_genero", "cod_genero", EcCodGeneroPesq)
        RefinaPesquisa ""
    End If
End Sub

Private Sub EcCodPostal_Change()
    
    EcCodPostal.Text = UCase(EcCodPostal.Text)

End Sub

Private Sub EcCodPostalCompleto_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostalCompleto.Text <> "" Then
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal = " & BL_TrataStringParaBD(EcCodPostalCompleto)
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrPostal = ""
            EcCodPostal.SetFocus
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.Text = s1
            EcRuaPostal.Text = s2
            EcDescrPostal.Text = Trim(rsCodigo!descr_postal)
            EcCodPostalAux.Text = Trim(rsCodigo!cod_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

Private Sub EcDataInscr_GotFocus()
    
    If EcDataInscr.Text = "" Then
        EcDataInscr.Text = Format(Bg_DaData_ADO, gFormatoData)
    End If
    EcDataInscr.SelStart = 0
    EcDataInscr.SelLength = Len(EcDataInscr)

End Sub

Private Sub EcDataInscr_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataInscr)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDataNasc_Change()
    
    ' PAULO  : Afinar

    EcDataNasc.Text = Trim(EcDataNasc.Text)

    If (Len(EcDataNasc.Text) = 2) Then
        EcDataNasc.Text = EcDataNasc.Text & "-"
        EcDataNasc.SelStart = Len(EcDataNasc.Text)
    End If
    
    If (Len(EcDataNasc.Text) = 5) Then
        EcDataNasc.Text = EcDataNasc.Text & "-"
        EcDataNasc.SelStart = Len(EcDataNasc.Text)
    End If
    '
    
    ' FIM PAULO  : Afinar

End Sub

Private Sub EcDataNasc_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = 8) Then
        EcDataNasc.Text = ""
    End If
    
'    If ((KeyAscii = Asc("-")) Or _
'        (KeyAscii = Asc("/")) Or _
'        (KeyAscii = Asc(":"))) Then
'    End If

End Sub


Private Sub EcDataNasc_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataNasc)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    PreencheIdade
End Sub

Private Sub EcDescrEfrPesq_GotFocus()
    EcDescrEfrPesqIni = EcDescrEfrPesq.Text
End Sub

Private Sub EcDescrEfrPesq_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcDescrEfrPesq_Validate False
        EcNomePesq.SetFocus
    End If

End Sub

Private Sub EcDescrEfrPesq_Validate(Cancel As Boolean)
    If EcDescrEfrPesq <> EcDescrEfrPesqIni Then
        EcDescrEfrPesqIni = EcDescrEfrPesq
        RefinaPesquisa ""
    End If
End Sub

Private Sub EcDescrGeneroPesq_GotFocus()
    EcDescrGeneroPesqIni = EcDescrEfrPesq.Text
End Sub

Private Sub EcDescrGeneroPesq_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcDescrGeneroPesq_Validate False
        
    End If
End Sub

Private Sub EcDescrGeneroPesq_Validate(Cancel As Boolean)
    If EcDescrGeneroPesq <> EcDescrGeneroPesqIni Then
        EcDescrGeneroPesqIni = EcDescrGeneroPesq
        RefinaPesquisa ""
    End If

End Sub

Private Sub EcDescrSexo_Click()
    
    BL_ColocaComboTexto "sl_tbf_sexo", "cod_sexo", "cod_sexo", EcSexo, EcDescrSexo

End Sub

Private Sub EcDescrSexo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcDescrSexo.ListIndex = -1

End Sub

Private Sub EcDescrTipoUtente_Click()
    
    BL_ColocaComboTexto "sl_t_utente", "cod_t_utente", "descr_t_utente", EcTipoUtente, EcDescrTipoUtente
    If EcDescrTipoUtente.ListIndex <> -1 And EcUtente.Enabled Then EcUtente.SetFocus

End Sub

Private Sub EcDescrTipoUtente_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcDescrTipoUtente.ListIndex = -1

End Sub





Private Sub EcDonoPesq_GotFocus()
    EcDonoPesqIni = EcDonoPesq.Text
End Sub

Private Sub EcDonoPesq_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcDonoPesq_Validate False
        EcCodGeneroPesq.SetFocus
    ElseIf KeyCode = vbKeyDown Then
        EcLista.SetFocus
        EcLista.Selected(0) = True
    End If
End Sub

Private Sub EcDonoPesq_Validate(Cancel As Boolean)
    If EcDonoPesqIni <> EcDonoPesq.Text Then
        EcDonoPesqIni = EcDonoPesq.Text
        RefinaPesquisa ""
    End If
End Sub

Private Sub EcEmail_Validate(Cancel As Boolean)
    If EcEmail <> "" Then
        If InStr(1, EcEmail, "@") = 0 Then
            BG_Mensagem mediMsgBox, "Email inv�lido.", vbExclamation, "Valida��o de email"
            EcEmail = ""
            EcEmail.SetFocus
        End If
    End If
End Sub



Private Sub EcIdade_Validate(Cancel As Boolean)
    PreencheDataNasc
End Sub



Private Sub EcLista_Click()
    BG_LimpaCampo_Todos CamposEc
    If Pesquisa = True Then
        EcCodSequencial = seqUtentes(EcLista.ListIndex + 1)
        FuncaoProcurarPesquisa
        If EcLista.Visible = True Then
            EcLista.SetFocus
        End If
    End If
End Sub

Private Sub EcLista_DblClick()
    If EcCodSequencial <> "" Then
        BtRequisicoes_Click
    End If
End Sub




Private Sub EcLista_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Me.Enabled = False
        BtRequisicoes_Click
    End If
End Sub

Private Sub EcNome_LostFocus()
    
    If gNomeCapitalizado = 1 Then
        EcNome.Text = StrConv(EcNome.Text, vbProperCase)
    Else
        EcNome.Text = UCase(EcNome.Text)
    End If
    
     
End Sub
Private Sub EcNomedono_LostFocus()
    
    EcNomeDono.Text = StrConv(EcNomeDono.Text, vbProperCase)
    
End Sub


Private Sub EcNomePesq_GotFocus()
    EcNomePesqIni = EcNomePesq.Text
End Sub

Private Sub EcNomePesq_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcNomePesq_Validate False
        EcDonoPesq.SetFocus
    ElseIf KeyCode = vbKeyDown Then
        EcLista.SetFocus
        EcLista.Selected(0) = True
    End If
End Sub

Private Sub EcNomePesq_Validate(Cancel As Boolean)
    If EcNomePesq.Text <> EcNomePesqIni Then
        EcNomePesqIni = EcNomePesq.Text
        RefinaPesquisa ""
        
    End If
End Sub


Private Sub EcRuaPostal_Validate(Cancel As Boolean)
    
    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset
    
    EcRuaPostal.Text = UCase(EcRuaPostal.Text)
    If Trim(EcCodPostal) <> "" Then
        If Trim(EcRuaPostal) <> "" Then
            Postal = Trim(EcCodPostal.Text) & "-" & Trim(EcRuaPostal.Text)
        Else
            Postal = EcCodPostal
        End If
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal=" & BL_TrataStringParaBD(Postal)
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        
        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrPostal = ""
            EcRuaPostal = ""
            EcCodPostal = ""
        Else
            EcCodPostalAux.Text = Postal
            EcDescrPostal.Text = rsCodigo!descr_postal
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrPostal.Text = ""
        EcCodPostalAux.Text = ""
        EcCodPostal.Text = ""
    End If
    
End Sub




Private Sub EcUtente_LostFocus()

    EcUtente.Text = UCase(EcUtente.Text)
    
End Sub

Private Sub EcUtentePesq_GotFocus()
    EcUtentePesqIni = EcUtentePesq.Text
End Sub

Private Sub EcUtentePesq_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcUtentePesq_Validate False
        EcCodEFRPesq.SetFocus
    End If
End Sub

Private Sub EcUtentePesq_Validate(Cancel As Boolean)
    If EcUtentePesqIni <> EcUtentePesq.Text Then
        EcUtentePesqIni = EcUtentePesq.Text
        RefinaPesquisa ""
    End If
End Sub

Private Sub Form_Activate()
    
    EventoActivate
    
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If
    
End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Sub Inicializacoes()
    
    Me.caption = " Identifica��o"
    Me.left = 5
    Me.top = 5
    Me.Width = 15360
    Me.Height = 8535
    
    NomeTabela = "sl_identif"
    Set CampoDeFocus = EcUtentePesq
    
    NumCampos = 22
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim CamposEcBckp(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_utente"
    CamposBD(1) = "utente"
    CamposBD(2) = "t_utente"
    CamposBD(3) = "dt_inscr"
    CamposBD(4) = "nome_ute"
    CamposBD(5) = "dt_nasc_ute"
    CamposBD(6) = "sexo_ute"
    CamposBD(7) = "descr_mor_ute"
    CamposBD(8) = "cod_postal_ute"
    CamposBD(9) = "telef_ute"
    CamposBD(10) = "cod_efr_ute"
    CamposBD(11) = "user_cri"
    CamposBD(12) = "dt_cri"
    CamposBD(13) = "user_act"
    CamposBD(14) = "dt_act"
    CamposBD(15) = "email"
    CamposBD(16) = "cod_genero"
    CamposBD(17) = "nome_dono"
    CamposBD(18) = "cod_raca"
    CamposBD(19) = "flg_diag_pri"
    CamposBD(20) = "flg_anexo"
    CamposBD(21) = "num_ext"
    
    ' Campos do Ecr�
    
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcUtente
    Set CamposEc(2) = EcTipoUtente
    Set CamposEc(3) = EcDataInscr
    Set CamposEc(4) = EcNome
    Set CamposEc(5) = EcDataNasc
    Set CamposEc(6) = EcDescrSexo
    Set CamposEc(7) = EcMorada
    Set CamposEc(8) = EcCodPostalAux
    Set CamposEc(9) = EcTelefone
    Set CamposEc(10) = EcCodEntFinR
    Set CamposEc(11) = EcUtilizadorCriacao
    Set CamposEc(12) = EcDataCriacao
    Set CamposEc(13) = EcUtilizadorAlteracao
    Set CamposEc(14) = EcDataAlteracao
    Set CamposEc(15) = EcEmail
    Set CamposEc(16) = EcCodEspecie
    Set CamposEc(17) = EcNomeDono
    Set CamposEc(18) = EcCodRaca
    Set CamposEc(19) = EcFlgDiagPrim
    Set CamposEc(20) = EcFlgAnexo
    Set CamposEc(21) = EcNumExt
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(4) = "Nome do utente"
    TextoCamposObrigatorios(5) = "Data de nascimento do utente"
    TextoCamposObrigatorios(6) = "Sexo do utente"
    TextoCamposObrigatorios(10) = "Entidade financeira"
    TextoCamposObrigatorios(4) = "Nome do Animal"
    TextoCamposObrigatorios(17) = "Nome do Dono"
    TextoCamposObrigatorios(18) = "Ra�a do Animal"
    TextoCamposObrigatorios(16) = "Esp�cie"
        
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_utente"
    Set ChaveEc = EcUtente
    
    BtRequisicoes.Enabled = False
    BtConsulta.Enabled = False
    CamposBDparaListBox = Array("utente", "descr_efr", "nome_ute", "nome_dono", "descr_genero")
    NumEspacos = Array(20, 35, 30, 30, 10)
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Funcao_DataActual()
    
    If Me.ActiveControl.Name = EcDataInscr.Name Then
        BL_PreencheData EcDataInscr, Me.ActiveControl
    Else
        If Me.ActiveControl.Name = EcDataNasc.Name Then
            BL_PreencheData EcDataNasc, Me.ActiveControl
        End If
    End If
    
End Sub

Private Sub PreencheIdade()
    On Error GoTo TrataErro
    If EcDataNasc <> "" And IsDate(EcDataNasc) Then
         EcIdade = BG_CalculaIdade(CDate(EcDataNasc.Text))
         'EcIdade = Mid(EcIdade, 1, InStr(1, EcIdade, " "))
    Else
        EcIdade = ""
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDataNasc: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDataNasc"
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheDataNasc()
    Dim anos As String
    Dim meses As String
    Dim hoje As String
    'RGONCALVES 29.05.2013 Cedivet-28
    Dim dia As Integer
    Dim dataAux As String
    '
    EcIdade.Text = Replace(EcIdade.Text, " anos", "")
    EcIdade.Text = Replace(EcIdade.Text, " dias", "")
    EcIdade.Text = Replace(EcIdade.Text, " meses", "")
    If EcIdade <> "" And EcDataNasc.Text = "" Then
        If InStr(1, EcIdade, ".") Then
            anos = Mid(EcIdade, 1, InStr(1, EcIdade, ".") - 1)
            meses = Mid(EcIdade, InStr(1, EcIdade, ".") + 1)
        ElseIf InStr(1, EcIdade, ",") Then
            anos = Mid(EcIdade, 1, InStr(1, EcIdade, ",") - 1)
            meses = Mid(EcIdade, InStr(1, EcIdade, ",") + 1)
        Else
            anos = EcIdade
            meses = 0
        End If
        
        If IsNumeric(anos) And IsNumeric(meses) Then
            
            hoje = Bg_DaData_ADO
            meses = Month(hoje) - meses
            If meses <= 0 Then
                meses = 12 + meses
                anos = anos + 1
            End If
            anos = (Year(hoje) - anos)
            'RGONCALVES 29.05.2013 Cedivet-28
            'EcDataNasc = Day(hoje) & "-" & meses & "-" & anos
            dataAux = Right("0" & Day(hoje), 2) & "-" & Right("0" & meses, 2) & "-" & anos
            dia = CInt(left(dataAux, 2))
            While IsDate(dataAux) = False And dia > 28
                dia = dia - 1
                dataAux = Right("0" & CStr(dia), 2) & Mid(dataAux, 3)
            Wend
            EcDataNasc.Text = dataAux
            '
        Else
            EcDataNasc = ""
        End If
    End If
    
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDataNasc: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDataNasc"
    'RGONCALVES 29.05.2013 Cedivet-28
    EcDataNasc.Text = ""
    EcIdade.Text = ""
    '
    Exit Sub
    Resume Next
End Sub

' pferreira 2010.08.10
' Sugere abreviatura do nome do utente.
Private Function SugereAbreviatura(nome As String, Optional abrevia_todos As Boolean) As String

    Dim i As Integer
    Dim inicio  As Integer
    Dim nomes() As String
    Dim retorno As String
    On Error GoTo TrataErro
    nomes = Split(nome, " ")
    If (Not abrevia_todos) Then: inicio = 1

    For i = 0 To UBound(nomes)
        If (Not WordsException(nomes(i)) And i >= inicio And i < UBound(nomes)) Then
            retorno = retorno & Mid$(Trim$(nomes(i)), 1, 1) + ". "
        ElseIf (Not WordsException(nomes(i))) Then
            retorno = retorno & Trim$(nomes(i)) + " "
        End If
    Next

    SugereAbreviatura = Trim$(retorno)
 Exit Function
TrataErro:
    BG_LogFile_Erros "SugereAbreviatura: " & Err.Number & " - " & Err.Description, Me.Name, "SugereAbreviatura"
    Exit Function
    Resume Next
End Function

' pferreira 2010.08.10
' Test words exception in the string.
Public Function WordsException(�sString� As String) As Boolean
    On Error GoTo TrataErro
    �sString� = UCase$(Space(1) & Trim$(�sString�) & Space(1))
    WordsException = TestRegularExpression("( DA $| DE $| DO $| DAS $| DOS $)", �sString�)
Exit Function
TrataErro:
    BG_LogFile_Erros "DeveSugerirAbreviatura: " & Err.Number & " - " & Err.Description, Me.Name, "DeveSugerirAbreviatura"
    Exit Function
    Resume Next
End Function

' pferreira 2010.08.10
' Test a regular expression string.
Private Function TestRegularExpression(�sPattern� As String, �sString� As String) As Boolean
   
    Dim objRegExp As RegExp
    Dim objMatch As Match
    Dim colMatches As MatchCollection
    Dim sResult As String
   
    On Error GoTo ErrorHandler
    Set objRegExp = New RegExp
    objRegExp.Pattern = �sPattern�
    objRegExp.IgnoreCase = True
    objRegExp.Global = True
    TestRegularExpression = objRegExp.Test(�sString�): Exit Function
    Set colMatches = objRegExp.Execute(�sString�)
    For Each objMatch In colMatches
        sResult = sResult & "Match found at position "
        sResult = sResult & objMatch.FirstIndex & ". Match Value is '"
        sResult = sResult & objMatch.value & "'." & vbCrLf
    Next
    TestRegularExpression = True
    Exit Function
      
ErrorHandler:
    BG_LogFile_Erros "Error in function 'TestRegularExpression' in FormIdentificaVet (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

' pferreira 2010.08.10
Private Function DeveSugerirAbreviatura(nome As String, Abreviatura As String) As Boolean
    
    Dim nomes_abreviatura() As String
    Dim nomes_nome() As String
    On Error GoTo TrataErro
    If (nome = Empty) Then: DeveSugerirAbreviatura = False: Exit Function
    If (Abreviatura = Empty) Then: DeveSugerirAbreviatura = True: Exit Function
    nomes_abreviatura = Split(Abreviatura, " ")
    nomes_nome = Split(nome, " ")
    If (UBound(nomes_abreviatura) = Empty Or UBound(nomes_nome) = Empty) Then: DeveSugerirAbreviatura = True: Exit Function
    If (nomes_abreviatura(0) = nomes_nome(0) And nomes_abreviatura(UBound(nomes_abreviatura)) = nomes_nome(UBound(nomes_nome))) Then: DeveSugerirAbreviatura = False: Exit Function
    DeveSugerirAbreviatura = True
Exit Function
TrataErro:
    BG_LogFile_Erros "DeveSugerirAbreviatura: " & Err.Number & " - " & Err.Description, Me.Name, "DeveSugerirAbreviatura"
    Exit Function
    Resume Next
End Function

Private Sub BtNotas_Click()
    On Error GoTo TrataErro
    FormIdentificaVet.Enabled = False
    FormNotasReq.Show
    FormNotasReq.EcSeqUtente = EcCodSequencial
    FormNotasReq.grupoNota = gGrupoNotasUtente
    FormNotasReq.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotas_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotas_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtNotasVRM_Click()
    On Error GoTo TrataErro
    FormGestaoRequisicao.Enabled = False
    FormNotasReq.Show
    FormNotasReq.EcSeqUtente = EcCodSequencial
    FormNotasReq.grupoNota = gGrupoNotasUtente
    FormNotasReq.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotasVRM_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotasVRM_Click"
    Exit Sub
    Resume Next
End Sub
    
' --------------------------------------------------------

' VERIFICA SE EXISTEM NOTAS PARA REQUISI��O EM CAUSA

' --------------------------------------------------------
Private Sub PreencheNotas()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsNotas As New ADODB.recordset
    
    If EcCodSequencial = "" Then Exit Sub
    sSql = "SELECT count(*) total FROM sl_obs_ana_req WHERE seq_utente = " & EcCodSequencial & " AND cod_gr_nota = " & gGrupoNotasUtente

    RsNotas.CursorType = adOpenStatic
    RsNotas.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsNotas.Open sSql, gConexao
    If RsNotas.RecordCount > 0 Then
        If RsNotas!total > 0 Then
            BtNotas.Visible = False
            BtNotasVRM.Visible = True
            
        Else
            BtNotas.Visible = True
            BtNotasVRM.Visible = False
        End If
    End If
    RsNotas.Close
    Set RsNotas = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheNotas: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheNotas"
    Exit Sub
    Resume Next
End Sub

Private Sub EcCodespecie_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodEspecie.Text = UCase(EcCodEspecie.Text)
    If EcCodEspecie.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_genero FROM sl_genero WHERE cod_genero=" & BL_TrataStringParaBD(Trim(EcCodEspecie.Text))
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Esp�cie inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodEspecie.Text = ""
            EcDescrEspecie.Text = ""
        Else
            EcDescrEspecie.Text = BL_HandleNull(Tabela!descr_genero)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrEspecie.Text = ""
    End If
End Sub
Private Sub EcCodraca_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodRaca.Text = UCase(EcCodRaca.Text)
    If EcCodRaca.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_raca FROM sl_racas WHERE cod_raca=" & EcCodRaca.Text
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Ra�a inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodRaca.Text = ""
            EcDescrRaca.Text = ""
        Else
            EcDescrRaca.Text = BL_HandleNull(Tabela!descr_raca)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrRaca.Text = ""
    End If
End Sub



Private Sub RefinaPesquisa(seq_utente As String)
    Dim sSql As String
    Dim rsPesq As New ADODB.recordset
    Dim i As Long
    On Error GoTo TrataErro
    EcLista.Clear
    FramePesquisa.Enabled = False
    DoEvents
    LimpaCampos
    If EcUtentePesq.Text <> "" Or EcCodEFRPesq.Text <> "" Or EcDescrEfrPesq.Text <> "" Or EcNomePesq.Text <> "" Or EcDonoPesq.Text <> "" Or EcCodGeneroPesq.Text <> "" Or EcDescrGeneroPesq.Text <> "" Then
        sSql = "SELECT x1.seq_utente, x1.t_utente, x1.utente, x1.nome_ute, x1.nome_dono, x1.cod_efr_ute, x2.descr_efr, x1.cod_genero, x3.descr_genero "
        sSql = sSql & " FROM sl_identif x1 LEFT OUTER JOIN sl_efr x2 ON X1.cod_efr_ute = x2.cod_EFR LEFT OUTER JOIN sl_genero x3 ON x1.cod_genero = x3.cod_genero "
        sSql = sSql & " WHERE x1.seq_utente = x1.seq_utente "
        If EcUtentePesq.Text <> "" Then
            sSql = sSql & " AND x1.utente = " & EcUtentePesq.Text
        End If
        If EcNomePesq.Text <> "" Then
            sSql = sSql & " AND upper(utl_raw.cast_to_varchar2(nlssort(x1.nome_ute , 'nls_sort=binary_ai')) )  like '%" & UCase(BL_RemovePortuguese(EcNomePesq.Text)) & "%'"
        End If
        If EcDonoPesq.Text <> "" Then
            sSql = sSql & " AND  upper(utl_raw.cast_to_varchar2(nlssort(x1.nome_dono, 'nls_sort=binary_ai')))  like '%" & UCase(BL_RemovePortuguese(EcDonoPesq.Text)) & "%'"
        End If
        If EcCodEFRPesq.Text <> "" Then
            sSql = sSql & " AND x1.cod_efr_ute= " & EcCodEFRPesq.Text
        End If
        If EcCodGeneroPesq.Text <> "" Then
            sSql = sSql & " AND x1.cod_genero= " & EcCodGeneroPesq.Text
        End If
        If EcDescrEfrPesq.Text <> "" Then
            sSql = sSql & " AND upper(x2.descr_efr) like '%" & UCase(EcDescrEfrPesq.Text) & "%'"
        End If
        If EcDescrGeneroPesq.Text <> "" Then
            sSql = sSql & " AND upper(x3.descr_genero) like '%" & UCase(EcDescrGeneroPesq.Text) & "%'"
        End If
        If seq_utente <> "" Then
            sSql = sSql & " AND x1.seq_utente = " & seq_utente
        End If
        sSql = sSql & " ORDER by x2.descr_efr, x1.nome_ute "
        rsPesq.CursorType = adOpenStatic
        rsPesq.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsPesq.Open sSql, gConexao
    
        If rsPesq.RecordCount > 0 Then
            ReDim seqUtentes(0)
            ReDim seqUtentes(rsPesq.RecordCount)
            i = 1
            While Not rsPesq.EOF
                seqUtentes(i) = BL_HandleNull(rsPesq!seq_utente, mediComboValorNull)
                rsPesq.MoveNext
                i = i + 1
            Wend
            rsPesq.MoveFirst
            Pesquisa = False
            EcLista.Visible = False
            DoEvents
            BG_PreencheListBoxMultipla_ADO EcLista, rsPesq, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
            EcLista.Selected(0) = False
            EcLista.Visible = True
            DoEvents
            Pesquisa = True
        End If
        rsPesq.Close
        Set rsPesq = Nothing
    End If
    If seq_utente <> "" Then
        EcLista.ListIndex = 0
        EcLista_Click
    End If
    FramePesquisa.Enabled = True

Exit Sub
TrataErro:
    BG_LogFile_Erros "RefinaPesquisa: " & Err.Number & " - " & Err.Description, Me.Name, "RefinaPesquisa"
    Exit Sub
    Resume Next
End Sub

Private Sub LimpaCamposPesquisa()
    ReDim seqUtentes(0)
    FramePesquisa.Enabled = True
    FrameUtente.Enabled = False
    EcLista.Clear
    EcUtentePesq.Text = ""
    EcCodEFRPesq.Text = ""
    EcDescrEfrPesq.Text = ""
    EcNomePesq.Text = ""
    EcDonoPesq.Text = ""
    EcCodGeneroPesq.Text = ""
    EcDescrGeneroPesq.Text = ""
    
    EcUtentePesqIni = ""
    EcCodEfrPesqIni = ""
    EcDescrEfrPesqIni = ""
    EcNomePesqIni = ""
    EcDonoPesqIni = ""
    EcCodGeneroPesqIni = ""
    EcDescrGeneroPesqIni = ""
    
End Sub

Sub Funcao_CopiaUte()
    On Error GoTo TrataErro
    
    If Trim(gDUtente.seq_utente) <> "" Then
        EcCodSequencial = CLng(gDUtente.seq_utente)
        FuncaoProcurarPesquisa
    Else
        
        BG_Mensagem mediMsgStatus, "N�o existe utente activo!", , "Copiar utente"
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_CopiaUte: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_CopiaUte"
    Exit Sub
    Resume Next
End Sub


Private Function VerificaAlteracoes() As Boolean
    Dim i As Integer
    Dim ValorControloNovo As String
    On Error GoTo TrataErro
    
    VerificaAlteracoes = False
    
    For i = 0 To NumCampos - 1
        ValorControloNovo = ""
        'TextBox, Label, MaskEdBox, RichTextBox
        If TypeOf CamposEc(i) Is TextBox Or _
           TypeOf CamposEc(i) Is Label Or _
           TypeOf CamposEc(i) Is MaskEdBox Or _
           TypeOf CamposEc(i) Is RichTextBox Then
           
            ValorControloNovo = CamposEc(i)
        'DataCombo
        ElseIf TypeOf CamposEc(i) Is DataCombo Then
            ValorControloNovo = BG_DataComboSel(CamposEc(i))
            If ValorControloNovo = mediComboValorNull Then
                ValorControloNovo = ""
            End If
        'ComboBox
        ElseIf TypeOf CamposEc(i) Is ComboBox Then
            ValorControloNovo = BG_DaComboSel(CamposEc(i))
            If ValorControloNovo = mediComboValorNull Then
                ValorControloNovo = ""
            End If
        'CheckBox
        ElseIf TypeOf CamposEc(i) Is CheckBox Then
            If CamposEc(i).value = 0 Then
                ValorControloNovo = "0"
            ElseIf CamposEc(i).value = 1 Then
                ValorControloNovo = "1"
            Else
                ValorControloNovo = ""
            End If
        Else
        End If
        
        If CamposEcBckp(i) <> ValorControloNovo Then
            VerificaAlteracoes = True
            Exit Function
        End If
    Next
    VerificaAlteracoes = False
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaAlteracoes: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaAlteracoes"
    Exit Function
    Resume Next
End Function





Private Sub PreencheDadosEFR(cod_efr As String)
    Dim sSql As String
    Dim rsPesq As New ADODB.recordset
    If cod_efr = "" Then
        EcMorada.Text = ""
        EcCodPostalCompleto.Text = ""
        Exit Sub
    End If
    sSql = "select * from sl_efr WHERE cod_efr = " & cod_efr
    rsPesq.CursorType = adOpenStatic
    rsPesq.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsPesq.Open sSql, gConexao
    If rsPesq.RecordCount > 0 Then
        EcMorada.Text = BL_HandleNull(rsPesq!mor_efr, "")
        EcCodPostalCompleto.Text = BL_HandleNull(rsPesq!cod_postal_efr, "")
    Else
        EcMorada.Text = ""
        EcCodPostalCompleto.Text = ""
    End If
    rsPesq.Close
    Set rsPesq = Nothing
End Sub

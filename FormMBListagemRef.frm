VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormMBListagemRef 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormMBListagemRef"
   ClientHeight    =   4605
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   6480
   Icon            =   "FormMBListagemRef.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4605
   ScaleWidth      =   6480
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CbEstado 
      Height          =   315
      Left            =   1680
      Style           =   2  'Dropdown List
      TabIndex        =   21
      Top             =   3840
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   240
      TabIndex        =   14
      Top             =   600
      Width           =   6015
      Begin VB.OptionButton OptData 
         Caption         =   "Data Conclus�o"
         Height          =   255
         Index           =   1
         Left            =   2160
         TabIndex        =   20
         Top             =   240
         Width           =   1455
      End
      Begin VB.OptionButton OptData 
         Caption         =   "Data Emiss�o"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   1455
      End
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   300
         Left            =   2040
         TabIndex        =   15
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   130875393
         CurrentDate     =   39300
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   300
         Left            =   4320
         TabIndex        =   16
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   130875393
         CurrentDate     =   39300
      End
      Begin VB.Label Label1 
         Caption         =   "Data Inicial"
         Height          =   255
         Index           =   0
         Left            =   1200
         TabIndex        =   18
         Top             =   600
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Data Final"
         Height          =   255
         Index           =   1
         Left            =   3360
         TabIndex        =   17
         Top             =   600
         Width           =   855
      End
   End
   Begin VB.Frame Frame1 
      Height          =   495
      Left            =   240
      TabIndex        =   11
      Top             =   120
      Width           =   6015
      Begin VB.OptionButton OptListagem 
         Caption         =   "Listagem Refer�ncias"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   13
         Top             =   120
         Width           =   1935
      End
      Begin VB.OptionButton OptListagem 
         Caption         =   "Listagem Ficheiros"
         Height          =   255
         Index           =   1
         Left            =   2160
         TabIndex        =   12
         Top             =   120
         Width           =   1935
      End
   End
   Begin VB.ListBox EcListaMotivos 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1680
      TabIndex        =   9
      Top             =   1920
      Width           =   4215
   End
   Begin VB.CommandButton BtPesquisaMotivoConcl 
      Height          =   315
      Left            =   5880
      Picture         =   "FormMBListagemRef.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Utilizadores"
      Top             =   1920
      Width           =   375
   End
   Begin VB.TextBox EcNumDias 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4800
      TabIndex        =   6
      ToolTipText     =   "N� Utente"
      Top             =   4250
      Width           =   375
   End
   Begin VB.ComboBox CbTipoUtente 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1680
      Style           =   2  'Dropdown List
      TabIndex        =   1
      ToolTipText     =   "Tipo de Utente"
      Top             =   3360
      Width           =   1455
   End
   Begin VB.TextBox EcUtente 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3240
      TabIndex        =   2
      ToolTipText     =   "N� Utente"
      Top             =   3360
      Width           =   2655
   End
   Begin VB.CommandButton BtPesquisaUtil 
      Height          =   315
      Left            =   5880
      Picture         =   "FormMBListagemRef.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Utilizadores"
      Top             =   2640
      Width           =   375
   End
   Begin VB.ListBox EcListaUtil 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1680
      TabIndex        =   0
      Top             =   2640
      Width           =   4215
   End
   Begin VB.Label LbData 
      AutoSize        =   -1  'True
      Caption         =   "Estado"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Index           =   1
      Left            =   240
      TabIndex        =   22
      ToolTipText     =   "Data de Nascimento"
      Top             =   3840
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "Motivo Conclus�o"
      Height          =   255
      Index           =   2
      Left            =   240
      TabIndex        =   10
      Top             =   1920
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "N�mero de dias para refer�ncias emitidas e sem segundo aviso :"
      Height          =   255
      Index           =   3
      Left            =   240
      TabIndex        =   7
      Top             =   4320
      Width           =   4935
   End
   Begin VB.Label LbNrDoe 
      AutoSize        =   -1  'True
      Caption         =   "Utente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   240
      TabIndex        =   5
      Top             =   3360
      Width           =   465
   End
   Begin VB.Label Label1 
      Caption         =   "Utilizador Emiss�o"
      Height          =   255
      Index           =   8
      Left            =   240
      TabIndex        =   4
      Top             =   2640
      Width           =   1455
   End
End
Attribute VB_Name = "FormMBListagemRef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim estado As Integer

Private Sub BtPesquisaMotivoConcl_Click()
    PA_PesquisaMbMotivosMultiSel EcListaMotivos
End Sub

Private Sub BtPesquisaUtil_Click()
    PA_PesquisaUtiliMultiSel EcListaUtil
End Sub

Private Sub CbEstado_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbEstado.ListIndex = mediComboValorNull
    End If
End Sub

Private Sub EcListaUtil_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        PA_ApagaItemListaMultiSel EcListaUtil, KeyCode, False
    End If
End Sub

Private Sub EcListaMotivos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        PA_ApagaItemListaMultiSel EcListaMotivos, KeyCode, False
    End If
End Sub
Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    
    Me.caption = " Listagem de Refer�ncias Emitidas"
    Me.Left = 50
    Me.Top = 50
    Me.Width = 6570
    Me.Height = 5040 ' Normal
    OptListagem(0) = True
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormScriptFactus = Nothing
End Sub



Sub DefTipoCampos()
    BG_PreencheComboBD_ADO "sl_tbf_estado_mb", "cod_estado", "descr_estado", CbEstado
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    EcDtInicio.Value = Bg_DaData_ADO
    EcDtFim.Value = Bg_DaData_ADO
    OptData(0).Value = True
    OptListagem(0).Value = True
End Sub

Sub PreencheCampos()
    

End Sub


Sub FuncaoLimpar()
        

End Sub

Sub FuncaoEstadoAnterior()

End Sub

Sub FuncaoProcurar()
    
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()


End Sub

Sub FuncaoImprimir()

'    IM_InserirImagemBD "c:\Users\fgoncalves\Desktop\image002.png", "sl_cod_gr_epid", "imagem_anexo", "cod_gr_epid", "1", "", ""
    
    If OptListagem(0) = True Then
        FuncaoImprimirReferencias
    Else
    End If
End Sub
Sub FuncaoImprimirReferencias()
    Dim continua As Boolean
    Dim StrTemp As String
    Dim sSql As String
    Dim i As Integer
    Dim tData As String
    
    If OptData(1).Value = True Then
        tData = "dt_conc"
    Else
        tData = "dt_cri"
    End If
    
    sSql = "DELETE FROM sl_cr_mb_referencias WHERE num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
    
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("ListagemRefEmitidas", "Listagens Refer�ncias Emitidas", crptToPrinter)
    Else
        continua = BL_IniciaReport("ListagemRefEmitidas", "Listagens Refer�ncias Emitidas", crptToWindow)
    End If
    
    If continua = False Then Exit Sub
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    sSql = "INSERT INTO sl_cr_mb_referencias (nome_computador, num_sessao, t_utente, utente, nome_ute, cod_ref_pag, val_pag, dt_cri, nome_util, flg_estado)  SELECT "
    sSql = sSql & BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ", sl_identif.t_utente, sl_identif.utente, "
    sSql = sSql & " sl_identif.nome_ute , sl_mb_ref_emitida.cod_ref_pag, sl_mb_ref_emitida.val_pag, sl_mb_ref_emitida.dt_cri, sl_idutilizador.nome, "
    sSql = sSql & "  sl_mb_ref_emitida.flg_estado FROM sl_identif,"
    sSql = sSql & " sl_mb_ref_emitida , sl_idutilizador"
    sSql = sSql & " WHERE sl_identif.seq_utente = sl_mb_ref_emitida.seq_utente and sl_mb_ref_emitida.user_cri = sl_idutilizador.cod_utilizador "
    sSql = sSql & " AND trunc(sl_mb_ref_emitida." & tData & ") BETWEEN " & BL_TrataDataParaBD(EcDtInicio.Value) & " AND " & BL_TrataDataParaBD(EcDtFim.Value)
    
    If EcUtente.Text <> "" And CbTipoUtente.ListIndex > mediComboValorNull Then
        sSql = sSql & " AND sl_identif.t_utente = " & BL_TrataStringParaBD(CbTipoUtente.Text) & " AND sl_identif.utente = " & BL_TrataStringParaBD(EcUtente.Text)
    End If
    
    If CbEstado.ListIndex > mediComboValorNull Then
        sSql = sSql & " AND sl_mb_ref_emitida.flg_estado = " & CbEstado.ItemData(CbEstado.ListIndex)
    End If
    
    If EcNumDias.Text <> "" Then
        If IsNumeric(EcNumDias.Text) Then
            sSql = sSql & " AND trunc(sl_mb_ref_emitida.dt_cri)<= (trunc(sysdate) - " & EcNumDias.Text & ") and flg_estado = 0 "
            sSql = sSql & " and seq_ref_emitida not in (Select seq_ref_emitida FROM sl_mb_emissao_doc WHERE versao_documento = 2)"
        End If
    End If
    
    ' ------------------------------------------------------------- ----------------
    ' UTILIZADORES PREENCHIDA
    ' ------------------------------------------------------------------------------
    If EcListaUtil.ListCount > 0 Then
        sSql = sSql & " AND sl_mb_ref_emitida.user_cri IN ("
        For i = 0 To EcListaUtil.ListCount - 1
            sSql = sSql & EcListaUtil.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------- ----------------
    ' MOTIVOS PREENCHIDA
    ' ------------------------------------------------------------------------------
    If EcListaMotivos.ListCount > 0 Then
        sSql = sSql & " AND sl_mb_ref_emitida.cod_motivo_conc IN ("
        For i = 0 To EcListaMotivos.ListCount - 1
            sSql = sSql & EcListaMotivos.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
        
    BG_ExecutaQuery_ADO sSql
    
    Report.SQLQuery = "SELECT sl_cr_mb_referencias.t_utente, sl_cr_mb_referencias.utente, sl_cr_mb_referencias.nome_ute, sl_cr_mb_referencias.cod_ref_pag, "
    Report.SQLQuery = Report.SQLQuery & " sl_cr_mb_referencias.val_pag, sl_cr_mb_referencias.dt_cri, sl_cr_mb_referencias.nome_util, sl_cr_mb_referencias.flg_estado"
    Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_mb_referencias WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao =" & gNumeroSessao
    Report.SQLQuery = Report.SQLQuery & " ORDER BY dt_cri "
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtInicio.Value)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Value)
    
    'UTILIZADORES
    StrTemp = ""
    For i = 0 To EcListaUtil.ListCount - 1
        StrTemp = StrTemp & EcListaUtil.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(2) = "Utilizadores=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(2) = "Utilizadores=" & BL_TrataStringParaBD("Todos")
    End If
    
    
    'UTENTES
    If EcUtente.Text <> "" And CbTipoUtente.ListIndex > mediComboValorNull Then
        Report.formulas(3) = "Utente=" & BL_TrataStringParaBD(CbTipoUtente.Text & "/" & EcUtente.Text)
    Else
        Report.formulas(3) = "Utente=" & BL_TrataStringParaBD("Todos")
    End If
    
    If EcNumDias.Text <> "" Then
        Report.formulas(4) = "NumDias=" & BL_TrataStringParaBD(EcNumDias.Text)
    Else
        Report.formulas(4) = "Utente=" & BL_TrataStringParaBD("0")
    End If
    
    'UTILIZADORES
    StrTemp = ""
    For i = 0 To EcListaMotivos.ListCount - 1
        StrTemp = StrTemp & EcListaMotivos.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(2) = "Utilizadores=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(2) = "Utilizadores=" & BL_TrataStringParaBD("Todos")
    End If
    
    
    Call BL_ExecutaReport
    sSql = "DELETE FROM sl_cr_mb_referencias WHERE num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql

End Sub



Sub FuncaoModificar()


End Sub



Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub

Private Sub OptListagem_Click(Index As Integer)
    If Index = 0 Then
        CbTipoUtente.ListIndex = mediComboValorNull
        EcUtente.Text = ""
        CbTipoUtente.Enabled = False
        EcUtente.Enabled = False
    Else
        CbTipoUtente.Enabled = False
        EcUtente.Enabled = False
    End If
End Sub

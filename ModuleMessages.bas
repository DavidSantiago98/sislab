Attribute VB_Name = "ModuleMessages"


'     .................................................................
'    .            Module for application messaging system.             .
'   .                                                                   .
'   .                    Paulo Ferreira 2009.08.07                      .
'    .                         � 2008 CPC|HS                           .
'     .................................................................

Option Explicit

' Constant to define message navigation root.
Global Const MessageNavigationRoot = "ROOT"

' Constant to define message navigation received.
Global Const MessageNavigationReceived = "RECEIVED"

' Constant to define message navigation sent.
Global Const MessageNavigationSent = "SENT"

' Constant to define message navigation archived.
Global Const MessageNavigationArchived = "ARCHIVED"

' Constant to define message navigation deleted.
Global Const MessageNavigationDeleted = "DELETED"

' Constant to define message state unread.
Global Const MessageStateUnread = 2

' Constant to define message state read.
Global Const MessageStateRead = 5

' Constant to define message state archive.
Global Const MessageStateArchive = 4

' Constant to define message state delete.
Global Const MessageStateDelete = 3

' Constant to define message state destroy.
Global Const MessageStateDestroy = 6

' Constant to define message state sent.
Global Const MessageStateSent = 1

' Constant to define message recipient TO.
Global Const RecipientTO = 0

' Constant to define message recipient CC.
Global Const RecipientCC = 1

' Message module global variable: mediMsgBox.
Global Const cr_mediMsgBox = 0

' Message module global variable: mediMsgStatus.
Global Const cr_mediMsgStatus = 1

' Message module global variable: mediMsgPermanece.
Global Const cr_mediMsgPermanece = 500

' Message module global variable: mediMsgBeep.
Global Const cr_mediMsgBeep = 1000

' Message module global variable: cmediTipoDefeito.
Global Const cr_mediTipoDefeito = 0

' Message module global variable: mediTipoMaiusculas.
Global Const cr_mediTipoMaiusculas = 101

' Message module global variable: mediTipoMinusculas.
Global Const cr_mediTipoMinusculas = 102

' Message module global variable: mediTipoCapitalizado.
Global Const cr_mediTipoCapitalizado = 103

' Message module global variable: mediTipoData.
Global Const cr_mediTipoData = 104

' Message module global variable: mediTipoHora.
Global Const cr_mediTipoHora = 105

' Message module global variable: mediComboValorNull.
Global Const cr_mediComboValorNull = -1

' Message module global variable: mediMP_Activo.
Global Const cr_mediMP_Activo = vbArrow

' Message module global variable: mediComboValorNull.
Global Const cr_mediMP_Espera = vbHourglass

' Message module global variable: cFormatoDataBD.
Global Const cr_cFormatoDataBD = "dd-MM-yyyy"

' Message module global variable: FormatoDecBD.
Global Const cr_cFormatoDecBD = "."

' Message module global variable: mediAscComboCodigo.
Global Const cr_mediAscComboCodigo = 1

' Message module global variable: mediDescComboCodigo.
Global Const cr_mediDescComboCodigo = 0

' Message module global variable: mediAscComboDesignacao.
Global Const cr_mediAscComboDesignacao = 3

' Message module global variable: mediDescComboDesignacao.
Global Const cr_mediDescComboDesignacao = 2

' Message module global variable: mediAmbitoGeral.
Global Const cr_mediAmbitoGeral = 0

' Message module global variable: mediAmbitoComputador.
Global Const cr_mediAmbitoComputador = 1

' Message module global variable: mediAmbitoUtilizador.
Global Const cr_mediAmbitoUtilizador = 2

' Change message state.
Public Sub ChangeMessageState(�cmMessage� As ClassModuleMessage, �iState� As Integer)

    On Error GoTo ErrorHandler
    �cmMessage�.SetState = �iState�
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Create a new message.
Public Function MESS_CreateMessage(�lCode� As Long, �sSubject� As String, �lFrom� As Long, �cTo� As Collection, �cCC� As Collection, �iState� As Integer, �sMessage� As String, Optional �cAttachments� As Collection) As ClassModuleMessage

    Dim cmMessage As New ClassModuleMessage
    
    On Error GoTo ErrorHandler
    cmMessage.SetCode = �lCode�
    cmMessage.SetSubject = �sSubject�
    cmMessage.SetFrom = �lFrom�
    cmMessage.SetTo = �cTo�
    cmMessage.SetCC = �cCC�
    cmMessage.SetState = �iState�
    cmMessage.SetMessage = �sMessage�
    cmMessage.SetAttachments = �cAttachments�
    Set MESS_CreateMessage = cmMessage
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' Get message recipients.
Public Function GetMessageRecipients(�lMessageId� As String, Optional �bCC� As Boolean) As String
    
    Dim rMessages As ADODB.recordset
    Dim sSql As String
    
    On Error GoTo ErrorHandler
    sSql = "select cod_util, flg_cc from " & gBD_PREFIXO_TAB & "mensagens_destinatarios where id_mensagem = " & �lMessageId�
    Set rMessages = New ADODB.recordset
    rMessages.CursorLocation = adUseServer
    rMessages.Open sSql, gConexao, adOpenStatic
    While (Not rMessages.EOF)
        If (Not �bCC� And rMessages!flg_cc = ModuleMessages.RecipientTO) Then: GetMessageRecipients = GetMessageRecipients & CR_BL_DevolveNomeUtilizadorActual(rMessages!cod_util) & "; "
        If (�bCC� And rMessages!flg_cc = ModuleMessages.RecipientCC) Then: GetMessageRecipients = GetMessageRecipients & CR_BL_DevolveNomeUtilizadorActual(rMessages!cod_util) & "; "
        rMessages.MoveNext
    Wend
    If (rMessages.state = adStateOpen) Then: rMessages.Close
    Exit Function

ErrorHandler:
    Exit Function

End Function

' Message module global function: BG_ExecutaQuery_ADO.
Public Function CR_BG_ExecutaQuery_ADO(SQLQuery As String, Optional iMensagem) As Long
'   O argumento 'iMensagem' serve para que seja apresentada uma mensagem caso a
' opera��o que se pretende n�o seja efectuada.
'
'   Hip�teses:
'       <Missing>     -> Aparece uma MsgBox
'       cr_mediMsgBox    -> Aparece uma MsgBox
'       mediMsgStatus -> Aparece uma MsgStatus
'       -1            -> N�o aparece mensagem

    Dim sMsg As String
    Dim iRes As Long
    Dim registos As Long
    
    gSQLError = 0
    gSQLISAM = 0
    
    On Error GoTo TrataErro
    
    gConexao.Execute SQLQuery, registos, adCmdText + adExecuteNoRecords
    
    If registos <= 0 Then
        iRes = -1
        BG_LogFile_Erros "(BG_ExecutaQuery: RecordsAffected = 0) -> " & SQLQuery
        sMsg = "Esta opera��o pode n�o ter sido efectuada." & vbCrLf & vbCrLf & "Raz�o prov�vel: Integridade dos Dados."
        If IsMissing(iMensagem) Then
            If gModoDebug = 1 Then BG_Mensagem cr_mediMsgBox, sMsg, vbExclamation, "..."
        Else
            If iMensagem = cr_mediMsgBox Then
                BG_Mensagem cr_mediMsgBox, sMsg, vbExclamation, "..."
            ElseIf iMensagem = cr_mediMsgStatus Then
                BG_Mensagem cr_mediMsgStatus, sMsg, cr_mediMsgBeep + 10
            ElseIf iMensagem = -1 Then
                ' N�o d� mensagem.
            End If
        End If
    Else
        iRes = registos
        
    End If
    
    CR_BG_ExecutaQuery_ADO = iRes
    Exit Function

TrataErro:
    BG_TrataErro "CR_BG_ExecutaQuery_ADO", Err
    CR_BG_ExecutaQuery_ADO = -1
    
    Exit Function

End Function

' Message module global function: BG_Trata_BDErro.
Public Function CR_BG_Trata_BDErro() As Integer

    On Error GoTo TrataErro

    Dim aux
    Dim retorno As Integer
    
    'Verifica a validade da ultima opera��o SQL via ODBC e faz o display duma
    'mensagem conforme o erro.
    'O erro tratado � o erro nativo retornado pelo ODBC e corresponde
    'ao erro emitido pelo gSGBD usado.
    'As vari�veis gInsRegistoDuplo, gErroSyntax s�o arrays com os erros correspondentes
    'em SQLSERVER, ORACLE, INFORMIX. Estas variaveis s�o globais ao projecto,
    'e ter�o que ser criadas � medida que surgem os erros.
    'Poder� haver a necessidade de fazer uma revis�o aos v�rios erros
    'nativos para criar vari�veis que estejam em falta.
    
    
    retorno = 0
    For Each aux In gValoresInsuf
        If aux = gSQLError Then
            If retorno = 0 Then
                BG_Mensagem cr_mediMsgBox, "Valores insuficientes para inser��o !", vbExclamation, "Inserir"
                retorno = -1
            End If
        End If
    Next

    For Each aux In gInsRegistoDuplo
        If aux = gSQLError Then
            BG_Mensagem cr_mediMsgBox, "J� existe registo com esse c�digo !", vbExclamation, "Inserir"
            If retorno = 0 Then retorno = -1
        End If
    Next
    aux = 0
    For Each aux In gUpdRegistoDuplo
        If aux = gSQLError Then
            BG_Mensagem cr_mediMsgBox, "J� existe registo com esse c�digo !", vbExclamation, "Inserir"
            If retorno = 0 Then retorno = -1
        End If
    Next
    aux = 0
    For Each aux In gInsRegistoNulo
        If aux = gSQLError Then
            BG_Mensagem cr_mediMsgBox, "N�o se pode inserir registo nulo num dos campos da tabela!", vbExclamation, "Inserir"
            If retorno = 0 Then retorno = -1
        End If
    Next
    aux = 0
    For Each aux In gErroSyntax
        If aux = gSQLError Then
            BG_Mensagem cr_mediMsgBox, "Erro de sintaxe !", vbExclamation, "Aten��o"
            If retorno = 0 Then retorno = -1
        End If
    Next
    
    If retorno = 0 And gSQLError <> 0 Then
        BG_Mensagem cr_mediMsgBox, "Erro de processamento na BD (Erro:" & gSQLError & ",Isam:" & gSQLISAM & ") !" & vbCrLf & Err.Description, vbExclamation, "Erro"
        If retorno = 0 Then retorno = -1
    End If
    
    CR_BG_Trata_BDErro = retorno
    
    Exit Function
    
    
TrataErro:

    BG_Mensagem cr_mediMsgBox, "erro na CR_BG_Trata_BDErro"
    
End Function

' Message module global function: BG_ConstroiCriterio_INSERT_ADO.
Public Function CR_BG_ConstroiCriterio_INSERT_ADO(NomeTabela As String, NomesCampo As Variant, NomesControl As Variant) As String

' Constroi um statment SQL para INSERT baseado nos campos do Form,
' ou seja utilizando as variaveis NomesControl (=CamposEc) e NomesCampo (=CamposBD)
    
    Dim Criterio, CriterioAux As String
    Dim ValorCampo, ValorControl As Variant
    Dim i, nPassou, nTipo, pos As Integer

    i = 0
    nPassou = 0
    pos = 0
        
    'criar a instru�ao de INSERT
    Criterio = "INSERT INTO " & NomeTabela & " ("
    CriterioAux = ") VALUES ("
    
    'Tratar e retirar o conteudo dos campos para criar o criterio
    For Each ValorCampo In NomesCampo
        'TextBox, Label, MaskEdBox, RichTextBox
        If TypeOf NomesControl(i) Is TextBox Or TypeOf NomesControl(i) Is Label Or TypeOf NomesControl(i) Is MaskEdBox Or TypeOf NomesControl(i) Is RichTextBox Then
            ValorControl = NomesControl(i)
            ValorControl = CR_BG_CvPlica(ValorControl)
        'DataCombo
        ElseIf TypeOf NomesControl(i) Is DataCombo Then
            ValorControl = CR_BG_DataComboSel(NomesControl(i))
            If ValorControl = cr_mediComboValorNull Then
                ValorControl = ""
            End If
        'ComboBox
        ElseIf TypeOf NomesControl(i) Is ComboBox Then
            ValorControl = CR_BG_DataComboSel(NomesControl(i))
            If ValorControl = cr_mediComboValorNull Then
                ValorControl = ""
            End If
        'CheckBox
        ElseIf TypeOf NomesControl(i) Is CheckBox Then
            If NomesControl(i).value = 0 Then
                ValorControl = "0"
            ElseIf NomesControl(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        ' Nada ?
        Else
        End If
        
        'controlar o primeiro campo para nao levar virgula
        If nPassou <> 0 Then
            Criterio = Criterio & ", "
            CriterioAux = CriterioAux & ", "
        End If
        
        Criterio = Criterio & ValorCampo
        
        'se o campo do form tem conteudo formata o conteudo para o statment SQL
        If ValorControl <> "" Then
            If Trim(NomesControl(i).Tag) = "" Then
                'retirar o tipo de campo da base de dados
                CR_BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                nTipo = gFieldObjectProperties(0).tipo
            Else
                'caso j� esteja defenido
                pos = InStr(NomesControl(i).Tag, "(")
                If pos <> 0 Then
                    'tratar o decimal
                    nTipo = CInt(Mid(NomesControl(i).Tag, 1, pos - 1))
                Else
                    nTipo = CInt(NomesControl(i).Tag)
                End If
                
                'verificar se n�o se trata duma defini��o local
                If nTipo = cr_mediTipoCapitalizado Or nTipo = cr_mediTipoData _
                   Or nTipo = cr_mediTipoDefeito Or nTipo = cr_mediTipoHora _
                   Or nTipo = cr_mediTipoMaiusculas Or nTipo = cr_mediTipoMaiusculas _
                   Or nTipo = cr_mediTipoMinusculas Then
                    CR_BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                    nTipo = gFieldObjectProperties(0).tipo
                End If
            
            End If

            'numericos: adDecimal, adNumeric, adSingle, adVarNumeric,
            '           adDouble, adInteger
            If nTipo = adDecimal Or nTipo = adNumeric Or nTipo = adSingle Or nTipo = adVarNumeric Or nTipo = adDouble Or nTipo = adInteger Then
                CriterioAux = CriterioAux & CR_BG_CvDecimalParaWhere_ADO(ValorControl)
            'data e hora: adDate, adDBDate, adDBTimeStamp, adDBTime
            ElseIf nTipo = adDate Or nTipo = adDBDate Or nTipo = adDBTimeStamp Or nTipo = adDBTime Then
                If NomesControl(i).Tag = CStr(cr_mediTipoHora) Then
                    'hora
                    CriterioAux = CriterioAux & "'" & BG_CvHora(ValorControl) & "'"
                Else
                    'data
                    CriterioAux = CriterioAux & "'" & CR_BG_CvDataParaWhere_ADO(ValorControl) & "'"
                End If
            'outro
            Else
                CriterioAux = CriterioAux & "'" & RTrim(ValorControl) & "'"
            End If
        'se o campo do form esta vazio, poe null
        Else
            CriterioAux = CriterioAux & "null"
        End If
        
        nPassou = 1
        i = i + 1
    Next
    Criterio = Criterio & CriterioAux & ")"

    'Mensagens
    BG_LogFile_Erros "CR_BG_ConstroiCriterio_INSERT_ADO:"
    BG_LogFile_Erros "    " & Criterio

    'para debug:
    'BG_Mensagem cr_mediMsgBox, Criterio, , "CR_BG_ConstroiCriterio_INSERT_ADO"

    ' Retorno de resultados
    CR_BG_ConstroiCriterio_INSERT_ADO = Criterio
End Function

' Message module global function: BG_CvPlica.
Public Function CR_BG_CvPlica(ByVal sEntrada As String, Optional iArg As Variant) As String

    Dim sSaida As String
    Dim sRestante As String
    Dim sPlica As String
    Dim sPlicaNova As String
    Dim i As Integer
    
    If IsMissing(iArg) = True Then
        iArg = 2
    End If
    
    Select Case iArg
    
    Case 1
        sPlica = "'"
        sPlicaNova = "" ' Nada -> Tira Plica
        sSaida = ""
        sRestante = sEntrada
        
        i = InStr(sRestante, sPlica)
        While i <> 0
            sSaida = sSaida & left(sRestante, i - 1) & sPlicaNova
            sRestante = Right(sRestante, Len(sRestante) - i)
            i = InStr(sRestante, sPlica)
        Wend
        sSaida = sSaida & sRestante
    Case 2
        sPlica = "'"
        sPlicaNova = "'" ' Coloca outra plica para tirar o significado que ela tem de fechar uma string.
        sSaida = ""
        sRestante = sEntrada
        
        i = InStr(sRestante, sPlica)
        While i <> 0
            sSaida = sSaida & left(sRestante, i) & sPlicaNova
            sRestante = Right(sRestante, Len(sRestante) - i)
            i = InStr(sRestante, sPlica)
        Wend
        sSaida = sSaida & sRestante
    Case 3
        sPlica = "'"
        sPlicaNova = "�" ' Substitui plica por acento (N�o funciona bem com as RichText Boxes)
        sSaida = ""
        sRestante = sEntrada
        
        i = InStr(sRestante, sPlica)
        While i <> 0
            sSaida = sSaida & left(sRestante, i - 1) & sPlicaNova
            sRestante = Right(sRestante, Len(sRestante) - i)
            i = InStr(sRestante, sPlica)
        Wend
        sSaida = sSaida & sRestante
    End Select
    
    CR_BG_CvPlica = sSaida

End Function

' Message module global function: BGAux_DaFieldObjectProperties_ADO.
Public Function CR_BGAux_DaFieldObjectProperties_ADO(NomeTabela As String, NomeCamposBD As Variant)
'NOTA:
'   Esta fun��o retorna o valor das propriedades dum ou ou todos campos duma tabela na estrutura:
'       gFieldObjectProperties().tipo As String
'       gFieldObjectProperties().Precisao As String
'       gFieldObjectProperties().EscalaNumerica As String
'       gFieldObjectProperties().TamanhoConteudo As String
'       gFieldObjectProperties().TamanhoCampo As String
'       gFieldObjectProperties().Nome As String

    Dim MyQuery As String
    Dim rs As ADODB.recordset
    Dim VersaoBD As Double
    Dim NomeCampoBD As Variant
    Dim Ind As Integer
    Dim CamposSelect As String
    
    Set rs = New ADODB.recordset
        
    On Error GoTo TrataErro
           
    ' reinizializar estrutura
    Ind = 0
    ReDim gFieldObjectProperties(0)
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    
    CamposSelect = ""
    If IsArray(NomeCamposBD) Then
        For Each NomeCampoBD In NomeCamposBD
            If Trim(CamposSelect) <> "" Then CamposSelect = CamposSelect & ","
            
            CamposSelect = CamposSelect & NomeCampoBD
        Next
    Else
        CamposSelect = CamposSelect & NomeCamposBD
    End If
    
    If Trim(gSGBD) = gInformix Then
        ' Verificar vers�o do INFORMIX
        ' (SELECT FIRST s� � suportado a partir da versao 7.30)
        VersaoBD = BL_String2Double(Trim(CR_BG_CvDecimalParaDisplay_ADO(gVersaoSGBD)))
        
        If VersaoBD >= 7.3 Then
            MyQuery = "SELECT FIRST 1 " & CamposSelect & " FROM " & NomeTabela
        Else
            MyQuery = "SELECT " & CamposSelect & " FROM " & NomeTabela
        End If
    ElseIf Trim(gSGBD) = gOracle Then
        MyQuery = "SELECT " & CamposSelect & " FROM " & NomeTabela & _
                  " WHERE ROWNUM = 1"
                  '" WHERE ROWNUM < 2 " 'Comentado a 27-12-2005 - Sempre que este select era feito no IPOLX estourava a conex�o
    ElseIf Trim(gSGBD) = gSqlServer Then
        MyQuery = "SELECT TOP 1 " & CamposSelect & " FROM " & NomeTabela
    Else
        MyQuery = "SELECT " & CamposSelect & " FROM " & NomeTabela
    End If
    rs.Open MyQuery, gConexao
        
    If IsArray(NomeCamposBD) Then
        ' se o pedido de propriedades for para mais que um campo
        For Each NomeCampoBD In NomeCamposBD
            ' redimencionar a estrutura
            ReDim Preserve gFieldObjectProperties(Ind)
            
            ' atribuir valores
            gFieldObjectProperties(Ind).tipo = rs.Fields(NomeCampoBD).Type
            gFieldObjectProperties(Ind).Precisao = rs.Fields(NomeCampoBD).Precision
            gFieldObjectProperties(Ind).EscalaNumerica = rs.Fields(NomeCampoBD).NumericScale
            gFieldObjectProperties(Ind).TamanhoConteudo = rs.Fields(NomeCampoBD).ActualSize
            gFieldObjectProperties(Ind).TamanhoCampo = rs.Fields(NomeCampoBD).DefinedSize
            gFieldObjectProperties(Ind).nome = rs.Fields(NomeCampoBD).Name
        
            Ind = Ind + 1
        Next
    Else
        ' atribuir valores
        gFieldObjectProperties(Ind).tipo = rs.Fields(NomeCamposBD).Type
        gFieldObjectProperties(Ind).Precisao = rs.Fields(NomeCamposBD).Precision
        gFieldObjectProperties(Ind).EscalaNumerica = rs.Fields(NomeCamposBD).NumericScale
        gFieldObjectProperties(Ind).TamanhoConteudo = rs.Fields(NomeCamposBD).ActualSize
        gFieldObjectProperties(Ind).TamanhoCampo = rs.Fields(NomeCamposBD).DefinedSize
        gFieldObjectProperties(Ind).nome = rs.Fields(NomeCamposBD).Name
    End If
    
    Exit Function

TrataErro:
    
    BG_LogFile_Erros "BGAux_DaFieldObjectProperties: " & Err & "." & Err.Description & " - " & NomeTabela & " - " & NomeCampoBD
    Resume Next

End Function

' Message module global function: CR_BL_DevolveNomeUtilizadorActual.
Public Function CR_BL_DevolveNomeUtilizadorActual(Optional Codigo As Variant) As String
    
    Dim sql As String
    Dim RsNome As ADODB.recordset
    Dim CodigoUtilizador As String
    
    If Not IsMissing(Codigo) Then
        CodigoUtilizador = Codigo
    End If
    
    On Error GoTo TrataErro
    If CodigoUtilizador = "" Then
        sql = "SELECT nome FROM " & gBD_PREFIXO_TAB & "utilizadores WHERE codigo=" & gCodUtilizador
    Else
        sql = "SELECT nome FROM " & gBD_PREFIXO_TAB & "utilizadores WHERE codigo=" & CodigoUtilizador
    End If
    
    Set RsNome = New ADODB.recordset
    RsNome.CursorLocation = adUseServer
    RsNome.CursorType = adOpenStatic
    RsNome.Open sql, gConexao
    If RsNome.RecordCount > 0 Then
        CR_BL_DevolveNomeUtilizadorActual = BL_HandleNull(RsNome!nome, "")
    End If
    RsNome.Close
    Set RsNome = Nothing
    
    Exit Function

TrataErro:

    CR_BL_DevolveNomeUtilizadorActual = ""

End Function

' Message module global function: BG_PreencheComboBD_ADO.
Public Sub CR_BG_PreencheComboBD_ADO(NomeTabelaBD As String, NomeCampoBD_codigo As String, NomeCampoBD_Descr As String, NomeControl As Control, Optional TipoOrdenacao1 As Variant, Optional TipoOrdenacao2 As Variant)
' Preenche uma ComboBox com base numa query

    Dim obTabelaAux As ADODB.recordset
    Dim CriterioAux As String
    Dim CriterioInicial As String
    Dim iTabelaOuQuery As Integer
    Dim i, mi As Integer
    On Error GoTo TrataErro
    
    'Inicio: Verificar se trata sobre Tabela ou sobre Query
    iTabelaOuQuery = InStr(StrConv(NomeTabelaBD, vbUpperCase), "SELECT ")
    If iTabelaOuQuery = 1 Then
        CriterioInicial = NomeTabelaBD
    Else
        CriterioInicial = "SELECT " & NomeCampoBD_codigo & ", " & NomeCampoBD_Descr & " FROM " & NomeTabelaBD
    End If
    'Fim: Verificar se trata sobre Tabela ou sobre Query
        
    'Inicio: Crit�rio de Ordena��o
    If IsMissing(TipoOrdenacao1) Then
        CriterioAux = CriterioInicial
    Else
        If TipoOrdenacao1 = cr_mediAscComboCodigo Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_codigo & " ASC"
        ElseIf TipoOrdenacao1 = cr_mediDescComboCodigo Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_codigo & " DESC"
        ElseIf TipoOrdenacao1 = cr_mediAscComboDesignacao Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_Descr & " ASC"
        ElseIf TipoOrdenacao1 = cr_mediDescComboDesignacao Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_Descr & " DESC"
        End If
        
        If Not IsMissing(TipoOrdenacao2) Then
            If TipoOrdenacao2 = cr_mediAscComboCodigo Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_codigo & " ASC"
            ElseIf TipoOrdenacao2 = cr_mediDescComboCodigo Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_codigo & " DESC"
            ElseIf TipoOrdenacao2 = cr_mediAscComboDesignacao Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_Descr & " ASC"
            ElseIf TipoOrdenacao2 = cr_mediDescComboDesignacao Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_Descr & " DESC"
            End If
        End If
    End If
    'Fim: Crit�rio de Ordena��o

    Set obTabelaAux = New ADODB.recordset
    obTabelaAux.Open CriterioAux, gConexao, adOpenStatic, adLockReadOnly

    NomeControl.Clear

    mi = 0
    Do Until obTabelaAux.EOF
        NomeControl.AddItem RTrim(obTabelaAux(NomeCampoBD_Descr))
        NomeControl.ItemData(i) = CLng(obTabelaAux(NomeCampoBD_codigo))
        obTabelaAux.MoveNext
        i = i + 1
    Loop

    obTabelaAux.Close
    
    Exit Sub

TrataErro:
    If Err = 6 Then
        NomeControl.List(i) = "!!Sup. 32767: " & NomeControl.List(i)
        NomeControl.ItemData(i) = -10
    End If
    Resume Next

End Sub

' Message module global function: BL_PoeMousePointer.
Public Sub CR_BL_PoeMousePointer(iTipoMousePointer As Integer)
    MDIFormInicio.MousePointer = iTipoMousePointer
    gFormActivo.MousePointer = iTipoMousePointer
End Sub

' Message module global function: Bg_DaData_ADO.
Public Function CR_Bg_DaData_ADO() As String
    Dim CmdData As New ADODB.Command
    Dim PmtData As ADODB.Parameter
    Dim rsData As ADODB.recordset
        
    On Error GoTo TrataErro
    
    If gSGBD = "ACCESS" Then
        CR_Bg_DaData_ADO = Format(Date, gFormatoData)
        Exit Function
    End If
    
    With CmdData
        .ActiveConnection = gConexao
        .CommandText = "DEVOLVE_DATA"
        .CommandType = adCmdStoredProc
    End With
    
    If gSGBD = gOracle Or gSGBD = gSqlServer Then
        Set PmtData = CmdData.CreateParameter("DATA", adVarChar, adParamOutput, 12)
        CmdData.Parameters.Append PmtData
        CmdData.Execute
    Else
        Set rsData = CmdData.Execute
        If rsData.EOF Then CR_Bg_DaData_ADO = "-1"
    End If
    
    If CR_Bg_DaData_ADO = "-1" Then
        BG_LogFile_Erros "CR_Bg_DaData_ADO: ATEN��O - Data obtida localmente! Contactar o Administrador da aplica��o."
        CR_Bg_DaData_ADO = Format(Date, gFormatoData)
    Else
        If gSGBD = gOracle Or gSGBD = gSqlServer Then
            CR_Bg_DaData_ADO = Format(Trim(CmdData.Parameters.item(0).value), gFormatoData)
            'CR_Bg_DaData_ADO = Format(Trim(Date), gFormatoData)
        Else
            CR_Bg_DaData_ADO = Format(Trim(rsData.Fields(0).value), gFormatoData)
        End If
    End If
    
    Set CmdData = Nothing
    Set PmtData = Nothing
    Set rsData = Nothing

    Exit Function

TrataErro:
    BG_TrataErro "CR_Bg_DaData_ADO", Err
    BG_Mensagem cr_mediMsgBox, "CR_Bg_DaData_ADO: " & Error(Err), vbExclamation, "ATEN��O: ERRO GRAVE"
    CR_Bg_DaData_ADO = "-1"
    Resume Next

End Function

' Message module global function: Bg_DaHora_ADO.
Public Function CR_Bg_DaHora_ADO() As String
    Dim CmdHora As New ADODB.Command
    Dim PmtHora As ADODB.Parameter
    Dim RsHora As ADODB.recordset
        
    On Error GoTo TrataErro
    
    If gSGBD = "ACCESS" Then
        CR_Bg_DaHora_ADO = Format(time, gFormatoHora)
        Exit Function
    End If
    
    With CmdHora
        .ActiveConnection = gConexao
        .CommandText = "DEVOLVE_HORA"
        .CommandType = adCmdStoredProc
    End With
    
    If gSGBD = gOracle Or gSGBD = gSqlServer Then
        Set PmtHora = CmdHora.CreateParameter("HORA", adVarChar, adParamOutput, 10)
        CmdHora.Parameters.Append PmtHora
    Else
        Set RsHora = CmdHora.Execute
        If RsHora.EOF Then CR_Bg_DaHora_ADO = "-1"
    End If
    
    CmdHora.Execute
    If CR_Bg_DaHora_ADO = "-1" Then
        BG_LogFile_Erros "cr_Bg_DaHora_ADO: ATEN��O - Hora obtida localmente! Contactar o Administrador da aplica��o."
        CR_Bg_DaHora_ADO = Format(Date, gFormatoHora)
    Else
        If gSGBD = gOracle Or gSGBD = gSqlServer Then
            CR_Bg_DaHora_ADO = Format(Trim(CmdHora.Parameters.item(0).value), gFormatoHora)
        Else
            CR_Bg_DaHora_ADO = Format(Trim(RsHora.Fields(0).value), gFormatoHora)
        End If
    End If
    
    Set CmdHora = Nothing
    Set PmtHora = Nothing
    Set RsHora = Nothing

    Exit Function

TrataErro:
    BG_TrataErro "cr_Bg_DaHora_ADO", Err
    BG_Mensagem cr_mediMsgBox, "cr_Bg_DaHora_ADO: " & Error(Err), vbExclamation, "ATEN��O: ERRO GRAVE"
    CR_Bg_DaHora_ADO = "-1"
    Resume Next

End Function

' Message module global function: BG_BeginTransaction.
Public Sub CR_BG_BeginTransaction()
    If gEstadoTransacao = False Then
        gConexao.BeginTrans
        gEstadoTransacao = True
    ElseIf gEstadoTransacao = True Then
        gConexao.CommitTrans
        gConexao.BeginTrans
        gEstadoTransacao = True
    End If
End Sub

' Message module global function: BG_RollbackTransaction.
Public Sub CR_BG_RollbackTransaction()
    If gEstadoTransacao = True Then
        gConexao.RollbackTrans
        gConexao.BeginTrans
    End If
End Sub

' Message module global function: BG_CommitTransaction.
Public Sub CR_BG_CommitTransaction()

    ' Grava as altera��es efectuadas desde a abertura da ultima transac��o
    
    If gEstadoTransacao = True Then
        gConexao.CommitTrans
        gEstadoTransacao = False
    End If
    
End Sub

' Message module global function: BG_ParametrizaPermissoes_ADO.
Public Sub CR_BG_ParametrizaPermissoes_ADO(nomeForm As String)
' Parametriza os objectos dum form conforme
' as defeni�oes da tabela de parametriza�oes de acessos

    Dim rs As ADODB.recordset
    Dim sql As String
    Dim NomeControlo As String
    Dim NomePropriedade As String
    Dim ValorPropriedade As String
    Dim PropBag As PropertyBag
        
    On Error GoTo Trata_Erro
    
    gCmdParamAcessos.Parameters(0).value = gCodGrupo
    gCmdParamAcessos.Parameters(1).value = gCodUtilizador
    gCmdParamAcessos.Parameters(2).value = UCase(nomeForm)
    Set rs = gCmdParamAcessos.Execute
    
    If Not rs.EOF Then
        With rs
            Do While Not .EOF
                NomeControlo = !nome_obj
                NomePropriedade = !descr_prop
                ValorPropriedade = !val_prop
                BL_PoePermissao_ADO Trim(nomeForm), Trim(NomeControlo), Trim(NomePropriedade), Trim(ValorPropriedade), Empty
                .MoveNext
            Loop
        End With
    End If
    
    rs.Close
    Set rs = Nothing
    Exit Sub
    
Trata_Erro:
    BG_LogFile_Erros "Erro na CR_BG_ParametrizaPermissoes_ADO :" & Err.Number & "/" & Err.Description
    
End Sub

' Message module global function: BG_DataComboSel.
Public Function CR_BG_DataComboSel(ByVal NomeControl As Control) As Long

    If NomeControl.Index = -1 Then
        CR_BG_DataComboSel = cr_mediComboValorNull
    Else
        CR_BG_DataComboSel = NomeControl.ItemData(NomeControl.ListIndex)
    End If

End Function

' Message module global function: BG_CvDecimalParaWhere_ADO.
Public Function CR_BG_CvDecimalParaWhere_ADO(ByVal sArg1 As String) As String
' Converte o simbolo decimal utilizado pela aplica��o para o da BD
    Dim iTemp As Integer
    Dim sTemp As String
    
    If sArg1 = "" Then
        sTemp = sArg1
    Else
        iTemp = InStr(sArg1, gSimboloDecimal)
        If iTemp <> 0 Then
            sTemp = Mid(sArg1, 1, iTemp - 1) & cr_cFormatoDecBD & Mid(sArg1, iTemp + 1)
        Else
            sTemp = sArg1
        End If
    End If

    CR_BG_CvDecimalParaWhere_ADO = sTemp

End Function

' Message module global function: BG_CvDataParaWhere_ADO.
Public Function CR_BG_CvDataParaWhere_ADO(ByVal sArg1 As String) As String
    Dim iDia, iMes, iAno As Integer
    Dim sTemp As String

    If sArg1 <> "" Then
        CR_BG_CvDataParaWhere_ADO = Format(CDate(sArg1), cr_cFormatoDataBD)
    Else
        CR_BG_CvDataParaWhere_ADO = ""
    End If

End Function

' Message module global function: BG_CvDataParaWhere_ADO.
Public Function CR_BG_CvDecimalParaDisplay_ADO(ByVal sArg1 As String) As String
' Converte o simbolo decimal da BD para o utilizado pela aplica��o
    Dim iPos As Integer
    Dim sTemp As String
    
    iPos = InStr(sArg1, cFormatoDecBD)
    If iPos <> 0 Then
        sTemp = Mid(sArg1, 1, iPos - 1) & gSimboloDecimal & Mid(sArg1, iPos + 1)
    Else
        sTemp = sArg1
    End If

    CR_BG_CvDecimalParaDisplay_ADO = sTemp
    
End Function

' Message module global function: BL_ToolbarEstadoN.
Public Sub CR_BL_ToolbarEstadoN(iEstado As Integer)

    If iEstado = 0 Then
        CR_BL_Toolbar_BotaoEstado "Limpar", "InActivo"
        CR_BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        CR_BL_Toolbar_BotaoEstado "Procurar", "InActivo"
        CR_BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        CR_BL_Toolbar_BotaoEstado "Remover", "InActivo"
        CR_BL_Toolbar_BotaoEstado "Anterior", "InActivo"
        CR_BL_Toolbar_BotaoEstado "Seguinte", "InActivo"
        CR_BL_Toolbar_BotaoEstado "EstadoAnterior", "InActivo"
        
        CR_BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
        CR_BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
        
        CR_BL_Toolbar_BotaoEstado "DataActual", "InActivo"
        'CR_BL_Toolbar_BotaoEstado "ActivarRegisto", "InActivo"
    ElseIf iEstado = 1 Then
        CR_BL_Toolbar_BotaoEstado "Limpar", "Activo"
        CR_BL_Toolbar_BotaoEstado "Inserir", "Activo"
        CR_BL_Toolbar_BotaoEstado "Procurar", "Activo"
        CR_BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        CR_BL_Toolbar_BotaoEstado "Remover", "InActivo"
        CR_BL_Toolbar_BotaoEstado "Anterior", "InActivo"
        CR_BL_Toolbar_BotaoEstado "Seguinte", "InActivo"
        CR_BL_Toolbar_BotaoEstado "EstadoAnterior", "Activo"
        
        CR_BL_Toolbar_BotaoEstado "DataActual", "Activo"
        
        
        
        
    ElseIf iEstado = 2 Then
        CR_BL_Toolbar_BotaoEstado "Limpar", "Activo"
        CR_BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        CR_BL_Toolbar_BotaoEstado "Procurar", "Activo"
        CR_BL_Toolbar_BotaoEstado "Modificar", "Activo"
        CR_BL_Toolbar_BotaoEstado "Remover", "Activo"
        CR_BL_Toolbar_BotaoEstado "Anterior", "Activo"
        CR_BL_Toolbar_BotaoEstado "Seguinte", "Activo"
        CR_BL_Toolbar_BotaoEstado "EstadoAnterior", "Activo"
        
        CR_BL_Toolbar_BotaoEstado "DataActual", "Activo"
        
        
        
    End If
End Sub

' Message module global function: BL_Toolbar_BotaoEstado.
Public Sub CR_BL_Toolbar_BotaoEstado(sFuncao As String, sEstado As String)

#If Win16 Then

' Imprimir
    If StrComp(sFuncao, "Imprimir", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonImprimir.Enabled = True
            MDIFormInicio.IDM_IMPRIMIR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonImprimir.Enabled = False
            MDIFormInicio.IDM_IMPRIMIR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

' Limpar
    If StrComp(sFuncao, "Limpar", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonLimpar.Enabled = True
            MDIFormInicio.IDM_LIMPAR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonLimpar.Enabled = False
            MDIFormInicio.IDM_LIMPAR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

'Inserir
    If StrComp(sFuncao, "Inserir", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonInserir.Enabled = True
            MDIFormInicio.IDM_INSERIR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonInserir.Enabled = False
            MDIFormInicio.IDM_INSERIR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

'Procurar
    If StrComp(sFuncao, "Procurar", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonProcurar.Enabled = True
            MDIFormInicio.IDM_PROCURAR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonProcurar.Enabled = False
            MDIFormInicio.IDM_PROCURAR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Modificar
    If StrComp(sFuncao, "Modificar", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonModificar.Enabled = True
            MDIFormInicio.IDM_MODIFICAR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonModificar.Enabled = False
            MDIFormInicio.IDM_MODIFICAR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Remover
    If StrComp(sFuncao, "Remover", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonRemover.Enabled = True
            MDIFormInicio.IDM_REMOVER.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonRemover.Enabled = False
            MDIFormInicio.IDM_REMOVER.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If
'ActivarRegisto
    'If StrComp(sFuncao, "ActivarRegisto", 0) = 0 Then
    '    If StrComp(sEstado, "Activo", 0) = 0 Then
    '        MDIFormInicio.Toolbar1.Buttons.Item("ActivarRegisto").Enabled = True
    '        MDIFormInicio.IDM_ACTIVAR_REGISTO.Enabled = True
    '    ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
    '        MDIFormInicio.Toolbar1.Buttons.Item("ActivarRegisto").Enabled = False
    '        MDIFormInicio.IDM_ACTIVAR_REGISTO.Enabled = False
    '    Else
    '        BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
    '        End If
    'End If

'Anterior
    If StrComp(sFuncao, "Anterior", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonAnterior.Enabled = True
            MDIFormInicio.IDM_ANTERIOR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonAnterior.Enabled = False
            MDIFormInicio.IDM_ANTERIOR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Seguinte
    If StrComp(sFuncao, "Seguinte", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonSeguinte.Enabled = True
            MDIFormInicio.IDM_SEGUINTE.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonSeguinte.Enabled = False
            MDIFormInicio.IDM_SEGUINTE.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'EstadoAnterior
    If StrComp(sFuncao, "EstadoAnterior", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonEstadoAnterior.Enabled = True
            MDIFormInicio.IDM_ESTADO_ANTERIOR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonEstadoAnterior.Enabled = False
            MDIFormInicio.IDM_ESTADO_ANTERIOR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If


' 32 BITS **************************************
#ElseIf Win32 Then


' Imprimir
    If StrComp(sFuncao, "Imprimir", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Imprimir").Enabled = True
            MDIFormInicio.IDM_IMPRIMIR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Imprimir").Enabled = False
            MDIFormInicio.IDM_IMPRIMIR.Enabled = False
        Else
            BG_Mensagem cr_mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

' ImprimirVerAntes
    If StrComp(sFuncao, "ImprimirVerAntes", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("ImprimirVerAntes").Enabled = True
            MDIFormInicio.IDM_IMPRIMIR_VER_ANTES.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("ImprimirVerAntes").Enabled = False
            MDIFormInicio.IDM_IMPRIMIR_VER_ANTES.Enabled = False
        Else
            BG_Mensagem cr_mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

' Limpar
    If StrComp(sFuncao, "Limpar", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Limpar").Enabled = True
            MDIFormInicio.IDM_LIMPAR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Limpar").Enabled = False
            MDIFormInicio.IDM_LIMPAR.Enabled = False
        Else
            BG_Mensagem cr_mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

'Inserir
    If StrComp(sFuncao, "Inserir", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Inserir").Enabled = True
            MDIFormInicio.IDM_INSERIR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Inserir").Enabled = False
            MDIFormInicio.IDM_INSERIR.Enabled = False
        Else
            BG_Mensagem cr_mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

'Procurar
    If StrComp(sFuncao, "Procurar", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Procurar").Enabled = True
            MDIFormInicio.IDM_PROCURAR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Procurar").Enabled = False
            MDIFormInicio.IDM_PROCURAR.Enabled = False
        Else
            BG_Mensagem cr_mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Modificar
    If StrComp(sFuncao, "Modificar", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Modificar").Enabled = True
            MDIFormInicio.IDM_MODIFICAR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Modificar").Enabled = False
            MDIFormInicio.IDM_MODIFICAR.Enabled = False
        Else
            BG_Mensagem cr_mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Remover
    If StrComp(sFuncao, "Remover", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Remover").Enabled = True
            MDIFormInicio.IDM_REMOVER.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Remover").Enabled = False
            MDIFormInicio.IDM_REMOVER.Enabled = False
        Else
            BG_Mensagem cr_mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If
'ActivarRegisto
    'If StrComp(sFuncao, "ActivarRegisto", 0) = 0 Then
    '    If StrComp(sEstado, "Activo", 0) = 0 Then
    '        MDIFormInicio.Toolbar1.Buttons.Item("ActivarRegisto").Enabled = True
    '        MDIFormInicio.IDM_ACTIVAR_REGISTO.Enabled = True
    '    ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
    '        MDIFormInicio.Toolbar1.Buttons.Item("ActivarRegisto").Enabled = False
    '        MDIFormInicio.IDM_ACTIVAR_REGISTO.Enabled = False
    '    Else
    '        BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
    '        End If
    'End If

'Anterior
    If StrComp(sFuncao, "Anterior", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Anterior").Enabled = True
            MDIFormInicio.IDM_ANTERIOR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Anterior").Enabled = False
            MDIFormInicio.IDM_ANTERIOR.Enabled = False
        Else
            BG_Mensagem cr_mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Seguinte
    If StrComp(sFuncao, "Seguinte", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Seguinte").Enabled = True
            MDIFormInicio.IDM_SEGUINTE.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("Seguinte").Enabled = False
            MDIFormInicio.IDM_SEGUINTE.Enabled = False
        Else
            BG_Mensagem cr_mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'EstadoAnterior
    If StrComp(sFuncao, "EstadoAnterior", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("EstadoAnterior").Enabled = True
            MDIFormInicio.IDM_ESTADO_ANTERIOR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.item("EstadoAnterior").Enabled = False
            MDIFormInicio.IDM_ESTADO_ANTERIOR.Enabled = False
        Else
            BG_Mensagem cr_mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'DataActual
    If StrComp(sFuncao, "DataActual", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar2.Buttons.item("DataActual").Enabled = True
            MDIFormInicio.IDM_DATA_ACTUAL.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar2.Buttons.item("DataActual").Enabled = False
            MDIFormInicio.IDM_DATA_ACTUAL.Enabled = False
        Else
            BG_Mensagem cr_mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

#End If

End Sub

' Message module global function: BGAux_DefTipoCampoEc_ADO.
Public Sub CR_BGAux_DefTipoCampoEc_ADO(ByVal NomeCampoBD As String, ByVal NomeControl As Control, TipoCampo As Integer, ObjectPropertiesIndex As Integer, Optional DecimalLength As Variant, Optional DecimalScale As Variant)
    
    Dim nTipo As Integer
    Dim nPrecision As Integer
    Dim nNumericScale As Integer
    
    If TypeOf NomeControl Is TextBox Or TypeOf NomeControl Is Label Or TypeOf NomeControl Is MaskEdBox Or TypeOf NomeControl Is RichTextBox Then
        ' Continua a defini��o do tipo de campo.
    ElseIf TypeOf NomeControl Is ComboBox Then
        Exit Sub
    ElseIf TypeOf NomeControl Is CheckBox Then
        Exit Sub
    Else
        Exit Sub
    End If

    If NomeControl.Tag <> "" Then Exit Sub  ' Por causa de 'BG_DefTipoCampoEc_Todos_ADO
    
    If TypeOf NomeControl Is MaskEdBox Then
        NomeControl.AllowPrompt = True
        NomeControl.ClipMode = mskExcludeLiterals
        NomeControl.FontUnderline = False
        NomeControl.PromptInclude = False
    
        NomeControl.PromptChar = "_"
        NomeControl.AutoTab = False
    End If
    
    nTipo = gFieldObjectProperties(ObjectPropertiesIndex).tipo
    
    If TipoCampo = cr_mediTipoDefeito Then
        NomeControl.Tag = nTipo
    Else
        NomeControl.Tag = TipoCampo
    End If
    
    Select Case TipoCampo
    Case cr_mediTipoDefeito
        Select Case nTipo
            ' Boolean: Tratado por defeito
            Case adBoolean
            ' Binarios: Tratado por defeito
            Case adBinary, adLongVarBinary, adVarBinary
            ' Currency: N�o tratado
            Case adCurrency
            ' Large integers: a precisao pode ir at� 38 digitos
            '          logo limito a 37 (n� digitos da precis�o -1)
            Case adBigInt, adUnsignedBigInt, adVarNumeric, adSingle
                NomeControl.MaxLength = 37
            ' Integer: a precisao pode ir at� +/-2147483647,
            '          logo limito a 10 (n� digitos da precis�o -1)
            Case adInteger, adUnsignedInt
                NomeControl.MaxLength = 10
            ' Small integers: a precisao pode ir at� +/-32767
            '          logo limito a 4 (n� digitos da precis�o -1)
            Case adSmallInt, adTinyInt, adUnsignedSmallInt, adUnsignedTinyInt
                NomeControl.MaxLength = 4
            ' Decimal: define pelos parametros da funcao ou pelas
            '          defini�oes da BD
            Case adDecimal, adDouble, adNumeric
                If (IsMissing(DecimalLength) And Not IsMissing(DecimalScale)) Or (Not IsMissing(DecimalLength) And IsMissing(DecimalScale)) Then
                    BG_Mensagem cr_mediMsgBox, "Falta par�metro 'DecimalLength' ou 'DecimalScale' em <DefTipoCampos> !", vbExclamation, "BG_DefTipoCampoEc - '" & NomeControl.Name & "'"
                ElseIf (Not IsMissing(DecimalLength) And Not IsMissing(DecimalScale)) Then
                    NomeControl.Tag = NomeControl.Tag & "(" & CInt(DecimalLength) & "," & CInt(DecimalScale) & ")"
                    NomeControl.MaxLength = DecimalLength + 1
                    If DecimalLength <= DecimalScale Then
                        BG_Mensagem cr_mediMsgBox, "'DecimalLength' ter� de ser maior do que 'DecimalScale' em <DefTipoCampos> !", vbExclamation, "BG_DefTipoCampoEc - '" & NomeControl.Name & "'"
                    End If
                Else
                    nPrecision = gFieldObjectProperties(ObjectPropertiesIndex).Precisao
                    nNumericScale = gFieldObjectProperties(ObjectPropertiesIndex).EscalaNumerica
                    NomeControl.Tag = NomeControl.Tag & "(" & CInt(nPrecision) & "," & CInt(nNumericScale) & ")"
                    NomeControl.MaxLength = nPrecision + nNumericScale + 1
                End If
            ' Data: Tratado por defeito
            Case adDate, adDBDate, adDBTimeStamp, adDBTime
                BG_Mensagem cr_mediMsgBox, "Em vez de utilizar 'mediTipoDefeito' para o campo Data ou Hora, utilizar 'mediTipoData' ou 'mediTipoHora', consoante o caso!", vbExclamation, "BG_DefTipoCampoEc - '" & NomeControl.Name & "'"
            ' Strings: Usa a defenicao na BD
            Case adBSTR, adChar, adLongVarChar, adLongVarWChar, adVarChar, adVariant, adVarWChar, adWChar
                If gFieldObjectProperties(ObjectPropertiesIndex).TamanhoCampo <= 65535 Then
                    NomeControl.MaxLength = gFieldObjectProperties(ObjectPropertiesIndex).TamanhoCampo
                Else
                    NomeControl.MaxLength = 65535
                End If
            ' Outro!? N�o tratado.
            Case Else
            End Select
    
        Case cr_mediTipoMaiusculas
            If nTipo = adBSTR Or nTipo = adChar Or nTipo = adLongVarChar Or nTipo = adLongVarWChar Or nTipo = adVarChar Or nTipo = adVariant Or nTipo = adVarWChar Or nTipo = adWChar Then
                NomeControl.MaxLength = gFieldObjectProperties(ObjectPropertiesIndex).TamanhoCampo
            Else
                NomeControl = "(Erro na defini��o do tipo de campo)"
            End If
        
        Case cr_mediTipoMinusculas
            If nTipo = adBSTR Or nTipo = adChar Or nTipo = adLongVarChar Or nTipo = adLongVarWChar Or nTipo = adVarChar Or nTipo = adVariant Or nTipo = adVarWChar Or nTipo = adWChar Then
                NomeControl.MaxLength = gFieldObjectProperties(ObjectPropertiesIndex).TamanhoCampo
            Else
                NomeControl = "(Erro na defini��o do tipo de campo)"
            End If
        
        Case cr_mediTipoCapitalizado
            If nTipo = adBSTR Or nTipo = adChar Or nTipo = adLongVarChar Or nTipo = adLongVarWChar Or nTipo = adVarChar Or nTipo = adVariant Or nTipo = adVarWChar Or nTipo = adWChar Then
                NomeControl.MaxLength = gFieldObjectProperties(ObjectPropertiesIndex).TamanhoCampo
            Else
                NomeControl = "(Erro na defini��o do tipo de campo)"
            End If
        
        Case cr_mediTipoData
            If nTipo = adDate Or nTipo = adDBDate Or nTipo = adDBTimeStamp Then
                ' Tratado por defeito
            Else
                NomeControl = "(Erro na defini��o do tipo de campo)"
            End If
        
        Case cr_mediTipoHora
            If nTipo = adDBTime Or nTipo = adDBTimeStamp Then
                ' Tratado por defeito
            'ElseIf nTipo = adBSTR Or nTipo = adChar Or nTipo = adLongVarChar Or nTipo = adLongVarWChar Or nTipo = adVarChar Or nTipo = adVariant Or nTipo = adVarWChar Or nTipo = adWChar Then
            ElseIf nTipo = adDBTimeStamp Or nTipo = adBSTR Or nTipo = adChar Or nTipo = adLongVarChar Or nTipo = adLongVarWChar Or nTipo = adVarChar Or nTipo = adVariant Or nTipo = adVarWChar Or nTipo = adWChar Then
                ' para o caso de se guardar na BD a hora em strings
                NomeControl.MaxLength = gFieldObjectProperties(ObjectPropertiesIndex).TamanhoCampo
            Else
                NomeControl = "(Erro na defini��o do tipo de campo)"
            End If
        
        Case Else
            ' N�o tratado!
    
    End Select

End Sub

' Message module global function: BG_DefTipoCampoEc_ADO.
Public Sub CR_BG_DefTipoCampoEc_ADO(NomeTabela As String, ByVal NomeCampoBD As String, ByVal NomeControl As Control, TipoCampo As Integer, Optional DecimalLength As Variant, Optional DecimalScale As Variant)
' Trata controlos dum form de forma a que os formatos e tamanhos
' sejam compativeis com os campos da BD

    CR_BGAux_DaFieldObjectProperties_ADO NomeTabela, NomeCampoBD
    
    If Not IsMissing(DecimalLength) And Not IsMissing(DecimalScale) Then
        CR_BGAux_DefTipoCampoEc_ADO NomeCampoBD, NomeControl, TipoCampo, 0, DecimalLength, DecimalScale
    ElseIf Not IsMissing(DecimalLength) Then
        CR_BGAux_DefTipoCampoEc_ADO NomeCampoBD, NomeControl, TipoCampo, 0, DecimalLength
    ElseIf Not IsMissing(DecimalScale) Then
        CR_BGAux_DefTipoCampoEc_ADO NomeCampoBD, NomeControl, TipoCampo, 0, , DecimalScale
    Else
        CR_BGAux_DefTipoCampoEc_ADO NomeCampoBD, NomeControl, TipoCampo, 0
    End If
    
End Sub

' Message module global function: BG_PreencheCampoEc_Todos_ADO.
Public Sub CR_BG_PreencheCampoEc_Todos_ADO(NomeRecordset As ADODB.recordset, CamposBD, CamposEc)
' preenche todos os controlos com o conteudo dos campos da BD

    Dim TamArray As Integer
    Dim i As Integer
    
    On Error GoTo TrataErro
    
    TamArray = UBound(CamposEc) - LBound(CamposEc) + 1

    For i = 0 To TamArray - 1
        CR_BG_PreencheCampoEc_ADO NomeRecordset, CamposBD(i), CamposEc(i)
    Next i

    Exit Sub
    
TrataErro:
    BG_TrataErro "CR_BG_PreencheCampoEc_Todos_ADO('" & CamposEc(i).Name & "')", Err
    Resume Next

End Sub

' Message module global function: BL_PreencheControloAcessos.
Public Function CR_BL_PreencheControloAcessos(EcUtilizadorCriacao As TextBox, EcUtilizadorActualizacao As TextBox, LaLoginCriacao As Label, LaLoginAlteracao As Label)
    'Preenche os campos de controlo de acessos a um form

    Dim fraseSQL As String
    Dim rs As ADODB.recordset
    
    Set rs = New ADODB.recordset
    
    LaLoginCriacao = ""
    LaLoginAlteracao = ""
    
    If EcUtilizadorCriacao <> "" Then
        fraseSQL = "SELECT cod_util FROM " & gBD_PREFIXO_TAB & "utilizadores WHERE codigo=" & EcUtilizadorCriacao
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenStatic
        rs.Open fraseSQL, gConexao
        If rs.RecordCount > 0 Then
            LaLoginCriacao = rs!cod_util
        Else
            LaLoginCriacao = EcUtilizadorCriacao
        End If
        rs.Close
    End If
    
    If EcUtilizadorActualizacao <> "" Then
        fraseSQL = "SELECT cod_util FROM " & gBD_PREFIXO_TAB & "utilizadores WHERE codigo=" & EcUtilizadorActualizacao
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenStatic
        rs.Open fraseSQL, gConexao
        If rs.RecordCount > 0 Then
            LaLoginAlteracao = rs!cod_util
        Else
            LaLoginAlteracao = EcUtilizadorActualizacao
        End If
        rs.Close
    End If
    
    Set rs = Nothing
End Function

' Message module global function: BG_ConstroiCriterio_SELECT_ADO.
Public Function CR_BG_ConstroiCriterio_SELECT_ADO(NomeTabela As String, NomesCampo As Variant, NomesControl As Variant, SelTotal As Boolean) As String
    
' Constroi um statment SQL para SELECT baseado nos campos do Form,
' ou seja utilizando as variaveis NomesControl (=CamposEc) e NomesCampo (=CamposBD)
    
    Dim nTipo, i, nPassou, iCount, pos As Integer
    Dim res As Integer
    Dim iTempDecimal As Integer
    Dim Criterio As String
    Dim sTempDecimal As String
    Dim ValorCampo, ValorControl As Variant

    i = 0
    iCount = 0
    nPassou = 0
    pos = 0
    
    'cria a instru�ao de SELECT
    Criterio = "SELECT * FROM " & NomeTabela & " WHERE "
    
    'Tratar e retirar o conteudo dos campos para criar o criterio
    For Each ValorCampo In NomesCampo
        'TextBox, Label, MaskEdBox, RichTextBox
        If TypeOf NomesControl(i) Is TextBox Or TypeOf NomesControl(i) Is Label Or TypeOf NomesControl(i) Is MaskEdBox Or TypeOf NomesControl(i) Is RichTextBox Then
            ValorControl = NomesControl(i)
            ValorControl = CR_BG_CvPlica(ValorControl)
        'DataCombo
        ElseIf TypeOf NomesControl(i) Is DataCombo Then
            ValorControl = CR_BG_DataComboSel(NomesControl(i))
            If ValorControl = cr_mediComboValorNull Then
                ValorControl = ""
            End If
        'ComboBox
        ElseIf TypeOf NomesControl(i) Is ComboBox Then
            ValorControl = CR_BG_DataComboSel(NomesControl(i))
            If ValorControl = cr_mediComboValorNull Then
                ValorControl = ""
            End If
        'CheckBox
        ElseIf TypeOf NomesControl(i) Is CheckBox Then
            If NomesControl(i).value = 0 Then
                ValorControl = "0"
            ElseIf NomesControl(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        ' Nada ?
        Else
        End If
        
        'se o campo do form esta vazio nao entra no criterio
        If ValorControl = "" Then
            iCount = iCount + 1
        'se o campo do form tem conteudo, formata o conteudo para o statment SQL
        Else
            'controlar o primeiro campo para nao levar virgula
            If nPassou <> 0 Then
                Criterio = Criterio & " AND "
            End If

            If Trim(NomesControl(i).Tag) = "" Then
                'retirar o tipo de campo da base de dados
                CR_BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                nTipo = gFieldObjectProperties(0).tipo
            Else
                'caso j� esteja defenido
                pos = InStr(NomesControl(i).Tag, "(")
                If pos <> 0 Then
                    'tratar o decimal
                    nTipo = CInt(Mid(NomesControl(i).Tag, 1, pos - 1))
                Else
                    nTipo = CInt(NomesControl(i).Tag)
                End If
                
                'verificar se n�o se trata duma defini��o local
                If nTipo = cr_mediTipoCapitalizado Or nTipo = cr_mediTipoData _
                   Or nTipo = cr_mediTipoDefeito Or nTipo = cr_mediTipoHora _
                   Or nTipo = cr_mediTipoMaiusculas Or nTipo = cr_mediTipoMaiusculas _
                   Or nTipo = cr_mediTipoMinusculas Then
                    CR_BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                    nTipo = gFieldObjectProperties(0).tipo
                End If
            End If

        
            Select Case nTipo
            'numericos: adDecimal, adNumeric, adSingle, adVarNumeric,
            '           adDouble, adInteger
            Case adDecimal, adNumeric, adSingle, adVarNumeric, adDouble, adInteger
                Criterio = Criterio & ValorCampo & " = " & CR_BG_CvDecimalParaWhere_ADO(ValorControl)
            'data e hora: adDate, adDBDate, adDBTimeStamp, adDBTime
            Case adDate, adDBDate, adDBTimeStamp, adDBTime
                If NomesControl(i).Tag = CStr(cr_mediTipoHora) Then
                    'hora
                    Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvHora(ValorControl) & "'"
                Else
                    'data
                    Criterio = Criterio & ValorCampo & " = " & "'" & CR_BG_CvDataParaWhere_ADO(ValorControl) & "'"
                End If
            'string: adBSTR, adChar, adCurrency, adLongVarChar, adLongVarWChar,
            '        adVarChar, adVariant, adVarWChar, adWChar
            '(controlar os wilcards - '*' e '?' )
            Case adBSTR, adChar, adCurrency, adLongVarChar, adLongVarWChar, adVarChar, adVariant, adVarWChar, adWChar
                If InStr(ValorControl, "*") <> 0 Or InStr(ValorControl, "?") <> 0 Then
                    ValorControl = BG_CvWilcard(ValorControl)
                    Criterio = Criterio & ValorCampo & " LIKE " & "'" & ValorControl & "'"
                Else
                    Criterio = Criterio & ValorCampo & " = " & "'" & ValorControl & "'"
                End If
            'outro
            Case Else
                Criterio = Criterio & ValorCampo & " = " & ValorControl
            End Select
            
            nPassou = 1
        End If
        
        i = i + 1
    Next

    'caso todos os campos estejam vazios, selecciona tudo
    If iCount = i Then
        Criterio = "SELECT * FROM " & NomeTabela
        SelTotal = True
    End If
    
    ' Mensagens
    BG_LogFile_Erros "CR_BG_ConstroiCriterio_SELECT_ADO:"
    BG_LogFile_Erros "    " & Criterio
    
    'para debug:
    'BG_Mensagem mediMsgBox, Criterio, , "CR_BG_ConstroiCriterio_SELECT_ADO"
    
    ' Retorno de resultados
    CR_BG_ConstroiCriterio_SELECT_ADO = Criterio

End Function

' Message module global function: BG_ConstroiCriterio_UPDATE_ADO.
Public Function CR_BG_ConstroiCriterio_UPDATE_ADO(NomeTabela As String, NomesCampo As Variant, NomesControl As Variant, condicao As String) As String

' Constroi um statment SQL para UPDATE baseado nos campos do Form,
' ou seja utilizando as variaveis NomesControl (=CamposEc) e NomesCampo (=CamposBD)

    Dim Criterio As String
    Dim ValorCampo, ValorControl As Variant
    Dim i, nPassou, nTipo, pos As Integer

    i = 0
    nPassou = 0
    
    'criar a instru�ao de UPDATE
    Criterio = "UPDATE " & NomeTabela & " SET "
    
    'Tratar e retirar o conteudo dos campos para criar o criterio
    For Each ValorCampo In NomesCampo
        'TextBox, Label, MaskEdBox, RichTextBox
        If TypeOf NomesControl(i) Is TextBox Or TypeOf NomesControl(i) Is Label Or TypeOf NomesControl(i) Is MaskEdBox Or TypeOf NomesControl(i) Is RichTextBox Then
            ValorControl = NomesControl(i)
            ValorControl = BG_CvPlica(ValorControl)
        'DataCombo
        ElseIf TypeOf NomesControl(i) Is DataCombo Then
            ValorControl = CR_BG_DataComboSel(NomesControl(i))
            If ValorControl = cr_mediComboValorNull Then
                ValorControl = ""
            End If
        'ComboBox
        ElseIf TypeOf NomesControl(i) Is ComboBox Then
            ValorControl = CR_BG_DataComboSel(NomesControl(i))
            If ValorControl = cr_mediComboValorNull Then
                ValorControl = ""
            End If
        'CheckBox
        ElseIf TypeOf NomesControl(i) Is CheckBox Then
            If NomesControl(i).value = 0 Then
                ValorControl = "0"
            ElseIf NomesControl(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        ' Nada ?
        Else
        End If
        
        'controlar o primeiro campo para nao levar virgula
        If nPassou <> 0 Then
            Criterio = Criterio & ", "
        End If
        
        'se o campo do form tem conteudo formata o conteudo para o statment SQL
        If ValorControl <> "" Then
            
            If Trim(NomesControl(i).Tag) = "" Then
                'retirar o tipo de campo da base de dados
                CR_BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                nTipo = gFieldObjectProperties(0).tipo
            Else
                'caso j� esteja defenido
                pos = InStr(NomesControl(i).Tag, "(")
                If pos <> 0 Then
                    'tratar o decimal
                    nTipo = CInt(Mid(NomesControl(i).Tag, 1, pos - 1))
                Else
                    nTipo = CInt(NomesControl(i).Tag)
                End If
                
                'verificar se n�o se trata duma defini��o local
                If nTipo = cr_mediTipoCapitalizado Or nTipo = cr_mediTipoData _
                   Or nTipo = cr_mediTipoDefeito Or nTipo = cr_mediTipoHora _
                   Or nTipo = cr_mediTipoMaiusculas Or nTipo = cr_mediTipoMaiusculas _
                   Or nTipo = cr_mediTipoMinusculas Then
                    CR_BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                    nTipo = gFieldObjectProperties(0).tipo
                End If
            End If

            'numericos: adDecimal, adNumeric, adSingle, adVarNumeric,
            '           adDouble, adInteger
            If nTipo = adDecimal Or nTipo = adNumeric Or nTipo = adSingle Or nTipo = adVarNumeric Or nTipo = adDouble Or nTipo = adInteger Then
                Criterio = Criterio & ValorCampo & " = " & CR_BG_CvDecimalParaWhere_ADO(ValorControl)
            'data e hora: adDate, adDBDate, adDBTimeStamp, adDBTime
            ElseIf nTipo = adDate Or nTipo = adDBDate Or nTipo = adDBTimeStamp Or nTipo = adDBTime Then
                If NomesControl(i).Tag = CStr(cr_mediTipoHora) Then
                    'hora
                    Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvHora(ValorControl) & "'"
                Else
                    'data
                    Criterio = Criterio & ValorCampo & " = " & "'" & CR_BG_CvDataParaWhere_ADO(ValorControl) & "'"
                End If
            'outro
            Else
                Criterio = Criterio & ValorCampo & " = " & "'" & RTrim(ValorControl) & "'"
            End If
        'se o campo do form esta vazio, poe null
        Else
            Criterio = Criterio & ValorCampo & " = " & "null"
        End If
        nPassou = 1
        i = i + 1
    Next
    
    'acrescenta o criterio de UPDATE
    Criterio = Criterio & " WHERE " & condicao

    'Mensagens
    BG_LogFile_Erros "CR_BG_ConstroiCriterio_UPDATE_ADO:"
    BG_LogFile_Erros "    " & Criterio

    'para debug:
    'BG_Mensagem mediMsgBox, Criterio, , "CR_BG_ConstroiCriterio_UPDATE_ADO"

    'Retorno de resultados
    CR_BG_ConstroiCriterio_UPDATE_ADO = Criterio

End Function

' Message module global function: BG_PreencheCampoEc_ADO.
Function CR_BG_PreencheCampoEc_ADO(NomeRecordset As ADODB.recordset, ByVal NomeCampoBD As String, ByVal NomeControl As Control) As Integer
' preenche um controlo com o conteudo dum campo da BD

    On Error GoTo TrataErro
    
    'TextBox, Label, MaskEdBox, RichTextBox
    If TypeOf NomeControl Is TextBox Or TypeOf NomeControl Is Label Or TypeOf NomeControl Is MaskEdBox Or TypeOf NomeControl Is RichTextBox Then
        If IsNull(NomeRecordset(NomeCampoBD)) Then
            NomeControl = ""
        Else
            NomeControl = RTrim(NomeRecordset(NomeCampoBD))
            
            If left(NomeControl.Tag, 2) = adDecimal Or left(NomeControl.Tag, 1) = adDouble Or left(NomeControl.Tag, 3) = adNumeric Then
                NomeControl = BG_CvDecimalParaDisplay_ADO(NomeControl)
            ElseIf NomeControl.Tag = mediTipoData Then
                NomeControl = NomeControl
            ElseIf NomeControl.Tag = mediTipoHora Then
                NomeControl = BG_CvHora(NomeControl)
            End If
        End If
    'ComboBox
    ElseIf TypeOf NomeControl Is ComboBox Then
        If IsNull(NomeRecordset(NomeCampoBD)) Then
            NomeControl.ListIndex = -1
        Else
            BG_MostraComboSel NomeRecordset(NomeCampoBD), NomeControl
        End If
    'DataCombo
    ElseIf TypeOf NomeControl Is DataCombo Then
            If IsNull(NomeRecordset(NomeCampoBD)) Then
                NomeControl.Index = -1
            Else
                BG_MostraDataComboSel NomeRecordset(NomeCampoBD), NomeControl
            End If
    'CheckBox
    ElseIf TypeOf NomeControl Is CheckBox Then
        If IsNull(NomeRecordset(NomeCampoBD)) Then
            NomeControl.value = vbGrayed
        Else
            NomeControl.value = NomeRecordset(NomeCampoBD)
        End If
    ' Nada
    Else
    End If
    
    CR_BG_PreencheCampoEc_ADO = 0
    Exit Function

TrataErro:
    CR_BG_PreencheCampoEc_ADO = -1
    Exit Function

End Function


' Insert message in data base.
Public Function MESS_InsertMessage(�cmMessage� As ClassModuleMessage, EscondeRecep As Integer) As Boolean
    
    Dim sSql As String
    
    On Error GoTo ErrorHandler
    sSql = "insert into " & gBD_PREFIXO_TAB & "mensagens (id,assunto,mensagem,flg_ocultar,user_cri,dt_cri) values (" & _
           �cmMessage�.GetCode & "," & BL_TrataStringParaBD(�cmMessage�.GetSubject) & "," & _
           BL_TrataStringParaBD(�cmMessage�.GetMessage) & "," & EscondeRecep & "," & _
           �cmMessage�.GetFrom & ",sysdate)"
    CR_BG_ExecutaQuery_ADO sSql: CR_BG_Trata_BDErro
    MESS_InsertMessage = (gSQLError = 0)
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function



' Insert attachments in data base.
Public Function MESS_InsertAttachments(�cmMessage� As ClassModuleMessage) As Boolean
    
    On Error GoTo ErrorHandler
    Dim sSql As String
    Dim vItem As Variant
    
    On Error GoTo ErrorHandler
    For Each vItem In �cmMessage�.GetAttachments
        sSql = "insert into " & gBD_PREFIXO_TAB & "mensagens_anexos (id_mensagem,cod_anexo,anexo) values (" & �cmMessage�.GetCode & "," & _
               vItem.Index & "," & BG_VfValor(vItem.Text, "'") & ")"
        CR_BG_ExecutaQuery_ADO sSql: CR_BG_Trata_BDErro
        Call FileCopy(vItem.Key, gPathAnexos & �cmMessage�.GetCode & "_" & vItem.Index & "_" & vItem.Text)
    Next
    MESS_InsertAttachments = (gSQLError = 0)
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' Insert recipients in data base.
Public Function MESS_InsertRecipients(�cmMessage� As ClassModuleMessage) As Boolean
    
    Dim sSql As String
    Dim vItem As Variant

    On Error GoTo ErrorHandler
    For Each vItem In �cmMessage�.GetTo
        sSql = "insert into " & gBD_PREFIXO_TAB & "mensagens_destinatarios (id_mensagem,cod_util,cod_estado, flg_cc, flg_lida) values (" & �cmMessage�.GetCode & "," & _
               vItem & "," & ModuleMessages.MessageStateUnread & "," & ModuleMessages.RecipientTO & ", 0)"
        CR_BG_ExecutaQuery_ADO sSql: CR_BG_Trata_BDErro
    Next
    MESS_InsertRecipients = (gSQLError = 0)
    For Each vItem In �cmMessage�.GetCC
        sSql = "insert into " & gBD_PREFIXO_TAB & "mensagens_destinatarios (id_mensagem,cod_util,cod_estado, flg_cc, flg_lida) values (" & �cmMessage�.GetCode & "," & _
               vItem & "," & ModuleMessages.MessageStateUnread & "," & ModuleMessages.RecipientCC & ", 0)"
        CR_BG_ExecutaQuery_ADO sSql: CR_BG_Trata_BDErro
    Next
    MESS_InsertRecipients = (MESS_InsertRecipients = gSQLError = 0)
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function




VERSION 5.00
Begin VB.Form FormEntFinanceiras 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEntFinanceiras"
   ClientHeight    =   7800
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9255
   Icon            =   "FormEFR.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7800
   ScaleWidth      =   9255
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CbDestino 
      Height          =   315
      Left            =   1200
      Style           =   2  'Dropdown List
      TabIndex        =   57
      Top             =   840
      Width           =   1455
   End
   Begin VB.CheckBox CkInvisivel 
      Caption         =   "Entidade Desactiva"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   7080
      TabIndex        =   55
      Top             =   840
      Width           =   2055
   End
   Begin VB.CheckBox CkADSE 
      Caption         =   "ADSE"
      Height          =   375
      Left            =   5880
      TabIndex        =   54
      Top             =   3480
      Width           =   2775
   End
   Begin VB.CheckBox CkNovaArs 
      Caption         =   "ARS Nova Factura��o"
      Height          =   375
      Left            =   2880
      TabIndex        =   53
      Top             =   3480
      Width           =   2775
   End
   Begin VB.TextBox EcCodExt 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   5280
      TabIndex        =   51
      Top             =   120
      Width           =   855
   End
   Begin VB.ListBox EcLocais 
      Appearance      =   0  'Flat
      Height          =   705
      Left            =   0
      Style           =   1  'Checkbox
      TabIndex        =   49
      Top             =   4080
      Width           =   9015
   End
   Begin VB.CheckBox CkHemodialise 
      Caption         =   "Regra Hemodi�lise"
      Height          =   375
      Left            =   240
      TabIndex        =   48
      Top             =   3480
      Width           =   2775
   End
   Begin VB.CheckBox CkImprRelARS 
      Caption         =   "Imprimir Resultados Relat�rio ARS"
      Height          =   375
      Left            =   5880
      TabIndex        =   47
      Top             =   3120
      Width           =   2775
   End
   Begin VB.TextBox EcCodEmpresa 
      Height          =   285
      Left            =   6360
      TabIndex        =   45
      Top             =   8880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ComboBox CbEmpresa 
      Height          =   315
      Left            =   6960
      Style           =   2  'Dropdown List
      TabIndex        =   43
      Top             =   120
      Width           =   2175
   End
   Begin VB.CheckBox CkInsereCodRubrEFR 
      Caption         =   "Insere C�digo da R�brica da EFR"
      Height          =   195
      Left            =   5880
      TabIndex        =   42
      Top             =   2880
      Width           =   2775
   End
   Begin VB.CheckBox CkControlaIsentos 
      Caption         =   "Controla Utentes Isentos (Envio de pre�os)"
      Height          =   375
      Left            =   5880
      TabIndex        =   39
      Top             =   2040
      Width           =   3375
   End
   Begin VB.CheckBox CkObrigaNumBenef 
      Caption         =   "Obriga N�mero Benefici�rio"
      Height          =   375
      Left            =   2880
      TabIndex        =   37
      Top             =   2400
      Width           =   2775
   End
   Begin VB.CheckBox CkSeparaDom 
      Caption         =   "Separa Factura��o dos Domic�lios"
      Height          =   375
      Left            =   2880
      TabIndex        =   36
      Top             =   2760
      Width           =   2775
   End
   Begin VB.CheckBox CkInibeDomNaoUrb 
      Caption         =   "Inibe Domicilios N�o Urbanos"
      Height          =   375
      Left            =   2880
      TabIndex        =   35
      Top             =   3120
      Width           =   2775
   End
   Begin VB.CheckBox CkInibeImpAna 
      Caption         =   "Inibe Impress�o de Resultados"
      Height          =   375
      Left            =   2880
      TabIndex        =   28
      Top             =   1320
      Width           =   2775
   End
   Begin VB.CheckBox Ck65Anos 
      Caption         =   "50% Desconto em Utentes 65 Anos"
      Height          =   375
      Left            =   2880
      TabIndex        =   27
      Top             =   1680
      Width           =   3015
   End
   Begin VB.CheckBox CkPercentagem 
      Caption         =   "Permite Factura��o por Percentagem"
      Height          =   375
      Left            =   2880
      TabIndex        =   26
      Top             =   2040
      Width           =   3255
   End
   Begin VB.CheckBox CkMostraDivida 
      Caption         =   "Mostra D�vida na Entrega de Resultados"
      Height          =   375
      Left            =   5880
      TabIndex        =   41
      Top             =   1320
      Width           =   3375
   End
   Begin VB.CheckBox CkAvisosPagamento 
      Caption         =   "Imprime Avisos de Pagamento"
      Height          =   375
      Left            =   5880
      TabIndex        =   40
      Top             =   1680
      Width           =   2775
   End
   Begin VB.CheckBox CkValFixo 
      Caption         =   "Fins de Semana com Valor Fixo"
      Height          =   375
      Left            =   5880
      TabIndex        =   38
      Top             =   2400
      Width           =   3255
   End
   Begin VB.CheckBox CkDomicilioDefeito 
      Caption         =   "Domic�lio por Defeito"
      Height          =   375
      Left            =   240
      TabIndex        =   34
      Top             =   3120
      Width           =   2535
   End
   Begin VB.CheckBox CkCompartDom 
      Caption         =   "Comparticipa Domicilios"
      Height          =   375
      Left            =   240
      TabIndex        =   33
      Top             =   1320
      Width           =   2415
   End
   Begin VB.CheckBox CkObrigaRecibo 
      Caption         =   "Obriga Recibo no Envio"
      Height          =   375
      Left            =   240
      TabIndex        =   32
      Top             =   1680
      Width           =   2775
   End
   Begin VB.CheckBox CkPrecoEnt 
      Caption         =   "Usa Pre�o da Entidade"
      Height          =   375
      Left            =   240
      TabIndex        =   31
      Top             =   2040
      Width           =   2415
   End
   Begin VB.CheckBox CkValidaNumBenef 
      Caption         =   "Valida N�mero Benefici�rio"
      Height          =   375
      Left            =   240
      TabIndex        =   30
      Top             =   2400
      Width           =   2415
   End
   Begin VB.CheckBox CkDescrAnaRec 
      Caption         =   "Descrimina An�lises no Recibo"
      Height          =   375
      Left            =   240
      TabIndex        =   29
      Top             =   2760
      Width           =   2655
   End
   Begin VB.TextBox EcAbreviatura 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3540
      TabIndex        =   1
      Top             =   120
      Width           =   855
   End
   Begin VB.Frame Frame2 
      Height          =   795
      Left            =   0
      TabIndex        =   14
      Top             =   6840
      Width           =   9045
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   20
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   19
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   18
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   17
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4320
      TabIndex        =   13
      Top             =   8520
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   12
      Top             =   8520
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4320
      TabIndex        =   11
      Top             =   8880
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   10
      Top             =   8880
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1200
      TabIndex        =   0
      Top             =   120
      Width           =   1425
   End
   Begin VB.TextBox EcDescricao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1200
      TabIndex        =   2
      Top             =   480
      Width           =   7935
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6360
      TabIndex        =   4
      Top             =   8520
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2130
      Left            =   0
      TabIndex        =   3
      Top             =   5040
      Width           =   9015
   End
   Begin VB.Label Label3 
      Caption         =   "Destino"
      Height          =   225
      Index           =   1
      Left            =   240
      TabIndex        =   56
      Top             =   840
      Width           =   615
   End
   Begin VB.Label Label13 
      Caption         =   "C�digo Ext."
      Height          =   255
      Left            =   4440
      TabIndex        =   52
      Top             =   120
      Width           =   975
   End
   Begin VB.Label LbLocal 
      AutoSize        =   -1  'True
      Caption         =   "Locais"
      Height          =   195
      Index           =   0
      Left            =   0
      TabIndex        =   50
      Top             =   3840
      Width           =   465
   End
   Begin VB.Label Label1 
      Caption         =   "Empresa"
      Height          =   225
      Index           =   1
      Left            =   4800
      TabIndex        =   46
      Top             =   8880
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.Label Label1 
      Caption         =   "Empresa"
      Height          =   255
      Index           =   25
      Left            =   6240
      TabIndex        =   44
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "Abreviatura"
      Height          =   255
      Left            =   2700
      TabIndex        =   25
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2880
      TabIndex        =   24
      Top             =   8880
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   3000
      TabIndex        =   23
      Top             =   8520
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   120
      TabIndex        =   22
      Top             =   8880
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   8520
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   2700
      TabIndex        =   9
      Top             =   4800
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   0
      TabIndex        =   8
      Top             =   4800
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo "
      Height          =   225
      Index           =   0
      Left            =   240
      TabIndex        =   7
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o "
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Index           =   0
      Left            =   4800
      TabIndex        =   5
      Top             =   8520
      Visible         =   0   'False
      Width           =   1485
   End
End
Attribute VB_Name = "FormEntFinanceiras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset




Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub
Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub

Private Sub EcDescricao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDescricao_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub


Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = " Entidades Financeiras"
    Me.left = 540
    Me.top = 350
    Me.Width = 9345
    Me.Height = 8220 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_efr"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 33
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_efr"
    CamposBD(1) = "cod_efr"
    CamposBD(2) = "descr_efr"
    CamposBD(3) = "abr_efr"
    CamposBD(4) = "user_cri"
    CamposBD(5) = "dt_cri"
    CamposBD(6) = "user_act"
    CamposBD(7) = "dt_act"
    CamposBD(8) = "flg_compart_dom"
    CamposBD(9) = "flg_obriga_recibo"
    CamposBD(10) = "flg_usa_preco_ent"
    CamposBD(11) = "valida_num_benef"
    CamposBD(12) = "flg_descrimina_ana_rec"
    CamposBD(13) = "flg_inibe_imp_analises"
    CamposBD(14) = "flg_65anos"
    CamposBD(15) = "flg_perc_facturar"
    CamposBD(16) = "flg_obriga_nr_benef"
    CamposBD(17) = "flg_separa_dom"
    CamposBD(18) = "flg_nao_urbano"
    CamposBD(19) = "flg_dom_defeito"
    CamposBD(20) = "flg_mostra_divida"
    CamposBD(21) = "flg_imp_aviso_pag"
    CamposBD(22) = "flg_controla_isencao"
    CamposBD(23) = "flg_fds_fixo"
    CamposBD(24) = "flg_insere_cod_rubr_efr"
    CamposBD(25) = "cod_empresa"
    CamposBD(26) = "flg_imprime_res_ars"
    CamposBD(27) = "flg_hemodialise"
    CamposBD(28) = "cod_ext"
    CamposBD(29) = "flg_nova_ars"
    CamposBD(30) = "flg_ADSE"
    CamposBD(31) = "flg_invisivel"
    CamposBD(32) = "cod_destino"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcAbreviatura
    Set CamposEc(4) = EcUtilizadorCriacao
    Set CamposEc(5) = EcDataCriacao
    Set CamposEc(6) = EcUtilizadorAlteracao
    Set CamposEc(7) = EcDataAlteracao
    Set CamposEc(8) = CkCompartDom
    Set CamposEc(9) = CkObrigaRecibo
    Set CamposEc(10) = CkPrecoEnt
    Set CamposEc(11) = CkValidaNumBenef
    Set CamposEc(12) = CkDescrAnaRec
    Set CamposEc(13) = CkInibeImpAna
    Set CamposEc(14) = Ck65Anos
    Set CamposEc(15) = CkPercentagem
    Set CamposEc(16) = CkObrigaNumBenef
    Set CamposEc(17) = CkSeparaDom
    Set CamposEc(18) = CkInibeDomNaoUrb
    Set CamposEc(19) = CkDomicilioDefeito
    Set CamposEc(20) = CkMostraDivida
    Set CamposEc(21) = CkAvisosPagamento
    Set CamposEc(22) = CkControlaIsentos
    Set CamposEc(23) = CkValFixo
    Set CamposEc(24) = CkInsereCodRubrEFR
    Set CamposEc(25) = EcCodEmpresa
    Set CamposEc(26) = CkImprRelARS
    Set CamposEc(27) = CkHemodialise
    Set CamposEc(28) = EcCodExt
    Set CamposEc(29) = CkNovaArs
    Set CamposEc(30) = CkAdse
    Set CamposEc(31) = CkInvisivel
    Set CamposEc(32) = CbDestino
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo da Entidade Financeira"
    TextoCamposObrigatorios(2) = "Descri��o da Entidade Financeira"
    TextoCamposObrigatorios(25) = "Empresa"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_efr"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_efr", "descr_efr")
    NumEspacos = Array(22, 32)
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    BL_FimProcessamento Me
End Sub
Sub PreencheValoresDefeito()
    CkNovaArs.value = vbGrayed
    CkCompartDom.value = vbGrayed
    CkObrigaRecibo.value = vbGrayed
    CkPrecoEnt.value = vbGrayed
    CkValidaNumBenef.value = vbGrayed
    CkDescrAnaRec.value = vbGrayed
    CkInibeImpAna.value = vbGrayed
    Ck65Anos.value = vbGrayed
    CkPercentagem.value = vbGrayed
    CkObrigaNumBenef.value = vbGrayed
    CkSeparaDom.value = vbGrayed
    CkInibeDomNaoUrb.value = vbGrayed
    CkDomicilioDefeito.value = vbGrayed
    CkMostraDivida.value = vbGrayed
    CkAvisosPagamento.value = vbGrayed
    CkControlaIsentos.value = vbGrayed
    CkValFixo.value = vbGrayed
    CkInsereCodRubrEFR.value = vbGrayed
    CkImprRelARS.value = vbGrayed
    CkHemodialise.value = vbGrayed
    CkAdse.value = vbGrayed
    CkInvisivel.value = vbGrayed
    
    BG_PreencheComboBD_ADO "SL_EMPRESAS", "SEQ_EMPRESA", "NOME_EMPRESA", CbEmpresa
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocais, mediAscComboCodigo
    ' PFerreira 04.04.2007
    BG_PreencheComboBD_ADO "sl_tbf_t_destino", "cod_t_dest", "descr_t_dest", CbDestino, mediAscComboCodigo

End Sub
Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormEntFinanceiras = Nothing
End Sub

Sub LimpaCampos()
    Dim i As Integer
    Me.SetFocus
    For i = 0 To EcLocais.ListCount - 1
        EcLocais.Selected(i) = False
    Next

    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    CkNovaArs.value = vbGrayed
    CkCompartDom.value = vbGrayed
    CkObrigaRecibo.value = vbGrayed
    CkPrecoEnt.value = vbGrayed
    CkValidaNumBenef.value = vbGrayed
    CkDescrAnaRec.value = vbGrayed
    CkInibeImpAna.value = vbGrayed
    Ck65Anos.value = vbGrayed
    CkPercentagem.value = vbGrayed
    CkObrigaNumBenef.value = vbGrayed
    CkSeparaDom.value = vbGrayed
    CkInibeDomNaoUrb.value = vbGrayed
    CkDomicilioDefeito.value = vbGrayed
    CkMostraDivida.value = vbGrayed
    CkAvisosPagamento.value = vbGrayed
    CkControlaIsentos.value = vbGrayed
    CkValFixo.value = vbGrayed
    CkInsereCodRubrEFR.value = vbGrayed
    CbEmpresa.ListIndex = mediComboValorNull
    CkImprRelARS.value = vbGrayed
    CkHemodialise.value = vbGrayed
    CkAdse.value = vbGrayed
    CkInvisivel.value = vbGrayed

End Sub

Sub DefTipoCampos()
        
    EcDataCriacao.Tag = adDate
    EcDataAlteracao.Tag = adDate
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        CarregaLocaisEFR EcCodigo
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        PreencheEmpresa
        If CkInvisivel.value = vbChecked Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
    End If
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & " ORDER BY cod_efr ASC,descr_efr ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
            GravaLocaisEFR EcCodigo
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String

    gSQLError = 0
    gSQLISAM = 0
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_efr") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
        
    
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            GravaLocaisEFR EcCodigo
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A desactivar registo."
        CkInvisivel.value = vbChecked
        BD_Update
        BL_FimProcessamento Me
    End If
    
End Sub

Private Sub CbEmpresa_Click()
    If CbEmpresa.ListIndex <> mediComboValorNull Then
        EcCodEmpresa = BL_SelCodigo("SL_EMPRESAS", "COD_EMPRESA", "SEQ_EMPRESA", CbEmpresa.ItemData(CbEmpresa.ListIndex))
    Else
        EcCodEmpresa = ""
    End If
End Sub

' ------------------------------------------------------

' PREENCHE COMBO DA EMPRESA

' ------------------------------------------------------
Private Sub PreencheEmpresa()
    Dim i As Long
    Dim seqEmpresa As Long
    On Error GoTo TrataErro
    If EcCodEmpresa <> "" Then
        seqEmpresa = BL_SelCodigo("SL_EMPRESAS", "SEQ_EMPRESA", "COD_EMPRESA", EcCodEmpresa, "V")
        For i = 0 To CbEmpresa.ListCount - 1
            If CbEmpresa.ItemData(i) = seqEmpresa Then
                CbEmpresa.ListIndex = i
                Exit Sub
            End If
        Next
    Else
        CbEmpresa.ListIndex = mediComboValorNull
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher Empresa: " & Err.Description, Me.Name, "PreencheEmpresa"
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------------------------

' CARREGA LOCAIS ASSOCIADOS A EFR

' ------------------------------------------------------------------------------------------------
Public Sub CarregaLocaisEFR(cod_efr As Long)
    Dim sSql As String
    Dim rsAnaLocais As New ADODB.recordset
    On Error GoTo TrataErro
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_ass_efr_locais WHERE cod_efr = " & cod_efr
    Set rsAnaLocais = BG_ExecutaSELECT(sSql)
    If rsAnaLocais.RecordCount > 0 Then
        While Not rsAnaLocais.EOF
            For i = 0 To EcLocais.ListCount - 1
                If EcLocais.ItemData(i) = BL_HandleNull(rsAnaLocais!cod_local, "") Then
                    EcLocais.Selected(i) = True
                End If
            Next
            rsAnaLocais.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao carregar locais: " & sSql & " " & Err.Description, Me.Name, "CarregaLocaisEFR", False
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------------------------

' GRAVA LOCAIS ASSOCIADOS A PROVENIENCIA

' ------------------------------------------------------------------------------------------------

Public Sub GravaLocaisEFR(cod_efr As Long)
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    sSql = "DELETE FROM sl_ass_efr_locais WHERE cod_efr = " & cod_efr
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To EcLocais.ListCount - 1
        If EcLocais.Selected(i) = True Then
            sSql = "INSERT into sl_ass_efr_locais (cod_efr, cod_local) VALUES("
            sSql = sSql & cod_efr & ","
            sSql = sSql & EcLocais.ItemData(i) & ")"
            BG_ExecutaQuery_ADO sSql
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Gravar locais: " & sSql & " " & Err.Description, Me.Name, "GravaLocaisEFR", False
    Exit Sub
    Resume Next
End Sub


' PFerreira 04.04.2007
Private Sub CbDestino_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    BG_LimpaOpcao CbDestino, KeyCode
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbDestino_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbDestino_KeyDown"
    Exit Sub
    Resume Next
End Sub


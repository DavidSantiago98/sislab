VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormOrdenaAnaNew 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormOrdenaAnaNew"
   ClientHeight    =   8985
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   12540
   Icon            =   "FormOrdenaAnaNew.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8985
   ScaleWidth      =   12540
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcOperacao 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   975
      Left            =   4440
      TabIndex        =   9
      Top             =   2520
      Width           =   3495
   End
   Begin VB.CommandButton BtPesqRapAna 
      Height          =   340
      Left            =   12000
      Picture         =   "FormOrdenaAnaNew.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "  Pesquisa R�pida de  An�lises "
      Top             =   120
      Width           =   375
   End
   Begin VB.CheckBox CkExcluiCanceladas 
      Caption         =   "Excluir canceladas"
      Height          =   255
      Left            =   4800
      TabIndex        =   7
      Top             =   8280
      Width           =   2655
   End
   Begin VB.CheckBox CkExcluiMembros 
      Caption         =   "Excluir membros de complexas"
      Height          =   255
      Left            =   2040
      TabIndex        =   6
      Top             =   8280
      Width           =   2655
   End
   Begin VB.CommandButton BtGravar 
      Caption         =   "Guardar"
      Height          =   540
      Left            =   11640
      Picture         =   "FormOrdenaAnaNew.frx":0396
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Guardar Altera��es"
      Top             =   8280
      Width           =   735
   End
   Begin VB.CommandButton BtEncolherGrupos 
      Caption         =   "Encolher"
      Height          =   540
      Left            =   1200
      Picture         =   "FormOrdenaAnaNew.frx":0920
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Encolher Profile"
      Top             =   8160
      Width           =   735
   End
   Begin VB.CommandButton BtExpandirGrupos 
      Caption         =   "Expandir"
      Height          =   540
      Left            =   240
      Picture         =   "FormOrdenaAnaNew.frx":0EAA
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Expandir Profile"
      Top             =   8160
      Width           =   735
   End
   Begin VB.TextBox EcAux 
      Height          =   285
      Left            =   840
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   855
   End
   Begin MSComctlLib.ImageList ImageListGrupos 
      Left            =   3720
      Top             =   4800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   16777215
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAnaNew.frx":1434
            Key             =   "Grupo"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAnaNew.frx":7C96
            Key             =   "Todos"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormOrdenaAnaNew.frx":E4F8
            Key             =   "SubGrupo"
         EndProperty
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid FgAnalises 
      Height          =   7455
      Left            =   4560
      TabIndex        =   1
      Top             =   600
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   13150
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
   End
   Begin MSComctlLib.TreeView TreeViewGrupos 
      DragIcon        =   "FormOrdenaAnaNew.frx":14D5A
      Height          =   7635
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   13467
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   354
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      Appearance      =   1
   End
End
Attribute VB_Name = "FormOrdenaAnaNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const Amarelo = &HC0C0&
Const AmareloClaro = &HC0FFFF

Private Type analise
   descricao As String
   Codigo As String
   ordem As Long
   estado As Integer
   indiceFg As Long
   flgAlterada As Boolean
End Type

Private Type subGrupo
    descricao As String
    Codigo As String
    ordem As Integer
    chave As String
    indice As Integer
    estrutAnalises() As analise
    totalAnalises As Long
End Type

Private Type Grupo
   descricao As String
   Codigo As String
   ordem As Integer
   chave As String
   indice As Integer
   EstrutSubGrupos() As subGrupo
   TotalSubGrupos As Long
End Type
Dim EstrutGrupos() As Grupo
Dim TotalGrupos As Long

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Const lTodos = 0
Const lGrupo = 1
Const lSubGrupo = 2

Const lColCodigo = 0
Const lColDescricao = 1
Const lColEstado = 2
Const lColOrdem = 3

Dim indiceActivoGr As Long
Dim indiceActivoSGr As Long
Dim indiceActivoAna As Long

Private Sub BtGravar_Click()
    Dim sSql As String
    Dim i As Long
    Dim iGr As Integer
    Dim iSgr As Integer
    Dim iAna As Integer
    On Error GoTo TrataErro

    BG_BeginTransaction

    
    AtualizaOperacao 1, "Gravar Ordena��es"
    
    For iGr = 1 To TotalGrupos
        For iSgr = 1 To EstrutGrupos(iGr).TotalSubGrupos
            For iAna = 1 To EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises
                If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).flgAlterada = True Then
                    Select Case Mid(EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).Codigo, 1, 1)
                        Case "S"
                            sSql = "UPDATE sl_ana_s set ordem = " & EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem
                            sSql = sSql & " WHERE cod_ana_s = " & BL_TrataStringParaBD(EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).Codigo)
                        Case "C"
                            sSql = "UPDATE sl_ana_c set ordem = " & EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem
                            sSql = sSql & " WHERE cod_ana_c = " & BL_TrataStringParaBD(EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).Codigo)
                        Case "P"
                            sSql = "UPDATE sl_perfis set ordem = " & EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem
                            sSql = sSql & " WHERE cod_perfis = " & BL_TrataStringParaBD(EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).Codigo)
                    End Select
                    EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).flgAlterada = False
                    BG_ExecutaQuery_ADO sSql
                End If
            Next iAna
        Next iSgr
    Next iGr
    
    If ActualizaRequisicoes = True Then
        BG_CommitTransaction
        AtualizaOperacao 1, "Sucesso"
    Else
        BG_RollbackTransaction
        AtualizaOperacao 1, "Erro"
    End If
    AtualizaOperacao 0, ""
    InicializaTreeGrupos
Exit Sub
TrataErro:
    AtualizaOperacao 0, ""
    BG_LogFile_Erros "Erro  ao gravar ordens " & Err.Description, Me.Name, "BtGravar_Click", True
    Exit Sub
    Resume Next
End Sub

Private Sub BtEncolherGrupos_Click()
    expandeNo TreeViewGrupos.SelectedItem, False
End Sub

Private Sub BtExpandirGrupos_Click()
    expandeNo TreeViewGrupos.SelectedItem, True
End Sub

Private Sub BtPesqRapAna_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1) As Variant
    Dim PesqRapida As Boolean
    Dim campoPesquisaCod As String
    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3900
    Headers(2) = "Descri��o"
    
    CamposEcran(3) = "ordem"
    
    Tamanhos(3) = 2000
    Headers(3) = "Ordem"
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "slv_analises "
    CampoPesquisa = "descr_ana"
    campoPesquisaCod = "cod_ana"
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, _
                                                                   ChavesPesq, _
                                                                   CamposEcran, _
                                                                   CamposRetorno, _
                                                                   Tamanhos, _
                                                                   Headers, _
                                                                   CWhere, _
                                                                   CFrom, _
                                                                   "", _
                                                                   CampoPesquisa, _
                                                                   " ORDER BY descr_ana ", _
                                                                   " Pesquisa R�pida de An�lises", campoPesquisaCod)
                                                                   

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            ColocaTop CStr(resultados(1))
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises", vbExclamation, "ATEN��O"
        
    End If

End Sub

Private Sub CkExcluiCanceladas_Click()
    InicializaTreeGrupos
    If CkExcluiMembros.value = vbChecked Or CkExcluiCanceladas.value = vbChecked Then
        BtGravar.Enabled = False
    Else
        BtGravar.Enabled = True
    End If
End Sub

Private Sub CkExcluiMembros_Click()
    InicializaTreeGrupos
    If CkExcluiMembros.value = vbChecked Or CkExcluiCanceladas.value = vbChecked Then
        BtGravar.Enabled = False
    Else
        BtGravar.Enabled = True
    End If
End Sub

Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim flg As Integer
    Dim Codigo As String
    Dim iOld As Integer
    Dim iNew As Integer
    If KeyCode = vbKeyReturn Then
        If IsNumeric(EcAux) Then
            iOld = EstrutGrupos(indiceActivoGr).EstrutSubGrupos(indiceActivoSGr).estrutAnalises(indiceActivoAna).ordem
            If EcAux < EstrutGrupos(indiceActivoGr).EstrutSubGrupos(indiceActivoSGr).estrutAnalises(indiceActivoAna).ordem Then
                flg = 0
            ElseIf EcAux > EstrutGrupos(indiceActivoGr).EstrutSubGrupos(indiceActivoSGr).estrutAnalises(indiceActivoAna).ordem Then
                flg = 1
            End If
            Codigo = EstrutGrupos(indiceActivoGr).EstrutSubGrupos(indiceActivoSGr).estrutAnalises(indiceActivoAna).Codigo
            EstrutGrupos(indiceActivoGr).EstrutSubGrupos(indiceActivoSGr).estrutAnalises(indiceActivoAna).ordem = EcAux
            EstrutGrupos(indiceActivoGr).EstrutSubGrupos(indiceActivoSGr).estrutAnalises(indiceActivoAna).flgAlterada = True
            iNew = EstrutGrupos(indiceActivoGr).EstrutSubGrupos(indiceActivoSGr).estrutAnalises(indiceActivoAna).ordem
            FGAnalises.TextMatrix(FGAnalises.row, lColOrdem) = EcAux
            ReordenaEstrutAna iOld, iNew, EstrutGrupos(indiceActivoGr).EstrutSubGrupos(indiceActivoSGr).estrutAnalises(indiceActivoAna).Codigo
            EcAux = ""
            EcAux.Visible = False
            FGAnalises.SetFocus
            TreeViewGrupos_Click
            FGAnalises.Col = lColOrdem
            FGAnalises.row = EstrutGrupos(indiceActivoGr).EstrutSubGrupos(indiceActivoSGr).estrutAnalises(indiceActivoAna).indiceFg
            FGAnalises.CellBackColor = Amarelo
            
        End If
    End If
End Sub

Private Sub EcAux_LostFocus()
    EcAux.Visible = False
    FGAnalises.SetFocus
End Sub

Private Sub FgAnalises_dblClick()
    Dim iGr As Integer
    Dim iSgr As Integer
    Dim iAna As Integer
    
    indiceActivoGr = mediComboValorNull
    indiceActivoSGr = mediComboValorNull
    indiceActivoAna = mediComboValorNull
    
    If FGAnalises.row < FGAnalises.rows - 1 And FGAnalises.Col = lColOrdem Then
        EcAux.left = FGAnalises.CellLeft + FGAnalises.left
        EcAux.top = FGAnalises.CellTop + FGAnalises.top
        EcAux.Width = FGAnalises.CellWidth + 10
        
        For iGr = 1 To TotalGrupos
            For iSgr = 1 To EstrutGrupos(iGr).TotalSubGrupos
                For iAna = 1 To EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises
                    If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).Codigo = FGAnalises.TextMatrix(FGAnalises.row, lColCodigo) Then
                        indiceActivoGr = iGr
                        indiceActivoSGr = iSgr
                        indiceActivoAna = iAna
                        Exit For
                    End If
                Next iAna
            Next iSgr
        Next iGr
        EcAux = EstrutGrupos(indiceActivoGr).EstrutSubGrupos(indiceActivoSGr).estrutAnalises(indiceActivoAna).ordem
        EcAux.Visible = True
        EcAux.SetFocus
        Sendkeys ("{END}")
    End If

End Sub

Private Sub FgAnalises_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        FgAnalises_dblClick
    End If
End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()

    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = "Ordena��o de An�lises"
    Me.left = 5
    Me.top = 5
    Me.Width = 12630 ' 14340
    Me.Height = 9375 ' Normal
    Set CampoDeFocus = TreeViewGrupos
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    AtualizaOperacao 0, ""
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormAdministracaoProfiles = Nothing
    
    Set gFormActivo = FormOrdenaAnaNew

End Sub

Sub LimpaCampos()
    Me.SetFocus
End Sub

Sub DefTipoCampos()
    With FGAnalises
        .rows = 2
        .FixedRows = 1
        .Cols = 4
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        
        .ColWidth(lColCodigo) = 900
        .Col = lColCodigo
        .ColAlignment(lColCodigo) = flexAlignLeftCenter
        .TextMatrix(0, lColCodigo) = "C�digo"
        
        .ColWidth(lColDescricao) = 3500
        .Col = lColDescricao
        .ColAlignment(lColDescricao) = flexAlignLeftCenter
        .TextMatrix(0, lColDescricao) = "Descri��o"
        
        .ColWidth(lColEstado) = 2000
        .Col = lColEstado
        .ColAlignment(lColEstado) = flexAlignLeftCenter
        .TextMatrix(0, lColEstado) = "Estado"
        
        .ColWidth(lColOrdem) = 1000
        .Col = lColOrdem
        .ColAlignment(lColOrdem) = flexAlignLeftCenter
        .TextMatrix(0, lColOrdem) = "Ordem"
                
        .WordWrap = False
        .row = 0
        .Col = 0
    End With

End Sub

Sub PreencheCampos()
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
End Function

Sub FuncaoProcurar()
End Sub

Sub FuncaoAnterior()
End Sub

Sub FuncaoSeguinte()
End Sub

Sub FuncaoInserir()
End Sub

Sub BD_Insert()
End Sub

Sub FuncaoModificar()
End Sub

Sub BD_Update()
End Sub

Sub FuncaoRemover()
End Sub

Sub BD_Delete()
End Sub


Sub PreencheValoresDefeito()
    AtualizaOrdens
    InicializaTreeGrupos
End Sub

' ----------------------------------------------------------------------------------

' CARREGA A ARVORE DOS Grupos

' ----------------------------------------------------------------------------------
Private Sub InicializaTreeGrupos()
    Dim sSql As String
    Dim rsGrupo As New ADODB.recordset
    Dim i As Integer
    Dim iTree As Integer
    Dim iGr As Integer
    Dim iSgr As Integer
    Dim iAna As Integer
    Dim flgGr As Boolean
    Dim flgSgr As Boolean
    Dim flgAna As Boolean
    Dim root As Node
   
    On Error GoTo TrataErro
    AtualizaOperacao 1, "A preencher ordens..."
    
    FGAnalises.rows = 2
    For i = 0 To FGAnalises.Cols - 1
        FGAnalises.TextMatrix(1, i) = ""
    Next
    TreeViewGrupos.Nodes.Clear
    TreeViewGrupos.LineStyle = tvwRootLines
    TreeViewGrupos.ImageList = ImageListGrupos
    iTree = 1
    
    TotalGrupos = 0
    ReDim EstrutGrupos(0)
    
    sSql = "SELECT x1.cod_ana, x1.descr_ana, x1.ordem, x2.cod_gr_ana, x2.descr_gr_ana, x2.ordem_imp ordem_gr_ana, x3.cod_sgr_ana, x3.descr_sgr_ana, x3.ordem_imp ordem_sgr_ana, "
    sSql = sSql & " x1.flg_invisivel "
    sSql = sSql & " FROM slv_analises x1 LEFT OUTER JOIN sl_gr_ana x2 ON x1.gr_ana = x2.cod_gr_ana LEFT OUTER JOIN sl_sgr_ana x3 ON x1.cod_sgr_ana = x3.cod_sgr_ana "
    sSql = sSql & " WHERE x1.cod_ana = x1.cod_ana "
    If CkExcluiCanceladas.value = vbChecked Then
        sSql = sSql & " AND (flg_invisivel = 0 OR flg_invisivel IS NULL) "
    End If
    If CkExcluiMembros.value = vbChecked Then
        sSql = sSql & " AND cod_ana NOT IN (SELECT cod_membro FROM sl_membro) "
        sSql = sSql & " AND cod_ana NOT IN (SELECT cod_analise FROM sl_ana_perfis where cod_perfis in (Select cod_perfis from sl_perfis where flg_activo = 1) )"
    End If
    
    sSql = sSql & " ORDER BY ordem_gr_ana, ordem_sgr_ana, ordem "
    rsGrupo.CursorLocation = adUseServer
    rsGrupo.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsGrupo.Open sSql, gConexao
    If rsGrupo.RecordCount >= 1 Then
        
        While Not rsGrupo.EOF
            flgGr = False
            flgAna = False
            flgSgr = False
            
            ' PREENCHE GRUPOS DE ANALISES
            For iGr = 1 To TotalGrupos
                If EstrutGrupos(iGr).Codigo = BL_HandleNull(rsGrupo!cod_gr_ana, "--") Then
                    flgGr = True
                    Exit For
                End If
            Next iGr
            If flgGr = False Then
                TotalGrupos = TotalGrupos + 1
                ReDim Preserve EstrutGrupos(TotalGrupos)
                EstrutGrupos(TotalGrupos).Codigo = BL_HandleNull(rsGrupo!cod_gr_ana, "--")
                EstrutGrupos(TotalGrupos).descricao = BL_HandleNull(rsGrupo!descr_gr_ana, "Sem Grupo Definido")
                EstrutGrupos(TotalGrupos).ordem = BL_HandleNull(rsGrupo!ordem_gr_ana, 9999)
                EstrutGrupos(TotalGrupos).chave = "GR_" & EstrutGrupos(TotalGrupos).Codigo
                EstrutGrupos(TotalGrupos).indice = iTree
                EstrutGrupos(TotalGrupos).TotalSubGrupos = 0
                ReDim EstrutGrupos(TotalGrupos).EstrutSubGrupos(0)
                iGr = TotalGrupos
                Set root = TreeViewGrupos.Nodes.Add(, tvwChild, EstrutGrupos(TotalGrupos).chave, EstrutGrupos(TotalGrupos).ordem & "-" & EstrutGrupos(TotalGrupos).descricao, DevolveImagemGrupos(lGrupo))
                root.Expanded = False
                iTree = iTree + 1
            End If
            flgSgr = False
            ' SUBGRUPOS DE ANALISES
            For iSgr = 1 To EstrutGrupos(iGr).TotalSubGrupos
                If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).Codigo = BL_HandleNull(rsGrupo!cod_sgr_ana, "--") Then
                    flgSgr = True
                    Exit For
                End If
            Next iSgr
            
            If flgSgr = False Then
                EstrutGrupos(iGr).TotalSubGrupos = EstrutGrupos(iGr).TotalSubGrupos + 1
                ReDim Preserve EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos)
                
                EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos).Codigo = BL_HandleNull(rsGrupo!cod_sgr_ana, "--")
                EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos).descricao = BL_HandleNull(rsGrupo!descr_sgr_ana, "Sem SubGrupo Definido")
                EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos).ordem = BL_HandleNull(rsGrupo!ordem_sgr_ana, 9999)
                EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos).chave = EstrutGrupos(iGr).chave & "_SGR_" & EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos).Codigo
                EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos).indice = iTree
                EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos).totalAnalises = 0
                ReDim EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos).estrutAnalises(EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos).totalAnalises)
                iSgr = EstrutGrupos(iGr).TotalSubGrupos
                
                Set root = TreeViewGrupos.Nodes.Add(EstrutGrupos(iGr).indice, tvwChild, EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos).chave, EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos).ordem & "-" & EstrutGrupos(iGr).EstrutSubGrupos(EstrutGrupos(iGr).TotalSubGrupos).descricao, DevolveImagemGrupos(lSubGrupo))
                root.Expanded = False
                iTree = iTree + 1
            End If
            
            flgAna = False
            ' ANALISES
            For iAna = 1 To EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises
                If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).Codigo = BL_HandleNull(rsGrupo!cod_ana, "--") Then
                    flgAna = True
                    Exit For
                End If
            Next iAna
            
            If flgAna = False Then
                EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises + 1
                ReDim Preserve EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises)
                
                EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises).Codigo = BL_HandleNull(rsGrupo!cod_ana, "--")
                EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises).descricao = BL_HandleNull(rsGrupo!descr_ana, "Sem Descri��o")
                EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises).ordem = BL_HandleNull(rsGrupo!ordem, 9999)
                EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises).indiceFg = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises
                EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises).estado = BL_HandleNull(rsGrupo!flg_invisivel, 0)
                EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises).flgAlterada = False
                iAna = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises
            End If
            
            rsGrupo.MoveNext
        Wend
    End If
    rsGrupo.Close
    Set rsGrupo = Nothing
    AtualizaOperacao 1, "Done!"
    AtualizaOperacao 0, ""
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  InicializaTreeProfiles " & Err.Description, Me.Name, "InicializaTreeProfiles", True
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' EXPANDE OU COMPRIME UM NO DA ARVORE

' ----------------------------------------------------------------------------------
Private Sub expandeNo(no As Node, expande As Boolean)
    On Error GoTo TrataErro
    If (no Is Nothing) Then
      Exit Sub
    End If
    no.Expanded = expande
    Dim i As Integer
    Dim filho As Node
    Set filho = no.Child
    For i = 1 To no.children
      expandeNo filho, expande
      Set filho = filho.Next
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  expandeNo " & Err.Description, Me.Name, "expandeNo"
    Exit Sub
    Resume Next
End Sub



' ----------------------------------------------------------------------------------

' DEVOLVE A IMAGEM DE ACORDO COM O ESTADO DO MENU

' ----------------------------------------------------------------------------------
Private Function DevolveImagemGrupos(state As Long) As String
    On Error GoTo TrataErro
    
    If (state = lGrupo) Then
      DevolveImagemGrupos = "Grupo"
    ElseIf (state = lSubGrupo) Then
      DevolveImagemGrupos = "SubGrupo"
    ElseIf (state = lTodos) Then
      DevolveImagemGrupos = "Todos"
    End If
Exit Function
TrataErro:
    DevolveImagemGrupos = ""
    BG_LogFile_Erros "Erro  DevolveImagemGrupos " & Err.Description, Me.Name, "DevolveImagemGrupos"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' PREENCHE FLEX GRID

' ----------------------------------------------------------------------------------
Private Sub TreeViewGrupos_Click()
    Dim i As Integer
    Dim j As Integer
    Dim iGr As Integer
    Dim iSgr As Integer
    Dim iAna As Integer
    
    On Error GoTo TrataErro
    FGAnalises.Enabled = False
    TreeViewGrupos.Enabled = False
    FGAnalises.rows = 2

    For i = 0 To FGAnalises.Cols - 1
        FGAnalises.TextMatrix(1, i) = ""
    Next
    iGr = mediComboValorNull
    iSgr = mediComboValorNull
    iAna = mediComboValorNull
    
    For iGr = 1 To TotalGrupos
        If TreeViewGrupos.SelectedItem.Index = EstrutGrupos(iGr).indice Then
            For iSgr = 1 To EstrutGrupos(iGr).TotalSubGrupos
                For iAna = 1 To EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises
                    FGAnalises.TextMatrix(FGAnalises.rows - 1, lColCodigo) = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).Codigo
                    FGAnalises.TextMatrix(FGAnalises.rows - 1, lColDescricao) = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).descricao
                    If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).estado = 1 Then
                        FGAnalises.TextMatrix(FGAnalises.rows - 1, lColEstado) = "Cancelada"
                    Else
                        FGAnalises.TextMatrix(FGAnalises.rows - 1, lColEstado) = "Activa"
                    End If
                    FGAnalises.TextMatrix(FGAnalises.rows - 1, lColOrdem) = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem
                    FGAnalises.Col = lColOrdem
                    FGAnalises.row = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).indiceFg
                    If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).flgAlterada = True Then
                        FGAnalises.CellBackColor = AmareloClaro
                    Else
                        FGAnalises.CellBackColor = vbWhite
                    End If
        
                    FGAnalises.AddItem ""
                Next iAna
            Next iSgr
        Else
            For iSgr = 1 To EstrutGrupos(iGr).TotalSubGrupos
                If TreeViewGrupos.SelectedItem.Index = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).indice Then
                    For iAna = 1 To EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises
                        FGAnalises.TextMatrix(FGAnalises.rows - 1, lColCodigo) = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).Codigo
                        FGAnalises.TextMatrix(FGAnalises.rows - 1, lColDescricao) = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).descricao
                        If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).estado = 1 Then
                            FGAnalises.TextMatrix(FGAnalises.rows - 1, lColEstado) = "Cancelada"
                        Else
                            FGAnalises.TextMatrix(FGAnalises.rows - 1, lColEstado) = "Activa"
                        End If
                        FGAnalises.TextMatrix(FGAnalises.rows - 1, lColOrdem) = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem
                        FGAnalises.Col = lColOrdem
                        FGAnalises.row = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).indiceFg
                        If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).flgAlterada = True Then
                            FGAnalises.CellBackColor = AmareloClaro
                        Else
                            FGAnalises.CellBackColor = vbWhite
                        End If
            
                        FGAnalises.AddItem ""
                    Next iAna
                    Exit For
                End If
            Next iSgr
            
        End If
    Next iGr
    FGAnalises.Enabled = True
    TreeViewGrupos.Enabled = True
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  TreeViewGrupos_Click " & Err.Description, Me.Name, "TreeViewGrupos_Click"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' ACTUALIZA ORDENS DAS REQUISICOES QUE EST�O PENDENTES

' ----------------------------------------------------------------------------------

Private Function ActualizaRequisicoes() As Boolean
    Dim sSql As String
    On Error GoTo TrataErro
    AtualizaOperacao 1, "Atualizar resultados"
    sSql = "UPDATE sl_realiza SET ord_ana = nvl((select ordem from slv_analises where sl_realiza.cod_agrup =slv_analises.cod_ana),ord_ana)"
    sSql = sSql & " WHERE n_req in (select distinct n_req from sl_marcacoes) "
    BG_ExecutaQuery_ADO sSql
    
    AtualizaOperacao 1, "Atualizar An�lises marcadas"
    sSql = " update sl_marcacoes set ord_ana =  nvl((select ordem from slv_analises where sl_marcacoes.cod_agrup =slv_analises.cod_ana),ord_ana)"
    BG_ExecutaQuery_ADO sSql

    
    ActualizaRequisicoes = True
Exit Function
TrataErro:
    ActualizaRequisicoes = False
    BG_LogFile_Erros "Erro  ActualizaRequisicoes " & Err.Description, Me.Name, "ActualizaRequisicoes"
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------------------

' REORDENA A ESTRUTURA

' ----------------------------------------------------------------------------------
Private Function ReordenaEstrutAna(iOld As Integer, iNew As Integer, codAna As String) As Long
    Dim iGr As Integer
    Dim iSgr As Integer
    Dim iAna As Integer
    
    If iNew < iOld Then
        For iGr = 1 To TotalGrupos
            For iSgr = 1 To EstrutGrupos(iGr).TotalSubGrupos
                For iAna = 1 To EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises
                    If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem >= iNew Then
                        If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem + 1 <= iOld Then
                            If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).Codigo <> codAna Then
                                EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).flgAlterada = True
                                EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem + 1

                            End If
                        End If
                    End If
                Next iAna
            Next iSgr
        Next iGr
        
    ElseIf iNew > iOld Then
        For iGr = 1 To TotalGrupos
            For iSgr = 1 To EstrutGrupos(iGr).TotalSubGrupos
                For iAna = 1 To EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises
                    If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem > iOld Then
                        If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem <= iNew Then
                            If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).Codigo <> codAna Then
                                EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).flgAlterada = True
                                EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).ordem - 1

                            End If
                        End If
                    End If
                Next iAna
            Next iSgr
        Next iGr
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  ao reordenar estrutura " & Err.Description, Me.Name, "ReordenaEstrutAna", True
    Exit Function
    Resume Next
End Function

Private Sub OrdenaAnalises(indice As Long, ordem As Long)
    Dim i As Long
'    For i = 2 To totalAnalises
'        If estrutAnalises(i).ordem <= estrutAnalises(i - 1).ordem Then
'            estrutAnalises(i).ordem = estrutAnalises(i - 1).ordem + 1
'        End If
'    Next
End Sub

'' ----------------------------------------------------------------------------------
'
'' TROCA ESTRTURA
'
'' ----------------------------------------------------------------------------------
'Private Sub TrocaEstrut(indice1 As Long, indice2 As Long)
'    Dim codigo As String
'    Dim descricao As String
'    Dim estado As Integer
'    Dim grupo As String
'    Dim subGrupo As String
'    Dim ordem As Long
'    Dim tipo As String
'
'    On Error GoTo TrataErro
'
'    codigo = estrutAnalises(indice1).codigo
'    descricao = estrutAnalises(indice1).descricao
'    estado = estrutAnalises(indice1).estado
'    grupo = estrutAnalises(indice1).grupo
'    subGrupo = estrutAnalises(indice1).subGrupo
'    ordem = estrutAnalises(indice1).ordem
'    tipo = estrutAnalises(indice1).tipo
'
'    estrutAnalises(indice1).codigo = estrutAnalises(indice2).codigo
'    estrutAnalises(indice1).descricao = estrutAnalises(indice2).descricao
'    estrutAnalises(indice1).estado = estrutAnalises(indice2).estado
'    estrutAnalises(indice1).grupo = estrutAnalises(indice2).grupo
'    estrutAnalises(indice1).subGrupo = estrutAnalises(indice2).subGrupo
'    estrutAnalises(indice1).ordem = estrutAnalises(indice2).ordem
'    estrutAnalises(indice1).tipo = estrutAnalises(indice2).tipo
'
'    estrutAnalises(indice2).codigo = codigo
'    estrutAnalises(indice2).descricao = descricao
'    estrutAnalises(indice2).estado = estado
'    estrutAnalises(indice2).grupo = grupo
'    estrutAnalises(indice2).subGrupo = subGrupo
'    estrutAnalises(indice2).ordem = ordem
'    estrutAnalises(indice2).tipo = tipo
'Exit Sub
'TrataErro:
'    BG_LogFile_Erros "Erro  ao Trocar estrutura " & Err.Description, Me.Name, "TrocaEstrut", True
'    Exit Sub
'    Resume Next
'End Sub


Private Sub ColocaTop(codAna As String)
    Dim iGr As Integer
    Dim iSgr As Integer
    Dim iAna As Integer
    
    For iGr = 1 To TotalGrupos
        For iSgr = 1 To EstrutGrupos(iGr).TotalSubGrupos
            For iAna = 1 To EstrutGrupos(iGr).EstrutSubGrupos(iSgr).totalAnalises
                If EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).Codigo = codAna Then
                    TreeViewGrupos.Nodes(EstrutGrupos(iGr).EstrutSubGrupos(iSgr).indice).Selected = True
                    TreeViewGrupos_Click
                    DoEvents
                    FGAnalises.topRow = EstrutGrupos(iGr).EstrutSubGrupos(iSgr).estrutAnalises(iAna).indiceFg
                    
                    
                End If
            Next iAna
        Next iSgr
    Next iGr
    
End Sub

Private Sub AtualizaOrdens()
    Dim sSql As String
    Dim RsOrd As New ADODB.recordset
    Dim iOrdem As Long
    On Error GoTo TrataErro
    AtualizaOperacao 1, "A verificar ordens das an�lises"

    sSql = "SELECT x1.cod_ana "
    sSql = sSql & " FROM slv_analises x1 LEFT OUTER JOIN sl_gr_ana x2 ON x1.gr_ana = x2.cod_gr_ana LEFT OUTER JOIN sl_sgr_ana x3 ON x1.cod_sgr_ana = x3.cod_sgr_ana "
    sSql = sSql & " WHERE x1.cod_ana = x1.cod_ana "
    sSql = sSql & " ORDER BY x2.ordem_imp, x3.ordem_imp, ordem "
    RsOrd.CursorLocation = adUseServer
    RsOrd.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsOrd.Open sSql, gConexao
    If RsOrd.RecordCount >= 1 Then
        AtualizaOperacao 1, "A atualizar ordens das an�lises"
        
        iOrdem = 1
        BG_BeginTransaction
        While Not RsOrd.EOF
            Select Case Mid(BL_HandleNull(RsOrd!cod_ana, ""), 1, 1)
                Case "S"
                    sSql = "UPDATE sl_ana_s set ordem  = " & iOrdem & " WHERE cod_ana_s = " & BL_TrataStringParaBD(BL_HandleNull(RsOrd!cod_ana, ""))
                Case "C"
                    sSql = "UPDATE sl_ana_c set ordem  = " & iOrdem & " WHERE cod_ana_c = " & BL_TrataStringParaBD(BL_HandleNull(RsOrd!cod_ana, ""))
                Case "P"
                    sSql = "UPDATE sl_perfis set ordem  = " & iOrdem & " WHERE flg_activo = 1 AND cod_perfis = " & BL_TrataStringParaBD(BL_HandleNull(RsOrd!cod_ana, ""))
            End Select
            BG_ExecutaQuery_ADO sSql
            iOrdem = iOrdem + 1
            RsOrd.MoveNext
        Wend
    End If
    RsOrd.Close
    Set RsOrd = Nothing
    AtualizaOperacao 1, "Atualiza��o com sucesso "
    BG_CommitTransaction
Exit Sub
TrataErro:
    AtualizaOperacao 1, "Atualiza��o com erros"
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro  ao Atualizar ordens " & Err.Description, Me.Name, "AtualizaOrdens", True
    Exit Sub
    Resume Next
End Sub


Private Sub AtualizaOperacao(tipo As Integer, texto As String)
    If tipo = 0 Then
        EcOperacao.Text = ""
        EcOperacao.Visible = False
    Else
        EcOperacao.Text = texto
        EcOperacao.Visible = True
    End If
    DoEvents
End Sub

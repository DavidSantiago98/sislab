VERSION 5.00
Begin VB.Form FormUtilFactExternos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormUtilFactExternos"
   ClientHeight    =   4440
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4575
   Icon            =   "FormUtilFactExternos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4440
   ScaleWidth      =   4575
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcRequisicao 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1080
      TabIndex        =   3
      Top             =   120
      Width           =   1065
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      ItemData        =   "FormUtilFactExternos.frx":000C
      Left            =   240
      List            =   "FormUtilFactExternos.frx":000E
      TabIndex        =   2
      Top             =   600
      Width           =   3015
   End
   Begin VB.CommandButton BtAcrecenta 
      Caption         =   "Adicionar"
      Height          =   375
      Left            =   2280
      TabIndex        =   1
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton BtImprimir 
      Caption         =   "&Imprimir"
      Height          =   615
      Left            =   960
      Picture         =   "FormUtilFactExternos.frx":0010
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   2880
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "Requisi��o"
      Height          =   225
      Left            =   240
      TabIndex        =   4
      Top             =   120
      Width           =   975
   End
End
Attribute VB_Name = "FormUtilFactExternos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Private Type DadosRequisLista
    num_req As String
    seq_utente As String
    estado_req As String
    dt_chegada As String
    dt_val As String
    hr_val As String
    Proven As String
    userVal As String
End Type

Dim ListaRequis() As DadosRequisLista
Dim TamListaRequis As Long

Public rs As ADODB.recordset

Private Sub BtAcrecenta_Click()
    AcrescentaLista
End Sub

Private Sub BtImprimir_Click()

    Dim i As Integer
    Dim sql As String
    Dim RsRequis As ADODB.recordset
    Dim RsUtente As ADODB.recordset
    Dim continua As Boolean
    
    Dim nome As String
    Dim Utente As String
    Dim morada As String
    Dim codPostal As String
    Dim Externo As String
    Dim Sexo As String
    Dim DtNasc As String
    
    On Error GoTo TrataErro
    
    If EcLista.ListCount <= 0 Then
        Exit Sub
    End If
    
    'gImprimirDestino = 0
    gImprimirDestino = 1
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("Facturacao_Externos", "Facturacao Externos", crptToPrinter, False, False)
    Else
        continua = BL_IniciaReport("Facturacao_Externos", "Facturacao Externos", crptToWindow)
    End If
    If continua = False Then Exit Sub

    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
                
    Set RsRequis = New ADODB.recordset
    RsRequis.CursorLocation = adUseServer
    RsRequis.CursorType = adOpenStatic
    
    Set RsUtente = New ADODB.recordset
    RsUtente.CursorLocation = adUseServer
    RsUtente.CursorType = adOpenStatic
    
    sql = "DELETE FROM sl_cr_fact_externos" ' WHERE nome_computador='" & BG_SYS_GetComputerName & "'"
    BG_ExecutaQuery_ADO sql
    
    For i = 1 To TamListaRequis
        sql = "SELECT x1.cod_ana_gh, x1.descr_ana_facturacao, x1.centro_custo, x1.preco FROM sl_ana_facturacao x1, sl_realiza x2 " & _
            " WHERE X1.cod_ana = X2.cod_ana_s And x2.n_req =" & ListaRequis(i).num_req
        sql = sql & " AND cod_portaria IN (Select x3.cod_portaria FROM sl_portarias x3 WHERE x2.dt_cri BETWEEN "
        sql = sql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        RsRequis.Open sql, gConexao
        If RsRequis.RecordCount > 0 Then
            sql = "DELETE FROM sl_cr_fact_externos" ' WHERE nome_computador='" & BG_SYS_GetComputerName & "'"
            BG_ExecutaQuery_ADO sql
            
            While Not RsRequis.EOF
                sql = "INSERT INTO sl_cr_fact_externos(nome_computador, cod_ana_gh, descr_ana_gh, " & _
                    "centro_custo, preco) VALUES('" & BG_SYS_GetComputerName & "', '" & RsRequis!cod_ana_gh & "', " & _
                    "'" & RsRequis!descr_ana_facturacao & "', '" & RsRequis!centro_custo & "', '" & RsRequis!preco & "')"
                BG_ExecutaQuery_ADO sql
                
                RsRequis.MoveNext
            Wend
                'Seleccionar Dados do Utente
                nome = ""
                Utente = ""
                morada = ""
                codPostal = ""
                Externo = ""
                DtNasc = ""
                Sexo = "0"
                sql = "SELECT utente, nome_ute, dt_nasc_ute, descr_mor_ute, " & _
                    " cod_postal_ute, sexo_ute, externo FROM sl_identif " & _
                    " WHERE seq_utente=" & ListaRequis(i).seq_utente
                RsUtente.Open sql, gConexao
                If RsUtente.RecordCount > 0 Then
                    nome = BL_HandleNull(RsUtente!nome_ute, "")
                    Utente = BL_HandleNull(RsUtente!Utente, "")
                    morada = BL_HandleNull(RsUtente!descr_mor_ute, "")
                    codPostal = BL_HandleNull(RsUtente!cod_postal_ute, "")
                    Externo = BL_HandleNull(RsUtente!Externo, "")
                    DtNasc = BL_HandleNull(RsUtente!dt_nasc_ute, "")
                    Sexo = BL_HandleNull(RsUtente!sexo_ute, "0")
                    If CInt(Sexo) = gT_Masculino Then
                        Sexo = "Masculino"
                    ElseIf CInt(Sexo) = gT_Feminino Then
                        Sexo = "Feminino"
                    Else
                        Sexo = ""
                    End If
                End If
                RsUtente.Close
                            
                Report.formulas(0) = "NomeUtente='" & nome & "'"
                Report.formulas(1) = "NumUtente='" & Utente & "'"
                Report.formulas(2) = "Morada='" & morada & "'"
                Report.formulas(3) = "CodPostal='" & codPostal & "'"
                Report.formulas(4) = "Externo='" & Externo & "'"
                Report.formulas(5) = "SexoUtente='" & Sexo & "'"
                Report.formulas(6) = "DtNascUtente='" & DtNasc & "'"
                Report.formulas(7) = "NumReq='" & ListaRequis(i).num_req & "'"
                Report.formulas(8) = "DtChegada='" & ListaRequis(i).dt_chegada & "'"
                Report.formulas(9) = "DtValidacao='" & ListaRequis(i).dt_val & "'"
                Report.formulas(10) = "HrValidacao='" & ListaRequis(i).hr_val & "'"
                Report.formulas(11) = "ProvReq='" & ListaRequis(i).Proven & "'"
                Report.formulas(12) = "RespMedicos='" & ListaRequis(i).userVal & "'"
                
                Call BL_ExecutaReport(, True)
        End If
        RsRequis.Close
    Next i
    
    Set RsRequis = Nothing
    Set RsUtente = Nothing
    
    Report.Reset
    
    EcRequisicao.SetFocus
    
    Exit Sub
    
TrataErro:

    Exit Sub
    Resume Next
    
End Sub

Private Sub EcRequisicao_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        AcrescentaLista
    End If
End Sub

Sub AcrescentaLista()

    Dim rs As ADODB.recordset
    Dim RsProven As ADODB.recordset
    Dim RsRealiza As ADODB.recordset
    Dim sql As String
    Dim DtValidacao As String
    Dim HrValidaca As String
    Dim userVal As String
    
    On Error GoTo TrataErro
    
    If EcRequisicao.Text <> "" Then
        If IsNumeric(EcRequisicao.Text) Then
            If ValidaRepetidas(EcRequisicao.Text) Then
                BG_Mensagem mediMsgBox, "Essa requisi��o j� foi indicada", vbExclamation, App.ProductName
                EcRequisicao.SetFocus
                Sendkeys ("{HOME}+{END}")
                Exit Sub
            End If
            
            Set RsProven = New ADODB.recordset
            RsProven.CursorLocation = adUseServer
            RsProven.CursorType = adOpenStatic
            
            Set RsRealiza = New ADODB.recordset
            RsRealiza.CursorLocation = adUseServer
            RsRealiza.CursorType = adOpenStatic
        
            sql = "SELECT estado_req,seq_utente, dt_chega, cod_proven FROM sl_requis WHERE n_req = " & EcRequisicao.Text & " AND estado_req in ('3','D','F','I') "
            Set rs = New ADODB.recordset
            rs.CursorLocation = adUseServer
            rs.CursorType = adOpenStatic
            rs.Open sql, gConexao
            
            If rs.RecordCount > 0 Then
                TamListaRequis = TamListaRequis + 1
                ReDim Preserve ListaRequis(TamListaRequis)
                ListaRequis(TamListaRequis).num_req = EcRequisicao.Text
                ListaRequis(TamListaRequis).seq_utente = BL_HandleNull(rs!seq_utente)
                ListaRequis(TamListaRequis).estado_req = BL_HandleNull(rs!estado_req)
                ListaRequis(TamListaRequis).dt_chegada = BL_HandleNull(rs!dt_chega, "")
                
                sql = "SELECT descr_proven FROM sl_proven WHERE cod_proven='" & BL_HandleNull(rs!cod_proven, "0") & "'"
                RsProven.Open sql, gConexao
                If RsProven.RecordCount > 0 Then
                    ListaRequis(TamListaRequis).Proven = RsProven!descr_proven
                Else
                    ListaRequis(TamListaRequis).Proven = ""
                End If
                RsProven.Close
                
                userVal = ""
                sql = "SELECT x2.nome, x2.titulo FROM sl_realiza x1, sl_idutilizador x2 WHERE n_req=" & EcRequisicao.Text & _
                    " AND x1.user_val=x2.cod_utilizador"
                RsRealiza.Open sql, gConexao
                If RsRealiza.RecordCount > 0 Then
                    While Not RsRealiza.EOF
                        If InStr(1, userVal, BL_HandleNull(RsRealiza!nome, "")) = 0 Then
                            userVal = userVal & BL_HandleNull(RsRealiza!Titulo, "") & " " & BL_HandleNull(RsRealiza!nome, "") & "|"
                        End If
                        
                        RsRealiza.MoveNext
                    Wend
                End If
                RsRealiza.Close
                ListaRequis(TamListaRequis).userVal = userVal
                
                DtValidacao = ""
                HrValidaca = ""
                Devolve_Dt_Hr_Validacao EcRequisicao.Text, DtValidacao, HrValidaca
                ListaRequis(TamListaRequis).dt_val = DtValidacao
                ListaRequis(TamListaRequis).hr_val = HrValidaca
                
                EcLista.AddItem EcRequisicao.Text
                EcRequisicao.Text = ""
            Else
                BG_Mensagem mediMsgBox, "N� REQUISI��O INCORRECTO", vbInformation, App.ProductName
                EcRequisicao.Text = ""
                EcRequisicao.SetFocus
            End If
            
            rs.Close
            Set rs = Nothing
            
            Set RsProven = Nothing
            Set RsRealiza = Nothing
        End If
    End If
    
    Exit Sub
    
TrataErro:

    BG_Mensagem mediMsgBox, "Erro ao procurar dados da requisi��o." & vbCrLf & Err.Description, vbExclamation, App.ProductName
    Exit Sub
    Resume Next
    
End Sub
Private Sub EcRequisicao_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcRequisicao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Factura��o de Requisi��es Externas"
    Me.left = 540
    Me.top = 450
    Me.Width = 3885
    Me.Height = 4170 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    Set CampoDeFocus = EcRequisicao
    
    TamListaRequis = 0
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormImprimeListaReq = Nothing
End Sub
    
Sub LimpaCampos()
    Me.SetFocus
    
    EcRequisicao.Text = ""
    EcLista.Clear
    TamListaRequis = 0
    ReDim ListaRequis(TamListaRequis)
    
End Sub
    
Sub DefTipoCampos()
    EcRequisicao.Tag = adInteger
End Sub

Sub PreencheCampos()
    
End Sub


Sub FuncaoLimpar()
    LimpaCampos
End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()

End Sub

Sub BD_Insert()
    
End Sub

Sub FuncaoModificar()

End Sub

Sub BD_Update()

End Sub

Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub

Function ValidaRepetidas(NumRequis As String) As Boolean

    Dim i As Integer

    ValidaRepetidas = False
    For i = 1 To TamListaRequis
        If NumRequis = ListaRequis(i).num_req Then
            ValidaRepetidas = True
        End If
    Next i
    
End Function

Sub Devolve_Dt_Hr_Validacao(NumReq As String, DtValidacao As String, HrValidacao As String)
    
    Dim sql As String
    Dim RsValidacao As ADODB.recordset
    
    On Error GoTo TrataErro
    
    DtValidacao = ""
    HrValidacao = ""
    sql = "SELECT dt_val, hr_val FROM sl_realiza WHERE n_req=" & NumReq & _
        " AND dt_val=(SELECT min(dt_val) FROM sl_realiza WHERE n_req=" & NumReq & ") ORDER BY dt_val desc,hr_val desc"
    Set RsValidacao = New ADODB.recordset
    RsValidacao.CursorLocation = adUseServer
    RsValidacao.CursorType = adOpenStatic
    RsValidacao.Open sql, gConexao
    If RsValidacao.RecordCount > 0 Then
        RsValidacao.MoveFirst
        DtValidacao = BL_HandleNull(RsValidacao!dt_val, "")
        HrValidacao = BL_HandleNull(RsValidacao!hr_val, "")
    End If
    RsValidacao.Close
    Set RsValidacao = Nothing

    Exit Sub
    
TrataErro:

    DtValidacao = ""
    HrValidacao = ""
    Exit Sub

End Sub



VERSION 5.00
Begin VB.Form FormSerCodCaixas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormSerCodCaixas"
   ClientHeight    =   5895
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7020
   Icon            =   "FormSerCodCaixas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5895
   ScaleWidth      =   7020
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtAnalises 
      Height          =   495
      Left            =   5520
      Picture         =   "FormSerCodCaixas.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   25
      ToolTipText     =   "An�lises"
      Top             =   840
      Width           =   615
   End
   Begin VB.CommandButton BtImprEtiq 
      Height          =   495
      Left            =   6240
      Picture         =   "FormSerCodCaixas.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   840
      Width           =   615
   End
   Begin VB.TextBox EcPrinterEtiq 
      Height          =   285
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   23
      Top             =   8280
      Width           =   495
   End
   Begin VB.TextBox EcTemperatura 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4920
      TabIndex        =   21
      Top             =   840
      Width           =   375
   End
   Begin VB.TextBox EcPrazoCong 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3360
      TabIndex        =   19
      Top             =   840
      Width           =   375
   End
   Begin VB.TextBox EcVert 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   840
      TabIndex        =   17
      Top             =   840
      Width           =   375
   End
   Begin VB.TextBox EcHoriz 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   840
      TabIndex        =   15
      Top             =   480
      Width           =   375
   End
   Begin VB.TextBox EcUserAct 
      Height          =   285
      Left            =   1800
      TabIndex        =   14
      Top             =   6240
      Width           =   975
   End
   Begin VB.TextBox EcUserCri 
      Height          =   285
      Left            =   360
      TabIndex        =   13
      Top             =   6360
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Height          =   795
      Left            =   120
      TabIndex        =   8
      Top             =   5040
      Width           =   6885
      Begin VB.TextBox EcHoraAlteracao 
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   5400
         TabIndex        =   29
         Top             =   480
         Width           =   1455
      End
      Begin VB.TextBox EcDataAlteracao 
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   3240
         TabIndex        =   28
         Top             =   480
         Width           =   1455
      End
      Begin VB.TextBox EcHoraCriacao 
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   5400
         TabIndex        =   27
         Top             =   120
         Width           =   1455
      End
      Begin VB.TextBox EcDataCriacao 
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   3240
         TabIndex        =   26
         Top             =   120
         Width           =   1455
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   12
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   11
         Top             =   120
         Width           =   1695
      End
      Begin VB.Label Label7 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   480
         Width           =   765
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   195
         Width           =   615
      End
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      Height          =   3540
      Left            =   120
      TabIndex        =   7
      Top             =   1440
      Width           =   6735
   End
   Begin VB.ComboBox CbSeroteca 
      Height          =   315
      Left            =   3360
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   480
      Width           =   3495
   End
   Begin VB.TextBox EcTamanho 
      Appearance      =   0  'Flat
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2070
         SubFormatType   =   1
      EndProperty
      Height          =   285
      Left            =   4200
      TabIndex        =   4
      Top             =   6840
      Width           =   735
   End
   Begin VB.TextBox EcDescricao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3360
      TabIndex        =   3
      Top             =   120
      Width           =   3495
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   840
      TabIndex        =   2
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Temperatura:"
      Height          =   255
      Index           =   6
      Left            =   3840
      TabIndex        =   22
      Top             =   840
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Prazo Congela��o"
      Height          =   255
      Index           =   5
      Left            =   1800
      TabIndex        =   20
      Top             =   840
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Colunas"
      Height          =   255
      Index           =   4
      Left            =   240
      TabIndex        =   18
      Top             =   840
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Linhas"
      Height          =   255
      Index           =   2
      Left            =   240
      TabIndex        =   16
      Top             =   480
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Arquivo"
      Height          =   255
      Index           =   3
      Left            =   1800
      TabIndex        =   6
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Descri��o"
      Height          =   255
      Index           =   1
      Left            =   1800
      TabIndex        =   1
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "FormSerCodCaixas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Private Sub BtAnalises_Click()
    FormAnaCaixaSeroteca.Show
End Sub

Private Sub EcHoriz_validate(Cancel As Boolean)
    If EcHoriz = "" Then EcHoriz = "0"
    If IsNumeric(EcHoriz) = False Then
        Cancel = True
        EcHoriz = ""
        Exit Sub
    End If
    EcTamanho = BL_HandleNull(EcHoriz, 0) * BL_HandleNull(EcVert, 0)
End Sub



Private Sub EcVert_validate(Cancel As Boolean)
    If EcVert = "" Then EcVert = "0"
    EcTamanho = BL_HandleNull(EcHoriz, 0) * BL_HandleNull(EcVert, 0)
End Sub
Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub
Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub

Private Sub EcDescricao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDescricao_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Arquivo - Codifica��o de Suportes"
    Me.left = 540
    Me.top = 450
    Me.Width = 7110
    Me.Height = 6315 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "SL_SER_CAIXAS"
    Set CampoDeFocus = EcDescricao
    
    NumCampos = 14
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "cod_caixa"
    CamposBD(1) = "descr_caixa"
    CamposBD(2) = "tamanho"
    CamposBD(3) = "cod_seroteca"
    CamposBD(4) = "user_cri"
    CamposBD(5) = "dt_cri"
    CamposBD(6) = "hr_cri"
    CamposBD(7) = "user_act"
    CamposBD(8) = "dt_act"
    CamposBD(9) = "hr_act"
    CamposBD(10) = "hor"
    CamposBD(11) = "ver"
    CamposBD(12) = "prazo_cong"
    CamposBD(13) = "temperatura"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcDescricao
    Set CamposEc(2) = EcTamanho
    Set CamposEc(3) = CbSeroteca
    Set CamposEc(4) = EcUserCri
    Set CamposEc(5) = EcDataCriacao
    Set CamposEc(6) = EcHoraCriacao
    Set CamposEc(7) = EcUserAct
    Set CamposEc(8) = EcDataAlteracao
    Set CamposEc(9) = EcHoraAlteracao
    Set CamposEc(10) = EcHoriz
    Set CamposEc(11) = EcVert
    Set CamposEc(12) = EcPrazoCong
    Set CamposEc(13) = EcTemperatura
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Descri��o do Suporte"
    TextoCamposObrigatorios(2) = "Capacidade do Suporte"
    TextoCamposObrigatorios(3) = "Arquivo de Amostras"
    TextoCamposObrigatorios(10) = "N�mero de Linhas"
    TextoCamposObrigatorios(11) = "N�mero de Colunas"
    TextoCamposObrigatorios(12) = "Prazo Congela��o"
      
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_caixa"
    Set ChaveEc = EcCodigo
    
    CamposBDparaListBox = Array("cod_caixa", "cod_seroteca", "descr_caixa")
    NumEspacos = Array(22, 15, 32)
    EcPrinterEtiq.Text = BL_SelImpressora("Etiqueta.rpt")
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormSerCodCaixas = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    EcCodigo.Enabled = True
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
End Sub

Sub DefTipoCampos()
    EcDataAlteracao.Tag = adDate
    EcDataCriacao.Tag = adDate
    BG_PreencheComboBD_ADO "sl_serotecas", "cod_seroteca", "descr_seroteca", CbSeroteca, mediAscComboCodigo
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUserCri, EcUserAct, LaUtilCriacao, LaUtilAlteracao
        EcCodigo.Enabled = False
    End If
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_caixa ASC "
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'
        
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUserCri = gCodUtilizador
        EcDataCriacao.Text = Bg_DaData_ADO
        EcHoraCriacao.Text = Bg_DaHora_ADO
        iRes = ValidaCamposEc
        BG_BeginTransaction
        If iRes = True Then
            BD_Insert
        End If
        BG_CommitTransaction
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
        
    Dim SQLQuery As String

    gSQLError = 0
    gSQLISAM = 0
    
    If EcCodigo.Text = "" Or IsNumeric(EcCodigo.Text) = False Then
        EcCodigo = SER_DevolveIndice("CAIXA", mediComboValorNull, CbSeroteca.ItemData(CbSeroteca.ListIndex))
    End If
    If EcCodigo <> mediComboValorNull Then
        SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
        BG_ExecutaQuery_ADO SQLQuery
    End If
    InsereReutil
    
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao.Text = Bg_DaData_ADO
        EcUserAct = gCodUtilizador
        EcHoraAlteracao.Text = Bg_DaHora_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'

    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    
    LimpaCampos
    PreencheCampos

End Sub

Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub



Private Sub InsereReutil()
    Dim sSql As String
    Dim seq_reutil As Long
    seq_reutil = SER_DevolveIndice("REUTIL", CLng(EcCodigo), CbSeroteca.ItemData(CbSeroteca.ListIndex))
    
    sSql = "INSERT INTO sl_ser_reutil_caixa (cod_seroteca, cod_caixa, seq_reutil, dt_cri, hr_cri, user_cri,flg_activo) VALUES("
    sSql = sSql & CbSeroteca.ItemData(CbSeroteca.ListIndex) & ","
    sSql = sSql & EcCodigo & ","
    sSql = sSql & seq_reutil & ","
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ","
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ","
    sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ","
    sSql = sSql & "1)"
    BG_ExecutaQuery_ADO sSql
End Sub

Private Function EtiqPrint(ByVal cod_caixa As Long, descr_caixa As Long, cod_seroteca As Long, descr_seroteca As String) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{COD_CAIXA}", cod_caixa)
    sWrittenData = Replace(sWrittenData, "{DESCR_CAIXA}", descr_caixa)
    sWrittenData = Replace(sWrittenData, "{COD_SEROTECA}", cod_seroteca)
    sWrittenData = Replace(sWrittenData, "{DESCR_SEROTECA}", descr_seroteca)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True

End Function




Private Sub BtImprEtiq_Click()
    Dim i As Integer
    Dim j As Integer
    Dim NReq As Long
    Dim qtd As Integer
    Dim EtqCrystal As String
    
    If EcUserCri = "" Then
        Exit Sub
    End If
    If Not BL_LerEtiqIni("Suporte.ini") Then
        MsgBox "Ficheiro de inicializa��o de etiquetas de suportes de seroteca ausente!"
        Exit Sub
    End If
    If Not BL_EtiqOpenPrinter(EcPrinterEtiq) Then
        MsgBox "Imposs�vel abrir impressora etiquetas de suportes de seroteca"
        Exit Sub
    End If
    
    Call EtiqPrint(EcCodigo, EcDescricao, CbSeroteca.ItemData(CbSeroteca.ListIndex), CbSeroteca)
    
    BL_EtiqClosePrinter
End Sub


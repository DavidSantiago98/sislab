VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClDadosAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' Actualização : --/--/----
' Técnico

Public SeqRealiza As Double
Public CodPerfil As String
Public DescrPerfil As String
Public CodAnaC As String
Public DescrAnaC As String
Public codAnaS As String
Public DescrAnaS As String
Public codAgrup As String
Public OrdMarca As String
Public ord As String
Public OrdC As String
Public OrdP As String
Public FTrab As String
Public TRes As String
Public UnidActiva As String
Public Unid1 As String
Public Unid2 As String
Public ConvU1U2 As String
Public ValRef As String
Public res As String

Public ResAnter As String
Public ResAnter2 As String
Public ResAnter3 As String

Public CorResAnter As Variant
Public CorResAnter2 As Variant
Public CorResAnter3 As Variant

Public FlgResAnter As String
Public FlgResAnter2 As String
Public FlgResAnter3 As String

Public DtAnter As String
Public DtAnter2 As String
Public DtAnter3 As String

Public DescrFrAlf As String
Public Aviso As String
Public formula As String
Public CasasDec As String
Public LimInf As String
Public LimSup As String
Public ValDefeito As String
Public DtChega As String
Public check As String
Public Info As String
Public CorRes As Variant
Public CorResSel As Variant
Public FrasesIndex As Long
Public MicroIndex As Long
Public AntibIndex As Long
Public UserCri As String
Public DtCri As String
Public HrCri As String
Public UserAct As String
Public DtAct As String
Public HrAct As String
Public estado As Integer

Public userVal As String
Public dtVal As String
Public hrVal As String

Public UserValTec As String
Public DtValTec As String
Public HrValTec As String

Public ObsAna As String
Public SeqObsAna As Long
Public FlgCutOff As String
Public Flg1paraX As String
Public ComAna As String
Public ConcAna As String
Public ComMa As String
Public ComMi As String
Public COMinNeg As String
Public COMaxNeg As String
Public COComNeg As String
Public COMinPos As String
Public COMaxPos As String
Public COComPos As String
Public COMinDuv As String
Public COMaxDuv As String
Public COComDuv As String
Public Dados2Res As New ClDados2Res
Public ResMicro As New Collection
Public ResFrases As New Collection
Public ResAntib As New Collection

Public FlgAparTrans As String
Public FlgFacturado As Integer
Public CodProd As String

Public UsaFormulaVolume As Boolean
Public AnaVolume As String

Public UserReg As String
Public DtReg As String
Public HrReg As String
Public EstadoReg As String
Public ana_sReg As String
Public ana_cReg As String
Public ana_pReg As String

'Para o form de consulta de requisições: ordem da frase
Public OrdemFrase As Integer

'INTERFERENCIAS
Public CodAccaoInterferencia As Integer
Public CodEfeitoInterferencia As String
Public CodInterferencia As String

'SE FOR DENTRO DE COMPLEXA DA TAB
Public prefixo As String
Public obs As Boolean

Public seqReqTubo As Long
Public flgImprimir As Integer




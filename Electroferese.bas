Attribute VB_Name = "Electroferese"
Option Explicit

' Actualiza��o : 23/01/2002
' Paulo Costa

' Painel.

'Global Const EF_LARGURA_PAINEL = 12000
Global Const EF_ALTURA_PAINEL = 5300

' Global Const EF_COR_FUNDO_PAINEL = &H80000005
' Global Const EF_COR_FUNDO_PAINEL = &H4000&

Global Const EF_COR_FUNDO_PAINEL = &H80000005

' Tra�o.

Global Const EF_LARGURA_TRACO = 10

Global Const EF_COR_TRACO = vbBlack
Global Const EF_COR_EIXO = &HBA644B

'Global Const EF_COR_TRACO = &HC00000

Global Const EF_ESPESSURA_TRACO_X = 1
Global Const EF_ESPESSURA_TRACO_Y = 1
' Origem.

Global EF_X0 As Double
Global EF_Y0 As Double

' Cont�nuo.

Global EF_X_ULTIMO As Double
Global EF_Y_ULTIMO As Double

' Calibragem.

Global EF_ZOOM_X   As Integer

Global EF_ZOOM_Y As Integer

Global Const EF_AJUSTE_X = 1#
'Global Const EF_AJUSTE_Y = 11250#
Global Const EF_AJUSTE_Y = 11200#

' JPEG
Global Const EF_JPG_QUALITY = 1000

' Inicializa o gr�fico.

Public Function EF_Ini_Grafico(p As PictureBox, EF_LARGURA_PAINEL As Long) As Integer

    On Error GoTo ErrorHandler
    
    p.BackColor = EF_COR_FUNDO_PAINEL
    p.ForeColor = EF_COR_TRACO
    
    p.Width = EF_LARGURA_PAINEL
    p.Height = EF_ALTURA_PAINEL

    EF_X0 = EF_LARGURA_PAINEL / EF_LARGURA_PAINEL
    EF_Y0 = EF_ALTURA_PAINEL / 2#

    p.AutoRedraw = True
    
    EF_Ini_Grafico = 1
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
'            Call ErrorToLog("EF_Ini_Grafico (Electroferese)", Err.Number, Err.Description, Err.Source, "Erro Inesperado                                    ")
            EF_Ini_Grafico = -1
            Exit Function
    End Select
End Function

' Escreve um ponto do gr�fico.

Public Sub EF_Ponto(p As PictureBox, _
                    X As Double, _
                    Y As Double, _
                    Cor As Double)

    On Error GoTo ErrorHandler
    
    Dim x_aux As Double
    Dim y_aux As Double
    
    Call EF_Transf_Coords(X, Y, x_aux, y_aux)

    p.PSet (x_aux, y_aux), Cor

    EF_X_ULTIMO = x_aux
    EF_Y_ULTIMO = y_aux

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
'            Call ErrorToLog("EF_Ponto (Electroferese)", Err.Number, Err.Description, Err.Source, "Erro Inesperado                                    ")
            Exit Sub
    End Select
End Sub

' Escreve um ponto do gr�fico e liga-o ao �ltimo.

Public Sub EF_Ponto_Continuo(p As PictureBox, _
                          X As Double, _
                          Y As Double, _
                          Cor As Double)

    On Error GoTo ErrorHandler
    Dim i As Long
    Dim x_aux As Double
    Dim y_aux As Double
    Dim declive As Double
    Dim b As Double
    Dim y3 As Double
    
    Call EF_Transf_Coords(X, Y, x_aux, y_aux)
    'p.PSet (x_aux, y_aux), Cor
    p.Line (x_aux, y_aux)-(EF_X_ULTIMO, EF_Y_ULTIMO), Cor
'
    p.Line (x_aux + EF_ESPESSURA_TRACO_X, y_aux - EF_ESPESSURA_TRACO_Y)-(EF_X_ULTIMO + EF_ESPESSURA_TRACO_X, EF_Y_ULTIMO - EF_ESPESSURA_TRACO_Y), Cor
    
        If (x_aux - EF_X_ULTIMO) <> 0 Then
            declive = (EF_Y_ULTIMO - y_aux) / (x_aux - EF_X_ULTIMO)
        Else
            declive = 0
        End If
        b = Y - (declive * x_aux)
'        For x3 = x2 To X Step 0.1
'            y3 = declive * x3 + c
'            If ((X < 30000) And (Y < 30000)) Then
'
'                Call EF_Ponto_Continuo(p, x3, y3, EF_COR_TRACO)
'
'            End If
'
'        Next
'
'        x2 = X
'        y2 = Y
    For i = EF_X_ULTIMO To x_aux
        y3 = declive * i + b
       ' p.Line (i, 4770 - y3)-(i, 4770), &HE0E0E0
    Next
    
    EF_X_ULTIMO = x_aux
    EF_Y_ULTIMO = y_aux
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
'            Call ErrorToLog("EF_Ponto_Continuo (Electroferese)", Err.Number, Err.Description, Err.Source, "Erro Inesperado                                    ")
           Exit Sub
    End Select
End Sub

' Transforma coordenadas reais para visualiz�veis.

Private Sub EF_Transf_Coords(X1 As Double, _
                             Y1 As Double, _
                             ByRef x2 As Double, _
                             ByRef y2 As Double)

    On Error GoTo ErrorHandler
    
        
    x2 = (X1 + EF_X0) * EF_ZOOM_X + EF_AJUSTE_X
    'y2 = (EF_ALTURA_PAINEL - y1 - EF_Y0) * EF_ZOOM_Y + EF_AJUSTE_Y
    y2 = (EF_ALTURA_PAINEL - Y1) * 0.9 '* EF_ZOOM_Y

'y2 = y1

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
'            Call ErrorToLog("EF_Transf_Coords (Electroferese)", Err.Number, Err.Description, Err.Source, "Erro Inesperado                                    ")
            Exit Sub
    End Select
End Sub

Public Function EF_Cria_Grafico_Electroferese(Pontos As String, _
                                              p As PictureBox, _
                                              Optional path_saida As String, _
                                              Optional EF_LARGURA_PAINEL As Long) As Integer

    On Error GoTo ErrorHandler
    
    Dim rv  As Integer
    Dim lista_pontos As String
    If EF_LARGURA_PAINEL = 0 Then
        EF_LARGURA_PAINEL = 12000
        EF_ZOOM_X = 6
        EF_ZOOM_Y = 3
    Else
        EF_ZOOM_X = 2
        EF_ZOOM_Y = 1
    End If
    Dim r As RegExp
    Dim mc As MatchCollection
    Dim i As Long
    Dim X As Double
    Dim Y As Double
    Dim x2 As Double
    Dim y2 As Double
    Dim declive As Double
    Dim x3 As Double
    Dim y3 As Double
    Dim c As Double
    ' ----------------------------------
    
    Pontos = Trim(Pontos)
    
    If (Len(Pontos) = 0) Then
        EF_Cria_Grafico_Electroferese = -1
        Exit Function
    End If
                          
    lista_pontos = Pontos
    
    ' ----------------------------------
    
    Call EF_Ini_Grafico(p, EF_LARGURA_PAINEL)
    
    Set r = New RegExp
    
    r.Global = True
    r.IgnoreCase = True
    r.Pattern = "[-01234567890.]+"
    
    Set mc = r.Execute(lista_pontos)
    
    i = 0
    
    ' 1� ponto.
    If mc.Count > 0 Then
        
        X = CDbl(Trim(mc.item(0)))
        Y = CDbl(Trim(mc.item(1)))
        
        Call EF_Ponto(p, X, Y, EF_COR_TRACO)
    
    End If
    
    ' Tra�a o gr�fico.
    While i < mc.Count - 1
        
        X = CDbl(Trim(mc.item(i)))
        Y = CDbl(Trim(mc.item(i + 1)))
        If ((X < 30000) And (Y < 30000)) Then

            Call EF_Ponto_Continuo(p, X, Y, EF_COR_TRACO)

        End If

'
'        If (x2 - X) <> 0 Then
'            declive = (y2 - Y) / (x2 - X)
'        Else
'            declive = 0
'        End If
'        c = Y - (declive * X)
'        For x3 = x2 To X Step 0.1
'            y3 = declive * x3 + c
'            If ((X < 30000) And (Y < 30000)) Then
'
'                Call EF_Ponto_Continuo(p, x3, y3, EF_COR_TRACO)
'
'            End If
'
'        Next
'
'        x2 = X
'        y2 = Y
        i = i + 2
    Wend
    
    

    DoEvents
    
    ' ----------------------------------

    On Error Resume Next
    Kill path_saida
    On Error GoTo ErrorHandler
 
    If Not IsMissing(path_saida) Then
        SavePicture p.Image, path_saida
    End If

    EF_Cria_Grafico_Electroferese = 1

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
'            Call ErrorToLog("EF_Cria_Grafico_Electroferese (Electroferese)", Err.Number, Err.Description, Err.Source, "Erro Inesperado                                    ")
            EF_Cria_Grafico_Electroferese = -1
            MsgBox Err.Number & " : " & Err.Description, vbCritical, " Erro "
            Exit Function
            Resume Next
    End Select
End Function


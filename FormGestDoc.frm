VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FormGestDoc 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormGestDoc"
   ClientHeight    =   6150
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   11355
   Icon            =   "FormGestDoc.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6150
   ScaleWidth      =   11355
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameNovaPasta 
      Height          =   1455
      Left            =   4320
      TabIndex        =   0
      Top             =   1800
      Width           =   3015
      Begin VB.CommandButton BtNovaPastaVoltar 
         Height          =   375
         Left            =   2520
         Picture         =   "FormGestDoc.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   960
         Width           =   375
      End
      Begin VB.CommandButton BtNovaPastaOk 
         Height          =   375
         Left            =   2040
         Picture         =   "FormGestDoc.frx":0396
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   960
         Width           =   375
      End
      Begin VB.CheckBox CkPublica 
         Caption         =   "Pasta P�blica"
         Height          =   255
         Left            =   720
         TabIndex        =   3
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   720
         TabIndex        =   2
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "Nome"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Frame FrameGeral 
      Height          =   5775
      Left            =   120
      TabIndex        =   6
      Top             =   0
      Width           =   11055
      Begin VB.CommandButton BtApagar 
         Height          =   375
         Left            =   1080
         Picture         =   "FormGestDoc.frx":0720
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Apagar Pasta"
         Top             =   5280
         Width           =   375
      End
      Begin VB.CommandButton BtRenomear 
         Height          =   375
         Left            =   600
         Picture         =   "FormGestDoc.frx":0AAA
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Renomear Pasta"
         Top             =   5280
         Width           =   375
      End
      Begin VB.CommandButton BtNovo 
         Height          =   375
         Left            =   120
         Picture         =   "FormGestDoc.frx":0E34
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Criar Pasta"
         Top             =   5280
         Width           =   375
      End
      Begin MSComctlLib.TreeView TreeViewPastas 
         Height          =   5055
         Left            =   240
         TabIndex        =   7
         Top             =   240
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   8916
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   530
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         Appearance      =   0
      End
   End
   Begin MSComctlLib.ImageList IL 
      Left            =   240
      Top             =   6360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestDoc.frx":11BE
            Key             =   "pasta_priv"
            Object.Tag             =   "Pasta Privada"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestDoc.frx":1558
            Key             =   "pasta_ger"
            Object.Tag             =   "pasta_ger"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestDoc.frx":18F2
            Key             =   "pasta_pub"
            Object.Tag             =   "Pasta P�blica"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormGestDoc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

' ---------------------------------------
' ESTRUTURA COM AS PASTAS
' ---------------------------------------
Private Type pasta
    SEQ As Long
    seq_pai As Long
    Nome As String
    publico As Integer
    user_cri As String
    dt_cri As String
    hr_cri As String
End Type
Dim totalEstrutPasta As Long
Dim estrutPasta() As pasta

Private Sub BtApagar_Click()
    RemovePasta TreeViewPastas.SelectedItem.index
End Sub

Private Sub BtNovaPastaOk_Click()
    If EcNome <> "" Then
        CriaPasta
    End If
    ControlaFrameNovaPasta False
End Sub

Private Sub BtNovaPastaVoltar_Click()
    ControlaFrameNovaPasta False
End Sub
Private Sub BtNovo_Click()
    ControlaFrameNovaPasta True
End Sub

Private Sub BtRenomear_Click()
    RenomeiaPasta
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()

    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Gest�o Documental"
    Me.left = 5
    Me.top = 5
    Me.Width = 10200 ' 14340
    Me.Height = 7185 ' Normal
    Set CampoDeFocus = BtNovo
    
    
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormGestDoc = Nothing
    
    Set gFormActivo = MDIFormInicio

End Sub

Sub LimpaCampos()
    Me.SetFocus
End Sub

Sub DefTipoCampos()
    ControlaFrameNovaPasta False
End Sub

Sub PreencheCampos()
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
End Function

Sub FuncaoProcurar()
End Sub

Sub FuncaoAnterior()
End Sub

Sub FuncaoSeguinte()
End Sub

Sub FuncaoInserir()
End Sub

Sub BD_Insert()
End Sub

Sub FuncaoModificar()
End Sub

Sub BD_Update()
End Sub

Sub FuncaoRemover()
End Sub

Sub BD_Delete()
End Sub

Sub PreencheValoresDefeito()
    TreeViewPastas.LineStyle = tvwRootLines
    TreeViewPastas.ImageList = IL
    
End Sub

' ----------------------------------------------------------------------------------

' REMOVE PASTA

' ----------------------------------------------------------------------------------
Private Sub RemovePasta(indiceP As Long)
    On Error GoTo TrataErro
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  RemovePasta " & Err.Description, Me.Name, "RemovePasta", True
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' CRIA PASTA

' ----------------------------------------------------------------------------------
Private Sub CriaPasta()
    Dim root As Node
    Dim no As Node
    Dim Pai As Integer
    Dim novoNo As Node
    Dim maximoCod As Integer
    Dim i As Integer
    On Error GoTo TrataErro

    Set no = TreeViewPastas.SelectedItem
    If (no Is Nothing) Then
        Pai = 0

    Else
        gMsgMsg = "Pretende criar dentro da pasta seleccionada ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            Pai = DevolveCodigoProfile(no.Key)
        Else
            Pai = 0
        End If

    End If
    totalEstrutPasta = totalEstrutPasta + 1
    maximoCod = totalEstrutPasta

    ReDim Preserve estrutPasta(totalEstrutPasta)
    estrutPasta(totalEstrutPasta).dt_cri = Bg_DaData_ADO
    estrutPasta(totalEstrutPasta).hr_cri = Bg_DaHora_ADO
    estrutPasta(totalEstrutPasta).Nome = BL_HandleNull(EcNome, "")
    estrutPasta(totalEstrutPasta).SEQ = maximoCod
    estrutPasta(totalEstrutPasta).seq_pai = Pai
    estrutPasta(totalEstrutPasta).user_cri = gCodUtilizador
    If CkPublica.Value = vbChecked Then
        If VerificaPastaPublica(totalEstrutPasta) = False Then
            estrutPasta(totalEstrutPasta).publico = 0
        Else
            estrutPasta(totalEstrutPasta).publico = 1
        End If
    Else
        estrutPasta(totalEstrutPasta).publico = 0
    End If
    If Pai > 0 Then
        If estrutPasta(totalEstrutPasta).publico = 1 Then
            Set root = TreeViewPastas.Nodes.Add(Pai, tvwChild, "KeyPR" & maximoCod, estrutPasta(totalEstrutPasta).Nome, "pasta_ger")
        Else
            Set root = TreeViewPastas.Nodes.Add(Pai, tvwChild, "KeyPR" & maximoCod, estrutPasta(totalEstrutPasta).Nome, "pasta_priv")
        End If
    Else
        If estrutPasta(totalEstrutPasta).publico = 1 Then
            Set root = TreeViewPastas.Nodes.Add(, tvwChild, "KeyPR" & maximoCod, estrutPasta(totalEstrutPasta).Nome, "pasta_ger")
        Else
            Set root = TreeViewPastas.Nodes.Add(, tvwChild, "KeyPR" & maximoCod, estrutPasta(totalEstrutPasta).Nome, "pasta_priv")
        End If
    End If
    root.Expanded = True

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  CriaPasta " & Err.Description, Me.Name, "CriaPasta", True
    Exit Sub
    Resume Next
End Sub



' ----------------------------------------------------------------------------------

' RENOMEIA PASTA

' ----------------------------------------------------------------------------------
Private Sub RenomeiaPasta()
    On Error GoTo TrataErro
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  RenomeiaPasta " & Err.Description, Me.Name, "RenomeiaPasta", True
    Exit Sub
    Resume Next
End Sub


' ----------------------------------------------------------------------------------

' Mostra ou esconde frame da nova pasta

' ----------------------------------------------------------------------------------

Private Sub ControlaFrameNovaPasta(res As Boolean)
    FrameGeral.Enabled = Not (res)
    FrameNovaPasta.Enabled = res
    FrameNovaPasta.Visible = res
    FrameNovaPasta.top = (Me.Height / 2) - (FrameNovaPasta.Height / 2)
    FrameNovaPasta.left = (Me.Width / 2) - (FrameNovaPasta.Width / 2)
    EcNome = ""
    CkPublica.Value = vbUnchecked
End Sub

' ----------------------------------------------------------------------------------

' DADO UM ID DEVOLVE CODIGO DO PROFILE

' ----------------------------------------------------------------------------------
Private Function DevolveCodigoProfile(ID As String) As Integer
    Dim i As Long
    On Error GoTo TrataErro
    
    For i = 1 To totalEstrutPasta
        If "KeyPR" & estrutPasta(i).SEQ = ID Then
            DevolveCodigoProfile = estrutPasta(i).SEQ
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro  DevolveCodigoProfile " & Err.Description, Me.Name, "DevolveCodigoProfile"
    Exit Function
    Resume Next
End Function



' ----------------------------------------------------------------------------------

' VERIFICA SE PASTA PODE OU N�O SER MARCADO COMO PUBLICO

' ----------------------------------------------------------------------------------
Private Function VerificaPastaPublica(indice As Long) As Boolean
    Dim i As Long
    On Error GoTo TrataErro
    If estrutPasta(indice).seq_pai <= 0 Then
        VerificaPastaPublica = True
        Exit Function
    End If
    For i = 1 To totalEstrutPasta
        If i <> indice Then
            If estrutPasta(indice).seq_pai = estrutPasta(i).SEQ Then
                If estrutPasta(i).publico = 0 Then
                    VerificaPastaPublica = False
                    BG_Mensagem mediMsgBox, "N�o � poss�vel criar pasta como p�blica."
                Else
                    VerificaPastaPublica = VerificaPastaPublica(i)
                End If
                Exit Function
            End If
        End If
    Next
Exit Function
TrataErro:
    VerificaPastaPublica = False
    BG_LogFile_Erros "Erro  VerificaPastaPublica " & Err.Description, Me.Name, "VerificaPastaPublica"
    Exit Function
    Resume Next
End Function





Attribute VB_Name = "Integracao_SONHO"
Option Explicit

Global gInsereAnaFact As Boolean
Global gCodAna_aux As String
Global gReq_aux As String
Global gCodPerfil_aux As String
Global gQuantidadeAFact As Integer
Global gRsFact As ADODB.recordset
Global gAnalise_Ja_Facturada As Boolean

Public Function SONHO_Constroi_Requisicao(FRM As Form, _
                                          proc1 As String, _
                                          situacao As String, _
                                          episodio As String, _
                                          data_analises As String) As Integer
    
    On Error GoTo ErrorHandler
    
    Dim HisAberto As Integer
    Dim rs As ADODB.recordset
    Dim rv As Integer
    Dim sql As String
    Dim seq_utente As String
    Dim proc1_aux As String
    Dim sit_aux As String
    Dim i As Long
    Dim an_aux As String
    Dim sit_aux2 As String
    Dim NReq As String
    
    Dim UT_cod_efr As String
    
    BL_InicioProcessamento FRM, "Pesquisar Registo do " & HIS.nome & "."
 
    proc1 = UCase(Trim(proc1))
    episodio = UCase(Trim(episodio))
    situacao = UCase(Trim(situacao))
    
    If ((Len(episodio) = 0) Or _
        (Len(situacao) = 0)) Then
        Set rs = Nothing
        Set gConnHIS = Nothing
        BL_FimProcessamento FRM
        MsgBox " Indique a Situa��o e o Epis�dio.        ", vbInformation, " Erro"
        SONHO_Constroi_Requisicao = -2
        Exit Function
    End If
       
    NReq = ""
    sit_aux2 = situacao
    
    ' Converte a situa��o para o c�digo do SONHO.
    
    Select Case situacao
        
        Case gT_Urgencia
            situacao = "URG"
        Case gT_Consulta
            situacao = "CON"
        Case gT_Internamento
            situacao = "INT"
        Case gT_Externo
            situacao = "HDI"
        Case gT_LAB
            situacao = "LAB"
        Case gT_Bloco
            situacao = "BLO"
        Case Else
            situacao = ""
            
    End Select
    
    HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
    
    If (HisAberto = 1) Then
    
        ' -----------------------------------------------------------------
        ' Vai buscar informa��o acerca do Utente
        ' -----------------------------------------------------------------
    
        If (UCase(HIS.nome) = UCase("SONHO")) Then
            
            rv = SONHO_PesquisaUtente(proc1, episodio, situacao)
                            
            If (rv = -1) Then
                Set rs = Nothing
                Set gConnHIS = Nothing
                BG_Mensagem mediMsgBox, " Registo inexistente.      ", vbInformation, " Importa��o do " & HIS.nome
                BL_FimProcessamento FRM
                SONHO_Constroi_Requisicao = -4
                Exit Function
            End If
                            
            sql = "SELECT " & vbCrLf & _
                  "      * " & vbCrLf & _
                  "FROM " & vbCrLf & _
                  "      sl_identif " & vbCrLf & _
                  "WHERE " & vbCrLf & _
                  "      cod_apl = " & gNumeroSessao
            
        End If
                                        
        Set rs = New ADODB.recordset
        
        With rs
            .Source = sql
            .CursorType = adOpenForwardOnly
            .CursorLocation = adUseServer
            .LockType = adLockReadOnly
            .Open , gConnHIS
        End With
                        
        If (rs.RecordCount <= 0) Then
            Set rs = Nothing
            Set gConnHIS = Nothing
            BL_FimProcessamento FRM
            MsgBox " Erro 6: Importa��o de Utente do " & HIS.nome & ". ", vbCritical, "ERRO"
            SONHO_Constroi_Requisicao = -6
            Exit Function
        Else
                
            ' Verifica se o Utente j� existe no SISLAB.
            
            proc1_aux = Trim(BL_HandleNull(rs!n_proc_1, ""))
            
            If (proc1_aux <> proc1) Then
                proc1 = proc1_aux
            End If
            
            Select Case situacao
                Case "URG"
                    sit_aux = gT_Urgencia
                Case "CON"
                    sit_aux = gT_Consulta
                Case "INT"
                    sit_aux = gT_Internamento
                Case "HDI"
                    sit_aux = gT_Externo
                Case "LAB"
                    sit_aux = gT_LAB
                Case "BLO"
                    sit_aux = gT_Bloco
                Case Else
                    sit_aux = ""
            End Select
            
            UT_cod_efr = BL_HandleNull(rs!cod_efr_ute, "")
            
            If Not (Utente_Existe(proc1, sit_aux, episodio, seq_utente)) Then
                ' Inserir.
                

                rv = UTENTE_Inserir(BL_HandleNull(rs!Utente, ""), _
                                    BL_HandleNull(rs!t_utente, ""), _
                                    BL_HandleNull(rs!n_proc_1, ""), _
                                    BL_HandleNull(rs!n_proc_2, ""), _
                                    BL_HandleNull(rs!n_cartao_ute, ""), _
                                    BL_HandleNull(rs!dt_inscr, ""), _
                                    BL_HandleNull(rs!nome_ute, ""), _
                                    BL_HandleNull(rs!dt_nasc_ute, ""), _
                                    BL_HandleNull(rs!sexo_ute, ""), _
                                    BL_HandleNull(rs!est_civ_ute, ""), _
                                    BL_HandleNull(rs!descr_mor_ute, ""), _
                                    BL_HandleNull(rs!cod_postal_ute, ""), _
                                    BL_HandleNull(rs!telef_ute, ""), _
                                    BL_HandleNull(rs!cod_profis_ute, ""), _
                                    UT_cod_efr, _
                                    BL_HandleNull(rs!n_benef_ute, ""), _
                                    BL_HandleNull(rs!cod_isencao_ute, ""), _
                                    BL_HandleNull(rs!obs, ""), _
                                    Trim(CStr(gCodUtilizador)), _
                                    seq_utente)
                
                
                If (rv <> 1) Then
        
                    If UCase(HIS.nome) = UCase("SONHO") Then
                        ' Apaga o registo da tabela tempor�ria do SONHO.
                        sql = "DELETE FROM sl_identif " & vbCrLf & _
                              "WHERE cod_apl = " & gNumeroSessao
                        With rs
                            .Source = sql
                            .CursorType = adOpenForwardOnly
                            .CursorLocation = adUseServer
                            .Open , gConnHIS
                        End With
                    End If
                    
                    ' Erro ao inserir o utente.
                    Set rs = Nothing
                    Set gConnHIS = Nothing
                    BL_FimProcessamento FRM
                    MsgBox " Erro 5: Importa��o de Utente do " & HIS.nome & ". ", vbCritical, "ERRO"
                    SONHO_Constroi_Requisicao = -5
                    Exit Function
                End If
            Else
                ' Existe. N�o faz update.
            End If
            
            rs.Close
            Set rs = Nothing
        
        End If
                        
        If UCase(HIS.nome) = UCase("SONHO") Then
                             
            ' Apaga o registo da tabela tempor�ria do SONHO.
            
            sql = "DELETE FROM sl_identif " & vbCrLf & _
                  "WHERE cod_apl = " & gNumeroSessao
                            
            
            Set rs = New ADODB.recordset
            
            With rs
                .Source = sql
                .CursorType = adOpenForwardOnly
                .CursorLocation = adUseServer
                .Open , gConnHIS
            End With
                             
            Set rs = Nothing
        
        End If
               
        ' -----------------------------------------------------------------
        ' Criar requisi��o.
        ' -----------------------------------------------------------------
               
        ' Vai buscar informa��o no SISLAB acerca do Utente.
        
        sql = "SELECT " & vbCrLf & _
              "     t_utente, " & vbCrLf & _
              "     utente, " & vbCrLf & _
              "     cod_efr_ute " & vbCrLf & _
              "FROM " & vbCrLf & _
              "     sl_identif " & vbCrLf & _
              "WHERE " & vbCrLf & _
              "     seq_utente = " & seq_utente
                                        
        Set rs = New ADODB.recordset
        
        With rs
            .Source = sql
            .CursorType = adOpenForwardOnly
            .CursorLocation = adUseServer
            .LockType = adLockReadOnly
            .Open , gConexao
        End With
                        
        If (rs.RecordCount < 1) Then
            ' Erro.
            Set rs = Nothing
            Set gConnHIS = Nothing
            BL_FimProcessamento FRM
            MsgBox " Erro 6: Importa��o de Utente do " & HIS.nome & ". ", vbCritical, "ERRO"
            SONHO_Constroi_Requisicao = -6
            Exit Function
        Else
            
            On Error Resume Next
            Err.Clear
            i = 0
            While (FormGestaoRequisicao.CbTipoUtente.List(i) <> UCase(Trim(BL_HandleNull(rs!t_utente, "")))) And _
                  (Err.Number = 0)
            
                i = i + 1
            Wend
            If (Err.Number = 0) Then
                FormGestaoRequisicao.CbTipoUtente.ListIndex = i
            Else
                FormGestaoRequisicao.CbTipoUtente.ListIndex = -1
            End If
            
            On Error GoTo ErrorHandler
            
            FormGestaoRequisicao.EcUtente.Text = BL_HandleNull(rs!Utente, "")
            FormGestaoRequisicao.EcUtente_Validate (True)
            
            rs.Close
            
        End If
        
        Set rs = Nothing
        
        ' Coloca a data prevista por defeito - Hoje.
        FormGestaoRequisicao.EcDataPrevista_GotFocus
        FormGestaoRequisicao.EcDataPrevista_Validate (True)
        
        ' Coloca a data de chegada a nulo.
        FormGestaoRequisicao.EcDataChegada.Text = ""
        
        ' Coloca a urg�ncia � URGENTE por defeito.
        FormGestaoRequisicao.CbUrgencia.ListIndex = 1
        
        ' Coloca a proveni�ncia a desconhecida por defeito.
        Dim cod_proven As String
        Dim descr_proven As String
        cod_proven = ""
        descr_proven = ""
        rv = SONHO_Get_Proveniencia(episodio, situacao, cod_proven, descr_proven)
        If (Len(cod_proven) > 0) Then
            FormGestaoRequisicao.EcCodProveniencia.Text = cod_proven
            FormGestaoRequisicao.EcCodProveniencia_Validate (True)
        Else
            FormGestaoRequisicao.EcCodProveniencia.Text = "1"
            FormGestaoRequisicao.EcCodProveniencia_Validate (True)
        End If
        
        ' 0 coloca a situa��o.
        On Error Resume Next
        FormGestaoRequisicao.CbSituacao.ListIndex = CInt(Trim(sit_aux2))
        On Error GoTo ErrorHandler
        
        ' Coloca o epis�dio.
        FormGestaoRequisicao.EcEpisodio.Text = episodio
        
        ' Coloca a EFR desconhecida por defeito.
        FormGestaoRequisicao.EcCodEfr.Text = "1"
        FormGestaoRequisicao.EcCodEFR_Validate (True)
        
        ' Grava a requisi��o.
        Call FormGestaoRequisicao.FuncaoInserir
        
        NReq = Trim(FormGestaoRequisicao.EcNumReq.Text)
        
        ' 0 coloca a situa��o (bug).
        On Error Resume Next
        FormGestaoRequisicao.CbSituacao.ListIndex = CInt(Trim(sit_aux2))
        On Error GoTo ErrorHandler
        
        ' -----------------------------------------------------------------
        ' Inserir as an�lises.
        ' -----------------------------------------------------------------
        
        sql = "SELECT DISTINCT " & vbCrLf & _
              "     cod_analise, " & vbCrLf & _
              "     dta_analise, " & vbCrLf & _
              "     num_analise, " & vbCrLf & _
              "     num_sequencial, " & vbCrLf & _
              "     cod_modulo, " & vbCrLf & _
              "     num_episodio " & vbCrLf & _
              "FROM " & vbCrLf & _
              "     analises_realizadas " & vbCrLf & _
              "WHERE " & vbCrLf & _
              "     num_episodio = '" & episodio & "' AND " & vbCrLf & _
              "     cod_modulo   = '" & situacao & "' AND " & vbCrLf & _
              "     dta_analise = '" & data_analises & "' " & vbCrLf & _
              "ORDER BY " & vbCrLf & _
              "     num_analise DESC "

        Set rs = New ADODB.recordset
        
        With rs
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenForwardOnly
            .LockType = adLockReadOnly
            .Open , gConnHIS
        End With
        
        While Not rs.EOF
            
            an_aux = ""
            
            If (Not IsNull(rs!cod_analise)) Then
                
                ' Verifica se j� foi registada.

                If Not (SONHO_Realizada(BL_HandleNull(rs!num_analise, ""), _
                                        BL_HandleNull(rs!num_sequencial, ""), _
                                        BL_HandleNull(rs!cod_modulo, ""), _
                                        BL_HandleNull(rs!num_episodio, ""), _
                                        BL_HandleNull(rs!dta_analise, ""))) Then
                
                    rv = SONHO_Mapeia_Analise_para_SISLAB(Right(rs!cod_analise, Len(rs!cod_analise) - 1), an_aux)
                
                    If (rv = -2) Then
                        ' N�o mapeou.
                        MsgBox "A an�lise " & an_aux & " n�o tem correspond�ncia no SISLAB.   ", vbExclamation, "Inporta��o de An�lises do SONHO"
                        an_aux = ""
                    End If
            
                Else
                    an_aux = ""
                End If
            
            Else
                an_aux = ""
            End If
            
            If (Len(an_aux) > 0) Then
'                FormGestaoRequisicao.FGAna.TextMatrix(FormGestaoRequisicao.FGAna.Rows - 1, 0) = an_aux
                FormGestaoRequisicao.SSTGestReq.Tab = 2
                
                DoEvents
                DoEvents
                DoEvents
                
                Call FormGestaoRequisicao.FgAna_KeyDown(13, 0)
                
                DoEvents
                DoEvents
                DoEvents
                
                FormGestaoRequisicao.EcAuxAna.Text = an_aux
                
                DoEvents
                DoEvents
                DoEvents
                
                Call FormGestaoRequisicao.EcAuxAna_KeyDown(13, 0)
                
                DoEvents
                DoEvents
                DoEvents
            
            End If
            
            rs.MoveNext
        Wend
        rs.Close
        
        ' Insere as an�lises.
        FormGestaoRequisicao.FuncaoModificar
        
        ' 0 coloca a situa��o (bug).
        On Error Resume Next
        FormGestaoRequisicao.CbSituacao.ListIndex = CInt(Trim(sit_aux2))
        On Error GoTo ErrorHandler
        
        ' Fecha a conexao
        gConnHIS.Close
        Set gConnHIS = Nothing
        
        ' -----------------------------------------------------------------
    
    Else
        Set rs = Nothing
        Set gConnHIS = Nothing
        BL_FimProcessamento FRM
        MsgBox " Erro : N�o abre liga��o ao " & HIS.nome & ".   ", vbCritical, "ERRO"
        SONHO_Constroi_Requisicao = -3
        Exit Function
    End If
    
    Set rs = Nothing
    BL_FimProcessamento FRM
    
    ' ---------------- teste
    
    ' Teste
    If (gConexao Is Nothing) Then
        
        Call BG_LogFile_Erros("SONHO_Constroi_Requisicao (Integracao_SONHO) : Conex�o quebrada, Reinicializa.")
    
        rv = BG_Abre_ConexaoBD_ADO
        If (rv = 0) Then
            MDIFormInicio.Show
            AplicacaoVazia True
            Form_Ini.Show
            Exit Function
        End If
    
    End If
    
    SONHO_Constroi_Requisicao = 1
    
    ' ----------------
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            On Error Resume Next
            Set rs = Nothing
            Set gConnHIS = Nothing
            BL_FimProcessamento FRM
            Call BG_LogFile_Erros("Erro Inesperado : SONHO_Constroi_Requisicao (Integracao_SONHO) -> " & Err.Number & ", " & Err.Description)
            SONHO_Constroi_Requisicao = -1
            Exit Function
    End Select
End Function

Function SONHO_PesquisaUtente(NumSequencial As String, Optional NumProcesso As String, _
                              Optional NumEpisodio As String, _
                              Optional situacao As String) As Integer

    Dim CmdGera As New ADODB.Command
        
    Dim PmtNumSeq As ADODB.Parameter
    Dim PmtNumProcesso As ADODB.Parameter
    Dim PmtNumUtente As ADODB.Parameter
    Dim PmtNumEpisodio As ADODB.Parameter
    Dim PmtCodModulo As ADODB.Parameter
    Dim PmtDtNasc As ADODB.Parameter
    Dim PmtNome As ADODB.Parameter
    Dim PmtSexo As ADODB.Parameter
    Dim PmtIdadeInf As ADODB.Parameter
    Dim PmtIdadeSup As ADODB.Parameter
    Dim PmtCodApl As ADODB.Parameter
    Dim PmtNumReg As ADODB.Parameter
    
    Dim ret As Integer
        
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdGera
        .ActiveConnection = gConnHIS
        .CommandText = "PESQUISA_UTENTE"
        .CommandType = adCmdStoredProc
    End With
    
    If gUtilizaNumSeqSONHO = 1 Then
        Set PmtNumSeq = CmdGera.CreateParameter("NUM_SEQUENCIAL", adDecimal, adParamInput)     'teste so
    End If
    
    Set PmtNumProcesso = CmdGera.CreateParameter("NUM_PROCESSO", adDecimal, adParamInput)
 
    ' -------
'    Set PmtNumUtente = CmdGera.CreateParameter("NUM_UTENTE", adDecimal, adParamInput)
    ' -------
    
    Set PmtNumEpisodio = CmdGera.CreateParameter("NUM_EPISODIO", adDecimal, adParamInput)
    Set PmtCodModulo = CmdGera.CreateParameter("COD_MODULO", adVarChar, adParamInput, 10)
 '   Set PmtDtNasc = CmdGera.CreateParameter("DATA_NASC", adVarChar, adParamInput, 10)
 '   Set PmtNome = CmdGera.CreateParameter("NOME", adVarChar, adParamInput, 120)
 '   Set PmtSexo = CmdGera.CreateParameter("SEXO", adVarChar, adParamInput, 10)
 '   Set PmtIdadeInf = CmdGera.CreateParameter("IDADE_INF", adDecimal, adParamInput)
 '   Set PmtIdadeSup = CmdGera.CreateParameter("IDADE_SUP", adDecimal, adParamInput)
    Set PmtCodApl = CmdGera.CreateParameter("COD_APL", adDecimal, adParamInput)
    Set PmtNumReg = CmdGera.CreateParameter("NUM_REG", adDecimal, adParamInputOutput)
    
    If gUtilizaNumSeqSONHO = 1 Then
        CmdGera.Parameters.Append PmtNumSeq        'teste so
    End If
    CmdGera.Parameters.Append PmtNumProcesso
 
    ' -------
'    CmdGera.Parameters.Append PmtNumUtente
    ' -------
    
    CmdGera.Parameters.Append PmtNumEpisodio
    CmdGera.Parameters.Append PmtCodModulo
 '   CmdGera.Parameters.Append PmtDtNasc
 '   CmdGera.Parameters.Append PmtNome
 '   CmdGera.Parameters.Append PmtSexo
 '   CmdGera.Parameters.Append PmtIdadeInf
 '   CmdGera.Parameters.Append PmtIdadeSup
    CmdGera.Parameters.Append PmtCodApl
    CmdGera.Parameters.Append PmtNumReg
    
'    If gLAB = "CHVNG" Then
'        If NumProcesso = "" Then
'            CmdGera.Parameters.Item("NUM_SEQUENCIAL").Value = Null
'        Else
'            CmdGera.Parameters.Item("NUM_SEQUENCIAL").Value = CLng(NumProcesso)
'        End If
'    Else
       If NumSequencial = "" And NumProcesso = "" Then
            If gUtilizaNumSeqSONHO = 1 Then
               CmdGera.Parameters.item("NUM_SEQUENCIAL").value = Null 'teste so
            End If
           CmdGera.Parameters.item("NUM_PROCESSO").value = Null
        ElseIf NumSequencial <> "" And NumProcesso = "" Then
            If gUtilizaNumSeqSONHO = 1 Then
               CmdGera.Parameters.item("NUM_SEQUENCIAL").value = NumSequencial 'teste so
               CmdGera.Parameters.item("NUM_PROCESSO").value = Null
            Else
               CmdGera.Parameters.item("NUM_PROCESSO").value = CLng(NumSequencial)
            End If
        ElseIf NumSequencial = "" And NumProcesso <> "" Then
            If gUtilizaNumSeqSONHO = 1 Then
               CmdGera.Parameters.item("NUM_SEQUENCIAL").value = Null 'teste so
            End If
           CmdGera.Parameters.item("NUM_PROCESSO").value = NumProcesso
       Else
            If gUtilizaNumSeqSONHO = 1 Then
                CmdGera.Parameters.item("NUM_SEQUENCIAL").value = CLng(NumSequencial) 'teste so
            End If
           CmdGera.Parameters.item("NUM_PROCESSO").value = CLng(NumSequencial)
       End If
'    End If
 
' -------------------
'    CmdGera.Parameters.Item("NUM_UTENTE").Value = Null
' -------------------
    
    If Trim(NumEpisodio) = "" Then
        CmdGera.Parameters.item("NUM_EPISODIO").value = Null
    Else
       CmdGera.Parameters.item("NUM_EPISODIO").value = CLng(NumEpisodio)
    End If
    If Trim(situacao) = "" Then
        CmdGera.Parameters.item("COD_MODULO").value = Null
    Else
        CmdGera.Parameters.item("COD_MODULO").value = situacao
    End If
 '   CmdGera.Parameters.Item("DATA_NASC").Value = Null
 '   CmdGera.Parameters.Item("NOME").Value = Null
 '   CmdGera.Parameters.Item("SEXO").Value = Null
 '   CmdGera.Parameters.Item("IDADE_INF").Value = Null
 '   CmdGera.Parameters.Item("IDADE_SUP").Value = Null
    CmdGera.Parameters.item("COD_APL").value = gNumeroSessao
    CmdGera.Parameters.item("NUM_REG").value = -1
    
    CmdGera.Execute

    SONHO_PesquisaUtente = Trim(CmdGera.Parameters.item("NUM_REG").value)
    
    Set CmdGera = Nothing
    
    Exit Function

TrataErro:
    SONHO_PesquisaUtente = -1
    'BG_TrataErro "SONHO_PesquisaUtente", Err.Number
        
    Set CmdGera = Nothing
    
End Function

Function SONHO_RemoveUtente() As Integer

    Dim CmdGera As New ADODB.Command
    Dim PmtCodApl As ADODB.Parameter
    Dim PmtNumReg As ADODB.Parameter
    Dim ret As Integer
        
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdGera
        .ActiveConnection = gConnHIS
        .CommandText = "APAGA_UTENTE"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtCodApl = CmdGera.CreateParameter("COD_APL", adDecimal, adParamInput)
    Set PmtNumReg = CmdGera.CreateParameter("NUM_REG", adDecimal, adParamInputOutput)
    
    CmdGera.Parameters.Append PmtCodApl
    CmdGera.Parameters.Append PmtNumReg
    
    CmdGera.Parameters.item("COD_APL").value = gNumeroSessao
    CmdGera.Parameters.item("NUM_REG").value = -1
    
    CmdGera.Execute
    
    SONHO_RemoveUtente = Trim(CmdGera.Parameters.item("NUM_REG").value)
    
    Set CmdGera = Nothing

    Exit Function

TrataErro:
    SONHO_RemoveUtente = -1
    'BG_TrataErro "SONHO_RemoveUtente", Err.Number
        
    Set CmdGera = Nothing
    
End Function

Function SONHO_Realizada(num_analise As String, _
                         num_sequencial As String, _
                         cod_modulo As String, _
                         num_episodio As String, _
                         dta_analise As String) As Boolean
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim rv As Boolean

    num_analise = Trim(num_analise)
    num_sequencial = Trim(num_sequencial)
    cod_modulo = Trim(cod_modulo)
    num_episodio = Trim(num_episodio)
    dta_analise = Trim(dta_analise)
    
    If (Len(dta_analise) = 0) Then
        dta_analise = "SYSDATE"
    Else
        dta_analise = "'" & Format(dta_analise, gFormatoData) & "'"
    End If
    
    rv = True
    
    sql = "INSERT INTO sl_realizadas_sonho " & vbCrLf & _
          "( " & vbCrLf & _
          "     num_analise, " & vbCrLf & _
          "     num_sequencial, " & vbCrLf & _
          "     cod_modulo, " & vbCrLf & _
          "     num_episodio, " & vbCrLf & _
          "     dta_analise " & vbCrLf & _
          ") " & vbCrLf & _
          "VALUES " & vbCrLf & _
          "( " & vbCrLf & _
          "     " & BL_TrataStringParaBD(num_analise) & ", " & vbCrLf & _
          "     " & BL_TrataStringParaBD(num_sequencial) & ", " & vbCrLf & _
          "     " & BL_TrataStringParaBD(cod_modulo) & ", " & vbCrLf & _
          "     " & BL_TrataStringParaBD(num_episodio) & ", " & vbCrLf & _
          "     " & dta_analise & " " & vbCrLf & _
          ") "

        Set rs = New ADODB.recordset
        
        Err.Clear
        On Error Resume Next
        With rs
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenForwardOnly
            .LockType = adLockReadOnly
            .Open , gConexao
        End With
        
        Select Case Err.Number
            Case 0
                ' OK
                rv = False
            Case -2147217900
                ' Unique Constraint
                rv = True
            Case Else
                GoTo ErrorHandler
        End Select
        Err.Clear
        
        On Error GoTo ErrorHandler

        Set rs = Nothing

        SONHO_Realizada = rv

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Set rs = Nothing
            Call BG_LogFile_Erros("Erro Inesperado : SONHO_Realizada (Integracao_SONHO) -> " & Err.Number & ", " & Err.Description)
            SONHO_Realizada = True
            Exit Function
    End Select
End Function

Public Function SONHO_Nova_ANALISES_IN(ByVal situacao As String, _
                                       ByVal num_episodio As String, _
                                       ByVal num_processo As String, _
                                       ByVal cod_especialidade As String, _
                                       ByVal cod_analise As String, _
                                       ByVal dta_episodio As String, _
                                       ByVal n_req As Long, _
                                       Optional ByVal cod_ana_sislab As String, _
                                       Optional ByVal Cod_Perfil As String, _
                                       Optional ByVal cod_ana_c As String, _
                                       Optional ByVal cod_ana_s As String, _
                                       Optional ByVal cod_agrup As String, _
                                       Optional ByVal cod_micro As String, _
                                       Optional ByVal Cod_Gr_Antibio As String, _
                                       Optional ByVal cod_antibio As String, _
                                       Optional ByVal Cod_Prova As String, _
                                       Optional ByVal centro_custo As String, _
                                       Optional ByVal CodLocalFact As Long, _
                                       Optional ByVal numMarcadores As Integer) As Integer

    Dim cod_modulo As String
    Dim retorno As String
    Dim sql As String
    Dim rsHIS As ADODB.recordset
    Dim RsTestaExDirecto As ADODB.recordset
    Dim Cod_Analise_Aux As String
    
    Dim dta_analise As String
    Dim Insere_record As Boolean
    Dim HisAberto As Integer
    
    
    On Error GoTo TrataErro
    
    If gHIS_Import = 1 Then
        HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
    Else
        HisAberto = 0
    End If
    
    num_episodio = Trim(num_episodio)
    
    ' S� envia com epis�dio preenchido.
    If (Len(num_episodio) = 0) Then
        SONHO_Nova_ANALISES_IN = -2
        Exit Function
    End If
    
    cod_analise = Trim(cod_analise)
    situacao = Trim(situacao)
    num_processo = Trim(num_processo)
    cod_especialidade = Trim(cod_especialidade)
    dta_episodio = Trim(dta_episodio)
    
    If (Trim(dta_episodio) = "") Or dta_episodio = "NULL" Then
        dta_analise = Format(Date, gFormatoData)
    Else
        dta_analise = Format(dta_episodio, gFormatoData)
    End If

    ' Por defeito a quantidade de an�lises pedidas � 1.

    ' retorno indica o que o SONHO deve fazer com o registo.
    ' I : Inserir no SONHO como Realizada.
    ' P : Inserir no SONHO como Pedida.
    ' R : O SONHO j� conhece. Indica que foi realizada.
    ' A : Indica que n�o foi realizada e que o SONHO a deve apagar.
    retorno = "I"
    
    If (Len(cod_analise) = 0) Or _
       (Len(dta_episodio) = 0) Then
        ' Par�metros incompletos.
        SONHO_Nova_ANALISES_IN = -2
        Exit Function
    End If
    
    ' Mapeia a situa��o do SISLAB para o SONHO.
    Select Case situacao
        Case gT_Urgencia
            cod_modulo = "URG"
        Case gT_Consulta
            'If gLAB = "HPOVOA" And cod_especialidade <> "20007" Then Retorno = "P"
            If cod_analise = gCodAnaSONHO Then retorno = "A"
            cod_modulo = "CON"
        Case gT_Internamento
            cod_modulo = "INT"
        Case gT_Externo
            cod_modulo = "HDI"
        Case gT_LAB
            cod_modulo = "LAB"
        Case gT_Bloco
            cod_modulo = "BLO"
        Case Else
            cod_modulo = ""
    End Select
    
    If gLAB = "HSMARTA" And num_episodio = "24000594" Then
        cod_modulo = "LAB"
    End If
    
    ' Insere a an�lise do SISLAB no SONHO
    
    If (Len(cod_modulo) > 0) And _
       (Len(num_episodio) > 0) Then
       
       ' Caso 1 : situa��o (modulo) + num_episodio
    
        
        If Not (IsNumeric(num_episodio)) Then
            ' N� de epis�dio n�o � um n�mero.
            SONHO_Nova_ANALISES_IN = -3
            Exit Function
        End If
        
        ' Caso 1 : Situa��o (m�dulo) + epis�dio
    
        cod_modulo = "'" & cod_modulo & "'"
        num_episodio = num_episodio
        num_processo = "NULL"
        cod_especialidade = "NULL"
        dta_episodio = "NULL"
    Else
    
        ' Caso 2 : Situa��o (m�dulo) + processo + data de epis�dio + cod_especialidade.
        
        cod_modulo = "'" & cod_modulo & "'"
        num_processo = num_processo
        dta_episodio = "'" & Format(Trim(dta_episodio), gFormatoData) & "'"
        cod_especialidade = cod_especialidade
        num_episodio = "NULL"
    
    End If
    
    
  
    Insere_record = True
    
    
    If Insere_record = True Then
            sql = "INSERT INTO analises_in " & vbCrLf & _
                  "( " & vbCrLf & _
                  "     cod_modulo, " & vbCrLf & _
                  "     num_episodio, " & vbCrLf & _
                  "     num_processo, " & vbCrLf & _
                  "     dta_episodio, " & vbCrLf & _
                  "     dta_analise, " & vbCrLf & _
                  "     cod_especialidade, " & vbCrLf & _
                  "     cod_analise, " & vbCrLf & _
                  "     quantidade, " & vbCrLf & _
                  "     retorno, " & vbCrLf & _
                  "     centro_custo " & vbCrLf
            If gEnviaLocalSONHO = mediSim Then
                sql = sql & ", cod_aparelho "
            End If
            sql = sql & ") " & vbCrLf & _
                  "VALUES " & vbCrLf & _
                  "( " & vbCrLf & _
                  "     " & cod_modulo & ", " & vbCrLf & _
                  "     " & num_episodio & ", " & vbCrLf & _
                  "     " & num_processo & ", " & vbCrLf & _
                  "     " & dta_episodio & ", " & vbCrLf & _
                  "     '" & dta_analise & "', " & vbCrLf & _
                  "     " & cod_especialidade & ", " & vbCrLf & _
                  "     '" & cod_analise & "', " & vbCrLf & _
                  "     1, " & vbCrLf & _
                  "     '" & retorno & "', " & vbCrLf & _
                  "     " & BL_TrataStringParaBD(centro_custo)
            If gEnviaLocalSONHO = mediSim Then
                sql = sql & ",  " & CodLocalFact
            End If
            sql = sql & ")"
              
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            gConnHIS.Execute sql
            
            BG_Trata_BDErro
            
            
            'soliveira 25-07-2003
            '-------------------------------------------------------------
            sql = "INSERT INTO sl_analises_in " & vbCrLf & _
                  "( " & vbCrLf & _
                  "     cod_modulo, " & vbCrLf & _
                  "     num_episodio, " & vbCrLf & _
                  "     num_processo, " & vbCrLf & _
                  "     dta_episodio, " & vbCrLf & _
                  "     dta_analise, " & vbCrLf & _
                  "     cod_especialidade, " & vbCrLf & _
                  "     cod_analise, " & vbCrLf & _
                  "     quantidade, " & vbCrLf & _
                  "     retorno,cod_ana_sislab, " & vbCrLf & _
                  "     n_req, cod_perfil, cod_ana_c, cod_ana_s, " & _
                  "     cod_agrup, cod_micro, cod_gr_antibio,cod_antibio, cod_prova, centro_custo, DT_CRI " & vbCrLf
                If gEnviaLocalSONHO = mediSim Then
                    sql = sql & ", cod_aparelho "
                End If
                sql = sql & ") " & vbCrLf & _
                  "VALUES " & vbCrLf & _
                  "( " & vbCrLf & _
                  "     " & cod_modulo & ", " & vbCrLf & _
                  "     " & num_episodio & ", " & vbCrLf & _
                  "     " & num_processo & ", " & vbCrLf & _
                  "     " & dta_episodio & ", " & vbCrLf & _
                  "     '" & dta_analise & "', " & vbCrLf & _
                  "     " & cod_especialidade & ", " & vbCrLf & _
                  "     '" & cod_analise & "', " & vbCrLf & _
                  "     1, " & vbCrLf & _
                  "     '" & retorno & "','" & cod_ana_sislab & "', " & vbCrLf & _
                  "     " & n_req & ",'" & Cod_Perfil & "', '" & cod_ana_c & "','" & _
                  cod_ana_s & "','" & cod_agrup & "','" & cod_micro & "','" & Cod_Gr_Antibio & _
                  "','" & cod_antibio & "','" & Cod_Prova & "'," & BL_TrataStringParaBD(centro_custo)
                sql = sql & ", sysdate "
                If gEnviaLocalSONHO = mediSim Then
                    sql = sql & ",  " & CodLocalFact
                End If
                sql = sql & ")"


            BG_ExecutaQuery_ADO sql
            BG_Trata_BDErro
    End If
    '-------------------------------------------------------------
        
    ' ------------------------------
    
    'MsgBox Sql, vbInformation, "DEBUG"
        
    ' ------------------------------
        
'    Set rsHIS = New ADODB.Recordset
'
''    rsHIS.Source = sql
''    rsHIS.CursorType = adOpenForwardOnly
''    rsHIS.CursorLocation = adUseServer
''    rsHIS.Open , gConnHIS
'
'    Set rsHIS = Nothing
    
    SONHO_Nova_ANALISES_IN = 1
    Exit Function
    
TrataErro:
    SONHO_Nova_ANALISES_IN = -1
    BG_LogFile_Erros "Erro na SONHO_Nova_ANALISES_IN " & Err.Description
    
End Function

Public Function SONHO_Get_Processo(situacao As Integer, _
                                   episodio As String, _
                                   ByRef processo As String, _
                                   ByRef nome As String, _
                                   ByRef dt_nasc As String)
    
    Dim rsHIS As ADODB.recordset
    Dim HisAberto  As Integer
    Dim sSql As String
    Dim RsCodEFR As ADODB.recordset
    Dim RetPesquisaSONHO As Integer
    Dim modulo As String
    
    episodio = Trim(episodio)
    processo = Trim(processo)
    nome = ""
    dt_nasc = ""
    
    HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
                    
    If (HisAberto = 1) Then
    
        Select Case situacao
            Case gT_Urgencia
                modulo = "URG"
            Case gT_Consulta
                modulo = "CON"
            Case gT_Internamento
                modulo = "INT"
            Case gT_Externo
                modulo = "HDI"
            Case gT_LAB
                modulo = "LAB"
            Case gT_Bloco
                modulo = "BLO"
            Case Else
                modulo = ""
        End Select
        
        If (UCase(HIS.nome) = UCase("SONHO")) Then
            ' Pesquisa pelo episodio.
            RetPesquisaSONHO = SONHO_PesquisaUtente("", episodio, modulo)
        End If
    
        Set rsHIS = New ADODB.recordset
        
        sSql = "SELECT " & vbCrLf & _
               "        * " & vbCrLf & _
               "FROM " & vbCrLf & _
               "        sl_identif " & vbCrLf & _
               "WHERE " & vbCrLf & _
               "        cod_apl = " & gNumeroSessao
    
        If (Len(processo) > 0) Then
            sSql = sSql & " AND utente = " & processo
        End If
        
        With rsHIS
            .Source = sSql
            .CursorType = adOpenStatic
            .CursorLocation = adUseServer
            .Open , gConnHIS
        End With
        
        If (rsHIS.RecordCount <= 0) Then
            ' Nenhum registo.
            processo = ""
            nome = ""
            dt_nasc = ""
        Else
            '  Actualiza
            
            processo = BL_HandleNull(rsHIS!Utente, "")
            nome = BL_HandleNull(rsHIS!nome_ute, "")
            dt_nasc = Format(BL_HandleNull(rsHIS!dt_nasc_ute, ""), gFormatoData)
            
            ' Apaga o registo da tabela tempor�ria do SONHO.
            If (UCase(HIS.nome) = UCase("SONHO")) Then
                 
                 Dim rsAux As ADODB.recordset
                 Set rsAux = New ADODB.recordset
                 
                 sSql = "DELETE FROM sl_identif " & _
                        "WHERE cod_apl = " & gNumeroSessao
                
                 With rsAux
                     .Source = sSql
                     .CursorType = adOpenForwardOnly
                     .CursorLocation = adUseServer
                     .Open , gConnHIS
                 End With
                 
                 Set rsAux = Nothing
            End If
        End If
        rsHIS.Close
        Set rsHIS = Nothing
        BL_Fecha_conexao_HIS
    Else
        ' Problemas com a liga�ao.
        SONHO_Get_Processo = -2
        Exit Function
    End If
    
    SONHO_Get_Processo = 1

End Function

Function SONHO_Get_Proveniencia(episodio As String, _
                                situacao As String, _
                                ByRef cod_proven As String, _
                                ByRef des_proven As String)

    On Error GoTo TrataErro
    
    Dim CmdGera As New ADODB.Command
    
    Dim PmtNumEpisodio As ADODB.Parameter
    Dim PmtCodModulo As ADODB.Parameter
    Dim PmtCodEsp As ADODB.Parameter
    Dim PmtDesEsp As ADODB.Parameter
    
    Dim ret As Integer
        
    On Error GoTo TrataErro
    
    episodio = Trim(episodio)
    situacao = UCase(Trim(situacao))
    cod_proven = ""
    des_proven = ""
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdGera
        .ActiveConnection = gConnHIS
        .CommandText = "DA_SERVICO"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtNumEpisodio = CmdGera.CreateParameter("NUM_EPISODIO", adDecimal, adParamInput)
    Set PmtCodModulo = CmdGera.CreateParameter("COD_MODULO", adVarChar, adParamInput, 10)
    Set PmtCodEsp = CmdGera.CreateParameter("COD_ESP", adDecimal, adParamOutput)
    Set PmtDesEsp = CmdGera.CreateParameter("DES_ESP", adVarChar, adParamOutput, 100)
    
    CmdGera.Parameters.Append PmtNumEpisodio
    CmdGera.Parameters.Append PmtCodModulo
    CmdGera.Parameters.Append PmtCodEsp
    CmdGera.Parameters.Append PmtDesEsp
    
    CmdGera.Parameters.item("NUM_EPISODIO").value = CLng(episodio)
    CmdGera.Parameters.item("COD_MODULO").value = situacao
    
    CmdGera.Execute

    cod_proven = Trim(BL_HandleNull(Trim(CmdGera.Parameters.item("COD_ESP").value), ""))
    des_proven = Trim(BL_HandleNull(Trim(CmdGera.Parameters.item("DES_ESP").value), ""))
    
    Set CmdGera = Nothing
    
    SONHO_Get_Proveniencia = 1
    Exit Function

TrataErro:
    SONHO_Get_Proveniencia = -1
    Set CmdGera = Nothing
End Function

Public Function SONHO_Mapeia_Analise_para_SISLAB(an_Sonho As String, _
                                                 ByRef an_Sislab As String) As Integer
    
    On Error GoTo ErrorHandler
    
    ' Mapeia uma an�lise do SONHO para o SISLAB.
    
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim ret As Integer
    
    an_Sonho = UCase(Trim(an_Sonho))
    an_Sislab = ""
    
    sql = "SELECT " & _
          "     cod_sislab " & _
          "FROM " & _
          "     sl_ana_sonho " & _
          "WHERE " & _
          "     cod_sonho = '" & an_Sonho & "'"
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    rs.Open sql, gConexao
    
    If (rs.RecordCount > 0) Then
        If (Not IsNull(rs!cod_SISLAB)) Then
            an_Sislab = UCase(Trim(rs!cod_SISLAB))
            ret = 1
        Else
            an_Sislab = an_Sonho
            ret = -2
        End If
    Else
        an_Sislab = an_Sonho
        ret = -2
    End If
    rs.Close
    Set rs = Nothing
    
    SONHO_Mapeia_Analise_para_SISLAB = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Set rs = Nothing
            an_Sislab = ""
            Call BG_LogFile_Erros("Erro Inesperado : SONHO_Mapeia_Analise_para_SISLAB (Integracao_SONHO) -> " & Err.Number & ", " & Err.Description)
            SONHO_Mapeia_Analise_para_SISLAB = -1
            Exit Function
    End Select
End Function

Public Function Mapeia_Analises_para_FACTURACAO(Optional Cod_Perfil As String, _
                                                Optional cod_ana_c As String, _
                                                Optional cod_ana_s As String, _
                                                Optional n_req As String, _
                                                Optional cod_agrup As String, _
                                                Optional VerificaInsercao As Boolean) As String
    
    Dim RsVerifInsercao As ADODB.recordset
    'Dim RsTestaExDirecto As ADODB.Recordset
    On Error GoTo ErrorHandler
    
    gQuantidadeAFact = 1
    'gCodAna_aux = ""
    
    ' Mapeia uma an�lise do SISLAB para o SONHO.
    
    Dim sql As String
    Dim SqlP As String
    
    Dim i As Integer
    Dim CriterioTabela As String
    
    Cod_Perfil = BL_HandleNull(Trim(Cod_Perfil), "0")
    cod_ana_c = BL_HandleNull(Trim(cod_ana_c), "0")
    cod_ana_s = BL_HandleNull(Trim(cod_ana_s), "0")
    
    gInsereAnaFact = False
    
    For i = 1 To 3
        Select Case i
            Case 1
                CriterioTabela = Cod_Perfil
            Case 2
                CriterioTabela = cod_ana_c
            Case 3
                CriterioTabela = cod_ana_s
        End Select
    
        If (gCodAna_aux <> CriterioTabela) Or (gReq_aux <> n_req) Then
            
            If (CriterioTabela <> "0") Then
                
                If UCase(HIS.nome) = UCase("SONHO") Then
                    '-------------------------------------------------------------
                    'soliveira - 02-12-2003 - verifica se a an�lise j� foi inserida no sonho
                    If SONHO_Verifica_Ana_ja_Existe(cod_agrup, n_req) = True Then
                        'A an�lise n�o � para facturar, nem para colocar como j� facturada
                        Mapeia_Analises_para_FACTURACAO = "-1"
                        gCodAna_aux = CriterioTabela
                        gReq_aux = n_req
                        Exit Function
                    End If
                    '-------------------------------------------------------------
                End If
                
                Set gRsFact = New ADODB.recordset
                gRsFact.CursorLocation = adUseServer
                gRsFact.CursorType = adOpenStatic
                
                sql = "SELECT x1.cod_ana_gh FROM  sl_ana_facturacao x1, slv_analises_factus x2 WHERE x1.cod_ana = x2.cod_ana"
                sql = sql & " AND x1.cod_ana = '" & CriterioTabela & "' "
                sql = sql & " AND cod_portaria IN (Select x3.cod_portaria FROM sl_portarias x3 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
                sql = sql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
                
                If gF_REQUIS = 1 Then
                    SqlP = sql & " AND cod_prod = '" & FormGestaoRequisicao.FgProd.TextMatrix(1, 0) & "'"
                    gRsFact.Open SqlP, gConexao
                    
                Else
                    sql = sql & " AND cod_prod IS NULL "
                    gRsFact.Open sql, gConexao
                
                End If

                
                If (gRsFact.RecordCount > 0) Then
                    
                    If (BL_HandleNull(gRsFact!cod_ana_gh, "") = "") Then
                        
                        'A an�lise n�o � para facturar
                        Mapeia_Analises_para_FACTURACAO = "-2"
                        Exit Function
                    
                    Else

                        Mapeia_Analises_para_FACTURACAO = BL_HandleNull(gRsFact!cod_ana_gh, "")
                        gQuantidadeAFact = gRsFact.RecordCount
                        
                        gCodAna_aux = CriterioTabela
                        gReq_aux = n_req
                        gInsereAnaFact = True
                        Exit Function
                    End If
                    
                ElseIf gRsFact.RecordCount = 0 And gF_REQUIS = 1 Then
            
                    sql = sql & " AND cod_prod IS NULL "
                
                    gRsFact.Close
                    Set gRsFact = Nothing
                    
                    Set gRsFact = New ADODB.recordset
                    gRsFact.CursorLocation = adUseServer
                    gRsFact.CursorType = adOpenStatic
                    
                    gRsFact.Open sql, gConexao
                    If (gRsFact.RecordCount > 0) Then
                        If (BL_HandleNull(gRsFact!cod_ana_gh, "") = "") Then
                            
                            'A an�lise n�o � para facturar
                            Mapeia_Analises_para_FACTURACAO = "-2"
                            Exit Function
                        Else

                            Mapeia_Analises_para_FACTURACAO = BL_HandleNull(gRsFact!cod_ana_gh, "")
                            gQuantidadeAFact = gRsFact.RecordCount
                            
                            gCodAna_aux = CriterioTabela
                            gReq_aux = n_req
                            gInsereAnaFact = True
                            Exit Function

                        End If
                                        
                    Else
                           ' Logs.
'                            Call BG_Escr_Fich(gDirCliente & "\Bin\Logs\FACTURACAO_Nao_Mapeadas.log", _
'                                              Format(Date, gFormatoData) & " " & Format(Time, gFormatoHora) & _
'                                              " [R:" & n_req & "][P:" & Cod_Perfil & "][C:" & Cod_Ana_C & "][S:" & cod_ana_s & "]")
'
                            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s) VALUES (" & _
                                n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "')"
                            BG_ExecutaQuery_ADO sql
                            
                                
                    End If
                
                Else
                    ' Logs.
'                    If (((cod_ana_s <> "S485") And _
'                          (cod_ana_s <> "S99999"))) Then
                    If i = 3 Then
'                          Call BG_Escr_Fich(gDirCliente & "\Bin\Logs\FACTURACAO_Nao_Mapeadas.log", _
'                                            Format(Date, gFormatoData) & " " & Format(Time, gFormatoHora) & _
'                                            " [R:" & n_req & "][P:" & Cod_Perfil & "][C:" & Cod_Ana_C & "][S:" & cod_ana_s & "]")
                            
                            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s) VALUES( " & _
                                n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "')"
                            BG_ExecutaQuery_ADO sql
                    End If
                End If
                
            End If
        Else
            'A an�lise j� foi facturada
            Mapeia_Analises_para_FACTURACAO = "-2"
            Exit Function
        End If
    Next i
    
'    If Not (CriterioTabela = "S1244" Or _
'            CriterioTabela = "S1245" Or _
'            CriterioTabela = "S1247" Or _
'            CriterioTabela = "S1248" Or _
'            CriterioTabela = "S1249" Or _
'            CriterioTabela = "S1250" Or _
'            CriterioTabela = "S1251" Or _
'            CriterioTabela = "S1252" Or _
'            CriterioTabela = "S703") Then
'
''           MsgBox "A an�lise " & CriterioTabela & " n�o tem correspond�ncia no " & HIS.Nome
'
'            Call BG_LogFile_Erros(CriterioTabela & " n�o tem correspond�ncia no " & HIS.Nome)
'
'    End If
    
    Mapeia_Analises_para_FACTURACAO = "-1"

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Mapeia_Analises_para_FACTURACAO (Integracao_SONHO) -> " & Err.Description)
            Mapeia_Analises_para_FACTURACAO = "-1"
            Exit Function
    End Select
End Function

Public Function Mapeia_Analises_para_FACTURACAO_Lista(cod_ana As String, _
                                                      ByRef encontrados As Integer, _
                                                      ByRef lista() As String) As Integer
    ' Mapeia uma an�lise do SISLAB para uma lista de an�lises do sistema de FACTURACAO.
    
    On Error GoTo ErrorHandler
    
    Dim rs As ADODB.recordset
    Dim sql As String
    Dim str_aux As String
    
    cod_ana = UCase(Trim(cod_ana))
    encontrados = 0
    ReDim lista(0)

    If (cod_ana = "") Then
        Mapeia_Analises_para_FACTURACAO_Lista = -2
        Exit Function
    End If
    
    ' -----------------------------------------------------------------------
        
    sql = "SELECT x1.cod_ana_gh FROM  sl_ana_facturacao x1, slv_analises_factus x2 WHERE x1.cod_ana = x2.cod_ana"
    sql = sql & " AND x1.cod_ana = '" & cod_ana & "' "
    sql = sql & " AND cod_portaria IN (Select x3.cod_portaria FROM sl_portarias x3 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
    sql = sql & " dt_ini and NVL(dt_fim, '31-12-2099')) ORDER BY cod_ana_gh "
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    rs.Open sql, gConexao
        
    While Not rs.EOF
        
        str_aux = Trim(BL_HandleNull(rs!cod_ana_gh, ""))
        
        If (str_aux <> "") Then
        
            lista(UBound(lista)) = str_aux
            ReDim Preserve lista(UBound(lista) + 1)
            
            encontrados = encontrados + 1
        
        End If
        
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
    
    ' -----------------------------------------------------------------------
    
    ' Elimina o elemento em branco.
    ReDim Preserve lista(UBound(lista) - 1)
        
    ' -----------------------------------------------------------------------
    
    Mapeia_Analises_para_FACTURACAO_Lista = 1

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Set rs = Nothing
            Call BG_LogFile_Erros("Erro Inesperado : Mapeia_Analises_para_FACTURACAO_Lista (Integracao_SONHO) -> " & Err.Description)
            Mapeia_Analises_para_FACTURACAO_Lista = -1
            Exit Function
    End Select
End Function

Public Function SONHO_Remove_ANALISES_IN(cod_modulo As String, _
                                       num_episodio As String, _
                                       num_processo As String, _
                                       cod_especialidade As String, _
                                       cod_analise As String, _
                                       dta_episodio As String, _
                                       n_req As Long) As Integer

    Dim retorno As String
    Dim sql As String
    Dim rsHIS As ADODB.recordset
    Dim Cod_Analise_Aux As String
    
    Dim dta_analise As String
    
    cod_analise = BL_HandleNull(Trim(cod_analise), "")
    cod_modulo = Trim(cod_modulo)
    num_episodio = Trim(num_episodio)
    num_processo = BL_HandleNull(Trim(num_processo), "")
    cod_especialidade = BL_HandleNull(Trim(cod_especialidade), "")
    dta_episodio = BL_HandleNull(Trim(dta_episodio), "")

    ' Por defeito a quantidade de an�lises pedidas � 1.

    ' retorno indica o que o SONHO deve fazer com o registo.
    ' I : Inserir no SONHO.
    ' R : O SONHO j� conhece. Indica que foi realizada.
    ' A : Indica que n�o foi realizada e que o SONHO a deve apagar.
    retorno = "A"
    
'    If (Len(Cod_Analise) = 0) Or _
'       (Len(dta_episodio) = 0) Then
'        ' Par�metros incompletos.
'        SONHO_Nova_ANALISES_IN = -2
'        Exit Function
'    End If
    
'    ' Mapeia a situa��o do SISLAB para o SONHO.
'    Select Case situacao
'        Case gT_Urgencia
'            cod_modulo = "URG"
'        Case gT_Consulta
'            If gLAB = "HPOVOA" Then Retorno = "P"
'            cod_modulo = "CON"
'        Case gT_Internamento
'            cod_modulo = "INT"
'        Case gT_Externo
'            cod_modulo = "HDI"
'        Case gT_LAB
'            cod_modulo = "LAB"
'        Case Else
'            cod_modulo = ""
'    End Select
    
    ' Mapeia a an�lise do SISLAB para o SONHO
    
    If (Len(cod_modulo) > 0) And _
       (Len(num_episodio) > 0) Then
    
        
        If Not (IsNumeric(num_episodio)) Then
            ' N� de epis�dio n�o � um n�mero.
            SONHO_Remove_ANALISES_IN = -3
            Exit Function
        End If
        
        ' Caso 1 : Situa��o (m�dulo) + epis�dio
    
        cod_modulo = "'" & cod_modulo & "'"
        num_episodio = num_episodio
        num_processo = "NULL"
        cod_especialidade = "NULL"
        dta_episodio = "'" & dta_episodio & "'"

    Else
    
        ' Caso 2 : Situa��o (m�dulo) + processo + data de epis�dio + cod_especialidade.
        
        cod_modulo = "'" & cod_modulo & "'"
        num_processo = num_processo
        dta_episodio = "'" & Format(Trim(dta_episodio), gFormatoData) & "'"
        cod_especialidade = cod_especialidade
        num_episodio = "NULL"
    
    End If

    
    sql = "INSERT INTO analises_in " & vbCrLf & _
          "( " & vbCrLf & _
          "     cod_modulo, " & vbCrLf & _
          "     num_episodio, " & vbCrLf & _
          "     num_processo, " & vbCrLf & _
          "     dta_episodio, " & vbCrLf & _
          "     dta_analise, " & vbCrLf & _
          "     cod_especialidade, " & vbCrLf & _
          "     cod_analise, " & vbCrLf & _
          "     quantidade, " & vbCrLf & _
          "     retorno " & vbCrLf & _
          ") " & vbCrLf & _
          "VALUES " & vbCrLf & _
          "( " & vbCrLf & _
          "     " & cod_modulo & ", " & vbCrLf & _
          "     " & num_episodio & ", " & vbCrLf & _
          "     " & num_processo & ", " & vbCrLf & _
          "     " & dta_episodio & ", " & vbCrLf & _
          "     " & dta_episodio & ", " & vbCrLf & _
          "     " & cod_especialidade & ", " & vbCrLf & _
          "     '" & cod_analise & "', " & vbCrLf & _
          "     1, " & vbCrLf & _
          "     '" & retorno & "') "
    
    gConnHIS.Execute sql
    BG_Trata_BDErro
    
    
    sql = "DELETE FROM sl_analises_in WHERE " & _
          "cod_modulo = " & cod_modulo & " AND " & _
          "num_episodio = " & num_episodio & " AND " & _
          "cod_analise = '" & cod_analise & "' AND n_req = " & n_req
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
        

    SONHO_Remove_ANALISES_IN = 1

End Function

Public Function Mapeia_Microrg_para_FACTURACAO(Optional cod_micro As String, _
                                                Optional Prova As String, _
                                                Optional n_req As String) As String
    
    Dim RsVerifInsercao As ADODB.recordset
    On Error GoTo ErrorHandler
    
    gQuantidadeAFact = 1
    
    ' Mapeia uma an�lise do SISLAB para o SONHO.
    
    Dim sql As String
    Dim SqlP As String
    
    Dim i As Integer
    Dim CriterioTabela As String
    
    gInsereAnaFact = False
    
    If BL_HandleNull(Prova, "") <> "" Then
            
        Set gRsFact = New ADODB.recordset
        gRsFact.CursorLocation = adUseServer
        gRsFact.CursorType = adOpenStatic
        
        sql = "SELECT x1.cod_ana_gh FROM  sl_ana_facturacao x1, slv_analises_factus x2 WHERE x1.cod_ana = x2.cod_ana"
        sql = sql & " AND x1.cod_ana = '" & cod_micro & "'  AND x1.prova = " & BL_TrataStringParaBD(Prova)
        sql = sql & " AND cod_portaria IN (Select x3.cod_portaria FROM sl_portarias x3 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
        sql = sql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
    
        gRsFact.Open sql, gConexao
                    
        If (gRsFact.RecordCount > 0) Then
            If (BL_HandleNull(gRsFact!cod_ana_gh, "") = "") Then
                
                'A an�lise n�o � para facturar
                Mapeia_Microrg_para_FACTURACAO = "-2"
                Exit Function
            Else
                
                Mapeia_Microrg_para_FACTURACAO = BL_HandleNull(gRsFact!cod_ana_gh, "")
                gQuantidadeAFact = gRsFact.RecordCount
        
                
                gCodAna_aux = CriterioTabela
                gReq_aux = n_req
                gInsereAnaFact = True
                Exit Function
                    
            End If
                                        
        Else
               ' Logs.
              
                Call BG_Escr_Fich(gDirCliente & "\Bin\Logs\FACTURACAO_Nao_Mapeadas.log", _
                                  Format(Date, gFormatoData) & " " & Format(time, gFormatoHora) & _
                                  " [R:" & n_req & "][Cod_Micro:" & cod_micro & "][Prova:" & Prova & "]")
        End If
        
        Mapeia_Microrg_para_FACTURACAO = "-1"
    Else
        Mapeia_Microrg_para_FACTURACAO = "-1"
    End If

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Mapeia_Analises_para_FACTURACAO (Integracao_SONHO) -> " & Err.Description)
            Mapeia_Microrg_para_FACTURACAO = "-1"
            Exit Function
    End Select
End Function

Public Function Mapeia_Antib_para_FACTURACAO(Optional cod_micro As String, _
                                                Optional CodGrAntibio As String, _
                                                Optional Metodo_Tsq As String, _
                                                Optional n_req As String) As String
    
    Dim RsVerifInsercao As ADODB.recordset
    Dim s As String
    Dim PosAux As Integer
    On Error GoTo ErrorHandler
    
    gQuantidadeAFact = 1
    
    ' Mapeia uma an�lise do SISLAB para o SONHO.
    
    Dim sql As String
    Dim SqlP As String
    
    Dim i As Integer
    Dim CriterioTabela As String
    
    gInsereAnaFact = False
    
    'Verifica se tem grupo de antibi�ticos registados
    If CodGrAntibio <> "" Then
    
        Dim RsMetodoTsq As ADODB.recordset
        
        Set RsMetodoTsq = New ADODB.recordset
        sql = " SELECT cod_metodo,sl_gr_antibio.cod_rubr " & _
              " FROM sl_gr_antibio,sl_tbf_metodo_tsq " & _
              " WHERE sl_gr_antibio.cod_metodo = sl_tbf_metodo_tsq.cod_metodo_tsq " & _
              " AND cod_gr_antibio = " & BL_TrataStringParaBD(CodGrAntibio)
        RsMetodoTsq.CursorLocation = adUseServer
        RsMetodoTsq.CursorType = adOpenStatic
        RsMetodoTsq.Open sql, gConexao
        If (RsMetodoTsq.RecordCount > 0) Then
            'Os m�todos E-Test e BK s�o analisados no fim
            If (BL_HandleNull(RsMetodoTsq!cod_rubr, "") = "") Or BL_HandleNull(RsMetodoTsq!cod_metodo, "") = "2" Or BL_HandleNull(RsMetodoTsq!cod_metodo, "") = "3" Then
                
                'A an�lise n�o � para facturar
                Mapeia_Antib_para_FACTURACAO = "-2"
                Exit Function
            Else
                
                Mapeia_Antib_para_FACTURACAO = BL_HandleNull(RsMetodoTsq!cod_rubr, "")
                gQuantidadeAFact = RsMetodoTsq.RecordCount
              
                gCodAna_aux = CriterioTabela
                gReq_aux = n_req
                gInsereAnaFact = True
                Exit Function
                    
            End If
                                        
        Else
             ' Logs.
            
              Call BG_Escr_Fich(gDirCliente & "\Bin\Logs\FACTURACAO_Nao_Mapeadas.log", _
                                Format(Date, gFormatoData) & " " & Format(time, gFormatoHora) & _
                                " [R:" & n_req & "][Cod_Micro:" & cod_micro & "][CodGrAntibio:" & CodGrAntibio & "]")
                                
        End If
        Mapeia_Antib_para_FACTURACAO = "-1"
        
        
              
    'Se n�o, verifica se tem o m�todo tsq a facturar,por defeito, para este microrganismo
    ElseIf BL_HandleNull(Metodo_Tsq, "") <> "" Then
            
        Set gRsFact = New ADODB.recordset
        gRsFact.CursorLocation = adUseServer
        gRsFact.CursorType = adOpenStatic
        
        sql = "SELECT " & vbCrLf & _
              "     cod_rubr " & vbCrLf & _
              "FROM " & vbCrLf & _
              "     sl_tbf_metodo_tsq " & vbCrLf & _
              "WHERE " & vbCrLf & _
              " cod_metodo_tsq = " & BL_TrataStringParaBD(Metodo_Tsq)
    
        gRsFact.Open sql, gConexao
                    
        If (gRsFact.RecordCount > 0) Then
            If (BL_HandleNull(gRsFact!cod_rubr, "") = "") Then
                
                'A an�lise n�o � para facturar
                Mapeia_Antib_para_FACTURACAO = "-2"
                Exit Function
            Else
                
                Mapeia_Antib_para_FACTURACAO = BL_HandleNull(gRsFact!cod_rubr, "")
                gQuantidadeAFact = gRsFact.RecordCount
        
                
                gCodAna_aux = CriterioTabela
                gReq_aux = n_req
                gInsereAnaFact = True
                Exit Function
                    
            End If
                                        
        Else
               ' Logs.
              
                Call BG_Escr_Fich(gDirCliente & "\Bin\Logs\FACTURACAO_Nao_Mapeadas.log", _
                                  Format(Date, gFormatoData) & " " & Format(time, gFormatoHora) & _
                                  " [R:" & n_req & "][Cod_Micro:" & cod_micro & "][Metodo_TSQ:" & Metodo_Tsq & "]")
        End If
        Mapeia_Antib_para_FACTURACAO = "-1"
    Else
        If BL_HandleNull(Metodo_Tsq, "") = "" Then
                Call BG_Escr_Fich(gDirCliente & "\Bin\Logs\FACTURACAO_Nao_Mapeadas.log", _
                                  Format(Date, gFormatoData) & " " & Format(time, gFormatoHora) & _
                                  " [R:" & n_req & "][Cod_Micro:" & cod_micro & "][Metodo:" & Metodo_Tsq & " n�o codificado]")
        End If
        Mapeia_Antib_para_FACTURACAO = "-1"
    End If

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Mapeia_Antib_para_FACTURACAO (Integracao_SONHO) -> " & Err.Description)
            Mapeia_Antib_para_FACTURACAO = "-1"
            Exit Function
    End Select
End Function

Function SONHO_Verifica_Ana_ja_Existe(codAna As String, NReq As String) As Boolean
    'Fun��o que verifica se a an�lise j� foi inserida na tabela do SONHO
    
    Dim RsVerifInsercao As ADODB.recordset
    Dim sql As String

    If UCase(HIS.nome) = UCase("SONHO") Then
        '-------------------------------------------------------------
        'soliveira - 02-12-2003 - verifica se a an�lise j� foi inserida no sonho
        Set RsVerifInsercao = New ADODB.recordset
        RsVerifInsercao.CursorLocation = adUseServer
        RsVerifInsercao.CursorType = adOpenStatic

        sql = "SELECT " & vbCrLf & _
              "     cod_analise " & vbCrLf & _
              "FROM " & vbCrLf & _
              "     sl_analises_in " & vbCrLf & _
              "WHERE " & vbCrLf & _
              "     cod_ana_sislab = '" & codAna & "' and n_req = " & NReq
        RsVerifInsercao.Open sql, gConexao
        If RsVerifInsercao.RecordCount > 0 Then
            'A an�lise j� se encontra na tabela do SONHO
            'A an�lise n�o � para facturar, nem para colocar como j� facturada
            SONHO_Verifica_Ana_ja_Existe = True
        Else
            SONHO_Verifica_Ana_ja_Existe = False
        End If
        '-------------------------------------------------------------
    End If

End Function


Public Function SONHO_Factura_Microrg(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, _
                                                cod_micro As String, _
                                                Prova As String) As Boolean
    'soliveira: 23.09.2006
    'nova fun��o para facturar microrganismos
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim SqlP As String
    Dim RsFact As ADODB.recordset
    Dim i As Integer
    Dim CriterioTabela As String
    
    SONHO_Factura_Microrg = False
    
    If BL_HandleNull(Prova, "") <> "" Then
    
'        If UCase(HIS.Nome) = UCase("SONHO") Then
'            '-------------------------------------------------------------
'            'verifica se a an�lise j� foi inserida no sonho
'            If SONHO_Verifica_Ana_ja_Existe(cod_micro, n_req) = True Then
'                'A an�lise n�o � para facturar, nem para colocar como j� facturada
'                SONHO_Factura_Microrg = False
'                Exit Function
'            End If
'            '-------------------------------------------------------------
'        End If
                
        'prova: campo para facturar diferentes rubricas(identifica��es de microrganismo) conforme id de prova
        Set RsFact = New ADODB.recordset
        RsFact.CursorLocation = adUseServer
        RsFact.CursorType = adOpenStatic
        
'        Sql = "SELECT " & vbCrLf & _
'              "     cod_ana_gh " & vbCrLf & _
'              "FROM " & vbCrLf & _
'              "     sl_ana_facturacao " & vbCrLf & _
'              "WHERE " & vbCrLf & _
'              "     cod_ana = '" & cod_micro & "' " & _
'              " AND prova = " & BL_TrataStringParaBD(Prova)
        sql = "SELECT cod_rubr from sl_tbf_t_prova where cod_t_prova = '" & Prova & "'"
    
        RsFact.Open sql, gConexao
        
        If RsFact.RecordCount > 0 Then
            While Not RsFact.EOF
                If (BL_HandleNull(RsFact!cod_rubr, "") <> "") Then
                    If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                n_epis, _
                                                processo, _
                                                cod_proven, _
                                                RsFact!cod_rubr, _
                                                dt_chega, _
                                                CLng(n_req), _
                                                cod_micro) = 1) Then
                    
                                                            
                        SONHO_Factura_Microrg = True
                    End If
'                Else
'                    Sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro) VALUES (" & _
'                            n_req & ",'" & Cod_Perfil & "','" & Cod_Ana_C & "','" & cod_ana_s & "','" & cod_micro & "')"
'                    BG_ExecutaQuery_ADO Sql
                End If
                RsFact.MoveNext
            Wend
        Else
            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro) VALUES (" & _
                    n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "')"
            BG_ExecutaQuery_ADO sql
        End If
        RsFact.Close
        Set RsFact = Nothing
    Else
        sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro) VALUES (" & _
                n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "')"
        BG_ExecutaQuery_ADO sql
    End If

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : SONHO_Factura_Microrg (Integracao_SONHO) -> " & Err.Description)
            SONHO_Factura_Microrg = False
            Exit Function
    End Select
End Function

Public Function SONHO_Factura_Antibiograma(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, _
                                                CodGrAntibio As String, _
                                                Metodo_Tsq As String, _
                                                cod_micro As String, _
                                                seq_realiza As String) As Boolean
    'soliveira: 23.09.2006
    'nova fun��o para facturar antibiograma do microrganismo
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim SqlP As String
    Dim RsFact As ADODB.recordset
    Dim i As Integer
    Dim CriterioTabela As String
    
    SONHO_Factura_Antibiograma = False
    
    'Verifica se tem grupo de antibi�ticos registados
    If CodGrAntibio <> "" Then
    
'        If UCase(HIS.Nome) = UCase("SONHO") Then
'            '-------------------------------------------------------------
'            'verifica se a an�lise j� foi inserida no sonho
'            If SONHO_Verifica_Ana_ja_Existe(CodGrAntibio, n_req) = True Then
'                'A an�lise n�o � para facturar, nem para colocar como j� facturada
'                SONHO_Factura_Antibiograma = False
'                Exit Function
'            End If
'            '-------------------------------------------------------------
'        End If
    
        Dim RsMetodoTsq As ADODB.recordset
        
        'a rubrica associada a carta esta registada na codifca��o de grupo de antibioticos
        Set RsMetodoTsq = New ADODB.recordset
'        Sql = " SELECT cod_metodo,sl_gr_antibio.cod_rubr " & _
'              " FROM sl_gr_antibio,sl_tbf_metodo_tsq " & _
'              " WHERE sl_gr_antibio.cod_metodo = sl_tbf_metodo_tsq.cod_metodo_tsq " & _
'              " AND cod_gr_antibio = " & BL_TrataStringParaBD(CodGrAntibio)
        'selecciona o metodo e a rubrica associdada a carta na codifica��o de cartas (grupo de antibioticos)
        sql = " SELECT cod_metodo,sl_gr_antibio.cod_rubr " & _
              " FROM sl_gr_antibio" & _
              " WHERE cod_gr_antibio = " & BL_TrataStringParaBD(CodGrAntibio)
        RsMetodoTsq.CursorLocation = adUseServer
        RsMetodoTsq.CursorType = adOpenStatic
        RsMetodoTsq.Open sql, gConexao
        If (RsMetodoTsq.RecordCount > 0) Then
            'Os m�todos E-Test e BK s�o analisados no fim
            If BL_HandleNull(RsMetodoTsq!cod_metodo, "") <> "2" And BL_HandleNull(RsMetodoTsq!cod_metodo, "") <> "3" Then
                If (BL_HandleNull(RsMetodoTsq!cod_rubr, "") <> "") Then
                    
                    'insere carta no sonho
                    If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                n_epis, _
                                                processo, _
                                                cod_proven, _
                                                RsMetodoTsq!cod_rubr, _
                                                dt_chega, _
                                                CLng(n_req), _
                                                CodGrAntibio) = 1) Then
                                           
                        SONHO_Factura_Antibiograma = True
                    End If
                
                Else
                    sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_gr_antibio) VALUES (" & _
                            n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & CodGrAntibio & "')"
                    BG_ExecutaQuery_ADO sql
                End If
            End If
        Else
            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_gr_antibio) VALUES (" & _
                    n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & CodGrAntibio & "')"
            BG_ExecutaQuery_ADO sql
        End If
        RsMetodoTsq.Close
        Set RsMetodoTsq = Nothing
        
    'Se n�o tem carta, verifica se tem o m�todo tsq a facturar,por defeito, para este microrganismo
    ElseIf BL_HandleNull(Metodo_Tsq, "") <> "" Then
            
        Set RsFact = New ADODB.recordset
        RsFact.CursorLocation = adUseServer
        RsFact.CursorType = adOpenStatic
        
        sql = "SELECT " & vbCrLf & _
              "     cod_rubr " & vbCrLf & _
              "FROM " & vbCrLf & _
              "     sl_tbf_metodo_tsq " & vbCrLf & _
              "WHERE " & vbCrLf & _
              " cod_metodo_tsq = " & BL_TrataStringParaBD(Metodo_Tsq)
    
        RsFact.Open sql, gConexao
                    
        If RsFact.RecordCount > 0 Then
            While Not RsFact.EOF
                If BL_HandleNull(RsFact!cod_rubr, "") <> "" Then
                    If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                n_epis, _
                                                processo, _
                                                cod_proven, _
                                                RsFact!cod_rubr, _
                                                dt_chega, _
                                                CLng(n_req), _
                                                Metodo_Tsq) = 1) Then
                                           
                        SONHO_Factura_Antibiograma = True
                    End If
                End If
                RsFact.MoveNext
            Wend
        Else
            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro,cod_gr_antibio) VALUES (" & _
                    n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "','" & Metodo_Tsq & "')"
            BG_ExecutaQuery_ADO sql
        End If
        RsFact.Close
        Set RsFact = Nothing

        If BL_HandleNull(Metodo_Tsq, "") = "" Then
            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro,cod_gr_antibio) VALUES (" & _
                    n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "','" & cod_micro & "','" & CodGrAntibio & "')"
            BG_ExecutaQuery_ADO sql

        End If
    End If


Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : SONHO_Factura_Antibiograma (Integracao_SONHO) -> " & Err.Description)
            SONHO_Factura_Antibiograma = False
            Exit Function
    End Select
End Function

Public Function SONHO_Factura_Analise(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, seq_realiza As String) As Boolean
    'soliveira: 23.09.2006
    'nova fun��o para facturar analises
    
    Dim RsVerifInsercao As ADODB.recordset
    Dim RsFact As ADODB.recordset
    Dim RsRegra As ADODB.recordset
    'Dim RsTestaExDirecto As ADODB.Recordset
    On Error GoTo ErrorHandler
    
    gQuantidadeAFact = 1
    gAnalise_Ja_Facturada = False
    
    ' Mapeia uma an�lise do SISLAB para o SONHO.
    
    Dim sql As String
    Dim SqlP As String
    
    Dim i As Integer
    Dim CriterioTabela As String

    
    Cod_Perfil = BL_HandleNull(Trim(Cod_Perfil), "0")
    cod_ana_c = BL_HandleNull(Trim(cod_ana_c), "0")
    cod_ana_s = BL_HandleNull(Trim(cod_ana_s), "0")
    
    gInsereAnaFact = False
    SONHO_Factura_Analise = False
    
    For i = 1 To 3
FacturaMembro:
        Select Case i
            Case 1
                CriterioTabela = Cod_Perfil
            Case 2
                CriterioTabela = cod_ana_c
            Case 3
                CriterioTabela = cod_ana_s
        End Select
    
        If (gCodAna_aux <> CriterioTabela) Or (gReq_aux <> n_req) Then
            
            If (CriterioTabela <> "0") Then
                
                Set RsFact = New ADODB.recordset
                RsFact.CursorLocation = adUseServer
                RsFact.CursorType = adOpenStatic
                
                sql = "SELECT cod_ana_gh, flg_regra, cod_ana_regra, factura_membros, centro_custo "
                sql = sql & " FROM sl_ana_facturacao x1, slv_analises_factus x2  WHERE x1.cod_ana = x2.cod_ana "
                sql = sql & " AND cod_ana = '" & CriterioTabela & "' "
                sql = sql & " AND cod_portaria IN (Select x3.cod_portaria FROM sl_portarias x3 WHERE " & BL_TrataDataParaBD(BL_HandleNull(dt_chega, Bg_DaData_ADO)) & " BETWEEN "
                sql = sql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
                If gF_REQUIS = 1 Then
                    SqlP = sql & " AND cod_prod = '" & FormGestaoRequisicao.FgProd.TextMatrix(1, 0) & "'"
                    RsFact.Open SqlP, gConexao
                    
                Else
                    sql = sql & " AND cod_prod IS NULL "
                    RsFact.Open sql, gConexao
                
                End If

                If RsFact.RecordCount > 0 Then
                    '-------------------------------------------------------------
                    'verifica se a an�lise j� foi inserida no sonho
                    If UCase(HIS.nome) = UCase("SONHO") Then
                        If SONHO_Verifica_Ana_ja_Existe(CriterioTabela, n_req) = True Then
                        'If SONHO_Verifica_Ana_ja_Existe(cod_agrup, n_req) = True Then
                            'A an�lise j� foi facturada, colocar como facturada
                            gAnalise_Ja_Facturada = True
                            SONHO_Factura_Analise = False
                            gCodAna_aux = CriterioTabela
                            gReq_aux = n_req
                            'como � para facturar os membros, n�o sai da funcao para ir facturar com i = 3 o membro da complexa
                            If BL_HandleNull(RsFact!factura_membros, "0") = "1" Then
                                i = i + 1
                                GoTo FacturaMembro
                            Else
                                Exit Function
                            End If
                        End If
                    End If
                    '-------------------------------------------------------------
                    
                    While Not RsFact.EOF
                        If (BL_HandleNull(RsFact!cod_ana_gh, "") <> "") Then
                            

                            'Verifica se existe regra para facturar a an�lise, se a regra se verifica, n�o factura
                            If SONHO_Verifica_Regra(BL_HandleNull(RsFact!Flg_Regra, "0"), BL_HandleNull(RsFact!Cod_Ana_Regra, ""), n_req) = True Then
                                '� para colocar como facturado
                                'usamos a mm variavel gAnalise_Ja_Facturada pq faz o mesmo efeito
                                gAnalise_Ja_Facturada = True
                                gCodAna_aux = CriterioTabela
                                gReq_aux = n_req
                                SONHO_Factura_Analise = False
                                Exit Function
                            Else
                            
                                If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                    n_epis, _
                                                    processo, _
                                                    cod_proven, _
                                                    RsFact!cod_ana_gh, _
                                                    dt_chega, _
                                                    CLng(n_req), _
                                                    CriterioTabela, , , , , , , , , BL_HandleNull(RsFact!centro_custo, "")) = 1) Then
                                    SONHO_Factura_Analise = True
                                    'se quero que facture os proximos membros da complexa, n�o posso indicar a analise anterior facturada
                                    'para entrar no if inicial: "if gcodana_aux <> criteriotabela"
                                    If BL_HandleNull(RsFact!factura_membros, "0") <> "1" Then
                                        gCodAna_aux = CriterioTabela
                                    End If
                                    gReq_aux = n_req
                                    gInsereAnaFact = True
                                End If
                            End If
                        Else
                            '� para colocar como facturado
                            'usamos a mm variavel gAnalise_Ja_Facturada pq faz o mesmo efeito
                            gAnalise_Ja_Facturada = True
                            SONHO_Factura_Analise = False
                            Exit Function
                        End If
                        RsFact.MoveNext
                    Wend
                    If SONHO_Factura_Analise = True Then Exit Function
                    
                ElseIf RsFact.RecordCount = 0 And gF_REQUIS = 1 Then
            
                    sql = sql & " AND cod_prod IS NULL "
                
                    RsFact.Close
                    Set RsFact = Nothing
                    
                    Set RsFact = New ADODB.recordset
                    RsFact.CursorLocation = adUseServer
                    RsFact.CursorType = adOpenStatic
                    
                    RsFact.Open sql, gConexao
                    If (RsFact.RecordCount > 0) Then
                        While Not RsFact.EOF
                            If (BL_HandleNull(RsFact!cod_ana_gh, "") <> "") Then
                                '-------------------------------------------------------------
                                'verifica se a an�lise j� foi inserida no sonho
                                If UCase(HIS.nome) = UCase("SONHO") Then
                                    If SONHO_Verifica_Ana_ja_Existe(CriterioTabela, n_req) = True Then
                                    'If SONHO_Verifica_Ana_ja_Existe(cod_agrup, n_req) = True Then
                                        'A an�lise j� foi facturada, colocar como facturada
                                        gAnalise_Ja_Facturada = True
                                        SONHO_Factura_Analise = False
                                        gCodAna_aux = CriterioTabela
                                        gReq_aux = n_req
                                        If BL_HandleNull(RsFact!factura_membros, "0") = "1" Then
                                            i = i + 1
                                            GoTo FacturaMembro
                                        Else
                                            Exit Function
                                        End If
                                    End If
                                End If
                                '-------------------------------------------------------------
                                
                                'Verifica se existe regra para facturar a an�lise, se a regra se verifica, n�o factura
                                If SONHO_Verifica_Regra(BL_HandleNull(RsFact!Flg_Regra, "0"), BL_HandleNull(RsFact!Cod_Ana_Regra, ""), n_req) = True Then
                                    '� para colocar como facturado
                                    'usamos a mm variavel gAnalise_Ja_Facturada pq faz o mesmo efeito
                                    gAnalise_Ja_Facturada = True
                                    SONHO_Factura_Analise = False
                                Else
                                        If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                        n_epis, _
                                                        processo, _
                                                        cod_proven, _
                                                        RsFact!cod_ana_gh, _
                                                        dt_chega, _
                                                        CLng(n_req), _
                                                        CriterioTabela, , , , , , , , , BL_HandleNull(RsFact!centro_custo, "")) = 1) Then
                                                        
                                            SONHO_Factura_Analise = True
                                        End If
                                        'se quero que facture os proximos membros da complexa, n�o posso indicar a analise anterior facturada
                                        'para entrar no if inicial: "if gcodana_aux <> criteriotabela"
                                        If BL_HandleNull(RsFact!factura_membros, "0") <> "1" Then
                                            gCodAna_aux = CriterioTabela
                                        End If
                                        gReq_aux = n_req
                                        gInsereAnaFact = True
                                End If
                            
                            Else
                                '� para colocar como facturado
                                'usamos a mm variavel gAnalise_Ja_Facturada pq faz o mesmo efeito
                                gAnalise_Ja_Facturada = True
                                SONHO_Factura_Analise = False
                                Exit Function
                            End If
                            RsFact.MoveNext
                        Wend
                        If SONHO_Factura_Analise = True Then Exit Function
                                        
                    Else
                        
                        sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s) VALUES (" & _
                            n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "')"
                        BG_ExecutaQuery_ADO sql
                            
                    End If
                
                Else
                    ' Logs.
'                    If (((cod_ana_s <> "S485") And _
'                          (cod_ana_s <> "S99999"))) Then
                    If i = 3 Then
'                          Call BG_Escr_Fich(gDirCliente & "\Bin\Logs\FACTURACAO_Nao_Mapeadas.log", _
'                                            Format(Date, gFormatoData) & " " & Format(Time, gFormatoHora) & _
'                                            " [R:" & n_req & "][P:" & Cod_Perfil & "][C:" & Cod_Ana_C & "][S:" & cod_ana_s & "]")
                            
                            sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s) VALUES (" & _
                                n_req & ",'" & Cod_Perfil & "','" & cod_ana_c & "','" & cod_ana_s & "')"
                            BG_ExecutaQuery_ADO sql
                            
                    End If
                End If
                
            End If
        Else
            'A an�lise j� foi facturada, mas n�o faz parte da contagem
            gAnalise_Ja_Facturada = True
            SONHO_Factura_Analise = False
            Exit Function
        End If
    Next i
    

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : sonho_factura_analise (Integracao_SONHO) -> " & Err.Description)
            SONHO_Factura_Analise = False
            Exit Function
    End Select
End Function

Public Function SONHO_Factura_Teste(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, _
                                                cod_micro As String, _
                                                seq_realiza As String) As Boolean
    'soliveira: 17.10.2006
    'nova fun��o para facturar teste do microrganismo
    
    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim SqlP As String
    Dim RsFact As ADODB.recordset
    Dim i As Integer
    Dim CriterioTabela As String
    
    SONHO_Factura_Teste = False
                
    'a rubrica associada ao teste do microrganismo encontra-se na codifica��o de provas
    Set RsFact = New ADODB.recordset
    RsFact.CursorLocation = adUseServer
    RsFact.CursorType = adOpenStatic
        
    sql = "select distinct sl_res_prova.cod_prova,sl_prova.cod_rubr " & _
            "from sl_res_prova, sl_prova " & _
            "where sl_res_prova.cod_prova = sl_prova.cod_prova and " & _
            "sl_res_prova.seq_realiza = " & seq_realiza & _
            " "
    If cod_micro <> "" Then
        sql = sql & " and sl_res_prova.cod_micro = " & BL_TrataStringParaBD(cod_micro)
    End If

    RsFact.Open sql, gConexao
        
    If RsFact.RecordCount > 0 Then
        While Not RsFact.EOF
            If (BL_HandleNull(RsFact!cod_rubr, "") <> "") Then
                
                If SONHO_Verifica_Ana_ja_Existe(RsFact!Cod_Prova, n_req) = False Then
                
                    If (SONHO_Nova_ANALISES_IN(t_sit, _
                                                n_epis, _
                                                processo, _
                                                cod_proven, _
                                                RsFact!cod_rubr, _
                                                dt_chega, _
                                                CLng(n_req), _
                                                RsFact!Cod_Prova) = 1) Then
                    
                                                            
                        SONHO_Factura_Teste = True
                    End If
                End If
            Else
'                Sql = "INSERT INTO SL_NAO_FACTURADAS(n_req,cod_perfil,cod_ana_c,cod_ana_s,cod_micro,cod_teste) VALUES (" & _
'                        n_req & ",'" & Cod_Perfil & "','" & Cod_Ana_C & "','" & cod_ana_s & "','" & cod_micro & "','" & RsFact!cod_prova & "')"
'                BG_ExecutaQuery_ADO Sql
            End If
            RsFact.MoveNext
        Wend
        
    End If
    RsFact.Close
    Set RsFact = Nothing

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : SONHO_Factura_Teste (Integracao_SONHO) -> " & Err.Description)
            SONHO_Factura_Teste = False
            Exit Function
    End Select
End Function

Public Function SONHO_Factura_Antibioticos(n_req As String, _
                                                t_sit As String, _
                                                n_epis As String, _
                                                dt_chega As String, _
                                                processo As String, _
                                                cod_proven As String, _
                                                Cod_Perfil As String, _
                                                cod_ana_c As String, _
                                                cod_ana_s As String, _
                                                cod_agrup As String, _
                                                CodGrAntibio As String, _
                                                Metodo_Tsq As String, _
                                                cod_micro As String, _
                                                seq_realiza As String) As Boolean
    Dim RsFactAntib As ADODB.recordset
    Dim sql As String


    'Verificar se existem antibi�ticos registados cujo seu grupo de antibi�ticos tenham o m�todo TSQ "E-TEST" ou "BK"
    'n�o � poss�vel colocar estes antibioticos em grupos de antibioticos? op��o de cima
    Set RsFactAntib = New ADODB.recordset
    sql = " SELECT distinct sl_res_tsq.cod_antib, cod_metodo, cod_rubr " & _
          " from sl_res_tsq, sl_rel_grantib,sl_gr_antibio " & _
          " WHERE sl_res_tsq.cod_antib = sl_rel_grantib.cod_antibio " & _
          " and sl_rel_grantib.cod_gr_antibio = sl_gr_antibio.cod_gr_antibio " & _
          " and seq_realiza = " & seq_realiza & " and n_res = " & 1 & _
          " and cod_micro = " & BL_TrataStringParaBD(cod_micro) & "" & _
          " and cod_metodo in ( 2,3) "
    RsFactAntib.CursorType = adOpenStatic
    RsFactAntib.CursorLocation = adUseServer
    RsFactAntib.Open sql, gConexao
    If RsFactAntib.RecordCount > 0 Then
        While Not RsFactAntib.EOF
            If (SONHO_Nova_ANALISES_IN(t_sit, _
                                    n_epis, _
                                    processo, _
                                    cod_proven, _
                                    RsFactAntib!cod_rubr, _
                                    dt_chega, _
                                    CLng(n_req), _
                                    RsFactAntib!cod_antib) = 1) Then

                SONHO_Factura_Antibioticos = True
            End If
            RsFactAntib.MoveNext
        Wend
    End If
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : SONHO_Factura_Antibioticos (Integracao_SONHO) -> " & Err.Description)
            SONHO_Factura_Antibioticos = False
            Exit Function
    End Select
End Function

Function SONHO_Verifica_Regra(Flg_Regra As String, Cod_Ana_Regra As String, n_req As String) As Boolean
    'Verifica se tem regra, e se tem, se a regra � verdadeira
    'True - n�o factura an�lise
    'False - factura an�lise
    Dim RsRegra As ADODB.recordset
    Dim CriterioTabela As String
    
    On Error GoTo TrataErro

    SONHO_Verifica_Regra = True
    
    If Flg_Regra = "1" And Cod_Ana_Regra <> "" Then
        If Mid(Cod_Ana_Regra, 1, 1) = "S" Then
            CriterioTabela = " cod_ana_s = "
        ElseIf Mid(Cod_Ana_Regra, 1, 1) = "C" Then
            CriterioTabela = " cod_ana_c = "
        ElseIf Mid(Cod_Ana_Regra, 1, 1) = "P" Then
            CriterioTabela = " cod_perfil = "
        End If
        
        If Cod_Ana_Regra = "C455" Then
            CriterioTabela = ""
        End If
        
        Set RsRegra = New ADODB.recordset

        RsRegra.CursorLocation = adUseServer
        RsRegra.CursorType = adOpenStatic
        RsRegra.Source = "select * from sl_realiza where n_req = " & BL_TrataStringParaBD(n_req) & _
                        " and " & IIf(Cod_Ana_Regra = "C455", " cod_ana_c in ('C455', 'C482') ", CriterioTabela & BL_TrataStringParaBD(Cod_Ana_Regra))
        RsRegra.ActiveConnection = gConexao
        RsRegra.Open
        If Not RsRegra.EOF Then
            'se a an�lise da regra est� presenta na requisi��o, n�o factura
            SONHO_Verifica_Regra = True
        Else
            SONHO_Verifica_Regra = False
        End If
    Else
        SONHO_Verifica_Regra = False
    End If
    
Exit Function
TrataErro:
    SONHO_Verifica_Regra = True
    Exit Function
End Function

' --------------------------------------------------------------------------------------

'  QUANDO UTILIZADOR QUER FACTURAR NOVAMENTE ENTAO ENVIA O QUE JA EXISTIA PARA HISTORICO

' --------------------------------------------------------------------------------------
Public Function SONHO_EnviaParaHistorico(n_req As String) As Boolean
    Dim sSql As String
    Dim rsSonho As New ADODB.recordset
    Dim iRegistos As Integer
    On Error GoTo TrataErro
    SONHO_EnviaParaHistorico = False
    
    sSql = "SELECT * FROM sl_analises_in WHERE n_req = " & n_req
    rsSonho.CursorLocation = adUseServer
    rsSonho.CursorType = adOpenStatic
    rsSonho.Source = sSql
    rsSonho.ActiveConnection = gConexao
    rsSonho.Open
    If rsSonho.RecordCount > 0 Then
        While Not rsSonho.EOF
            sSql = "INSERT INTO sl_analises_in_h (cod_modulo, num_episodio, num_processo, dta_episodio,"
            sSql = sSql & " cod_especialidade, cod_analise, quantidade, retorno,dta_analise, n_req, cod_ana_sislab,"
            sSql = sSql & " cod_perfil,cod_ana_c, cod_ana_s, cod_micro, cod_gr_antibio,cod_antibio, cod_prova, "
            sSql = sSql & " cod_agrup, centro_custo,cod_aparelho, dt_cri, dt_hist, user_hist) VALUES ( "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSonho!cod_modulo, "")) & ", "
            sSql = sSql & BL_HandleNull(rsSonho!num_episodio, "Null") & ", "
            sSql = sSql & BL_HandleNull(rsSonho!num_processo, "null") & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsSonho!dta_episodio, "")) & ", "
            sSql = sSql & BL_HandleNull(rsSonho!cod_especialidade, "null") & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSonho!cod_analise, "")) & ", "
            sSql = sSql & BL_HandleNull(rsSonho!quantidade, "1") & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSonho!retorno, "")) & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsSonho!dta_analise, "")) & ", "
            sSql = sSql & BL_HandleNull(rsSonho!n_req, "null") & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSonho!cod_ana_sislab, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSonho!Cod_Perfil, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSonho!cod_ana_c, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSonho!cod_ana_s, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSonho!cod_micro, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSonho!Cod_Gr_Antibio, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSonho!cod_antibio, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSonho!Cod_Prova, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSonho!cod_agrup, "")) & ", "
            sSql = sSql & BL_HandleNull(rsSonho!centro_custo, "null") & ", "
            sSql = sSql & BL_HandleNull(rsSonho!cod_aparelho, "null") & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsSonho!dt_cri, "")) & ", "
            sSql = sSql & "sysdate, "
            sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ") "
            iRegistos = BG_ExecutaQuery_ADO(sSql)
            If iRegistos < 1 Then
                GoTo TrataErro
            End If
            rsSonho.MoveNext
        Wend
        
        'APAGA DA SL_ANALISES_IN
        sSql = "DELETE FROM sl_analises_in WHERE n_Req = " & n_req
        iRegistos = BG_ExecutaQuery_ADO(sSql)
        If iRegistos < 0 Then
            GoTo TrataErro
        End If
        
        'ALTERA ESTADO NA SL_REALIZA
        sSql = "UPDATE sl_realiza SET flg_facturado = 0 WHERE n_Req = " & n_req
        iRegistos = BG_ExecutaQuery_ADO(sSql)
        If iRegistos < 0 Then
            GoTo TrataErro
        End If
        
        'ALTERA ESTADO NA SL_MARCACOES
        sSql = "UPDATE sl_marcacoes SET flg_facturado = 0 WHERE n_Req = " & n_req
        iRegistos = BG_ExecutaQuery_ADO(sSql)
        If iRegistos < 0 Then
            GoTo TrataErro
        End If
        
        End If
    rsSonho.Close
    Set rsSonho = Nothing
    SONHO_EnviaParaHistorico = True
Exit Function
TrataErro:
    SONHO_EnviaParaHistorico = False
    BG_LogFile_Erros "SONHO_EnviaParaHistorico: " & Err.Description & " " & sSql, "SONHO", "SONHO_EnviaParaHistorico", False
    Exit Function
End Function

'BRUNODSANTOS CHVNG-7389
Public Function SONHO_RetornaNumAdmissoesHospitalares(ByVal Utente As String, ByVal seq_ute As String, ByVal n_proc_1 As String) As String
    Dim sSql As String
    Dim cmdRetornaNumAdmissoes As New ADODB.Command
    Dim pmt_ute As ADODB.Parameter
    Dim pmt_seq_ute As ADODB.Parameter
    Dim pmt_n_proc_1 As ADODB.Parameter
    Dim pmt_n_admissoes As ADODB.Parameter
    Dim rs As New ADODB.recordset
    
    On Error GoTo TrataErro
    
    With cmdRetornaNumAdmissoes
        .ActiveConnection = gConexaoSecundaria
        .CommandText = "devolve_num_admiss_hosp_sonho"
        .CommandType = ADODB.CommandTypeEnum.adCmdStoredProc
    End With
    
    Set pmt_ute = cmdRetornaNumAdmissoes.CreateParameter("p_n_utente", adVarChar, adParamInput, 10)
    Set pmt_seq_ute = cmdRetornaNumAdmissoes.CreateParameter("p_n_seq_utente", adVarChar, adParamInput, 10)
    Set pmt_n_proc_1 = cmdRetornaNumAdmissoes.CreateParameter("p_n_proc_1", adVarChar, adParamInput, 10)
    Set pmt_n_admissoes = cmdRetornaNumAdmissoes.CreateParameter("o_n_admissoes", adNumeric, adParamOutput, 10)
    
    cmdRetornaNumAdmissoes.Parameters.Append pmt_ute
    cmdRetornaNumAdmissoes.Parameters.Append pmt_seq_ute
    cmdRetornaNumAdmissoes.Parameters.Append pmt_n_proc_1
    cmdRetornaNumAdmissoes.Parameters.Append pmt_n_admissoes
    
    cmdRetornaNumAdmissoes.Parameters.item("p_n_utente").value = Utente
    cmdRetornaNumAdmissoes.Parameters.item("p_n_seq_utente").value = seq_ute
    cmdRetornaNumAdmissoes.Parameters.item("p_n_proc_1").value = n_proc_1
    
    cmdRetornaNumAdmissoes.Execute
    
    SONHO_RetornaNumAdmissoesHospitalares = CStr(cmdRetornaNumAdmissoes("o_n_admissoes").value)
    
    GoTo fim

Exit Function
TrataErro:
    SONHO_RetornaNumAdmissoesHospitalares = "-1"
    BG_LogFile_Erros Err.Number & " - " & Err.Description & "gconexao sec: " & gConexaoSecundaria, "Integracao_SONHO", "SONHO_RetornaNumAdmissoesHospitalares"
    Exit Function
    Resume Next
    

fim:
Set pmt_ute = Nothing
Set pmt_seq_ute = Nothing
Set pmt_n_proc_1 = Nothing
Set pmt_n_admissoes = Nothing
Exit Function

Resume Next

End Function


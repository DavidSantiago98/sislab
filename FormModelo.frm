VERSION 5.00
Begin VB.Form FormModelo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormModelo"
   ClientHeight    =   9675
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6765
   Icon            =   "FormModelo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9675
   ScaleWidth      =   6765
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcTextoLivre 
      Height          =   1215
      Left            =   240
      MultiLine       =   -1  'True
      TabIndex        =   45
      Top             =   2160
      Width           =   6255
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1530
      Left            =   240
      TabIndex        =   10
      Top             =   4320
      Width           =   6345
   End
   Begin VB.TextBox EcNumUltRes 
      Height          =   285
      Left            =   240
      TabIndex        =   9
      Top             =   3720
      Width           =   975
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   240
      TabIndex        =   25
      Top             =   6120
      Width           =   6315
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   31
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   30
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   29
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   28
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcInfCli 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2880
      TabIndex        =   24
      Top             =   9240
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.TextBox EcSexo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   23
      Top             =   9240
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.TextBox EcIdade 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2670
      TabIndex        =   22
      Top             =   8640
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.TextBox EcNome 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      TabIndex        =   21
      Top             =   8640
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.TextBox EcUrgencia 
      Enabled         =   0   'False
      Height          =   285
      Left            =   270
      TabIndex        =   20
      Top             =   8640
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.TextBox EcSituacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3480
      TabIndex        =   19
      Top             =   7320
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados da Requisi��o a Incluir"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1275
      Left            =   240
      TabIndex        =   18
      Top             =   840
      Width           =   6255
      Begin VB.CheckBox CkMembros 
         Caption         =   "Imprimir membros"
         Height          =   285
         Left            =   2460
         TabIndex        =   47
         Top             =   960
         Width           =   1635
      End
      Begin VB.CheckBox CkAntibioticos 
         Caption         =   "Imprimir Antibi�ticos"
         Height          =   255
         Left            =   150
         TabIndex        =   46
         Top             =   960
         Width           =   2055
      End
      Begin VB.CheckBox flg_inf_cli 
         Caption         =   "Informa��o Cl�nica"
         Height          =   285
         Left            =   4320
         TabIndex        =   8
         Top             =   630
         Width           =   1695
      End
      Begin VB.CheckBox flg_sexo 
         Caption         =   "Sexo do Utente"
         Height          =   285
         Left            =   2460
         TabIndex        =   7
         Top             =   630
         Width           =   2115
      End
      Begin VB.CheckBox flg_idade 
         Caption         =   "Idade do Utente"
         Height          =   285
         Left            =   2460
         TabIndex        =   6
         Top             =   270
         Width           =   1515
      End
      Begin VB.CheckBox flg_nome 
         Caption         =   "Nome do Utente"
         Height          =   285
         Left            =   4320
         TabIndex        =   5
         Top             =   240
         Width           =   1635
      End
      Begin VB.CheckBox flg_urgencia 
         Caption         =   "Urg�ncia na Requisi��o"
         Height          =   285
         Left            =   150
         TabIndex        =   4
         Top             =   630
         Width           =   2115
      End
      Begin VB.CheckBox flg_situacao 
         Caption         =   "Situa��o da Requisi��o"
         Height          =   285
         Left            =   150
         TabIndex        =   3
         Top             =   270
         Width           =   2115
      End
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   480
      TabIndex        =   14
      Top             =   9240
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox EcDescricao 
      Height          =   285
      Left            =   1440
      TabIndex        =   2
      Top             =   480
      Width           =   5025
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   1440
      TabIndex        =   1
      Top             =   120
      Width           =   1785
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   13
      Top             =   7320
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   360
      TabIndex        =   12
      Top             =   7320
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2040
      TabIndex        =   11
      Top             =   8040
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   240
      TabIndex        =   0
      Top             =   7920
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.Label Label18 
      Caption         =   "EcInfCli"
      Height          =   255
      Left            =   2880
      TabIndex        =   44
      Top             =   9000
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label17 
      Caption         =   "EcSexo"
      Height          =   255
      Left            =   1560
      TabIndex        =   43
      Top             =   9000
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label16 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   1920
      TabIndex        =   42
      Top             =   7800
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label15 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   120
      TabIndex        =   41
      Top             =   7680
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label14 
      Caption         =   "EcIdade"
      Height          =   255
      Left            =   2760
      TabIndex        =   40
      Top             =   8400
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label13 
      Caption         =   "EcNome"
      Height          =   255
      Left            =   1560
      TabIndex        =   39
      Top             =   8400
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label12 
      Caption         =   "EcUrgencia"
      Height          =   255
      Left            =   360
      TabIndex        =   38
      Top             =   8400
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label11 
      Caption         =   "EcSituacao"
      Height          =   255
      Left            =   3480
      TabIndex        =   37
      Top             =   7080
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   1800
      TabIndex        =   36
      Top             =   7080
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   120
      TabIndex        =   35
      Top             =   7080
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   240
      TabIndex        =   34
      Top             =   4080
      Width           =   855
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   3000
      TabIndex        =   33
      Top             =   4080
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "N�mero de resultados anteriores das an�lises do grupo a incluir:"
      Height          =   435
      Left            =   240
      TabIndex        =   32
      Top             =   3480
      Width           =   4635
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   -120
      TabIndex        =   17
      Top             =   9000
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o "
      Height          =   255
      Left            =   240
      TabIndex        =   16
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo "
      Height          =   225
      Left            =   240
      TabIndex        =   15
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "FormModelo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String


Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Private Sub EcCodigo_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub
Private Sub EcCodigo_LostFocus()

    EcCodigo.Text = UCase(EcCodigo.Text)
    
End Sub

Private Sub EcCodigo_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodigo)
    
End Sub


Private Sub EcDescricao_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDescricao_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDescricao)
    
End Sub


Private Sub EcLista_Click()
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub

Private Sub EcNumUltRes_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcNumUltRes)
    
End Sub


Private Sub flg_idade_Click()

If flg_idade.value = 1 Then
    EcIdade = 1
Else
    EcIdade = 0
End If

End Sub

Private Sub flg_inf_cli_Click()
    If flg_inf_cli.value = 1 Then
        EcInfCli = 1
    Else
        EcInfCli = 0
    End If
End Sub

Private Sub flg_nome_Click()
    If flg_nome.value = 1 Then
        EcNome = 1
    Else
        EcNome = 0
    End If
End Sub

Private Sub flg_sexo_Click()

If flg_sexo.value = 1 Then
    EcSexo = 1
Else
    EcSexo = 0
End If

End Sub

Private Sub flg_situacao_Click()
    If flg_situacao.value = 1 Then
        EcSituacao = 1
    Else
        EcSituacao = 0
    End If
End Sub

Private Sub flg_urgencia_Click()
    If flg_urgencia.value = 1 Then
        EcUrgencia = 1
    Else
        EcUrgencia = 0
    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Modelos Livres"
    Me.left = 540
    Me.top = 450
    Me.Width = 6900
    Me.Height = 7500 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_modelo"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 17
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_modelo"
    CamposBD(1) = "cod_modelo"
    CamposBD(2) = "descr_modelo"
    CamposBD(3) = "flg_situacao"
    CamposBD(4) = "flg_urgencia"
    CamposBD(5) = "flg_nome"
    CamposBD(6) = "flg_idade"
    CamposBD(7) = "flg_sexo"
    CamposBD(8) = "flg_inf_cli"
    CamposBD(9) = "num_ult_res"
    CamposBD(10) = "texto_livre"
    CamposBD(11) = "user_cri"
    CamposBD(12) = "dt_cri"
    CamposBD(13) = "user_act"
    CamposBD(14) = "dt_act"
    CamposBD(15) = "flg_antibioticos"
    CamposBD(16) = "flg_membros"
            
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcSituacao
    Set CamposEc(4) = EcUrgencia
    Set CamposEc(5) = EcNome
    Set CamposEc(6) = EcIdade
    Set CamposEc(7) = EcSexo
    Set CamposEc(8) = EcInfCli
    Set CamposEc(9) = EcNumUltRes
    Set CamposEc(10) = EcTextoLivre
    Set CamposEc(11) = EcUtilizadorCriacao
    Set CamposEc(12) = EcDataCriacao
    Set CamposEc(13) = EcUtilizadorAlteracao
    Set CamposEc(14) = EcDataAlteracao
    Set CamposEc(15) = CkAntibioticos
    Set CamposEc(16) = CkMembros
        
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo do Modelo"
    TextoCamposObrigatorios(2) = "Descri��o do Modelo"
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_modelo"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_modelo", "descr_modelo")
    NumEspacos = Array(22, 43)
        

End Sub


Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    gF_MODELOLIVRE = 1
    Inicializacoes
    BG_ParametrizaPermissoes_ADO Me.Name
    
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    gF_MODELOLIVRE = 0
    Set FormModelo = Nothing
End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    
    flg_situacao.value = vbGrayed
    flg_urgencia.value = vbGrayed
    flg_urgencia.value = vbGrayed
    flg_nome.value = vbGrayed
    flg_nome.value = vbGrayed
    flg_idade.value = vbGrayed
    flg_inf_cli.value = vbGrayed
    flg_sexo.value = vbGrayed
    CkAntibioticos.value = vbGrayed
    CkMembros.value = vbGrayed
    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    
End Sub

Sub DefTipoCampos()
       
    EcNumUltRes.Tag = adInteger
    EcNumUltRes.MaxLength = 4
       
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData

    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    CkAntibioticos.value = vbGrayed
    flg_situacao.value = vbGrayed
    flg_urgencia.value = vbGrayed
    flg_urgencia.value = vbGrayed
    flg_nome.value = vbGrayed
    flg_nome.value = vbGrayed
    flg_idade.value = vbGrayed
    flg_inf_cli.value = vbGrayed
    flg_sexo.value = vbGrayed
    CkAntibioticos.value = vbGrayed
    CkMembros.value = vbGrayed
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        If EcSituacao = "1" Then
            flg_situacao.value = 1
        Else
            flg_situacao.value = 0
        End If
        If EcUrgencia = "1" Then
            flg_urgencia.value = 1
        Else
            flg_urgencia.value = 0
        End If
        If EcNome = "1" Then
            flg_nome.value = 1
        Else
            flg_nome.value = 0
        End If
        If EcIdade = "1" Then
            flg_idade.value = 1
        Else
            flg_idade.value = 0
        End If
        If EcSexo = "1" Then
            flg_sexo.value = 1
        Else
            flg_sexo.value = 0
        End If
        If EcInfCli = "1" Then
            flg_inf_cli.value = 1
        Else
            flg_inf_cli.value = 0
        End If
    End If
    
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_modelo ASC, descr_modelo ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos
        EcLista.Clear
        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        EcLista.ListIndex = EcLista.ListIndex - 1
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        EcLista.ListIndex = EcLista.ListIndex + 1
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoInserir()

    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()

    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_modelo") + 1
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
        
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub


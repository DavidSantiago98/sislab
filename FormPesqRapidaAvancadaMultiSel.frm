VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form FormPesqRapidaAvancadaMultiSel 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormPesqRapidaAvancadaMultiSel"
   ClientHeight    =   6975
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6690
   Icon            =   "FormPesqRapidaAvancadaMultiSel.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6840.29
   ScaleMode       =   0  'User
   ScaleWidth      =   6690
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox CkTodos 
      Appearance      =   0  'Flat
      Caption         =   "Seleccionar Tudo"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   185
      TabIndex        =   4
      Top             =   1800
      Visible         =   0   'False
      Width           =   6255
   End
   Begin VB.TextBox EcPesquisa 
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   6375
   End
   Begin VB.CommandButton CmdOK 
      Caption         =   "&OK"
      Height          =   375
      Left            =   1320
      TabIndex        =   2
      Top             =   6360
      Width           =   1455
   End
   Begin VB.CommandButton CmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   3360
      TabIndex        =   1
      Top             =   6360
      Width           =   1455
   End
   Begin MSComctlLib.ListView LwPesquisa 
      Height          =   4122
      Left            =   120
      TabIndex        =   3
      Top             =   2053
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   7276
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Label lbDescrOriginal 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Descri��o da an�lise da credencial:"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   720
      Width           =   6375
   End
   Begin VB.Label lbDescrMCDT 
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000009&
      Height          =   735
      Left            =   120
      TabIndex        =   5
      Top             =   966
      Width           =   6375
   End
End
Attribute VB_Name = "FormPesqRapidaAvancadaMultiSel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 28/03/2003
' T�cnico Paulo Costa

Private ChavesPesquisa() As String
Private CamposEcra() As String
Private Tamanhos() As String
Private CamposRetorno As ClassPesqResultados
Private Headers() As String
Private NumeroChavesPesquisa As Integer
Private NumeroCamposEcra As Integer
Private ClausulaWhere As String
Private ClausulaFrom As String
Private MyDBConnection As ADODB.Connection
Private CampoPesquisa As String
Private sql As String
Private rs As New ADODB.recordset

Dim Inicializa As Boolean

Dim local_order_by As String

Dim numPosVector As Integer
Dim IndiceFrase(1000) As Integer
Dim IndiceLwFrase() As Variant
Dim CodFraseSel(1000) As String

'edgar.parada Glintt-HS-21197 16.05.2019 parametro descrMcdt
Public Function InicializaFormPesqAvancada(gConex As ADODB.Connection, _
                                           TChavesPesq() As String, _
                                           TCamposEcra() As String, _
                                           TCamposRetorno As ClassPesqResultados, _
                                           TTamanhos() As Long, _
                                           THeaders() As String, _
                                           TClausulaWhere As String, _
                                           TClausulaFrom As String, _
                                           TPesquisaInicial As String, _
                                           TCampoPesquisa As String, _
                                           order_by As String, _
                                           Optional TTitulo As String, _
                                           Optional descrMcdt As String, _
                                           Optional p1Importado As Boolean) As Boolean
    
    Dim i As Integer
    Dim n As Integer
    
    'edgar.parada Glintt-HS-21197 16.05.2019
    If descrMcdt <> "" And p1Importado Then
      lbDescrMCDT.caption = descrMcdt
    Else
       lbDescrMCDT.Visible = False
       lbDescrOriginal.Visible = False
       Me.Height = "6435"
       CkTodos.top = "720"
       LwPesquisa.top = "1080"
       CmdCancelar.top = "5400"
       CmdOK.top = "5400"
    End If
    '
    
    local_order_by = " " & order_by & " "
    numPosVector = 0
    ' valor por defeito
    InicializaFormPesqAvancada = True
    Inicializa = True
    Me.caption = TTitulo
    ' copiar os campos chave
    n = UBound(TChavesPesq)
    NumeroChavesPesquisa = n
    ReDim ChavesPesquisa(1 To n)
    For i = 1 To n
        ChavesPesquisa(i) = TChavesPesq(i)
    Next i
    ' copiar os campos de ecra
    n = UBound(TCamposEcra)
    NumeroCamposEcra = n
    ReDim CamposEcra(1 To n)
    For i = 1 To n
        CamposEcra(i) = TCamposEcra(i)
    Next i
    ' copiar os campos de retorno
    'n = UBound(TCamposRetorno)
    Set CamposRetorno = TCamposRetorno
    TCamposRetorno.PesquisaCancelada True

'    ReDim CamposRetorno(1 To n)
'    For i = 1 To n
'        CamposRetorno(i) = TCamposRetorno(i)
'    Next i
    ' copiar os campos de Tamanho de colunas
    n = UBound(TTamanhos)
    If n <> NumeroCamposEcra Then
        MsgBox "O n�mero de elementos do vector de Tamanhos tem de ser igual ao do de Campos de Ecra"
        Unload Me
        Exit Function
    End If
    ReDim Tamanhos(1 To n)
    For i = 1 To n
        Tamanhos(i) = TTamanhos(i)
    Next i
    ' copiar o nome da tabela
    If TClausulaFrom = "" Then
        MsgBox "Tem de Indicar o nome da tabela para pesquisa rapida"
        Unload Me
        Exit Function
    End If
    ClausulaFrom = TClausulaFrom
    ClausulaWhere = TClausulaWhere
    Set MyDBConnection = gConex
    CampoPesquisa = TCampoPesquisa
    EcPesquisa = TPesquisaInicial
    ' copiar os headers
    n = UBound(THeaders)
    If n <> NumeroCamposEcra Then
        MsgBox "O n�mero de elementos do vector de Headers tem de ser igual ao do de Campos de Ecra"
        Unload Me
        Exit Function
    End If
    
    ReDim Headers(1 To n)
    For i = 1 To n
        Headers(i) = IIf(THeaders(i) = "", TCamposEcra(i), THeaders(i))
    Next i
    Inicializacoes
    If InStr(1, TClausulaWhere, "SELECT") < 1 Or InStr(1, TClausulaWhere, "SELECT") > 5 Then
        Call ConstroiSQLPesquisaRapida(order_by)
    Else
        sql = TClausulaWhere
    End If
    LimpaAliases
    If PreencheLista = False Then
        InicializaFormPesqAvancada = False
        Unload Me
    End If
    Inicializa = False
    'NELSONPSILVA UALIA-856 31.01.2019
    If gPassaParams.id = "CarregaCativacao" Then
        CkTodos.Enabled = False
    End If
End Function

Sub LimpaAliases()

    ' Esta sub retira os aliases tipo fa_movi_fact.t_doente -> t_doente do array camposecra.
    
    Dim i As Integer
    Dim posicao As Integer
    For i = 1 To NumeroCamposEcra
        posicao = InStr(1, CamposEcra(i), ".") + 1
        If posicao > 0 Then
            CamposEcra(i) = Mid(CamposEcra(i), posicao)
        End If
    Next i
    For i = 1 To NumeroChavesPesquisa
        posicao = InStr(1, ChavesPesquisa(i), ".") + 1
        If posicao > 0 Then
            ChavesPesquisa(i) = Mid(ChavesPesquisa(i), posicao)
        End If
    Next i

End Sub

Private Sub ConstroiSQLPesquisaRapida(order_by As String)
    
    Dim TmpSQL As String
    Dim i As Integer, n As Integer
    Dim CampoDuplicado As Boolean
    
    TmpSQL = "SELECT "
    ' acrescentar os campos de ecra
    For i = 1 To NumeroCamposEcra
        CampoDuplicado = False
        For n = 1 To NumeroChavesPesquisa
            If UCase(Trim(CamposEcra(i))) = UCase(Trim(ChavesPesquisa(n))) Then
                CampoDuplicado = True
                Exit For
            End If
        Next n
        If Not CampoDuplicado Then
            TmpSQL = TmpSQL & IIf(TmpSQL <> "SELECT ", ",", "") & CamposEcra(i)
        End If
    Next i
    ' selecccionar os campos chave
    For i = 1 To NumeroChavesPesquisa
        TmpSQL = TmpSQL & IIf(TmpSQL <> "SELECT ", ",", "") & ChavesPesquisa(i)
    Next i
    ' acrescentar as tabelas
    TmpSQL = TmpSQL & " FROM " & ClausulaFrom & " "
    If Trim(ClausulaWhere) <> "" Then
        TmpSQL = TmpSQL & " WHERE " & ClausulaWhere
    End If

    sql = TmpSQL

End Sub
Private Function PreencheLista() As Boolean
    
    Dim TmpSQL As String
    Dim i As Integer
    Dim Lt As ListItem
    Dim LwCol As ListItems
    Dim ChaveItem As String
    Dim rs As New ADODB.recordset
    Dim nCount As Long
    Dim k As Integer
    
    On Error GoTo Trata_Erro
    
    ' valor de retorno por defeito
    PreencheLista = True
    If Trim(ClausulaWhere) = "" And EcPesquisa <> "" Then
        TmpSQL = " WHERE "
    ElseIf EcPesquisa <> "" Then
        TmpSQL = TmpSQL & " AND "
    End If
    
    
    If EcPesquisa <> "" Then
        EcPesquisa = Replace(EcPesquisa, "*", "%")
        EcPesquisa = Replace(EcPesquisa, "?", "_")
        If (gPesquisaDentroCampo) Then
            TmpSQL = TmpSQL & "UPPER(" & CampoPesquisa & ") LIKE '%" & UCase(Trim(EcPesquisa)) & "%'"
        Else
            TmpSQL = TmpSQL & "UPPER(" & CampoPesquisa & ") LIKE '" & UCase(Trim(EcPesquisa)) & "%'"
        End If
    End If
    
    If InStr(1, UCase(ClausulaWhere), "SELECT") > 1 And InStr(1, UCase(ClausulaWhere), "WHERE") > 1 Then
        TmpSQL = Replace(UCase(sql), " WHERE ", " WHERE  1=1 " & TmpSQL & " AND ")
    Else
    
        TmpSQL = sql & TmpSQL
    End If
    
    With rs
        '.CacheSize = 100
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
    End With
    
    TmpSQL = TmpSQL & " " & local_order_by
    
    BL_MudaCursorRato vbHourglass, Me
    If gModoDebug = mediSim Then BG_LogFile_Erros TmpSQL
    rs.Open TmpSQL, MyDBConnection
    LwPesquisa.ListItems.Clear
    
    If rs.EOF Then
       rs.Close
       Set rs = Nothing
        PreencheLista = False
        BL_MudaCursorRato vbNormal, Me
       Exit Function
    End If
    
    Set LwCol = LwPesquisa.ListItems
    nCount = 0
    Do While Not rs.EOF 'And NCount < 500
        nCount = nCount + 1
        ChaveItem = ""
        For i = 1 To NumeroChavesPesquisa
            ChaveItem = ChaveItem & "<KEY>" & rs(ChavesPesquisa(i)) & "</KEY>"
        Next i
        
        For i = 1 To NumeroCamposEcra
            If i = 1 Then
                Set Lt = LwCol.Add(, ChaveItem, Trim(BL_HandleNull(rs(CamposEcra(i)), "")))
            Else
                Lt.SubItems(i - 1) = Trim(BL_HandleNull(rs(CamposEcra(i)), ""))
            End If
        Next i
        For k = 1 To numPosVector
            If CodFraseSel(k) = Mid(Lt.key, 6) Then
                IndiceFrase(k) = Lt.Index
                Lt.checked = True
            End If
        Next k
        rs.MoveNext
    Loop
    rs.Close
    Set rs = Nothing
    
    BL_MudaCursorRato vbNormal, Me
        
    Exit Function
    
Trata_Erro:
   
    BG_LogFile_Erros "ERRO NA FormPesquisaRapidaAvancada : " & Err.Number & "-" & Err.Description

    If Not rs Is Nothing Then
        Set rs = Nothing
    End If
    
    PreencheLista = False
    BL_MudaCursorRato vbNormal, Me
    Exit Function
    Resume Next
End Function

Private Sub Inicializacoes()
    
    InicializaListView
    With rs
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
    End With

End Sub

Private Sub InicializaListView()
    
    Dim i As Integer
    LwPesquisa.View = lvwReport
    LwPesquisa.LabelEdit = lvwManual
    For i = 1 To NumeroCamposEcra
        LwPesquisa.ColumnHeaders.Add , , Headers(i), Tamanhos(i)
    Next i
    LwPesquisa.MultiSelect = False

End Sub

Private Sub CkTodos_Click()
    Dim i As Long
    For i = 1 To LwPesquisa.ListItems.Count
        If CkTodos.value = vbChecked Then
            LwPesquisa.ListItems(i).checked = True
        Else
            LwPesquisa.ListItems(i).checked = False
        End If
        LwPesquisa.ListItems(i).Selected = True
        LwPesquisa_ItemCheck LwPesquisa.ListItems(i)
    Next
End Sub

Private Sub CmdCancelar_Click()
    If LwPesquisa.ListItems.Count > 0 Then
        CamposRetorno.PesquisaCancelada True
    End If
    Unload Me

End Sub

Private Sub cmdOK_Click()
    
    CamposRetorno.PesquisaCancelada False
'    RetornaChaves
    Unload Me

End Sub

Private Sub EcPesquisa_Change()
    
    If Not Inicializa Then
        PreencheLista
    End If

End Sub

Private Sub EcPesquisa_GotFocus()
    
    'cmdOK.Enabled = False

End Sub

Private Sub Form_Activate()
    
    CmdOK.Enabled = False

End Sub

Private Sub LwPesquisa_Click()
    Dim Str1, Str2 As String
    Dim i, pos1, pos2 As Integer
    
'    CmdOK.Enabled = True
'
'
'    If LwPesquisa.SelectedItem.Checked = True Then
'        NumPosVector = NumPosVector + 1
'        CamposRetorno.InicializaResultados NumPosVector
'        'Retorna Chaves para os resultados ficarem na ordem de marca��o
'        Str1 = LwPesquisa.SelectedItem.Key
'        For i = 1 To NumeroChavesPesquisa
'            Pos1 = InStr(1, Str1, "<KEY>") + 5
'            Str2 = Mid(Str1, Pos1)
'            Pos2 = InStr(1, Str2, "<KEY>")
'
'            If Pos2 > 0 Then
'                Str1 = Mid(Str2, Pos2)
'                CamposRetorno.ResPesquisaLet NumPosVector, Mid(Str2, 1, Pos2 - 1)
'            Else
'                CamposRetorno.ResPesquisaLet NumPosVector, Str2
'            End If
'        Next i
'
'    ElseIf NumPosVector > 0 And LwPesquisa.SelectedItem.Checked = False Then
'        NumPosVector = NumPosVector - 1
'        If (NumPosVector = 0) Then
'            NumPosVector = 1
'        End If
'
'        CamposRetorno.InicializaResultados NumPosVector
'    End If

End Sub

Private Sub LwPesquisa_DblClick()
    
'    CamposRetorno.PesquisaCancelada False
'    If Not (LwPesquisa.SelectedItem Is Nothing) Then
'        RetornaChaves
'        Unload Me
'    End If

End Sub

Private Sub RetornaChaves()
    
    Dim i As Long, j As Long, k As Long
    Dim Str1 As String, Str2 As String
    Dim NumeroElementos As Long
    Dim PosicaoSelec As Long
    Dim ColItems As ListItems
    Dim pos1 As Integer, pos2 As Integer
    
    Set ColItems = LwPesquisa.ListItems
    NumeroElementos = ColItems.Count
    k = 0
    For j = 1 To NumeroElementos
        PosicaoSelec = 0
        If LwPesquisa.ListItems(j).checked = True Then
            PosicaoSelec = j
            Set ColItems = LwPesquisa.ListItems
            k = k + 1
        End If
    
        If PosicaoSelec = 0 Then
            Set ColItems = Nothing
        Else
            Str1 = ColItems(PosicaoSelec).key
            For i = 1 To NumeroChavesPesquisa
                pos1 = InStr(1, Str1, "<KEY>") + 5
                Str2 = Mid(Str1, pos1)
                pos2 = InStr(1, Str2, "<KEY>")
                
                If pos2 > 0 Then
                    Str1 = Mid(Str2, pos2)
                    CamposRetorno.ResPesquisaLet k, Mid(Str2, 1, pos2 - 1)
                Else
                    CamposRetorno.ResPesquisaLet k, Str2
                End If
            Next i
        End If
    Next j
    i = 0
    PosicaoSelec = 0
    Str1 = ""
    Str2 = ""
    Set ColItems = Nothing
    NumeroElementos = 0

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    LwPesquisa.ListItems.Clear
    Erase ChavesPesquisa
    Erase CamposEcra
    Erase Tamanhos
    Set CamposRetorno = Nothing
    Erase Headers
    ClausulaFrom = ""
    ClausulaWhere = ""
    NumeroCamposEcra = 0
    NumeroChavesPesquisa = 0
    CampoPesquisa = ""
    sql = ""
    
    Set FormPesqRapidaAvancadaMultiSel = Nothing

End Sub

Private Sub LwPesquisa_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    
    Dim Str1, Str2 As String
    Dim i, j, pos1, pos2, nselect As Integer
    
    CmdOK.Enabled = True
    'NELSONPSILVA UALIA-856 31.01.2019
    If gPassaParams.id = "CarregaCativacao" Then
        nselect = 0
        For i = 1 To LwPesquisa.ListItems.Count
            If LwPesquisa.ListItems.Item(i).checked = True Then
                nselect = nselect + 1
            End If
        Next i

        If nselect > 1 Then
            LwPesquisa.SelectedItem.checked = False
            Exit Sub
        End If
    End If
    '
    If LwPesquisa.SelectedItem.checked = True Then
        numPosVector = numPosVector + 1
        IndiceFrase(numPosVector) = LwPesquisa.SelectedItem.Index
        CamposRetorno.InicializaResultados numPosVector
        'Retorna Chaves para os resultados ficarem na ordem de marca��o
        Str1 = LwPesquisa.SelectedItem.key
        For i = 1 To NumeroChavesPesquisa
            pos1 = InStr(1, Str1, "<KEY>") + 5
            Str2 = Replace(Mid(Str1, pos1), "</KEY>", "")
            pos2 = InStr(1, Str2, "<KEY>")
    
            If pos2 > 0 Then
                Str1 = Mid(Str2, pos2)
                CamposRetorno.ResPesquisaLet numPosVector, Mid(Str2, 1, pos2 - 1)
            Else
                CodFraseSel(numPosVector) = Str2
                CamposRetorno.ResPesquisaLet numPosVector, Str2
            End If
        Next i

    ElseIf numPosVector > 0 And LwPesquisa.SelectedItem.checked = False Then
        numPosVector = numPosVector - 1
'        If (NumPosVector = 0) Then
'            NumPosVector = 1
'        End If
        CamposRetorno.InicializaResultados numPosVector
        For i = 1 To numPosVector + 1
            If InStr(1, LwPesquisa.SelectedItem.key, "<KEY>" & CodFraseSel(i) & "</KEY>") > 0 Then
                'refaz estrutura
                For j = i To numPosVector
                    CamposRetorno.ResPesquisaLet j, CodFraseSel(j + 1)
                    IndiceFrase(j) = IndiceFrase(j + 1)
                    CodFraseSel(j) = CodFraseSel(j + 1)
      
                Next j
                IndiceFrase(numPosVector + 1) = 0
                CodFraseSel(numPosVector + 1) = ""
                GoTo fim
            End If
        Next i
fim:
        
    End If
End Sub

Private Sub LwPesquisa_KeyDown(KeyCode As Integer, Shift As Integer)
    
'    If KeyCode = 13 Then 'Enter
'        Call LwPesquisa_DblClick
'    End If

End Sub

Private Sub LwPesquisa_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Set LwPesquisa.SelectedItem = LwPesquisa.HitTest(X, Y)

End Sub



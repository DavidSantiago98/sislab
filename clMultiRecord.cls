VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clMultiRecord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Ultima Actualiza��o: 04-02-2000 por carlos sousa

' Eventos
Public Event AntesMudarLinhaActiva(PosicaoListaControlos As Integer, PosicaoListaRegistos As Long, TotalRegistos As Long)
' este evento dispara quando o utilizador muda de linha activa
Public Event DepoisMudarLinhaActiva(PosicaoListaControlos As Integer, PosicaoListaRegistos As Long, TotalRegistos As Long)
' este evento dispara depois do utilizador mudar de linha activa
Public Event DepoisCriarRegistoNovo(PosicaoListaRegistos As Long, FlagGravaRegisto As Boolean, TotalRegistos As Long)
' este evento dispara quando � inserido um novo registo novo registo na coleccao
Public Event DepoisActualizarRegisto(PosicaoListaRegistos As Long, FlagGravaRegisto As Boolean, TotalRegistos As Long)
' este evento dispara quando � actualizado um registo na coleccao
' propriedades publicas
Public Event AntesApagarRegisto(PosicaoListaRegistos As Long, TotalRegistos As Long)
' este evento dispara antes de ser apagado um registo
Public Event DepoisApagarRegisto(PosicaoListaRegistos As Long, TotalRegistos As Long)
' este evento dispara depois de ser apagado um registo
Public PodeCriarNovasLinhas As Boolean


'propriedades privadas
Public ListaRegistos  As New Collection ' registos
Private ListaControlos ' controlos de ecra
Private clRegistos As Object
Private NumeroRegistosLista As Long ' numero total de registos da lista
Private PosicaoListaControlos As Integer ' posicao na lista de controlos de ecra
Private PosicaoListaRegistos As Long ' posicao na lista que guarda os registos em memoria
Private NumeroLinhasEcra As Integer ' numero de linhas visiveis no ecra
Private NumeroLinhasVisiveis As Integer

' constantes
' tipos de preenchimento
Private Const PREENCHE_BAIXO_CIMA = 1
Private Const PREENCHE_CIMA_BAIXO = 2



Public Function TotalRegistos() As Long
    TotalRegistos = NumeroRegistosLista
End Function
Public Function LinhaEcraActual() As Integer
    LinhaEcraActual = PosicaoListaControlos
End Function
Public Function RegistoActual() As Long
    RegistoActual = PosicaoListaRegistos
End Function
Public Sub VaiParaUltimoRegisto()
    Dim Deslocacao
    On Error GoTo HandleError
    PosicaoListaControlos = NumeroLinhasVisiveis
    If PosicaoListaControlos <= NumeroLinhasEcra And NumeroRegistosLista <= NumeroLinhasEcra Then
        PosicaoListaRegistos = 0
        PreencheRegistos PREENCHE_CIMA_BAIXO
    Else
        PosicaoListaRegistos = NumeroRegistosLista - NumeroLinhasEcra + 1
        PreencheRegistos PREENCHE_BAIXO_CIMA
    End If
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.VaiParaUltimoRegisto"

End Sub
Public Sub LimpaRegistos()
    Dim i As Long
    On Error GoTo HandleError
    i = ListaRegistos.Count
    
    While i > 0
        ListaRegistos.Remove i
        i = i - 1
    Wend
    PosicaoListaRegistos = 0
    NumeroRegistosLista = 0
    PosicaoListaControlos = 0
    i = NumeroLinhasVisiveis
    While i > -1
        ListaControlos.LimpaLinha (i)
        EscondeLinha (i)
        i = i - 1
    Wend
    MostraRegistos
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.LimpaRegistos"
    
End Sub

Public Sub InicializaListaControlos(ListaControlosRegisto, NumeroRegistosVisiveisEcra As Integer, ObjRegisto As Object, DeslocacaoVertical As Long)
    'copiar o objecto com a lista de registos
    'copiar o objecto com a lista de registos
    Dim i As Integer
    On Error GoTo HandleError
    Set ListaControlos = ListaControlosRegisto
    NumeroRegistosLista = ListaRegistos.Count
    PosicaoListaControlos = 0
    PosicaoListaRegistos = 1
    NumeroLinhasEcra = NumeroRegistosVisiveisEcra - 1
    NumeroLinhasVisiveis = NumeroLinhasEcra
    PodeCriarNovasLinhas = True
    Set clRegistos = ObjRegisto
    
    ' vamos esconder as linhas todas
    
    For i = 1 To NumeroLinhasEcra
        ListaControlos.CarregaControlo i, DeslocacaoVertical
    Next i
    i = NumeroLinhasVisiveis
    While i >= 0
        EscondeLinha i
        i = i - 1
    Wend
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.InicializaControlos"
    
    
End Sub
Public Sub GravaRegistoActual(Indice, Optional FlagGravaRegisto)
    Dim GravaRegisto As Boolean
    On Error GoTo HandleError
    If IsMissing(FlagGravaRegisto) Then
        GravaRegisto = False
    Else
        GravaRegisto = FlagGravaRegisto
    End If
    If PosicaoListaRegistos <= NumeroRegistosLista Then
        ListaRegistos.Item(PosicaoListaRegistos).AlteraRegisto ListaControlos, Indice
        RaiseEvent DepoisActualizarRegisto(PosicaoListaRegistos, GravaRegisto, NumeroRegistosLista)
    Else
        ListaRegistos.Add clRegistos.CriaInstanciaClasse(ListaControlos, Indice)
        NumeroRegistosLista = NumeroRegistosLista + 1
        RaiseEvent DepoisCriarRegistoNovo(PosicaoListaRegistos, GravaRegisto, NumeroRegistosLista)
    End If
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.GravaRegistoActual"

End Sub
Public Sub Terminate()
    Dim i As Integer
    On Error GoTo HandleError
    For i = 1 To NumeroLinhasVisiveis
        ListaControlos.DescarregaControlo i
    Next i
    
    Set ListaControlos = Nothing
    Set ListaRegistos = Nothing
    Set clRegistos = Nothing
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.Terminate"
End Sub
Public Sub MudaLinha(PrimeiroCampo, Indice)
    Dim TmpPosicaoControlo As Integer
    Dim TmpIndice As Integer
    Dim MostraNovaLinha As Boolean
    Dim Campo As Object
    On Error GoTo HandleError
    ' se for um registo novo
    If PosicaoListaRegistos > NumeroRegistosLista Then
        MudaLinhaActiva Campo '(PrimeiroCampo(PosicaoListaControlos))
        Exit Sub
    End If
    ' � preciso puxar uma linha para cima
    MostraNovaLinha = False
    TmpIndice = Indice
    ListaControlos.DesactivaLinha PosicaoListaControlos
    If TmpIndice = NumeroLinhasEcra Or PosicaoListaRegistos = NumeroRegistosLista Then
        TmpPosicaoControlo = PosicaoListaControlos
        PosicaoListaControlos = 0
        If PosicaoListaRegistos = NumeroRegistosLista And TmpIndice < NumeroLinhasEcra Then
            ' se estivermos no ultimo registo e n�o for a ultima linha do ecra
            PosicaoListaRegistos = PosicaoListaRegistos - TmpPosicaoControlo
            MostraNovaLinha = True
            TmpIndice = TmpIndice + 1
        Else
            PosicaoListaRegistos = PosicaoListaRegistos - TmpPosicaoControlo + 1 ' vamos avan�ar um registo
        End If
        PreencheRegistos PREENCHE_BAIXO_CIMA
        
        PosicaoListaControlos = TmpPosicaoControlo
        PosicaoListaRegistos = PosicaoListaRegistos + TmpPosicaoControlo
        If MostraNovaLinha Then
            PosicaoListaControlos = PosicaoListaControlos + 1
            PosicaoListaRegistos = PosicaoListaRegistos + 1
        End If

        
        
        If PosicaoListaRegistos > NumeroRegistosLista Or MostraNovaLinha Then
            ListaControlos.LimpaLinha PosicaoListaControlos
            ListaControlos.PreencheValoresDefeito PosicaoListaControlos
            MostraLinha TmpIndice
            MostraNovaLinha = False
        End If
    Else
        If PosicaoListaRegistos <= NumeroRegistosLista Then
            TmpIndice = TmpIndice + 1
            PosicaoListaRegistos = PosicaoListaRegistos + 1
            PosicaoListaControlos = PosicaoListaControlos + 1
        End If
    End If
    Set Campo = PrimeiroCampo(TmpIndice)
    MudaLinhaActiva Campo '(PrimeiroCampo(TmpIndice))
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.MudaLinha"
    
End Sub


Public Sub ApagaRegistoActual()
    Dim TmpPos As Integer
    On Error GoTo HandleError
    If NumeroRegistosLista = 0 Or PosicaoListaRegistos > NumeroRegistosLista Then
        Beep
        Exit Sub
    End If
    RaiseEvent AntesApagarRegisto(PosicaoListaRegistos, NumeroRegistosLista)
    ListaRegistos.Remove PosicaoListaRegistos
    RaiseEvent DepoisApagarRegisto(PosicaoListaRegistos, NumeroRegistosLista)
    If PosicaoListaRegistos = NumeroRegistosLista Then ' se for o ultimo registo
        PosicaoListaRegistos = PosicaoListaRegistos - 1
        If PosicaoListaRegistos <= NumeroLinhasEcra Then
            PosicaoListaControlos = PosicaoListaControlos - 1
        End If
        If PosicaoListaControlos < 0 Then
            PosicaoListaControlos = 0
        End If
    End If
    NumeroRegistosLista = NumeroRegistosLista - 1
    TmpPos = PosicaoListaControlos
    PosicaoListaControlos = 0
    PosicaoListaRegistos = PosicaoListaRegistos - TmpPos
    PreencheRegistos PREENCHE_CIMA_BAIXO
    PosicaoListaControlos = TmpPos
    PosicaoListaRegistos = PosicaoListaRegistos + TmpPos
    If PosicaoListaRegistos = 0 And PodeCriarNovasLinhas Then
        ListaControlos.LimpaLinha 0
        ListaControlos.PreencheValoresDefeito 0
        MostraLinha 0
        PosicaoListaRegistos = 1
    End If
    If NumeroRegistosLista > 0 Then
        RaiseEvent AntesMudarLinhaActiva(PosicaoListaControlos, PosicaoListaRegistos, NumeroRegistosLista)
        ListaRegistos.Item(PosicaoListaRegistos).PreencheRegisto ListaControlos, PosicaoListaControlos
        RaiseEvent DepoisMudarLinhaActiva(PosicaoListaControlos, PosicaoListaRegistos, NumeroRegistosLista)
    End If
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.ApagaRegistoActual"

End Sub
Public Function ProcessaDeslocacao(TeclaPremida As Integer, CampoActivo As Object, ObjForm As Form) As Boolean
    Dim RealizaValidacaoCampos As Boolean
    Dim SaltoRegistos As Integer
    On Error GoTo HandleError
    ProcessaDeslocacao = False
    If TeclaPremida <> 38 And TeclaPremida <> 40 Then
        Exit Function
    End If
    
    ProcessaDeslocacao = True
    Select Case TeclaPremida
        Case 40 ' cursor para baixo
            ListaControlos.DesactivaLinha (PosicaoListaControlos)
            If PosicaoListaControlos < NumeroLinhasEcra And PosicaoListaControlos <= NumeroLinhasVisiveis Then
            ' se n�o for a ultima linha visivel
                If PosicaoListaRegistos > NumeroRegistosLista Then
                    Beep
                    ListaControlos.ActivaLinha PosicaoListaControlos
                    Exit Function
                End If
                PosicaoListaControlos = PosicaoListaControlos + 1
                PosicaoListaRegistos = PosicaoListaRegistos + 1
                If PosicaoListaControlos > NumeroLinhasVisiveis Then
                    If PosicaoListaRegistos > NumeroRegistosLista Then
                        ListaControlos.LimpaLinha PosicaoListaControlos
                        ListaControlos.PreencheValoresDefeito PosicaoListaControlos
                        MostraLinha PosicaoListaControlos
                    Else
                        MostraLinha PosicaoListaControlos
                    End If
                End If
            Else
                'vamos decrementar as variaveis globais para estarem na posi��o certa para a preencheRegistos
                If PosicaoListaRegistos > NumeroRegistosLista Then
                    Beep
                    MudaLinhaActiva CampoActivo(PosicaoListaControlos)
                    Exit Function
                End If
                SaltoRegistos = PosicaoListaControlos - 1
                PosicaoListaRegistos = PosicaoListaRegistos - SaltoRegistos
                PosicaoListaControlos = 0
                PreencheRegistos PREENCHE_BAIXO_CIMA
                ' vamos repor as posicoes
                PosicaoListaRegistos = PosicaoListaRegistos + SaltoRegistos + 1
                PosicaoListaControlos = SaltoRegistos + 1
                ' temos de detectar se ultrapassamos o fim da lista
                If PosicaoListaRegistos > NumeroRegistosLista Then
                    'PosicaoListaRegistos = NumeroRegistosLista
                    If PodeCriarNovasLinhas Then ' se for possivel acrescentar novos registos
                        ListaControlos.LimpaLinha (PosicaoListaControlos)
                        ListaControlos.PreencheValoresDefeito PosicaoListaControlos
                        MostraLinha PosicaoListaControlos
                        'ListaControlos.MostraLinha (PosicaoListaControlos)
                    Else ' sen�o decrementa e a linha n�o aparece
                        PosicaoListaControlos = PosicaoListaControlos - 1
                    End If
                End If
                
            End If
            MudaLinhaActiva CampoActivo(PosicaoListaControlos)
        Case 38 ' cursor para cima
            If PosicaoListaControlos > 0 Then
                ListaControlos.DesactivaLinha (PosicaoListaControlos)
                PosicaoListaControlos = PosicaoListaControlos - 1
                If PosicaoListaRegistos > NumeroRegistosLista Then ' � uma nova linha
                    EscondeLinha PosicaoListaControlos + 1, True
                End If
                PosicaoListaRegistos = PosicaoListaRegistos - 1
            Else
                'vamos decrementar as variaveis globais para estarem na posi��o certa para a preencheRegistos
                PosicaoListaRegistos = PosicaoListaRegistos - 1
                If PosicaoListaRegistos = 0 Then
                    PosicaoListaRegistos = 1
                    'MudaLinhaActiva CampoActivo(PosicaoListaControlos)
                    Beep
                    Exit Function
                End If
                PreencheRegistos PREENCHE_CIMA_BAIXO
            End If
            
            MudaLinhaActiva CampoActivo(PosicaoListaControlos)
    End Select
    Exit Function
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.ProcessaDeslocacao"
End Function

Private Sub PreencheRegistos(TipoPreenchimento As Integer)
    Dim i As Integer
    Dim iPosIniReg As Long
    Dim MaiorLinhaVisivel As Integer
    On Error GoTo HandleError
    If PosicaoListaRegistos = 0 Then
        Exit Sub
    End If
    If TipoPreenchimento = PREENCHE_CIMA_BAIXO Then
        
        For i = 0 To NumeroLinhasEcra
            If NumeroRegistosLista < (PosicaoListaRegistos + i) Then 'se n�o h� mais registos
                If i <= NumeroLinhasVisiveis Then
                    EscondeLinha i, True
                End If
            Else
                RaiseEvent AntesMudarLinhaActiva(i, PosicaoListaRegistos, NumeroRegistosLista)
                ListaRegistos.Item(PosicaoListaRegistos + i).PreencheRegisto ListaControlos, i
                RaiseEvent DepoisMudarLinhaActiva(i, PosicaoListaRegistos, NumeroRegistosLista)
                If NumeroLinhasVisiveis < i Then
                    MostraLinha i
                End If
            End If
        Next i
    Else
        i = NumeroLinhasEcra
        MaiorLinhaVisivel = -1
        iPosIniReg = PosicaoListaRegistos
        Do While i >= 0
            If NumeroRegistosLista < (PosicaoListaRegistos + i) And NumeroRegistosLista < NumeroLinhasEcra + 1 Then
                EscondeLinha i, True
            ElseIf NumeroRegistosLista >= (PosicaoListaRegistos + i) Then
                RaiseEvent AntesMudarLinhaActiva(i, PosicaoListaRegistos, NumeroRegistosLista)
                ListaRegistos(iPosIniReg + i).PreencheRegisto ListaControlos, i
                RaiseEvent DepoisMudarLinhaActiva(i, PosicaoListaRegistos, NumeroRegistosLista)
                ListaControlos.MostraLinha i
                If MaiorLinhaVisivel = -1 Then ' se ainda n�o tiver sido alterada
                    MaiorLinhaVisivel = i
                End If
            End If
            i = i - 1
        Loop
        NumeroLinhasVisiveis = MaiorLinhaVisivel
    End If
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.PreencheRegistos"

End Sub

Public Sub MostraRegistos()
    On Error GoTo HandleError
    If NumeroRegistosLista > 0 Then
        PreencheRegistos PREENCHE_BAIXO_CIMA
        RaiseEvent AntesMudarLinhaActiva(PosicaoListaControlos, PosicaoListaRegistos, NumeroRegistosLista)
        ListaControlos.ActivaLinha PosicaoListaControlos
        RaiseEvent DepoisMudarLinhaActiva(PosicaoListaControlos, PosicaoListaRegistos, NumeroRegistosLista)
    Else
        If PodeCriarNovasLinhas Then
            PosicaoListaControlos = 0
            PosicaoListaRegistos = 1
            MostraLinha PosicaoListaControlos
            ListaControlos.PreencheValoresDefeito PosicaoListaControlos
            ListaControlos.ActivaLinha PosicaoListaControlos
        End If
    End If
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.MostraRegistos"
    
End Sub
Public Sub CriaLinhaNova()
    On Error GoTo HandleError
    If NumeroRegistosLista = 0 Then
        Exit Sub
    End If
    If PodeCriarNovasLinhas Then
        ListaControlos.DesactivaLinha PosicaoListaControlos
        If NumeroRegistosLista > NumeroLinhasEcra Then  ' � preciso preencher
                                                        'os registos de novo
                                                        
            PosicaoListaControlos = 0 ' come�ar a preencher no primeiro registo
            PosicaoListaRegistos = NumeroRegistosLista - (NumeroLinhasEcra) + 1
            PreencheRegistos PREENCHE_CIMA_BAIXO
            PosicaoListaControlos = NumeroLinhasVisiveis
            PosicaoListaRegistos = NumeroRegistosLista
        Else
            PosicaoListaRegistos = 1
            PosicaoListaControlos = 0
            PreencheRegistos PREENCHE_CIMA_BAIXO
            PosicaoListaRegistos = NumeroRegistosLista
            PosicaoListaControlos = NumeroLinhasVisiveis
            
        End If
        PosicaoListaRegistos = PosicaoListaRegistos + 1
        PosicaoListaControlos = PosicaoListaControlos + 1
        
        MostraLinha PosicaoListaControlos
        ListaControlos.LimpaLinha PosicaoListaControlos
        ListaControlos.PreencheValoresDefeito PosicaoListaControlos
        RaiseEvent AntesMudarLinhaActiva(PosicaoListaControlos, PosicaoListaRegistos, NumeroRegistosLista)
        ListaControlos.ActivaLinha PosicaoListaControlos
        RaiseEvent DepoisMudarLinhaActiva(PosicaoListaControlos, PosicaoListaRegistos, NumeroRegistosLista)
    End If
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.CriaLinhaNova"
    
End Sub

Public Sub ActualizaPosicao(IndiceControlo As Integer)
    ' quando o utilizador muda de registo sem altera��es
    Dim RegistosDeslocados  As Integer ' quantos registos se saltaram
    On Error GoTo HandleError
    If NumeroRegistosLista = 0 Or PosicaoListaControlos = IndiceControlo Then
        RaiseEvent AntesMudarLinhaActiva(PosicaoListaControlos, PosicaoListaRegistos, NumeroRegistosLista)
        ListaControlos.ActivaLinha PosicaoListaControlos
        RaiseEvent DepoisMudarLinhaActiva(PosicaoListaControlos, PosicaoListaRegistos, NumeroRegistosLista)
        
        Exit Sub
    End If
    ListaControlos.DesactivaLinha PosicaoListaControlos
    If PosicaoListaRegistos > NumeroRegistosLista Then
        EscondeLinha PosicaoListaControlos
    End If
    If PosicaoListaControlos <> IndiceControlo Then
        RegistosDeslocados = IndiceControlo - PosicaoListaControlos
        PosicaoListaRegistos = PosicaoListaRegistos + RegistosDeslocados
        PosicaoListaControlos = IndiceControlo
        If PosicaoListaRegistos <= NumeroRegistosLista Then
            ListaRegistos(PosicaoListaRegistos).PreencheRegisto ListaControlos, PosicaoListaControlos
        End If
    End If
    RaiseEvent AntesMudarLinhaActiva(PosicaoListaControlos, PosicaoListaRegistos, NumeroRegistosLista)
    ListaControlos.ActivaLinha PosicaoListaControlos
    RaiseEvent DepoisMudarLinhaActiva(PosicaoListaControlos, PosicaoListaRegistos, NumeroRegistosLista)
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.ActualizaPosicao"

End Sub
Public Sub AcrescentaRegisto(Registo)
    On Error GoTo HandleError
    NumeroRegistosLista = NumeroRegistosLista + 1
    ListaRegistos.Add Registo
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.AcrescentaRegisto"
    
End Sub
Private Sub EscondeLinha(Indice As Integer, Optional ActivaAnterior)
    ' esta funcao poe o visible de uma linha do multi-record a false
    On Error GoTo HandleError
    If Indice <= NumeroLinhasVisiveis Then
        If Not IsMissing(ActivaAnterior) Then
            If ActivaAnterior Then
                ListaControlos.MudaLinhaActiva (Indice - 1)
            End If
        End If
        ListaControlos.EscondeLinha Indice
        NumeroLinhasVisiveis = NumeroLinhasVisiveis - 1
        
    End If
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.EscondeLinha"
    
End Sub
Private Sub MostraLinha(Indice As Integer)
    ' esta funcao poe o visible de uma linha do multi-record a true
    On Error GoTo HandleError
    If Indice > NumeroLinhasVisiveis Then
        ListaControlos.MostraLinha Indice
        NumeroLinhasVisiveis = NumeroLinhasVisiveis + 1
    End If
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.MostraLinha"
    
End Sub
Private Sub MudaLinhaActiva(Campo As Control)
    ' esta sub muda a linha activa no obj. multirecord e preenche de novo os campos
    On Error GoTo HandleError
    If PosicaoListaRegistos <= NumeroRegistosLista Then
        ListaRegistos(PosicaoListaRegistos).PreencheRegisto ListaControlos, Campo.Index 'PosicaoListaControlos
    End If
    RaiseEvent AntesMudarLinhaActiva(PosicaoListaControlos, PosicaoListaRegistos, NumeroRegistosLista)
    ListaControlos.MudaLinhaCursor Campo, (PosicaoListaRegistos > NumeroRegistosLista)
    RaiseEvent DepoisMudarLinhaActiva(PosicaoListaControlos, PosicaoListaRegistos, NumeroRegistosLista)
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "MultiRecord.MudaLinhaActiva"
    
End Sub
Public Function MudaLinha_Perfil(Indice) As Integer
'Este procedumento � usado nas marca��es das an�lises sempre que se quer marcar um perfil falso
    '� criada uma nova linha mas n�o � colocado o focus em lado nenhum, o que faz com que o
    'c�digo dos controlos(GotFocus,LostFocus,...) n�o seja executado
        
    Dim TmpPosicaoControlo As Integer
    Dim TmpIndice As Integer
    Dim MostraNovaLinha As Boolean
    
    ' � preciso puxar uma linha para cima
    MostraNovaLinha = False
    TmpIndice = Indice
    ListaControlos.DesactivaLinha PosicaoListaControlos
    If TmpIndice = NumeroLinhasEcra Or PosicaoListaRegistos = NumeroRegistosLista Then
        TmpPosicaoControlo = PosicaoListaControlos
        PosicaoListaControlos = 0
        If PosicaoListaRegistos = NumeroRegistosLista And TmpIndice < NumeroLinhasEcra Then
            PosicaoListaRegistos = PosicaoListaRegistos - TmpPosicaoControlo
            MostraNovaLinha = True
            TmpIndice = TmpIndice + 1
        Else
            PosicaoListaRegistos = PosicaoListaRegistos - TmpPosicaoControlo + 1 ' vamos avan�ar um registo
        End If
        PreencheRegistos PREENCHE_BAIXO_CIMA
        PosicaoListaControlos = TmpPosicaoControlo
        PosicaoListaRegistos = PosicaoListaRegistos + TmpPosicaoControlo
        If MostraNovaLinha Then
            PosicaoListaControlos = PosicaoListaControlos + 1
            PosicaoListaRegistos = PosicaoListaRegistos + 1
        End If

        If PosicaoListaRegistos > NumeroRegistosLista Or MostraNovaLinha Then
            MostraLinha TmpIndice
            MostraNovaLinha = False
        End If
    Else
        If PosicaoListaRegistos <= NumeroRegistosLista Then
            TmpIndice = TmpIndice + 1
            PosicaoListaRegistos = PosicaoListaRegistos + 1
            PosicaoListaControlos = PosicaoListaControlos + 1
        End If
    End If
    MostraLinha TmpIndice
    MudaLinha_Perfil = TmpIndice
    'MudaLinhaActiva (PrimeiroCampo(TmpIndice))
End Function


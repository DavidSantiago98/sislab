VERSION 5.00
Begin VB.Form FormMudarSenha 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionMudarSenha"
   ClientHeight    =   3390
   ClientLeft      =   1170
   ClientTop       =   2415
   ClientWidth     =   7065
   Icon            =   "FormMudSen.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3390
   ScaleWidth      =   7065
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Chaves 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   5950
      Picture         =   "FormMudSen.frx":000C
      ScaleHeight     =   480
      ScaleMode       =   0  'User
      ScaleWidth      =   480
      TabIndex        =   15
      Top             =   480
      Width           =   480
   End
   Begin VB.TextBox EcConfirmarSenha 
      Enabled         =   0   'False
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   4320
      PasswordChar    =   "*"
      TabIndex        =   11
      Top             =   2640
      Width           =   1815
   End
   Begin VB.TextBox EcNovaSenha 
      Enabled         =   0   'False
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   2280
      PasswordChar    =   "*"
      TabIndex        =   10
      Top             =   2640
      Width           =   1815
   End
   Begin VB.TextBox EcAntigaSenha 
      Enabled         =   0   'False
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   240
      PasswordChar    =   "*"
      TabIndex        =   9
      Top             =   2640
      Width           =   1815
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   2055
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   5055
      Begin VB.TextBox EcNovaSenhaAux 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2280
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   960
         Width           =   2295
      End
      Begin VB.TextBox EcConfirmarSenhaAux 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2280
         PasswordChar    =   "*"
         TabIndex        =   3
         Top             =   1440
         Width           =   2295
      End
      Begin VB.TextBox EcAntigaSenhaAux 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2280
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   480
         Width           =   2295
      End
      Begin VB.Label Label1 
         Caption         =   "Antiga Senha"
         Height          =   255
         Left            =   360
         TabIndex        =   6
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Nova Senha"
         Height          =   255
         Left            =   360
         TabIndex        =   7
         Top             =   960
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "Confirmar Nova Senha"
         Height          =   255
         Left            =   360
         TabIndex        =   8
         Top             =   1440
         Width           =   1815
      End
   End
   Begin VB.CommandButton ButtonCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   5640
      TabIndex        =   5
      Top             =   1800
      Width           =   1095
   End
   Begin VB.CommandButton ButtonOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   5640
      TabIndex        =   4
      Top             =   1320
      Width           =   1095
   End
   Begin VB.Label Label6 
      Caption         =   "EcConfirmarSenha"
      Height          =   255
      Left            =   4320
      TabIndex        =   14
      Top             =   2400
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label Label5 
      Caption         =   "EcNovaSenha"
      Height          =   255
      Left            =   2280
      TabIndex        =   13
      Top             =   2400
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "EcAntigaSenha"
      Height          =   255
      Left            =   240
      TabIndex        =   12
      Top             =   2400
      Visible         =   0   'False
      Width           =   1455
   End
End
Attribute VB_Name = "FormMudarSenha"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/01/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim obTabela As ADODB.Recordset
Dim CriterioTabela As String

Dim NumTentativas As Integer

Private Sub ButtonCancelar_Click()
    
    BG_Mensagem mediMsgBox, "A Senha n�o foi alterada !     ", vbExclamation, Me.Caption
    Unload Me

End Sub

Private Sub ButtonOK_Click()
    
    Dim SQLQuery As String
    Dim Erro As Boolean
    Dim Rs As ADODB.Recordset
    
    Set Rs = New ADODB.Recordset
    On Error GoTo TrataErro
    
    Erro = False
    NumTentativas = NumTentativas + 1
    

    If EcNovaSenhaAux <> EcConfirmarSenhaAux Then
        BG_Mensagem mediMsgBox, "A Nova Senha e a Confirma��o da Nova Senha n�o s�o iguais!", vbExclamation, Me.Caption
        If NumTentativas = 3 Then
            BG_Mensagem mediMsgBox, "Fim das tentativas permitidas!", vbExclamation, Me.Caption
            Unload Me
        End If
        Exit Sub
    End If
    
    EcAntigaSenha = BL_Encripta(EcAntigaSenhaAux)
    If EcAntigaSenha = "" Then
        CriterioTabela = "SELECT count(*) as conta FROM sl_idutilizador WHERE utilizador = '" & Trim(gIdUtilizador) & "'"
    Else
        CriterioTabela = "SELECT count(*) as conta FROM sl_idutilizador WHERE utilizador = '" & Trim(gIdUtilizador) & "' AND senha ='" & Trim(EcAntigaSenha) & "'"
    End If
        
    Rs.Open CriterioTabela, gConexao, adOpenForwardOnly, adLockOptimistic
          
    If Rs!Conta = 0 Or Erro = True Then
        If NumTentativas < 3 Then
            BG_Mensagem mediMsgBox, "A Senha indicada � incorrecta!", vbExclamation, Me.Caption
            EcAntigaSenhaAux.SetFocus
            Exit Sub
        Else
            BG_Mensagem mediMsgBox, "Fim das tentativas permitidas!", vbExclamation, Me.Caption
            obTabela.Close
            Set obTabela = Nothing
            Unload Me
        End If
    Else
        
        EcNovaSenha = BL_Encripta(EcNovaSenhaAux)
        
        SQLQuery = "UPDATE sl_idutilizador SET senha = '" & BG_CvPlica(EcNovaSenha) & "' WHERE utilizador = '" & Trim(gIdUtilizador) & "'"
        gConexao.Execute SQLQuery
        BG_Mensagem mediMsgBox, "A senha foi mudada com sucesso.", vbInformation, Me.Caption
        Rs.Close
        Set Rs = Nothing
        Unload Me
    End If
    
    Exit Sub
    
TrataErro:
    Erro = True
    Resume Next

End Sub

Private Sub EcAntigaSenha_Change()
    
    EcAntigaSenhaAux = BG_CryptStringDecode(EcAntigaSenha)

End Sub

Private Sub EcAntigaSenhaAux_GotFocus()
    
    Dim EsteControl As Control

    Set EsteControl = EcAntigaSenhaAux
    
    EsteControl.SelStart = 0
    EsteControl.SelLength = Len(EsteControl)

End Sub

Private Sub EcConfirmarSenhaAux_GotFocus()
    
    Dim EsteControl As Control

    Set EsteControl = EcConfirmarSenhaAux
    
    EsteControl.SelStart = 0
    EsteControl.SelLength = Len(EsteControl)

End Sub

Private Sub EcNovaSenhaAux_GotFocus()
    
    Dim EsteControl As Control

    Set EsteControl = EcNovaSenhaAux
    
    EsteControl.SelStart = 0
    EsteControl.SelLength = Len(EsteControl)

End Sub

Private Sub Form_Activate()
    
    EcAntigaSenhaAux.SetFocus

End Sub

Private Sub Form_Load()
    
    Set obTabela = New ADODB.Recordset
    
    Me.Caption = "Mudar a Senha"
    Me.Left = 1665
    Me.Top = 2160
    Me.Width = 7185
    Me.Height = 2835

    EcAntigaSenhaAux.MaxLength = 14
    EcNovaSenhaAux.MaxLength = 14
    EcConfirmarSenhaAux.MaxLength = 14

    NumTentativas = 0
    
    CriterioTabela = "SELECT nome FROM sl_idutilizador where cod_utilizador= " & gCodUtilizador
    obTabela.Open CriterioTabela, gConexao, adOpenForwardOnly, adLockOptimistic
    
    Frame1.Caption = "Utilizador: " & obTabela!Nome
    MDIFormInicio.MousePointer = vbArrow
    
End Sub



VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormFactusEnvio 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "/edrw2"
   ClientHeight    =   8895
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   14670
   Icon            =   "FormFACTUSEnvio.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8895
   ScaleWidth      =   14670
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameCodBarras 
      Height          =   975
      Left            =   10080
      TabIndex        =   27
      Top             =   7200
      Visible         =   0   'False
      Width           =   3495
      Begin VB.TextBox EcCodAnaEFR 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   360
         TabIndex        =   31
         Top             =   480
         Width           =   1335
      End
      Begin VB.CheckBox CkActivarCodBarras 
         Caption         =   "Activar Leitura das An�. por C�d. da EFR"
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   120
         Width           =   3255
      End
      Begin VB.Label LbCred 
         Height          =   255
         Left            =   2760
         TabIndex        =   30
         Top             =   480
         Width           =   495
      End
      Begin VB.Label Label6 
         Caption         =   "Cred "
         Height          =   255
         Left            =   2040
         TabIndex        =   29
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Frame FrameDadosFact 
      Caption         =   "Dados Factura��o"
      Height          =   975
      Left            =   8280
      TabIndex        =   41
      Top             =   7200
      Visible         =   0   'False
      Width           =   3375
      Begin VB.Label LbLote 
         Height          =   255
         Left            =   120
         TabIndex        =   43
         Top             =   600
         Width           =   3015
      End
      Begin VB.Label LbUserEnvio 
         Height          =   255
         Left            =   120
         TabIndex        =   42
         Top             =   240
         Width           =   3135
      End
   End
   Begin VB.Frame Frame4 
      Height          =   1095
      Left            =   1080
      TabIndex        =   54
      Top             =   7800
      Width           =   13095
      Begin VB.CommandButton BtValida 
         Caption         =   "Enviar"
         Enabled         =   0   'False
         Height          =   855
         Left            =   5040
         Picture         =   "FormFACTUSEnvio.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   62
         ToolTipText     =   " Enviar para a Factura��o "
         Top             =   120
         Width           =   1335
      End
      Begin VB.CommandButton BtGestReq 
         Enabled         =   0   'False
         Height          =   600
         Left            =   1680
         Picture         =   "FormFACTUSEnvio.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   61
         ToolTipText     =   "Requisi��o"
         Top             =   285
         Width           =   1335
      End
      Begin VB.CommandButton BtIdentif 
         Enabled         =   0   'False
         Height          =   600
         Left            =   120
         Picture         =   "FormFACTUSEnvio.frx":19A0
         Style           =   1  'Graphical
         TabIndex        =   60
         ToolTipText     =   "Identifica��o"
         Top             =   285
         Width           =   1215
      End
      Begin VB.CommandButton BtImprimeResumo 
         Height          =   615
         Left            =   3360
         Picture         =   "FormFACTUSEnvio.frx":266A
         Style           =   1  'Graphical
         TabIndex        =   59
         ToolTipText     =   "Imprimir Resumo"
         Top             =   285
         Width           =   1215
      End
      Begin VB.CommandButton BtDadosFact 
         Height          =   600
         Left            =   6720
         Picture         =   "FormFACTUSEnvio.frx":3334
         Style           =   1  'Graphical
         TabIndex        =   58
         ToolTipText     =   "Dados Factura��o"
         Top             =   285
         Width           =   1215
      End
      Begin VB.CommandButton BtCodBarras 
         Height          =   600
         Left            =   8280
         Picture         =   "FormFACTUSEnvio.frx":3FFE
         Style           =   1  'Graphical
         TabIndex        =   57
         ToolTipText     =   "Leitura C�digo Barras"
         Top             =   285
         Width           =   1335
      End
      Begin VB.CommandButton BtVerificEstadoFact 
         Enabled         =   0   'False
         Height          =   600
         Left            =   9960
         Picture         =   "FormFACTUSEnvio.frx":4308
         Style           =   1  'Graphical
         TabIndex        =   56
         ToolTipText     =   "Verificar Estado Factura��o"
         Top             =   285
         Width           =   1335
      End
      Begin VB.CommandButton BtEtiq 
         Height          =   615
         Left            =   11640
         Picture         =   "FormFACTUSEnvio.frx":4FD2
         Style           =   1  'Graphical
         TabIndex        =   55
         ToolTipText     =   "Imprimir Etiquetas ARS"
         Top             =   285
         Width           =   975
      End
   End
   Begin VB.PictureBox Pic2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3600
      Picture         =   "FormFACTUSEnvio.frx":573C
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   51
      Top             =   10320
      Width           =   255
   End
   Begin VB.PictureBox Pic 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   3240
      Picture         =   "FormFACTUSEnvio.frx":5AC6
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   50
      Top             =   10560
      Width           =   255
   End
   Begin VB.CommandButton BExpandir 
      Height          =   495
      Left            =   480
      Picture         =   "FormFACTUSEnvio.frx":5E50
      Style           =   1  'Graphical
      TabIndex        =   47
      Top             =   9240
      Width           =   495
   End
   Begin VB.CommandButton BtMinimizar 
      Height          =   495
      Left            =   0
      Picture         =   "FormFACTUSEnvio.frx":6B1A
      Style           =   1  'Graphical
      TabIndex        =   46
      Top             =   9240
      Width           =   495
   End
   Begin VB.Frame FrameAccao 
      Height          =   1095
      Left            =   6480
      TabIndex        =   44
      Top             =   4200
      Width           =   3495
      Begin VB.Label LbAccao 
         Caption         =   " A Preencher Requisi��es...."
         Height          =   495
         Left            =   600
         TabIndex        =   45
         Top             =   360
         Width           =   2415
      End
   End
   Begin MSFlexGridLib.MSFlexGrid FgReq 
      Height          =   7695
      Left            =   0
      TabIndex        =   36
      Top             =   120
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   13573
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
   End
   Begin VB.Frame Frame2 
      Caption         =   "An�lises "
      Height          =   4695
      Left            =   1080
      TabIndex        =   6
      Top             =   3240
      Width           =   12855
      Begin VB.TextBox EcAux 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   0
         TabIndex        =   37
         Top             =   480
         Width           =   615
      End
      Begin MSFlexGridLib.MSFlexGrid FGAnalises 
         Height          =   4260
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   12615
         _ExtentX        =   22251
         _ExtentY        =   7514
         _Version        =   393216
         FixedCols       =   0
         ForeColor       =   0
         BackColorSel    =   -2147483643
         ForeColorSel    =   0
         BackColorBkg    =   -2147483633
         ScrollBars      =   2
         BorderStyle     =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label dragging_label 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   120
         TabIndex        =   49
         Top             =   360
         Visible         =   0   'False
         Width           =   1455
      End
   End
   Begin VB.TextBox EcCodAna 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   13965
      TabIndex        =   26
      Top             =   3960
      Width           =   615
   End
   Begin VB.Frame Frame5 
      Height          =   3015
      Left            =   12960
      TabIndex        =   13
      Top             =   10080
      Width           =   2175
      Begin VB.Label Label1 
         Caption         =   "An�lise Isenta"
         Height          =   255
         Index           =   12
         Left            =   480
         TabIndex        =   25
         Top             =   2475
         Width           =   1335
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFC0&
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   24
         Top             =   2475
         Width           =   255
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFC0FF&
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   23
         Top             =   2040
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "An�lise Gr�tis"
         Height          =   255
         Index           =   9
         Left            =   480
         TabIndex        =   22
         Top             =   2040
         Width           =   1215
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0FFFF&
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   21
         Top             =   600
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "An�lise Rejeitada"
         Height          =   255
         Index           =   3
         Left            =   480
         TabIndex        =   20
         Top             =   1560
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "An�lise Sem Fac.Rec"
         Height          =   255
         Index           =   8
         Left            =   480
         TabIndex        =   19
         Top             =   1080
         Width           =   1575
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFC0C0&
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   18
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "An�lise Retirada"
         Height          =   255
         Index           =   6
         Left            =   480
         TabIndex        =   17
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0C0FF&
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   16
         Top             =   1560
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "An�lise Enviada"
         Height          =   255
         Index           =   2
         Left            =   480
         TabIndex        =   15
         Top             =   150
         Width           =   1335
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0FFC0&
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   14
         Top             =   150
         Width           =   255
      End
   End
   Begin VB.CommandButton BtAdiciona 
      Enabled         =   0   'False
      Height          =   615
      Left            =   13965
      Picture         =   "FormFACTUSEnvio.frx":77E4
      Style           =   1  'Graphical
      TabIndex        =   12
      ToolTipText     =   "Adicionar An�lise"
      Top             =   4440
      Width           =   615
   End
   Begin VB.CommandButton BtRetira 
      Enabled         =   0   'False
      Height          =   615
      Left            =   13965
      Picture         =   "FormFACTUSEnvio.frx":84AE
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Retirar An�lise"
      Top             =   7200
      Width           =   615
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   10
      Top             =   8520
      Visible         =   0   'False
      Width           =   14670
      _ExtentX        =   25876
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton BtBaixo 
      Enabled         =   0   'False
      Height          =   495
      Left            =   13965
      Picture         =   "FormFACTUSEnvio.frx":9178
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Mudar Ordem"
      Top             =   6240
      Width           =   615
   End
   Begin VB.CommandButton BtCima 
      Enabled         =   0   'False
      Height          =   495
      Left            =   13965
      Picture         =   "FormFACTUSEnvio.frx":9E42
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Mudar Ordem"
      Top             =   5640
      Width           =   615
   End
   Begin VB.Frame Frame1 
      Height          =   3255
      Left            =   1080
      TabIndex        =   0
      Top             =   0
      Width           =   13575
      Begin VB.TextBox EcCredencial 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   8400
         TabIndex        =   105
         ToolTipText     =   "Credencial"
         Top             =   2280
         Width           =   3495
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   5880
         Picture         =   "FormFACTUSEnvio.frx":AB0C
         Style           =   1  'Graphical
         TabIndex        =   90
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   1800
         Width           =   375
      End
      Begin VB.CommandButton BtPesqProven 
         Height          =   315
         Left            =   5880
         Picture         =   "FormFACTUSEnvio.frx":B096
         Style           =   1  'Graphical
         TabIndex        =   101
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   2820
         Width           =   375
      End
      Begin VB.TextBox EcDescrProven 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   103
         TabStop         =   0   'False
         Top             =   2820
         Width           =   3855
      End
      Begin VB.TextBox EcCodProven 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   102
         Top             =   2820
         Width           =   975
      End
      Begin VB.TextBox EcDescrEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   94
         TabStop         =   0   'False
         Top             =   1800
         Width           =   3855
      End
      Begin VB.TextBox EcCodEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   93
         Top             =   1800
         Width           =   975
      End
      Begin VB.TextBox EcEstado 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   92
         Top             =   960
         Width           =   2055
      End
      Begin VB.TextBox EcNumReq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   285
         Left            =   1080
         TabIndex        =   91
         Top             =   960
         Width           =   975
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   5880
         Picture         =   "FormFACTUSEnvio.frx":B620
         Style           =   1  'Graphical
         TabIndex        =   89
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   1440
         Width           =   375
      End
      Begin VB.TextBox EcDescrSala 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   88
         TabStop         =   0   'False
         Top             =   1440
         Width           =   3855
      End
      Begin VB.TextBox EcCodSala 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   87
         Top             =   1440
         Width           =   975
      End
      Begin VB.TextBox EcDtChega 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   4680
         TabIndex        =   86
         Top             =   960
         Width           =   1215
      End
      Begin VB.CommandButton BtPesquisaMed 
         Height          =   315
         Left            =   5880
         Picture         =   "FormFACTUSEnvio.frx":BBAA
         Style           =   1  'Graphical
         TabIndex        =   85
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de M�dicos  "
         Top             =   2140
         Width           =   375
      End
      Begin VB.TextBox EcCodMed 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   84
         Top             =   2140
         Width           =   975
      End
      Begin VB.TextBox EcNomeMed 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   83
         TabStop         =   0   'False
         Top             =   2140
         Width           =   3855
      End
      Begin VB.CommandButton BtPesqPais 
         Height          =   315
         Left            =   5880
         Picture         =   "FormFACTUSEnvio.frx":C134
         Style           =   1  'Graphical
         TabIndex        =   82
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   2480
         Width           =   375
      End
      Begin VB.TextBox EcCodPais 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   81
         Top             =   2480
         Width           =   975
      End
      Begin VB.TextBox EcDescrPais 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   80
         TabStop         =   0   'False
         Top             =   2480
         Width           =   3855
      End
      Begin VB.ComboBox CbCodUrbano 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   11160
         Style           =   2  'Dropdown List
         TabIndex        =   78
         Top             =   1440
         Width           =   1455
      End
      Begin VB.TextBox EcKm 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11160
         TabIndex        =   73
         Top             =   1800
         Width           =   615
      End
      Begin VB.ComboBox CbConvencao 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   11160
         Style           =   2  'Dropdown List
         TabIndex        =   72
         Top             =   600
         Width           =   1455
      End
      Begin VB.ComboBox CbHemodialise 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   11160
         Style           =   2  'Dropdown List
         TabIndex        =   71
         Top             =   1035
         Width           =   1455
      End
      Begin VB.ComboBox CbTipoIsencao 
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         Height          =   315
         Left            =   8400
         Style           =   2  'Dropdown List
         TabIndex        =   66
         Top             =   1005
         Width           =   1335
      End
      Begin VB.TextBox EcNumBenef 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   8400
         TabIndex        =   65
         Top             =   1440
         Width           =   1335
      End
      Begin VB.TextBox EcReqAssoc 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   8400
         TabIndex        =   64
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox EcDoenteProf 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8400
         TabIndex        =   63
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox EcSexo 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   8400
         TabIndex        =   53
         Top             =   195
         Width           =   1335
      End
      Begin VB.CommandButton BtAlteraUtente 
         Height          =   375
         Left            =   6720
         Picture         =   "FormFACTUSEnvio.frx":C6BE
         Style           =   1  'Graphical
         TabIndex        =   38
         ToolTipText     =   "Alterar utente a facturar"
         Top             =   120
         Width           =   615
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "d/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   3
         EndProperty
         Height          =   285
         Left            =   11160
         TabIndex        =   34
         Top             =   195
         Width           =   1455
      End
      Begin VB.TextBox EcIdade 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   12600
         TabIndex        =   33
         Top             =   195
         Width           =   855
      End
      Begin VB.TextBox EcTipoUte 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   540
         Width           =   975
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   2040
         TabIndex        =   2
         Top             =   540
         Width           =   2295
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   195
         Width           =   5655
      End
      Begin VB.Label Label1 
         Caption         =   "Credencial"
         Height          =   255
         Index           =   19
         Left            =   7440
         TabIndex        =   106
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Proveni�ncia"
         Height          =   195
         Index           =   18
         Left            =   120
         TabIndex        =   104
         Top             =   2820
         Width           =   930
      End
      Begin VB.Label Label10 
         Caption         =   "E.F.R."
         Height          =   255
         Left            =   120
         TabIndex        =   100
         Top             =   1800
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "N� Req."
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   99
         Top             =   960
         Width           =   975
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Data"
         Height          =   195
         Index           =   14
         Left            =   4200
         TabIndex        =   98
         Top             =   975
         Width           =   345
      End
      Begin VB.Label Label1 
         Caption         =   "Posto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   97
         Top             =   1440
         Width           =   1155
      End
      Begin VB.Label Label5 
         Caption         =   "M�dico"
         Height          =   255
         Left            =   120
         TabIndex        =   96
         Top             =   2140
         Width           =   735
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Pa�s"
         Height          =   195
         Index           =   36
         Left            =   120
         TabIndex        =   95
         Top             =   2480
         Width           =   330
      End
      Begin VB.Label LbCredenciais 
         Caption         =   "LbCredenciais"
         Height          =   255
         Left            =   11400
         TabIndex        =   79
         Top             =   2880
         Width           =   2055
      End
      Begin VB.Label LbUtenteFact 
         Caption         =   "UTENTE ALTERADO PARA FACTURA��O"
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   8400
         TabIndex        =   39
         Top             =   2880
         Visible         =   0   'False
         Width           =   3615
      End
      Begin VB.Label Label4 
         Caption         =   "Km"
         Height          =   255
         Left            =   9960
         TabIndex        =   77
         Top             =   1800
         Width           =   615
      End
      Begin VB.Label Label3 
         Caption         =   "C�digo Urbano"
         Height          =   255
         Left            =   9960
         TabIndex        =   76
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Conven��o"
         Height          =   255
         Index           =   0
         Left            =   9960
         TabIndex        =   75
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Hemodialise"
         Height          =   255
         Index           =   1
         Left            =   9960
         TabIndex        =   74
         Top             =   1035
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Isen��o"
         Height          =   255
         Index           =   13
         Left            =   7440
         TabIndex        =   70
         Top             =   1005
         Width           =   615
      End
      Begin VB.Label Label11 
         Caption         =   "Nr. Benef."
         Height          =   255
         Left            =   7440
         TabIndex        =   69
         Top             =   1410
         Width           =   855
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Nr. Posto"
         Height          =   195
         Index           =   16
         Left            =   7440
         TabIndex        =   68
         Top             =   600
         Width           =   660
      End
      Begin VB.Label Labe41 
         Caption         =   "Doente Prof."
         Height          =   255
         Index           =   2
         Left            =   7440
         TabIndex        =   67
         Top             =   1800
         Width           =   1500
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Sexo"
         Height          =   195
         Left            =   7440
         TabIndex        =   52
         ToolTipText     =   "Data Nascimento"
         Top             =   195
         Width           =   360
      End
      Begin VB.Label LbFimSemana 
         Caption         =   "FIM SEMANA"
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   6960
         TabIndex        =   40
         Top             =   2880
         Width           =   1695
      End
      Begin VB.Label LbDataNasc 
         AutoSize        =   -1  'True
         Caption         =   "Data &Nasc."
         Height          =   195
         Left            =   9960
         TabIndex        =   35
         ToolTipText     =   "Data Nascimento"
         Top             =   195
         Width           =   810
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "N� Interno"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   540
         Width           =   720
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   195
         Width           =   420
      End
   End
   Begin MSComctlLib.ImageList ImageListGrid 
      Left            =   0
      Top             =   10440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormFACTUSEnvio.frx":CC48
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormFACTUSEnvio.frx":134AA
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Nova Ana."
      Height          =   255
      Index           =   17
      Left            =   13920
      TabIndex        =   48
      Top             =   3720
      Width           =   975
   End
   Begin VB.Label LbTotalFactEFR 
      Height          =   255
      Left            =   3120
      TabIndex        =   32
      Top             =   9840
      Width           =   735
   End
End
Attribute VB_Name = "FormFactusEnvio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim NumCampos As Integer

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim ListarRemovidos As Boolean

Const Verde = &HC0FFC0
Const vermelho = &HC0C0FF
Const azul = &HFFC0C0
Const Amarelo = &HC0FFFF
Const rosa = &HFFC0FF
Const azul2 = &HFFFFC0

Const CorRetirada = Amarelo
Const CorEnviada = Verde
Const CorAdicionada = azul
Const CorRejeitada = vermelho
Const CorBorla = rosa
Const CorIsento = azul2

Const NumLinhas = 16
'Linha da grid selecionada.
Dim LINHA_SEL As Integer
Dim COLUNA_SEL As Integer

'Linha da grid selecionada.
Dim LINHA_EDITA As Integer
Dim COLUNA_EDITA As Integer

Const TAM_LINHA = 7

Public rs As ADODB.recordset

' pferreira 2010.05.17
' DRAG'N'DROP
Private is_dragging As Boolean
Private source_row As Integer
Private deliver_row As Integer
' -------------------------------------------------------------------
' COLUNAS NA GRELHA ENVIO PARA FACTURACAO
' -------------------------------------------------------------------
Const lColFactLinhaSel = 0
Const lColFactEFR = 1
Const lColFactP1 = 2
Const lColFactMedico = 3
Const lColFactNrBenef = 4
Const lColFactDescrAna = 5
Const lColFactTaxa = 6
Const lColFactCodAnaGH = 7
Const lColFactNrC = 8
Const lColFactDescrAnaGH = -1
Const lColFactRecibo = 9
Const lColFactQuantidade = 10
Const lColFactCodAna = 11
Const lColFactPercent = 12
Const lColFactOrdem = 13
Const lColFactPortaria = 14

Dim lCredencialActiva  As Long
Dim lPosicaoActual As Long
Dim ReqActual As Long

Dim FLG_ExecutaFGAna As Boolean

Dim flg_pergunta As Boolean

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    
    Call Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    Call PreencheValoresDefeito
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
    gF_FACTUSENVIO = 1
    
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    'soliveira correccao lacto
    If gF_TRATAREJ = 1 And gPassaParams.id = "TRATA_REJEITADOS" Then
        EcNumReq.Text = gPassaParams.Param(0)
        BG_LimpaPassaParams
        FuncaoProcurar
    End If

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    gF_FACTUSENVIO = 0
    
    If gF_TRATAREJ = 1 And gSISTEMA_FACTURACAO = cFACTURACAO_FACTUS Then
        FormFACTUSTrataRejeitados.Enabled = True
        Set gFormActivo = FormFACTUSTrataRejeitados
        gPassaParams.id = "ABRIU_REQUIS"
    Else
        Set gFormActivo = MDIFormInicio
    End If
    
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    ' Fecha a conex�o secund�ria.
    BL_Fecha_Conexao_Secundaria
    
    Set FormFactusEnvio = Nothing
    
End Sub

Sub Inicializacoes()

    Me.caption = " Envio de Dados para Factura��o"
    
    Me.left = 100
    Me.top = 20
    Me.Width = 14760
    Me.Height = 9315 ' Normal
    'Me.Height = 8700 ' Campos Extras
    
    Set CampoDeFocus = EcNumReq
    
    EcKm.Enabled = False
    
    LINHA_SEL = 1
    
    ' Abre a liga��o ao Factus.
    Call BL_Abre_Conexao_Secundaria(gSGBD_SECUNDARIA)
    ' -----------------------
    ' ABRE CONEXAO COM FACTUS
    ' -----------------------
    BL_Abre_Conexao_HIS gConexaoSecundaria, gOracle
    ReqActual = 1
    DoEvents
    DoEvents
    DoEvents
    FLG_ExecutaFGAna = False
End Sub

 Sub DefTipoCampos()
    
    With FGAnalises
        .Cols = 15
        .rows = NumLinhas
        .FixedCols = 0
        .FixedRows = 1
        
        .Col = lColFactLinhaSel
        .ColWidth(lColFactLinhaSel) = 300
        .TextMatrix(0, lColFactLinhaSel) = ""
        .MergeCells = flexMergeFree
        
        .Col = lColFactEFR
        .ColWidth(lColFactEFR) = 500
        .TextMatrix(0, lColFactEFR) = "EFR"
        .MergeCells = flexMergeFree
        
        .Col = lColFactP1
        .MergeCol(lColFactP1) = True
        .ColAlignment(lColFactP1) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(lColFactP1) = 1800
        .TextMatrix(0, lColFactP1) = "Credencial"
        
        .Col = lColFactMedico
        .ColWidth(lColFactMedico) = 800
        .TextMatrix(0, lColFactMedico) = "M�dico"
        
        .Col = lColFactNrBenef
        .ColWidth(lColFactNrBenef) = 1200
        .TextMatrix(0, lColFactNrBenef) = "Nr Benef"
        
        .Col = lColFactDescrAna
        .ColWidth(lColFactDescrAna) = 2500
        .TextMatrix(0, lColFactDescrAna) = "An�lise"
        
        .Col = lColFactTaxa
        .ColWidth(lColFactTaxa) = 700
        .TextMatrix(0, lColFactTaxa) = "Pre�o."
        
        .Col = lColFactCodAnaGH
        .ColWidth(lColFactCodAnaGH) = 1000
        .TextMatrix(0, lColFactCodAnaGH) = "C�digo"
        
        .Col = lColFactNrC
        .ColWidth(lColFactNrC) = 0
        .TextMatrix(0, lColFactNrC) = "C"
        
        .Col = lColFactRecibo
        .ColWidth(lColFactRecibo) = 1000
        .TextMatrix(0, lColFactRecibo) = "Fact.Recibo"
        
        .Col = lColFactQuantidade
        .ColWidth(lColFactQuantidade) = 400
        .TextMatrix(0, lColFactQuantidade) = "Qtd."
        
        .Col = lColFactCodAna
        .ColWidth(lColFactCodAna) = 700
        .TextMatrix(0, lColFactCodAna) = "An�lise"
        
        .Col = lColFactPercent
        .ColWidth(lColFactPercent) = 500
        .TextMatrix(0, lColFactPercent) = "%Ute"
        
        .Col = lColFactOrdem
        .ColWidth(lColFactOrdem) = 500
        .TextMatrix(0, lColFactOrdem) = "N�"
        
        .Col = lColFactPortaria
        .ColWidth(lColFactPortaria) = 500
        .TextMatrix(0, lColFactPortaria) = "Prt."
        
        .ColAlignment(0) = 4
        .ColAlignment(2) = 4
        .ColAlignment(3) = 4
        .ColAlignment(4) = 4
        .ColAlignment(5) = flexAlignLeftCenter
    End With
    
    With FgReq
        .Cols = 1
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .ColWidth(lColFactEFR - 1) = 1000
        .TextMatrix(0, lColFactEFR - 1) = "Req."
        
    End With
    
    EcKm.Tag = adDouble
    EcKm.MaxLength = 6
    EcDtChega.Tag = adDate
    EcCodEFR.Tag = adNumeric
    
   ' Inicializa a status bar.
    StatusBar1.Panels(1).Width = Width
    StatusBar1.Panels(1).Alignment = sbrLeft
    StatusBar1.Panels(1).Style = sbrText
    StatusBar1.Panels(1).Text = " " & caption
    EcCodAnaEFR.Enabled = False
    BtEtiq.Enabled = False
    If gImprimeEtiqARS = mediSim Then
        BtEtiq.Visible = True
    Else
        BtEtiq.Visible = False
    End If
 End Sub
 
Private Sub BExpandir_Click()
    FGAnalises.Height = 7380
    Frame2.Height = 7695
    Frame2.top = 0

End Sub

Private Sub BtAdiciona_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean
    Dim sSql As String
    Dim i As Integer
    Dim TotalElementosSel As Integer
    Dim rsAnalises As New ADODB.recordset
    Dim linhaA As Long
    
    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposEcran(3) = "descr_produto"
    Tamanhos(3) = 1500
    Headers(3) = "Produto"
    
    
    CamposRetorno.InicializaResultados 1
    
    If gSGBD = gOracle Then
        CFrom = "slv_analises_factus "
        CWhere = " seq_sinonimo is null "
    ElseIf gSGBD = gSqlServer Then
        CFrom = "slv_analises_factus "
        CWhere = " seq_sinonimo is null "
    End If
    CampoPesquisa = "descr_ana"
           
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana ", _
                                                                           " An�lises")
        
    
    If PesqRapida = True Then
        FormPesqRapidaAvancadaMultiSel.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
        
        If Not CancelouPesquisa Then
        
            For i = 1 To TotalElementosSel
                ' ---------------------------------------------------------------------------
                ' VERIFICA SE ANALISE ESTA MAPEADA
                ' ---------------------------------------------------------------------------
                sSql = "SELECT * FROM sl_ana_facturacao WHERE cod_ana = " & BL_TrataStringParaBD(CStr(resultados(i)))
                sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EF_ReqFact(ReqActual).dt_chega, Bg_DaData_ADO)) & " BETWEEN "
                sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
                rsAnalises.CursorLocation = adUseServer
                rsAnalises.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsAnalises.Open sSql, gConexao
                
                If rsAnalises.RecordCount > 0 Then
                    linhaA = EF_PreencheEstrutAnalises(ReqActual, mediComboValorNull, resultados(i), resultados(i), BL_HandleNull(rsAnalises!descr_ana, ""), BL_HandleNull(rsAnalises!cod_ana_gh, ""), _
                                                BL_HandleNull(rsAnalises!descr_ana_facturacao, ""), "1", gEstadoFactRequisNaoFacturado, 0, 1, BL_HandleNull(EF_ReqFact(ReqActual).codIsencao, mediComboValorNull), _
                                                0, "0", "", mediComboValorNull, "B", EF_ReqFact(ReqActual).codEfr, 0, BL_HandleNull(rsAnalises!qtd, 1), "", "", BL_HandleNull(EF_ReqFact(ReqActual).fimSemana, "0"), _
                                                BL_SelCodigo("SL_EFR", "COD_EMPRESA", "COD_EFR", EF_ReqFact(ReqActual).codEfr, "V"), 0, 0, EF_ReqFact(ReqActual).codEfr, BL_HandleNull(rsAnalises!qtd, 1), mediComboValorNull)
                    
                    BL_MudaCorFg Me.FGAnalises, linhaA, CorAdicionada
                    ' ---------------------------------------------------------------------------
                    ' PREENCHE A GRELHA
                    ' ---------------------------------------------------------------------------
                    PreencheFGAna ReqActual, linhaA
                    ' ---------------------------------------------------------------------------
                    ' AUMENTA GRELHA E ESTRUTURA
                    ' ---------------------------------------------------------------------------
                    If linhaA >= (NumLinhas - 1) Then
                        FGAnalises.AddItem ""
                    End If
                    
                    AdicionaAnaliseBD linhaA
                Else
                    BG_Mensagem mediMsgBox, " An�lise n�o est� mapeada para o FACTUS!", vbOKOnly + vbInformation, "Taxas"
                End If
                rsAnalises.Close
                Set rsAnalises = Nothing
            Next
        End If
    End If
End Sub

Private Sub BtAlteraUtente_Click()
    FormIdUtentePesq.Show
    FormFactusEnvio.Enabled = False
    Set gFormActivo = FormIdUtentePesq
End Sub

' ---------------------------------------------------------------------------

' TROCA A ORDEM COM A LINHA DE BAIXO

' ---------------------------------------------------------------------------
Private Sub BtBaixo_Click()
    
    On Error Resume Next
    
    Dim rv As Integer
    Dim l1 As Integer
    Dim l2 As Integer
    FLG_ExecutaFGAna = False
    
        
    If (LINHA_SEL >= EF_ReqFact(ReqActual).TotalAnalisesFact) Then
        Exit Sub
    End If
    
    l1 = LINHA_SEL
    If EF_ReqFact(ReqActual).AnalisesFact(l1).Flg_Facturada = gEstadoFactRequisFacturado Or EF_ReqFact(ReqActual).AnalisesFact(l1 + 1).Flg_Facturada = gEstadoFactRequisFacturado Then
        Exit Sub
    End If
    rv = Troca_Linha(l1 + 1, l1)
    If (rv = 1) Then
        LINHA_SEL = l1 + 1
    End If
    FGAnalises.row = LINHA_SEL + 1
    FGAnalises.Col = 0
    FGAnalises_click
    FLG_ExecutaFGAna = True

End Sub

' ---------------------------------------------------------------------------

' TROCA A ORDEM COM A LINHA DE CIMA

' ---------------------------------------------------------------------------
Private Sub BtCima_Click()

    On Error Resume Next
    
    Dim rv As Integer
    Dim l1 As Integer
    Dim l2 As Integer
    FLG_ExecutaFGAna = False

    
    If LINHA_SEL <= 1 Or LINHA_SEL > EF_ReqFact(ReqActual).TotalAnalisesFact Then Exit Sub
    l1 = LINHA_SEL
    If EF_ReqFact(ReqActual).AnalisesFact(l1).Flg_Facturada = gEstadoFactRequisFacturado Or EF_ReqFact(ReqActual).AnalisesFact(l1 - 1).Flg_Facturada = gEstadoFactRequisFacturado Then
        Exit Sub
    End If
        
    rv = Troca_Linha(l1 - 1, l1)
    If (rv = 1) Then
        LINHA_SEL = l1 - 1
    End If
    
    
    FGAnalises.row = LINHA_SEL - 1
    FGAnalises.Col = 0
    FGAnalises_click
    FLG_ExecutaFGAna = True

End Sub

Private Sub BtCodBarras_Click()
    If FrameCodBarras.Visible = True Then
        FrameCodBarras.Visible = False
    Else
        FrameCodBarras.Visible = True
    End If
    
End Sub

Private Sub BtDadosFact_Click()
    If FrameDadosFact.Visible = True Then
        FrameDadosFact.Visible = False
    Else
        FrameDadosFact.top = 6960
        FrameDadosFact.left = 8520
        FrameDadosFact.Visible = True
    End If
End Sub

Private Sub BtEtiq_Click()
    ImprimeEtiq
End Sub

Private Sub BtGestReq_Click()
    If EcNumReq <> "" Then
        FormGestaoRequisicaoPrivado.Show
        Set gFormActivo = FormGestaoRequisicaoPrivado
        FormGestaoRequisicaoPrivado.EcNumReq = EF_ReqFact(ReqActual).NReq
        FormGestaoRequisicaoPrivado.FuncaoProcurar
    End If
End Sub


Private Sub BtIdentif_Click()
    Dim i As Long
    If EcNumReq <> "" Then
        FormIdentificaUtente.Show
        Set gFormActivo = FormIdentificaUtente
        FormIdentificaUtente.EcUtente = EF_ReqFact(ReqActual).Utente
        For i = 0 To FormIdentificaUtente.EcDescrTipoUtente.ListCount - 1
            If FormIdentificaUtente.EcDescrTipoUtente.List(i) = EF_ReqFact(ReqActual).TipoUtente Then
                FormIdentificaUtente.EcDescrTipoUtente.ListIndex = i
            End If
        Next
        FormIdentificaUtente.FuncaoProcurar
    End If
End Sub

Private Sub BtImprimeResumo_Click()
    Dim continua As Boolean
    If EF_ReqFact(ReqActual).TotalAnalisesFact = 0 Then Exit Sub
    'Printer Common Dialog
    continua = BL_IniciaReport("EnvioFactusResumo", "An�lises a Facturar Entidade", crptToPrinter)
    
        
    
    PreencheTabelaTemporaria
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
        
    'F�rmulas do Report
    'Report.Formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.text)
    Report.SQLQuery = "SELECT * from sl_cr_envio_factus WHERE sl_cr_envio_factus.nome_computador = " & BL_TrataStringParaBD(gComputador)
    Report.SQLQuery = Report.SQLQuery & " AND sl_cr_envio_factus.num_sessao = " & gNumeroSessao & " AND n_req  =" & EF_ReqFact(ReqActual).NReq
    Call BL_ExecutaReport

End Sub

Private Sub BtMinimizar_Click()
    FGAnalises.Height = 4260
    Frame2.Height = 4695
    Frame2.top = 3000
End Sub

Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
End Sub

Private Sub BtPesquisaMed_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False
    
    ChavesPesq(1) = "nome_med"
    CamposEcran(1) = "nome_med"
    Tamanhos(1) = 4000
    Headers(1) = "Nome"
    
    ChavesPesq(2) = "cod_med"
    CamposEcran(2) = "cod_med"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_medicos"
    CampoPesquisa = "nome_med"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY nome_med ", " M�dico")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMed.Text = resultados(2)
            EcNomeMed.Text = resultados(1)
            BtPesquisaMed.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem m�dicos", vbExclamation, "M�dicos"
        EcCodMed.SetFocus
    End If

End Sub

Private Sub BtPesquisaSala_Click()
       PA_PesquisaSala EcCodSala, EcDescrSala, ""

End Sub


Private Sub BtVerificEstadoFact_Click()
    Dim estado As Integer
    On Error GoTo TrataErro
    
    estado = IF_VerificaEstadoFactReq(EF_ReqFact(ReqActual).NReq)
    
    If estado = 0 Then
        BG_Mensagem mediMsgBox, "Requisi��o n�o est� no FACTUS.", vbOKOnly + vbInformation, " Verifica��o de Estado"
    ElseIf estado = 1 Or estado = 4 Then
        BG_Mensagem mediMsgBox, "Requisi��o j� est� no FACTUS, mas est� incoerente.", vbOKOnly + vbInformation, " Verifica��o de Estado"
    ElseIf estado = 2 Then
        BG_Mensagem mediMsgBox, "Requisi��o n�o est� no FACTUS, mas est� incoerente.", vbOKOnly + vbInformation, " Verifica��o de Estado"
    ElseIf estado = 3 Then
        BG_Mensagem mediMsgBox, "Requisi��o j� est� no FACTUS", vbOKOnly + vbInformation, " Verifica��o de Estado"
    ElseIf estado = 5 Then
        BG_Mensagem mediMsgBox, "Requisi��o rejeitada no FACTUS mas n�o no SISLAB.", vbOKOnly + vbInformation, " Verifica��o de Estado"
    ElseIf estado = 6 Then
        BG_Mensagem mediMsgBox, "Requisi��o rejeitada.", vbOKOnly + vbInformation, " Verifica��o de Estado"
    End If
    CorrigeRequisicao EF_ReqFact(ReqActual).NReq, estado
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Verificar Estados: " & Err.Number & " - " & Err.Description, Me.Name, "BtVerificEstadoFact_Click", True
    Exit Sub
    Resume Next
End Sub

Private Sub CbConvencao_Click()
    If CbConvencao.ListIndex > -1 Then
        EF_ReqFact(ReqActual).convencao = CbConvencao.ItemData(CbConvencao.ListIndex)
    End If
End Sub


Public Sub EcCodMed_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodMed.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT nome_med FROM sl_medicos WHERE cod_med=" & BL_TrataStringParaBD(Trim(EcCodMed.Text))
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            DoEvents
            EcCodMed.Text = ""
            EcNomeMed.Text = ""
            EF_ReqFact(ReqActual).codMed = ""
        Else
            EcNomeMed.Text = Tabela!nome_med
            EF_ReqFact(ReqActual).codMed = EcCodMed
        End If
        Tabela.Close
        Set Tabela = Nothing
        
    Else
        EcNomeMed.Text = ""
    End If
    
End Sub

Public Sub EcCodsala_Validate(Cancel As Boolean)
    Cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
End Sub

Public Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, "")
End Sub


' ---------------------------------------------------------------------------

' RETIRA A AN�LISE DO ENVIO PARA FACTURACAO

' ---------------------------------------------------------------------------
Private Sub BtRetira_Click()
    
    On Error Resume Next
    
    If LINHA_SEL > EF_ReqFact(ReqActual).TotalAnalisesFact Then Exit Sub
    If EF_ReqFact(ReqActual).AnalisesFact(LINHA_SEL).Flg_Facturada = gEstadoFactRequisFacturado Then
        Exit Sub
    End If
    FLG_ExecutaFGAna = False
    If (BG_Mensagem(mediMsgBox, "Deseja n�o facturar " & _
                                FGAnalises.TextMatrix(LINHA_SEL, lColFactDescrAna) & " ?      ", _
                                vbQuestion + vbYesNo + vbDefaultButton2, _
                                " " & cAPLICACAO_NOME_CURTO) = vbNo) Then
        ' Sa�.
        Exit Sub
    Else
        'FLG_ExecutaFGAna = False
        BL_MudaCorFg Me.FGAnalises, CLng(LINHA_SEL), CorRetirada
        RetiraAnalise (LINHA_SEL)
    End If
    FLG_ExecutaFGAna = True

End Sub

Private Sub CbCodUrbano_Click()

    If CbCodUrbano.ListIndex <> mediComboValorNull Then
        If CbCodUrbano.ListIndex <> mediComboValorNull Then
            EcKm.Enabled = True
            EcKm.SetFocus
        Else
            EcKm.Text = ""
            EcKm.Enabled = False
            EF_ReqFact(ReqActual).km = 0
        End If
        EF_ReqFact(ReqActual).codUrbano = BG_DaComboSel(CbCodUrbano)
    End If

End Sub

Private Sub CbCodUrbano_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 Then
        BG_LimpaOpcao CbCodUrbano, KeyCode
        EcKm.Text = ""
    End If
    
End Sub



Private Sub CkActivarCodBarras_Click()
    If CkActivarCodBarras.value = vbChecked Then
        LbCred = "1"
        lCredencialActiva = 1
        EcCodAnaEFR.Enabled = True
    Else
        LbCred = ""
        EcCodAnaEFR = ""
        EcCodAnaEFR.Enabled = False
        'LbCodRubrEFR = ""
    End If
End Sub

Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        EcAux_LostFocus
    End If
End Sub

Private Sub EcAux_LostFocus()
    Dim i As Integer
    Dim flg_alterouOrdem As Boolean
    Dim num_credencial As String
    EcAux = UCase(EcAux)
    If COLUNA_EDITA = lColFactP1 Then
        'RGONCALVES 23.07.2013 LJMANSO-109
        num_credencial = EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).p1
        '
        For i = LINHA_EDITA To EF_ReqFact(ReqActual).TotalAnalisesFact
            'RGONCALVES 23.07.2013 LJMANSO-109
            If num_credencial <> EF_ReqFact(ReqActual).AnalisesFact(i).p1 Then
                Exit For
            End If
            '
            If EcAux <> "" Then
                AlteraP1 CLng(i), EcAux
            End If
        Next
        CalculaCredenciais
    ElseIf COLUNA_EDITA = lColFactQuantidade Then
        If EcAux <> "" Then
            FGAnalises.TextMatrix(LINHA_EDITA, lColFactQuantidade) = CInt(BL_HandleNull(EcAux, 1))
            EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).qtd_ana_envio = CInt(BL_HandleNull(EcAux, 1))
            AlteraQuantidade CLng(LINHA_EDITA), EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).qtd_ana_envio
        End If
    ElseIf COLUNA_EDITA = lColFactEFR Then
        If EcAux <> "" Then
            If (BG_Mensagem(mediMsgBox, "Deseja alterar a entidade para todas an�lises?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
                For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
                    AlteraEntidade CLng(i), EcAux
                    
                Next
            Else
                AlteraEntidade CLng(LINHA_EDITA), EcAux
            End If
        End If
    ElseIf COLUNA_EDITA = lColFactMedico Then
        If EcAux <> "" Then
            For i = LINHA_EDITA To EF_ReqFact(ReqActual).TotalAnalisesFact
                If EF_ReqFact(ReqActual).AnalisesFact(i).cod_efr = EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).cod_efr And EF_ReqFact(ReqActual).AnalisesFact(i).p1 = EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).p1 Then
                    FGAnalises.TextMatrix(i, lColFactMedico) = BL_HandleNull(EcAux, 1)
                    EF_ReqFact(ReqActual).AnalisesFact(i).cod_medico = BL_HandleNull(EcAux, 1)
                    AlteraMedico CLng(LINHA_EDITA), EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).cod_medico
                End If
            Next
        End If
    ElseIf COLUNA_EDITA = lColFactNrBenef Then
        If EcAux <> "" Then
            EcAux = UCase(EcAux)
            For i = LINHA_EDITA To EF_ReqFact(ReqActual).TotalAnalisesFact
                'If EF_ReqFact(ReqActual).AnalisesFact(i).cod_efr = EF_ReqFact(ReqActual).AnalisesFact(LINHA_SEL).cod_efr And EF_ReqFact(ReqActual).AnalisesFact(i).p1 = EF_ReqFact(ReqActual).AnalisesFact(LINHA_SEL).p1 Then
                If EF_ReqFact(ReqActual).AnalisesFact(i).cod_efr = EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).cod_efr Then
                    FGAnalises.TextMatrix(i, lColFactNrBenef) = BL_HandleNull(EcAux, 1)
                    EF_ReqFact(ReqActual).AnalisesFact(i).nr_benef = BL_HandleNull(EcAux, 1)
                    AlteraNrBenef CLng(i), EF_ReqFact(ReqActual).AnalisesFact(i).nr_benef
                End If
            Next
        End If
    ElseIf COLUNA_EDITA = lColFactCodAna Then
        If EcAux <> "" Then
            EcAux = UCase(EcAux)
            AdicionaAnalisePos EcAux, True, FGAnalises.row
        End If
    ElseIf COLUNA_EDITA = lColFactPercent Then
        If EcAux <> "" Then
            EcAux = UCase(EcAux)
            If (BG_Mensagem(mediMsgBox, "Deseja alterar a percentagem para todas an�lises?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
                For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
                    If EF_ReqFact(ReqActual).AnalisesFact(i).cod_efr = EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).cod_efr And EF_ReqFact(ReqActual).AnalisesFact(i).nRec = EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).nRec Then
                        FGAnalises.TextMatrix(i, lColFactPercent) = CDbl(BL_HandleNull(EcAux, 1))
                        EF_ReqFact(ReqActual).AnalisesFact(i).PercDescEnvio = CDbl(BL_HandleNull(EcAux, 1))
                    End If
                Next
            Else
                FGAnalises.TextMatrix(LINHA_EDITA, lColFactPercent) = CDbl(BL_HandleNull(EcAux, 1))
                EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).PercDescEnvio = CDbl(BL_HandleNull(EcAux, 1))
            End If
        End If
    ElseIf COLUNA_EDITA = lColFactPercent Then
        If EcAux <> "" Then
            EcAux = UCase(EcAux)
            If (BG_Mensagem(mediMsgBox, "Deseja alterar a percentagem para todas an�lises?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
                For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
                    If EF_ReqFact(ReqActual).AnalisesFact(i).cod_efr = EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).cod_efr And EF_ReqFact(ReqActual).AnalisesFact(i).nRec = EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).nRec Then
                        FGAnalises.TextMatrix(i, lColFactPercent) = CDbl(BL_HandleNull(EcAux, 1))
                        EF_ReqFact(ReqActual).AnalisesFact(i).PercDescEnvio = CDbl(BL_HandleNull(EcAux, 1))
                    End If
                Next
            Else
                FGAnalises.TextMatrix(LINHA_EDITA, lColFactPercent) = CDbl(BL_HandleNull(EcAux, 1))
                EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).PercDescEnvio = CDbl(BL_HandleNull(EcAux, 1))
            End If
        End If
    ElseIf COLUNA_EDITA = lColFactOrdem Then
        If EcAux <> "" Then
            EcAux = UCase(EcAux)
            If (BG_Mensagem(mediMsgBox, "Esta altera��o pode afectar outras an�lises. Tem a certeza?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
                FGAnalises.TextMatrix(LINHA_EDITA, lColFactOrdem) = CInt(BL_HandleNull(EcAux, 1))
                If CInt(EcAux) <> EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).ordem Then
                    flg_alterouOrdem = True
                Else
                    flg_alterouOrdem = False
                End If
                EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).ordem = CInt(BL_HandleNull(EcAux, 1))
                If flg_alterouOrdem = True Then
                    GereOrdens LINHA_EDITA, EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).ordem
                End If
            End If
        End If
    End If
    EcAux = ""
    EcAux.Visible = False
    FGAnalises.SetFocus
End Sub




Private Sub EcCodAna_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 13 Then
        EcCodAna.Text = UCase(EcCodAna.Text)
        AdicionaAnalisePos EcCodAna, False
        EcCodAna = ""
    End If
End Sub
Private Sub EcCodAnaEFR_KeyUp(KeyCode As Integer, Shift As Integer)
If EcCodAnaEFR = "+" Or EcCodAnaEFR = "-" Then
    EcCodAnaEFR = ""
End If
End Sub



Private Sub EcDtChega_LostFocus()
    If ReqActual <= EF_TotalRequisicoes Then
        EF_ReqFact(ReqActual).dt_chega = EcDtChega
    End If
End Sub

Private Sub EcKm_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcKm)
    If EcKm.Text = "" Then
        EcKm = "0"
    End If
    EF_ReqFact(ReqActual).km = IIf(EcKm.Text = "", 0, EcKm.Text)
    
End Sub


Private Sub EcNumBenef_Validate(Cancel As Boolean)
    If ReqActual <= EF_TotalRequisicoes Then
        EF_ReqFact(ReqActual).n_benef = EcNumBenef.Text
    End If
End Sub

Private Sub EcNumReq_Click()

    'EcNumReq.text = ""
    DoEvents
    
End Sub

Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    If EcNumReq <> "" And KeyCode = 13 Then
        FuncaoProcurar
    End If
End Sub

Private Sub EcNumReq_LostFocus()
    
    EcNumReq.Tag = adDecimal
    If Not BG_ValidaTipoCampo_ADO(Me, EcNumReq) Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub




Private Sub FgAnalises_dblClick()
    If FGAnalises.Col = lColFactP1 Then
        If FGAnalises.TextMatrix(FGAnalises.row, lColFactP1) <> "" And EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Facturada <> gEstadoFactRequisFacturado And EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Retirada = False Then
            EcAux = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).p1
            EcAux.left = FGAnalises.CellLeft + 100
            EcAux.top = FGAnalises.CellTop + 220
            EcAux.Width = FGAnalises.CellWidth + 20
            EcAux.Visible = True
            EcAux.SetFocus
            LINHA_EDITA = FGAnalises.row
            COLUNA_EDITA = FGAnalises.Col
        End If
    ElseIf FGAnalises.Col = lColFactQuantidade Then
        If FGAnalises.TextMatrix(FGAnalises.row, lColFactQuantidade) <> "" And EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Facturada <> gEstadoFactRequisFacturado And EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Retirada = False Then
            EcAux = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).qtd_ana_envio
            EcAux.left = FGAnalises.CellLeft + 100
            EcAux.top = FGAnalises.CellTop + 220
            EcAux.Width = FGAnalises.CellWidth + 20
            EcAux.Visible = True
            EcAux.SetFocus
            LINHA_EDITA = FGAnalises.row
            COLUNA_EDITA = FGAnalises.Col
        End If
    ElseIf FGAnalises.Col = lColFactMedico Then
        If EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Facturada <> gEstadoFactRequisFacturado And EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Retirada = False Then
            EcAux = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).cod_medico
            EcAux.left = FGAnalises.CellLeft + 100
            EcAux.top = FGAnalises.CellTop + 220
            EcAux.Width = FGAnalises.CellWidth + 20
            EcAux.Visible = True
            EcAux.SetFocus
            LINHA_EDITA = FGAnalises.row
            COLUNA_EDITA = FGAnalises.Col
        End If
    ElseIf FGAnalises.Col = lColFactNrBenef Then
        If EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Facturada <> gEstadoFactRequisFacturado And EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Retirada = False Then
            EcAux = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).nr_benef
            EcAux.left = FGAnalises.CellLeft + 100
            EcAux.top = FGAnalises.CellTop + 220
            EcAux.Width = FGAnalises.CellWidth + 20
            EcAux.Visible = True
            EcAux.SetFocus
            LINHA_EDITA = FGAnalises.row
            COLUNA_EDITA = FGAnalises.Col
        End If
    ElseIf FGAnalises.Col = lColFactEFR Then
        If EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Facturada <> gEstadoFactRequisFacturado And EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Retirada = False Then
            EcAux = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).cod_efr
            EcAux.left = FGAnalises.CellLeft + 100
            EcAux.top = FGAnalises.CellTop + 220
            EcAux.Width = FGAnalises.CellWidth + 20
            EcAux.Visible = True
            EcAux.SetFocus
            LINHA_EDITA = FGAnalises.row
            COLUNA_EDITA = FGAnalises.Col
        End If
    ElseIf FGAnalises.Col = lColFactCodAna Then
        If FGAnalises.row <= EF_ReqFact(ReqActual).TotalAnalisesFact Then
            If EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).codAna = "" Then
                EcAux = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).codAna
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        End If
    ElseIf FGAnalises.Col = lColFactPercent Then
        If FGAnalises.row <= EF_ReqFact(ReqActual).TotalAnalisesFact Then
            If EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Facturada <> gEstadoFactRequisFacturado And EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Retirada = False Then
                EcAux = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).PercDescEnvio
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        End If
        
    ElseIf FGAnalises.Col = lColFactOrdem Then
        If FGAnalises.row <= EF_ReqFact(ReqActual).TotalAnalisesFact Then
            If EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Facturada <> gEstadoFactRequisFacturado And EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Retirada = False Then
                EcAux = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).ordem
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        End If
    ElseIf FGAnalises.Col = lColFactPortaria Then
        If FGAnalises.row <= EF_ReqFact(ReqActual).TotalAnalisesFact Then
            If EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Facturada <> gEstadoFactRequisFacturado And EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Retirada = False Then
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
                AlteraPortaria EF_ReqFact(ReqActual).codEfr
            End If
        End If
    
    End If
End Sub

Private Sub FgAnalises_KeyDown(KeyCode As Integer, Shift As Integer)
    If FGAnalises.row >= 1 And FGAnalises.Col = lColFactP1 And (KeyCode = 86 Or KeyCode = 66) Then
        
        AlteraCorP1 (KeyCode)
        Exit Sub
    ElseIf FGAnalises.row >= 1 And FGAnalises.row <= EF_ReqFact(ReqActual).TotalAnalisesFact Then
        If KeyCode = 119 Then               'F8 - ISENTO
            AlteraIsencao (gTipoIsencaoIsento)
            Exit Sub
        ElseIf KeyCode = 120 Then               'F9 - NAO ISENTO
            AlteraIsencao (gTipoIsencaoNaoIsento)
            Exit Sub
        ElseIf KeyCode = 121 Then               'F10 - BORLA
            AlteraIsencao (gTipoIsencaoBorla)
            Exit Sub
        ElseIf KeyCode = vbKeyReturn Then
            FgAnalises_dblClick
        End If
    End If

End Sub

Private Sub FGAnalises_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'soliveira correccao lacto
    'Possibilidade de desactivar retirada da an�lise
    If FGAnalises.row > EF_ReqFact(ReqActual).TotalAnalisesFact Then
        Exit Sub
    End If
    If Button = 2 And EF_ReqFact(ReqActual).TotalAnalisesFact > 0 Then
        If EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).Flg_Retirada = True And FGAnalises.row > 0 Then
            MDIFormInicio.PopupMenu MDIFormInicio.HIDE_FACTANA
        ElseIf FGAnalises.row > 0 Then
            MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANA
        End If
    End If
      
    ' pferreira 2010.05.17
    ' DRAG'N'DROP
    With FGAnalises
        If (.MouseRow > 0 And .MouseRow <= EF_ReqFact(ReqActual).TotalAnalisesFact) Then
            is_dragging = True
            source_row = .row
            deliver_row = .MouseRow
            FGAnalises_click
        End If
    End With
    
End Sub

Private Sub FGAnalises_RowColChange()
    If FLG_ExecutaFGAna = True Then
        FGAnalises_click
    End If
End Sub

Public Sub FgReq_Click()
    Dim i As Long
    LimpaCamposEcra
    LimpaFgAnalises
    If FgReq.row > EF_TotalRequisicoes Then
        Exit Sub
    End If
    ReqActual = FgReq.row
    
    EcNumReq = EF_ReqFact(ReqActual).NReq
    EcTipoUte = EF_ReqFact(ReqActual).TipoUtente
    EcUtente = EF_ReqFact(ReqActual).Utente
    EcNome = EF_ReqFact(ReqActual).NomeUtente
    EcCodSala = EF_ReqFact(ReqActual).cod_sala
    EcCodsala_Validate False
    EcCodEFR = EF_ReqFact(ReqActual).codEfr
    EcDescrEFR = EF_ReqFact(ReqActual).DescrEFR
    EcCodMed = EF_ReqFact(ReqActual).codMed
    EcNomeMed = BL_SelCodigo("SL_MEDICOS", "nome_med", "cod_med", EF_ReqFact(ReqActual).codMed, "V")
    EcNumBenef = EF_ReqFact(ReqActual).n_benef
    EcDataNasc = EF_ReqFact(ReqActual).DataNasc
    EcReqAssoc = EF_ReqFact(ReqActual).NReqAssoc
    If EcDataNasc <> "" Then
        EcIdade = BG_CalculaIdade(CDate(EcDataNasc.Text), CDate(EF_ReqFact(ReqActual).dt_chega))
    End If
    EcKm = EF_ReqFact(ReqActual).km
    EcCodPais = EF_ReqFact(ReqActual).codPais
    EcCodpais_Validate False
    EcDoenteProf = EF_ReqFact(ReqActual).numDoeProf
    EcCodProven = EF_ReqFact(ReqActual).cod_proven
    EcCodProven_Validate False
    If EF_ReqFact(ReqActual).Sexo = gT_Masculino Then
        EcSexo = "Masculino"
    ElseIf EF_ReqFact(ReqActual).Sexo = gT_Feminino Then
        EcSexo = "Feminino"
    End If
    LbLote.caption = EF_ReqFact(ReqActual).n_fac & " " & EF_ReqFact(ReqActual).n_lote
    LbUserEnvio.caption = EF_ReqFact(ReqActual).dt_envio & " - " & BL_SelCodigo("sl_idutilizador", "UTILIZADOR", "COD_UTILIZADOR", EF_ReqFact(ReqActual).user_envio, "V")
    If EF_ReqFact(ReqActual).SeqUtenteFact > -1 Then
        LbUtenteFact.Visible = True
    Else
        LbUtenteFact.Visible = False
    End If
    
    If EF_ReqFact(ReqActual).fimSemana = 1 Then
        LbFimSemana.Visible = True
    Else
        LbFimSemana.Visible = False
    End If
    
    EcDtChega = EF_ReqFact(ReqActual).dt_chega
    
    
    ' ---------
    ' ISENCAO
    ' --------
    If EF_ReqFact(ReqActual).codIsencao <> "" Then
        For i = 0 To CbTipoIsencao.ListCount - 1
            If EF_ReqFact(ReqActual).codIsencao = CbTipoIsencao.ItemData(i) Then
                CbTipoIsencao.ListIndex = i
                Exit For
            End If
        Next
    End If
    
    ' ---------
    ' CONVENCAO
    ' ---------
    For i = 0 To CbConvencao.ListCount - 1
        If EF_ReqFact(ReqActual).convencao = CbConvencao.ItemData(i) Then
            CbConvencao.ListIndex = i
            Exit For
        End If
    Next
    
    ' ---------
    ' URBANO
    ' ---------
    For i = 0 To CbCodUrbano.ListCount - 1
        If EF_ReqFact(ReqActual).codUrbano = CbCodUrbano.ItemData(i) Then
            CbCodUrbano.ListIndex = i
            Exit For
        End If
    Next
    
    ' ---------
    ' HEMODIALISE
    ' ---------
    For i = 0 To CbHemodialise.ListCount - 1
        If EF_ReqFact(ReqActual).hemodialise = CbHemodialise.ItemData(i) Then
            CbHemodialise.ListIndex = i
            Exit For
        End If
    Next
        
    If EF_ReqFact(ReqActual).estadoFact = gEstadoFactRequisFacturado Then
        StatusBar1.Panels(1).Text = "Requisi��o Facturada"
    ElseIf EF_ReqFact(1).estadoFact = gEstadoFactRequisRejeitado Then
        StatusBar1.Panels(1).Text = "Requisi��o Rejeitada"
    ElseIf EF_ReqFact(1).estadoFact = gEstadoFactRequisNaoFacturado Then
        StatusBar1.Panels(1).Text = "Requisi��o N�o Facturada"
    End If
    
    EcEstado = BL_DevolveEstadoReq(EF_ReqFact(ReqActual).EstadoReq)
                                                
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    
    DoEvents
    DoEvents
    DoEvents
    FuncaoProcurar_continua FgReq.row
    flg_pergunta = True
End Sub

Private Sub FuncaoProcurar_continua(linhaR As Long)
    Dim i As Integer
    FLG_ExecutaFGAna = False
    FGAnalises.Visible = False
    FrameAccao.Visible = True
    LbAccao = " A Preencher An�lises....."
    DoEvents
    
    EF_PreencheAnalises linhaR
    
    For i = 1 To EF_ReqFact(linhaR).TotalAnalisesFact
        PreencheFGAna linhaR, i
        FLG_ExecutaFGAna = False
        AvaliaCorFG (i)
        FLG_ExecutaFGAna = True
        
        ' ---------------------------------------------------------------------------
        ' AUMENTA GRELHA E ESTRUTURA
        ' ---------------------------------------------------------------------------
        If i >= (NumLinhas - 1) Then
            FGAnalises.AddItem ""
        End If
    Next
    FGAnalises.row = 1
    FrameAccao.Visible = False
    FGAnalises.Visible = True
    
    CalculaCredenciais
    VerificaEFRPagaActoMedico (EF_ReqFact(linhaR).codEfr)
            
    BtRetira.Enabled = True
    BtAdiciona.Enabled = True
    BtCima.Enabled = True
    BtBaixo.Enabled = True
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        BtValida.Enabled = True
    End If
    BtEtiq.Enabled = True
    BtIdentif.Enabled = True
    BtGestReq.Enabled = True
    BtVerificEstadoFact.Enabled = True
    DoEvents
    
    LbCred = "1"
    lCredencialActiva = 1
    lPosicaoActual = 1
    FLG_ExecutaFGAna = True
End Sub
Private Sub Form_Activate()

    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub FuncaoProcurar()
    
    On Error Resume Next
    
    Dim sRet As String
    
    Me.BtValida.Enabled = False
    BtEtiq.Enabled = False
    DoEvents
    
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    DoEvents
    EF_TotalRequisicoes = 0
    ReDim EF_ReqFact(ReqActual).AnalisesFact(EF_TotalRequisicoes)
    
            
    Preenche_Dados_Requisicao
    FLG_ExecutaFGAna = True
End Sub
    
' ---------------------------------------------------------------------------

' PREENCHE TODOS OS DADOS DA REQUISICAO E DO UTENTE EM CAUSA

' ---------------------------------------------------------------------------

Function Preenche_Dados_Requisicao() As Boolean
        
    On Error GoTo TrataErro
    
    Dim sSql As String
    Dim rsReq As ADODB.recordset
    Dim i As Integer
    Dim flg_criterio As Boolean
    FrameAccao.Visible = True
    LbAccao = " A Preencher Requisi��es...."
    DoEvents
    ' ---------------------------------------------------------------------------
    ' LIMPA ESTRUTURA DAS REQUISICOES A FACTURAR
    ' ---------------------------------------------------------------------------
    LimpaEF_ReqFact
    
    Preenche_Dados_Requisicao = True
    flg_criterio = False
    
    sSql = ConstroiCriterio(flg_criterio)
    Set rsReq = New ADODB.recordset
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsReq.Open sSql, gConexao
        
    If (rsReq.RecordCount <= 0) Then
        Preenche_Dados_Requisicao = False
        MsgBox "Requisi��o inexistente.    ", vbExclamation, Me.caption
        rsReq.Close
        Set rsReq = Nothing
        Exit Function
    Else
        EcCodEFR.Enabled = False
        BtPesquisaEntFin.Enabled = False
        While Not rsReq.EOF
            
            Call EF_PreencheRequisicao(BL_HandleNull(rsReq!n_req, ""), BL_HandleNull(rsReq!dt_chega, ""), BL_HandleNull(rsReq!user_cri, ""), _
                                        BL_HandleNull(rsReq!hr_chega, ""), BL_HandleNull(rsReq!n_benef, ""), BL_HandleNull(rsReq!cod_isencao, ""), _
                                        BL_HandleNull(rsReq!Flg_Facturado, -1), BL_HandleNull(rsReq!cod_urbano, 0), BL_HandleNull(rsReq!km, 0), _
                                        BL_HandleNull(rsReq!cod_efr, ""), BL_HandleNull(rsReq!cod_med, ""), BL_HandleNull(rsReq!seq_utente, ""), _
                                        BL_HandleNull(rsReq!t_utente, ""), BL_HandleNull(rsReq!Utente, ""), BL_HandleNull(rsReq!nome_ute, ""), _
                                        BL_HandleNull(rsReq!dt_nasc_ute, ""), BL_HandleNull(rsReq!sexo_ute, ""), BL_HandleNull(rsReq!n_cartao_ute, ""), _
                                        BL_HandleNull(rsReq!convencao, -1), BL_HandleNull(rsReq!hemodialise, -1), BL_HandleNull(rsReq!fim_semana, 0), _
                                        BL_HandleNull(rsReq!cod_sala, ""), BL_HandleNull(rsReq!seq_utente_fact, -1), BL_HandleNull(rsReq!descr_efr, ""), _
                                        BL_HandleNull(rsReq!flg_perc_facturar, "0"), BL_HandleNull(rsReq!flg_controla_isencao, "0"), _
                                        BL_HandleNull(rsReq!flg_obriga_recibo, "0"), BL_HandleNull(rsReq!deducao_ute, ""), BL_HandleNull(rsReq!flg_acto_med_efr, "0"), _
                                        BL_HandleNull(rsReq!cod_empresa, ""), BL_HandleNull(rsReq!flg_compart_dom, "0"), BL_HandleNull(rsReq!flg_dom_defeito, "0"), _
                                        BL_HandleNull(rsReq!flg_nao_urbano, "0"), BL_HandleNull(rsReq!flg_65anos, "0"), BL_HandleNull(rsReq!flg_nova_ars, 0), _
                                        BL_HandleNull(rsReq!flg_separa_dom, "0"), BL_HandleNull(rsReq!flg_insere_cod_rubr_efr, "0"), BL_HandleNull(rsReq!flg_adse, 0), _
                                        BL_HandleNull(rsReq!n_Req_assoc, ""), BL_HandleNull(rsReq!estado_req, " "), BL_HandleNull(rsReq!cod_pais, ""), _
                                        BL_HandleNull(rsReq!num_doe_prof, ""), BL_HandleNull(rsReq!cod_postal, ""), BL_HandleNull(rsReq!descr_postal, ""), _
                                        BL_HandleNull(rsReq!cod_proven, ""), BL_HandleNull(rsReq!cod_proven_efr, ""), BL_HandleNull(rsReq!descr_proven, ""), _
                                        BL_HandleNull(rsReq!nome_dono, ""))
            
            FgReq.TextMatrix(EF_TotalRequisicoes, 0) = EF_ReqFact(EF_TotalRequisicoes).NReq
            FgReq.AddItem ""
            rsReq.MoveNext
        Wend
        FgReq.row = 1
        FgReq_Click
    End If
         'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
        Dim requestJson As String
        Dim responseJson As String
        Dim CamposEcra() As Object
        Dim NumCampos As Integer
        NumCampos = 7
        ReDim CamposEcra(0 To NumCampos - 1)
         Set CamposEcra(0) = EcNome
         Set CamposEcra(1) = EcTipoUte
         Set CamposEcra(2) = EcUtente
         Set CamposEcra(3) = EcNumReq
         Set CamposEcra(4) = EcEstado
         Set CamposEcra(5) = EcDataNasc
         Set CamposEcra(6) = EcNumBenef
        
        requestJson = BL_TrataCamposRGPD(CamposEcra)
        responseJson = BL_TrataListBoxRGPD(Nothing, rsReq, False)
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
    End If
    rsReq.Close
    Set rsReq = Nothing
    Preenche_Dados_Requisicao = True
    FrameAccao.Visible = False
Exit Function
TrataErro:
    BG_LogFile_Erros Me.Name & "Preenche_Dados_Requisicao :" & Err.Description
    BG_Mensagem mediMsgBox, "Erro ao preencher Requisi��o: " & Err.Description, vbOKOnly + vbInformation, "Preencher Requisi��o"
    Preenche_Dados_Requisicao = False
    Exit Function
    Resume Next
End Function


Sub FuncaoLimpar()
    
    EcNumReq.Text = ""
    EcCredencial.Text = ""
    Me.EcNumReq.ForeColor = &H8000&
    LimpaCamposEcra
    EcNumReq.SetFocus
    
    LimpaFgAnalises
    StatusBar1.Panels(1).Text = " " & Me.caption
    
    Me.BtRetira.Enabled = False
    Me.BtAdiciona.Enabled = False
    Me.BtCima.Enabled = False
    Me.BtBaixo.Enabled = False
    Me.BtValida.Enabled = False
    BtEtiq.Enabled = False
    LINHA_SEL = 0
    If ReqActual <= EF_TotalRequisicoes Then
        EF_ReqFact(ReqActual).TotalAnalisesFact = 0
    End If
    BtIdentif.Enabled = False
    BtGestReq.Enabled = False
    BtVerificEstadoFact.Enabled = False
    
    DoEvents
    DoEvents
    'soliveira correccao lacto
    'EcNumReq.SetFocus
    LbTotalFactEFR = ""
    LimpaEF_ReqFact
    EcCodEFR.Enabled = True
    BtPesquisaEntFin.Enabled = True
    FrameAccao.Visible = False
    
End Sub

Sub FuncaoModificar()
    
    Dim Erro As String
    Dim sql As String
    
    On Error GoTo Trata_Erro
    
    'Actualizar codigo urbano e km na tabela de requisi��es
    If CbCodUrbano.ListIndex <> mediComboValorNull Then
                
        If (Len(Trim(EcKm.Text)) = 0) Then
            EcKm.Text = "0"
        End If
        
        sql = "UPDATE sl_requis " & _
              "SET    cod_urbano = " & CbCodUrbano.ItemData(CbCodUrbano.ListIndex) & ", " & _
              "       km = " & IIf((EcKm.Text = ""), 0, EcKm.Text) & " " & _
              "WHERE  n_req =" & EcNumReq.Text
        
        BG_ExecutaQuery_ADO sql
    End If
    
    Exit Sub
Trata_Erro:
    BG_LogFile_Erros "FuncaoModificar (FormFACTUSEnvio):" & Err.Number & "-" & Err.Description & "(" & Erro & ")"
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_tbf_t_urbano", "cod_t_urbano", "descr_t_urbano", CbCodUrbano
    BG_PreencheComboBD_ADO "sl_isencao", "cod_isencao", "descr_isencao", CbTipoIsencao
    BG_PreencheComboBD_ADO "sl_tbf_convencao", "cod_convencao", "descr_convencao", CbConvencao
    BG_PreencheComboBD_ADO "sl_cod_hemodialise", "cod_hemodialise", "descr_hemodialise", CbHemodialise
    LbFimSemana.Visible = False
    EcAux.Visible = False
    FrameAccao.Visible = False
    LbCredenciais = ""
    
End Sub

' Seleciona uma linha da grid.
Private Sub FGAnalises_click()

    On Error Resume Next
    Dim coluna As Integer
    Dim msf As Object
    Dim i As Integer
    Dim linha As Integer
    If FLG_ExecutaFGAna = True Then
        FLG_ExecutaFGAna = False
        Set msf = FGAnalises
        coluna = FGAnalises.Col
        If ((msf.row = 0) Or (msf.row = msf.rows - 1)) Then
            FLG_ExecutaFGAna = True
            Exit Sub
        End If
        linha = msf.row
        coluna = msf.Col
        msf.row = LINHA_SEL
        msf.Col = lColFactLinhaSel
        FGAnalises.TextMatrix(LINHA_SEL, lColFactLinhaSel) = ""
        Set FGAnalises.CellPicture = Nothing
        
        msf.row = linha
        msf.Col = lColFactLinhaSel
        FGAnalises.TextMatrix(linha, lColFactLinhaSel) = "*"
        Pic2.BackColor = FGAnalises.CellBackColor
        Set FGAnalises.CellPicture = Pic2.Image
        
        LINHA_SEL = linha
        FGAnalises.Col = coluna
        COLUNA_SEL = coluna
        FLG_ExecutaFGAna = True
    End If
End Sub

' ---------------------------------------------------------------------------

' TROCA DUAS LINHAS NA ESTRUTURA E NA GRELHA

' ---------------------------------------------------------------------------

Private Function Troca_Linha(linha1 As Integer, linha2 As Integer) As Integer
    
    On Error GoTo TrataErro
    
    EF_TrocaLinhas ReqActual, linha1, linha2
    
    ' -----------------------------------------------
    ' GRAVA ALTERACAO DAS ORDENS NA TABELA DE RECIBOS
    ' -----------------------------------------------
    GravaOrdem linha1
    GravaOrdem linha2
    
    ' ---------------------------------------------------------------------------
    ' ALTERA AS LINHAS DA GRELHA E CORES
    ' ---------------------------------------------------------------------------
    AvaliaCorFG (linha1)
    AvaliaCorFG (linha2)
    
    PreencheFGAna ReqActual, linha1
    PreencheFGAna ReqActual, linha2
   
Exit Function
TrataErro:
    BL_LogFile_BD Me.Name, "Troca_Linha", Err.Number, Err.Description, ""
    Troca_Linha = -1
    Exit Function
End Function


' ---------------------------------------------------------------------------

' PREENCHE A ESTRUTURA COM AS ANALISES DA REQUISI��O

' ---------------------------------------------------------------------------

Private Sub BtValida_Click()

    On Error GoTo TrataErro
    Me.BtRetira.Enabled = False
    Me.BtAdiciona.Enabled = False
    Me.BtCima.Enabled = False
    Me.BtBaixo.Enabled = False
    Me.BtValida.Enabled = False
    BtEtiq.Enabled = False
    FGAnalises.Visible = False
    FrameAccao.Visible = True
    LbAccao = "A Enviar dados..."
    DoEvents
    Call EF_EnviaDados(ReqActual)
    FrameAccao.Visible = False
    FGAnalises.Visible = True
    If EF_ReqFact(ReqActual).estadoFact = gEstadoFactRequisFacturado Then
        StatusBar1.Panels(1).Text = "Requisi��o Facturada"
        FgReq.CellBackColor = vbGreen
    ElseIf EF_ReqFact(ReqActual).estadoFact = gEstadoFactRequisRejeitado Then
        StatusBar1.Panels(1).Text = "Requisi��o Rejeitada"
    ElseIf EF_ReqFact(ReqActual).estadoFact = gEstadoFactRequisNaoFacturado Then
        StatusBar1.Panels(1).Text = "Requisi��o N�o Facturada"
    End If
    If ReqActual < EF_TotalRequisicoes Then
        FgReq.row = FgReq.row + 1
        FgReq_Click
    Else
        FuncaoLimpar
    End If
    
Exit Sub
TrataErro:
    BL_LogFile_BD Me.Name, "BtValida_Click", Err.Number, Err.Description, ""
    BG_RollbackTransaction
    Exit Sub
    Resume Next
End Sub

Private Function Get_EstadoFact(REQ_ACTUAL As String) As Integer
    Dim sSql As String
    Dim RsEstado As ADODB.recordset
    Dim estadoFact As Integer
    
    On Error GoTo ErrorHandler
    
    Set RsEstado = New ADODB.recordset
    sSql = "select max(flg_estado) MaxEstado from fa_movi_fact where episodio = '" & Trim(REQ_ACTUAL) & "'"
    RsEstado.CursorLocation = adUseServer
    RsEstado.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEstado.Open sSql, gConexaoSecundaria
    estadoFact = BL_HandleNull(RsEstado!MaxEstado, 0)
    If estadoFact = 3 Then
        Me.StatusBar1.Panels(1).Text = " Requisi��o : " & Trim(REQ_ACTUAL) & " - N�o � poss�vel enviar para a factura��o. Deve primeiro rejeita-la."
        Me.EcNumReq.ForeColor = &HFF&
        MsgBox "N�o � poss�vel enviar esta requisi��o para a factura��o. Deve primeiro rejeita-la.   ", vbExclamation, Me.caption
    End If
    Get_EstadoFact = estadoFact
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros "Erro Inesperado : Get_EstadoFact (FormFACTUSEnvio) -> " & Err.Description
            
            Get_EstadoFact = 3
            Exit Function
    End Select
End Function

' ---------------------------------------------------------------------------

' LIMPA A ESTRUTURA QUE CONTEM OS DADOS DA REQUISICAO A FACTURAR

' ---------------------------------------------------------------------------


Private Sub LimpaEF_ReqFact()
    FgReq.rows = 2
    FgReq.TextMatrix(1, 0) = ""
    FgReq.CellBackColor = vbWhite
End Sub


' ---------------------------------------------------------------------------

' VERIFICA SE UTENTE PAGOU TODAS AS TAXAS

' ---------------------------------------------------------------------------
Public Function RequisicaoIsenta(n_req As String) As Boolean
        
    On Error GoTo TrataErro
    
    Dim sSql As String
    Dim RsTaxa As ADODB.recordset
    Dim ret As Boolean
    
    n_req = Trim(n_req)
    
    If (Len(n_req) = 0) Then
        RequisicaoIsenta = False
        Exit Function
    End If
        
    sSql = "SELECT cod_isencao FROM sl_requis WHERE n_req = " & n_req
    Set RsTaxa = New ADODB.recordset
    RsTaxa.CursorLocation = adUseServer
    RsTaxa.CursorType = adOpenForwardOnly
    RsTaxa.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsTaxa.Open sSql, gConexao
    
    
    If (RsTaxa.RecordCount > 0) Then
        If BL_HandleNull(RsTaxa!cod_isencao, "2") <> "2" Then
            ret = False
        Else
            ret = True
        End If
    End If
    RsTaxa.Close
    Set RsTaxa = Nothing

    RequisicaoIsenta = ret

Exit Function
TrataErro:
    Call BL_LogFile_BD(Me.Name, "RequisicaoIsenta", Err.Number, Err.Description, "")
    RequisicaoIsenta = False
    Exit Function
    Resume Next
End Function


' ---------------------------------------------------------------------------

' LIMPA A GRELHA DAS ANALISES

' ---------------------------------------------------------------------------

Private Sub LimpaFgAnalises()
    Dim i As Integer
    Dim j As Integer
    FLG_ExecutaFGAna = False
    FGAnalises.rows = 1
    FGAnalises.rows = NumLinhas
    FLG_ExecutaFGAna = True
End Sub

Private Sub Ecdatareq_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub



' ---------------------------------------------------------------------------

' CALCULA QUANTAS AN�LISES EXISTEM POR CREDENCIAL

' ---------------------------------------------------------------------------
Private Sub CalculaCredenciais()
    Dim credenciais() As Integer
    Dim tot_credenciais() As Integer
    Dim i As Integer
    Dim j As Integer
        
    On Error GoTo TrataErro
    
    ReDim credenciais(0)
    ReDim tot_credenciais(0)
    
    For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
        For j = 1 To UBound(credenciais)
            If EF_ReqFact(ReqActual).AnalisesFact(i).p1 = credenciais(j) Then
                tot_credenciais(j) = tot_credenciais(j) + 1
                Exit For
            End If
        Next
        If j > UBound(credenciais) Or UBound(credenciais) = 0 Then
            If UBound(credenciais) = 0 Then
                ReDim credenciais(1)
                ReDim tot_credenciais(1)
                credenciais(1) = BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(i).p1, -1)
                tot_credenciais(1) = 1
            Else
                ReDim Preserve credenciais(UBound(credenciais) + 1)
                ReDim Preserve tot_credenciais(UBound(credenciais) + 1)
                credenciais(UBound(credenciais)) = EF_ReqFact(ReqActual).AnalisesFact(i).p1
                tot_credenciais(UBound(credenciais)) = 1
            End If
        End If
    Next
    LbCredenciais = "Credenciais: "
    For i = 1 To UBound(credenciais)
        LbCredenciais = LbCredenciais & tot_credenciais(i) & "   "
    Next
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub



Sub Reactiva_Analise(LINHA_SEL As Integer)
    Dim sSql As String
    
    On Error Resume Next
    
    If LINHA_SEL > EF_ReqFact(ReqActual).TotalAnalisesFact Then Exit Sub
    If EF_ReqFact(ReqActual).AnalisesFact(LINHA_SEL).Flg_Facturada = gEstadoFactRequisFacturado Then
        BG_Mensagem mediMsgBox, "N�o � poss�vel reactivar uma an�lise facturada!"
        Exit Sub
    Else
        EF_ReqFact(ReqActual).AnalisesFact(LINHA_SEL).Flg_Retirada = False
    
        sSql = "UPDATE sl_recibos_det SET flg_retirado = 0 WHERE n_req = " & EF_ReqFact(ReqActual).NReq
        sSql = sSql & " AND cod_efr = " & EF_ReqFact(ReqActual).AnalisesFact(LINHA_SEL).cod_efr & " AND cod_facturavel = " & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(LINHA_SEL).CodFacturavel)
        'sSql = sSql & " AND serie_rec = " & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(Linha).SerieRec) & " and n_rec = " & EF_ReqFact(ReqActual).AnalisesFact(Linha).nRec
        BG_ExecutaQuery_ADO (sSql)
        
        AvaliaCorFG (LINHA_SEL)
 
    End If
End Sub


' --------------------------------------------------------------------------------------------

' SE DA ENTER NA COLUNA DO P1

' --------------------------------------------------------------------------------------------

Private Sub AlteraCorP1(cor As Long)
    Dim ent_temp As String
    Dim p1_temp As String
    Dim cor_temp As Long
    Dim cor2_temp As String
    Dim linha_temp As Integer
    Dim sSql As String
    Dim i As Integer
    
    ent_temp = FGAnalises.TextMatrix(FGAnalises.row, lColFactEFR)
    p1_temp = FGAnalises.TextMatrix(FGAnalises.row, lColFactP1)
    linha_temp = FGAnalises.row
    If cor = 86 Then 'V
        FGAnalises.CellBackColor = Verde
        cor_temp = Verde
        cor2_temp = "V"
    ElseIf cor = 66 Then 'B
        FGAnalises.CellBackColor = vbWhite
        cor_temp = vbWhite
        cor2_temp = "B"
    End If

    For i = 1 To FGAnalises.rows - 1
        If FGAnalises.TextMatrix(i, lColFactP1) = p1_temp And FGAnalises.TextMatrix(i, lColFactEFR) = ent_temp Then
            FGAnalises.row = i
            FGAnalises.Col = 1
            FGAnalises.CellBackColor = cor_temp
            EF_ReqFact(ReqActual).AnalisesFact(i).cor_p1 = cor2_temp
            sSql = "UPDATE sl_recibos_det SET cor_p1 = " & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).cor_p1) & " WHERE n_req = " & EF_ReqFact(ReqActual).NReq
            sSql = sSql & " AND cod_efr = " & EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).cod_efr & " AND cod_facturavel = " & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row).CodFacturavel)
            BG_ExecutaQuery_ADO (sSql)
            
        End If
    Next
    FGAnalises.row = linha_temp
End Sub


' ------------------------------------------------------------------------------------------------

' VERIFICA SE ANALISE FOI MARCACDA PELO CODIGO DE BARRAS DO P1

' ------------------------------------------------------------------------------------------------
Private Function MarcacaoCodigoBarrasP1() As Boolean
    Dim codRubrEfr As String
    Dim rsAnaFact As New ADODB.recordset
    Dim encontrou As Boolean
    Dim sSql As String
    Dim temp  As String
    Dim i As Integer
    Dim j As Integer
    
    If EcCodAnaEFR <> "" And EF_ReqFact(ReqActual).TotalAnalisesFact > 0 Then
        codRubrEfr = IF_ObtemRubrParaEFR(EcCodEFR, EcCodAnaEFR)
        If codRubrEfr <> "" Then
            sSql = "SELECT distinct x1.cod_ana FROM sl_ana_facturacao x1, slv_analises_factus x2 WHERE x1.cod_ana_gh IN " & codRubrEfr
            sSql = sSql & " AND cod_portaria IN (Select x3.cod_portaria FROM sl_portarias x3 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EF_ReqFact(ReqActual).dt_chega, Bg_DaData_ADO)) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099'))"
            sSql = sSql & " AND x1.cod_ana = x2.cod_ana "
            rsAnaFact.CursorLocation = adUseServer
            rsAnaFact.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAnaFact.Open sSql, gConexao
            
            If rsAnaFact.RecordCount = 0 Then
                MarcacaoCodigoBarrasP1 = False
                Exit Function
            ElseIf rsAnaFact.RecordCount = 1 Then
                MarcacaoCodigoBarrasP1 = True
                EcCodAnaEFR = BL_HandleNull(rsAnaFact!cod_ana, "")
            ElseIf rsAnaFact.RecordCount > 1 Then
                temp = "("
                i = 1
                encontrou = False
                While Not rsAnaFact.EOF
                    
                    For j = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
                        If EF_ReqFact(ReqActual).AnalisesFact(j).codAna = BL_HandleNull(rsAnaFact!cod_ana, "") And EF_ReqFact(ReqActual).AnalisesFact(j).cod_efr = EcCodEFR And _
                            EF_ReqFact(ReqActual).AnalisesFact(j).cod_rubr_efr = EcCodAnaEFR Then
                            encontrou = True
                            EcCodAnaEFR = BL_HandleNull(rsAnaFact!cod_ana, "")
                            Exit For
                        End If
                    Next
                    If encontrou = False Then
                        temp = temp & "'" & BL_HandleNull(rsAnaFact!cod_ana, "0") & "'"
                        If i < rsAnaFact.RecordCount Then
                            temp = temp & ", "
                        Else
                            temp = temp & ")"
                        End If
                        i = i + 1
                    End If
                    rsAnaFact.MoveNext
                Wend
                If encontrou = False Then
                    BtPesqRapAna_Click temp
                End If
                'BL_PesquisaAnalises_FACTUS "", temp
                MarcacaoCodigoBarrasP1 = True
            End If
            rsAnaFact.Close
            Set rsAnaFact = Nothing
        End If
    End If

End Function

Private Sub EcCodAnaEFR_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    Dim j As Integer
    Dim encontrou As Boolean
    
    If KeyCode = 13 Then
        ' RETORNA CODIGO DA ANALISE
        MarcacaoCodigoBarrasP1
        
        ' VERIFICA SE ANALISE ESTA MARCADA
        encontrou = False
        For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
            If EF_ReqFact(ReqActual).AnalisesFact(i).codAna = EcCodAnaEFR And EF_ReqFact(ReqActual).AnalisesFact(i).cod_efr = EcCodEFR Then
                encontrou = True
                Exit For
            End If
        Next
        
        ' SE ENCONTROU MAS ESTA FORA DE ORDEM
        If encontrou = True And i > lPosicaoActual Then
            EF_ReqFact(ReqActual).AnalisesFact(i).p1 = lCredencialActiva
            For j = i To (lPosicaoActual + 1) Step -1
                Troca_Linha j, j - 1
            Next
            EF_ReqFact(ReqActual).AnalisesFact(lPosicaoActual).tratado = True
            BL_MudaCorFg Me.FGAnalises, CLng(lPosicaoActual), CorEnviada
            lPosicaoActual = lPosicaoActual + 1
        
        ' ENCONTROU MAS ESTA NO SITIO CERTO.
        ElseIf encontrou = True And i = lPosicaoActual Then
            BL_MudaCorFg Me.FGAnalises, CLng(i), CorEnviada
            If lPosicaoActual < EF_ReqFact(ReqActual).TotalAnalisesFact Then
                lPosicaoActual = lPosicaoActual + 1
                EF_ReqFact(ReqActual).AnalisesFact(lPosicaoActual).tratado = True
            End If
            
        ' NAO ENCONTROU
        ElseIf encontrou = False Then
            EcCodAna = EcCodAnaEFR
            If Mid(EcCodAna, 1, 1) <> "S" And Mid(EcCodAna, 1, 1) <> "C" And Mid(EcCodAna, 1, 1) <> "P" Then
                Exit Sub
            End If
            EcCodAna_KeyDown 13, 0
            AlteraP1 CLng(EF_ReqFact(ReqActual).TotalAnalisesFact), CStr(lCredencialActiva)
            For j = EF_ReqFact(ReqActual).TotalAnalisesFact To (lPosicaoActual + 1) Step -1
                Troca_Linha j, j - 1
            Next
            EF_ReqFact(ReqActual).AnalisesFact(lPosicaoActual).tratado = True
            BL_MudaCorFg Me.FGAnalises, CLng(lPosicaoActual), CorEnviada
            If lPosicaoActual < EF_ReqFact(ReqActual).TotalAnalisesFact Then
                lPosicaoActual = lPosicaoActual + 1
                EF_ReqFact(ReqActual).AnalisesFact(lPosicaoActual).tratado = True
            End If
            
        End If
        EcCodAnaEFR = ""
        EcCodAnaEFR.SetFocus
        
    ElseIf KeyCode = 187 Then
        lCredencialActiva = lCredencialActiva + 1
        LbCred = lCredencialActiva
        EcCodAnaEFR = ""
        KeyCode = 0
    ElseIf KeyCode = 189 Then
        If lCredencialActiva > 1 Then
            lCredencialActiva = lCredencialActiva - 1
            LbCred = lCredencialActiva
            EcCodAnaEFR = ""
        End If
    End If
End Sub

Private Sub BtPesqRapAna_Click(analises As String)
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_ana"
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "slv_analises_factus"
    CampoPesquisa1 = "descr_ana"
    If analises <> "" Then
        ClausulaWhere = ClausulaWhere & "  cod_ana in " & analises
    End If
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar An�lises")
    
    mensagem = "N�o foi encontrada nenhuma An�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAnaEFR.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


' ---------------------------------------------------------------------------

' ALTERA NUMERO DO P1

' ---------------------------------------------------------------------------
Private Sub AlteraP1(linha As Long, NovoP1 As String)
    Dim sSql As String
    EF_ReqFact(ReqActual).AnalisesFact(linha).p1 = NovoP1
    FGAnalises.TextMatrix(linha, lColFactP1) = NovoP1
    
    sSql = "UPDATE sl_recibos_det SET p1 = " & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(linha).p1) & " WHERE n_req = " & EF_ReqFact(ReqActual).NReq
    sSql = sSql & " AND cod_efr = " & EF_ReqFact(ReqActual).AnalisesFact(linha).cod_efr & " AND cod_facturavel = " & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(linha).CodFacturavel)
    'sSql = sSql & " AND serie_rec = " & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(Linha).SerieRec) & " and n_rec = " & EF_ReqFact(ReqActual).AnalisesFact(Linha).nRec
    BG_ExecutaQuery_ADO (sSql)

End Sub


' ---------------------------------------------------------------------------

' ALTERA CODIGOMEDICO

' ---------------------------------------------------------------------------
Private Sub AlteraMedico(linha As Long, NovoMedico As String)
    Dim sSql As String
    EF_ReqFact(ReqActual).AnalisesFact(linha).cod_medico = NovoMedico
    FGAnalises.TextMatrix(linha, lColFactMedico) = NovoMedico
    

End Sub

' ---------------------------------------------------------------------------

' ALTERA NUM BENEF

' ---------------------------------------------------------------------------
Private Sub AlteraNrBenef(linha As Long, NovoNr As String)
    Dim sSql As String
    EF_ReqFact(ReqActual).AnalisesFact(linha).nr_benef = NovoNr
    FGAnalises.TextMatrix(linha, lColFactNrBenef) = NovoNr
    

End Sub


Private Sub AlteraEntidade(linha As Long, NovoEFR As String)
    Dim sSql As String
    Dim RsEFR As New ADODB.recordset
    sSql = "SELECT  * FROM sl_efr WHERE cod_efr = " & NovoEFR
    RsEFR.CursorType = adOpenStatic
    RsEFR.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEFR.Open sSql, gConexao
    If RsEFR.RecordCount > 0 Then
        EF_ReqFact(ReqActual).AnalisesFact(linha).cod_efr_envio = NovoEFR
        EF_ReqFact(ReqActual).AnalisesFact(linha).cod_empresa = BL_HandleNull(RsEFR!cod_empresa, "")
        EF_ReqFact(ReqActual).AnalisesFact(linha).flg_novaArs = BL_HandleNull(RsEFR!flg_nova_ars, 0)
        FGAnalises.TextMatrix(linha, lColFactEFR) = NovoEFR
        sSql = "UPDATE sl_recibos_det SET cod_efr_envio = " & NovoEFR & " WHERE n_req = " & EF_ReqFact(ReqActual).NReq & " AND cod_efr = " & EF_ReqFact(ReqActual).AnalisesFact(linha).cod_efr & " AND cod_facturavel = " & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(linha).codAna)
        BG_ExecutaQuery_ADO sSql
    End If
    RsEFR.Close
    Set RsEFR = Nothing
End Sub
Private Sub RetiraAnalise(linha As Long)
    Dim sSql As String
    EF_ReqFact(ReqActual).AnalisesFact(linha).Flg_Retirada = True
    
    sSql = "UPDATE sl_recibos_det SET flg_retirado = 1 WHERE n_req = " & EF_ReqFact(ReqActual).NReq
    sSql = sSql & " AND cod_efr = " & EF_ReqFact(ReqActual).AnalisesFact(linha).cod_efr & " AND cod_facturavel = " & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(linha).CodFacturavel)
    BG_ExecutaQuery_ADO (sSql)
    
    CalculaCredenciais

End Sub

' ---------------------------------------------------------------------------

' ALTERA QUANTIDADE

' ---------------------------------------------------------------------------
Private Sub AlteraQuantidade(linhaA As Long, quantidade As Integer)
    Dim sSql As String
    EF_ReqFact(ReqActual).AnalisesFact(linhaA).preco = quantidade * Replace(EF_ReqFact(ReqActual).AnalisesFact(linhaA).PrecoUnitario, ".", ",")
    FGAnalises.TextMatrix(linhaA, lColFactTaxa) = EF_ReqFact(ReqActual).AnalisesFact(linhaA).preco
    sSql = "UPDATE sl_recibos_det SET quantidade_envio = " & EF_ReqFact(ReqActual).AnalisesFact(linhaA).qtd_ana_envio
    sSql = sSql & " WHERE n_req = " & EF_ReqFact(ReqActual).NReq
    sSql = sSql & " AND cod_efr = " & EF_ReqFact(ReqActual).AnalisesFact(linhaA).cod_efr & " AND cod_facturavel = " & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(linhaA).CodFacturavel)
    BG_ExecutaQuery_ADO (sSql)

End Sub

' ---------------------------------------------------------------------------

' INSERE DIRECTAMENTE A LINHA NA BASE DADOS REFERENTE A NOVA ANALISE

' ---------------------------------------------------------------------------
Private Sub AdicionaAnaliseBD(linha As Long)
    Dim sSql As String
    Dim rsRec As New ADODB.recordset
    Dim seq_Recibo As Long
    'soliveira correccao lacto
    
    seq_Recibo = mediComboValorNull
    'VERIFICA SE EXISTE LINHA NA SL_RECIBOS PARA A ENTIDADE EM CAUSA E SEM RECIBO EMITIDO
    sSql = "SELECT *  FROM SL_RECIBOS WHERE n_req = " & EF_ReqFact(ReqActual).NReq & " AND cod_efr = " & EF_ReqFact(ReqActual).codEfr
    sSql = sSql & " AND n_rec = 0 "
    rsRec.CursorLocation = adUseServer
    rsRec.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsRec.Open sSql, gConexao
    If rsRec.RecordCount <= 0 Then
        seq_Recibo = RECIBO_RetornaSeqRecibo
        sSql = "INSERT INTO sl_recibos (seq_recibo,n_req, cod_efr, serie_rec, n_rec, total_pagar, user_cri, dt_cri, hr_cri, flg_impresso, estado, "
        sSql = sSql & " desconto, num_analises, flg_doc_caixa, valor_original, cod_empresa,cod_forma_pag) VALUES ("
        sSql = sSql & seq_Recibo & ", "
        sSql = sSql & EF_ReqFact(ReqActual).NReq & ", "
        sSql = sSql & EF_ReqFact(ReqActual).codEfr & ",'0',0,0, "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
        sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ",0, "
        sSql = sSql & BL_TrataStringParaBD(gEstadoReciboNulo) & ",0,1,0,0, "
        sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).cod_empresa) & "," & gModoPagNaoPago & ") "
        BG_ExecutaQuery_ADO sSql
    ElseIf BL_HandleNull(rsRec!total_pagar, 0) >= 1 Then
        seq_Recibo = BL_HandleNull(rsRec!seq_Recibo, mediComboValorNull)
        sSql = "UPDATE sl_recibos SET num_analises = num_analises + 1 WHERE n_req = " & EF_ReqFact(ReqActual).NReq & " AND seq_recibo = " & seq_Recibo
        BG_ExecutaQuery_ADO sSql
    
    End If
    
    sSql = "INSERT INTO sl_recibos_det ( n_req,cod_efr,cod_facturavel,cod_ana,n_rec,p1,user_cri,dt_cri, "
    sSql = sSql & "hr_cri,flg_impresso,flg_retirado, flg_adicionada,flg_facturado, ordem_marcacao, isencao,cor_p1, "
    sSql = sSql & " serie_rec, taxa, cod_ana_efr, n_benef, fds,quantidade, taxa_unitario, flg_marcacao_retirada, "
    sSql = sSql & " flg_recibo_manual,descr_ana_efr, flg_cod_barras, flg_automatico, cod_efr_envio, quantidade_envio, seq_recibo ) VALUES( "
    sSql = sSql & EF_ReqFact(ReqActual).NReq & ", "
    sSql = sSql & EF_ReqFact(ReqActual).codEfr & ", "
    sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(linha).CodFacturavel) & ", "
    sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(linha).codAna) & ", "
    sSql = sSql & "0, " & BL_TrataStringParaBD(BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(linha).p1, "1")) & ", "
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
    sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ",0,0,1,0," & EF_ReqFact(ReqActual).AnalisesFact(linha).ordem & ","
    sSql = sSql & BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(linha).isencao, gTipoIsencaoNaoIsento) & ", 'B', '0'," & Replace(EF_ReqFact(ReqActual).AnalisesFact(linha).precoUte, ",", ".") & ", "
    sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(linha).cod_rubr_efr) & "," & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(linha).nr_benef) & ","
    sSql = sSql & BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(linha).fds, "0") & ", "
    sSql = sSql & BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(linha).Qtd_Ana, "1") & ", "
    sSql = sSql & BL_HandleNull(Replace(EF_ReqFact(ReqActual).AnalisesFact(linha).precoUnitarioUte, ",", "."), "0") & ",0,"
    sSql = sSql & EF_ReqFact(ReqActual).AnalisesFact(linha).flg_recibo_manual & ", "
    sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(linha).descr_rubr_efr) & ",0,0,"
    sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(linha).cod_efr_envio) & ","
    sSql = sSql & EF_ReqFact(ReqActual).AnalisesFact(linha).qtd_ana_envio & ","
    sSql = sSql & seq_Recibo & ")"
    BG_ExecutaQuery_ADO sSql
    
    CalculaCredenciais

End Sub

' --------------------------------------------------------------------------------

' PERMITE MUDAR TIPO DE ISENCAO DE UMA ANALISE ATRAVES DAS TECLAS F8, F9, F10

' --------------------------------------------------------------------------------
Private Sub AlteraIsencao(TipoIsencao As Integer)
    Dim linha As Long
    Dim codAnaEfr As String
    Dim mens As String
    Dim sSql As String
    Dim i As Long
    Dim cor As Long
    
    linha = FGAnalises.row
    If TipoIsencao = gTipoIsencaoBorla Then
        cor = CorBorla
    ElseIf TipoIsencao = gTipoIsencaoIsento Then
        cor = CorIsento
    Else
        cor = vbWhite
    End If
    
    If EF_ReqFact(ReqActual).AnalisesFact(linha).codAna <> "" And (EF_ReqFact(ReqActual).AnalisesFact(linha).Flg_Facturada = 0 Or EF_ReqFact(ReqActual).AnalisesFact(linha).Flg_Facturada = 2) Then
        For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
            If EF_ReqFact(ReqActual).AnalisesFact(linha).cod_efr = EF_ReqFact(ReqActual).AnalisesFact(i).cod_efr And EF_ReqFact(ReqActual).AnalisesFact(linha).p1 = EF_ReqFact(ReqActual).AnalisesFact(i).p1 Then
                EF_ReqFact(ReqActual).AnalisesFact(i).isencao = TipoIsencao
                BL_MudaCorFg Me.FGAnalises, i, cor
            End If
        Next
    End If
    'BL_MudaCorFg Me.FGAnalises, CLng(EF_ReqFact(reqactual).totalanalisesfact), CorIsento
    
End Sub


Sub VerificaEFRPagaActoMedico(codEfr As String)
    Dim i As Integer
    Dim Flg_ExisteActoMed As Boolean
    
        If BL_HandleNull(EF_ReqFact(ReqActual).flg_acto_med, 0) = 1 Then
            For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
                If Trim(EF_ReqFact(ReqActual).AnalisesFact(i).cod_efr) = codEfr And EF_ReqFact(ReqActual).AnalisesFact(i).codAna = GCodAnaActMed Then
                    Flg_ExisteActoMed = True
                    Exit For
                End If
            Next i
            
            If Flg_ExisteActoMed = False Then
                AdicionaAnaliseAEFR codEfr, GCodAnaActMed
            End If
        End If
End Sub

Sub AdicionaAnaliseAEFR(codEfr As String, CodFacturavel As String)
    Dim sSql As String
    Dim rsAnalises As ADODB.recordset
    Dim i As Integer
    Dim TotalElementosSel As Integer
    Dim linhaA As Long
    
    Set rsAnalises = New ADODB.recordset
    ' ---------------------------------------------------------------------------
    ' VERIFICA SE ANALISE ESTA MAPEADA
    ' ---------------------------------------------------------------------------
    sSql = "SELECT f.cod_ana, v.descr_ana, cod_ana_gh, descr_ana_facturacao, qtd FROM sl_ana_facturacao f, slv_analises_factus v WHERE f.cod_ana = v.cod_ana and f.cod_ana = " & BL_TrataStringParaBD(CodFacturavel)
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EF_ReqFact(ReqActual).dt_chega, Bg_DaData_ADO)) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
    rsAnalises.CursorLocation = adUseServer
    rsAnalises.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnalises.Open sSql, gConexao
    
    If rsAnalises.RecordCount > 0 Then
        linhaA = EF_PreencheEstrutAnalises(ReqActual, mediComboValorNull, CodFacturavel, CodFacturavel, BL_HandleNull(rsAnalises!descr_ana, ""), BL_HandleNull(rsAnalises!cod_ana_gh, ""), _
                                    BL_HandleNull(rsAnalises!descr_ana_facturacao, ""), "1", gEstadoFactRequisNaoFacturado, 0, 1, BL_HandleNull(EF_ReqFact(ReqActual).codIsencao, mediComboValorNull), _
                                    0, "0", "", mediComboValorNull, "B", EF_ReqFact(ReqActual).codEfr, 0, BL_HandleNull(rsAnalises!qtd, 1), "", "", BL_HandleNull(EF_ReqFact(ReqActual).fimSemana, "0"), _
                                    BL_SelCodigo("SL_EFR", "COD_EMPRESA", "COD_EFR", EF_ReqFact(ReqActual).codEfr, "V"), 0, 0, EF_ReqFact(ReqActual).codEfr, BL_HandleNull(rsAnalises!qtd, 1), mediComboValorNull)
        BL_MudaCorFg Me.FGAnalises, linhaA, CorAdicionada
        
        ' ---------------------------------------------------------------------------
        ' PREENCHE A GRELHA
        ' ---------------------------------------------------------------------------
        PreencheFGAna ReqActual, linhaA
        ' ---------------------------------------------------------------------------
        ' AUMENTA GRELHA E ESTRUTURA
        ' ---------------------------------------------------------------------------
        If linhaA >= (NumLinhas - 1) Then
            FGAnalises.AddItem ""
        End If
        
        AdicionaAnaliseBD CLng(linhaA)
    Else
        BG_Mensagem mediMsgBox, " An�lise n�o est� mapeada para o FACTUS!", vbOKOnly + vbInformation, "Taxas"
    End If
    rsAnalises.Close
    Set rsAnalises = Nothing
End Sub



Private Sub PreencheTabelaTemporaria()
    Dim sSql As String
    Dim i As Long
    
    sSql = "DELETE FROM sl_cr_envio_factus WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
        sSql = " INSERT INTO sl_cr_envio_factus (nome_computador, num_sessao, n_req, cod_efr, p1, cod_ana, descr_ana, "
        sSql = sSql & " preco, cod_rubr_efr, n_rec, qtd, nome_ute, n_benef_ute, cod_medico)  VALUES ("
        sSql = sSql & BL_TrataStringParaBD(gComputador) & " , "
        sSql = sSql & (gNumeroSessao) & " , "
        sSql = sSql & BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(i).NReq, EF_ReqFact(ReqActual).NReq) & " , "
        sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(i).cod_efr) & " , "
        sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(i).p1) & " , "
        sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(i).codAna) & " , "
        sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(i).descrAna) & " , "
        sSql = sSql & Replace(EF_ReqFact(ReqActual).AnalisesFact(i).preco, ",", ".") & " , "
        sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(i).cod_rubr_efr) & " , "
        sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(i).nRec) & " , "
        sSql = sSql & BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(i).Qtd_Ana, 1) & " , "
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(EF_ReqFact(ReqActual).NomeUtente, "")) & " , "
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(i).nr_benef, "")) & " , "
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(i).cod_medico, "")) & " ) "
        BG_ExecutaQuery_ADO sSql
    Next
End Sub

Private Sub EcDtChega_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
   If Trim(EcDtChega.Text) = "" Then
        EcDtChega.Text = Bg_DaData_ADO
        'SendKeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDtchega_Validate(Cancel As Boolean)
    Dim i As Integer
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtChega)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    Else
        If ReqActual <= EF_TotalRequisicoes Then
            EF_ReqFact(ReqActual).dt_chega = EcDtChega
            If EF_ReqFact(ReqActual).TotalAnalisesFact > 0 Then
                If EF_ReqFact(ReqActual).AnalisesFact(1).Portaria = EF_ReqFact(ReqActual).portariaDefeito Then
                    For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
                        EF_ReqFact(ReqActual).AnalisesFact(i).data = EF_ReqFact(ReqActual).dt_chega
                    Next
                Else
                    BG_Mensagem mediMsgBox, "A data da requisi��o n�o pode ser alterada porque a portaria foi modificada.", vbOKOnly + vbInformation, "Factura��o"
                End If
            End If
        End If
    End If
    
End Sub


Private Sub LimpaCamposEcra()
    EcNumReq = ""
    EcCredencial.Text = ""
    EcTipoUte = ""
    EcUtente = ""
    EcNome = ""
    EcCodSala = ""
    EcDescrSala = ""
    EcCodEFR = ""
    EcDescrEFR = ""
    EcCodMed = ""
    EcNomeMed = ""
    EcNumBenef = ""
    EcDataNasc = ""
    EcReqAssoc = ""
    EcIdade = ""
    LbFimSemana.Visible = False
    EcDtChega = ""
    CbTipoIsencao.ListIndex = mediComboValorNull
    CbConvencao.ListIndex = mediComboValorNull
    CbCodUrbano.ListIndex = mediComboValorNull
    EcEstado = ""
    LbLote = ""
    EcKm.Text = ""
    LbUtenteFact.Visible = False
    CbHemodialise.ListIndex = mediComboValorNull
    EcDescrPais = ""
    EcCodPais = ""
    EcDoenteProf = ""
    EcDescrProven = ""
    EcCodProven = ""
    
End Sub


Sub AvaliaCorFG(linha As Integer)

    If EF_ReqFact(ReqActual).AnalisesFact(linha).cor_p1 = "B" Then
        FGAnalises.row = linha
        FGAnalises.Col = 1
        FGAnalises.CellBackColor = vbWhite
    ElseIf EF_ReqFact(ReqActual).AnalisesFact(linha).cor_p1 = "V" Then
        FGAnalises.row = linha
        FGAnalises.Col = 1
        FGAnalises.CellBackColor = Verde
    End If
    
    FLG_ExecutaFGAna = False
    If EF_ReqFact(ReqActual).AnalisesFact(linha).Flg_Retirada = False Then
        BL_MudaCorFg Me.FGAnalises, CLng(linha), vbWhite
    End If

    ' ---------------------------------------------------------------------------
    ' ANALISES ENVIADAS COLOCA A VERDE. ANALISES REJEITADAS A VERMELHO. ISENTAS A
    ' AZUL E  BORLAS A ROSA
    ' ---------------------------------------------------------------------------
    If EF_ReqFact(ReqActual).AnalisesFact(linha).Flg_Facturada = gEstadoFactRequisFacturado Then
        BL_MudaCorFg Me.FGAnalises, CLng(linha), CorEnviada
    ElseIf EF_ReqFact(ReqActual).AnalisesFact(linha).isencao = gTipoIsencaoBorla Then
        BL_MudaCorFg Me.FGAnalises, CLng(linha), CorBorla
    ElseIf EF_ReqFact(ReqActual).AnalisesFact(linha).isencao = gTipoIsencaoIsento Then
        BL_MudaCorFg Me.FGAnalises, CLng(linha), CorIsento
    'soliveira correccao lacto
    ElseIf EF_ReqFact(ReqActual).AnalisesFact(linha).Flg_Retirada = True Then
        BL_MudaCorFg Me.FGAnalises, CLng(linha), CorRetirada
    ElseIf EF_ReqFact(ReqActual).AnalisesFact(linha).Flg_Facturada = gEstadoFactRequisRejeitado Then
        BL_MudaCorFg Me.FGAnalises, CLng(linha), CorRejeitada
    End If
    
    ' ---------------------------------------------------------------------------
    ' ANALISES ADICIONADAS E AINDA NAO ENVIADAS
    ' ---------------------------------------------------------------------------
    If EF_ReqFact(ReqActual).AnalisesFact(linha).Flg_Facturada = gEstadoFactRequisNaoFacturado And EF_ReqFact(ReqActual).AnalisesFact(linha).Flg_adicionada = True And EF_ReqFact(ReqActual).AnalisesFact(linha).Flg_Retirada = False Then
        BL_MudaCorFg Me.FGAnalises, CLng(linha), CorAdicionada
    End If
    FLG_ExecutaFGAna = True
    
    ' ---------------------------------------------------------------------------
    ' ANALISES RETIRADAS COLOCA A AMARELO
    ' ---------------------------------------------------------------------------
    If EF_ReqFact(ReqActual).AnalisesFact(linha).Flg_Retirada = True Then
        FLG_ExecutaFGAna = False
        BL_MudaCorFg Me.FGAnalises, CLng(linha), CorRetirada
        FLG_ExecutaFGAna = True
    End If
End Sub


Sub AcrescentaLinha(linhaA As Integer)
    
    Dim i As Integer
    Dim p1 As String
    Dim nr_benef As String
    Dim cod_medico As String
    
    On Error GoTo Trata_Erro
    FLG_ExecutaFGAna = False
    'Cria linhaA vazia
    FGAnalises.AddItem "", FGAnalises.row
    
    EF_MoveEstrutAnalises ReqActual, linhaA
    
    If FGAnalises.row = 1 Then
        p1 = "1"
        nr_benef = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row + 1).nr_benef
        cod_medico = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row + 1).cod_medico
    Else
        p1 = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row - 1).p1
        nr_benef = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row - 1).nr_benef
        cod_medico = EF_ReqFact(ReqActual).AnalisesFact(FGAnalises.row - 1).cod_medico
    End If
    
    EF_PreencheEstrutAnalises ReqActual, linhaA, "", "", "", "", "", p1, gEstadoFactRequisNaoFacturado, 0, 1, BL_HandleNull(EF_ReqFact(ReqActual).codIsencao, mediComboValorNull), 0, "0", "", _
                              FGAnalises.row, "B", EF_ReqFact(ReqActual).codEfr, 0, 1, cod_medico, nr_benef, EF_ReqFact(ReqActual).fimSemana, EF_ReqFact(ReqActual).cod_empresa, _
                              0, 0, EF_ReqFact(ReqActual).codEfr, 1, mediComboValorNull
    
    BL_MudaCorFg Me.FGAnalises, CLng(FGAnalises.row), CorAdicionada
    
    FLG_ExecutaFGAna = True
    Exit Sub

Trata_Erro:
    If Err.Number <> 35601 Then
        BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "aCRESCENTAlinhaA"
    End If
    Resume Next
End Sub


Sub AdicionaAnalisePos(codAna As String, DeterminaPos As Boolean, Optional PosicaoAna As Integer)
    Dim sSql As String
    Dim rsAnalises As New ADODB.recordset
    Dim i As Long
    Dim p1 As String
    Dim nr_benef As String
    Dim cod_medico As String
    Dim cod_efr As String
    Dim cod_empresa As String
    Dim flg_novaArs As Integer
    Dim flg_recibo_manual As Integer
    Dim linhaCopia As Integer
    If PosicaoAna > 1 Then
        linhaCopia = PosicaoAna - 1
    Else
        linhaCopia = EF_ReqFact(ReqActual).TotalAnalisesFact
    End If

    If UCase(Mid(codAna, 1, 1)) <> "S" And UCase(Mid(codAna, 1, 1)) <> "C" And UCase(Mid(codAna, 1, 1)) <> "P" Then
        If gSGBD = gSqlServer Then
            sSql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE substring(cod_ana,2,10) = " & BL_TrataStringParaBD(UCase(codAna))
        ElseIf gSGBD = gOracle Then
            sSql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE upper(substr(cod_ana,2,10))= " & BL_TrataStringParaBD(UCase(codAna))
        End If
        rsAnalises.CursorLocation = adUseServer
        rsAnalises.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAnalises.Open sSql, gConexao
        
        If rsAnalises.RecordCount > 0 Then
            codAna = rsAnalises!cod_ana
            rsAnalises.Close
        Else
            codAna = ""
            rsAnalises.Close
            Exit Sub
        End If
        
    End If
    ' ---------------------------------------------------------------------------
    ' VERIFICA SE ANALISE ESTA MAPEADA
    ' ---------------------------------------------------------------------------
    sSql = "SELECT * FROM sl_ana_facturacao WHERE cod_ana = " & BL_TrataStringParaBD(UCase(codAna))
    sSql = sSql & " AND cod_efr  = " & EcCodEFR
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EF_ReqFact(ReqActual).dt_chega, Bg_DaData_ADO)) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAnalises.CursorLocation = adUseServer
    rsAnalises.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnalises.Open sSql, gConexao
    If rsAnalises.RecordCount = 0 Then
        rsAnalises.Close
        sSql = "SELECT * FROM sl_ana_facturacao WHERE cod_ana = " & BL_TrataStringParaBD(UCase(codAna))
        sSql = sSql & " AND cod_efr  IS NULL "
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EF_ReqFact(ReqActual).dt_chega, Bg_DaData_ADO)) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAnalises.CursorLocation = adUseServer
        rsAnalises.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAnalises.Open sSql, gConexao
    End If
    If rsAnalises.RecordCount > 0 Then
    
        p1 = EF_ReqFact(ReqActual).AnalisesFact(linhaCopia).p1
        nr_benef = EF_ReqFact(ReqActual).AnalisesFact(linhaCopia).nr_benef
        cod_medico = EF_ReqFact(ReqActual).AnalisesFact(linhaCopia).cod_medico
        cod_efr = EF_ReqFact(ReqActual).AnalisesFact(linhaCopia).cod_efr
        cod_empresa = EF_ReqFact(ReqActual).AnalisesFact(linhaCopia).cod_empresa
        flg_recibo_manual = 0
        flg_novaArs = EF_ReqFact(ReqActual).AnalisesFact(linhaCopia).flg_novaArs
    
        
    
        If DeterminaPos = True Then
            i = EF_PreencheEstrutAnalises(ReqActual, PosicaoAna, codAna, codAna, BL_HandleNull(rsAnalises!descr_ana, ""), BL_HandleNull(rsAnalises!cod_ana_gh, ""), _
                                            BL_HandleNull(rsAnalises!cod_ana_gh, ""), p1, gEstadoFactRequisNaoFacturado, 0, 1, BL_HandleNull(EF_ReqFact(ReqActual).codIsencao, mediComboValorNull), _
                                            0, "0", "", mediComboValorNull, "B", EF_ReqFact(ReqActual).codEfr, 0, BL_HandleNull(rsAnalises!qtd, 1), _
                                            cod_medico, nr_benef, BL_HandleNull(EF_ReqFact(ReqActual).fimSemana, "0"), cod_empresa, 0, flg_recibo_manual, EF_ReqFact(ReqActual).codEfr, _
                                             BL_HandleNull(rsAnalises!qtd, 1), mediComboValorNull)
        Else
            i = EF_PreencheEstrutAnalises(ReqActual, mediComboValorNull, codAna, codAna, BL_HandleNull(rsAnalises!descr_ana, ""), BL_HandleNull(rsAnalises!cod_ana_gh, ""), _
                                            BL_HandleNull(rsAnalises!cod_ana_gh, ""), p1, gEstadoFactRequisNaoFacturado, 0, 1, BL_HandleNull(EF_ReqFact(ReqActual).codIsencao, mediComboValorNull), _
                                            0, "0", "", mediComboValorNull, "B", EF_ReqFact(ReqActual).codEfr, 0, BL_HandleNull(rsAnalises!qtd, 1), _
                                            cod_medico, nr_benef, BL_HandleNull(EF_ReqFact(ReqActual).fimSemana, "0"), cod_empresa, 0, flg_recibo_manual, EF_ReqFact(ReqActual).codEfr, _
                                             BL_HandleNull(rsAnalises!qtd, 1), mediComboValorNull)
        End If
        FLG_ExecutaFGAna = False
        BL_MudaCorFg Me.FGAnalises, CLng(i), CorAdicionada

        PreencheFGAna ReqActual, i
        
        ' ---------------------------------------------------------------------------
        ' AUMENTA GRELHA E ESTRUTURA
        ' ---------------------------------------------------------------------------
        If EF_ReqFact(ReqActual).TotalAnalisesFact >= (NumLinhas - 1) Then
            FGAnalises.AddItem ""
        End If
        
        AdicionaAnaliseBD CLng(i)
    End If
    FLG_ExecutaFGAna = True

End Sub

Sub EliminaLinha(linha As Integer)
End Sub

' ---------------------------------------------------------------------------

' GERE ALTERA��O DAS ORDENS DAS AN�LISES

' ---------------------------------------------------------------------------

Private Sub GereOrdens(indice As Integer, ordem As Integer)
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
        If EF_ReqFact(ReqActual).AnalisesFact(i).p1 = EF_ReqFact(ReqActual).AnalisesFact(indice).p1 Then
            If EF_ReqFact(ReqActual).AnalisesFact(i).ordem >= ordem And indice <> i Then
                If ExisteAnaComOrdem(i, EF_ReqFact(ReqActual).AnalisesFact(i).ordem) = True Then
                    EF_ReqFact(ReqActual).AnalisesFact(i).ordem = EF_ReqFact(ReqActual).AnalisesFact(i).ordem + 1
                    FGAnalises.TextMatrix(i, lColFactOrdem) = EF_ReqFact(ReqActual).AnalisesFact(i).ordem
                    GravaOrdem i
                End If
            End If
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao trocar ordens:" & Err.Number & " - " & Err.Description, Me.Name, "GereOrdens", True
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------

' VERIFICA SE ANALISE PODE MANTER ORDEM QUE TINHA

' ---------------------------------------------------------------------------
Private Function ExisteAnaComOrdem(indice As Integer, ordem As Integer) As Boolean
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
        If EF_ReqFact(ReqActual).AnalisesFact(i).p1 = EF_ReqFact(ReqActual).AnalisesFact(indice).p1 Then
            If EF_ReqFact(ReqActual).AnalisesFact(i).ordem = ordem And indice <> i Then
                ExisteAnaComOrdem = True
                Exit Function
            End If
        End If
    Next
    ExisteAnaComOrdem = False
    Exit Function
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao trocar ordens:" & Err.Number & " - " & Err.Description, Me.Name, "ExisteAnaComOrdem"
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------

' GRAVA ORDENACAO DA ANALISE NA BD

' ---------------------------------------------------------------------------
Private Sub GravaOrdem(linha As Integer)
    Dim sSql As String
    On Error GoTo TrataErro
    
    sSql = "UPDATE sl_recibos_det SET ordem_marcacao = " & EF_ReqFact(ReqActual).AnalisesFact(linha).ordem & " WHERE n_req = " & EF_ReqFact(ReqActual).NReq
    sSql = sSql & " AND cod_efr = " & EF_ReqFact(ReqActual).AnalisesFact(linha).cod_efr
    sSql = sSql & " AND cod_facturavel = " & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(linha).CodFacturavel)
    BG_ExecutaQuery_ADO (sSql)
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao GRAVAR ordens:" & Err.Number & " - " & Err.Description, Me.Name, "GravaOrdem"
    Exit Sub
    Resume Next
End Sub



' -----------------------------------------------------------------------------------------------

' DEPENDENDO DO ESTADO, CORRIGE A REQUISI��O EM CAUSA

' -----------------------------------------------------------------------------------------------
Private Sub CorrigeRequisicao(n_req As String, estado As Integer)
    Dim sSql As String
    Dim rsFACTUS As New ADODB.recordset
    ' -1 - ERRO
    ' 0 - N�o facturado
    ' 1 - facturado mas SL_REQUIS incoerente
    ' 2 - N�o facturado mas SL_REQUIS incoerente
    ' 3 - Facturado.
    ' 4  - com linha na fa_movi_resp mas nao na fa_movi_Fact
    ' 5 - rejeitada FACTUS mas nao no SISLAB
    ' 6 - rejeitada no FACTUS e SISLAB    On Error GoTo TrataErro
    
    If estado = 0 Or estado = 1 Or estado = 4 Or estado = 2 Or estado = 7 Then        ' CORRIGE SL_REQUIS
        BG_BeginTransaction
        sSql = "UPDATE sl_requis set flg_facturado = 0 WHERE n_req = " & n_req
        BG_ExecutaQuery_ADO sSql
        
        ' CORRIGE SL_RECIBOS_DET
        sSql = "UPDATE sl_recibos_Det set flg_facturado = 0 WHERE n_req = " & n_req
        BG_ExecutaQuery_ADO sSql
        
        
        'CORRIGE NA MOVI_FACT
        sSql = "DELETE FROM sl_movi_fact WHERE episodio = " & BL_TrataStringParaBD(n_req)
        BG_ExecutaQuery_ADO sSql
        'CORRIGE NA MOVI_RESP
        sSql = "DELETE FROM sl_movi_resp WHERE episodio = " & BL_TrataStringParaBD(n_req)
        BG_ExecutaQuery_ADO sSql
        
        IF_ApagaReqARS n_req, ""
        
        BG_CommitTransaction
        
        'CORRIGE NA MOVI_FACT
        gConexaoSecundaria.BeginTrans
        sSql = "DELETE FROM fa_movi_fact WHERE episodio = " & BL_TrataStringParaBD(n_req)
        BL_ExecutaQuery_Secundaria sSql
        'CORRIGE NA MOVI_RESP
        sSql = "DELETE FROM fa_movi_resp WHERE episodio = " & BL_TrataStringParaBD(n_req)
        BL_ExecutaQuery_Secundaria sSql
        gConexaoSecundaria.CommitTrans
        
        
        EF_ReqFact(ReqActual).estadoFact = gEstadoFactRequisNaoFacturado

    ElseIf estado = 5 Then
        sSql = "SELECT x1.cod_mot_rej_anul,x1.n_seq_prog, x2.episodio, x2.nr_req_ars FROM Fa_movi_fact_rej x1, fa_hist_dados_rej x2 WHERE x1.cod_prog = " & gCodProg & " AND x1.n_seq_prog = x2.n_seq_prog "
        sSql = sSql & " AND x2.episodio = " & BL_TrataStringParaBD(n_req)
        If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
        Set rsFACTUS = New ADODB.recordset
        rsFACTUS.CursorLocation = adUseServer
        rsFACTUS.Open sSql, gConexaoSecundaria, adOpenStatic
        If rsFACTUS.RecordCount > 0 Then
            ' --------------------------------------------------------------------------------------------
            ' COLOCA NA TABELA SL_REQUIS COMO REQUISICAO TENDO SIDO REJEITADA
            ' --------------------------------------------------------------------------------------------
            sSql = "UPDATE sl_requis SET mot_fact_rej = " & BG_VfValor(BL_HandleNull(rsFACTUS!cod_mot_rej_anul, ""), "'") & ",user_fact_rej = "
            sSql = sSql & gCodUtilizador & ", dt_fact_rej = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & " hr_fact_rej = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", flg_facturado = "
            sSql = sSql & gEstadoFactRequisRejeitado & " WHERE n_req = " & EF_ReqFact(ReqActual).NReq
            If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
            BG_ExecutaQuery_ADO sSql
            
            While Not rsFACTUS.EOF
                ' --------------------------------------------------------------------------------------------
                ' REJEITA TODAS AS ANALISES
                ' --------------------------------------------------------------------------------------------
                'soliveira correccao lacto rejeita todas as an�lises desse chave_prog (P1)
                sSql = "UPDATE sl_recibos_det set flg_facturado = " & gEstadoFactRequisRejeitado
                sSql = sSql & " WHERE n_req = " & EF_ReqFact(ReqActual).NReq
                sSql = sSql & " AND p1 = " & BL_TrataStringParaBD(Replace(rsFACTUS!nr_req_ars, n_req, ""))
                If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
                BG_ExecutaQuery_ADO sSql
                
                ' --------------------------------------------------------------------------------------------
                ' REMOVE TODAS AS LINHAS DA TABELA DE REJEITADOS DO FACTUS
                ' --------------------------------------------------------------------------------------------
                sSql = "DELETE FROM Fa_movi_fact_rej WHERE n_Seq_prog = '" & rsFACTUS!n_seq_prog & "'"
                If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
                BL_ExecutaQuery_Secundaria sSql
                
                rsFACTUS.MoveNext
            Wend
            EF_ReqFact(ReqActual).estadoFact = gEstadoFactRequisRejeitado
        End If
        rsFACTUS.Close
    End If
    FgReq_Click
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao corrigir requisi��o:" & Err.Number & " - " & Err.Description, Me.Name, "CorrigeRequisicao"
    BG_RollbackTransaction
    gConexaoSecundaria.RollbackTrans
    Exit Sub
    Resume Next
End Sub

Private Sub AlteraPortaria(codEfr As String)
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False
    
    ChavesPesq(1) = "descr_port"
    CamposEcran(1) = "descr_port"
    Tamanhos(1) = 4000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "portaria"
    CamposEcran(2) = "portaria"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "fa_portarias x1, fa_efr x2 "
    CampoPesquisa = "descr_port"
    CWhere = "x1.cod_Efr = x2.tab_utilizada AND x2.cod_efr = " & codEfr
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexaoSecundaria, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY dt_fim DESC ", " Portarias")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            If flg_pergunta = True Then
                If (BG_Mensagem(mediMsgBox, "Deseja alterar a portaria para todas an�lises?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
                    Dim i As Long
                    For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
                        EF_ReqFact(ReqActual).AnalisesFact(i).Portaria = resultados(2)
                        FGAnalises.TextMatrix(i, lColFactPortaria) = resultados(2)
                        EF_ReqFact(ReqActual).AnalisesFact(i).data = IF_RetornaDataReqPortaria(EF_ReqFact(ReqActual).AnalisesFact(i).Portaria, EF_ReqFact(ReqActual).AnalisesFact(i).data)
                        ActualizaPrecoAnaPortaria i
                        
                    Next
                Else
                    flg_pergunta = False
                    EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).Portaria = resultados(2)
                    FGAnalises.TextMatrix(LINHA_EDITA, lColFactPortaria) = resultados(2)
                    EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).data = IF_RetornaDataReqPortaria(EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).Portaria, EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).data)
                    ActualizaPrecoAnaPortaria CLng(LINHA_EDITA)
                
                End If
            Else
                EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).Portaria = resultados(2)
                FGAnalises.TextMatrix(LINHA_EDITA, lColFactPortaria) = resultados(2)
                EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).data = IF_RetornaDataReqPortaria(EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).Portaria, EF_ReqFact(ReqActual).AnalisesFact(LINHA_EDITA).data)
                ActualizaPrecoAnaPortaria CLng(LINHA_EDITA)
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem Portarias Codificadas", vbExclamation, "Portarias"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Alterar Portaria: " & Err.Number & " - " & Err.Description, Me.Name, "AlteraPortaria"
    Exit Sub
    Resume Next
End Sub


Private Sub ImprimeEtiq()
    Dim i As Integer
    Dim nomeReport As String
    Dim continua As Boolean
    nomeReport = "EtiqARS"

    PreencheEtiquetas
    RegistaImprEtiq
    'Printer Common Dialog
    If gLAB = "CDELVAS" Then
        continua = BL_IniciaReport(nomeReport, "Etiqueta ARS", crptToWindow)
    Else
        If gModoDebug = 0 Then
            continua = BL_IniciaReport(nomeReport, "Etiqueta ARS", crptToPrinter)
        Else
            continua = BL_IniciaReport(nomeReport, "Etiqueta ARS", crptToWindow)
        End If
    End If
    If continua = False Then Exit Sub
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT  sl_cr_etiq_ars.nome_ute, sl_cr_etiq_ars.n_req, sl_cr_etiq_ars.cod_efr, sl_cr_etiq_ars.descr_efr, sl_cr_etiq_ars.num_benef, sl_cr_etiq_ars.cod_rubr_efr,"
    Report.SQLQuery = Report.SQLQuery & " sl_cr_etiq_ars.descr_rubr_efr, sl_cr_etiq_ars.quantidade, sl_cr_etiq_ars.nr_c, sl_cr_etiq_ars.preco, sl_cr_etiq_ars.p1, sl_cr_etiq_ars.ordem"
    Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_etiq_ars WHERE sl_cr_etiq_ars.nome_computador = " & BL_TrataStringParaBD(gComputador)
    Report.SQLQuery = Report.SQLQuery & " AND sl_cr_etiq_ars.num_sessao = " & gNumeroSessao
    Report.SQLQuery = Report.SQLQuery & " ORDER BY sl_cr_etiq_ars.p1 asc, sl_cr_etiq_ars.ordem "
    
    
    'F�rmulas do Report
    Report.formulas(0) = ""
    
       
    
    Call BL_ExecutaReport

End Sub

Private Sub PreencheEtiquetas()
    Dim i As Long
    Dim sSql As String
    On Error GoTo TrataErro
    sSql = "DELETE FROM SL_CR_ETIQ_ARS WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To EF_ReqFact(ReqActual).TotalAnalisesFact
        If EF_ReqFact(ReqActual).AnalisesFact(i).Flg_Retirada = False Then
            sSql = "INSERT INTO SL_CR_ETIQ_ARS (nome_computador, num_sessao, nome_ute, n_req, cod_efr, descr_efr,num_benef,"
            sSql = sSql & " cod_rubr_efr, descr_rubr_efr, quantidade, nr_c, preco,preco_ute, p1,ordem  ) VALUES( "
            sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
            sSql = sSql & gNumeroSessao & ", "
            sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).NomeUtente) & ", "
            sSql = sSql & EF_ReqFact(ReqActual).NReq & ", "
            sSql = sSql & EF_ReqFact(ReqActual).codEfr & ", "
            sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).DescrEFR) & ", "
            sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).n_benef) & ", "
            sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(i).cod_rubr_efr) & ", "
            sSql = sSql & BL_TrataStringParaBD(EF_ReqFact(ReqActual).AnalisesFact(i).descrAna) & ", "
            sSql = sSql & BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(i).qtd_ana_envio, 1) & ", "
            sSql = sSql & Replace(BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(i).nr_c, 0), ",", ".") & ", "
            sSql = sSql & Replace(BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(i).preco, 0), ",", ".") & ", "
            sSql = sSql & Replace(BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(i).precoUte, 0), ",", ".") & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(i).p1, 0)) & ", "
            sSql = sSql & BL_HandleNull(EF_ReqFact(ReqActual).AnalisesFact(i).ordem, 0) & ") "
            BG_ExecutaQuery_ADO sSql
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Inserir Etiquetas: " & sSql & " " & Err.Number & " - " & Err.Description, Me.Name, "PreencheEtiquetas", True
    Exit Sub
    Resume Next

End Sub

Private Sub RegistaImprEtiq()
    Dim sSql As String
    On Error GoTo TrataErro
    sSql = "INSERT INTO SL_IMPR_ETIQ_ARS(n_Req, cod_Efr, user_cri, dt_cri, hr_cri) VALUES( "
    sSql = sSql & EF_ReqFact(ReqActual).NReq & ", "
    sSql = sSql & EF_ReqFact(ReqActual).codEfr & ", "
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
    sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ") "
    BG_ExecutaQuery_ADO sSql
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Registar Impr Etiquetas: " & sSql & " " & Err.Number & " - " & Err.Description, Me.Name, "RegistaImprEtiq", True
    Exit Sub
    Resume Next
End Sub




Private Sub ActualizaPrecoAnaPortaria(linhaA As Long)
    Dim taxa As String
    Dim mens As String
    
    EF_PreencheTaxaUtente ReqActual, linhaA
    EF_PreencheTaxaEFR ReqActual, linhaA
    FGAnalises.TextMatrix(linhaA, lColFactTaxa) = EF_ReqFact(ReqActual).AnalisesFact(linhaA).preco & " �"
    
    
End Sub
Private Sub BtPesqPais_click()

    
    FormPesquisaRapida.InitPesquisaRapida "sl_pais", _
                        "descr_pais", "cod_pais", _
                        EcCodPais
    EcCodpais_Validate False
End Sub

Private Sub BtPesqproven_click()

    
    FormPesquisaRapida.InitPesquisaRapida "sl_proven", _
                        "descr_proven", "cod_proven", _
                        EcCodProven
    EcCodProven_Validate False
End Sub
Private Sub EcCodpais_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodPais.Text = UCase(EcCodPais.Text)
    If EcCodPais.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_pais FROM sl_pais WHERE cod_pais='" & Trim(EcCodPais.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Pa�s inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodPais.Text = ""
            EcDescrPais.Text = ""
        Else
            EcDescrPais.Text = BL_HandleNull(Tabela!descr_pais)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrPais.Text = ""
    End If
End Sub
Private Sub EcCodProven_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodProven.Text = UCase(EcCodProven.Text)
    If EcCodProven.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descR_proven FROM sl_proven WHERE cod_proven ='" & Trim(EcCodProven.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Proveni�ncia inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodProven.Text = ""
            EcDescrProven.Text = ""
        Else
            EcDescrProven.Text = BL_HandleNull(Tabela!descr_proven)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProven.Text = ""
    End If
End Sub

Private Function ConstroiCriterio(ByRef flg_criterio As Boolean) As String
    Dim sSql As String
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
    sSql = "SELECT DISTINCT x1.n_req,x1.n_req_assoc, x1.n_epis, x1.seq_utente, x4.cod_efr, x1.n_benef, x1.cod_med, x1.estado_req,  x1.cod_urbano,  x1.km,  x1.flg_facturado, "
    sSql = sSql & " x1.estado_req, x1.dt_chega, x1.user_cri, x1.hr_chega, x2.utente, x2.t_utente, x2.n_cartao_ute, x2.nome_ute, x1.cod_isencao, "
    sSql = sSql & " x1.convencao, x1.hemodialise, x1.fim_semana, x2.dt_nasc_ute, x1.cod_sala, x1.seq_utente_fact,x3.descr_efr, x3.flg_perc_facturar, "
    sSql = sSql & " x3.flg_controla_isencao, x3.flg_obriga_recibo, x3.deducao_ute, x3.FLG_ACTO_MED_EFR, x3.cod_empresa,x3.flg_compart_dom, "
    sSql = sSql & " x3.flg_nao_urbano, x3.flg_65anos, x2.dt_nasc_ute, x3.flg_separa_dom, x3.flg_dom_defeito, x3.flg_fds_fixo, x3.flg_insere_cod_rubr_efr, x2.sexo_ute, "
    sSql = sSql & " x1.cod_postal, x1.num_doe_prof, x1.cod_pais, x3.flg_nova_ars, x5.descr_postal, x3.flg_adse, x6.cod_proven ,x6.cod_efr cod_proven_efr, x6.descr_proven, x2.nome_dono  "
    sSql = sSql & " FROM sl_requis x1 LEFT OUTER JOIN sl_cod_postal x5 ON x1.cod_postal = x5.cod_postal LEFT OUTER JOIN SL_PROVEN x6 "
    sSql = sSql & " ON x1.cod_proven  = x6.cod_proven, "
    sSql = sSql & " " & tabela_aux & " x2, sl_efr x3, sl_recibos x4, sl_recibos_det x5 WHERE x1.seq_utente = x2.seq_utente AND x1.n_req = x4.n_req "
    sSql = sSql & " AND x4.cod_efr = x3.cod_efr AND x4.estado <> " & BL_TrataStringParaBD(gEstadoReciboAnulado)
    sSql = sSql & " AND x4.seq_recibo = x5.seq_recibo "
    'RGONCALVES 08.07.2014
    'sSql = sSql & " AND( x1.cod_isencao <> " & gTipoIsencaoNaoFacturar & " OR x1.cod_isencao IS NULL)"
    If gLAB <> "ICIL" Then
        sSql = sSql & " AND( x1.cod_isencao <> " & gTipoIsencaoNaoFacturar & " OR x1.cod_isencao IS NULL)"
    End If
    '
        
    If EcNumReq <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.n_req = " & EcNumReq
    End If
    
    If EcCredencial.Text <> "" And Len(EcCredencial.Text) >= 5 Then
        flg_criterio = True
        sSql = sSql & " AND x5.p1 = " & BL_TrataStringParaBD(EcCredencial.Text)
    End If
    
    If EcTipoUte <> "" And EcUtente <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x2.t_utente = " & BL_TrataStringParaBD(EcTipoUte) & " AND x2.utente = " & BL_TrataStringParaBD(EcUtente)
    End If
    
    If EcCodSala <> "" Then
        sSql = sSql & " AND x1.cod_sala = " & EcCodSala
        flg_criterio = True
    End If
    
    If EcDtChega <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.dt_chega = " & BL_TrataDataParaBD(EcDtChega)
    End If
        
    If EcCodEFR <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.cod_Efr = " & EcCodEFR
    End If
    
    If EcCodMed <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.cod_med = " & BL_TrataStringParaBD(EcCodMed)
    End If
    
    If EcCodProven <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.cod_proven = " & BL_TrataStringParaBD(EcCodProven)
    End If
    
    If CbConvencao.ListIndex <> mediComboValorNull Then
        flg_criterio = True
        sSql = sSql & " AND x1.convencao = " & BG_DaComboSel(CbConvencao)
    End If
    
    If CbCodUrbano.ListIndex <> mediComboValorNull Then
        flg_criterio = True
        sSql = sSql & " AND x1.cod_urbano = " & BG_DaComboSel(CbCodUrbano)
    End If
    
    If CbTipoIsencao.ListIndex <> mediComboValorNull Then
        flg_criterio = True
        sSql = sSql & " AND x1.cod_isencao = " & BG_DaComboSel(CbTipoIsencao)
    End If
    
    If EcNumBenef <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.n_benef = " & BL_TrataStringParaBD(EcNumBenef)
    End If
            
    If CbHemodialise.ListIndex <> mediComboValorNull Then
        flg_criterio = True
        sSql = sSql & " AND x1.hemodialise = " & BG_DaComboSel(CbHemodialise)
    End If
    
            
    If flg_criterio = False Then
        sSql = sSql & " AND x1.flg_facturado = 0 "
        flg_criterio = True
    End If
    
    If flg_criterio = False Then
        BG_Mensagem mediMsgBox, "Tem que indicar um crit�rio.", vbOKOnly + vbInformation, "Crit�rios"
        Exit Function
    End If
    
    If gSGBD = gOracle Then
        sSql = sSql & " ORDER BY n_req "
    Else
        sSql = sSql & " ORDER BY x1.n_req "
    End If
    ConstroiCriterio = sSql
End Function

' ---------------------------------------------------------------------------
' PREENCHE A GRELHA
' ---------------------------------------------------------------------------
Private Sub PreencheFGAna(ByVal linhaR As Long, ByVal linhaA As Long)
    FGAnalises.TextMatrix(linhaA, lColFactEFR) = EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_efr_envio
    FGAnalises.TextMatrix(linhaA, lColFactP1) = EF_ReqFact(linhaR).AnalisesFact(linhaA).p1
    FGAnalises.TextMatrix(linhaA, lColFactDescrAna) = EF_ReqFact(linhaR).AnalisesFact(linhaA).descrAna
    FGAnalises.TextMatrix(linhaA, lColFactTaxa) = EF_ReqFact(linhaR).AnalisesFact(linhaA).preco & " �"
    FGAnalises.TextMatrix(linhaA, lColFactCodAnaGH) = EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_rubr_efr
    FGAnalises.TextMatrix(linhaA, lColFactNrC) = EF_ReqFact(linhaR).AnalisesFact(linhaA).nr_c
    FGAnalises.TextMatrix(linhaA, lColFactRecibo) = EF_ReqFact(linhaR).AnalisesFact(linhaA).nRec & EF_RetornaTaxaRecibo(linhaR, EF_ReqFact(linhaR).AnalisesFact(linhaA).serieRec, EF_ReqFact(linhaR).AnalisesFact(linhaA).nRec, EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_efr, False) & "�"
    FGAnalises.TextMatrix(linhaA, lColFactQuantidade) = EF_ReqFact(linhaR).AnalisesFact(linhaA).qtd_ana_envio
    FGAnalises.TextMatrix(linhaA, lColFactCodAna) = EF_ReqFact(linhaR).AnalisesFact(linhaA).CodFacturavel
    FGAnalises.TextMatrix(linhaA, lColFactMedico) = EF_ReqFact(linhaR).AnalisesFact(linhaA).cod_medico
    FGAnalises.TextMatrix(linhaA, lColFactNrBenef) = EF_ReqFact(linhaR).AnalisesFact(linhaA).nr_benef
    FGAnalises.TextMatrix(linhaA, lColFactPercent) = EF_ReqFact(linhaR).AnalisesFact(linhaA).PercDescEnvio
    FGAnalises.TextMatrix(linhaA, lColFactOrdem) = EF_ReqFact(linhaR).AnalisesFact(linhaA).ordem
    FGAnalises.TextMatrix(linhaA, lColFactPortaria) = EF_ReqFact(linhaR).AnalisesFact(linhaA).Portaria

End Sub


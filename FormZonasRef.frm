VERSION 5.00
Begin VB.Form FormZonasRef 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormZonasRef"
   ClientHeight    =   3840
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9135
   Icon            =   "FormZonasRef.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3840
   ScaleWidth      =   9135
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcIdadeDias2 
      Height          =   285
      Left            =   5160
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   4680
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcIdadeDias 
      Height          =   285
      Left            =   5160
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   4320
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcNRes 
      Height          =   285
      Left            =   3240
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   4320
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcCodAnaRef 
      Height          =   285
      Left            =   1320
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   4680
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcSeqZonaRef 
      Height          =   285
      Left            =   1320
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   4320
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Frame FrZonas 
      Caption         =   " Zonas de Refer�ncia "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   120
      TabIndex        =   14
      Top             =   0
      Width           =   8895
      Begin VB.TextBox EcSemanas 
         Height          =   310
         Left            =   8040
         TabIndex        =   34
         Top             =   240
         Width           =   735
      End
      Begin VB.ComboBox EcTipoIdade2 
         Height          =   315
         Left            =   5640
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   240
         Width           =   1215
      End
      Begin VB.ComboBox EcTipoIdade 
         Height          =   315
         Left            =   3600
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcIdade 
         Height          =   310
         Left            =   2880
         TabIndex        =   1
         Top             =   240
         Width           =   735
      End
      Begin VB.Frame Frame5 
         Height          =   1215
         Left            =   6000
         TabIndex        =   15
         Top             =   600
         Width           =   2775
         Begin VB.CheckBox CkNaoImprVal 
            Caption         =   "N�o imprimir valores"
            Height          =   255
            Left            =   120
            TabIndex        =   35
            Top             =   840
            Width           =   2535
         End
         Begin VB.TextBox EcMax 
            Height          =   285
            Left            =   840
            TabIndex        =   12
            Tag             =   "200"
            Top             =   600
            Width           =   1815
         End
         Begin VB.TextBox EcMin 
            Height          =   285
            Left            =   840
            TabIndex        =   11
            Tag             =   "200"
            Top             =   240
            Width           =   1815
         End
         Begin VB.Label Label16 
            Caption         =   "M�nimo"
            Height          =   255
            Left            =   120
            TabIndex        =   17
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label15 
            Caption         =   "M�ximo"
            Height          =   255
            Left            =   120
            TabIndex        =   16
            Top             =   600
            Width           =   975
         End
      End
      Begin VB.ComboBox EcDescrSexo 
         Height          =   315
         Left            =   720
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   240
         Width           =   1455
      End
      Begin VB.TextBox EcCodDiag 
         Height          =   285
         Left            =   720
         TabIndex        =   5
         Tag             =   "200"
         Top             =   720
         Width           =   1095
      End
      Begin VB.TextBox EcDescrDiag 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1800
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   720
         Width           =   3615
      End
      Begin VB.TextBox EcCodOr 
         Height          =   285
         Left            =   720
         TabIndex        =   8
         Tag             =   "200"
         Top             =   1080
         Width           =   1095
      End
      Begin VB.TextBox EcDescrOr 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1800
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   1080
         Width           =   3615
      End
      Begin VB.CommandButton BtPesqDiag 
         Height          =   375
         Left            =   5400
         Picture         =   "FormZonasRef.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Pesquisa R�pida de Diagn�sticos"
         Top             =   675
         Width           =   375
      End
      Begin VB.CommandButton BtPesqOr 
         Height          =   375
         Left            =   5400
         Picture         =   "FormZonasRef.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Pesquisa R�pida de Outras Raz�es para a Zona"
         Top             =   1065
         Width           =   375
      End
      Begin VB.TextBox EcTextLivre 
         Height          =   1815
         Left            =   120
         MaxLength       =   1000
         MultiLine       =   -1  'True
         TabIndex        =   13
         Tag             =   "200"
         Top             =   1800
         Width           =   8655
      End
      Begin VB.TextBox EcIdade2 
         Height          =   310
         Left            =   5040
         TabIndex        =   3
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label7 
         Caption         =   "S. Gesta��o"
         Height          =   255
         Left            =   7080
         TabIndex        =   33
         Top             =   240
         Width           =   975
      End
      Begin VB.Line Line1 
         BorderWidth     =   2
         X1              =   4800
         X2              =   4920
         Y1              =   360
         Y2              =   360
      End
      Begin VB.Label Label4 
         Caption         =   "Idade"
         Height          =   255
         Left            =   2350
         TabIndex        =   28
         Top             =   240
         Width           =   735
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Sexo"
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Width           =   360
      End
      Begin VB.Label Label12 
         Caption         =   "Diag."
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label14 
         Caption         =   "Outros"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label18 
         Caption         =   "Texto Livre"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   1560
         Width           =   1455
      End
   End
   Begin VB.Label Label6 
      Caption         =   "EcIdadeDias2"
      Height          =   255
      Left            =   4080
      TabIndex        =   32
      Top             =   4680
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label5 
      Caption         =   "EcIdadeDias"
      Height          =   255
      Left            =   4080
      TabIndex        =   30
      Top             =   4320
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "EcNRes"
      Height          =   255
      Left            =   2520
      TabIndex        =   27
      Top             =   4320
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "EcCodAnaRef"
      Height          =   255
      Left            =   120
      TabIndex        =   25
      Top             =   4680
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "EcSeqZonaRef"
      Height          =   255
      Left            =   120
      TabIndex        =   23
      Top             =   4320
      Visible         =   0   'False
      Width           =   1215
   End
End
Attribute VB_Name = "FormZonasRef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    
    EcCodAnaRef.Text = FormCodAna.EcSeqAnaRef
    
    FuncaoProcurar
    BL_FimProcessamento Me
    
End Sub

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_ana_ref ASC"
    Else
    End If
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        FuncaoLimpar
    Else
        LimpaCampos
        PreencheCampos
        estado = 2
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"

    End If
    
    BL_FimProcessamento Me
End Sub

Sub PreencheCampos()

    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        Call EcCodOr_Validate(False)
        Call EcCodDiag_Validate(False)
    End If
    
End Sub

Sub FuncaoLimpar()
        
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        estado = 1
        BL_ToolbarEstadoN estado
    End If
    
End Sub


Sub LimpaCampos()
    
    BG_LimpaCampo_Todos CamposEc
    
    
    EcDescrDiag.Text = ""
    EcDescrOr = ""
    
    EcCodAnaRef.Text = FormCodAna.EcSeqAnaRef
    EcNRes.Text = 1
    CkNaoImprVal.value = vbGrayed
    
End Sub
Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub


Sub PreencheValoresDefeito()
    
    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    BG_PreencheComboBD_ADO "sl_tbf_t_data", "cod_t_data", "descr_t_data", EcTipoIdade
    BG_PreencheComboBD_ADO "sl_tbf_t_data", "cod_t_data", "descr_t_data", EcTipoIdade2
          
End Sub


Sub EventoUnload()
    
    BG_StackJanelas_Pop
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormZonasRef = Nothing
    
    FormCodAna.Enabled = True
    Set gFormActivo = FormCodAna
    
End Sub


Sub FuncaoAnterior()

    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
    
End Sub
Sub DefTipoCampos()
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    CkNaoImprVal.value = vbGrayed
End Sub


Private Sub BtPesqDiag_Click()
Dim ChavesPesq(1 To 2) As String
Dim CampoPesquisa As String
Dim CamposEcran(1 To 2) As String
Dim CWhere As String
Dim CFrom As String
Dim CamposRetorno As New ClassPesqResultados
Dim Tamanhos(1 To 2) As Long
Dim Headers(1 To 2) As String
Dim CancelouPesquisa As Boolean
Dim resultados(1 To 2)  As Variant
Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "DESCR_DIAG"
    CamposEcran(1) = "DESCR_DIAG"
    Tamanhos(1) = 3000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "sl_diag.cod_diag"
    CamposEcran(2) = "sl_diag.cod_diag"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_diag"
    CampoPesquisa = "DESCR_DIAG"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Diagn�sticos")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            FormZonasRef.EcCodDiag.Text = resultados(2)
            FormZonasRef.EcDescrDiag.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem diagn�sticos codificados!", vbExclamation, "Diagn�sticos"
    End If
End Sub

Private Sub BtPesqOr_Click()
Dim ChavesPesq(1 To 2) As String
Dim CampoPesquisa As String
Dim CamposEcran(1 To 2) As String
Dim CWhere As String
Dim CFrom As String
Dim CamposRetorno As New ClassPesqResultados
Dim Tamanhos(1 To 2) As Long
Dim Headers(1 To 2) As String
Dim CancelouPesquisa As Boolean
Dim resultados(1 To 2)  As Variant
Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "DESCR_OR_ZONAS"
    CamposEcran(1) = "DESCR_OR_ZONAS"
    Tamanhos(1) = 3000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "sl_or_zonas.cod_or_zonas"
    CamposEcran(2) = "sl_or_zonas.cod_or_zonas"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_or_zonas"
    CampoPesquisa = "DESCR_OR_ZONAS"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Outros")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            FormZonasRef.EcCodOr.Text = resultados(2)
            FormZonasRef.EcDescrOr.Text = resultados(1)
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem outras raz�es codificadas!", vbExclamation, "Outros"
    End If
End Sub





Private Sub EcCodDiag_Validate(Cancel As Boolean)
    
    Dim RsDiag As ADODB.recordset
    
    EcCodDiag.Text = UCase(EcCodDiag.Text)
    
    If Trim(EcCodDiag.Text) <> "" Then
        Set RsDiag = New ADODB.recordset
        With RsDiag
            .Source = "SELECT descr_diag FROM sl_diag WHERE cod_diag = " & BL_TrataStringParaBD(EcCodDiag.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
        
        If RsDiag.RecordCount > 0 Then
            EcDescrDiag.Text = BL_HandleNull(RsDiag!Descr_Diag, "")
        Else
            Cancel = True
            BG_Mensagem mediMsgBox, "Diagn�stico inexistente!", vbOK + vbExclamation, "Diagn�sticos"
        End If
        
        RsDiag.Close
        Set RsDiag = Nothing
    Else
        EcDescrDiag.Text = ""
    End If
    
End Sub






Private Sub EcCodOr_Validate(Cancel As Boolean)
    
    Dim RsOr As ADODB.recordset
    
    EcCodOr.Text = UCase(EcCodOr.Text)
    
    If Trim(EcCodOr.Text) <> "" Then
        Set RsOr = New ADODB.recordset
        
        With RsOr
            .Source = "SELECT descr_or_zonas FROM sl_or_zonas WHERE cod_or_zonas = " & BL_TrataStringParaBD(EcCodOr.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
        
        If RsOr.RecordCount <> 0 Then
            EcDescrOr.Text = BL_HandleNull(RsOr!descr_or_zonas, "")
        Else
            Cancel = True
            BG_Mensagem mediMsgBox, "'Outra raz�o' inexistente!", vbOK + vbInformation, App.ProductName
        End If
        
        RsOr.Close
        Set RsOr = Nothing
    Else
        EcDescrOr.Text = ""
    End If
    
End Sub


Private Sub EcDescrSexo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcDescrSexo.ListIndex = -1
End Sub


Private Sub EcTipoIdade_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcTipoIdade.ListIndex = -1
End Sub



Private Sub EcTipoIdade_Validate(Cancel As Boolean)
    
    If EcTipoIdade.ListIndex = CInt(gT_Crianca) Or EcTipoIdade.ListIndex = CInt(gT_Adulto) Then
        EcIdade.Text = "999"
        EcIdade2.Text = "999"
        EcIdadeDias.Text = "999"
        EcIdadeDias2.Text = "999"
        EcTipoIdade2.ListIndex = EcTipoIdade.ListIndex
    End If
    
    If EcIdade2.Text <> "999" And EcIdade.Text <> "999" And Trim(EcIdade.Text) <> "" Then
        EcIdadeDias.Text = BL_Converte_Para_Dias(EcIdade, BL_SelCodigo("sl_tbf_t_data", "cod_t_data", "descr_t_data", EcTipoIdade))
    
        If BL_String2Double(EcIdadeDias) > BL_String2Double(EcIdadeDias2) And BL_String2Double(EcIdadeDias2) <> 0 Then
            Cancel = True
            MsgBox "O valor inferior tem que ser menor que o valor superior", vbOKOnly + vbInformation, App.ProductName
        End If
    End If
    
End Sub


Private Sub EcTipoIdade2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcTipoIdade2.ListIndex = -1
End Sub




Private Sub EcTipoIdade2_Validate(Cancel As Boolean)
    
    If EcTipoIdade2.ListIndex = CInt(gT_Crianca) Or EcTipoIdade2.ListIndex = CInt(gT_Adulto) Then
        EcIdade2.Text = "999"
        EcIdade.Text = "999"
        EcIdadeDias.Text = "999"
        EcIdadeDias2.Text = "999"
        EcTipoIdade.ListIndex = EcTipoIdade2.ListIndex
    End If
    
    If EcIdade2.Text <> "999" And EcIdade.Text <> "999" And Trim(EcIdade2.Text) <> "" And Trim(EcIdade.Text) <> "" Then
        EcIdadeDias2.Text = BL_Converte_Para_Dias(EcIdade2, BL_SelCodigo("sl_tbf_t_data", "cod_t_data", "descr_t_data", EcTipoIdade2))
    
        If BL_String2Double(EcIdadeDias) > BL_String2Double(EcIdadeDias2) And BL_String2Double(EcIdadeDias) <> 0 Then
            Cancel = True
            MsgBox "O valor inferior tem que ser menor que o valor superior", vbOKOnly + vbInformation, App.ProductName
        End If
    End If
    
End Sub


Private Sub Form_Activate()
    EventoActivate
End Sub
Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    estado = 1
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub FuncaoModificar()

    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim RegistosAfectados As Integer
    
    gSQLError = 0
    condicao = ChaveBD & " = '" & BG_CvPlica(EcSeqZonaRef) & "'"
    
    Call EcTipoIdade_Validate(False)
    Call EcTipoIdade2_Validate(False)
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    RegistosAfectados = BG_ExecutaQuery_ADO(SQLQuery)
    
    
    rs.Requery
    
    LimpaCampos
    PreencheCampos
        
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Function ValidaCamposEc() As Integer

    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub BD_Insert()
    Dim SQLQuery As String

    gSQLError = 0
    gSQLISAM = 0
    
    EcSeqZonaRef = BG_DaMAX(NomeTabela, "seq_zona_ref") + 1
    Call EcTipoIdade_Validate(False)
    Call EcTipoIdade2_Validate(False)
    
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
        
    
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub

Sub Inicializacoes()

    Me.caption = " Zonas de Refer�ncia"
    Me.left = 740
    Me.top = 1100
    Me.Width = 9270
    Me.Height = 4215 '+ 2000
    
    NomeTabela = "sl_zona_ref"
    Set CampoDeFocus = EcDescrSexo
    
    NumCampos = 17
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "seq_zona_ref"
    CamposBD(1) = "cod_ana_ref"
    CamposBD(2) = "n_res"
    CamposBD(3) = "sexo"
    CamposBD(4) = "idade_inf"
    CamposBD(5) = "t_idade_inf"
    CamposBD(6) = "idade_inf_dias"
    CamposBD(7) = "idade_sup"
    CamposBD(8) = "t_idade_sup"
    CamposBD(9) = "idade_sup_dias"
    CamposBD(10) = "cod_diag"
    CamposBD(11) = "cod_or_zonas"
    CamposBD(12) = "ref_min"
    CamposBD(13) = "ref_max"
    CamposBD(14) = "txt_livre"
    CamposBD(15) = "s_gestacao"
    CamposBD(16) = "flg_inibe_val"
     
    ' Campos do Ecr�
    Set CamposEc(0) = EcSeqZonaRef
    Set CamposEc(1) = EcCodAnaRef
    Set CamposEc(2) = EcNRes
    Set CamposEc(3) = EcDescrSexo
    Set CamposEc(4) = EcIdade
    Set CamposEc(5) = EcTipoIdade
    Set CamposEc(6) = EcIdadeDias
    Set CamposEc(7) = EcIdade2
    Set CamposEc(8) = EcTipoIdade2
    Set CamposEc(9) = EcIdadeDias2
    Set CamposEc(10) = EcCodDiag
    Set CamposEc(11) = EcCodOr
    Set CamposEc(12) = EcMin
    Set CamposEc(13) = EcMax
    Set CamposEc(14) = EcTextLivre
    Set CamposEc(15) = EcSemanas
    Set CamposEc(16) = CkNaoImprVal
    
    ChaveBD = "seq_zona_ref"
    Set ChaveEc = EcSeqZonaRef
        
End Sub
Sub FuncaoRemover()

    Dim sql As String
    
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    BL_InicioProcessamento Me, "A eliminar registo."
    If gMsgResp = vbYes Then
        BD_Delete
    End If
    BL_FimProcessamento Me
    
End Sub

Sub FuncaoSeguinte()

    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
    
End Sub
Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    
    condicao = ChaveBD & " = '" & BG_CvPlica(EcSeqZonaRef) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    
    rs.Requery
    
    LimpaCampos
    PreencheCampos
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub



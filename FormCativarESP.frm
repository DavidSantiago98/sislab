VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormCativarESP 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cativa��o de Requisi��es"
   ClientHeight    =   6855
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   7005
   Icon            =   "FormCativarESP.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6855
   ScaleWidth      =   7005
   ShowInTaskbar   =   0   'False
   Begin VB.Frame frInfoDoente 
      Height          =   3255
      Left            =   240
      TabIndex        =   14
      Top             =   3480
      Width           =   6615
      Begin VB.Label lbNomeDescr 
         Caption         =   "Nome:"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   30
         Top             =   360
         Width           =   615
      End
      Begin VB.Label lSNSDescr 
         Caption         =   "Num. SNS:"
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   840
         Width           =   975
      End
      Begin VB.Label lbSNS 
         Height          =   255
         Left            =   960
         TabIndex        =   28
         Top             =   840
         Width           =   1695
      End
      Begin VB.Label lbDTNASCDescr 
         Caption         =   "Data Nascimento:"
         Height          =   255
         Left            =   3000
         TabIndex        =   27
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label lbSexoDescr 
         Caption         =   "Sexo:"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label lbSexo 
         Height          =   255
         Left            =   720
         TabIndex        =   25
         Top             =   1320
         Width           =   2175
      End
      Begin VB.Label lbNome 
         Height          =   255
         Left            =   720
         TabIndex        =   24
         Top             =   360
         Width           =   5295
      End
      Begin VB.Label lbDataNasc 
         Height          =   255
         Left            =   4440
         TabIndex        =   23
         Top             =   840
         Width           =   1935
      End
      Begin VB.Label lbEntidadeDescr 
         Caption         =   "Entidade:"
         Height          =   375
         Left            =   120
         TabIndex        =   22
         Top             =   1800
         Width           =   855
      End
      Begin VB.Label lbDominioDescr 
         Caption         =   "Dom�nio Entidade:"
         Height          =   375
         Left            =   120
         TabIndex        =   21
         Top             =   2760
         Width           =   1455
      End
      Begin VB.Label lbPaisDecr 
         Caption         =   "Pa�s:"
         Height          =   255
         Left            =   4320
         TabIndex        =   20
         Top             =   2280
         Width           =   495
      End
      Begin VB.Label lbEntidade 
         Height          =   375
         Left            =   960
         TabIndex        =   19
         Top             =   1800
         Width           =   5295
      End
      Begin VB.Label lbDominio 
         Height          =   375
         Left            =   1560
         TabIndex        =   18
         Top             =   2760
         Width           =   2535
      End
      Begin VB.Label lbPais 
         Height          =   375
         Left            =   4920
         TabIndex        =   17
         Top             =   2280
         Width           =   1575
      End
      Begin VB.Label lbCodEntDescr 
         Caption         =   "Cod. Entidade:"
         Height          =   375
         Left            =   120
         TabIndex        =   16
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label lbCodEnt 
         Height          =   375
         Left            =   1320
         TabIndex        =   15
         Top             =   2280
         Width           =   2655
      End
   End
   Begin VB.TextBox TextAux 
      Height          =   375
      Left            =   120
      TabIndex        =   12
      Top             =   7800
      Width           =   1095
   End
   Begin VB.TextBox EcNome 
      Height          =   375
      Left            =   4920
      TabIndex        =   11
      Top             =   7560
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox EcTipoU 
      Height          =   375
      Left            =   1320
      TabIndex        =   10
      Top             =   7800
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox EcUtente 
      Height          =   375
      Left            =   2400
      TabIndex        =   9
      Top             =   7680
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox EcPrestadora 
      Height          =   375
      Left            =   5040
      TabIndex        =   8
      Top             =   7920
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox EcMedico 
      Height          =   375
      Left            =   2400
      TabIndex        =   7
      Top             =   7800
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox EcEFR 
      Height          =   375
      Left            =   1440
      TabIndex        =   6
      Top             =   7680
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.PictureBox PicInsucesso 
      Height          =   290
      Left            =   3840
      Picture         =   "FormCativarESP.frx":000C
      ScaleHeight     =   225
      ScaleWidth      =   225
      TabIndex        =   5
      Top             =   7920
      Width           =   290
   End
   Begin VB.PictureBox PicSucesso 
      Height          =   285
      Left            =   4440
      Picture         =   "FormCativarESP.frx":00E4
      ScaleHeight     =   225
      ScaleWidth      =   225
      TabIndex        =   4
      Top             =   7920
      Width           =   290
   End
   Begin MSComctlLib.ImageList ImageListESP 
      Left            =   6000
      Top             =   7680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormCativarESP.frx":04AE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton btAvancar 
      Caption         =   "Avan�ar"
      Height          =   375
      Left            =   5160
      TabIndex        =   3
      Top             =   3000
      Width           =   1575
   End
   Begin VB.CommandButton BtCativar 
      Caption         =   "Cativar"
      Height          =   375
      Left            =   3360
      TabIndex        =   2
      Top             =   3000
      Width           =   1575
   End
   Begin VB.CommandButton BtCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   3000
      Width           =   1575
   End
   Begin VB.Frame FrCativar 
      Height          =   2535
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   6615
      Begin MSFlexGridLib.MSFlexGrid FGCativar 
         Height          =   2235
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   6375
         _ExtentX        =   11245
         _ExtentY        =   3942
         _Version        =   393216
      End
   End
End
Attribute VB_Name = "FormCativarESP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Private Const colPin = 0
Private Const colCodPrest = 1
Private Const colCred = 2
Private Const ColEstado = 3

Private estructESPOp() As ESPOperation

Dim totalCativa As Integer
Dim total
Dim erros As String
Dim UnloadForm As Boolean
Dim first As Boolean

'edgar.parada Num. SNS
Dim numSNS As String
'

Private Sub btAvancar_Click()

Dim i As Integer

    If Verifica_Cativacao = True Then
        'For i = 1 To UBound(estructESPOp)
        '    FGCativar.row = i
        '    FGCativar.Col = ColEstado
                FormGestaoRequisicaoPrivado.Enabled = True
                'FormGestaoRequisicaoPrivado.Show
                'Carrega a estrutura de an�lises do form de requisi��es
                FormGestaoRequisicaoPrivado.CarregaCativacao estructESPOp
                
                
            'End If
        'Next i
        UnloadForm = True
        Unload Me
    End If
    
End Sub

Private Sub BtCancelar_Click()
    UnloadForm = False
    Unload Me
End Sub

Private Sub BtCativar_Click()
    
    If Valida_Dados = True Then
        FazCativacao
    Else
        MsgBox "C�digos necess�rios para a cativa��o do pedido em falta!", vbInformation
    End If

End Sub

'Private Sub FGCativar_KeyDown(KeyCode As Integer, Shift As Integer)
'  If KeyCode = vbKeyDelete Then
'      FGCativar.RemoveItem (FGCativar.row)
'  End If
'End Sub

Private Sub Form_Load()
 'Inicializacoes
 'DefTipoCampos
 EventoLoad
End Sub

Sub DefTipoCampos()
  On Error GoTo TrataErro
  
  With FGCativar
        .rows = 2
        .FixedRows = 1

        .Cols = 4
        .FixedCols = 0
        
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        .row = 0
        
        .ColWidth(0) = 1400
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, 0) = "C�digo de Acesso"
        
        .ColWidth(1) = 1800
        .Col = 1
        .ColAlignment(1) = flexAlignLeftCenter
        .TextMatrix(0, 1) = "C�digo de Presta��o"
        
        .ColWidth(2) = 2000
        .Col = 2
        .TextMatrix(0, 2) = "Credencial"
        .ColAlignment(2) = flexAlignLeftCenter
        
        .ColWidth(3) = 1080
        .Col = 3
        .TextMatrix(0, 3) = "Estado"
        .ColAlignment(3) = flexAlignLeftCenter
        .WordWrap = False
        .row = 1
        .Col = 0
        
    End With
Exit Sub
TrataErro:
    BG_LogFile_Erros "DefTipoCampos: " & Err.Number & " - " & Err.Description, Me.Name, "DefTipoCampos"
    Exit Sub
    Resume Next
End Sub

Sub Form_Activate()

    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    Dim CapBt(2) As String
    Dim iret As Integer
    
    If UnloadForm = True Then
        EventoUnload
        Exit Sub
    End If
    
    If Verifica_Cativacao = False Then
       EventoUnload
    Else
        'gMsgTitulo = "Cativa��o de Requisi��es"
        'gMsgMsg = "J� existem requisi��es cativadas!" & vbNewLine & "Deseja continuar? Se disser que n�o ir� descativar as requisi��es!"
        'gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        CapBt(1) = "  &Cancelar "
        CapBt(2) = "  &Avan�ar "
        iret = FormMsgBox.CaixaMensagem("Existem credenciais cativadas. Continuar para gest�o de requisi��es?", 2, CapBt(), "Cativa��o de Requisi��es", vbQuestion)
        
        If iret = 1 Then
            EventoUnload
            'Cancel = False
        Else
            btAvancar_Click
            EventoUnload
        End If
        
    End If
End Sub


Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    'totalCativa = 0
    DefTipoCampos
    btAvancar.Enabled = False
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
        'edgar.parada Verificar se o utente tem SNS
    numSNS = ESP_GetSNSDoente(gDUtente.seq_utente)
    '
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    UnloadForm = False
End Sub

Sub Inicializacoes()

    Me.caption = "Cativa��o de Requisi��es"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 7215
    Me.Height = 3990 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    first = False
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = FormGestaoRequisicaoPrivado
    FormGestaoRequisicaoPrivado.Enabled = True
    
    BL_ToolbarEstadoN 0
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    'Set FormCativarESP = Nothing
    
  
End Sub

Sub LimpaCampos()

     If Verifica_Cativacao = False Then
        LimpaFGAna
        TextAux.text = Empty
        totalCativa = 0
        ReDim estructESPOp(1)
            Me.Height = 3990
    Else
        MsgBox "J� existem credenciais cativadas!", vbInformation
    End If
    

End Sub

Sub FuncaoLimpar()
    
    LimpaCampos

End Sub

Sub LimpaFGAna()
    Dim j As Long
    
    
    j = FGCativar.rows - 1
    While j > 0
        If j > 1 Then
            FGCativar.RemoveItem j
        Else
            FGCativar.TextMatrix(j, colPin) = ""
            FGCativar.TextMatrix(j, colCred) = ""
            FGCativar.TextMatrix(j, ColEstado) = ""
            FGCativar.TextMatrix(j, colCodPrest) = ""
            Set FGCativar.CellPicture = Nothing
        End If
        
        j = j - 1
    Wend
    
End Sub

'Private Sub FGCativar_keyPress(KeyAscii As Integer)
    
    'If FGCativar.Col = 0 Or FGCativar.Col = 1 Then
    '        FGCativar.Text = FGCativar.Text & Chr(KeyAscii)
    'End If
    
    'If KeyAscii = 13 And FGCativar.TextMatrix(FGCativar.rows - 1, colPin) <> Empty And FGCativar.TextMatrix(FGCativar.rows - 1, colCred) <> Empty Then
    '    FGCativar.rows = FGCativar.rows + 1
    'End If
  '  If KeyAscii = 13 Then
  '      FGCativar.AddItem TextAux.Text, 1
   ' End If
    
'End Sub

Private Sub FGCativar_DblClick()

On Error GoTo TrataErro
    Dim tT, tL, tH, Tw As Long
    
    With FGCativar

    FGCativar.row = .RowSel

    FGCativar.Col = .ColSel

    If (FGCativar.Col = colPin Or FGCativar.Col = colCred Or FGCativar.Col = colCodPrest) And estructESPOp(FGCativar.row).OpState = "" Then
        tT = .CellTop
        tL = .CellLeft
        tH = .CellHeight
        Tw = .CellWidth
        
        TextAux.left = tL + 350
        TextAux.top = tT + 460
        TextAux.Width = Tw + 20
        TextAux.Height = tH
        'Move the text box on the cell we dbl click on
        'Text1.Move tL + .left, tT + .top, tW, tH
        TextAux.Visible = True
        TextAux.ZOrder 0
        TextAux.SetFocus
        TextAux.text = FGCativar.TextMatrix(.RowSel, .ColSel)
        
    ElseIf FGCativar.Col = ColEstado And getErros(FGCativar.row, erros) = True Then
          BG_LimpaPassaParams
          gPassaParams.id = "FormCativarESP"
          gPassaParams.Param(0) = FGCativar.TextMatrix(FGCativar.row, colCred)
          gPassaParams.Param(1) = FGCativar.TextMatrix(FGCativar.row, colPin)
          gPassaParams.Param(2) = EcTipoU.text
          gPassaParams.Param(3) = estructESPOp(FGCativar.row).OpState
          gPassaParams.Param(4) = erros
          gPassaParams.Param(5) = EcUtente.text
          gPassaParams.Param(6) = EcNome.text
          'gPassaParams.Param(7) = estructESPOp(FGCativar.row).req.Patient.snsSISLAB
          gPassaParams.Param(7) = IIf(estructESPOp(FGCativar.row).req.Patient.snsSISLAB = "", ESP_GetSNSDoente(gDUtente.seq_utente), estructESPOp(FGCativar.row).req.Patient.snsSISLAB)
          FormVerificacaoCativESP.Show
    End If
        
    End With

Exit Sub


TrataErro:
    'edgar.parada Glintt-HS-19165 * tratar exception
    If Err.Number = 9 Then
      Resume Next
    End If
    '
    BG_LogFile_Erros "FGCativar_DblClick: " & Err.Number & " - " & Err.Description, Me.Name, "FGCativar_DblClick"
    Exit Sub
    Resume Next
End Sub

Private Sub TextAux_keyPress(KeyAscii As Integer)
    Dim key As Integer
    If KeyAscii = str(13) Then  'Enter
        If Valida_TamPinRequisicao() Then
            If CredencialRepetida(Trim(TextAux.text)) = False Then
                 Insere_Valores FGCativar.row, FGCativar.Col
                            If FGCativar.TextMatrix(FGCativar.RowSel, colCodPrest) = Empty Then
                    FGCativar.row = FGCativar.RowSel
                    FGCativar.Col = colCodPrest
                    FGCativar_DblClick
                ElseIf FGCativar.TextMatrix(FGCativar.RowSel, colCred) = Empty Then
                    FGCativar.row = FGCativar.RowSel
                    FGCativar.Col = colCred
                    FGCativar_DblClick
                ElseIf FGCativar.TextMatrix(FGCativar.RowSel, colPin) = Empty Then
                    FGCativar.row = FGCativar.RowSel
                    FGCativar.Col = colPin
                    FGCativar_DblClick
                End If
            Else
                TextAux.text = ""
                TextAux.Visible = False
                BG_Mensagem mediMsgBox, "Credencial repetida!", vbInformation, App.ProductName
            End If
        Else
           BG_Mensagem mediMsgBox, "O c�digo de acesso e c�digo de presta��o n�o podem exceder o tamanho 8. Credencial n�o pode exceder o tamanho 20!", vbInformation, App.ProductName
        End If
    End If
End Sub

Private Function CredencialRepetida(ByVal credencialInput As String) As Boolean
Dim i As Integer
CredencialRepetida = False

If FGCativar.Col <> colCred Then Exit Function

    For i = 1 To FGCativar.row
        If FGCativar.TextMatrix(i, colCred) = credencialInput Then
            CredencialRepetida = True
            Exit For
        End If
    Next i
    
End Function

Private Sub Insere_Valores(row As Integer, Col As Integer)
On Error GoTo TrataErro
Dim j As Integer

    If FGCativar.ColSel <> ColEstado Then
        FGCativar.TextMatrix(FGCativar.row, FGCativar.Col) = Trim(TextAux.text)
        
        TextAux.text = ""
    
        TextAux.Visible = False
        
        'BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        If FGCativar.TextMatrix(FGCativar.rows - 1, colPin) <> Empty And FGCativar.TextMatrix(FGCativar.rows - 1, colCred) <> Empty And FGCativar.TextMatrix(FGCativar.rows - 1, colCodPrest) <> Empty Then
'             If Not ExisteCredencial("", FGCativar.TextMatrix(FGCativar.row, colCred)) Then
'             'PreencheFGCativar
'            'ReDim estrutCativa(0)
'
'            'For j = 1 To FGCativar.row
                PreencheEstruturaOp FGCativar.TextMatrix(FGCativar.row, colPin), FGCativar.TextMatrix(FGCativar.row, colCred), FGCativar.TextMatrix(FGCativar.row, colCodPrest)
            'Next j
               FGCativar.rows = FGCativar.rows + 1
               BtCativar.Enabled = True
'            Else
'              FGCativar.Col = ColEstado
'              FGCativar.Text = "J� Existe!"
'              FGCativar.CellFontBold = True
'              BtCativar.Enabled = False
'              FGCativar.rows = FGCativar.rows + 1
'              BG_Mensagem mediMsgBox, "Credencial " & FGCativar.TextMatrix(FGCativar.row, colCred) & " j� existe em sistema", vbInformation, "Credencial j� existe"
'            End If
      End If
        
'        ElseIf totalCativa > 0 Then
'            If ((FGCativar.TextMatrix(FGCativar.row, colPin) <> CStr(estructESPOp(FGCativar.row).pin)) _
'            Or (FGCativar.TextMatrix(FGCativar.row, colCred) <> CStr(estructESPOp(FGCativar.row).ReqNumber))) And _
'            estructESPOp(FGCativar.row).OpState = "" Then
''
'                ActualizaEstrutura FGCativar.TextMatrix(FGCativar.row, colPin), FGCativar.TextMatrix(FGCativar.row, colCred), FGCativar.row
''Elsecol
''                FGCativar.TextMatrix(FGCativar.row, colCred) = estructESPOp(FGCativar.row).ReqNumber
''                FGCativar.TextMatrix(FGCativar.row, colPin) = estructESPOp(FGCativar.row).pin
''            End If
    End If
    Exit Sub
TrataErro:
    BG_LogFile_Erros "FGCativar_DblClick: " & Err.Number & " - " & Err.Description, Me.Name, "FGCativar_DblClick"
    Exit Sub
    Resume Next
End Sub


Private Function Verifica_Cativacao() As Boolean

On Error GoTo TrataErro
    Dim i As Integer
    Dim sucesso As String
    sucesso = cSUCESSO
    
    Verifica_Cativacao = False
        If totalCativa > 0 Then
        For i = 1 To FGCativar.rows - 2
            If estructESPOp(i).OpState = sucesso Then 'And estructESPOp(i).ExistFgAna = "" Then  'And estructESP(i).OperationId = estructESP(i).req(i).OperationId Then
                Verifica_Cativacao = True
            End If
        Next i
    End If
    Exit Function
    
TrataErro:
    BG_LogFile_Erros "Verifica_Cativacao: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Cativacao"
    Exit Function
    Resume Next
End Function

Private Function Valida_Dados() As Boolean

On Error GoTo TrataErro
    Dim i As Integer
    'EstadoCativacao.erro
    Dim Estado As Boolean
    
    Valida_Dados = True
    For i = 1 To FGCativar.row
        If FGCativar.TextMatrix(i, colPin) <> "" And FGCativar.TextMatrix(i, colCred) <> "" And FGCativar.TextMatrix(i, colCodPrest) <> "" Then
            Valida_Dados = True
        Else
            Valida_Dados = False
        End If
    Next i
    
 Exit Function

TrataErro:
    BG_LogFile_Erros "Valida_Dados: " & Err.Number & " - " & Err.Description, Me.Name, "Valida_Dados"
    Exit Function
    Resume Next
End Function

Private Function Valida_TamPinRequisicao() As Boolean

On Error GoTo TrataErro
    Dim i As Integer
    'EstadoCativacao.erro
    Dim Estado As Boolean
    'Len(FGCativar.TextMatrix(i, colCred)) > 20
    'For i = 1 To FGCativar.row
        If FGCativar.Col = colPin And Len(Trim(TextAux.text)) > 8 Then
            Valida_TamPinRequisicao = False
        ElseIf FGCativar.Col = colCred And Len(Trim(TextAux.text)) > 20 Then
            Valida_TamPinRequisicao = False
        ElseIf FGCativar.Col = colCodPrest And Len(Trim(TextAux.text)) > 8 Then
            Valida_TamPinRequisicao = False
        Else
            Valida_TamPinRequisicao = True
        End If
    'Next i
    
 Exit Function
TrataErro:
    BG_LogFile_Erros "Valida_TamPinRequisicao: " & Err.Number & " - " & Err.Description, Me.Name, "Valida_TamPinRequisicao"
    Exit Function
    Resume Next
End Function

Private Sub FazCativacao()
    Dim i As Integer
    Dim sql As String
    Dim rs As New ADODB.recordset
    
'    FGCativar.Col = ColEstado
'    Set FGCativar.CellPicture = ImageListESP.ListImages(1).Picture
    
    estructESPOp() = ESP_CativaPedido(estructESPOp, EcPrestadora.text, EcMedico.text, EcEFR.text, numSNS)
    'If UBound(estructESPOp) = LBound(estructESPOp) Then
    For i = 1 To UBound(estructESPOp)
        'If estructESPOp(i).ExistFgAna = "" Then
        
'            FGCativar.row = i
'            FGCativar.Col = ColEstado
    
'            If estructESPOp(i).OpState = cINSUCESSO Then
'                'btAvancar.Enabled = False
'                'BtCativar.Enabled = False
'                Set FGCativar.CellPicture = PicInsucesso.Image
'            ElseIf estructESPOp(i).OpState = cSUCESSO Then
'                btAvancar.Enabled = True
'                Set FGCativar.CellPicture = PicSucesso.Image
'            End If
'        'End If
          colocaEstado i
                  preencheValoresPatient i
    Next i
    If Verifica_Cativacao Then
        btAvancar.Enabled = True
    End If
    'FGCativar.ColAlignment(ColEstado) = flexAlignCenterCenter
End Sub



Private Sub PreencheEstruturaOp(ByVal pin_agend As String, ByVal credencial As String, ByVal codPrestacao As String)
                             
    totalCativa = totalCativa + 1
    ReDim Preserve estructESPOp(totalCativa)
    'If pin_agend <> "" And credencial <> "" Then
      estructESPOp(totalCativa).pin = pin_agend
      estructESPOp(totalCativa).ReqNumber = credencial
      estructESPOp(totalCativa).codPrestacao = codPrestacao
      estructESPOp(totalCativa).OperationDate = Bg_DaDataHora_ADO
      estructESPOp(totalCativa).OperationUser = gCodUtilizador
      estructESPOp(totalCativa).codLocal = gCompanyDb '"DEMOPRIV" 'NELSONPSILVA 23.10.2018
      estructESPOp(totalCativa).OperationType = "Cativa��o"
      estructESPOp(totalCativa).Origin = "SISLAB"
   ' End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheEstruturaOp: " & Err.Description, Me.Name, "PreencheEstruturaOp", False
    Exit Sub
    Resume Next
End Sub
'edgar.parada Glintt-HS-19165 * obter erros de cativa��o para apresentar no ecr� de valida��o de erros
Function getErros(ByVal Index As Integer, ByRef erros As String) As Boolean
  'Dim i As Integer
  Dim j As Integer

  On Error GoTo TrataErro
  
  erros = ""
   
     If estructESPOp(Index).OpState = cINSUCESSO Then
        For j = 1 To UBound(estructESPOp(Index).ErrorOp)
          If estructESPOp(Index).ErrorOp(j).errorCode <> "" Then
            erros = erros & estructESPOp(Index).ErrorOp(j).errorDescr & vbCrLf
          End If
        Next j
       getErros = True
     Else
       getErros = False
     End If
  Exit Function
TrataErro:
    BG_LogFile_Erros "getErros: " & Err.Number & " - " & Err.Description, Me.Name, "getErros"
    Exit Function
    Resume Next
End Function
'
'edgar.parada Glintt-HS-19165 * estrutura passada para do form Gestap de Requisi��es privado
Friend Sub passaEstrur(ByRef Estrut() As ESPOperation)
    Dim i As Integer
    
    estructESPOp = Estrut
    totalCativa = UBound(estructESPOp)
    
    On Error GoTo TrataErro
    
    If totalCativa > 0 Then
       For i = 1 To UBound(estructESPOp)
       FGCativar.TextMatrix(i, colPin) = estructESPOp(i).pin
       FGCativar.TextMatrix(i, colCodPrest) = estructESPOp(i).codPrestacao
       FGCativar.TextMatrix(i, colCred) = estructESPOp(i).ReqNumber
       colocaEstado i
       FGCativar.AddItem ""
       Next i
    End If
    If Verifica_Cativacao Then
        btAvancar.Enabled = True
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "passaEstrur: " & Err.Description, Me.Name, "passaEstrur", False
    Exit Sub
    Resume Next
End Sub
'
'edgar.parada Glintt-HS-19165 * Coloca estado na linha de cativa��o
Private Sub colocaEstado(indice As Integer)
On Error GoTo TrataErro
    FGCativar.row = indice
    FGCativar.Col = ColEstado
    
    'first = False
    
    If estructESPOp(indice).OpState = cINSUCESSO Then
        Set FGCativar.CellPicture = PicInsucesso.Image
    ElseIf estructESPOp(indice).OpState = cSUCESSO Then
        'btAvancar.Enabled = True
        Set FGCativar.CellPicture = PicSucesso.Image
        'para saber qual a primeira com sucesso
        If first = False Then
           estructESPOp(indice).first = "S"
           first = True
        End If
        '
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "colocaEstado: " & Err.Description, Me.Name, "colocaEstado", False
    Exit Sub
    Resume Next
End Sub
'
'edgar.parada Glintt-HS-19165 * Preencher dados do patient cativado
Private Sub preencheValoresPatient(indice As Integer)

On Error GoTo TrataErro

 Me.Height = 7215
 lbNome = estructESPOp(indice).req.Patient.Name
 lbSNS = estructESPOp(indice).req.Patient.SNS
 lbDataNasc = Format(estructESPOp(indice).req.Patient.birthDate, "dd/mm/yyyy")
 If estructESPOp(indice).req.Patient.sex = "" Then
   lbSexo = ""
 Else
   lbSexo = IIf(estructESPOp(indice).req.Patient.sex = "F", "Feminino", "Maculino")
 End If
 
 lbEntidade = estructESPOp(indice).req.Entity.EntityDescr
 lbCodEnt = estructESPOp(indice).req.Entity.codEntity
 lbDominio = estructESPOp(indice).req.Entity.entityDomain
 lbPais = estructESPOp(indice).req.Entity.Country
 
Exit Sub
TrataErro:
    BG_LogFile_Erros "preencheValoresPatient: " & Err.Description, Me.Name, "preencheValoresPatient", False
    Exit Sub
    Resume Next
End Sub
'

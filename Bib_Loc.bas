Attribute VB_Name = "BibliotecaLocal"
Option Explicit

' Actualiza��o : 15/02/2002
' T�cnico Paulo Costa
 
 ' Biblioteca de Rotinas Locais a esta Aplica��o

' APIS --------------------------------------- ---------
 
'Defeni��es para a API que desactiva o bot�es de fechar a form
Public Declare Function GetSystemMenu Lib "user32" (ByVal hwnd As Long, ByVal bRevert As Long) As Long
Public Declare Function GetMenuItemCount Lib "user32" (ByVal hMenu As Long) As Long
Public Declare Function RemoveMenu Lib "user32" (ByVal hMenu As Long, ByVal nPosition As Long, ByVal wFlags As Long) As Long
Public Const MF_BYPOSITION = &H400&
Public Const MF_REMOVE = &H1000&

' ProgressBar
#If Win32 Then
    Public Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long
#Else
    Public Declare Function BitBlt Lib "GDI" (ByVal hDestDC As Integer, ByVal X As Integer, ByVal Y As Integer, ByVal nWidth As Integer, ByVal nHeight As Integer, ByVal hSrcDC As Integer, ByVal xSrc As Integer, ByVal ySrc As Integer, ByVal dwRop As Long) As Integer
#End If


' Cursor animado
Public Const GCL_HCURSOR = -12
Declare Function ClipCursor Lib "user32" (lpRect As Any) As Long
Declare Function DestroyCursor Lib "user32" (ByVal hCursor As Any) As Long
Declare Function LoadCursorFromFile Lib "user32" Alias "LoadCursorFromFileA" (ByVal lpFileName As String) As Long
Declare Function SetClassLong Lib "user32" Alias "SetClassLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Declare Function GetClassLong Lib "user32" Alias "GetClassLongA" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Public CursorNor As Long, CursorAni As Long
Public Estadocursor As Integer '0-normal, 1-animado

' Forms
Type RECT
    left As Long
    top As Long
    Right As Long
    Bottom As Long
End Type
Type POINTAPI
    X As Long
    Y As Long
End Type
'Define e retira a posicao do cursor
Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Declare Function SetCursorPos Lib "user32" (ByVal X As Long, ByVal Y As Long) As Long
'Retira os left, top, right, bottom duma Form
Declare Function GetWindowRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long
'Procura uma Form (ou pela Classe ou pelo T�tulo)
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Public Declare Function FindWindowEx Lib "user32" Alias "FindWindowExA" (ByVal hWnd1 As Long, ByVal hWnd2 As Long, ByVal lpsz1 As String, ByVal lpsz2 As String) As Long
'P�e a Form activa
Public Declare Function Putfocus Lib "user32" Alias "SetFocus" (ByVal hwnd As Long) As Long
'Fecha a Form
Public Declare Function PostMessage Lib "user32" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Public Const WM_CLOSE = &H10
'Abre a Form
Public Declare Function ShowWindow Lib "user32" (ByVal hwnd As Long, ByVal nCmdShow As Long) As Long
Public Declare Function BringWindowToTop Lib "user32" (ByVal hwnd As Long) As Long
'Modos de abertura para o Show Window
Public Const SW_MAXIMIZE = 3
Public Const SW_MINIMIZE = 6
Public Const SW_HIDE = 0
Public Const SW_NORMAL = 1
'Minimiza a Form
Public Declare Function DestroyWindow Lib "user32" (ByVal hwnd As Long) As Long


' CONSTANTES ------------------------------------------------

'constantes para formata��o de campos num�ricos
'utilizadas nas rotinas de gest�o de classes do tipo DataRepeater
Public Const cF_INTEGER = "############"
Public Const cF_DOUBLE_SIMPLES = "############0.00"
Public Const cF_DOUBLE = "###,###,###0.00"
Public Const cF_INTEGER_FORMATED = "###,###,###,###"
Public Const cF_MONEY_ESC_LNG = "###,###,###,### Esc"
Public Const cF_MONEY_ESC_DBL = "###,###,###,###0.00 Esc"
Public Const cF_MONEY_EUR = "###,###,###,###0.00 Eur"

' N�mero da an�lise simples caso a flg marca��o membros seja=0
Global Const gGHOSTMEMBER_S = "S99999"

' pferreira 2010.06.30
' N�mero da an�lise perfis caso a flg marca��o membros seja=0
Global Const gGHOSTMEMBER_C = "C99999"

'constantes usadas para se saber a origem da an�lise marcada para preencher a esttrutura de memoria de marcacoes (Mareq)
'Exemplo:quando se faz procurar e a an�lise vem da tabela de marcacoes a constante usada
    '� OrigemMarcacoes, qd se est� a introduzir an�lises novas a constante usada � a cOrigemNova
Global Const cOrigemNova = 0
Global Const cOrigemMarcacoes = 1
Global Const cOrigemRealiza = 2
Global Const cOrigemRealiza_h = 3

'Modos de edi��o de resultados
Global Const cModoResNada = 0
Global Const cModoResIns = 1
Global Const cModoResAlt = 2
Global Const cModoResVal = 3
Global Const cModoResTecAltVal = 5
Global Const cModoResTecVal = 6
Global Const cModoResInsVal = 7
Global Const cModoResValApar = 8
Global Const cModoResGeral = 9
Global Const cModoResValAValTec = 10
Global Const cModoResMicro = 11
' FGONCALVES - ALTEREI - 2007.10.09
Global Const cModoResAssinatura = 12
Global Const cModoResHistorico = 13
Global Const cModoResAltValidacao = 14
Global Const cModoResAssinParcial = 15
Global Const cModoResValPendentes = 16
Global Const cModoResValAutomatica = 17

'Tipos de permiss�o dos utlizadores no tratamento de resultados
Global Const cIntRes = 0
Global Const cValTecRes = 1
Global Const cValMedRes = 2

'Conteudo do resultado de an�lises sem resultado com observa��es
Global Const cVerObservacoes = "{Ver Observa��es}"
Global Const cVerAnexo = "{Ver Anexo}"
'Conteudo do resultado de an�lises sem resultado que n�o s�o para efectuar,
'mas que deve-se manter o registo
Global Const cSemResultado = "Sem Resultado"
Global Const cAnaliseEmCurso = "An�lise em curso"

'Constante usada para os Protocolos:N� de requisi��es por linha
Global Const cMAX_PROT = 4


' Vari�veis ------------------------------------------------

' Identifica��o dos grupos de utilizadores da aplica��o
Global gGrupoAdministradores As Integer
Global gGrupoMedicos As Integer
Global gGrupoEnfermeiros As Integer
Global gGrupoTecnicos As Integer
Global gGrupoSecretariado As Integer
Global gGrupoChefeSecretariado As Integer
Global gGrupoExternos As Integer
Global gGrupoFarmaceuticos As Integer
'rcoelho 22.08.2013 chvng-4412
Global gGrupoTecnicosSuperiores As Integer
Global gGrupoTecSupValidacao As Integer

'Vari�vel que guarda o Handle da Form MDICLIENT calculada no Load da Form MDI
Public gParentChild As Long

'Declara��es globais

Private Type Tipo_BotoesToolbarVisiveis
    sForm As String
    sBotao As String
    iEstado As Integer
    sObjectivo As String
End Type

Global gInstituicao As String

Global gTipoInstituicao As String
Global Const gTipoInstituicaoPrivada = "PRIVADA"
Global Const gTipoInstituicaoHospitalar = "HOSPITALAR"
Global Const gTipoInstituicaoVet = "VETERINARIA"
Global Const gTipoInstituicaoAguas = "AGUAS"

Global gSistemaInterrogacao As String

'Defini��es necess�rias para a Rotina BL_CriaTabela
Public Type DefTable
    NomeCampo As String
    tipo As String
    tamanho As Integer
    PInteira As Byte
    PDecimal As Byte
End Type

'Defini��es necess�rias para a Rotina Verifica_DC na Form Resultados
Public Enum ValidadeAnalise
    SemValidade = 0
    Invalido = 1
    Valido = 2
End Enum

' Cria��o do tipo 'gPassaParams'
Public Type Tipo_PassaParams
    id As String
    Param(0 To 10) As Variant
End Type
Global gPassaParams As Tipo_PassaParams

'Variaveis de Tipos
Public gT_Horizontal As String
Public gT_Vertical As String
Public gT_Livre As String
Public gT_Solteiro As String
Public gT_Casado As String
Public gT_Viuvo As String
Public gT_Divorciado As String
Public gT_Sep_Judicial As String
Public gT_Uniao_Facto As String
Public gT_Outro As String
Public gT_Feminino  As String
Public gT_Masculino As String
Public gT_Indeterminado As String
Public gT_Sim As String
Public gT_Nao As String
Public gT_NaoVisualizar As String
Public gT_ApenasVisualizar As String
Public gT_VisualizarAlterar As String
Public gT_Geral As String
Public gT_Computador As String
Public gT_Utilizador As String
Public gT_Telefone As String
Public gT_Email As String
Public gT_Fax As String
Public gT_ImpressoraRede As String
Public gT_Dias As String
Public gT_Meses As String
Public gT_Anos As String
Public gT_Crianca As String
Public gT_Adulto As String
Public gT_Alfanumerico As String
Public gT_Numerico As String
Public gT_Frase As String
Public gT_Microrganismo As String
Public gT_Antibiograma As String
Public gT_Auxiliar As String
Public gT_Relatorio As String
Public gT_Protegido As String

Public gT_Acidente As String
Public gT_Ambulatorio As String
Public gT_Cons_Inter As String
Public gT_Cons_Telef As String
Public gT_Consulta As String
Public gT_Consumos As String
Public gT_Credenciais As String
Public gT_Diagnosticos As String
Public gT_Exame As String
Public gT_Ficha_ID As String
Public gT_Fisio As String
Public gT_HDI As String
Public gT_Internamento As String
Public gT_Intervencao As String
Public gT_MCDT As String
Public gT_Plano_Oper As String
Public gT_Pre_Intern As String
Public gT_Prescricoes As String
Public gT_Prog_Cirugico As String
Public gT_Protoc As String
Public gT_Referenciacao As String
Public gT_Reg_Oper As String
Public gT_Tratamentos As String
Public gT_Urgencia As String
Public gT_Externo As String
Public gT_LAB As String
Public gT_RAD As String
Public gT_Bloco As String
Public gT_PROVISORIO As String

Public gEFRCodificada As String
Public gEFRNaoCodificada As String
Public gEFRIndependente As String
Public gEFRBancos As String

'Variaveis globais para ver se os forms est�o abertos
'0 - Fechado; 1 - Aberto
Public gF_GRUPOSTRAB As Integer
Public gF_PERFISANA As Integer
Public gF_EXAMES As Integer
Public gF_MODELOLIVRE As Integer
Public gF_PROVENIENCIA As Integer
Public gF_GRANTIB As Integer
Public gF_AUTORES As Integer
Public gF_AUTORESSERIE As Integer
Public gF_IDENTIF As Integer
Public gF_IDENTIF_VET As Integer
Public gF_IDENTIF_PESQ As Integer
Public gF_REQUIS As Integer
Public gF_REQUIS_PRIVADO As Integer
Public gF_REQUIS_AGUAS As Integer
Public gF_CHEGA_TUBOS As Integer
Public gF_REQUIS_VET As Integer
Public gF_FILA_ESPERA As Integer
Public gF_2RES As Integer
Public gF_PRODUTOS As Integer
Public gF_REQUTE As Integer
Public gF_RESULT As Integer
Public gF_RESULT_NOVO As Integer
Public gF_RESMICRO As Integer
Public gF_LRES As Integer
Public gF_TRANSF As Integer
Public gF_REQPENDANA As Integer
Public gF_DIARIOPEND As Integer
Public gF_REQPENDELECT As Integer
Public gF_TRATAREJ As Integer
Public gF_REQCONS As Integer
Public gF_REQCONS_NOVO As Integer
Public gF_INFCLI As Integer
Public gF_SELRES As Integer
Public gF_RESMULT As Integer
Public gF_IMPRRESULTEXT As Integer
Public gF_FACTUSENVIO As Integer
Public gF_ENTREGA_RESULTADOS As Integer
Public gF_HIPO_RESULTADOS As Integer
Public gF_EMISSAO_RECIBOS As Integer
Public gF_EMISSAO_NOTA_CRED As Integer
Public gF_SEROTECA_GESTAO As Integer

'Moeda utilizada: PTE ou ESC
Public gMoedaActiva As String
'Factor de convers�o do Euro
Public gConvEur As Double

'Vari�vel que indica a utiliza��o ou n�o de recibos na aplica��o
Public gRecibos As String

'Estrutura que guarda os dados do utente
Public Type Dados_Utentes
    seq_utente As String
    Utente As String
    t_utente As String
    n_proc_1 As String
    n_proc_2 As String
    n_cartao_ute As String
    dt_inscr As String
    nome_ute As String
    dt_nasc_ute As String
    sexo_ute As String
    descr_sexo_ute As String
    est_civil As String
    descr_est_civil As String
    cod_efr As String
    nr_benef As String
    morada As String
    cod_postal As String
    telefone As String
    telemovel As String
    cod_profis As String
    situacao As String
    episodio As String
    n_prescricao As String
    cod_pais As String
    num_doe_prof As String
    cod_isencao_ute As String
    email As String
    cod_genero As String
    'ICIL-930
    num_contribuinte As String
End Type
Global gDUtente As Dados_Utentes

'Para a Form da impressora - FormPrinter
Public Type Tipo_Impressora
    nome As String
    Copias As Long
    Collate As Boolean
    PgInicial As Long
    PgFinal As Long
End Type
Global gImpressoraActiva As Tipo_Impressora

'indica se j� foi procurada uma requisi��o (que j� existe)
Global gFlg_JaExisteReq As Boolean

'Vector que vai conter o codigo das entidades pertencentes � gARS
Public gARS()

'Vector que vai conter o codigo das entidades pertencentes � gADSE
Public gADSE()

' Ultima requisi��o seleccionada
Global gRequisicaoActiva As Long

' Serie dos recibos
Global gSerieRec As String

' Impress�o de hist�rico de an�lises no boletim de resultados
Global gImp_Res_Ant As Integer

' Variavel que guarda o modo de tratamento dos resultados
Global gModoRes As Integer

' Variavel que guarda a permiss�o que o utilizador tem para tratamento dos resultados
Global gPermResUtil As Integer

' Variavel que guarda a permiss�o que o utilizador tem para Assinar requisicoes
Global gPermReqAssinar As Integer

' Variavel que guarda o select dos resultados
Global gSqlSelReq As String
Global gSqlSelRes As String
Global gSqlSelResSELECT1 As String
Global gSqlSelResFROM1 As String
Global gSqlSelResWHERE1 As String
Global gSqlSelResSELECT2 As String
Global gSqlSelResFROM2 As String
Global gSqlSelResWHERE2 As String
Global gSqlSelResORDER As String

' Variavel que guarda a impressora de etiquetas activa para o utilizador
Global gImpZebra As String

' Vari�vel que indica se � obrigat�rio descriminar o grupo de an�lises
' na sele��o de resultados
Global gObrigaGrupo As String

'Variavel que guarda a permissao do utilizador para alterar pre�os dos recibos
Global gPermAltPreco As Integer



'Lista que vai conter os dados necess�rios � marca��o das an�lises
Public Type MarcacaoAnalises
    Cod_Perfil As String
    Descr_Perfil As String
    cod_ana_s As String
    descr_ana_s As String
    cod_ana_c As String
    Descr_Ana_C As String
    Ord_Ana As Long
    Ord_Ana_Compl As Integer
    Ord_Ana_Perf As Integer
    cod_agrup As String
    N_Folha_Trab As Long
    dt_chega As String
    Flg_Apar_Transf As String
    flg_estado As String
    Flg_Facturado As Integer
    Ord_Marca As Long
    transmit_psm As Integer
    n_envio As Long
    seq_req_tubo As Long
    indice_tubo As Integer
End Type
'Lista que vai conter os dados necess�rios � marca��o das an�lises
Global MaReq() As MarcacaoAnalises

'Estrutura usada para calcular o valor do recibo da requisi��o
Public Type Estrutura_Recibo
    cod_rubr As String
    descr_rubr As String
    Valor_Taxa As Double
    Valor_Ute As Double
    Valor_Ent As Double
    Perc_Ute As Double
End Type

'Tamanho da letra para as observa��es e coment�rios no Report Resultados
Public gResTamLetra As Integer
Public gResTipoLetra As String


'Estrutura usada para guardar os dados do HIS
Public Type Tipo_HIS
    nome As String
    DataSource As String
    uID As String
    PWD As String
    LoginTimout As String
End Type

Public HIS As Tipo_HIS
'Indica se existe liga��o � gest�o de doentes
Global gHIS_Import As Integer
'Vari�vel de conex�o ao HIS
Global gConnHIS As ADODB.Connection


'Variavel que indica se o nome do utente no cliente � capitalizado ou n�o
Global gNomeCapitalizado As Integer

'Variaveis usadas na factura��o pela aplica��o FACTUS
Global gCodGrupoRubr As Long
Global gCodTipRubr As Long
Global gCodServExec As String
Global gCodServExecFACTUS As String
Global gCodProg As Long

'Conexao secundaria
Global gConexaoSecundaria As ADODB.Connection
Global gConexaoEresults As ADODB.Connection

Global gConSec As String

'Conexao Interfaces - SMS
Global gConexaoInterfaces As ADODB.Connection
Global gConInterfaces As String

Global gCodIsencaoDefeito As Integer

'Indica se a morada passa para o report de resultados ou n�o
Global gImprimeMoradaRes As Integer

'Indica o c�digo das an�lises simples a inserir por bot�o(utilizado no FormResultados)
Global gCodAnaSimples1 As String
Global gCodAnaSimples2 As String

'variaveis usadas na liga��o � GesDoc
Global gGDServerPath As String
Global gGDServerPort As String
Global gGDServerAddr As String
'Indica se � para abrir ecr� de relat�rios ou n�o
Global gMultiReport As Integer
'Variavel com dados para a Conexao a GesDoc
Global gDSNGesDoc As String
'Vari�vel de conex�o a GesDoc
Global gConnGesDoc As ADODB.Connection
'variavel q indica se a liga��o a gesdoc esta aberta
Dim GesDocAberto As Integer
'Variavel q indica o url do servlet GesDoc
Global gPutServlet As String
'variavel q indica o url para editar doc na GesDoc
Global gGDEditDoc As String
'Indica o caminho para os documentos a abrir pela GesDoc
Global gGDDocPath As String
'variavel q indica o id da pasta default GesDoc
Global gGDDefFolder As String
'variavel q indica o id do tipo de documento GesDoc
Global gGDDocType As String
'variaveis usadas para guardar o relatorio a imprimir na Gesdoc
Global gRelActivoFich As String
Global gRelActivoDescr As String
Global gRelActivoTipo As Integer
Global ErroImpressao As Boolean

'Indica se se pode imprimir o boletim de resultados apenas com valida��o tecnica
Global gImprimirComValTec As Integer

'Indica as analises complexas cujas as analises simples podem
    'ser feitas em 2 aparelhos diferentes e cada aparelho tem
    'valores de referencia diferentes
Global gAnaComp2Aparelhos As String

'Indica se a validacao do campo requisicao associada no formgestaorequisicoes deve ser feito ou nao
Global gValidarReqAssociada As Integer

'Indica o texto (relativo ao responsavel) a sair no boletim para o caso deste sair apenas com valida��o tecnica
Global gTextoValidacaoTecnicaBoletim As String

'Usada na BL_ImprimeResultados
Public Type Estrutura_Frases
    CodAnaC As String
    ordem As Integer
    frase As String
    Ja_Foi_Inserida As Boolean
End Type


' Ultimo episodio seleccionado
Global gEpisodioActivo As String
Global gEpisodioActivoAux As String
'Ultima ReqAssoc(carimbo do IGM) selecionado
Global gReqAssocActiva As String

' Ultima situa��o seleccionada
Global gSituacaoActiva As Integer
Global gSituacaoActivaAux As Integer

'Argumentos recebidos do exterior (GH)
Global gArgTEpisodio As String '1
Global gArgNEpisodio As String '2
Global gArgTUtente As String '3
Global gArgNUtente As String '4
Global gArgServReq As String '5
Global gArgEFR As String '6
Global gArgNBenef As String '7
Global gArgCodMed As String '8
Global gArgModoServidor As Integer  '9

' pferreira 2010.09.27
' Indica a an�lise (tipo frase) para cancelamento da amostra (chegada de tubos V2)
Global gAnaliseCancelamentoAmostra As String

' pferreira 2010.09.28
Global gMensagensNovas As Boolean
Public Const cTextoNovasMensagens = "Tem nova(s) mensagem(s)!"

'Vector que vai conter o codigo das an�lises do tipo microrganismo
Public gMicro()

'Vector que vai conter o c�digo das analises a nao facturar na gest�o de Requisicoes
Public gAnaNaoFacturaGESREQ()

' pferreira 2010.07.01
Public Enum TIPOS_CANCELAMENTO
    
    t_REQUISICAO = 0
    t_RECIBO = 1
    t_TUBO = 2
    
End Enum

'---------------VAR. USADAS NA IMPRESSAO DE ETIQUETAS SEM O CRYSTAL--------------
Public Type DOCINFO
    pDocName As String
    pOutputFile As String
    pDatatype As String
End Type

Public Declare Function ClosePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Public Declare Function EndDocPrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Public Declare Function EndPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Public Declare Function OpenPrinter Lib "winspool.drv" Alias "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, ByVal pDefault As Long) As Long
Public Declare Function StartDocPrinter Lib "winspool.drv" Alias "StartDocPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, pDocInfo As DOCINFO) As Long
Public Declare Function StartPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Public Declare Function WritePrinter Lib "winspool.drv" (ByVal hPrinter As Long, pBuf As Any, ByVal cdBuf As Long, pcWritten As Long) As Long

Public EtiqStartJob As String
Public EtiqJob As String
Public EtiqEndJob As String
Public EtiqHPrinter As Long
Public PrinterX As Long

Public Enum TiposAgenda

    e_Criancas = 1
    e_Adultos = 2
    e_Diabeticos = 3
    e_Hipocoagulados = 4

End Enum

Public Enum TiposActividade

    e_Requisicao = 1
    e_Marcacao = 2

End Enum


Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, _
ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Private Const SW_SHOWNORMAL As Long = 1
Private Const SW_SHOWMINIMIZED As Long = 2
Private Const SW_SHOWMAXIMIZED As Long = 3

Private Const ERROR_FILE_NOT_FOUND As Long = 2&
Private Const ERROR_PATH_NOT_FOUND As Long = 3&
Private Const ERROR_BAD_FORMAT As Long = 11&
Private Const SE_ERR_ACCESSDENIED As Long = 5
Private Const SE_ERR_ASSOCINCOMPLETE As Long = 27
Private Const SE_ERR_DDEBUSY As Long = 30
Private Const SE_ERR_DDEFAIL As Long = 29
Private Const SE_ERR_DDETIMEOUT As Long = 28
Private Const SE_ERR_DLLNOTFOUND As Long = 32
Private Const SE_ERR_FNF As Long = 2
Private Const SE_ERR_NOASSOC As Long = 31
Private Const SE_ERR_OOM As Long = 8
Private Const SE_ERR_PNF As Long = 3
Private Const SE_ERR_SHARE As Long = 26

'FMG
Global Const gColTuboSeqTubo = 0
Global Const gColTuboSeqTuboLocal = 1
Global Const gColTuboCodTubo = 2
Global Const gColTuboDescrTubo = 3
Global Const gColTuboGarrafa = 4
Global Const gColTuboDtPrevi = 5
Global Const gColTuboDtChega = 6
Global Const gColTuboHrChega = 7
Global Const gColTuboUtilChega = 8
Global Const gColTuboEstado = 9

'BRUNODSANTOS CHUC-7934 - 27.04.2016
Public Type estruturaTubos
    seq_req_tubo As Long
    Index As Long
    agrupador_estrutura As Long
    requisicao As Long
    codigo_tubo As String
    data_prevista As String
    data_chegada As String
    hora_chegada  As String
    utilizador_chegada  As String
    data_colheita As String
    hora_colheita As String
    utilizador_colheita As String
    data_eliminacao As String
    hora_eliminacao As String
    utilizador_eliminacao As String
    codigo_local As Integer
    codigo_analise As String
    descricao_analise As String
    estado_tubo As Integer
    descricao_tubo As String
    codigo_etiqueta As String
    motivo_eliminacao As String
    numero_garrafa As String
    codigo_produto As String
    descricao_produto As String
    flag_transito As Boolean
    num_obs_ana As Integer
    dt_inicio_transp As String
    hr_inicio_transp As String
    user_inicio_transp As String
    dt_fim_transp As String
    hr_fim_transp As String
    user_fim_transp As String
    cod_local_ana As String
    cod_local_extra As String
    flg_informacao As Integer
    indiceEstrutTubos As Integer
    seq_req_tubo_local As Integer
    flg_nota As Integer
    etiqueta As String
End Type

Public eTubos() As estruturaTubos

Public Type PassaParamImpForm

    PrinterEtiq As String

    NumReq As String

End Type

Public PassaParam As PassaParamImpForm
'Dim arrReq() As Long
'ReDim arrReq(1)
'

'BRUNODSANTOS CHSJ-2670 - 10.05.2016
Public Type InfoHash
   NumRequis As String
   Hash As String
   versao As String
End Type

Public HashReport As InfoHash
'
'NELSONPSILVA Glintt-HS-18011 - 04.04.2017
Public Enum TipoOperacao
   acesso_form = 1
   sa�da_form = 2
   Procura = 3
   Insercao = 4
   Alteracao = 5
   acesso_apl = 6
   sa�da_apl = 7
   Elimina = 8
End Enum
'
'NELSONPSILVA CHVNG-7461 29.10.2018
Public Type trataResultado
    resultado As String
    n_req As String
    n_pres As String
    n_colheita As String
End Type

'edgar.parada BACKLOG-11940 (COLHEITAS) 24.01.2019
Public Type alertas
    existem_alertas As Boolean
    existem_alertas_por_visualizar As Boolean
End Type

Public eAlertas As alertas

Public Enum estado_alertas
    cinzento = 0
    Verde = 1
    Amarelo = 2
    vermelho = 3
End Enum


Public Function BL_Abre_Conexao_HIS(ByRef ConnHIS As ADODB.Connection, SGBD As String)
Attribute BL_Abre_Conexao_HIS.VB_Description = "teste"

    'Abre a conex�o ao HIS
    '------------------------
    'SGBD = gSqlServer ou
    'SGBD = gOracle
    '------------------------

    Dim DSN As String

    'Se j� foi aberta n�o abre de novo
    If Not ConnHIS Is Nothing Then
        If ConnHIS.state = adStateOpen Then
            BL_Abre_Conexao_HIS = 1
            Exit Function
        Else
            Set ConnHIS = Nothing
        End If
    End If
    
    Set ConnHIS = New ADODB.Connection
    
    On Error GoTo TrataErro
    
    DSN = HIS.DataSource & ";UID=" & HIS.uID & ";PWD=" & HIS.PWD
    
    With ConnHIS
        If gProvider <> "" Then
             .Provider = gProvider
            .Open gOracleServerSec, HIS.uID, HIS.PWD
        Else
             .Provider = "MSDASQL"
            .ConnectionString = "DSN=" & DSN
             .CommandTimeout = "180"
            .Open
        End If
    End With
    
    If Trim(gSGBD) = gSqlServer Then
        ConnHIS.Execute "SET DATEFORMAT dmy"
    ElseIf Trim(gSGBD) = gPostGres Then
        ConnHIS.Execute "SET datestyle = ""ISO, DMY"""
    Else
       ConnHIS.Execute "ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY'"
    End If

    BL_Abre_Conexao_HIS = 1
    
    Exit Function
    
TrataErro:
    Beep
    Select Case UCase(HIS.nome)
        Case "SONHO"
            BG_Mensagem mediMsgBox, "N�o � poss�vel efectuar a conex�o ao SONHO.      ", vbCritical, " Conex�o a Base de Dados Externa"
        Case Else
            BG_Mensagem mediMsgBox, HIS.nome & " : Erro ao efectuar a conex�o.", vbCritical, " " & HIS.nome
    End Select
    BG_LogFile_Erros "Abertura da BD (" & SGBD & " - " & DSN & ") : " & Err.Number & " (" & Err.Description & ")"
    BL_Abre_Conexao_HIS = 0
    Exit Function
    Resume Next
End Function

Public Sub BL_Fecha_conexao_HIS()
    On Error Resume Next
    If gConnHIS.state = adStateOpen Then
        gConnHIS.Close
        Set gConnHIS = Nothing
    End If
End Sub
Public Function BL_Abre_Conexao_Secundaria(SGBD As String)
    Dim lPwd As String
    Dim lUser As String
    Dim lServer As String
    Dim tokens() As String
    Dim i As Integer
    Set gConexaoSecundaria = New ADODB.Connection
    BL_Tokenize gConSec, ";", tokens
    If UBound(tokens) = 2 Then
        lUser = Replace((tokens(1)), "UID=", "")
        lPwd = Replace((tokens(2)), "PWD=", "")
        
    End If
    On Error GoTo TrataErro
    With gConexaoSecundaria
        If gProvider <> "" Then
             .Provider = gProvider
            .Open gOracleServerSec, lUser, lPwd
        Else
             .Provider = "MSDASQL"
            .ConnectionString = "DSN=" & gConSec
             .CommandTimeout = "180"
            .Open
        End If
    End With

    If Trim(gSGBD) = gSqlServer Then
        gConexaoSecundaria.Execute "SET DATEFORMAT dmy"
    ElseIf Trim(gSGBD) = gPostGres Then
        gConexaoSecundaria.Execute "SET datestyle = ""ISO, DMY"""
    Else
       gConexaoSecundaria.Execute "ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY'"
    End If

    BL_Abre_Conexao_Secundaria = 1
    
    Exit Function
    
TrataErro:
    Beep
    BG_Mensagem mediMsgBox, "Erro a Abrir a Base de Dados secundaria", vbCritical, App.ProductName
    BG_LogFile_Erros "Abertura da BD (" & SGBD & " - " & gConSec & ") : " & Err.Number & " (" & Err.Description & ")"
    BL_Abre_Conexao_Secundaria = 0
    Exit Function
    Resume Next
End Function

Public Function BL_Abre_Conexao_eResults(SGBD As String)

    Set gConexaoEresults = New ADODB.Connection
    
    On Error GoTo TrataErro
    
    
    gConexaoEresults.Provider = "MSDASQL"
    gConexaoEresults.ConnectionString = "DSN=" & geResultsDSN & ";UID=" & geResultsUTIL & ";PWD=" & geResultsPWD & ";Connect Timeout=90"
    gConexaoEresults.Open

    If SGBD = gSqlServer Then
        gConexaoEresults.Execute "SET DATEFORMAT dmy"
    ElseIf SGBD = gOracle Then
        gConexaoEresults.Execute "ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY'"
    End If

    BL_Abre_Conexao_eResults = 1
    
    Exit Function
    
TrataErro:
    Beep
    BG_Mensagem mediMsgBox, "Erro a Abrir a Base de Dados eresults", vbCritical, App.ProductName
    BG_LogFile_Erros "Abertura da BD (" & SGBD & " - " & gConexaoEresults & ") : " & Err.Number & " (" & Err.Description & ")"
    BL_Abre_Conexao_eResults = 0
    Exit Function
    Resume Next
End Function

Public Function BL_Abre_Conexao_Interfaces(SGBD As String)

    Set gConexaoInterfaces = New ADODB.Connection
    
    On Error GoTo TrataErro
    
    gConexaoInterfaces.Provider = "MSDASQL"
    gConexaoInterfaces.ConnectionString = "DSN=" & gConInterfaces
    gConexaoInterfaces.Open

    If SGBD = gSqlServer Then
        gConexaoInterfaces.Execute "SET DATEFORMAT dmy"
    ElseIf SGBD = gOracle Then
        gConexaoInterfaces.Execute "ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY'"
    End If

    BL_Abre_Conexao_Interfaces = 1
    
    Exit Function
    
TrataErro:
    Beep
    BG_Mensagem mediMsgBox, "Erro a Abrir a Base de Dados Interfaces", vbCritical, App.ProductName
    BG_LogFile_Erros "Abertura da BD (" & SGBD & " - " & gConSec & ") : " & Err.Number & " (" & Err.Description & ")"
    BL_Abre_Conexao_Interfaces = 0
    Exit Function
    Resume Next
End Function
Sub BL_SelGhostMember(complexa As String)

    Dim RsGhost As ADODB.recordset
    Dim Opcao As menu
    Dim i As Integer
    
    Set RsGhost = New ADODB.recordset
    With RsGhost
        If Mid(complexa, 1, 1) = "C" Then
            .Source = "SELECT cod_membro,descr_ana_s FROM sl_membro, sl_ana_s WHERE cod_membro = cod_ana_s AND cod_ana_c = " & BL_TrataStringParaBD(complexa) & " AND t_membro = 'A' order by sl_membro.ordem"
        ElseIf Mid(complexa, 1, 1) = "P" Then
            .Source = "SELECT cod_analise cod_membro,descr_ana_s, sl_ana_perfis.ordem FROM sl_ana_perfis, sl_ana_s WHERE cod_analise = cod_ana_s AND cod_perfis = " & BL_TrataStringParaBD(complexa)
            .Source = .Source & " UNION SELECT cod_analise cod_membro,descr_ana_c descr_ana_s, sl_ana_perfis.ordem FROM sl_ana_perfis, sl_ana_c WHERE cod_analise = cod_ana_c AND cod_perfis = " & BL_TrataStringParaBD(complexa)
            .Source = .Source & " UNION SELECT cod_analise cod_membro, descr_perfis descr_ana_s, sl_ana_perfis.ordem FROM sl_ana_perfis, sl_perfis WHERE cod_analise = sl_perfis.cod_perfis AND sl_ana_perfis.cod_perfis = " & BL_TrataStringParaBD(complexa) & " order by ordem"
        End If
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open , gConexao
    End With
    
    ' Apagar membros anteriores
    MDIFormInicio.MenuList(0).Visible = True
    For Each Opcao In MDIFormInicio.MenuList
        If Opcao.Index <> 0 Then
            Unload Opcao
        End If
    Next Opcao
        
    i = 1
    If RsGhost.RecordCount > 0 Then
        While Not RsGhost.EOF
            Load MDIFormInicio.MenuList(i)
            MDIFormInicio.MenuList(i).caption = BL_HandleNull(RsGhost!cod_membro, " ") & " - " & BL_HandleNull(RsGhost!descr_ana_s, " ")
            MDIFormInicio.MenuList(i).Visible = True
            If MDIFormInicio.MenuList(0).Visible = True Then MDIFormInicio.MenuList(0).Visible = False
            i = i + 1
            RsGhost.MoveNext
        Wend
    Else
        Load MDIFormInicio.MenuList(i)
        MDIFormInicio.MenuList(i).caption = "N�o existem membros para esta complexa!"
        If MDIFormInicio.MenuList(0).Visible = True Then MDIFormInicio.MenuList(0).Visible = False
    End If
    
    RsGhost.Close
    Set RsGhost = Nothing

End Sub

Public Function BL_CaixaImpressora(Optional impressora As Boolean = True, Optional Paginas As Boolean = True, Optional Copias As Boolean = True, Optional NomeImpressora As String, Optional Titulo As String) As Boolean
    
    'MICROSOFT WINDOWS COMMON CONTROLS-2 6.0
    
    Dim i As Integer
    
    'Verifica se existem impressoras definidas no sistema
    If Printers.Count = 0 Then
        MsgBox "N�o existe nenhuma impressora definida no sistema!", vbExclamation + vbOKOnly, App.ProductName
        Exit Function
    End If
        
    'PROVOCA O EVENTO LOAD DA FORMPRINTER=>MODAL=>HIDE PARA CONTINUAR C�DIGO LOCAL
    With FormPrinter
        
        'Carrega os valores da Impressora
        .CmbPrinters.ListIndex = -1
        For i = 0 To .CmbPrinters.ListCount - 1
            If .CmbPrinters.List(i) = NomeImpressora Then
                'Nome
                .CmbPrinters.ListIndex = i
                Exit For
            End If
        Next i
        'Verifica se a impressora associada foi inicializada na Combo
        If .CmbPrinters.ListIndex = -1 Then
            .CmbPrinters = Printer.DeviceName
        End If
        
        'Inicializa estrutura com os dados da impressora
        With gImpressoraActiva
            .nome = FormPrinter.CmbPrinters.text
            .Copias = 1
            .Collate = False
            .PgInicial = 0
            .PgFinal = 0
        End With
        
        'Controlos de cada Frame
        'Impressora
        If impressora = True Then
            .FrameImpressora.Enabled = True
            .lbltitnome.Enabled = True
            .lbltitOnde.Enabled = True
            .lblOnde.Enabled = True
            .CmbPrinters.Enabled = True
        Else
            .FrameImpressora.Enabled = False
            .lbltitnome.Enabled = False
            .lbltitOnde.Enabled = False
            .lblOnde.Enabled = False
            .CmbPrinters.Enabled = False
        End If
            
        'P�ginas
        .FrameIntervalo.Enabled = True
        .optTudo.Enabled = True
        'Os restantes controlos da p�gina s�o actualizados no evento Click de OptTudo
        If Paginas = True Then
            .optPaginas.Enabled = True
        Else
            .optPaginas.Enabled = False
        End If
        
        'C�pias
        If Copias = True Then
            .FrameCopias.Enabled = True
            .lbltitnumcopias.Enabled = True
            .txtCopias.Enabled = True
            .UpDown1.Enabled = True
            .ChkAgrupar.Enabled = True
        Else
            .FrameCopias.Enabled = False
            .lbltitnumcopias.Enabled = False
            .txtCopias.Enabled = False
            .UpDown1.Enabled = False
            .ChkAgrupar.Enabled = False
        End If
        
        'T�tulo da Form
        If Trim(Titulo) <> "" Then
            .caption = Titulo
        Else
            .caption = "Imprimir"
        End If
        
    End With
        
    'Abre a Form como Modal
    
    
    FormPrinter.Show vbModal
    
    'Valor da Fun��o
    Select Case FormPrinter.Tag
        Case "SAI"
            BL_CaixaImpressora = False
        Case "CONTINUA"
            BL_CaixaImpressora = True
    End Select
    
    'Fecha a Form FormPrinter (est� apenas oculta!)
    Unload FormPrinter

End Function

Sub BL_LimpaDadosUtente()
    
    'Limpa a estrutura de dados de Utente
    gDUtente.seq_utente = ""
    gDUtente.Utente = ""
    gDUtente.t_utente = ""
    gDUtente.n_proc_1 = ""
    gDUtente.n_proc_2 = ""
    gDUtente.n_cartao_ute = ""
    gDUtente.dt_inscr = ""
    gDUtente.nome_ute = ""
    gDUtente.dt_nasc_ute = ""
    gDUtente.sexo_ute = ""
    gDUtente.descr_sexo_ute = ""
    gDUtente.est_civil = ""
    gDUtente.descr_est_civil = ""
    gDUtente.cod_efr = ""
    gDUtente.nr_benef = ""
    gDUtente.morada = ""
    gDUtente.cod_postal = ""
    gDUtente.telefone = ""
    gDUtente.cod_profis = ""
    gDUtente.situacao = ""
    gDUtente.episodio = ""
    gDUtente.telemovel = ""
    gDUtente.email = ""
    gDUtente.n_prescricao = ""
    gDUtente.cod_isencao_ute = ""
    gDUtente.email = ""
    gDUtente.cod_genero = ""
    gDUtente.num_contribuinte = ""

End Sub

Sub BL_PreencheDadosUtente(Optional seq_utente As String = "", _
                           Optional Utente As String = "", _
                           Optional t_utente As String = "", _
                           Optional n_proc_1 As String = "", _
                           Optional n_proc_2 As String = "", _
                           Optional n_cartao_ute As String = "", _
                           Optional dt_inscr As String = "", _
                           Optional nome_ute As String = "", _
                           Optional dt_nasc_ute As String = "", _
                           Optional sexo_ute As String = "", _
                           Optional descr_sexo_ute As String = "", _
                           Optional telefone As String = "", _
                           Optional est_civil As String = "", _
                           Optional descr_est_civil = "", _
                           Optional cod_efr As String = "", _
                           Optional nr_benef As String = "", _
                           Optional morada As String = "", _
                           Optional cod_postal As String = "", _
                           Optional cod_profis As String = "", _
                           Optional situacao As Integer, _
                           Optional episodio As String, Optional telemovel As String, Optional n_prescricao As String, _
                           Optional isencao As String, Optional email As String, Optional cod_genero As String, _
                           Optional num_contribuinte As String)
    
    ' Preenche a estrutura de dados de utente
    gDUtente.seq_utente = seq_utente
    gDUtente.Utente = Utente
    gDUtente.t_utente = t_utente
    gDUtente.n_proc_1 = n_proc_1
    gDUtente.n_proc_2 = n_proc_2
    gDUtente.n_cartao_ute = n_cartao_ute
    gDUtente.dt_inscr = dt_inscr
    gDUtente.nome_ute = nome_ute
    gDUtente.dt_nasc_ute = dt_nasc_ute
    gDUtente.sexo_ute = sexo_ute
    gDUtente.descr_sexo_ute = descr_sexo_ute
    gDUtente.est_civil = est_civil
    gDUtente.descr_est_civil = descr_est_civil
    gDUtente.cod_efr = cod_efr
    gDUtente.nr_benef = nr_benef
    gDUtente.morada = morada
    gDUtente.cod_postal = cod_postal
    gDUtente.telefone = telefone
    gDUtente.cod_profis = cod_profis
    gDUtente.situacao = situacao
    gDUtente.episodio = episodio
    gDUtente.telemovel = telemovel
    gDUtente.n_prescricao = n_prescricao
    gDUtente.cod_isencao_ute = isencao
    gDUtente.email = email
    gDUtente.cod_genero = cod_genero
    gDUtente.num_contribuinte = num_contribuinte

End Sub

Public Sub BL_ActualizaProgressBar(ByRef Barra As PictureBox, percentagem As Long)

    ' Actualiza a PictureBox como uma barra de Progressao:
    ' - Criar uma PictureBox num form
    ' - Defenir o ScaleWidth da PictureBox como por exemplo:
    '       PictureBox.ScaleWidth = Rs!RecordCount
    ' - Deve-se chamar a funcao a primeira vez com o Percentagem a 0
    '       para inicializar as variaveis estaticas.
    ' - A partir dai a percentagem representa a quantidade
    '   a evoluir na barra como por exemplo:
    '       BL_ActualizaProgressBar PictureBox, 1
    ' - Para avan�ar 2 elementos :
    '       BL_ActualizaProgressBar PictureBox, 2

    Static progress As Long
    Dim r As Long

    Const SRCCOPY = &HCC0020
    Dim Txt$
    
    If percentagem = 0 Then
        progress = 0
    End If
    
    progress = progress + percentagem
    If progress > Barra.ScaleWidth Then
        progress = Barra.ScaleWidth
    End If
    
    Txt$ = Format$(CLng((progress / Barra.ScaleWidth) * 100)) + "%"
    
    Barra.Cls
    Barra.CurrentX = _
    (Barra.ScaleWidth - Barra.TextWidth(Txt$)) \ 2
    Barra.CurrentY = _
    (Barra.ScaleHeight - Barra.TextHeight(Txt$)) \ 2
    
    Barra.Print Txt$
    Barra.Line (0, 0)-(progress, Barra.ScaleHeight), _
    Barra.ForeColor, BF
    r = BitBlt(Barra.hdc, 0, 0, Barra.ScaleWidth, _
        Barra.ScaleHeight, Barra.hdc, 0, 0, SRCCOPY)
        
    DoEvents
End Sub

Public Sub BL_MudaPosicaoRato(left As Long, top As Long)
    
    SetCursorPos gFormActivo.left + left, gFormActivo.top + top

End Sub

Public Function BL_PreviewAberto(ByVal Titulo As String) As Boolean
        
    Dim ReportHwnd As Long
     
    BL_PreviewAberto = False
     
    'Se verificar que a Form Preview j� estava aberta ent�o apenas visualiza-a novamente!!!
    ReportHwnd = FindWindowEx(gParentChild, ByVal 0&, vbNullString, Trim(Titulo))
    If ReportHwnd <> 0 Then
        'Call DestroyWindow(ReportHwnd)
        Call ShowWindow(ReportHwnd, SW_NORMAL)
        Call Putfocus(ReportHwnd)
        BL_PreviewAberto = True
    End If
    
End Function

Public Sub BL_CriaTabela(ByVal NomeTabela As String, ByRef Estrutura() As DefTable)
    
    'Esta rotina constr�i uma tabela a partir de uma estrutura j� inicializada
    'no m�dulo em que a rotina � invocada!
    
    'Ex�
'    Dim DefRegisto(2) As DefTable
'
'    DefRegisto(1).NomeCampo = "Nome"
'    DefRegisto(1).Tipo = "STRING"
'    DefRegisto(1).Tamanho = 80
'    DefRegisto(2).NomeCampo = "Peso"
'    DefRegisto(2).Tipo = "DECIMAL"
'    DefRegisto(2).PInteira = 5
'    DefRegisto(2).PDecimal = 2
'    e ent�o
'    Call BL_CriaTabela("TESTE", DefRegisto)
    
    'gSGBD e Conexao s�o vari�veis globais
    
    On Error GoTo ErrorHandler
    
    Dim ExpSql As String
    Dim NFields As Byte
    Dim i As Integer
    
    ' -------------------------------------------------------------
    ' Faz um DROP pr�vio.
'    On Error Resume Next
'    gConexao.Execute "DROP TABLE " & NomeTabela
'    On Error GoTo ErrorHandler
    ' -------------------------------------------------------------
    
    'Calcula o n� de campos a definir na Tabela
    NFields = UBound(Estrutura)
    
    If gSGBD = gOracle Then
        ExpSql = "CREATE TABLE " & NomeTabela & " ("
    Else
        ExpSql = "CREATE TABLE  " & NomeTabela & " ("
    End If
    For i = 1 To NFields
        Select Case Trim(UCase(Estrutura(i).tipo))
            Case "STRING", "CURRENCY"
                Select Case Trim(UCase(gSGBD))
                    Case gOracle
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " VARCHAR2(" & Estrutura(i).tamanho & "),"
                    Case gSqlServer
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " VARCHAR(" & Estrutura(i).tamanho & "),"
                        'ExpSql = ExpSql & Estrutura(i).NomeCampo & " TEXT,"
                    Case gInformix
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " CHAR(" & Estrutura(i).tamanho & "),"
                End Select
            Case "DATE"
                Select Case Trim(UCase(gSGBD))
                    Case gOracle, gInformix
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " DATE,"
                    Case gSqlServer
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " SMALLDATETIME,"
                End Select
            Case "BOOLEAN"
                ExpSql = ExpSql & Estrutura(i).NomeCampo & " CHAR(1),"
            Case "DECIMAL", "DOUBLE"
                Select Case Trim(UCase(gSGBD))
                    Case gOracle
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " NUMBER(" & Estrutura(i).PInteira & "," & Estrutura(i).PDecimal & "),"
                    Case gSqlServer
                        ' ExpSql = ExpSql & Estrutura(i).NomeCampo & " DECIMAL(" & Estrutura(i).PInteira & "," & Estrutura(i).PDecimal & "),"
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " DECIMAL,"
                    Case gInformix
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " DECIMAL(" & Estrutura(i).PInteira & "," & Estrutura(i).PDecimal & "),"
                End Select
            Case "INTEGER"
                Select Case Trim(UCase(gSGBD))
                    Case gOracle
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " NUMBER,"
                    Case gSqlServer
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " INTEGER,"
                    Case gInformix
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " INTEGER,"
                End Select
            Case "SINGLE"
                Select Case Trim(UCase(gSGBD))
                    Case gOracle
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " NUMBER,"
                    Case gSqlServer
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " REAL,"
                    Case gInformix
                        ExpSql = ExpSql & Estrutura(i).NomeCampo & " INTEGER,"
                End Select
        End Select
    Next i
    
    'Tira a ',' do fim e acrescenta o ')' para completar a express�o!
    ExpSql = left(ExpSql, Len(ExpSql) - 1) & ")"
    
    gSQLError = 0
    
    Call BG_ExecutaQuery_ADO(ExpSql, -1)
    
    If gSQLError <> 0 Then
        gSQLError = 0
        BG_ExecutaQuery_ADO ("DELETE FROM " & NomeTabela)
    End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : BL_CriaTabela (BibliotecaLocal) -> " & Err.Description)
            Exit Sub
    End Select
End Sub

Function BL_GeraNumero(NomeCampo As String) As Long

    'NomeCampo: Campo a gerar.
    '           N_REQUIS
    '           N_REC
    '           N_SESSAO
    '           N_FOLHA_TRAB
    '           SEQ_REALIZA
    '           SEQ_UTENTE
    '           SEQ_ETIQ
    'Retorna o n�mero gerado
    'Caso haja erro retorna -1 e no gSQLError/gSQLIsam o erro da base de dados

    Dim CmdGera As New ADODB.Command
    Dim PmtGera As ADODB.Parameter
    Dim PmtCampo As ADODB.Parameter
    Dim rsData As ADODB.recordset
        
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdGera
        .ActiveConnection = gConexao
        .CommandText = "gerador"
        .CommandType = adCmdStoredProc
    End With
    If gSGBD = gOracle Or gSGBD = gSqlServer Then
    
        Set PmtGera = CmdGera.CreateParameter("RES", adNumeric, adParamOutput, 0)
        PmtGera.Precision = 12
        Set PmtCampo = CmdGera.CreateParameter("CAMPO", adVarChar, adParamInput, 30)
        CmdGera.Parameters.Append PmtCampo
        CmdGera.Parameters.Append PmtGera
        CmdGera.Parameters("CAMPO") = NomeCampo
        CmdGera.Execute
        BL_GeraNumero = Trim(CmdGera.Parameters.Item("RES").value)
    ElseIf gSGBD = gPostGres Then
        Set PmtCampo = CmdGera.CreateParameter("campo", adVarChar, adParamInput, 20)
        CmdGera.Parameters.Append PmtCampo
        CmdGera.Parameters("campo") = NomeCampo
        Set rsData = CmdGera.Execute
        If rsData.EOF Then BL_GeraNumero = "-1"
        BL_GeraNumero = BL_HandleNull(rsData.Fields(0).value, mediComboValorNull)
        rsData.Close
        Set rsData = Nothing

    End If
    
    
       
    GoTo fim

TrataErro:
    BL_GeraNumero = -1
    BG_TrataErro "BL_Geranumero", Err.Number
        
fim:
    Set rsData = Nothing
    Set CmdGera = Nothing
    Set PmtGera = Nothing
    Set PmtCampo = Nothing

End Function

Function BL_GeraNumeroFact(NomeCampo As String) As Long

    'NomeCampo: Campo a gerar.
    '           SEQ_FACT
    'Retorna o n�mero gerado
    'Caso haja erro retorna -1 e no gSQLError/gSQLIsam o erro da base de dados

    Dim CmdGera As New ADODB.Command
    Dim PmtGera As ADODB.Parameter
    Dim PmtCampo As ADODB.Parameter
        
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdGera
        .ActiveConnection = gConexao
        .CommandText = "GERADOR_FACT"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtGera = CmdGera.CreateParameter("RETORNO", adNumeric, adParamOutput, 0)
    PmtGera.Precision = 12
    Set PmtCampo = CmdGera.CreateParameter("CAMPO", adVarChar, adParamInput, 30)
    CmdGera.Parameters.Append PmtCampo
    CmdGera.Parameters.Append PmtGera
    
    CmdGera.Parameters("CAMPO") = NomeCampo
    CmdGera.Execute
    
    BL_GeraNumeroFact = Trim(CmdGera.Parameters.Item("RETORNO").value)
       
    GoTo fim

TrataErro:
    BL_GeraNumeroFact = -1
    BG_TrataErro "BL_GeraNumeroFact", Err.Number
        
fim:
    Set CmdGera = Nothing
    Set PmtGera = Nothing
    Set PmtCampo = Nothing

End Function
 
Function BL_GeraUtente(TUtente As String) As Long

    'Retorna o n�mero gerado
    'Caso haja erro retorna -1 e no gSQLError/gSQLIsam o erro da base de dados

    Dim CmdGera As New ADODB.Command
    Dim PmtGera As ADODB.Parameter
    Dim PmtTUte As ADODB.Parameter
    Dim rsData As New ADODB.recordset
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdGera
        .ActiveConnection = gConexao
        .CommandText = "gera_n_ute"
        .CommandType = adCmdStoredProc
    End With
    
    If gSGBD = gOracle Or gSGBD = gSqlServer Then
        Set PmtGera = CmdGera.CreateParameter("RETORNO", adNumeric, adParamOutput, 0)
        PmtGera.Precision = 20
        Set PmtTUte = CmdGera.CreateParameter("DESCR_T_UTENTE", adVarChar, adParamInput, 5)
        CmdGera.Parameters.Append PmtTUte
        CmdGera.Parameters.Append PmtGera
        
        CmdGera.Parameters("DESCR_T_UTENTE") = TUtente
        CmdGera.Execute
        
        BL_GeraUtente = Trim(CmdGera.Parameters.Item("RETORNO").value)
    ElseIf gSGBD = gPostGres Then
        Set PmtTUte = CmdGera.CreateParameter("tutente", adVarChar, adParamInput, 20)
        CmdGera.Parameters.Append PmtTUte
        CmdGera.Parameters("tutente") = TUtente
        Set rsData = CmdGera.Execute
        If rsData.EOF Then BL_GeraUtente = "-1"
        BL_GeraUtente = BL_HandleNull(rsData.Fields(0).value, mediComboValorNull)
        rsData.Close
        Set rsData = Nothing
    End If
       
    GoTo fim

TrataErro:
    BL_GeraUtente = -1
    BG_TrataErro "BL_GeraUtente", Err.Number
        
fim:
    Set CmdGera = Nothing
    Set PmtGera = Nothing
    Set PmtTUte = Nothing

End Function

' -----------------------------------------------------------------------

' DEVOLVE SEQUENCIAL PARA CADA POSTO DO NUMERO DE ANULACAO

' -----------------------------------------------------------------------
Function BL_GeraAnulacao(NomeCampo As String) As Long
    Dim CmdGera As New ADODB.Command
    Dim PmtGera As ADODB.Parameter
    Dim PmtCampo As ADODB.Parameter
        
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdGera
        .ActiveConnection = gConexao
        .CommandText = "GERADOR_ANULACAO"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtGera = CmdGera.CreateParameter("RETORNO", adNumeric, adParamOutput, 0)
    PmtGera.Precision = 12
    Set PmtCampo = CmdGera.CreateParameter("CAMPO", adVarChar, adParamInput, 30)
    CmdGera.Parameters.Append PmtCampo
    CmdGera.Parameters.Append PmtGera
    
    CmdGera.Parameters("CAMPO") = NomeCampo
    CmdGera.Execute
    
    BL_GeraAnulacao = Trim(CmdGera.Parameters.Item("RETORNO").value)
       
    GoTo fim

TrataErro:
    BL_GeraAnulacao = -1
    BG_TrataErro "BL_GeraAnulacao", Err.Number
        
fim:
    Set CmdGera = Nothing
    Set PmtGera = Nothing
    Set PmtCampo = Nothing

End Function
 
Sub BL_TrataErroRuntime(CodigoErro, DescErro, FuncaoOrigem)
    
    BG_LogFile_Erros "BL_TrataErroRuntime - Ocorreu o erro : " & CodigoErro & " " & DescErro & " em " & FuncaoOrigem
    If gModoDebug = 1 Then
        BG_Mensagem mediMsgBox, "BL_TrataErroRuntime - Ocorreu o erro : " & CodigoErro & " " & DescErro & " em " & FuncaoOrigem, vbOKOnly + vbError, "..."
    End If

End Sub

Function BL_FormulaResolve(formula As String, Optional teste As Variant, Optional CasasDecimais As Variant) As String

    'Se o Teste estiver a True, apenas verifica se a formula est� correcta
    
    Dim i As Integer
    Dim Sinais As String
    Dim Expressao As String
    Dim s As String
    Dim erro As Boolean
    Dim caracter As String * 1
    Dim codAnalise As Boolean
    Dim sinalTemp As String
    'Inicializa as vari�veis
    Sinais = "+-*/^()<>="
    Expressao = ""
    If Mid(formula, 1, 1) = ">" And gLAB = "HSJ" Then
        sinalTemp = ">"
    End If

'    Comentado em 29/01/2001
'    Dava erro em 'x >= y & x <= z'. N�o percebi o objectivo do c�digo.
'    Marcio
'
'    Formula = Replace(Formula, "AND", "*")
'    Formula = Replace(Formula, "&", "*")
'
'    Formula = Replace(Formula, "OR", "+")
'    Formula = Replace(Formula, "|", "+")
'
'    Formula = Replace(Formula, "NOT", "")
'    Formula = Replace(Formula, "!", "")
'
    
    'Tira os espa�os da string
    formula = Replace(formula, " ", "")
    
    'Testa a sintaxe da F�rmula
    If Not IsMissing(teste) Then
        If teste = True Then
        
            codAnalise = False
            
            For i = 1 To Len(formula)
                caracter = Mid(formula, i, 1)
                
                'Verifica se o caracter � um operador
                If InStr(1, Sinais, caracter) <> 0 Then
                    Expressao = Expressao & caracter
                    codAnalise = False
                Else
                    'Verifica se o caracter tratado n�o faz parte do c�digo de uma an�lise
                    If codAnalise = False Then
                        If InStr(1, "PAIVTX$@", caracter) <> 0 Then
                            Expressao = Expressao & "1"     '???? qu�?
                            codAnalise = True
                        Else
                            Expressao = Expressao & caracter
                        End If
                    Else
                        'Se o c�digo da an�lise est� activo ent�o verifica a validade do c�digo
                        '=>N�o pode conter '$' ou '@' ou 'V'!
                        If InStr(1, "PAIVTX$@", caracter) <> 0 Then
                            'Provoca o erro ou ignora
                            Expressao = Expressao & caracter
                        End If
                    End If
                End If
            Next i
            
            s = BL_AvaliaExp(Expressao, erro, , False)
            
            If erro = True Then
                BL_FormulaResolve = ""
            Else
                If Len(s) <> 0 Then
                    If Mid(s, 1, 1) = "+" Then s = Mid(s, 2)
                End If
                BL_FormulaResolve = s
            End If
            
        End If
    Else
        s = BL_AvaliaExp(formula, erro, CasasDecimais)
        If Len(s) <> 0 Then
            If Mid(s, 1, 1) = "+" Then s = Mid(s, 2)
        End If
        If Len(s) > 1 Then
        If Mid(s, Len(s) - 1) = "/0" Then
            s = "0"
        End If
        End If
        If IsNumeric(s) Then
            BL_FormulaResolve = Round(s, CasasDecimais)
        ElseIf s = "TRUE" Or s = "FALSE" Then
            BL_FormulaResolve = s
        Else
            'soliveira : se n�o for num�rico n�o interessa colocar o resultado
            BL_FormulaResolve = ""
        End If
        
    End If
    If sinalTemp <> "" Then
        BL_FormulaResolve = sinalTemp & BL_FormulaResolve
    End If
End Function

Public Function BL_SelCodigo(ByVal NomeTabela As String, _
                             ByVal NomeCampoChave As String, _
                             ByVal NomeCampoPesq As String, _
                             ByVal NomeValorPesq As String, Optional TipoChave As String) As String
        
    ' Esta fun��o retorna a partir da descri��o de uma tabela o c�digo
    ' associado a esta, caso esta exista.
        
    Dim s As String
    Dim Registo As ADODB.recordset
    
    On Error GoTo TrataErro
    If (Len(Trim(NomeValorPesq)) = 0) Then
        BL_SelCodigo = ""
        Exit Function
    End If
    
    Set Registo = New ADODB.recordset
    Set Registo.ActiveConnection = gConexao
    Registo.CursorLocation = adUseClient
    Registo.CursorType = adOpenForwardOnly
    Registo.LockType = adLockReadOnly
    
    If TipoChave <> "" Then
        Select Case TipoChave
            Case "N"
                s = "select " & _
                            NomeCampoChave & _
                   " from " & _
                            NomeTabela & _
                   " where " & _
                            NomeCampoPesq & "=" & BL_HandleNull(NomeValorPesq, -1)
            Case "V"
                s = "select " & _
                            NomeCampoChave & _
                   " from " & _
                            NomeTabela & _
                   " where " & _
                            NomeCampoPesq & "='" & Trim(NomeValorPesq) & "'"
        End Select
    Else
    
    If IsNumeric(NomeValorPesq) Then
        s = "select " & _
                    NomeCampoChave & _
           " from " & _
                    NomeTabela & _
           " where " & _
                    NomeCampoPesq & "=" & BL_HandleNull(NomeValorPesq, -1)
    Else
        s = "select " & _
                    NomeCampoChave & _
           " from " & _
                    NomeTabela & _
           " where " & _
                    NomeCampoPesq & "='" & Trim(NomeValorPesq) & "'"
    End If
    End If
    
    Registo.Source = s
    If gModoDebug = mediSim Then BG_LogFile_Erros s
    Registo.Open
    
    If Registo.RecordCount <> 0 Then
        BL_SelCodigo = BL_HandleNull(Registo.Fields(NomeCampoChave), "")
    Else
        BL_SelCodigo = ""
    End If
    
    Registo.Close
    Set Registo = Nothing
    Exit Function
TrataErro:

    BG_LogFile_Erros "BL_SelCodigo: ERRO ->Sql = " & s & vbCrLf & Err.Number & "-" & Err.Description
    BL_SelCodigo = ""
    Exit Function
End Function
Public Function BL_SelCodigo_Sec(ByVal NomeTabela As String, _
                             ByVal NomeCampoChave As String, _
                             ByVal NomeCampoPesq As String, _
                             ByVal NomeValorPesq As String, Optional TipoChave As String) As String
        
    ' Esta fun��o retorna a partir da descri��o de uma tabela o c�digo
    ' associado a esta, caso esta exista.
        
    Dim s As String
    Dim Registo As ADODB.recordset
    
    If (Len(Trim(NomeValorPesq)) = 0) Then
        BL_SelCodigo_Sec = ""
        Exit Function
    End If
    
    Set Registo = New ADODB.recordset
    Set Registo.ActiveConnection = gConexaoSecundaria
    Registo.CursorLocation = adUseServer
    Registo.CursorType = adOpenForwardOnly
    Registo.LockType = adLockReadOnly
    
    If TipoChave <> "" Then
        Select Case TipoChave
            Case "N"
                s = "select " & _
                            NomeCampoChave & _
                   " from " & _
                            NomeTabela & _
                   " where " & _
                            NomeCampoPesq & "=" & BL_HandleNull(NomeValorPesq, -1)
            Case "V"
                s = "select " & _
                            NomeCampoChave & _
                   " from " & _
                            NomeTabela & _
                   " where " & _
                            NomeCampoPesq & "='" & Trim(NomeValorPesq) & "'"
        End Select
    Else
    
    If IsNumeric(NomeValorPesq) Then
        s = "select " & _
                    NomeCampoChave & _
           " from " & _
                    NomeTabela & _
           " where " & _
                    NomeCampoPesq & "=" & BL_HandleNull(NomeValorPesq, -1)
    Else
        s = "select " & _
                    NomeCampoChave & _
           " from " & _
                    NomeTabela & _
           " where " & _
                    NomeCampoPesq & "='" & Trim(NomeValorPesq) & "'"
    End If
    End If
    
    Registo.Source = s
    If gModoDebug = mediSim Then BG_LogFile_Erros s
    Registo.Open
    
    If Registo.RecordCount <> 0 Then
        BL_SelCodigo_Sec = BL_HandleNull(Registo.Fields(NomeCampoChave), "")
    Else
        BL_SelCodigo_Sec = ""
    End If
    
    Registo.Close
    Set Registo = Nothing
    
End Function

Function BL_Converte_Para_Dias(ValorConverter As Integer, tipoValorConverter As String) As Single

    'Converte uma data para dias.
    
    Dim Dias As Single
    Dim factor As Double
     
    Select Case tipoValorConverter
        Case gT_Anos
            factor = 365.25
        Case gT_Meses
            factor = 30.438
        Case gT_Dias
            factor = 1
    End Select
    If tipoValorConverter = gT_Dias Then
        Dias = ValorConverter * factor
    Else
        Dias = ValorConverter * factor + 0.5
    End If
        
    BL_Converte_Para_Dias = Dias
    
End Function

Public Function BL_SelNomeUtil(CodUtil As String) As String
    
    Dim rsUtil As ADODB.recordset
    
    If Trim(CodUtil) = "" Then
        BL_SelNomeUtil = ""
        Exit Function
    End If
    
    Set rsUtil = New ADODB.recordset
    With rsUtil
        .Source = "SELECT nome FROM sl_idutilizador WHERE cod_utilizador = " & BG_CvPlica(CodUtil)
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With
    
    If rsUtil.RecordCount <> 0 Then
        BL_SelNomeUtil = Trim(BL_HandleNull(rsUtil!nome, CodUtil))
    Else
        BL_SelNomeUtil = CodUtil
    End If
    
    rsUtil.Close
    Set rsUtil = Nothing

End Function

' ------------------------------------------------------------------------------

' VERIFICA SE UTENTE TEM REQUISICOES CRIADAS

' ------------------------------------------------------------------------------
Function BL_VfUteResultados(SeqUte As String) As Boolean
    
    Dim sSql As String
    Dim RsReqUte As ADODB.recordset
    
    On Error GoTo Trata_Erro
    
    sSql = "SELECT count(*) as conta FROM sl_requis WHERE seq_utente = " & SeqUte & _
        " AND estado_req in ( 'F', 'D', '3', 'B', '2', '1' ) "

    Set RsReqUte = New ADODB.recordset
    RsReqUte.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsReqUte.Open sSql, gConexao, adOpenStatic
    
    If RsReqUte.EOF Then
        BL_VfUteResultados = False
    Else
        If BL_HandleNull(RsReqUte!conta, 0) = 0 Then
            BL_VfUteResultados = False
        Else
            BL_VfUteResultados = True
        End If
    End If
    
    RsReqUte.Close
    Set RsReqUte = Nothing
    
Exit Function
Trata_Erro:
    BL_VfUteResultados = False
    Set RsReqUte = Nothing
End Function

Function BL_Constroi_INSERT_Simples(NomeTabela As String, CamposBD() As String, valores() As Variant) As String

    ' Semelhante ao BG_ConstroiCriterioINSERT_ADO, s� que sem controlos de campos
    ' � utilizado num form que contem uma treeview (FormParametrizacaoAcessos) e
    ' a fun�ao foi apenas criada para facilitar o desenvolvimento.

    Dim Criterio, CriterioAux, ListaCampos As String
    Dim ValorCampo, ValorControl As Variant
    Dim i, nPassou, nTipo As Integer

    i = 0
    nPassou = 0
    Criterio = "INSERT INTO " & NomeTabela & " ("
    CriterioAux = ") VALUES ("
    For i = 1 To UBound(CamposBD)
        ListaCampos = ListaCampos & IIf(ListaCampos <> "", ",", "") & CamposBD(i)
    Next i

    Criterio = Criterio & ListaCampos
    
    For i = 1 To UBound(valores)
            
        If valores(i) <> "NULL" And valores(i) <> "" Then
            CriterioAux = CriterioAux & "'" & valores(i) & "'"
        Else
            CriterioAux = CriterioAux & "Null"
        End If
        
        If i < UBound(valores) Then
            CriterioAux = CriterioAux & " ,"
        End If
        
    Next
    Criterio = Criterio & CriterioAux & ")"

    ' Mensagens
    BG_LogFile_Erros "BL_Constroi_INSERT_Simples:"
    BG_LogFile_Erros "    " & Criterio

    ' Retorno de resultados
    BL_Constroi_INSERT_Simples = Criterio

End Function

Function BL_Constroi_SELECT_Simples(Tabela As String, CamposBD() As String, valores() As Variant, _
            Ignorados() As Integer) As String

    ' Esta fun�ao n�o � semelhante ao BG_ConstroiCriterioSELECT_ADO,
    ' pois para alem de nao ter controlos de campos, tambem tem particularidades
    ' � utilizado num form que contem uma treeview (FormParametrizacaoAcessos) e
    ' a fun�ao foi apenas criada para facilitar o desenvolvimento.
            
    Dim CabSQL As String
    Dim ListaCampos As String
    Dim ListaValores As String
    Dim SQLValores As String
    Dim SQLSelect As String
    Dim Str1 As String
    Dim i As Long
    Dim n As Integer
    Dim nTipo As Integer
    Dim NumeroIgnorados As Integer
    Dim Ignora As Boolean
    
    ' pode interessar que alguns campos sejam ignorados para que o select
    ' d� os resultados esperados
    NumeroIgnorados = UBound(Ignorados)
    
    ' concatenar os campos
    For i = 1 To UBound(CamposBD)
        Ignora = False
        For n = 1 To NumeroIgnorados
            If Ignorados(n) = i Then
                Ignora = True
                Exit For
            End If
        Next n
        If Not Ignora Then
            ListaCampos = ListaCampos & IIf(ListaCampos <> "", ",", "") & CamposBD(i)
        End If
    Next i
    CabSQL = "SELECT " & CabSQL & ListaCampos
    For i = 1 To UBound(valores)
        Ignora = False
        For n = 1 To NumeroIgnorados
            If Ignorados(n) = i Then
                Ignora = True
                Exit For
            End If
        Next n
        If Not Ignora Then
        
            If valores(i) <> "NULL" And valores(i) <> "" Then
                Str1 = Str1 & "'" & valores(i) & "'"
            Else
                Str1 = "Null"
            End If
        
            Str1 = CamposBD(i) & "=" & Str1
            ListaValores = ListaValores & IIf(ListaValores <> "", " AND ", "") & Str1
            Str1 = ""
        End If
    Next i
    SQLValores = " FROM " & Tabela & " WHERE " & ListaValores
    SQLSelect = CabSQL & SQLValores

    ' Mensagens
    BG_LogFile_Erros "BL_Constroi_SELECT_Simples:"
    BG_LogFile_Erros "    " & SQLSelect

    ' Retorno de resultados
    BL_Constroi_SELECT_Simples = SQLSelect
        
End Function

Public Function BL_HandleNull(campo As Variant, Optional ValorRetorno As Variant, Optional Delimitador As Variant, Optional Aviso As Variant) As Variant

    'Retorna se o valor � ou n�o nulo, ou ent�o retorna um valor (opcional)no caso de nulo
    
    If IsNull(campo) Or campo = "" Then
        If IsMissing(ValorRetorno) Then
            BL_HandleNull = "Null"
            If Not IsMissing(Aviso) Then
                MsgBox "Aten��o:O Campo " & Aviso & " � nulo"
            End If
        Else
            If Not IsMissing(Delimitador) Then
                BL_HandleNull = Delimitador & ValorRetorno & Delimitador
            Else
                BL_HandleNull = ValorRetorno
            End If
        End If
    Else
        If Not IsMissing(Delimitador) Then
            BL_HandleNull = Delimitador & campo & Delimitador
        Else
            BL_HandleNull = campo
        End If
    End If
    
End Function

Sub BL_Toolbar_BotaoEstado(sFuncao As String, sEstado As String)

#If Win16 Then

' Imprimir
    If StrComp(sFuncao, "Imprimir", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonImprimir.Enabled = True
            MDIFormInicio.IDM_IMPRIMIR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonImprimir.Enabled = False
            MDIFormInicio.IDM_IMPRIMIR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

' Limpar
    If StrComp(sFuncao, "Limpar", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonLimpar.Enabled = True
            MDIFormInicio.IDM_LIMPAR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonLimpar.Enabled = False
            MDIFormInicio.IDM_LIMPAR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

'Inserir
    If StrComp(sFuncao, "Inserir", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonInserir.Enabled = True
            MDIFormInicio.IDM_INSERIR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonInserir.Enabled = False
            MDIFormInicio.IDM_INSERIR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

'Procurar
    If StrComp(sFuncao, "Procurar", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonProcurar.Enabled = True
            MDIFormInicio.IDM_PROCURAR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonProcurar.Enabled = False
            MDIFormInicio.IDM_PROCURAR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Modificar
    If StrComp(sFuncao, "Modificar", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonModificar.Enabled = True
            MDIFormInicio.IDM_MODIFICAR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonModificar.Enabled = False
            MDIFormInicio.IDM_MODIFICAR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Remover
    If StrComp(sFuncao, "Remover", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonRemover.Enabled = True
            MDIFormInicio.IDM_REMOVER.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonRemover.Enabled = False
            MDIFormInicio.IDM_REMOVER.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Anterior
    If StrComp(sFuncao, "Anterior", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonAnterior.Enabled = True
            MDIFormInicio.IDM_ANTERIOR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonAnterior.Enabled = False
            MDIFormInicio.IDM_ANTERIOR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Seguinte
    If StrComp(sFuncao, "Seguinte", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonSeguinte.Enabled = True
            MDIFormInicio.IDM_SEGUINTE.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonSeguinte.Enabled = False
            MDIFormInicio.IDM_SEGUINTE.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'EstadoAnterior
    If StrComp(sFuncao, "EstadoAnterior", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.ButtonEstadoAnterior.Enabled = True
            MDIFormInicio.IDM_ESTADO_ANTERIOR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.ButtonEstadoAnterior.Enabled = False
            MDIFormInicio.IDM_ESTADO_ANTERIOR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If


' 32 BITS **************************************
#ElseIf Win32 Then


' Imprimir
    If StrComp(sFuncao, "Imprimir", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Imprimir").Enabled = True
            MDIFormInicio.IDM_IMPRIMIR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Imprimir").Enabled = False
            MDIFormInicio.IDM_IMPRIMIR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

' ImprimirVerAntes
    If StrComp(sFuncao, "ImprimirVerAntes", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("ImprimirVerAntes").Enabled = True
            MDIFormInicio.IDM_IMPRIMIR_VER_ANTES.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("ImprimirVerAntes").Enabled = False
            MDIFormInicio.IDM_IMPRIMIR_VER_ANTES.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

' Limpar
    If StrComp(sFuncao, "Limpar", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Limpar").Enabled = True
            MDIFormInicio.IDM_LIMPAR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Limpar").Enabled = False
            MDIFormInicio.IDM_LIMPAR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

'Inserir
    If StrComp(sFuncao, "Inserir", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Inserir").Enabled = True
            MDIFormInicio.IDM_INSERIR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Inserir").Enabled = False
            MDIFormInicio.IDM_INSERIR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

'Procurar
    If StrComp(sFuncao, "Procurar", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Procurar").Enabled = True
            MDIFormInicio.IDM_PROCURAR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Procurar").Enabled = False
            MDIFormInicio.IDM_PROCURAR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Modificar
    If StrComp(sFuncao, "Modificar", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Modificar").Enabled = True
            MDIFormInicio.IDM_MODIFICAR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Modificar").Enabled = False
            MDIFormInicio.IDM_MODIFICAR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Remover
    If StrComp(sFuncao, "Remover", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Remover").Enabled = True
            MDIFormInicio.IDM_REMOVER.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Remover").Enabled = False
            MDIFormInicio.IDM_REMOVER.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Anterior
    If StrComp(sFuncao, "Anterior", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Anterior").Enabled = True
            MDIFormInicio.IDM_ANTERIOR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Anterior").Enabled = False
            MDIFormInicio.IDM_ANTERIOR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'Seguinte
    If StrComp(sFuncao, "Seguinte", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Seguinte").Enabled = True
            MDIFormInicio.IDM_SEGUINTE.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("Seguinte").Enabled = False
            MDIFormInicio.IDM_SEGUINTE.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'EstadoAnterior
    If StrComp(sFuncao, "EstadoAnterior", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("EstadoAnterior").Enabled = True
            MDIFormInicio.IDM_ESTADO_ANTERIOR.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar1.Buttons.Item("EstadoAnterior").Enabled = False
            MDIFormInicio.IDM_ESTADO_ANTERIOR.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If

'DataActual
    If StrComp(sFuncao, "DataActual", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar2.Buttons.Item("DataActual").Enabled = True
            MDIFormInicio.IDM_DATA_ACTUAL.Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar2.Buttons.Item("DataActual").Enabled = False
            MDIFormInicio.IDM_DATA_ACTUAL.Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
            End If
    End If
    
'Copiar Utente
    If StrComp(sFuncao, "COPIANUTE", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar2.Buttons.Item("COPIANUTE").Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar2.Buttons.Item("COPIANUTE").Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

'Copiar Requisicao
    If StrComp(sFuncao, "COPIANREQ", 0) = 0 Then
        If StrComp(sEstado, "Activo", 0) = 0 Then
            MDIFormInicio.Toolbar2.Buttons.Item("COPIANREQ").Enabled = True
        ElseIf StrComp(sEstado, "InActivo", 0) = 0 Then
            MDIFormInicio.Toolbar2.Buttons.Item("COPIANREQ").Enabled = False
        Else
            BG_Mensagem mediMsgBox, "Fun��o '" & sFuncao & "' sem indica��o de Estado Activo ou InActivo", , "BG_BotaoEstado"
        End If
    End If

#End If

End Sub

Sub BL_ToolbarEstadoN(iEstado As Integer)

    If iEstado = 0 Then
        BL_Toolbar_BotaoEstado "Limpar", "InActivo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        BL_Toolbar_BotaoEstado "Procurar", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        BL_Toolbar_BotaoEstado "Anterior", "InActivo"
        BL_Toolbar_BotaoEstado "Seguinte", "InActivo"
        BL_Toolbar_BotaoEstado "EstadoAnterior", "InActivo"
        
        BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
        BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
        
        BL_Toolbar_BotaoEstado "DataActual", "InActivo"
        BL_Toolbar_BotaoEstado "COPIANREQ", "InActivo"
        BL_Toolbar_BotaoEstado "COPIANUTE", "InActivo"
    ElseIf iEstado = 1 Then
        BL_Toolbar_BotaoEstado "Limpar", "Activo"
        BL_Toolbar_BotaoEstado "Inserir", "Activo"
        BL_Toolbar_BotaoEstado "Procurar", "Activo"
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        BL_Toolbar_BotaoEstado "Anterior", "InActivo"
        BL_Toolbar_BotaoEstado "Seguinte", "InActivo"
        BL_Toolbar_BotaoEstado "EstadoAnterior", "Activo"
        
        BL_Toolbar_BotaoEstado "DataActual", "Activo"
        BL_Toolbar_BotaoEstado "COPIANREQ", "Activo"
        BL_Toolbar_BotaoEstado "COPIANUTE", "Activo"
        
        BL_ActualizaBotoesEspecificosDaToolbar iEstado
        
    ElseIf iEstado = 2 Then
        BL_Toolbar_BotaoEstado "Limpar", "Activo"
        BL_Toolbar_BotaoEstado "Inserir", "InActivo"
        BL_Toolbar_BotaoEstado "Procurar", "Activo"
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        BL_Toolbar_BotaoEstado "Anterior", "Activo"
        BL_Toolbar_BotaoEstado "Seguinte", "Activo"
        BL_Toolbar_BotaoEstado "EstadoAnterior", "Activo"
        
        BL_Toolbar_BotaoEstado "DataActual", "Activo"
        BL_Toolbar_BotaoEstado "COPIANREQ", "Activo"
        BL_Toolbar_BotaoEstado "COPIANUTE", "Activo"
        
        BL_ActualizaBotoesEspecificosDaToolbar iEstado
    End If
    
End Sub

Public Sub BL_ActualizaBotoesEspecificosDaToolbar(iValor As Integer)

    Dim BotoesToolbarVisiveis(0 To 80 - 1) As Tipo_BotoesToolbarVisiveis
    Dim i, iIndice As Integer
    
    iIndice = 0
    
'----------
    iIndice = iIndice + 1
    BotoesToolbarVisiveis(iIndice).sForm = "FormIdUtilizadores"
    BotoesToolbarVisiveis(iIndice).sBotao = "DataActual"
    BotoesToolbarVisiveis(iIndice).iEstado = 1
    BotoesToolbarVisiveis(iIndice).sObjectivo = "InActivo"
    
    iIndice = iIndice + 1
    BotoesToolbarVisiveis(iIndice).sForm = "FormIdUtilizadores"
    BotoesToolbarVisiveis(iIndice).sBotao = "DataActual"
    BotoesToolbarVisiveis(iIndice).iEstado = 2
    BotoesToolbarVisiveis(iIndice).sObjectivo = "InActivo"
    
'----------
    iIndice = iIndice + 1
    BotoesToolbarVisiveis(iIndice).sForm = "FormRegistoAcessos"
    BotoesToolbarVisiveis(iIndice).sBotao = "Inserir"
    BotoesToolbarVisiveis(iIndice).iEstado = 1
    BotoesToolbarVisiveis(iIndice).sObjectivo = "InActivo"
    
    iIndice = iIndice + 1
    BotoesToolbarVisiveis(iIndice).sForm = "FormRegistoAcessos"
    BotoesToolbarVisiveis(iIndice).sBotao = "Modificar"
    BotoesToolbarVisiveis(iIndice).iEstado = 2
    BotoesToolbarVisiveis(iIndice).sObjectivo = "InActivo"
    
    iIndice = iIndice + 1
    BotoesToolbarVisiveis(iIndice).sForm = "FormRegistoAcessos"
    BotoesToolbarVisiveis(iIndice).sBotao = "Remover"
    BotoesToolbarVisiveis(iIndice).iEstado = 2
    BotoesToolbarVisiveis(iIndice).sObjectivo = "InActivo"

    For i = 0 To iIndice
        If gFormActivo.Name = BotoesToolbarVisiveis(i).sForm Then
            If iValor = BotoesToolbarVisiveis(i).iEstado Then
                BL_Toolbar_BotaoEstado BotoesToolbarVisiveis(i).sBotao, BotoesToolbarVisiveis(i).sObjectivo
            End If
        End If
    Next i

End Sub

Public Sub BL_PreencheData(cControlData As Control, cControlActual As Control)

    ' preenche um campo com a data do sistema

    If cControlData.Name = cControlActual.Name Then
        cControlData = Bg_DaData_ADO
    Else
        BG_Mensagem mediMsgStatus, "O campo activo n�o � um campo de Data.", mediMsgBeep
    End If
    
End Sub

Function BL_Encripta(password As String) As String

    'Encripta a password do utilizador
    'a chave de encripta��o � o alfabeto

    Dim tamanho As Integer
    Dim i
    Dim somaletra As Integer
    Dim posLetra As Integer
    Dim posEncripta As Integer
    Dim alfabeto As String
    Dim letra As String
    Dim letraEncripta As String
    Dim encriptaP As String
    Dim letraAlfabeto As String
    Dim posLetraAlfabeto As Integer

    alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz_ "

    encriptaP = ""
    tamanho = Len(password)
    For i = 1 To tamanho
        letra = Mid(password, i, 1)
        posLetra = InStr(1, alfabeto, letra)
        letraAlfabeto = Mid(alfabeto, i, 1)
        posLetraAlfabeto = InStr(1, alfabeto, letraAlfabeto)
        somaletra = posLetra + posLetraAlfabeto
        If somaletra > 64 Then
            posEncripta = somaletra - 64
            letraEncripta = Mid(alfabeto, posEncripta, 1)
            encriptaP = encriptaP & letraEncripta
        Else
            letraEncripta = Mid(alfabeto, somaletra, 1)
            encriptaP = encriptaP & letraEncripta
        End If
    Next i

    BL_Encripta = encriptaP
    
End Function

Function BL_Desencripta(cripta As String) As String

    'Desencripta a password do utilizador
    'a chave de encripta��o � o alfabeto

    Dim alfabeto As String
    Dim tamanho As Integer
    Dim i As Integer
    Dim letraC As String
    Dim posletraC As Integer
    Dim posletraP As Integer
    Dim letraP As String
    Dim somaletra As Integer
    Dim posDesencripta As Integer
    Dim letraDesencripta As String
    Dim pDesencripta As String

    alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz_ "


    tamanho = Len(cripta)
    For i = 1 To tamanho
        letraC = Mid(cripta, i, 1)
        posletraC = InStr(1, alfabeto, letraC)
        letraP = Mid(alfabeto, i, 1)
        posletraP = InStr(1, alfabeto, letraP)
        somaletra = posletraC - posletraP
        If somaletra < 0 Then
            posDesencripta = somaletra + 64
            letraDesencripta = Mid(alfabeto, posDesencripta, 1)
            pDesencripta = pDesencripta + letraDesencripta
        Else
            If somaletra = 0 Then somaletra = 64
            letraDesencripta = Mid(alfabeto, somaletra, 1)
            pDesencripta = pDesencripta & letraDesencripta
        End If
    Next i

    BL_Desencripta = pDesencripta

End Function

Function BL_PreencheControloAcessos(EcUtilizadorCriacao As TextBox, EcUtilizadorAlteracao As TextBox, LaLoginCriacao As Label, LaLoginAlteracao As Label)

    ' Preenche os campos de controlo de acessos a um form

    Dim fraseSQL As String
    Dim rs As ADODB.recordset

    Set rs = New ADODB.recordset

    LaLoginCriacao = ""
    LaLoginAlteracao = ""
    If EcUtilizadorCriacao <> "" And IsNumeric(EcUtilizadorCriacao) Then
        fraseSQL = "SELECT utilizador FROM sl_idutilizador WHERE cod_utilizador=" & EcUtilizadorCriacao
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros fraseSQL
        rs.Open fraseSQL, gConexao
        If rs.RecordCount > 0 Then
            LaLoginCriacao = BL_HandleNull(rs!utilizador, EcUtilizadorCriacao)
        Else
            LaLoginCriacao = EcUtilizadorCriacao
        End If
        rs.Close
    End If

    If EcUtilizadorAlteracao <> "" And IsNumeric(EcUtilizadorAlteracao) Then
        fraseSQL = "SELECT utilizador FROM sl_idutilizador WHERE cod_utilizador=" & EcUtilizadorAlteracao
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros fraseSQL
        rs.Open fraseSQL, gConexao
        If rs.RecordCount > 0 Then
            LaLoginAlteracao = BL_HandleNull(rs!utilizador, EcUtilizadorAlteracao)
        Else
            LaLoginAlteracao = EcUtilizadorAlteracao
        End If
        rs.Close
    End If

    If Not IsNumeric(EcUtilizadorCriacao) Then
        LaLoginCriacao = EcUtilizadorCriacao
    End If

    If Not IsNumeric(EcUtilizadorAlteracao) Then
        LaLoginAlteracao = EcUtilizadorAlteracao
    End If

    Set rs = Nothing
    
End Function

Public Sub BL_PoePermissao_ADO(NomeFormStr As String, NomeControlo As String, propriedade As String, valor As String, indice As Integer)

    ' ROTINA GERADA PELO UTILIT�RIO 'CriaInfoForm'
    ' Altera os valores da propriedade 'Propriedade' do controlo 'NomeControlo'
    ' do form 'NomeFormStr' para 'Valor'.
    ' Utilizado pelo BG_ParametrizaPermissoes.

    Dim nomeForm As Form
    On Error GoTo Trata_Erro

    Select Case UCase(NomeFormStr)
        Case UCase("FormAjudaFormulaAna")
            Set nomeForm = FormAjudaFormulaAna
        Case UCase("FormAjudaValAuto")
            Set nomeForm = FormAjudaValAuto
        Case UCase("FormAnaComplexas")
            Set nomeForm = FormAnaComplexas
        Case UCase("FormAnaFrase")
            Set nomeForm = FormAnaFrase
        Case UCase("FormAnaGrTrab")
            Set nomeForm = FormAnaGrTrab
        Case UCase("FormAnaPerfil")
            Set nomeForm = FormAnaPerfil
        Case UCase("FormAntibio")
            Set nomeForm = FormAntibio
        Case UCase("FormApresentacao")
            Set nomeForm = FormApresentacao
        Case UCase("FormAutoRes")
            Set nomeForm = FormAutoRes
        Case UCase("FormCancelarRequisicoes")
            Set nomeForm = FormCancelarRequisicoes
        Case UCase("FormCodPostal")
            Set nomeForm = FormCodPostal
        Case UCase("FormConcAntibio")
            Set nomeForm = FormConcAntibio
        Case UCase("FormConsRequis")
            Set nomeForm = FormConsRequis
        Case UCase("FormContacProv")
            Set nomeForm = FormContacProv
        Case UCase("FormDeclaracaoPres")
            Set nomeForm = FormDeclaracaoPres
        Case UCase("FormDeltaCheck")
            Set nomeForm = FormDeltaCheck
        Case UCase("FormDiag")
            Set nomeForm = FormDiag
        Case UCase("FormEntFinanceiras")
            Set nomeForm = FormEntFinanceiras
        Case UCase("FormEspecProd")
            Set nomeForm = FormEspecProd
        Case UCase("FormFolhasTrab")
            Set nomeForm = FormFolhasTrab
        Case UCase("FormFrase")
            Set nomeForm = FormFrase
        Case UCase("FormGestaoRequisicao")
            Set nomeForm = FormGestaoRequisicao
        Case UCase("FormGrafico")
            Set nomeForm = FormGrafico
        Case UCase("FormGrAnalises")
            Set nomeForm = FormGrAnalises
        Case UCase("FormGruposMicro")
            Set nomeForm = FormGruposMicro
        Case UCase("FormGruposAntibio")
            Set nomeForm = FormGruposAntibio
        Case UCase("FormGruposDiag")
            Set nomeForm = FormGruposDiag
        Case UCase("FormGruposTrab")
            Set nomeForm = FormGruposTrab
        Case UCase("FormGruposUtilizador")
            Set nomeForm = FormGruposUtilizador
        Case UCase("FormHistorico")
            Set nomeForm = FormHistorico
        Case UCase("FormIdentCliente")
            Set nomeForm = FormIdentCliente
        Case UCase("FormIdentClienteAdc")
            Set nomeForm = FormIdentClienteAdc
        Case UCase("FormIdentificaUtente")
            Set nomeForm = FormIdentificaUtente
        Case UCase("FormIdUtilizadores")
            Set nomeForm = FormIdUtilizadores
        Case UCase("FormImprimeCodificacoes")
            Set nomeForm = FormImprimeCodificacoes
        Case UCase("FormImprimeProtocolo")
            Set nomeForm = FormImprimeProtocolo
        Case UCase("FormIncRes")
            Set nomeForm = FormIncRes
        Case UCase("FormInformacaoClinica")
            Set nomeForm = FormInformacaoClinica
        Case UCase("Form_Ini")
            Set nomeForm = Form_Ini
        Case UCase("FormIsencao")
            Set nomeForm = FormIsencao
        Case UCase("FormListarAnalises")
            Set nomeForm = FormListarAnalises
        Case UCase("FormListarResMult")
            Set nomeForm = FormListarResMult
        Case UCase("FormListarResultados")
            Set nomeForm = FormListarResultados
        Case UCase("FormMapaRegisto")
            Set nomeForm = FormMapaRegisto
        Case UCase("FormMedicos")
            Set nomeForm = FormMedicos
        Case UCase("FormMenusPersonalizados")
            Set nomeForm = FormMenusPersonalizados
        Case UCase("FormMetodos")
            Set nomeForm = FormMetodos
        Case UCase("FormMicro")
            Set nomeForm = FormMicro
        Case UCase("FormModelo")
            Set nomeForm = FormModelo
        Case UCase("FormMsgBox")
            Set nomeForm = FormMsgBox
        Case UCase("FormMudarSenha")
            Set nomeForm = FormMudarSenha
        Case UCase("FormNEtiq")
            Set nomeForm = FormNEtiq
        Case UCase("FormOrdenaAna")
            Set nomeForm = FormOrdenaAna
        Case UCase("FormOutrasRazoes")
            Set nomeForm = FormOutrasRazoes
        Case UCase("FormPaises")
            Set nomeForm = FormPaises
        Case UCase("FormParametrizacaoAcessos")
            Set nomeForm = FormParametrizacaoAcessos
        Case UCase("FormParamAmbiente")
            Set nomeForm = FormParamAmbiente
        Case UCase("FormPerfisAna")
            Set nomeForm = FormPerfisAna
        Case UCase("FormPergProd")
            Set nomeForm = FormPergProd
        Case UCase("FormPesqRapidaAvancada")
            Set nomeForm = FormPesqRapidaAvancada
        Case UCase("FormPesquisaRapida")
            Set nomeForm = FormPesquisaRapida
        Case UCase("FormPesquisaUtentes")
            Set nomeForm = FormPesquisaUtentes
        Case UCase("FormPrinter")
            Set nomeForm = FormPrinter
        Case UCase("FormProdutos")
            Set nomeForm = FormProdutos
        Case UCase("FormProfissao")
            Set nomeForm = FormProfissao
        Case UCase("FormProveniencia")
            Set nomeForm = FormProveniencia
        Case UCase("FormReagentes")
            Set nomeForm = FormReagentes
        Case UCase("FormRegistoAcessos")
            Set nomeForm = FormRegistoAcessos
        Case UCase("FormRelAutRes")
            Set nomeForm = FormRelAutRes
        Case UCase("FormRelGrAntibio")
            Set nomeForm = FormRelGrAntibio
        Case UCase("FormReports")
            Set nomeForm = FormReports
        Case UCase("FormReqPend")
            Set nomeForm = FormReqPend
        Case UCase("FormReqPrev")
            Set nomeForm = FormReqPrev
        Case UCase("FormRequisicoesCanceladas")
            Set nomeForm = FormRequisicoesCanceladas
        Case UCase("FormReqUte")
            Set nomeForm = FormReqUte
        Case UCase("FormResultados")
            Set nomeForm = FormResultados
        Case UCase("FormSalasColh")
            Set nomeForm = FormSalasColh
        Case UCase("FormSelRes")
            Set nomeForm = FormSelRes
        Case UCase("FormSobre")
            Set nomeForm = FormSobre
        Case UCase("FormSubGrAnalises")
            Set nomeForm = FormSubGrAnalises
        Case UCase("FormTeraMed")
            Set nomeForm = FormTeraMed
        Case UCase("FormTipoAnulamentoRecibos")
            Set nomeForm = FormTipoAnulamentoRecibos
        Case UCase("FormTipoCancelamento")
            Set nomeForm = FormTipoCancelamento
        Case UCase("FormTransferencia")
            Set nomeForm = FormTransferencia
        Case UCase("FormTransPermissoes")
            Set nomeForm = FormTransPermissoes
        Case UCase("FormTubos")
            Set nomeForm = FormTubos
        Case UCase("FormUtilProv")
            Set nomeForm = FormUtilProv
        Case UCase("FormValAuto")
            Set nomeForm = FormValAuto
        Case UCase("FormVerificarUtilizador")
            Set nomeForm = FormVerificarUtilizador
        Case UCase("FormZonasRef")
            Set nomeForm = FormZonasRef
        Case UCase("MDIFormInicio")
            Set nomeForm = MDIFormInicio
        Case UCase("FormGestaoRequisicaoPrivado")
            Set nomeForm = FormGestaoRequisicaoPrivado
        Case Else
            Exit Sub
    End Select

    'caso o controlo seja a pr�pria form
    If UCase(Trim(nomeForm.Name)) = UCase(Trim(NomeControlo)) Then
        Select Case UCase(propriedade)
            Case UCase("BackColor")
                nomeForm.BackColor = valor
            Case UCase("Enabled")
                nomeForm.Enabled = CBool(valor)
            Case UCase("FontBold")
                nomeForm.FontBold = CBool(valor)
            Case UCase("FontItalic")
                nomeForm.FontItalic = CBool(valor)
            Case UCase("FontName")
                nomeForm.FontName = valor
            Case UCase("FontSize")
                nomeForm.FontSize = CInt(valor)
            Case UCase("FontStrikethru")
                nomeForm.FontStrikethru = CBool(valor)
            Case UCase("FontUnderline")
                nomeForm.FontUnderline = CBool(valor)
            Case UCase("ForeColor")
                nomeForm.ForeColor = valor
            Case UCase("Height")
                nomeForm.Height = CInt(valor)
            Case UCase("Left")
                nomeForm.left = CInt(valor)
            Case UCase("Top")
                nomeForm.top = CInt(valor)
            Case UCase("Visible")
                nomeForm.Visible = CBool(valor)
            Case UCase("Width")
                nomeForm.Width = CInt(valor)
            Case Else
                Exit Sub
        End Select
    Else
        Select Case UCase(propriedade)
            Case UCase("BackColor")
                    nomeForm.Controls(NomeControlo).BackColor = valor
            Case UCase("Enabled")
                    nomeForm.Controls(NomeControlo).Enabled = CBool(valor)
            Case UCase("FontBold")
                    nomeForm.Controls(NomeControlo).FontBold = CBool(valor)
            Case UCase("FontItalic")
                    nomeForm.Controls(NomeControlo).FontItalic = CBool(valor)
            Case UCase("FontName")
                    nomeForm.Controls(NomeControlo).FontName = valor
            Case UCase("FontSize")
                    nomeForm.Controls(NomeControlo).FontSize = valor
            Case UCase("FontStrikethru")
                    nomeForm.Controls(NomeControlo).FontStrikethru = CBool(valor)
            Case UCase("FontUnderline")
                    nomeForm.Controls(NomeControlo).FontUnderline = CBool(valor)
            Case UCase("FontUnderline")
                    nomeForm.Controls(NomeControlo).FontUnderline = CBool(valor)
            Case UCase("Height")
                nomeForm.Controls(NomeControlo).Height = CInt(valor)
            Case UCase("Left")
                nomeForm.Controls(NomeControlo).left = CInt(valor)
            Case UCase("Top")
                nomeForm.Controls(NomeControlo).top = CInt(valor)
            Case UCase("Visible")
                If indice > -1 Then
                    nomeForm.Controls(NomeControlo)(indice).Visible = CBool(valor)
                Else
                    nomeForm.Controls(NomeControlo).Visible = CBool(valor)
                End If
            Case UCase("Width")
                nomeForm.Controls(NomeControlo).Width = CInt(valor)
            Case Else
                Exit Sub
        End Select
    End If

Exit Sub
Trata_Erro:
    'BG_LogFile_Erros "BL_PoePermissao_ADO (" & UCase(NomeFormStr) & ", " & _
    '                                           UCase(Trim(nomeForm.Name)) & ", " & _
    '                                           UCase(Trim(NomeControlo)) & ", " & _
    '                                           UCase(Propriedade) & "): " & Err.Number & " - " & Err.Description
    Resume Next
End Sub

Sub BL_CarregaImagemNaLista(lista As ListImages, NomeIcon As String)

    ' Esta sub carrega uma imagem/icon numa ImageList...
    
    Dim Directoria As String
    Dim Imagem As ListImage
    Dim DirIcons As String
    Dim IconACarregar As String
    
    On Error GoTo Trata_Erro
    
    Directoria = gDirServidor
    
    IconACarregar = Directoria & "\doc\simbolos\" & NomeIcon
    Set Imagem = lista.Add(, , LoadPicture(IconACarregar, vbLPSmall, vbLPDefault, 16, 16))
    
Exit Sub
Trata_Erro:
    If Directoria = gDirCliente Then
        Directoria = gDirServidor
        Resume Next
    End If
End Sub

Sub BL_AcrescentaNodoFilho(ByRef Tw As TreeView, TextoNodo As String, CodPai, CodFilho, NumeroImagem, Optional SELEC)

    ' Acrescentar sub-nodos a arvores.
    
    On Error GoTo TrataErro

    Dim NodoTmp As Node
    Dim ChildId As String
    Dim ParentId As String
    Dim NumeroImagemSelec As Integer
    
    If IsMissing(SELEC) Then
        NumeroImagemSelec = NumeroImagem
    Else
        NumeroImagemSelec = SELEC
    End If
    ParentId = CodPai
    ChildId = CodFilho
    Set NodoTmp = Tw.Nodes.Add(ParentId, tvwChild, ChildId, TextoNodo, NumeroImagem, NumeroImagemSelec)
    
    Exit Sub
TrataErro:
    ' BG_LogFile_Erros "BL_AcrescentaNodoFilho: " & Err.Description & "(" & ParentId & ")(" & ChildId & ")(" & TextoNodo & ")"
End Sub

Sub BL_AcrescentaNodoPai(ByRef Tw As TreeView, TextoNodo As String, id, NumeroImagem, Optional SELEC)

    ' Acrescentar nodos-topo a arvores.
    
    Dim NodoTmp As Node
    Dim ParentId As String
    Dim NumeroImagemSelec As Integer
    
    On Error GoTo Trata_Erro
    
    If IsMissing(SELEC) Then
        NumeroImagemSelec = NumeroImagem
    Else
        NumeroImagemSelec = SELEC
    End If
    
    ParentId = id
    Set NodoTmp = Tw.Nodes.Add(, , ParentId, TextoNodo, NumeroImagem, NumeroImagemSelec)

    Exit Sub
Trata_Erro:
    ' BG_LogFile_Erros "BL_AcrescentaNodoPai: " & Err.Description & "(" & ParentId & ")(" & TextoNodo & ")"
End Sub

Sub BL_ColapsaTreeview(Tw As TreeView)

    ' Sub para comprimir Treeviews.
    
    Dim Nodo As Node
    Dim Nodos As Nodes
    
    Set Nodos = Tw.Nodes
    If Not (Nodos Is Nothing) Then
        For Each Nodo In Nodos
            Nodo.Expanded = False
        Next
    End If

End Sub

Sub BL_ExpandeTreeview(Tw As TreeView)

    ' Sub para expandir Treeviews.
    
    Dim Nodo As Node
    Dim Nodos As Nodes
    Set Nodos = Tw.Nodes
    If Not (Nodos Is Nothing) Then
        For Each Nodo In Nodos
            Nodo.Expanded = True
        Next
    End If

End Sub

Sub BL_MudaCursorRato(cursor As Variant, f As Form)

    ' Muda o cursor do Rato no Form F.
    
    f.MousePointer = cursor
    MDIFormInicio.MousePointer = cursor
    
End Sub

' pferreira 2010.02.15
Sub BL_PoeMousePointer(cursor As Variant, f As Form)

    ' Muda o cursor do Rato no Form F.
    
    f.MousePointer = cursor
    MDIFormInicio.MousePointer = cursor
    
End Sub

Public Sub BL_InicioProcessamento(f As Form, Optional MsgInicio As String, Optional CursorAnimado As Boolean = False)
    
    If CursorAnimado = False Then
        
        'Muda o cursor do rato
        BL_MudaCursorRato vbArrowHourglass, f
    
    Else

        Dim lResult As Long
        Dim j As Integer
    
        On Error GoTo Trata_Erro
    
        ' Limpa o ponteiro do form
        f.MousePointer = 0
        MDIFormInicio.MousePointer = 0

    
        ' Muda o cursor do Rato no Form F para o cursor animado
        CursorAni = LoadCursorFromFile(gDirServidor & "\doc\simbolos\cursor.ani")
        lResult = SetClassLong(ByVal f.hwnd, GCL_HCURSOR, CursorAni)
        DoEvents
    
        'Form do Fundo do Mdi
        DoEvents
    
        For j = 0 To f.Controls.Count - 1
            ' Verificar se o controlo tem a propriedade Hwnd!
            ' Poderia ir pela negativa explicitando apenas os que tem
            ' a propriedade. Mas assim evito problemas caso surjam
            ' novos controlos que n�o a suportem e n�o estejam
            ' aqui contemplados. � melhor fazer a altera��o de
            ' cursor aos que sei que posso do que ao contr�rio.
            If TypeOf f.Controls(j) Is TextBox Or TypeOf f.Controls(j) Is RichTextBox Or _
               TypeOf f.Controls(j) Is Frame Or TypeOf f.Controls(j) Is SSTab Or _
               TypeOf f.Controls(j) Is CheckBox Or TypeOf f.Controls(j) Is OptionButton Or _
               TypeOf f.Controls(j) Is ComboBox Or TypeOf f.Controls(j) Is ListBox Or _
               TypeOf f.Controls(j) Is CommandButton Or TypeOf f.Controls(j) Is MSFlexGrid Or _
               TypeOf f.Controls(j) Is DataGrid Or TypeOf f.Controls(j) Is HScrollBar Or _
               TypeOf f.Controls(j) Is VScrollBar Or TypeOf f.Controls(j) Is TreeView Or _
               TypeOf f.Controls(j) Is DriveListBox Or TypeOf f.Controls(j) Is DirListBox Or _
               TypeOf f.Controls(j) Is FileListBox Or TypeOf f.Controls(j) Is TabStrip Or _
               TypeOf f.Controls(j) Is ToolBar Or TypeOf f.Controls(j) Is StatusBar Or _
               TypeOf f.Controls(j) Is ProgressBar Or TypeOf f.Controls(j) Is ListView Or _
               TypeOf f.Controls(j) Is TabStrip Or TypeOf f.Controls(j) Is Slider Or _
               TypeOf f.Controls(j) Is ImageCombo Or TypeOf f.Controls(j) Is Animation Or _
               TypeOf f.Controls(j) Is UpDown Or TypeOf f.Controls(j) Is MonthView Or _
               TypeOf f.Controls(j) Is DTPicker Or TypeOf f.Controls(j) Is FlatScrollBar Or _
               TypeOf f.Controls(j) Is CoolBar Or TypeOf f.Controls(j) Is MaskEdBox Or _
               TypeOf f.Controls(j) Is DataList Or TypeOf f.Controls(j) Is DataCombo Or _
               TypeOf f.Controls(j) Is PictureBox Then
                lResult = SetClassLong(ByVal f.Controls(j).hwnd, GCL_HCURSOR, CursorAni)
                DoEvents
            End If
        Next j
    
        Estadocursor = 1
    End If
    
    ' Emite mensagem no StatusBar
    If Not IsMissing(MsgInicio) Then
        BG_Mensagem mediMsgStatus, MsgInicio
    End If
    
    
    'ModuleTransparency.MakeTransparent MDIFormInicio.hWnd, 90
    
Exit Sub
Trata_Erro:
    Call BG_LogFile_Erros(Err.Description & " " & Err.Number)
End Sub

Public Sub BL_FimProcessamento(f As Form, Optional MsgFim As String, Optional CursorAnimado As Boolean = False)
    
    If CursorAnimado = False Then
        
        'Muda o cursor do rato
        BL_MudaCursorRato vbArrow, f
        
    Else
        
        Dim lResult As Long
        Dim j As Integer
    
        On Error GoTo Trata_Erro
    
        ' Muda o cursor do Rato no Form F para o Arrow
        lResult = SetClassLong(ByVal f.hwnd, GCL_HCURSOR, CursorNor)
        DoEvents
    
        'Form do Fundo do Mdi
        DoEvents
    
        For j = 0 To f.Controls.Count - 1
            ' Verificar se o controlo tem a propriedade Hwnd!
            ' Poderia ir pela negativa explicitando apenas os que tem
            ' a propriedade. Mas assim evito problemas caso surjam
            ' novos controlos que n�o a suportem e n�o estejam
            ' aqui contemplados. � melhor fazer a altera��o de
            ' cursor aos que sei que posso do que ao contr�rio.
            If TypeOf f.Controls(j) Is TextBox Or TypeOf f.Controls(j) Is RichTextBox Or _
               TypeOf f.Controls(j) Is Frame Or TypeOf f.Controls(j) Is SSTab Or _
               TypeOf f.Controls(j) Is CheckBox Or TypeOf f.Controls(j) Is OptionButton Or _
               TypeOf f.Controls(j) Is ComboBox Or TypeOf f.Controls(j) Is ListBox Or _
               TypeOf f.Controls(j) Is CommandButton Or TypeOf f.Controls(j) Is MSFlexGrid Or _
               TypeOf f.Controls(j) Is DataGrid Or TypeOf f.Controls(j) Is HScrollBar Or _
               TypeOf f.Controls(j) Is VScrollBar Or TypeOf f.Controls(j) Is TreeView Or _
               TypeOf f.Controls(j) Is DriveListBox Or TypeOf f.Controls(j) Is DirListBox Or _
               TypeOf f.Controls(j) Is FileListBox Or TypeOf f.Controls(j) Is TabStrip Or _
               TypeOf f.Controls(j) Is ToolBar Or TypeOf f.Controls(j) Is StatusBar Or _
               TypeOf f.Controls(j) Is ProgressBar Or TypeOf f.Controls(j) Is ListView Or _
               TypeOf f.Controls(j) Is TabStrip Or TypeOf f.Controls(j) Is Slider Or _
               TypeOf f.Controls(j) Is ImageCombo Or TypeOf f.Controls(j) Is Animation Or _
               TypeOf f.Controls(j) Is UpDown Or TypeOf f.Controls(j) Is MonthView Or _
               TypeOf f.Controls(j) Is DTPicker Or TypeOf f.Controls(j) Is FlatScrollBar Or _
               TypeOf f.Controls(j) Is CoolBar Or TypeOf f.Controls(j) Is MaskEdBox Or _
               TypeOf f.Controls(j) Is DataList Or TypeOf f.Controls(j) Is DataCombo Or _
               TypeOf f.Controls(j) Is PictureBox Then
                lResult = SetClassLong(ByVal f.Controls(j).hwnd, GCL_HCURSOR, CursorNor)
                DoEvents
            End If
        Next j
        DestroyCursor (CursorAni)
        DoEvents
    
        ' Retoma o valor o ponteiro
        f.MousePointer = vbArrow
        MDIFormInicio.MousePointer = vbArrow
    
        Estadocursor = 0
    
    End If
    
    ' Emite mensagem no StatusBar
    If Not IsMissing(MsgFim) Then
        BG_Mensagem mediMsgStatus, MsgFim
    Else
        BG_LimpaMensagem
    End If
    'ModuleTransparency.MakeOpaque MDIFormInicio.hWnd
Exit Sub
Trata_Erro:
    Call BG_LogFile_Erros(Err.Description & " " & Err.Number)
End Sub

Public Sub BL_ColocaTextoCombo(Tabela As String, _
                               sequencia As String, _
                               codigo As String, _
                               Ec As Control, _
                               Cb As Control)

    ' Pega no c�digo existente numa TextBox (Ec), e procura numa tabela (Tabela)
    ' o sequencial inteiro respectivo (no campo da tabela Sequencia).
    ' Apos obter o sequencial pesquisa os ItemData da Combo (Cb) e activa a
    ' descri��o correspondente ao c�digo.

    Dim RS2 As ADODB.recordset
    Dim SQLQuery As String
    Dim VersaoBD As Double
    Dim i As Integer
    
    On Error GoTo Trata_Erro
            
    If Ec <> "" Then
        Set RS2 = New ADODB.recordset
        RS2.CursorLocation = adUseServer
        
        'Verificar se o c�digo � texto ou num�rico
        If Trim(gSGBD) = gInformix Then
            ' Verificar vers�o do INFORMIX
            ' (SELECT FIRST s� � suportado a partir da versao 7.30)
            VersaoBD = BL_String2Double(Trim(BG_CvDecimalParaDisplay_ADO(gVersaoSGBD)))
        
            If VersaoBD >= 7.3 Then
                SQLQuery = "SELECT FIRST 1 " & codigo & " FROM " & Tabela
            Else
                SQLQuery = "SELECT " & codigo & " FROM " & Tabela
            End If
        ElseIf Trim(gSGBD) = gOracle Then
            SQLQuery = "SELECT " & codigo & " FROM " & Tabela & _
                  " WHERE ROWNUM < 2 "
        ElseIf Trim(gSGBD) = gSqlServer Then
            SQLQuery = "SELECT TOP 1 " & codigo & " FROM " & Tabela
        Else
            SQLQuery = "SELECT " & codigo & " FROM " & Tabela
        End If
        If gModoDebug = mediSim Then BG_LogFile_Erros SQLQuery
        RS2.Open SQLQuery, gConexao, adOpenStatic, adLockReadOnly
        Select Case RS2.Fields(codigo).Type
            'Numerico: N�o leva pelicas
            Case adDecimal, adNumeric, adSingle, adVarNumeric, adDouble, adInteger
                SQLQuery = "SELECT " & sequencia & " FROM " & Tabela & " WHERE " & codigo & " = " & Trim(Ec)
            Case Else
                SQLQuery = "SELECT " & sequencia & " FROM " & Tabela & " WHERE " & codigo & " = " & BL_TrataStringParaBD(Ec.text)
        End Select
        
        RS2.Close
        If gModoDebug = mediSim Then BG_LogFile_Erros SQLQuery
        RS2.Open SQLQuery, gConexao, adOpenStatic, adLockReadOnly
        If RS2.RecordCount > 0 Then
            i = 0
            While i <> Cb.ListCount
                If Cb.ItemData(i) = val(RS2(sequencia)) Then
                    Cb.ListIndex = i
                End If
                i = i + 1
            Wend
        Else
            BG_Mensagem mediMsgBox, "O c�digo escolhido n�o existe", vbExclamation
            On Error Resume Next
            Ec.SetFocus
            On Error GoTo Trata_Erro
            Ec.text = ""
        End If
        
        RS2.Close
        Set RS2 = Nothing
    Else
        Cb.ListIndex = -1
    End If
    
Exit Sub
Trata_Erro:
    BG_LogFile_Erros "Erro na BL_ColocaTextoCombo :" & Err.Number & "/" & Err.Description
End Sub

Public Sub BL_ColocaComboTexto(Tabela As String, _
                               sequencia As String, _
                               codigo As String, _
                               Ec As Control, _
                               Cb As Control)

    ' Faz o inverso da Sub anterior.
    ' Pega no sequencial existente no DataItem da ComboBox (Cb) e procura na
    ' tabela Tabela o Codigo respectivo, preenchendo a TextBox (Ec) com o mesmo

    Dim SQLQuery As String
    Dim RS2 As ADODB.recordset

    Set RS2 = New ADODB.recordset
    RS2.CursorLocation = adUseServer

    If BG_DaComboSel(Cb) <> -1 Then
        SQLQuery = "SELECT " & codigo & " FROM " & Tabela & " WHERE " & sequencia & " = " & BG_DaComboSel(Cb)
        If gModoDebug = mediSim Then BG_LogFile_Erros SQLQuery
        RS2.Open SQLQuery, gConexao, adOpenStatic, adLockReadOnly
        If RS2.RecordCount > 0 Then
            Ec.text = RS2(codigo)
        Else
            Ec.text = ""
        End If
        RS2.Close
        Set RS2 = Nothing
    Else
        Ec.text = ""
    End If

End Sub

Public Sub BL_PreencheCombo(sql As String, Objecto As ComboBox, CampoDesc As Variant, Optional campoCodigo As Variant)

    'Preenche uma ComboBox com o resultado dum SQL
            
    Dim i As Long
    Dim rs As ADODB.recordset
    
    Set rs = New ADODB.recordset
    
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rs.Open sql, gConexao
    
    If rs.RecordCount > 0 Then
        i = 0
        Do While Not rs.EOF
            Objecto.AddItem Trim(rs(CampoDesc))
            If Not IsMissing(campoCodigo) Then
                Objecto.ItemData(i) = rs(campoCodigo)
            End If
            rs.MoveNext
            i = i + 1
        Loop
    End If
    rs.Close
    Set rs = Nothing
    
End Sub

Public Sub BL_PreencheListBoxMultipla_ADO(NomeControl As Object, NomeRecordset As ADODB.recordset, CamposRequeridos, NumEspacos, CamposBD, CamposEc, Localizacao As String, Optional ValoresCombo As Variant, Optional seq As String)

    ' Semelhante ao BG_PreencheListBoxMultipla_ADO.
    ' Preenche uma listbox com varios campos da BD.
    
    Dim NomeCampoBD As Variant
    Dim Marca As Variant
    Dim sLinha As String
    Dim bValorNaCombo As Boolean
    Dim sValorParaMostrar As String
    Dim i As Long
    Dim j, k, l, m As Integer
    Dim iAux As Integer
    Dim CampoAux As Variant
    Dim bEncontrou As Boolean
    Dim lPosicao, lNumTotal As Long

    On Error GoTo TrataErro

    lPosicao = NomeControl.ListIndex
    lNumTotal = NomeControl.ListCount
    
    NomeControl.Clear
    Marca = NomeRecordset.Bookmark
    
    i = 0
    Do Until NomeRecordset.EOF
        sLinha = ""
        j = 0
        For Each NomeCampoBD In CamposRequeridos
            If (IsNull(NomeRecordset(NomeCampoBD))) Or (NomeRecordset(NomeCampoBD) = "") Then
                sLinha = sLinha & String(NumEspacos(j), " ")
            Else
                bValorNaCombo = False

                If Not IsMissing(ValoresCombo) Then
                    If ValoresCombo = mediOff Then GoTo IGNORA_COMBO
                End If

                ' In�cio: Trata Campos do tipo ComboBox
                k = 0
                bEncontrou = False
                For Each CampoAux In CamposBD
                    If NomeCampoBD = CampoAux Then
                        bEncontrou = True
                        Exit For
                    End If
                    k = k + 1
                Next
                If bEncontrou = True Then
                    If TypeOf CamposEc(k) Is ComboBox Then
                        iAux = CInt(NomeRecordset(NomeCampoBD))
                        For l = 0 To CamposEc(k).ListCount - 1
                            If iAux = CamposEc(k).ItemData(l) Then
                                bValorNaCombo = True
                                sValorParaMostrar = CamposEc(k).List(l)
                                Exit For
                            End If
                        Next
                    End If
                End If
                
                ' Fim: Trata Campos do tipo ComboBox
IGNORA_COMBO:
                If bValorNaCombo = False Then
                    sValorParaMostrar = NomeRecordset(NomeCampoBD)

                    ' In�cio: Trata convers�es para Decimal, Data e Hora
                    m = 0
                    For Each CampoAux In CamposBD
                        If NomeCampoBD = CampoAux Then

                            If left(CamposEc(m).Tag, 1) = adSingle Or left(CamposEc(m).Tag, 1) = adDouble Or left(CamposEc(m).Tag, 1) = adDecimal Or left(CamposEc(m).Tag, 1) = adNumeric Or left(CamposEc(m).Tag, 1) = adVarNumeric Or left(CamposEc(m).Tag, 1) = adInteger Then
                                sValorParaMostrar = BG_CvDecimalParaDisplay(sValorParaMostrar)
                            ElseIf CamposEc(m).Tag = mediTipoData Then
                                sValorParaMostrar = BG_CvData(sValorParaMostrar)
                            ElseIf CamposEc(m).Tag = mediTipoHora Then
                                sValorParaMostrar = BG_CvHora(sValorParaMostrar)
                            End If
                            
                            Exit For
                        End If
                        m = m + 1
                    Next
                    ' Fim: Trata convers�es para Decimal, Data e Hora
                End If

                sLinha = sLinha & left(sValorParaMostrar, NumEspacos(j))
                If Len(sValorParaMostrar) < NumEspacos(j) Then
                    sLinha = sLinha & String(NumEspacos(j) - Len(sValorParaMostrar), " ")
                End If
            End If
            sLinha = sLinha & " "
            j = j + 1
        Next
        NomeControl.AddItem sLinha
        NomeControl.ItemData(i) = NomeRecordset(seq)
        NomeRecordset.MoveNext
        i = i + 1
        If i > 32767 Then
            BG_Mensagem mediMsgBox, "N�o � poss�vel mostrar nesta lista mais do que " & i - 1 & " registos!", vbExclamation
            Exit Do
        End If
    Loop
    
    If Localizacao = "SELECT" Then
        NomeControl.ListIndex = 0
    ElseIf Localizacao = "UPDATE" Then
        NomeControl.ListIndex = lPosicao
    ElseIf Localizacao = "DELETE" Then
        If lNumTotal = lPosicao + 1 Then
            NomeControl.ListIndex = lPosicao - 1
        Else
            NomeControl.ListIndex = lPosicao
        End If
    End If
    
    If Localizacao <> "SELECT" And Localizacao <> "UPDATE" And Localizacao <> "DELETE" Then
        NomeControl.ListIndex = 0
        BG_Mensagem mediMsgBox, "BG_PreencheListBoxMultipla: Necess�rio indicar SELECT, UPDATE ou DELETE"
    End If
    
    NomeRecordset.Bookmark = Marca
    Exit Sub
    
TrataErro:
    BG_Mensagem mediMsgBox, "Problema na rotina 'BL_PreencheListBoxMultipla'" & vbCrLf & vbCrLf & Err & " - " & Error(Err), vbExclamation, "BG " & bgVersao
    Resume Next
End Sub

Public Function BL_IniciaReport(ByVal nomeReport As String, _
                                ByVal Titulo As String, _
                                ByVal destino As DestinationConstants, _
                                Optional MostraCaixa As Boolean = True, _
                                Optional EscolheImpressora As Boolean = True, _
                                Optional EscolhePaginas As Boolean = True, _
                                Optional EscolheCopias As Boolean = True, _
                                Optional MostraProgress As Boolean = True, _
                                Optional NomeImpressora As String, _
                                Optional TituloCaixa As String, _
                                Optional OrientacaoFolha As Variant, _
                                Optional NomeReport2 As Variant, _
                                Optional gDSN_STRING As String) As Boolean
                                   
    'A impressora e a orienta��o foram activadas pela Common Dialog!!
    
    Dim Report As CrystalReport
    
    Dim s As String
    Dim i As Integer
    
    Dim AchouPrinter As Boolean
    
    Dim GravaPrinter As Boolean
    Dim RsNomeReport As ADODB.recordset
    Dim flg_InSession As Integer
    
    'Para detectar o erro gerado pelo Cancel (.CancelError = True)
    On Local Error GoTo Trata_Erro
    
    'Existe Impressora?
    If Printers.Count = 0 Then
        MsgBox "N�o existe nenhuma impressora definida no sistema!", vbExclamation + vbOKOnly, App.ProductName
        Exit Function
    End If
        
    Set Report = forms(0).Controls("Report")
          
    'Inicia a Fun��o a False
    BL_IniciaReport = False
    
    'Limpa as inicializa��es anteriores do Report
    Report.Reset
    
    'Verifica se foi indicada uma impressora espec�fica (par�metro)
    GravaPrinter = False
    If Trim(NomeImpressora) = "" Then
        'Verifica se existe alguma impressora associado ao Report para o Utilizador e Computador Activo.
        If Not IsMissing(NomeReport2) Then
            NomeImpressora = BL_SelImpressora(BL_HandleNull(NomeReport2, nomeReport) & ".rpt")
        Else
            NomeImpressora = BL_SelImpressora(nomeReport & ".rpt")
        End If
        'flg_InSession = BL_SelFlgImpressora(NomeReport & ".rpt")
        'Se n�o tem impressora associada ent�o fica a impressora por defeito do sistema
        'e permite escolher outra
        If NomeImpressora = "" Then
            NomeImpressora = Printer.DeviceName
            GravaPrinter = True
            MostraCaixa = True
            EscolheImpressora = True
            EscolheCopias = False
            EscolhePaginas = False
        Else
            MostraCaixa = False
        End If
    End If
    
    'Verifica se existe no computador
    If NomeImpressora <> Printer.DeviceName Then
        AchouPrinter = False
        If flg_InSession = 0 Then
            For i = 0 To Printers.Count - 1
                If Printers(i).DeviceName = NomeImpressora Then
                    AchouPrinter = True
                    Exit For
                End If
            Next i
        Else
            For i = 0 To Printers.Count - 1
                If InStr(1, Printers(i).DeviceName, NomeImpressora) > 0 Then
                    AchouPrinter = True
                    NomeImpressora = Printers(i).DeviceName
                    Exit For
                End If
            Next i
        End If
        If AchouPrinter = False Then
            NomeImpressora = Printer.DeviceName
        End If
    End If
        
        
    If destino = crptToPrinter And MostraCaixa = True Then
    
        'Mostra a caixa de di�logo da impressora=>Carrega Estrutura
        If BL_CaixaImpressora(EscolheImpressora, EscolhePaginas, EscolheCopias, NomeImpressora, TituloCaixa) = False Then
            Exit Function
        End If
        
        NomeImpressora = gImpressoraActiva.nome
        
        'Se o utilizador escolheu uma impressora para
        'o report grava autom�ticamente na tabela de
        'configura��o de impressoras
        If GravaPrinter = True Then
            Set RsNomeReport = New ADODB.recordset
            With RsNomeReport
                .Source = "SELECT descr_relatorio FROM sl_relatorios WHERE fich_relatorio = " & BL_TrataStringParaBD(nomeReport & ".rpt")
                .CursorLocation = adUseServer
                .CursorType = adOpenStatic
                .ActiveConnection = gConexao
                If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                .Open
            End With
        
            If Not RsNomeReport.EOF Then
'                BG_ExecutaQuery_ADO "INSERT INTO sl_conf_prt " & _
'                                    "(  SEQ_CONF_PRT, COD_UTILIZADOR, COD_COMPUTADOR, DESCR_PRT, DESCR_RPT, USER_CRI, DT_CRI ) VALUES " & _
'                                    "( " & BG_DaMAX("sl_conf_prt", "SEQ_CONF_PRT") + 1 & ", " & gCodUtilizador & ", " & BL_GetComputerCodigo & ", " & BL_TrataStringParaBD(NomeImpressora) & "," & BL_TrataStringParaBD(BL_HandleNull(RsNomeReport!descr_relatorio, "Sem descri��o ???")) & ", '" & gCodUtilizador & "', '" & Bg_DaData_ADO & "')", -1
            End If
            RsNomeReport.Close
            Set RsNomeReport = Nothing
        End If
        
        If Not forms(0).ActiveForm Is Nothing Then
            Call BL_InicioProcessamento(forms(0).ActiveForm, "A imprimir...")
        End If
    End If
    
    If destino = crptToWindow Then
        If Not forms(0).ActiveForm Is Nothing Then
            Call BL_InicioProcessamento(forms(0).ActiveForm, "A visualizar impress�o...")
        End If
    End If
    
    
    '********** O Utilizador fez OK na Common Dialog!!!! **********
    
    'S� estabelece a liga��o e inicializa o Report quando se faz o Ok na Common Dialog!!
    s = Trim(gDirCliente & "\bin\" & nomeReport & ".rpt")
    If Dir(s) = "" Then
        'Se n�o encontrou no cliente, tenta aceder ao servidor
        s = Trim(gDirServidor & "\bin\" & nomeReport & ".rpt")
        If Dir(s) = "" Then
            MsgBox "Report '" & s & "' n�o encontrado!", vbOKOnly + vbCritical, App.ProductName
            If Not forms(0).ActiveForm Is Nothing Then
                Call BL_FimProcessamento(forms(0).ActiveForm, "")
            End If
            Exit Function
        End If
    End If
    
    With Report
        
        '_______________________________________________________________________________________________
    
        'Ficheiro que cont�m o Report
        .ReportFileName = s
        
        'Ignora os dados gravados com o report
        .DiscardSavedData = True
        
        'Destino do Report
        .Destination = destino
        
        'Conex�es � Base de Dados do Main Report e SubReports
'        For i = 0 To Report.GetNSubreports - 1
'            .SubreportToChange = .GetNthSubreportName(i)
'            .Connect = "DSN=" & gDSN
'        Next i
        'Main Report
        .SubreportToChange = ""
        .Connect = "DSN=" & BL_HandleNull(gDSN_STRING, gDSN)
        
        'Barra de evolu��o da Impress�o do PrintPreview
        .ProgressDialog = MostraProgress
                
                
        'Associa as propriedades da impressora
        For i = 0 To Printers.Count - 1
            If Printers(i).DeviceName = NomeImpressora Then
                .PrinterName = Printers(i).DeviceName
                .PrinterDriver = Printers(i).DriverName
                .PrinterPort = Printers(i).Port
                Exit For
            End If
        Next i
                
        If Not IsMissing(OrientacaoFolha) Then
            If UCase(OrientacaoFolha) = "HORIZONTAL" Then
                Printer.Orientation = cdlLandscape
            End If
        End If
        
        If destino = crptToWindow Then
            'Layout da Form Preview (Pode ser definido em modo de Desenho)
        
            'Tipo de Form
            .WindowBorderStyle = crptSizable
            'Modo de abertura da Form
            .WindowState = crptMaximized
          
            'Bot�es Minimizar,Maximizar e Close
            .WindowControlBox = True
            .WindowMinButton = True
            .WindowMaxButton = True
            .WindowShowCancelBtn = True
            'Bot�es de controlo do Report
            .WindowControls = True
            'Bot�o para exportar ficheiros
            .WindowShowExportBtn = True
            'Find do Report
            .WindowShowSearchBtn = True
            'Bot�es de navega��o de registos
            .WindowShowNavigationCtls = True
            'Indica��o da quantidade e percentagem de registos lidos no Preview
            .WindowShowProgressCtls = True
            'Zoom do Report
            .WindowShowZoomCtl = True
            
            'Bot�o Close do Report (diferente do ControlBox)
            .WindowShowCloseBtn = False
            '�rvore de navega��o dos registos
            .WindowShowGroupTree = False
            'Bot�o Refresh
            .WindowShowRefreshBtn = False
            
            'Common Dialog para a impressora
            'Para o Print feito a partir do Preview
            .WindowShowPrintBtn = True
            .WindowShowPrintSetupBtn = True
            
            'T�tulo do Report=>Para Controlar se o Report j� estava aberto!
            .WindowTitle = Titulo
        Else
            If MostraCaixa = True Then
                '_______________________________________________________________________________________________
                
                'Imprime o Report com base nos valores da estrutura atribuidos pela CommonDialog da Impressora:
                
                'C�pias
                .CopiesToPrinter = gImpressoraActiva.Copias
                
                'Collate
                If gImpressoraActiva.Collate = True Then
                    .PrinterCollation = crptCollated
                Else
                    .PrinterCollation = crptUncollated
                End If
                
                'P�ginas
                If gImpressoraActiva.PgInicial <> 0 Then
                    .PrinterStartPage = gImpressoraActiva.PgInicial
                    .PrinterStopPage = gImpressoraActiva.PgFinal
                End If
            End If
        End If
        
    End With
    
    If Not forms(0).ActiveForm Is Nothing Then
        Call BL_FimProcessamento(forms(0).ActiveForm, "")
    End If
        
    'Se chegou ao fim ent�o a fun��o � bem sucedida!
    BL_IniciaReport = True
    Exit Function
    
Trata_Erro:
    'Muda o ponteiro do mouse no fim da Common Dialog
    If Not forms(0).ActiveForm Is Nothing Then
        Call BL_FimProcessamento(forms(0).ActiveForm, "")
    End If
    
    MsgBox Err.Description & Chr(13) & Err.Number, vbCritical, App.ProductName
    Call BG_LogFile_Erros(Err.Description & " " & Err.Number)
    
    'Limpa as inicializa��es do Report
    Report.Reset
    Exit Function
    Resume Next
End Function

Sub BL_RefinaPesquisa(EcPesquisa As Control, NomeControl As Control, TNomeTabela As String, TCampoChave As String, TCampoPesquisa As String)
    
    ' Refina o conteudo duma lista com base no texto duma text box.
    
    Dim i As Integer
    Dim TSQLQueryAux As String
    Dim TClausulaWhereAuxiliar As String
    Dim TTabelaAux As ADODB.recordset
    
    Set TTabelaAux = New ADODB.recordset
    
    TSQLQueryAux = "SELECT " & TCampoChave & " ," & (TCampoPesquisa) & " FROM " & TNomeTabela
    TSQLQueryAux = TSQLQueryAux & " WHERE UPPER(" & TCampoPesquisa & ") LIKE '" & Trim(UCase(EcPesquisa)) & "%'"
    If TClausulaWhereAuxiliar <> "" Then
        TSQLQueryAux = TSQLQueryAux & " AND " & TClausulaWhereAuxiliar
    End If
    TSQLQueryAux = TSQLQueryAux & " ORDER BY " & TCampoPesquisa
    
    TTabelaAux.CursorLocation = adUseServer
    TTabelaAux.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros TSQLQueryAux
    TTabelaAux.Open TSQLQueryAux, gConexao
    NomeControl.ListIndex = mediComboValorNull
    NomeControl.Clear
    i = 0
    Do Until TTabelaAux.EOF
        NomeControl.AddItem TTabelaAux(TCampoPesquisa)
        NomeControl.ItemData(i) = CInt(TTabelaAux(TCampoChave))
        TTabelaAux.MoveNext
        i = i + 1
    Loop

End Sub

Public Function BL_ValidaData(ByVal data As String) As Boolean
    
    ' Controla o formato duma data com mascara (utilizado na EditGrid).
    
    Dim Diasmes As Variant
    Dim ano, mes, dia As String
    Dim Sep1, Sep2 As Integer
    
    Dim MascaraVazia As String
    Dim MascaraFormato As String
    Dim i As Integer
    
    BL_ValidaData = True
    
    
    'Pode ter gerado o LostFocus sem ter passado pelo GotFocus
    If data = "" Then Exit Function
    
    'Pode n�o ter introduzido nenhum dado
    'GOTFOCUS sem LOSTFOCUS ao desactivar os Controlos na Form
    MascaraFormato = BL_MaskData
    MascaraVazia = ""
    For i = 1 To Len(MascaraFormato)
        If Mid(MascaraFormato, i, 1) = "9" Then
            MascaraVazia = MascaraVazia & "_"
        Else
            MascaraVazia = MascaraVazia & Mid(MascaraFormato, i, 1)
        End If
    Next i
    If Trim(data) = Trim(MascaraVazia) Then Exit Function
    
    Diasmes = Array(0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
    
    Sep1 = InStr(1, data, DefLocal.DatasSeparador)
    Sep2 = InStr(Sep1 + 1, data, DefLocal.DatasSeparador)
        
    'M�scara -Todos os d�gitos
    Select Case DefLocal.DatasOrdem
        'M�s/Dia/Ano
        Case 0
            dia = Mid(data, Sep1 + 1, Sep2 - (Sep1 + 1))
            mes = left(data, Sep1 - 1)
            ano = Mid(data, Sep2 + 1)
        'Dia/M�s/Ano
        Case 1
            mes = Mid(data, Sep1 + 1, Sep2 - (Sep1 + 1))
            dia = left(data, Sep1 - 1)
            ano = Mid(data, Sep2 + 1)
        'Ano/M�s/Dia
        Case 2
            mes = Mid(data, Sep1 + 1, Sep2 - (Sep1 + 1))
            dia = left(data, Sep2 + 1)
            ano = left(data, Sep1 - 1)
        End Select
    
    If InStr(1, data, "_") = 0 Then 'Tudo Preenchido
       'Ano
        'Verifica numero de dias para Fevereiro
        If val(ano) / 4 = Int(val(ano) / 4) Then
            Diasmes(2) = 29
        Else
            Diasmes(2) = 28
        End If
        
       'M�s
       If BL_ValidaData = True Then
         If val(mes) = 0 Or val(mes) > 12 Then BL_ValidaData = False
       End If
        
       'Dia
        If BL_ValidaData = True Then
            If val(dia) = 0 Or val(dia) > Diasmes(mes) Then BL_ValidaData = False
        End If
    Else
       BL_ValidaData = False
    End If
 
End Function

Public Function BL_MaskData() As String
    
    ' A usar no GotFocus da M�scara.
    ' 0=(m/d/a); 1=(d/m/a); 2=(a/m/d)
    
    Dim Sepdata As String
    Dim Possep As Integer
    Dim Numy As Integer
    Dim dt As String
    Dim i As Integer
    
    dt = UCase(DefLocal.DatasFormato)
    Sepdata = DefLocal.DatasSeparador
    
    For i = 1 To Len(dt)
        If InStr("dDmMyY", Mid(dt, i, 1)) = 0 Then
            Possep = i
            Exit For
        End If
    Next i
    
    If DefLocal.DatasOrdem = 2 Then
        'Ano no princ�pio
        Numy = Possep - 1
        BL_MaskData = String(Numy, "9") & Sepdata & "99" & Sepdata & "99"
    Else
        'Ano no fim
        Numy = Len(Mid(dt, InStr(Possep + 1, dt, Sepdata) + 1))
        BL_MaskData = "99" & Sepdata & "99" & Sepdata & String(Numy, "9")
    End If
    
End Function

Function BL_DaDadosUtente(Campos() As String, ByRef retorno() As String, Optional SeqUtente As String = "", Optional TUtente As String = "", Optional Utente As String = "", Optional ExtAccess As Boolean) As Integer

    ' Retorna o conteudo de campos da tabela de utentes no Retorno() em fun��o do Campos()
    ' para um utente identificado por seq_utente ou t_utente+utente
    ' A fun��o retorna 0 se OK, -1 se deu ERRO
    '
    ' Exemplo :
    '   Dim Campos(3) as String
    '   Dim Retorno() as String
    '   Dim iRet as Integer
    '
    '   Campos(0) = "nome_ute"
    '   Campos(1) = "dt_nasc_ute"
    '   Campos(2) = "cod_efr_ute"
    '   iRet = BL_DaDadosUtente(Campos, Retorno, "2")
    '
    '   If iRet = 0 Then
    '       BG_LogFile_Erros "NOME: " & Retorno(0)
    '       BG_LogFile_Erros "DATA NASC.: " & Retorno(1)
    '       BG_LogFile_Erros "EFR: " & Retorno(2)
    '   End If

    Dim sSql As String
    Dim i As Integer
    Dim rsUte As ADODB.recordset
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String

    On Error GoTo Trata_Erro
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    
    If UBound(Campos) = 0 Then
        BG_LogFile_Erros "BL_DaDadosUtente : Ubound(Campos) = 0 ??"
        BL_DaDadosUtente = -1
        Exit Function
    End If
    
    Set rsUte = New ADODB.recordset
    rsUte.CursorType = adOpenStatic
    rsUte.CursorLocation = adUseServer
    
    sSql = "SELECT "
    For i = 0 To UBound(Campos)
        If Trim(Campos(i)) <> "" Then
            sSql = sSql & Campos(i) & ","
        End If
    Next i
    
    If Trim(sSql) = "SELECT" Then
        BG_LogFile_Erros "BL_DaDadosUtente : Campos(...) = "" ??"
        BL_DaDadosUtente = -1
        Exit Function
    End If
    
    sSql = Mid(sSql, 1, Len(sSql) - 1)
    
    sSql = sSql & " FROM " & tabela_aux & " WHERE "
    If Trim(SeqUtente) <> "" Then
        sSql = sSql & " seq_utente = " & SeqUtente
    ElseIf Trim(TUtente) <> "" And Trim(Utente) <> "" Then
        sSql = sSql & " utente = " & BL_TrataStringParaBD(Utente) & " AND t_utente = " & BL_TrataStringParaBD(TUtente)
    Else
        BG_LogFile_Erros "BL_DaDadosUtente : SeqUtente = '' AND TUtente = '' AND Utente = '' ??"
        BL_DaDadosUtente = -1
        Exit Function
    End If
    
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsUte.Open sSql, gConexao
    
    If rsUte.RecordCount > 0 Then
        ReDim retorno(UBound(Campos))
        
        For i = 0 To UBound(Campos)
            If Trim(Campos(i)) <> "" Then
                retorno(i) = BL_HandleNull(rsUte.Fields(i), "")
            End If
        Next i
        
        rsUte.Close
        Set rsUte = Nothing
        BL_DaDadosUtente = 0
    Else
        rsUte.Close
        Set rsUte = Nothing
        
        ReDim retorno(0)
        BL_DaDadosUtente = -1
    End If
       
    Exit Function

Trata_Erro:
    BG_LogFile_Erros "BL_DaDadosUtente : " & Err.Description
    BL_DaDadosUtente = -1
End Function

Sub BL_CriaNumeroSessao()

    'Gera um n�mero �nico por sess�o da aplica��o (semelhante a um PID).
    
    Dim i As Integer

    i = 0
    gNumeroSessao = -1
    While (gNumeroSessao = -1 And i <= 10)
        gNumeroSessao = BL_GeraNumero("N_SESSAO")
        i = i + 1
    Wend
    
    
    If gNumeroSessao = -1 Then
        BG_Mensagem mediMsgBox, "Erro a gerar n�mero de sess�o !    ", vbCritical + vbOKOnly, "ERRO"
        Exit Sub
    End If
    
End Sub

Public Function BL_String2Double(ByVal str As String, Optional CasasDecimais As Variant) As Double
    
    ' Substitui o CDBL.
    
    Dim Conversao As Double
    Dim res As Double
    Dim e As Double
    Dim Sinal As Integer
    Dim iTemp As Double
    Dim jTemp As Double
    Dim kTemp As Double
    Dim rTemp As String
    Dim sTemp As String
    Dim tTemp As String
    Dim aPos As Single
    Dim bPos As Single
    Dim cPos As Single
    Dim dPos As Single
    Dim SDecimal As String
    Dim flgNaoNumerico As Boolean
    Dim casas As Long
        
    On Error Resume Next
    
    If IsMissing(CasasDecimais) Then
        casas = -1
    Else
        If CasasDecimais < 0 Or CasasDecimais = "" Then
            casas = -1
        Else
            casas = CLng(CasasDecimais)
        End If
    End If
    
    Conversao = 0
    str = Trim(str)
    
    ' Tratar n�meros cientificos
    ' 1.234E+12 ou 1.234E-12 (base=1.234 e exponencial=12)
    If InStr(1, str, "E+") <> 0 Or InStr(1, str, "E-") <> 0 Then
        aPos = InStr(1, str, "E+")
        bPos = InStr(1, str, "E-")
        cPos = 0
        
        ' Seleccionar o E- ou o E+
        If aPos <> 0 Then
            cPos = aPos
        ElseIf bPos <> 0 Then
            cPos = bPos
        End If
        
        'Retirar o exponencial
        e = Mid(str, cPos + 1)
        
        'Verificar se a base veio inteira
        dPos = InStr(1, str, ",")
        If dPos = 0 Then dPos = InStr(1, str, ".")
        
        If dPos <> 0 Then
            ' Se n�o veio inteira resume a 3 casas decimais
            res = BL_String2Double(Mid(str, 1, cPos - 1))
        Else
            ' Se veio, inteira � directo
            res = Mid(str, 1, cPos - 1)
        End If
        
        ' Calculo
        str = res * (10 ^ e)
    End If

    
    ' Verificar a passagem de sinal
    Sinal = 1
    If Mid(str, 1, 1) = "+" Then
        str = Mid(str, 2, Len(str))
    ElseIf Mid(str, 1, 1) = "-" Then
        str = Mid(str, 2, Len(str))
        Sinal = -1
    End If
    
    ' Verificar string vazia
    If str <> "" Then
        ' Verificar se tem caracteres nao numericos
        '   Particularidade: ",345" n�o � numerico, ao passo que
        '                    ".345" �.
        '   De seguida verifica-se essa situa�ao para retornar 0.345 mesmo
        '   que a string seja ",345"
        
        flgNaoNumerico = False
        If left(str, 1) = "," And Len(str) > 1 Then
            ' ",345" (n�o considera o ",")
            If Not IsNumeric(Mid(str, 2, (Len(str) - 1))) Then
                flgNaoNumerico = True
            End If
        ElseIf Not IsNumeric(str) Then
            ' Normal
            flgNaoNumerico = True
        End If
        
        ' Fim de verificar se tem caracteres nao numericos
        
        ' Se � numerico
        If flgNaoNumerico = False Then
            
            aPos = InStr(1, str, ",") + 1
            bPos = InStr(1, str, ".") + 1
            SDecimal = ""
            
            ' Verificar se s� tem uma virgula ou ponto decimal
            If Not ((InStr(aPos, str, ",") = 0 And aPos <> 1) And _
               (InStr(bPos, str, ".") = 0 And bPos <> 1) And _
               (InStr(aPos, str, ".") = 0 And aPos <> 1) And _
               (InStr(bPos, str, ",") = 0 And bPos <> 1)) Then

                If left(str, 1) = "," Or left(str, 1) = "." Then
                    'A come�ar pela virgula ou ponto decimal
                    If casas = -1 Then
                        iTemp = Mid(str, 2, Len(str))
                        jTemp = 10 ^ (Len(str) - 1)
                    Else
                        iTemp = Mid(str, 2, casas)
                        If casas < (Len(str) - 1) Then
                            jTemp = 10 ^ casas
                        Else
                            jTemp = 10 ^ (Len(str) - 1)
                        End If
                    End If
                    Conversao = Conversao + (iTemp / jTemp)
                ElseIf Right(str, 1) = "," Or Right(str, 1) = "." Then
                    'A terminar pela virgula ou ponto decimal
                    
                    iTemp = Mid(str, 1, Len(str) - 1)
                    Conversao = Conversao + iTemp
                ElseIf aPos = 1 And bPos = 1 Then
                    ' Sem virgula ou ponto decimal
                    
                    Conversao = str
                Else
                    ' Virgula ou ponto decimal no meio da string
                    
                    If aPos <> 1 Then
                        SDecimal = ","
                    ElseIf bPos <> 1 Then
                        SDecimal = "."
                    End If
                    
                    cPos = InStr(1, str, SDecimal)
                    iTemp = Mid(str, 1, cPos - 1)
                    If casas = -1 Then
                        jTemp = Mid(str, cPos + 1, Len(str))
                        kTemp = 10 ^ (Len(Mid(str, cPos + 1, Len(str))))
                    Else
                        jTemp = Mid(str, cPos + 1, casas)
                        If casas < (Len(str) - cPos) Then
                            kTemp = 10 ^ casas
                        Else
                            kTemp = 10 ^ (Len(str) - cPos)
                        End If
                    End If
                    
                    Conversao = iTemp + (jTemp / kTemp)
                End If
                
            End If ' Fim de verificar se s� tem uma virgula ou ponto decimal
        End If ' Fim de � numerico
        
    End If ' Fim de verificar string vazia
    If Mid(Conversao, Len(Conversao), 1) <> "+" Then
        BL_String2Double = Conversao * Sinal
    End If
    
End Function

Public Function BL_AvaliaExp(ByVal s As String, ByRef erro As Boolean, Optional CasasDecimais As Variant, Optional Ocorrencia As Boolean) As String
            
    ' Fun��o que calcula recursivamente o valor das express�es
    ' alg�bricas e/ou l�gicas atendendo �s prioridades dos operadores respectivos.
        
    ' Sem Operadores Recursivos=> A+B*C+F <=>A+(B*C)+F =>S MODIFICA-SE E CONTINUA A TRATAR S!!
    ' Com Operadores Recursivos=>A+B=C AND F=G*H+J=> � o 'A+B=C' e 'F=G*H+J'
        
    Dim i As Integer
    
    Dim posoperador As Integer
    Dim posinicial As Integer
    Dim posfinal As Integer
    
    Dim Operador As String
    Dim operandoA As String
    Dim operandoB As String
    Dim operadorC As String
    Dim resultado As String
    
    Dim temp As String
    
    '********************************************************************************
    'Verifica se � a primeira vez que corre!
    If Ocorrencia = False Then
        
        'Actualiza a ocorrencia
        Ocorrencia = True
            
        'Inicializa o Erro
        erro = False
        
        'Tira os espa�os existentes na String
        s = Replace(s, " ", "", 1, , vbTextCompare)
        
        'Verifica se a string � vazia
        If s = "" Then
            BL_AvaliaExp = ""
            Exit Function
        End If
        
        'P�e a string a mai�sculas
        s = UCase(s)
        
        'Traduz os operadores usados na aplica��o
        s = Replace(s, "&", "AND")
        s = Replace(s, "|", "OR")
        s = Replace(s, "!", "NOT")
        
        temp = s
        temp = Replace(temp, "AND", " ")
        temp = Replace(temp, "OR", " ")
        temp = Replace(temp, "TRUE", " ")
        temp = Replace(temp, "FALSE", " ")
        temp = Replace(temp, "NOT", " ")
        
'        For i = 1 To Len(temp)
'            If InStr(1, "0123456789.,+*/\^-()<>= ", Mid(temp, i, 1)) = 0 Then
'                Erro = True
'                BL_AvaliaExp = s
'                Exit Function
'            End If
'        Next i
        
        'Ajusta os And e Or com os respectivos espa�os em branco.
        s = Replace(s, "AND", " AND ", 1, , vbTextCompare)
        s = Replace(s, "OR", " OR ", 1, , vbTextCompare)
        
        'Trata os factores na express�o
        s = Replace(s, "+(", "+1*(", 1, , vbTextCompare)
        s = Replace(s, "-(", "-1*(", 1, , vbTextCompare)
                        'ICIL-1179 - 2016.04.21
        s = Replace(s, "--", "+", 1, vbTextCompare)
        s = Replace(s, "++", "+", 1, vbTextCompare)
        '
        'Substitui os operadores consecutivos pela multiplica��o (Ex� --4=-1*-4=+4)
        While InStr(1, s, "++") <> 0
            s = Replace(s, "++", "+1*+1*")
        Wend
        
        While InStr(1, s, "+-") <> 0
            s = Replace(s, "+-", "+1*-1*")
        Wend
        
        While InStr(1, s, "-+") <> 0
            s = Replace(s, "-+", "-1*+1*")
        Wend
        
        While InStr(1, s, "--") <> 0
            s = Replace(s, "--", "-1*-1*")
        Wend
        
        
        'Verifica se existem casos do tipo (-3+2)^2=>Indicar que a Base vai ser positiva
        s = Replace(s, ")^", ") ^", 1, , vbTextCompare)
        
        'Calcula a expressao final recursivamente
        s = BL_AvaliaExp(s, erro, CasasDecimais, True)
        BL_AvaliaExp = s
    End If
    
    '********************************************************************************
    
    
    
    
    '********************************************************************************
    'A. Express�o com parenteses Ex� A+B*(C+D^2)
    '********************************************************************************

    posinicial = 0
    posfinal = 0
    
    'Calcula a 1� ocorr�ncia de ')'
    posfinal = InStr(1, s, ")")
    
    'Calcula o respectivo '(' relativo � posfinal
    For i = posfinal - 1 To 1 Step -1
        If Mid(s, i, 1) = "(" Then
            posinicial = i
            Exit For
        End If
    Next i
    
    'Modifica a String (se ainda tiver '(' ou ')'
    If posinicial <> 0 And posfinal <> 0 Then
        resultado = BL_AvaliaExp(Mid(s, posinicial + 1, posfinal - posinicial - 1), erro, CasasDecimais, True)
        
        s = Mid(s, 1, posinicial - 1) & resultado & Mid(s, posfinal + 1)
        If erro = True Then
            BL_AvaliaExp = s
        Else
            s = BL_AvaliaExp(s, erro, CasasDecimais, True)
            BL_AvaliaExp = s
        End If
        Exit Function
    End If
    
         
         
    '********************************************************************************
    'B. OPERADOR L�GICO (AND) (Sem parenteses) Ex� s="TRUE AND FALSE AND TRUE"
    '********************************************************************************
    
    'D� a 1� ocorr�ncia de 'AND' a contar da esquerda
    posoperador = InStr(1, s, " AND ")
    
    If posoperador <> 0 Then
        
        'Calcula o operador
        Operador = " AND "
    
        'Calcula os operandos A e B respectivamente
        
        'Procura os operandos pelos espa�os em brancos ' AND '
        '3<5 OR 5+3>8 AND 3=3
        
        'operandoA
        For i = posoperador - 1 To 1 Step -1
            If Mid(s, i, 1) = " " Then
                Exit For
            End If
        Next i
        If i = 0 Then
           i = 1
           posinicial = 0
           operandoA = BL_AvaliaExp(Mid(s, 1, posoperador - i), erro, CasasDecimais, True)
        Else
           operandoA = BL_AvaliaExp(Mid(s, i + 1, posoperador - i), erro, CasasDecimais, True)
           'Posi��o do �ltimo caracter antes da express�o
            'N�o inclui o ' ' do ' AND'
    '        posinicial = i - 1
            posinicial = i
        End If
        
        
        'operandoB
        For i = posoperador + Len(Operador) To Len(s)
            If Mid(s, i, 1) = " " Then
                Exit For
            End If
        Next i
        operandoB = BL_AvaliaExp(Mid(s, posoperador + Len(Operador), i - (posoperador + Len(Operador))), erro, CasasDecimais, True)
        
        'Se chegou ao fim do ciclo ent�o i>Len(s)
        posfinal = i
        
        
        resultado = BL_OpString(operandoA, operandoB, Trim(Operador), erro, CasasDecimais)
        '� DIFERENTE POIS A EXPRESS�O PODE ESTAR NO MEIO
        s = Mid(s, 1, posinicial) & resultado & Mid(s, posfinal)
        
        If erro = True Then
            BL_AvaliaExp = s
        Else
            s = BL_AvaliaExp(s, erro, CasasDecimais, True)
            BL_AvaliaExp = s
        End If
        Exit Function
    End If
    
    
    
    '********************************************************************************
    'C. OPERADOR L�GICO (OR) (Sem parenteses) Ex� s="TRUE OR FALSE OR TRUE"
    '********************************************************************************

    'D� a 1� ocorr�ncia de 'OR ' a contar da esquerda
    posoperador = InStr(1, s, " OR ")
    
    If posoperador <> 0 Then
        
        'Calcula o operador
        Operador = " OR "
    
        'Calcula os operandos A e B respectivamente
        
        'Procura os operandos pelos espa�os em brancos
        '3<5 OR 5+3>8 AND 3=3
        
        'operandoA
        For i = posoperador - 1 To 1 Step -1
            If Mid(s, i, 1) = " " Then
                Exit For
            End If
        Next i
        If i = 0 Then
           i = 1
           posinicial = 0
           operandoA = BL_AvaliaExp(Mid(s, 1, posoperador - i), erro, CasasDecimais, True)
        Else
           'Posi��o do �ltimo caracter antes da express�o
            'N�o inclui o ' ' do ' OR'
    '        posinicial = i - 1
            posinicial = i
            operandoA = BL_AvaliaExp(Mid(s, i + 1, posoperador - i), erro, CasasDecimais, True)
        End If
        
        
        'operandoB
        For i = posoperador + Len(Operador) To Len(s)
            If Mid(s, i, 1) = " " Then
                Exit For
            End If
        Next i
        operandoB = BL_AvaliaExp(Mid(s, posoperador + Len(Operador), i - (posoperador + Len(Operador))), erro, CasasDecimais, True)
        
        posfinal = i
        
        resultado = BL_OpString(operandoA, operandoB, Trim(Operador), erro, CasasDecimais)
        '� DIFERENTE POIS A EXPRESS�O PODE ESTAR NO MEIO
        s = Mid(s, 1, posinicial) & resultado & Mid(s, posfinal)
        
        If erro = True Then
            BL_AvaliaExp = s
        Else
            s = BL_AvaliaExp(s, erro, CasasDecimais, True)
            BL_AvaliaExp = s
        End If
        Exit Function
    End If
        
        
        
    '********************************************************************************
    'D. OPERADOR L�GICO (NOT) (Sem parenteses) Ex� s="NOT NOT NOT TRUE"
    '********************************************************************************
    
    'D� a 1� ocorr�ncia a contar da esquerda
    posoperador = InStr(1, s, "NOT")
    
    If posoperador <> 0 Then
        'Calcula o operador
        Operador = "NOT"
        
        'Calcula o operando A
        operandoA = BL_AvaliaExp(Mid(s, posoperador + 3), erro, CasasDecimais, True)
        
        'PODE TER MAIS DO QUE UM OPERADOR Ex� Not Not Not False!
        resultado = BL_OpString(operandoA, "", Operador, erro, CasasDecimais)
        s = left(s, posoperador - 1) & resultado
        BL_AvaliaExp = s
        Exit Function
    End If
        
        
        
    '********************************************************************************
    'E. OPERADORES L�GICOS (<,<=,=,<>,>,>=) (Sem parenteses)  Ex� s="5+2>10"
    '********************************************************************************
    
    
    'D� a 1� ocorr�ncia a contar da esquerda
    posoperador = InStr(1, s, "<")
    If posoperador = 0 Then posoperador = InStr(1, s, ">")
    If posoperador = 0 Then posoperador = InStr(1, s, "=")
    
    
    If posoperador <> 0 Then
        
        'Verificar os casos <0.35<0.35
         operadorC = ""
         If posoperador = 1 Then
             operadorC = Mid(s, 1, 1)
             s = Mid(s, 2)
            
            'D� de novo a 1� ocorr�ncia a contar da esquerda
            posoperador = InStr(1, s, "<")
            If posoperador = 0 Then posoperador = InStr(1, s, ">")
            If posoperador = 0 Then posoperador = InStr(1, s, "=")
         End If
        
        'Calcula o operador (Verifica o tamanho do operador)
        If InStr(1, "< = >", Mid(s, posoperador + 1, 1)) <> 0 Then
            Operador = Mid(s, posoperador, 2)
        Else
            'soliveira teste 20.07.2004
            If posoperador = 0 Then
                BL_AvaliaExp = s
                Exit Function
            Else
                Operador = Mid(s, posoperador, 1)
            End If
        End If
        
        'Calcula os operandos A e B respectivamente
        
        'operandoA
        operandoA = BL_AvaliaExp(Mid(s, 1, posoperador - 1), erro, CasasDecimais, True)
        
        'operandoB
        For i = posoperador + Len(Operador) To Len(s)
            If Mid(s, i, 1) = " " Then
                Exit For
            End If
        Next i
        operandoB = BL_AvaliaExp(Mid(s, posoperador + Len(Operador), i - (posoperador + Len(Operador))), erro, CasasDecimais, True)
        
        resultado = BL_OpString(operandoA, operandoB, Operador, erro, CasasDecimais)
        
        'Verificar os casos <0.35<0.35
        If operadorC = ">" And (Operador = ">" Or Operador = ">=") And resultado = "TRUE" Then
            resultado = "TRUE"
        ElseIf operadorC = ">" And (Operador = ">" Or Operador = ">=") And resultado = "FALSE" Then
            If BL_OpString(operandoA, operandoB, "=", erro, CasasDecimais) = "FALSE" Then
                resultado = "FALSE"
            Else
                resultado = "TRUE"
            End If
        ElseIf operadorC = "<" And (Operador = ">" Or Operador = ">=") And resultado = "TRUE" Then
            resultado = "FALSE"
        ElseIf operadorC = "<" And (Operador = ">" Or Operador = ">=") And resultado = "FALSE" Then
            resultado = "FALSE"
        ElseIf operadorC = ">" And (Operador = "<" Or Operador = "<=") And resultado = "TRUE" Then
            resultado = "FALSE"
        ElseIf operadorC = ">" And (Operador = "<" Or Operador = "<=") And resultado = "FALSE" Then
            resultado = "FALSE"
        ElseIf operadorC = "<" And (Operador = "<" Or Operador = "<=") And resultado = "TRUE" Then
            resultado = "TRUE"
        ElseIf operadorC = "<" And (Operador = "<" Or Operador = "<=") And resultado = "FALSE" Then
            If BL_OpString(operandoA, operandoB, "=", erro, CasasDecimais) = "FALSE" Then
                resultado = "FALSE"
            Else
                resultado = "TRUE"
            End If
        End If
        
        '� DIFERENTE POIS S� TEM UM OPERADOR!
        s = resultado
        BL_AvaliaExp = s
        Exit Function
    End If
    
    
    
    '********************************************************************************
    'F. POT�NCIA Ex� s="2^4^2"
    '********************************************************************************
    
    'Posi��o da pot�ncia a contar da direita na string tratada
    posoperador = InStrRev(s, "^")
    If posoperador <> 0 Then
        
        'Calcula o operador
        Operador = "^"
    
        'Calcula os operandos A e B respectivamente
        
        'operandoA
        For i = posoperador - 1 To 1 Step -1
            'Tem o espa�o se o valor derivou de uma express�o do tipo (A+B)^2=>C ^2
            If InStr(1, "0123456789,. ", Mid(s, i, 1)) = 0 Then
                'Verifica se � o caso dos n�s cientificos (1,777E+10^B)!!
                If i <> 1 Then
                    If UCase(Mid(s, i - 1, 1)) = "E" Then
                        'Salta 2 caracteres
                        i = i - 1
                    Else
                        Exit For
                    End If
                End If
            End If
        Next i
        
        If i = 0 Then
            i = 1
        Else
            'O simbolo n�o � n�mero nem ',' ou '.' ou ' '
            If InStr(1, "+-", Mid(s, i, 1)) = 0 Then
                i = i + 1
            End If
        End If
        operandoA = Mid(s, i, posoperador - i)
        
        
        'Posi��o do �ltimo caracter antes da express�o
        posinicial = i - 1
        
        'operandoB
        For i = posoperador + 2 To Len(s)
            If InStr(1, "0123456789,.", Mid(s, i, 1)) = 0 Then
                'Verifica se � o caso dos n�s cientificos (A^1,777E+10)!!
                If i <> Len(s) Then
                    If UCase(Mid(s, i, 1)) = "E" Then
                        'Salta 2 caracteres (Tem sempre 2 caracteres 'E+')
                        i = i + 1
                    Else
                        Exit For
                    End If
                Else
                    Exit For
                End If
            End If
        Next i
        
        operandoB = Mid(s, posoperador + 1, i - posoperador - 1)
        posfinal = i
        
        
        resultado = BL_OpString(operandoA, operandoB, Operador, erro, CasasDecimais)
        
        If erro <> True Then
            'O OperandoA � negativo e n�o estava dentro de '()' anteriormente e ficou positivo?
            If left(operandoA, 1) = "-" And Right(operandoA, 1) <> " " And left(resultado, 1) <> "-" Then
                resultado = "-" & Mid(resultado, 2)
            End If
        End If
        
        'Verifica se o operandoA vem de um parenteses (Manter a indica��o)
        If Right(operandoA, 1) = " " Then
            resultado = resultado & " "
        End If
        
            
        '� DIFERENTE POIS A EXPRESS�O PODE ESTAR NO MEIO
        s = Mid(s, 1, posinicial) & resultado & Mid(s, posfinal)
        
        If erro = True Then
            BL_AvaliaExp = s
        Else
            s = BL_AvaliaExp(s, erro, CasasDecimais, True)
            BL_AvaliaExp = s
        End If
        Exit Function
    End If
    
    
    '********************************************************************************
    'G. MULTIPLICA��O E DIVIS�O
    '********************************************************************************
    
    'Calcula o operador
    'Posi��o do operador =>1� a contar da esquerda na string tratada
    
    posoperador = 0
    For i = 1 To Len(s)
        If InStr(1, "*/\", Mid(s, i, 1)) <> 0 Then
            posoperador = i
            Exit For
        End If
    Next i
    
    If posoperador <> 0 Then
        
        'Calcula o operador
        Operador = Mid(s, posoperador, 1)
    
        'Calcula os operandos A e B respectivamente
        
        'operandoA
        For i = posoperador - 1 To 1 Step -1
            'Tem o espa�o por causa das pot�ncias dentro de parenteses (-1)^2*2=>1 *2
            If InStr(1, "0123456789,. ", Mid(s, i, 1)) = 0 Then
                'Verifica se � o caso dos n�s cientificos (1,777E+10*B)!!
                If i <> 1 Then
                    If UCase(Mid(s, i - 1, 1)) = "E" Then
                        'Salta 2 caracteres
                        i = i - 1
                    Else
                        Exit For
                    End If
                End If
            End If
        Next i
        
        If i = 0 Then i = 1
        
        'N�o � preciso verificar se � para inclu�r o sinal '+' ou '-'
        operandoA = Mid(s, i, posoperador - i)
        
        'Posi��o do �ltimo caracter antes da express�o
        posinicial = i - 1
        
        
        'operandoB
        For i = posoperador + 2 To Len(s)
            If InStr(1, "0123456789,.", Mid(s, i, 1)) = 0 Then
                'Verifica se � o caso dos n�s cientificos (A*1,777E+10)!!
                If i <> Len(s) Then
                    If UCase(Mid(s, i, 1)) = "E" Then
                        'Salta 2 caracteres (Tem sempre 2 caracteres 'E+')
                        i = i + 1
                    Else
                        Exit For
                    End If
                Else
                    Exit For
                End If
            End If
        Next i
        operandoB = Mid(s, posoperador + 1, i - posoperador - 1)
        posfinal = i
        
        
        resultado = BL_OpString(operandoA, operandoB, Operador, erro, CasasDecimais)
        
        '� DIFERENTE POIS A EXPRESS�O PODE ESTAR NO MEIO
        s = Mid(s, 1, posinicial) & resultado & Mid(s, posfinal)
        
        If erro = True Then
            BL_AvaliaExp = s
        Else
            s = BL_AvaliaExp(s, erro, CasasDecimais, True)
            BL_AvaliaExp = s
        End If
        
        Exit Function
    End If
    
    
    
    '********************************************************************************
    'H. SOMA E SUBTRAC��O
    '********************************************************************************
    
    'Calcula o operando e o respectivo sinal
    'Come�a por 2 pois se o operador estiver no inicio n�o conta!
    'Posi��o do sinal => 1� a contar da esquerda na string tratada
    posoperador = 0
    For i = 2 To Len(s)
        If (InStr(1, "+-", Mid(s, i, 1)) <> 0) And UCase(Mid(s, i - 1, 1)) <> "E" Then
            posoperador = i
            Exit For
        End If
    Next i
    
    'Verifica o fim da recursividade
    If posoperador <> 0 Then
        'Calcula o operador
        Operador = Mid(s, posoperador, 1)
        
        'Calcula os operandos A e B respectivamente
        
        'operandoA
        operandoA = left(s, posoperador - 1)
        
        'operandoB
        For i = posoperador + 2 To Len(s)
            If InStr(1, "0123456789,.", Mid(s, i, 1)) = 0 Then
                'Verifica se � o caso dos n�s cientificos (A*1,777E+10)!!
                If i <> Len(s) Then
                    If UCase(Mid(s, i, 1)) = "E" Then
                        'Salta 2 caracteres (Tem sempre 2 caracteres 'E+')
                        i = i + 1
                    Else
                        Exit For
                    End If
                Else
                    Exit For
                End If
            End If
        Next i
        
        operandoB = Mid(s, posoperador + 1, i - posoperador - 1)
        
        resultado = BL_OpString(operandoA, operandoB, Operador, erro, CasasDecimais)
        s = Trim(resultado & Mid(s, i))
        If erro = True Then
            BL_AvaliaExp = s
        Else
            s = BL_AvaliaExp(s, erro, CasasDecimais, True)
            BL_AvaliaExp = s
        End If
        Exit Function
    End If
        
        
        
    '********************************************************************************
    'I. CONSTANTES
    '********************************************************************************
    
    Operador = ""
    operandoA = s
    resultado = BL_OpString(operandoA, "", "", erro, CasasDecimais)
    BL_AvaliaExp = resultado
    Exit Function
End Function

Public Function BL_OpString(ByVal Op1 As String, ByVal Op2 As String, ByVal Operador As String, ByRef erro As Boolean, Optional CasasDecimais As Variant) As String
    
    Dim resLog As Boolean
    Dim resNum As Double
    
    Dim ValLog1 As Boolean
    Dim ValLog2 As Boolean
    Dim ValNum1 As Double
    Dim ValNum2 As Double
    Dim ValStr1 As String
    Dim ValStr2 As String
   
    Dim temp As Double
    
    'Trata o Erro
    On Error GoTo TrataErro
    
    'Inicializa o n� de casa decimais por defeito
    If IsMissing(CasasDecimais) Then
        CasasDecimais = 9
    Else
        If CasasDecimais < 0 Then
            CasasDecimais = 9
        End If
    End If
    
    If (InStr(1, "<>=", Operador) <> 0 Or Operador = "<=") And ((Not IsNumeric(Op1) And InStr(1, "TRUE FALSE", Op1) = 0) Or (Not IsNumeric(Op2) And InStr(1, "TRUE FALSE", Op1) = 0)) Then
        ValStr1 = Op1
        ValStr2 = Op2
    Else
        '>= j� inclu�do na String de Pesquisa
        If (InStr(1, "*/\+-^<>=LOG", Operador) <> 0 Or Operador = "<=") And InStr(1, "TRUE FALSE", Op1) = 0 Then
            If Not IsNumeric(Op1) Then
                'Provoca o erro
                ValNum1 = CDbl(Op1)
            End If
            'soliveira 05-06-2003 ao efectuar a opera��o estava a fazer de acordo
            'com o n�mero de casas decimais que pretendiamos q aparecesse o resultado
            'ValNum1 = BL_String2Double(Op1, CasasDecimais)
            ValNum1 = BL_String2Double(Op1, 9)
            'Para os casos sem operador (Constantes)
            If Operador <> "" Then
                If Not IsNumeric(Op2) Then
                    'Provoca o erro
                    ValNum2 = CDbl(Op2)
                End If
                'soliveira 05-06-2003
                'ValNum2 = BL_String2Double(Op2, CasasDecimais)
                ValNum2 = BL_String2Double(Op2, 9)
            End If
        Else
            ValLog1 = Op1
            'Para o caso do NOT (sem operandoA)
            If Operador <> "" And Operador <> "NOT" Then
                ValLog2 = Op2
            End If
        End If
    End If
    
    Select Case Trim(UCase(Operador))
        Case "<"
            If (ValStr1 <> "" Or ValStr2 <> "") Then
                resLog = ValStr1 < ValStr2
            Else
                resLog = ValNum1 < ValNum2
            End If
            BL_OpString = UCase(CStr(resLog))
        Case "<="
            If (ValStr1 <> "" Or ValStr2 <> "") Then
                resLog = ValStr1 <= ValStr2
            Else
                resLog = ValNum1 <= ValNum2
            End If
            BL_OpString = UCase(CStr(resLog))
        Case "="
            If (ValStr1 <> "" Or ValStr2 <> "") Then
                resLog = Replace(Replace(ValStr1, "�", ""), "�", "") = Replace(Replace(ValStr2, "�", ""), "�", "")
            Else
                If InStr(1, "TRUE FALSE", Op1) = 0 Then
                    resLog = ValNum1 = ValNum2
                Else
                    resLog = ValLog1 = ValLog2
                End If
            End If
            BL_OpString = UCase(CStr(resLog))
        Case ">"
            If (ValStr1 <> "" Or ValStr2 <> "") Then
                resLog = ValStr1 > ValStr2
            Else
                resLog = ValNum1 > ValNum2
            End If
            BL_OpString = UCase(CStr(resLog))
        Case ">="
            If (ValStr1 <> "" Or ValStr2 <> "") Then
                resLog = ValStr1 >= ValStr2
            Else
                resLog = ValNum1 >= ValNum2
            End If
            BL_OpString = UCase(CStr(resLog))
        Case "<>"
            If (ValStr1 <> "" Or ValStr2 <> "") Then
                resLog = ValStr1 <> ValStr2
            Else
                If InStr(1, "TRUE FALSE", Op1) = 0 Then
                    resLog = ValNum1 <> ValNum2
                Else
                    resLog = ValLog1 <> ValLog2
                End If
            End If
            BL_OpString = UCase(CStr(resLog))
        Case "NOT"
            resLog = Not (ValLog1)
            BL_OpString = UCase(CStr(resLog))
        Case "AND"
            resLog = ValLog1 And ValLog2
            BL_OpString = UCase(CStr(resLog))
        Case "OR"
            resLog = ValLog1 Or ValLog2
            BL_OpString = UCase(CStr(resLog))
            
        '***************************************
        
        Case "+"
                                        'ICIL-1179 - 2016.04.21
            resNum = Round(ValNum1 + ValNum2, 9)
                                        '

            BL_OpString = resNum
        Case "-"
            resNum = Round(ValNum1 - ValNum2, 9)
            BL_OpString = resNum
        Case "*"
            'soliveira 05-06-2003
            'ResNum = Round(ValNum1 * ValNum2, CasasDecimais)
            resNum = Round(ValNum1 * ValNum2, 9)
            BL_OpString = resNum
            If Mid(BL_OpString, 1, 1) <> "-" Then BL_OpString = "+" & BL_OpString
        Case "/"
            'soliveira 05-06-2003
            'ResNum = Round(ValNum1 / ValNum2, CasasDecimais)
            resNum = Round(ValNum1 / ValNum2, 9)
            BL_OpString = resNum
            If Mid(BL_OpString, 1, 1) <> "-" Then BL_OpString = "+" & BL_OpString
        Case "\"
            resNum = ValNum1 \ ValNum2
            BL_OpString = resNum
            If Mid(BL_OpString, 1, 1) <> "-" Then BL_OpString = "+" & BL_OpString
        Case "^"
            'Se a Base � negativa o expoente tem que ser um inteiro!!
            'soliveira 05-06-2003
            'ResNum = Round(ValNum1 ^ ValNum2, CasasDecimais)
            resNum = Round(ValNum1 ^ ValNum2, 9)
            BL_OpString = resNum
            If Mid(BL_OpString, 1, 1) <> "-" Then BL_OpString = "+" & BL_OpString

        Case ""
            'Verifica a validade da constante
            If InStr(1, "TRUE FALSE", Trim(UCase(Op1))) = 0 And (InStr(1, "TRUE FALSE", Trim(UCase(Op2))) = 0 Or Trim(Op2) = "") Then
                'Constante num�rica
                'Ajusta se tiver o n� incompleto Ex� '5,' ou ',0'
                'Valida se tiver outro simbolo n�o v�lido!
                If InStr(1, Op1, "�") > 0 And InStr(1, Op1, "�") > 0 Then
                    Op1 = Op1
                ElseIf IsNumeric(Op1) Then
                    Op1 = BL_String2Double(Op1) * 1
                Else
                    Op1 = Op1
                End If
                
            End If
            BL_OpString = Trim(UCase(Op1))
    End Select
    
    'Para n�o executar a Rotina de tratamento de Erros!!!
    erro = False
    Exit Function
 
TrataErro:
        
    erro = True
    Select Case Operador
        Case "NOT"
            BL_OpString = Operador & Op1
        Case ""
            BL_OpString = Op1
        Case "AND", "OR"
            BL_OpString = Op1 & " " & Operador & " " & Op2
        Case Else
            BL_OpString = Op1 & Operador & Op2
    End Select
End Function

Function BL_TrataStringParaBD(NomeStr As String) As String
    
    ' Faz o Rtrim e coloca pelicas.
    If gSGBD = gPostGres Then
        If Trim(NomeStr) = "" Then
            BL_TrataStringParaBD = "NULL"
        Else
            BL_TrataStringParaBD = "'" + RTrim(BG_CvPlica(NomeStr)) + "'"
        End If
    Else
        BL_TrataStringParaBD = "'" + RTrim(BG_CvPlica(NomeStr)) + "'"
    End If

End Function
Function BL_TrataNumberParaBD(NomeStr As String) As String
    If NomeStr = "-1" Then
        BL_TrataNumberParaBD = "Null"
    Else
        BL_TrataNumberParaBD = Replace(NomeStr, ",", ".")
    End If
End Function
Function BL_TrataDoubleParaBD(NomeStr As String) As String
    BL_TrataDoubleParaBD = Replace(NomeStr, ",", ".")
End Function

Function BL_TrataStringParaCrystal(NomeStr As String) As String
    
    ' Faz o Rtrim e coloca pelicas.
        BL_TrataStringParaCrystal = "'" + RTrim(BG_CvPlica(NomeStr)) + "'"

End Function

Function BL_TrataDataParaBD(data As String, Optional mascara As String) As String
    
    ' Converte a data e coloca pelicas.
    
    Dim aux As String
    
    aux = BG_CvDataParaWhere_ADO(data)
    If Trim(aux) <> "" Then
        If gSGBD = gOracle Then
            If mascara = "" Then
                BL_TrataDataParaBD = "'" & BG_CvDataParaWhere_ADO(data) & "'"
            Else
                BL_TrataDataParaBD = "TO_DATE('" & (data) & "','" & mascara & "')"
            End If
        Else
            BL_TrataDataParaBD = "'" & BG_CvDataParaWhere_ADO(data) & "'"
        End If
    Else
        BL_TrataDataParaBD = "null"
    End If
    
End Function

Function BL_ExecutaMudancaLinha(TabIndexNovo, TabIndexControlo, TabIndexPrimeiroControlo) As Boolean
    
    ' Utilizada na gest�o de classes do tipo DataRepeater
    ' deve utilizar-se no evento Lostfocus do ultimo campo do bloco, a fun��o devolve um valor (true ou false)
    ' se for true � porque deve ser executado o m�todo MudaLinha do MultiRecord, caso contr�rio(false) n�o deve
    ' ser executado
    ' O valor de retorno � calculado a partir do tabindex do controlo actual, do tabindex do controlo em que est�
    ' a ocorrer o evento LostFocus, e do Tabindex do primeiro controlo do bloco MultiRecord, se o TabIndex do controlo
    ' actual se enquadrar entre os valores do dois outros, � poque o focus est� num campo do bloco, sen�o � porque o
    ' focus est� noutro campo do form, e deve ser executada o m�todo MudaLinha do MultiRecord para repor o focus num
    ' campo do Bloco MultiRecord
    
    BL_ExecutaMudancaLinha = True
    If TabIndexNovo.TabIndex < TabIndexControlo.TabIndex And TabIndexNovo.TabIndex >= TabIndexPrimeiroControlo.TabIndex Then
        BL_ExecutaMudancaLinha = False
    End If

End Function

Public Function BL_FormataCampo(texto, Formato) As Variant
    
    ' Utilizada na gest�o de classes do tipo DataRepeater.
    
    Select Case Formato
        Case cF_INTEGER, cF_DOUBLE, cF_DOUBLE_SIMPLES, cF_INTEGER_FORMATED, cF_MONEY_ESC_DBL, cF_MONEY_ESC_LNG, cF_MONEY_EUR
            BL_FormataCampo = Format(texto, Formato)
        Case Else
            BL_FormataCampo = BL_HandleNull(texto, "")
            
    End Select
    
End Function

Public Function BL_RetiraFormato(texto, Formato) As Variant
    
    ' Utilizada na gest�o de classes do tipo DataRepeater.
    
    If Trim(texto) = "" Then
        BL_RetiraFormato = texto
        Exit Function
    End If
    Select Case Formato
        Case cF_INTEGER, cF_INTEGER_FORMATED
            BL_RetiraFormato = Format(texto, cF_INTEGER)
        Case cF_DOUBLE
            BL_RetiraFormato = texto
        Case cF_MONEY_ESC_DBL, cF_MONEY_EUR
            BL_RetiraFormato = Format(Trim(Mid(texto, 1, Len(texto) - 3)), cF_DOUBLE_SIMPLES)
        Case cF_MONEY_ESC_LNG
            BL_RetiraFormato = Format(Trim(Mid(texto, 1, Len(texto) - 3)), cF_INTEGER)
        Case Else
            BL_RetiraFormato = texto
    End Select
    
End Function

Function BL_ValidaCampoNumerico1(campo) As Boolean
    
    ' Utilizada na gest�o de classes do tipo DataRepeater.
    
    Dim TmpStr As String
    Dim ValorCampo
    
    BL_ValidaCampoNumerico1 = True
    If campo.text = "" Then
        Exit Function
    End If
    If campo.Tag <> "" Then
        
        campo.text = BL_FormataCampo(campo.text, campo.Tag)
        ' a linha anterior serve para assegurar que o campo est� formatado quando o utilizador o alterou
        ' caso contr�rio seria necess�rio detectar se o campo tem a formata��o certa
        ValorCampo = BL_RetiraFormato(campo.text, campo.Tag)
        If Not IsNumeric(ValorCampo) Then
            MsgBox "O campo tem de ser num�rico."
            campo.SetFocus
            BL_ValidaCampoNumerico1 = False
            Exit Function
        End If
    
        TmpStr = Format(campo.text, campo.Tag)
        If Len(TmpStr) > Len(campo.Tag) Then
            MsgBox "O valor � muito grande."
            campo.SetFocus
            BL_ValidaCampoNumerico1 = False
            Exit Function
        End If
        campo.text = TmpStr

    End If
End Function

Public Sub BL_DesactivaCampo(campo As Control)
    
    ' Utilizada na gest�o de classes do tipo DataRepeater.
    
    campo.Enabled = False
    campo.BackColor = &HE0E0E0

End Sub

Public Sub BL_SeleccionaValorCombo(valor As String, controlo)

    ' Selecciona o valor numa combo com base no seu conteudo
    ' e n�o no DataItem.
    
    Dim i As Integer
    valor = UCase(Trim(valor))
    If valor = "" Then
        Exit Sub
    End If
    For i = 0 To controlo.ListCount - 1
        If UCase(Trim(controlo.ItemData(i))) = valor Then
            controlo.ListIndex = i
            Exit Sub
        End If
    Next i

End Sub

Public Sub BL_RemoveTabela(ByVal NomeTabela As String)

    Dim sql As String
    
    sql = "DELETE FROM " & NomeTabela
'    BG_ExecutaQuery_ADO Sql

    Call BG_ExecutaQuery_ADO("DROP TABLE " & NomeTabela, -1)
    
End Sub

Public Sub BL_ActualizaEstadoMenusPersonalizados(Estado As String)

    ' Estado = ABERTO/FECHADO

    Dim SQLQuery As String
    Dim codigo As Integer
    Dim registos As Integer
    
    SQLQuery = "UPDATE " & cParamAmbiente & " set conteudo = " & BL_TrataStringParaBD(Estado) & _
            ", user_act = '" & gCodUtilizador & "', dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & _
            " WHERE chave = " & BL_TrataStringParaBD("ESTADO_MENU_PERSONALIZADO") & _
            " AND cod_utilizador = " & gCodUtilizador

    gSQLError = 0
    gSQLISAM = 0
    registos = 0
    registos = BG_ExecutaQuery_ADO(SQLQuery)
    
    If (gSQLError <> 0 And gSQLISAM <> 0) Or (registos <= 0) Then
        codigo = BG_DaMAX(cParamAmbiente, "codigo") + 1
        
        SQLQuery = "INSERT INTO " & cParamAmbiente & _
            " ( codigo, acesso, ambito, cod_utilizador,conteudo,chave," & _
            " conteudo_fixo,obs,user_cri,dt_cri ) VALUES " & _
            "(" & codigo & "," & gT_NaoVisualizar & "," & gT_Utilizador & "," & _
            gCodUtilizador & "," & BL_TrataStringParaBD(Estado) & _
            "," & BL_TrataStringParaBD("ESTADO_MENU_PERSONALIZADO") & "," & BL_TrataStringParaBD("ABERTO;FECHADO") & _
            "," & BL_TrataStringParaBD("Indica se o menu personalizado do utilizador ficou aberto ou fechado na �ltima utiliza��o.") & _
            ",'" & gCodUtilizador & "'," & BL_TrataDataParaBD(Date) & ")"
        gSQLError = 0
        gSQLISAM = 0
        BG_ExecutaQuery_ADO SQLQuery
        
        If gSQLError <> 0 And gSQLISAM <> 0 Then
            BG_LogFile_Erros "Erro a escrever estado no MenuPersonalizado:" & gSQLError & "/" & gSQLISAM
        End If
    End If

End Sub

Public Sub BL_ActualizaEstadoImpResAnt()

    Dim SQLQuery As String
    Dim codigo As Integer
    Dim registos As Integer
    
    SQLQuery = "UPDATE " & cParamAmbiente & " set conteudo = " & BL_TrataStringParaBD(str(gImp_Res_Ant)) & _
            ", user_act = '" & gCodUtilizador & "', dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & _
            " WHERE chave = " & BL_TrataStringParaBD("IMP_RES_ANT") & _
            " AND cod_utilizador = " & gCodUtilizador

    gSQLError = 0
    gSQLISAM = 0
    registos = 0
    registos = BG_ExecutaQuery_ADO(SQLQuery)
    
    If (gSQLError <> 0 And gSQLISAM <> 0) Or (registos <= 0) Then
        codigo = BG_DaMAX(cParamAmbiente, "codigo") + 1
        
        SQLQuery = "INSERT INTO " & cParamAmbiente & _
            " ( codigo, acesso, ambito, cod_utilizador,conteudo,chave," & _
            " conteudo_fixo,obs,user_cri,dt_cri ) VALUES " & _
            "(" & codigo & "," & gT_NaoVisualizar & "," & gT_Utilizador & "," & _
            gCodUtilizador & "," & BL_TrataStringParaBD(str(gImp_Res_Ant)) & _
            "," & BL_TrataStringParaBD("IMP_RES_ANT") & ", null " & _
            "," & BL_TrataStringParaBD("Indica se o utilizador escolheu imprimir os resultados anteriores de um utente, na ultima impress�o de resultados.") & _
            ",'" & gCodUtilizador & "'," & BL_TrataDataParaBD(Date) & ")"
        gSQLError = 0
        gSQLISAM = 0
        BG_ExecutaQuery_ADO SQLQuery
        
        If gSQLError <> 0 And gSQLISAM <> 0 Then
            BG_LogFile_Erros "Erro a escrever IMP_RES_ANT:" & gSQLError & "/" & gSQLISAM
        End If
    End If

End Sub

Public Function BL_ExecutaReport(Optional SemForm As Boolean = False, Optional SemReset As Boolean = False, Optional NumReq As String, Optional MultiReport As Integer) As Boolean
    
    Dim Report As CrystalReport
        
    On Local Error GoTo fim
        
    BL_ExecutaReport = False
    Set Report = forms(0).Controls("Report")
    'Faz o Lock � aplica��o
    If SemForm = False Then
        forms(0).Enabled = False
    End If
    
       
    If gModoDebug = mediSim Then BG_LogFile_Erros Report.SQLQuery
    If Report.Destination = crptToWindow Then
        'Atribui a Form como sendo Child da Form Mdi
         Report.WindowParentHandle = forms(0).hwnd
         If gRelActivoFich = "MapaResultadosSimples" Then
            Report.WindowShowPrintBtn = False
            Report.WindowShowPrintSetupBtn = False
            Report.WindowShowExportBtn = False
         End If
    End If
        
    'verifica se � da lista de relat�rios ou n�o
    'se sim cria doc na directoria especificada na chave DOC_PATH
    If MultiReport = 1 Then
        
        'Indica o caminho para os documentos a abrir pela GesDoc
        gGDDocPath = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "DOC_PATH")
        
        Report.Destination = crptToFile
        Report.PrintFileType = crptRTF
        Report.PrintFileName = gGDDocPath & "Req" & NumReq & ".rtf"
        gGDDocPath = Report.PrintFileName
    End If
    
    If gF_RESMULT = 1 Then
        Sleep 500
    End If
        
    'Report.Connect = gDSN
    Report.Action = 1
    If Report.Destination = crptToWindow Then
'         Report.PageShow (Report.ReportLatestPage)
         Report.PageShow (Report.ReportStartPage)
    End If

    ' Cria o documento para o eResults.
    If (geResults) Then
'        Report.Destination = crptToPrinter
'        Report.PrinterName = geResults_Impr_PDF
'        Report.Action = 1
    End If
    
    'verifica se � da lista de relat�rios ou n�o
    'se sim cria doc na directoria especificada na chave DOC_PATH
    If gMultiReport = 1 And MultiReport = 0 And (gRelActivoFich = "MapaResultadosSimples" Or gRelActivoFich = "RelAminoacidos" Or gRelActivoFich = "RelSegundasAnalises") Then
        Dim ReportRTF As CrystalReport
        Set ReportRTF = Report
        ReportRTF.Destination = crptToFile
        ReportRTF.PrintFileType = crptRTF
        ReportRTF.PrintFileName = gGesDocPathRelatorios & "\" & gRelActivoFich & NumReq & ".rtf"
        ReportRTF.Action = 1
    End If
    
    'BRUNODSANTOS glintt-hs-18011
    If gATIVA_LOGS_RGPD = mediSim And BL_DevolveModoLog = response Then
        
        Dim RPT As CrystalReport
        Dim strB64 As String
        Dim f As Long
        Dim b() As Byte
        
        Set RPT = Report
        Dim sFileName As String
        sFileName = gDirCliente & "\TEMP_" & CStr(gComputador) & ".doc"
        
        If Dir(sFileName) <> "" Then
            Kill (sFileName)
        End If
        
        RPT.Destination = crptToFile
        RPT.PrintFileType = crptWinWord
        RPT.PrintFileName = sFileName
        RPT.Action = 1
            
        f = FreeFile
        Open sFileName For Binary As f
        ReDim b(LOF(f))
        Get f, , b()
        Close f
        
        strB64 = BG_EncodeBase64(b)
        
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
               Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", "{}", strB64
                
                
        If Dir(sFileName) <> "" Then
            Kill (sFileName)
        End If
        
    End If
    '
fim:
    'Quer tenha sido bem sucedida ou n�o, � feito sempre o tratamento dos erros!
    
    If SemForm = False Then
        forms(0).Enabled = True
    End If

    'Limpa as propriedades do report
    If SemReset = False Then
        Report.Reset
    End If
    
    'Muda o ponteiro do mouse no fim
    If Not forms(0).ActiveForm Is Nothing Then
        Call BL_FimProcessamento(forms(0).ActiveForm, "")
    End If
    'Erros do Cancel da Common Dialog(32755) e do Cancel do Print File do Crystal Reports (20545)
    'O Erro 0 � quando a opera��o foi bem sucedida nos 2 casos!
'    F.Enabled = True
    Select Case Err.Number
        Case 0, 20545
            BL_ExecutaReport = True
            Exit Function
        Case 20520, 20524
            'Impress�o ainda em curso
            Call BG_Mensagem(mediMsgBox, Err.Description & Chr(13) & Err.Number, vbCritical + vbOKOnly, App.ProductName & " - Erro de impress�o")
            Call BG_LogFile_Erros(Err.Description & " " & Err.Number)
            Exit Function
        Case 20529
            'Disco Cheio
            Call BG_Mensagem(mediMsgBox, Err.Description & Chr(13) & Err.Number, vbCritical + vbOKOnly, App.ProductName & " - Erro de impress�o")
            Call BG_LogFile_Erros(Err.Description & " " & Err.Number)
            Exit Function
        Case 20500
            'Mem�ria insuficiente para impress�o
            Call BG_Mensagem(mediMsgBox, Err.Description & Chr(13) & Err.Number, vbCritical + vbOKOnly, App.ProductName & " - Erro de impress�o")
            Call BG_LogFile_Erros(Err.Description & " " & Err.Number)
            Exit Function
        Case 20545
            'ODBC inexistente
            Call BG_Mensagem(mediMsgBox, Err.Description & Chr(13) & Err.Number, vbCritical + vbOKOnly, App.ProductName & " - Erro de impress�o")
            Call BG_LogFile_Erros(Err.Description & " " & Err.Number)
            Exit Function
        Case 20544
            'Ficheiro inexistente=>(Prevenido com a fun��o Dir(s))!
            Call BG_Mensagem(mediMsgBox, Err.Description & Chr(13) & Err.Number, vbCritical + vbOKOnly, App.ProductName & " - Erro de impress�o")
            Call BG_LogFile_Erros(Err.Description & " " & Err.Number)
            Exit Function
        Case Else
            'Erro do programa
            Call BG_Mensagem(mediMsgBox, Err.Description & Chr(13) & Err.Number, vbCritical + vbOKOnly, App.ProductName & " - Erro de impress�o")
            Call BG_LogFile_Erros(Err.Description & " " & Err.Number)
'            End
    End Select
    'Limpa o erro
    Err.Clear
        
    'Desinstancia o Report
    Set Report = Nothing
    Exit Function
    Resume Next
End Function

Public Sub BL_FechaPreview(ByVal Titulo As String)
    
    Dim ReportHwnd As Long
    
    'Se verificar que a Form Preview j� estava aberta ent�o apenas fecha a Form e muda o destino do Report para a impressora!!
    ReportHwnd = FindWindowEx(gParentChild, ByVal 0&, vbNullString, Trim(Titulo))
    If ReportHwnd <> 0 Then
        'Fecha o Print Preview
        Call PostMessage(ReportHwnd, WM_CLOSE, 0&, 0&)
    End If
    
End Sub

Public Function BL_SelCountAnaReq(num_req As Long, Estado_Ana As Integer) As Long
    
    Dim SqlM As String
    Dim RsM As ADODB.recordset
    Dim CountM As Long
    Dim SqlR As String
    Dim rsR As ADODB.recordset
    Dim CountR As Long
    
    If Estado_Ana = 0 Then
        SqlM = "SELECT COUNT(*) as Conta FROM sl_marcacoes WHERE n_req=" & BL_TrataStringParaBD(CStr(num_req))
        If gLAB = "LHL" Then
            SqlM = SqlM & " AND cod_ana_S <> 'S99999' "
        End If
        
        Set RsM = New ADODB.recordset
        RsM.CursorLocation = adUseServer
        RsM.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros SqlM
        RsM.Open SqlM, gConexao
        CountM = BL_HandleNull(RsM!conta, 0)
        If BL_HandleNull(RsM!conta, 0) = 0 Then
            SqlM = "DELETE FROM sl_marcacoes where n_req = " & BL_TrataStringParaBD(CStr(num_req))
            BG_ExecutaQuery_ADO SqlM
        End If
        BL_SelCountAnaReq = CountM
    Else
        SqlR = "SELECT COUNT(*) as Conta FROM sl_realiza WHERE n_req=" & BL_TrataStringParaBD(CStr(num_req)) & " AND flg_estado=" & BL_TrataStringParaBD(CStr(Estado_Ana))
        Set rsR = New ADODB.recordset
        rsR.CursorLocation = adUseServer
        rsR.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros SqlR
        rsR.Open SqlR, gConexao
        CountR = BL_HandleNull(rsR!conta, 0)
        BL_SelCountAnaReq = CountR
    End If
    
    If Not RsM Is Nothing Then
        RsM.Close
        Set RsM = Nothing
    End If
    If Not rsR Is Nothing Then
        rsR.Close
        Set rsR = Nothing
    End If
    
End Function

Public Function BL_SelCountAnaHist(num_req As Long) As Long
    
    Dim SqlH As String
    Dim rsH As ADODB.recordset
    Dim CountH As Long
    
    SqlH = "SELECT COUNT(*) as Conta FROM sl_realiza_h WHERE n_req=" & num_req
    Set rsH = New ADODB.recordset
    rsH.CursorLocation = adUseServer
    rsH.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros SqlH
    rsH.Open SqlH, gConexao
    CountH = BL_HandleNull(rsH!conta, 0)
    
    BL_SelCountAnaHist = CountH
    
    If Not rsH Is Nothing Then
        rsH.Close
        Set rsH = Nothing
    End If

End Function

Public Function BL_SelCountAnaBloq(num_req As Long) As Long
    
    Dim SqlH As String
    Dim rsH As ADODB.recordset
    Dim CountH As Long
    
    SqlH = "SELECT COUNT(*) as Conta FROM sl_req_bloq WHERE n_req=" & num_req
    Set rsH = New ADODB.recordset
    rsH.CursorLocation = adUseServer
    rsH.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros SqlH
    rsH.Open SqlH, gConexao
    CountH = BL_HandleNull(rsH!conta, 0)
    
    BL_SelCountAnaBloq = CountH
    
    If Not rsH Is Nothing Then
        rsH.Close
        Set rsH = Nothing
    End If

End Function

Public Function BL_Mediana(valores() As String) As Double
    
    Dim n As Integer
    Dim T1 As Integer
    Dim T2 As Integer
    Dim t As Integer
    
    'Esta fun��o calcula a Mediana(Valor central numa gama de valores)
    'a partir de N elementos (repetidos ou n�o) ordenados e discretos.
    'Ex� N Par=> 2,4,4,5,7,10
    'Mediana= (4+5)/2=4,5=>Valor central de refer�ncia para os 6 valores!!
    
    'Ordena o Vector
    Call BL_OrdenaVector(valores)
    
    'Verifica se o N de elementos do Vector � par ou �mpar
    n = UBound(valores)
    If n Mod 2 <> 0 Then
        '� �mpar
        t = (n + 1) / 2
        BL_Mediana = BL_String2Double(valores(t))
    Else
        '� Par=>� a m�dia dos elementos mais pr�ximos do centro!
        T1 = n / 2
        T2 = (n + 2) / 2
        BL_Mediana = (BL_String2Double(valores(T1)) + BL_String2Double(valores(T2))) / 2
    End If
    
End Function

Function BL_MudaEstadoReq(n_req As Long, Optional Imprimir As Boolean, Optional GrupoAna As String) As String

    Dim Estado As String
    Dim S_Resultado As Long
    Dim N_Validados As Long
    Dim n_semChegada As Long
    Dim AnaBloqueados As Long
    Dim Validados As Long
    Dim Impressos As Long
    Dim Historico As Long
    Dim ReqBloq As Long
    Dim Canceladas As Long
    Dim sql As String
    Dim rs As New ADODB.recordset
    On Error GoTo TrataErro
    
    '15/7/2002
    'Filipe Nogueira
    
    'If gImprimirComValTec = mediSim And Imprimir = True And (gModoRes = cModoResTecVal Or gModoRes = cModoResTecAltVal) Then
    If gImprimirComValTec = mediSim And Imprimir = True And (gPermResUtil = cValTecRes Or gPermResUtil = cValMedRes) And GrupoAna = "" Then
        'SE A IMPRESSAO TECNICA ESTIVER ACTIVA
        'Qd estivermos a validar (e imprimir) tecnicamente coloca o estado da requisi��o como impresso (F),
            'para permitir que a requisi��o fique no estado final sem que exista a valida��o m�dica
        '(Filipe Nogueira) ('HPP,HPOVOA')
        'If gModoRes = cModoResTecVal Or gModoRes = cModoResTecAltVal Then
        If gPermResUtil = cValTecRes Or gPermResUtil = cValMedRes Then
            Estado = gEstadoReqTodasImpressas
        End If
    Else
        AnaBloqueados = BL_SelCountAnaReq(n_req, gEstadoAnaBloqueada)
        S_Resultado = BL_SelCountAnaReq(n_req, 0)
        N_Validados = BL_SelCountAnaReq(n_req, 1)
        N_Validados = N_Validados + BL_SelCountAnaReq(n_req, gEstadoAnaValidacaoTecnica)
        N_Validados = N_Validados + BL_SelCountAnaReq(n_req, gEstadoAnaPendente)
        Validados = BL_SelCountAnaReq(n_req, gEstadoAnaValidacaoMedica)
        Impressos = BL_SelCountAnaReq(n_req, gEstadoAnaImpressa)
        Historico = BL_SelCountAnaHist(n_req)
        ReqBloq = BL_SelCountAnaBloq(n_req)
        
        sql = "SELECT * FROM SL_REQ_CANC WHERE n_req = " & n_req
        Set rs = New ADODB.recordset
        rs.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rs.Open sql, gConexao, adOpenStatic
        Canceladas = rs.RecordCount
        rs.Close
        Set rs = Nothing
        
        'analises sem chegada de tubos
        sql = "SELECT * FROM sl_req_tubo WHERE n_req = " & n_req & " AND dt_chega IS NULL "
        Set rs = New ADODB.recordset
        rs.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rs.Open sql, gConexao, adOpenStatic
        n_semChegada = rs.RecordCount
        rs.Close
        Set rs = Nothing
        If Historico <> 0 Then
            'HISTORICO
            Estado = gEstadoReqHistorico
        ElseIf Canceladas > 0 Then
            'CANCELADA
            Estado = gEstadoReqCancelada
        ElseIf ReqBloq > 0 Then
            'BLOQUEADA
            Estado = gEstadoReqBloqueada
        ElseIf S_Resultado = 0 And N_Validados = 0 And Validados = 0 And Impressos = 0 Then
            'Sem an�lises
            Estado = gEstadoReqSemAnalises
        ElseIf S_Resultado = 0 And N_Validados = 0 And Validados = 0 And Impressos > 0 Then
            'Tudo Impresso
            Estado = gEstadoReqTodasImpressas
        ElseIf S_Resultado = 0 And N_Validados = 0 And Validados > 0 And Impressos = 0 Then
            'Tudo Validado
            Estado = gEstadoReqValicacaoMedicaCompleta
        ElseIf S_Resultado = 0 And N_Validados = 0 And Validados > 0 And Impressos > 0 Then
            'Algumas impressas
            Estado = gEstadoReqImpressaoParcial
        ElseIf S_Resultado = 0 And N_Validados > 0 And Validados = 0 And Impressos = 0 Then
            'Tudo por Validar
            Estado = gEstadoReqEsperaValidacao
        ElseIf S_Resultado = 0 And N_Validados > 0 And Validados = 0 And Impressos > 0 Then
            'Algumas Impressas
            Estado = gEstadoReqImpressaoParcial
        ElseIf S_Resultado = 0 And N_Validados > 0 And Validados > 0 And Impressos = 0 Then
            'Algumas Validadas
            Estado = gEstadoReqValidacaoMedicaParcial
        ElseIf S_Resultado = 0 And N_Validados > 0 And Validados > 0 And Impressos > 0 Then
            'Algumas Impressas
            Estado = gEstadoReqImpressaoParcial
        ElseIf S_Resultado > 0 And N_Validados = 0 And Validados = 0 And Impressos = 0 And n_semChegada = 0 Then
            'Tudo sem resultado
            Estado = gEstadoReqEsperaResultados
        ElseIf S_Resultado > 0 And N_Validados = 0 And Validados = 0 And Impressos = 0 And n_semChegada > 0 Then
            'A ESPERA DE PRODUTO
            Estado = gEstadoReqEsperaProduto
        ElseIf S_Resultado > 0 And N_Validados = 0 And Validados = 0 And Impressos > 0 Then
            'Algumas Impressas
            Estado = gEstadoReqImpressaoParcial
        ElseIf S_Resultado > 0 And N_Validados = 0 And Validados > 0 And Impressos = 0 Then
            'Algumas validadas
            Estado = gEstadoReqValidacaoMedicaParcial
        ElseIf S_Resultado > 0 And N_Validados = 0 And Validados > 0 And Impressos > 0 Then
            'Algumas Impressas
            Estado = gEstadoReqImpressaoParcial
        ElseIf S_Resultado > 0 And N_Validados > 0 And Validados = 0 And Impressos = 0 Then
            'Algumas com resultado
            Estado = gEstadoReqResultadosParciais
        ElseIf S_Resultado > 0 And N_Validados > 0 And Validados = 0 And Impressos > 0 Then
            'Algumas Impressas
            Estado = gEstadoReqImpressaoParcial
        ElseIf S_Resultado > 0 And N_Validados > 0 And Validados > 0 And Impressos = 0 Then
            'Algumas Validadas
            Estado = gEstadoReqValidacaoMedicaParcial
        ElseIf S_Resultado > 0 And N_Validados > 0 And Validados > 0 And Impressos > 0 Then
            'Algumas Impressas
            Estado = gEstadoReqImpressaoParcial
        Else
            '??
            Estado = ""
        End If
    End If
    
    If Estado <> "" Then
        sql = "UPDATE sl_requis SET estado_req='" & Estado & "' WHERE n_req=" & BL_TrataStringParaBD(CStr(n_req))
        BG_ExecutaQuery_ADO sql
        If gSQLError <> 0 Then
            BG_Mensagem mediMsgBox, "Erro a actualizar estado da requisi��o!", vbOKOnly + vbError, "ERRO"
        End If
    End If
    
    BL_MudaEstadoReq = Estado
Exit Function
TrataErro:
    BL_MudaEstadoReq = ""
    BG_LogFile_Erros "Erro ao Mudar Estado Requisi��o: -> " & sql & Err.Description & " ", "BL", "BL_MudaEstadoReq", False
    Exit Function
    Resume Next
    
End Function


Public Sub BL_OrdenaVector(Vector() As String)
    
    ' Este algoritmo ordena um vector unidimensional com elementos do tipo
    ' Double a partir do m�todo de ordena��o Shell.
     
    Dim intervalo As Integer
    Dim n As Integer
    Dim i As Integer
    Dim indice As Integer
    Dim temp As String
    
    n = UBound(Vector)
    intervalo = n \ 2
    While intervalo > 0
        For i = intervalo + 1 To n
            indice = i - intervalo
            While indice > 0
                If BL_String2Double(Vector(indice)) > BL_String2Double(Vector(indice + intervalo)) Then
                    temp = Vector(indice)
                    Vector(indice) = Vector(indice + intervalo)
                    Vector(indice + intervalo) = temp
                    indice = indice - intervalo
                Else
                    indice = 0
                End If
            Wend
        Next i
        intervalo = intervalo \ 2
    Wend
     
     
End Sub

Public Function BL_GetComputerCodigo() As Integer

    On Error GoTo ErrorHandler
    Dim codigo As Integer
    Dim obComputador As ADODB.recordset
    Dim SQLQueryAux As String
    
    ' In�cio: Determinar o c�digo do computador.
    
    Set obComputador = New ADODB.recordset
    
    SQLQueryAux = "SELECT " & vbCrLf & _
                  "     cod_computador " & vbCrLf & _
                  "FROM " & vbCrLf & _
                  "     sl_computador " & vbCrLf & _
                  "WHERE " & vbCrLf & _
                  "     descr_computador = '" & gComputador & "' "
        
    obComputador.CursorLocation = adUseServer
    obComputador.CursorType = adOpenStatic
     
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLQueryAux
    obComputador.Open SQLQueryAux, gConexao
        
    If obComputador.RecordCount <= 0 Or IsNull(obComputador!cod_computador) Then
        ' Computador n�o existe => Codifica��o autom�tica
        codigo = BL_CriaComputer
    Else
        codigo = obComputador!cod_computador
    End If
    BL_GetComputerCodigo = codigo
    obComputador.Close
    Set obComputador = Nothing
    
    ' Fim: Determinar c�digo computador
        
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : BL_GetComputerCodigo (BibliotecaLocal) -> " & Err.Number & " : " & Err.Description)
            Set obComputador = Nothing
            BL_GetComputerCodigo = -1
            Exit Function
    End Select
End Function

Public Function BL_CriaComputer() As Integer
    
    Dim SQLInsComputador As String
    Dim Max As Integer
    
    Max = BG_DaMAX("sl_computador", "cod_computador")
    Max = Max + 1
    
    SQLInsComputador = "INSERT INTO sl_computador (cod_computador,descr_computador) VALUES ( " & Max & ", '" & BG_SYS_GetComputerName & "')"
    BG_LogFile_Erros "Novo Computador : " & SQLInsComputador
    BG_ExecutaQuery_ADO SQLInsComputador
     
End Function

Public Function BL_CriaImpressora() As Integer
    Dim sSql As String
    Dim codComputador As Integer
    Dim codTerminal As Integer
    Dim i As Integer
    codComputador = BL_GetComputerCodigo
    codTerminal = BL_GetTerminalCodigo
    sSql = "DELETE FROM Sl_impressora WHERE cod_computador = " & codComputador
    If codTerminal > mediComboValorNull Then
        sSql = sSql & " AND cod_terminal = " & codTerminal
    Else
        sSql = sSql & " AND (cod_terminal IS NULL or cod_terminal = -1)"
    End If
    BG_ExecutaQuery_ADO sSql
    For i = 0 To Printers.Count - 1
        sSql = "INSERT INTO sl_impressora (cod_computador,cod_terminal, descr_impressora) VALUES(" & codComputador & "," & codTerminal & ","
        sSql = sSql & BL_TrataStringParaBD(Printers(i).DeviceName) & ")"
        BG_ExecutaQuery_ADO sSql
    Next
End Function

Public Function BL_GetTerminalCodigo() As Integer

    On Error GoTo ErrorHandler
    
    Dim obComputador As ADODB.recordset
    Dim SQLQueryAux As String
    
    ' In�cio: Determinar o c�digo do computador.
    
    Set obComputador = New ADODB.recordset
    If BG_SYS_GetTerminalName = "" Then
        BL_GetTerminalCodigo = -1
        Exit Function
    End If
    
    SQLQueryAux = "SELECT " & vbCrLf & _
                  "     cod_terminal " & vbCrLf & _
                  "FROM " & vbCrLf & _
                  "     sl_terminal " & vbCrLf & _
                  "WHERE " & vbCrLf & _
                  "     descr_terminal = '" & BG_SYS_GetTerminalName & "' "
        
    obComputador.CursorLocation = adUseServer
    obComputador.CursorType = adOpenStatic
     
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLQueryAux
    obComputador.Open SQLQueryAux, gConexao
        
    If obComputador.RecordCount <= 0 Or IsNull(obComputador!cod_terminal) Then
        ' Computador n�o existe => Codifica��o autom�tica
        BL_GetTerminalCodigo = BL_CriaTerminal
    Else
        BL_GetTerminalCodigo = obComputador!cod_terminal
    End If
    
    obComputador.Close
    Set obComputador = Nothing
    
    ' Fim: Determinar c�digo computador
        
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : BL_GetTerminalCodigo (BibliotecaLocal) -> " & Err.Number & " : " & Err.Description)
            Set obComputador = Nothing
            BL_GetTerminalCodigo = -1
            Exit Function
    End Select
End Function

Public Function BL_CriaTerminal() As Integer
    
    Dim SQLInsComputador As String
    Dim Max As Integer
    
    Max = BG_DaMAX("sl_terminal", "cod_terminal")
    Max = Max + 1
    
    SQLInsComputador = "INSERT INTO sl_terminal (cod_terminal,descr_terminal) VALUES ( " & Max & ", '" & BG_SYS_GetTerminalName & "')"
    BG_LogFile_Erros "Novo terminal : " & SQLInsComputador
    BG_ExecutaQuery_ADO SQLInsComputador
     
End Function

Public Function BL_SelImpressora(ByVal Report As String) As String
    Dim i As Long
    Dim RegImp As ADODB.recordset
    Dim s As String
    
    Set RegImp = New ADODB.recordset
    RegImp.LockType = adLockReadOnly
    RegImp.CursorLocation = adUseServer
    RegImp.CursorType = adOpenStatic
    RegImp.ActiveConnection = gConexao
    If gTerminal <> "" Then
        RegImp.Source = "SELECT descr_prt FROM sl_conf_prt, sl_relatorios " & _
               "WHERE (cod_utilizador IS NULL OR cod_utilizador=" & gCodUtilizador & " )AND cod_computador=" & _
               BL_SelCodigo("SL_COMPUTADOR", "COD_COMPUTADOR", "DESCR_COMPUTADOR", gComputador) & " AND fich_relatorio=" & BL_TrataStringParaBD(Report) & _
               " AND descr_relatorio = descr_rpt AND cod_terminal = " & _
               BL_HandleNull(BL_SelCodigo("SL_TERMINAL", "COD_TERMINAL", "DESCR_TERMINAL", gTerminal), -1)
    Else
        RegImp.Source = "SELECT descr_prt FROM sl_conf_prt, sl_relatorios " & _
                "WHERE (cod_utilizador IS NULL OR cod_utilizador=" & gCodUtilizador & " )AND cod_computador=" & _
                BL_SelCodigo("SL_COMPUTADOR", "COD_COMPUTADOR", "DESCR_COMPUTADOR", gComputador) & " AND fich_relatorio=" & BL_TrataStringParaBD(Report) & _
               " AND descr_relatorio = descr_rpt AND (cod_terminal IS NULL OR cod_terminal = -1)"
    End If
    If gModoDebug = mediSim Then BG_LogFile_Erros RegImp.Source
    RegImp.Open
    
    If RegImp.RecordCount = 0 Then
        BL_SelImpressora = ""
    Else
        If InStr(1, UCase(RegImp!descr_prt), "IN SESSION") > 0 Then
            For i = 0 To (Printers.Count - 1)
                If InStr(1, UCase(Printers(i).DeviceName), "IN SESSION") > 0 Then
                    If Trim(Mid(RegImp!descr_prt, 1, InStr(1, UCase(RegImp!descr_prt), "IN SESSION") - 1)) = Trim(Mid(Printers(i).DeviceName, 1, InStr(1, UCase(Printers(i).DeviceName), "IN SESSION") - 1)) Then
                        BL_SelImpressora = Trim("" & Printers(i).DeviceName)
                        Exit For
                    End If
                End If
            Next
        ElseIf InStr(1, UCase(RegImp!descr_prt), "REDIRECTED") > 0 Then
            For i = 0 To (Printers.Count - 1)
                If InStr(1, UCase(Printers(i).DeviceName), "REDIRECTED") > 0 Then
                    If Trim(Mid(RegImp!descr_prt, 1, InStr(1, UCase(RegImp!descr_prt), "REDIRECTED") - 1)) = Trim(Mid(Printers(i).DeviceName, 1, InStr(1, UCase(Printers(i).DeviceName), "REDIRECTED") - 1)) Then
                        BL_SelImpressora = Trim("" & Printers(i).DeviceName)
                        Exit For
                    End If
                End If
            Next
        Else
            BL_SelImpressora = Trim("" & RegImp!descr_prt)
        End If
    End If
    
    RegImp.Close
    Set RegImp = Nothing
    
End Function

Public Function BL_FormulaDescodifica(ByVal n_req As String, ByVal HistoricoReq As Boolean, ByVal formula As String) As String

    Dim tam As Integer
    Dim i As Integer
    
    Dim PosCod As Integer
    Dim Sinais As String
    Dim codigo As String
    Dim valor As String
        
    Dim CmdRes As ADODB.Command
    Dim CmdResHist As ADODB.Command
    
    Dim regRes As ADODB.recordset
    Dim RegAssoc As ADODB.recordset
    Dim sql As String
        
    Dim n_Req_assoc As String
    Dim Historico_Req_Assoc As Boolean
            
    On Error GoTo erro
            
    'Verifica a sintaxe da F�rmula
'    Formula = Replace(Formula, " ", "")
    If BL_FormulaResolve(formula, True, 2) = "" Then
        BL_FormulaDescodifica = ""
        Exit Function
    End If
    
    Sinais = "+-*/^()<>="
        
    '____________________________________________________________________________________
    
    'Verifica se existe Requisi��o Associada para a Requisi��o em causa
'    Set RegAssoc = New ADODB.recordset
'    RegAssoc.CursorLocation = adUseServer
'    RegAssoc.CursorType = adOpenForwardOnly
'    RegAssoc.LockType = adLockReadOnly
'    RegAssoc.ActiveConnection = gConexao
'    Sql = "SELECT n_req,estado_req FROM sl_requis " & _
'          "WHERE n_req = (SELECT N_Req_Assoc FROM sl_requis WHERE n_req=" & n_req & ")"
'    RegAssoc.Source = Sql
'    RegAssoc.Open
'    If RegAssoc.RecordCount <> 0 Then
'        N_REQ_ASSOC = "" & RegAssoc!n_req
'        If Trim(UCase("" & RegAssoc!estado_req)) = "H" Then
'            Historico_Req_Assoc = True
'        Else
'            Historico_Req_Assoc = False
'        End If
'    Else
'        N_REQ_ASSOC = ""
'        Historico_Req_Assoc = False
'    End If
'    RegAssoc.Close
'    Set RegAssoc = Nothing
    
    '____________________________________________________________________________________
    
    
    'Define o Comando dos Resultados Actuais e Hist�rico (APENAS OS VALIDADOS)
    
    'Actuais
    Set CmdRes = New ADODB.Command
    CmdRes.ActiveConnection = gConexao
    CmdRes.CommandType = adCmdText
    CmdRes.Prepared = True
    sql = " SELECT Res.result " & _
          " FROM sl_realiza ResCab,sl_res_alfan Res" & _
          " WHERE ResCab.flg_estado IN ('3','4','1','5') " & _
          " AND ResCab.cod_ana_s=?" & _
          " AND ResCab.n_req=?" & _
          " AND Res.n_res=?" & _
          " AND ResCab.seq_realiza=Res.seq_realiza "
    CmdRes.CommandText = sql
    CmdRes.Parameters.Append CmdRes.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
    CmdRes.Parameters.Append CmdRes.CreateParameter("N_REQ", adInteger, adParamInput, 7)
    CmdRes.Parameters.Append CmdRes.CreateParameter("N_RES", adInteger, adParamInput, 1)
    
    'Hist�rico
    Set CmdResHist = New ADODB.Command
    CmdResHist.ActiveConnection = gConexao
    CmdResHist.CommandType = adCmdText
    CmdResHist.Prepared = True
    sql = " SELECT Res.result " & _
          " FROM sl_realiza_h ResCab,sl_res_alfan_h Res" & _
          " WHERE ResCab.flg_estado IN ('3','4','1','5') " & _
          " AND ResCab.cod_ana_s=?" & _
          " AND ResCab.n_req=?" & _
          " AND Res.n_res=?" & _
          " AND ResCab.seq_realiza=Res.seq_realiza"
    CmdResHist.CommandText = sql
    CmdResHist.Parameters.Append CmdResHist.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
    CmdResHist.Parameters.Append CmdResHist.CreateParameter("N_REQ", adInteger, adParamInput, 7)
    CmdResHist.Parameters.Append CmdResHist.CreateParameter("N_RES", adInteger, adParamInput, 1)
    
    
    'Verifica se existem c�digos de An�lises na F�rmula
    PosCod = InStr(1, formula, "$")
    If PosCod = 0 Then
        PosCod = InStr(1, formula, "@")
    End If
    While PosCod <> 0
        'Extrai o C�digo na F�rmula
        codigo = ""
        valor = ""
        tam = Len(formula)
        For i = PosCod To tam
            If InStr(1, Sinais, Mid(formula, i, 1)) <> 0 Then
                Exit For
            End If
        Next i
        
        If i <= tam Then
            codigo = Mid(formula, PosCod, i - PosCod)
        Else
            codigo = Mid(formula, PosCod)
        End If
        
        'Verifica qual � o valor do resultado da an�lise
        If HistoricoReq = False Then
            CmdRes.Parameters("COD_ANA_S").value = Mid(codigo, 2)
            CmdRes.Parameters("N_REQ").value = n_req
            CmdRes.Parameters("N_RES").value = IIf(left(codigo, 1) = "$", 1, 2)
            Set regRes = CmdRes.Execute
        Else
            CmdResHist.Parameters("COD_ANA_S").value = Mid(codigo, 2)
            CmdResHist.Parameters("N_REQ").value = n_req
            CmdResHist.Parameters("N_RES").value = IIf(left(codigo, 1) = "$", 1, 2)
            Set regRes = CmdResHist.Execute
        End If
        
        If regRes.EOF = True And n_Req_assoc <> "" Then
            'Volta a tentar para a Requisi��o associada
            regRes.Close
            Set regRes = Nothing
            If Historico_Req_Assoc = False Then
                CmdRes.Parameters("COD_ANA_S").value = Mid(codigo, 2)
                CmdRes.Parameters("N_REQ").value = n_Req_assoc
                CmdRes.Parameters("N_RES").value = IIf(left(codigo, 1) = "$", 1, 2)
                Set regRes = CmdRes.Execute
            Else
                CmdResHist.Parameters("COD_ANA_S").value = Mid(codigo, 2)
                CmdResHist.Parameters("N_REQ").value = n_Req_assoc
                CmdResHist.Parameters("N_RES").value = IIf(left(codigo, 1) = "$", 1, 2)
                Set regRes = CmdResHist.Execute
            End If
        End If
        
        'A an�lise tem Resultado?
        If regRes.EOF = True Then
            Set CmdRes = Nothing
            Set CmdResHist = Nothing
            regRes.Close
            Set regRes = Nothing
            
            'Sai da fun��o
            BL_FormulaDescodifica = ""
            Exit Function
        End If
        
        'Tem Valor=>N�o saiu da fun��o
        valor = "" & regRes!Result
        regRes.Close
        Set regRes = Nothing
                
                
        'Actualiza novamente as vari�veis para o ciclo
        formula = Replace(formula, codigo, valor)
        PosCod = InStr(1, formula, "$")
        If PosCod = 0 Then
            PosCod = InStr(1, formula, "@")
        End If
    Wend
    
    BL_FormulaDescodifica = BL_FormulaResolve(formula)
    Exit Function
erro:
    BL_FormulaDescodifica = ""
        
End Function


Public Sub BL_PesquisaFrases()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "descr_frase"
    CamposEcran(1) = "descr_frase"
    Tamanhos(1) = 4500
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_frase"
    CamposEcran(2) = "cod_frase"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_dicionario"
    CWhere = ""
    CampoPesquisa = "descr_frase"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Consulta de frases")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            'N�o devolve nenhum resultado da form 'Resultados(i)'
            If gF_RESULT = 1 Then
                If FormResultados.EcInfo.locked = False And FormResultados.EcInfo.Height = 6805 And FormResultados.EcInfo.text = "" Then
                    FormResultados.EcInfo.SetFocus
                    FormResultados.EcInfo.TextRTF = Trim(Resultados(1))
                    FormResultados.EcInfo.SelStart = Len(FormResultados.EcInfo.TextRTF)
                End If
            ElseIf gF_RESULT_NOVO = 1 Then
                If FormResultadosNovo.EcObsAna.Visible = True Then
                    FormResultadosNovo.EcObsAna.SetFocus
                    FormResultadosNovo.EcObsAna.text = FormResultadosNovo.EcObsAna.text & Trim(Resultados(1))
                    FormResultadosNovo.EcObsAna.SelStart = Len(FormResultadosNovo.EcObsAna.TextRTF)
                    Sendkeys ("{END}")
                End If
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem frases codificadas!", vbExclamation + vbOKOnly, App.ProductName
    End If
    
End Sub

Function BL_ExecutaQuery_Secundaria(SQLQuery As String, Optional iMensagem) As Integer

    ' O argumento 'iMensagem' serve para que seja apresentada uma mensagem caso a
    ' opera��o que se pretende n�o seja efectuada.
    '
    '   Hip�teses:
    '       <Missing>     -> Aparece uma MsgBox
    '       mediMsgBox    -> Aparece uma MsgBox
    '       mediMsgStatus -> Aparece uma MsgStatus
    '       -1            -> N�o aparece mensagem

    Dim sMsg As String
    Dim iRes As Integer
    Dim registos As Integer
    
    gSQLError = 0
    gSQLISAM = 0
    On Error GoTo TrataErro
    
    gConexaoSecundaria.Execute SQLQuery, registos, adCmdText + adExecuteNoRecords
        
    If (registos <= 0) Then
    
        iRes = -1
        If gModoDebug = 1 Then BG_LogFile_Erros "(BL_ExecutaQuery_Secundaria: RecordsAffected = 0) -> " & SQLQuery
        sMsg = "Esta opera��o pode n�o ter sido efectuada." & vbCrLf & vbCrLf & "Raz�o prov�vel: Integridade dos Dados."
        If IsMissing(iMensagem) Then
            If gModoDebug = 1 Then BG_Mensagem mediMsgBox, sMsg, vbExclamation, "..."
        Else
            If iMensagem = mediMsgBox Then
                If gModoDebug = 1 Then BG_Mensagem mediMsgBox, sMsg, vbExclamation, "..."
            ElseIf iMensagem = mediMsgStatus Then
                If gModoDebug = 1 Then BG_Mensagem mediMsgStatus, sMsg, mediMsgBeep + 10
            ElseIf iMensagem = -1 Then
                ' N�o d� mensagem.
            End If
        End If
    Else
        iRes = registos
        
    End If
    BL_ExecutaQuery_Secundaria = iRes
    Exit Function

TrataErro:
    BG_TrataErro "BL_ExecutaQuery_Secundaria" & vbNewLine & "(" & SQLQuery & ")" & vbNewLine, Err
    If IsMissing(iMensagem) Then
        BG_Trata_BDErro
    Else
        If iMensagem <> -1 Then BG_Trata_BDErro
    End If
    BL_ExecutaQuery_Secundaria = -1
    Resume Next
    Exit Function
End Function

Public Function BL_Fecha_Conexao_Secundaria()

    On Error Resume Next
    gConexaoSecundaria.Close
    
End Function

Public Function BL_Fecha_Conexao_eResults()

    On Error Resume Next
    gConexaoEresults.Close
     
End Function

Public Function BL_Fecha_Conexao_interfaces()

    On Error Resume Next
    gConexaoInterfaces.Close
    
End Function

Public Function BL_Forca_Casas_Decimais(str_in As String, _
                                        casas As Integer) As String

    ' For�a as casas decimais para uma string do tipo 43.0
    
    On Error GoTo ErrorHandler

    Dim cont As Integer
    Dim ret As String
    Dim pos_decimal As Integer
                           
    str_in = Trim(str_in)
    
    If (Len(str_in) = 0) Then
        BL_Forca_Casas_Decimais = ""
        Exit Function
    End If
                       
    If (casas <= 0) Then
        BL_Forca_Casas_Decimais = str_in
        Exit Function
    End If
    
    pos_decimal = InStr(1, str_in, gSimboloDecimal, vbTextCompare)
    If pos_decimal = 0 And gSimboloDecimal = "," Then
        pos_decimal = InStr(1, str_in, ".", vbTextCompare)
    ElseIf pos_decimal = 1 And gSimboloDecimal = "." Then
        pos_decimal = InStr(1, str_in, ",", vbTextCompare)
    End If
    If ((pos_decimal = 0) And _
        (casas > 0)) Then
        ret = str_in & gSimboloDecimal
    Else
        ret = str_in
    End If
    
    ' Caso 1.1 -> 1.100
    ret = ret & "0000000000000000000000000"
    
    pos_decimal = InStr(1, ret, gSimboloDecimal, vbTextCompare)
    If pos_decimal = 0 And gSimboloDecimal = "," Then
        pos_decimal = InStr(1, str_in, ".", vbTextCompare)
    ElseIf pos_decimal = 1 And gSimboloDecimal = "." Then
        pos_decimal = InStr(1, str_in, ",", vbTextCompare)
    End If
    ret = left(ret, pos_decimal + casas)
    
    BL_Forca_Casas_Decimais = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            
            Call BG_LogFile_Erros("BL_Forca_Casas_Decimais (BibliotecaGenerica) " & Err.Number & " -> " & Err.Description)

            BL_Forca_Casas_Decimais = ""
            Exit Function
    End Select
End Function

Public Function BL_PrecoExtenso(ByVal preco) As String
    
    Dim PrecoStr As String
    Dim PosCur, posicao, Grupo As Integer
    Dim i, Digit As Integer

    If preco >= 1000000000 Then
        Exit Function
    End If

    PrecoStr = CStr(preco)
    Grupo = CInt(Len(PrecoStr) / 3 + 0.45)
    PosCur = 1
    
    Do While (Grupo)
        Select Case Grupo
            Case 1
                posicao = Len(PrecoStr)
                BL_PrecoExtenso = BL_PrecoExtenso & BL_PrecoExtenso_Aux(PrecoStr, Grupo, posicao, (PosCur))
            Case 2
                If CInt(Mid(PrecoStr, 1, 3)) <> 0 Then
                    posicao = Len(PrecoStr) - 3
                    BL_PrecoExtenso = BL_PrecoExtenso & BL_PrecoExtenso_Aux(PrecoStr, Grupo, posicao, (PosCur))
                    BL_PrecoExtenso = BL_PrecoExtenso & " MIL"
                    Digit = 0
                    For i = PosCur To Len(PrecoStr)
                        If CInt(Mid(PrecoStr, i, 1)) <> 0 Then
                            Digit = Digit + 1
                        End If
                    Next i
                    Select Case Digit
                        Case 0
                            Grupo = 1
                        Case 1
                            BL_PrecoExtenso = BL_PrecoExtenso & " E"
                        Case 2
                            If CInt(Mid(PrecoStr, PosCur, 1)) = 0 Then
                                BL_PrecoExtenso = BL_PrecoExtenso & " E"
                            End If
                    End Select
                Else
                    PosCur = PosCur + 3
                End If
            Case 3
                posicao = Len(PrecoStr) - 6
                BL_PrecoExtenso = BL_PrecoExtenso & BL_PrecoExtenso_Aux(PrecoStr, Grupo, posicao, (PosCur))
                If Len(PrecoStr) = 7 And CInt(Mid(PrecoStr, 1, 1)) = 1 Then
                    BL_PrecoExtenso = BL_PrecoExtenso & " MILH�O"
                Else
                    BL_PrecoExtenso = BL_PrecoExtenso & " MILH�ES"
                End If
                Digit = 0
                For i = PosCur To Len(PrecoStr)
                        If CInt(Mid(PrecoStr, i, 1)) <> 0 Then
                            Digit = Digit + 1
                        End If
                Next i
                Select Case Digit
                    Case 0
                        Grupo = 1
                        BL_PrecoExtenso = BL_PrecoExtenso & " DE"
                        PosCur = PosCur + 6
                    Case 1
                        BL_PrecoExtenso = BL_PrecoExtenso & " E"
                        PosCur = PosCur + 3
                    Case 2
                        If CInt(Mid(PrecoStr, 4, 3)) = 0 And CInt(Mid(PrecoStr, PosCur, 1)) = 0 Then
                            BL_PrecoExtenso = BL_PrecoExtenso & " E"
                        End If
                   End Select
                   PrecoStr = Right(PrecoStr, 6)
        End Select
        Grupo = Grupo - 1
    Loop
    
    If preco = 0 Then
        BL_PrecoExtenso = " ZERO"
    ElseIf preco = 1 Then
        BL_PrecoExtenso = BL_PrecoExtenso & ""
    Else
        BL_PrecoExtenso = BL_PrecoExtenso & ""
    End If

End Function

Public Function BL_Preco_EURO_Extenso(ByVal valor As Double) As String

    ' Indica o valor por extenso em EUROS e C�NTIMOS.
    ' Arredonda para duas casas decimais.
    
    On Error GoTo ErrorHandler
    
    Dim str_euro As String
    Dim str_centimos As String
    Dim lng_centimos As Long
    Dim lng_euro As Long
    Dim euros As String
    Dim centimos As String
    
    Dim str_out As String
    
    str_out = ""
    
    str_euro = Fix(valor)
    lng_euro = CLng(str_euro)
    If (lng_euro = 1) Then
        euros = "EURO"
    Else
        euros = "EUROS"
    End If
    
    str_centimos = CStr(CInt((valor - CDbl(str_euro)) * 100))
    lng_centimos = CLng(str_centimos)
    If (lng_centimos = 1) Then
        centimos = "C�NTIMO"
    Else
        centimos = "C�NTIMOS"
    End If
    
    If ((lng_euro = 0) And (lng_centimos = 0)) Then
        str_out = "ZERO " & euros
    End If
    
    If ((lng_euro <> 0) And (lng_centimos <> 0)) Then
        str_out = Trim(BL_PrecoExtenso(str_euro)) & " " & euros & " E " & Trim(BL_PrecoExtenso(str_centimos)) & " " & centimos
    End If
    
    If ((lng_euro <> 0) And (lng_centimos = 0)) Then
        str_out = Trim(BL_PrecoExtenso(str_euro)) & " " & euros
    End If
    
    If ((lng_euro = 0) And (lng_centimos <> 0)) Then
        str_out = Trim(BL_PrecoExtenso(str_centimos)) & " " & centimos
    End If
    
    BL_Preco_EURO_Extenso = str_out
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            
            Call BG_LogFile_Erros("BL_Preco_EURO_Extenso (BibliotecaGenerica) " & Err.Number & " - " & Err.Description)

            BL_Preco_EURO_Extenso = ""
            Exit Function
    End Select
End Function

Public Function BL_PrecoExtenso_Aux(ByVal PrecoStr As String, ByVal Grupo As Integer, ByVal posicao As Integer, ByRef PosCur As Integer) As String
    
    Dim Palavras(9, 3), PalavrasUnit(9) As String
    Dim CompPreco As Integer
    
    Palavras(1, 1) = " UM"
    Palavras(2, 1) = " DOIS"
    Palavras(3, 1) = " TR�S"
    Palavras(4, 1) = " QUATRO"
    Palavras(5, 1) = " CINCO"
    Palavras(6, 1) = " SEIS"
    Palavras(7, 1) = " SETE"
    Palavras(8, 1) = " OITO"
    Palavras(9, 1) = " NOVE"
    
    Palavras(1, 2) = " DEZ"
    Palavras(2, 2) = " VINTE"
    Palavras(3, 2) = " TRINTA"
    Palavras(4, 2) = " QUARENTA"
    Palavras(5, 2) = " CINQUENTA"
    Palavras(6, 2) = " SESSENTA"
    Palavras(7, 2) = " SETENTA"
    Palavras(8, 2) = " OITENTA"
    Palavras(9, 2) = " NOVENTA"
    
    Palavras(1, 3) = " CENTO"
    Palavras(2, 3) = " DUZENTOS"
    Palavras(3, 3) = " TREZENTOS"
    Palavras(4, 3) = " QUATROCENTOS"
    Palavras(5, 3) = " QUINHENTOS"
    Palavras(6, 3) = " SEISCENTOS"
    Palavras(7, 3) = " SETECENTOS"
    Palavras(8, 3) = " OITOCENTOS"
    Palavras(9, 3) = " NOVECENTOS"
    
    PalavrasUnit(1) = " ONZE"
    PalavrasUnit(2) = " DOZE"
    PalavrasUnit(3) = " TREZE"
    PalavrasUnit(4) = " CATORZE"
    PalavrasUnit(5) = " QUINZE"
    PalavrasUnit(6) = " DEZASSEIS"
    PalavrasUnit(7) = " DEZASSETE"
    PalavrasUnit(8) = " DEZOITO"
    PalavrasUnit(9) = " DEZANOVE"
    
    CompPreco = Len(PrecoStr)
    PosCur = 1
    
    Do While (posicao)
        Select Case posicao
            Case 1
                If Not (CompPreco = 4 And Grupo = 2 And InStr(PrecoStr, "1") = 1) Then
                    BL_PrecoExtenso_Aux = BL_PrecoExtenso_Aux & Palavras(CInt(Mid(PrecoStr, PosCur, 1)), 1)
                End If
            Case 2
                If CInt(Mid(PrecoStr, PosCur, 1)) <> 0 Then
                    If CInt(Mid(PrecoStr, PosCur + 1, 1)) = 0 Then
                        BL_PrecoExtenso_Aux = BL_PrecoExtenso_Aux & Palavras(CInt(Mid(PrecoStr, PosCur, 1)), 2)
                        posicao = 1
                        PosCur = PosCur + 1
                    ElseIf CInt(Mid(PrecoStr, PosCur, 1)) = 1 Then
                        PosCur = PosCur + 1
                        BL_PrecoExtenso_Aux = BL_PrecoExtenso_Aux & PalavrasUnit(CInt(Mid(PrecoStr, PosCur, 1)))
                        posicao = 1
                    Else
                        BL_PrecoExtenso_Aux = BL_PrecoExtenso_Aux & Palavras(CInt(Mid(PrecoStr, PosCur, 1)), 2)
                        BL_PrecoExtenso_Aux = BL_PrecoExtenso_Aux & " E"
                    End If
                End If
            Case 3
                If CInt(Mid(PrecoStr, PosCur, 1)) <> 0 Then
                    If CInt(Mid(PrecoStr, PosCur + 1, 2)) = 0 Then
                        If CInt(Mid(PrecoStr, PosCur, 1)) = 1 Then
                            BL_PrecoExtenso_Aux = BL_PrecoExtenso_Aux & " CEM"
                        Else
                            BL_PrecoExtenso_Aux = BL_PrecoExtenso_Aux & Palavras(CInt(Mid(PrecoStr, PosCur, 1)), 3)
                            posicao = 1
                            PosCur = PosCur + 2
                        End If
                    Else
                        BL_PrecoExtenso_Aux = BL_PrecoExtenso_Aux & Palavras(CInt(Mid(PrecoStr, PosCur, 1)), 3)
                        BL_PrecoExtenso_Aux = BL_PrecoExtenso_Aux & " E"
                    End If
                End If
        End Select
        posicao = posicao - 1
        PosCur = PosCur + 1
    Loop
End Function

Public Function BL_Decimal_Windows() As String
    
    Dim sDecimalDoWindows As String
    Dim StrTemp As String
    Dim NumTemp As Single
    
    ' In�cio: Obter Simbolo Decimal do Windows
    StrTemp = "2.5"
    NumTemp = CSng(StrTemp)
    If NumTemp = 25 Then
        sDecimalDoWindows = ","
    Else
        sDecimalDoWindows = "."
    End If
    ' Fim: Obter Simbolo Decimal do Windows

    BL_Decimal_Windows = sDecimalDoWindows
    
End Function

' Retira o ENTER (/n) de uma string.
Public Function BL_Retira_ENTER(str_in As String) As String

    On Error GoTo ErrorHandler
    
    Dim str_out As String
    Dim c As String
    Dim i As Integer
    
    str_out = ""
    For i = 1 To Len(str_in)
    
        c = Mid(str_in, i, 1)
        Select Case Asc(c)
            Case 13, 10
            Case Else
            str_out = str_out & c
        End Select
    
    Next
    BL_Retira_ENTER = str_out

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("BL_Retira_ENTER (BibliotecaLocal) -> (" & Err.Number & ") " & Err.Description)
            BL_Retira_ENTER = ""
            Exit Function
    End Select
End Function

Function BL_Insert_QuebraPagina(Arg_seqrealiza As Long, Arg_grupo As String, ByVal Arg_subgrupo As String, Arg_perfil As String, Arg_complexa As String, Arg_OrdemImpressao As Integer, ByVal DtResAnt1 As String, ByVal DtResAnt2 As String, ByVal DtResAnt3 As String) As Boolean
    
    Dim sql As String
    
'    If UCase(Arg_subgrupo) <> "NULL" Then 'Colocar o subgrupo <> do anterior Para no report repetir o subgrupo na pagina seguinte
'        Arg_subgrupo = Mid(Arg_subgrupo, 1, Len(Arg_subgrupo) - 1) & "1'"
'    End If
    
    sql = " INSERT INTO SL_CR_LR " & _
        " (" & _
        "n_resultado,data," & _
        "grupo,subgrupo,perfil,complexa,simples," & _
        "resnum1unid1,resnum1unid2,Unid1ResNum1, Unid2ResNum1,resalf1,tamresalf1,valref1,zonaref1," & _
        "resnum2unid1,resnum2unid2,Unid1ResNum2, Unid2ResNum2,resalf2,tamresalf2,valref2,zonaref2," & _
        "observanalise,observresultado,comautomatico," & _
        "flagresalf2,flagobserv,flagcomaut,flagresult,flagresanormal1,flagresanormal2,flagcutoff,flagRTFanalise," & _
        "id_empresa, res_ant1, dt_res_ant1, res_ant2, dt_res_ant2, res_ant3, dt_res_ant3, classe, ordem_imp, nome_computador, num_sessao " & _
        ") " & _
        "VALUES " & _
        "( " & _
        Arg_seqrealiza & ",null," & Arg_grupo & ", " & _
        Arg_subgrupo & ", '//NEW_PAGE', '//NEW_PAGE', 'NEW_PAGE'," & _
        "null, null, null, null, null, '0' , null, null, " & _
        "null, null, null, null, null, '0', null, null, " & _
        "null, null, null, " & _
        "null, null, null, null, null, null, null, null, " & _
        "Null, " & _
        "null," & BL_TrataStringParaBD(DtResAnt1) & "," & _
        "null," & BL_TrataStringParaBD(DtResAnt2) & "," & _
        "null," & BL_TrataStringParaBD(DtResAnt3) & "," & _
        "null, " & Arg_OrdemImpressao & ",'" & gComputador & "'," & gNumeroSessao & " )"
    
        BG_ExecutaQuery_ADO sql
        
End Function


Function BL_ContaMembrosPerfil(ByVal seqRealiza As Long, ByVal NReq As Long, ByVal Grupo As String, ByVal subGrupo As String, ByVal Perfil As String, ByVal complexa As String, ByRef UltimoPerfil As String, ByRef ContaMembrosPerfil As Integer, ByRef RsContaMembros As ADODB.recordset, ByRef OrdemImpressao As Integer, ByRef LinhasPagina As Integer, CodPerfil As String, ByRef UltimoGrupo As String, ByRef UltimoSubgrupo As String, ByVal DtResAnt1 As String, ByVal DtResAnt2 As String, ByVal DtResAnt3 As String)

    Dim sql As String

    sql = "SELECT count(*) conta FROM sl_realiza WHERE n_req=" & NReq & " AND cod_perfil=" & BL_TrataStringParaBD(CodPerfil)
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsContaMembros.Open sql, gConexao
    If RsContaMembros.RecordCount > 0 Then
        ContaMembrosPerfil = BL_HandleNull(RsContaMembros!conta, 0)
    Else
        ContaMembrosPerfil = -1
    End If
    RsContaMembros.Close
    UltimoPerfil = Perfil

    If ContaMembrosPerfil >= LinhasPagina Or LinhasPagina <= 0 Then
        OrdemImpressao = OrdemImpressao + 1
        If UltimoGrupo = Grupo And UltimoSubgrupo = subGrupo Then
            BL_Insert_QuebraPagina seqRealiza, Grupo, subGrupo, Perfil, complexa, OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
        ElseIf UltimoGrupo <> Grupo And UltimoSubgrupo <> subGrupo Then
            BL_Insert_QuebraPagina seqRealiza, "''", "''", Perfil, complexa, OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
        ElseIf UltimoGrupo = Grupo And UltimoSubgrupo <> subGrupo Then
            BL_Insert_QuebraPagina seqRealiza, Grupo, "''", Perfil, complexa, OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
        ElseIf UltimoGrupo <> Grupo And UltimoSubgrupo = subGrupo Then
            BL_Insert_QuebraPagina seqRealiza, "''", subGrupo, Perfil, complexa, OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
        End If
        BL_Insert_QuebraPagina seqRealiza, Grupo, subGrupo, Perfil, complexa, OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
        LinhasPagina = gLinhasPagina
        'Por causa da repeti��o do grupo e subgrupo qd se muda de p�gina
        'UltimoGrupo = ""
        'UltimoSubgrupo = ""
    End If

End Function

Function BL_ContaMembrosComplexa(ByVal seqRealiza As Long, ByVal NReq As Long, ByVal Grupo As String, ByVal subGrupo As String, ByVal Perfil As String, ByVal complexa As String, ByRef UltimaComplexa As String, ByRef ContaMembrosComplexa As Integer, ByRef RsContaMembros As ADODB.recordset, ByRef OrdemImpressao As Integer, ByRef LinhasPagina As Integer, CodComplexa As String, ByRef UltimoGrupo As String, ByRef UltimoSubgrupo As String, ByVal DtResAnt1 As String, ByVal DtResAnt2 As String, ByVal DtResAnt3 As String)

    Dim sql As String

    'Verifica se a complexa � do tipo GhostMember; se for nao conta os membros da analise mas os membros marcados(em sl_realiza)
    sql = "SELECT flg_marc_memb FROM sl_ana_c WHERE cod_ana_c=" & BL_TrataStringParaBD(CodComplexa)
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsContaMembros.Open sql, gConexao
    If RsContaMembros.RecordCount > 0 Then
        If BL_HandleNull(RsContaMembros!flg_marc_memb, -1) = 0 Then
            RsContaMembros.Close
            sql = "SELECT count(*) conta FROM sl_realiza WHERE n_req=" & NReq & " AND cod_ana_c=" & BL_TrataStringParaBD(CodComplexa)
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            RsContaMembros.Open sql, gConexao
            If RsContaMembros.RecordCount > 0 Then
                ContaMembrosComplexa = BL_HandleNull(RsContaMembros!conta, 0)
            Else
                ContaMembrosComplexa = -1
            End If
        Else
            RsContaMembros.Close
            sql = "SELECT count(*) conta FROM sl_membro WHERE cod_ana_c=" & BL_TrataStringParaBD(CodComplexa)
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            RsContaMembros.Open sql, gConexao
            If RsContaMembros.RecordCount > 0 Then
                ContaMembrosComplexa = BL_HandleNull(RsContaMembros!conta, 0)
            Else
                ContaMembrosComplexa = -1
            End If
        End If
    Else
        sql = "SELECT count(*) conta FROM sl_membro WHERE cod_ana_c=" & BL_TrataStringParaBD(CodComplexa)
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsContaMembros.Open sql, gConexao
        If RsContaMembros.RecordCount > 0 Then
            ContaMembrosComplexa = BL_HandleNull(RsContaMembros!conta, 0)
        Else
            ContaMembrosComplexa = -1
        End If
    End If
    RsContaMembros.Close
    UltimaComplexa = complexa

    If ContaMembrosComplexa >= LinhasPagina Or LinhasPagina <= 0 Then
        OrdemImpressao = OrdemImpressao + 1
        If UltimoGrupo = Grupo And UltimoSubgrupo = subGrupo Then
            BL_Insert_QuebraPagina seqRealiza, Grupo, subGrupo, Perfil, complexa, OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
        ElseIf UltimoGrupo <> Grupo And UltimoSubgrupo <> subGrupo Then
            BL_Insert_QuebraPagina seqRealiza, "'_'", "''", Perfil, complexa, OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
        ElseIf UltimoGrupo = Grupo And UltimoSubgrupo <> subGrupo Then
            BL_Insert_QuebraPagina seqRealiza, Grupo, "''", Perfil, complexa, OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
        ElseIf UltimoGrupo <> Grupo And UltimoSubgrupo = subGrupo Then
            BL_Insert_QuebraPagina seqRealiza, "'_'", subGrupo, Perfil, complexa, OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
        End If
        LinhasPagina = gLinhasPagina
        'Por causa da repeti��o do grupo e subgrupo qd se muda de p�gina
        'UltimoGrupo = ""
        'UltimoSubgrupo = ""
    End If
    
End Function

Function BL_InsereZonasReferencia(Arg_seqrealiza As Long, Arg_grupo As String, ByVal Arg_subgrupo As String, ArgClasse As String, Arg_perfil As String, Arg_complexa As String, Arg_Simples As String, ByRef Arg_OrdemImpressao As Integer, ByRef LinhasPagina As Integer, ByRef Arg_EstrutZonas() As String, TotalEstrutZonas As Integer, ByVal DtResAnt1 As String, ByVal DtResAnt2 As String, ByVal DtResAnt3 As String) As Boolean

    Dim sql As String
    Dim l As Integer
    
    If TotalEstrutZonas > 0 Then 'ZONAS REF 1
        For l = 1 To TotalEstrutZonas
            LinhasPagina = LinhasPagina - 1
            Arg_OrdemImpressao = Arg_OrdemImpressao + 1
            sql = " INSERT INTO SL_CR_LR " & _
                " (" & _
                "n_resultado,data," & _
                "grupo,subgrupo,perfil,complexa,simples,classe,ordem_imp,zonaref1,tamresalf1,tamresalf2, nome_computador, num_sessao) VALUES (" & _
                Arg_seqrealiza & ", '', " & _
                Arg_grupo & "," & Arg_subgrupo & "," & Arg_perfil & "," & Arg_complexa & "," & Arg_Simples & "," & _
                ArgClasse & "," & Arg_OrdemImpressao & ", " & BL_TrataStringParaBD(Arg_EstrutZonas(l)) & ", 0, 0, '" & gComputador & "'," & gNumeroSessao & " )"
                      
            BG_ExecutaQuery_ADO sql
            
            If LinhasPagina <= 0 Then
                BL_Insert_QuebraPagina Arg_seqrealiza, Arg_grupo, Arg_subgrupo, Arg_perfil, Arg_complexa, Arg_OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
                LinhasPagina = gLinhasPagina
            End If
        Next l
    End If
    
End Function

Function BL_InsereObsAnalise(Arg_seqrealiza As Long, Arg_grupo As String, ByVal Arg_subgrupo As String, Arg_Classe As String, Arg_perfil As String, Arg_complexa As String, Arg_Simples As String, ByRef Arg_OrdemImpressao As Integer, ByRef LinhasPagina As Integer, ByRef Arg_EstrutObsAna() As String, TotalEstrutObsAna As Integer, ByVal DtResAnt1 As String, ByVal DtResAnt2 As String, ByVal DtResAnt3 As String) As Boolean

    Dim sql As String
    Dim l As Integer

    If TotalEstrutObsAna > 0 Then
        For l = 1 To TotalEstrutObsAna
            LinhasPagina = LinhasPagina - 1
            Arg_OrdemImpressao = Arg_OrdemImpressao + 1
            sql = " INSERT INTO SL_CR_LR " & _
                " (" & _
                "n_resultado,data," & _
                "grupo,subgrupo,perfil,complexa,simples,classe,ordem_imp,flagobserv,observanalise,dt_res_ant1, dt_res_ant2, dt_res_ant3, nome_computador, num_sessao) VALUES (" & _
                Arg_seqrealiza & ", ''," & _
                Arg_grupo & "," & Arg_subgrupo & "," & Arg_perfil & "," & Arg_complexa & "," & Arg_Simples & "," & _
                Arg_Classe & "," & Arg_OrdemImpressao & ", 'S', " & BL_TrataStringParaBD(Arg_EstrutObsAna(l)) & ", " & BL_TrataStringParaBD(DtResAnt1) & ", " & _
                BL_TrataStringParaBD(DtResAnt2) & "," & BL_TrataStringParaBD(DtResAnt3) & ",'" & gComputador & "'," & gNumeroSessao & ")"
                      
            BG_ExecutaQuery_ADO sql
            
             If LinhasPagina <= 0 Then
                BL_Insert_QuebraPagina Arg_seqrealiza, Arg_grupo, Arg_subgrupo, Arg_perfil, Arg_complexa, Arg_OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
                LinhasPagina = gLinhasPagina
            End If
        Next l
    End If
    
End Function

Function BL_InsereMicro(Arg_seqrealiza As Long, Arg_grupo As String, ByVal Arg_subgrupo As String, ArgClasse As String, Arg_perfil As String, Arg_complexa As String, Arg_Simples As String, ByRef Arg_OrdemImpressao As Integer, ByRef LinhasPagina As Integer, ByRef Arg_EstrutMicro() As String, TotalEstrutMicro As Integer, ByVal DtResAnt1 As String, ByVal DtResAnt2 As String, ByVal DtResAnt3 As String) As Boolean

    Dim sql As String
    Dim l As Integer
    
    If TotalEstrutMicro > 0 Then 'Micro resalf 1
        For l = 1 To TotalEstrutMicro
            LinhasPagina = LinhasPagina - 1
            Arg_OrdemImpressao = Arg_OrdemImpressao + 1
            sql = " INSERT INTO SL_CR_LR " & _
                " (" & _
                "n_resultado,data," & _
                "grupo,subgrupo,perfil,complexa,simples,classe,ordem_imp,resalf1, zonaref1,tamresalf1,tamresalf2,nome_computador, num_sessao) VALUES (" & _
                Arg_seqrealiza & ", '', " & _
                Arg_grupo & "," & Arg_subgrupo & "," & Arg_perfil & "," & Arg_complexa & "," & Arg_Simples & "," & _
                ArgClasse & "," & Arg_OrdemImpressao & ", " & BL_TrataStringParaBD(Arg_EstrutMicro(l)) & ",'', 0, 0,'" & gComputador & "'," & gNumeroSessao & " )"
                      
            BG_ExecutaQuery_ADO sql
            
            If LinhasPagina <= 0 Then
                BL_Insert_QuebraPagina Arg_seqrealiza, Arg_grupo, Arg_subgrupo, Arg_perfil, Arg_complexa, Arg_OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
                LinhasPagina = gLinhasPagina
            End If
        Next l
    End If
    
End Function


Function BL_Insert_LinhaBranco(Arg_seqrealiza As Long, Arg_grupo As String, ByVal Arg_subgrupo As String, Arg_perfil As String, Arg_complexa As String, Arg_Simples As String, ByRef Arg_OrdemImpressao As Integer, ByVal DtResAnt1 As String, ByVal DtResAnt2 As String, ByVal DtResAnt3 As String, ByRef LinhasPagina As Integer, Optional analises As Boolean = False) As Boolean

    Dim sql As String
    
    If analises = False Then
        LinhasPagina = LinhasPagina - 1
        sql = " INSERT INTO SL_CR_LR " & _
            " (" & _
            "n_resultado,data," & _
            "grupo,subgrupo,perfil,complexa,simples," & _
            "resnum1unid1,resnum1unid2,Unid1ResNum1, Unid2ResNum1,resalf1,tamresalf1,valref1,zonaref1," & _
            "resnum2unid1,resnum2unid2,Unid1ResNum2, Unid2ResNum2,resalf2,tamresalf2,valref2,zonaref2," & _
            "observanalise,observresultado,comautomatico," & _
            "flagresalf2,flagobserv,flagcomaut,flagresult,flagresanormal1,flagresanormal2,flagcutoff,flagRTFanalise," & _
            "id_empresa, res_ant1, dt_res_ant1, res_ant2, dt_res_ant2, res_ant3, dt_res_ant3, classe, ordem_imp, nome_computador, num_sessao " & _
            ") " & _
            "VALUES " & _
            "( " & _
            Arg_seqrealiza & ",null, 'NEW_LINE', " & _
            "'NEW_LINE', 'NEW_LINE', 'NEW_LINE', 'NEW_LINE'," & _
            "null, null, null, null, null, '0' , null, null, " & _
            "null, null, null, null, null, '0', null, null, " & _
            "null, null, null, " & _
            "null, null, null, null, null, null, null, null, " & _
            "Null, " & _
            "null," & BL_TrataStringParaBD(DtResAnt1) & "," & _
            "null," & BL_TrataStringParaBD(DtResAnt2) & "," & _
            "null," & BL_TrataStringParaBD(DtResAnt3) & "," & _
            "null, " & Arg_OrdemImpressao & ",'" & gComputador & "'," & gNumeroSessao & " )"
            
            BG_ExecutaQuery_ADO sql
    Else
        Dim RsLinhas As ADODB.recordset
        Dim NumLinhas As Integer
        Dim l As Integer
        
        sql = "SELECT linhas_branco_imp FROM sl_ana_s WHERE cod_ana_s='" & Arg_Simples & "' AND linhas_branco_imp IS NOT NULL AND linhas_branco_imp<>0"
        Set RsLinhas = New ADODB.recordset
        RsLinhas.CursorLocation = adUseServer
        RsLinhas.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsLinhas.Open sql, gConexao
        If RsLinhas.RecordCount > 0 Then
            NumLinhas = BL_HandleNull(RsLinhas!linhas_branco_imp, 0)
            l = 1
            While l <= NumLinhas And LinhasPagina > 0
                Arg_OrdemImpressao = Arg_OrdemImpressao + 1
                
                sql = " INSERT INTO SL_CR_LR " & _
                    " (" & _
                    "n_resultado,data," & _
                    "grupo,subgrupo,perfil,complexa,simples," & _
                    "resnum1unid1,resnum1unid2,Unid1ResNum1, Unid2ResNum1,resalf1,tamresalf1,valref1,zonaref1," & _
                    "resnum2unid1,resnum2unid2,Unid1ResNum2, Unid2ResNum2,resalf2,tamresalf2,valref2,zonaref2," & _
                    "observanalise,observresultado,comautomatico," & _
                    "flagresalf2,flagobserv,flagcomaut,flagresult,flagresanormal1,flagresanormal2,flagcutoff,flagRTFanalise," & _
                    "id_empresa, res_ant1, dt_res_ant1, res_ant2, dt_res_ant2, res_ant3, dt_res_ant3, classe, ordem_imp, nome_computador, num_sessao " & _
                    ") " & _
                    "VALUES " & _
                    "( " & _
                    Arg_seqrealiza & ",null," & Arg_grupo & ", " & _
                    Arg_subgrupo & ", " & Arg_perfil & ", " & Arg_complexa & ", 'NEW_LINE'," & _
                    "null, null, null, null, null, '0' , null, null, " & _
                    "null, null, null, null, null, '0', null, null, " & _
                    "null, null, null, " & _
                    "null, null, null, null, null, null, null, null, " & _
                    "Null, " & _
                    "null," & BL_TrataStringParaBD(DtResAnt1) & "," & _
                    "null," & BL_TrataStringParaBD(DtResAnt2) & "," & _
                    "null," & BL_TrataStringParaBD(DtResAnt3) & "," & _
                    "null, " & Arg_OrdemImpressao & ",'" & gComputador & "'," & gNumeroSessao & " )"
                
                BG_ExecutaQuery_ADO sql
                LinhasPagina = LinhasPagina - 1
                l = l + 1
            Wend
            If LinhasPagina <= 0 Then
                BL_Insert_QuebraPagina Arg_seqrealiza, Arg_grupo, Arg_subgrupo, Arg_perfil, Arg_complexa, Arg_OrdemImpressao, DtResAnt1, DtResAnt2, DtResAnt3
                LinhasPagina = gLinhasPagina
            End If
        End If
    End If

End Function

Function BL_ActualizaEstruturaObsAna(ByRef ObservAna As String, ByRef ObservResultado As String, ByRef TotalEstrutObsResultado, ByRef EstrutObsResultado() As String, _
                            ByRef TotalEstrutObsAnalise, ByRef EstrutObsAnalise() As String) As Boolean

    Dim l As Integer
    Dim VecSplit() As String

    If ObservResultado <> "" Then
        If gModeloMapaRes = 3 Then
            ReDim VecSplit(0)
            VecSplit = Split(ObservResultado, vbCrLf)
            For l = 0 To UBound(VecSplit) - 1
                TotalEstrutObsResultado = TotalEstrutObsResultado + 1
                ReDim Preserve EstrutObsResultado(TotalEstrutObsResultado)
                EstrutObsResultado(TotalEstrutObsResultado) = VecSplit(l)
            Next l
            ObservResultado = ""
        End If
    End If
    
    If ObservAna <> "" Then
        If gModeloMapaRes = 3 Then
            ReDim VecSplit(0)
            VecSplit = Split(ObservAna, vbCrLf)
            For l = 0 To UBound(VecSplit) - 1
                TotalEstrutObsAnalise = TotalEstrutObsAnalise + 1
                ReDim Preserve EstrutObsAnalise(TotalEstrutObsAnalise)
                EstrutObsAnalise(TotalEstrutObsAnalise) = VecSplit(l)
            Next l
            ObservAna = ""
        End If
    End If
    
End Function

Public Function BL_Upper_Campo(sql As String, _
                               CampoUpper As String, _
                               pesq_dentro_campo As Boolean) As String

    ' Numa string SQL
    ' SELECT * FROM B WHERE C = 'D'
    ' Troca C = 'D' por
    '   UPPER(C) = LIKE('D') ou
    '   UPPER(C) = LIKE('%D%')
    ' Conforme pesq_dentro_campo False ou True.
    
    ' Nota : Cuidado com o formato de "C = D". N�o pode ser "C=D" por exemplo.
    
    On Error GoTo ErrorHandler
    
    Dim i As Integer
    Dim aux1 As String
    Dim aux2 As String
    Dim valor_campo As String
    Dim saida As String
        
    CampoUpper = Trim(CampoUpper)
        
    saida = ""
    
    If (InStr(1, UCase(sql), CampoUpper & " LIKE '", vbTextCompare) <> 0) Then
    
        ' CampoUpper LIKE(...
    
        i = InStr(1, UCase(sql), CampoUpper & " LIKE '", vbTextCompare)
        If i = 0 Then
            BL_Upper_Campo = sql
            Exit Function
        End If
        
        aux1 = Mid(sql, i, Len(sql))
        
        i = InStr(1, aux1, "'", vbTextCompare)
        If i = 0 Then
            BL_Upper_Campo = sql
            Exit Function
        End If
    
        aux2 = Mid(aux1, i + 1, Len(sql))
    
        i = InStr(1, aux2, "'", vbTextCompare)
        If i = 0 Then
            BL_Upper_Campo = sql
            Exit Function
        End If
    
        valor_campo = Mid(aux2, 1, i - 1)
        
        If (pesq_dentro_campo) Then
            saida = Replace(sql, CampoUpper & " LIKE '" & valor_campo & "'", _
                                "UPPER(" & CampoUpper & ") LIKE('%" & UCase(valor_campo) & "%')")
        Else
            saida = Replace(sql, CampoUpper & " LIKE '" & valor_campo & "'", _
                                "UPPER(" & CampoUpper & ") LIKE('" & UCase(valor_campo) & "')")
        End If
    
    Else
    
        ' CampoUpper = ...
    
        i = InStr(1, UCase(sql), CampoUpper & " = '", vbTextCompare)
        If i = 0 Then
            BL_Upper_Campo = sql
            Exit Function
        End If
        
        aux1 = Mid(sql, i, Len(sql))
        
        i = InStr(1, aux1, "'", vbTextCompare)
        If i = 0 Then
            BL_Upper_Campo = sql
            Exit Function
        End If
    
        aux2 = Mid(aux1, i + 1, Len(sql))
    
        i = InStr(1, aux2, "'", vbTextCompare)
        If i = 0 Then
            BL_Upper_Campo = sql
            Exit Function
        End If
    
        valor_campo = Mid(aux2, 1, i - 1)
        
        If (pesq_dentro_campo) Then
            saida = Replace(sql, CampoUpper & " = '" & valor_campo & "'", _
                                "UPPER(" & CampoUpper & ") LIKE('%" & UCase(valor_campo) & "%')")
        Else
            saida = Replace(sql, CampoUpper & " = '" & valor_campo & "'", _
                                "UPPER(" & CampoUpper & ") LIKE('" & UCase(valor_campo) & "')")
        End If
    
    End If
    
    BL_Upper_Campo = saida

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : BL_Upper_Campo () -> " & Err.Description)
            BL_Upper_Campo = sql
            Exit Function
    End Select
End Function


Function BL_Da_Idade(str_dia As String, _
                     str_mes As String, _
                     str_ano As String) As String

    On Error GoTo ErrorHandler
    
    Dim ret As String
    Dim d As Date
    Dim dif As Integer
    
    Dim dia1 As Integer
    Dim mes1 As Integer
    Dim ano1 As Integer
    
    Dim dia2 As Integer
    Dim mes2 As Integer
    Dim ano2 As Integer
    
    str_dia = Trim(str_dia)
    str_mes = Trim(str_mes)
    str_ano = Trim(str_ano)
    
    If (Len(str_dia) = 0) Or _
       (Len(str_mes) = 0) Or _
       (Len(str_ano) = 0) Then
        BL_Da_Idade = ""
       Exit Function
    End If
    
    dia1 = CInt(str_dia)
    mes1 = CInt(str_mes)
    ano1 = CInt(str_ano)
    
    d = DateSerial(CInt(ano1), CInt(mes1), CInt(dia1))

    ' Contagem de anos
    dif = DateDiff("yyyy", d, Date)

    If ((Date) < (DateSerial(Year(Date), mes1, dia1))) Then
        dif = dif - 1
    End If

    BL_Da_Idade = CStr(dif)
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("BL_Da_Idade (BibliotecaLocal) -> " & Err.Number & " : " & Err.Description)
            BL_Da_Idade = ""
            Exit Function
    End Select
End Function



Function BL_SeleccionaDescrPostal(codPostal As String) As String

    Dim sql As String
    Dim RsDescrPostal As ADODB.recordset
    Dim cod

    BL_SeleccionaDescrPostal = ""

    sql = "SELECT " & _
              "     descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal=" & BL_TrataStringParaBD(codPostal)
        
    Set RsDescrPostal = New ADODB.recordset
    RsDescrPostal.CursorLocation = adUseServer
    RsDescrPostal.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsDescrPostal.Open sql, gConexao
    If RsDescrPostal.RecordCount > 0 Then
        BL_SeleccionaDescrPostal = BL_HandleNull(RsDescrPostal!descr_postal, "")
    Else
        BL_SeleccionaDescrPostal = ""
    End If
    RsDescrPostal.Close
    Set RsDescrPostal = Nothing
    
End Function


Function BL_SeleccionaDescrImp(CodProv As String, TipoContacto As String) As String

    Dim sql As String
    Dim RsImp As ADODB.recordset

    BL_SeleccionaDescrImp = ""

    sql = "SELECT sl_contac_prov.conteudo_cont " & _
        "FROM sl_contac_prov " & _
        "WHERE sl_contac_prov.cod_proven=" & BL_TrataStringParaBD(Trim(CodProv)) & " " & _
        "AND sl_contac_prov.t_contac=" & TipoContacto
    Set RsImp = New ADODB.recordset
    RsImp.CursorLocation = adUseServer
    RsImp.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsImp.Open sql, gConexao
    If RsImp.RecordCount <> 0 Then
        BL_SeleccionaDescrImp = "" & BL_HandleNull(RsImp!conteudo_cont, "")
    End If
    RsImp.Close
    Set RsImp = Nothing
    
End Function

Function BL_SeleccionaDescrProduto(NReq As String, Optional cod_produto As String) As String

    Dim sql As String
    Dim Produtos As String
    Dim RsProd As ADODB.recordset
    Dim totalProd As Integer
    Dim i As Integer

    sql = "SELECT sl_produto.cod_produto,sl_produto.descr_produto,sl_especif.descr_especif, sl_req_prod.obs_especif,sl_especif.flg_nao_impr "
    sql = sql & " FROM sl_produto,sl_req_prod LEFT OUTER JOIN sl_especif ON sl_req_prod.cod_especif = sl_especif.cod_especif "
    sql = sql & " WHERE sl_req_prod.n_req=" & Trim(NReq) & " AND sl_produto.cod_produto=sl_req_prod.cod_prod "
    If cod_produto <> "" Then
        sql = sql & " AND sl_Req_prod.cod_prod = " & BL_TrataStringParaBD(cod_produto)
    End If
    Set RsProd = New ADODB.recordset
    RsProd.CursorLocation = adUseServer
    RsProd.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsProd.Open sql, gConexao
    totalProd = RsProd.RecordCount
    Produtos = ""
    For i = 1 To totalProd
        If BL_HandleNull(RsProd!flg_nao_impr, "0") = 1 Then
            Produtos = Produtos & RsProd!descr_produto & " " & RsProd!obs_especif & ", "
        Else
            Produtos = Produtos & RsProd!descr_produto & " " & RsProd!descr_especif & " " & RsProd!obs_especif & ", "
        End If
        RsProd.MoveNext
    Next i
    'Remove a virgula do final da constante
    If Len(Produtos) <> 0 Then
        Produtos = left(Produtos, Len(Produtos) - 2)
    End If
    RsProd.Close
    Set RsProd = Nothing
    
    BL_SeleccionaDescrProduto = Produtos
    
End Function

Function BL_SeleccionaComentarioFinal(NReq As String) As String

    Dim sql As String
    Dim RsComFinal As ADODB.recordset
    Dim Comentario As String

    Comentario = ""
    sql = "SELECT descr_cm_fin FROM sl_cm_fin WHERE n_req=" & Trim(NReq)
    Set RsComFinal = New ADODB.recordset
    RsComFinal.CursorLocation = adUseServer
    RsComFinal.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsComFinal.Open sql, gConexao
    If RsComFinal.RecordCount > 0 Then
        Comentario = "" & BL_HandleNull(RsComFinal!descr_cm_fin, "")
    End If
    RsComFinal.Close
    Set RsComFinal = Nothing

    BL_SeleccionaComentarioFinal = Comentario

End Function

Sub BL_MudaEstadoAnalise(FlgEstado As String, seqRealiza As String, codAgrup As String, NumReq As String)

    
    If FlgEstado = "3" Or FlgEstado = "4" Then
        Call BG_ExecutaQuery_ADO("UPDATE sl_realiza SET flg_estado='4' WHERE seq_realiza=" & BL_TrataStringParaBD(seqRealiza))
        'Ghost Members (S9999)
        Call BG_ExecutaQuery_ADO("UPDATE sl_realiza SET flg_estado='4' WHERE flg_estado='3' AND cod_agrup=" & BL_TrataStringParaBD(codAgrup) & " AND n_req = " & BL_TrataStringParaBD(Trim(NumReq)) & " AND cod_ana_s = " & BL_TrataStringParaBD(gGHOSTMEMBER_S))
    End If

End Sub

Function BL_SeleccionaMetodo(CodMetodo As String, ByRef caracMetodo As String) As String
    Dim rsMetodo As ADODB.recordset
    
    On Error GoTo TrataErro
    Set rsMetodo = New ADODB.recordset
    With rsMetodo
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .Source = "select descr_metodo, carac_metodo from sl_metodo where cod_metodo = " & BL_TrataStringParaBD(CodMetodo)
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With
    If Not rsMetodo.EOF Then
        BL_SeleccionaMetodo = BL_HandleNull(rsMetodo!descr_metodo, "")
        caracMetodo = BL_HandleNull(rsMetodo!carac_metodo, "")
    Else
        BL_SeleccionaMetodo = ""
        caracMetodo = ""
    End If
    
    rsMetodo.Close
    Set rsMetodo = Nothing
    Exit Function
    
TrataErro:
    MsgBox "Erro a seleccionar m�todo!", vbExclamation, "SISLAB"
    BL_SeleccionaMetodo = ""
    caracMetodo = ""
    Exit Function
    
End Function

Public Sub BL_PreencheSelComboLocais(EcCombo As Object, Optional bPreenchimentoCondicionado As Boolean)
    Dim sSql As String
    Dim rs As ADODB.recordset
    
    On Error GoTo TrataErro
    
    EcCombo.Clear
    gTotalLocais = 0
    ReDim gEstruturaLocais(0)
    
    If bPreenchimentoCondicionado = False Then
        sSql = "SELECT nome_empr, cod_empr FROM gr_empr_inst"
    Else
        sSql = "SELECT nome_empr, cod_empr FROM gr_empr_inst,sl_ass_util_locais WHERE gr_empr_inst.cod_empr=sl_ass_util_locais.cod_local "
        sSql = sSql & " AND cod_util=" & gCodUtilizador
    End If
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao, adOpenStatic
    If rs.RecordCount > 0 Then
        'maximizar tamanho
        'FormVerificarUtilizador.BtOpcoes.caption = "O&p��es <<"
        FormVerificarUtilizador.Height = 4720
        FormVerificarUtilizador.ButtonOK.top = 3800
        FormVerificarUtilizador.ButtonCancelar.top = 3800
        FormVerificarUtilizador.BtOpcoes.top = 3800
        FormVerificarUtilizador.LaLocal.top = 3000
        FormVerificarUtilizador.EcLocal.top = 3000
        
        Do While Not rs.EOF
            gTotalLocais = gTotalLocais + 1
            ReDim Preserve gEstruturaLocais(gTotalLocais)
            gEstruturaLocais(gTotalLocais).cod_local = rs!cod_empr
            gEstruturaLocais(gTotalLocais).descr_local = rs!nome_empr
            EcCombo.AddItem rs!nome_empr
            EcCombo.ItemData(EcCombo.NewIndex) = rs!cod_empr
            rs.MoveNext
        Loop
        
        If gCodLocal <> -1 Then
            BG_MostraComboSel gCodLocal, EcCombo
'            'minimizar tamanho
'            FormVerificarUtilizador.BtOpcoes.Caption = "O&p��es >>"
'            FormVerificarUtilizador.Height = 4140
'            FormVerificarUtilizador.ButtonOK.Top = 3000
'            FormVerificarUtilizador.ButtonCancelar.Top = 3000
'            FormVerificarUtilizador.BtOpcoes.Top = 3000
'            FormVerificarUtilizador.LaLocal.Top = 4500
'            FormVerificarUtilizador.EcLocal.Top = 4500
            
            
        End If
    Else
        'minimizar tamanmho
        'FormVerificarUtilizador.BtOpcoes.caption = "O&p��es >>"
        FormVerificarUtilizador.Height = 4440
        FormVerificarUtilizador.ButtonOK.top = 3000
        FormVerificarUtilizador.ButtonCancelar.top = 3000
        FormVerificarUtilizador.BtOpcoes.top = 3000
        FormVerificarUtilizador.LaLocal.top = 4500
        FormVerificarUtilizador.EcLocal.top = 4500
    End If
    rs.Close
    Set rs = Nothing
    
    If EcCombo.ListCount <= 1 And EcCombo.ListIndex <> mediComboValorNull Then
        'Caso apenas exista um local
        If TypeOf EcCombo Is ComboBox Then
            EcCombo.Enabled = False
        End If
    End If
    Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro na <BL_PreencheSelComboLocais>: " & Err.Number & Err.Description
End Sub

Function BL_PesquisaCodDes(sNomeTabela As String, sCampoCodigo As String, sCampoDescricao As String, sArg As String, sObjectivo As String) As String
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim sRes As String
    
    On Error GoTo TrataErro
    
    sRes = ""
    
    If Not IsNull(sArg) And sArg <> "" Then
        Set rs = New ADODB.recordset
        
        If sObjectivo = "COD" Then
            sSql = "SELECT " & sCampoCodigo & " FROM " & sNomeTabela & " WHERE " & sCampoDescricao & " = '" & sArg & "'"
        Else ' DES
            sSql = "SELECT " & sCampoDescricao & " FROM " & sNomeTabela & " WHERE " & sCampoCodigo & " = " & sArg
        End If
        
        rs.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rs.Open sSql, gConexao, adOpenStatic
        
        If rs.RecordCount <> 0 Then
            If sObjectivo = "COD" Then
                If Not IsNull(rs(sCampoCodigo)) Then
                    sRes = rs(sCampoCodigo)
                End If
            Else
                If Not IsNull(rs(sCampoDescricao)) Then
                    sRes = rs(sCampoDescricao)
                End If
            End If
        End If
    
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    End If
    
    BL_PesquisaCodDes = sRes
    
    Exit Function

TrataErro:
    BG_LogFile_Erros "Erro nr. " & Err.Number & " - " & Err.Description & " na BL_PesquisaCodDes"
    Exit Function
    
End Function

Function BL_VerificaConclusaoRequisicao(n_req As String, dt_pr_req As String) As String
    Dim sql As String
    Dim rs As New ADODB.recordset
    Dim data As String
    Dim dataAnterior As String
    Dim prazo_conc As Long
    Dim prazo_conc_max As Long
    Dim data_chega As String
    
    On Error GoTo TrataErro
    


    ' CALCULA NOVO PRAZO VALIDADE
    sql = "SELECT x1.prazo_conc, x1.cod_ana, x3.dt_chega, x3.dt_previ "
    sql = sql & " FROM  slv_analises x1, sl_marcacoes x2 LEFT OUTER JOIN sl_req_tubo x3 ON x2.seq_Req_tubo = x3.seq_Req_tubo WHERE " & _
        " x2.n_req = " & n_req & _
        " AND x2.cod_Agrup = x1.cod_ana "
    sql = sql & " UNION SELECT x1.prazo_conc, x1.cod_ana, x3.dt_chega, x3.dt_previ "
    sql = sql & " FROM  slv_analises x1, sl_realiza x2 LEFT OUTER JOIN sl_req_tubo x3 ON x2.seq_Req_tubo = x3.seq_Req_tubo WHERE " & _
        " x2.n_req = " & n_req & _
        " AND x2.cod_Agrup = x1.cod_ana AND x2.seq_req_tubo = x3.seq_req_tubo "
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rs.Open sql, gConexao
    
    If rs.RecordCount > 0 Then
        data = CDate(1)
        While Not rs.EOF
            prazo_conc = BL_HandleNull(rs!prazo_conc, 0)
            If CDate(data) < (CDate(prazo_conc) + CDate(BL_HandleNull(rs!dt_chega, BL_HandleNull(rs!dt_previ, BL_HandleNull(dt_pr_req, 0))))) Then
                data = CDate(prazo_conc) + CDate(BL_HandleNull(rs!dt_chega, BL_HandleNull(rs!dt_previ, BL_HandleNull(dt_pr_req, 0))))
                ' pferreira 2009.09.23
                prazo_conc_max = prazo_conc
                data_chega = CDate(BL_HandleNull(rs!dt_chega, BL_HandleNull(rs!dt_previ, BL_HandleNull(dt_pr_req, 0))))
            End If
            rs.MoveNext
        Wend
    Else
        data = ""
    End If
    rs.Close
    Set rs = Nothing
    '---------------------------------
    
    '---------------------------------
    ' pferreira 2009.09.23
    BL_VerificaConclusaoRequisicao = BL_VerficaDiasIndisponiveis(prazo_conc_max, data_chega)
    Exit Function

TrataErro:
    BG_LogFile_Erros "Erro ao Verificar a Conclusao Requisicao - " & "Erro nr. " & Err.Number & " - " & Err.Description
    Exit Function
    Resume Next
End Function


Function BL_InsereGraficoReport(NReq As Long, CodComplexa As String, DescrComplexa As String) As Integer
    Dim rs As ADODB.recordset
    Dim rv As Integer
    Dim sql As String
    Dim v_count As Integer
    Set rs = New ADODB.recordset
    On Error GoTo TrataErro
    BL_DeleteFile (gDirCliente & "\GR_" & NReq & "_" & CodComplexa & ".bmp")
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.ActiveConnection = gConexao
    rs.Source = " select grafico from sl_res_grafico where n_req = " & NReq & " and cod_ana_c = " & BL_TrataStringParaBD(CodComplexa)
    If gModoDebug = mediSim Then BG_LogFile_Erros rs.Source
    rs.Open
    If rs.RecordCount > 0 Then
        EF_Cria_Grafico_Electroferese rs!Grafico, FormGraficoReport.Picture1, gDirCliente & "\GR_" & NReq & "_" & CodComplexa & ".bmp"
    ElseIf Not (FormGraficoReport.Picture1.Picture = 0) Then
        SavePicture FormGraficoReport.Picture1, gDirCliente & "\GR_" & NReq & "_" & CodComplexa & ".bmp"
    Else
        Exit Function
    End If
    rs.Close
    Set rs = Nothing
    v_count = 0
    While BL_IsFileDistilledYet("\GR_" & NReq & "_" & CodComplexa & ".bmp", gDirCliente) = False And v_count < 20
        Sleep 1000
        DoEvents
        DoEvents
        DoEvents
        v_count = v_count + 1
    Wend
    If v_count >= 20 Then
        BL_LogFile_BD "", "BL_InsereGraficoReport", "0", "", "(Falha 20 vezes) EF: - " & "EF_" & NReq & ".bmp"
    End If
    IM_InserirImagemBD gDirCliente & "\GR_" & NReq & "_" & CodComplexa & ".bmp", "sl_res_grafico_im", "grafico", "n_req", CStr(NReq), "complexa", DescrComplexa
    Kill gDirCliente & "\GR_" & NReq & "_" & CodComplexa & ".bmp"
        

    DoEvents
Exit Function
TrataErro:
    BL_InsereGraficoReport = -1
    BG_LogFile_Erros "BibliotecaLocal: BL_InsereGraficoReport: " & Err.Description, "BL", "BL_InsereGraficoReport"
    Exit Function
    Resume Next
End Function


Sub BL_GraficoReport(NReq As Long, ByVal CodAnaC As String, ByVal DescrComplexa As String)
    Call BL_InsereGraficoReport(NReq, CodAnaC, DescrComplexa)
End Sub

' ===================

' FGONCALVES
' 02/05/2006  CHVNG

' ===================
Public Function BL_DevolveDescrLocal(codLocal As String) As String
    Dim sSql As String
    Dim rs As New ADODB.recordset
    
    On Error GoTo TrataErro
    sSql = "SELECT nome_empr FROM gr_empr_inst WHERE cod_empr = " & BL_TrataStringParaBD(codLocal)
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.ActiveConnection = gConexao
    rs.Open sSql
    If rs.RecordCount = 1 Then
        BL_DevolveDescrLocal = BL_HandleNull(rs!nome_empr, "")
    Else
        BL_DevolveDescrLocal = ""
    End If
    rs.Close
    Set rs = Nothing
TrataErro:
    Set rs = Nothing
    
End Function

Sub BL_PesquisaAnalises(Optional ByVal codLocal As String, Optional ByVal analises As String)
    Dim ChavesPesq(2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 4) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 4) As Long
    Dim Headers(1 To 4) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    
    
    
    ChavesPesq(1) = "si.seq_sinonimo"
    ChavesPesq(2) = "v.cod_ana"
    CamposEcran(1) = "v.cod_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_sinonimo"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposEcran(3) = "p.descr_produto"
    Tamanhos(3) = 1500
    Headers(3) = "Produto"
    
    CamposEcran(4) = " sgr.descr_sgr_ana"
    Tamanhos(4) = 1500
    Headers(4) = "Sub-Grupo"
    
    CamposRetorno.InicializaResultados 1
    CFrom = "slv_analises v LEFT OUTER JOIN sl_sgr_ana sgr ON v.cod_sgr_ana = sgr.cod_sgr_ana LEFT OUTER JOIN sl_produto p on v.cod_produto = p.cod_produto "
    CFrom = CFrom & ", sl_ana_sinonimos si"
    CWhere = " v.cod_ana = si.cod_ana AND v.cod_ana in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ") AND (flg_invisivel is null or flg_invisivel = 0) AND (inibe_marcacao is null or inibe_marcacao = 0) "
    CampoPesquisa = "descr_sinonimo"
    
    If analises <> "" Then
        CWhere = CWhere & " AND v.cod_ana in " & analises
    End If
    
    Select Case gFormActivo.Name
        
        Case "FormGestaoRequisicao"
            PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                                   ChavesPesq, _
                                                                                   CamposEcran, _
                                                                                   CamposRetorno, _
                                                                                   Tamanhos, _
                                                                                   Headers, _
                                                                                   CWhere, _
                                                                                   CFrom, _
                                                                                   "", _
                                                                                   CampoPesquisa, _
                                                                                   " ORDER BY v.descr_ana ", _
                                                                                   " An�lises ")
        Case "FormGestaoRequisicaoPrivado"
            PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                                   ChavesPesq, _
                                                                                   CamposEcran, _
                                                                                   CamposRetorno, _
                                                                                   Tamanhos, _
                                                                                   Headers, _
                                                                                   CWhere, _
                                                                                   CFrom, _
                                                                                   "", _
                                                                                   CampoPesquisa, _
                                                                                   " ORDER BY v.descr_ana ", _
                                                                                   " An�lises ")
        Case "FormGestaoRequisicaoVet"
            PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                                   ChavesPesq, _
                                                                                   CamposEcran, _
                                                                                   CamposRetorno, _
                                                                                   Tamanhos, _
                                                                                   Headers, _
                                                                                   CWhere, _
                                                                                   CFrom, _
                                                                                   "", _
                                                                                   CampoPesquisa, _
                                                                                   " ORDER BY v.descr_ana ", _
                                                                                   " An�lises ")
        Case "FormGestaoRequisicaoAguas"
            PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                                   ChavesPesq, _
                                                                                   CamposEcran, _
                                                                                   CamposRetorno, _
                                                                                   Tamanhos, _
                                                                                   Headers, _
                                                                                   CWhere, _
                                                                                   CFrom, _
                                                                                   "", _
                                                                                   CampoPesquisa, _
                                                                                   " ORDER BY v.descr_ana ", _
                                                                                   " An�lises ")
        
        Case "FormGesReqCons"
            PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                                   ChavesPesq, _
                                                                                   CamposEcran, _
                                                                                   CamposRetorno, _
                                                                                   Tamanhos, _
                                                                                   Headers, _
                                                                                   CWhere, _
                                                                                   CFrom, _
                                                                                   "", _
                                                                                   CampoPesquisa, _
                                                                                   " ORDER BY descr_ana ", _
                                                                                   " An�lises ")
        
        Case "FormSelRes"
            PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana ", _
                                                                           " An�lises ")
    End Select
    
    If PesqRapida = True Then
        Select Case gFormActivo.Name
            
            Case "FormGestaoRequisicao"
                FormPesqRapidaAvancadaMultiSel.Show vbModal
            
            Case "FormGestaoRequisicaoPrivado"
                FormPesqRapidaAvancadaMultiSel.Show vbModal
            Case "FormGestaoRequisicaoVet"
                FormPesqRapidaAvancadaMultiSel.Show vbModal
            Case "FormGestaoRequisicaoAguas"
                FormPesqRapidaAvancadaMultiSel.Show vbModal
            Case "FormGesReqCons"
                FormPesqRapidaAvancadaMultiSel.Show vbModal
            Case "FormSelRes"
                FormPesqRapidaAvancada.Show vbModal
        
        End Select
        
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
        If Not CancelouPesquisa Then
            Select Case gFormActivo.Name
            
                Case "FormGestaoRequisicao"
                    FormGestaoRequisicao.ExecutaCodigoA = True
                    For i = 1 To TotalElementosSel
                        FormGestaoRequisicao.EcAuxAna = Resultados(i)
                        FormGestaoRequisicao.EcAuxAna_KeyDown 13, 0
                    Next i
                    If FormGestaoRequisicao.FGAna.Enabled = True Then
                        FormGestaoRequisicao.FGAna.SetFocus
                    End If
            
                Case "FormGestaoRequisicaoPrivado"
                
                    For i = 1 To TotalElementosSel
                        FormGestaoRequisicaoPrivado.EcAuxAna.Enabled = True
                        FormGestaoRequisicaoPrivado.EcAuxAna = Resultados(i)
                        FormGestaoRequisicaoPrivado.EcAuxAna_KeyDown 13, 0
                    Next i
                    If FormGestaoRequisicaoPrivado.FGAna.Enabled = True Then
                        FormGestaoRequisicaoPrivado.FGAna.SetFocus
                    End If
            
                Case "FormGestaoRequisicaoVet"
                
                    For i = 1 To TotalElementosSel
                        FormGestaoRequisicaoVet.EcAuxAna.Enabled = True
                        FormGestaoRequisicaoVet.EcAuxAna = Resultados(i)
                        FormGestaoRequisicaoVet.EcAuxAna_KeyDown 13, 0
                    Next i
                    If FormGestaoRequisicaoVet.FGAna.Enabled = True Then
                        FormGestaoRequisicaoVet.FGAna.SetFocus
                    End If
                Case "FormGestaoRequisicaoAguas"
                
                    For i = 1 To TotalElementosSel
                        FormGestaoRequisicaoAguas.EcAuxAna.Enabled = True
                        FormGestaoRequisicaoAguas.EcAuxAna = Resultados(i)
                        FormGestaoRequisicaoAguas.EcAuxAna_KeyDown 13, 0
                    Next i
                    If FormGestaoRequisicaoAguas.FGAna.Enabled = True Then
                        FormGestaoRequisicaoAguas.FGAna.SetFocus
                    End If
                Case "FormGesReqCons"
                    FormGesReqCons.FGAna.row = FormGesReqCons.FGAna.rows - 1
                    For i = 1 To TotalElementosSel
                        FormGesReqCons.EcAuxAna.Enabled = True
                        FormGesReqCons.EcAuxAna = Resultados(i)
                        FormGesReqCons.EcAuxAna_KeyDown 13, 0
                    Next i
                    If FormGesReqCons.FGAna.Enabled = True Then
                        FormGesReqCons.FGAna.SetFocus
                    End If
            
                Case "FormSelRes"
                
                    FormSelRes.EcCodAna.text = Resultados(1)
                    FormSelRes.EcCodAna.SetFocus
            
            End Select
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises ", vbExclamation, "ATEN��O"
        If gFormActivo.Name = "FormGestaoRequisicao" Then
            FormGestaoRequisicao.FGAna.SetFocus
        ElseIf gFormActivo.Name = "FormGestaoRequisicaoPrivado" Then
            FormGestaoRequisicaoPrivado.FGAna.SetFocus
        ElseIf gFormActivo.Name = "FormGestaoRequisicaoVet" Then
            FormGestaoRequisicaoVet.FGAna.SetFocus
        ElseIf gFormActivo.Name = "FormGestaoRequisicaoAguas" Then
            FormGestaoRequisicaoAguas.FGAna.SetFocus
        End If
    End If

End Sub
Sub BL_PesquisaAnalises_FACTUS(Optional ByVal codLocal As String, Optional ByVal analises As String)
    Dim ChavesPesq(2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 4) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 4) As Long
    Dim Headers(1 To 4) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    
    
    
    ChavesPesq(1) = "cod_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposEcran(3) = "descr_produto"
    Tamanhos(3) = 1500
    Headers(3) = "Produto"
    
    CamposEcran(4) = "descr_sgr_ana"
    Tamanhos(4) = 1500
    Headers(4) = "Sub-Grupo"
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "slv_analises_factus v "
    CampoPesquisa = "descr_ana"
    'CWhere = " (cod_local = " & IIf(CodLocal <> "", CodLocal, gCodLocal) & " OR cod_local IS NULL) AND (flg_invisivel is null or flg_invisivel = 0) AND (inibe_marcacao is null or inibe_marcacao = 0) "
    'CWhere = " (cod_local like '%" & IIf(CodLocal <> "", CodLocal, gCodLocal) & "%' OR cod_local IS NULL) AND (flg_invisivel is null or flg_invisivel = 0) AND (inibe_marcacao is null or inibe_marcacao = 0) "
    'CHVNG-Espinho lista an�lises que perten�am ao local activo
    CWhere = " cod_ana in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ")  "
    If analises <> "" Then
        CWhere = CWhere & " AND cod_ana in " & analises
    End If
    
        
        PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                               ChavesPesq, _
                                                                               CamposEcran, _
                                                                               CamposRetorno, _
                                                                               Tamanhos, _
                                                                               Headers, _
                                                                               CWhere, _
                                                                               CFrom, _
                                                                               "", _
                                                                               CampoPesquisa, _
                                                                               " ORDER BY v.descr_ana ", _
                                                                               " An�lises ")
        
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
         
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
        If Not CancelouPesquisa Then
            For i = 1 To TotalElementosSel
                FormFactusEnvio.EcCodAnaEFR = Resultados(i)
            Next i
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises ", vbExclamation, "ATEN��O"
    End If

End Sub



' Verifica se Utilizador est� no local URG
' FGONCALVES - 22 JUL 2006
' ------------------------------------------------------
Public Function BL_SeleccionaGrupoAnalises() As Boolean
    Dim sSql As String
    Dim rsGrupoAna As New ADODB.recordset
    
    sSql = " SELECT  flg_grupo_ana FROM gr_empr_inst WHERE empresa_id = " & BL_TrataStringParaBD(CStr(gCodLocal))
    Set rsGrupoAna = New ADODB.recordset
    rsGrupoAna.CursorType = adOpenStatic
    rsGrupoAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsGrupoAna.Open sSql, gConexao, adOpenStatic
    
    If rsGrupoAna.RecordCount > 0 Then
        If BL_HandleNull(rsGrupoAna!flg_grupo_ana, 0) = 1 Then
           BL_SeleccionaGrupoAnalises = True
        Else
           BL_SeleccionaGrupoAnalises = False
        End If
    End If
    rsGrupoAna.Close
    Set rsGrupoAna = Nothing
End Function

Public Function SelDescrGrAntibio(CodGrAntibio As String) As String
    Dim RsGrAntibio As ADODB.recordset
    
    If CodGrAntibio <> "" Then
        Set RsGrAntibio = New ADODB.recordset
        RsGrAntibio.CursorLocation = adUseServer
        RsGrAntibio.CursorType = adOpenStatic
        RsGrAntibio.Source = "SELECT descr_gr_antibio, flg_imprime FROM sl_gr_antibio " & _
                            " WHERE cod_gr_antibio = " & BL_TrataStringParaBD(CodGrAntibio)
        RsGrAntibio.ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros RsGrAntibio.Source
        RsGrAntibio.Open
        If RsGrAntibio.RecordCount > 0 Then
            If BL_HandleNull(RsGrAntibio!flg_imprime, "") = "1" Then
                SelDescrGrAntibio = RsGrAntibio!descr_gr_antibio
            Else
                SelDescrGrAntibio = ""
            End If
        Else
            SelDescrGrAntibio = ""
        End If
    Else
        SelDescrGrAntibio = ""
    End If
End Function

Public Sub BL_TestaLocal()

    If Dir(gDirCliente & "\bin\Local.ini") <> "" Then
        Dim sLocal As String
        Dim i As Integer
        'Existe o ficheiro de indica��o de local
        sLocal = DevolveValorINI(gDirCliente & "\bin\Local.ini", "Local", "Codigo")
        If sLocal <> "(Desconhecido)" Then
            Dim sSql As String
            Dim rs As ADODB.recordset
            sSql = "SELECT cod_empr FROM gr_empr_inst WHERE cod_empr=" & sLocal
            Set rs = New ADODB.recordset
            rs.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rs.Open sSql, gConexao, adOpenStatic
            If rs.RecordCount > 0 Then
                gCodLocal = rs!cod_empr
                rs.Close
                Set rs = Nothing
                Exit Sub
            End If
            rs.Close
            Set rs = Nothing
        End If
    End If
    'MsgBox "Ficheiro de indica��o de local inexistente ou com parametriza��o inv�lida.", vbCritical
    'Abrir form de esoclha de local
    FormAssLocal.Show vbModal
End Sub

Function BL_QueryResRef(DtNascUtente As String, Especial As String, DiasUtente As Long, cod_genero As String) As String
    Dim s As String
    
    s = "SELECT sl_ana_ref.n_res,sl_ana_ref.seq_ana_ref," & _
        "sl_esc_ref.t_esc_inf,sl_esc_ref.h_ref_min AS HEMIN,sl_esc_ref.h_ref_max AS HEMAX,sl_esc_ref.f_ref_min AS MEMIN,sl_esc_ref.f_ref_max AS MEMAX," & _
        "sl_val_ref.h_ref_min AS HVMIN,sl_val_ref.h_ref_max AS HVMAX,sl_val_ref.f_ref_min AS MVMIN,sl_val_ref.f_ref_max AS MVMAX,sl_val_ref.com_mah,sl_val_ref.com_maf,sl_val_ref.com_mih,sl_val_ref.com_mif," & _
        "sl_cut_off.val_neg_min,sl_cut_off.val_neg_max,sl_cut_off.t_comp_nmin,sl_cut_off.t_comp_nmax,sl_cut_off.val_pos_min,sl_cut_off.val_pos_max,sl_cut_off.t_comp_pmin,sl_cut_off.t_comp_pmax,sl_cut_off.val_duv_min, " & _
        "sl_cut_off.val_duv_max,sl_cut_off.t_comp_dmin,sl_cut_off.t_comp_dmax, sl_ana_ref.obs_ref, sl_cut_off.com_pos, sl_cut_off.com_neg, sl_cut_off.com_duv "
    'OUTER JOIN
    If gSGBD = gInformix Then
        s = s & "FROM sl_ana_ref,OUTER sl_esc_ref,OUTER sl_val_ref,OUTER sl_cut_off WHERE "
    Else
        s = s & "FROM sl_ana_ref,sl_esc_ref,sl_val_ref,sl_cut_off WHERE "
    End If
    
    s = s & "sl_ana_ref.cod_ana_s=? AND sl_ana_ref.dt_valid_inf<=? AND (sl_ana_ref.dt_valid_sup IS NULL OR (sl_ana_ref.dt_valid_sup IS NOT NULL AND sl_ana_ref.dt_valid_sup>?) ) "
    
    Select Case gSGBD
        Case gOracle
            s = s & " AND sl_ana_ref.seq_ana_ref=sl_val_ref.cod_ana_ref(+) AND  sl_ana_ref.seq_ana_ref=sl_esc_ref.cod_ana_ref(+) AND sl_ana_ref.seq_ana_ref=sl_cut_off.cod_ana_ref(+) "
        Case gInformix
            s = s & "AND sl_ana_ref.seq_ana_ref=sl_val_ref.cod_ana_ref AND  sl_ana_ref.seq_ana_ref=sl_esc_ref.cod_ana_ref AND sl_ana_ref.seq_ana_ref=sl_cut_off.cod_ana_ref "
        Case gSqlServer
            s = s & "AND sl_ana_ref.seq_ana_ref*=sl_val_ref.cod_ana_ref AND  sl_ana_ref.seq_ana_ref*=sl_esc_ref.cod_ana_ref  AND sl_ana_ref.seq_ana_ref*=sl_cut_off.cod_ana_ref "
    End Select
    
    If Trim(DtNascUtente) <> "" Then
        s = s & " AND (sl_esc_ref.cod_ana_ref IS NULL OR (sl_esc_ref.cod_ana_ref IS NOT NULL AND " & _
                "(sl_esc_ref.t_esc_inf IN " & Especial & _
                " OR (sl_esc_ref.t_esc_inf NOT IN " & Especial & " AND sl_esc_ref.esc_inf_dias <= " & DiasUtente & " AND sl_esc_ref.esc_sup_dias > " & DiasUtente & ")))) "
    Else
        'N�o tem Data de nascimento e tem registo em escal�es
        s = s & " AND (sl_esc_ref.cod_ana_ref IS NULL OR (sl_esc_ref.cod_ana_ref IS NOT NULL AND sl_esc_ref.t_esc_inf IN " & Especial & ")) "
    End If
    s = s & " AND (sl_ana_ref.cod_genero = " & cod_genero & " OR sl_ana_ref.cod_genero IS NULL) "
    s = s & "ORDER BY sl_ana_ref.n_res,sl_esc_ref.t_esc_inf"
    
    BL_QueryResRef = s

End Function


Sub BL_InsereEResults(NReq As String, Optional SemValidacao As Boolean)
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim rsReq As ADODB.recordset
    Dim dt_val As String
    Dim hr_val As String
    Dim dt_chega As String
    Dim hr_chega As String
    Dim t_sit As String
    Dim n_epis As String
    Dim estado_req As String
    Dim cod_med As String
    Dim cod_proven As String
    Dim seq_utente As String
    Dim cod_efr As String
    Dim nr_benef As String
    Dim cod_local As String
    
    On Error GoTo TrataErro

    If Not IsNumeric(NReq) Then
        Exit Sub
    End If
    
    'dt_emissao = ultima data de validacao de resultados

    'Sql = "INSERT INTO SL_ERESULTS_H (SELECT * FROM SL_ERESULTS WHERE cod_original=" & BL_TrataStringParaBD(NReq) & ")"
    'BG_ExecutaQuery_ADO Sql
    sql = "DELETE FROM SL_ERESULTS WHERE cod_original=" & BL_TrataStringParaBD(Trim(NReq))
    BG_ExecutaQuery_ADO sql
    
    If gTipoInstituicao = gTipoInstituicaoPrivada Then
        sql = "SELECT x1.dt_val, x1.hr_val,x2.dt_chega, x2.hr_chega,x2.t_sit, x2.n_epis,x2.estado_req, "
        sql = sql & " x2.cod_med,x2.cod_proven, x2.seq_utente, x2.cod_Efr, x2.n_benef, x2.cod_local FROM sl_requis x2 LEFT OUTER JOIN  "
        sql = sql & " sl_proven x3 ON x2.cod_proven = x3.cod_proven LEFT OUTER JOIN sl_realiza x1  ON x1.n_req = x2.n_req"
        sql = sql & " WHERE x2.n_req = " & BL_TrataStringParaBD(NReq)
        If SemValidacao = False Then
            sql = sql & " AND x1.dt_val is not null and x1.hr_val is not null "
        End If
        sql = sql & " AND (x3.FLG_NAO_ENVIA_ER IS NULL or x3.FLG_NAO_ENVIA_ER = 0)"
        sql = sql & " AND x2.estado_req IN (" & BL_TrataStringParaBD(gEstadoReqTodasImpressas) & "," & BL_TrataStringParaBD(gEstadoReqValicacaoMedicaCompleta) & ")"
    Else
        sql = "SELECT x1.dt_val, x1.hr_val,x2.dt_chega, x2.hr_chega,x2.t_sit, x2.n_epis,x2.estado_req, "
        sql = sql & " x2.cod_med,x2.cod_proven, x2.seq_utente, x2.cod_Efr, x2.n_benef, x2.cod_local FROM sl_requis x2 LEFT OUTER JOIN sl_realiza x1"
        sql = sql & " ON x1.n_Req = x2.n_req,  "
        sql = sql & " sl_proven x3 "
        sql = sql & " WHERE x2.n_req = " & BL_TrataStringParaBD(NReq) & " AND  x2.cod_proven = x3.cod_proven "
        If SemValidacao = False Then
            sql = sql & " AND x1.dt_val is not null and x1.hr_val is not null "
        End If
        sql = sql & " AND (x3.FLG_NAO_ENVIA_ER IS NULL or x3.FLG_NAO_ENVIA_ER = 0)"
    End If
    sql = sql & " ORDER by dt_val desc, hr_val desc "
    
    Set rsReq = New ADODB.recordset
    rsReq.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rsReq.Open sql, gConexao, adOpenStatic
    
    If rsReq.RecordCount > 0 Then
        dt_val = BL_HandleNull(rsReq!dt_val, "")
        hr_val = BL_HandleNull(rsReq!hr_val, "")
        dt_chega = BL_HandleNull(rsReq!dt_chega, Bg_DaData_ADO)
        hr_chega = BL_HandleNull(rsReq!hr_chega, Bg_DaHora_ADO)
        t_sit = BL_HandleNull(rsReq!t_sit, "")
        n_epis = BL_HandleNull(rsReq!n_epis, "")
        estado_req = BL_HandleNull(rsReq!estado_req, "")
        cod_med = BL_HandleNull(rsReq!cod_med, "")
        cod_proven = BL_HandleNull(rsReq!cod_proven, "")
        seq_utente = BL_HandleNull(rsReq!seq_utente, "")
        cod_efr = BL_HandleNull(rsReq!cod_efr, "")
        nr_benef = BL_HandleNull(rsReq!n_benef, "")
        cod_local = BL_HandleNull(rsReq!cod_local, gCodLocal)
        
        sql = "INSERT INTO SL_ERESULTS (cod_original,tipo_episodio,episodio,dt_recepcao,dt_emissao, " & _
            " estado, id_pessoa_req, " & _
            " cod_servico_req,cod_utente,subsistema,nr_benef,flg_estado, cod_local) VALUES(" & _
            BL_TrataStringParaBD(Trim(NReq)) & ", " & BL_TrataStringParaBD(t_sit) & "," & BL_TrataStringParaBD(n_epis) & "," & _
            "to_Date('" & CDate(dt_chega & " " & BL_TrataHora(hr_chega)) & "', 'DD-MM-YYYY HH24:MI:SS'),"
        If dt_val <> "" And hr_val <> "" Then
            sql = sql & "to_date('" & CDate(dt_val & " " & hr_val) & "', 'DD-MM-YYYY HH24:MI:SS'),"
        Else
            sql = sql & " NULL,"
        End If
        sql = sql & BL_TrataStringParaBD(estado_req) & "," & BL_TrataStringParaBD(cod_med) & "," & _
            BL_TrataStringParaBD(cod_proven) & "," & BL_TrataStringParaBD(seq_utente) & "," & BL_TrataStringParaBD(cod_efr) & ", " & BL_TrataStringParaBD(nr_benef) & "," & _
            "  0, " & cod_local & " )"
            BG_ExecutaQuery_ADO sql
            BG_Trata_BDErro
    
    End If
    rsReq.Close
    Set rsReq = Nothing
    
    Exit Sub


TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_InsereEResults: " & Err.Description, "BL", "BL_InsereEResults", False
    Exit Sub
    Resume Next
End Sub


Sub BL_InsereEmail(NReq As String)
    Dim sql As String
    Dim seq_email_completos As String
    Dim rsReq As New ADODB.recordset
    On Error GoTo TrataErro

    If Not IsNumeric(NReq) Then
        Exit Sub
    End If
    sql = "SELECT * FROM sl_email_completos WHERE n_req = " & NReq & " AND flg_estado = 0"
    Set rsReq = New ADODB.recordset
    rsReq.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rsReq.Open sql, gConexao, adOpenStatic
    If rsReq.RecordCount = 0 Then
        seq_email_completos = GUID_GET
        sql = "INSERT INTO sl_email_completos (seq_email_completos,n_req, dt_cri, user_cri,flg_estado ) VALUES("
        sql = sql & BL_TrataStringParaBD(seq_email_completos) & ", "
        sql = sql & NReq & ", sysdate, "
        sql = sql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ",0)"
        BG_ExecutaQuery_ADO sql
    End If
    rsReq.Close
    Set rsReq = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_InsereEmail: " & Err.Description, "BL", "BL_InsereEmail", False
    Exit Sub
    Resume Next
End Sub




' -----------------------------------------------------------------------------------------

' RETORNA O VOLUME DO PRODUTO DE UMA ANALISE DE UMA REQUISICAO

' -----------------------------------------------------------------------------------------

Public Function BL_RetornaVolumeProd(flg_imprime_volume As String, codAna As String, n_req As String)
    Dim sSql As String
    Dim RsProd As New ADODB.recordset
    Dim resultado As String
    sSql = "select sl_req_prod.volume from sl_req_prod , sl_ana_s  "
    sSql = sSql & " where  sl_ana_s.cod_produto = sl_req_prod.cod_prod AND n_req = " & n_req
    sSql = sSql & " AND sl_ana_s.cod_Ana_s = " & BL_TrataStringParaBD(codAna)
    Set RsProd = New ADODB.recordset
    RsProd.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsProd.Open sSql, gConexao, adOpenStatic
    If RsProd.RecordCount > 0 Then
        If flg_imprime_volume = "1" Then
            resultado = BL_HandleNull(RsProd!Volume, "Null")
        Else
            resultado = "Null"
        End If
    Else
        resultado = "Null"
    End If
    RsProd.Close
    Set RsProd = Nothing
    If resultado <> "Null" Then
        BL_RetornaVolumeProd = BL_TrataStringParaBD(resultado)
    Else
        BL_RetornaVolumeProd = resultado
    End If
End Function


' -----------------------------------------------------------------------------------------

' APAGA REGISTOS DA TABELA TEMPORARIA USADA NOS REPORTS

' -----------------------------------------------------------------------------------------
Public Sub BL_ApagaRegistosTabelaTemp(NomeTab As String)
    On Error GoTo TrataErro
    Dim sSql As String
    
    BG_BeginTransaction
    sSql = "DELETE FROM " & NomeTab & " WHERE nome_computador = "
    sSql = sSql & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    
    BG_ExecutaQuery_ADO sSql
    
    If gSQLError = 0 Then
    BG_CommitTransaction
    Else
    BG_RollbackTransaction
    GoTo TrataErro
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros " ----------------------------------------------------------------"
    BG_LogFile_Erros " BL_ApagaRegistos_tabelaTemp: " & Err.Description
    BG_LogFile_Erros sSql
    BG_LogFile_Erros " ----------------------------------------------------------------"
End Sub



' -----------------------------------------------------------------------------------------

' REGISTA O UTILIZADOR QUE ALTERA A VALIDACAO

' -----------------------------------------------------------------------------------------
Public Sub BL_RegistaValidacao(n_req As Long, Estado As String, data As String, hora As String, _
                                cod_ana_s As String, cod_ana_c As String, cod_perfis As String, _
                                seq_realiza As String, resultado As String)
    Dim sSql As String
    On Error GoTo TrataErro
    
    BG_BeginTransaction
    gSQLError = 0
    sSql = "INSERT INTO sl_validacoes(nome_computador,n_req,seq_realiza,resultado,cod_ana_s, cod_ana_c, cod_perfil, "
    sSql = sSql & " Estado ,user_val,dt_val,hr_val) VALUES( "
    sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
    sSql = sSql & n_req & ", "
    sSql = sSql & seq_realiza & ", "
    sSql = sSql & BL_TrataStringParaBD(resultado) & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_ana_s) & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_ana_c) & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_perfis) & ", "
    sSql = sSql & BL_TrataStringParaBD(Estado) & ", "
    sSql = sSql & gCodUtilizador & ", "
    sSql = sSql & BL_TrataDataParaBD(data) & ", "
    sSql = sSql & BL_TrataStringParaBD(hora) & ")"
    
    BG_ExecutaQuery_ADO sSql
    
    If gSQLError = 0 Then
        BG_CommitTransaction
    Else
        BG_RollbackTransaction
        GoTo TrataErro
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros " ----------------------------------------------------------------"
    BG_LogFile_Erros " BL_RegistaValidacao: " & Err.Description
    BG_LogFile_Erros sSql
    BG_LogFile_Erros " ----------------------------------------------------------------"
    
End Sub


Public Function BL_DevolveGrupoRequis(n_req As Long) As String
    BL_DevolveGrupoRequis = BL_SelCodigo("sl_requis", "gr_ana", "n_req", CStr(n_req))
End Function

'RGONCALVES 10.12.2015 CHSJ-2511
'Public Sub BL_VerificaExternasAcreditadas(ByRef totalAcreditadas As Integer, ana_acreditadas As Integer, _
'                                          ByRef simples As String, ByRef totalExterior As Integer, _
'                                          ana_exterior As Integer, ByRef totalNaoAcreditadas As Integer)
'    '-----------------------------------------------------------------------------------
'    ' CONTA TOTAL DE ANALISES ACREDITADAS E FEITAS NO EXTERIOR PARA ENVIAR PARA EXTERIOR
'    ' ANALISE NAO ACREDITADA SAI COM *
'    '-----------------------------------------------------------------------------------
'
'    If ana_acreditadas = 0 And simples <> "Null" Then
'        simples = simples & " (*) "
'        totalNaoAcreditadas = totalNaoAcreditadas + 1
'    Else
'        totalAcreditadas = totalAcreditadas + ana_acreditadas
'    End If
'    totalExterior = totalExterior + ana_exterior
'    If ana_exterior = 1 And simples <> "Null" Then
'        simples = simples & " (**) "
'    End If
'    ' ---------------------------------------------------------------------------------
'    ' PREPARA ANALISE PARA INSERIR NA BD
'    ' ---------------------------------------------------------------------------------
'    If simples <> "Null" Then simples = BL_TrataStringParaBD(simples)
'
'End Sub

'RGONCALVES 10.12.2015 CHSJ-2511 - alterada assinatura da fun��o
Public Sub BL_VerificaExternasAcreditadas(ByRef totalAcreditadas As Integer, ana_acreditadas As Integer, _
                                          ByRef simples As String, ByRef totalExterior As Integer, _
                                          ana_exterior As Integer, ByRef totalNaoAcreditadas As Integer, _
                                          ByVal sql_source As String, ByRef complexa As String, ByRef Perfil As String, _
                                          ByVal ana_c_acreditada As Integer, ByVal ana_p_acreditada As Integer, _
                                          ByRef totalAcreditadasCodAgrup As Integer, ByRef totalNaoAcreditadasCodAgrup As Integer)
    
     On Error GoTo TrataErro
     
    Dim RsAcreditadas As ADODB.recordset
    Dim sql As String
    '-----------------------------------------------------------------------------------
    ' CONTA TOTAL DE ANALISES ACREDITADAS E FEITAS NO EXTERIOR PARA ENVIAR PARA EXTERIOR
    ' ANALISE NAO ACREDITADA SAI COM *
    '-----------------------------------------------------------------------------------

    If ana_acreditadas = 0 And simples <> "Null" Then
        simples = simples & " (*) "
        totalNaoAcreditadas = totalNaoAcreditadas + 1
    Else
        totalAcreditadas = totalAcreditadas + ana_acreditadas
    End If
    totalExterior = totalExterior + ana_exterior
    If ana_exterior = 1 And simples <> "Null" Then
        simples = simples & " (**) "
    End If
    ' ---------------------------------------------------------------------------------
    ' PREPARA ANALISE PARA INSERIR NA BD
    ' ---------------------------------------------------------------------------------
    If simples <> "Null" Then simples = BL_TrataStringParaBD(simples)
   
    
    'RGONCALVES 10.12.2015 CHSJ-2511
    If ana_c_acreditada = 0 And complexa <> "Null" And Right(Trim(complexa), 3) <> "(*)" Then
        complexa = complexa & " (*) "
    End If
    If ana_p_acreditada = 0 And Perfil <> "Null" And Right(Trim(Perfil), 3) <> "(*)" Then
        Perfil = Perfil & " (*) "
    End If
    
    If complexa <> "Null" Then complexa = BL_TrataStringParaBD(complexa)
    If Perfil <> "Null" Then Perfil = BL_TrataStringParaBD(Perfil)
    
    If totalAcreditadasCodAgrup + totalNaoAcreditadasCodAgrup <= 0 Then 'S� Faz a pesquisa 1 vez
        Set RsAcreditadas = New ADODB.recordset
        RsAcreditadas.CursorType = adOpenStatic
        RsAcreditadas.CursorLocation = adUseServer
        RsAcreditadas.Open sql_source, gConexao
        While Not RsAcreditadas.EOF
            If RsAcreditadas!acreditada > 0 Then
                totalAcreditadasCodAgrup = totalAcreditadasCodAgrup + 1
            Else
                totalNaoAcreditadasCodAgrup = totalNaoAcreditadasCodAgrup + 1
            End If
            RsAcreditadas.MoveNext
        Wend
        RsAcreditadas.Close
        Set RsAcreditadas = Nothing
    End If
    
    Exit Sub
    
TrataErro:

End Sub

 
Public Sub BL_CarregaFichINI()


    Dim sCaminhoFicheiro As String
    
    Call ControlPanel
    
    sCaminhoFicheiro = App.Path & "\SISLAB_REG.ini"
    
    gFormatoData = DevolveValorINI(sCaminhoFicheiro, "Internacional", "Data")
    gFormatoHora = DevolveValorINI(sCaminhoFicheiro, "Internacional", "Hora")
    gSimboloDecimal = DevolveValorINI(sCaminhoFicheiro, "Internacional", "Decimal")
   
    gDirCliente = DevolveValorINI(sCaminhoFicheiro, "Geral", "DirCliente")
    gDirServidor = DevolveValorINI(sCaminhoFicheiro, "Geral", "DirServidor")
    
    
    
    
    
    gModoDebug = 0
    
    gSQLError = 0
    gSQLISAM = 0
    
    gInsRegistoDuplo = Array(-239, 1)
    gUpdRegistoDuplo = Array(-346)
    gInsRegistoNulo = Array(-391, 1407, 1400)
    gErroSyntax = Array(-201)
    gValoresInsuf = Array(947, -236)
    gChaveViolada = Array(2292)
    gEstadoTransacao = False ' Iniciar estado da transac��o (N�o existe transac��o)
    
    
    

    '****************** data source **********************+
    Dim iniTipoBD As String
    Dim iniDataSource As String
    Dim iniFileDataSource
    Dim iniUID As String
    Dim iniPWD As String
    Dim iniLoginTimeout As String
        
    
    gProvider = DevolveValorINI(sCaminhoFicheiro, "SGBD", "PROVIDER")
    gOracleServer = DevolveValorINI(sCaminhoFicheiro, "SGBD", "SERVER")
    gOracleServerSec = DevolveValorINI(sCaminhoFicheiro, "SGBD", "SERVER_SEC")
    gOracleUID = DevolveValorINI(sCaminhoFicheiro, "SGBD", "UID")
    gOraclePWD = DevolveValorINI(sCaminhoFicheiro, "SGBD", "PWD")
    gOraclePWD = Desencripta_Trivial(gOraclePWD)
    
    iniTipoBD = DevolveValorINI(sCaminhoFicheiro, "SGBD", "TipoBD")
    iniDataSource = DevolveValorINI(sCaminhoFicheiro, "SGBD", "DataSource")
    iniFileDataSource = DevolveValorINI(sCaminhoFicheiro, "SGBD", "FileDataSource")
    iniUID = DevolveValorINI(sCaminhoFicheiro, "SGBD", "UID")
    iniPWD = DevolveValorINI(sCaminhoFicheiro, "SGBD", "PWD")
    If iniPWD <> "" Then
        iniPWD = Desencripta_Trivial(iniPWD)
    End If
    iniLoginTimeout = DevolveValorINI(sCaminhoFicheiro, "SGBD", "LoginTimeout")
    
    gDSN = iniDataSource
    
    If iniFileDataSource <> "" Then
        gFileDSN = iniFileDataSource
    End If
    
    If iniUID <> "Nada" And Trim(iniUID) <> "" Then
        gDSN = gDSN & ";UID=" & iniUID
        If gFileDSN <> "" Then gFileDSN = gFileDSN & ";UID=" & iniUID
    End If
    If iniPWD <> "Nada" And Trim(iniPWD) <> "" Then
        gDSN = gDSN & ";PWD=" & iniPWD
        gBDPwd = iniPWD
        If gFileDSN <> "" Then gFileDSN = gFileDSN & ";PWD=" & iniPWD
    End If
    gDSN = gDSN & ";Connect Timeout=" & iniLoginTimeout
    If gFileDSN <> "" Then gFileDSN = gFileDSN & ";Connect Timeout=" & iniLoginTimeout
End Sub
Public Function BL_FileExists(ByVal sFileName As String) As Boolean

    Dim sFile As String

On Error GoTo ErrorHandler

    BL_FileExists = False

    sFile = Dir$(sFileName)
    If (Len(sFile) > 0) And (Err = 0) Then
        BL_FileExists = True
    End If
    Exit Function
ErrorHandler:
    BG_LogFile_Erros ("Erro BL_FileExists: " & Err.Number & " -> " & Err.Description)
    BL_FileExists = False
    Exit Function
End Function

Public Function BL_IsFileDistilledYet(strFileName As String, strDistillerDir As String) As Boolean
    Dim strOutputFileName As String
    'Dim StrLen As Integer
    
    strOutputFileName = LCase(strDistillerDir + "\" & strFileName)

    If BL_FileExists(strOutputFileName) Then
        If (BL_FileLocked(strOutputFileName) Or (FileLen(strOutputFileName) < 5000)) Then
        
            BL_IsFileDistilledYet = False
        Else
            BL_IsFileDistilledYet = True
        End If
    Else
        BL_IsFileDistilledYet = False
    End If
End Function

Public Sub BL_DeleteFile(ByVal sFileName As String)

On Error GoTo ErrorHandler
    If Dir(sFileName) <> "" Then
        Kill sFileName
    End If
    Exit Sub
ErrorHandler:
    BG_LogFile_Erros ("Erro BL_DeleteFile: " & Err.Number & " -> " & Err.Description)
End Sub

 
Sub BL_LogFile_BD(nomeForm As String, nomeFunc As String, codErro As String, DescrErro As String, _
                    obsErro As String)
    Dim sSql As String
    
    On Error Resume Next
    
    If gSGBD = gOracle Then
        sSql = "INSERT INTO sl_logs(seq_log, nome_computador,user_cri,dt_cri,hr_cri,nome_form,nome_funcao,cod_erro,"
        sSql = sSql & " descr_erro, obs_erro) VALUES ( seq_log.nextval, " & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", "
        sSql = sSql & BL_TrataStringParaBD(nomeForm) & ", "
        sSql = sSql & BL_TrataStringParaBD(nomeFunc) & ", "
        sSql = sSql & BL_TrataStringParaBD(codErro) & ", "
        sSql = sSql & BL_TrataStringParaBD(DescrErro) & ", "
        sSql = sSql & BL_TrataStringParaBD(obsErro) & ")"
    ElseIf gSGBD = gSqlServer Then
        sSql = "INSERT INTO sl_logs( nome_computador,user_cri,dt_cri,hr_cri,nome_form,nome_funcao,cod_erro,"
        sSql = sSql & " descr_erro, obs_erro) VALUES (" & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", "
        sSql = sSql & BL_TrataStringParaBD(nomeForm) & ", "
        sSql = sSql & BL_TrataStringParaBD(nomeFunc) & ", "
        sSql = sSql & BL_TrataStringParaBD(codErro) & ", "
        sSql = sSql & BL_TrataStringParaBD(DescrErro) & ", "
        sSql = sSql & BL_TrataStringParaBD(obsErro) & ")"
    End If
    gConexao.Execute sSql
    
End Sub

' -----------------------------------------------------------------------------------------------

' MUDA A COR DE UMA LINHA DE UMA FLEXGRID

' -----------------------------------------------------------------------------------------------
Public Sub BL_MudaCorFg(Flex As Object, linha As Long, cor As Long)
    Dim i As Integer
    Flex.row = linha
    For i = 0 To Flex.Cols - 1
        Flex.Col = i
        Flex.CellBackColor = cor
    Next
End Sub


' -----------------------------------------------------------------------------------------------

' GERADOR DE NUMERO SEQUENCIAL PARA SERVICOS E SALAS/POSTOS

' -----------------------------------------------------------------------------------------------
Function BL_GeraNumeroPreImpressas(Tabela As String, prefixo As String) As Long


    Dim CmdGera As New ADODB.Command
    Dim PmtGera As ADODB.Parameter
    Dim PmtCampo As ADODB.Parameter
    Dim PmtTabela As ADODB.Parameter
        
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdGera
        .ActiveConnection = gConexao
        .CommandText = "GERADOR_PRE_IMPRESSAS"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtGera = CmdGera.CreateParameter("RETORNO", adNumeric, adParamOutput, 0)
    PmtGera.Precision = 12
    Set PmtCampo = CmdGera.CreateParameter("CAMPO", adVarChar, adParamInput, 30)
    Set PmtTabela = CmdGera.CreateParameter("TABELA", adVarChar, adParamInput, 30)
    CmdGera.Parameters.Append PmtCampo
    CmdGera.Parameters.Append PmtTabela
    CmdGera.Parameters.Append PmtGera
    
    CmdGera.Parameters("CAMPO") = prefixo
    CmdGera.Parameters("TABELA") = Tabela
    CmdGera.Execute
    
    BL_GeraNumeroPreImpressas = Trim(CmdGera.Parameters.Item("RETORNO").value)
       
    GoTo fim

TrataErro:
    BL_GeraNumeroPreImpressas = -1
    BG_TrataErro "BL_GeraNumeroPreImpressas", Err.Number
        
fim:
    Set CmdGera = Nothing
    Set PmtGera = Nothing
    Set PmtCampo = Nothing
    Set PmtTabela = Nothing

End Function


' -----------------------------------------------------------------------------------------------

' VERIFICA SE IMPRIME HISTORICO DE DETERMINADA ANALISE

' -----------------------------------------------------------------------------------------------
Public Function BL_AnaSemHistorico(cod_ana_s As String) As Boolean
    Dim sSql  As String
    Dim rsAna As New ADODB.recordset
    sSql = "SELECT sem_historico FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        BL_AnaSemHistorico = BL_HandleNull(rsAna!sem_historico, False)
    Else
        BL_AnaSemHistorico = False
    End If
    rsAna.Close
    Set rsAna = Nothing
    'BL_AnaHistorico IIf(BL_SelCodigo("SL_ANA_S", "SEM_HISTORICO", "COD_ANA_S", cod_ana_s) = "1", True, False)
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA SE COLOCA A NEGRITO A ANALISE NA IMPRESSAO NA FICHA CRUZADA.

' -----------------------------------------------------------------------------------------------
Public Function BL_AnaNegrito(cod_ana_s As String) As Boolean
    BL_AnaNegrito IIf(BL_SelCodigo("SL_ANA_S", "FLG_Negrito", "COD_ANA_S", cod_ana_s) = 1, True, False)
End Function



Public Function BL_FormataNumBenefPT(codigo As String) As String

End Function





' -----------------------------------------------------------------------------------------------

' VERIFICA SE SE TRATA DE UMA SALA OU DE UM POSTO

' -----------------------------------------------------------------------------------------------
Public Function BL_VerificaPosto(codSala As Integer) As Boolean
    If BL_SelCodigo("SL_COD_SALAS", "FLG_COLHEITA", "COD_SALA", CStr(codSala)) = "1" Then
        BL_VerificaPosto = False
    Else
        BL_VerificaPosto = True
    End If
End Function



' -----------------------------------------------------------------------------------------------

' VERIFICA SE UTENTE DEVE ALGUMA TAXA PARA ALGUMA ENTIDADE

' -----------------------------------------------------------------------------------------------
Public Function BL_VerificaTaxasUtente(SeqUtente As String, Optional n_req As String, Optional efr As String) As Boolean
    Dim sSql As String
    Dim rsTaxas As New ADODB.recordset
    
    On Error GoTo TrataErro
    
    sSql = "SELECT n_rec, estado, total_pagar FROM sl_requis x1, sl_recibos x2, sl_identif x3 WHERE x1.seq_utente = x3.seq_utente "
    sSql = sSql & " AND x1.n_req = x2.n_Req "
    sSql = sSql & " AND x1.seq_utente = " & SeqUtente
    
    If Not IsMissing(n_req) And n_req <> "" Then
        sSql = sSql & " AND x1.n_req = " & n_req
    End If
    
    If Not IsMissing(efr) And efr <> "" Then
        sSql = sSql & " AND x2.cod_efr = " & efr
    End If
    
    rsTaxas.CursorLocation = adUseServer
    rsTaxas.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTaxas.Open sSql, gConexao
    If rsTaxas.RecordCount > 0 Then
        BL_VerificaTaxasUtente = True
        If CDbl(BL_HandleNull(rsTaxas!total_pagar, "0")) > 0 Then
            BL_VerificaTaxasUtente = True
        End If
    End If
    rsTaxas.Close
    Set rsTaxas = Nothing
Exit Function
TrataErro:
    BL_LogFile_BD "Biblioteca Local", "BL_VerificaTaxasUtente", Err.Number, Err.Description, sSql
    Exit Function
    Resume Next
End Function





' --------------------------------------------------------------------------------------------------

' DEVOLVE A DIVIDA DE UM UTENTE. SE ENVIAR ARGUMENTO n_req ENTAO PROCURA DIVIDAS PARA REQ ESPECIFICA

' --------------------------------------------------------------------------------------------------
Public Sub BL_ImprimeDividaUtente(seq_utente As Long, Optional n_req As String)
    Dim sSql As String
    Dim rsDividas As New ADODB.recordset
    Dim divida As Double
    Dim continua As Boolean
    
    'Printer Common Dialog
    gImprimirDestino = 0
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("DividasUtente", "Listagem Dividas Utente", crptToPrinter)
    Else
        continua = BL_IniciaReport("DividasUtente", "Listagem Dividas Utente", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    Report.WindowState = crptMaximized
    
    ' ---------------------------
    ' APAGA TABELA TEMPORARIA
    ' ---------------------------
    sSql = " DELETE FROM sl_cr_dividas WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    
    
    ' ---------------------------
    ' ANALISES SEM RECIBO
    ' ---------------------------
    sSql = " SELECT x3.taxa, x3.n_req, x1.seq_utente,x3.cod_facturavel,x3.cod_efr FROM  sl_identif x1, sl_requis x2, sl_recibos_det x3 WHERE x1.seq_utente= x2.seq_utente"
    sSql = sSql & " AND x2.n_req = x3.n_req  AND x1.seq_utente = " & seq_utente
    sSql = sSql & " AND x3.n_rec = 0 AND (flg_retirado = 0  or flg_retirado is null)"
    If n_req <> "" Then
        sSql = sSql & " AND sl_requis.n_Req = " & n_req
    End If
    
    rsDividas.CursorLocation = adUseServer
    rsDividas.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDividas.Open sSql, gConexao
    If rsDividas.RecordCount > 0 Then
        divida = 0
        While Not rsDividas.EOF
            sSql = " INSERT INTO sl_cr_dividas (nome_computador, seq_utente, n_req, cod_ana, descr_ana,taxa, cod_efr) VALUES ( "
            sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
            sSql = sSql & seq_utente & ", "
            sSql = sSql & rsDividas!n_req & ", "
            sSql = sSql & BL_TrataStringParaBD(rsDividas!cod_facturavel) & ", "
            If Mid(rsDividas!cod_facturavel, 1, 1) = "P" Then
                sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_PERFIS", "DESCR_PERFIS", "COD_PERFIS", rsDividas!cod_facturavel)) & ", "
            Else
                sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SLV_ANALISES", "DESCR_ANA", "COD_ANA", rsDividas!cod_facturavel)) & ", "
            End If
            sSql = sSql & Replace(rsDividas!taxa, ",", ".") & ", "
            sSql = sSql & rsDividas!cod_efr & ") "
            BG_ExecutaQuery_ADO sSql
            BG_Trata_BDErro
            divida = divida + CDbl(BL_HandleNull(rsDividas!taxa, 0))
            rsDividas.MoveNext
        Wend
    End If
    rsDividas.Close
    
    Set rsDividas = Nothing
    
    Report.SQLQuery = "SELECT  SL_CR_DIVIDAS.SEQ_UTENTE, SL_IDENTIF.NOME_UTE, SL_CR_DIVIDAS.COD_ANA, SL_CR_DIVIDAS.DESCR_ANA, SL_CR_DIVIDAS.TAXA, "
    Report.SQLQuery = Report.SQLQuery & " SL_CR_DIVIDAS.COD_EFR, SL_EFR.DESCR_EFR "
    Report.SQLQuery = Report.SQLQuery & " FROM SL_CR_DIVIDAS, SL_IDENTIF, SL_EFR "
    Report.SQLQuery = Report.SQLQuery & " WHERE SL_CR_DIVIDAS.SEQ_UTENTE = SL_IDENTIF.SEQ_UTENTE "
    Report.SQLQuery = Report.SQLQuery & " AND SL_CR_DIVIDAS.COD_EFR = SL_EFR.COD_EFR "
    Report.SQLQuery = Report.SQLQuery & " AND SL_CR_DIVIDAS.NOME_COMPUTADOR =  " & BL_TrataStringParaBD(gComputador)
    
      
    
    Call BL_ExecutaReport
    
    ' ---------------------------
    ' APAGA TABELA TEMPORARIA
    ' ---------------------------
    sSql = " DELETE FROM sl_cr_dividas WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
End Sub


' --------------------------------------------------------------------------------------------------

' DETERMINA SE RELATORIO E FINAL OU PARCIAL

' --------------------------------------------------------------------------------------------------
Function Determina_Relatorio_Final_Parcial(NReq As String, EstadoReq As String) As String

    On Error GoTo TrataErro
    
    Dim EstadoReqTmp As String
    Dim RsEstado As ADODB.recordset
    Dim sql As String
    
    If EstadoReq = "" Then
        If NReq = "" Then
            NReq = "-1"
        End If
        sql = "SELECT estado_req FROM sl_requis WHERE n_req=" & NReq
        Set RsEstado = New ADODB.recordset
        RsEstado.CursorLocation = adUseServer
        RsEstado.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsEstado.Open sql, gConexao
        If RsEstado.RecordCount > 0 Then
            EstadoReqTmp = BL_HandleNull(RsEstado!estado_req, "0")
            If EstadoReqTmp = gEstadoReqTodasImpressas Or EstadoReqTmp = gEstadoReqValicacaoMedicaCompleta Or EstadoReqTmp = gEstadoReqHistorico Then
                Determina_Relatorio_Final_Parcial = "Null"
            Else
                Determina_Relatorio_Final_Parcial = "'S'"
            End If
        End If
        RsEstado.Close
        Set RsEstado = Nothing
    Else
        If EstadoReq = gEstadoReqValicacaoMedicaCompleta Or EstadoReq = gEstadoReqTodasImpressas Or EstadoReq = gEstadoReqHistorico Then
            Determina_Relatorio_Final_Parcial = "Null"
        Else
            Determina_Relatorio_Final_Parcial = "'S'"
        End If
    End If
    
    Exit Function
    
TrataErro:
    Exit Function
    Resume Next

End Function


' --------------------------------------------------------------------------------------------------

' IMPRESSAO DE CARTOES COM GRUPO SANGUINEO

' --------------------------------------------------------------------------------------------------
Public Function BL_ImprimeCartao(NomeImpressora As String, NomeUtente As String, DataNasc As String, SeqUtente As Long, _
                            TipoUtente As String, Utente As String, MBarcode As Object, MostraMgm As Boolean, NBenef As String) As Boolean
    Dim i As Long
    Dim rs As New ADODB.recordset
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    Dim sSql As String
    Dim teste As String
    Dim Result As Integer
    Dim AnaliseGrupo As String
    Dim AnaliseRH As String
    
    For i = 0 To Printers.Count - 1
        If Printers(i).DeviceName = NomeImpressora Then
            Set Printer = Printers(i)
            Exit For
        End If
    Next
    
    AnaliseGrupo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseGrupoSanguineo")
    AnaliseRH = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseRhSanguineo")
    
    
    
    Dim Mg As Long
    Dim Y As Long
    If gLAB = "HSS" Then
        Mg = 1000
    Else
        Mg = 250
    End If
    
    Printer.FontName = "Arial"
    Printer.FontSize = 10
    Printer.CurrentX = Mg
    If gLAB = "HSS" Then
        Printer.CurrentY = 1420
    Else
        Printer.CurrentY = 1300
    End If
    Y = Printer.CurrentY
    Printer.Print StrConv(NomeUtente, vbProperCase)
    Printer.CurrentY = Printer.CurrentY + (Printer.CurrentY - Y) / 2
    Y = Printer.CurrentY
    If gLAB = "HSS" Then
        Y = 1920
        Printer.CurrentY = Y
        If NBenef <> "" Then
            Printer.CurrentX = Mg
            Printer.Print NBenef
        End If
    Else
        If DataNasc <> "" Then
            Printer.CurrentX = Mg
            Printer.Print Format(CDate(DataNasc), "dd/mm/yyyy")
        End If
    End If
    
   
    If gImprCartoesInscritos <> mediSim Then
       If AnaliseRH <> "-1" Then
             sSql = "SELECT sl_res_alfan1.result resultgrupo , sl_res_alfan2.result resultrh, sl_realiza1.dt_chega " & _
                     " FROM sl_realiza sl_realiza1, sl_realiza sl_realiza2, sl_res_alfan sl_res_alfan1, sl_res_alfan sl_res_alfan2 " & _
                     " WHERE sl_realiza1.seq_realiza = sl_res_alfan1.seq_realiza " & _
                     " and sl_realiza1.cod_ana_s = " & BL_TrataStringParaBD(AnaliseGrupo) & _
                     " and sl_realiza1.seq_utente = " & CStr(val(SeqUtente)) & _
                     " and sl_realiza2.seq_realiza = sl_res_alfan2.seq_realiza " & _
                     " and sl_realiza2.cod_ana_s = " & BL_TrataStringParaBD(AnaliseRH) & _
                     " and sl_realiza2.seq_utente = " & CStr(val(SeqUtente))
             sSql = sSql & " UNION SELECT sl_res_alfan3.result resultgrupo , sl_res_alfan4.result resultrh, sl_realiza3.dt_chega " & _
                     " FROM sl_realiza_h sl_realiza3, sl_realiza_h sl_realiza4, sl_res_alfan_h sl_res_alfan3, sl_res_alfan_h sl_res_alfan4 " & _
                     " WHERE sl_realiza3.seq_realiza = sl_res_alfan3.seq_realiza " & _
                     " and sl_realiza3.cod_ana_s = " & BL_TrataStringParaBD(AnaliseGrupo) & _
                     " and sl_realiza3.seq_utente = " & CStr(val(SeqUtente)) & _
                     " and sl_realiza4.seq_realiza = sl_res_alfan4.seq_realiza " & _
                     " and sl_realiza4.cod_ana_s = " & BL_TrataStringParaBD(AnaliseRH) & _
                     " and sl_realiza4.seq_utente = " & CStr(val(SeqUtente))
            sSql = sSql & " ORDER by dt_chega desc "
        ElseIf AnaliseGrupo <> "-1" Then
            sSql = "SELECT result resultgrupo,q.dt_chega FROM sl_requis q, sl_realiza r, sl_res_alfan a  " + _
                 " WHERE r.n_req=q.n_req AND r.seq_realiza=a.seq_realiza AND " + _
                 " r.cod_perfil='0' " + _
                 " AND r.cod_ana_s in ('" + BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseGrupoSanguineo") + "')" + _
                 " AND q.seq_utente=" + CStr(val(SeqUtente))
            sSql = sSql & " UNION SELECT result resultgrupo,q.dt_chega FROM sl_requis q, sl_realiza_h r, sl_res_alfan_h a  " + _
                 " WHERE r.n_req=q.n_req AND r.seq_realiza=a.seq_realiza AND " + _
                 " r.cod_perfil='0' " + _
                 " AND r.cod_ana_s in ('" + BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseGrupoSanguineo") + "')" + _
                 " AND q.seq_utente=" + CStr(val(SeqUtente))
            sSql = sSql & " ORDER by dt_chega desc "
        End If
        rs.CursorType = adOpenStatic
        rs.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rs.Open sSql, gConexao
    
        If rs.RecordCount > 0 Then
            If gLAB = "HSS" Then
                Printer.CurrentY = Y
                Printer.CurrentX = Mg + 1800
                
                If BL_HandleNull(rs!resultrh, "") = "{Ver Observa��es}" Then
                    Printer.Print "" & BL_HandleNull(rs!RESULTGRUPO, "") & " " & BL_HandleNull(rs!obsrh, "");
                Else
                    Printer.Print "" & BL_HandleNull(rs!RESULTGRUPO, "") & " " & BL_HandleNull(rs!resultrh, "");
                End If
            Else
                Printer.CurrentY = 2400
                Printer.CurrentX = 1000
    
                Printer.Print "" & BL_HandleNull(rs!RESULTGRUPO, "");
            End If
        End If
        rs.Close
        Set rs = Nothing
    End If
    
    MBarcode.PrinterScaleMode = Printer.ScaleMode
    If gLAB = "HSS" Then
        Y = 500
        MBarcode.PrinterLeft = 1000
        MBarcode.PrinterTop = Y
    Else
        MBarcode.PrinterLeft = 2200
        MBarcode.PrinterTop = Y
    End If
    MBarcode.PrinterWidth = 2500
    MBarcode.PrinterHeight = 650
    If gLAB = "HSS" Then
        MBarcode.caption = left(Trim(Utente) + Space(7), 7)
    Else
        MBarcode.caption = left(Trim(TipoUtente), 1) + left(Trim(Utente) + Space(7), 7)
    End If
    MBarcode.PrinterHDC = Printer.hdc
    If gLAB = "HSS" Then
        Printer.CurrentX = 1800
        Printer.CurrentY = Y + 670
    Else
        Printer.CurrentX = 2900
        Printer.CurrentY = Y + 670
    End If
    Printer.FontName = "Arial"
    Printer.FontSize = 8
    Printer.Print Trim(TipoUtente) + "/" + Trim(Utente)
    Printer.EndDoc
    BL_ActualizaDataImprCartao TipoUtente, Utente

End Function

' --------------------------------------------------------------------------------------------------

' IMPRESSAO DE CARTOES COM GRUPO SANGUINEO

' --------------------------------------------------------------------------------------------------
Public Function BL_ImprimeCartaoEltron(NomeImpressora As String, NomeUtente As String, DataNasc As String, SeqUtente As Long, _
                            TipoUtente As String, Utente As String, MBarcode As Object, MostraMgm As Boolean, NBenef As String) As Boolean
    Dim i As Long
    Dim rs As New ADODB.recordset
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    Dim sSql As String
    Dim teste As String
    Dim Result As Integer
    Dim AnaliseGrupo As String
    Dim AnaliseRH As String
    
    
    If gImprCartoesInscritos <> mediSim Then
        
        AnaliseGrupo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseGrupoSanguineo")
        AnaliseRH = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseRhSanguineo")
        If AnaliseRH <> "-1" Then
             sSql = "SELECT sl_res_alfan1.result resultgrupo , sl_res_alfan2.result resultrh, sl_realiza1.dt_chega " & _
                     " FROM sl_realiza sl_realiza1, sl_realiza sl_realiza2, sl_res_alfan sl_res_alfan1, sl_res_alfan sl_res_alfan2 " & _
                     " WHERE sl_realiza1.seq_realiza = sl_res_alfan1.seq_realiza " & _
                     " and sl_realiza1.cod_ana_s = " & BL_TrataStringParaBD(AnaliseGrupo) & _
                     " and sl_realiza1.seq_utente = " & CStr(val(SeqUtente)) & _
                     " and sl_realiza2.seq_realiza = sl_res_alfan2.seq_realiza " & _
                     " and sl_realiza2.cod_ana_s = " & BL_TrataStringParaBD(AnaliseRH) & _
                     " and sl_realiza2.seq_utente = " & CStr(val(SeqUtente)) & _
                     " AND sl_res_alfan2.n_res = 1 "
             sSql = sSql & " UNION SELECT sl_res_alfan3.result resultgrupo , sl_res_alfan4.result resultrh, sl_realiza3.dt_chega " & _
                     " FROM sl_realiza_h sl_realiza3, sl_realiza_h sl_realiza4, sl_res_alfan_h sl_res_alfan3, sl_res_alfan_h sl_res_alfan4 " & _
                     " WHERE sl_realiza3.seq_realiza = sl_res_alfan3.seq_realiza " & _
                     " and sl_realiza3.cod_ana_s = " & BL_TrataStringParaBD(AnaliseGrupo) & _
                     " and sl_realiza3.seq_utente = " & CStr(val(SeqUtente)) & _
                     " and sl_realiza4.seq_realiza = sl_res_alfan4.seq_realiza " & _
                     " and sl_realiza4.cod_ana_s = " & BL_TrataStringParaBD(AnaliseRH) & _
                     " and sl_realiza4.seq_utente = " & CStr(val(SeqUtente)) & _
                     " AND sl_res_alfan4.n_res = 1 "
            sSql = sSql & " ORDER by dt_chega desc "
         ElseIf AnaliseGrupo <> "-1" Then
             sSql = "SELECT result resultgrupo,q.dt_chega FROM sl_requis q, sl_realiza r, sl_res_alfan a  " + _
                  " WHERE r.n_req=q.n_req AND r.seq_realiza=a.seq_realiza AND " + _
                  " r.cod_perfil='0' " + _
                  " AND r.cod_ana_s in ('" + BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseGrupoSanguineo") + "')" + _
                  " AND q.seq_utente=" + CStr(val(SeqUtente))
             sSql = sSql & " UNION SELECT result resultgrupo,q.dt_chega FROM sl_requis q, sl_realiza_h r, sl_res_alfan_h a  " + _
                  " WHERE r.n_req=q.n_req AND r.seq_realiza=a.seq_realiza AND " + _
                  " r.cod_perfil='0' " + _
                  " AND r.cod_ana_s in ('" + BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseGrupoSanguineo") + "')" + _
                  " AND q.seq_utente=" + CStr(val(SeqUtente))
            sSql = sSql & " ORDER by dt_chega desc "
         End If
        rs.CursorType = adOpenStatic
        rs.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rs.Open sSql, gConexao
    
        If rs.RecordCount > 0 Then
            If Not BL_LerEtiqIni("CARTAO.INI") Then
                MsgBox "Ficheiro de inicializa��o do Cart�o ausente!"
                Exit Function
            End If
            If Not BL_EtiqOpenPrinter(NomeImpressora) Then
                MsgBox "Imposs�vel abrir impressora Cart�es."
                Exit Function
            End If
            
            Call BL_EtiqPrintCartoes(Utente, NomeUtente, BL_HandleNull(rs!RESULTGRUPO, "") & " " & BL_HandleNull(rs!resultrh, ""))
            
            BL_EtiqClosePrinter
        
        End If
        rs.Close
        Set rs = Nothing
    Else
        If Not BL_LerEtiqIni("CARTAO.INI") Then
            MsgBox "Ficheiro de inicializa��o do Cart�o ausente!"
            Exit Function
        End If
        If Not BL_EtiqOpenPrinter(NomeImpressora) Then
            MsgBox "Imposs�vel abrir impressora Cart�es."
            Exit Function
        End If
        
        Call BL_EtiqPrintCartoes(Utente, NomeUtente, "")
        
        BL_EtiqClosePrinter
        BL_ActualizaDataImprCartao TipoUtente, Utente
    End If
    
End Function

' --------------------------------------------------------------------------------------------------
' DEVOLVE ULTIMORESULTADO PARA DETERMINADA ANALISE
' --------------------------------------------------------------------------------------------------
Public Function BL_UltimoResultado(cod_ana_s As String, seq_utente As Long) As String
    Dim sSql As String
    Dim RsRes As New ADODB.recordset
    
    If Mid(cod_ana_s, 1, 1) <> "S" Then Exit Function
    
    sSql = "SELECT sl_realiza.dt_val, sl_res_alfan.result FROM sl_realiza, sl_res_alfan "
    sSql = sSql & " WHERE sl_realiza.seq_realiza = sl_res_alfan.seq_realiza"
    sSql = sSql & " AND (sl_realiza.flg_estado = '3' or sl_realiza.flg_estado = '4')"
    sSql = sSql & " AND sl_realiza.cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    sSql = sSql & " AND seq_utente = " & seq_utente
    sSql = sSql & " ORDER BY dt_val DESC"
    
    RsRes.CursorType = adOpenStatic
    RsRes.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRes.Open sSql, gConexao
    If RsRes.RecordCount >= 1 Then
        BL_UltimoResultado = BL_HandleNull(RsRes!Result, "") & " (" & BL_HandleNull(RsRes!dt_val, "") & ")"
    Else
        BL_UltimoResultado = ""
    End If
    RsRes.Close
    Set RsRes = Nothing
End Function



' -----------------------------------------------------------------------------------------------

' ATRIBUI OS RESULTADOS ANTERIORES ENVIADOS POR ARGUMENTO
' MAIS UM PARA EVITAR PROCEDURE TO LARGE

' -----------------------------------------------------------------------------------------------
Public Function BL_ResultadosAnteriores(cod_ana_s As String, res_ant1 As String, res_ant2 As String, res_ant3 As String, dt_res_ant1 As String, dt_res_ant2 As String, dt_res_ant3 As String, _
                                        local_ant1 As String, local_ant2 As String, local_ant3 As String, ByRef ResAlf1_Ant1 As String, ByRef ResAlf1_Ant2 As String, _
                                        ByRef ResAlf1_Ant3 As String, ByRef Dt_ResAlf1_Ant1 As String, ByRef Dt_ResAlf1_Ant2 As String, ByRef Dt_ResAlf1_Ant3 As String, _
                                        ByRef LocalRes_Ant1 As String, ByRef LocalRes_Ant2 As String, ByRef LocalRes_Ant3 As String, seq_utente As Long, InibeHistoricoReq As Boolean)

    On Error GoTo TrataErro
    Dim CodFrase As String
    
    If BL_AnaSemHistorico(cod_ana_s) = True Or BL_DoenteNaoIdentificado(seq_utente) = True Or InibeHistoricoReq = True Then
        ResAlf1_Ant1 = ""
        ResAlf1_Ant2 = ""
        ResAlf1_Ant3 = ""
        Dt_ResAlf1_Ant1 = ""
        Dt_ResAlf1_Ant2 = ""
        Dt_ResAlf1_Ant3 = ""
        LocalRes_Ant1 = ""
        LocalRes_Ant2 = ""
        LocalRes_Ant3 = ""
        Exit Function
    End If
    
    If IsNull(res_ant1) Or res_ant1 = "" Then
        ResAlf1_Ant1 = ""
    Else
        ResAlf1_Ant1 = Trim(res_ant1)
    End If
    If InStr(1, ResAlf1_Ant1, "\\") <> 0 Then
        CodFrase = UCase(Mid(ResAlf1_Ant1, 3, Len(ResAlf1_Ant1) - 2))
        ResAlf1_Ant1 = BL_HandleNull(BL_SelCodigo("SL_DICIONARIO", "DESCR_FRASE", "COD_FRASE", CodFrase, "V"), Trim(res_ant1))
    End If
    If IsNull(res_ant2) Or res_ant2 = "" Then
        ResAlf1_Ant2 = ""
    Else
        ResAlf1_Ant2 = Trim(res_ant2)
    End If
    If InStr(1, ResAlf1_Ant2, "\\") <> 0 Then
        CodFrase = UCase(Mid(ResAlf1_Ant2, 3, Len(ResAlf1_Ant2) - 2))
        ResAlf1_Ant2 = BL_HandleNull(BL_SelCodigo("SL_DICIONARIO", "DESCR_FRASE", "COD_FRASE", CodFrase, "V"), Trim(res_ant2))
    End If
    If IsNull(res_ant3) Or res_ant3 = "" Then
        ResAlf1_Ant3 = ""
    Else
        ResAlf1_Ant3 = Trim(res_ant3)
    End If
    If InStr(1, ResAlf1_Ant3, "\\") <> 0 Then
        CodFrase = UCase(Mid(ResAlf1_Ant3, 3, Len(ResAlf1_Ant3) - 2))
        ResAlf1_Ant3 = BL_HandleNull(BL_SelCodigo("SL_DICIONARIO", "DESCR_FRASE", "COD_FRASE", CodFrase, "V"), Trim(res_ant3))
    End If
    If IsNull(dt_res_ant1) Or dt_res_ant1 = "" Then
        Dt_ResAlf1_Ant1 = ""
    Else
        Dt_ResAlf1_Ant1 = Trim(dt_res_ant1)
    End If
    If IsNull(dt_res_ant2) Or dt_res_ant2 = "" Then
        Dt_ResAlf1_Ant2 = ""
    Else
        Dt_ResAlf1_Ant2 = Trim(dt_res_ant2)
    End If
    If IsNull(dt_res_ant3) Or dt_res_ant3 = "" Then
        Dt_ResAlf1_Ant3 = ""
    Else
        Dt_ResAlf1_Ant3 = Trim(dt_res_ant3)
    End If
    If IsNull(local_ant1) Or local_ant1 = "" Then
        LocalRes_Ant1 = ""
    Else
        LocalRes_Ant1 = Trim(local_ant1)
    End If
    If IsNull(local_ant2) Or local_ant2 = "" Then
        LocalRes_Ant2 = ""
    Else
        LocalRes_Ant2 = Trim(local_ant2)
    End If
    If IsNull(local_ant3) Or local_ant3 = "" Then
        LocalRes_Ant3 = ""
    Else
        LocalRes_Ant3 = Trim(local_ant3)
    End If
Exit Function
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "BL_ResultadosAnteriores", Err.Number, Err.Description, "Rotina dentro da BL_ImprimeResultados"
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' DADO UMA ANALISE RETORNA O C�DIGO A MARCAR

' -----------------------------------------------------------------------------------------------
Public Function BL_RetornaCodigoMarcar(codAnalise As String, ByRef CodFacturavel As String) As String
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    CodFacturavel = codAnalise
    
    If InStr(1, "SCP", Mid(codAnalise, 1, 1)) > 0 Then
        sSql = "SELECT * FROM sl_ana_map WHERE cod_analise = " & BL_TrataStringParaBD(codAnalise)
    Else
        If gSGBD = gOracle Then
            sSql = "SELECT * FROM sl_ana_map WHERE substr(cod_analise,2,5) = " & BL_TrataStringParaBD(codAnalise)
        ElseIf gSGBD = gSqlServer Or gSGBD = gPostGres Then
            sSql = "SELECT * FROM sl_ana_map WHERE substring(cod_analise,2,5) = " & BL_TrataStringParaBD(codAnalise)
        End If
    End If
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    BL_RetornaCodigoMarcar = codAnalise
    If rsAna.RecordCount > 0 Then
        BL_RetornaCodigoMarcar = BL_HandleNull(rsAna!cod_marcar, "")
        CodFacturavel = BL_HandleNull(rsAna!cod_analise, "")
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "BL_RetornaCodigoMarcar", Err.Number, Err.Description, ""
    Exit Function
    Resume Next
End Function



' -----------------------------------------------------------------------------------------------

' DADO UMA ANALISE RETORNA O C�DIGO A MARCAR

' -----------------------------------------------------------------------------------------------
Public Sub BL_DevolveObsAnaReq(NumReq As Long, codAgrup As String, ByRef ObsAnaReq As String, _
                               ByRef seqObsAnaReq As Long, Optional cod_gr_nota As String)
    Dim sSql As String
    Dim rsOBS As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_obs_ana_req WHERE n_req = " & BL_TrataStringParaBD(CStr(NumReq))
    sSql = sSql & " AND (flg_invisivel IS NULL OR flg_invisivel = 0 )"
    If codAgrup <> "" Then
        sSql = sSql & " AND cod_agrup = " & BL_TrataStringParaBD(codAgrup)
    End If
    If cod_gr_nota <> "" Then
        sSql = sSql & " AND cod_gr_nota = " & cod_gr_nota
    End If
    rsOBS.CursorType = adOpenStatic
    rsOBS.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsOBS.Open sSql, gConexao
    ObsAnaReq = ""
    seqObsAnaReq = 0
    If rsOBS.RecordCount > 0 Then
        ObsAnaReq = BL_HandleNull(rsOBS!descr_obs, "")
        seqObsAnaReq = BL_HandleNull(rsOBS!seq_obs, 0)
    End If
    rsOBS.Close
    Set rsOBS = Nothing
Exit Sub
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "BL_DevolveObsAnaReq", Err.Number, Err.Description, ""
    Exit Sub
    Resume Next
End Sub



' -----------------------------------------------------------------------------------------------

' GRAVA OBSERVACOES DAS ANALISES DE UMA REQUISICAO NA TABELA SL_OBS_ANA_REQ

' -----------------------------------------------------------------------------------------------
Public Sub BL_GravaObsAnaReq(NumReq As String, codAgrup As String, ByVal ObsAnaReq As String, ByRef seqObsAnaReq As Long, _
                             cod_gr_nota As Integer, seq_utente As Long, seq_req_tubo As Long)
    Dim sSql As String
    Dim rsOBS As New ADODB.recordset
    On Error GoTo TrataErro
    
    If Trim(ObsAnaReq) = "" And seqObsAnaReq > 0 Then
        sSql = "DELETE FROM sl_obs_ana_Req WHERE seq_obs = " & seqObsAnaReq
        BG_ExecutaQuery_ADO sSql
        BG_Trata_BDErro
    ElseIf Trim(ObsAnaReq) = "" Then
        Exit Sub
    ElseIf Trim(codAgrup) <> "" And (NumReq = "" Or NumReq = "0") Then
        Exit Sub
    ElseIf Trim(codAgrup) <> "" And (NumReq <> "" And NumReq <> "0") And seqObsAnaReq <= 0 Then
        sSql = "DELETE FROM sl_obs_ana_Req WHERE n_Req = " & NumReq & " AND cod_agrup = " & BL_TrataStringParaBD(codAgrup)
        BG_ExecutaQuery_ADO sSql
        If seq_req_tubo > mediComboValorNull Then
            sSql = "UPDATE sl_req_tubo set flg_nota = 0 WHERE seq_req_tubo = " & seq_req_tubo
            BG_ExecutaQuery_ADO sSql
        End If
        BG_Trata_BDErro
    End If
    
    If seqObsAnaReq > 0 And NumReq <> "" Then
        sSql = "UPDATE sl_obs_ana_Req SET descr_obs = " & BL_TrataStringParaBD(ObsAnaReq)
        sSql = sSql & " WHERE seq_obs = " & seqObsAnaReq
        BG_ExecutaQuery_ADO sSql
        BG_Trata_BDErro
    ElseIf seq_utente <> 0 Then
        If gSGBD = gOracle Then
            sSql = "INSERT INTO sl_obs_ana_Req (seq_obs,descr_obs,n_req, cod_agrup, user_cri, dt_cri, hr_cri, cod_gr_nota, seq_utente, seq_req_tubo) VALUES("
            sSql = sSql & " SEQ_OBS_ANA_REQ.NEXTVAL, "
            sSql = sSql & BL_TrataStringParaBD(ObsAnaReq) & ", "
            sSql = sSql & BL_HandleNull(NumReq, "null") & ", "
            sSql = sSql & BL_TrataStringParaBD(codAgrup) & ", "
            sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", "
            sSql = sSql & BL_HandleNull(cod_gr_nota, 0) & ", " & seq_utente & "," & BL_TrataNumberParaBD(CStr(seq_req_tubo)) & " ) "
            BG_ExecutaQuery_ADO sSql
            
            If seq_req_tubo > mediComboValorNull Then
                sSql = "UPDATE sl_req_tubo set flg_nota = 1 WHERE seq_req_tubo = " & seq_req_tubo
                BG_ExecutaQuery_ADO sSql
            End If
        End If
        BG_Trata_BDErro
        sSql = "SELECT * FROM sl_obs_ana_req WHERE seq_utente = " & seq_utente
        If NumReq <> "" Then
            sSql = sSql & " AND n_req = " & NumReq
        End If
        If codAgrup <> "" Then
            sSql = sSql & " AND cod_agrup = " & BL_TrataStringParaBD(codAgrup)
        End If
        rsOBS.CursorType = adOpenStatic
        rsOBS.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsOBS.Open sSql, gConexao
        If rsOBS.RecordCount > 0 Then
            seqObsAnaReq = BL_HandleNull(rsOBS!seq_obs, 0)
        End If
        rsOBS.Close
        Set rsOBS = Nothing
    End If
Exit Sub
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "BL_GravaObsAnaReq", Err.Number, Err.Description, ""
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------------------------------------

' IMPRIMIR CARTAO ( ABRIR FICHEIRO)

' -----------------------------------------------------------------------------------------------
Public Function BL_LerEtiqIni(ficheiro As String) As Boolean
    Dim aux As String
    On Error GoTo erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\" & ficheiro
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\" & ficheiro
    End If

    If gImprimeEPL <> 1 And ficheiro <> "CARTAO.INI" Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        BL_LerEtiqIni = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While aux <> "P1" And aux <> "P{QUANTIDADE}" And aux <> "P{NUM_COPIAS}"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            If gInstituicao <> "LHL" Then
                EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Else
                EtiqEndJob = EtiqEndJob & vbCrLf
            End If
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    BL_LerEtiqIni = True
    aux = ""
    Exit Function
erro:
Close #1
Exit Function
Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' IMPRIMIR CARTAO

' -----------------------------------------------------------------------------------------------
Public Function BL_TrocaBinarios(ByVal s As String) As String
    
    Dim p As Long
    Dim k As Byte
    
    Do
        p = InStr(s, "&H")
        If p = 0 Then Exit Do
        k = val(Mid(s, p, 4))
        s = left(s, p - 1) + Chr(k) + Mid(s, p + 4)
    Loop
    BL_TrocaBinarios = s

End Function

' -----------------------------------------------------------------------------------------------

' IMPRIMIR CARTAO - ABRIR IMPRESSORA

' -----------------------------------------------------------------------------------------------
Public Function BL_EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    
    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String
    Dim i As Integer
    
    For i = 0 To Printers.Count - 1
        If InStr(1, Printers(i).DeviceName, PrinterName) > 0 Then
            PrinterName = Printers(i).DeviceName
            Exit For
        End If
    Next i
    
    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function
    
    MyDocInfo.pDocName = "Etiquetas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function
    
    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    sWrittenData = BL_TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    
    BL_EtiqOpenPrinter = True

End Function

' -----------------------------------------------------------------------------------------------

' IMPRIMIR CARTAO - FECHAR IMPRESSORA

' -----------------------------------------------------------------------------------------------
Public Function BL_EtiqClosePrinter() As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = BL_TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    BL_EtiqClosePrinter = True

End Function

Public Function BL_EtiqSecPrint( _
    ByVal t_utente As String, _
    ByVal Utente As String, _
    ByVal n_req As String, _
    ByVal nome_ute As String, _
    ByVal N_req_bar As String, _
    ByVal Designacao_tubo As String, _
    ByVal Copias As Integer, _
    ByVal abr_ana As String, _
    ByVal t_urg As String, _
    ByVal n_proc_1 As String, _
    ByVal n_proc_2 As String, _
    ByVal dt_chega As String, _
    ByVal descr_tubo_primario As String, _
    ByVal etiqueta As String _
    ) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{T_UTENTE}", t_utente)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", BL_RemovePortuguese(StrConv(nome_ute, vbProperCase)))
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", N_req_bar)
    sWrittenData = Replace(sWrittenData, "{DS_TUBO}", Designacao_tubo)
    sWrittenData = Replace(sWrittenData, "{DT_REQ}", dt_chega)
    sWrittenData = Replace(sWrittenData, "{DT_IMP}", Bg_DaData_ADO)
    sWrittenData = Replace(sWrittenData, "{HR_IMP}", Bg_DaHora_ADO)

    sWrittenData = Replace(sWrittenData, "{GR_ANA}", "")
    sWrittenData = Replace(sWrittenData, "{ABR_ANA}", abr_ana)
    sWrittenData = Replace(sWrittenData, "{N_PROC}", n_proc_1 & " " & n_proc_2)
    If t_urg = "1" Then
        sWrittenData = Replace(sWrittenData, "{T_URG}", "U")
    Else
        sWrittenData = Replace(sWrittenData, "{T_URG}", "N")
    End If
    sWrittenData = Replace(sWrittenData, "{PRODUTO}", "")
    sWrittenData = Replace(sWrittenData, "{NOME1}", "")
    sWrittenData = Replace(sWrittenData, "{NOME2}", "")
    sWrittenData = Replace(sWrittenData, "{NOME3}", "")
    sWrittenData = Replace(sWrittenData, "{ABREV_UTE}", "")
    sWrittenData = Replace(sWrittenData, "{N_REQ_ARS}", "")
    sWrittenData = Replace(sWrittenData, "{INF_COMPLEMENTAR}", "")
    sWrittenData = Replace(sWrittenData, "{DESCR_ETIQ_ANA}", "")
    sWrittenData = Replace(sWrittenData, "{QUANTIDADE}", Copias)
    sWrittenData = Replace(sWrittenData, "{NUM_COPIAS}", Copias)
    sWrittenData = Replace(sWrittenData, "{COD_ETIQ}", "")
    sWrittenData = Replace(sWrittenData, "{DESCR_TUBO_PRIMARIO}", descr_tubo_primario)
    If gAtiva_Nova_Numeracao_Tubo = mediSim Then
        sWrittenData = Replace(sWrittenData, "{ETIQUETA}", etiqueta)
    End If
    If gModoDebug = mediSim Then BG_LogFile_Erros sWrittenData

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    BL_EtiqSecPrint = True
    If gImprimeEPL = mediSim Then
        EtiqEndJob = Replace(EtiqEndJob, "{NUM_COPIAS}", Copias)
        EtiqEndJob = Replace(EtiqEndJob, "{QUANTIDADE}", Copias)
    End If

End Function


' -----------------------------------------------------------------------------------------------

' IMPRIMIR CARTOES - IMPRIMIR

' -----------------------------------------------------------------------------------------------
Public Function BL_EtiqPrintCartoes( _
    ByVal Utente As String, _
    ByVal nome_utente As String, _
    ByVal Grupo As String _
    ) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String


    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", BL_RemovePortuguese(StrConv(nome_utente, vbProperCase)))
    sWrittenData = Replace(sWrittenData, "{GRUPO}", Grupo)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    BL_EtiqPrintCartoes = True

End Function

' -----------------------------------------------------------------------------------------------

' IMPRIMIR CARTOES - REMOVER ACENTOS.

' -----------------------------------------------------------------------------------------------
Public Function BL_RemovePortuguese(ByVal s As String) As String
    
    Dim r As String
    
    Do While s <> ""
        Select Case left(s, 1)
            Case "�", "�", "�", "�"
                r = r + "a"
            Case "�", "�", "�", "�"
                r = r + "A"
            Case "�", "�", "�"
                r = r + "e"
            Case "�", "�", "�"
                r = r + "E"
            Case "�", "�", "�"
                r = r + "i"
            Case "�", "�", "�", "�"
                r = r + "o"
            Case "�", "�", "�", "�"
                r = r + "O"
            Case "�", "�", "�"
                r = r + "u"
            Case "�", "�", "�"
                r = r + "U"
            Case "�"
                r = r + "c"
            Case "�"
                r = r + "C"
            Case Else
                r = r + left(s, 1)
        End Select
        s = Mid(s, 2)
    Loop
    
    BL_RemovePortuguese = r

End Function



Function BL_ExecutaQueryADO_Interfaces(SQLQuery As String, Optional iMensagem) As Integer

    ' O argumento 'iMensagem' serve para que seja apresentada uma mensagem caso a
    ' opera��o que se pretende n�o seja efectuada.
    '
    '   Hip�teses:
    '       <Missing>     -> Aparece uma MsgBox
    '       mediMsgBox    -> Aparece uma MsgBox
    '       mediMsgStatus -> Aparece uma MsgStatus
    '       -1            -> N�o aparece mensagem

    Dim sMsg As String
    Dim iRes As Integer
    Dim registos As Integer
    
    gSQLError = 0
    gSQLISAM = 0
    On Error GoTo TrataErro
    
    gConexaoInterfaces.Execute SQLQuery, registos, adCmdText + adExecuteNoRecords
        
    If (registos <= 0) Then
    
        iRes = -1
        If gModoDebug = 1 Then
            BL_LogFile_BD "BibliotecaLocal", "Bl_ExectaQueryAdo_Interfaces", Err.Number, Err.Description, SQLQuery
            sMsg = "Esta opera��o pode n�o ter sido efectuada." & vbCrLf & vbCrLf & "Raz�o prov�vel: Integridade dos Dados."
        End If
        If IsMissing(iMensagem) Then
            If gModoDebug = 1 Then BG_Mensagem mediMsgBox, sMsg, vbExclamation, "..."
        Else
            If iMensagem = mediMsgBox Then
                If gModoDebug = 1 Then BG_Mensagem mediMsgBox, sMsg, vbExclamation, "..."
            ElseIf iMensagem = mediMsgStatus Then
                If gModoDebug = 1 Then BG_Mensagem mediMsgStatus, sMsg, mediMsgBeep + 10
            ElseIf iMensagem = -1 Then
                ' N�o d� mensagem.
            End If
        End If
    Else
        iRes = registos
        
    End If
    BL_ExecutaQueryADO_Interfaces = iRes
    Exit Function

TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "Bl_ExectaQueryAdo_Interfaces", Err.Number, Err.Description, SQLQuery
    If IsMissing(iMensagem) Then
        BL_LogFile_BD "BibliotecaLocal", "Bl_ExectaQueryAdo_Interfaces", Err.Number, Err.Description, SQLQuery
    Else
        If iMensagem <> -1 Then BG_Trata_BDErro
    End If
    BL_ExecutaQueryADO_Interfaces = -1
    Resume Next
    Exit Function
End Function


' -----------------------------------------------------------------------------------------------

' FUNCAO QUE ASSINA REQUISICAO - INSERE NA TABELA SL_REQ_ASSINATURA

' -----------------------------------------------------------------------------------------------
Public Sub BL_AssinaRequisicao(NumReq As String)
    Dim sSql As String
    On Error GoTo TrataErro
    
    sSql = "INSERT INTO SL_REQ_ASSINATURA(n_req, user_cri, dt_cri, hr_cri, flg_res_mv) VALUES("
    sSql = sSql & NumReq & ", "
    sSql = sSql & BL_TrataStringParaBD(str(gCodUtilizador)) & ", "
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
    sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ",0) "
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
Exit Sub
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "BL_AssinaRequisicao", Err.Number, Err.Description, sSql
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------

' VERIFICA SE A REQUISICAO ESTA ASSINADA

' -----------------------------------------------------------------------------------------------
Public Function BL_VerificaRequisAssinada(NumReq As String) As Boolean
    Dim sSql As String
    Dim rsAssina As New ADODB.recordset
    
    On Error GoTo TrataErro
    If gAssinaturaActivo <> mediSim Or Trim(NumReq) = "" Then
        BL_VerificaRequisAssinada = False
        Exit Function
    End If
    sSql = "SELECT * FROM sl_req_assinatura WHERE n_req = " & NumReq
    rsAssina.CursorType = adOpenStatic
    rsAssina.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAssina.Open sSql, gConexao
    If rsAssina.RecordCount > 0 Then
        BL_VerificaRequisAssinada = True
    Else
        BL_VerificaRequisAssinada = False
    End If
    rsAssina.Close
    Set rsAssina = Nothing
    
Exit Function
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "BL_VerificaRequisAssinada", Err.Number, Err.Description, sSql
    Exit Function
    Resume Next
End Function


' -----------------------------------------------------------------------------------------------

' VERIFICA SE A REQUISICAO ESTA COMPLETA

' -----------------------------------------------------------------------------------------------
Public Function BL_VerificaRequisCompleta(NumReq As String) As Boolean
    Dim sSql As String
    Dim rsAssina As New ADODB.recordset
    
    On Error GoTo TrataErro
    
    sSql = "SELECT n_req FROM sl_marcacoes WHERE n_req = " & NumReq
    sSql = sSql & " UNION SELECT n_req from sl_realiza WHERE  n_req = " & NumReq
    If gAssinaturaActivo = mediSim Then
        sSql = sSql & " AND flg_assinado <> 1 "
    Else
        sSql = sSql & " AND flg_estado not in (" & BL_TrataStringParaBD(gEstadoAnaValidacaoMedica) & ", "
        sSql = sSql & BL_TrataStringParaBD(gEstadoAnaImpressa) & ")"
    End If
    rsAssina.CursorType = adOpenStatic
    rsAssina.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAssina.Open sSql, gConexao
    If rsAssina.RecordCount > 0 Then
        BL_VerificaRequisCompleta = False
    Else
        BL_VerificaRequisCompleta = True
    End If
    rsAssina.Close
    Set rsAssina = Nothing
    
Exit Function
TrataErro:
    BL_VerificaRequisCompleta = False
    BL_LogFile_BD "BibliotecaLocal", "BL_VerificaRequisCompleta", Err.Number, Err.Description, sSql
    Exit Function
    Resume Next
End Function

Public Function BL_DevolveTUrg(cod_t_urg As String) As String
    Dim i As Integer
    For i = 1 To UBound(gEstrutTUrg)
        If Trim(cod_t_urg) = Trim(gEstrutTUrg(i).codigo) Then
            BL_DevolveTUrg = BL_HandleNull(gEstrutTUrg(i).descricao, "")
            Exit Function
        End If
    Next
End Function

Public Function BL_DevolveCorTURG(cod_t_urg As String) As Long
    Dim i As Integer
    For i = 1 To UBound(gEstrutTUrg)
        If Trim(cod_t_urg) = Trim(gEstrutTUrg(i).codigo) Then
            BL_DevolveCorTURG = BL_HandleNull(gEstrutTUrg(i).cor, vbWhite)
            Exit Function
        End If
    Next
End Function

' pferreira 2007.10.24
' Devolve a cor correspondente ao tipo de urgencia.
Public Function BL_DevolveCorTipoUrgencia(str_cor As String) As String
    
    Dim RsCor As ADODB.recordset
    Dim sql As String
    
    BL_DevolveCorTipoUrgencia = "&H0"
    Set RsCor = New ADODB.recordset
    sql = "select cod_cor from sl_map_cores where upper(str_cor) = " & BL_TrataStringParaBD(UCase(str_cor))
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsCor.Open sql, gConexao, adOpenStatic, adLockOptimistic
    If (RsCor.RecordCount > 0) Then
        BL_DevolveCorTipoUrgencia = BL_HandleNull(RsCor!cod_cor, "&H0")
    Else
        BL_DevolveCorTipoUrgencia = "&H0"
    End If
    If (RsCor.state = adStateOpen) Then: RsCor.Close
End Function

Public Function BL_DoenteNaoIdentificado(seq_utente As Long) As Boolean
    Dim sSql As String
    Dim nome As String
    Dim rsDoente As New ADODB.recordset
    sSql = "SELECT * FROM SL_IDENTIF WHERE SEQ_UTENTE = " & seq_utente
    BL_DoenteNaoIdentificado = False
    rsDoente.CursorType = adOpenStatic
    rsDoente.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDoente.Open sSql, gConexao
    If rsDoente.RecordCount > 0 Then
        nome = BL_HandleNull(rsDoente!nome_ute, "")
        If InStr(1, UCase(nome), "NAO IDENTIFICADO") > 1 Or InStr(1, UCase(nome), "N�O IDENTIFICADO") > 1 Then
            BL_DoenteNaoIdentificado = True
        ElseIf InStr(1, UCase(nome), "NAO IDENTIFICADA") > 1 Or InStr(1, UCase(nome), "N�O IDENTIFICADA") > 1 Then
            BL_DoenteNaoIdentificado = True
        End If
    End If
    rsDoente.Close
    Set rsDoente = Nothing
End Function

Public Function BL_Tokenize(str_in As String, _
                         Sep As String, _
                         saida() As String) As Integer

    On Error GoTo ErrorHandler
    
    Dim c As String
    Dim i As Integer
    Dim token As String
    
    ' Testa a string se entrada.
    If (Len(str_in) = 0) Then
        BL_Tokenize = -2
        Exit Function
    End If
    
    ' Testa o separador.
    If (Len(Sep) = 0) Then
        BL_Tokenize = -3
        Exit Function
    End If
    
    ReDim saida(0)
    
    Sep = left(Sep, 1)
    
    token = ""
    For i = 1 To Len(str_in)
    
        c = Mid(str_in, i, 1)
        
        Select Case c
            
            Case Sep
                
                
                    If (Len(token) > 0) Then
                    
                        ReDim Preserve saida(UBound(saida) + 1)
                        saida(UBound(saida) - 1) = Trim(token)
                        token = ""
                    
                    End If
            Case Else
                token = token & c
        
        End Select
    
    Next
                
    If (Len(token) > 0) Then
                
        ReDim Preserve saida(UBound(saida) + 1)
        saida(UBound(saida) - 1) = token
                
    End If
        
    ReDim Preserve saida(UBound(saida) - 1)

    BL_Tokenize = 1

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BL_Tokenize = -1
            Exit Function
    End Select
End Function

' pferreira 2007.11.12
Public Sub BL_VerificaMsgCorreio()
    
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim iRes As Integer
    
    Select Case (gVersaoModuloCorreio)
        Case "V1":
            sql = "select * from " & gTabelaCorreioUtil & " where cod_util = " & gCodUtilizador & " and cod_estado = 1"
        Case "V2":
            If (gSGBD = gOracle) Then: sql = "select * from " & gBD_PREFIXO_TAB & "mensagens_destinatarios where cod_util = " & gCodUtilizador & " and nvl(flg_lida,0) = 0 and cod_estado in (5,2) "
            If (gSGBD = gPostGres) Then: sql = "select * from " & gBD_PREFIXO_TAB & "mensagens_destinatarios where cod_util = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & " and COALESCE(flg_lida,0) = 0 and cod_estado in (5,2)"
            If (gSGBD = gSqlServer) Then: sql = "select * from " & gBD_PREFIXO_TAB & "mensagens_destinatarios where cod_util = " & gCodUtilizador & " and isnull(flg_lida,0) = 0 and cod_estado in (5,2)"
        Case Else:
            Exit Sub
    End Select
    
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sql, gConexao
    
    gMensagensNovas = False
    If (rs.RecordCount <> 0) Then
        gMensagensNovas = True: ShowBalloon MDIFormInicio.f_cSystray, TTIconInfo, cTextoNovasMensagens
    End If
    
End Sub

' pferreira 2010.09.28
Public Sub bl_AbreModuloCorreio(Optional versao_modulo_correio As String = "V2")
  
    FormMessages.Show
    FormMessages.TreeViewNavigation.Nodes.Item(ModuleMessages.MessageNavigationReceived).Selected = True
    Call FormMessages.TreeViewNavigation_NodeClick(FormMessages.TreeViewNavigation.Nodes.Item(ModuleMessages.MessageNavigationReceived))
    
End Sub

' -----------------------------------------------------------------------------------------------

' IMPRIME NOTAS DA REQUISI��O

' -----------------------------------------------------------------------------------------------

Public Sub BL_ImprimeNotasRequisicao(NumReq As String)
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim iRes As Integer
    
    sSql = "SELECT n_req, cod_agrup, user_cri, dt_cri, hr_cri, descr_obs, descr_ana FROM sl_obs_ana_req, slv_analises "
    sSql = sSql & " WHERE n_req = " & NumReq & " AND sl_obs_ana_req.cod_agrup = slv_analises.cod_ana (+) AND (sl_obs_ana_req.flg_invisivel IS NULL OR sl_obs_ana_req.flg_invisivel = 0 ) "
    
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
    
    If rs.RecordCount > 0 Then
        While Not rs.EOF
            sSql = "INSERT INTO sl_cr_notas(nome_computador, num_sessao, n_req, descr_ana, descr_obs, user_cri, dt_cri, hr_cri)"
            sSql = sSql & " VALUES ( " & BL_TrataStringParaBD(gComputador)
            sSql = sSql & ", " & gNumeroSessao
            sSql = sSql & ", " & NumReq
            sSql = sSql & ", " & BL_TrataStringParaBD(BL_HandleNull(rs!descr_ana, ""))
            sSql = sSql & ", " & BL_TrataStringParaBD(BL_HandleNull(rs!descr_obs))
            sSql = sSql & ", " & BL_TrataStringParaBD(BL_HandleNull(rs!user_cri))
            sSql = sSql & ", " & BL_TrataDataParaBD(BL_HandleNull(rs!dt_cri))
            sSql = sSql & ", " & BL_TrataStringParaBD(BL_HandleNull(rs!hr_cri)) & ")"
            BG_ExecutaQuery_ADO sSql
            rs.MoveNext
        Wend
    End If
    
End Sub


' -----------------------------------------------------------------------------------------------

' AVISOS PAGAMENTO

' -----------------------------------------------------------------------------------------------

Public Sub BL_AvisosPagamento(NumReq As String)
    Dim sSql As String
    Dim rsDividas As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT x1.cod_efr, x2.cod_facturavel, x2.taxa, x1.total_pagar, x3.cod_sala, x4.nome_ute FROM sl_recibos x1, sl_recibos_det x2, sl_requis x3, sl_identif x4 WHERE x3.n_req = " & NumReq & " AND x1.estado = " & BL_TrataStringParaBD(gEstadoReciboNaoEmitido)
    sSql = sSql & " AND x3.seq_utente = x4.seq_utente AND x1.total_pagar > 0 AND x1.n_req = x2.n_req AND x1.n_rec = x2.n_rec and x1.cod_efr = x2.cod_efr AND x1.n_req = x3.n_Req and (x2.flg_adicionada = 0 or x2.flg_adicionada is null) "
    sSql = sSql & " AND x3.cod_efr  IN (SELECT cod_efr FROM sl_efr where flg_imp_aviso_pag = 1) AND  x3.cod_sala IN (SELECT cod_sala FROM sl_cod_salas WHERE flg_colheita is null or flg_colheita = 0)"
    
    Set rsDividas = BG_ExecutaSELECT(sSql)
    
    If rsDividas.RecordCount >= 1 Then
        While Not rsDividas.EOF
            sSql = " INSERT INTO SL_CR_AVISOS_PAGAMENTO (nome_computador, num_sessao,n_req,cod_efr, cod_facturavel, taxa, total_pagar, cod_sala, nome_ute) VALUES( "
            sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
            sSql = sSql & gNumeroSessao & ", "
            sSql = sSql & NumReq & ", "
            sSql = sSql & BL_TrataStringParaBD(rsDividas!cod_efr) & ", "
            sSql = sSql & BL_TrataStringParaBD(rsDividas!cod_facturavel) & ", "
            sSql = sSql & Replace(rsDividas!taxa, ",", ".") & ", "
            sSql = sSql & Replace(rsDividas!total_pagar, ",", ".") & ", "
            sSql = sSql & BL_TrataStringParaBD(rsDividas!cod_sala) & ","
            sSql = sSql & BL_TrataStringParaBD(rsDividas!nome_ute) & ")"
            BG_ExecutaQuery_ADO sSql
            
            rsDividas.MoveNext
        Wend
    End If
    BG_DestroiRecordSet rsDividas
Exit Sub
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "BL_RegistaImprEtiq", Err.Number, Err.Description, sSql
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------------------------------------

' REGISTA IMPRESS�O DE ETIQUETAS DOS TUBOS

' -----------------------------------------------------------------------------------------------

Public Sub BL_RegistaImprEtiq(n_req As String, cod_tubo As String)
    Dim sSql As String
    Dim rsReqTubo As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_req_tubo WHERE n_req = " & n_req & " AND cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
    sSql = sSql & " AND dt_eliminacao IS NULL"
    
    Set rsReqTubo = BG_ExecutaSELECT(sSql)
    
    If rsReqTubo.RecordCount >= 1 Then
        If BL_HandleNull(rsReqTubo!dt_imp, "") <> "" Then
            sSql = "UPDATE sl_Req_tubo set dt_ult_imp = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & " hr_ult_imp = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", "
            sSql = sSql & " user_ult_imp = " & BL_TrataStringParaBD(CStr(gCodUtilizador))
            sSql = sSql & " WHERE n_req = " & n_req & " AND cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
            sSql = sSql & " AND dt_eliminacao IS NULL"
        Else
            sSql = "UPDATE sl_Req_tubo set dt_imp = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & " hr_imp = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", "
            sSql = sSql & " user_imp = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & " dt_ult_imp = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & " hr_ult_imp = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", "
            sSql = sSql & " user_ult_imp = " & BL_TrataStringParaBD(CStr(gCodUtilizador))
            sSql = sSql & " WHERE n_req = " & n_req & " AND cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
            sSql = sSql & " AND dt_eliminacao IS NULL"
        End If
        BG_ExecutaQuery_ADO sSql
        
    End If
    BG_DestroiRecordSet rsReqTubo

Exit Sub
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "BL_RegistaImprEtiq", Err.Number, Err.Description, sSql
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' VERIFICA O ESTADO DA IMPRESSAO DE ETIQUETAS(TODAS IMPRESSAS, NENHUMA IMPRESSA, ALGUMAS IMPRESSAS

' ------------------------------------------------------------------------------------------------

Public Function BL_EstadoEtiq(n_req As String) As String
    Dim sSql As String
    Dim semImpr As Integer
    Dim comImpr As Integer
    Dim rsReqTubo As New ADODB.recordset
    On Error GoTo TrataErro
    
    ' NUMERO DE TUBOS POR IMPRIMIR
    sSql = "SELECT count(*) conta_null FROM sl_req_tubo WHERE n_req = " & n_req & " AND dt_imp IS NULL "
    sSql = sSql & " AND dt_eliminacao IS NULL"
    Set rsReqTubo = BG_ExecutaSELECT(sSql)
    If rsReqTubo.RecordCount >= 1 Then
        semImpr = BL_HandleNull(rsReqTubo!conta_null, 0)
    End If
    BG_DestroiRecordSet rsReqTubo

    ' NUMERO DE TUBOS IMPRESSOS
    sSql = "SELECT count(*) conta FROM sl_req_tubo WHERE n_req = " & n_req & " AND dt_imp IS NOT NULL "
    sSql = sSql & " AND dt_eliminacao IS NULL"
    Set rsReqTubo = BG_ExecutaSELECT(sSql)
    If rsReqTubo.RecordCount >= 1 Then
        comImpr = BL_HandleNull(rsReqTubo!conta, 0)
    End If
    BG_DestroiRecordSet rsReqTubo


    If comImpr = 0 And semImpr = 0 Then
        BL_EstadoEtiq = gEstadoEtiqSemTubo
    ElseIf comImpr > 0 And semImpr > 0 Then
        BL_EstadoEtiq = gEstadoEtiqAlgumasImpressas
    ElseIf comImpr > 0 And semImpr = 0 Then
        BL_EstadoEtiq = gEstadoEtiqTodasImpressas
    ElseIf comImpr = 0 And semImpr > 0 Then
        BL_EstadoEtiq = gEstadoEtiqNenhumaImpressa
    End If
Exit Function
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "BL_EstadoEtiq", Err.Number, Err.Description, sSql
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' GRAVA LOCAIS ASSOCIADOS A ANALISE, SEJA PERFIS, COMPLEXAS OU SIMPLES

' ------------------------------------------------------------------------------------------------

Public Function BL_GravaLocaisAna(cod_ana As String, formAna As Form) As Boolean
    Dim sSql As String
    Dim i As Integer
    Dim RsLocal As New ADODB.recordset
    
    On Error GoTo TrataErro
    
    BL_GravaLocaisAna = False
    
    For i = 0 To formAna.EcAnaLocais.ListCount - 1
        If formAna.EcAnaLocais.Selected(i) = True Then
            sSql = " SELECT * FROM SL_ANA_LOCAIS WHERE COD_ANA = " & BL_TrataStringParaBD(cod_ana)
            sSql = sSql & " AND cod_local = " & formAna.EcAnaLocais.ItemData(i)
            RsLocal.CursorLocation = adUseServer
            RsLocal.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            RsLocal.Open sSql, gConexao
            If RsLocal.RecordCount >= 1 Then
                'nada
            Else
                sSql = "INSERT into sl_ana_locais (cod_ana, cod_local) VALUES("
                sSql = sSql & BL_TrataStringParaBD(cod_ana) & ","
                sSql = sSql & formAna.EcAnaLocais.ItemData(i) & ")"
                BG_ExecutaQuery_ADO sSql
            End If
            RsLocal.Close
            Set RsLocal = Nothing
        Else
            sSql = "DELETE FROM sl_ana_locais WHERE cod_ana = " & BL_TrataStringParaBD(cod_ana)
            sSql = sSql & " AND cod_local = " & formAna.EcAnaLocais.ItemData(i)
            BG_ExecutaQuery_ADO sSql
        
        End If
    Next
    BL_GravaLocaisAna = True
Exit Function
TrataErro:
    BL_GravaLocaisAna = False
    BG_LogFile_Erros "BibliotecaLocal: BL_GravaLocaisAna: " & Err.Description, "BL", "BL_GravaLocaisAna", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' GRAVA LOCAIS DE EXECUCAI ASSOCIADOS A ANALISE, SEJA PERFIS, COMPLEXAS OU SIMPLES

' ------------------------------------------------------------------------------------------------

Public Function BL_GravaLocaisAnaExec(cod_ana As String, formAna As Form) As Boolean
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    BL_GravaLocaisAnaExec = False
    sSql = "DELETE FROM sl_ana_locais_exec WHERE cod_ana = " & BL_TrataStringParaBD(cod_ana)
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To formAna.EcAnaLocaisExec.ListCount - 1
        If formAna.EcAnaLocaisExec.Selected(i) = True Then
            sSql = "INSERT into sl_ana_locais_exec (cod_ana, cod_local) VALUES("
            sSql = sSql & BL_TrataStringParaBD(cod_ana) & ","
            sSql = sSql & formAna.EcAnaLocais.ItemData(i) & ")"
            BG_ExecutaQuery_ADO sSql
        End If
    Next
    BL_GravaLocaisAnaExec = True
Exit Function
TrataErro:
    BL_GravaLocaisAnaExec = False
    BG_LogFile_Erros "BibliotecaLocal - BL_GravaLocaisAnaExec: " & Err.Description, "BL", "BL_GravaLocaisAnaExec"
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' GRAVA FOLHAS DE TRABALHO

' ------------------------------------------------------------------------------------------------

Public Function BL_GravaFolhasTrab(cod_ana As String, formAna As Form) As Boolean
    Dim sSql As String
    Dim RsOrdem As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    BL_GravaFolhasTrab = False
    
    sSql = "DELETE FROM sl_ana_trab WHERE cod_analise = " & BL_TrataStringParaBD(cod_ana)
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To formAna.EcListaFolhasTrab.ListCount - 1
        
        If formAna.EcListaFolhasTrab.Selected(i) = True Then
            If gFolhasTrab(i + 1).ordem = -1 Then
                sSql = "SELECT max(ordem) +1 orde FROM sl_ana_trab WHERE cod_gr_trab = " & BL_TrataStringParaBD(gFolhasTrab(i + 1).codFolhaTrab)
                RsOrdem.CursorLocation = adUseServer
                RsOrdem.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                RsOrdem.Open sSql, gConexao
                gFolhasTrab(i + 1).ordem = BL_HandleNull(RsOrdem!orde, 0)
                RsOrdem.Close
            End If
            
            sSql = "INSERT into sl_ana_trab (cod_analise, cod_gr_trab, ordem) "
            sSql = sSql & " SELECT " & BL_TrataStringParaBD(cod_ana) & ", cod_gr_trab," & gFolhasTrab(i + 1).ordem & " FROM sl_gr_trab "
            sSql = sSql & " WHERE seq_gr_trab = " & formAna.EcListaFolhasTrab.ItemData(i)
            BG_ExecutaQuery_ADO sSql
        End If
    Next
    BL_GravaFolhasTrab = True
    
Exit Function
TrataErro:
    BL_GravaFolhasTrab = False
    BG_LogFile_Erros "BibliotecaLocal - BL_GravaFolhasTrab: " & Err.Description, "BL", "BL_GravaFolhasTrab"
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' CARREGA LOCAIS ASSOCIADOS A ANALISE, SEJA PERFIS, COMPLEXAS OU SIMPLES

' ------------------------------------------------------------------------------------------------
Public Sub BL_CarregaLocaisAna(cod_ana As String, formAna As Form)
    Dim sSql As String
    Dim rsAnaLocais As New ADODB.recordset
    
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_ana_locais WHERE cod_ana = " & BL_TrataStringParaBD(cod_ana)
    Set rsAnaLocais = BG_ExecutaSELECT(sSql)
    If rsAnaLocais.RecordCount > 0 Then
        While Not rsAnaLocais.EOF
            For i = 0 To formAna.EcAnaLocais.ListCount - 1
                If formAna.EcAnaLocais.ItemData(i) = BL_HandleNull(rsAnaLocais!cod_local, "") Then
                    formAna.EcAnaLocais.Selected(i) = True
                End If
            Next
            rsAnaLocais.MoveNext
        Wend
    End If
End Sub

' ------------------------------------------------------------------------------------------------

' CARREGA LOCAIS de EXECUCAO ASSOCIADOS A ANALISE, SEJA PERFIS, COMPLEXAS OU SIMPLES

' ------------------------------------------------------------------------------------------------
Public Sub BL_CarregaLocaisAnaExec(cod_ana As String, formAna As Form)
    Dim sSql As String
    Dim rsAnaLocais As New ADODB.recordset
    
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_ana_locais_exec WHERE cod_ana = " & BL_TrataStringParaBD(cod_ana)
    Set rsAnaLocais = BG_ExecutaSELECT(sSql)
    If rsAnaLocais.RecordCount > 0 Then
        While Not rsAnaLocais.EOF
            For i = 0 To formAna.EcAnaLocaisExec.ListCount - 1
                If formAna.EcAnaLocaisExec.ItemData(i) = BL_HandleNull(rsAnaLocais!cod_local, "") Then
                    formAna.EcAnaLocaisExec.Selected(i) = True
                End If
            Next
            rsAnaLocais.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal - BL_CarregaLocaisAnaExec: " & Err.Description, "BL", "BL_CarregaLocaisAnaExec"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' INICIALIZA  FOLHAS DE TRABALHO

' ------------------------------------------------------------------------------------------------
Public Sub BL_IniFolhasTrab(formAna As Form)
    Dim sSql As String
    Dim RsAnaTrab As New ADODB.recordset
    Dim totalFolhasTrab As Long
    Dim i As Integer
    totalFolhasTrab = 0
    ReDim gFolhasTrab(totalFolhasTrab)
    
    sSql = "SELECT * FROM sl_gr_trab ORDER BY descr_gr_trab "
    Set RsAnaTrab = BG_ExecutaSELECT(sSql)
    If RsAnaTrab.RecordCount > 0 Then
        While Not RsAnaTrab.EOF
            totalFolhasTrab = totalFolhasTrab + 1
            ReDim Preserve gFolhasTrab(totalFolhasTrab)
            gFolhasTrab(totalFolhasTrab).codFolhaTrab = BL_HandleNull(RsAnaTrab!cod_gr_trab, "")
            gFolhasTrab(totalFolhasTrab).seqFolhaTrab = BL_HandleNull(RsAnaTrab!seq_gr_trab, -1)
            gFolhasTrab(totalFolhasTrab).DescrFolhaTrab = BL_HandleNull(RsAnaTrab!descr_gr_trab, "")
            gFolhasTrab(totalFolhasTrab).ordem = -1
            gFolhasTrab(totalFolhasTrab).Estado = 0
            RsAnaTrab.MoveNext
            
            formAna.EcListaFolhasTrab.AddItem gFolhasTrab(totalFolhasTrab).DescrFolhaTrab
            formAna.EcListaFolhasTrab.ItemData(formAna.EcListaFolhasTrab.NewIndex) = gFolhasTrab(totalFolhasTrab).seqFolhaTrab
        Wend
    End If
    RsAnaTrab.Close
    Set RsAnaTrab = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal - BL_CarregaFolhasTrab: " & Err.Description, "BL", "BL_CarregaFolhasTrab"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' INICIALIZA  INFORMACAO

' ------------------------------------------------------------------------------------------------
Public Sub BL_Informacao(formAna As Form)
    Dim sSql As String
    Dim rsAnaInfo As New ADODB.recordset
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_informacao ORDER BY descr_informacao "
    Set rsAnaInfo = BG_ExecutaSELECT(sSql)
    If rsAnaInfo.RecordCount > 0 Then
        While Not rsAnaInfo.EOF
        
            formAna.EcInformacao.AddItem BL_HandleNull(rsAnaInfo!descr_informacao, "")
            formAna.EcInformacao.ItemData(formAna.EcInformacao.NewIndex) = BL_HandleNull(rsAnaInfo!seq_informacao, "")
            rsAnaInfo.MoveNext
        Wend
    End If
    rsAnaInfo.Close
    Set rsAnaInfo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal - BL_Informacao: " & Err.Description, "BL", "BL_Informacao"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' CARREGA FOLHAS DE TRABALHO

' ------------------------------------------------------------------------------------------------
Public Sub BL_CarregaFolhasTrab(cod_ana As String, formAna As Form)
    Dim sSql As String
    Dim RsAnaTrab As New ADODB.recordset
    Dim totalFolhasTrab As Long
    Dim i As Integer
    
    
    sSql = "SELECT x1.seq_gr_trab, x2.cod_analise,x2.ordem FROM sl_gr_trab x1, sl_ana_Trab x2 WHERE x1.cod_gr_trab = x2.cod_gr_trab "
    sSql = sSql & " AND x2.cod_analise = " & BL_TrataStringParaBD(cod_ana)
    Set RsAnaTrab = BG_ExecutaSELECT(sSql)
    If RsAnaTrab.RecordCount > 0 Then
        While Not RsAnaTrab.EOF
        
            For i = 0 To formAna.EcListaFolhasTrab.ListCount - 1
                If formAna.EcListaFolhasTrab.ItemData(i) = BL_HandleNull(RsAnaTrab!seq_gr_trab, "") Then
                    formAna.EcListaFolhasTrab.Selected(i) = True
                    gFolhasTrab(i + 1).Estado = 1
                    gFolhasTrab(i + 1).ordem = BL_HandleNull(RsAnaTrab!ordem, 999)
                End If
            Next
            RsAnaTrab.MoveNext
        Wend
    End If
    RsAnaTrab.Close
    Set RsAnaTrab = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal - BL_CarregaFolhasTrab: " & Err.Description, "BL", "BL_CarregaFolhasTrab"
    Exit Sub
    Resume Next
End Sub

' pferreira 2008.05.09
Public Sub BL_Inserir_Modificar()
    If MDIFormInicio.Toolbar1.Buttons(5).Enabled = True Then
        gFormActivo.FuncaoInserir
    ElseIf MDIFormInicio.Toolbar1.Buttons(8).Enabled = True Then
        gFormActivo.FuncaoModificar
    End If
End Sub


' ------------------------------------------------------------------------------------------------

' CALCULA O VALOR DO POSTO, DEPOIS DE CALCULAR A PERCENTAGEM QUE TEM DIREITO

' ------------------------------------------------------------------------------------------------
Public Function BL_CalculaPercentagemPosto(n_req As String, cod_posto As String, valorRecebido As String, valorReceber As String, valorEFR As String) As String
    Dim margem As Integer
    Dim sSql As String
    Dim rsMargem As New ADODB.recordset
    Dim valorTotal As Double
    Dim ma As String
    
    If CDbl(valorEFR) < 0 Then valorEFR = "0"
    ma = BL_SelCodigo("SL_COD_SALAS", "MARGEM", "COD_SALA", cod_posto, "V")
    If ma <> "" Then
        margem = CInt(ma)
        valorTotal = CDbl(valorEFR) - CDbl(valorReceber)
        BL_CalculaPercentagemPosto = CStr((margem * valorTotal) / 100)
    End If
End Function

' ------------------------------------------------------------------------------------------------

' RETORNA DESCRICAO PARA ETIQUETA DA CODIFICACAO DE ANALISE OU DOS MEMBROS

' ------------------------------------------------------------------------------------------------
Public Function BL_RetornaDescrEtiqAna(codAna As String) As String
    Dim sSql As String
    Dim rsDescr As New ADODB.recordset
    
    If Mid(codAna, 1, 1) = "S" Then
        sSql = "SELECT distinct descr_etiq FROM sl_ana_S where cod_ana_S = " & BL_TrataStringParaBD(codAna)
    ElseIf Mid(codAna, 1, 1) = "C" Then
        sSql = "SELECT distinct descr_etiq FROM sl_ana_S where cod_ana_S IN (SELECT cod_membro FROM sl_membro WHERE COD_ANA_C = " & BL_TrataStringParaBD(codAna) & ")"
    ElseIf Mid(codAna, 1, 1) = "P" Then
        sSql = "SELECT distinct descr_etiq FROM sl_ana_S where cod_ana_S IN (SELECT cod_analise FROM sl_Ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAna) & ")"
        sSql = sSql & " UNION SELECT distinct descr_etiq FROM sl_ana_S where cod_ana_S IN (SELECT cod_membro FROM sl_membro WHERE COD_ANA_C IN ("
        sSql = sSql & " SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAna) & "))"
    End If
    rsDescr.CursorLocation = adUseServer
    rsDescr.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDescr.Open sSql, gConexao
    BL_RetornaDescrEtiqAna = ""
    If rsDescr.RecordCount > 0 Then
        While Not rsDescr.EOF
            If UCase(BL_HandleNull(rsDescr!descr_etiq, "")) <> "" And InStr(1, BL_RetornaDescrEtiqAna, BL_HandleNull(rsDescr!descr_etiq, "")) = 0 Then
                BL_RetornaDescrEtiqAna = BL_RetornaDescrEtiqAna & UCase(rsDescr!descr_etiq) & ", "
            End If
            rsDescr.MoveNext
        Wend
    End If
    rsDescr.Close
    Set rsDescr = Nothing
End Function

' ------------------------------------------------------------------------------------------------

' RETORNA NUMERO DE ANALISES ASSOCIADOS A CADA ANALISE, SEJA SIMPLES, COMPLEXA OU PERFIL

' ------------------------------------------------------------------------------------------------
Public Function BL_RetornaNumAna(codAna As String) As Integer
    Dim sSql As String
    Dim rsDescr As New ADODB.recordset
    
    If Mid(codAna, 1, 1) = "S" Then
        BL_RetornaNumAna = 1
        Exit Function
    ElseIf Mid(codAna, 1, 1) = "C" Then
        sSql = "SELECT descr_etiq FROM sl_ana_S where cod_ana_S IN (SELECT cod_membro FROM sl_membro WHERE COD_ANA_C = " & BL_TrataStringParaBD(codAna) & ")"
    ElseIf Mid(codAna, 1, 1) = "P" Then
        sSql = "SELECT descr_etiq FROM sl_ana_S where cod_ana_S IN (SELECT cod_analise FROM sl_Ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAna) & ")"
        sSql = sSql & " UNION SELECT descr_etiq FROM sl_ana_S where cod_ana_S IN (SELECT cod_membro FROM sl_membro WHERE COD_ANA_C IN ("
        sSql = sSql & " SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAna) & "))"
    End If
    rsDescr.CursorLocation = adUseServer
    rsDescr.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDescr.Open sSql, gConexao
    BL_RetornaNumAna = 0
    If rsDescr.RecordCount > 0 Then
        BL_RetornaNumAna = rsDescr.RecordCount
    End If
    rsDescr.Close
    Set rsDescr = Nothing
End Function

' ------------------------------------------------------------------------------------------------

' DEVOLVE UMA STRING CONCATENADA COM OS ESTADOS DOS DIVERSOS RECIBOS

' ------------------------------------------------------------------------------------------------
Public Function BL_DevolveEstadoRecibos(n_req As String)
    Dim sSql As String
    Dim rsDescr As New ADODB.recordset
    BL_DevolveEstadoRecibos = ""
    sSql = "SELECT cod_isencao FROM sl_requis WHERE n_req = " & n_req
    rsDescr.CursorLocation = adUseServer
    rsDescr.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDescr.Open sSql, gConexao
    BL_DevolveEstadoRecibos = ""
    If rsDescr.RecordCount > 0 Then
        If BL_HandleNull(rsDescr!cod_isencao, "0") <> gTipoIsencaoBorla Then
            rsDescr.Close
            sSql = "SELECT * FROM sl_recibos WHERE n_req = " & n_req
            rsDescr.CursorLocation = adUseServer
            rsDescr.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsDescr.Open sSql, gConexao
            BL_DevolveEstadoRecibos = ""
            If rsDescr.RecordCount > 0 Then
                While Not rsDescr.EOF
                    If BL_HandleNull(rsDescr!n_rec, "0") = "0" And BL_HandleNull(rsDescr!Estado, "N") = gEstadoReciboNaoEmitido And BL_HandleNull(rsDescr!total_pagar, 0) > "0" Then
                        BL_DevolveEstadoRecibos = BL_DevolveEstadoRecibos & " N"
                    ElseIf BL_HandleNull(rsDescr!n_rec, "0") <> "0" And BL_HandleNull(rsDescr!Estado, "N") = gEstadoReciboCobranca Then
                        BL_DevolveEstadoRecibos = BL_DevolveEstadoRecibos & " N"
                    ElseIf BL_HandleNull(rsDescr!n_rec, "0") <> "0" And BL_HandleNull(rsDescr!Estado, "N") = gEstadoReciboPago Then
                        BL_DevolveEstadoRecibos = BL_DevolveEstadoRecibos & " P"
                    End If
                    rsDescr.MoveNext
                Wend
            End If
            rsDescr.Close
        Else
            BL_DevolveEstadoRecibos = "B"
        End If
    End If
    Set rsDescr = Nothing
End Function

' ------------------------------------------------------------------------------------------------

' VERIFICA SE EXISTE ALGUMA OBSERVA��O PARA BLOQUEAR AN�LISES - INTERFER�NCIAS

' ------------------------------------------------------------------------------------------------
Public Function BL_VerificaObsAutoBloq(n_req As String) As String
    Dim sSql As String
    Dim rsDescr As New ADODB.recordset
    On Error GoTo TrataErro
    
    BL_VerificaObsAutoBloq = "N"
    
    sSql = "SELECT * FROM SL_OBS_AUTO WHERE n_req = " & n_req & " AND flg_bloqueio in( " & gINTERFBloquear & ") "
    sSql = sSql & " AND descr_obs_auto IS NOT NULL "
    rsDescr.CursorLocation = adUseServer
    rsDescr.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDescr.Open sSql, gConexao
    If rsDescr.RecordCount > 0 Then
        If BL_HandleNull(rsDescr!flg_activo, "0") = 1 Then
            BL_VerificaObsAutoBloq = "S"
        End If
    End If
    rsDescr.Close
    Set rsDescr = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "BL_VerificaObsAutoBloq: " & Err.Description
    BG_Mensagem mediMsgBox, Err.Description, vbCritical, " Verifica ObsAuto Bloqueios"
    BL_VerificaObsAutoBloq = "N"
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' VERIFICA SE EXISTE ALGUMA OBSERVA��O PARA BLOQUEAR AN�LISES - INTERFER�NCIAS

' ------------------------------------------------------------------------------------------------
Public Function BL_VerificaObsAutoAviso(n_req As String, seq_realiza As String) As String
    Dim sSql As String
    Dim rsDescr As New ADODB.recordset
    On Error GoTo TrataErro
    BL_VerificaObsAutoAviso = "N"
    
    If gUsaInterferencia = mediSim Then
        sSql = "SELECT * FROM SL_OBS_AUTO WHERE seq_realiza = " & seq_realiza & " AND flg_bloqueio in( " & gINTERFAvisar & ", " & gINTERFDesBloquear & ") "
    ElseIf gUsaDeltaCheck = mediSim Then
        sSql = "SELECT * FROM SL_OBS_AUTO WHERE seq_realiza = " & seq_realiza & " AND flg_bloqueio in( " & gDCAvisar & ")"
    Else
        Exit Function
    End If
    sSql = sSql & " AND descr_obs_auto IS NOT NULL "
    rsDescr.CursorLocation = adUseServer
    rsDescr.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDescr.Open sSql, gConexao
    If rsDescr.RecordCount > 0 Then
        If BL_HandleNull(rsDescr!flg_activo, "0") = 1 Then
            BL_VerificaObsAutoAviso = "S"
        End If
    End If
    rsDescr.Close
    Set rsDescr = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "BL_VerificaObsAutoAviso: " & Err.Description
    BG_Mensagem mediMsgBox, Err.Description, vbCritical, " Verifica ObsAuto Avisos"
    BL_VerificaObsAutoAviso = "N"
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' DEVOLVE OBSERVACOES QUE SAO PARA IMPRIMIR - CONCATENADAS

' ------------------------------------------------------------------------------------------------
Public Function BL_DevolveObsAnaImprimir(n_req As String) As String
    Dim sSql As String
    Dim rsDescr As New ADODB.recordset
    
    BL_DevolveObsAnaImprimir = ""
    
    sSql = "SELECT * FROM sl_obs_ana_Req WHERE n_req = " & n_req & " AND cod_gr_nota =" & gGrupoNotasImprRel
    sSql = sSql & " AND (flg_invisivel IS NULL OR flg_invisivel = 0 ) "
    rsDescr.CursorLocation = adUseServer
    rsDescr.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDescr.Open sSql, gConexao
    If rsDescr.RecordCount > 0 Then
        While Not rsDescr.EOF
            BL_DevolveObsAnaImprimir = BL_DevolveObsAnaImprimir & BL_HandleNull(rsDescr!descr_obs, "")
            rsDescr.MoveNext
        Wend
    End If
    rsDescr.Close
    Set rsDescr = Nothing
End Function
Public Function BL_ArredondaCima(numero As Double) As Long
    If numero > Round(numero) Then
        BL_ArredondaCima = Round(numero) + 1
    Else
        BL_ArredondaCima = Round(numero)
    End If
End Function

' ------------------------------------------------------------------------------------------------

' RETORNA A ORDEM DE UMA ANALISE DENTRO DE UMA COMPLEXA

' ------------------------------------------------------------------------------------------------

Public Function BL_RetornaOrdAnaC(cod_membro As String, cod_ana_c As String) As String
    Dim sSql As String
    Dim rsDescr As New ADODB.recordset
    If cod_membro = gCodAnaObs Then
        BL_RetornaOrdAnaC = 0
        Exit Function
    End If
    BL_RetornaOrdAnaC = "999"
    sSql = "SELECT ordem FROM sl_membro WHERE cod_ana_c =  " & BL_TrataStringParaBD(cod_ana_c) & " AND cod_membro = " & BL_TrataStringParaBD(cod_membro)
    rsDescr.CursorLocation = adUseServer
    rsDescr.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDescr.Open sSql, gConexao
    If rsDescr.RecordCount >= 1 Then
        BL_RetornaOrdAnaC = BL_HandleNull(rsDescr!ordem, "999")
    End If
    rsDescr.Close
End Function

' ------------------------------------------------------------------------------------------------

' RETORNA A ORDEM DE UMA ANALISE DENTRO DE UMA COMPLEXA

' ------------------------------------------------------------------------------------------------

Public Function BL_RetornaOrdPerfil(codAnalise As String, Cod_Perfil As String) As String
    Dim sSql As String
    Dim rsDescr As New ADODB.recordset
    BL_RetornaOrdPerfil = "0"
    sSql = "SELECT ordem FROM sl_ana_perfis WHERE cod_perfis =  " & BL_TrataStringParaBD(Cod_Perfil) & " AND cod_analise = " & BL_TrataStringParaBD(codAnalise)
    rsDescr.CursorLocation = adUseServer
    rsDescr.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDescr.Open sSql, gConexao
    If rsDescr.RecordCount >= 1 Then
        BL_RetornaOrdPerfil = BL_HandleNull(rsDescr!ordem, "999")
    End If
    rsDescr.Close
End Function

' ------------------------------------------------------------------------------------------------

' GERA AUTOMATICAMENTE UM CODIGO N�O USADO PARA A AN�LISE

' ------------------------------------------------------------------------------------------------
Public Function BL_GeraCodigoAnalise() As String
    Dim i As Long
    Dim numero As Long
    Dim flg_gerou As Boolean
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    On Error GoTo TrataErro
    flg_gerou = False
    While flg_gerou = False
        numero = -1
        numero = BL_GeraNumero("COD_ANA")
        flg_gerou = True
        
        ' SIMPLES
        sSql = "SELECT count(*) conta FROM sl_ana_s WHERE cod_ana_s = 'S" & numero & "'"
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount >= 1 Then
            If BL_HandleNull(rsAna!conta, 0) > 0 Then
                flg_gerou = False
            End If
        End If
        rsAna.Close
        
        ' COMPLEXAS
        If flg_gerou = True Then
            sSql = "SELECT count(*) conta FROM sl_ana_c WHERE cod_ana_c = 'C" & numero & "'"
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount >= 1 Then
                If BL_HandleNull(rsAna!conta, 0) > 0 Then
                    flg_gerou = False
                End If
            End If
            rsAna.Close
        End If
        
        ' COMPLEXAS
        If flg_gerou = True Then
            sSql = "SELECT count(*) conta FROM sl_ana_c WHERE cod_ana_c = 'C" & numero & "'"
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount >= 1 Then
                If BL_HandleNull(rsAna!conta, 0) > 0 Then
                    flg_gerou = False
                End If
            End If
            rsAna.Close
        End If
        
        ' PERFIS
        If flg_gerou = True Then
            sSql = "SELECT count(*) conta FROM sl_perfis WHERE cod_perfis = 'P" & numero & "'"
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount >= 1 Then
                If BL_HandleNull(rsAna!conta, 0) > 0 Then
                    flg_gerou = False
                End If
            End If
            rsAna.Close
        End If
    Wend
    If numero <> -1 And flg_gerou = True Then
        BL_GeraCodigoAnalise = CStr(numero)
    Else
        BL_GeraCodigoAnalise = ""
    End If
Exit Function
TrataErro:
    BG_TrataErro "BibliotecaLocal - BL_GeraCodigoAnalise: " & Err.Description, Err.Number
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' VERIFICA SE UM DETERMINADO CODIGO DE ANALISE JA EXISTE

' ------------------------------------------------------------------------------------------------
Public Function BL_VerificaCodigoExiste(cod_ana As String) As Boolean
    Dim i As Long
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    On Error GoTo TrataErro
        
    ' SIMPLES
    sSql = "SELECT count(*) conta FROM sl_ana_s WHERE cod_ana_s = 'S" & Mid(cod_ana, 2) & "'"
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount >= 1 Then
        If BL_HandleNull(rsAna!conta, 0) > 0 Then
            BL_VerificaCodigoExiste = True
            Exit Function
        End If
    End If
    rsAna.Close
    
     ' COMPLEXAS
    sSql = "SELECT count(*) conta FROM sl_ana_c WHERE cod_ana_c = 'C" & Mid(cod_ana, 2) & "'"
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount >= 1 Then
        If BL_HandleNull(rsAna!conta, 0) > 0 Then
            BL_VerificaCodigoExiste = True
            Exit Function
        End If
    End If
    rsAna.Close
    
    ' PERFIS
    sSql = "SELECT count(*) conta FROM sl_perfis WHERE cod_perfis = 'P" & Mid(cod_ana, 2) & "'"
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount >= 1 Then
        If BL_HandleNull(rsAna!conta, 0) > 0 Then
            BL_VerificaCodigoExiste = True
            Exit Function
        End If
    End If
    rsAna.Close
    
    BL_VerificaCodigoExiste = False
Exit Function
TrataErro:
    BG_TrataErro "BibliotecaLocal - BL_VerificaCodigoExiste: " & Err.Description, Err.Number
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' COPIA PDF IMPRESSO PARA UMA OUTRA PASTA

' ------------------------------------------------------------------------------------------------
Public Function BL_CopiaRelatorioEmail(sFicheiroOrigem As String, sFicheiroDestino As String) As Boolean
    Dim bRetorno As Boolean
    Dim sDestino As String
    bRetorno = False
    
    On Error GoTo TrataErro
    If BL_FileExists(gDirCliente & "\bin\" & sFicheiroDestino) = True Then
        
        Kill gDirCliente & "\bin\" & sFicheiroDestino
    End If
    
    sDestino = gDirCliente & "\bin\" & sFicheiroDestino
    
    If BL_FileExists(gDirServidor & "\bin\" & sFicheiroOrigem) = True Then
        'est� na pasta cliente
        'Detectou o ficheiro no servidor
        FileCopy gDirServidor & "\bin\" & sFicheiroOrigem, sDestino
        If Dir(sDestino) <> "" Then
            'Copia bem sucedida
            bRetorno = True
        End If
    End If
            
    BL_CopiaRelatorioEmail = bRetorno
    Exit Function
TrataErro:
    BG_LogFile_Erros "Erro em BL_CopiaFicheiro: " & Err.Number & " - " & Err.Description
    BL_CopiaRelatorioEmail = bRetorno
    If Err.Number = 70 Then
        BL_CopiaRelatorioEmail = True
    End If
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' VERIFICA SE FICHEIRO ESTA LOCKED

' ------------------------------------------------------------------------------------------------
Function BL_FileLocked(strFileName As String) As Boolean
   On Error Resume Next

   ' If the file is already opened by another process,
   ' and the specified type of access is not allowed,
   ' the Open operation fails and an error occurs.
   Open strFileName For Binary Access Read Lock Read As #1
   Close #1

   ' If an error occurs, the document is currently open.
   If Err.Number <> 0 Then
      ' Display the error number and description.
      'BG_LogFile_erros "Error #" & str(Err.Number) & " - " & Err.Description
      BL_FileLocked = True
      Err.Clear
   End If
End Function

' ------------------------------------------------------------------------------------------------

' ENVIA UM RELATORIO POR EMAIL

' ------------------------------------------------------------------------------------------------
Public Function BL_EnviaEmail(emailDestino As String, assunto As String, mensagem As String, anexo As Boolean, _
                              pastaAnexo As String, ficheiros() As String, flg_html As Integer, imagem_anexo As String, _
                              descr_imagem_anexo As String, flg_recibo As Integer, flg_cc As Integer, config As String, _
                              email_cc As String) As Boolean
                              
    Dim rsMail As New ADODB.recordset
    Dim sSql As String
    Dim iMsg As New CDO.message
    Dim iConf As New CDO.Configuration
    Dim Flds As ADODB.Fields
    Dim strHTML
    Dim sNextFile As String
    Dim requis As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    Set Flds = iConf.Fields
    
    sSql = "SELECT * FROM sl_email_config WHERE cod_config = " & BL_TrataStringParaBD(config)
    sSql = sSql & " AND cod_local = " & CStr(gCodLocal)
    rsMail.CursorLocation = adUseServer
    rsMail.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsMail.Open sSql, gConexao
    If rsMail.RecordCount = 0 Then
        rsMail.Close
        sSql = "SELECT * FROM sl_email_config WHERE cod_config = " & BL_TrataStringParaBD(config)
        sSql = sSql & " AND cod_local IS NULL "
        rsMail.CursorLocation = adUseServer
        rsMail.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsMail.Open sSql, gConexao
        
        If rsMail.RecordCount <= 0 Then
            rsMail.Close
            BG_Mensagem mediMsgBox, "N�o existem configura��es (SL_EMAIL_CONFIG)", vbCritical, " N�o � possivel enviar eMail."
            BL_EnviaEmail = False
            Exit Function
        End If
    End If
    
    With Flds
        .Item(cdoSendUsingMethod) = BL_HandleNull(rsMail!auth_type, cdoSendUsingPort)
        .Item(cdoSMTPServer) = BL_HandleNull(rsMail!server, "")
        .Item(cdoSMTPServerPort) = BL_HandleNull(rsMail!porta, "")
        'Use SSL to connect to the SMTP server:
        .Item(cdoSMTPAuthenticate) = BL_HandleNull(rsMail!requer_autenticacao, "1")
        .Item(cdoSMTPUseSSL) = BL_HandleNull(rsMail!ssl, "1")
        .Item(cdoSMTPConnectionTimeout) = BL_HandleNull(rsMail!timeout, "10")
        .Item(cdoSendUserName) = BL_HandleNull(rsMail!Username, "")
        .Item(cdoSendPassword) = BL_HandleNull(rsMail!password, "")

        .Update
    End With
    
    With iMsg
        Set .Configuration = iConf
        .To = emailDestino
        .From = BL_HandleNull(rsMail!email_from, "")
        .Subject = assunto
        If flg_html = mediSim Then
            .TextBody = ""
            .HTMLBody = mensagem
            
            ' SE EST� CODIFICADO PARA ENVIAR ALGUMA IMAGEM NO HTML
            If imagem_anexo <> "" Then
                .AddRelatedBodyPart imagem_anexo, descr_imagem_anexo, cdoRefTypeId
            End If
        Else
            '.HTMLBody = ""
            .TextBody = mensagem
        End If
        If flg_cc = mediSim And email_cc = "" Then
            .cc = BL_HandleNull(rsMail!email_from, "")
        ElseIf email_cc <> "" Then
            .cc = BL_HandleNull(email_cc)
        Else
            .cc = ""
        End If
        
        If flg_recibo = mediSim Then
            .Fields(cdoDispositionNotificationTo) = BL_HandleNull(rsMail!email_from, "")
            .Fields(cdoReturnReceiptTo) = BL_HandleNull(rsMail!email_from, "")
        Else
            .Fields(cdoDispositionNotificationTo) = ""
            .Fields(cdoReturnReceiptTo) = ""
        End If
        If anexo = True Then
            requis = ""
            For i = 0 To UBound(ficheiros)
                If ficheiros(i) <> "" Then
                    requis = requis & Replace(ficheiros(i), ".pdf", ", ")
                    .AddAttachment (pastaAnexo & "\" & ficheiros(i))
                End If
            Next
        End If
        '.DSNOptions = cdoDSNSuccessFailOrDelay
        .Fields.Update
        .ReplyTo = BL_HandleNull(rsMail!email_from, "")
        .Send
    End With
    rsMail.Close
    Set rsMail = Nothing
    
    ' cleanup of variables
    Set iMsg = Nothing
    Set iConf = Nothing
    Set Flds = Nothing
    BL_EnviaEmail = True
    
Exit Function
TrataErro:
    BG_LogFile_Erros "BL_EnviaEmail: " & Err.Description
    BG_Mensagem mediMsgBox, "Erro ao enviar eMail " & requis & Err.Description, vbCritical, " N�o � possivel enviar eMail. " & Err.Description
     BL_EnviaEmail = False
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' DADO UMA ANALISE, UMA VARIA��O, UM SEQ UTENTE - AVALIA SE A VARIA��O EST� CORRECTA OU N�O

' ------------------------------------------------------------------------------------------------
Public Function BL_DC_AvaliaVariacoes(codAnaS As String, CodVariacao As String, SeqUtente As String, data As String, _
                                      N_Res As Integer, resultado As String) As Boolean
    Dim sSql As String
    Dim rsVar As New ADODB.recordset
    Dim RsRes As New ADODB.recordset
    Dim conta As Long
    Dim nr_resultado As Integer
    Dim limite_dias As Long
    Dim var_abs As String
    Dim var_per  As String
    Dim var_qual As String
    Dim dataLimite As String
    Dim Mediana As Double
    Dim Resultados() As String
    Dim FlagDiferenca As Boolean
    Dim flg_obriga_res_ant As Integer
    sSql = "SELECT * FROM sl_varia_dc WHERE cod_varia_dc = " & BL_TrataStringParaBD(CodVariacao)
    rsVar.CursorLocation = adUseServer
    rsVar.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsVar.Open sSql, gConexao
    
    If rsVar.RecordCount = 1 Then
        nr_resultado = BL_HandleNull(rsVar!nr_resultado, 0)
        limite_dias = BL_HandleNull(rsVar!limite_dias, 0)
        var_abs = BL_HandleNull(rsVar!variacao_abs, "0")
        var_per = BL_HandleNull(rsVar!variacao_per, "0")
        dataLimite = CStr(CDate(CDate(data) - limite_dias))
        flg_obriga_res_ant = BL_HandleNull(rsVar!flg_obriga_res_ant, 0)
        
        ' ---------------------------------------------------------------------------------
        ' SE N�O INDICOU QUALQUER RESULTADO. � AUTOMATICAMENTE TRUE
        ' ---------------------------------------------------------------------------------
        If nr_resultado = 0 Then
            BL_DC_AvaliaVariacoes = True
            Exit Function
        End If
        
    ' ---------------------------------------------------------------------------------
    ' SE N�O ENCONTROU QUALQUER CODIFICACAO PARA VARIACAO EM CAUSA D� FALSE
    ' ---------------------------------------------------------------------------------
    Else
        BL_DC_AvaliaVariacoes = False
        Exit Function
    End If
    rsVar.Close
    Set rsVar = Nothing
    
    ' ---------------------------------------------------------------------------------
    ' VAI PESQUISAR OS RESULTADOS PARA A AN�LISE EM CAUSA NO INTERVALO DATAS PRETENDIDO
    ' ---------------------------------------------------------------------------------
    sSql = "SELECT x2.result FROM sl_realiza x1, sl_Res_alfan x2 WHERE x1.seq_utente = " & SeqUtente & " AND x1.cod_Ana_s = " & BL_TrataStringParaBD(codAnaS)
    sSql = sSql & " AND (dt_chega between " & BL_TrataDataParaBD(dataLimite) & " AND " & BL_TrataDataParaBD(data) & ")"
    sSql = sSql & " AND x1.seq_realiza = x2.seq_realiza AND x1.flg_estado IN (" & gEstadoAnaValidacaoMedica & ", " & gEstadoAnaImpressa & ")"
    sSql = sSql & " AND x2.n_res = " & N_Res & " ORDER BY dt_val DESC, hr_val DESC"
    RsRes.CursorLocation = adUseServer
    RsRes.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRes.Open sSql, gConexao
    If RsRes.RecordCount > 0 Then
        ReDim Resultados(nr_resultado)
        While Not RsRes.EOF
            If conta <= nr_resultado Then
                ' --------------------------------------------------------------
                ' SE TIVER VALORES N�O NUMERICOS D� FALSE E SAI
                ' --------------------------------------------------------------
                If Not IsNumeric(BL_HandleNull(RsRes!Result, "")) Or Not IsNumeric(resultado) Then
                    BL_DC_AvaliaVariacoes = False
                    Exit Function
                End If
                Resultados(conta) = BL_HandleNull(RsRes!Result, "")
                conta = conta + 1
            End If
            RsRes.MoveNext
        Wend
        RsRes.Close
        Set RsRes = Nothing
        
        ' --------------------------------------------------------------
        ' UTENTE N�O TEM NUMERO DE RESULTADOS SUFICIENTES ENT�O DA FALSE
        ' --------------------------------------------------------------
        If conta < nr_resultado And flg_obriga_res_ant = 1 Then
            BL_DC_AvaliaVariacoes = False
            Exit Function
        End If
        Mediana = BL_Mediana(Resultados)
        
        If var_abs > 0 Then
            FlagDiferenca = Abs(resultado - Mediana) <= var_abs
        ElseIf var_per > 0 Then
            FlagDiferenca = (Abs(Mediana - resultado) * 100) / Abs(resultado) <= var_per
        End If
        
        If FlagDiferenca = True Then
            BL_DC_AvaliaVariacoes = True
        Else
            BL_DC_AvaliaVariacoes = False
        End If
    Else
        If flg_obriga_res_ant = 0 Then
            BL_DC_AvaliaVariacoes = True
        Else
            BL_DC_AvaliaVariacoes = False
        End If
    End If
End Function


' ------------------------------------------------------------------------------------------------

' DEVOLVE RESULTADO DE UMA INTERFERENCIA PARA UMA REQUISICAO

' ------------------------------------------------------------------------------------------------
Public Function BL_DevolveResultadoInterferencia(NReq As String, CodInterferencia As String) As String
    Dim sSql As String
    Dim rsInterferencias As New ADODB.recordset
    
    sSql = "SELECT x1.cod_interferencia, x1.resultado FROM sl_res_interferencias x1 WHERE x1.n_req = " & NReq
    sSql = sSql & " AND cod_interferencia = " & BL_TrataStringParaBD(CodInterferencia)
    Set rsInterferencias = New ADODB.recordset
    rsInterferencias.CursorLocation = adUseServer
    rsInterferencias.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsInterferencias.Open sSql, gConexao
    
    If rsInterferencias.RecordCount = 1 Then
        BL_DevolveResultadoInterferencia = BL_HandleNull(rsInterferencias!resultado, "")
    Else
        BL_DevolveResultadoInterferencia = "0"
    End If
    rsInterferencias.Close
    Set rsInterferencias = Nothing

End Function

' --------------------------------------------------------------------------------------------------

' IMPRESSAO DE CARTOES COM GRUPO SANGUINEO EM CRYSTAL

' --------------------------------------------------------------------------------------------------
Public Function BL_ImprimeCartaoCrystal(NomeImpressora As String, NomeUtente As String, DataNasc As String, SeqUtente As Long, _
                            TipoUtente As String, Utente As String, MBarcode As Object, MostraMgm As Boolean, NBenef As String) As Boolean
    Dim i As Long
    Dim rs As New ADODB.recordset
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    Dim sSql As String
    Dim teste As String
    Dim Result As Integer
    Dim AnaliseGrupo As String
    Dim AnaliseRH As String
    Dim continua As Boolean
    Dim Report As CrystalReport
    Dim Grupo As String
    Dim rh As String
    Dim codPostal As String
    Dim descrPostal As String
    Dim dtValidade As String
    Dim DescrEFR As String
    
    If gSGBD = gOracle Then
        sSql = "SELECT cod_postal_ute, cod_efr_ute, n_benef_ute, dt_validade, descr_efr FROM sl_identif, sl_efr "
        sSql = sSql & " WHERE sl_identif.cod_efr_ute = sl_efr.cod_efr (+) "
    ElseIf gSGBD = gSqlServer Then
        sSql = "SELECT cod_postal_ute, cod_efr_ute, n_benef_ute, dt_validade, descr_efr FROM sl_identif LEFT OUTER JOIN sl_efr "
        sSql = sSql & " ON sl_identif.cod_efr_ute = sl_efr.cod_efr "
    End If
    sSql = sSql & " AND sl_identif.seq_utente = " & SeqUtente
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
    If rs.RecordCount = 1 Then
        codPostal = BL_HandleNull(rs!cod_postal_ute, "")
        descrPostal = BL_DevolveDescrPostal(codPostal)
        dtValidade = BL_HandleNull(rs!dt_validade, "")
        DescrEFR = BL_HandleNull(rs!descr_efr, "")
        
    End If
    rs.Close
    
    
     AnaliseGrupo = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseGrupoSanguineo")
     AnaliseRH = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseRhSanguineo")
    If AnaliseRH <> "-1" Then
         sSql = "SELECT sl_res_alfan1.result resultgrupo , sl_res_alfan2.result resultrh, sl_realiza1.dt_chega " & _
                 " FROM sl_realiza sl_realiza1, sl_realiza sl_realiza2, sl_res_alfan sl_res_alfan1, sl_res_alfan sl_res_alfan2 " & _
                 " WHERE sl_realiza1.seq_realiza = sl_res_alfan1.seq_realiza " & _
                 " and sl_realiza1.cod_ana_s = " & BL_TrataStringParaBD(AnaliseGrupo) & _
                 " and sl_realiza1.seq_utente = " & CStr(val(SeqUtente)) & _
                 " and sl_realiza2.seq_realiza = sl_res_alfan2.seq_realiza " & _
                 " and sl_realiza2.cod_ana_s = " & BL_TrataStringParaBD(AnaliseRH) & _
                 " and sl_realiza2.seq_utente = " & CStr(val(SeqUtente))
         sSql = sSql & " UNION SELECT sl_res_alfan3.result resultgrupo , sl_res_alfan4.result resultrh, sl_realiza3.dt_chega " & _
                 " FROM sl_realiza_h sl_realiza3, sl_realiza_h sl_realiza4, sl_res_alfan_h sl_res_alfan3, sl_res_alfan_h sl_res_alfan4 " & _
                 " WHERE sl_realiza3.seq_realiza = sl_res_alfan3.seq_realiza " & _
                 " and sl_realiza3.cod_ana_s = " & BL_TrataStringParaBD(AnaliseGrupo) & _
                 " and sl_realiza3.seq_utente = " & CStr(val(SeqUtente)) & _
                 " and sl_realiza4.seq_realiza = sl_res_alfan4.seq_realiza " & _
                 " and sl_realiza4.cod_ana_s = " & BL_TrataStringParaBD(AnaliseRH) & _
                 " and sl_realiza4.seq_utente = " & CStr(val(SeqUtente))
        sSql = sSql & " ORDER by dt_chega desc "
     ElseIf AnaliseGrupo <> "-1" Then
         sSql = "SELECT result resultgrupo,q.dt_chega FROM sl_requis q, sl_realiza r, sl_res_alfan a  " + _
              " WHERE r.n_req=q.n_req AND r.seq_realiza=a.seq_realiza AND " + _
              " r.cod_perfil='0' " + _
              " AND r.cod_ana_s in ('" + BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseGrupoSanguineo") + "')" + _
              " AND q.seq_utente=" + CStr(val(SeqUtente))
         sSql = sSql & " UNION SELECT result resultgrupo,q.dt_chega FROM sl_requis q, sl_realiza_h r, sl_res_alfan_h a  " + _
              " WHERE r.n_req=q.n_req AND r.seq_realiza=a.seq_realiza AND " + _
              " r.cod_perfil='0' " + _
              " AND r.cod_ana_s in ('" + BG_DaParamAmbiente_NEW(mediAmbitoGeral, "AnaliseGrupoSanguineo") + "')" + _
              " AND q.seq_utente=" + CStr(val(SeqUtente))
        sSql = sSql & " ORDER by dt_chega DESC "
     End If
     
     rs.CursorType = adOpenStatic
     rs.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
     rs.Open sSql, gConexao
    
    If rs.RecordCount = 0 And gImprCartoesInscritos <> mediSim Then
        Exit Function
    ElseIf rs.RecordCount = 0 And gImprCartoesInscritos = mediSim Then
        rh = ""
        Grupo = ""
    Else
        If AnaliseRH <> "-1" Then
            rh = BL_HandleNull(rs!resultrh, "")
        End If
        Grupo = BL_HandleNull(rs!RESULTGRUPO, "")
    End If
    rs.Close
    Set rs = Nothing
    
    sSql = "DELETE FROM SL_CR_CARTAO WHERE NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    
    sSql = "INSERT INTO SL_CR_CARTAO(t_utente, UTENTE, NOME_UTE,GRUPO,RH, NOME_COMPUTADOR, N_BENEF_UTE, COD_POSTAL,"
    sSql = sSql & " DESCR_POSTAL,DESCR_EFR,DT_VALIDADE, DT_NASC) VALUES ("
    sSql = sSql & BL_TrataStringParaBD(TipoUtente) & "," & BL_TrataStringParaBD(Utente) & "," & BL_TrataStringParaBD(NomeUtente)
    sSql = sSql & "," & BL_TrataStringParaBD(Grupo) & "," & BL_TrataStringParaBD(rh) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ","
    sSql = sSql & BL_TrataStringParaBD(NBenef) & ","
    sSql = sSql & BL_TrataStringParaBD(codPostal) & ","
    sSql = sSql & BL_TrataStringParaBD(descrPostal) & ","
    sSql = sSql & BL_TrataStringParaBD(DescrEFR) & ","
    sSql = sSql & BL_TrataDataParaBD(dtValidade) & ","
    sSql = sSql & BL_TrataDataParaBD(DataNasc) & ")"
    
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    

    If BL_PreviewAberto("Cartao") = True Then Exit Function
    continua = BL_IniciaReport("Cartao", "Cartao", crptToPrinter, False, , , , , NomeImpressora)
    If continua = False Then Exit Function

    'Imprime o relat�rio no Crystal Reports
    Set Report = forms(0).Controls("Report")

    Report.SQLQuery = ""
    Report.Connect = "DSN=" & gDSN
    Report.SelectionFormula = "{SL_CR_CARTAO.nome_computador} = '" & gComputador & "' "
    
    Call BL_ExecutaReport
    BL_ActualizaDataImprCartao TipoUtente, Utente
End Function


Function BL_GeraRecEmpresa(NomeCampo As String, codEmpresa As String) As Long


    Dim CmdGera As New ADODB.Command
    Dim PmtGera As ADODB.Parameter
    Dim PmtCampo As ADODB.Parameter
    Dim PmtEmpresa As ADODB.Parameter
        
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdGera
        .ActiveConnection = gConexao
        .CommandText = "GERADOR_REC_EMPRESA"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtGera = CmdGera.CreateParameter("RES", adNumeric, adParamOutput, 0)
    PmtGera.Precision = 12
    Set PmtCampo = CmdGera.CreateParameter("CAMPO", adVarChar, adParamInput, 30)
    Set PmtEmpresa = CmdGera.CreateParameter("EMPRESA", adVarChar, adParamInput, 30)
    CmdGera.Parameters.Append PmtCampo
    CmdGera.Parameters.Append PmtEmpresa
    CmdGera.Parameters.Append PmtGera
    
    CmdGera.Parameters("CAMPO") = NomeCampo
    CmdGera.Parameters("EMPRESA") = codEmpresa
    CmdGera.Execute
    
    BL_GeraRecEmpresa = Trim(CmdGera.Parameters.Item("RES").value)
       
    Set CmdGera = Nothing
    Set PmtGera = Nothing
    Set PmtCampo = Nothing

    
Exit Function
TrataErro:
    BL_GeraRecEmpresa = -1
    BG_LogFile_Erros "Erro ao gerar sequencial por empresa: " & Err.Description, "BL", "BL_GeraRecEmpresa"
    Set CmdGera = Nothing
    Set PmtGera = Nothing
    Set PmtCampo = Nothing
    Exit Function
    Resume Next
End Function
Function BL_DesbloqueiaRecibo(nRec As String, sucesso As Integer) As Long


    Dim CmdGera As New ADODB.Command
    Dim PmtGera As ADODB.Parameter
    Dim PmtRec As ADODB.Parameter
    Dim PmtSuc As ADODB.Parameter
        
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdGera
        .ActiveConnection = gConexao
        .CommandText = "DESBLOQUEIA_RECIBO"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtGera = CmdGera.CreateParameter("RES", adNumeric, adParamOutput, 0)
    PmtGera.Precision = 12
    Set PmtRec = CmdGera.CreateParameter("P_N_REC", adNumeric, adParamInput, 0)
    Set PmtSuc = CmdGera.CreateParameter("PSUCESSO", adNumeric, adParamInput, 0)
    CmdGera.Parameters.Append PmtRec
    CmdGera.Parameters.Append PmtSuc
    CmdGera.Parameters.Append PmtGera
    
    CmdGera.Parameters("P_N_REC") = nRec
    CmdGera.Parameters("PSUCESSO") = sucesso
    CmdGera.Execute
    
    BL_DesbloqueiaRecibo = Trim(CmdGera.Parameters.Item("RES").value)
       
    Set CmdGera = Nothing
    Set PmtRec = Nothing
    Set PmtSuc = Nothing

    
Exit Function
TrataErro:
    BL_DesbloqueiaRecibo = -1
    BG_LogFile_Erros "Erro ao desbloquear recibo: " & Err.Description, "BL", "BL_DesbloqueiaRecibo"
    Set CmdGera = Nothing
    Set PmtRec = Nothing
    Set PmtSuc = Nothing
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' VERIFICA SE UM DETERMINADO LOCAL LHE INTERESSAM ANALISES SEM CODIGO LOCAL

' ------------------------------------------------------------------------------------------------
Public Function BL_PesquisaAnalisesSemLocal(cod_local As Integer) As Boolean
    Dim i As Long
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    On Error GoTo TrataErro
    BL_PesquisaAnalisesSemLocal = False
    
    sSql = "SELECT * FROM gr_empr_inst WHERE cod_empr = " & gCodLocal
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount >= 1 Then
        If BL_HandleNull(rsAna!flg_inibe_analises_nulas, 0) = 0 Then
            BL_PesquisaAnalisesSemLocal = True
            Exit Function
        End If
    End If
    rsAna.Close
    
    BL_PesquisaAnalisesSemLocal = False
Exit Function
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal - BL_PesquisaAnalisesSemLocal: " & Err.Description, "BL", "BL_PesquisaAnalisesSemLocal"
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' DEVOLVE OS MICRORGANISMOS QUE ESTAO ASSOCIADOS A ESTE.

' ------------------------------------------------------------------------------------------------
Public Function BL_DevolveMicroAssociados(cod_micro As String) As String
    Dim sSql As String
    Dim rsMicro As New ADODB.recordset
    On Error GoTo TrataErro
    
    BL_DevolveMicroAssociados = ""
    sSql = "SELECT descr_microrg FROM sl_microrg WHERE cod_microrg_assoc = " & BL_TrataStringParaBD(cod_micro)
    rsMicro.CursorLocation = adUseServer
    rsMicro.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsMicro.Open sSql, gConexao
    If rsMicro.RecordCount >= 1 Then
        While Not rsMicro.EOF
            BL_DevolveMicroAssociados = BL_DevolveMicroAssociados & BL_HandleNull(rsMicro!descr_microrg, "") & vbCrLf
            rsMicro.MoveNext
        Wend
    End If
    rsMicro.Close
    Set rsMicro = Nothing
    BL_DevolveMicroAssociados = Mid(BL_DevolveMicroAssociados, 1, 400)
Exit Function
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: " & Err.Description, "BL", "BL_DevolveMicroAssociados"
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' ACTUALIZA DATA DE IMPRESSAO DE CARTOES PARA DATA DO DIA.

' ------------------------------------------------------------------------------------------------
Public Sub BL_ActualizaDataImprCartao(t_utente As String, Utente As String)
    Dim sSql As String
    On Error GoTo TrataErro
    sSql = "UPDATE sl_identif SET dt_impr_cartao = " & BL_TrataDataParaBD(Bg_DaData_ADO) & " WHERE t_utente = " & BL_TrataStringParaBD(t_utente)
    sSql = sSql & " AND utente = " & BL_TrataStringParaBD(Utente)
    BG_ExecutaQuery_ADO sSql
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: " & Err.Description, "BL", "BL_ActualizaDataImprCartao"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' DEVOLVE DATA E HORA DE VALIDACAO DA REQUISICAO

' ------------------------------------------------------------------------------------------------
Sub BL_Devolve_Max_Dt_Hr_Validacao(NumReq As String, ByRef DtValidacao As String, ByRef HrValidacao As String)
    
    Dim sql As String
    Dim RsValidacao As ADODB.recordset
    
    On Error GoTo TrataErro
    
    DtValidacao = ""
    HrValidacao = ""
    sql = "SELECT dt_fecho, hr_fecho FROM sl_Requis where n_Req = " & NumReq & " AND user_fecho IS NOT NULL "
    Set RsValidacao = New ADODB.recordset
    RsValidacao.CursorLocation = adUseServer
    RsValidacao.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsValidacao.Open sql, gConexao
    If RsValidacao.RecordCount > 0 Then
        RsValidacao.MoveFirst
        DtValidacao = BL_HandleNull(RsValidacao!dt_fecho, "")
        HrValidacao = BL_HandleNull(RsValidacao!hr_fecho, "")
        RsValidacao.Close
        Set RsValidacao = Nothing
    Else
        RsValidacao.Close
        'rcoelho 18.06.2013 chvng-4229
        sql = "SELECT dt_val, hr_val FROM sl_realiza WHERE n_req=" & NumReq & _
            " AND dt_val IS NOT NULL ORDER BY dt_val desc,hr_val desc"
        RsValidacao.CursorLocation = adUseServer
        RsValidacao.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsValidacao.Open sql, gConexao
        If RsValidacao.RecordCount > 0 Then
            RsValidacao.MoveFirst
            DtValidacao = BL_HandleNull(RsValidacao!dt_val, "")
            HrValidacao = BL_HandleNull(RsValidacao!hr_val, "")
        End If
        RsValidacao.Close
        sql = "SELECT dt_val, hr_val FROM sl_res_tsq WHERE seq_realiza in (SELECT seq_realiza from sl_realiza WHERE n_req = " & NumReq & ") ORDER BY dt_val desc, hr_val desc "
        RsValidacao.CursorLocation = adUseServer
        RsValidacao.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsValidacao.Open sql, gConexao
        If RsValidacao.RecordCount > 0 Then
            RsValidacao.MoveFirst
            If BL_HandleNull(RsValidacao!dt_val, "") > DtValidacao Then
                DtValidacao = BL_HandleNull(RsValidacao!dt_val, "")
                HrValidacao = BL_HandleNull(RsValidacao!hr_val, "")
            ElseIf BL_HandleNull(RsValidacao!dt_val, "") = DtValidacao Then
                If BL_HandleNull(RsValidacao!hr_val, "") >= HrValidacao Then
                    DtValidacao = BL_HandleNull(RsValidacao!dt_val, "")
                    HrValidacao = BL_HandleNull(RsValidacao!hr_val, "")
                End If
            End If
        End If
        RsValidacao.Close
        Set RsValidacao = Nothing
    End If
Exit Sub
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_Devolve_Max_Dt_Hr_Validacao: " & Err.Description, "BL", "BL_Devolve_Max_Dt_Hr_Validacao"
    Exit Sub
    Resume Next
End Sub



' ------------------------------------------------------------------------------------------------

' APLICA PROFILE DEFINIDO AO UTILIZADOR

' ------------------------------------------------------------------------------------------------
Public Sub BL_AplicaProfile(profile As String)
    Dim sSql As String
    Dim rsProfile As New ADODB.recordset
    On Error GoTo TrataErro
    
    If profile = "" Then Exit Sub
    ' VERIFICA SE PROFILE TEM ALGUM PAI
    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "profiles WHERE cod_profile = " & profile
    rsProfile.CursorLocation = adUseServer
    rsProfile.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsProfile.Open sSql, gConexao
    If rsProfile.RecordCount >= 1 Then
        If BL_HandleNull(rsProfile!cod_pai, "0") <> "0" Then
            BL_AplicaProfile rsProfile!cod_pai
        End If
    ElseIf rsProfile.RecordCount <= 0 Then
        Exit Sub
    End If
    'APLICA DETALHE DO PROFILE
    BL_AplicaProfileDet profile
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_AplicaProfileUtil: " & Err.Description, "BL", "BL_AplicaProfileUtil"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' APLICA DETALHE DO PROFILE DEFINIDO AO UTILIZADOR

' ------------------------------------------------------------------------------------------------
Private Sub BL_AplicaProfileDet(profile As String)
    Dim sSql As String
    Dim rsProfile As New ADODB.recordset
    Dim flg_preenche As Boolean
    Dim i As Integer
    On Error GoTo TrataErro
    flg_preenche = False
    If gTotalMenus = 0 Then
        ReDim gEstrutMenus(0)
        flg_preenche = True
    ElseIf gMudouUtilizador = True Then
        gMudouUtilizador = False
        For i = 1 To gTotalMenus
            If gEstrutMenus(i).indice > -1 Then
                MDIFormInicio.Controls(gEstrutMenus(i).Objecto)(gEstrutMenus(i).indice).Visible = gEstrutMenus(i).visivel
            Else
                MDIFormInicio.Controls(gEstrutMenus(i).Objecto).Visible = gEstrutMenus(i).visivel
            End If
        Next i
    End If
    ' VERIFICA SE PROFILE TEM ALGUM PAI
    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "profiles_det WHERE cod_profile = " & profile
    rsProfile.CursorLocation = adUseServer
    rsProfile.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsProfile.Open sSql, gConexao
    If rsProfile.RecordCount >= 1 Then
        While Not rsProfile.EOF
            If flg_preenche = True Then
                gTotalMenus = gTotalMenus + 1
                ReDim Preserve gEstrutMenus(gTotalMenus)
                gEstrutMenus(gTotalMenus).Objecto = rsProfile!cod_menu
                gEstrutMenus(gTotalMenus).indice = BL_HandleNull(rsProfile!indice_menu, -1)
                If BL_HandleNull(rsProfile!indice_menu, -1) > -1 Then
                    gEstrutMenus(gTotalMenus).visivel = CBool(MDIFormInicio.Controls(rsProfile!cod_menu)(rsProfile!indice_menu).Visible)
                Else
                    gEstrutMenus(gTotalMenus).visivel = MDIFormInicio.Controls(rsProfile!cod_menu).Visible
                End If
            End If
            If BL_HandleNull(rsProfile!Estado, gProfileDefeito) = gProfileVisivel Then
                BL_PoePermissao_ADO "MDIFormInicio", BL_HandleNull(rsProfile!cod_menu, ""), "Visible", "True", BL_HandleNull(rsProfile!indice_menu, -1)
            ElseIf BL_HandleNull(rsProfile!Estado, gProfileDefeito) = gProfileInvisivel Then
                BL_PoePermissao_ADO "MDIFormInicio", BL_HandleNull(rsProfile!cod_menu, ""), "Visible", "False", BL_HandleNull(rsProfile!indice_menu, -1)
            End If
            rsProfile.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    'BG_LogFile_Erros "BibliotecaLocal: BL_AplicaProfileDet: " & Err.Description, "BL", "BL_AplicaProfileDet"
    'Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' APLICA PROFILE DEFINIDO AO UTILIZADOR (Objectos)

' ------------------------------------------------------------------------------------------------
Public Sub BL_AplicaProfile2(profile As String, nomeForm As String)
    Dim sSql As String
    Dim rsProfile As New ADODB.recordset
    On Error GoTo TrataErro
    If profile = "" Then Exit Sub
    ' VERIFICA SE PROFILE TEM ALGUM PAI
    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "profiles WHERE cod_profile = " & profile
    rsProfile.CursorLocation = adUseServer
    rsProfile.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsProfile.Open sSql, gConexao
    If rsProfile.RecordCount >= 1 Then
        If BL_HandleNull(rsProfile!cod_pai, "0") <> "0" Then
            BL_AplicaProfile2 rsProfile!cod_pai, nomeForm
        End If
    ElseIf rsProfile.RecordCount <= 0 Then
        Exit Sub
    End If
    'APLICA OBJECTOS DO PROFILE
    BL_AplicaProfileObjecto profile, nomeForm
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_AplicaProfile2: " & Err.Description, "BL", "BL_AplicaProfile2"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' APLICA DETALHE DO PROFILE DEFINIDO AO UTILIZADOR

' ------------------------------------------------------------------------------------------------
Private Sub BL_AplicaProfileObjecto(profile As String, nomeForm As String)
    Dim sSql As String
    Dim rsProfile As New ADODB.recordset
    On Error GoTo TrataErro
    
    ' VERIFICA SE PROFILE TEM ALGUM PAI
    sSql = "SELECT * FROM " & gBD_PREFIXO_TAB & "profiles_obj WHERE cod_profile = " & profile & " AND cod_form = " & BL_TrataStringParaBD(nomeForm)
    rsProfile.CursorLocation = adUseServer
    rsProfile.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsProfile.Open sSql, gConexao
    If rsProfile.RecordCount >= 1 Then
        While Not rsProfile.EOF
            If BL_HandleNull(rsProfile!Estado, 3) = 5 Then
                BL_PoePermissao_ADO nomeForm, BL_HandleNull(rsProfile!cod_objecto, ""), "Visible", "True", BL_HandleNull(rsProfile!indice_objecto, -1)
            ElseIf BL_HandleNull(rsProfile!Estado, 3) = 4 Then
                BL_PoePermissao_ADO nomeForm, BL_HandleNull(rsProfile!cod_objecto, ""), "Visible", "False", BL_HandleNull(rsProfile!indice_objecto, -1)
            End If
            rsProfile.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_AplicaProfileObjecto: " & Err.Description, "BL", "BL_AplicaProfileObjecto"
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------------------------

' SE NOME > 30 CARACTERES ENTAO ABREVIA

' ------------------------------------------------------------------------------------------------
Public Function BL_AbreviaNome(nome As String, tamanho As Integer) As String
    Dim nomes() As String
    Dim old As String
    Dim neww As String
    Dim nome_new As String
    Dim i As Integer
    If Len(nome) > tamanho Then
        BL_Tokenize nome, " ", nomes
        old = ""
        neww = ""
        For i = UBound(nomes) - 1 To 1 Step -1
            If Len(nomes(i)) > 2 Then
                old = nomes(i)
                neww = Mid(nomes(i), 1, 1) & "."
                Exit For
            End If
        Next
        If old = "" Then
            BL_AbreviaNome = nome
            Exit Function
        End If
        nome_new = Replace(nome, old, neww)
        If Len(nome_new) > tamanho Then
            nome_new = BL_AbreviaNome(nome_new, tamanho)
        End If
        BL_AbreviaNome = nome_new
    Else
        BL_AbreviaNome = nome
    End If
Exit Function
TrataErro:
    BL_AbreviaNome = nome
    BG_LogFile_Erros "BibliotecaLocal: Erro ao abreviar nome: " & Err.Description, "BL", "BL_AbreviaNome"
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' REGISTA IMPRESSAO DA REQUISI��O

' ------------------------------------------------------------------------------------------------
Public Sub BL_RegistaImprimeReq(NReq As String, seq_impr As Long)
    Dim sSql As String
    On Error GoTo TrataErro
    
    'BRUNODSANTOS LRV-396 - 30.08.2016
    If gValidaGeraVersaoReport = mediSim Then
        If Trim(NReq) <> Trim(HashReport.NumRequis) Then
            Exit Sub
        End If
    End If
    ' -------------------------------------------------------------------------------------------------------
    ' INSERE NA TABELA DE IMPRESSOES
    ' -------------------------------------------------------------------------------------------------------
    'BRUNODSANTOS CHSJ-2670 - 10.05.2016 -> Adicionado hash, versao
    If gImprimirDestino = 1 Then
        sSql = "INSERT INTO sl_impr_completas(seq_impr, n_req, user_imp, dt_imp, hr_imp, hash, versao) VALUES("
        sSql = sSql & seq_impr & ", " & IIf(gValidaGeraVersaoReport = mediSim, HashReport.NumRequis, NReq) & ", " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", " & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", " & BL_TrataStringParaBD(HashReport.Hash) & ", " & BL_TrataStringParaBD(HashReport.versao) & " )"
        BG_ExecutaQuery_ADO sSql
    End If
    
    'BRUNODSANTOS CHSJ-2670 - 10.05.2016
    HashReport.NumRequis = ""
    HashReport.Hash = ""
    HashReport.versao = ""
    '
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_RegistaImprimeReq: " & Err.Description, "BL", "BL_RegistaImprimeReq"
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------------------------

' RETORNA ULTIMA DATA DO ENVIO PARA ERESULTS DA REQUISI��O EM CAUSA

' ------------------------------------------------------------------------------------------------
Public Function BL_RetornaDataEnvioEresults(n_req As String) As String
    Dim sSql As String
    Dim rsER As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT dt_cri, hr_cri FROM sl_eresults_envio WHERE n_Req = " & (n_req) & " ORDER BY dt_cri DESC, hr_cri DESC "
    rsER.CursorLocation = adUseServer
    rsER.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsER.Open sSql, gConexao
    If rsER.RecordCount >= 1 Then
        BL_RetornaDataEnvioEresults = BL_HandleNull(rsER!dt_cri, "") & " " & BL_HandleNull(rsER!hr_cri, "")
    Else
        BL_RetornaDataEnvioEresults = ""
    End If
    rsER.Close
    Set rsER = Nothing
    
Exit Function
TrataErro:
    BL_RetornaDataEnvioEresults = ""
    BG_LogFile_Erros "BibliotecaLocal: BL_RetornaDataEnvioEresults: " & Err.Description, "BL", "BL_RetornaDataEnvioEresults"
    Exit Function
    Resume Next
End Function
' ------------------------------------------------------------------------------------------------

' RETORNA O ESTADO DO ENVIO PARA ERESULTS DA REQUISI��O EM CAUSA

' ------------------------------------------------------------------------------------------------
Public Function BL_RetornaEstadoEnvioEresults(n_req As String) As Integer
    Dim sSql As String
    Dim rsER As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT flg_estado FROM sl_eresults WHERE cod_original = " & BL_TrataStringParaBD(n_req)
    rsER.CursorLocation = adUseServer
    rsER.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsER.Open sSql, gConexao
    If rsER.RecordCount >= 1 Then
        BL_RetornaEstadoEnvioEresults = BL_HandleNull(rsER!flg_estado, -1)
    Else
        BL_RetornaEstadoEnvioEresults = -1
    End If
    rsER.Close
    Set rsER = Nothing
    
Exit Function
TrataErro:
    BL_RetornaEstadoEnvioEresults = -1
    BG_LogFile_Erros "BibliotecaLocal: BL_RetornaEstadoEnvioEresults: " & Err.Description, "BL", "BL_RetornaEstadoEnvioEresults"
    Exit Function
    Resume Next
End Function

' pferreira 2009.09.23
' Verifica dias indisponiveis e dias fim-de-semana num intervalo de dias.
Public Function BL_VerficaDiasIndisponiveis(ByVal n_dias As Long, data_chegada As String) As Date
    
    Dim i As Integer
    Dim j As Integer
    Dim n_indisp As Long
    Dim n_indisp2 As Long
    Dim diasIndisponiveis() As String
    Dim total_indisp As Long
    Dim flgJaExiste As Boolean
    On Error GoTo TrataErro
    ReDim diasIndisponiveis(0)
    
    n_indisp = 0
    For i = 1 To n_dias
        If Weekday(CDate(data_chegada) + i) = vbSaturday Then
            ReDim Preserve diasIndisponiveis(UBound(diasIndisponiveis) + 2)
            diasIndisponiveis(UBound(diasIndisponiveis) - 1) = CDate(data_chegada) + i
            diasIndisponiveis(UBound(diasIndisponiveis)) = CDate(data_chegada) + i + 1
            n_indisp = n_indisp + 2
            i = i + 1
        ElseIf Weekday(CDate(data_chegada) + i) = vbSunday Or BL_VerificaDiaFeriado(CDate(data_chegada) + i) Then
            ReDim Preserve diasIndisponiveis(UBound(diasIndisponiveis) + 1)
            diasIndisponiveis(UBound(diasIndisponiveis) - 1) = CDate(data_chegada) + i
            n_indisp = n_indisp + 1
        End If
    Next
    total_indisp = 0
    While n_indisp > 0
        n_indisp2 = n_indisp
        n_indisp = 0
        For i = 1 To n_indisp2
            If Weekday(CDate(data_chegada) + n_dias + i) = vbSaturday Then
                'VERIFICA SE SABADO JA ESTA NA LISTA DOS INDISPONIVEIS
                flgJaExiste = False
                For j = 1 To UBound(diasIndisponiveis)
                    If diasIndisponiveis(j) <> "" Then
                        If diasIndisponiveis(j) = CDate(data_chegada) + n_dias + i Then
                            flgJaExiste = True
                            Exit For
                        End If
                    End If
                Next
                If flgJaExiste = False Then
                    n_indisp = n_indisp + 1
                End If
                
'                'VERIFICA SE DOMINGO JA ESTA NA LISTA DOS INDISPONIVEIS
'                flgJaExiste = False
'                For J = 1 To UBound(diasIndisponiveis)
'                    If diasIndisponiveis(J) <> "" Then
'                        If diasIndisponiveis(J) = CDate(data_chegada) + n_dias + i + 1 Then
'                            flgJaExiste = True
'                            Exit For
'                        End If
'                    End If
'                Next
'                If flgJaExiste = False Then
'                    n_indisp = n_indisp + 1
'                End If
'
'                i = i + 1
            ElseIf Weekday(CDate(data_chegada) + n_dias + i) = vbSunday Or BL_VerificaDiaFeriado(CDate(data_chegada) + n_dias + i) Then
                'VERIFICA SE DOMINGO JA ESTA NA LISTA DOS INDISPONIVEIS
                flgJaExiste = False
                For j = 1 To UBound(diasIndisponiveis)
                    If diasIndisponiveis(j) <> "" Then
                        If diasIndisponiveis(j) = CDate(data_chegada) + n_dias + i Then
                            flgJaExiste = True
                            Exit For
                        End If
                    End If
                Next
                If flgJaExiste = False Then
                    n_indisp = n_indisp + 1
                End If
                
            End If
        Next
        n_dias = n_dias + n_indisp2
    Wend
    BL_VerficaDiasIndisponiveis = CDate(data_chegada) + n_dias
    Exit Function
    
TrataErro:
    BL_VerficaDiasIndisponiveis = CDate(data_chegada) + n_indisp
    BG_LogFile_Erros "BibliotecaLocal: BL_VerficaDiasIndisponiveis: " & Err.Description, "BL", "BL_VerficaDiasIndisponiveis"
    Exit Function
    Resume Next
    
End Function
' pferreira 2009.09.28
' Verifica se a data passada como argumento � um feriado.
Public Function BL_VerificaDiaFeriado(data As String) As Boolean

    Dim sSql As String
    Dim RsFeriado As New ADODB.recordset
    
    On Error GoTo TrataErro
    sSql = "select * from sl_agenda_indisponiveis where t_dia = 'F' and dia_indisp = " & BL_TrataDataParaBD(data)
    RsFeriado.CursorLocation = adUseServer
    RsFeriado.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsFeriado.Open sSql, gConexao
    BL_VerificaDiaFeriado = (RsFeriado.RecordCount > 0)
    If (RsFeriado.state = adStateOpen) Then: RsFeriado.Close
    Exit Function
    
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_VerificaDiaFeriado: " & Err.Description, "BL", "BL_VerificaDiaFeriado"
    Exit Function
    Resume Next
    
End Function




Public Function BL_VerificaAlarmeMicro(n_req As String) As Boolean
    Dim sSql As String
    Dim rsAviso As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT * FROM sl_realiza x1, sl_res_micro x2, sl_microrg x3 WHERE x1.n_Req = " & n_req
    sSql = sSql & " AND x1.seq_realiza = x2.seq_realiza AND x2.cod_micro = x3.cod_microrg AND x3.flg_alerta = 1 "
    rsAviso.CursorLocation = adUseServer
    rsAviso.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAviso.Open sSql, gConexao
    If rsAviso.RecordCount > 0 Then
        BL_VerificaAlarmeMicro = True
    Else
        BL_VerificaAlarmeMicro = False
    End If
    rsAviso.Close
Exit Function
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_VerificaAlarmeMicro: " & Err.Description, "BL", "BL_VerificaAlarmeMicro", True
    Exit Function
    Resume Next
End Function

Function BL_DevolveNomeUtilizadorActual(Optional codigo As Variant) As String
    
    Dim sql As String
    Dim RsNome As ADODB.recordset
    Dim CodigoUtilizador As String
    
    If Not IsMissing(codigo) Then
        CodigoUtilizador = codigo
    End If
    
    On Error GoTo TrataErro
    If CodigoUtilizador = "" Then
        sql = "SELECT nome FROM sl_idutilizador WHERE cod_utilizador=" & gCodUtilizador
    Else
        sql = "SELECT nome FROM sl_idutilizador WHERE cod_utilizador=" & CodigoUtilizador
    End If
    
    Set RsNome = New ADODB.recordset
    RsNome.CursorLocation = adUseServer
    RsNome.CursorType = adOpenStatic
    RsNome.Open sql, gConexao
    If RsNome.RecordCount > 0 Then
        BL_DevolveNomeUtilizadorActual = BL_HandleNull(RsNome!nome, "")
    End If
    RsNome.Close
    Set RsNome = Nothing
    
    Exit Function

TrataErro:

    BL_DevolveNomeUtilizadorActual = ""

End Function

' pferreira 2010.02.15
Function BL_DevolveNomeUtilizador(codigo As String, num_mecanografico As String, utilizador As String) As String
    
    Dim sql As String
    Dim RsNome As ADODB.recordset
    Dim CodigoUtilizador As String
    
    If Not IsMissing(codigo) Or codigo = "" Then
        CodigoUtilizador = codigo
    End If
    
    On Error GoTo TrataErro
    If utilizador <> "" Then
        sql = "SELECT nome FROM sl_idutilizador WHERE utilizador=" & BL_TrataStringParaBD(utilizador)
    ElseIf num_mecanografico = "" Then
        If CodigoUtilizador = "" Then
            sql = "SELECT nome FROM sl_idutilizador WHERE cod_utilizador=" & gCodUtilizador
        Else
            sql = "SELECT nome FROM sl_idutilizador WHERE cod_utilizador=" & CodigoUtilizador
        End If
    Else
        sql = "SELECT nome FROM sl_idutilizador WHERE num_mecanografico=" & BL_TrataStringParaBD(num_mecanografico)
    End If
    Set RsNome = New ADODB.recordset
    RsNome.CursorLocation = adUseServer
    RsNome.CursorType = adOpenStatic
    RsNome.Open sql, gConexao
    If RsNome.RecordCount > 0 Then
        BL_DevolveNomeUtilizador = BL_HandleNull(RsNome!nome, "")
    End If
    RsNome.Close
    Set RsNome = Nothing
    
Exit Function
TrataErro:
    BL_DevolveNomeUtilizador = ""
    BG_LogFile_Erros "BibliotecaLocal: BL_DevolveNomeUtilizador: " & Err.Description, "BL", "BL_DevolveNomeUtilizador"
    Exit Function
    Resume Next
End Function

Function BL_DevolveNomeUtilizadorColheita(codigo As String) As String
    
    Dim sql As String
    Dim RsNome As ADODB.recordset
    
    On Error GoTo TrataErro
    sql = "SELECT descr_util_colh FROM sl_cod_util_colh WHERE cod_util_colh=" & BL_TrataStringParaBD(codigo)
    Set RsNome = New ADODB.recordset
    RsNome.CursorLocation = adUseServer
    RsNome.CursorType = adOpenStatic
    RsNome.Open sql, gConexao
    If RsNome.RecordCount > 0 Then
        BL_DevolveNomeUtilizadorColheita = BL_HandleNull(RsNome!descr_util_colh, "")
    End If
    RsNome.Close
    Set RsNome = Nothing
    
Exit Function
TrataErro:
    BL_DevolveNomeUtilizadorColheita = ""
    BG_LogFile_Erros "BibliotecaLocal: BL_DevolveNomeUtilizadorColheita: " & Err.Description, "BL", "BL_DevolveNomeUtilizadorColheita"
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' VERIFICA SE ENTIDADE PERMITE IMPRESS�O DE RESULTADOS NO RELAT�RIO ARS

' ------------------------------------------------------------------------------------------------
Public Function BL_VerificaEfrImpARS(cod_efr) As Boolean
    Dim sSql As String
    Dim rsER As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT flg_imprime_res_ars FROM sl_efr WHERE cod_efr = " & cod_efr
    rsER.CursorLocation = adUseServer
    rsER.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsER.Open sSql, gConexao
    If rsER.RecordCount >= 1 Then
        If BL_HandleNull(rsER!flg_imprime_res_ars, 0) = 1 Then
            BL_VerificaEfrImpARS = True
        Else
            BL_VerificaEfrImpARS = False
        End If
    Else
        BL_VerificaEfrImpARS = False
    End If
    rsER.Close
    Set rsER = Nothing
    
Exit Function
TrataErro:
    BL_VerificaEfrImpARS = False
    BG_LogFile_Erros "BibliotecaLocal: BL_VerificaEfrImpARS: " & Err.Description, "BL", "BL_VerificaEfrImpARS"
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' REGISTA ALTERACAO A TABELA SL_REQUIS

' ------------------------------------------------------------------------------------------------
Public Sub BL_RegistaAltRequis(n_req As String, campo As String, valor_ant As String, valor_novo As String, seq_alteracao As Integer)
    Dim sSql As String
    On Error GoTo TrataErro
    
    sSql = "INSERT INTO sl_req_alteracoes ( n_req, campo,valor_ant, valor_novo, user_cri, dt_cri, hr_cri, seq_alteracao) VALUES("
    sSql = sSql & BL_HandleNull(n_req, -1) & ","
    sSql = sSql & BL_TrataStringParaBD(campo) & ","
    sSql = sSql & BL_TrataStringParaBD(valor_ant) & ","
    sSql = sSql & BL_TrataStringParaBD(valor_novo) & ","
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ","
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ","
    sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ","
    sSql = sSql & seq_alteracao & ")"
    BG_ExecutaQuery_ADO sSql
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_RegistaAltRequis: " & Err.Description, "BL", "BL_RegistaAltRequis"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------

' CONTA O NUMERO DE XS QUE UMA REQUISICAO FOI ALTERADA

' ---------------------------------------------------------------------------------------------------
Public Function BL_ContaAltRequis(n_req As String) As Integer
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAlt As New ADODB.recordset
    BL_ContaAltRequis = 0
    If n_req = "" Then Exit Function
    sSql = "SELECT max(seq_alteracao) maximo FROM sl_Req_alteracoes WHERE n_Req = " & n_req
    rsAlt.CursorType = adOpenStatic
    rsAlt.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAlt.Open sSql, gConexao

    If rsAlt.RecordCount = 1 Then
        BL_ContaAltRequis = BL_HandleNull(rsAlt!maximo, 0)
    End If
    rsAlt.Close
    Set rsAlt = Nothing
Exit Function
TrataErro:
    BL_ContaAltRequis = 0
    BG_LogFile_Erros "Erro  BL_ContaAlteracoes: " & Err.Description, "BL", "BL_ContaAlteracoes", False
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' PREENCHE VECTOR DE BACKUP COM VALORES DO ECRA

' ------------------------------------------------------------------------------------------------
Public Sub BL_PreencheVectorBackup(NumCampos As Integer, ByVal CamposEc, CamposBckp() As String)
    On Error GoTo TrataErro
    Dim i As Integer
    For i = 0 To NumCampos - 1
        'TextBox, Label, MaskEdBox, RichTextBox
        If TypeOf CamposEc(i) Is TextBox Or _
           TypeOf CamposEc(i) Is Label Or _
           TypeOf CamposEc(i) Is MaskEdBox Or _
           TypeOf CamposEc(i) Is RichTextBox Then
           
             CamposBckp(i) = CamposEc(i)
        'DataCombo
        ElseIf TypeOf CamposEc(i) Is DataCombo Then
            CamposBckp(i) = BG_DataComboSel(CamposEc(i))
            If CamposBckp(i) = mediComboValorNull Then
                CamposBckp(i) = ""
            End If
        'ComboBox
        ElseIf TypeOf CamposEc(i) Is ComboBox Then
            CamposBckp(i) = BG_DaComboSel(CamposEc(i))
            If CamposBckp(i) = mediComboValorNull Then
                CamposBckp(i) = ""
            End If
        'CheckBox
        ElseIf TypeOf CamposEc(i) Is CheckBox Then
            If CamposEc(i).value = 0 Then
                CamposBckp(i) = "0"
            ElseIf CamposEc(i).value = 1 Then
                CamposBckp(i) = "1"
            Else
                CamposBckp(i) = ""
            End If
        Else
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_PreencheVectorBackup: " & Err.Description, "BL", "BL_PreencheVectorBackup"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' TRATA HORA

' ------------------------------------------------------------------------------------------------
Private Function BL_TrataHora(hora As String) As String
    On Error GoTo TrataErro
    Dim v() As String
    BL_Tokenize hora, ":", v
    If Len(v(0)) = 0 Then
        v(0) = "00"
    ElseIf Len(v(0)) = 1 Then
        v(0) = "0" & v(0)
    End If
    If UBound(v) >= 1 Then
        If Len(v(1)) = 0 Then
            v(1) = "00"
        ElseIf Len(v(1)) = 1 Then
            v(1) = "0" & v(1)
        End If
    End If
    If UBound(v) >= 2 Then
        If Len(v(2)) = 0 Then
            v(2) = "00"
        ElseIf Len(v(2)) = 1 Then
            v(2) = "0" & v(2)
        End If
    Else
        ReDim Preserve v(3)
        v(2) = "00"
    End If
    BL_TrataHora = v(0) & ":" & v(1) & ":" & v(2)
    
Exit Function
TrataErro:
    BL_TrataHora = "00:00:00"
    BG_LogFile_Erros "BibliotecaLocal: BL_TrataHora: " & Err.Description, "BL", "BL_TrataHora"
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' VERIFICA QUAL TIPO DE ANALISE E PROCURA RESULTADO ANTERIOR

' ------------------------------------------------------------------------------------------------
Public Function BL_RetornaResultadoAnterior(n_req As String, dt_chega As String, Cod_Perfil As String, _
                                            cod_ana_c As String, cod_ana_s As String) As String
    Dim sSql As String
    Dim seq_utente As String
    Dim seq_realiza As String
    Dim resultado As String
    Dim t_result As String
    Dim RsRes As New ADODB.recordset
    On Error GoTo TrataErro
    
    'SEQ_UTENTE
    seq_utente = ""
    sSql = "SELECT seq_utente FROM sl_requis WHERE n_req = " & n_req
    RsRes.CursorLocation = adUseServer
    RsRes.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRes.Open sSql, gConexao
    If RsRes.RecordCount >= 1 Then
        seq_utente = BL_HandleNull(RsRes!seq_utente, "")
    End If
    RsRes.Close
    
    'TIPO DE RESULTADO
    t_result = ""
    sSql = "SELECT t_result FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    RsRes.CursorLocation = adUseServer
    RsRes.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRes.Open sSql, gConexao
    If RsRes.RecordCount >= 1 Then
        t_result = BL_HandleNull(RsRes!t_result, "")
    End If
    RsRes.Close
    
    If seq_utente = "" Or t_result = "" Then
        Exit Function
    End If
    
    Select Case t_result
        Case gT_Alfanumerico, gT_Numerico, gT_Auxiliar
            sSql = " SELECT x1.seq_realiza, x2.result resultado, x1.dt_cri data, null descr_produto "
            sSql = sSql & " FROM sl_realiza x1, sl_res_alfan x2 "
            sSql = sSql & " WHERE x1.seq_realiza = x2.seq_realiza AND x1.seq_utente = " & seq_utente
            sSql = sSql & " AND x1.n_req <> " & n_req & " AND x1.dt_cri <= " & BL_TrataDataParaBD(dt_chega)
            sSql = sSql & " AND x1.cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
            sSql = sSql & " AND x1.flg_estado in ('3','4') "
            sSql = sSql & " ORDER BY x1.dt_cri DESC "
        Case gT_Frase
            sSql = " SELECT x1.seq_realiza, x3.descr_frase resultado, x1.dt_cri data, null descr_produto "
            sSql = sSql & " FROM sl_realiza x1, sl_res_frase x2, sl_dicionario x3 "
            sSql = sSql & " WHERE x1.seq_realiza = x2.seq_realiza AND x1.seq_utente = " & seq_utente
            sSql = sSql & " AND x1.n_req <> " & n_req & " AND x1.dt_cri <= " & BL_TrataDataParaBD(dt_chega)
            sSql = sSql & " AND x1.cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
            sSql = sSql & " AND x1.flg_estado in ('3','4') "
            sSql = sSql & " AND x2.cod_Frase = x3.cod_frase "
            sSql = sSql & " ORDER BY x1.dt_cri DESC "
        Case gT_Microrganismo
            sSql = " SELECT x1.seq_realiza, x3.descr_microrg resultado, x1.dt_cri data, x5.descr_produto "
            sSql = sSql & " FROM sl_realiza x1, sl_res_micro x2, sl_microrg x3, sl_perfis x4, sl_produto x5 "
            sSql = sSql & " WHERE x1.seq_realiza = x2.seq_realiza AND x1.seq_utente = " & seq_utente
            sSql = sSql & " AND x1.n_req <> " & n_req & " AND x1.dt_cri <= " & BL_TrataDataParaBD(dt_chega)
            sSql = sSql & " AND x1.cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
            sSql = sSql & " AND x1.flg_estado in ('3','4') "
            sSql = sSql & " AND x2.cod_micro = x3.cod_microrg "
            sSql = sSql & " AND x1.cod_perfil = x4.cod_perfis"
            sSql = sSql & " AND x4.cod_produto = x5.cod_produto"
            sSql = sSql & " ORDER BY x1.dt_cri DESC "
    End Select
    
    RsRes.CursorLocation = adUseServer
    RsRes.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRes.Open sSql, gConexao
    If RsRes.RecordCount >= 1 Then
        seq_realiza = BL_HandleNull(RsRes!seq_realiza, "")
        resultado = BL_HandleNull(RsRes!data, "") & ": "
        While Not RsRes.EOF
            If RsRes!seq_realiza = seq_realiza Then
                resultado = resultado & BL_HandleNull(RsRes!resultado, "")
                If BL_HandleNull(RsRes!descr_produto, "") <> "" Then
                    resultado = resultado & " (" & RsRes!descr_produto & ") "
                End If
                resultado = resultado & "; "
            Else
                resultado = resultado & vbCrLf & BL_HandleNull(RsRes!data, "") & ": " & BL_HandleNull(RsRes!resultado, "")
                If BL_HandleNull(RsRes!descr_produto, "") <> "" Then
                    resultado = resultado & " (" & RsRes!descr_produto & ") "
                End If
                resultado = resultado & "; "
            End If
            RsRes.MoveNext
        Wend
    End If
    RsRes.Close
    Set RsRes = Nothing
    BL_RetornaResultadoAnterior = resultado
Exit Function
TrataErro:
    BL_RetornaResultadoAnterior = ""
    BG_LogFile_Erros "BibliotecaLocal: BL_RetornaResultadoAnterior: " & Err.Description, "BL", "BL_RetornaResultadoAnterior", False
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' RETORNA A DATA DE CHEGADA DO TUBO

' ------------------------------------------------------------------------------------------------
Public Function BL_RetornaDtChegaTubo(n_req As String, cod_tubo As String) As String
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM  sl_req_tubo WHERE n_req = " & n_req & " AND cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
    sSql = sSql & " AND dt_eliminacao IS NULL "
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTubo.Open sSql, gConexao
    If rsTubo.RecordCount >= 1 Then
        BL_RetornaDtChegaTubo = BL_HandleNull(rsTubo!dt_chega, "")
    Else
        BL_RetornaDtChegaTubo = ""
    End If
    rsTubo.Close
    Set rsTubo = Nothing
    
Exit Function
TrataErro:
    BL_RetornaDtChegaTubo = ""
    BG_LogFile_Erros "BibliotecaLocal: BL_RetornaDtChegaTubo: " & Err.Description, "BL", "BL_RetornaDtChegaTubo"
    Exit Function
    Resume Next
End Function

' pferreira 2011.06.16
Public Function BL_DevolveDescricaoAnalise(codigo_analise As String, Optional abrev As Boolean) As String
    
    Dim sql As String
    Dim Tabela As String
    Dim rsAna As ADODB.recordset
    
    On Error GoTo TrataErro
    If (codigo_analise = Empty) Then: Exit Function
    Select Case (Mid(codigo_analise, 1, 1))
        Case "P":
            sql = "select descr_perfis descr_ana "
            If (abrev = True) Then: sql = sql & ",abr_ana_p abr_ana"
            sql = sql & " from sl_perfis where cod_perfis = " & BL_TrataStringParaBD(codigo_analise)
        Case "C":
            sql = "select descr_ana_c descr_ana "
            If (abrev = True) Then: sql = sql & ",abr_ana_c abr_ana"
            sql = sql & " from sl_ana_c where cod_ana_c = " & BL_TrataStringParaBD(codigo_analise)
        Case "S":
            sql = "select descr_ana_s descr_ana "
            If (abrev = True) Then: sql = sql & ",abr_ana_s abr_ana"
            sql = sql & " from sl_ana_s where cod_ana_s = " & BL_TrataStringParaBD(codigo_analise)
        Case Else: Exit Function
    End Select
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    rsAna.Open sql, gConexao
    If (rsAna.RecordCount > Empty) Then: BL_DevolveDescricaoAnalise = BL_HandleNull(rsAna!descr_ana, Empty) & "_" & BL_HandleNull(rsAna!abr_ana, Empty)
    If (rsAna.state = adStateOpen) Then: rsAna.Close
    Exit Function

TrataErro:
    BG_Mensagem mediMsgBox, "Erro ao extrair informa��o da an�lise (" & codigo_analise & "): " & Err.Description
    BL_DevolveDescricaoAnalise = Empty

End Function

' pferreira 2010.07.22
Function BL_DevolveDescricaoCancelamento(codigo_cancelamento As String, tipo_cancelamento As String) As String
    
    Dim sql As String
    Dim rsCanc As ADODB.recordset
    
    On Error GoTo TrataErro
    sql = "select descr_canc from sl_t_canc where  cod_t_canc = " & codigo_cancelamento & " and t_util = " & BL_TrataStringParaBD(tipo_cancelamento)
    Set rsCanc = New ADODB.recordset
    rsCanc.CursorLocation = adUseServer
    rsCanc.CursorType = adOpenStatic
    rsCanc.Open sql, gConexao
    If (rsCanc.RecordCount > Empty) Then: BL_DevolveDescricaoCancelamento = BL_HandleNull(rsCanc!descr_canc, Empty)
    If (rsCanc.state = adStateOpen) Then: rsCanc.Close
    Exit Function

TrataErro:
    BL_DevolveDescricaoCancelamento = Empty

End Function

' ------------------------------------------------------------------------------------------------

' INICIALIZA  INFORMACAO

' ------------------------------------------------------------------------------------------------
Public Sub BL_Questoes(formAna As Form)
    Dim sSql As String
    Dim rsAnaInfo As New ADODB.recordset
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_dicionario WHERE flg_questionario = 1 ORDER BY descr_frase "
    Set rsAnaInfo = BG_ExecutaSELECT(sSql)
    If rsAnaInfo.RecordCount > 0 Then
        While Not rsAnaInfo.EOF
        
            formAna.EcQuestoes.AddItem BL_HandleNull(rsAnaInfo!descr_frase, "")
            formAna.EcQuestoes.ItemData(formAna.EcQuestoes.NewIndex) = BL_HandleNull(rsAnaInfo!seq_frase, "")
            rsAnaInfo.MoveNext
        Wend
    End If
    rsAnaInfo.Close
    Set rsAnaInfo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal - BL_Questoes: " & Err.Description, "BL", "BL_Questoes"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' LIMPA DADOS DOS CAMPOS DE QUESTIONARIO DA ANALISE NOS FORMS DE CODIFICACAO DE ANALISE

' ------------------------------------------------------------------------------------------------
Public Sub BL_LimpaDadosQuestoes(formE As Form)
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 0 To formE.EcQuestoes.ListCount - 1
        formE.EcQuestoes.Selected(i) = False
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_LimpaDadosQuestoes: " & Err.Description, "BL", "BL_LimpaDadosQuestoes"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' Grava DADOS DOS CAMPOS DE QUESTIONARIO DA ANALISE NOS FORMS DE CODIFICACAO DE ANALISE

' ------------------------------------------------------------------------------------------------
Public Function BL_GravaDadosQuestoes(formE As Form, codAna As String) As Boolean
    Dim sSql As String
    Dim RsOrdem As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    BL_GravaDadosQuestoes = False
    
    sSql = "DELETE FROM sl_ana_questoes WHERE cod_ana = " & BL_TrataStringParaBD(codAna)
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To formE.EcQuestoes.ListCount - 1
        
        If formE.EcQuestoes.Selected(i) = True Then
            
            sSql = "INSERT INTO sl_ana_questoes (cod_ana, cod_frase) "
            sSql = sSql & " SELECT " & BL_TrataStringParaBD(codAna) & ", cod_frase FROM sl_dicionario "
            sSql = sSql & " WHERE seq_Frase = " & formE.EcQuestoes.ItemData(i)
            BG_ExecutaQuery_ADO sSql
        End If
    Next
    BL_GravaDadosQuestoes = True
Exit Function
TrataErro:
    BL_GravaDadosQuestoes = False
    BG_LogFile_Erros "BibliotecaLocal: BL_GravaDadosQuestoes: " & Err.Description, "BL", "BL_GravaDadosQuestoes", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' LIMPA DADOS DOS CAMPOS DE INFORMA��O DA ANALISE NOS FORMS DE CODIFICACAO DE ANALISE

' ------------------------------------------------------------------------------------------------
Public Sub BL_PreencheDadosQuestoes(formE As Form, codAna As String)
    Dim sSql As String
    Dim rsAnaInfo As New ADODB.recordset
    Dim totalFolhasTrab As Long
    Dim i As Integer
    
    
    sSql = "SELECT x2.seq_frase ,x1.cod_frase FROM sl_ana_questoes x1, sl_dicionario x2 WHERE x1.cod_frase = x2.cod_frase "
    sSql = sSql & " AND x1.cod_ana = " & BL_TrataStringParaBD(codAna)
    Set rsAnaInfo = BG_ExecutaSELECT(sSql)
    If rsAnaInfo.RecordCount > 0 Then
        While Not rsAnaInfo.EOF
        
            For i = 0 To formE.EcQuestoes.ListCount - 1
                If formE.EcQuestoes.ItemData(i) = BL_HandleNull(rsAnaInfo!seq_frase, "") Then
                    formE.EcQuestoes.Selected(i) = True
                End If
            Next
            rsAnaInfo.MoveNext
        Wend
    End If
    rsAnaInfo.Close
    Set rsAnaInfo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_PreencheDadosQuestoes: " & Err.Description, "BL", "BL_PreencheDadosQuestoes", True
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' LIMPA DADOS DOS CAMPOS DE INFORMA��O DA ANALISE NOS FORMS DE CODIFICACAO DE ANALISE

' ------------------------------------------------------------------------------------------------
Public Sub BL_LimpaDadosInfAna(formE As Form)
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 0 To formE.EcInformacao.ListCount - 1
        formE.EcInformacao.Selected(i) = False
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_LimpaDadosInfAna: " & Err.Description, "BL", "BL_LimpaDadosInfAna"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' Grava DADOS DOS CAMPOS DE INFORMA��O DA ANALISE NOS FORMS DE CODIFICACAO DE ANALISE

' ------------------------------------------------------------------------------------------------
Public Function BL_GravaDadosInfAna(formE As Form, codAna As String) As Boolean
    Dim sSql As String
    Dim RsOrdem As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    BL_GravaDadosInfAna = False
    
    sSql = "DELETE FROM sl_ana_informacao WHERE cod_ana = " & BL_TrataStringParaBD(codAna)
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To formE.EcInformacao.ListCount - 1
        
        If formE.EcInformacao.Selected(i) = True Then
            
            sSql = "INSERT INTO sl_ana_informacao (cod_ana, cod_informacao) "
            sSql = sSql & " SELECT " & BL_TrataStringParaBD(codAna) & ", cod_informacao FROM sl_informacao "
            sSql = sSql & " WHERE seq_informacao = " & formE.EcInformacao.ItemData(i)
            BG_ExecutaQuery_ADO sSql
        End If
    Next
    BL_GravaDadosInfAna = True
Exit Function
TrataErro:
    BL_GravaDadosInfAna = False
    BG_LogFile_Erros "BibliotecaLocal: BL_GravaDadosInfAna: " & Err.Description, "BL", "BL_GravaDadosInfAna", True
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' LIMPA DADOS DOS CAMPOS DE INFORMA��O DA ANALISE NOS FORMS DE CODIFICACAO DE ANALISE

' ------------------------------------------------------------------------------------------------
Public Sub BL_PreencheDadosInfAna(formE As Form, codAna As String)
    Dim sSql As String
    Dim rsAnaInfo As New ADODB.recordset
    Dim totalFolhasTrab As Long
    Dim i As Integer
    
    
    sSql = "SELECT x2.seq_informacao,x1.cod_informacao FROM sl_ana_informacao x1, sl_informacao x2 WHERE x1.cod_informacao = x2.cod_informacao "
    sSql = sSql & " AND x1.cod_ana = " & BL_TrataStringParaBD(codAna)
    Set rsAnaInfo = BG_ExecutaSELECT(sSql)
    If rsAnaInfo.RecordCount > 0 Then
        While Not rsAnaInfo.EOF
        
            For i = 0 To formE.EcInformacao.ListCount - 1
                If formE.EcInformacao.ItemData(i) = BL_HandleNull(rsAnaInfo!seq_informacao, "") Then
                    formE.EcInformacao.Selected(i) = True
                End If
            Next
            rsAnaInfo.MoveNext
        Wend
    End If
    rsAnaInfo.Close
    Set rsAnaInfo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_PreencheDadosInfAna: " & Err.Description, "BL", "BL_PreencheDadosInfAna", True
    Exit Sub
    Resume Next
End Sub


Function BL_DevolveDescrPostal(codPostal As String) As String
    
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    Dim rua As String
    Dim codigo As String
    
    If codPostal <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal = " & BL_TrataStringParaBD(codPostal)
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BL_DevolveDescrPostal = ""
        Else
            BL_DevolveDescrPostal = Trim(rsCodigo!descr_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Function
Public Sub BL_FormataCodigo(EcCampo As TextBox)
    
End Sub

' ----------------------------------------------------------------------------------

' EXPANDE OU COMPRIME UM NO DA ARVORE

' ----------------------------------------------------------------------------------
Public Sub BL_expandeNo(no As Node, expande As Boolean)
    On Error GoTo TrataErro
    If (no Is Nothing) Then
      Exit Sub
    End If
    no.Expanded = expande
    Dim i As Integer
    Dim filho As Node
    Set filho = no.Child
    For i = 1 To no.children
      BL_expandeNo filho, expande
      Set filho = filho.Next
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  BL_expandeNo " & Err.Description, "BL", "BL_expandeNo"
    Exit Sub
    Resume Next
End Sub



Public Function BL_DaConteudoCombo(ValorCampoBD As Variant, NomeControl As Control) As String
    'Esta fun��o � usada no SIBAS,BRIGADAS na Area m�dica nos forms hist. recente e ex. fisico,
    'a fun�ao � usada para retornar os resultados anteriores de cada campo tipo combo
    'Esta fun��o � em tudo id�ntica � BG_MostraComboSel mas ao contrario desta n�o activa
    'nenhum item do controlo, que � passado como parametro, apenas retorna o texto da combo
    'correspondente ao item (ItemData) passado no parametro ValorCampoBD
    '(Filipe Nogueira)
    
    Dim i As Integer
    Dim Seleccionado As Integer

    If IsNull(ValorCampoBD) Or Not IsNumeric(ValorCampoBD) Then
        BL_DaConteudoCombo = ""
        Exit Function
    End If

    i = 0
    Seleccionado = False
    While (Seleccionado = False) And (i < NomeControl.ListCount)
        If NomeControl.ItemData(i) = CInt(ValorCampoBD) Then
            BL_DaConteudoCombo = NomeControl.List(i)
            Seleccionado = True
        End If
        i = i + 1
    Wend

    If Seleccionado = False Then
        BL_DaConteudoCombo = ""
    End If
    
End Function


' ----------------------------------------------------------------------------------

' VERIFICA PROVENIENCIA VALIDA

' ----------------------------------------------------------------------------------
Public Function BL_VerificaProvenValida(cod_proven As String, cod_local As String) As Boolean
    Dim sSql As String
    Dim mensagem As String
    Dim rsVal As New ADODB.recordset
    On Error GoTo TrataErro
    BL_VerificaProvenValida = False
    
    sSql = " SELECT * FROM sl_proven x1, sl_ass_proven_locais x2 WHERE x1.cod_proven = " & BL_TrataStringParaBD(cod_proven)
    sSql = sSql & " AND x1.cod_proven = x2.cod_proven AND x2.cod_local = " & cod_local
    rsVal.CursorLocation = adUseServer
    rsVal.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsVal.Open sSql, gConexao
    
    If rsVal.RecordCount < 1 Then
        mensagem = "Proveni�ncia n�o existe para este local."
    ElseIf BL_HandleNull(rsVal!flg_invisivel, 0) = 1 Then
        mensagem = "Proveni�ncia desactivada."
    Else
        mensagem = ""
    End If
    If mensagem <> "" Then
        BG_Mensagem mediMsgBox, mensagem, vbInformation, "Aten��o"
        BL_VerificaProvenValida = False
        
    Else
        BL_VerificaProvenValida = True
    End If
    rsVal.Close
    Set rsVal = Nothing

Exit Function
TrataErro:
    BL_VerificaProvenValida = False
    BG_LogFile_Erros "Erro  BL_VerificaProvenValida " & Err.Description & " " & sSql, "BL", "BL_VerificaProvenValida"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' VERIFICA ENTIDADE VALIDA

' ----------------------------------------------------------------------------------
Public Function BL_VerificaEfrValida(cod_efr As String, cod_local As String) As Boolean
    Dim sSql As String
    Dim mensagem As String
    Dim rsVal As New ADODB.recordset
    On Error GoTo TrataErro
    BL_VerificaEfrValida = False
    
    If cod_local <> mediComboValorNull Then
        sSql = " SELECT * FROM sl_efr x1, sl_ass_efr_locais x2 WHERE x1.cod_efr = " & cod_efr
        sSql = sSql & " AND x1.cod_efr = x2.cod_efr AND x2.cod_local = " & cod_local
    Else
        sSql = " SELECT * FROM sl_efr x1 WHERE x1.cod_efr = " & cod_efr
    End If
    rsVal.CursorLocation = adUseServer
    rsVal.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsVal.Open sSql, gConexao
    
    If rsVal.RecordCount < 1 Then
        mensagem = "Entidade n�o existe para este local."
    Else
        If BL_HandleNull(rsVal!flg_invisivel, 0) = mediSim Then
            mensagem = "Entidade desactiva"
        End If
        mensagem = ""
    End If
    If mensagem <> "" Then
        BG_Mensagem mediMsgBox, mensagem, vbInformation, "Aten��o"
        BL_VerificaEfrValida = False
        
    Else
        BL_VerificaEfrValida = True
    End If
    rsVal.Close
    Set rsVal = Nothing

Exit Function
TrataErro:
    BL_VerificaEfrValida = False
    BG_LogFile_Erros "Erro  BL_VerificaEfrValida " & Err.Description & " " & sSql, "BL", "BL_VerificaEfrValida"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' VERIFICA MEDICO VALIDA

' ----------------------------------------------------------------------------------
Public Function BL_VerificaMedValida(cod_med As String, cod_local As String) As Boolean
    Dim sSql As String
    Dim mensagem As String
    Dim rsVal As New ADODB.recordset
    On Error GoTo TrataErro
    BL_VerificaMedValida = False
    
    sSql = " SELECT * FROM sl_medicos x1, sl_ass_med_locais x2 WHERE x1.cod_med = " & BL_TrataStringParaBD(cod_med)
    sSql = sSql & " AND x1.cod_med = x2.cod_med AND x2.cod_local = " & cod_local
    rsVal.CursorLocation = adUseServer
    rsVal.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsVal.Open sSql, gConexao
    
    If rsVal.RecordCount < 1 Then
        mensagem = "M�dico n�o codificado para este local."
    Else
        mensagem = ""
    End If
    If mensagem <> "" Then
        BG_Mensagem mediMsgBox, mensagem, vbInformation, "Aten��o"
        BL_VerificaMedValida = False
        
    Else
        BL_VerificaMedValida = True
    End If
    rsVal.Close
    Set rsVal = Nothing

Exit Function
TrataErro:
    BL_VerificaMedValida = False
    BG_LogFile_Erros "Erro  BL_VerificaMedValida " & Err.Description & " " & sSql, "BL", "BL_VerificaMedValida"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' VERIFICA SALA VALIDA

' ----------------------------------------------------------------------------------
Public Function BL_VerificaSalaValida(cod_sala As String, cod_local As String) As Boolean
    Dim sSql As String
    Dim mensagem As String
    Dim rsVal As New ADODB.recordset
    On Error GoTo TrataErro
    BL_VerificaSalaValida = False
    
    sSql = " SELECT * FROM sl_cod_salas x1, sl_ass_salas_locais x2 WHERE x1.cod_sala = " & cod_sala
    sSql = sSql & " AND x1.cod_sala = x2.cod_sala AND x2.cod_local = " & cod_local
    rsVal.CursorLocation = adUseServer
    rsVal.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsVal.Open sSql, gConexao
    
    If rsVal.RecordCount < 1 Then
        mensagem = "Sala n�o existe para este local."
    ElseIf BL_HandleNull(rsVal!flg_invisivel, 0) = 1 Then
        mensagem = "Sala desactivada."
    Else
        mensagem = ""
    End If
    If mensagem <> "" Then
        BG_Mensagem mediMsgBox, mensagem, vbInformation, "Aten��o"
        BL_VerificaSalaValida = False
        
    Else
        BL_VerificaSalaValida = True
    End If
    rsVal.Close
    Set rsVal = Nothing

Exit Function
TrataErro:
    BL_VerificaSalaValida = False
    BG_LogFile_Erros "Erro  BL_VerificaSalaValida " & Err.Description & " " & sSql, "BL", "BL_VerificaSalaValida"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' VERIFICA NOTAS PARA UM UTENTE, COM OU SEM REQUISICAO, COM OU SEM GRUPO

' ----------------------------------------------------------------------------------
Public Function BL_VerificaNotas(seq_utente As String, n_req As String, cod_gr_nota As String) As Boolean
    Dim rsNota As New ADODB.recordset
    Dim sSql As String
    sSql = "SELECT*  FROM sl_obs_ana_req WHERE seq_utente = " & seq_utente
    If n_req <> "" Then
        sSql = sSql & " AND n_req = " & n_req
    End If
    If cod_gr_nota <> "" Then
        sSql = sSql & " AND cod_gr_nota = " & cod_gr_nota
    End If
    
    rsNota.CursorLocation = adUseServer
    rsNota.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsNota.Open sSql, gConexao
    If rsNota.RecordCount > 0 Then
        BL_VerificaNotas = True
    Else
        BL_VerificaNotas = False
    End If
    rsNota.Close
    Set rsNota = Nothing
    
Exit Function
TrataErro:
    BL_VerificaNotas = False
    BG_LogFile_Erros "Erro  BL_VerificaNotas " & Err.Description & " " & sSql, "BL", "BL_VerificaNotas"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------

' RETORNA DESCRICOES DAS ANALISES ASSOCIADAS A UM TUBO

' ----------------------------------------------------------------------------------
Public Function BL_RetornaAnalisesTubos(n_req As String, cod_tubo As String) As String
    Dim sSql As String
    Dim descr_etiq As String
    Dim rsTub As New ADODB.recordset
    On Error GoTo TrataErro
    BL_RetornaAnalisesTubos = ""
    sSql = "SELECT x1.cod_Ana, x2.cod_ana_s, x3.descr_etiq FROM slv_analises x1, sl_marcacoes x2, sl_ana_s x3 WHERE x2.n_req = " & n_req
    sSql = sSql & " AND x1.cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
    sSql = sSql & " AND (x1.cod_ana = x2.cod_perfil OR x1.cod_ana = x2.cod_ana_c OR x1.cod_ana = x2.cod_ana_s ) "
    sSql = sSql & " AND x2.cod_ana_S = x3.cod_ana_s and x3.descr_etiq is not null "
    rsTub.CursorLocation = adUseServer
    rsTub.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTub.Open sSql, gConexao
    If rsTub.RecordCount > 0 Then
        descr_etiq = ""
        While Not rsTub.EOF
            If InStr(1, UCase(descr_etiq), UCase(BL_HandleNull(rsTub!descr_etiq, ""))) = 0 Then
                descr_etiq = descr_etiq & UCase(BL_HandleNull(rsTub!descr_etiq, "")) & ";"
            End If
            rsTub.MoveNext
        Wend
    End If
    rsTub.Close
    Set rsTub = Nothing
    BL_RetornaAnalisesTubos = descr_etiq
Exit Function
TrataErro:
    BL_RetornaAnalisesTubos = ""
    BG_LogFile_Erros "Erro  BL_RetornaAnalisesTubos " & Err.Description & " " & sSql, "BL", "BL_RetornaAnalisesTubos"
    Exit Function
    Resume Next
End Function

Public Function BL_DevolveEstadoReq(EstadoReq As String) As String
    Dim i As Integer
    For i = 1 To UBound(gEstrutEstadosReq)
        If Trim(EstadoReq) = Trim(gEstrutEstadosReq(i).codigo) Then
            BL_DevolveEstadoReq = BL_HandleNull(gEstrutEstadosReq(i).descricao, "")
            Exit Function
        End If
    Next
End Function

Public Function BL_DevolveEstadoAna(EstadoAna As String) As String
    Dim i As Integer
    For i = 1 To UBound(gEstrutEstadosAna)
        If Trim(EstadoAna) = Trim(gEstrutEstadosAna(i).codigo) Then
            BL_DevolveEstadoAna = BL_HandleNull(gEstrutEstadosAna(i).descricao, "")
            Exit Function
        End If
    Next
End Function
Public Function BL_DevolveCorEstadoAna(EstadoAna As String) As Long
    Dim i As Integer
    For i = 1 To UBound(gEstrutEstadosAna)
        If Trim(EstadoAna) = Trim(gEstrutEstadosAna(i).codigo) Then
            BL_DevolveCorEstadoAna = BL_HandleNull(gEstrutEstadosAna(i).cor, vbWhite)
            Exit Function
        End If
    Next
End Function
Public Function BL_DevolveCorEstadoReq(EstadoReq As String) As Long
    Dim i As Integer
    For i = 1 To UBound(gEstrutEstadosReq)
        If Trim(EstadoReq) = Trim(gEstrutEstadosReq(i).codigo) Then
            BL_DevolveCorEstadoReq = BL_HandleNull(gEstrutEstadosReq(i).cor, vbWhite)
            Exit Function
        End If
    Next
End Function

' -------------------------------------------------------------------

' REGISTA AS ANALISES NA TABELA DE ANALISES ELIMINADAS

' -------------------------------------------------------------------
Public Function BL_RegistaAnaEliminadas(ByVal n_req As String, ByVal cod_agrup As String, ByVal Cod_Perfil As String, _
                                        ByVal cod_ana_c As String, ByVal cod_ana_s As String) As Boolean
    
    Dim i As Integer
    Dim total As Integer
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    BL_RegistaAnaEliminadas = False
    sSql = "INSERT INTO sl_ana_eliminadas(n_req,cod_agrup, cod_ana, cod_perfis, cod_ana_c, cod_ana_s,dt_cri,user_cri) values ( "
    sSql = sSql & n_req & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_agrup) & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_agrup) & ", "
    sSql = sSql & BL_TrataStringParaBD(Cod_Perfil) & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_ana_c) & ", "
    sSql = sSql & BL_TrataStringParaBD(cod_ana_s) & ", "
    If gSGBD = gOracle Then
        sSql = sSql & "SYSDATE, "
    ElseIf gSGBD = gPostGres Then
        sSql = sSql & " NOW(), "
    End If
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ") "
    total = BG_ExecutaQuery_ADO(sSql)
    If total <> 1 Then GoTo TrataErro
    
    sSql = "SELECT n_req from sl_marcacoes where n_req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(cod_agrup)
    sSql = sSql & " UNION SELECT n_req from sl_realiza where n_req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(cod_agrup)
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount = 0 Then
        sSql = "UPDATE sl_ana_Acrescentadas SET flg_eliminada = 1 WHERE n_req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(cod_agrup)
        sSql = sSql & " AND (flg_eliminada IS NULL or flg_eliminada = 0) "
        BG_ExecutaQuery_ADO sSql
    End If
    BL_RegistaAnaEliminadas = True
Exit Function
TrataErro:
    BL_RegistaAnaEliminadas = False
    BG_LogFile_Erros "Erro  BL_RegistaAnaEliminadas " & Err.Description & " " & sSql, "BL", "BL_RegistaAnaEliminadas"
    Exit Function
    Resume Next
    
End Function



' -------------------------------------------------------------------

' REGISTA PEDIDO NOVA AMOSTRA

' -------------------------------------------------------------------
Public Function BL_RegistaPedidoNovaAmostra(ByVal n_req As String, ByVal cod_tubo As String, _
                                            ByVal cod_motivo As String, ByVal descr_motivo As String) As Boolean
    
    Dim i As Integer
    Dim total As Integer
    Dim sSql As String
    
    BL_RegistaPedidoNovaAmostra = False
    If gSGBD = gOracle Then
        sSql = "INSERT INTO sl_pedido_nova_amostra(seq_pedido, n_req, cod_tubo, cod_motivo, descr_motivo, user_cri, dt_cri, hr_cri ) VALUES("
        sSql = sSql & "seq_pedido_nova_amostra.nextval, " & n_req & ", " & BL_TrataStringParaBD(cod_tubo) & ", "
        sSql = sSql & BL_TrataStringParaBD(cod_motivo) & ", " & BL_TrataStringParaBD(descr_motivo) & ", "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
        sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
    ElseIf gSGBD = gSqlServer Then
        sSql = "INSERT INTO sl_pedido_nova_amostra( n_req, cod_tubo, cod_motivo, descr_motivo, user_cri, dt_cri, hr_cri ) VALUES("
        sSql = sSql & n_req & ", " & BL_TrataStringParaBD(cod_tubo) & ", "
        sSql = sSql & BL_TrataStringParaBD(cod_motivo) & ", " & BL_TrataStringParaBD(descr_motivo) & ", "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
        sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
    End If
    total = BG_ExecutaQuery_ADO(sSql)
    If total <> 1 Then GoTo TrataErro
    If BL_HandleNull(gUtilEnvioMensagemPedNovaAmostra, "-1") <> -1 Then
        TB_EnviaMensagemPedidoNovaAmostra n_req, cod_tubo, descr_motivo
    End If
    BL_RegistaPedidoNovaAmostra = True
Exit Function
TrataErro:
    BL_RegistaPedidoNovaAmostra = False
    BG_LogFile_Erros "Erro  BL_RegistaPedidoNovaAmostra " & Err.Description & " " & sSql, "BL", "BL_RegistaPedidoNovaAmostra"
    Exit Function
    Resume Next
    
End Function

' -------------------------------------------------------------------

' PREENCHE ESTRUTURA DE ESTADOS DA REQUISI��O

' -------------------------------------------------------------------
Public Sub BL_PreencheEstrutEstadosReq()
    Dim totaEstadoReq As Integer
    On Error GoTo TrataErro
    Dim sSql  As String
    Dim rsEstados As New ADODB.recordset
    ReDim gEstrutEstadosReq(0)
    totaEstadoReq = 0
    
    sSql = "SELECT * FROM SL_TBF_ESTADO_REQ"
    rsEstados.CursorLocation = adUseServer
    rsEstados.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsEstados.Open sSql, gConexao
    If rsEstados.RecordCount > 0 Then
        While Not rsEstados.EOF
            totaEstadoReq = totaEstadoReq + 1
            ReDim Preserve gEstrutEstadosReq(totaEstadoReq)
             gEstrutEstadosReq(totaEstadoReq).codigo = BL_HandleNull(rsEstados!cod_estado, " ")
             gEstrutEstadosReq(totaEstadoReq).descricao = BL_HandleNull(rsEstados!descr_estado, "SEM DESCRI��O")
             gEstrutEstadosReq(totaEstadoReq).cor = BL_HandleNull(rsEstados!cor, vbWhite)
            rsEstados.MoveNext
        Wend
    Else
        ReDim gEstrutEstadosReq(13)
        gEstrutEstadosReq(1).codigo = gEstadoReqSemAnalises
        gEstrutEstadosReq(1).descricao = "Sem an�lises"
        
        gEstrutEstadosReq(2).codigo = gEstadoReqEsperaProduto
        gEstrutEstadosReq(2).descricao = "Espera de produtos"
        
        gEstrutEstadosReq(3).codigo = gEstadoReqEsperaResultados
        gEstrutEstadosReq(3).descricao = "Espera de resultados"
        
        gEstrutEstadosReq(4).codigo = gEstadoReqEsperaValidacao
        gEstrutEstadosReq(4).descricao = "Espera de valida��o m�dica"
        
        gEstrutEstadosReq(5).codigo = gEstadoReqCancelada
        gEstrutEstadosReq(5).descricao = "Cancelada"
        
        gEstrutEstadosReq(6).codigo = gEstadoReqValicacaoMedicaCompleta
        gEstrutEstadosReq(6).descricao = "Valida��o m�dica completa"
        
        gEstrutEstadosReq(7).codigo = gEstadoReqNaoPassarHistorico
        gEstrutEstadosReq(7).descricao = "N�o passar a hist�rico"
        
        gEstrutEstadosReq(8).codigo = gEstadoReqTodasImpressas
        gEstrutEstadosReq(8).descricao = "Todas impressas"
        
        gEstrutEstadosReq(9).codigo = gEstadoReqHistorico
        gEstrutEstadosReq(9).descricao = "Hist�rico"
        
        gEstrutEstadosReq(10).codigo = gEstadoReqResultadosParciais
        gEstrutEstadosReq(10).descricao = "Resultados parciais"
        
        gEstrutEstadosReq(11).codigo = gEstadoReqValidacaoMedicaParcial
        gEstrutEstadosReq(11).descricao = "Valida��o m�dica parcial"
        
        gEstrutEstadosReq(12).codigo = gEstadoReqImpressaoParcial
        gEstrutEstadosReq(12).descricao = "Impress�o parcial"
        
        gEstrutEstadosReq(13).codigo = gEstadoReqBloqueada
        gEstrutEstadosReq(13).descricao = "Bloqueada"

    End If
    rsEstados.Close
    Set rsEstados = Nothing
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  BL_PreencheEstrutEstadosReq " & Err.Description & " " & sSql, "BL", "BL_PreencheEstrutEstadosReq"
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------

' PREENCHE ESTRUTURA DE ESTADOS DA ANALISE

' -------------------------------------------------------------------
Public Sub BL_PreencheEstrutEstadosAna()
    Dim totaEstadoAna As Integer
    On Error GoTo TrataErro
    Dim sSql  As String
    Dim rsEstados As New ADODB.recordset
    ReDim gEstrutEstadosAna(0)
    totaEstadoAna = 0
    
    sSql = "SELECT * FROM SL_TBF_ESTADO_ANA "
    rsEstados.CursorLocation = adUseServer
    rsEstados.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsEstados.Open sSql, gConexao
    If rsEstados.RecordCount > 0 Then
        While Not rsEstados.EOF
            totaEstadoAna = totaEstadoAna + 1
            ReDim Preserve gEstrutEstadosAna(totaEstadoAna)
             gEstrutEstadosAna(totaEstadoAna).codigo = BL_HandleNull(rsEstados!cod_estado, " ")
             gEstrutEstadosAna(totaEstadoAna).descricao = BL_HandleNull(rsEstados!descr_estado, "SEM DESCRI��O")
             gEstrutEstadosAna(totaEstadoAna).cor = CLng(BL_HandleNull(rsEstados!cor, vbWhite))
            rsEstados.MoveNext
        Wend
    Else
        ReDim gEstrutEstadosAna(7)
        gEstrutEstadosAna(1).codigo = "-1"
        gEstrutEstadosAna(1).descricao = "Sem Resultados"
        
        gEstrutEstadosAna(2).codigo = "1"
        gEstrutEstadosAna(2).descricao = "Com Resultados"
        
        gEstrutEstadosAna(3).codigo = "2"
        gEstrutEstadosAna(3).descricao = "Bloqueada"
        
        gEstrutEstadosAna(4).codigo = "3"
        gEstrutEstadosAna(4).descricao = "Validada M�dicamente"
        
        gEstrutEstadosAna(5).codigo = "4"
        gEstrutEstadosAna(5).descricao = "Impressa"
        
        gEstrutEstadosAna(6).codigo = "5"
        gEstrutEstadosAna(6).descricao = "Validada T�cnicamente"
        
        gEstrutEstadosAna(7).codigo = "6"
        gEstrutEstadosAna(7).descricao = "Pendente"

    End If
    rsEstados.Close
    Set rsEstados = Nothing
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  BL_PreencheEstrutEstadosAna " & Err.Description & " " & sSql, "BL", "BL_PreencheEstrutEstadosAna"
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------

' VERIFICA SE NUMERO DA REQUISI��O EST� CORRECTO

' -------------------------------------------------------------------
Public Function BL_VerificaCampoRequis(Form As Form, campoReq As String, campoSala As String) As Boolean
    Dim flg_gera_auto As Integer
    Dim i As Integer
    Dim req As Long
    Dim iRes As Integer
    On Error GoTo TrataErro
    BL_VerificaCampoRequis = False
    
    If gNReqPreImpressa = 1 And campoReq = "" And campoSala = "" Then
        BG_Mensagem mediMsgBox, "N�mero de requisi��o obrigat�rio! ", vbInformation, "Aten��o"
        Exit Function
    ElseIf gNReqPreImpressa = 1 And campoSala <> "" Then
        flg_gera_auto = BL_HandleNull(BL_SelCodigo("sl_Cod_salas", "flg_gera_auto", "cod_sala", campoSala), 0)
        If flg_gera_auto = 1 And campoReq <> "" Then
            BG_Mensagem mediMsgBox, "N�mero de requisi��o gerado autom�ticamente! ", vbInformation, "Aten��o"
            Exit Function
        ElseIf flg_gera_auto = 1 And campoReq = "" Then
            i = 0
            req = -1
            While req = -1 And i <= 10
                req = BL_GeraNumero("N_REQUIS")
                i = i + 1
            Wend
            If req = -1 Then
                BG_Mensagem mediMsgBox, "Erro a gerar n�mero de requisi��o !", vbCritical + vbOKOnly, "ERRO"
                Exit Function
            End If
            campoReq = req
            
            If BL_ValidaNumReq(campoReq, 1, True) = False Then
                Exit Function
            Else
                Form.EcNumReq = campoReq
            End If
        ElseIf flg_gera_auto = 0 And campoReq = "" Then
            BG_Mensagem mediMsgBox, "N�mero de requisi��o obrigat�rio! ", vbInformation, "Aten��o"
            Exit Function
        ElseIf flg_gera_auto = 0 And campoReq <> "" Then
            If BL_ValidaNumReq(campoReq, 0, True) = False Then
                Exit Function
            End If
        End If
    ElseIf gNReqPreImpressa = 1 And campoSala = "" Then
        If campoReq = "" Then
            BG_Mensagem mediMsgBox, "N�mero de requisi��o obrigat�rio! ", vbInformation, "Aten��o"
            Exit Function
        ElseIf campoReq <> "" Then
            If BL_ValidaNumReq(campoReq, 0, True) = False Then
                Exit Function
            End If
        End If
    End If
    BL_VerificaCampoRequis = True
Exit Function
TrataErro:
    BL_VerificaCampoRequis = False
    BG_LogFile_Erros "Erro  BL_VerificaCampoRequis " & Err.Description, "BL", "BL_VerificaCampoRequis"
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' VERIFICA SE NUMERO DE REQUISICAO JA FOI GERADO PREVIAMENTE OU SE ESTA ATRIBUIDO OU SE CAI NUM INTERVALO DE ALGUMA SALA

' --------------------------------------------------------------------------------------------------------------------------------------
Public Function BL_ValidaNumReq(NumReq As String, flg_gera_auto As Integer, flg As Boolean) As Boolean
    Dim sSql As String
    Dim rsSala As New ADODB.recordset
    BL_ValidaNumReq = False
    On Error GoTo TrataErro

    If IsNumeric(NumReq) = False Then
        BG_Mensagem mediMsgBox, "N�mero inv�lido!", vbInformation, "Aten��o"
        Exit Function
    ElseIf BL_SelCodigo("sl_requis", "n_req", "n_req", NumReq) = NumReq Then
        BG_Mensagem mediMsgBox, "N�mero de requisi��o j� existente!", vbInformation, "Aten��o"
        Exit Function
    ElseIf BL_SelCodigo("SL_ETIQ_SALAS", "n_req", "n_req", NumReq) = NumReq And flg_gera_auto = 1 Then
        BG_Mensagem mediMsgBox, "N�mero de requisi��o j� atribuido!", vbInformation, "Aten��o"
        Exit Function
    ElseIf flg_gera_auto = 1 Then
        sSql = "SELECT * FROM sl_cod_salas WHERE " & NumReq & " BETWEEN min_impressao and (max_impressao-1) "
        rsSala.CursorLocation = adUseServer
        rsSala.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsSala.Open sSql, gConexao
        If rsSala.RecordCount > 0 Then
            If flg = True Then
                BG_Mensagem mediMsgBox, "N�mero de requisi��o dentro do intervalo da sala:" & BL_HandleNull(rsSala!descr_sala, "") & "!", vbInformation, "Aten��o"
            End If
            rsSala.Close
            Set rsSala = Nothing
            Exit Function
        End If
        rsSala.Close
        Set rsSala = Nothing
    End If
    BL_ValidaNumReq = True
Exit Function
TrataErro:
    BL_ValidaNumReq = False
    BG_LogFile_Erros "Erro  BL_ValidaNumReq " & Err.Description & " " & sSql, "BL", "BL_ValidaNumReq"
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' PROCURA PROXIMO NUMERO DE REQUISICAO DISPONIVEL

' --------------------------------------------------------------------------------------------------------------------------------------
Private Function BL_ProcuraProxNumDisp() As Long
    On Error GoTo TrataErro
    Dim i As Long
    Dim req As Long
    BL_ProcuraProxNumDisp = -1
    While 1 = 1
        req = -1
        req = BL_GeraNumero("N_REQUIS")
        If req = -1 Then
            BL_ProcuraProxNumDisp = -1
            Exit Function
        ElseIf BL_ValidaNumReq(CStr(req), 1, False) = True Then
            BL_ProcuraProxNumDisp = req
            Exit Function
        End If
        i = i + 1
    Wend
    BL_ProcuraProxNumDisp = -1
Exit Function
TrataErro:
    BL_ProcuraProxNumDisp = -1
    BG_LogFile_Erros "Erro  BL_ProcuraProxNumDisp " & Err.Description, "BL", "BL_ProcuraProxNumDisp"
    Exit Function
    Resume Next
End Function

Sub BL_TrataNomes(nome As String, ByRef nome_1 As String, ByRef nome_2 As String, _
        Optional ByRef nome_3 As String, Optional PosicaoQuebra As Variant)
    Dim pos As Integer
    Dim pos_aux As Integer
    Dim pos_ant As Integer
    Dim flag As Integer
    Dim PosQuebra As Integer
    
    If Not IsMissing(PosicaoQuebra) Then
        PosQuebra = CInt(PosicaoQuebra)
    Else
        PosQuebra = 26
    End If
    
    nome = nome & " "
    
    'se tiver em falta
    If nome_3 = "" Then
        flag = 1
    End If
    nome_3 = ""
    If Len(nome) <= PosQuebra Then
        nome_1 = Trim(nome)
        nome_2 = ""
    Else
        pos_aux = 1
        Do While pos_aux < Len(nome)
            pos = InStr(pos_aux + 1, nome, " ")
            If pos > PosQuebra Or pos = 0 Then
                If flag Then
                    nome_1 = Trim(Mid(nome, 1, pos_aux))
                    nome_2 = Trim(Mid(nome, pos_aux))
                    Exit Do
                Else
                    nome_1 = Trim(Mid(nome, 1, pos_aux))
                    pos_ant = pos_aux
                    Do While pos_aux <= Len(nome)
                        pos = InStr(pos_aux + 1, nome, " ")
                        If pos > (2 * PosQuebra) Or pos = 0 Then
                            nome_2 = Trim(Mid(nome, pos_ant, pos_aux - pos_ant + 1))
                            nome_3 = Trim(Mid(nome, pos_aux))
                            pos_aux = Len(nome)
                            Exit Do
                        Else
                            pos_aux = pos
                            pos = pos + 1
                        End If
                    Loop
                End If
            Else
                pos_aux = pos
                pos = pos + 1
            End If
        Loop
    End If
    
End Sub

Public Sub BL_AplExt(AppName As String, Optional Form2Open As String = "")
    Dim cmdlinpar As String
    
    Dim cmdlin As String
    Dim Tmp
    
    cmdlin = ""
    cmdlinpar = ""
    
    Select Case AppName
    Case "DIGITALIZADOR"
        cmdlin = gDirServidor & "\BIN\Digitalizador.exe"
        
        ' Comp�e par�metros para linha de comando
        cmdlinpar = "userid:" & gCodUtilizador & " username:" & BL_TrataStringParaBD(gNomeUtilizador)
    End Select
    
    If Len(cmdlin) = 0 Then Exit Sub
    
    cmdlin = cmdlin & IIf(Len(cmdlinpar) > 0, " " & cmdlinpar, "")
    
    On Error Resume Next
    
    Tmp = Shell(cmdlin, vbNormalFocus)
End Sub


' --------------------------------------------------------------------------------------------------------------------------------------

' RETORNA ANALISES MARCADAS ASSOCIADAS AO TUBO SECUNDARIO

' --------------------------------------------------------------------------------------------------------------------------------------
Public Function BL_RetornaAbrAnaTubo(n_req As String, cod_tubo As String, tubo_prim As Boolean) As String
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsTuboSec As New ADODB.recordset
    BL_RetornaAbrAnaTubo = ""
    sSql = "SELECT distinct slv_analises.abr_ana FROM sl_marcacoes, slv_analises WHERE n_req = " & n_req
    If tubo_prim = True Then
        sSql = sSql & " AND slv_analises.cod_ana = Sl_marcacoes.cod_Agrup "
        sSql = sSql & " AND slv_analises.cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
    Else
        sSql = sSql & " AND (slv_analises.cod_ana = Sl_marcacoes.cod_ana_S OR slv_analises.cod_ana = sl_marcacoes.cod_ana_c or slv_Analises.cod_ana = sl_marcacoes.cod_perfil )"
        sSql = sSql & " AND slv_analises.cod_tubo_sec = " & BL_TrataStringParaBD(cod_tubo)
    End If
    rsTuboSec.CursorLocation = adUseServer
    rsTuboSec.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTuboSec.Open sSql, gConexao
    If rsTuboSec.RecordCount > 0 Then
        While Not rsTuboSec.EOF
            BL_RetornaAbrAnaTubo = BL_RetornaAbrAnaTubo & BL_HandleNull(rsTuboSec!abr_ana, "---") & ";"
            rsTuboSec.MoveNext
        Wend
    End If
    rsTuboSec.Close
    Set rsTuboSec = Nothing
Exit Function
TrataErro:
    BL_RetornaAbrAnaTubo = ""
    BG_LogFile_Erros "Erro  BL_RetornaAbrAnaTubo: " & sSql & " " & Err.Description, "BL", "BL_RetornaAbrAnaTubo"
    Exit Function
    Resume Next
End Function




' --------------------------------------------------------------------------------------------------------------------------------------

' RETORNA ORDENS DAS ANALISES

' --------------------------------------------------------------------------------------------------------------------------------------
Public Sub BL_RetornaOrdens(ByVal Cod_Perfil As String, ByVal cod_ana_c As String, ByVal cod_ana_s As String, _
                                 ByRef ordem As Integer, ByRef ord_compl As Integer, ByRef ord_perf As Integer)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    If Cod_Perfil = "0" And cod_ana_c = "0" Then
        'ORDEM DAS SIMPLES MARCADAS SOZINHAS
        ord_perf = 0
        ord_compl = 0
        sSql = "SELECT ordem FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount >= 1 Then
            ordem = BL_HandleNull(rsAna!ordem, -1)
        Else
            ordem = -1
        End If
        rsAna.Close
                
    ElseIf Cod_Perfil <> "0" Then
        'ORDEM DOS PERFIS
        sSql = "SELECT ordem FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(Cod_Perfil)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount >= 1 Then
            ordem = BL_HandleNull(rsAna!ordem, -1)
        Else
            ordem = -1
        End If
        rsAna.Close
        
        If cod_ana_c = gGHOSTMEMBER_C Then
            'SE COMPLEXA = c999999 E SIMPLES = S99999
            ord_compl = -1
            ord_perf = -1
        ElseIf cod_ana_c = "0" Then
            'SE NAO TIVER COMPLEXA MAS TIVER SIMPLES
            ord_compl = -1
            sSql = "SELECT ordem FROM SL_ANA_PERFIS WHERE cod_perfis = " & BL_TrataStringParaBD(Cod_Perfil)
            sSql = sSql & " AND cod_analise = " & BL_TrataStringParaBD(cod_ana_s)
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount >= 1 Then
                ord_perf = BL_HandleNull(rsAna!ordem, -1)
            Else
                ord_perf = -1
            End If
            rsAna.Close
        ElseIf cod_ana_c <> "0" Then
            ' SE TIVER COMPLEXA
            sSql = "SELECT ordem FROM SL_ANA_PERFIS WHERE cod_perfis = " & BL_TrataStringParaBD(Cod_Perfil)
            sSql = sSql & " AND cod_analise = " & BL_TrataStringParaBD(cod_ana_c)
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount >= 1 Then
                ord_perf = BL_HandleNull(rsAna!ordem, -1)
            Else
                ord_perf = -1
            End If
            rsAna.Close
            
            If cod_ana_s = gGHOSTMEMBER_S Then
                'SE SIMPLES = S99999
                ord_compl = -1
            Else
                'SE COMPLEXA TIVER SIMPLES
                sSql = "SELECT ordem FROM SL_MEMBRO WHERE COD_ANA_C = " & BL_TrataStringParaBD(cod_ana_c)
                sSql = sSql & " AND cod_membro = " & BL_TrataStringParaBD(cod_ana_s)
                rsAna.CursorLocation = adUseServer
                rsAna.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsAna.Open sSql, gConexao
                If rsAna.RecordCount >= 1 Then
                    ord_compl = BL_HandleNull(rsAna!ordem, -1)
                Else
                    ord_compl = -1
                End If
                rsAna.Close
            End If
        End If
    ElseIf Cod_Perfil = "0" And cod_ana_c <> "0" Then
        ord_perf = 0
        sSql = "SELECT ordem FROM sl_ana_c WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount >= 1 Then
            ordem = BL_HandleNull(rsAna!ordem, -1)
        Else
            ordem = -1
        End If
        rsAna.Close
        If cod_ana_s = gGHOSTMEMBER_S Then
            'SE SIMPLES = S99999
            ord_compl = -1
        Else
            'SE COMPLEXA TIVER SIMPLES
            sSql = "SELECT ordem FROM SL_MEMBRO WHERE COD_ANA_C = " & BL_TrataStringParaBD(cod_ana_c)
            sSql = sSql & " AND cod_membro = " & BL_TrataStringParaBD(cod_ana_s)
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount >= 1 Then
                ord_compl = BL_HandleNull(rsAna!ordem, -1)
            Else
                ord_compl = -1
            End If
            rsAna.Close
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  BL_RetornaOrdens: " & sSql & " " & Err.Description, "BL", "BL_RetornaOrdens"
    Exit Sub
    Resume Next
End Sub





Public Sub BL_GerirMenusResultados(descr_ana As String, modoLeitura As Boolean)
    Dim RsAnaS As New ADODB.recordset
    Dim sSql As String
    On Error GoTo TrataErro
    MDIFormInicio.IDM_RESU_New(0).Visible = True
    MDIFormInicio.IDM_RESU_New(1).Visible = False
    MDIFormInicio.IDM_RESU_New(2).Visible = False
    MDIFormInicio.IDM_RESU_New(3).Visible = False
    MDIFormInicio.IDM_RESU_New(4).Visible = False
    MDIFormInicio.IDM_RESU_New(5).Visible = False
    MDIFormInicio.IDM_RESU_New(6).Visible = False
    MDIFormInicio.IDM_RESU_New(7).Visible = False
    MDIFormInicio.IDM_RESU_New(8).Visible = False
    MDIFormInicio.IDM_RESU_New(9).Visible = True
    MDIFormInicio.IDM_RESU_New(9).Visible = False
    MDIFormInicio.IDM_RESU_New(10).Visible = False
    MDIFormInicio.IDM_RESU_New(11).Visible = False
    MDIFormInicio.IDM_RESU_New(12).Visible = False
    MDIFormInicio.IDM_RESU_New(13).Visible = False
    MDIFormInicio.IDM_RESU_New(14).Visible = False
    DoEvents
    
    If modoLeitura = True Then
        MDIFormInicio.IDM_RESU_New(0).Visible = True
        MDIFormInicio.IDM_RESU_New(1).Visible = False
        MDIFormInicio.IDM_RESU_New(2).Visible = True
            MDIFormInicio.IDM_RESU_NEW_REP(0).Visible = False
            MDIFormInicio.IDM_RESU_NEW_REP(1).Visible = False
            MDIFormInicio.IDM_RESU_NEW_REP(2).Visible = True
        MDIFormInicio.IDM_RESU_New(3).Visible = True
            MDIFormInicio.IDM_RESU_NEW_OBS(0).Visible = True
            MDIFormInicio.IDM_RESU_NEW_OBS(1).Visible = False
            MDIFormInicio.IDM_RESU_NEW_OBS(2).Visible = False
        MDIFormInicio.IDM_RESU_New(4).Visible = True
        MDIFormInicio.IDM_RESU_New(5).Visible = False
        MDIFormInicio.IDM_RESU_New(6).Visible = False
        MDIFormInicio.IDM_RESU_New(7).Visible = True
        MDIFormInicio.IDM_RESU_New(8).Visible = True
        MDIFormInicio.IDM_RESU_New(9).Visible = False
        MDIFormInicio.IDM_RESU_New(9).Visible = False
        MDIFormInicio.IDM_RESU_New(10).Visible = False
        MDIFormInicio.IDM_RESU_New(11).Visible = False
        MDIFormInicio.IDM_RESU_New(12).Visible = False
        MDIFormInicio.IDM_RESU_New(13).Visible = False
        MDIFormInicio.IDM_RESU_New(14).Visible = False
    Else
            
        If gTipoMenuResultados = gTipoMenuGeral Then
            MDIFormInicio.IDM_RESU_New(0).caption = descr_ana
            MDIFormInicio.IDM_RESU_New(0).Visible = True
            MDIFormInicio.IDM_RESU_New(1).Visible = True
            MDIFormInicio.IDM_RESU_New(2).Visible = True
            MDIFormInicio.IDM_RESU_New(3).Visible = True
            MDIFormInicio.IDM_RESU_New(4).Visible = True
            MDIFormInicio.IDM_RESU_New(5).Visible = True
            MDIFormInicio.IDM_RESU_New(6).Visible = True
            MDIFormInicio.IDM_RESU_New(7).Visible = True
            MDIFormInicio.IDM_RESU_New(8).Visible = True
            MDIFormInicio.IDM_RESU_New(9).Visible = True
            If gCodAnaSimples1 <> "-1" And gCodAnaSimples1 <> "" Then
                Set RsAnaS = New ADODB.recordset
                sSql = "SELECT abr_ana_s, descr_ana_s FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(gCodAnaSimples1)
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                RsAnaS.Open sSql, gConexao, adOpenStatic, adLockReadOnly
                If Not RsAnaS.EOF Then
                    MDIFormInicio.IDM_RESU_New(10).Visible = True
                    MDIFormInicio.IDM_RESU_New(10).caption = "Adicionar " & BL_HandleNull(RsAnaS!abr_ana_s, RsAnaS!descr_ana_s)
                End If
                RsAnaS.Close
            End If
            
            If gCodAnaSimples2 <> "-1" And gCodAnaSimples2 <> "" Then
                Set RsAnaS = New ADODB.recordset
                sSql = "SELECT abr_ana_s, descr_ana_s FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(gCodAnaSimples2)
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                RsAnaS.Open sSql, gConexao, adOpenStatic, adLockReadOnly
                If Not RsAnaS.EOF Then
                    MDIFormInicio.IDM_RESU_New(11).Visible = True
                    MDIFormInicio.IDM_RESU_New(11).caption = "Adicionar " & BL_HandleNull(RsAnaS!abr_ana_s, RsAnaS!descr_ana_s)
                End If
                RsAnaS.Close
            End If
            Set RsAnaS = Nothing
            MDIFormInicio.IDM_RESU_New(12).Visible = False
            MDIFormInicio.IDM_RESU_New(13).Visible = True
            MDIFormInicio.IDM_RESU_New(14).Visible = False
        ElseIf gTipoMenuResultados = gTipoMenuAnexo Then
            MDIFormInicio.IDM_RESU_New(12).Visible = True
        ElseIf gTipoMenuResultados = gTipoMenuVigEpid Then
                    'edgar.parada CHSJ-4387
            MDIFormInicio.IDM_RESU_New(0).caption = descr_ana
            '
            MDIFormInicio.IDM_RESU_New(14).Visible = True
        End If
    End If
Exit Sub
TrataErro:
    
    Resume Next
End Sub



Public Sub BL_ShellExecute(caminho As String, Opcao As String)
    On Error GoTo MyError
    Dim lRet As Long
    Dim WinWnd As Long
    Dim i As Integer
    If Opcao = "Open" Then
        lRet = ShellExecute(0, "Open", caminho, "", "C:\", SW_SHOWNORMAL)
    ElseIf Opcao = "Print" Then
        lRet = ShellExecute(0, "PrintTo", caminho, "", "C:\", SW_SHOWNORMAL)
        'Sleep (1000)
    End If
    If Opcao = "Print" Then
        i = 0
        WinWnd = 0
        While WinWnd = 0 And i < 9000
            WinWnd = FindWindow(vbNullString, "Adobe Reader")
        Wend
        If WinWnd > 0 Then
            PostMessage WinWnd, WM_CLOSE, 0&, 0&
        End If
        If lRet <= 32 Then
            Select Case lRet
                Case 0
                    'MsgBox "The operating system is out of memory or resources."
                Case ERROR_FILE_NOT_FOUND
                    'MsgBox "The specified file was not found."
                Case ERROR_PATH_NOT_FOUND
                    'MsgBox "The specified path was not found."
                Case ERROR_BAD_FORMAT
                    'MsgBox "The .EXE file is invalid (non-Win32 .EXE or error in .EXE image)."
                Case SE_ERR_ACCESSDENIED
                    'MsgBox "The operating system denied access to the specified file."
                Case SE_ERR_ASSOCINCOMPLETE
                    'MsgBox "The filename association is incomplete or invalid."
                Case SE_ERR_DDEBUSY
                    'MsgBox "The DDE transaction could not be completed because other DDE transactions were being processed."
                Case SE_ERR_DDEFAIL
                    'MsgBox "The DDE transaction failed."
                Case SE_ERR_DDETIMEOUT
                    'MsgBox "The DDE transaction could not be completed because the request timed out."
                Case SE_ERR_DLLNOTFOUND
                    'MsgBox "The specified dynamic-link library was not found."
                Case SE_ERR_FNF
                    'MsgBox "The specified file was not found."
                Case SE_ERR_NOASSOC
                    'MsgBox "There is no application associated with the given filename extension."
                Case SE_ERR_OOM
                    'MsgBox "There was not enough memory to complete the operation."
                Case SE_ERR_PNF
                    'MsgBox "The specified path was not found."
                Case SE_ERR_SHARE
                    'MsgBox "A sharing violation occurred."
            End Select
        End If
    End If
    Exit Sub
MyError:
    MsgBox Err.Number & " - " & Err.Description
End Sub


' --------------------------------------------------------------------------------------------------------------------------------------

' RETORNA ESTADO DA SMS

' --------------------------------------------------------------------------------------------------------------------------------------
Public Function BL_RetornaEstadoSMS(n_req As String, flg_sms As Integer, flg_sms_enviada As Integer, campo As Label) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsSMS As New ADODB.recordset
    If flg_sms = 0 Then
        campo.caption = ""
        Exit Function
    End If
    If flg_sms_enviada = 0 Then
        campo.caption = "SMS N�O ENVIADA"
        Exit Function
    End If
    campo.caption = "SMS ENVIADA"
    sSql = "SELECT * FROM SL_SMS_ENVIO WHERE n_Req = " & n_req
    rsSMS.CursorLocation = adUseServer
    rsSMS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSMS.Open sSql, gConexao
    If rsSMS.RecordCount >= 1 Then
        campo.caption = campo.caption & "(" & BL_HandleNull(rsSMS!Estado, "") & ")"
    End If
    rsSMS.Close
    Set rsSMS = Nothing
    BL_RetornaEstadoSMS = True
Exit Function
TrataErro:
    BL_RetornaEstadoSMS = False
    BG_LogFile_Erros "Erro  BL_RetornaEstadoSMS: " & sSql & " " & Err.Description, "BL", "BL_RetornaEstadoSMS"
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' INSERE REGISTO NA TABELA SL_ANA_ACRESCENTADAS

' --------------------------------------------------------------------------------------------------------------------------------------
Public Function BL_RegistaAnaAcrescentadas(seq_utente As String, n_req As String, cod_ana_s As String, cod_ana_c As String, Cod_Perfil As String, _
                                           cod_agrup As String, data As String, hora As String, t_urg As Integer, t_sit As Integer) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim sql As String
    Dim iReg As Long
    Dim rsAna As New ADODB.recordset
    Dim codLocal As String
    Dim codLocalPretend As String
    codLocal = ""
    
    If t_sit > mediComboValorNull Then
        codLocalPretend = BL_HandleNull(BL_SelCodigo("SL_TBF_T_sit", "cod_local_ana", "cod_t_sit", t_sit), mediComboValorNull)
        If codLocalPretend = mediComboValorNull Or codLocalPretend = "" Then
            codLocalPretend = BL_HandleNull(BL_SelCodigo("SL_TBF_T_URG", "cod_local_ana", "cod_t_urg", t_urg), gCodLocal)
        End If
    Else
        codLocalPretend = BL_HandleNull(BL_SelCodigo("SL_TBF_T_URG", "cod_local_ana", "cod_t_urg", t_urg), gCodLocal)
    End If
    
    If cod_ana_s = "" Then
        codLocal = BL_DevolveLocalPretendidoExecAna(cod_agrup, codLocalPretend)
    Else
        codLocal = BL_DevolveLocalJaMarcado(n_req, cod_agrup, codLocalPretend)
    End If
    If codLocal = "" Then
        codLocal = "NULL"
    End If
    
     'NELSONPSILVA 30.04.2019 Glintt-HS-21232
    'ssql = "SELECT n_req from sl_ana_eliminadas where n_req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(cod_agrup)
    sSql = "SELECT n_req from sl_marcacoes where n_req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(cod_agrup)
    sSql = sSql & " UNION SELECT n_req from sl_realiza where n_req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(cod_agrup)
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        If cod_ana_c = "" And cod_ana_s = "" And Cod_Perfil = "" Then
            sSql = "UPDATE sl_ana_acrescentadas set flg_eliminada = 1 WHERE n_req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(cod_agrup)
            sSql = sSql & " AND cod_perfis IS NULL AND cod_ana_c is null and cod_ana_s is null "
            BG_ExecutaQuery_ADO sSql
        End If
         
        sSql = "INSERT INTO sl_ana_ACRESCENTADAS(n_req,cod_agrup, cod_perfis, cod_ana_c, cod_ana_s ,dt_cri,user_cri, hr_cri, cod_local) values ( "
        sSql = sSql & n_req & "," & BL_TrataStringParaBD(cod_agrup) & "," & BL_TrataStringParaBD(Cod_Perfil) & ", "
        sSql = sSql & BL_TrataStringParaBD(cod_ana_c) & ", " & BL_TrataStringParaBD(cod_ana_s) & ", "
        sSql = sSql & BL_TrataDataParaBD(data) & "," & gCodUtilizador & ", " & BL_TrataStringParaBD(hora) & "," & codLocal & ")"
         
        iReg = BG_ExecutaQuery_ADO(sSql)
    Else
        'NELSONPSILVA 30.12.2019 CHUC-12912 (update -> insert)
        If cod_ana_c = "" And cod_ana_s = "" And Cod_Perfil = "" Then
           sSql = "UPDATE sl_ana_acrescentadas set flg_eliminada = 1 WHERE n_req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(cod_agrup)
           sSql = sSql & " AND cod_perfis IS NULL AND cod_ana_c is null and cod_ana_s is null "
           BG_ExecutaQuery_ADO sSql
        End If
        '
        sSql = "INSERT INTO sl_ana_ACRESCENTADAS(n_req,cod_agrup, cod_perfis, cod_ana_c, cod_ana_s ,dt_cri,user_cri, hr_cri, cod_local) values ( "
        sSql = sSql & n_req & "," & BL_TrataStringParaBD(cod_agrup) & "," & BL_TrataStringParaBD(Cod_Perfil) & ", "
        sSql = sSql & BL_TrataStringParaBD(cod_ana_c) & ", " & BL_TrataStringParaBD(cod_ana_s) & ", "
        sSql = sSql & BL_TrataDataParaBD(data) & "," & gCodUtilizador & ", " & BL_TrataStringParaBD(hora) & "," & codLocal & ")"
         
        iReg = BG_ExecutaQuery_ADO(sSql)
    End If
    '

    If iReg <> 1 Then
        BL_RegistaAnaAcrescentadas = False
    Else
        BL_RegistaAnaAcrescentadas = True
    End If
    
    If DIA_RegistaDiario(seq_utente, n_req, cod_agrup, False) = False Then
        
    End If
    
Exit Function
TrataErro:
    BL_RegistaAnaAcrescentadas = False
    BG_LogFile_Erros "Erro  BL_RegistaAnaAcrescentadas: " & sSql & " " & Err.Description, "BL", "BL_RegistaAnaAcrescentadas"
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' VERIFICA SE ANALISE PODE SER MARCADA PARA O LOCAL PRETENDIDO
' Altera��o FG-2013-07-25
' --------------------------------------------------------------------------------------------------------------------------------------
Public Function BL_DevolveLocalPretendidoExecAna(codAgrup As String, cod_local_pretend As String) As String
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    ' N�O TEMOS INSTITUICOES PRIVADAS COM MULTILOCAL
'    If gTipoInstituicao = gTipoInstituicaoPrivada Then
'        BL_DevolveLocalPretendidoExecAna = cod_local_pretend
'        Exit Function
'    End If
    
    sSql = "SELECT * from sl_ana_locais_exec WHERE cod_ana = " & BL_TrataStringParaBD(codAgrup) & " AND cod_local = " & cod_local_pretend
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount >= 1 Then
        BL_DevolveLocalPretendidoExecAna = cod_local_pretend
    Else
        rsAna.Close
        Set rsAna = Nothing
        sSql = "select cod_local_defeito FROM sl_ana_locais WHERE cod_ana = " & BL_TrataStringParaBD(codAgrup) & " AND cod_local = " & cod_local_pretend
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount >= 1 Then
            If BL_HandleNull(rsAna!cod_local_defeito, "") <> "" Then
                BL_DevolveLocalPretendidoExecAna = BL_HandleNull(rsAna!cod_local_defeito, "")
                Exit Function
            End If
        End If
        rsAna.Close
        Set rsAna = Nothing
        
        
        sSql = "SELECT * from sl_ana_locais_exec WHERE cod_ana = " & BL_TrataStringParaBD(codAgrup) & " AND cod_local = " & gCodLocal
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount >= 1 Then
            BL_DevolveLocalPretendidoExecAna = gCodLocal
        Else
            rsAna.Close
            Set rsAna = Nothing
            sSql = "SELECT * from sl_ana_locais_exec WHERE cod_ana = " & BL_TrataStringParaBD(codAgrup)
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount >= 1 Then
                BL_DevolveLocalPretendidoExecAna = BL_HandleNull(rsAna!cod_local, "")
            Else
                BL_DevolveLocalPretendidoExecAna = ""
            End If
        End If
    End If
    rsAna.Close
    Set rsAna = Nothing
        
Exit Function
TrataErro:
    BL_DevolveLocalPretendidoExecAna = -1
    BG_LogFile_Erros "Erro  BL_DevolveLocalPretendidoExecAna: " & sSql & " " & Err.Description, "BL", "BL_DevolveLocalPretendidoExecAna"
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' VERIFICA SE ANALISE PODE SER MARCADA PARA O LOCAL PRETENDIDO

' --------------------------------------------------------------------------------------------------------------------------------------
Private Function BL_DevolveLocalJaMarcado(n_req As String, codAgrup As String, cod_local_pretend As String) As String
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    ' N�O TEMOS INSTITUICOES PRIVADAS COM MULTILOCAL
    If gTipoInstituicao = gTipoInstituicaoPrivada Then
        BL_DevolveLocalJaMarcado = gCodLocal
        Exit Function
    End If
    
    sSql = "SELECT * from sl_ana_acrescentadas WHERE n_req = " & n_req & " AND  cod_ana = " & BL_TrataStringParaBD(codAgrup)
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount >= 1 Then
        BL_DevolveLocalJaMarcado = BL_HandleNull(rsAna!cod_local, "")
    Else
        BL_DevolveLocalJaMarcado = BL_DevolveLocalPretendidoExecAna(codAgrup, cod_local_pretend)
    End If
    rsAna.Close
    Set rsAna = Nothing
        
Exit Function
TrataErro:
    BL_DevolveLocalJaMarcado = -1
    BG_LogFile_Erros "Erro  BL_DevolveLocalJaMarcado: " & sSql & " " & Err.Description, "BL", "BL_DevolveLocalJaMarcado"
    Exit Function
    Resume Next
End Function


' --------------------------------------------------------------------------------------------------------------------------------------

' VERIFICA SE REQUISICAO FOI ASSINADA NA TOTALIDADE OU PARCIALMENTE

' --------------------------------------------------------------------------------------------------------------------------------------
Public Function BL_RetornaEstadoAssinatura(n_req As String) As Integer
    
    Dim sSql As String
    Dim rsRequisicoes As ADODB.recordset
    
    On Error GoTo TrataErro
    Set rsRequisicoes = New ADODB.recordset
    If gAssinaturaActivo <> mediSim Then
        BL_RetornaEstadoAssinatura = mediComboValorNull
        Exit Function
    End If
    sSql = "select * from sl_req_assinatura where n_req = " & n_req
    rsRequisicoes.CursorLocation = adUseServer
    rsRequisicoes.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsRequisicoes.Open sSql, gConexao
    If rsRequisicoes.RecordCount >= 1 Then
        BL_RetornaEstadoAssinatura = 1
    Else
        rsRequisicoes.Close
        sSql = "SELECT * FROM sl_realiza WHERE n_req = " & n_req & " AND flg_assinado = 1"
        rsRequisicoes.CursorLocation = adUseServer
        rsRequisicoes.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsRequisicoes.Open sSql, gConexao
        If rsRequisicoes.RecordCount >= 1 Then
            BL_RetornaEstadoAssinatura = 2
        Else
            BL_RetornaEstadoAssinatura = 0
        End If
    End If
        rsRequisicoes.Close
        Set rsRequisicoes = Nothing
    Exit Function
    
TrataErro:
    BL_RetornaEstadoAssinatura = -1
    BG_LogFile_Erros "Erro  BL_RetornaEstadoAssinatura: " & sSql & " " & Err.Description, "BL", "BL_RetornaEstadoAssinatura"
    Exit Function
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' INSERE LINHA NA TABELA SL_ENVIO_RESULTADOS E ENVIA PARA HISTORICO SE FOR CASO DISSO.

' --------------------------------------------------------------------------------------------------------------------------------------
Public Function BL_EnvioResultadosExt(seq_utente As Long, n_req As Long, seq_realiza As Long, Cod_Perfil As String, _
                                            cod_ana_c As String, cod_ana_s As String, n_prescricao As Long, resultado As String, unidade As String, _
                                            valores_referencia As String, dentro_normalidade As String, data_resultado As String, data_registo As String, _
                                            operacao As String, Estado As String, erro As String) As Boolean
    Dim sSql As String
    Dim registos As Integer
    Dim RsProven As New ADODB.recordset
    Dim flg_executa As Boolean
    
    On Error GoTo TrataErro
    BL_EnvioResultadosExt = False
    
    sSql = "SELECT flg_inibe_envio_ext FROM sl_proven, sl_requis WHERE sl_requis.cod_proven = sl_proven.cod_proven AND n_req = " & n_req
    RsProven.CursorLocation = adUseServer
    RsProven.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsProven.Open sSql, gConexao
    If RsProven.RecordCount >= 1 Then
        If BL_HandleNull(RsProven!flg_inibe_envio_ext, 0) = 0 Then
            flg_executa = True
        Else
            flg_executa = False
        End If
    Else
        flg_executa = False
    End If
    RsProven.Close
    Set RsProven = Nothing
    
    If flg_executa = True Then
        If BL_EnvioResultadosExtHist(seq_realiza) = False Then
            GoTo TrataErro
        End If
        
        sSql = "INSERT INTO Sl_envio_resultados(seq_envio,seq_utente, n_req, seq_Realiza, cod_perfil, "
        sSql = sSql & " cod_ana_c, cod_Ana_S,flg_processado, user_cri, dt_cri, hr_cri, n_prescricao, resultado, unidade, valores_referencia, dentro_normalidade, "
        sSql = sSql & " data_resultado, data_registo, operacao, estado, erro) VALUES( "
        sSql = sSql & " seq_envio.nextval, "
        sSql = sSql & seq_utente & ","
        sSql = sSql & n_req & ","
        sSql = sSql & seq_realiza & ","
        sSql = sSql & BL_TrataStringParaBD(Cod_Perfil) & ","
        sSql = sSql & BL_TrataStringParaBD(cod_ana_c) & ","
        sSql = sSql & BL_TrataStringParaBD(cod_ana_s) & ","
        sSql = sSql & "0,"
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ","
        sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ","
        sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ","
        sSql = sSql & n_prescricao & ","
        sSql = sSql & BL_TrataStringParaBD(resultado) & ","
        sSql = sSql & BL_TrataStringParaBD(unidade) & ","
        sSql = sSql & BL_TrataStringParaBD(valores_referencia) & ","
        sSql = sSql & BL_TrataStringParaBD(dentro_normalidade) & ","
        sSql = sSql & BL_TrataDataParaBD(data_resultado) & ","
        sSql = sSql & BL_TrataStringParaBD(data_registo) & ","
        sSql = sSql & BL_TrataStringParaBD(operacao) & ","
        sSql = sSql & BL_TrataStringParaBD(Estado) & ","
        sSql = sSql & BL_TrataStringParaBD(erro) & ")"
        registos = BG_ExecutaQuery_ADO(sSql)
        If registos <> 1 Then
            GoTo TrataErro
        End If
    End If
    BL_EnvioResultadosExt = True
Exit Function
TrataErro:
    BL_EnvioResultadosExt = False
    BG_LogFile_Erros "BL_EnvioResultadosExt: " & Err.Description & " " & sSql, "BL", "BL_EnvioResultadosExt", True
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' ENVIA PARA TABELA DE HISTORICO

' --------------------------------------------------------------------------------------------------------------------------------------

Public Function BL_EnvioResultadosExtHist(seq_realiza As Long) As Boolean
    Dim sSql As String
    Dim rsEnvio As New ADODB.recordset
    Dim registos As Integer
    On Error GoTo TrataErro
    BL_EnvioResultadosExtHist = False
    
    sSql = "SELECT *FROM sl_envio_resultados WHERE seq_realiza = " & seq_realiza
    rsEnvio.CursorLocation = adUseServer
    rsEnvio.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsEnvio.Open sSql, gConexao
    If rsEnvio.RecordCount = 1 Then
        sSql = "INSERT INTO sl_envio_resultados_h(seq_envio,seq_utente, n_req, seq_Realiza, cod_perfil, "
        sSql = sSql & " cod_ana_c, cod_Ana_S,flg_processado, user_cri, dt_cri, hr_cri, user_act, dt_act, hr_act) VALUES( "
        sSql = sSql & BL_HandleNull(rsEnvio!seq_envio, mediComboValorNull) & ","
        sSql = sSql & BL_HandleNull(rsEnvio!seq_utente, mediComboValorNull) & ","
        sSql = sSql & BL_HandleNull(rsEnvio!n_req, mediComboValorNull) & ","
        sSql = sSql & BL_HandleNull(rsEnvio!seq_realiza, mediComboValorNull) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsEnvio!Cod_Perfil, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsEnvio!cod_ana_c, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsEnvio!cod_ana_s, "")) & ","
        sSql = sSql & BL_HandleNull(rsEnvio!flg_processado, 0) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsEnvio!user_cri, "")) & ","
        sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsEnvio!dt_cri, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsEnvio!hr_cri, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsEnvio!user_act, "")) & ","
        sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsEnvio!dt_act, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsEnvio!hr_act, "")) & ")"
        registos = BG_ExecutaQuery_ADO(sSql)
        If registos <> 1 Then
            GoTo TrataErro
        End If
        
        sSql = "DELETE FROM sl_envio_resultados WHERE seq_envio = " & BL_HandleNull(rsEnvio!seq_envio, mediComboValorNull)
        registos = BG_ExecutaQuery_ADO(sSql)
        If registos <> 1 Then
            GoTo TrataErro
        End If
    End If
    rsEnvio.Close
    Set rsEnvio = Nothing
    BL_EnvioResultadosExtHist = True
Exit Function
TrataErro:
    BL_EnvioResultadosExtHist = False
    BG_LogFile_Erros "BL_EnvioResultadosExtHist: " & Err.Description & " " & sSql, "BL", "BL_EnvioResultadosExtHist", True
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' BLOQUEIA REQUISICAO

' --------------------------------------------------------------------------------------------------------------------------------------
Public Function BL_BloqueiaReq(n_req As String, Motivo As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iReg As Integer
    BL_BloqueiaReq = False
    BG_BeginTransaction
    
    sSql = "INSERT INTO sl_req_bloq (seq_bloq,n_req,user_bloq, dt_bloq,hr_bloq, motivo_bloq) VALUES( seq_bloq.nextval, " & n_req & ","
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & ","
    sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & "," & BL_TrataStringParaBD(Motivo) & ")"
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg <> 1 Then
        GoTo TrataErro
    End If
    If BL_BloqueiaResultados(n_req) = False Then
        GoTo TrataErro
    End If
    BL_MudaEstadoReq CLng(n_req)
    BG_CommitTransaction
    BL_BloqueiaReq = True
Exit Function
TrataErro:
    BG_RollbackTransaction
    BL_BloqueiaReq = False
    BG_LogFile_Erros "BL_BloqueiaReq: " & Err.Description & " " & sSql, "BL", "BL_BloqueiaReq", True
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' DESBLOQUEIA REQUISICAO

' --------------------------------------------------------------------------------------------------------------------------------------
Public Function BL_DesbloqueiaReq(n_req As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iReg As Integer
    Dim rsBl As New ADODB.recordset
    BL_DesbloqueiaReq = False
    BG_BeginTransaction
    
    sSql = "SELECT * FROM sl_req_bloq WHERE n_req = " & n_req
    rsBl.CursorLocation = adUseServer
    rsBl.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsBl.Open sSql, gConexao
    If rsBl.RecordCount = 1 Then
        
        sSql = "INSERT INTO sl_req_bloq_h (seq_bloq,n_req,user_bloq, dt_bloq,hr_bloq, motivo_bloq, user_desbloq, dt_desbloq, hr_desbloq) "
        sSql = sSql & " VALUES( " & rsBl!seq_bloq & ", " & n_req & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsBl!user_bloq, "")) & "," & BL_TrataDataParaBD(BL_HandleNull(rsBl!dt_bloq, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsBl!hr_bloq, "")) & "," & BL_TrataStringParaBD(BL_HandleNull(rsBl!motivo_bloq, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & ","
        sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg <> 1 Then
            GoTo TrataErro
        End If
        
        sSql = "DELETE FROM Sl_req_bloq WHERE seq_bloq = " & rsBl!seq_bloq
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg <> 1 Then
            GoTo TrataErro
        End If
        If BL_DesBloqueiaResultados(n_req) = False Then
            GoTo TrataErro
        End If
    End If
    BG_CommitTransaction
    BL_DesbloqueiaReq = True
Exit Function
TrataErro:
    BG_RollbackTransaction
    BL_DesbloqueiaReq = False
    BG_LogFile_Erros "BL_DesbloqueiaReq: " & Err.Description & " " & sSql, "BL", "BL_DesbloqueiaReq", True
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' BLOQUEIA RESULTADOS NO SL_REALIZA

' --------------------------------------------------------------------------------------------------------------------------------------
Public Function BL_BloqueiaResultados(n_req As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iReg As Integer
    Dim rsBl As New ADODB.recordset
    Dim TRes As String
    BL_BloqueiaResultados = False
    sSql = "SELECT * FROM sl_realiza WHERE n_req = " & n_req
    rsBl.CursorLocation = adUseServer
    rsBl.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsBl.Open sSql, gConexao
    If rsBl.RecordCount >= 1 Then
        If BL_HandleNull(rsBl!cod_ana_s, "") <> gGHOSTMEMBER_S Then
            TRes = BL_HandleNull(BL_SelCodigo("SL_ANA_S", "T_RESULT", "COD_ANA_S", BL_HandleNull(rsBl!cod_ana_s, ""), "V"), "")
            ' ---------------------------------------------------------------------------------
            ' REGISTA ALTERA��ES
            ' ---------------------------------------------------------------------------------
            If BL_RegistaAlteracoes(TRes, BL_HandleNull(rsBl!seq_realiza, mediComboValorNull)) = False Then
                GoTo TrataErro
            End If
        End If
        
        sSql = "UPDATE sl_realiza SET user_val = null, dt_val = null, hr_val = null, user_tec_val = null, dt_tec_val = null, hr_tec_val = null,"
        sSql = sSql & "flg_assinado = 0, user_assinado = null, dt_assinado = null, hr_assinado = null, flg_estado = " & BL_TrataStringParaBD(gEstadoAnaBloqueada)
        sSql = sSql & " WHERE seq_realiza = " & BL_HandleNull(rsBl!seq_realiza, mediComboValorNull)
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg <> 1 Then
            GoTo TrataErro
        End If
    End If
    BL_BloqueiaResultados = True
Exit Function
TrataErro:
    BL_BloqueiaResultados = False
    BG_LogFile_Erros "BL_BloqueiaResultados: " & Err.Description & " " & sSql, "BL", "BL_BloqueiaResultados", True
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------------------------------------------------------------------------------------

' DESBLOQUEIA RESULTADOS NO SL_REALIZA

' --------------------------------------------------------------------------------------------------------------------------------------
Public Function BL_DesBloqueiaResultados(n_req As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iReg As Integer
    Dim rsBl As New ADODB.recordset
    Dim TRes As String
    BL_DesBloqueiaResultados = False
    sSql = "SELECT * FROM sl_realiza WHERE n_req = " & n_req & " AND flg_estado = " & BL_TrataStringParaBD(gEstadoAnaBloqueada)
    rsBl.CursorLocation = adUseServer
    rsBl.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsBl.Open sSql, gConexao
    If rsBl.RecordCount >= 1 Then
        If BL_HandleNull(rsBl!cod_ana_s, "") <> gGHOSTMEMBER_S Then
            TRes = BL_HandleNull(BL_SelCodigo("SL_ANA_S", "T_RESULT", "COD_ANA_S", BL_HandleNull(rsBl!cod_ana_s, ""), "V"), "")
            ' ---------------------------------------------------------------------------------
            ' REGISTA ALTERA��ES
            ' ---------------------------------------------------------------------------------
            If BL_RegistaAlteracoes(TRes, BL_HandleNull(rsBl!seq_realiza, mediComboValorNull)) = False Then
                GoTo TrataErro
            End If
        End If
        
        sSql = "UPDATE sl_realiza SET user_val = null, dt_val = null, hr_val = null, user_tec_val = null, dt_tec_val = null, hr_tec_val = null,"
        sSql = sSql & "flg_assinado = 0, user_assinado = null, dt_assinado = null, hr_assinado = null, flg_estado = " & BL_TrataStringParaBD(gEstadoAnaComResultado)
        sSql = sSql & " WHERE seq_realiza = " & BL_HandleNull(rsBl!seq_realiza, mediComboValorNull)
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg <> 1 Then
            GoTo TrataErro
        End If
    End If
    BL_DesBloqueiaResultados = True
Exit Function
TrataErro:
    BL_DesBloqueiaResultados = False
    BG_LogFile_Erros "BL_DesBloqueiaResultados: " & Err.Description & " " & sSql, "BL", "BL_DesBloqueiaResultados", True
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' retorna o numero de resultados criticos

' ------------------------------------------------------------------------------------------------

Public Function BL_RetornaResCriticos() As Integer
    Dim sSql As String
    Dim rsResCrit As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT count(*) total from sl_res_critico x1, sl_Realiza x2 WHERE x1.seq_realiza = x2.seq_Realiza AND x1.flg_tratado  = 0 and x2.flg_estado in (" & BL_TrataStringParaBD(gEstadoAnaComResultado) & "," & BL_TrataStringParaBD(gEstadoAnaValidacaoTecnica) & ")"
    rsResCrit.CursorLocation = adUseServer
    rsResCrit.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsResCrit.Open sSql, gConexao
    If rsResCrit.RecordCount >= 1 Then
        BL_RetornaResCriticos = BL_HandleNull(rsResCrit!total, 0)
    Else
        BL_RetornaResCriticos = 0
    End If
    rsResCrit.Close
    Set rsResCrit = Nothing
Exit Function
TrataErro:
    
    BL_RetornaResCriticos = 0
    BG_LogFile_Erros "BibliotecaLocal: BL_RetornaResCriticos: " & Err.Description, "BL", "BL_RetornaResCriticos", True
    Exit Function
    Resume Next
End Function



''''''''''''''''''''''''''''''''''''''''''''
' Define propriedades das linhas da lista ''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub BL_SetListViewColor(pCtrlListView As ListView, pCtrlPictureBox As PictureBox, Color1 As Long, Color2 As Long)

    Dim iLineHeight As Long
    Dim iBarHeight  As Long
    Dim lBarWidth   As Long
    Dim lColor1     As Long
    Dim lColor2     As Long
 
    lColor1 = Color1
    lColor2 = Color2
    pCtrlListView.Picture = LoadPicture("")
    pCtrlListView.Refresh
    pCtrlPictureBox.Cls
    pCtrlPictureBox.AutoRedraw = True
    pCtrlPictureBox.BorderStyle = vbBSNone
    pCtrlPictureBox.ScaleMode = vbTwips
    pCtrlPictureBox.Visible = False
    pCtrlListView.PictureAlignment = lvwTile
    pCtrlPictureBox.Font = pCtrlListView.Font
    pCtrlPictureBox.top = pCtrlListView.top
    pCtrlPictureBox.Font = pCtrlListView.Font
    With pCtrlPictureBox.Font
        .Size = pCtrlListView.Font.Size + 2
        .Bold = pCtrlListView.Font.Bold
        .Charset = pCtrlListView.Font.Charset
        .Italic = pCtrlListView.Font.Italic
        .Name = pCtrlListView.Font.Name
        .Strikethrough = pCtrlListView.Font.Strikethrough
        .Underline = pCtrlListView.Font.Underline
        .Weight = pCtrlListView.Font.Weight
    End With
    pCtrlPictureBox.Refresh
    iLineHeight = pCtrlPictureBox.TextHeight("W") + Screen.TwipsPerPixelY
    iBarHeight = (iLineHeight * 1)
    lBarWidth = pCtrlListView.Width
    pCtrlPictureBox.Height = iBarHeight * 2
    pCtrlPictureBox.Width = lBarWidth
    pCtrlPictureBox.Line (0, 0)-(lBarWidth, iBarHeight), lColor1, BF
    pCtrlPictureBox.Line (0, iBarHeight)-(lBarWidth, iBarHeight * 2), lColor2, BF
    pCtrlPictureBox.AutoSize = True
    pCtrlListView.Picture = pCtrlPictureBox.Image
    pCtrlListView.Refresh
End Sub

' ----------------------------------------------------------------------------------

' REGISTA NUMA TABELA A PARTE AS ALTERA��ES DE RESULTADO

' ----------------------------------------------------------------------------------
Public Function BL_RegistaAlteracoes(tipo As String, seqRealiza As Long) As Boolean
    
    Dim i As Integer
    Dim sSql As String
    On Error GoTo TrataErro
    
    BL_RegistaAlteracoes = False
    
    sSql = "INSERT INTO sl_ana_alteracoes(seq_ana_alteracoes, n_req,cod_perfis, cod_ana_c, cod_ana_s ,data,util, res, n_res, dt_cri, hr_cri, "
    sSql = sSql & " user_cri, dt_val, hr_val, user_val, dt_tec_val, hr_tec_val, user_tec_val, flg_estado, RES_FRASE, RES_MICRO, OBS_ANA)  "
    sSql = sSql & " SELECT seq_ana_alteracoes.nextval, x1.n_req, x1.cod_perfil, x1.cod_ana_c, x1.cod_Ana_s, data_hora," & BL_TrataStringParaBD(CStr(gCodUtilizador))
    sSql = sSql & ", x2.result, x2.n_res, x1.dt_cri, x1.hr_cri, x1.user_cri, x1.dt_val, x1.hr_val, x1.user_val, x1.dt_tec_val, "
    sSql = sSql & " x1.hr_tec_val, x1.user_tec_val, x1.flg_estado, DEVOLVE_FRASES(X1.SEQ_REALIZA), DEVOLVE_MICRO(X1.SEQ_REALIZA),x3.descr_obs_ana "
    sSql = sSql & " FROM sl_realiza x1 LEFT OUTER JOIN SL_RES_ALFAN x2 ON X1.SEQ_REALIZA = X2.SEQ_REALIZA "
    sSql = sSql & " LEFT OUTER JOIN  sl_obs_ana x3 on x1.seq_realiza = x3.seq_realiza WHERE x1.seq_realiza = " & seqRealiza
        
    
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    
    BL_RegistaAlteracoes = True
    
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro ao registar Altera��es " & Err.Description, "BL", "BL_RegistaAlteracoes"
    BL_RegistaAlteracoes = False
    Exit Function
    Resume Next
End Function


Public Sub BL_Preenche_MAReq(FgTubos As MSFlexGrid, Cod_Perfil As Variant, Descr_Perfil As Variant, cod_ana_c As Variant, Descr_Ana_C As Variant, _
                    cod_ana_s As String, descr_ana_s As String, _
                    Ord_Ana As Long, Ord_Ana_Compl As Variant, Ord_Ana_Perf As Variant, _
                    cod_agrup As String, N_Folha_Trab As Long, dt_chega As String, Flg_Apar_Transf As String, _
                    flg_estado As String, Flg_Facturado As Integer, Ord_Marca As Long, transmit_psm As Integer, _
                    n_envio As Long, dt_previ As String)
    On Error GoTo TrataErro
    ReDim Preserve MaReq(UBound(MaReq) + 1)
    MaReq(UBound(MaReq)).Cod_Perfil = Cod_Perfil
    MaReq(UBound(MaReq)).Descr_Perfil = Descr_Perfil
    MaReq(UBound(MaReq)).cod_ana_c = cod_ana_c
    MaReq(UBound(MaReq)).Descr_Ana_C = Descr_Ana_C
    MaReq(UBound(MaReq)).cod_ana_s = cod_ana_s
    MaReq(UBound(MaReq)).descr_ana_s = descr_ana_s
    MaReq(UBound(MaReq)).Ord_Ana = Ord_Ana
    MaReq(UBound(MaReq)).Ord_Ana_Compl = Ord_Ana_Compl
    MaReq(UBound(MaReq)).Ord_Ana_Perf = Ord_Ana_Perf
    MaReq(UBound(MaReq)).cod_agrup = cod_agrup
    MaReq(UBound(MaReq)).N_Folha_Trab = N_Folha_Trab
    MaReq(UBound(MaReq)).dt_chega = dt_chega
    MaReq(UBound(MaReq)).Flg_Apar_Transf = Flg_Apar_Transf
    MaReq(UBound(MaReq)).flg_estado = flg_estado
    MaReq(UBound(MaReq)).Flg_Facturado = Flg_Facturado
    MaReq(UBound(MaReq)).Ord_Marca = Ord_Marca
    MaReq(UBound(MaReq)).transmit_psm = transmit_psm
    MaReq(UBound(MaReq)).n_envio = n_envio
    TB_AdicionaTuboEstrut FgTubos, UBound(MaReq), DateValue(dt_previ)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "BL_Preenche_MAReq: " & Err.Number & " - " & Err.Description, "BL", "BL_Preenche_MAReq"
    Exit Sub
    Resume Next
    
End Sub

Sub BL_Actualiza_Estrutura_MAReq(inicio As Integer)
    On Error GoTo TrataErro

    Dim j As Long

    If inicio <> UBound(MaReq) Then
        For j = inicio To UBound(MaReq) - 1
            MaReq(j).Cod_Perfil = MaReq(j + 1).Cod_Perfil
            MaReq(j).Descr_Perfil = MaReq(j + 1).Descr_Perfil
            MaReq(j).cod_ana_c = MaReq(j + 1).cod_ana_c
            MaReq(j).Descr_Ana_C = MaReq(j + 1).Descr_Ana_C
            MaReq(j).cod_ana_s = MaReq(j + 1).cod_ana_s
            MaReq(j).descr_ana_s = MaReq(j + 1).descr_ana_s
            MaReq(j).Ord_Ana = MaReq(j + 1).Ord_Ana
            MaReq(j).Ord_Ana_Compl = MaReq(j + 1).Ord_Ana_Compl
            MaReq(j).Ord_Ana_Perf = MaReq(j + 1).Ord_Ana_Perf
            MaReq(j).cod_agrup = MaReq(j + 1).cod_agrup
            MaReq(j).N_Folha_Trab = MaReq(j + 1).N_Folha_Trab
            MaReq(j).dt_chega = MaReq(j + 1).dt_chega
            MaReq(j).Flg_Apar_Transf = MaReq(j + 1).Flg_Apar_Transf
            MaReq(j).flg_estado = MaReq(j + 1).flg_estado
            MaReq(j).Flg_Facturado = MaReq(j + 1).Flg_Facturado
            MaReq(j).seq_req_tubo = MaReq(j + 1).seq_req_tubo
            MaReq(j).indice_tubo = MaReq(j + 1).indice_tubo
        Next
    End If
    
    ReDim Preserve MaReq(UBound(MaReq) - 1)

Exit Sub
TrataErro:
    BG_LogFile_Erros "BL_Actualiza_Estrutura_MAReq: " & Err.Number & " - " & Err.Description, "BL", "BL_Actualiza_Estrutura_MAReq"
    Exit Sub
    Resume Next
End Sub


' ---------------------------------------------------------------------------------------------------

' CHAMA PROCEDIMENTO QUE DEVOLVE LINK DAS FICHAS DINAMICAS E CHAMA LINK

' ---------------------------------------------------------------------------------------------------
Public Function BL_GeraTokenFichasDinamicas(ByVal n_req_exame As String) As Boolean
     Dim CmdGera As New ADODB.Command
    Dim PmtGera As ADODB.Parameter
    Dim PmtUsername As ADODB.Parameter
    Dim PmtPassword As ADODB.Parameter
    Dim PmtNReqExame As ADODB.Parameter
    Dim link As String
    'HPP-49871
    Dim password As String
    BL_GeraTokenFichasDinamicas = False
    
    On Error GoTo TrataErro
    
    'HPP-49871
    password = "." & BL_Desencripta("iujpnt_ ") & ","
    gSQLError = 0
    gSQLISAM = 0
    
    With CmdGera
        .ActiveConnection = gConexao
        .CommandText = "SLP_GERADOR_TOKEN_PRESCRICAO"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtGera = CmdGera.CreateParameter("RES", adVarChar, adParamOutput, 400)
    'PmtGera.Precision = 12
    Set PmtUsername = CmdGera.CreateParameter("TOKEN_USERNAME", adVarChar, adParamInput, 30)
    Set PmtPassword = CmdGera.CreateParameter("TOKEN_PASSWORD", adVarChar, adParamInput, 30)
    Set PmtNReqExame = CmdGera.CreateParameter("TOKEN_N_REQ_EXAME", adVarChar, adParamInput, 30)
    CmdGera.Parameters.Append PmtUsername
    CmdGera.Parameters.Append PmtPassword
    CmdGera.Parameters.Append PmtNReqExame
    CmdGera.Parameters.Append PmtGera
    
    CmdGera.Parameters("TOKEN_USERNAME") = "glintths"
    CmdGera.Parameters("TOKEN_PASSWORD") = password
    CmdGera.Parameters("TOKEN_N_REQ_EXAME") = n_req_exame
    CmdGera.Execute
    
    link = Trim(BL_HandleNull(CmdGera.Parameters.Item("RES").value, ""))
    If link <> "" Then
        BL_ShellExecute link, "Open"
        BL_GeraTokenFichasDinamicas = True
    Else
        'HPP-49871
        BG_Mensagem mediMsgBox, "O estado da prescri��o n�o permite visualizar o documento!", vbCritical, "Aten��o"
    End If
       
    Set CmdGera = Nothing
    Set PmtGera = Nothing
    Set PmtNReqExame = Nothing
    Set PmtUsername = Nothing
    Set PmtPassword = Nothing

    
Exit Function
TrataErro:
    BL_GeraTokenFichasDinamicas = False
    BG_LogFile_Erros "BL_GeraTokenFichasDinamicas: " & Err.Description, "BL", "BL_GeraTokenFichasDinamicas"
    Set CmdGera = Nothing
    Set PmtGera = Nothing
    Set PmtNReqExame = Nothing
    Set PmtUsername = Nothing
    Set PmtPassword = Nothing
    Exit Function
    Resume Next
End Function

' pferreira 2010.09.07
Function BL_Coloca_Estado(DtPrev, DtChega, Optional DtElim = "", Optional em_transito As Boolean) As String

    If (em_transito) Then: BL_Coloca_Estado = "Transito": Exit Function
    If DtPrev = "" Then Exit Function
    If DtElim <> "" Then
        BL_Coloca_Estado = "Cancelado"
    ElseIf DateValue(DtPrev) < DateValue(Bg_DaData_ADO) And DtChega = "" Then
        BL_Coloca_Estado = "Falta"
    ElseIf DateValue(DtPrev) >= DateValue(Bg_DaData_ADO) And DtChega = "" Then
        BL_Coloca_Estado = "Espera"
    ElseIf DtChega <> "" Then
        BL_Coloca_Estado = "Entrou"
    Else
        BL_Coloca_Estado = ""
    End If

End Function


Public Function BL_ValidaNIF(nif As String) As Boolean
    On Error GoTo TrataErro
    Dim soma As Long
    Dim resto As Long
    If nif = "" Then
        BL_ValidaNIF = True
        Exit Function
    End If
    If Not IsNumeric(nif) Then
        BG_Mensagem mediMsgBox, "Valor do NIF n�o � num�rico.", vbCritical, " Valida��o do NIF"
        BL_ValidaNIF = False
        Exit Function
    End If
    If Len(nif) <> 9 Then
        BG_Mensagem mediMsgBox, "NIF Tem que ter 9 digitos.", vbCritical, " Valida��o do NIF"
        BL_ValidaNIF = False
        Exit Function
    End If
    'tania.oliveira LJMANSO-411 10.12.2019 (NIF come�ado por 3)
    If Mid(nif, 1, 1) <> 1 And Mid(nif, 1, 1) <> 2 And Mid(nif, 1, 1) <> 3 And Mid(nif, 1, 1) <> 5 And Mid(nif, 1, 1) <> 6 And Mid(nif, 1, 1) <> 8 And Mid(nif, 1, 1) <> 9 Then
        BG_Mensagem mediMsgBox, "Primeiro digito do NIF irregular.", vbCritical, " Valida��o do NIF"
        BL_ValidaNIF = False
        Exit Function
    End If
    soma = (9 * CInt(Mid(nif, 1, 1))) + (8 * CInt(Mid(nif, 2, 1))) + (7 * CInt(Mid(nif, 3, 1))) + (6 * CInt(Mid(nif, 4, 1))) + (5 * CInt(Mid(nif, 5, 1))) + (4 * CInt(Mid(nif, 6, 1))) + (3 * CInt(Mid(nif, 7, 1))) + (2 * CInt(Mid(nif, 8, 1))) + (1 * CInt(Mid(nif, 9, 1)))
    resto = soma Mod 11
    If resto <> 0 Then
        If CInt(Mid(nif, 9, 1)) = 0 Then
            soma = (9 * CInt(Mid(nif, 1, 1))) + (8 * CInt(Mid(nif, 2, 1))) + (7 * CInt(Mid(nif, 3, 1))) + (6 * CInt(Mid(nif, 4, 1))) + (5 * CInt(Mid(nif, 5, 1))) + (4 * CInt(Mid(nif, 6, 1))) + (3 * CInt(Mid(nif, 7, 1))) + (2 * CInt(Mid(nif, 8, 1))) + (1 * 10)
            resto = soma Mod 11
            If resto <> 0 Then
                BG_Mensagem mediMsgBox, "Algoritmo para c�lculo do NIF irregular.", vbCritical, " Valida��o do NIF"
                BL_ValidaNIF = False
                Exit Function
            End If
        Else
            BG_Mensagem mediMsgBox, "Algoritmo para c�lculo do NIF irregular.", vbCritical, " Valida��o do NIF"
            BL_ValidaNIF = False
            Exit Function
        End If
    End If
    BL_ValidaNIF = True
    Exit Function
    
Exit Function
TrataErro:
    BL_ValidaNIF = False
    BG_LogFile_Erros "Erro ao validar NIF: -> " & Err.Description & " ", "BL", "BL_ValidaNIF", True
    Exit Function
    Resume Next
End Function

Public Sub BL_VerificaAnexosUtente(nomeForm As Form, flg As Integer)
    Const vermelhoClaro = &HBDC8F4
    If flg = 1 Then
        nomeForm.EcFlgAnexo = "1"
        nomeForm.BtAnexos.BackColor = vermelhoClaro
    ElseIf flg = 0 Then
        nomeForm.EcFlgAnexo = "0"
        nomeForm.BtAnexos.BackColor = &H8000000F
    ElseIf flg = mediComboValorNull Then
        nomeForm.EcFlgAnexo = ""
        nomeForm.BtAnexos.BackColor = &H8000000F
    End If
End Sub

Public Sub BL_ImportaPDFsLabPets()
    Dim LeftOver As Long
    Dim i As Integer
    Dim sSql As String
    Dim RsSit As New ADODB.recordset
    Dim rsB As New ADODB.recordset
    Dim FileLength As Long
    Dim Numblocks As Integer
    Dim ByteData() As Byte 'Byte array for file.
    Dim DestFileNum As Integer
    Dim DiskFile As String
    Dim seq_anexo As Long
    Const BlockSize = 10000
    
    geResultsDSN = "DS_LABPETS"
    geResultsUTIL = "labpets"
    geResultsPWD = "labpets"
    BL_Abre_Conexao_eResults gSqlServer
    sSql = "select * from dbo.pedfile where pedfile_tipo = 'application/pdf' and pedfile_codigo between  9670  and 9999   order by pedfile_codigo"
    RsSit.CursorLocation = adUseServer
    RsSit.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsSit.Open sSql, gConexaoEresults
    If RsSit.RecordCount > 0 Then
        While Not RsSit.EOF
        
            FileLength = RsSit.Fields("pedfile_dados").ActualSize
            
            ' Remove any existing destination file.
            DiskFile = "D:\Projectos\LabPets\Migra��o de dados\PDF\" & BL_HandleNull(RsSit!pedfile_codigo, "") & ".pdf"
            If Len(Dir$(DiskFile)) > 0 Then
                Kill DiskFile
            End If
            
            DestFileNum = FreeFile
            Open DiskFile For Binary As DestFileNum
            Numblocks = FileLength \ BlockSize
            LeftOver = FileLength Mod BlockSize
            
            ByteData() = RsSit.Fields("pedfile_dados").GetChunk(LeftOver)
            Put DestFileNum, , ByteData()
            For i = 1 To Numblocks
                ByteData() = RsSit.Fields("pedfile_dados").GetChunk(BlockSize)
                Put DestFileNum, , ByteData()
            Next i
            Close DestFileNum
        
            sSql = "SELECT x1.seq_utente, x1.n_req, x2.seq_realiza FROM sl_requis x1, sl_Realiza_h x2 where x1.n_req_assoc = " & BL_TrataStringParaBD(BL_HandleNull(RsSit!pedfile_ped_codigo, "")) & " AND x1.n_Req = x2.n_req AND x2.ord_ana = " & BL_HandleNull(RsSit!pedfile_pos, 1)
            rsB.CursorLocation = adUseServer
            rsB.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsB.Open sSql, gConexao
            If rsB.RecordCount > 0 Then
                seq_anexo = mediComboValorNull
               seq_anexo = RB_NovoRegisto(rsB!seq_utente, rsB!n_req, rsB!seq_realiza, BL_HandleNull(RsSit!pedfile_codigo, "") & ".pdf", "D:\Projectos\LabPets\Migra��o de dados\PDF", "PDF")
              
            End If
            rsB.Close
            
        
            RsSit.MoveNext
        Wend
        RsSit.Close
    End If
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

Public Sub BL_ImportaOBSLabPets()
    Dim LeftOver As Long
    Dim i As Integer
    Dim sSql As String
    Dim RsSit As New ADODB.recordset
    Dim rsB As New ADODB.recordset
    Dim FileLength As Long
    Dim Numblocks As Integer
    Dim ByteData() As Byte 'Byte array for file.
    Dim DestFileNum As Integer
    Dim DiskFile As String
    Dim seq_anexo As Long
    Const BlockSize = 10000
    
    geResultsDSN = "DS_LABPETS"
    geResultsUTIL = "labpets"
    geResultsPWD = "labpets"
    BL_Abre_Conexao_eResults gSqlServer
    sSql = "select * from dbo.peddet where  peddet_analiseext <> '' "
    RsSit.CursorLocation = adUseServer
    RsSit.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsSit.Open sSql, gConexaoEresults
    If RsSit.RecordCount > 0 Then
        i = 0
        While Not RsSit.EOF
            sSql = "SELECT x1.seq_utente, x1.n_req, x2.seq_realiza, x1.dt_chega FROM sl_requis x1, sl_Realiza_h x2 where x1.n_req_assoc = " & BL_TrataStringParaBD(BL_HandleNull(RsSit!peddet_ped_codigo, "")) & " AND x1.n_Req = x2.n_req AND x2.ord_ana = " & BL_HandleNull(RsSit!peddet_pos, 1)
            rsB.CursorLocation = adUseServer
            rsB.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsB.Open sSql, gConexao
            If rsB.RecordCount > 0 And Trim(BL_HandleNull(RsSit!peddet_analiseext, "")) <> "" Then
                i = i + 1
                BG_BeginTransaction
                sSql = "INSERT into sl_obs_ana (seq_obs_ana, seq_realiza, descr_obs_ana, user_cri, dt_cri, hr_Cri) VALUES("
                sSql = sSql & 3090035 + i & ", " & rsB!seq_realiza & "," & BL_TrataStringParaBD(RsSit!peddet_analiseext) & ",'1', " & BL_TrataDataParaBD(rsB!dt_chega) & ",'00:00')"
              BG_ExecutaQuery_ADO sSql
              BG_CommitTransaction
            End If
            rsB.Close
            
        
            RsSit.MoveNext
        Wend
        RsSit.Close
    End If
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------

' PREENCHE ESTRUTURA DE ESTADOS DA ANALISE

' -------------------------------------------------------------------
Public Sub BL_PreencheEstrutTURG()
    Dim totaTURG As Integer
    On Error GoTo TrataErro
    Dim sSql  As String
    Dim rsEstados As New ADODB.recordset
    ReDim gEstrutTUrg(0)
    totaTURG = 0
    
    sSql = "SELECT * FROM sl_tbf_t_urg "
    rsEstados.CursorLocation = adUseServer
    rsEstados.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsEstados.Open sSql, gConexao
    If rsEstados.RecordCount > 0 Then
        While Not rsEstados.EOF
            totaTURG = totaTURG + 1
            ReDim Preserve gEstrutTUrg(totaTURG)
             gEstrutTUrg(totaTURG).codigo = BL_HandleNull(rsEstados!cod_t_urg, " ")
             gEstrutTUrg(totaTURG).descricao = BL_HandleNull(rsEstados!descr_t_urg, "SEM DESCRI��O")
             gEstrutTUrg(totaTURG).cor = CLng(BL_HandleNull(rsEstados!cor, vbWhite))
            rsEstados.MoveNext
        Wend
    End If
    rsEstados.Close
    Set rsEstados = Nothing
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  BL_PreencheEstrutTURG " & Err.Description & " " & sSql, "BL", "BL_PreencheEstrutTURG"
    Exit Sub
    Resume Next
End Sub


' -------------------------------------------------------------------

' DADO UMA STRING DEVOLVE APENAS INICIAIS

' -------------------------------------------------------------------
Public Function BL_DevolveIniciais(nome As String) As String
    On Error GoTo TrataErro
    Dim i As Integer
    Dim tokens() As String
    BL_Tokenize nome, " ", tokens
    BL_DevolveIniciais = ""
    For i = 0 To UBound(tokens)
        BL_DevolveIniciais = BL_DevolveIniciais & Mid(tokens(i), 1, 1)
    Next i
    BL_DevolveIniciais = UCase(BL_DevolveIniciais)
Exit Function
TrataErro:
    BL_DevolveIniciais = ""
    BG_LogFile_Erros "Erro BL_DevolveIniciais: -> " & Err.Description & " ", "BL", "BL_DevolveIniciais", True
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------

' DADO UMA CONFIGURA��O RETORNA O URL PARA CHAMADA WEBSERVICE

' -------------------------------------------------------------------
Public Function BL_DevolveURLWebService(cod_config As String) As String
    Dim sSql As String
    Dim rsWS As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_webservice_config WHERE cod_config = " & BL_TrataStringParaBD(cod_config)
    rsWS.CursorLocation = adUseServer
    rsWS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsWS.Open sSql, gConexao
    If rsWS.RecordCount > 0 Then
        BL_DevolveURLWebService = BL_HandleNull(rsWS!url, "")
    Else
        BL_DevolveURLWebService = ""
    End If
    rsWS.Close
    Set rsWS = Nothing
Exit Function
TrataErro:
    BL_DevolveURLWebService = ""
    BG_LogFile_Erros "Erro BL_DevolveURLWebService: -> " & Err.Description & " ", "BL", "BL_DevolveURLWebService", True
    Exit Function
    Resume Next
End Function

'RGONCALVES 17.02.2015 CHSJ-1785
Public Function BL_DevolveCodTDest(ByVal cod_proven As String) As String
    Dim sSql As String
    Dim rs As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_cod_destino_proven WHERE cod_proven = " & BL_TrataStringParaBD(cod_proven)
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
    If rs.RecordCount > 0 Then
        BL_DevolveCodTDest = CStr(BL_HandleNull(rs!cod_t_dest, ""))
    Else
        BL_DevolveCodTDest = ""
    End If
    rs.Close
    Set rs = Nothing
Exit Function
TrataErro:
    BL_DevolveCodTDest = ""
    BG_LogFile_Erros "Erro BL_DevolveCodTDest: -> " & Err.Description & " ", "BL", "BL_DevolveCodTDest", False
    Exit Function
    Resume Next
End Function

'RGONCALVES 07.07.2015 CHSJ-2082
Public Function BL_AuthenticateActiveDirectoryUser(ByVal Path As String, ByVal user As String, ByVal password As String) As Boolean
    On Error GoTo TrataErro
   
    Dim RootDSE
    Dim DSObject
    Dim Auth
    Dim NamingContext
    Const ADS_SECURE_AUTHENTICATION = 1
    
    'Set RootDSE = GetObject("GC://rootDSE")
    Set RootDSE = GetObject("LDAP://rootDSE")
    
    NamingContext = RootDSE.get("defaultNamingContext")

    'Set DSObject = GetObject("GC:")
    Set DSObject = GetObject("LDAP:")

    'Set Auth = DSObject.OpenDSObject("GC://" & NamingContext, User, Password, ADS_SECURE_AUTHENTICATION)
    Set Auth = DSObject.OpenDSObject("LDAP://" & Path, user, password, ADS_SECURE_AUTHENTICATION)

    Set Auth = Nothing
    Set DSObject = Nothing
    Set RootDSE = Nothing
    
    BG_LogFile_Erros ("BL_AuthenticateActiveDirectoryUser USER: " & user & " - OK")
    BL_AuthenticateActiveDirectoryUser = True
    Exit Function
TrataErro:
    BG_LogFile_Erros ("BL_AuthenticateActiveDirectoryUser USER: " & user & " - ERRO: " & Err.Description)
    BL_AuthenticateActiveDirectoryUser = False
    Exit Function
    Resume Next
End Function



Public Function BL_RegistaReqEpisodio(n_req As String) As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    Dim sSql As String
    Dim cenas As Integer
    cenas = FormGestaoRequisicaoPrivado.RegistosA.Count - 1
        For i = 1 To cenas
            sSql = "DELETE FROM SL_REQ_EPISODIO WHERE n_req = " & n_req & " AND cod_agrup = " & BL_TrataStringParaBD(FormGestaoRequisicaoPrivado.RegistosA(i).codAna)
            BG_ExecutaQuery_ADO (sSql)
            
            sSql = "INSERT INTO SL_REQ_EPISODIO (n_req, cod_agrup, n_prescricao,cod_destino) VALUES("
            sSql = sSql & n_req & ", " & BL_TrataStringParaBD(FormGestaoRequisicaoPrivado.RegistosA(i).codAna) & "," & BL_TrataStringParaBD(FormGestaoRequisicaoPrivado.RegistosA(i).n_prescricao)
            sSql = sSql & "," & BL_HandleNull(FormGestaoRequisicaoPrivado.RegistosA(i).cod_destino, "NULL") & ")"
            BG_ExecutaQuery_ADO sSql
        Next i
    BL_RegistaReqEpisodio = True
    
Exit Function
TrataErro:
    BG_LogFile_Erros "BL_RegistaReqEpisodioPrivado - ERRO: " & Err.Description, "BL", "BL_RegistaReqEpisodio", True
    BL_RegistaReqEpisodio = False
    Exit Function
    Resume Next
End Function


Public Function BL_VerificaInfoClinObrig(cod_ana As String, tipo As String) As Boolean
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM slv_analises_apenas WHERE cod_ana = " & BL_TrataStringParaBD(cod_ana)
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount = 1 Then
        If tipo = "I" Then
            If BL_HandleNull(rsAna!flg_obriga_infocli, mediComboValorNull) = 1 Then
                BL_VerificaInfoClinObrig = True
            Else
                BL_VerificaInfoClinObrig = False
            End If
        ElseIf tipo = "T" Then
            If BL_HandleNull(rsAna!flg_obriga_terap, mediComboValorNull) = 1 Then
                BL_VerificaInfoClinObrig = True
            Else
                BL_VerificaInfoClinObrig = False
            End If
        Else
            BL_VerificaInfoClinObrig = False
        End If
    Else
        BL_VerificaInfoClinObrig = False
    End If
    rsAna.Close
    Set rsAna = Nothing
    
Exit Function
TrataErro:
    BL_VerificaInfoClinObrig = False
    BG_LogFile_Erros "Erro BL_VerificaInfoClinObrig: -> " & Err.Description & " ", "BL", "BL_VerificaInfoClinObrig", False
    Exit Function
    Resume Next
End Function

Public Function BL_ValidaDadosInfoCli(seq_utente As Long, n_req As String, tipo As String) As Boolean
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    
    If tipo = "T" Then
        sSql = "SELECT * FROM sl_tm_ute WHERE n_req in (-" & gNumeroSessao
        If n_req <> "" Then
            sSql = sSql & "," & n_req
        End If
    sSql = sSql & ")"
    ElseIf tipo = "I" Then
        sSql = "SELECT  seq_utente from sl_diag_pri where seq_utente =" & seq_utente
        sSql = sSql & " UNION select n_req from sl_diag_sec where  n_req in (-" & gNumeroSessao
        If n_req <> "" Then
            sSql = sSql & "," & n_req
        End If
    sSql = sSql & ")"
    End If
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount >= 1 Then
        BL_ValidaDadosInfoCli = True
    Else
        BL_ValidaDadosInfoCli = False
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    BL_ValidaDadosInfoCli = False
    BG_LogFile_Erros "Erro BL_ValidaDadosInfoCli: -> " & Err.Description & " ", "BL", "BL_ValidaDadosInfoCli", False
    Exit Function
    Resume Next
End Function

'BRUNODSANTOS CHUC-7934 - 27.04.2016
Function BL_ImprimeEtiqResumo(ByRef eTubos() As estruturaTubos, ByRef PassaParam As PassaParamImpForm, _
ByVal flg_VerificarSeJaImprimiuAntes As Boolean, ByRef arrReq() As Long) As Boolean

Dim NumReqBar As String
Dim j As Integer
Dim i As Integer
Dim sql As String
Dim EtqCrystal As String
Dim continua As Boolean


On Error GoTo TrataErro
'******INICIO ETIQUETA RESUMO*************************
If gImprimirEtiqResumo = mediSim Then

    

    NumReqBar = ""
    For j = 1 To (7 - Len(PassaParam.NumReq))
        NumReqBar = NumReqBar & "0"
    Next j
    NumReqBar = NumReqBar & PassaParam.NumReq

    Dim RsEtiqResumo As ADODB.recordset
    Dim TmpStr As String
    Dim StrComplexas As String
    Dim VecAbr() As String
    Dim TotalVecAbr As Integer
    Dim flgImprimir As Boolean
    
    
    Set RsEtiqResumo = New ADODB.recordset
    RsEtiqResumo.CursorLocation = adUseServer
    RsEtiqResumo.CursorType = adOpenStatic
    
    StrComplexas = "" ' Imprime so uma vez a descricao de cada complexa
    
    TotalVecAbr = 0
    
     If flg_VerificarSeJaImprimiuAntes = True Then
        If BL_JaExisisteReq(PassaParam.NumReq, arrReq()) = True Then
            Exit Function
        Else
            ReDim Preserve arrReq(UBound(arrReq) + 1)
            arrReq(UBound(arrReq)) = PassaParam.NumReq
        End If
    End If
    
    For i = 1 To UBound(eTubos())
                       
        sql = ""
        If eTubos(i).codigo_analise <> "0" And left(eTubos(i).codigo_analise, 1) = "C" Then
            sql = "SELECT abr_ana_c abr_ana FROM sl_ana_c WHERE cod_ana_c='" & eTubos(i).codigo_analise & "'"
            StrComplexas = StrComplexas & eTubos(i).codigo_analise & ";"
        ElseIf eTubos(i).codigo_analise <> "0" And left(eTubos(i).codigo_analise, 1) = "S" Then
            sql = "SELECT abr_ana_s abr_ana FROM sl_ana_s WHERE cod_ana_s='" & eTubos(i).codigo_analise & "'"
        End If
        If sql <> "" Then
            RsEtiqResumo.Open sql, gConexao
            If RsEtiqResumo.RecordCount > 0 Then
                TmpStr = TmpStr & BL_HandleNull(RsEtiqResumo!abr_ana, "")
                TotalVecAbr = TotalVecAbr + 1
                ReDim Preserve VecAbr(TotalVecAbr)
                VecAbr(TotalVecAbr) = BL_HandleNull(RsEtiqResumo!abr_ana, "")
            End If
            RsEtiqResumo.Close
        End If
    Next i
    Set RsEtiqResumo = Nothing
        
    If TotalVecAbr < 12 Then
        TotalVecAbr = 12
        ReDim Preserve VecAbr(TotalVecAbr)
    End If
        
    If gLAB = "HOVAR" Then
        EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoComputador, "ETIQ_CRYSTAL")
    Else
        EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ETIQ_CRYSTAL")
    
    If EtqCrystal = "-1" Or EtqCrystal = "" Then EtqCrystal = "1"
    End If
        
    If EtqCrystal = "1" Then 'Crystal
        If BL_PreviewAberto("EtiquetaResumo") = True Then Exit Function
            
        continua = BL_IniciaReport("EtiquetaResumo", "Etiqueta Resumo", crptToPrinter, False, , , , , PassaParam.PrinterEtiq)
        If continua = False Then Exit Function
        
        Dim ReportResumo As CrystalReport
        Set ReportResumo = forms(0).Controls("Report")
            
        For i = 0 To Printers.Count - 1
            If Printers(i).DeviceName = PassaParam.PrinterEtiq Then
                ReportResumo.PrinterName = Printers(i).DeviceName
                ReportResumo.PrinterDriver = Printers(i).DriverName
                ReportResumo.PrinterPort = Printers(i).Port
            End If
        Next i
        
        flgImprimir = False
        For i = 1 To TotalVecAbr
            If i <= 12 Then
                ReportResumo.formulas(i - 1) = "Abr" & i & " ='" & VecAbr(i) & "'"
                flgImprimir = True
            Else
                ReportResumo.formulas((i Mod 12) - 1) = "Abr" & i Mod 12 & " ='" & VecAbr(i) & "'"
                flgImprimir = True
            End If
            If i Mod 12 = 0 Then
                flgImprimir = False
                ReportResumo.formulas(i) = "NumReq='" & NumReqBar & "'"
                ReportResumo.Destination = crptToPrinter
                ReportResumo.Action = 1
                For j = 1 To 12
                    ReportResumo.formulas(j - 1) = "Abr" & j & " =''"
                Next j
    '            Call BL_ExecutaReport
            End If
        Next i
        If flgImprimir Then
            ReportResumo.formulas(i) = "NumReq='" & NumReqBar & "'"
            ReportResumo.Destination = crptToWindow
            ReportResumo.Action = 1
        End If
        
        ReportResumo.Reset
    Else
        'ZPL
        
        
        
        NumReqBar = ""
        For j = 1 To (7 - Len(PassaParam.NumReq))
            NumReqBar = NumReqBar & "0"
        Next j
        NumReqBar = NumReqBar & PassaParam.NumReq
        
        TmpStr = ""
        For i = 1 To TotalVecAbr
            If i <= 12 Then
                TmpStr = TmpStr & VecAbr(i) & ","
                flgImprimir = True
            Else
                TmpStr = TmpStr & VecAbr(i) & ","
                flgImprimir = True
            End If
            If i Mod 12 = 0 Then
                If Not BL_LerEtiqIni("EtiqResumo.ini") Then
                    MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                    Exit Function
                End If
                If Not BL_EtiqOpenPrinter(PassaParam.PrinterEtiq) Then
                    MsgBox "Imposs�vel abrir impressora etiquetas"
                    Exit Function
                End If
                flgImprimir = False
                
                Call BL_EtiqPrintResumo(TmpStr, NumReqBar)
                BL_EtiqClosePrinter
                TmpStr = ""
            End If
        Next i
        
        If flgImprimir = True Then
            If Not BL_LerEtiqIni("EtiqResumo.ini") Then
                MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                Exit Function
            End If
            If Not BL_EtiqOpenPrinter(PassaParam.PrinterEtiq) Then
                MsgBox "Imposs�vel abrir impressora etiquetas"
                Exit Function
            End If
            Call BL_EtiqPrintResumo(TmpStr, PassaParam.NumReq)
            BL_EtiqClosePrinter
        End If
    End If
End If
BL_ImprimeEtiqResumo = True
'******FIM ETIQUETA RESUMO*************************
     
Exit Function

TrataErro:
Exit Function
BL_ImprimeEtiqResumo = False

End Function

'BRUNODSANTOS CHUC-7934 - 27.04.2016
Public Function BL_EtiqPrintResumo(ByVal Abr As String, NReq As String) As Boolean
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    Dim VecAna() As String

    VecAna = Split(Abr, ",")
    
    If UBound(VecAna()) < 12 Then
        ReDim Preserve VecAna(12)
    End If

    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{ABR1}", Mid(VecAna(0), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR2}", Mid(VecAna(1), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR3}", Mid(VecAna(2), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR4}", Mid(VecAna(3), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR5}", Mid(VecAna(4), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR6}", Mid(VecAna(5), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR7}", Mid(VecAna(6), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR8}", Mid(VecAna(7), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR9}", Mid(VecAna(8), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR10}", Mid(VecAna(9), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR11}", Mid(VecAna(10), 1, 5))
    sWrittenData = Replace(sWrittenData, "{ABR12}", Mid(VecAna(11), 1, 5))
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", NReq)
    
    If sWrittenData <> "" Then
        If gModoDebug = mediSim Then
            BG_LogFile_Erros "BL_EtiqPrintResumo"
            BG_LogFile_Erros "Abr=" & Abr
            BG_LogFile_Erros "NReq=" & NReq
            BG_LogFile_Erros sWrittenData
        End If
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    BL_EtiqPrintResumo = True
    
End Function

'BRUNODSANTOS CHUC-7934 - 26.04.2016
Function BL_JaExisisteReq(ByVal requisicao As Long, ByRef arrReq() As Long) As Boolean

    Dim j As Integer

    For j = 1 To UBound(arrReq())
        If arrReq(j) = requisicao Then
            BL_JaExisisteReq = True
        Else
            BL_JaExisisteReq = False
        End If
    Next j

End Function

'BRUNODSANTOS CHSJ-2670 - 17.05.2016

Sub BL_GeraHash_ConteudoTabela(ByVal NReq As String)

    Dim sqlColunas As String
    Dim sqlTabelas As String
    Dim sSql As String
    Dim rsColunas As New ADODB.recordset
    Dim rsTabelas As New ADODB.recordset
    Dim rs As New ADODB.recordset
    Dim sString As String
    Dim sStringAux As String
    Dim sColunas As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    HashReport.NumRequis = ""
    HashReport.Hash = ""
    HashReport.versao = ""
    
    sString = ""
    
    HashReport.NumRequis = NReq
    
    rsTabelas.CursorLocation = adUseServer
    rsTabelas.CursorType = adOpenStatic
    
    rsColunas.CursorLocation = adUseServer
    rsColunas.CursorType = adOpenStatic
       
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    
    sqlTabelas = "SELECT DISTINCT tabela FROM sl_cod_report_hash order by tabela "
    rsTabelas.Open sqlTabelas, gConexao
    
    While Not rsTabelas.EOF
        sStringAux = ""
        sColunas = ""
        sqlColunas = "SELECT coluna FROM sl_cod_report_hash WHERE flg_activo = 1 and tabela = " & BL_TrataStringParaBD(rsTabelas!Tabela) & " order by seq "
        rsColunas.Open sqlColunas, gConexao
        
        
        While Not rsColunas.EOF
            sColunas = sColunas & rsColunas!coluna & ","
            rsColunas.MoveNext
        Wend
        
        rsColunas.Close
        If sColunas <> "" Then
            sColunas = left(sColunas, Len(sColunas) - 1)
                
            sSql = "SELECT " & sColunas & "  FROM " & rsTabelas!Tabela & " WHERE nome_computador = " & BL_TrataStringParaBD(BG_SYS_GetComputerName) & _
                  " AND num_sessao = " & gNumeroSessao & " ORDER BY " & sColunas
            rs.Open sSql, gConexao
            
            While Not rs.EOF
            
                sStringAux = "[" & rsTabelas!Tabela & "]" & vbCrLf
                For i = 0 To rs.Fields.Count - 1
                    sStringAux = sStringAux & "[" & BL_HandleNull(rs.Fields(i).Name, "") & "]" & BL_HandleNull(rs.Fields(i).value, "") & vbCrLf
                Next i
                
                sString = sString & sStringAux & vbCrLf
                
            rs.MoveNext
            Wend
               
            rs.Close
        End If
        rsTabelas.MoveNext
    Wend
    
    rsTabelas.Close
    Set rsTabelas = Nothing
    Set rsColunas = Nothing
    Set rs = Nothing
    
    HashReport.Hash = CreateMD5HashString(sString)
    BL_TrataTipoImpr HashReport.NumRequis, HashReport.Hash, sString
     
    Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro BL_GeraHash_ConteudoTabela: -> " & Err.Description & " ", "BL", "BL_GeraHash_ConteudoTabela"
    Exit Sub
    Resume Next
End Sub

'BRUNODSANTOS CHSJ-2670 - 17.05.2016

Sub BL_TrataTipoImpr(ByVal NReq As String, ByVal HashGerada As String, ByVal HashSource As String)

    Dim sSql As String
    Dim flg_segundaVia As Boolean
    Dim rs As New ADODB.recordset
    On Error GoTo TrataErro
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    
    sSql = "SELECT hash, versao FROM sl_impr_completas WHERE n_req = " & BL_TrataStringParaBD(NReq) & "AND hash IS NOT NULL ORDER BY id DESC "
    rs.Open sSql, gConexao

    
    If rs.RecordCount > 0 Then
    
        If BL_HandleNull(rs!Hash, "") = HashGerada Then
        
            flg_segundaVia = True
            HashReport.versao = BL_HandleNull(rs.Fields("versao").value, "")
            
        Else
         
            flg_segundaVia = False
            HashReport.versao = BL_HandleNull(rs.Fields("versao").value + 1, "")
            
        End If
        
        If HashReport.versao = "" Then
            HashReport.versao = 1
        End If
        
    Else
        HashReport.versao = "1"
    
    End If
    BL_Actualiza_SL_CR_LRVF NReq, flg_segundaVia, HashGerada, HashReport.versao
    BG_LogFile_Erros (HashGerada & ";" & HashReport.versao & ";" & HashSource)
    rs.Close
    Set rs = Nothing
    
    Exit Sub
    
TrataErro:

    BG_LogFile_Erros "Erro BL_TrataTipoImpr: -> " & Err.Description & " ", "BL", "BL_TrataTipoImpr"
    Resume Next
    Exit Sub

End Sub

'BRUNODSANTOS CHSJ-2670 - 05.05.2016
Sub BL_Actualiza_SL_CR_LRVF(ByVal NReq As String, ByVal flg_segundaVia As Boolean, ByVal HashGerada As String, ByVal versao As String)

Dim iRes As Long
Dim sSqlUpdate As String
Dim segunda_via As Integer

On Error GoTo TrataErro

    If flg_segundaVia = True Then
        segunda_via = 1
    Else
        segunda_via = 0
    End If

    sSqlUpdate = "UPDATE sl_cr_lrvf SET segunda_via = " & segunda_via & " , hash = " & BL_TrataStringParaBD(HashGerada) & _
                     ", versao = " & versao & " WHERE n_req = " & BL_TrataStringParaBD(NReq) & _
                     " AND nome_computador = " & BL_TrataStringParaBD(gComputador) & _
                     " AND num_sessao = " & gNumeroSessao
            
    iRes = BG_ExecutaQuery_ADO(sSqlUpdate)
        
    If iRes = -1 Then
        GoTo TrataErro
    End If
    
Exit Sub

TrataErro:

    BG_LogFile_Erros "Erro BL_Actualiza_SL_CR_LRVF: -> " & Err.Description & " ", "BL", "BL_Actualiza_SL_CR_LRVF"
    Resume Next
    Exit Sub

End Sub

'BRUNODSANTOS 20.06.2016

Public Function BL_RegGetExcelVersion(hLocation As Long, sSection As String, sKey As String, Optional sDefault As Variant) As String
'   Exemplos:
'   -------
'    Dim hLocation As Long
'    Dim sSection As String
'    Dim sKey As String
'    Dim sSetting As String
'
'    hLocation = HKEY_CURRENT_USER -- HKEY_CLASSES_ROOT
'    sSection = "Software\VB and VBA Program Settings\chip\gSGBD" -- Excel.Application
'
'    ' OBTER...
'    sKey = "LoginTimeout" -- CurVer
'    MsgBox BG_RegGetSetting(hLocation, sSection, sKey, "Nada")
'    MsgBox BG_RegGetSetting(hLocation, sSection, sKey)
'
'    ' CRIAR...
'    sKey = "zzTESTE"
'    sSetting = "Testando............."
'    BG_RegSaveSetting hLocation, sSection, sKey, sSetting
'    BG_RegSaveSetting hLocation, sSection, sKey
'
'    ' APAGAR
'    BG_RegDeleteSetting hLocation, sSection, sKey, mediRegLeft
'    BG_RegDeleteSetting hLocation, sSection, sKey, mediRegRight
'
    
    Dim res
    Dim sRes As String
    
    Dim phkResult As Long
    Dim lpType As Long
    Dim lpData As String
    Dim lpcbData As Long
    
    If Not IsMissing(sDefault) Then
        sRes = sDefault
    Else
        sRes = ""
    End If
    
    res = RegOpenKeyEx(hLocation, sSection, 0, KEY_QUERY_VALUE, phkResult)
    If res = 0 Then
        lpcbData = 255
        lpData = Space(lpcbData)
        res = RegQueryValueEx(phkResult, sKey, 0, lpType, (lpData), lpcbData)
    
        If res = 0 Then sRes = left(lpData, lpcbData - 1)
        
        res = RegCloseKey(phkResult)
    End If
    
    BL_RegGetExcelVersion = sRes
End Function

'BRUNODSANTOS 20.06.2016
Public Function BL_GetOfficeName(ByVal officeVersion As String) As String

Select Case officeVersion
    Case ""
        BL_GetOfficeName = ""
    Case "7"
        BL_GetOfficeName = "Office 95"
    Case "8"
        BL_GetOfficeName = "Office 97"
    Case "9"
        BL_GetOfficeName = "Office 2000"
    Case "10"
       BL_GetOfficeName = "Office 2002"
    Case "11"
        BL_GetOfficeName = "Office 2003"
    Case "12"
        BL_GetOfficeName = "Office 2007"
    Case "14"
        BL_GetOfficeName = "Office 2010"
    Case "15"
        BL_GetOfficeName = "Office 2013"
    Case "16"
        BL_GetOfficeName = "Office 2016"
End Select

End Function

Public Function BL_RetornaSequencia(sequencia As String) As Long
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsSMS As New ADODB.recordset
    
    BL_RetornaSequencia = -1
    sSql = "SELECT " & sequencia & ".nextval prox FROM dual "
    rsSMS.CursorLocation = adUseServer
    rsSMS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSMS.Open sSql, gConexao
    If rsSMS.RecordCount >= 1 Then
        BL_RetornaSequencia = BL_HandleNull(rsSMS!prox, -1)
    End If
    rsSMS.Close
    Set rsSMS = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "BL_RetornaSequencia - ERRO: " & Err.Description, "BL", "BL_RetornaSequencia", True
    BL_RetornaSequencia = mediComboValorNull
    Exit Function
    Resume Next
End Function

'BRUNODSANTOS Glintt-HS-17949 10.01.2018
Public Function BL_VerificaContaFechada(ByVal t_doente As String, ByVal doente As String, ByVal t_episodio As String, ByVal episodio As String, ByVal t_episodio_pai As String, _
                                        ByVal episodio_pai As String, ByVal dt_ini As Date, ByVal dt_fim As Date, ByVal epis_novo_alterado As String) As Boolean

Dim cmd As New ADODB.Command
Dim Pmt_t_doente As ADODB.Parameter
Dim Pmt_doente As ADODB.Parameter
Dim Pmt_t_episodio As ADODB.Parameter
Dim Pmt_episodio As ADODB.Parameter
Dim Pmt_t_episodio_pai As ADODB.Parameter
Dim Pmt_episodio_pai As ADODB.Parameter
Dim Pmt_dt_ini As ADODB.Parameter
Dim Pmt_dt_fim As ADODB.Parameter
Dim Pmt_epis_novo_alterado As ADODB.Parameter
Dim PmtRes As ADODB.Parameter


On Error GoTo TrataErro
BL_VerificaContaFechada = False
 
    With cmd
        .ActiveConnection = gConexao
        .CommandText = "slp_verifica_conta_fechada"
        .CommandType = adCmdStoredProc
    End With


    Set Pmt_t_doente = cmd.CreateParameter("i_t_doente", adVarChar, adParamInput, 20)
    Set Pmt_doente = cmd.CreateParameter("i_doente", adVarChar, adParamInput, 20)
    Set Pmt_t_episodio = cmd.CreateParameter("i_t_episodio", adVarChar, adParamInput, 20)
    Set Pmt_episodio = cmd.CreateParameter("i_episodio", adVarChar, adParamInput, 20)
    Set Pmt_t_episodio_pai = cmd.CreateParameter("i_t_episodio_pai", adVarChar, adParamInput, 20)
    Set Pmt_episodio_pai = cmd.CreateParameter("i_episodio_pai", adVarChar, adParamInput, 20)
    Set Pmt_dt_ini = cmd.CreateParameter("i_dt_ini", adDBDate, adParamInput, 0)
    Set Pmt_dt_fim = cmd.CreateParameter("i_dt_fim", adDBDate, adParamInput, 0)
    Set Pmt_epis_novo_alterado = cmd.CreateParameter("i_epis_novo_alterado", adVarChar, adParamInput, 2)
    Set PmtRes = cmd.CreateParameter("res", adVarChar, adParamOutput, 10)


    cmd.Parameters.Append Pmt_t_doente
    cmd.Parameters.Append Pmt_doente
    cmd.Parameters.Append Pmt_t_episodio
    cmd.Parameters.Append Pmt_episodio
    cmd.Parameters.Append Pmt_t_episodio_pai
    cmd.Parameters.Append Pmt_episodio_pai
    cmd.Parameters.Append Pmt_dt_ini
    cmd.Parameters.Append Pmt_dt_fim
    cmd.Parameters.Append Pmt_epis_novo_alterado
    cmd.Parameters.Append PmtRes

    cmd.Parameters("i_t_doente") = t_doente
    cmd.Parameters("i_doente") = doente
    cmd.Parameters("i_t_episodio") = t_episodio
    cmd.Parameters("i_episodio") = episodio
    cmd.Parameters("i_t_episodio_pai") = t_episodio_pai
    cmd.Parameters("i_episodio_pai") = episodio_pai
    cmd.Parameters("i_dt_ini") = dt_ini
    cmd.Parameters("i_dt_fim") = dt_fim
    cmd.Parameters("i_epis_novo_alterado") = epis_novo_alterado
     
    cmd.Execute
    
    If cmd.Parameters.Item("res").value = "TRUE" Then
        BL_VerificaContaFechada = True
    End If
    
   
    Set Pmt_t_doente = Nothing
    Set Pmt_doente = Nothing
    Set Pmt_t_episodio = Nothing
    Set Pmt_episodio = Nothing
    Set Pmt_t_episodio_pai = Nothing
    Set Pmt_episodio_pai = Nothing
    Set Pmt_dt_ini = Nothing
    Set Pmt_dt_fim = Nothing
    Set Pmt_epis_novo_alterado = Nothing
    Set PmtRes = Nothing
    Set cmd = Nothing
     
  Exit Function
  
TrataErro:
    BG_LogFile_Erros "BL_VerificaContaFechada - ERRO: " & Err.Description, "BL", "BL_VerificaContaFechada", True
    Set cmd = Nothing
    BL_VerificaContaFechada = False
    Exit Function
    Resume Next
  
    
End Function

Public Sub BL_Devolve_tEpis_Epis_FROM_SL_REQUIS(ByVal NReq As String, ByRef t_epis As String, ByRef Epis As String)
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT  * FROM sl_requis WHERE n_req =" & BL_TrataStringParaBD(NReq)
           
    rsAna.CursorLocation = adUseClient
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount >= 1 Then
        t_epis = rsAna.Fields("t_sit").value
        Epis = rsAna.Fields("n_epis").value
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro BL_Devolve_tEpis_Epis_FROM_SL_REQUIS: -> " & Err.Description & " ", "BL", "BL_Devolve_tEpis_Epis_FROM_SL_REQUIS", False
    Exit Sub
    Resume Next
End Sub

'NELSONPSILVA UnimedVitoria-1152 12.01.2018
Function BL_DevolveUtilizadorAtual(codigo As String) As String
    
    Dim sql As String
    Dim rsUtil As ADODB.recordset
    
    On Error GoTo TrataErro
    sql = "SELECT utilizador FROM sl_idutilizador WHERE cod_utilizador=" & gCodUtilizador
    Set rsUtil = New ADODB.recordset
    rsUtil.CursorLocation = adUseServer
    rsUtil.CursorType = adOpenStatic
    rsUtil.Open sql, gConexao
    If rsUtil.RecordCount > 0 Then
        BL_DevolveUtilizadorAtual = BL_HandleNull(rsUtil!utilizador, "")
    End If
    rsUtil.Close
    Set rsUtil = Nothing
    
Exit Function
TrataErro:
    BL_DevolveUtilizadorAtual = ""
    BG_LogFile_Erros "BibliotecaLocal: BL_DevolveUtilizadorAtual: " & Err.Description, "BL", "BL_DevolveUtilizadorAtual"
    Exit Function
    Resume Next
End Function

'NELSONPSILVA UnimedVitoria-1152 12.01.2018
Function BL_Devolve_URL_MIS_Data_Discovery(ByVal id_indicador As Long) As String

Dim CmdGera As New ADODB.Command
Dim PmtGera As ADODB.Parameter
Dim PmtUSER As ADODB.Parameter
Dim PmtIndicador As ADODB.Parameter
Dim PmtLocal As ADODB.Parameter
Dim PmtCompany As ADODB.Parameter
Dim PmtRetorno As ADODB.Parameter

On Error GoTo TrataErro
gSQLError = 0
gSQLISAM = 0

    With CmdGera
        .ActiveConnection = gConexao
        .CommandText = "slp_devolve_url_mis_dd"
        .CommandType = adCmdStoredProc
    End With

    Set PmtUSER = CmdGera.CreateParameter("P_USER", adVarChar, adParamInput, 50)
    Set PmtIndicador = CmdGera.CreateParameter("P_INDICADOR", adNumeric, adParamInput, 10)
    Set PmtLocal = CmdGera.CreateParameter("P_COD_LOCAL", adNumeric, adParamInput, 10)
    Set PmtCompany = CmdGera.CreateParameter("P_COMAPNY", adVarChar, adParamInput, 256)
    Set PmtRetorno = CmdGera.CreateParameter("O_RETORNO", adVarChar, adParamOutput, 1000)

    CmdGera.Parameters.Append PmtUSER
    CmdGera.Parameters.Append PmtIndicador
    CmdGera.Parameters.Append PmtLocal
    CmdGera.Parameters.Append PmtCompany
    CmdGera.Parameters.Append PmtRetorno

    CmdGera.Parameters("P_USER").value = BL_DevolveUtilizadorAtual(CStr(gCodUtilizador))
    CmdGera.Parameters("P_INDICADOR").value = id_indicador
    CmdGera.Parameters("P_COD_LOCAL").value = gCodLocal
    CmdGera.Parameters("P_COMAPNY").value = gCompanyDb

    CmdGera.Execute

    BL_Devolve_URL_MIS_Data_Discovery = Trim(BL_HandleNull(CmdGera.Parameters.Item("O_RETORNO").value, ""))

    Exit Function

TrataErro:
    BL_Devolve_URL_MIS_Data_Discovery = ""
    BG_LogFile_Erros "BL_Devolve_URL_MIS_Data_Discovery - ERRO: " & Err.Description, "BL", "BL_Devolve_URL_MIS_Data_Discovery", True
    Set CmdGera = Nothing
    Resume Next
    Exit Function
    
End Function

'NELSONPSILVA Glintt-HS-18011 04.04.2018
Public Sub BL_REGISTA_LOGS_RGPD(ByVal facility_id As Integer, ByVal Username As String, ByVal machineName As String, ByVal osuser As String, _
                                     ByVal audsid As String, ByVal context As String, ByVal StartDate As String, ByVal EndDate As String, ByVal contextinfo As String, _
                                     ByVal requestJson As String, ByVal responseJson As String)

Dim cmd As New ADODB.Command
Dim Pmt_facility_id As ADODB.Parameter
Dim Pmt_username As ADODB.Parameter
Dim Pmt_machineName As ADODB.Parameter
Dim Pmt_OSUser As ADODB.Parameter
Dim Pmt_audsid As ADODB.Parameter
Dim Pmt_context As ADODB.Parameter
Dim Pmt_startdate As ADODB.Parameter
Dim Pmt_enddate As ADODB.Parameter
Dim Pmt_contextinfo As ADODB.Parameter
Dim Pmt_requestJson As ADODB.Parameter
Dim Pmt_responseJson As ADODB.Parameter

On Error GoTo TrataErro
    Dim sdt As Date
    Dim edt As Date
    sdt = CDate(Format(StartDate, "dd-MM-yyyy hh:mm"))
    edt = CDate(Format(StartDate, "dd-MM-yyyy hh:mm"))
    
    If BL_DevolveModoLog = NONE Then
        Exit Sub
    End If
    
    With cmd
        .ActiveConnection = gConexao
        .CommandText = "slp_insere_log_rgpd"
        .CommandType = adCmdStoredProc
        
    End With

    Set Pmt_facility_id = cmd.CreateParameter("i_facility_id", adInteger, adParamInput, 0)
    Set Pmt_username = cmd.CreateParameter("i_username", adVarChar, adParamInput, 100)
    Set Pmt_machineName = cmd.CreateParameter("i_machinename", adVarChar, adParamInput, 2000)
    Set Pmt_OSUser = cmd.CreateParameter("i_osuser", adVarChar, adParamInput, 100)
    Set Pmt_audsid = cmd.CreateParameter("i_audsid", adVarChar, adParamInput, 100)
    Set Pmt_context = cmd.CreateParameter("i_context", adVarChar, adParamInput, 2000)
    Set Pmt_startdate = cmd.CreateParameter("i_startdate", adDBTimeStamp, adParamInput, 0)
    Set Pmt_enddate = cmd.CreateParameter("i_enddate", adDBTimeStamp, adParamInput, 0)
    Set Pmt_contextinfo = cmd.CreateParameter("i_contextinfo", adVarChar, adParamInput, 4000)
    Set Pmt_requestJson = cmd.CreateParameter("i_request", adLongVarChar, adParamInput, 2147483647)
    Set Pmt_responseJson = cmd.CreateParameter("i_response", adLongVarChar, adParamInput, 2147483647)
    'Set PmtRes = cmd.CreateParameter("vin", adVarChar, adParamOutput, 10)


    cmd.Parameters.Append Pmt_facility_id
    cmd.Parameters.Append Pmt_username
    cmd.Parameters.Append Pmt_machineName
    cmd.Parameters.Append Pmt_OSUser
    cmd.Parameters.Append Pmt_audsid
    cmd.Parameters.Append Pmt_context
    cmd.Parameters.Append Pmt_startdate
    cmd.Parameters.Append Pmt_enddate
    cmd.Parameters.Append Pmt_contextinfo
    cmd.Parameters.Append Pmt_requestJson
    cmd.Parameters.Append Pmt_responseJson
    'cmd.Parameters.Append PmtRes

    
    cmd.Parameters("i_facility_id").value = facility_id
    cmd.Parameters("i_username") = Username
    cmd.Parameters("i_machinename").value = machineName
    cmd.Parameters("i_osuser").value = osuser
    cmd.Parameters("i_audsid").value = audsid
    cmd.Parameters("i_context").value = context
    cmd.Parameters("i_startdate").value = sdt
    cmd.Parameters("i_enddate").value = edt
    cmd.Parameters("i_contextinfo").value = contextinfo
    cmd.Parameters("i_request").value = requestJson
    
    If BL_DevolveModoLog = REQUEST Then
        responseJson = "{}"
    End If
    
    cmd.Parameters("i_response").value = responseJson
   
     
    If cmd.Parameters("i_username").value = "" Or cmd.Parameters("i_context").value = "" Or cmd.Parameters("i_startdate").value = "" Then
        Exit Sub
    End If

    cmd.Execute
    
    Set Pmt_facility_id = Nothing
    Set Pmt_username = Nothing
    Set Pmt_machineName = Nothing
    Set Pmt_OSUser = Nothing
    Set Pmt_audsid = Nothing
    Set Pmt_context = Nothing
    Set Pmt_startdate = Nothing
    Set Pmt_enddate = Nothing
    Set Pmt_contextinfo = Nothing
    Set Pmt_requestJson = Nothing
    Set Pmt_responseJson = Nothing
    Set cmd = Nothing
     
  Exit Sub
  
TrataErro:
    BG_LogFile_Erros "BL_REGISTA_LOGS_RGPD - ERRO: " & Err.Description, "BL", "BL_REGISTA_LOGS_RGPD", True
    Set cmd = Nothing
    Exit Sub
    Resume Next
    
End Sub

'NELSONPSILVA Glintt-HS-18011 04.04.2018
 Public Function BL_DevolveTipOperecao(ByVal operacao As TipoOperacao) As String
    Dim sql As String
    Dim RsTipoper As ADODB.recordset

    On Error GoTo TrataErro

    sql = "SELECT desc_oper FROM sl_tbf_t_operacao WHERE cod_oper=" & operacao
    Set RsTipoper = New ADODB.recordset
    RsTipoper.CursorLocation = adUseClient
    RsTipoper.CursorType = adOpenStatic
    RsTipoper.Open sql, gConexao

    If RsTipoper.RecordCount > 0 Then
        BL_DevolveTipOperecao = BL_HandleNull(RsTipoper!desc_oper, "")
    End If

    RsTipoper.Close
    Set RsTipoper = Nothing
    
    Exit Function

TrataErro:
    BL_DevolveTipOperecao = ""
    BG_LogFile_Erros "BibliotecaLocal: BL_DevolveTipOperecao: " & Err.Description, "BL", "BL_DevolveTipOperecao"
    Exit Function
    Resume Next
End Function

'NELSONPSILVA Glintt-HS-18011 04.04.2018
Public Function BL_TrataCamposRGPD(ByRef Campos As Variant) As String
    'Dim arrCriterioPesq() As Object
    'ReDim arrCriterioPesq(0)
    'Dim index = arrCriterioPesq.Length - 1
    Dim sjson As String
    Dim i_sjson As String
    Dim f_sjson As String
    Dim ValorCampo As Variant
    Dim i As Integer
    i = 0
    sjson = vbNullString
    i_sjson = "{"
    f_sjson = "}"

    On Error GoTo TrataErro

    For Each ValorCampo In Campos
        'If TypeOf Control Is TextBox Then
        If TypeOf ValorCampo Is TextBox Or TypeOf ValorCampo Is MaskEdBox Or TypeOf ValorCampo Is RichTextBox Or TypeOf ValorCampo Is ComboBox Then
            'index = index + 1
            'ReDim Preserve arrCriterioPesq(index)
            ' arrCriterioPesq(index) = Control
            If ValorCampo.text <> vbNullString Then
                sjson = sjson & Chr(34) & CStr(ValorCampo.Name) & Chr(34) & ":" & Chr(34) & ValorCampo.text & Chr(34) & ", "
            End If
        End If
    Next
    sjson = Trim(sjson)
    '  sjson = sjson.Replace("\",

    sjson = left$(sjson, Len(sjson) - 1)
    sjson = i_sjson & sjson & f_sjson
    BL_TrataCamposRGPD = sjson
    
Exit Function
TrataErro:
    BL_TrataCamposRGPD = ""
    BG_LogFile_Erros "BibliotecaLocal: BL_TrataCamposRGPD: " & Err.Description, "BL", "BL_TrataCamposRGPD"
    Exit Function
    Resume Next
End Function

'NELSONPSILVA Glintt-HS-18011 04.04.2018
Public Function BL_TrataListBoxRGPD(ByRef Campos As Variant, NomeRecordset As ADODB.recordset, List As Boolean) As String
    'Dim arrCriterioPesq() As Object
    'ReDim arrCriterioPesq(0)
    'Dim index = arrCriterioPesq.Length - 1
    Dim sjson As String
    Dim i_sjson As String
    Dim f_sjson As String
    Dim ValorCampo As Variant
    Dim Record As New ADODB.recordset
    Dim i As Integer
    i = 0
    sjson = vbNullString
    i_sjson = "{"
    f_sjson = "}"

    On Error GoTo TrataErro
    
    Set Record = NomeRecordset.Clone
    'Record.Clone
    
    If Not Record Is Nothing Then
       Record.MoveFirst
    End If
    
    Do While Not Record.EOF
        If List = False Then
            For i = 0 To Record.Fields.Count - 1
                sjson = sjson & Chr(34) & Record.Fields(i).Name & Chr(34) & ":" & Chr(34) & Record.Fields(i).value & Chr(34) & ", "

            Next
            sjson = left$(sjson, Len(sjson) - 2)
            sjson = sjson & "}, {"
        Else
            For Each ValorCampo In Campos
                If Record(ValorCampo) <> vbNullString Then
                    sjson = sjson & Chr(34) & ValorCampo & Chr(34) & ":" & Chr(34) & Record(ValorCampo) & Chr(34) & ", "
                End If
            Next
            sjson = left$(sjson, Len(sjson) - 2)
            sjson = sjson & "}, {"
        End If
        Record.MoveNext
    Loop
    
'    If List = False Then
        sjson = Trim(sjson)
        sjson = left$(sjson, Len(sjson) - 3)
        sjson = i_sjson & sjson
'    Else
'        sjson = Trim(sjson)
'        sjson = left$(sjson, Len(sjson) - 1)
'        sjson = i_sjson & sjson & f_sjson
'    End If
    BL_TrataListBoxRGPD = sjson
    
Exit Function
TrataErro:
    BL_TrataListBoxRGPD = ""
    BG_LogFile_Erros "BibliotecaLocal: BL_TrataListBoxRGPD: " & Err.Description, "BL", "BL_TrataListBoxRGPD"
    Exit Function
    Resume Next
End Function
'edgar.parada Glintt-HS-17250 LOGIN GA_ACCESS
'edgar.parada HPP-49475 - Variavel o_user
Public Function BL_UsaGaAccess(ByVal i_Username As String, ByVal i_Password As String, ByRef exception As Boolean, ByRef o_user As String) As String

Dim cmd As New ADODB.Command
Dim Pmt_user As ADODB.Parameter
Dim Pmt_pass As ADODB.Parameter
Dim Pmt_user_aut As ADODB.Parameter
Dim Pmt_res As ADODB.Parameter


On Error GoTo TrataErro
 
    With cmd
        .ActiveConnection = gConexao
        .CommandText = "pck_sislab_external_login.login"
        .CommandType = adCmdStoredProc
    End With


    Set Pmt_user = cmd.CreateParameter("i_User", adVarChar, adParamInput, Len(i_Username))
    Set Pmt_pass = cmd.CreateParameter("i_Pass", adVarChar, adParamInput, Len(i_Password))
        Set Pmt_user_aut = cmd.CreateParameter("USER", adVarChar, adParamOutput, 2000)
    Set Pmt_res = cmd.CreateParameter("RES", adVarChar, adParamOutput, 2000)


    cmd.Parameters.Append Pmt_user
    cmd.Parameters.Append Pmt_pass
        cmd.Parameters.Append Pmt_user_aut
    cmd.Parameters.Append Pmt_res

    cmd.Parameters("i_User") = i_Username
    cmd.Parameters("i_Pass") = i_Password
     
    cmd.Execute
    
'    If cmd.Parameters.item("RES").value = "OK" Then
'        BL_UsaGaAccess = cmd.Parameters.item("RES").value
'        Exit Function
'    End If
     
  o_user = cmd.Parameters.Item("USER").value
  BL_UsaGaAccess = cmd.Parameters.Item("RES").value
  
  Set Pmt_user = Nothing
  Set Pmt_pass = Nothing
  Set Pmt_res = Nothing
  Set cmd = Nothing
  Exit Function
  
TrataErro:
    'BG_LogFile_Erros "BL_UsaGaAccess - ERRO: " & Err.Description, "BL", "BL_UsaGaAccess", True
    Set cmd = Nothing
    exception = True
    Exit Function
    Resume Next
End Function
'
'edgar.parada Glintt-HS-17250 LOGIN GA_ACCESS
Public Function BL_ValidaLogin(ByRef NumTentativas As Integer) As String

        If NumTentativas < 3 Then
            BL_ValidaLogin = "Entrada n�o validada !"
        Else
            BL_ValidaLogin = "Fim das tentativas permitidas!"
        End If
End Function

'NELSONPSILVA Glintt-HS-18011 STANDALONE
Public Function BL_RGPD_Stand(ByVal t_utente As String, ByVal Utente As String) As String

Dim cmd As New ADODB.Command
Dim Pmt_t_doente As ADODB.Parameter
Dim Pmt_doente As ADODB.Parameter
Dim Pmt_res As ADODB.Parameter


On Error GoTo TrataErro
 
    With cmd
        .ActiveConnection = gConexao
        .CommandText = "slp_direito_esquecimento"
        .CommandType = adCmdStoredProc
    End With


    Set Pmt_t_doente = cmd.CreateParameter("i_t_doente", adVarChar, adParamInput, Len(t_utente))
    Set Pmt_doente = cmd.CreateParameter("i_doente", adVarChar, adParamInput, Len(Utente))
    Set Pmt_res = cmd.CreateParameter("o_erro", adVarChar, adParamOutput, 2000)


    cmd.Parameters.Append Pmt_t_doente
    cmd.Parameters.Append Pmt_doente
    cmd.Parameters.Append Pmt_res

    cmd.Parameters("i_t_doente") = t_utente
    cmd.Parameters("i_doente") = Utente
     
    cmd.Execute
    
     
    BL_RGPD_Stand = cmd.Parameters.Item("o_erro").value
  
  Set Pmt_t_doente = Nothing
  Set Pmt_doente = Nothing
  Set Pmt_res = Nothing
  Set cmd = Nothing
  Exit Function
  
TrataErro:
    BG_LogFile_Erros "BL_RGPD_Stand - ERRO: " & Err.Description, "BL", "BL_RGPD_Stand", True
    Set cmd = Nothing
    Exit Function
    Resume Next
End Function


Public Sub BL_REGISTA_LOGS_RGPD_REPORTS(ByVal facility_id As Integer, ByVal Username As String, ByVal machineName As String, ByVal osuser As String, _
                                     ByVal audsid As String, ByVal context As String, ByVal StartDate As String, ByVal EndDate As String, ByVal contextinfo As String, _
                                     ByVal requestJson As String, ByVal responseJson As String)

Dim cmd As New ADODB.Command
Dim Pmt_facility_id As ADODB.Parameter
Dim Pmt_username As ADODB.Parameter
Dim Pmt_machineName As ADODB.Parameter
Dim Pmt_OSUser As ADODB.Parameter
Dim Pmt_audsid As ADODB.Parameter
Dim Pmt_context As ADODB.Parameter
Dim Pmt_startdate As ADODB.Parameter
Dim Pmt_enddate As ADODB.Parameter
Dim Pmt_contextinfo As ADODB.Parameter
Dim Pmt_requestJson As ADODB.Parameter
Dim Pmt_responseJson As ADODB.Parameter

On Error GoTo TrataErro
    Dim sdt As Date
    Dim edt As Date
    sdt = CDate(Format(StartDate, "dd-MM-yyyy hh:mm"))
    edt = CDate(Format(StartDate, "dd-MM-yyyy hh:mm"))
    
    With cmd
        .ActiveConnection = gConexao
        .CommandText = "slp_rgpd_repor_log_int"
        .CommandType = adCmdStoredProc
        
    End With

    Set Pmt_facility_id = cmd.CreateParameter("i_facility_id", adInteger, adParamInput, 0)
    Set Pmt_username = cmd.CreateParameter("i_username", adVarChar, adParamInput, 100)
    Set Pmt_machineName = cmd.CreateParameter("i_machinename", adVarChar, adParamInput, 2000)
    Set Pmt_OSUser = cmd.CreateParameter("i_osuser", adVarChar, adParamInput, 100)
    Set Pmt_audsid = cmd.CreateParameter("i_audsid", adVarChar, adParamInput, 100)
    Set Pmt_context = cmd.CreateParameter("i_context", adVarChar, adParamInput, 2000)
    Set Pmt_startdate = cmd.CreateParameter("i_startdate", adDBTimeStamp, adParamInput, 0)
    Set Pmt_enddate = cmd.CreateParameter("i_enddate", adDBTimeStamp, adParamInput, 0)
    Set Pmt_contextinfo = cmd.CreateParameter("i_contextinfo", adVarChar, adParamInput, 4000)
    Set Pmt_requestJson = cmd.CreateParameter("i_request", adLongVarChar, adParamInput, 2147483647)
    Set Pmt_responseJson = cmd.CreateParameter("i_response", adLongVarChar, adParamInput, 2147483647)
    'Set PmtRes = cmd.CreateParameter("vin", adVarChar, adParamOutput, 10)


    cmd.Parameters.Append Pmt_facility_id
    cmd.Parameters.Append Pmt_username
    cmd.Parameters.Append Pmt_machineName
    cmd.Parameters.Append Pmt_OSUser
    cmd.Parameters.Append Pmt_audsid
    cmd.Parameters.Append Pmt_context
    cmd.Parameters.Append Pmt_startdate
    cmd.Parameters.Append Pmt_enddate
    cmd.Parameters.Append Pmt_contextinfo
    cmd.Parameters.Append Pmt_requestJson
    cmd.Parameters.Append Pmt_responseJson
    'cmd.Parameters.Append PmtRes

    
    cmd.Parameters("i_facility_id").value = facility_id
    cmd.Parameters("i_username") = Username
    cmd.Parameters("i_machinename").value = machineName
    cmd.Parameters("i_osuser").value = osuser
    cmd.Parameters("i_audsid").value = audsid
    cmd.Parameters("i_context").value = context
    cmd.Parameters("i_startdate").value = sdt
    cmd.Parameters("i_enddate").value = edt
    cmd.Parameters("i_contextinfo").value = contextinfo
    cmd.Parameters("i_request").value = requestJson
    cmd.Parameters("i_response").value = responseJson
   
     
    If cmd.Parameters("i_username").value = "" Or cmd.Parameters("i_context").value = "" Or cmd.Parameters("i_startdate").value = "" Then
        Exit Sub
    End If

    cmd.Execute
    
    Set Pmt_facility_id = Nothing
    Set Pmt_username = Nothing
    Set Pmt_machineName = Nothing
    Set Pmt_OSUser = Nothing
    Set Pmt_audsid = Nothing
    Set Pmt_context = Nothing
    Set Pmt_startdate = Nothing
    Set Pmt_enddate = Nothing
    Set Pmt_contextinfo = Nothing
    Set Pmt_requestJson = Nothing
    Set Pmt_responseJson = Nothing
    Set cmd = Nothing
     
  Exit Sub
  
TrataErro:
    BG_LogFile_Erros "BL_REGISTA_LOGS_RGPD - ERRO: " & Err.Description, "BL", "BL_REGISTA_LOGS_RGPD", True
    Set cmd = Nothing
    Exit Sub
    Resume Next
    
End Sub


'BRUNODSANTOS glintt-hs-18011 - 21.06.2018
    Public Function BL_DevolveModoLog() As ModoLOG
        Dim cmd As New ADODB.Command
        Dim Pmt As ADODB.Parameter
        Dim PmtRes As ADODB.Parameter
        Dim Result As Integer

        On Error GoTo TrataErro

        With cmd
            .ActiveConnection = gConexao
            .CommandText = "slp_get_log_mode"
            .CommandType = ADODB.CommandTypeEnum.adCmdStoredProc
        End With
        
        Set Pmt = cmd.CreateParameter("i_aplicacao", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, Len(cSISLAB))
        Set PmtRes = cmd.CreateParameter("i_resp", ADODB.DataTypeEnum.adNumeric, ADODB.ParameterDirectionEnum.adParamOutput, 1)

        cmd.Parameters.Append Pmt
        cmd.Parameters.Append PmtRes

        cmd.Parameters("i_aplicacao").value = cSISLAB

        cmd.Execute
        Result = cmd.Parameters("i_resp").value

        Set cmd = Nothing
        Set Pmt = Nothing
        Set PmtRes = Nothing

        BL_DevolveModoLog = Result

        Exit Function

TrataErro:
        BG_LogFile_Erros "BL_DevolveModoLog ERRO : " & Err.Description
        BL_DevolveModoLog = ModoLOG.response
        Exit Function
        Resume Next
    End Function

'NELSONPSILVA CHVNG-7461 29.10.2018
Public Function PesqNomeMedico(cod_med As String) As String
    
    Dim rsCodigo As ADODB.recordset
    Dim sql As String
    
    If cod_med <> "" Then
        Set rsCodigo = New ADODB.recordset
        sql = "SELECT cod_med,nome_med FROM sl_medicos WHERE cod_med = " & cod_med & ""
        
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount > 0 Then
            PesqNomeMedico = Trim(rsCodigo!nome_med)
        Else
            PesqNomeMedico = ""
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Function

'NELSONPSILVA CHVNG-7461 29.10.2018
' DADO UMA REQUISI��O VERIFICA SE EST� PENDENTE DE REUTILIZA��O

' -----------------------------------------------------------------------------------------------
Public Function Bl_VerificaEstadoPendente(n_req As String) As Boolean
    Dim sSql As String
    Dim rs As New ADODB.recordset
    
    On Error GoTo TrataErro
    sSql = "SELECT * FROM sl_req_colheitas where n_Req = " & n_req & " AND flg_reutil = 1 AND nvl(estado_reutil,0) = 0 "
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
        If rs.RecordCount > 0 Then
            Bl_VerificaEstadoPendente = True
        Else
            Bl_VerificaEstadoPendente = ""
        End If
    rs.Close
    Set rs = Nothing
          
Exit Function
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "Bl_VerificaEstadoPendente", Err.Number, Err.Description, ""
    Bl_VerificaEstadoPendente = False
    Exit Function
    Resume Next
End Function


'NELSONPSILVA CHVNG-7461 29.10.2018
Public Function BL_TrataReutilizacao(ByVal n_req As Long, ByVal n_prescricao As Long, ByVal Estado As Integer, ByVal data As Date, ByVal hora As String, _
                                ByVal user As String, ByVal obs As String, ByVal Motivo As Long, ByVal cod_local As Integer, ByVal cod_colheita As Long) As trataResultado
                                
    Dim cmd As New ADODB.Command
    Dim Pmt_req As ADODB.Parameter
    Dim Pmt_pres As ADODB.Parameter
    Dim Pmt_estado As ADODB.Parameter
    Dim Pmt_data As ADODB.Parameter
    Dim Pmt_hora As ADODB.Parameter
    Dim Pmt_user As ADODB.Parameter
    Dim Pmt_obs As ADODB.Parameter
    Dim Pmt_motivo As ADODB.Parameter
    Dim Pmt_local As ADODB.Parameter
    Dim Pmt_colheita As ADODB.Parameter
    Dim PmtRes As ADODB.Parameter
    Dim PmtResReq As ADODB.Parameter
    Dim PmtResColheita As ADODB.Parameter
    Dim PmtResPresc As ADODB.Parameter
    
    Dim res As trataResultado

    On Error GoTo TrataErro

    With cmd
        .ActiveConnection = gConexao
        .CommandText = "pck_sl_reutil_colheitas.trata_reutilizacao"
        .CommandType = ADODB.CommandTypeEnum.adCmdStoredProc
    End With
    
    Set Pmt_req = cmd.CreateParameter("i_n_req", ADODB.DataTypeEnum.adVarNumeric, ADODB.ParameterDirectionEnum.adParamInput, 0)
    Set Pmt_pres = cmd.CreateParameter("i_n_prescricao", ADODB.DataTypeEnum.adVarNumeric, ADODB.ParameterDirectionEnum.adParamInput, 0)
    Set Pmt_estado = cmd.CreateParameter("i_estado", ADODB.DataTypeEnum.adVarNumeric, ADODB.ParameterDirectionEnum.adParamInput, 0)
    Set Pmt_data = cmd.CreateParameter("i_data", ADODB.DataTypeEnum.adDBDate, ADODB.ParameterDirectionEnum.adParamInput, 0)
    Set Pmt_hora = cmd.CreateParameter("i_hora", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, Len(hora))
    Set Pmt_user = cmd.CreateParameter("i_user", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, Len(user))
    
    Set Pmt_obs = cmd.CreateParameter("i_obs", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, 4000)
    Set Pmt_motivo = cmd.CreateParameter("i_motivo", ADODB.DataTypeEnum.adVarNumeric, ADODB.ParameterDirectionEnum.adParamInput, 0)

    Set Pmt_local = cmd.CreateParameter("i_local", ADODB.DataTypeEnum.adVarNumeric, ADODB.ParameterDirectionEnum.adParamInput, 0)
    Set Pmt_colheita = cmd.CreateParameter("i_colheita", ADODB.DataTypeEnum.adVarNumeric, ADODB.ParameterDirectionEnum.adParamInput, 0)
    
    Set PmtRes = cmd.CreateParameter("i_resp", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamOutput, 4000)
    Set PmtResReq = cmd.CreateParameter("i_resp_req", ADODB.DataTypeEnum.adNumeric, ADODB.ParameterDirectionEnum.adParamOutput, 1000)
    Set PmtResColheita = cmd.CreateParameter("i_resp_colh", ADODB.DataTypeEnum.adNumeric, ADODB.ParameterDirectionEnum.adParamOutput, 1000)
    Set PmtResPresc = cmd.CreateParameter("i_resp_presc", ADODB.DataTypeEnum.adNumeric, ADODB.ParameterDirectionEnum.adParamOutput, 1000)

    cmd.Parameters.Append Pmt_req
    cmd.Parameters.Append Pmt_pres
    cmd.Parameters.Append Pmt_estado
    cmd.Parameters.Append Pmt_data
    cmd.Parameters.Append Pmt_hora
    cmd.Parameters.Append Pmt_user
    cmd.Parameters.Append Pmt_obs
    cmd.Parameters.Append Pmt_motivo
    cmd.Parameters.Append Pmt_local
    cmd.Parameters.Append Pmt_colheita
    
    cmd.Parameters.Append PmtRes
    cmd.Parameters.Append PmtResReq
    cmd.Parameters.Append PmtResColheita
    cmd.Parameters.Append PmtResPresc
   
    cmd.Parameters("i_n_req").value = n_req
    cmd.Parameters("i_n_prescricao").value = n_prescricao
    cmd.Parameters("i_estado").value = Estado
    cmd.Parameters("i_data").value = data
    cmd.Parameters("i_hora").value = hora
    cmd.Parameters("i_user").value = user

    cmd.Parameters("i_obs").value = obs
    cmd.Parameters("i_motivo").value = Motivo
    cmd.Parameters("i_local").value = cod_local
    cmd.Parameters("i_colheita").value = cod_colheita

    
    cmd.Execute
    
    res.resultado = cmd.Parameters.Item("i_resp").value
    res.n_req = BL_HandleNull(cmd.Parameters.Item("i_resp_req").value, "")
    res.n_colheita = BL_HandleNull(cmd.Parameters.Item("i_resp_colh").value, "")
    res.n_pres = BL_HandleNull(cmd.Parameters.Item("i_resp_presc").value, "")

    
    BL_TrataReutilizacao = res

    Set cmd = Nothing
    Set Pmt_req = Nothing
    Set Pmt_pres = Nothing
    Set Pmt_estado = Nothing
    Set Pmt_data = Nothing
    Set Pmt_hora = Nothing
    Set Pmt_user = Nothing
    Set Pmt_obs = Nothing
    Set Pmt_motivo = Nothing
    Set Pmt_local = Nothing
    Set Pmt_colheita = Nothing
    
    Set PmtRes = Nothing
    Set PmtResReq = Nothing
    Set PmtResColheita = Nothing
    Set PmtResPresc = Nothing
    

    Exit Function

TrataErro:
        BG_LogFile_Erros "BL_TrataReutilizacao ERRO : " & Err.Description
        Set cmd = Nothing
        Exit Function
        Resume Next
End Function
'edgar.parada BACKLOG -11940 (COLHEITAS) 24.01.2019
Function BL_Executa_Procedimento_Alertas(ByVal ignorar_verificacao As Boolean) As String
     
     Dim p_ignorar_verificacao As Integer
     Dim CmdGera As New ADODB.Command

     On Error GoTo TrataErro
     
     Set CmdGera = New ADODB.Command
     
     p_ignorar_verificacao = IIf(ignorar_verificacao, 1, 0)
     
     With CmdGera
         .ActiveConnection = gConexao
         .CommandText = "pck_alertas.sislab_verifica_alertas"
         .CommandType = adCmdStoredProc
         .Parameters.Append .CreateParameter("p_cod_local", adNumeric, adParamInput, Len(gCodLocal), gCodLocal)
         .Parameters.Append .CreateParameter("p_ignorar_verificacao", adNumeric, adParamInput, Len(p_ignorar_verificacao), p_ignorar_verificacao)
         .Parameters.Append .CreateParameter("p_erro", adVarChar, adParamOutput, 4000)
     End With
     
     CmdGera.Execute
    
     BL_Executa_Procedimento_Alertas = BL_HandleNull(CmdGera.Parameters.Item("p_erro").value, "")


GoTo fim

TrataErro:
    BL_Executa_Procedimento_Alertas = "Erro ao Verificar Alertas."
    BG_LogFile_Erros "BL_Executa_Procedimento_Alertas - ERRO: " & Err.Description

fim:
        Exit Function
        Resume Next
End Function
'
'edgar.parada BACKLOG -11940 (COLHEITAS) 24.01.2019
Public Function BL_Devolve_Existem_Alertas_Activos() As alertas
        On Error GoTo TrataErro
        Dim sql As String
        Dim rs As New ADODB.recordset
        Dim rsUser As New ADODB.recordset
        Dim al As alertas
        Dim codigo As Integer
        
        eAlertas.existem_alertas = False
        eAlertas.existem_alertas_por_visualizar = False

        rsUser.CursorType = adOpenStatic
        rsUser.CursorLocation = ADODB.CursorLocationEnum.adUseClient

        sql = "SELECT * FROM sl_alertas_globais WHERE dt_fim IS NULL AND cod_local = " & gCodLocal
        rs.CursorType = adOpenStatic
        rs.CursorLocation = adUseClient
        rs.LockType = adLockReadOnly
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rs.Open sql, gConexao
        While Not rs.EOF
            eAlertas.existem_alertas = True
            codigo = BL_HandleNull(rs.Fields("codigo").value, -1)
            sql = "SELECT * FROM sl_alertas_globais_vistos WHERE codigo = " & codigo & " AND cod_utilizador = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & " AND cod_local = " & gCodLocal
            rsUser.Open sql, gConexao
            If rsUser.RecordCount = 0 Then
                eAlertas.existem_alertas_por_visualizar = True
            End If
            rsUser.Close
            rs.MoveNext
        Wend
        rs.Close
        BL_Devolve_Existem_Alertas_Activos = eAlertas
        Exit Function
TrataErro:
        BL_Devolve_Existem_Alertas_Activos = eAlertas
        BG_LogFile_Erros "BL_Devolve_Existem_Alertas_Activos - ERRO: " & Err.Description
        Exit Function
        Resume Next
End Function
'
Public Function BL_DevolveNumCopiasRecibo(ByVal cod_efr As String) As String
    Dim sSql As String
    Dim rs As New ADODB.recordset
    
    On Error GoTo TrataErro
    BL_DevolveNumCopiasRecibo = 0
    sSql = "SELECT num_copias_rec FROM sl_efr where cod_efr = " & cod_efr & ""
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.LockType = adLockReadOnly
    rs.Open sSql, gConexao
        If rs.RecordCount > 0 Then
            BL_DevolveNumCopiasRecibo = BL_HandleNull(rs!num_copias_rec, 0)
        End If
    rs.Close
    Set rs = Nothing
Exit Function
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "Bl_DevolveNumCopiasRecibo", Err.Number, Err.Description, ""
    BL_DevolveNumCopiasRecibo = 0
    Exit Function
    Resume Next
End Function

Public Function BL_EstadoRequis(ByVal n_req As String) As String

    Dim sSql As String
    Dim RsEstado As New ADODB.recordset

    On Error GoTo TrataErro
    
    sSql = " SELECT estado_req FROM sl_requis WHERE n_req = " & n_req & ""
    
    RsEstado.CursorLocation = adUseClient
    RsEstado.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEstado.Open sSql, gConexao
    If RsEstado.RecordCount = 1 Then
        BL_EstadoRequis = BL_HandleNull(RsEstado!estado_req, "")
    Else
        BL_EstadoRequis = ""
    End If
    RsEstado.Close
    Set RsEstado = Nothing
Exit Function
TrataErro:
    BL_LogFile_BD "BibliotecaLocal", "BL_EstadoRequis", Err.Number, Err.Description, ""
    Exit Function
    Resume Next
End Function

'edgar.parada CHSJ-4371 04.09.2019
Public Function BL_ValidaNumberDbl(ByVal v_String As String) As Boolean

    Dim v_number As Double

    On Error GoTo TrataErro
    
    v_number = CDbl(Replace(v_String, ".", ","))
    
    BL_ValidaNumberDbl = True
Exit Function
TrataErro:
    BL_LogFile_BD "BL_ValidaNumber", "N�o � number (double)", Err.Number, Err.Description, ""
    BL_ValidaNumberDbl = False
    Exit Function
    Resume Next
End Function

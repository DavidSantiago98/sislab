VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormEnvioEmail 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEnvioEmail"
   ClientHeight    =   16575
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   14250
   Icon            =   "FormEnvioEmail.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   16575
   ScaleWidth      =   14250
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkApenasImprimirSeleccionada 
      Caption         =   "Apenas Enviar Origem Seleccionada"
      Height          =   255
      Left            =   9960
      TabIndex        =   46
      Top             =   7920
      Width           =   3135
   End
   Begin VB.CheckBox CkImpressas 
      Caption         =   "Incluir Requisi��es J� Impressas"
      Height          =   255
      Left            =   6720
      TabIndex        =   45
      Top             =   7920
      Width           =   3135
   End
   Begin VB.CheckBox CkMarcarImpressas 
      Caption         =   "Marcar Requisi��es Como Impressas"
      Height          =   375
      Left            =   6720
      TabIndex        =   44
      Top             =   8160
      Width           =   3015
   End
   Begin VB.ComboBox CbTipo 
      Height          =   315
      Left            =   1560
      Style           =   2  'Dropdown List
      TabIndex        =   41
      Top             =   1800
      Width           =   3015
   End
   Begin VB.Frame FrameDir 
      Height          =   6255
      Left            =   6000
      TabIndex        =   32
      Top             =   9840
      Width           =   8175
      Begin VB.CommandButton BtCancDir 
         Height          =   735
         Left            =   5280
         Picture         =   "FormEnvioEmail.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "Cancelar Envio"
         Top             =   3720
         Width           =   1335
      End
      Begin VB.CommandButton BtSave 
         Height          =   735
         Left            =   3840
         Picture         =   "FormEnvioEmail.frx":0316
         Style           =   1  'Graphical
         TabIndex        =   35
         ToolTipText     =   "Salvar na Directoria Existente"
         Top             =   3720
         Width           =   1335
      End
      Begin VB.TextBox EcDirectoria 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   120
         TabIndex        =   34
         Top             =   3240
         Width           =   7935
      End
      Begin VB.DirListBox Dir 
         Appearance      =   0  'Flat
         Height          =   2790
         Left            =   120
         TabIndex        =   33
         Top             =   360
         Width           =   7935
      End
   End
   Begin VB.TextBox EcAux 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4560
      TabIndex        =   31
      Top             =   9120
      Width           =   1815
   End
   Begin MSFlexGridLib.MSFlexGrid FgReq 
      Height          =   5295
      Left            =   6120
      TabIndex        =   29
      Top             =   2400
      Width           =   7935
      _ExtentX        =   13996
      _ExtentY        =   9340
      _Version        =   393216
      BackColorBkg    =   -2147483644
      BorderStyle     =   0
   End
   Begin VB.Frame Frame2 
      Caption         =   "Pesquisa"
      Height          =   2295
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   14175
      Begin VB.CheckBox CkReenviar 
         Caption         =   "Reenviar Relat�rios"
         Height          =   375
         Left            =   4680
         TabIndex        =   43
         Top             =   1800
         Width           =   2415
      End
      Begin VB.TextBox EcCodLocalEntrega 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8160
         TabIndex        =   39
         Top             =   1320
         Width           =   975
      End
      Begin VB.TextBox EcDescrLocalEntrega 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9120
         Locked          =   -1  'True
         TabIndex        =   38
         TabStop         =   0   'False
         Top             =   1320
         Width           =   4335
      End
      Begin VB.CommandButton BtPesquisaLocalEntrega 
         Height          =   315
         Left            =   13440
         Picture         =   "FormEnvioEmail.frx":0FE0
         Style           =   1  'Graphical
         TabIndex        =   37
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   1320
         Width           =   375
      End
      Begin VB.OptionButton OptCompletos 
         Caption         =   "Resultados Incompletos"
         Height          =   255
         Index           =   1
         Left            =   11280
         TabIndex        =   8
         Top             =   1800
         Width           =   2175
      End
      Begin VB.OptionButton OptCompletos 
         Caption         =   "Resultados Completos"
         Height          =   255
         Index           =   0
         Left            =   8880
         TabIndex        =   7
         Top             =   1800
         Width           =   1935
      End
      Begin VB.TextBox EcNumReq 
         Appearance      =   0  'Flat
         BackColor       =   &H0080FFFF&
         Height          =   285
         Left            =   3480
         TabIndex        =   1
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox EcDataFinal 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8160
         TabIndex        =   3
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox EcSeqImpr 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1560
         TabIndex        =   0
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox EcDataInicial 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5640
         TabIndex        =   2
         Top             =   360
         Width           =   975
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   10080
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   360
         Width           =   1335
      End
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   12360
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   360
         Width           =   1335
      End
      Begin VB.TextBox EcCodSala 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1560
         TabIndex        =   5
         Top             =   1320
         Width           =   975
      End
      Begin VB.TextBox EcDescrSala 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   1320
         Width           =   3975
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   6480
         Picture         =   "FormEnvioEmail.frx":156A
         Style           =   1  'Graphical
         TabIndex        =   18
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1320
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   6480
         Picture         =   "FormEnvioEmail.frx":1AF4
         Style           =   1  'Graphical
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   840
         Width           =   375
      End
      Begin VB.TextBox EcDescrProveniencia 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   840
         Width           =   3975
      End
      Begin VB.TextBox EcCodProveniencia 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1560
         TabIndex        =   4
         Top             =   840
         Width           =   975
      End
      Begin VB.TextBox EcCodMedico 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   8160
         TabIndex        =   6
         Top             =   840
         Width           =   975
      End
      Begin VB.TextBox EcDescrMedico 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   9120
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   840
         Width           =   4335
      End
      Begin VB.CommandButton BtPesquisaMedico 
         Height          =   315
         Left            =   13440
         Picture         =   "FormEnvioEmail.frx":207E
         Style           =   1  'Graphical
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   840
         Width           =   375
      End
      Begin VB.Label Label2 
         Caption         =   "Tipo Envio"
         Height          =   255
         Left            =   120
         TabIndex        =   42
         Top             =   1800
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "Local Entrega"
         Height          =   255
         Index           =   1
         Left            =   7080
         TabIndex        =   40
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Requisi��o"
         Height          =   255
         Index           =   3
         Left            =   2640
         TabIndex        =   30
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Data Final"
         Height          =   255
         Index           =   2
         Left            =   7080
         TabIndex        =   27
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Seq Impress�o"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   26
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Data Inicial"
         Height          =   255
         Index           =   0
         Left            =   4680
         TabIndex        =   25
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   11520
         TabIndex        =   24
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label6 
         Caption         =   "Prioridade"
         Height          =   255
         Left            =   9240
         TabIndex        =   23
         Top             =   360
         Width           =   735
      End
      Begin VB.Label LbPosto 
         Caption         =   "Sala/Posto"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   1320
         Width           =   975
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "Medico"
         Height          =   255
         Left            =   7080
         TabIndex        =   20
         Top             =   840
         Width           =   975
      End
   End
   Begin VB.CommandButton BtEnviar 
      Height          =   735
      Left            =   13200
      Picture         =   "FormEnvioEmail.frx":2608
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Imprimir"
      Top             =   7920
      Width           =   855
   End
   Begin RichTextLib.RichTextBox RText 
      Height          =   375
      Left            =   1560
      TabIndex        =   12
      Top             =   9360
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"FormEnvioEmail.frx":32D2
   End
   Begin MSFlexGridLib.MSFlexGrid FgPesquisa 
      Height          =   6135
      Left            =   120
      TabIndex        =   28
      Top             =   2400
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   10821
      _Version        =   393216
      Rows            =   3
      Cols            =   3
      FixedCols       =   0
      BackColorBkg    =   -2147483644
      BorderStyle     =   0
   End
End
Attribute VB_Name = "FormEnvioEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
 
' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa


'Variavel que controla o cancelamento da impress�o
Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

' AGRUPAMENTOS DISPONIVEIS
Private Type config
    tipo As Integer
    Codigo As String
    nome As String
    email As String
    totalReq As Integer
    flg_html As Integer
    corpo_txt As String
    corpo_html As String
    descr_imagem_anexo As String
    flg_cc As Integer
    flg_recibo As Integer
End Type
Dim EstrutConfig() As config
Dim TotalConfig As Long

'  ESTRUTURA DE REQUISICOES
Private Type requisicoes
    n_req As String
    nome As String
    estado_req As String
    seq_utente As String
    dt_chega As String
    indice As Long
    flg_imprimir As Boolean
    ' pferreira 2010.08.04
    flg_assinatura As Boolean
    seq_impr As Long
    flg_enviado As Boolean
    iConfig As Integer
    'BRUNODSANTOS 05.098.2016 - LRV-388
    'flg_previsualizado As Boolean
    '
End Type

Dim EstrutReq() As requisicoes
Dim totalReq As Long

' TIPO DE PESQUISAS
Const lTipoPesqUtente = 3
Const lTipoPesqProven = 5
Const lTipoPesqMedico = 6
Const lTipoPesqPosto = 4
Const lTipoPesqLocalEntrega = 11


Const lColPesqTotal = 0
Const lColPesqNome = 2
Const lColPesqEmail = 3
Const lColPesqTipo = 1

Const lColReqNReq = 0
Const lColReqNomeUte = 1
Const lColReqDtChega = 2
Const lColReqImprimir = 3
Const lColReqIndice = 4

'BRUNODSANTOS 05.098.2016 - LRV-388
'Const lColReqPreVisualizar = 5
'

' INDICA QUAL A LINHA QUE ESTAMOS A TRATAR DA FgPesquisa
Dim indiceActivo As Long

Dim FLG_ExecutaFGPesquisa As Boolean

Const Verde = &HC0FFC0
Const vermelho = &HC0C0FF
Dim ProcuraRequis As Boolean


Private Sub BtCancDir_Click()
    FrameDir.Visible = False
    
End Sub

Private Sub BtEnviar_Click()
    Dim pastaEmail As String
    
    If gEscolhePastaEmail = mediSim Then
        InicializaDir
    Else
        If BL_FileExists(gDirCliente & "\eMail\") = False Then
            MkDir gDirCliente & "\eMail"
            FileCopy gDirServidor & "\bin\MapaResultadosSimples.rpt", gDirCliente & "\eMail\Teste"
        End If
        pastaEmail = gDirCliente & "\eMail\" & Bg_DaData_ADO & "_" & Replace(time, ":", "")
        If BL_FileExists(pastaEmail) = False Then
            MkDir pastaEmail
        End If
        ImprimeEmails pastaEmail
    End If
End Sub
Private Sub BtPesquisaProveniencia_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", "Pesquisar Proveniencias")
    
    mensagem = "N�o foi encontrada nenhuma proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.Text = resultados(1)
            EcDescrProveniencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
    
    EcCodProveniencia.SetFocus
    
End Sub

Private Sub BtSave_Click()
    Dim i As Long
    Dim j As Long
    Dim sSql As String
    Dim rsEmail As New ADODB.recordset
    Dim assunto As String
    Dim mensagem As String
    Dim ficheiros() As String
    On Error GoTo TrataErro
    'BtEnviar.Enabled = False
    
    Dim pastaEmail As String
    
    EcDirectoria_Validate False
    ReDim ficheiros(0)
    
    If BL_FileExists(gDirCliente & "\eMail\") = False Then
        MkDir gDirCliente & "\eMail"
        FileCopy gDirServidor & "\bin\MapaResultadosSimples.rpt", gDirCliente & "\eMail\Teste"
    End If
    pastaEmail = BL_HandleNull(EcDirectoria, gDirCliente & "\eMail\" & Bg_DaData_ADO & "_" & Replace(time, ":", ""))
    If BL_FileExists(pastaEmail) = False Then
        MkDir pastaEmail
    End If
    
    ImprimeEmails pastaEmail
    
Exit Sub
TrataErro:
If Err.Number = 75 Then
    Resume Next
End If
Exit Sub
Resume Next
    
End Sub

Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    BG_LimpaOpcao CbSituacao, KeyCode
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbUrgencia, KeyCode

End Sub
Private Sub Dir_Change()
    EcDirectoria = Dir.Path
End Sub

Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        FgPesquisa.TextMatrix(FgPesquisa.row, lColPesqEmail) = EcAux
        EstrutConfig(FgPesquisa.row).email = EcAux
        If EcAux <> "" Then
            FLG_ExecutaFGPesquisa = False
            BL_MudaCorFg FgPesquisa, FgPesquisa.row, vbWhite
            FLG_ExecutaFGPesquisa = True
        Else
            FLG_ExecutaFGPesquisa = False
            BL_MudaCorFg FgPesquisa, FgPesquisa.row, vermelho
            FLG_ExecutaFGPesquisa = True
        End If
        EcAux_LostFocus
    ElseIf KeyCode = vbKeyEscape Then
        EcAux_LostFocus
    End If
End Sub

Private Sub EcAux_LostFocus()
    FgPesquisa.SetFocus
    EcAux.Visible = False
    EcAux = ""
End Sub

Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodProveniencia.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_proven FROM sl_proven WHERE cod_proven='" & Trim(EcCodProveniencia.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodProveniencia.Text = ""
            EcDescrProveniencia.Text = ""
        Else
            EcDescrProveniencia.Text = Tabela!descr_proven
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub

Private Sub BtPesquisaMedico_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_med"
    CamposEcran(1) = "cod_med"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "nome_med"
    CamposEcran(2) = "nome_med"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_medicos"
    CampoPesquisa1 = "nome_med"
    
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", "Pesquisar M�dicos")
    
    mensagem = "N�o foi encontrada nenhum M�dico."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMedico.Text = resultados(1)
            EcDescrMedico.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
    
    EcCodMedico.SetFocus
    
End Sub




Private Sub EcDataInicial_GotFocus()
    If EcDataInicial.Text = "" Then
        EcDataInicial.Text = Format(Bg_DaData_ADO, gFormatoData)
    End If
    Set CampoActivo = Me.ActiveControl
End Sub
Private Sub EcDataFinal_GotFocus()
    If EcDataFinal.Text = "" Then
        EcDataFinal.Text = Format(Bg_DaData_ADO, gFormatoData)
    End If
    Set CampoActivo = Me.ActiveControl
End Sub
Private Sub EcDataInicial_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
End Sub
Private Sub EcDataFinal_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
End Sub
Private Sub EcCodMedico_Change()
    
    EcDescrMedico.Text = ""

End Sub

Private Sub EcCodMedico_Validate(Cancel As Boolean)
    
    Dim RsDescrGrAnalises As ADODB.recordset
       
    If Trim(EcCodMedico.Text) <> "" Then
        Set RsDescrGrAnalises = New ADODB.recordset
        
        With RsDescrGrAnalises
            .Source = "SELECT nome_med FROM sl_medicos WHERE cod_med= " & BL_TrataStringParaBD(EcCodMedico.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrAnalises.RecordCount <> 0 Then
            EcDescrMedico.Text = RsDescrGrAnalises!nome_med
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
        Else
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
            EcDescrMedico.Text = ""
            BG_Mensagem mediMsgBox, "O M�dico indicado n�o existe!", vbOKOnly, App.ProductName
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrMedico.Text = ""
    End If
    
End Sub




Private Sub DefTipoCampos()
    'Inicializar a barra de progresso
    
    'Tipo Data
    EcDataInicial.Tag = "104"
    EcDataFinal.Tag = "104"
    EcNumReq.Tag = adNumeric
    
    LbPosto.Visible = True
    EcCodSala.Visible = True
    EcDescrSala.Visible = True
    BtPesquisaSala.Visible = True
    
    With FgPesquisa
        .Cols = 4
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        
        .ColAlignment(lColPesqTotal) = flexAlignLeftCenter
        .ColWidth(lColPesqTotal) = 350
        .TextMatrix(0, lColPesqTotal) = "#"
        
        .ColAlignment(lColPesqTipo) = flexAlignLeftCenter
        .ColWidth(lColPesqTipo) = 1000
        .TextMatrix(0, lColPesqTipo) = "Tipo"
        
        .ColAlignment(lColPesqNome) = flexAlignLeftCenter
        .ColWidth(lColPesqNome) = 1750
        .TextMatrix(0, lColPesqNome) = "Nome"
        
        .ColAlignment(lColPesqEmail) = flexAlignLeftCenter
        .ColWidth(lColPesqEmail) = 2250
        .TextMatrix(0, lColPesqEmail) = "Email"
    
    End With
    
    With FgReq
        'BRUNODSANTOS 05.09.2016 - LRV-388
        .Cols = 5
        '.Cols = 6
        '
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        
        .ColAlignment(lColReqNReq) = flexAlignLeftCenter
        .ColWidth(lColReqNReq) = 900 '1000
        .TextMatrix(0, lColReqNReq) = "Requisi��o"
        
        .ColAlignment(lColReqNomeUte) = flexAlignLeftCenter
        .ColWidth(lColReqNomeUte) = 3900 '4000
        .TextMatrix(0, lColReqNomeUte) = "Nome"
        
        .ColAlignment(lColReqDtChega) = flexAlignLeftCenter
        .ColWidth(lColReqDtChega) = 1200 '1200
        .TextMatrix(0, lColReqDtChega) = "Data Chegada"
        
        .ColAlignment(lColReqImprimir) = flexAlignLeftCenter
        .ColWidth(lColReqImprimir) = 680 '1000
        .TextMatrix(0, lColReqImprimir) = "Enviar"
        
        .ColAlignment(lColReqIndice) = flexAlignLeftCenter
        .ColWidth(lColReqIndice) = 0
        .TextMatrix(0, lColReqIndice) = ""
        
'        'BRUNODSANTOS 05.09.2016 - LRV-388
'        .ColAlignment(lColReqPreVisualizar) = flexAlignLeftCenter
'        .ColWidth(lColReqPreVisualizar) = 1240
'        .TextMatrix(0, lColReqPreVisualizar) = "Pr�-Visualizar"
''        .Col = lColReqPreVisualizar
''        .ForeColor = &H8000000D
'        '
    End With
  
    
    TotalConfig = 0
    totalReq = 0
    ReDim EstrutConfig(0)
    ReDim EstrutReq(0)
    
    FLG_ExecutaFGPesquisa = False
    CkMarcarImpressas.value = 1
    CkImpressas.value = 1
    CkReenviar.value = vbGrayed
    FrameDir.Visible = False
    ProcuraRequis = False
End Sub


Private Sub EcCodsala_Validate(Cancel As Boolean)
    Cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
End Sub


Private Sub BtPesquisaSala_Click()
    PA_PesquisaSala EcCodSala, EcDescrSala, ""
    
End Sub

Private Sub EcDirectoria_Validate(Cancel As Boolean)
    Dim sStr As Variant
    Dim path_a As String
    Dim FSO
    Set FSO = CreateObject("Scripting.FileSystemObject")
    path_a = EcDirectoria
    
    If FSO.FolderExists(path_a) Then
        Dir.Path = EcDirectoria
    Else
        BG_Mensagem mediMsgBox, "Pasta indicada n�o existe", , "Erro"
        EcDirectoria = Dir.Path
        Exit Sub
    End If
End Sub

Private Sub FgPesquisa_Click()
    FgPesquisa_actualiza
End Sub


Private Sub FgPesquisa_DblClick()
    If FgPesquisa.Col = lColPesqEmail And FgPesquisa.row > 0 And FgPesquisa.row <= TotalConfig Then
        EcAux.top = FgPesquisa.top + FgPesquisa.CellTop
        EcAux.left = FgPesquisa.left + FgPesquisa.CellLeft
        EcAux.Text = FgPesquisa.TextMatrix(FgPesquisa.row, lColPesqEmail)
        EcAux.Width = FgPesquisa.CellWidth
        EcAux.Visible = True
        EcAux.SetFocus
    End If
End Sub

Private Sub FgPesquisa_RowColChange()
    If FLG_ExecutaFGPesquisa = True Then
        FgPesquisa_actualiza
    End If
End Sub

Private Sub FgPesquisa_actualiza()
    Dim i As Long
    Dim j As Long
    
    FgReq.rows = 2
    FgReq.TextMatrix(1, lColReqDtChega) = ""
    FgReq.TextMatrix(1, lColReqImprimir) = ""
    FgReq.TextMatrix(1, lColReqNomeUte) = ""
    FgReq.TextMatrix(1, lColReqNReq) = ""
    FgReq.TextMatrix(1, lColReqIndice) = ""
    'BRUNODSANTOS 05.09.2016 - LRV-388
'    If FgPesquisa.TextMatrix(FgPesquisa.RowSel, FgPesquisa.ColSel) <> "" Then
'        FgReq.TextMatrix(1, lColReqPreVisualizar) = "Pr�-Visualizar"
'    Else
'        FgReq.TextMatrix(1, lColReqPreVisualizar) = ""
'    End If
    
   
    If FgPesquisa.row > TotalConfig Then
        Exit Sub
    End If
    
    indiceActivo = FgPesquisa.row
    
    j = 1
    For i = 1 To totalReq
        If EstrutReq(i).indice = FgPesquisa.row Then
            FgReq.TextMatrix(j, lColReqDtChega) = EstrutReq(i).dt_chega
            FgReq.TextMatrix(j, lColReqNomeUte) = EstrutReq(i).nome
            FgReq.TextMatrix(j, lColReqNReq) = EstrutReq(i).n_req
            FgReq.TextMatrix(j, lColReqIndice) = j 'i BRUNODSANTOS 05.09.2016 - LRV-388 -> trocar i com j
            
            If EstrutReq(i).flg_imprimir = True Then
                FgReq.TextMatrix(j, lColReqImprimir) = "SIM"
            Else
                FgReq.TextMatrix(j, lColReqImprimir) = "N�O"
            End If
            j = j + 1
            FgReq.AddItem ""
        End If
    Next
    
     'BRUNODSANTOS 05.09.2016 - LRV-388
'     If FgReq.TextMatrix(FgReq.row, lColReqIndice) <> "" Then
'        If EstrutReq(FgReq.TextMatrix(FgReq.row, lColReqIndice)).flg_previsualizado = True Then
'            FlexGridColumnColor FgReq, lColReqPreVisualizar, &H400040
'        Else
'            FlexGridColumnColor FgReq, lColReqPreVisualizar, &H8000000D
'        End If
'    End If
    '
    
End Sub


 'BRUNODSANTOS 05.09.2016 - LRV-388
'Private Sub FgReq_Click()
'
'
'    If FgReq.ColSel = lColReqPreVisualizar And FgReq.TextMatrix(FgReq.RowSel, FgReq.ColSel) <> "" Then
'
'        If IR_ImprimeResultados(False, False, EstrutReq(FgReq.TextMatrix(FgReq.row, lColReqIndice)).n_req, EstrutReq(FgReq.TextMatrix(FgReq.row, lColReqIndice)).estado_req, _
'                                   EstrutReq(FgReq.TextMatrix(FgReq.row, lColReqIndice)).seq_utente, , , , True, , , , , , , False, False, , , , , , , True) = True Then
'
'            EstrutReq(FgReq.TextMatrix(FgReq.row, lColReqIndice)).flg_previsualizado = True
'
'        End If
'
'        If EstrutReq(FgReq.TextMatrix(FgReq.row, lColReqIndice)).flg_previsualizado = True Then
'            FlexGridColumnColor FgReq, lColReqPreVisualizar, &H400040
'        Else
'            FlexGridColumnColor FgReq, lColReqPreVisualizar, &H8000000D
'        End If
'
'    End If
'    Screen.MousePointer = vbDefault
'
'End Sub

Private Sub FgReq_DblClick()
    If FgReq.TextMatrix(FgReq.row, 0) <> "" Then
        If EstrutReq(FgReq.TextMatrix(FgReq.row, lColReqIndice)).flg_imprimir = True Then
            FgReq.TextMatrix(FgReq.row, lColReqImprimir) = "N�O"
            EstrutReq(FgReq.TextMatrix(FgReq.row, lColReqIndice)).flg_imprimir = False
        Else
            FgReq.TextMatrix(FgReq.row, lColReqImprimir) = "SIM"
            EstrutReq(FgReq.TextMatrix(FgReq.row, lColReqIndice)).flg_imprimir = True
        End If
    End If
    
    
End Sub

'BRUNODSANTOS 06.09.2016 - LRV-388
'Private Sub FgReq_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'    If FgReq.MouseCol = lColReqPreVisualizar And FgReq.TextMatrix(FgReq.MouseRow, FgReq.MouseCol) = "Pr�-Visualizar" Then
'        SetCursor LoadCursor(0, IDC_HAND)
'    Else
'        Screen.MousePointer = vbDefault
'    End If
'End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    DefTipoCampos
    'BRUNODSANTOS 05.09.2016 - LRV-388
    'FlexGridColumnColor FgReq, lColReqPreVisualizar, &H8000000D
    '
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Enviar Resultados por E-mail"
    
    Me.left = 5
    Me.top = 5
    Me.Width = 14340
    Me.Height = 8985 ' Normal
    
    Set CampoDeFocus = EcNumReq
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormEnvioEmail = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    EcCodMedico.Text = ""
    EcDescrMedico.Text = ""
    EcCodLocalEntrega.Text = ""
    EcDescrLocalEntrega.Text = ""
    EcCodProveniencia.Text = ""
    EcDescrProveniencia.Text = ""
    EcCodSala.Text = ""
    EcDescrSala.Text = ""
    EcDataInicial.Text = ""
    EcNumReq.Text = ""
    EcDataFinal.Text = ""
    EcDataInicial.Text = ""
    CbTipo.ListIndex = mediComboValorNull
    OptCompletos(0).value = True
    CbSituacao.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    
    TotalConfig = 0
    totalReq = 0
    ReDim EstrutConfig(0)
    ReDim EstrutReq(0)
    LimpaFgPesquisa
    FLG_ExecutaFGPesquisa = False
    EcAux.Visible = False
    EcAux = ""
    CkMarcarImpressas.value = 1
    CkReenviar.value = vbGrayed
    EcSeqImpr = ""
    EcNumReq.SetFocus
    FrameDir.Visible = False
    ProcuraRequis = False
    CkImpressas.value = 1
    
    'BRUNODSANTOS 05.09.2016 - LRV-388
    'LimpaFGReq
    '
End Sub
Private Sub LimpaFgPesquisa()
    Dim i As Long
    BL_MudaCorFg FgPesquisa, 1, vbWhite
    FgPesquisa.rows = 2
    FgPesquisa.TextMatrix(1, lColPesqTipo) = ""
    FgPesquisa.TextMatrix(1, lColPesqNome) = ""
    FgPesquisa.TextMatrix(1, lColPesqTotal) = ""
    FgPesquisa.TextMatrix(1, lColPesqEmail) = ""
    
End Sub
Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia
    BG_PreencheComboBD_ADO "SELECT cod_t_dest, descr_t_dest FROM sl_tbf_t_destino WHERE flg_email = 1 ", "cod_t_dest", "descr_t_dest", CbTipo

    OptCompletos(0).value = True
    If gCodSalaAssocUser <> 0 Then
        EcCodSala = gCodSalaAssocUser
        EcCodsala_Validate False
        EcCodSala.Enabled = False
        BtPesquisaSala.Enabled = False
    Else
        EcCodSala = ""
        EcCodsala_Validate True
        EcCodSala.Enabled = True
        BtPesquisaSala.Enabled = True
    End If
End Sub

Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsRequisicoes As New ADODB.recordset
    Dim i As Long
    Dim indice As Long
    
    TotalConfig = 0
    totalReq = 0
    ReDim EstrutConfig(0)
    ReDim EstrutReq(0)
    LimpaFgPesquisa
    If EcNumReq <> "" Then
        If IsNumeric(EcNumReq) = False Then
            Exit Sub
        End If
    End If
    sSql = ConstroiCriterio
    rsRequisicoes.CursorLocation = adUseServer
    rsRequisicoes.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsRequisicoes.Open sSql, gConexao
    
    If rsRequisicoes.RecordCount >= 0 Then
        If EcNumReq <> "" Then
            ProcuraRequis = True
        End If
        totalReq = 0
        ReDim EstrutReq(0)
        While Not rsRequisicoes.EOF
            Select Case BL_HandleNull(rsRequisicoes!cod_destino, "")
                Case lTipoPesqLocalEntrega
                    indice = PreencheEstrutConfig(BL_HandleNull(rsRequisicoes!cod_destino, ""), BL_HandleNull(rsRequisicoes!cod_local_entrega, ""), _
                                 BL_HandleNull(rsRequisicoes!descr_local_entrega, "Sem Local Entrega Atribuido"), BL_HandleNull(rsRequisicoes!email_entrega, ""))
                Case lTipoPesqMedico
                    indice = PreencheEstrutConfig(BL_HandleNull(rsRequisicoes!cod_destino, ""), BL_HandleNull(rsRequisicoes!cod_med, ""), _
                                 BL_HandleNull(rsRequisicoes!nome_med, "Sem M�dico Atribuido"), BL_HandleNull(rsRequisicoes!email_med, ""))
                Case lTipoPesqPosto
                    indice = PreencheEstrutConfig(BL_HandleNull(rsRequisicoes!cod_destino, ""), BL_HandleNull(rsRequisicoes!cod_sala, ""), _
                                 BL_HandleNull(rsRequisicoes!descr_sala, "Sem Posto Atribuido"), BL_HandleNull(rsRequisicoes!email_sala, ""))
                Case lTipoPesqProven
                    indice = PreencheEstrutConfig(BL_HandleNull(rsRequisicoes!cod_destino, ""), BL_HandleNull(rsRequisicoes!cod_proven, ""), _
                                 BL_HandleNull(rsRequisicoes!descr_proven, "Sem Proveni�ncia Atribuida"), BL_HandleNull(rsRequisicoes!email_proven, ""))
                Case lTipoPesqUtente
                    indice = PreencheEstrutConfig(BL_HandleNull(rsRequisicoes!cod_destino, ""), BL_HandleNull(rsRequisicoes!Utente, ""), _
                                 BL_HandleNull(rsRequisicoes!nome_ute, ""), BL_HandleNull(rsRequisicoes!email_ute, ""))
                Case Else
                    indice = PreencheEstrutConfig(mediComboValorNull, BL_HandleNull(rsRequisicoes!n_req, ""), _
                                 "Requisi��o", BL_HandleNull(rsRequisicoes!email_ute, ""))
                
            End Select
            totalReq = totalReq + 1
            ReDim Preserve EstrutReq(totalReq)
            EstrutReq(totalReq).dt_chega = BL_HandleNull(rsRequisicoes!dt_chega, "")
            EstrutReq(totalReq).n_req = BL_HandleNull(rsRequisicoes!n_req, "")
            EstrutReq(totalReq).nome = BL_HandleNull(rsRequisicoes!nome_ute, "")
            EstrutReq(totalReq).seq_utente = BL_HandleNull(rsRequisicoes!seq_utente, "")
            EstrutReq(totalReq).estado_req = BL_HandleNull(rsRequisicoes!estado_req, "")
            EstrutReq(totalReq).seq_impr = BL_HandleNull(EcSeqImpr, -1)
            EstrutReq(totalReq).flg_imprimir = True
            EstrutReq(totalReq).indice = indice
            EstrutReq(totalReq).flg_enviado = False
            ' pferreira 2010.08.04
            If (gAssinaturaActivo = mediSim) Then
                If (RequisicaoAssinada(EstrutReq(totalReq).n_req)) Then: EstrutReq(totalReq).flg_assinatura = True
            End If
            rsRequisicoes.MoveNext
        Wend
        FgPesquisa.row = 1
        FgPesquisa_Click
    Else
        BG_Mensagem mediMsgBeep, "N�o existem Resultados segundo os crit�rios"
    End If
    rsRequisicoes.Close
    Set rsRequisicoes = Nothing
    FLG_ExecutaFGPesquisa = True
    
    'BRUNODSANTOS 05.09.2016 - LRV-388
'    If FgReq.TextMatrix(FgReq.row, lColReqIndice) <> "" Then
'        If EstrutReq(FgReq.TextMatrix(FgReq.row, lColReqIndice)).flg_previsualizado = True Then
'            FlexGridColumnColor FgReq, lColReqPreVisualizar, &H400040
'        Else
'            FlexGridColumnColor FgReq, lColReqPreVisualizar, &H8000000D
'        End If
'    End If
    '
End Sub

' --------------------------------------------------------------------------------

' CONSTROI CRITERIO PARA DEVOLVER REQUISI��ES A ENVIAR POR EMAIL

' --------------------------------------------------------------------------------

Private Function ConstroiCriterio() As String
    Dim sSql As String
    
    ' ----------------------------------
    ' email do utilizador
    ' ----------------------------------
    sSql = "SELECT r.n_req, i.utente , r.dt_chega, r.estado_Req, i.nome_ute, i.seq_utente, r.cod_destino, i.email email_ute, "
    sSql = sSql & " p.cod_proven, p.descr_proven, p.email email_proven, s.cod_sala, s.descr_sala, s.email email_sala,m.cod_med, m.nome_med, "
    sSql = sSql & " m.email email_med, e.cod_local_entrega, e.descr_local_entrega, e.email email_entrega"
    sSql = sSql & " FROM sl_requis r LEFT OUTER JOIN sl_cod_salas s on r.cod_sala = s.cod_sala "
    sSql = sSql & " LEFT OUTER JOIN sl_proven p on r.cod_proven = p.cod_proven "
    sSql = sSql & " LEFT OUTER JOIN sl_medicos m on r.cod_med = m.cod_med "
    sSql = sSql & " LEFT OUTER JOIN sl_cod_local_entrega e on r.cod_local_entrega= e.cod_local_entrega "
    sSql = sSql & ", sl_identif i "
    sSql = sSql & " WHERE r.seq_utente = i.seq_utente "
    
    
    If gAssinaturaActivo = mediSim Then
        sSql = sSql & " AND r.n_req IN (SELECT n_req FROM sl_realiza re where re.n_req = r.n_req and flg_assinado = 1)"
    Else
        sSql = sSql & " AND ((r.cod_destino IN (SELECT cod_t_dest FROM sl_tbf_t_destino WHERE flg_usa_assinatura IS NULL or flg_usa_assinatura = 0) OR r.cod_destino IS NULL)"
        sSql = sSql & " OR (r.cod_destino IN (SELECT cod_t_dest FROM sl_tbf_t_destino WHERE flg_usa_assinatura  = 1) "
        sSql = sSql & " AND r.n_req in (SELECT n_req FROM sl_realiza re where re.n_req = r.n_req and flg_assinado = 1)))"
    End If
    
    ' SE E UMA IMPRESSAO NOVA O SEQ_IMPR  � VAZIO
    If EcSeqImpr <> "" Then
        CkReenviar.value = vbChecked
        sSql = sSql & " AND r.n_req  IN (Select n_req from sl_email_resultados WHERE seq_email = " & EcSeqImpr & ") "
    ElseIf CkReenviar.value = vbChecked Then
        sSql = sSql & " AND r.n_req  IN (Select n_req from sl_email_resultados )"
    End If
    If CbTipo.ListIndex > mediComboValorNull Then
        sSql = sSql & " AND r.cod_destino =" & CbTipo.ItemData(CbTipo.ListIndex)
    End If
    
    If EcNumReq.Text <> "" And CbTipo.ListIndex = mediComboValorNull Then
        sSql = sSql & " AND r.n_req = " & BL_TrataStringParaBD(EcNumReq.Text)
    Else
        ' SE FOI SELECCIONADO COMPLETOS, OU INCOMPLETOS
        If OptCompletos(1).value = True Then
            sSql = sSql & " AND r.estado_req in ('2','3') "
        Else
            If CkReenviar.value <> vbChecked Then
                sSql = sSql & " AND r.n_req IN (SELECT n_req  FROM sl_email_completos WHERE flg_estado = 0)"
            Else
                sSql = sSql & " AND r.estado_req in ('D'"
                If CkImpressas.value = vbChecked Then
                    sSql = sSql & ",'F'"
                End If
                sSql = sSql & ") "
            End If
            
        End If
    End If
    
    If EcNumReq.Text <> "" Then
        sSql = sSql & " AND r.n_req = " & BL_TrataStringParaBD(EcNumReq.Text)
    Else
        sSql = sSql & " AND r.cod_destino IN (SELECT cod_t_dest FROM sl_tbf_t_destino WHERE flg_email = 1)"
    End If
    
    If gApenasImprimeAssinadas = mediSim And OptCompletos(0).value = True Then
        sSql = sSql & " AND r.n_Req in (select sl_req_assinatura.n_req from sl_req_assinatura)"
    End If
        
    
    If EcDataInicial <> "" And EcDataFinal <> "" Then
        If CDate(EcDataFinal) >= CDate(EcDataInicial) Then
            sSql = sSql & " AND (r.dt_chega between " & BL_TrataDataParaBD(EcDataInicial) & " AND " & BL_TrataDataParaBD(EcDataFinal) & ") "
        End If
    End If
    
    If EcCodMedico.Text <> "" Then
        sSql = sSql & " AND r.cod_medico = " & BL_TrataStringParaBD(EcCodMedico.Text)
    End If
    
    If EcCodProveniencia.Text <> "" Then
        sSql = sSql & " AND r.cod_proven = " & BL_TrataStringParaBD(EcCodProveniencia.Text)
    End If
    
    If EcCodSala.Text <> "" Then
        sSql = sSql & " AND r.cod_sala = " & BL_TrataStringParaBD(EcCodSala.Text)
    End If
    
    If EcCodLocalEntrega.Text <> "" Then
        sSql = sSql & " AND r.cod_local_entrega = " & BL_TrataStringParaBD(EcCodLocalEntrega.Text)
    End If
    
    
    sSql = sSql & " ORDER BY n_req "
    ConstroiCriterio = sSql
End Function


Private Function PreencheEstrutConfig(tipo As String, Codigo As String, Designacao As String, email As String) As Long
    Dim i As Long
    
    For i = 1 To TotalConfig
        If EstrutConfig(i).Codigo = Codigo Then
            EstrutConfig(i).totalReq = EstrutConfig(i).totalReq + 1
            FgPesquisa.TextMatrix(i, lColPesqTotal) = EstrutConfig(i).totalReq
            PreencheEstrutConfig = i
            Exit Function
        End If
    Next
    
    TotalConfig = TotalConfig + 1
    ReDim Preserve EstrutConfig(TotalConfig)
    EstrutConfig(TotalConfig).Codigo = Codigo
    EstrutConfig(TotalConfig).email = email
    EstrutConfig(TotalConfig).nome = Designacao
    EstrutConfig(TotalConfig).tipo = tipo
    PreencheCorpoEmail (TotalConfig)
    
    EstrutConfig(TotalConfig).totalReq = 1
    PreencheEstrutConfig = TotalConfig
    
    Select Case tipo
        Case lTipoPesqLocalEntrega
            FgPesquisa.TextMatrix(TotalConfig, lColPesqTipo) = "Local Entr."
        Case lTipoPesqMedico
            FgPesquisa.TextMatrix(TotalConfig, lColPesqTipo) = "M�dico"
        Case lTipoPesqPosto
            FgPesquisa.TextMatrix(TotalConfig, lColPesqTipo) = "Posto"
        Case lTipoPesqProven
            FgPesquisa.TextMatrix(TotalConfig, lColPesqTipo) = "Proven."
        Case lTipoPesqUtente
            FgPesquisa.TextMatrix(TotalConfig, lColPesqTipo) = "Utente"
    End Select
    FgPesquisa.TextMatrix(TotalConfig, lColPesqTotal) = 1
    FgPesquisa.TextMatrix(TotalConfig, lColPesqNome) = Codigo & " - " & Designacao
    FgPesquisa.TextMatrix(TotalConfig, lColPesqEmail) = email
    If Trim(email) = "" Then
        BL_MudaCorFg FgPesquisa, TotalConfig, vermelho
    End If
    FgPesquisa.AddItem ""

End Function




Private Sub InicializaDir()
    Dir.Path = "C:\"
    FrameDir.top = 2280
    FrameDir.left = 6000
    FrameDir.Visible = True
End Sub

' pferreira 2010.08.04
Private Function RequisicaoAssinada(n_req As String) As Boolean
    
    Dim sSql As String
    Dim rsRequisicoes As ADODB.recordset
    
    On Error GoTo TrataErro
    Set rsRequisicoes = New ADODB.recordset
    sSql = "select * from sl_req_assinatura where n_req = " & n_req
    rsRequisicoes.CursorLocation = adUseServer
    rsRequisicoes.CursorType = adOpenStatic
    rsRequisicoes.Open sSql, gConexao
    RequisicaoAssinada = CBool(rsRequisicoes.RecordCount > Empty)
    If (rsRequisicoes.state = adStateOpen) Then: rsRequisicoes.Close
    Exit Function
    
TrataErro:
    Exit Function
    
End Function

Private Sub ImprimeEmails(pastaEmail As String)
    Dim i As Long
    Dim j As Long
    Dim k As Long
    Dim sSql As String
    Dim rsEmail As New ADODB.recordset
    Dim mensagem As String
    Dim ficheiros() As String
    Dim assunto As String
    Dim pdfjob As PDFCreator.clsPDFCreator
    Dim primeiroIndice As Integer
    Dim resEnvio As Boolean
    Dim dataActual As String
    Dim horaActual As String
    On Error GoTo TrataErro
    ReDim ficheiros(0)
    If EcSeqImpr = "" Then
        sSql = "SELECT max(seq_email) +1 maximo from sl_email_resultados "
        rsEmail.CursorLocation = adUseServer
        rsEmail.CursorType = adOpenStatic
        rsEmail.Open sSql, gConexao
        If rsEmail.RecordCount > 0 Then
            EcSeqImpr = BL_HandleNull(rsEmail!maximo, 0)
        Else
            BG_Mensagem mediMsgBox, "Erro ao gerar sequ�ncial.", vbCritical, " N�o � possivel enviar eMail."
            Exit Sub
        End If
        rsEmail.Close
    End If
    
    mensagem = ""
    If CkMarcarImpressas.value = vbChecked Then
        gImprimirDestino = crptToPrinter
    Else
        gImprimirDestino = crptToWindow
    End If
    For k = 1 To TotalConfig
        ReDim Preserve ficheiros(0)
        If (CkApenasImprimirSeleccionada.value = vbChecked And k = FgPesquisa.row) Or CkApenasImprimirSeleccionada.value <> vbChecked Then
            For i = 1 To totalReq
            
                ' pferreira 2010.08.04
                If gAssinaturaActivo <> mediSim Or (gAssinaturaActivo = mediSim And (EstrutReq(i).flg_assinatura Or OptCompletos(1).value = True)) Then
                
                    ' SE REQUISI��O PERTENCE AO INDICE INDICADO E SE ESTA MARCADO PARA IMPRIMIR
                    If EstrutReq(i).indice = k And EstrutReq(i).flg_imprimir = True And EstrutConfig(k).email <> "" Then
                        primeiroIndice = i
                        ' COPIA O RPT
                        If gUsaMesmoRelatorioEmail <> mediSim Then
                            BL_CopiaRelatorioEmail "MapaResultadosSimplesEmail.rpt", EstrutReq(i).n_req & ".rpt"
                        Else
                            BL_CopiaRelatorioEmail "MapaResultadosSimples.rpt", EstrutReq(i).n_req & ".rpt"
                        End If
                        ' IMPRIME O PDF
                        
                        EMAIL_IniciaPDFCreator pdfjob, gDirCliente & "\eMail\", EstrutReq(i).n_req & "", "", ""
                        
                        If IR_ImprimeResultados(False, False, EstrutReq(i).n_req, EstrutReq(i).estado_req, EstrutReq(i).seq_utente, , , , True, , , , , , , False, True) = True Then
                            EMAIL_FechaPdfCreator pdfjob
                            
                            j = 0
                            While BL_IsFileDistilledYet(EstrutReq(i).n_req & ".pdf", gDirCliente & "\eMail\") = False And j < 10
                                Sleep 1000
                                j = j + 1
                            Wend
                            If j < 10 Then
                                If BL_FileExists(gDirCliente & "\eMail\" & EstrutReq(i).n_req & ".pdf") Then
                                    If BL_FileExists(pastaEmail & "\" & EstrutReq(i).n_req & ".pdf") Then
                                        FileCopy pastaEmail & "\" & EstrutReq(i).n_req & ".pdf", pastaEmail & "\" & EstrutReq(i).n_req & "_" & Bg_DaData_ADO & "_" & Replace(Bg_DaHora_ADO, ":", "") & ".pdf"
                                        Kill pastaEmail & "\" & EstrutReq(i).n_req & ".pdf"
                                    End If
                                    FileCopy gDirCliente & "\eMail\" & EstrutReq(i).n_req & ".pdf", pastaEmail & "\" & EstrutReq(i).n_req & ".pdf"
                                    Kill gDirCliente & "\eMail\" & EstrutReq(i).n_req & ".pdf"
                                    EstrutReq(i).flg_enviado = True
                                End If
    
                            End If
                            ReDim Preserve ficheiros(UBound(ficheiros) + 1)
                            ficheiros(UBound(ficheiros)) = EstrutReq(i).n_req & ".pdf"
                        Else
                            EMAIL_FechaPdfCreator pdfjob
                            BG_Mensagem mediMsgBox, "Erro ao gerar PDF.", vbCritical, " N�o � possivel enviar eMail."
                            
                        End If
                    End If
                End If
            Next
            If UBound(ficheiros) = 1 And ProcuraRequis = False Then
                assunto = "Relat�rio(s) de An�lises "
                assunto = "Relat�rio de An�lises para" & " - " & EstrutConfig(k).nome
            ElseIf UBound(ficheiros) = 1 And ProcuraRequis = True Then
                assunto = "Relat�rio(s) de An�lises "
                assunto = "Relat�rio de An�lises para" & " - " & EstrutReq(primeiroIndice).nome
            ElseIf UBound(ficheiros) > 1 Then
                assunto = "Relat�rios de An�lises "
            End If
            If EstrutConfig(k).flg_html = mediNao Then
                mensagem = BL_HandleNull(EstrutConfig(k).corpo_txt, "") & vbCrLf
                For i = 1 To totalReq
                    If EstrutReq(i).indice = k And EstrutReq(i).flg_imprimir = True And EstrutConfig(k).email <> "" Then
                        mensagem = mensagem & "Relat�rio de An�lises de " & " - " & EstrutReq(i).nome & " - " & EstrutReq(i).n_req & vbCrLf
                    End If
                Next
            Else
                mensagem = BL_HandleNull(EstrutConfig(k).corpo_html, "")
            End If
            
            If UBound(ficheiros) > 0 Then
                If EstrutConfig(k).flg_html = mediSim And EstrutConfig(k).descr_imagem_anexo <> "" Then
                    resEnvio = BL_EnviaEmail(EstrutConfig(k).email, assunto, mensagem, True, pastaEmail, ficheiros, EstrutConfig(k).flg_html, gDirCliente & "\" & EstrutConfig(k).descr_imagem_anexo, EstrutConfig(k).descr_imagem_anexo, EstrutConfig(k).flg_recibo, EstrutConfig(k).flg_cc, "RESULTADOS", "")
                Else
                    resEnvio = BL_EnviaEmail(EstrutConfig(k).email, assunto, mensagem, True, pastaEmail, ficheiros, EstrutConfig(k).flg_html, "", "", EstrutConfig(k).flg_recibo, EstrutConfig(k).flg_cc, "RESULTADOS", "")
                End If
            End If
            For i = 1 To totalReq
                If resEnvio = True And EstrutReq(i).flg_enviado = True Then
                    dataActual = Bg_DaData_ADO
                    horaActual = Bg_DaHora_ADO
                    
                    ' INSERE REGISTO DO ENVIO NA TABELA
                    sSql = "INSERT INTO sl_email_resultados (seq_email,n_req,agrega, destino,assunto,mensagem, localizacao_anexo,estado,user_cri,dt_cri, hr_cri,destino_impr,estado_Req) VALUES ("
                    sSql = sSql & EcSeqImpr & ", " & EstrutReq(i).n_req & ",0, " & BL_TrataStringParaBD(EstrutConfig(k).email) & "," & BL_TrataStringParaBD(assunto) & "," & BL_TrataStringParaBD(Mid(mensagem, 1, 4000)) & ", "
                    sSql = sSql & BL_TrataStringParaBD(pastaEmail & "\" & EstrutReq(i).n_req & ".pdf") & ", 0, " & BL_TrataStringParaBD(CStr(gCodUtilizador))
                    sSql = sSql & ", " & BL_TrataDataParaBD(dataActual) & ", " & BL_TrataStringParaBD(horaActual) & "," & gImprimirDestino & "," & BL_TrataStringParaBD(EstrutReq(i).estado_req) & ")"
                    BG_ExecutaQuery_ADO sSql
                    EstrutReq(i).flg_enviado = False
                    
                    sSql = "UPDATE sl_email_completos SET flg_estado = 1, seq_email = " & EcSeqImpr.Text
                    sSql = sSql & " WHERE n_reQ = " & EstrutReq(i).n_req & " AND flg_estado = 0 "
                    BG_ExecutaQuery_ADO sSql
                    
                    EMAIL_ActualizaRequis dataActual, horaActual, EstrutReq(i).n_req
                End If
            Next
        End If
    Next
    If CkApenasImprimirSeleccionada.value <> vbChecked Then
        LimpaCampos
    Else
        FrameDir.Visible = False
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Gerar Email " & Err.Description, Me.Name, "ImprimeEmails"
    Exit Sub
    Resume Next
End Sub


Private Function PreencheCorpoEmail(indice As Integer) As Boolean
    Dim sSql As String
    Dim rsEmail As New ADODB.recordset
    PreencheCorpoEmail = False
    On Error GoTo TrataErro
    sSql = "SELECT corpo_email, flg_html, descr_imagem_anexo,flg_cc,flg_recibo FROM sl_tbf_t_destino  WHERE cod_t_dest = " & EstrutConfig(indice).tipo
    rsEmail.CursorLocation = adUseServer
    rsEmail.CursorType = adOpenKeyset
    rsEmail.LockType = adLockOptimistic
    rsEmail.Open sSql, gConexao
    If rsEmail.RecordCount > 0 Then
        EstrutConfig(indice).flg_html = BL_HandleNull(rsEmail!flg_html, 0)
        EstrutConfig(indice).corpo_txt = BL_HandleNull(rsEmail!corpo_email, "")
        EstrutConfig(indice).descr_imagem_anexo = BL_HandleNull(rsEmail!descr_imagem_anexo, "")
        EstrutConfig(indice).flg_cc = BL_HandleNull(rsEmail!flg_cc, 0)
        EstrutConfig(indice).flg_recibo = BL_HandleNull(rsEmail!flg_recibo, 0)
    End If
    rsEmail.Close
    If EstrutConfig(indice).flg_html = mediSim Then
        sSql = "SELECT corpo_html FROM sl_tbf_t_destino  WHERE cod_t_dest = " & EstrutConfig(indice).tipo
        rsEmail.CursorLocation = adUseServer
        rsEmail.CursorType = adOpenKeyset
        rsEmail.LockType = adLockOptimistic
        rsEmail.Open sSql, gConexao
        If rsEmail.RecordCount > 0 Then
            EstrutConfig(indice).corpo_html = BL_HandleNull(rsEmail!corpo_html, "")
    
        End If
        rsEmail.Close
    End If
    Set rsEmail = Nothing
    PreencheCorpoEmail = True
    If EstrutConfig(indice).descr_imagem_anexo <> "" Then
        'If Dir(gDirCliente & "\" & EstrutConfig(indice).descr_imagem_anexo & ".jpg") <> "" Then
            Kill gDirCliente & "\" & EstrutConfig(indice).descr_imagem_anexo
        'End If
        IM_RetiraImagemBDParaFicheiro gDirCliente & "\" & EstrutConfig(indice).descr_imagem_anexo, "SELECT imagem_anexo FROM sl_tbf_t_destino  WHERE cod_t_dest = " & EstrutConfig(indice).tipo
    End If
Exit Function
TrataErro:
    Select Case Err.Number
        Case 53
            Resume Next
        Case Else
            PreencheCorpoEmail = False
            BG_LogFile_Erros "Erro ao Preencher corpoEmail " & Err.Description, Me.Name, "PreencheCorpoEmail"
            Exit Function
            Resume Next
        End Select
End Function

Public Sub EcCodlocalentrega_Validate(Cancel As Boolean)
    Cancel = PA_ValidateLocalEntrega(EcCodLocalEntrega, EcDescrLocalEntrega)
End Sub

Private Sub BtPesquisaLocalEntrega_Click()
    PA_PesquisaLocalEntrega EcCodLocalEntrega, EcDescrLocalEntrega

End Sub

'BRUNODSANTOS 05.09.2016 - LRV-388
'Public Sub FlexGridColumnColor(FlexGrid As MSFlexGrid, ByVal lngColumn As Long, ByVal lngColor As Long)
'
'    Dim lngPrevCol As Long
'
'    Dim lngPrevColSel As Long
'
'    Dim lngPrevRow As Long
'
'    Dim lngPrevRowSel As Long
'
'    Dim lngPrevFillStyle As Long
'    If lngColumn > FlexGrid.Cols - 1 Then
'
'        Exit Sub
'
'    End If
'
'    lngPrevCol = FlexGrid.Col
'
'    lngPrevRow = FlexGrid.row
'    lngPrevColSel = FlexGrid.ColSel
'
'    lngPrevRowSel = FlexGrid.RowSel
'
'    lngPrevFillStyle = FlexGrid.FillStyle
'
'    FlexGrid.Col = lngColumn
'    FlexGrid.row = FlexGrid.FixedRows
'    FlexGrid.ColSel = lngColumn
'
'    FlexGrid.RowSel = FlexGrid.rows - 1
'    FlexGrid.FillStyle = flexFillRepeat
'    'FlexGrid.CellBackColor = lngColor
'    FlexGrid.CellForeColor = lngColor
'    FlexGrid.CellFontBold = True
'    FlexGrid.CellFontUnderline = True
'    FlexGrid.Col = lngPrevCol
'    FlexGrid.row = lngPrevRow
'    FlexGrid.ColSel = lngPrevColSel
'    FlexGrid.RowSel = lngPrevRowSel
'    FlexGrid.FillStyle = lngPrevFillStyle
'
'
'
'End Sub

'BRUNODSANTOS 05.09.2016 - LRV-388
'Private Sub LimpaFGReq()
'    FgReq.TextMatrix(1, lColPesqTotal) = ""
'    FgReq.TextMatrix(1, lColPesqNome) = ""
'    FgReq.TextMatrix(1, lColPesqEmail) = ""
'    FgReq.TextMatrix(1, lColPesqTipo) = ""
'    FgReq.TextMatrix(1, lColReqNReq) = ""
'    FgReq.TextMatrix(1, lColReqNomeUte) = ""
'    FgReq.TextMatrix(1, lColReqDtChega) = ""
'    FgReq.TextMatrix(1, lColReqImprimir) = ""
'    FgReq.TextMatrix(1, lColReqIndice) = ""
'    FgReq.TextMatrix(1, lColReqPreVisualizar) = ""
'End Sub

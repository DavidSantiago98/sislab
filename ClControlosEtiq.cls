VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClControlosEtiq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

Public CtlEcNumReq As Object
Public CtlEcCodProd As Object
Public CtlEcDescrProd As Object
Public CtlEcDtPrev As Object
Public CtlEcDtChega As Object

' constantes para as cores
Private Const FORECOLOR_SELEC_ENABLED = &H80000008
Private Const BACKCOLOR_SELEC_ENABLED = &H80000005
Private Const FORECOLOR_SELEC_DISABLED = &H80000008
Private Const BACKCOLOR_SELEC_DISABLED = &H8000000F
Private Const FORECOLOR_ENABLED = &H80000008
Private Const BACKCOLOR_ENABLED = &H80000009
Private Const FORECOLOR_DISABLED = &H80000008
Private Const BACKCOLOR_DISABLED = &H8000000F '&HE0E0E0

Public Sub EscondeLinha(indice As Integer)
    CtlEcNumReq(indice).Visible = False
    CtlEcCodProd(indice).Visible = False
    CtlEcDescrProd(indice).Visible = False
    CtlEcDtPrev(indice).Visible = False
    CtlEcDtChega(indice).Visible = False
    LimpaLinha indice
End Sub
Public Sub LimpaLinha(indice As Integer)
    CtlEcNumReq(indice).text = ""
    CtlEcCodProd(indice).text = ""
    CtlEcDescrProd(indice).text = ""
    CtlEcDtPrev(indice).text = ""
    CtlEcDtChega(indice).text = ""
End Sub
Public Sub MostraLinha(indice As Integer)
    CtlEcNumReq(indice).Visible = True
    CtlEcCodProd(indice).Visible = True
    CtlEcDescrProd(indice).Visible = True
    CtlEcDescrProd(indice).locked = True
    CtlEcDtPrev(indice).Visible = True
    CtlEcDtPrev(indice).locked = True
    CtlEcDtChega(indice).Visible = True
End Sub
Public Sub ActivaLinha(indice As Integer)
    CtlEcNumReq(indice).BackColor = BACKCOLOR_SELEC_ENABLED
    CtlEcNumReq(indice).ForeColor = FORECOLOR_SELEC_ENABLED
    CtlEcNumReq(indice).FontBold = True
    CtlEcCodProd(indice).BackColor = BACKCOLOR_SELEC_ENABLED
    CtlEcCodProd(indice).ForeColor = FORECOLOR_SELEC_ENABLED
    CtlEcCodProd(indice).FontBold = True
    CtlEcDescrProd(indice).BackColor = BACKCOLOR_SELEC_DISABLED
    CtlEcDescrProd(indice).ForeColor = FORECOLOR_SELEC_DISABLED
    CtlEcDescrProd(indice).FontBold = True
    CtlEcDtPrev(indice).BackColor = BACKCOLOR_SELEC_DISABLED
    CtlEcDtPrev(indice).ForeColor = FORECOLOR_SELEC_DISABLED
    CtlEcDtPrev(indice).FontBold = True
    CtlEcDtChega(indice).BackColor = BACKCOLOR_SELEC_ENABLED
    CtlEcDtChega(indice).ForeColor = FORECOLOR_SELEC_ENABLED
    CtlEcDtChega(indice).FontBold = True
End Sub
Public Sub DesactivaLinha(indice As Integer)
    CtlEcNumReq(indice).BackColor = BACKCOLOR_ENABLED
    CtlEcNumReq(indice).ForeColor = FORECOLOR_ENABLED
    CtlEcNumReq(indice).FontBold = False
    CtlEcCodProd(indice).BackColor = BACKCOLOR_ENABLED
    CtlEcCodProd(indice).ForeColor = FORECOLOR_ENABLED
    CtlEcCodProd(indice).FontBold = False
    CtlEcDescrProd(indice).BackColor = BACKCOLOR_DISABLED
    CtlEcDescrProd(indice).ForeColor = FORECOLOR_DISABLED
    CtlEcDescrProd(indice).FontBold = False
    CtlEcDtPrev(indice).BackColor = BACKCOLOR_DISABLED
    CtlEcDtPrev(indice).ForeColor = FORECOLOR_DISABLED
    CtlEcDtPrev(indice).FontBold = False
    CtlEcDtChega(indice).BackColor = BACKCOLOR_ENABLED
    CtlEcDtChega(indice).ForeColor = FORECOLOR_ENABLED
    CtlEcDtChega(indice).FontBold = False
End Sub

Public Sub CarregaControlo(indice As Integer, Deslocacao As Long)
    'On Error Resume Next
    Load CtlEcNumReq(indice)
    CtlEcNumReq(indice).Top = CtlEcNumReq(indice).Top + (indice * Deslocacao)
    Load CtlEcCodProd(indice)
    CtlEcCodProd(indice).Top = CtlEcCodProd(indice).Top + (indice * Deslocacao)
    Load CtlEcDescrProd(indice)
    CtlEcDescrProd(indice).Top = CtlEcDescrProd(indice).Top + (indice * Deslocacao)
    Load CtlEcDtPrev(indice)
    CtlEcDtPrev(indice).Top = CtlEcDtPrev(indice).Top + (indice * Deslocacao)
    Load CtlEcDtChega(indice)
    CtlEcDtChega(indice).Top = CtlEcDtChega(indice).Top + (indice * Deslocacao)
End Sub

Public Sub DescarregaControlo(indice As Integer)
    On Error Resume Next
    Unload CtlEcNumReq(indice)
    Unload CtlEcCodProd(indice)
    Unload CtlEcDescrProd(indice)
    Unload CtlEcDtPrev(indice)
    Unload CtlEcDtChega(indice)
End Sub

Public Sub MudaLinhaCursor(CampoActivo As Control, Optional NovaLinha)
    Dim IndiceActivo As Integer
    
    If CampoActivo.Enabled And CampoActivo.Visible Then
            CampoActivo.SetFocus
        End If
    IndiceActivo = CampoActivo.Index
    ActivaLinha IndiceActivo
    Exit Sub
 End Sub

Public Sub MudaLinhaActiva(indice As Integer)
    If CtlEcNumReq(indice).Enabled And CtlEcNumReq(indice).Visible Then
        CtlEcNumReq(indice).SetFocus
    End If
End Sub

Sub PreencheValoresDefeito(PosicaoListaControlos As Integer)

End Sub

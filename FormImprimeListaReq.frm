VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form FormImprimeListaReq 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormImprimeListaReq"
   ClientHeight    =   4290
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3855
   Icon            =   "FormImprimeListaReq.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4290
   ScaleWidth      =   3855
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtPesquisaRapidaGrAnalises 
      Height          =   375
      Left            =   3360
      Picture         =   "FormImprimeListaReq.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   120
      Width           =   375
   End
   Begin VB.TextBox EcDescrGrAnalises 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   1440
      Locked          =   -1  'True
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   120
      Width           =   1935
   End
   Begin VB.TextBox EcGrAnalises 
      BackColor       =   &H80000018&
      Height          =   285
      Left            =   960
      TabIndex        =   6
      Top             =   120
      Width           =   495
   End
   Begin VB.CommandButton BtImprimir 
      Caption         =   "&Imprimir"
      Height          =   615
      Left            =   1320
      Picture         =   "FormImprimeListaReq.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   3360
      Width           =   1215
   End
   Begin RichTextLib.RichTextBox RText 
      Height          =   615
      Left            =   2640
      TabIndex        =   4
      Top             =   3360
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   1085
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"FormImprimeListaReq.frx":06E0
   End
   Begin VB.CommandButton BtAcrecenta 
      Caption         =   "Adicionar"
      Height          =   375
      Left            =   2760
      TabIndex        =   3
      Top             =   600
      Width           =   975
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      ItemData        =   "FormImprimeListaReq.frx":0762
      Left            =   120
      List            =   "FormImprimeListaReq.frx":0764
      TabIndex        =   1
      Top             =   1080
      Width           =   3615
   End
   Begin VB.TextBox EcRequisicao 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   960
      TabIndex        =   0
      Top             =   600
      Width           =   1065
   End
   Begin VB.Label Label1 
      Caption         =   "Grupo"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label3 
      Caption         =   "Requisi��o"
      Height          =   225
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   975
   End
End
Attribute VB_Name = "FormImprimeListaReq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Private Type DadosRequisLista
    num_req As String
    seq_utente As String
    estado_req As String
End Type

Dim ListaRequis() As DadosRequisLista
Dim TamListaRequis As Long

Public rs As ADODB.recordset

Private Sub BtAcrecenta_Click()
    AcrescentaLista
End Sub

Private Sub BtImprimir_Click()
    Dim i As Integer
    
    For i = 1 To TamListaRequis
        gImprimirDestino = 1
        If EcGrAnalises <> "" Then
            Call IR_ImprimeResultados(False, False, ListaRequis(i).num_req, ListaRequis(i).estado_req, ListaRequis(i).seq_utente, , , , False, EcGrAnalises, , , True, , , , , , , False)
        Else
            Call IR_ImprimeResultados(False, False, ListaRequis(i).num_req, ListaRequis(i).estado_req, ListaRequis(i).seq_utente, , , , False, , , , True, , , , , , , False)
        End If
    Next i
    EcLista.Clear
    TamListaRequis = 0
    EcRequisicao.SetFocus
End Sub

Private Sub BtPesquisaRapidaGrAnalises_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupos de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcGrAnalises.Text = resultados(1)
            EcDescrGrAnalises.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub

Private Sub EcGrAnalises_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub
Private Sub EcGrAnalises_Validate(Cancel As Boolean)
    Dim RsDescrGrAnalises As ADODB.recordset
    
    If Trim(EcGrAnalises.Text) <> "" Then
        Set RsDescrGrAnalises = New ADODB.recordset
        
        With RsDescrGrAnalises
            .Source = "SELECT descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana= " & BL_TrataStringParaBD(EcGrAnalises.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrAnalises.RecordCount > 0 Then
            EcDescrGrAnalises.Text = RsDescrGrAnalises!descr_gr_ana
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
        Else
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
            EcDescrGrAnalises.Text = ""
            BG_Mensagem mediMsgBox, "O Grupo de An�lise indicado n�o existe!", vbOKOnly + vbExclamation, "Aten��o!"
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrGrAnalises.Text = ""
    End If
    
End Sub



Private Sub EcRequisicao_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        AcrescentaLista
    End If
End Sub
Sub AcrescentaLista()
    Dim rs As ADODB.recordset
    Dim sql As String
    
    If EcRequisicao.Text <> "" Then
        sql = "SELECT estado_req,seq_utente from sl_requis where n_req = " & EcRequisicao.Text & " and estado_req in ('2','D','3','F') "
        Set rs = New ADODB.recordset
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenStatic
        rs.Open sql, gConexao
        
        If rs.RecordCount > 0 Then
            TamListaRequis = TamListaRequis + 1
            ReDim Preserve ListaRequis(TamListaRequis)
            ListaRequis(TamListaRequis).num_req = EcRequisicao.Text
            ListaRequis(TamListaRequis).seq_utente = BL_HandleNull(rs!seq_utente)
            ListaRequis(TamListaRequis).estado_req = BL_HandleNull(rs!estado_req)
            
            EcLista.AddItem EcRequisicao.Text
            EcRequisicao.Text = ""
        Else
            BG_Mensagem mediMsgBox, "REQUISI��O INCORRECTA PARA IMPRIMIR", vbInformation, App.ProductName
            EcRequisicao.Text = ""
            EcRequisicao.SetFocus
        End If
        
        rs.Close
        Set rs = Nothing
    End If
End Sub
Private Sub EcRequisicao_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcRequisicao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Form Lista Requisi��es"
    Me.left = 540
    Me.top = 450
    Me.Width = 3885
    Me.Height = 4770 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    Set CampoDeFocus = EcRequisicao
    
    TamListaRequis = 0
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormImprimeListaReq = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    
End Sub

Sub DefTipoCampos()
    EcRequisicao.Tag = adInteger
End Sub

Sub PreencheCampos()
    
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()

End Sub

Sub BD_Insert()
    
End Sub

Sub FuncaoModificar()

End Sub

Sub BD_Update()

End Sub

Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub

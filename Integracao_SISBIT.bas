Attribute VB_Name = "Integracao_SISBIT"
Option Explicit

Public Function SISBIT_Nova_ANALISES_IN(ByVal situacao As String, _
                                       ByVal num_episodio As String, _
                                       ByVal num_processo As String, _
                                       ByVal cod_especialidade As String, _
                                       ByVal cod_analise As String, _
                                       ByVal dta_episodio As String, _
                                       ByVal n_req As Long, _
                                       Optional ByVal codEfr As String, _
                                       Optional ByVal cod_ana_sislab As String, _
                                       Optional ByVal Cod_Perfil As String, _
                                       Optional ByVal cod_ana_c As String, _
                                       Optional ByVal cod_ana_s As String, _
                                       Optional ByVal cod_agrup As String, _
                                       Optional ByVal cod_micro As String, _
                                       Optional ByVal Cod_Gr_Antibio As String, _
                                       Optional ByVal cod_antibio As String, _
                                       Optional ByVal Cod_Prova As String, _
                                       Optional ByVal centro_custo As String, _
                                       Optional ByVal CodLocalFact As Long, _
                                       Optional ByVal numMarcadores As Integer) As Integer

    Dim cod_modulo As String
    Dim retorno As String
    Dim sql As String
    Dim rsHIS As ADODB.recordset
    Dim Cod_Analise_Aux As String
    Dim dta_analise As String
    Dim cod_medico As String
    Dim Stuacao_SISBIT As String
    Dim NumEpisodio_SISBIT As String
    Dim NumProcesso_SISBIT As String
    Dim CodEspecialidade_SISBIT As String
    Dim CodAnalise_SISBIT As String
    Dim DtaEpisodio_SISBIT As String
    Dim CodEFR_SISBIT As String
    Dim qtd As Integer
    Dim NomeFicheiro As String
    Dim servReqCodSonho As String
    
    On Error GoTo TrataErro
    
    Stuacao_SISBIT = situacao
    NumEpisodio_SISBIT = num_episodio
    NumProcesso_SISBIT = num_processo
    CodEspecialidade_SISBIT = cod_especialidade
    CodAnalise_SISBIT = cod_analise
    DtaEpisodio_SISBIT = dta_episodio
    
    'RGONCALVES 16.09.2013 CHLeiria-Pombal-1
    servReqCodSonho = Trim(SISBIT_Devolve_Cod_Servico_sonho_sislab(CodEspecialidade_SISBIT))
    If servReqCodSonho <> "" Then
        CodEspecialidade_SISBIT = servReqCodSonho
    End If
    '
    
    If gLAB = "HLEIRIA" Then
        If cod_especialidade = 56 Then
            CodEFR_SISBIT = "990045"
        ElseIf cod_especialidade = 57 Then
            CodEFR_SISBIT = "990007"
        ElseIf cod_especialidade = 58 Then
            CodEFR_SISBIT = "990033"
        ElseIf cod_especialidade = 59 Then
            CodEFR_SISBIT = "090002"
        ElseIf cod_especialidade = 80 Then
            CodEFR_SISBIT = "990013"
        ElseIf cod_especialidade = 81 Then
            CodEFR_SISBIT = "090001"
        ElseIf cod_especialidade = 82 Then
            CodEFR_SISBIT = "270012"
        ElseIf cod_especialidade = 83 Then
            CodEFR_SISBIT = "090017"
        ElseIf cod_especialidade = 84 Then
            CodEFR_SISBIT = "270081"
        ElseIf cod_especialidade = 85 Then
            CodEFR_SISBIT = "090079"
        ElseIf cod_especialidade = 60 Then
            CodEFR_SISBIT = "090084"
        ElseIf cod_especialidade = 66 Then
            CodEFR_SISBIT = "990175"
        'RGONCALVES 16.09.2013 CHLeiria-Pombal-1
        ElseIf cod_especialidade = 86 Then
            CodEFR_SISBIT = "180004"
        '
        Else
            CodEFR_SISBIT = codEfr
        End If
    Else
        CodEFR_SISBIT = codEfr
    End If
    
    num_episodio = Trim(num_episodio)
    
    ' S� envia com epis�dio preenchido.
    If (Len(num_episodio) = 0) Then
        SISBIT_Nova_ANALISES_IN = -2
        Exit Function
    End If
    
    cod_analise = Trim(cod_analise)
    situacao = Trim(situacao)
    num_processo = Trim(num_processo)
    cod_especialidade = Trim(cod_especialidade)
    dta_episodio = Trim(dta_episodio)
    
    cod_medico = BL_SelCodigo("SL_REQUIS", "cod_med", "N_REQ", n_req)
    
    If (Trim(dta_episodio) = "") Then
        dta_analise = "'" & Format(Date, gFormatoData) & "'"
    Else
        dta_analise = "'" & Format(dta_episodio, gFormatoData) & "'"
    End If
    
    If (Len(cod_analise) = 0) Or _
       (Len(dta_episodio) = 0) Then
        ' Par�metros incompletos.
        SISBIT_Nova_ANALISES_IN = -2
        Exit Function
    End If
    
    ' Mapeia a situa��o do SISLAB para o SONHO.
    Select Case situacao
        Case gT_Urgencia
            cod_modulo = "URG"
        Case gT_Consulta
            If gLAB = "HPOVOA" Then retorno = "P"
            'If cod_analise = gCodAnaSONHO Then Retorno = "A"
            cod_modulo = "CON"
        Case gT_Internamento
            cod_modulo = "INT"
        Case gT_Externo
            cod_modulo = "HDI"
        Case gT_LAB
            cod_modulo = "LAB"
        Case gT_Ambulatorio
            cod_modulo = "AMB"
        Case Else
            cod_modulo = ""
    End Select
    
    ' Mapeia a an�lise do SISLAB para o SONHO
    
    If (Len(cod_modulo) > 0) And _
       (Len(num_episodio) > 0) Then
    
        
        If Not (IsNumeric(num_episodio)) Then
            ' N� de epis�dio n�o � um n�mero.
            SISBIT_Nova_ANALISES_IN = -3
            Exit Function
        End If
        
        ' Caso 1 : Situa��o (m�dulo) + epis�dio
        cod_modulo = "'" & cod_modulo & "'"
        num_episodio = num_episodio
        cod_especialidade = cod_especialidade
        dta_episodio = "'" & Format(Trim(dta_episodio), gFormatoData) & "'"
    Else
        ' Caso 2 : Situa��o (m�dulo) + processo + data de epis�dio + cod_especialidade.
        
        cod_modulo = "'" & cod_modulo & "'"
        num_processo = num_processo
        dta_episodio = "'" & Format(Trim(dta_episodio), gFormatoData) & "'"
        cod_especialidade = cod_especialidade
        num_episodio = "NULL"
    
    End If
    
    
        sql = "INSERT INTO sl_analises_in " & vbCrLf & _
              "( " & vbCrLf & _
              "     cod_modulo, " & vbCrLf & _
              "     num_episodio, " & vbCrLf & _
              "     num_processo, " & vbCrLf & _
              "     dta_episodio, " & vbCrLf & _
              "     dta_analise, " & vbCrLf & _
              "     cod_especialidade, " & vbCrLf & _
              "     cod_analise, " & vbCrLf & _
              "     quantidade, " & vbCrLf & _
              "     retorno,cod_ana_sislab, " & vbCrLf & _
              "     n_req, cod_perfil, cod_ana_c, cod_ana_s, cod_agrup, cod_micro, cod_gr_antibio,cod_antibio, cod_prova, centro_custo " & vbCrLf
            If gEnviaLocalSONHO = mediSim Then
                sql = sql & ", cod_aparelho "
            End If
            sql = sql & ") " & vbCrLf & _
              "VALUES " & vbCrLf & _
              "( " & vbCrLf & _
              "     " & cod_modulo & ", " & vbCrLf & _
              "     " & num_episodio & ", " & vbCrLf & _
              "     " & BL_TrataStringParaBD(num_processo) & ", " & vbCrLf & _
              "     " & dta_episodio & ", " & vbCrLf & _
              "     " & dta_analise & ", " & vbCrLf & _
              "     " & cod_especialidade & ", " & vbCrLf & _
              "     '" & cod_analise & "', " & vbCrLf & _
              "     1, " & vbCrLf & _
              "     'I','" & cod_ana_sislab & "', " & vbCrLf & _
              "     " & n_req & ",'" & Cod_Perfil & "', '" & cod_ana_c & "','" & cod_ana_s & "','" & cod_agrup & "','" & cod_micro & "','" & Cod_Gr_Antibio & "','" & cod_antibio & "','" & Cod_Prova & "'," & BL_TrataStringParaBD(centro_custo)
            If gEnviaLocalSONHO = mediSim Then
                sql = sql & ",  " & CodLocalFact
            End If
            sql = sql & ")"
    
    
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
    
        
    'Escrever ficheiros para a SISBIT
    Dim Fact As Integer
    Dim FactLocal As Integer
    Dim Erro As Integer
    
    qtd = 1
    
    If Dir(gCaminhoFacturacaoSISBIT & "\*.*") <> "" Then
        NomeFicheiro = gCaminhoFacturacaoSISBIT & "\H_" & n_req & ".txt"
    Else
        If Dir("L:\Facturacao\*.*") <> "" Then
            NomeFicheiro = "L:\Facturacao\H_" & n_req & ".txt"
        ElseIf Dir("L:\Facturacao\ssangue\*.*") <> "" Then
            NomeFicheiro = "L:\Facturacao\ssangue\H_" & n_req & ".txt"
        Else
            If gCaminhoFacturacaoSISBIT <> "-1" And gCaminhoFacturacaoSISBIT <> "0" And gCaminhoFacturacaoSISBIT <> "" Then
                NomeFicheiro = gCaminhoFacturacaoSISBIT & "\H_" & n_req & ".txt"
            End If
        End If
    End If
    If NomeFicheiro <> "" Then
        Fact = FileLock(NomeFicheiro, ModuleFilesHandle.eOpenType.eOpenUpdate)
        If (Fact = ModuleFilesHandle.FILE_FEEDBACK.FILE_IN_USE) Then: Exit Function
        Print #Fact, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT & "|" & cod_medico
        FileUnLock Fact, 1, True
        SISBIT_Nova_ANALISES_IN = 1
    End If
    
    SISBIT_Nova_ANALISES_IN = 1
    
    NomeFicheiro = gDirCliente & "\bin\H_" & n_req & ".txt"
    If NomeFicheiro <> "" Then
        FactLocal = FileLock(NomeFicheiro, ModuleFilesHandle.eOpenType.eOpenUpdate)
        If (FactLocal = ModuleFilesHandle.FILE_FEEDBACK.FILE_IN_USE) Then: Exit Function
        Print #FactLocal, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT & "|" & cod_medico
        FileUnLock FactLocal, 1, True
    End If
    
'    FactLocal = FreeFile
'    Open gDirCliente & "\bin\H_" & n_req & ".txt" For Append As #FactLocal 'Testes
'    Print #FactLocal, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT & "|" & cod_medico
'    Close #FactLocal
'
'    Fact = FreeFile
'    If Dir(gCaminhoFacturacaoSISBIT & "\*.*") <> "" Then
'        Open gCaminhoFacturacaoSISBIT & "\H_" & n_req & ".txt" For Append As #Fact
'    Else
'        If Dir("L:\Facturacao\*.*") <> "" Then
'            Open "L:\Facturacao\H_" & n_req & ".txt" For Append As #Fact
'        ElseIf Dir("L:\Facturacao\ssangue\*.*") <> "" Then
'            Open "L:\Facturacao\ssangue\H_" & n_req & ".txt" For Append As #Fact
'        Else
'            If gCaminhoFacturacaoSISBIT <> "-1" And gCaminhoFacturacaoSISBIT <> "0" And gCaminhoFacturacaoSISBIT <> "" Then
'                Open gCaminhoFacturacaoSISBIT & "\H_" & n_req & ".txt" For Append As #Fact
'            End If
'        End If
'    End If
'    'Print #Fact, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT
'    Print #Fact, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT & "|" & cod_medico
'    Close #Fact
    
    SISBIT_Nova_ANALISES_IN = 1
    Exit Function
    
TrataErro:
    SISBIT_Nova_ANALISES_IN = -1
    Exit Function
    Resume Next
    
End Function


Public Function SISBIT_Remove_ANALISES_IN(ByVal situacao As String, _
                                       num_episodio As String, _
                                       num_processo As String, _
                                       cod_especialidade As String, _
                                       cod_analise As String, _
                                       dta_episodio As String, _
                                       n_req As Long, _
                                       Optional ByVal codEfr As String, _
                                       Optional ByVal cod_ana_sislab As String, _
                                       Optional ByVal Cod_Perfil As String, _
                                       Optional ByVal cod_ana_c As String, _
                                       Optional ByVal cod_ana_s As String, _
                                       Optional ByVal cod_agrup As String, _
                                       Optional ByVal cod_micro As String, _
                                       Optional ByVal Cod_Gr_Antibio As String, _
                                       Optional ByVal cod_antibio As String, _
                                       Optional ByVal Cod_Prova As String, _
                                       Optional ByVal centro_custo As String, _
                                       Optional ByVal CodLocalFact As Long) As Integer
    Dim cod_modulo As String
    Dim retorno As String
    Dim sql As String
    Dim rsHIS As ADODB.recordset
    Dim Cod_Analise_Aux As String
    Dim dta_analise As String
    Dim cod_medico As String
    Dim Stuacao_SISBIT As String
    Dim NumEpisodio_SISBIT As String
    Dim NumProcesso_SISBIT As String
    Dim CodEspecialidade_SISBIT As String
    Dim CodAnalise_SISBIT As String
    Dim DtaEpisodio_SISBIT As String
    Dim CodEFR_SISBIT As String
    Dim qtd As Integer
    Dim NomeFicheiro As String
    Dim servReqCodSonho As String
    
    On Error GoTo TrataErro
    
    Stuacao_SISBIT = situacao
    NumEpisodio_SISBIT = num_episodio
    NumProcesso_SISBIT = num_processo
    CodEspecialidade_SISBIT = cod_especialidade
    CodAnalise_SISBIT = cod_analise
    DtaEpisodio_SISBIT = dta_episodio
    CodEFR_SISBIT = codEfr
    
    'RGONCALVES 16.09.2013 CHLeiria-Pombal-1
    servReqCodSonho = Trim(SISBIT_Devolve_Cod_Servico_sonho_sislab(CodEspecialidade_SISBIT))
    If servReqCodSonho <> "" Then
        CodEspecialidade_SISBIT = servReqCodSonho
    End If
    '
    
    num_episodio = Trim(num_episodio)
    
    ' S� envia com epis�dio preenchido.
    If (Len(num_episodio) = 0) Then
        SISBIT_Remove_ANALISES_IN = -2
        Exit Function
    End If
    
    cod_analise = Trim(cod_analise)
    situacao = Trim(situacao)
    num_processo = Trim(num_processo)
    cod_especialidade = Trim(cod_especialidade)
    dta_episodio = Trim(dta_episodio)
    
    cod_medico = BL_SelCodigo("SL_REQUIS", "cod_med", "N_REQ", n_req)
    
    If (Trim(dta_episodio) = "") Then
        dta_analise = "'" & Format(Date, gFormatoData) & "'"
    Else
        dta_analise = "'" & Format(dta_episodio, gFormatoData) & "'"
    End If
    
    If (Len(cod_analise) = 0) Or _
       (Len(dta_episodio) = 0) Then
        ' Par�metros incompletos.
        SISBIT_Remove_ANALISES_IN = -2
        Exit Function
    End If
    
    ' Mapeia a situa��o do SISLAB para o SONHO.
    Select Case situacao
        Case gT_Urgencia
            cod_modulo = "URG"
        Case gT_Consulta
            If gLAB = "HPOVOA" Then retorno = "P"
            'If cod_analise = gCodAnaSONHO Then Retorno = "A"
            cod_modulo = "CON"
        Case gT_Internamento
            cod_modulo = "INT"
        Case gT_Externo
            cod_modulo = "HDI"
        Case gT_LAB
            cod_modulo = "LAB"
        Case gT_Ambulatorio
            cod_modulo = "AMB"
        Case Else
            cod_modulo = ""
    End Select
    
    ' Mapeia a an�lise do SISLAB para o SONHO
    
    If (Len(cod_modulo) > 0) And _
       (Len(num_episodio) > 0) Then
    
        
        If Not (IsNumeric(num_episodio)) Then
            ' N� de epis�dio n�o � um n�mero.
            SISBIT_Remove_ANALISES_IN = -3
            Exit Function
        End If
        
        ' Caso 1 : Situa��o (m�dulo) + epis�dio
        cod_modulo = "'" & cod_modulo & "'"
        num_episodio = num_episodio
        cod_especialidade = cod_especialidade
        dta_episodio = "'" & Format(Trim(dta_episodio), gFormatoData) & "'"
    Else
        ' Caso 2 : Situa��o (m�dulo) + processo + data de epis�dio + cod_especialidade.
        
        cod_modulo = "'" & cod_modulo & "'"
        num_processo = num_processo
        dta_episodio = "'" & Format(Trim(dta_episodio), gFormatoData) & "'"
        cod_especialidade = cod_especialidade
        num_episodio = "NULL"
    
    End If
    
    
        sql = "INSERT INTO sl_analises_in " & vbCrLf & _
              "( " & vbCrLf & _
              "     cod_modulo, " & vbCrLf & _
              "     num_episodio, " & vbCrLf & _
              "     num_processo, " & vbCrLf & _
              "     dta_episodio, " & vbCrLf & _
              "     dta_analise, " & vbCrLf & _
              "     cod_especialidade, " & vbCrLf & _
              "     cod_analise, " & vbCrLf & _
              "     quantidade, " & vbCrLf & _
              "     retorno,cod_ana_sislab, " & vbCrLf & _
              "     n_req, cod_perfil, cod_ana_c, cod_ana_s, cod_agrup, cod_micro, cod_gr_antibio,cod_antibio, cod_prova, centro_custo " & vbCrLf
            If gEnviaLocalSONHO = mediSim Then
                sql = sql & ", cod_aparelho "
            End If
            sql = sql & ") " & vbCrLf & _
              "VALUES " & vbCrLf & _
              "( " & vbCrLf & _
              "     " & cod_modulo & ", " & vbCrLf & _
              "     " & num_episodio & ", " & vbCrLf & _
              "     " & BL_TrataStringParaBD(num_processo) & ", " & vbCrLf & _
              "     " & dta_episodio & ", " & vbCrLf & _
              "     " & dta_analise & ", " & vbCrLf & _
              "     " & cod_especialidade & ", " & vbCrLf & _
              "     '" & cod_analise & "', " & vbCrLf & _
              "     -1, " & vbCrLf & _
              "     'I','" & cod_ana_sislab & "', " & vbCrLf & _
              "     " & n_req & ",'" & Cod_Perfil & "', '" & cod_ana_c & "','" & _
              cod_ana_s & "','" & cod_agrup & "','" & cod_micro & "','" & Cod_Gr_Antibio & "','" & _
              "     " & cod_antibio & "','" & Cod_Prova & "'," & BL_TrataStringParaBD(centro_custo)
            If gEnviaLocalSONHO = mediSim Then
                sql = sql & ",  " & CodLocalFact
            End If
            sql = sql & ")"
    
    
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
    
        
    'Escrever ficheiros para a SISBIT
    Dim Fact As Integer
    Dim FactLocal As Integer
    Dim Erro As Integer
    
    qtd = -1
    
    If Dir(gCaminhoFacturacaoSISBIT & "\*.*") <> "" Then
        NomeFicheiro = gCaminhoFacturacaoSISBIT & "\H_" & n_req & ".txt"
    Else
        If Dir("L:\Facturacao\*.*") <> "" Then
            NomeFicheiro = "L:\Facturacao\H_" & n_req & ".txt"
        ElseIf Dir("L:\Facturacao\ssangue\*.*") <> "" Then
            NomeFicheiro = "L:\Facturacao\ssangue\H_" & n_req & ".txt"
        Else
            If gCaminhoFacturacaoSISBIT <> "-1" And gCaminhoFacturacaoSISBIT <> "0" And gCaminhoFacturacaoSISBIT <> "" Then
                NomeFicheiro = gCaminhoFacturacaoSISBIT & "\H_" & n_req & ".txt"
            End If
        End If
    End If
    If NomeFicheiro <> "" Then
        Fact = FileLock(NomeFicheiro, ModuleFilesHandle.eOpenType.eOpenUpdate)
        If (Fact = ModuleFilesHandle.FILE_FEEDBACK.FILE_IN_USE) Then: Exit Function
        Print #Fact, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT & "|" & cod_medico
        FileUnLock Fact, 1, True
        SISBIT_Remove_ANALISES_IN = 1
    End If
    
    SISBIT_Remove_ANALISES_IN = 1
    
    NomeFicheiro = gDirCliente & "\bin\H_" & n_req & ".txt"
    If NomeFicheiro <> "" Then
        FactLocal = FileLock(NomeFicheiro, ModuleFilesHandle.eOpenType.eOpenUpdate)
        If (FactLocal = ModuleFilesHandle.FILE_FEEDBACK.FILE_IN_USE) Then: Exit Function
        Print #FactLocal, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT & "|" & cod_medico
        FileUnLock FactLocal, 1, True
    End If
    
    Exit Function
    
TrataErro:
    SISBIT_Remove_ANALISES_IN = -1
    Exit Function
    Resume Next
    
End Function

Public Function SISBIT_Nova_ANALISES_IN_OLD(ByVal situacao As String, _
                                       ByVal num_episodio As String, _
                                       ByVal num_processo As String, _
                                       ByVal cod_especialidade As String, _
                                       ByVal cod_analise As String, _
                                       ByVal dta_episodio As String, _
                                       ByVal n_req As Long, _
                                       Optional ByVal codEfr As String, _
                                       Optional ByVal cod_ana_sislab As String, _
                                       Optional ByVal Cod_Perfil As String, _
                                       Optional ByVal cod_ana_c As String, _
                                       Optional ByVal cod_ana_s As String, _
                                       Optional ByVal cod_agrup As String, _
                                       Optional ByVal cod_micro As String, _
                                       Optional ByVal Cod_Gr_Antibio As String, _
                                       Optional ByVal cod_antibio As String, _
                                       Optional ByVal Cod_Prova As String, _
                                       Optional ByVal centro_custo As String, _
                                       Optional ByVal CodLocalFact As Long) As Integer

    Dim cod_modulo As String
    Dim retorno As String
    Dim sql As String
    Dim rsHIS As ADODB.recordset
    Dim Cod_Analise_Aux As String
    Dim dta_analise As String
    Dim cod_medico As String
    Dim Stuacao_SISBIT As String
    Dim NumEpisodio_SISBIT As String
    Dim NumProcesso_SISBIT As String
    Dim CodEspecialidade_SISBIT As String
    Dim CodAnalise_SISBIT As String
    Dim DtaEpisodio_SISBIT As String
    Dim CodEFR_SISBIT As String
    
    Dim qtd As Integer
    
    On Error GoTo TrataErro
    
    Stuacao_SISBIT = situacao
    NumEpisodio_SISBIT = num_episodio
    NumProcesso_SISBIT = num_processo
    CodEspecialidade_SISBIT = cod_especialidade
    CodAnalise_SISBIT = cod_analise
    DtaEpisodio_SISBIT = dta_episodio
    CodEFR_SISBIT = codEfr
    
    
    num_episodio = Trim(num_episodio)
    
    ' S� envia com epis�dio preenchido.
    If (Len(num_episodio) = 0) Then
        SISBIT_Nova_ANALISES_IN_OLD = -2
        Exit Function
    End If
    
    cod_analise = Trim(cod_analise)
    situacao = Trim(situacao)
    num_processo = Trim(num_processo)
    cod_especialidade = Trim(cod_especialidade)
    dta_episodio = Trim(dta_episodio)
    
    cod_medico = BL_SelCodigo("SL_REQUIS", "cod_med", "N_REQ", n_req)
    
    If (Trim(dta_episodio) = "") Then
        dta_analise = "'" & Format(Date, gFormatoData) & "'"
    Else
        dta_analise = "'" & Format(dta_episodio, gFormatoData) & "'"
    End If
    
    If (Len(cod_analise) = 0) Or _
       (Len(dta_episodio) = 0) Then
        ' Par�metros incompletos.
        SISBIT_Nova_ANALISES_IN_OLD = -2
        Exit Function
    End If
    
    ' Mapeia a situa��o do SISLAB para o SONHO.
    Select Case situacao
        Case gT_Urgencia
            cod_modulo = "URG"
        Case gT_Consulta
            If gLAB = "HPOVOA" Then retorno = "P"
            'If cod_analise = gCodAnaSONHO Then Retorno = "A"
            cod_modulo = "CON"
        Case gT_Internamento
            cod_modulo = "INT"
        Case gT_Externo
            cod_modulo = "HDI"
        Case gT_LAB
            cod_modulo = "LAB"
        Case gT_Ambulatorio
            cod_modulo = "AMB"
        Case Else
            cod_modulo = ""
    End Select
    
    ' Mapeia a an�lise do SISLAB para o SONHO
    
    If (Len(cod_modulo) > 0) And _
       (Len(num_episodio) > 0) Then
    
        
        If Not (IsNumeric(num_episodio)) Then
            ' N� de epis�dio n�o � um n�mero.
            SISBIT_Nova_ANALISES_IN_OLD = -3
            Exit Function
        End If
        
        ' Caso 1 : Situa��o (m�dulo) + epis�dio
        cod_modulo = "'" & cod_modulo & "'"
        num_episodio = num_episodio
        cod_especialidade = cod_especialidade
        dta_episodio = "'" & Format(Trim(dta_episodio), gFormatoData) & "'"
    Else
        ' Caso 2 : Situa��o (m�dulo) + processo + data de epis�dio + cod_especialidade.
        
        cod_modulo = "'" & cod_modulo & "'"
        num_processo = num_processo
        dta_episodio = "'" & Format(Trim(dta_episodio), gFormatoData) & "'"
        cod_especialidade = cod_especialidade
        num_episodio = "NULL"
    
    End If
    
    
        sql = "INSERT INTO sl_analises_in " & vbCrLf & _
              "( " & vbCrLf & _
              "     cod_modulo, " & vbCrLf & _
              "     num_episodio, " & vbCrLf & _
              "     num_processo, " & vbCrLf & _
              "     dta_episodio, " & vbCrLf & _
              "     dta_analise, " & vbCrLf & _
              "     cod_especialidade, " & vbCrLf & _
              "     cod_analise, " & vbCrLf & _
              "     quantidade, " & vbCrLf & _
              "     retorno,cod_ana_sislab, " & vbCrLf & _
              "     n_req, cod_perfil, cod_ana_c, cod_ana_s, cod_agrup, cod_micro, cod_gr_antibio,cod_antibio, cod_prova, centro_custo " & vbCrLf
            If gEnviaLocalSONHO = mediSim Then
                sql = sql & ", cod_aparelho "
            End If
            sql = sql & ") " & vbCrLf & _
              "VALUES " & vbCrLf & _
              "( " & vbCrLf & _
              "     " & cod_modulo & ", " & vbCrLf & _
              "     " & num_episodio & ", " & vbCrLf & _
              "     " & BL_TrataStringParaBD(num_processo) & ", " & vbCrLf & _
              "     " & dta_episodio & ", " & vbCrLf & _
              "     " & dta_analise & ", " & vbCrLf & _
              "     " & cod_especialidade & ", " & vbCrLf & _
              "     '" & cod_analise & "', " & vbCrLf & _
              "     1, " & vbCrLf & _
              "     'I','" & cod_ana_sislab & "', " & vbCrLf & _
              "     " & n_req & ",'" & Cod_Perfil & "', '" & cod_ana_c & "','" & cod_ana_s & "','" & cod_agrup & "','" & cod_micro & "','" & Cod_Gr_Antibio & "','" & cod_antibio & "','" & Cod_Prova & "'," & BL_TrataStringParaBD(centro_custo)
            If gEnviaLocalSONHO = mediSim Then
                sql = sql & ",  " & CodLocalFact
            End If
            sql = sql & ")"
    
    
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
    
        
    'Escrever ficheiros para a SISBIT
    Dim Fact As Integer
    Dim FactLocal As Integer
    Dim Erro As Integer
    
    qtd = 1
    
    FactLocal = FreeFile
    Open gDirCliente & "\bin\H_" & n_req & ".txt" For Append As #FactLocal 'Testes
    Print #FactLocal, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT & "|" & cod_medico
    Close #FactLocal
    
    Fact = FreeFile
    If Dir(gCaminhoFacturacaoSISBIT & "\*.*") <> "" Then
        Open gCaminhoFacturacaoSISBIT & "\H_" & n_req & ".txt" For Append As #Fact
    Else
        If Dir("L:\Facturacao\*.*") <> "" Then
            Open "L:\Facturacao\H_" & n_req & ".txt" For Append As #Fact
        ElseIf Dir("L:\Facturacao\ssangue\*.*") <> "" Then
            Open "L:\Facturacao\ssangue\H_" & n_req & ".txt" For Append As #Fact
        Else
            If gCaminhoFacturacaoSISBIT <> "-1" And gCaminhoFacturacaoSISBIT <> "0" And gCaminhoFacturacaoSISBIT <> "" Then
                Open gCaminhoFacturacaoSISBIT & "\H_" & n_req & ".txt" For Append As #Fact
            End If
        End If
    End If
    'Print #Fact, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT
    Print #Fact, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT & "|" & cod_medico
    Close #Fact
    
    SISBIT_Nova_ANALISES_IN_OLD = 1
    Exit Function
    
TrataErro:
    SISBIT_Nova_ANALISES_IN_OLD = -1
    Exit Function
    Resume Next
    
End Function



Public Function SISBIT_Remove_ANALISES_IN_OLD(ByVal situacao As String, _
                                       num_episodio As String, _
                                       num_processo As String, _
                                       cod_especialidade As String, _
                                       cod_analise As String, _
                                       dta_episodio As String, _
                                       n_req As Long, _
                                       Optional ByVal codEfr As String, _
                                       Optional ByVal cod_ana_sislab As String, _
                                       Optional ByVal Cod_Perfil As String, _
                                       Optional ByVal cod_ana_c As String, _
                                       Optional ByVal cod_ana_s As String, _
                                       Optional ByVal cod_agrup As String, _
                                       Optional ByVal cod_micro As String, _
                                       Optional ByVal Cod_Gr_Antibio As String, _
                                       Optional ByVal cod_antibio As String, _
                                       Optional ByVal Cod_Prova As String, _
                                       Optional ByVal centro_custo As String, _
                                       Optional ByVal CodLocalFact As Long) As Integer
    Dim cod_modulo As String
    Dim retorno As String
    Dim sql As String
    Dim rsHIS As ADODB.recordset
    Dim Cod_Analise_Aux As String
    Dim dta_analise As String
    Dim cod_medico As String
    Dim Stuacao_SISBIT As String
    Dim NumEpisodio_SISBIT As String
    Dim NumProcesso_SISBIT As String
    Dim CodEspecialidade_SISBIT As String
    Dim CodAnalise_SISBIT As String
    Dim DtaEpisodio_SISBIT As String
    Dim CodEFR_SISBIT As String
    
    Dim qtd As Integer
    
    On Error GoTo TrataErro
    
    Stuacao_SISBIT = situacao
    NumEpisodio_SISBIT = num_episodio
    NumProcesso_SISBIT = num_processo
    CodEspecialidade_SISBIT = cod_especialidade
    CodAnalise_SISBIT = cod_analise
    DtaEpisodio_SISBIT = dta_episodio
    CodEFR_SISBIT = codEfr
    
    
    num_episodio = Trim(num_episodio)
    
    ' S� envia com epis�dio preenchido.
    If (Len(num_episodio) = 0) Then
        SISBIT_Remove_ANALISES_IN_OLD = -2
        Exit Function
    End If
    
    cod_analise = Trim(cod_analise)
    situacao = Trim(situacao)
    num_processo = Trim(num_processo)
    cod_especialidade = Trim(cod_especialidade)
    dta_episodio = Trim(dta_episodio)
    
    cod_medico = BL_SelCodigo("SL_REQUIS", "cod_med", "N_REQ", n_req)
    
    If (Trim(dta_episodio) = "") Then
        dta_analise = "'" & Format(Date, gFormatoData) & "'"
    Else
        dta_analise = "'" & Format(dta_episodio, gFormatoData) & "'"
    End If
    
    If (Len(cod_analise) = 0) Or _
       (Len(dta_episodio) = 0) Then
        ' Par�metros incompletos.
        SISBIT_Remove_ANALISES_IN_OLD = -2
        Exit Function
    End If
    
    ' Mapeia a situa��o do SISLAB para o SONHO.
    Select Case situacao
        Case gT_Urgencia
            cod_modulo = "URG"
        Case gT_Consulta
            If gLAB = "HPOVOA" Then retorno = "P"
            'If cod_analise = gCodAnaSONHO Then Retorno = "A"
            cod_modulo = "CON"
        Case gT_Internamento
            cod_modulo = "INT"
        Case gT_Externo
            cod_modulo = "HDI"
        Case gT_LAB
            cod_modulo = "LAB"
        Case gT_Ambulatorio
            cod_modulo = "AMB"
        Case Else
            cod_modulo = ""
    End Select
    
    ' Mapeia a an�lise do SISLAB para o SONHO
    
    If (Len(cod_modulo) > 0) And _
       (Len(num_episodio) > 0) Then
    
        
        If Not (IsNumeric(num_episodio)) Then
            ' N� de epis�dio n�o � um n�mero.
            SISBIT_Remove_ANALISES_IN_OLD = -3
            Exit Function
        End If
        
        ' Caso 1 : Situa��o (m�dulo) + epis�dio
        cod_modulo = "'" & cod_modulo & "'"
        num_episodio = num_episodio
        cod_especialidade = cod_especialidade
        dta_episodio = "'" & Format(Trim(dta_episodio), gFormatoData) & "'"
    Else
        ' Caso 2 : Situa��o (m�dulo) + processo + data de epis�dio + cod_especialidade.
        
        cod_modulo = "'" & cod_modulo & "'"
        num_processo = num_processo
        dta_episodio = "'" & Format(Trim(dta_episodio), gFormatoData) & "'"
        cod_especialidade = cod_especialidade
        num_episodio = "NULL"
    
    End If
    
    
        sql = "INSERT INTO sl_analises_in " & vbCrLf & _
              "( " & vbCrLf & _
              "     cod_modulo, " & vbCrLf & _
              "     num_episodio, " & vbCrLf & _
              "     num_processo, " & vbCrLf & _
              "     dta_episodio, " & vbCrLf & _
              "     dta_analise, " & vbCrLf & _
              "     cod_especialidade, " & vbCrLf & _
              "     cod_analise, " & vbCrLf & _
              "     quantidade, " & vbCrLf & _
              "     retorno,cod_ana_sislab, " & vbCrLf & _
              "     n_req, cod_perfil, cod_ana_c, cod_ana_s, cod_agrup, cod_micro, cod_gr_antibio,cod_antibio, cod_prova, centro_custo " & vbCrLf
            If gEnviaLocalSONHO = mediSim Then
                sql = sql & ", cod_aparelho "
            End If
            sql = sql & ") " & vbCrLf & _
              "VALUES " & vbCrLf & _
              "( " & vbCrLf & _
              "     " & cod_modulo & ", " & vbCrLf & _
              "     " & num_episodio & ", " & vbCrLf & _
              "     " & BL_TrataStringParaBD(num_processo) & ", " & vbCrLf & _
              "     " & dta_episodio & ", " & vbCrLf & _
              "     " & dta_analise & ", " & vbCrLf & _
              "     " & cod_especialidade & ", " & vbCrLf & _
              "     '" & cod_analise & "', " & vbCrLf & _
              "     -1, " & vbCrLf & _
              "     'I','" & cod_ana_sislab & "', " & vbCrLf & _
              "     " & n_req & ",'" & Cod_Perfil & "', '" & cod_ana_c & "','" & _
              "      " & cod_ana_s & "','" & cod_agrup & "','" & cod_micro & "','" & Cod_Gr_Antibio & "','" & _
              "     " & cod_antibio & "','" & Cod_Prova & "'," & BL_TrataStringParaBD(centro_custo)
            If gEnviaLocalSONHO = mediSim Then
                sql = sql & ",  " & CodLocalFact
            End If
            sql = sql & ")"
    
    
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
    
        
    'Escrever ficheiros para a SISBIT
    Dim Fact As Integer
    Dim FactLocal As Integer
    Dim Erro As Integer
    
    qtd = -1
    
    FactLocal = FreeFile
    Open gDirCliente & "\bin\H_" & n_req & ".txt" For Append As #FactLocal 'Testes
    Print #FactLocal, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT & "|" & cod_medico
    Close #FactLocal
    
    Fact = FreeFile
    If Dir(gCaminhoFacturacaoSISBIT & "\*.*") <> "" Then
        Open gCaminhoFacturacaoSISBIT & "\H_" & n_req & ".txt" For Append As #Fact
    Else
        If Dir("L:\Facturacao\*.*") <> "" Then
            Open "L:\Facturacao\H_" & n_req & ".txt" For Append As #Fact
        ElseIf Dir("L:\Facturacao\ssangue\*.*") <> "" Then
            Open "L:\Facturacao\ssangue\H_" & n_req & ".txt" For Append As #Fact
        Else
            If gCaminhoFacturacaoSISBIT <> "-1" And gCaminhoFacturacaoSISBIT <> "0" And gCaminhoFacturacaoSISBIT <> "" Then
                Open gCaminhoFacturacaoSISBIT & "\H_" & n_req & ".txt" For Append As #Fact
            End If
        End If
    End If
    'Print #Fact, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT
    Print #Fact, NumProcesso_SISBIT & "|" & NumEpisodio_SISBIT & "|" & n_req & "|" & CodEFR_SISBIT & "|" & CodAnalise_SISBIT & "|" & qtd & "|" & CodEspecialidade_SISBIT & "|" & DtaEpisodio_SISBIT & "|" & cod_medico
    Close #Fact
    
    SISBIT_Remove_ANALISES_IN_OLD = 1
    Exit Function
    
TrataErro:
    SISBIT_Remove_ANALISES_IN_OLD = -1
    Exit Function
    Resume Next
    
End Function

' --------------------------------------------------------------------------------------

'  QUANDO UTILIZADOR QUER FACTURAR NOVAMENTE ENTAO ENVIA O QUE JA EXISTIA PARA HISTORICO

' --------------------------------------------------------------------------------------
Public Function SISBIT_EnviaParaHistorico(n_req As String) As Boolean
    Dim sSql As String
    Dim rsSISBIT As New ADODB.recordset
    Dim iRegistos As Integer
    On Error GoTo TrataErro
    SISBIT_EnviaParaHistorico = False
    
    sSql = "SELECT * FROM sl_analises_in WHERE n_req = " & n_req
    rsSISBIT.CursorLocation = adUseServer
    rsSISBIT.CursorType = adOpenStatic
    rsSISBIT.Source = sSql
    rsSISBIT.ActiveConnection = gConexao
    rsSISBIT.Open
    If rsSISBIT.RecordCount > 0 Then
        While Not rsSISBIT.EOF
            sSql = "INSERT INTO sl_analises_in_h (cod_modulo, num_episodio, num_processo, dta_episodio,"
            sSql = sSql & " cod_especialidade, cod_analise, quantidade, retorno,dta_analise, n_req, cod_ana_sislab,"
            sSql = sSql & " cod_perfil,cod_ana_c, cod_ana_s, cod_micro, cod_gr_antibio,cod_antibio, cod_prova, "
            sSql = sSql & " cod_agrup, centro_custo,cod_aparelho, dt_cri, dt_hist, user_hist) VALUES ( "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISBIT!cod_modulo, "")) & ", "
            sSql = sSql & BL_HandleNull(rsSISBIT!num_episodio, "Null") & ", "
            sSql = sSql & BL_HandleNull(rsSISBIT!num_processo, "null") & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsSISBIT!dta_episodio, "")) & ", "
            sSql = sSql & BL_HandleNull(rsSISBIT!cod_especialidade, "null") & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISBIT!cod_analise, "")) & ", "
            sSql = sSql & BL_HandleNull(rsSISBIT!quantidade, "1") & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISBIT!retorno, "")) & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsSISBIT!dta_analise, "")) & ", "
            sSql = sSql & BL_HandleNull(rsSISBIT!n_req, "null") & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISBIT!cod_ana_sislab, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISBIT!Cod_Perfil, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISBIT!cod_ana_c, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISBIT!cod_ana_s, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISBIT!cod_micro, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISBIT!Cod_Gr_Antibio, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISBIT!cod_antibio, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISBIT!Cod_Prova, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsSISBIT!cod_agrup, "")) & ", "
            sSql = sSql & BL_HandleNull(rsSISBIT!centro_custo, "null") & ", "
            sSql = sSql & BL_HandleNull(rsSISBIT!cod_aparelho, "null") & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsSISBIT!dt_cri, "")) & ", "
            sSql = sSql & "sysdate, "
            sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ") "
            iRegistos = BG_ExecutaQuery_ADO(sSql)
            If iRegistos < 1 Then
                GoTo TrataErro
            End If
            rsSISBIT.MoveNext
        Wend
        
        'APAGA DA SL_ANALISES_IN
        sSql = "DELETE FROM sl_analises_in WHERE n_Req = " & n_req
        iRegistos = BG_ExecutaQuery_ADO(sSql)
        If iRegistos < 1 Then
            GoTo TrataErro
        End If
        
        'ALTERA ESTADO NA SL_REALIZA
        sSql = "UPDATE sl_realiza SET flg_facturado = 0 WHERE n_Req = " & n_req
        iRegistos = BG_ExecutaQuery_ADO(sSql)
        If iRegistos < 1 Then
            GoTo TrataErro
        End If
        
        'ALTERA ESTADO NA SL_MARCACOES
        sSql = "UPDATE sl_marcacoes SET flg_facturado = 0 WHERE n_Req = " & n_req
        iRegistos = BG_ExecutaQuery_ADO(sSql)
        If iRegistos < 1 Then
            GoTo TrataErro
        End If
        
        End If
    rsSISBIT.Close
    Set rsSISBIT = Nothing
    SISBIT_EnviaParaHistorico = True
Exit Function
TrataErro:
    SISBIT_EnviaParaHistorico = False
    BG_LogFile_Erros "SISBIT_EnviaParaHistorico: " & Err.Description & " " & sSql, "SISBIT", "SISBIT_EnviaParaHistorico", False
    Exit Function
End Function



'RGONCALVES 16.09.2013 CHLeiria-Pombal-1
Function SISBIT_Devolve_Cod_Servico_sonho_sislab(cod_servico As String) As String
    Dim sql As String
    Dim rs As New ADODB.recordset
    
    sql = "SELECT cod_sonho FROM sl_proven WHERE cod_proven = " & cod_servico
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    If rs.RecordCount > 0 Then
        SISBIT_Devolve_Cod_Servico_sonho_sislab = BL_HandleNull(rs!cod_sonho, "")
    End If
    rs.Close
    Set rs = Nothing
End Function


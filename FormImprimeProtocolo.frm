VERSION 5.00
Begin VB.Form FormImprimeProtocolo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Reimpress�o de Protocolos"
   ClientHeight    =   3915
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4620
   Icon            =   "FormImprimeProtocolo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3915
   ScaleWidth      =   4620
   ShowInTaskbar   =   0   'False
   Begin VB.OptionButton OptOrdena 
      Caption         =   "Ordenar Proveni�ncia"
      Height          =   255
      Index           =   1
      Left            =   2280
      TabIndex        =   21
      Top             =   3480
      Width           =   2175
   End
   Begin VB.OptionButton OptOrdena 
      Caption         =   "Ordenar Requisi��o"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   20
      Top             =   3480
      Width           =   1815
   End
   Begin VB.TextBox EcCodProveniencia 
      BackColor       =   &H80000018&
      Height          =   285
      Left            =   1080
      TabIndex        =   2
      Top             =   600
      Width           =   615
   End
   Begin VB.TextBox EcDescrProveniencia 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   600
      Width           =   2415
   End
   Begin VB.CommandButton BtPesquisaProveniencia 
      Height          =   315
      Left            =   4080
      Picture         =   "FormImprimeProtocolo.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   17
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   600
      Width           =   375
   End
   Begin VB.TextBox EcGrAnalises 
      BackColor       =   &H80000018&
      Height          =   285
      Left            =   1080
      TabIndex        =   3
      Top             =   1080
      Width           =   615
   End
   Begin VB.TextBox EcDescrGrAnalises 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   1080
      Width           =   2415
   End
   Begin VB.CommandButton BtPesquisaRapidaGrAnalises 
      Height          =   375
      Left            =   4080
      Picture         =   "FormImprimeProtocolo.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   14
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   1080
      Width           =   375
   End
   Begin VB.ComboBox CbSituacao 
      Height          =   315
      Left            =   1080
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   120
      Width           =   3375
   End
   Begin VB.Frame Frame2 
      Caption         =   "Hora de emiss�o das Requisi��es "
      Height          =   855
      Left            =   120
      TabIndex        =   10
      Top             =   2520
      Width           =   4335
      Begin VB.TextBox EcHrFim 
         Height          =   285
         Left            =   2880
         TabIndex        =   7
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox EcHrInicio 
         Height          =   285
         Left            =   480
         TabIndex        =   6
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "at�"
         Height          =   255
         Left            =   2040
         TabIndex        =   12
         Top             =   360
         Width           =   375
      End
      Begin VB.Label Label3 
         Caption         =   "De"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   360
         Width           =   255
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Data de emiss�o das Requisi��es "
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   1560
      Width           =   4335
      Begin VB.TextBox EcDtInicio 
         Height          =   285
         Left            =   480
         TabIndex        =   4
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   2880
         TabIndex        =   5
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "De"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label2 
         Caption         =   "at�"
         Height          =   255
         Left            =   2040
         TabIndex        =   8
         Top             =   360
         Width           =   375
      End
   End
   Begin VB.Label Label9 
      Caption         =   "&Proveni�ncia"
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Grupo"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   16
      Top             =   1080
      Width           =   615
   End
   Begin VB.Label Label5 
      Caption         =   "Situa��o"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "FormImprimeProtocolo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim estado As Integer

Public Function Funcao_DataActual()
       
    Select Case CampoActivo.Name
        Case "EcDtInicio", "EcDtFim"
            CampoActivo.Text = Bg_DaData_ADO
        Case "EcHrInicio", "EcHrFim"
            CampoActivo.Text = Bg_DaHora_ADO
    End Select

End Function

Private Sub DefTipoCampos()

    'Tipo Data
    EcCodProveniencia.Tag = "200"
    EcCodProveniencia.MaxLength = 5
    EcDtInicio.Tag = mediTipoData
    EcDtFim.Tag = mediTipoData
    EcHrInicio.Tag = mediTipoHora
    EcHrFim.Tag = mediTipoHora
    
    'Tipo Inteiro
    EcDtInicio.MaxLength = 10
    EcDtFim.MaxLength = 10
    EcHrInicio.MaxLength = 8
    EcHrFim.MaxLength = 8
    OptOrdena(0).value = True
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
        
End Sub

Public Sub FuncaoImprimir()
    
    Dim sql As String
    Dim RegReq As ADODB.recordset
    Dim i As Long
    Dim s As String
    Dim total As Long
    Dim continua As Boolean
    Dim Existe As Boolean
    Dim ContaReq As Integer
    Dim InsereProt As Boolean
    Dim lastprovreq As String
    Dim DescrReq As String
    Dim NomeUte As String
    Dim hr_imp As String
    Dim sSql As String
    
    sSql = "DELETE FROM SL_CR_PROT WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Mapa de Protocolos") = True Then Exit Sub
    
    'Verifica os campos nulos
    If Trim(EcDtInicio.Text) = "" Then
        BG_Mensagem mediMsgBox, "Indique a data de emiss�o inicial! ", vbOKOnly + vbExclamation, "SISLAB"
        EcDtInicio.SetFocus
        Exit Sub
    End If
    If Trim(EcDtFim.Text) = "" Then
        BG_Mensagem mediMsgBox, "Indique a data de emiss�o final! ", vbOKOnly + vbExclamation, "SISLAB"
        EcDtFim.SetFocus
        Exit Sub
    End If
    If Trim(EcHrInicio.Text) = "" Then
        BG_Mensagem mediMsgBox, "Indique a hora de emiss�o inicial! ", vbOKOnly + vbExclamation, "SISLAB"
        EcHrInicio.SetFocus
        Exit Sub
    End If
    If Trim(EcHrFim.Text) = "" Then
        BG_Mensagem mediMsgBox, "Indique a hora de emiss�o final! ", vbOKOnly + vbExclamation, "SISLAB"
        EcHrFim.SetFocus
        Exit Sub
    End If
    
    'Abre RecordSet
    Set RegReq = New ADODB.recordset
    RegReq.CursorType = adOpenStatic
    RegReq.CursorLocation = adUseServer
    RegReq.ActiveConnection = gConexao
    sql = "SELECT sl_requis.n_req n_req, sl_requis.dt_imp dt_imp ,sl_requis.hr_imp hr_imp," & _
          "sl_proven.descr_proven descr_proven," & _
          "sl_identif.seq_utente seq_utente ,sl_identif.utente utente ,sl_identif.t_utente t_utente, sl_identif.nome_ute nome_ute" & _
          " FROM sl_requis,sl_identif," & IIf(gSGBD = gInformix, "OUTER sl_proven ", "sl_proven ") & _
          "WHERE sl_requis.seq_utente=sl_identif.seq_utente "
          Select Case gSGBD
              Case gInformix
                  sql = sql & "AND sl_requis.cod_proven=sl_proven.cod_proven  "
              Case gOracle
                  sql = sql & "AND sl_requis.cod_proven=sl_proven.cod_proven(+)  "
              Case gSqlServer
                  sql = sql & "AND sl_requis.cod_proven*=sl_proven.cod_proven  "
          End Select
    sql = sql & "AND sl_requis.dt_imp IS NOT NULL AND sl_requis.dt_imp2 is null AND (sl_requis.dt_imp BETWEEN " & BL_TrataDataParaBD(EcDtInicio.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text) & ") "
    If CbSituacao.ListIndex > -1 Then
        sql = sql & " AND sl_requis.t_sit = " & CbSituacao.ItemData(CbSituacao.ListIndex)
    End If
    If EcCodProveniencia <> "" Then
        sql = sql & " AND sl_requis.cod_proven = " & BL_TrataStringParaBD(EcCodProveniencia)
    End If
    If EcGrAnalises <> "" Then
        sql = sql & " AND (trim(sl_requis.gr_ana) ='" & EcGrAnalises & "' OR "
        sql = sql & " trim(sl_requis.gr_ana) like '%;" & EcGrAnalises & ";%' OR "
        sql = sql & " trim(sl_requis.gr_ana) like '%;" & EcGrAnalises & "' OR "
        sql = sql & " trim(sl_requis.gr_ana) like '" & EcGrAnalises & ";%') "
    End If
    
    sql = sql & "UNION SELECT sl_requis.n_req n_req, sl_requis.dt_imp2 dt_imp ,sl_requis.hr_imp2 hr_imp," & _
          "sl_proven.descr_proven descr_proven," & _
          "sl_identif.seq_utente seq_utente ,sl_identif.utente utente ,sl_identif.t_utente t_utente, sl_identif.nome_ute nome_ute" & _
          " FROM sl_requis,sl_identif," & IIf(gSGBD = gInformix, "OUTER sl_proven ", "sl_proven ") & _
          "WHERE sl_requis.seq_utente=sl_identif.seq_utente "
          Select Case gSGBD
              Case gInformix
                  sql = sql & "AND sl_requis.cod_proven=sl_proven.cod_proven  "
              Case gOracle
                  sql = sql & "AND sl_requis.cod_proven=sl_proven.cod_proven(+)  "
              Case gSqlServer
                  sql = sql & "AND sl_requis.cod_proven*=sl_proven.cod_proven  "
          End Select
    sql = sql & "AND sl_requis.dt_imp2 IS NOT NULL AND (sl_requis.dt_imp2 BETWEEN " & BL_TrataDataParaBD(EcDtInicio.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text) & ") "
    If CbSituacao.ListIndex > -1 Then
        sql = sql & " AND sl_requis.t_sit = " & CbSituacao.ItemData(CbSituacao.ListIndex)
    End If
    If EcCodProveniencia <> "" Then
        sql = sql & " AND sl_requis.cod_proven = " & BL_TrataStringParaBD(EcCodProveniencia)
    End If
    If EcGrAnalises <> "" Then
        sql = sql & " AND (trim(sl_requis.gr_ana) ='" & EcGrAnalises & "' OR "
        sql = sql & " trim(sl_requis.gr_ana) like '%;" & EcGrAnalises & ";%' OR "
        sql = sql & " trim(sl_requis.gr_ana) like '%;" & EcGrAnalises & "' OR "
        sql = sql & " trim(sl_requis.gr_ana) like '" & EcGrAnalises & ";%') "
    End If
    If OptOrdena(0).value = True Then
        RegReq.Source = sql & " ORDER BY n_req ASC "
    ElseIf OptOrdena(1).value = True Then
        RegReq.Source = sql & " ORDER BY descr_proven ASC ,n_req ASC "
    End If
    RegReq.Open
    total = RegReq.RecordCount
    
    'Verifica se existem registos para imprimir
    If total = 0 Then
        RegReq.Close
        Set RegReq = Nothing
        Call BG_Mensagem(mediMsgBox, "N�o existem requisi��es para listar!", vbOKOnly + vbInformation, App.ProductName)
        Exit Sub
    End If
    
    'Mostra a caixa de di�logo da impressora
    If gImprimirDestino = crptToWindow Then
        continua = BL_IniciaReport("Protocolo", "Mapa de Protocolos", crptToWindow, , , , , , , "Imprimir Protocolo")
    Else
        continua = BL_IniciaReport("Protocolo", "Mapa de Protocolos", crptToPrinter, , , , , , , "Imprimir Protocolo")
    End If
    If continua = False Then
        RegReq.Close
        Set RegReq = Nothing
        Exit Sub
    End If
    
    Existe = False
    ContaReq = 0
    InsereProt = False
    lastprovreq = ""
    DescrReq = ""
    For i = 1 To total
        If Not BL_HandleNull(RegReq!descr_proven, "") = "" Then
            If Not BL_HandleNull(RegReq!hr_imp, "") = "" Then
                If Mid(CStr(RegReq!hr_imp), Len(CStr(RegReq!hr_imp))) = ":" Then
                    hr_imp = CStr(RegReq!hr_imp) & "00"
                Else
                    hr_imp = CStr(RegReq!hr_imp)
                End If
                If TimeValue(hr_imp) >= TimeValue(Trim(EcHrInicio.Text)) And TimeValue(hr_imp) <= TimeValue(Trim(EcHrFim.Text)) Then
                        s = RegReq!n_req & Replace(Space(9 - Len("" & RegReq!n_req)), " ", ".") & "(" & RegReq!t_utente & "/" & RegReq!Utente & ") " & Space(5 - Len("" & RegReq!t_utente)) & Space(10 - Len("" & RegReq!Utente)) & RegReq!nome_ute & Space(80 - Len(Mid(RegReq!nome_ute, 1, 79)))
                        DescrReq = s
                        ContaReq = ContaReq + 1
                        NomeUte = RegReq!nome_ute
                            Existe = True
                            sql = "INSERT INTO SL_CR_PROT " & _
                                  " (DESCR_PROVEN, DESCR_REQ, n_req, nome_computador, num_sessao ) " & _
                                  "VALUES (" & _
                                  BL_TrataStringParaBD(RegReq!descr_proven) & ","
                            ContaReq = 1
                            lastprovreq = "" & RegReq!descr_proven
                            DescrReq = RegReq!n_req & Replace(Space(9 - Len("" & RegReq!n_req)), " ", ".") & "(" & RegReq!t_utente & "/" & RegReq!Utente & ") " & Space(5 - Len("" & RegReq!t_utente)) & Space(10 - Len("" & RegReq!Utente))
                            sql = sql & BL_TrataStringParaBD(DescrReq) & "," & BL_HandleNull(RegReq!n_req, "") & ", "
                            sql = sql & BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ")"
                            BG_ExecutaQuery_ADO sql
                End If
            End If
        End If
        RegReq.MoveNext
    Next i
        
    If Existe = False Then
        'Desaloca RecordSet principal
        RegReq.Close
        Set RegReq = Nothing
        
        Call BG_Mensagem(mediMsgBox, "N�o existem requisi��es para listar!", vbOKOnly + vbInformation, App.ProductName)
        Exit Sub
    End If
        
    Dim Report As CrystalReport
    Set Report = MDIFormInicio.Report
    Report.SQLQuery = "SELECT DESCR_PROVEN, DESCR_REQ, n_req " & _
                      "FROM SL_CR_PROT WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    If OptOrdena(0).value = True Then
        Report.formulas(0) = "OrderReq=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(0) = "OrderReq=" & BL_TrataStringParaBD("N")
    End If
    
    Call BL_ExecutaReport
    
    sSql = "DELETE FROM SL_CR_PROT WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
    
    RegReq.Close
    Set RegReq = Nothing
    
End Sub

Private Sub Inicializacoes()
    
    Me.left = 540
    Me.top = 450
    Me.Width = 4635
    Me.Height = 4350 ' Normal
    
    Me.caption = "Reimpress�o de Protocolos"
        
    'Campo com Focus
    Set CampoDeFocus = EcDtInicio
    
    Call DefTipoCampos
        
    EcDtFim.Text = Bg_DaData_ADO
    EcHrFim.Text = Bg_DaHora_ADO
           
End Sub

Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    
    Set gFormActivo = Me
    
    If CampoDeFocus.Enabled = True Then
        CampoDeFocus.SetFocus
    End If

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
                    
    Me.MousePointer = vbArrow
    
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
        
    Call Inicializacoes
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    
End Sub

Private Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Call BL_FechaPreview("Mapa de Protocolos")
    
    Set FormImprimeProtocolo = Nothing
     
End Sub

Public Sub FuncaoLimpar()
    
    EcDtInicio.Text = ""
    EcHrInicio.Text = ""
    EcDtFim.Text = Bg_DaData_ADO
    EcHrFim.Text = Bg_DaHora_ADO
    EcGrAnalises = ""
    EcGrAnalises_Validate False
    EcCodProveniencia = ""
    EcCodProveniencia_Validate False
    CbSituacao.ListIndex = -1
End Sub

Private Sub BtPesquisaProveniencia_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.Text = resultados(1)
            EcDescrProveniencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub



Private Sub BtPesquisaRapidaGrAnalises_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupos de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcGrAnalises.Text = resultados(1)
            EcDescrGrAnalises.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub


Private Sub EcDtFim_GotFocus()
    
    Set CampoActivo = EcDtFim
    
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtFim)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
        
End Sub

Private Sub EcDtInicio_GotFocus()
    
    Set CampoActivo = EcDtInicio
    
End Sub

Private Sub EcDtInicio_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtInicio)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub EcHrFim_GotFocus()
    
    Set CampoActivo = EcHrFim
    
End Sub

Private Sub EcHrFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcHrFim)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub EcHrInicio_GotFocus()
    
    Set CampoActivo = EcHrInicio
    
End Sub

Private Sub EcHrInicio_Validate(Cancel As Boolean)
    
    EcHrInicio = Format(EcHrInicio, gFormatoHora)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcHrInicio)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub Form_Activate()
    
    Call EventoActivate
    
End Sub

Private Sub Form_Load()

    Call EventoLoad
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub

Private Sub CriaTabela()
    
    Dim Protocolo(1 To 3) As DefTable
    
    'Protocolo
    Protocolo(1).NomeCampo = "DESCR_PROVEN"
    Protocolo(1).tipo = "STRING"
    Protocolo(1).tamanho = "30"
    
    Protocolo(2).NomeCampo = "DESCR_REQ"
    Protocolo(2).tipo = "STRING"
    Protocolo(2).tamanho = "1000"
    
    Protocolo(3).NomeCampo = "NOME"
    Protocolo(3).tipo = "STRING"
    Protocolo(3).tamanho = "100"
    
    Call BL_CriaTabela("SL_CR_PROT" & gNumeroSessao, Protocolo)
        
End Sub
Private Sub EcGrAnalises_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub
Private Sub EcGrAnalises_Validate(Cancel As Boolean)
    Dim RsDescrGrAnalises As ADODB.recordset
    
    If Trim(EcGrAnalises.Text) <> "" Then
        Set RsDescrGrAnalises = New ADODB.recordset
        
        With RsDescrGrAnalises
            .Source = "SELECT descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana= " & BL_TrataStringParaBD(EcGrAnalises.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrAnalises.RecordCount > 0 Then
            EcDescrGrAnalises.Text = RsDescrGrAnalises!descr_gr_ana
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
        Else
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
            EcDescrGrAnalises.Text = ""
            BG_Mensagem mediMsgBox, "O Grupo de An�lise indicado n�o existe!", vbOKOnly + vbExclamation, "Aten��o!"
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrGrAnalises.Text = ""
    End If
    
End Sub


Private Sub EcCodProveniencia_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub
Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProveniencia.Text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_proven FROM sl_proven WHERE cod_proven= " & BL_TrataStringParaBD(EcCodProveniencia.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrProveniencia.Text = "" & RsDescrProv!descr_proven
        Else
            Cancel = True
            EcDescrProveniencia.Text = ""
            BG_Mensagem mediMsgBox, "A Proveni�ncia indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub



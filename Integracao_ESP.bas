Attribute VB_Name = "Integracao_ESP"
Private Const moduleName = "Integracao_ESP"

Public Const cINSUCESSO = "ERRO"
Public Const cSUCESSO = "SUCESSO"

'Public Enum EstadoCativacao
'    Erro = 0
'    sucesso = 1
'End Enum

Public Type ESPOperationError
    OpErrId As String
    errorCode As String
    errorDescr As String
    codexame As String
End Type

Public Type ESPPrescriptionLocal
    LocalPrescId As String
    LocalPrescCode As String
    LocalPrescDescr As String
    LocalPrescDomain As String
    'Entidades MAPEADAS
    LPrescCodeSISLAB As String
    LPrescDescrSISLAB As String
    '
End Type

Public Type ESPPatient
    PatientId As String
    SNS As String
    'Entidades MAPEADAS
    snsSISLAB As String
    '
    Name As String
    birthDate As String
    sex As String
End Type

Public Type ESPEntity
   entityId As String
   codEntity As String
   EntityDescr As String
   entityDomain As String
   Country As String
   'Entidades MAPEADAS
   codEntitySISLAB  As String
   EntityDescrSISLAB  As String
   '
End Type


Public Type ESPRequisitionDet
    reqDetId As String
    CodigoMSP As String
    mspDescr As String
    codAreaMSP As String
    AreaMSPDescr As String
    mcdtID As String
    mcdtCodCPAL As String
    'ENTIDADES MAPEADAS
    mcdtCodSISLAB As String
    '
    mcdtDescr As String
    MCDTStateCode As String
    MCDTSateDescr As String
    LateralityCOde As String
    lateralityDescr As String
    ProductCode As String
    productDescr As String
    nSamples As Integer
    ClinicalInfoDet As String
    AuthRequered As String
    InsetTax As String
    internalize As String
    price As String
    moderatorTax As String
        cod_isencao As String
End Type

Public Type ESPRequisition
    reqID As String
    ReqDate As String
    validationDate As String
    nVias As String
    codIntity As String
    clinicalInfo As String
    episodeId As String
    consent As String
    DomicileExam As String
    UrgentExam As String
    notifType As String
    ExternalEmission As String
    NumCodProfissional As String
    'ENTIDADES MAPEADAS
    NumCodProfSISLAB  As String
    nomeMedico As String
    '
    pin As String
    ReqNumber As String
    ReqSate As String
    'Adicionado novos campos
    DomicileExamJust As String
    UrgentExamJust As String
    '
    DetReq() As ESPRequisitionDet
    PrescrLocal As ESPPrescriptionLocal
    Patient As ESPPatient
    Entity As ESPEntity
    'Instanciar um estrutura para as an�lises da requiis��o que podem ser mapeadadas para o codigo de conven��o enviado.
End Type

Public Type ESPOperation
    OperationId As String
    pin As String
    ReqNumber As String
    codPrestacao As String
    Operation_Code As String
    Description As String
    OperationDate As String
    OperationUser As String
    OpState As String
    codLocal As String
    Origin As String
    OperationType As String
    'Estado Req no Sislab
    ExistFgAna As String
    first As String
    ErrorOp() As ESPOperationError
    req As ESPRequisition
End Type

Private estrutOperation() As ESPOperation

Dim contaErros As Integer
Dim contaMCDTS As Integer

'edgar.parada Glintt-HS-19165 09.08.2018 Chamada ao servi�o para preencher as tabelas e estrutura de dados
'O sns do doente � validado na BD que se passa como parametro.
'Valida no formIdUtente se o SNS est� preenchifo, se n�o tiver obriga a preencher antes de lan�ar o form FormGestaoRequisicoesPrivado
Public Function ESP_CativaPedido(ByRef estrutOp() As ESPOperation, ByVal EntPrestadora As String, ByVal medicoPresc As String, _
    ByVal efrDef As String, ByVal pSNS As String) As ESPOperation()
    Dim methodName As String
    methodName = "ESP_CativaPedido"
    Dim nReqId As Long
    Dim nOpId As Long
    Dim p_nReq As String
    Dim efrState As Boolean
    Dim pos As Integer
    Dim p_sns As String
    
    Dim cmd As New adodb.Command
    
    pos = 1
    
    estrutOperation = estrutOp
    
    On Error GoTo TrataErro
    For i = 1 To UBound(estrutOperation)
      If estrutOperation(i).OpState = "" Then 'And estrutOperation(i).stateExpira <> "1" Then
          p_pin = estrutOperation(i).pin
          p_nReq = estrutOperation(i).ReqNumber
          'p_sns = ESP_GetSNSDoente(codUtente)
          
          Set cmd = New adodb.Command
         'estrutOperation(1).codLocal = "CPCHS_LOCAL_HSDEV"
          With cmd
                .ActiveConnection = gConexao
                .CommandText = "PCK_INTEGRACAO_ESP.cativa_pedido"
                .CommandType = adCmdStoredProc
                .Parameters.Append .CreateParameter("p_sns", adVarChar, adParamInput, 10, pSNS)
                .Parameters.Append .CreateParameter("p_cod_acesso", adVarChar, adParamInput, Len(p_pin), p_pin)
                .Parameters.Append .CreateParameter("p_n_req", adVarChar, adParamInput, Len(p_nReq), p_nReq)
                .Parameters.Append .CreateParameter("p_cod_local", adVarChar, adParamInput, Len(estrutOperation(i).codLocal), estrutOperation(i).codLocal)
                .Parameters.Append .CreateParameter("p_cod_prestacao", adVarChar, adParamInput, Len(estrutOperation(i).codPrestacao), estrutOperation(i).codPrestacao)
                .Parameters.Append .CreateParameter("p_user", adVarChar, adParamInput, 50, gCodUtilizador)
                .Parameters.Append .CreateParameter("p_origem", adVarChar, adParamInput, 10, "SISLAB")
                                .Parameters.Append .CreateParameter("p_valida_sns", adVarChar, adParamInput, 2, "S")
                                .Parameters.Append .CreateParameter("p_cod_ent", adVarChar, adParamInput, Len(gEntidadeESP), gEntidadeESP)
                .Parameters.Append .CreateParameter("p_estado", adVarChar, adParamOutput, 10)
                .Parameters.Append .CreateParameter("p_reqid", adNumeric, adParamOutput, 20)
                .Parameters.Append .CreateParameter("p_opId", adNumeric, adParamOutput, 20)
                .Parameters.Append .CreateParameter("o_msg_erro", adVarChar, adParamOutput, 4000)
          End With
    
          cmd.Execute
    
          If cmd.Parameters.Item("p_estado").value = cINSUCESSO Then
              'If p_opId = Empty Then
                                nReqId = BL_HandleNull(cmd.Parameters.Item("p_reqid").value, 0)
                nOpId = BL_HandleNull(cmd.Parameters.Item("p_opId").value, 0)
                                
                estrutOperation(i).OpState = cINSUCESSO
                ReDim Preserve estrutOperation(i).ErrorOp(pos + 1)
                estrutOperation(i).ErrorOp(pos).errorCode = "ERRO"
                estrutOperation(i).ErrorOp(pos).errorDescr = BL_HandleNull(cmd.Parameters.Item("o_msg_erro").value, "")
             ' Else
                Esp_PreencheEstrutura nReqId, nOpId, i ', True
             ' End If
          ElseIf cmd.Parameters.Item("p_estado").value = cSUCESSO And BL_HandleNull(cmd.Parameters.Item("o_msg_erro").value, "") = "" Then
                nReqId = cmd.Parameters.Item("p_reqid").value
                nOpId = cmd.Parameters.Item("p_opId").value
                
                                estrutOperation(i).OpState = cSUCESSO
                estrutOperation(i).req.Patient.snsSISLAB = pSNS
                'Preenche a estrutura com a informa��o da BD
                Esp_PreencheEstrutura nReqId, nOpId, i ', False
                                
                                'NELSONPSILVA 19.12.2018
                'Valida se o local de prescri��o existe (SL_PROVEN)
                ESP_ValidaLocalPresc i, nOpId
                '
                'Valida se a EFR est� mapeada, lan�a erro em caso de insuceso e muda o estado da opera��o
                ESP_ValidaEFR i, nOpId
                'Valida se o local de prescri��o existe (SL_PROVEN)
                'A patida as entidades fornecidas pelo SPMS v�o estar nesta tabela n�o sendo necess�rio mapeamentoo
                'Caso de incusesso lan�a erro e muda o estado da opera��o
               ' ESP_ValidaLocalPresc EntPrestadora, i, nOpId
                'Valida se o m�dico existe (SL_MEDICOS)
                'Cria se n�o existir, o nome ficou definido ser 'M�DICO N. ORDEM X'
                'CAUSA DO NOME: o servi�o so devolve o n� de cedula profissional (SITUA��O PENDENTE)
                ESP_ValidaMedico medicoPresc, i
                'Valida se a analises se encontram mapeadas
                'Caso de insucesso lan�a erro e muda o estado da opera��o
                ESP_ValidaMDCT i, nOpId
                                'Valida Isen��o
                ESP_ValidaIsencao i, nOpId
                'Valida se requisi��o est� fora do prazo
'                If BL_HandleNull(estrutOperation(i).req.validationDate, "01/01/9999") < Bg_DaData_ADO Then
'                    estrutOperation(i).stateExpira = "1" 'expirado
'                End If
                
          End If
      End If
    Next
    ESP_CativaPedido = estrutOperation
    Exit Function
TrataErro:
    ESP_CativaPedido = estrutOperation
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Function
    Resume Next
End Function
'edgar.parada Glintt-HS-19165 09.08.2018 Preenche a estutura
', ByVal Erro As Boolean
Public Sub Esp_PreencheEstrutura(ByVal nRqs As Long, ByVal opId As Long, ByVal i As Integer)
     Dim rsOpError As New adodb.recordset
     Dim rsReq As New adodb.recordset
     Dim methodName As String
     Dim sql As String
     Dim k As Integer
    
    contaErros = 1
    contaMCDTS = 1
    
    methodName = "Esp_PreencheEstrutura"
    
    On Error GoTo TrataErro
    
    sql = sql & "SELECT a.*, b.* "
    sql = sql & "FROM esp_operation a, esp_operation_detail b "
    sql = sql & "WHERE a.operation_id = " & opId & " AND a.operation_id = b.operation_id(+)"
    
    rsOpError.CursorLocation = adUseServer
    rsOpError.CursorType = adOpenStatic
    rsOpError.Open sql, gConexao
    
    If rsOpError.RecordCount > 0 Then
          While Not rsOpError.EOF
            ' estrutOperation(i).OpState = BL_HandleNull(rsOpError.Fields("operation_state").value, "")
             estrutOperation(i).Operation_Code = BL_HandleNull(rsOpError.Fields("operation_Code").value, "")
             'estrutOperation(i).Description = BL_HandleNull(rsOpError.Fields("OPERATION_DESCR").value, "")
             'estrutOperation(i).OperationType = BL_HandleNull(rsOpError.Fields("operation_type").value, "")
             estrutOperation(i).OperationId = BL_HandleNull(rsOpError.Fields("OPERATION_ID").value, opId)
             
             ReDim Preserve estrutOperation(i).ErrorOp(rsOpError.RecordCount)
        
             estrutOperation(i).ErrorOp(contaErros).codexame = BL_HandleNull(rsOpError.Fields("cod_exam").value, "")
             estrutOperation(i).ErrorOp(contaErros).errorCode = BL_HandleNull(rsOpError.Fields("error_code").value, "")
             estrutOperation(i).ErrorOp(contaErros).errorDescr = BL_HandleNull(rsOpError.Fields("error_description").value, "")
             estrutOperation(i).ErrorOp(contaErros).OpErrId = BL_HandleNull(rsOpError.Fields("OPERATION_ERR_ID").value, "")
             rsOpError.MoveNext
             contaErros = contaErros + 1
          Wend
          
     End If
     
    'If Erro = False Then
        
        sql = "SELECT b.*, c.*, d.*, e.*, f.*"
        sql = sql & " FROM esp_operation a, esp_requisition b, esp_requisition_det c, esp_prescription_local d, esp_patient e, esp_entity f"
        sql = sql & " WHERE A.req_id = b.req_id And b.req_id = c.req_id And d.local_presc_id = b.local_presc_id"
        sql = sql & " AND b.patient_id = e.patient_id AND b.entity_id = f.entity_id AND a.operation_id = " & opId & " AND b.req_id = " & nRqs
                '& " AND LOWER (b.req_state) = LOWER ('SUCESSO')"
        'sql = sql & " order by c.dt_cri"
        rsReq.CursorLocation = adUseServer
        rsReq.CursorType = adOpenStatic
        rsReq.Open sql, gConexao
        
        If rsReq.RecordCount > 0 Then
          While Not rsReq.EOF
            
             estrutOperation(i).req.ReqDate = BL_HandleNull(rsReq.Fields("req_date").value, "")
             estrutOperation(i).req.validationDate = BL_HandleNull(rsReq.Fields("validation_date").value, "")
             estrutOperation(i).req.nVias = BL_HandleNull(rsReq.Fields("n_vias").value, "")
             estrutOperation(i).req.codIntity = BL_HandleNull(rsReq.Fields("cod_entity_req").value, "")
             estrutOperation(i).req.clinicalInfo = BL_HandleNull(rsReq.Fields("clinical_info").value, "")
             estrutOperation(i).req.episodeId = BL_HandleNull(rsReq.Fields("episode_id").value, "")
             estrutOperation(i).req.consent = BL_HandleNull(rsReq.Fields("consent").value, "")
             estrutOperation(i).req.DomicileExam = BL_HandleNull(rsReq.Fields("domicile_exam").value, "")
             estrutOperation(i).req.UrgentExam = BL_HandleNull(rsReq.Fields("urgent_exam").value, "")
             estrutOperation(i).req.ExternalEmission = BL_HandleNull(rsReq.Fields("external_emission").value, "")
             estrutOperation(i).req.notifType = BL_HandleNull(rsReq.Fields("notification_type").value, "")
             estrutOperation(i).req.NumCodProfissional = BL_HandleNull(rsReq.Fields("num_c_professional").value, "")
             'estrutOperation(i).req.pin = BL_HandleNull(rsReq.Fields("pin").value, "")
             estrutOperation(i).req.ReqNumber = BL_HandleNull(rsReq.Fields("req_number").value, "")
             estrutOperation(i).req.ReqSate = BL_HandleNull(rsReq.Fields("req_state").value, "")
             estrutOperation(i).req.reqID = BL_HandleNull(rsReq.Fields("req_id").value, "")
             estrutOperation(i).req.DomicileExamJust = BL_HandleNull(rsReq.Fields("domicile_exam_just").value, "")
             estrutOperation(i).req.UrgentExamJust = BL_HandleNull(rsReq.Fields("urgent_exam_just").value, "")
       
             ReDim Preserve estrutOperation(i).req.DetReq(rsReq.RecordCount)

             estrutOperation(i).req.DetReq(contaMCDTS).CodigoMSP = BL_HandleNull(rsReq.Fields("codigo_msp").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).mspDescr = BL_HandleNull(rsReq.Fields("msp_descr").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).codAreaMSP = BL_HandleNull(rsReq.Fields("codigo_areamsp").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).AreaMSPDescr = BL_HandleNull(rsReq.Fields("areamsp_descr").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).mcdtDescr = BL_HandleNull(rsReq.Fields("mcdt_description").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).mcdtID = BL_HandleNull(rsReq.Fields("mcdt_id").value, "")
             'estrutOperation(i).req.DetReq(contaMCDTS).mcdtCodCPAL = "3122-9" 'MARTELAN�O C�DIGO CPAL NAO VEM DO SERVI�O (A VER COM A SPMS) PENDENTE
             estrutOperation(i).req.DetReq(contaMCDTS).MCDTStateCode = BL_HandleNull(rsReq.Fields("mcdt_state_code").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).MCDTSateDescr = BL_HandleNull(rsReq.Fields("mcdt_state_descr").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).LateralityCOde = BL_HandleNull(rsReq.Fields("laterality_code").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).lateralityDescr = BL_HandleNull(rsReq.Fields("laterality_descr").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).ProductCode = BL_HandleNull(rsReq.Fields("product_code").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).productDescr = BL_HandleNull(rsReq.Fields("product_description").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).nSamples = BL_HandleNull(rsReq.Fields("n_samples").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).ClinicalInfoDet = BL_HandleNull(rsReq.Fields("clinical_info_det").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).AuthRequered = BL_HandleNull(rsReq.Fields("auth_required").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).InsetTax = BL_HandleNull(rsReq.Fields("isent_tax").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).internalize = BL_HandleNull(rsReq.Fields("internalize").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).price = BL_HandleNull(rsReq.Fields("price").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).moderatorTax = BL_HandleNull(rsReq.Fields("moderator_tax").value, "")
             estrutOperation(i).req.DetReq(contaMCDTS).reqDetId = BL_HandleNull(rsReq.Fields("req_det_id").value, "")
             
             contaMCDTS = contaMCDTS + 1
             
'            ReDim Preserve estrutOperation(i).req(i).PrescrLocal(i)

             estrutOperation(i).req.PrescrLocal.LocalPrescCode = BL_HandleNull(rsReq.Fields("local_presc_code").value, "")
             estrutOperation(i).req.PrescrLocal.LocalPrescDescr = BL_HandleNull(rsReq.Fields("local_presc_descr").value, "")
             estrutOperation(i).req.PrescrLocal.LocalPrescDomain = BL_HandleNull(rsReq.Fields("local_presc_domain").value, "")
             estrutOperation(i).req.PrescrLocal.LocalPrescId = BL_HandleNull(rsReq.Fields("local_presc_id").value, "")

         '   ReDim Preserve estrutOperation(i).req(i).Patient(i)

             estrutOperation(i).req.Patient.SNS = BL_HandleNull(rsReq.Fields("sns").value, "")
             estrutOperation(i).req.Patient.Name = BL_HandleNull(rsReq.Fields("name").value, "")
             estrutOperation(i).req.Patient.birthDate = BL_HandleNull(rsReq.Fields("birth_date").value, "")
             estrutOperation(i).req.Patient.sex = BL_HandleNull(rsReq.Fields("sex").value, "")
             estrutOperation(i).req.Patient.PatientId = BL_HandleNull(rsReq.Fields("patient_id").value, "")

          '  ReDim Preserve estrutOperation(i).req(i).Entity(i)

             estrutOperation(i).req.Entity.codEntity = BL_HandleNull(rsReq.Fields("ENTITY_CODE").value, "")
             estrutOperation(i).req.Entity.EntityDescr = BL_HandleNull(rsReq.Fields("entity_descr").value, "")
             estrutOperation(i).req.Entity.entityDomain = BL_HandleNull(rsReq.Fields("entity_domain").value, "")
             estrutOperation(i).req.Entity.Country = BL_HandleNull(rsReq.Fields("country").value, "")
             estrutOperation(i).req.Entity.entityId = BL_HandleNull(rsReq.Fields("entity_id").value, "")

             rsReq.MoveNext
             k = k + 1
            Wend
        'End If
    End If
    
    If Not rsOpError Is Nothing Then
        If rsOpError.state = adStateOpen Then
             rsOpError.Close
        End If
        Set rsOpError = Nothing
    End If
    
    If Not rsReq Is Nothing Then
        If rsReq.state = adStateOpen Then
             rsReq.Close
        End If
        Set rsReq = Nothing
    End If
    Exit Sub
    
    
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Sub
    Resume Next
End Sub

'edgar.parada Glintt-HS-19165 09.08.2018 - Validar o mapeamento das Entidades Financeiras (sl_mapeamentos)
'Mais info na chamda de m�todo na ESP_CATIVAPEDIDO
'NELOSNPSILVA 19.12.2018 Vai buscar a efr � SL_PROVEN com base no local da prescri��o.
Public Sub ESP_ValidaEFR(ByVal Index As Integer, ByVal opId As Long)
   Dim rsMap As New adodb.recordset
   Dim RsEFR As New adodb.recordset
   Dim methodName As String
   Dim sql As String
    
   methodName = "ESP_ValidaEFR"
   
   On Error GoTo TrataErro
 
   
'   sql = "SELECT cod_mapeamento FROM sl_mapeamentos a, sl_tabelas b WHERE lower(cod_tipo_mapeamento) =lower('efr') "
'   sql = sql & "AND a.seq_tabela = b.seq_tabela AND b.cod_tabela = " & BL_TrataStringParaBD(estrutOperation(Index).req.Entity.codEntity)
'
   sql = "SELECT cod_efr FROM sl_proven a WHERE cod_proven = " & BL_TrataStringParaBD(estrutOperation(Index).req.PrescrLocal.LPrescCodeSISLAB)
   
    rsMap.CursorLocation = adUseServer
    rsMap.CursorType = adOpenStatic
    rsMap.Open sql, gConexao

   If rsMap.RecordCount = 0 Then
      InsertErro Index, "", opId, "MAP_ERROR_EFR", "A EFR " & estrutOperation(Index).req.Entity.EntityDescr & " n�o est� mapeada. "
      Exit Sub
   Else
   
     sql = "SELECT * FROM sl_efr WHERE cod_efr = " & BL_TrataStringParaBD(BL_HandleNull(rsMap.Fields("cod_efr").value, ""))

     RsEFR.CursorLocation = adUseServer
     RsEFR.CursorType = adOpenStatic
     RsEFR.Open sql, gConexao

     If RsEFR.RecordCount = 0 Then
      'Guardar na estrutrura os valores mapeados
        InsertErro Index, "", opId, "MAP_ERROR_EFR", "A EFR " & estrutOperation(Index).req.Entity.EntityDescr & " mapeada n�o est� definida."
        Exit Sub
     Else
                                                                                                  
        estrutOperation(Index).req.Entity.codEntitySISLAB = BL_HandleNull(RsEFR.Fields("cod_efr").value, "")
        estrutOperation(Index).req.Entity.EntityDescrSISLAB = BL_HandleNull(RsEFR.Fields("descr_efr").value, "")
     End If
         
   End If
   
   Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Sub
    Resume Next
End Sub

'edgar.parada Glintt-HS-19165 09.08.2018 - Validar o mapeamento das an�lises (sl_mapeamentos)
'Mais info na chamda de m�todo na ESP_CATIVAPEDIDO
Public Sub ESP_ValidaMDCT(ByVal Index As Integer, ByVal opId As Long)
   Dim rsMCDT As New adodb.recordset
   Dim methodName As String
   Dim sql As String
   Dim i As Integer
   
   methodName = "ESP_ValidaMDCT"
   
   On Error GoTo TrataErro
   
    For i = 1 To UBound(estrutOperation(Index).req.DetReq)

   
      sql = "SELECT cod_mapeamento FROM sl_mapeamentos a, sl_tabelas b WHERE lower(cod_tipo_mapeamento) =lower('ana') "
      sql = sql & "AND a.seq_tabela = b.seq_tabela AND b.cod_tabela = " & BL_TrataStringParaBD(estrutOperation(Index).req.DetReq(i).CodigoMSP)
   
      rsMCDT.CursorLocation = adUseServer
      rsMCDT.CursorType = adOpenStatic
      rsMCDT.Open sql, gConexao
   
      If rsMCDT.RecordCount = 0 Then
         InsertErro Index, estrutOperation(Index).req.DetReq(i).mcdtCodCPAL, opId, "MAP_ERROR_EXAM", "O exame " & estrutOperation(Index).req.DetReq(i).CodigoMSP & _
         " [" & estrutOperation(Index).req.DetReq(i).mcdtDescr & "] n�o est� mapeado."
         'Exit Sub
      ElseIf rsMCDT.RecordCount = 1 Then
         estrutOperation(Index).req.DetReq(i).mcdtCodSISLAB = rsMCDT.Fields("cod_mapeamento").value
      Else
         estrutOperation(Index).req.DetReq(i).mcdtCodSISLAB = ""
      End If
   
    If Not rsMCDT Is Nothing Then
        If rsMCDT.state = adStateOpen Then
             rsMCDT.Close
        End If
        Set rsMCDT = Nothing
    End If
    
   Next
   Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Sub
    Resume Next
End Sub

'NELSONPSILVA 19.12.2018 - Validar o mapeamento dos locais de prescricao(sl_mapeamentos)
'Mais info na chamda de m�todo na ESP_CATIVAPEDIDO
Public Sub ESP_ValidaLocalPresc(ByVal Index As Integer, ByVal opId As Long)
   Dim rsMap As New adodb.recordset
   Dim rsPresc As New adodb.recordset
   Dim methodName As String
   Dim sql As String
   Dim i As Integer
   
   methodName = "ESP_ValidaLocalPresc"
   
   On Error GoTo TrataErro

   sql = "SELECT cod_mapeamento FROM sl_mapeamentos a, sl_tabelas b WHERE lower(cod_tipo_mapeamento) =lower('lpresc') "
   sql = sql & "AND a.seq_tabela = b.seq_tabela AND b.cod_tabela = " & BL_TrataStringParaBD(estrutOperation(Index).req.PrescrLocal.LocalPrescCode)

   rsMap.CursorLocation = adUseServer
   rsMap.CursorType = adOpenStatic
   rsMap.Open sql, gConexao

   If rsMap.RecordCount = 0 Then
      InsertErro Index, "", opId, "MAP_ERROR_LPRESC", "O local de prescri��o " & estrutOperation(Index).req.PrescrLocal.LocalPrescCode & " n�o est� mapeado. "
      Exit Sub
   Else
   
     sql = "SELECT * FROM sl_proven WHERE cod_proven = " & BL_TrataStringParaBD(BL_HandleNull(rsMap.Fields("cod_mapeamento").value, "")) & " and flg_invisivel is null"

     rsPresc.CursorLocation = adUseServer
     rsPresc.CursorType = adOpenStatic
     rsPresc.Open sql, gConexao

     If rsPresc.RecordCount = 0 Then
        InsertErro Index, "", opId, "MAP_ERROR_LPRESC", "O local de prescri��o " & estrutOperation(Index).req.PrescrLocal.LocalPrescCode & " mapeado n�o est� definido."
        Exit Sub
     Else
        'Guardar na estrutrura os valores mapeados
        estrutOperation(Index).req.PrescrLocal.LPrescCodeSISLAB = BL_HandleNull(rsPresc.Fields("cod_proven").value, "")
        estrutOperation(Index).req.PrescrLocal.LPrescDescrSISLAB = BL_HandleNull(rsPresc.Fields("descr_proven").value, "")
     End If
     
     rsPresc.Close
     Set rsPresc = Nothing
   End If
   
   rsMap.Close
   Set rsMap = Nothing
    
   Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Sub
    Resume Next
End Sub

'NELSONPSILVA 19.12.2018 - Validar o mapeamento a isen��o(sl_mapeamentos)
'Mais info na chamda de m�todo na ESP_CATIVAPEDIDO
Public Sub ESP_ValidaIsencao(ByVal Index As Integer, ByVal opId As Long)
   Dim rsIsent As New adodb.recordset
   Dim methodName As String
   Dim sql As String
   Dim i As Integer
   
   methodName = "ESP_ValidaIsencao"
   
   On Error GoTo TrataErro
   
   For i = 1 To UBound(estrutOperation(Index).req.DetReq)

   sql = "SELECT cod_mapeamento FROM sl_mapeamentos a, sl_tabelas b WHERE lower(cod_tipo_mapeamento) =lower('isent') "
   sql = sql & "AND a.seq_tabela = b.seq_tabela AND b.cod_tabela = " & BL_TrataStringParaBD(estrutOperation(Index).req.DetReq(i).InsetTax)

   rsIsent.CursorLocation = adUseServer
   rsIsent.CursorType = adOpenStatic
   rsIsent.Open sql, gConexao

   If rsIsent.RecordCount = 0 Then
      'InsertErro Index, "", opId, "MAP_ERROR_ISENT", "A isen��o " & estrutOperation(Index).req.DetReq(i).InsetTax & " n�o est� mapeada. "
      InsertErro Index, "", opId, "MAP_ERROR_ISENT", "O tipo de isen��o (" & IIf(estrutOperation(Index).req.DetReq(i).InsetTax = "S", "ISENTO", "N�O ISENTO") & ") n�o esta " & _
      "mapeado para o c�digo interno correspondente."
      Exit Sub
   Else
       'Guardar na estrutrura os valores mapeados
        estrutOperation(Index).req.DetReq(i).cod_isencao = BL_HandleNull(rsIsent.Fields("cod_mapeamento").value, "")
   End If
   
    If Not rsIsent Is Nothing Then
     If rsIsent.state = adStateOpen Then
          rsIsent.Close
     End If
     Set rsIsent = Nothing
    End If
     
   Next
   Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Sub
    Resume Next
End Sub
'edgar.parada Glintt-HS-19165 09.08.2018 - Valida se o m�dico existe no SISLAB
'Mais info na chamda de m�todo na ESP_CATIVAPEDIDO
Public Sub ESP_ValidaMedico(ByVal Medico As String, ByVal Index As Integer)
   Dim rs As New adodb.recordset
   Dim methodName As String
   Dim sql As String
   Dim res As Long
   Dim nomeMedico As String
    
   methodName = "ESP_ValidaMedico"
   
   On Error GoTo TrataErro
    
   nomeMedico = "M�dico N�. Ordem " & estrutOperation(Index).req.NumCodProfissional
   
   sql = "select * from sl_medicos where cod_med = " & BL_TrataStringParaBD(estrutOperation(Index).req.NumCodProfissional) & " and flg_invisivel is null"
   
   rs.CursorLocation = adUseClient
   rs.CursorType = adOpenStatic
   rs.Open sql, gConexao

   If rs.RecordCount = 0 Then
        sql = "INSERT INTO sl_medicos(cod_med, nome_med, user_cri, dt_cri, seq_med) Values( "
        sql = sql & BL_TrataStringParaBD(estrutOperation(Index).req.NumCodProfissional) & ","
        sql = sql & BL_TrataStringParaBD(nomeMedico) & ", "
        sql = sql & gCodUtilizador & ", "
        sql = sql & BL_TrataStringParaBD(Bg_DaData_ADO) & ", "
        sql = sql & "(SELECT MAX(seq_med)+1 FROM sl_medicos))"
        
        res = BG_ExecutaQuery_ADO(sql)
        
        If res > 0 Then
           estrutOperation(Index).req.NumCodProfSISLAB = estrutOperation(Index).req.NumCodProfissional
           estrutOperation(Index).req.nomeMedico = nomeMedico
        End If
   Else
      estrutOperation(Index).req.NumCodProfSISLAB = BL_HandleNull(rs.Fields("cod_med").value, "")
      estrutOperation(Index).req.nomeMedico = BL_HandleNull(rs.Fields("nome_med").value, "")
   End If
   
   If Not rs Is Nothing Then
    If rs.state = adStateOpen Then
         rs.Close
    End If
    Set rs = Nothing
   End If
   
   Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Sub
    Resume Next
End Sub

'edgar.parada Glintt-HS-19165 09.08.2018 - inserir erros de mapeamento e muda o estado da opera��o
Public Sub InsertErro(ByVal Index As Integer, ByVal codexame As String, opId As Long, ByVal errorCode As String, ByVal errorDescr As String)
    
    Dim methodName As String
    Dim ssql As String
    Dim res  As Long
    Dim cmd As New adodb.Command
    
    methodName = "InsertErro"

    On Error GoTo TrataErro
    
    Set cmd = New adodb.Command
         
    With cmd
        .ActiveConnection = gConexao
        .CommandText = "PCK_INTEGRACAO_ESP.insere_erro_operacaov1"
        .CommandType = adCmdStoredProc
        If codexame <> "" Then
         .Parameters.Append .CreateParameter("p_codExame", adVarChar, adParamInput, Len(codexame), codexame)
        End If
        .Parameters.Append .CreateParameter("p_opId", adNumeric, adParamInput, Len(opId), opId)
        .Parameters.Append .CreateParameter("p_errorCode", adVarChar, adParamInput, Len(errorCode), errorCode)
        .Parameters.Append .CreateParameter("p_errorDescr", adVarChar, adParamInput, 4000, errorDescr)
        .Parameters.Append .CreateParameter("p_error", adVarChar, adParamOutput, 4000)
     End With
    
     cmd.Execute
     
     If BL_HandleNull(cmd.Parameters.Item("p_error").value, "") = "" Then
        contaErros = contaErros + 1
        
        ReDim Preserve estrutOperation(Index).ErrorOp(contaErros)
        estrutOperation(Index).OpState = cINSUCESSO
        estrutOperation(Index).ErrorOp(contaErros).errorCode = BL_HandleNull(errorCode, "")
        estrutOperation(Index).ErrorOp(contaErros).errorDescr = BL_HandleNull(errorDescr, "")
     End If
    
    Exit Sub
TrataErro:
     BG_LogFile_Erros "Erro ao Gravar Erro: " & Err.Description & "para operation_id = " & opId, moduleName, methodName, True
    Exit Sub
    Resume Next
End Sub

'edgar.parada Glintt-HS-19165 09.08.2018 - Obter SNS no doente
Public Function ESP_GetSNSDoente(ByVal cUtente As String) As String
   Dim rs As New adodb.recordset
   Dim methodName As String
   Dim sql As String
    
   methodName = "ESP_GetSNSDoente"
   
   On Error GoTo TrataErro
   
   If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
   Else
        tabela_aux = "sl_identif"
   End If
    
   sql = "select n_cartao_ute from " & tabela_aux & " where seq_utente = " & BL_TrataStringParaBD(cUtente)
   
   rs.CursorLocation = adUseServer
   rs.CursorType = adOpenStatic
   rs.Open sql, gConexao
   If rs.RecordCount > 0 Then
      If IsNull(rs.Fields("n_cartao_ute").value) Then
         BG_Mensagem mediMsgBox, "Doente sem o n�mero de SNS preenchido!", vbInformation, "SNS n�o definido"
         Exit Function
      Else
         ESP_GetSNSDoente = rs.Fields("n_cartao_ute").value
      End If
   End If
   Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Function
    Resume Next
End Function

'edgar.parada Glintt-HS-19165 * Verificar se a requisi��o � tipo ESP
Public Function RequisicaoESP(ByVal num_requis As String) As Boolean
    'Dim rs As New ADODB.recordset
    Dim cmd As New adodb.Command
    Dim methodName As String
    'Dim sql As String
    
    methodName = "RequisicaoESP"
    
    On Error GoTo TrataErro
    
    Set cmd = New adodb.Command
     
    If num_requis = "" Then Exit Function
    
'    If num_requis <> "" Then
'       sql = "SELECT * FROM SL_CREDENCIAIS WHERE n_req_orig = " & BL_TrataStringParaBD(num_requis)
'    End If
'
'    rs.CursorLocation = adUseClient
'    rs.CursorType = adOpenStatic
'
'    rs.Open sql, gConexao
'
'    If rs.RecordCount > 0 Then
'        RequisicaoESP = True
'    End If
'    rs.Close
'    Set rs = Nothing


 With cmd
          .ActiveConnection = gConexao
          .CommandText = "PCK_INTEGRACAO_ESP.requisicao_tipo_esp"
          .CommandType = adCmdStoredProc
          .Parameters.Append .CreateParameter("p_contexto", adVarChar, adParamInput, 10, "SISLAB")
          .Parameters.Append .CreateParameter("p_req", adVarChar, adParamInput, Len(num_requis), num_requis)
          .Parameters.Append .CreateParameter("p_credenciais", adVarChar, adParamOutput, 10)
    End With
    
    cmd.Execute
    
    If cmd.Parameters.Item("p_credenciais").value = "S" Then
       RequisicaoESP = True
    Else
       RequisicaoESP = False
    End If
    
    Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Function
    Resume Next
End Function
'

'edgar.parada Glintt-HS-19165 * Verificar se a credencial � tipo ESP
Public Function CredencialESP(ByVal credencial As String) As Boolean
    Dim rs As New adodb.recordset
    Dim methodName As String
    Dim cmd As New adodb.Command
    
    methodName = "CredencialESP"
    
    On Error GoTo TrataErro
    
    Set cmd = New adodb.Command
    
    If credencial = "" Then
       CredencialESP = False
       Exit Function
    End If
    
    With cmd
          .ActiveConnection = gConexao
          .CommandText = "PCK_INTEGRACAO_ESP.search_credencial_esp"
          .CommandType = adCmdStoredProc
          .Parameters.Append .CreateParameter("p_contexto", adVarChar, adParamInput, 10, "SISLAB")
          .Parameters.Append .CreateParameter("p_credencial", adVarChar, adParamInput, Len(credencial), credencial)
          .Parameters.Append .CreateParameter("p_credenciais", adVarChar, adParamOutput, 10)
    End With
    
    cmd.Execute
    
    If cmd.Parameters.Item("p_credenciais").value = "S" Then
       CredencialESP = True
    Else
       CredencialESP = False
    End If
    
    Exit Function
TrataErro:
    CredencialESP = False
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Function
    Resume Next
End Function
'
'NELSONPSILVA Glintt-HS-21031 * Fun��o para devolver KM domic�lio
Public Function Esp_GetKmDomicilio(ByVal cod_postal As String, ByVal data_efetivacao As Date) As String
    Dim rs As New adodb.recordset
    Dim methodName As String
    Dim cmd As New adodb.Command
    
    methodName = "Esp_GetKmDomicilio"
    
    On Error GoTo TrataErro
    
    Set cmd = New adodb.Command
    
    If cod_postal = "" Then
       Esp_GetKmDomicilio = ""
       Exit Function
    End If
    
    With cmd
          .ActiveConnection = gConexao
          .CommandText = "PCK_INTEGRACAO_ESP.get_km"
          .CommandType = adCmdStoredProc
          .Parameters.Append .CreateParameter("p_cod_postal", adVarChar, adParamInput, 8, cod_postal)
          .Parameters.Append .CreateParameter("p_data_efetivacao", adDate, adParamInput, Len(data_efetivacao), data_efetivacao)
          .Parameters.Append .CreateParameter("w_km", adVarChar, adParamOutput, 4)
    End With
    
    cmd.Execute
    
    Esp_GetKmDomicilio = BL_HandleNull(cmd.Parameters.Item("w_km").value, "")
    
    Exit Function
TrataErro:
    Esp_GetKmDomicilio = ""
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Function
    Resume Next
End Function
'

Public Function ESP_CredencialDom(ByVal p_credencial As String) As Boolean
    'Dim rs As New ADODB.recordset
    Dim cmd As New adodb.Command
    Dim methodName As String
    'Dim sql As String
    
    methodName = "ESP_CredencialDom"
    
    On Error GoTo TrataErro
    
    Set cmd = New adodb.Command
     
    If p_credencial = "" Then Exit Function

    With cmd
          .ActiveConnection = gConexao
          .CommandText = "PCK_INTEGRACAO_ESP.valida_domicilio_esp"
          .CommandType = adCmdStoredProc
          .Parameters.Append .CreateParameter("p_contexto", adVarChar, adParamInput, 10, "SISLAB")
          .Parameters.Append .CreateParameter("p_req", adVarChar, adParamInput, Len(p_credencial), p_credencial)
          .Parameters.Append .CreateParameter("w_domicile", adVarChar, adParamOutput, 10)
    End With
    
    cmd.Execute
    
    If cmd.Parameters.Item("w_domicile").value = "S" Then
       ESP_CredencialDom = True
    Else
       ESP_CredencialDom = False
    End If
    
    Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Function
    Resume Next
End Function
'
'NELSONPSILVA Glintt-HS-21031 *Domicilio
'Public Function Esp_ReqDomicilio(ByVal num_requis As String) As Boolean
'
'    Dim sSql As String
'    Dim RsCredencial As New ADODB.recordset
'
'    On Error GoTo TrataErro
'
'    sSql = " SELECT CASE WHEN cod_urbano IN (1, 2, 3) THEN 'S' ELSE 'N' END cod_urbano FROM sl_requis WHERE n_req = " & num_requis & ""
'
'    RsCredencial.CursorLocation = adUseClient
'    RsCredencial.CursorType = adOpenStatic
'    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
'    RsCredencial.Open sSql, gConexao
'    If RsCredencial.RecordCount = 1 Then
'        If RsCredencial!cod_urbano = "S" Then
'            Esp_ReqDomicilio = True
'        Else
'            Esp_ReqDomicilio = False
'        End If
'    Else
'        Esp_ReqDomicilio = False
'    End If
'    RsCredencial.Close
'    Set RsCredencial = Nothing
'Exit Function
'TrataErro:
'    BL_LogFile_BD "Integracao_ESP", "Esp_ReqDomicilio", Err.Number, Err.Description, ""
'    Exit Function
'    Resume Next
'End Function

Public Function esp_devolve_cod_prescritor(ByVal credencial As String) As String
Dim i As Integer
On Error GoTo TrataErro

For i = 0 To UBound(estrutOperation)
    If estrutOperation(i).ReqNumber = credencial Then
        esp_devolve_cod_prescritor = estrutOperation(i).req.PrescrLocal.LocalPrescCode
    End If
Next i

Exit Function
TrataErro:
    BL_LogFile_BD "Integracao_ESP", "esp_devolve_cod_prescritor", Err.Number, Err.Description, ""
    Exit Function
    Resume Next
End Function
        
'NELSONPSILVA 30.04.2019 Glintt-HS-21232
Public Function CredencialEfetivada(ByVal credencial As String) As Boolean
    Dim rs As New adodb.recordset
    Dim methodName As String
    Dim cmd As New adodb.Command
    
    methodName = "CredencialEfetivada"
    
    On Error GoTo TrataErro
    
    Set cmd = New adodb.Command
    
    If credencial = "" Then
       CredencialEfetivada = False
       Exit Function
    End If
    
    With cmd
          .ActiveConnection = gConexao
          .CommandText = "PCK_INTEGRACAO_ESP.verifica_credencial_efetivada"
          .CommandType = adCmdStoredProc
          .Parameters.Append .CreateParameter("p_credencial", adVarChar, adParamInput, Len(credencial), credencial)
          .Parameters.Append .CreateParameter("w_mcdt_efetivado", adVarChar, adParamOutput, 10)
    End With
    
    cmd.Execute
    
    If cmd.Parameters.Item("w_mcdt_efetivado").value = "S" Then
       CredencialEfetivada = True
    Else
       CredencialEfetivada = False
    End If
    
    Exit Function
TrataErro:
    CredencialEfetivada = False
    BG_LogFile_Erros Err.Number & " - " & Err.Description, moduleName, methodName
    Exit Function
    Resume Next
End Function
'

VERSION 5.00
Begin VB.Form Form_Ini 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Caption_Ini"
   ClientHeight    =   5805
   ClientLeft      =   525
   ClientTop       =   2070
   ClientWidth     =   15030
   Icon            =   "FormIni.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   MousePointer    =   1  'Arrow
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5805
   ScaleWidth      =   15030
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtGuardar 
      Height          =   615
      Left            =   14280
      TabIndex        =   44
      Top             =   5040
      Width           =   615
   End
   Begin VB.CommandButton BtCarregar 
      Height          =   615
      Left            =   13440
      Style           =   1  'Graphical
      TabIndex        =   43
      ToolTipText     =   "Carregar defini��es"
      Top             =   5040
      Width           =   615
   End
   Begin VB.Frame Frame4 
      Caption         =   "Arranque"
      Height          =   735
      Left            =   7560
      TabIndex        =   35
      Top             =   3240
      Width           =   7335
      Begin VB.TextBox EcImagem 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   18
         Top             =   240
         Width           =   4575
      End
      Begin VB.Label Label1 
         Caption         =   "Imagem"
         Height          =   255
         Index           =   15
         Left            =   480
         TabIndex        =   39
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Parametros"
      Height          =   1455
      Left            =   7560
      TabIndex        =   34
      Top             =   1680
      Width           =   7335
      Begin VB.TextBox EcNomeReg 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   17
         Top             =   960
         Width           =   4575
      End
      Begin VB.TextBox EcNomeApp 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   16
         Top             =   600
         Width           =   4575
      End
      Begin VB.TextBox EcPrefixo 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   15
         Top             =   240
         Width           =   4575
      End
      Begin VB.Label Label1 
         Caption         =   "Nome Reg"
         Height          =   255
         Index           =   14
         Left            =   480
         TabIndex        =   38
         Top             =   960
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Nome App"
         Height          =   255
         Index           =   13
         Left            =   480
         TabIndex        =   37
         Top             =   600
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Prefixo"
         Height          =   255
         Index           =   3
         Left            =   480
         TabIndex        =   36
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Internacional"
      Height          =   1455
      Left            =   7560
      TabIndex        =   30
      Top             =   120
      Width           =   7335
      Begin VB.TextBox EcDecimal 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   14
         Top             =   960
         Width           =   4575
      End
      Begin VB.TextBox EcHora 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   13
         Top             =   600
         Width           =   4575
      End
      Begin VB.TextBox EcData 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   12
         Top             =   240
         Width           =   4575
      End
      Begin VB.Label Label1 
         Caption         =   "Decimal"
         Height          =   255
         Index           =   11
         Left            =   480
         TabIndex        =   33
         Top             =   960
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Hora"
         Height          =   255
         Index           =   10
         Left            =   480
         TabIndex        =   32
         Top             =   600
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Data"
         Height          =   255
         Index           =   9
         Left            =   480
         TabIndex        =   31
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Base de Dados"
      Height          =   3495
      Left            =   120
      TabIndex        =   24
      Top             =   2160
      Width           =   7335
      Begin VB.TextBox EcServer 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   11
         Top             =   3000
         Width           =   4575
      End
      Begin VB.TextBox EcProvider 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   10
         Top             =   2640
         Width           =   4575
      End
      Begin VB.TextBox EcFileDataSource 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   9
         Top             =   2280
         Width           =   4575
      End
      Begin VB.TextBox EcLoginTimeout 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   8
         Top             =   1920
         Width           =   4575
      End
      Begin VB.TextBox EcPWDAux 
         Appearance      =   0  'Flat
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2520
         PasswordChar    =   "*"
         TabIndex        =   7
         Top             =   1560
         Width           =   4575
      End
      Begin VB.TextBox EcUID 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   6
         Top             =   1200
         Width           =   4575
      End
      Begin VB.TextBox EcDataSource 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   5
         Top             =   840
         Width           =   4575
      End
      Begin VB.ComboBox EcTipoBD 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2520
         TabIndex        =   4
         Top             =   480
         Width           =   4575
      End
      Begin VB.Label Label1 
         Caption         =   "Server"
         Height          =   255
         Index           =   18
         Left            =   480
         TabIndex        =   42
         Top             =   3000
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Provider"
         Height          =   255
         Index           =   17
         Left            =   480
         TabIndex        =   41
         Top             =   2640
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "File Data Source"
         Height          =   255
         Index           =   16
         Left            =   480
         TabIndex        =   40
         Top             =   2280
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "LoginTimeout"
         Height          =   255
         Index           =   8
         Left            =   480
         TabIndex        =   29
         Top             =   1920
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "PWD"
         Height          =   255
         Index           =   7
         Left            =   480
         TabIndex        =   28
         Top             =   1560
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "UID"
         Height          =   255
         Index           =   6
         Left            =   480
         TabIndex        =   27
         Top             =   1200
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "DataSource"
         Height          =   255
         Index           =   5
         Left            =   480
         TabIndex        =   26
         Top             =   840
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo BD"
         Height          =   255
         Index           =   4
         Left            =   480
         TabIndex        =   25
         Top             =   480
         Width           =   1935
      End
   End
   Begin VB.Frame FrameGeral 
      Caption         =   "Geral"
      Height          =   1935
      Left            =   120
      TabIndex        =   19
      Top             =   120
      Width           =   7335
      Begin VB.TextBox EcDirCliente 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   1
         Top             =   720
         Width           =   4575
      End
      Begin VB.TextBox EcFicheiroDeAjuda 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   3
         Top             =   1440
         Width           =   4575
      End
      Begin VB.TextBox EcAplicacaoParaAjuda 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   2
         Top             =   1080
         Width           =   4575
      End
      Begin VB.TextBox EcDirServidor 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2520
         TabIndex        =   0
         Top             =   360
         Width           =   4575
      End
      Begin VB.Label Label1 
         Caption         =   "Directoria Cliente"
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   23
         Top             =   720
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Ficheiro De Ajuda"
         Height          =   255
         Index           =   2
         Left            =   480
         TabIndex        =   22
         Top             =   1440
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Aplicacao Para Ajuda"
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   21
         Top             =   1080
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Directoria Servidor"
         Height          =   255
         Index           =   12
         Left            =   480
         TabIndex        =   20
         Top             =   360
         Width           =   1935
      End
   End
End
Attribute VB_Name = "Form_Ini"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Type geral
    dirServidor As String
    dirCliente As String
    aplicacaoParaAjuda As String
    ficheiroDeAjuda As String
    TipoBD As String
End Type

Private Type BD
    TipoBD As String
    DataSource As String
    uID As String
    PWD As String
    LoginTimeout As String
    fileDataSource As String
    Provider As String
    server As String
End Type
    
Private Type internacional
    data As String
    hora As String
    decimal As String
End Type

Private Type parametros
    prefixo As String
    nomeApp As String
    nomeReg As String
End Type

Private Type arranque
    Imagem As String
End Type

Private Type estrutFicheiro
    arranque As arranque
    parametro As parametros
    internacional As internacional
    BD As BD
    geral As geral
End Type
Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    Me.caption = " Inicializa��es"
    DIC_Inicializacao_Idioma Me, gIdioma
    Me.left = 5
    Me.top = 5
    Me.Width = 15120
    Me.Height = 6225 ' Normal
    




End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set Form_Ini = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus



End Sub

Sub DefTipoCampos()
    


End Sub

Sub PreencheValoresDefeito()

    DoEvents

End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    

    
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    'nada

End Sub

Function ValidaCamposEc() As Integer
    'nada

End Function

Sub FuncaoProcurar()
    'nada
End Sub

Sub FuncaoAnterior()
    
    'nada

End Sub

Sub FuncaoSeguinte()
    'nada
End Sub

Sub FuncaoInserir()
    
    ' nada
End Sub


Sub FuncaoModificar()
' nada
End Sub



Sub FuncaoRemover()
    'nada
End Sub





VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClRegTubos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico
Public seqTubo As Long
Public CodTubo As Variant
Public descrTubo As Variant
Public Garrafa As Variant
Public DtPrev As Variant

Public DtChega As Variant
Public HrChega As Variant
Public UserChega As Variant
Public LocalChega As Variant

Public DtSaida As Variant
Public HrSaida As Variant
Public UserSaida As Variant
Public LocalSaida As Variant

Public DtElim As Variant
Public EstadoTubo As Variant
Public Cor_Estado As Variant
Public CodEtiqTubo As Variant

Public NormaColheita As String

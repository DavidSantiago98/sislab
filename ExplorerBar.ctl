VERSION 5.00
Begin VB.UserControl ExplorerBar 
   Alignable       =   -1  'True
   AutoRedraw      =   -1  'True
   BackColor       =   &H00FFFFFF&
   ClientHeight    =   2055
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   1095
   ScaleHeight     =   137
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   73
   ToolboxBitmap   =   "ExplorerBar.ctx":0000
   Begin SISLAB.ThemedScrollBar tsbVertical 
      Height          =   1575
      Left            =   720
      TabIndex        =   4
      Top             =   120
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   2778
   End
   Begin VB.PictureBox picAnimation 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   492
      Left            =   120
      ScaleHeight     =   33
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   41
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   1080
      Visible         =   0   'False
      Width           =   612
   End
   Begin VB.PictureBox picGroup 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   252
      Index           =   0
      Left            =   120
      MouseIcon       =   "ExplorerBar.ctx":0312
      ScaleHeight     =   17
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   41
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Visible         =   0   'False
      Width           =   612
   End
   Begin VB.Timer tmrAnimation 
      Enabled         =   0   'False
      Interval        =   20
      Left            =   480
      Top             =   1680
   End
   Begin VB.PictureBox picItems 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   492
      Index           =   0
      Left            =   120
      MouseIcon       =   "ExplorerBar.ctx":061C
      ScaleHeight     =   33
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   41
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   480
      Visible         =   0   'False
      Width           =   612
   End
   Begin VB.PictureBox picButtonMasker 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   120
      Picture         =   "ExplorerBar.ctx":0926
      ScaleHeight     =   19
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   19
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   1680
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.Shape shpBorder 
      BorderColor     =   &H00FFFFFF&
      BorderStyle     =   6  'Inside Solid
      Height          =   2052
      Left            =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   1092
   End
End
Attribute VB_Name = "ExplorerBar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

'ExplorerBar Control
'
'Author Ben Vonk
'08-03-2008 First version (Based on Alex Flex's 'XP-Style ExplorerBar control - version 2' at http://www.planetsourcecode.com/vb/scripts/ShowCode.asp?txtCodeId=61899&lngWId=1)
'09-10-2008 Second version, Add ThemedScrollBar second version and fixed some minor bugs

Option Explicit

' Public Events
Public Event Collapse(Index As Integer)
Public Event Expand(Index As Integer)
Public Event GroupClick(Index As Integer, WindowState As WindowStates)
Public Event ItemClick(Index As Integer, Item As Integer)
Public Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

' Private Constants
Private Const EBP_NORMALGROUPBACKGROUND  As Integer = 5
Private Const ALL_MESSAGES               As Long = -1
Private Const DT_LEFT                    As Long = &H0
Private Const DT_WORD_ELLIPSIS           As Long = &H40000
Private Const GWL_WNDPROC                As Long = -4
Private Const PATCH_05                   As Long = 93
Private Const PATCH_09                   As Long = 137
Private Const MENU_DETAILS               As Long = 3
Private Const MENU_NORMAL                As Long = 1
Private Const MENU_SPECIAL               As Long = 2
Private Const STATE_HOT                  As Long = 2
Private Const STATE_NORMAL               As Long = 1
Private Const STATE_PRESSED              As Long = 3
Private Const WM_CTLCOLORSCROLLBAR       As Long = &H137
Private Const WM_MOUSELEAVE              As Long = &H2A3
Private Const WM_MOUSEMOVE               As Long = &H200
Private Const WM_MOUSEWHEEL              As Long = &H20A
Private Const WM_SYSCOLORCHANGE          As Long = &H15
Private Const WM_THEMECHANGED            As Long = &H31A
Private Const THEME_NAME                 As String = "ExplorerBar"

' Public Enumaration
Public Enum Animations
   None
   Slow
   Medium
   Fast
End Enum

Public Enum ButtonHeights
   Low
   High
End Enum

Public Enum WindowStates
   Expanded
   Collapsed
   Fixed
End Enum

' Private Enumerations
Private Enum ColorsRGB
   IsRed
   IsGreen
   IsBlue
End Enum

Private Enum GroupObjects
   IsDetailCaption
   IsDetailPicture
   IsDetailTitle
   IsGroupBigIcon
   IsGroupTitle
   IsItemActive
   IsItemCaption
   IsItemBackgroundPicture
   IsItemHyperlink
   IsItemSmallIcon
   IsItemTag
   IsToolTipText
   IsWindowState
End Enum

Private Enum MsgWhen
   MSG_BEFORE = 1
   MSG_AFTER = 2
   MSG_BEFORE_AFTER = MSG_BEFORE Or MSG_AFTER
End Enum

' Private Types
Private Type BlendFunction
  BlendOp                                As Byte
  BlendFlags                             As Byte
  SourceConstantAlpha                    As Byte
  AlphaFormat                            As Byte
End Type

Private Type GradientRect
   UpperLeft                             As Long
   LowerRight                            As Long
End Type

Private Type Rect
   Left                                  As Long
   Top                                   As Long
   Right                                 As Long
   Bottom                                As Long
End Type

Private Type GroupItemType
   State                                 As Long
   Rect                                  As Rect
   Caption                               As String
   FullTextShowed                        As Boolean
   Tag                                   As String
   ToolTipText                           As String
   Hyperlink                             As Boolean
   Active                                As Boolean
   SmallIcon                             As IPictureDisp
End Type

Private Type GroupType
   MenuType                              As Long
   State                                 As Long
   BigIcon                               As IPictureDisp
   Title                                 As String
   FullTextShowed                        As Boolean
   ToolTipText                           As String
   Items()                               As GroupItemType
   ItemsBackgroundPicture                As IPictureDisp
   ItemCount                             As Integer
   DetailPicture                         As IPictureDisp
   DetailTitle                           As String
   DetailCaption                         As String
   WindowState                           As WindowStates
End Type

Private Type PointAPI
   X                                     As Long
   Y                                     As Long
End Type

Private Type SubclassDataType
   hWnd                                  As Long
   nAddrSclass                           As Long
   nAddrOrig                             As Long
   nMsgCountA                            As Long
   nMsgCountB                            As Long
   aMsgTabelA()                          As Long
   aMsgTabelB()                          As Long
End Type

Private Type TrackMouseEventStruct
   cbSize                                As Long
   dwFlags                               As Long
   hwndTrack                             As Long
   dwHoverTime                           As Long
End Type

Private Type TriVertex
   X                                     As Long
   Y                                     As Long
   Red                                   As Integer
   Green                                 As Integer
   Blue                                  As Integer
   Alpha                                 As Integer
End Type

' Private Variables
Private m_Animation                      As Animations
Private DoAlignment                      As Boolean
Private HasTheme                         As Boolean
Private InControl                        As Boolean
Private m_GradientSwap                   As Boolean
Private m_Locked                         As Boolean
Private m_OpenOneGroupOnly               As Boolean
Private m_SoundGroupClicked              As Boolean
Private m_SoundItemClicked               As Boolean
Private m_UseAlphaBlend                  As Boolean
Private m_UseWindowTheme                 As Boolean
Private StateNormalGroup                 As Boolean
Private StateSpecialGroup                As Boolean
Private TrackUser32                      As Boolean
Private UseButtonPictures                As Boolean
Private m_HeaderHeight                   As ButtonHeights
Private SubclassFunk                     As Collection
Private Groups()                         As GroupType
Private AlphaPercent                     As Integer
Private AnimationIndex                   As Integer
Private BlendStep                        As Integer
Private GroupCount                       As Integer
Private GroupIndex                       As Integer
Private MoveLines                        As Integer
Private m_ButtonPicture(11)              As IPictureDisp
Private ItemHeight                       As Long
Private m_BackColor                      As Long
Private m_DetailsForeColor               As Long
Private m_GradientBackColor              As Long
Private m_GradientNormalHeaderBackColor  As Long
Private m_GradientNormalItemBackColor    As Long
Private m_GradientSpecialHeaderBackColor As Long
Private m_GradientSpecialItemBackColor   As Long
Private m_NormalArrowDownColor           As Long
Private m_NormalArrowHoverColor          As Long
Private m_NormalArrowUpColor             As Long
Private m_NormalButtonBackColor          As Long
Private m_NormalButtonDownColor          As Long
Private m_NormalButtonHoverColor         As Long
Private m_NormalButtonUpColor            As Long
Private m_NormalHeaderBackColor          As Long
Private m_NormalHeaderForeColor          As Long
Private m_NormalHeaderHoverColor         As Long
Private m_NormalItemBackColor            As Long
Private m_NormalItemBorderColor          As Long
Private m_NormalItemForeColor            As Long
Private m_NormalItemHoverColor           As Long
Private m_SpecialArrowDownColor          As Long
Private m_SpecialArrowHoverColor         As Long
Private m_SpecialArrowUpColor            As Long
Private m_SpecialButtonBackColor         As Long
Private m_SpecialButtonDownColor         As Long
Private m_SpecialButtonHoverColor        As Long
Private m_SpecialButtonUpColor           As Long
Private m_SpecialHeaderBackColor         As Long
Private m_SpecialHeaderForeColor         As Long
Private m_SpecialHeaderHoverColor        As Long
Private m_SpecialItemBackColor           As Long
Private m_SpecialItemBorderColor         As Long
Private m_SpecialItemForeColor           As Long
Private m_SpecialItemHoverColor          As Long
Private ScrollLines                      As Long
Private SubclassData()                   As SubclassDataType

' Private API's
Private Declare Function TrackMouseEventComCtl Lib "ComCtl32" Alias "_TrackMouseEvent" (lpEventTrack As TrackMouseEventStruct) As Long
Private Declare Function BitBlt Lib "GDI32" (ByVal hDCDest As Long, ByVal nXDest As Long, ByVal nYDest As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hDCSrc As Long, ByVal nXSrc As Long, ByVal nYSrc As Long, ByVal dwRop As Long) As Long
Private Declare Function FreeLibrary Lib "Kernel32" (ByVal hLibModule As Long) As Long
Private Declare Function GetModuleHandle Lib "Kernel32" Alias "GetModuleHandleA" (ByVal lpModuleName As String) As Long
Private Declare Function GetProcAddress Lib "Kernel32" (ByVal hModule As Long, ByVal lpProcName As String) As Long
Private Declare Function GlobalAlloc Lib "Kernel32" (ByVal wFlags As Long, ByVal dwBytes As Long) As Long
Private Declare Function GlobalFree Lib "Kernel32" (ByVal hMem As Long) As Long
Private Declare Function LoadLibrary Lib "Kernel32" Alias "LoadLibraryA" (ByVal lpLibFileName As String) As Long
Private Declare Function AlphaBlend Lib "MSImg32" (ByVal hDC As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal hDC As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal BLENDFUNCT As Long) As Long
Private Declare Function GradientFill Lib "MSImg32" (ByVal hDC As Long, ByRef pVertex As TriVertex, ByVal dwNumVertex As Long, pMesh As Any, ByVal dwNumMesh As Long, ByVal dwMode As Long) As Integer
Private Declare Function OleTranslateColor Lib "OLEPro32" (ByVal OLE_COLOR As Long, ByVal HPALETTE As Long, pccolorref As Long) As Long
Private Declare Function DrawText Lib "User32" Alias "DrawTextA" (ByVal hDC As Long, ByVal lpStr As String, ByVal nCount As Long, lpRect As Rect, ByVal wFormat As Long) As Long
Private Declare Function GetWindowThreadProcessId Lib "User32" (ByVal hWnd As Long, ByRef lpdwProcessId As Long) As Long
Private Declare Function PtInRect Lib "User32" (Rect As Rect, ByVal lPtX As Long, ByVal lPtY As Long) As Integer
Private Declare Function SetWindowLongA Lib "User32" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Private Declare Function SystemParametersInfo Lib "User32" Alias "SystemParametersInfoA" (ByVal uAction As Long, ByVal uParam As Long, lpvParam As Any, ByVal fuWinIni As Long) As Long
Private Declare Function TrackMouseEvent Lib "User32" (lpEventTrack As TrackMouseEventStruct) As Long
Private Declare Function CloseThemeData Lib "UxTheme" (ByVal lngTheme As Long) As Long
Private Declare Function DrawThemeBackground Lib "UxTheme" (ByVal hTheme As Long, ByVal lhDC As Long, ByVal iPartId As Long, ByVal iStateId As Long, rctFrame As Rect, rctClip As Rect) As Long
Private Declare Function OpenThemeData Lib "UxTheme" (ByVal hWnd As Long, ByVal pszClassList As Long) As Long
Private Declare Function SoundPlay Lib "WinMM" Alias "sndPlaySoundA" (ByVal lpszSoundName As String, ByVal uFlags As Long) As Long

Private Declare Sub CopyMemory Lib "Kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

Public Sub Subclass_WndProc(ByVal bBefore As Boolean, ByRef bHandled As Boolean, ByRef lReturn As Long, ByRef lhWnd As Long, ByRef uMsg As Long, ByRef wParam As Long, ByRef lParam As Long)

   If uMsg = WM_CTLCOLORSCROLLBAR Then
      bHandled = True
      
   ElseIf uMsg = WM_MOUSELEAVE Then
      InControl = False
      
      Call MouseMoveGroup(0, 0, 0, -1, -1)
      
   ElseIf uMsg = WM_MOUSEMOVE Then
      If Not InControl Then
         InControl = True
         
         Call TrackMouseLeave(lhWnd)
      End If
      
   ElseIf uMsg = WM_MOUSEWHEEL Then
      Call tsbVertical_MouseWheel(ScrollLines + ((ScrollLines * 2) * (wParam > 0)))
      
   ElseIf (uMsg = WM_THEMECHANGED) Or (uMsg = WM_SYSCOLORCHANGE) Then
      Call Refresh
   End If

End Sub

Private Function Subclass_AddrFunc(ByVal sDLL As String, ByVal sProc As String) As Long

   Subclass_AddrFunc = GetProcAddress(GetModuleHandle(sDLL), sProc)
   Debug.Assert Subclass_AddrFunc

End Function

Private Function Subclass_Index(ByVal lhWnd As Long, Optional ByVal bAdd As Boolean) As Long

   For Subclass_Index = UBound(SubclassData) To 0 Step -1
      If SubclassData(Subclass_Index).hWnd = lhWnd Then
         If Not bAdd Then Exit Function
         
      ElseIf SubclassData(Subclass_Index).hWnd = 0 Then
         If bAdd Then Exit Function
      End If
   Next 'Subclass_Index
   
   If Not bAdd Then Debug.Assert False

End Function

Private Function Subclass_InIDE() As Boolean

   Debug.Assert Subclass_SetTrue(Subclass_InIDE)

End Function

Private Function Subclass_Initialize(ByVal lhWnd As Long) As Long

Const CODE_LEN                  As Long = 200
Const GMEM_FIXED                As Long = 0
Const PATCH_01                  As Long = 18
Const PATCH_02                  As Long = 68
Const PATCH_03                  As Long = 78
Const PATCH_06                  As Long = 116
Const PATCH_07                  As Long = 121
Const PATCH_0A                  As Long = 186
Const FUNC_CWP                  As String = "CallWindowProcA"
Const FUNC_EBM                  As String = "EbMode"
Const FUNC_SWL                  As String = "SetWindowLongA"
Const MOD_USER                  As String = "User32"
Const MOD_VBA5                  As String = "vba5"
Const MOD_VBA6                  As String = "vba6"

Static bytBuffer(1 To CODE_LEN) As Byte
Static lngCWP                   As Long
Static lngEbMode                As Long
Static lngSWL                   As Long

Dim lngCount                    As Long
Dim lngIndex                    As Long
Dim strHex                      As String

   If bytBuffer(1) Then
      lngIndex = Subclass_Index(lhWnd, True)
      
      If lngIndex = -1 Then
         lngIndex = UBound(SubclassData) + 1
         
         ReDim Preserve SubclassData(lngIndex) As SubclassDataType
      End If
      
      Subclass_Initialize = lngIndex
      
   Else
      strHex = "5589E583C4F85731C08945FC8945F8EB0EE80000000083F802742185C07424E830000000837DF800750AE838000000E84D0000005F8B45FCC9C21000E826000000EBF168000000006AFCFF7508E800000000EBE031D24ABF00000000B900000000E82D000000C3FF7514FF7510FF750CFF75086800000000E8000000008945FCC331D2BF00000000B900000000E801000000C3E33209C978078B450CF2AF75278D4514508D4510508D450C508D4508508D45FC508D45F85052B800000000508B00FF90A4070000C3"
      
      For lngCount = 1 To CODE_LEN
         bytBuffer(lngCount) = val("&H" & Left(strHex, 2))
         strHex = Mid(strHex, 3)
      Next 'lngCount
      
      If Subclass_InIDE Then
         bytBuffer(16) = &H90
         bytBuffer(17) = &H90
         lngEbMode = Subclass_AddrFunc(MOD_VBA6, FUNC_EBM)
         
         If lngEbMode = 0 Then lngEbMode = Subclass_AddrFunc(MOD_VBA5, FUNC_EBM)
      End If
      
      lngCWP = Subclass_AddrFunc(MOD_USER, FUNC_CWP)
      lngSWL = Subclass_AddrFunc(MOD_USER, FUNC_SWL)
      
      ReDim SubclassData(0) As SubclassDataType
   End If
   
   With SubclassData(lngIndex)
      .hWnd = lhWnd
      .nAddrSclass = GlobalAlloc(GMEM_FIXED, CODE_LEN)
      .nAddrOrig = SetWindowLongA(.hWnd, GWL_WNDPROC, .nAddrSclass)
      
      Call CopyMemory(ByVal .nAddrSclass, bytBuffer(1), CODE_LEN)
      Call Subclass_PatchRel(.nAddrSclass, PATCH_01, lngEbMode)
      Call Subclass_PatchVal(.nAddrSclass, PATCH_02, .nAddrOrig)
      Call Subclass_PatchRel(.nAddrSclass, PATCH_03, lngSWL)
      Call Subclass_PatchVal(.nAddrSclass, PATCH_06, .nAddrOrig)
      Call Subclass_PatchRel(.nAddrSclass, PATCH_07, lngCWP)
      Call Subclass_PatchVal(.nAddrSclass, PATCH_0A, ObjPtr(Me))
   End With

End Function

Private Function Subclass_SetTrue(ByRef bValue As Boolean) As Boolean

   Subclass_SetTrue = True
   bValue = True

End Function

Private Sub Subclass_AddMsg(ByVal lhWnd As Long, ByVal uMsg As Long, Optional ByVal When As MsgWhen = MSG_AFTER)

   With SubclassData(Subclass_Index(lhWnd))
      If When And MSG_BEFORE Then Call Subclass_DoAddMsg(uMsg, .aMsgTabelB, .nMsgCountB, MSG_BEFORE, .nAddrSclass)
      If When And MSG_AFTER Then Call Subclass_DoAddMsg(uMsg, .aMsgTabelA, .nMsgCountA, MSG_AFTER, .nAddrSclass)
   End With

End Sub

Private Sub Subclass_DelMsg(ByVal lhWnd As Long, ByVal uMsg As Long, Optional ByVal When As MsgWhen = MSG_AFTER)

   With SubclassData(Subclass_Index(lhWnd))
      If When And MSG_BEFORE Then Call Subclass_DoDelMsg(uMsg, .aMsgTabelB, .nMsgCountB, MSG_BEFORE, .nAddrSclass)
      If When And MSG_AFTER Then Call Subclass_DoDelMsg(uMsg, .aMsgTabelA, .nMsgCountA, MSG_AFTER, .nAddrSclass)
   End With

End Sub

Private Sub Subclass_DoAddMsg(ByVal uMsg As Long, ByRef aMsgTabel() As Long, ByRef nMsgCount As Long, ByVal When As MsgWhen, ByVal nAddr As Long)

Const PATCH_04 As Long = 88
Const PATCH_08 As Long = 132

Dim lngEntry   As Long

   ReDim lngOffset(1) As Long
   
   If uMsg = ALL_MESSAGES Then
      nMsgCount = ALL_MESSAGES
      
   Else
      For lngEntry = 1 To nMsgCount - 1
         If aMsgTabel(lngEntry) = 0 Then
            aMsgTabel(lngEntry) = uMsg
            
            GoTo ExitSub
            
         ElseIf aMsgTabel(lngEntry) = uMsg Then
            GoTo ExitSub
         End If
      Next 'lngEntry
      
      nMsgCount = nMsgCount + 1
      
      ReDim Preserve aMsgTabel(1 To nMsgCount) As Long
      
      aMsgTabel(nMsgCount) = uMsg
   End If
   
   If When = MSG_BEFORE Then
      lngOffset(0) = PATCH_04
      lngOffset(1) = PATCH_05
      
   Else
      lngOffset(0) = PATCH_08
      lngOffset(1) = PATCH_09
   End If
   
   If uMsg <> ALL_MESSAGES Then Call Subclass_PatchVal(nAddr, lngOffset(0), VarPtr(aMsgTabel(1)))
   
   Call Subclass_PatchVal(nAddr, lngOffset(1), nMsgCount)
   
ExitSub:
   Erase lngOffset

End Sub

Private Sub Subclass_DoDelMsg(ByVal uMsg As Long, ByRef aMsgTabel() As Long, ByRef nMsgCount As Long, ByVal When As MsgWhen, ByVal nAddr As Long)

Dim lngEntry As Long

   If uMsg = ALL_MESSAGES Then
      nMsgCount = 0
      
      If When = MSG_BEFORE Then
         lngEntry = PATCH_05
         
      Else
         lngEntry = PATCH_09
      End If
      
      Call Subclass_PatchVal(nAddr, lngEntry, 0)
      
   Else
      For lngEntry = 1 To nMsgCount - 1
         If aMsgTabel(lngEntry) = uMsg Then
            aMsgTabel(lngEntry) = 0
            Exit For
         End If
      Next 'lngEntry
   End If

End Sub

Private Sub Subclass_PatchRel(ByVal nAddr As Long, ByVal nOffset As Long, ByVal nTargetAddr As Long)

   Call CopyMemory(ByVal nAddr + nOffset, nTargetAddr - nAddr - nOffset - 4, 4)

End Sub

Private Sub Subclass_PatchVal(ByVal nAddr As Long, ByVal nOffset As Long, ByVal nValue As Long)

   Call CopyMemory(ByVal nAddr + nOffset, nValue, 4)

End Sub

Private Sub Subclass_Stop(ByVal lhWnd As Long)

   With SubclassData(Subclass_Index(lhWnd))
      SetWindowLongA .hWnd, GWL_WNDPROC, .nAddrOrig
      
      Call Subclass_PatchVal(.nAddrSclass, PATCH_05, 0)
      Call Subclass_PatchVal(.nAddrSclass, PATCH_09, 0)
      
      GlobalFree .nAddrSclass
      .hWnd = 0
      .nMsgCountA = 0
      .nMsgCountB = 0
      Erase .aMsgTabelA, .aMsgTabelB
   End With

End Sub

Private Sub Subclass_Terminate()

Dim lngCount As Long

   For lngCount = UBound(SubclassData) To 0 Step -1
      If SubclassData(lngCount).hWnd Then Call Subclass_Stop(SubclassData(lngCount).hWnd)
   Next 'lngCount

End Sub

Public Property Get Animation() As Animations
Attribute Animation.VB_Description = "Returns/sets a the speed that will be used for the open and close window animations."

   Animation = m_Animation

End Property

Public Property Let Animation(ByVal NewAnimation As Animations)

   m_Animation = NewAnimation
   PropertyChanged "Animation"

End Property

Public Property Get BackColor() As OLE_COLOR
Attribute BackColor.VB_Description = "Returns/sets the background color used to display text and graphcs in an object."

   BackColor = m_BackColor

End Property

Public Property Let BackColor(ByVal NewBackColor As OLE_COLOR)

   m_BackColor = NewBackColor
   PropertyChanged "BackColor"
   
   Call Refresh

End Property

Public Property Get BorderColor() As OLE_COLOR
Attribute BorderColor.VB_Description = "Returns/sets the border color used to display the border in an object."

   BorderColor = shpBorder.BorderColor

End Property

Public Property Let BorderColor(ByVal NewBorderColor As OLE_COLOR)

   shpBorder.BorderColor = NewBorderColor
   PropertyChanged "BorderColor"

End Property

Public Property Get DetailsForeColor() As OLE_COLOR
Attribute DetailsForeColor.VB_Description = "Returns/sets the forground color used to display text in an detail window."

   DetailsForeColor = m_DetailsForeColor

End Property

Public Property Let DetailsForeColor(ByVal NewDetailsForeColor As OLE_COLOR)

   m_DetailsForeColor = NewDetailsForeColor
   PropertyChanged "DetailsForeColor"
   
   Call Refresh

End Property

Public Property Get Font() As StdFont
Attribute Font.VB_Description = "Returns a Font object."

   Set Font = UserControl.Font

End Property

Public Property Let Font(ByRef NewFont As StdFont)

   Set Font = NewFont

End Property

Public Property Set Font(ByRef NewFont As StdFont)

Dim intCount As Integer

   With NewFont
      If .Size < 8 Then .Size = 8
      If .Size > 10 Then .Size = 10
   End With
   
   UserControl.Font = NewFont
   PropertyChanged "Font"
   
   For intCount = 0 To picGroup.Count - 1
      picGroup.Item(intCount).Font = UserControl.Font
   Next 'intCount
   
   For intCount = 0 To picItems.Count - 1
      picItems.Item(intCount).Font = UserControl.Font
   Next 'intCount
   
   Call Refresh

End Property

Public Property Get GradientBackColor() As OLE_COLOR
Attribute GradientBackColor.VB_Description = "Returns/sets the gradient color for an object background."

   GradientBackColor = m_GradientBackColor

End Property

Public Property Let GradientBackColor(ByVal NewGradientBackColor As OLE_COLOR)

   m_GradientBackColor = NewGradientBackColor
   PropertyChanged "GradientBackColor"
   
   Call Refresh

End Property

Public Property Get GradientNormalHeaderBackColor() As OLE_COLOR
Attribute GradientNormalHeaderBackColor.VB_Description = "Returns/sets the gradient color for an normal header background."

   GradientNormalHeaderBackColor = m_GradientNormalHeaderBackColor

End Property

Public Property Let GradientNormalHeaderBackColor(ByVal NewGradientNormalHeaderBackColor As OLE_COLOR)

   m_GradientNormalHeaderBackColor = NewGradientNormalHeaderBackColor
   PropertyChanged "GradientNormalHeaderBackColor"
   
   Call Refresh

End Property

Public Property Get GradientNormalItemBackColor() As OLE_COLOR
Attribute GradientNormalItemBackColor.VB_Description = "Returns/sets the gradient color for an normal item background."

   GradientNormalItemBackColor = m_GradientNormalItemBackColor

End Property

Public Property Let GradientNormalItemBackColor(ByVal NewGradientNormalItemBackColor As OLE_COLOR)

   m_GradientNormalItemBackColor = NewGradientNormalItemBackColor
   PropertyChanged "GradientNormalItemBackColor"
   
   Call Refresh

End Property

Public Property Get GradientSpecialHeaderBackColor() As OLE_COLOR
Attribute GradientSpecialHeaderBackColor.VB_Description = "Returns/sets the gradient color for an special header background."

   GradientSpecialHeaderBackColor = m_GradientSpecialHeaderBackColor

End Property

Public Property Let GradientSpecialHeaderBackColor(ByVal NewGradientSpecialHeaderBackColor As OLE_COLOR)

   m_GradientSpecialHeaderBackColor = NewGradientSpecialHeaderBackColor
   PropertyChanged "GradientSpecialHeaderBackColor"
   
   Call Refresh

End Property

Public Property Get GradientSpecialItemBackColor() As OLE_COLOR
Attribute GradientSpecialItemBackColor.VB_Description = "Returns/sets the gradient color for an special item background."

   GradientSpecialItemBackColor = m_GradientSpecialItemBackColor

End Property

Public Property Let GradientSpecialItemBackColor(ByVal NewGradientSpecialItemBackColor As OLE_COLOR)

   m_GradientSpecialItemBackColor = NewGradientSpecialItemBackColor
   PropertyChanged "GradientSpecialItemBackColor"
   
   Call Refresh

End Property

Public Property Get GradientSwap() As Boolean
Attribute GradientSwap.VB_Description = "Determines whether gradient colors will be swapped."

   GradientSwap = m_GradientSwap

End Property

Public Property Let GradientSwap(ByVal NewGradientSwap As Boolean)

   m_GradientSwap = NewGradientSwap
   PropertyChanged "GradientSwap"
   
   Call Refresh

End Property

Public Property Get HeaderHeight() As ButtonHeights
Attribute HeaderHeight.VB_Description = "Returns/sets the height of the group headers."

   HeaderHeight = m_HeaderHeight

End Property

Public Property Let HeaderHeight(ByVal NewHeaderHeight As ButtonHeights)

   m_HeaderHeight = NewHeaderHeight
   PropertyChanged "HeaderHeight"
   
   Call Refresh

End Property

Public Property Get Locked() As Boolean
Attribute Locked.VB_Description = "Returns/sets to lock/unlock an object to fasten the property changes."

   Locked = m_Locked

End Property

Public Property Let Locked(ByVal NewLocked As Boolean)

   If m_Locked = NewLocked Then Exit Property
   
   m_Locked = NewLocked
   PropertyChanged "Locked"
   
   If Not m_Locked Then Call Refresh

End Property

Public Property Get NormalArrowDownColor() As OLE_COLOR
Attribute NormalArrowDownColor.VB_Description = "Returns/sets the arrow color when an normal header is down."

   NormalArrowDownColor = m_NormalArrowDownColor

End Property

Public Property Let NormalArrowDownColor(ByVal NewNormalArrowDownColor As OLE_COLOR)

   m_NormalArrowDownColor = NewNormalArrowDownColor
   PropertyChanged "NormalArrowDownColor"
   
   Call Refresh

End Property

Public Property Get NormalArrowHoverColor() As OLE_COLOR
Attribute NormalArrowHoverColor.VB_Description = "Returns/sets the arrow color when the mouse is hover an normal header."

   NormalArrowHoverColor = m_NormalArrowHoverColor

End Property

Public Property Let NormalArrowHoverColor(ByVal NewNormalArrowHoverColor As OLE_COLOR)

   m_NormalArrowHoverColor = NewNormalArrowHoverColor
   PropertyChanged "NormalArrowHoverColor"
   
   Call Refresh

End Property

Public Property Get NormalArrowUpColor() As OLE_COLOR
Attribute NormalArrowUpColor.VB_Description = "Returns/sets the arrow color when an normal header is up."

   NormalArrowUpColor = m_NormalArrowUpColor

End Property

Public Property Let NormalArrowUpColor(ByVal NewNormalArrowUpColor As OLE_COLOR)

   m_NormalArrowUpColor = NewNormalArrowUpColor
   PropertyChanged "NormalArrowUpColor"
   
   Call Refresh

End Property

Public Property Get NormalButtonBackColor() As OLE_COLOR
Attribute NormalButtonBackColor.VB_Description = "Returns/sets the background color for an normal header button."

   NormalButtonBackColor = m_NormalButtonBackColor

End Property

Public Property Let NormalButtonBackColor(ByVal NewNormalButtonBackColor As OLE_COLOR)

   m_NormalButtonBackColor = NewNormalButtonBackColor
   PropertyChanged "NormalButtonBackColor"
   
   Call Refresh

End Property

Public Property Get NormalButtonDownColor() As OLE_COLOR
Attribute NormalButtonDownColor.VB_Description = "Returns/sets the color for an normal header button when it is down."

   NormalButtonDownColor = m_NormalButtonDownColor

End Property

Public Property Let NormalButtonDownColor(ByVal NewNormalButtonDownColor As OLE_COLOR)

   m_NormalButtonDownColor = NewNormalButtonDownColor
   PropertyChanged "NormalButtonDownColor"
   
   Call Refresh

End Property

Public Property Get NormalButtonHoverColor() As OLE_COLOR
Attribute NormalButtonHoverColor.VB_Description = "Returns/sets the color for an normal header button when the mouse is hover the header."

   NormalButtonHoverColor = m_NormalButtonHoverColor

End Property

Public Property Let NormalButtonHoverColor(ByVal NewNormalButtonHoverColor As OLE_COLOR)

   m_NormalButtonHoverColor = NewNormalButtonHoverColor
   PropertyChanged "NormalButtonHoverColor"
   
   Call Refresh

End Property

Public Property Get NormalButtonPictureDown() As IPictureDisp
Attribute NormalButtonPictureDown.VB_Description = "Returns/sets a graphic for an normal header button when it is down."

   Set NormalButtonPictureDown = m_ButtonPicture(2)

End Property

Public Property Let NormalButtonPictureDown(ByRef NewNormalButtonPictureDown As IPictureDisp)

   Set NormalButtonPictureDown = NewNormalButtonPictureDown

End Property

Public Property Set NormalButtonPictureDown(ByRef NewNormalButtonPictureDown As IPictureDisp)

   Call SetButtonPicture(2, NewNormalButtonPictureDown, "NormalButtonPictureDown", False)

End Property

Public Property Get NormalButtonPictureHover() As IPictureDisp
Attribute NormalButtonPictureHover.VB_Description = "Returns/sets a graphic for an normal header button when the mouse is hover the header."

   Set NormalButtonPictureHover = m_ButtonPicture(1)

End Property

Public Property Let NormalButtonPictureHover(ByRef NewNormalButtonPictureHover As IPictureDisp)

   Set NormalButtonPictureHover = NewNormalButtonPictureHover

End Property

Public Property Set NormalButtonPictureHover(ByRef NewNormalButtonPictureHover As IPictureDisp)

   Call SetButtonPicture(1, NewNormalButtonPictureHover, "NormalButtonPictureHover", False)

End Property

Public Property Get NormalButtonPictureUp() As IPictureDisp
Attribute NormalButtonPictureUp.VB_Description = "Returns/sets a graphic for an normal header button when it is up."

   Set NormalButtonPictureUp = m_ButtonPicture(0)

End Property

Public Property Let NormalButtonPictureUp(ByRef NewNormalButtonPictureUp As IPictureDisp)

   Set NormalButtonPictureUp = NewNormalButtonPictureUp

End Property

Public Property Set NormalButtonPictureUp(ByRef NewNormalButtonPictureUp As IPictureDisp)

   Call SetButtonPicture(0, NewNormalButtonPictureUp, "NormalButtonPictureUp", False)

End Property

Public Property Get NormalButtonUpColor() As OLE_COLOR
Attribute NormalButtonUpColor.VB_Description = "Returns/sets the color for an normal header button when it is up."

   NormalButtonUpColor = m_NormalButtonUpColor

End Property

Public Property Let NormalButtonUpColor(ByVal NewNormalButtonUpColor As OLE_COLOR)

   m_NormalButtonUpColor = NewNormalButtonUpColor
   PropertyChanged "NormalButtonUpColor"
   
   Call Refresh

End Property

Public Property Get NormalHeaderBackColor() As OLE_COLOR
Attribute NormalHeaderBackColor.VB_Description = "Returns/sets the background color for an normal header."

   NormalHeaderBackColor = m_NormalHeaderBackColor

End Property

Public Property Let NormalHeaderBackColor(ByVal NewNormalHeaderBackColor As OLE_COLOR)

   m_NormalHeaderBackColor = NewNormalHeaderBackColor
   PropertyChanged "NormalHeaderBackColor"
   
   Call Refresh

End Property

Public Property Get NormalHeaderForeColor() As OLE_COLOR
Attribute NormalHeaderForeColor.VB_Description = "Returns/sets the foreground color for an normal header."

   NormalHeaderForeColor = m_NormalHeaderForeColor

End Property

Public Property Let NormalHeaderForeColor(ByVal NewNormalHeaderForeColor As OLE_COLOR)

   m_NormalHeaderForeColor = NewNormalHeaderForeColor
   PropertyChanged "NormalHeaderForeColor"
   
   Call Refresh

End Property

Public Property Get NormalHeaderHoverColor() As OLE_COLOR
Attribute NormalHeaderHoverColor.VB_Description = "Returns/sets the color for an normal header when the mouse is hover the header."

   NormalHeaderHoverColor = m_NormalHeaderHoverColor

End Property

Public Property Let NormalHeaderHoverColor(ByVal NewNormalHeaderHoverColor As OLE_COLOR)

   m_NormalHeaderHoverColor = NewNormalHeaderHoverColor
   PropertyChanged "NormalHeaderHoverColor"
   
   Call Refresh

End Property

Public Property Get NormalItemBackColor() As OLE_COLOR
Attribute NormalItemBackColor.VB_Description = "Returns/sets the background color for an normal item group."

   NormalItemBackColor = m_NormalItemBackColor

End Property

Public Property Let NormalItemBackColor(ByVal NewNormalItemBackColor As OLE_COLOR)

   m_NormalItemBackColor = NewNormalItemBackColor
   PropertyChanged "NormalItemBackColor"
   
   Call Refresh

End Property

Public Property Get NormalItemBorderColor() As OLE_COLOR
Attribute NormalItemBorderColor.VB_Description = "Returns/sets the border color for an normal item group."

   NormalItemBorderColor = m_NormalItemBorderColor

End Property

Public Property Let NormalItemBorderColor(ByVal NewNormalItemBorderColor As OLE_COLOR)

   m_NormalItemBorderColor = NewNormalItemBorderColor
   PropertyChanged "NormalItemBorderColor"
   
   Call Refresh

End Property

Public Property Get NormalItemForeColor() As OLE_COLOR
Attribute NormalItemForeColor.VB_Description = "Returns/sets the foreground color for an normal item group."

   NormalItemForeColor = m_NormalItemForeColor

End Property

Public Property Let NormalItemForeColor(ByVal NewNormalItemForeColor As OLE_COLOR)

   m_NormalItemForeColor = NewNormalItemForeColor
   PropertyChanged "NormalItemForeColor"
   
   Call Refresh

End Property

Public Property Get NormalItemHoverColor() As OLE_COLOR
Attribute NormalItemHoverColor.VB_Description = "Returns/sets the color for an normal item group when the mouse is hover the item."

   NormalItemHoverColor = m_NormalItemHoverColor

End Property

Public Property Let NormalItemHoverColor(ByVal NewNormalItemHoverColor As OLE_COLOR)

   m_NormalItemHoverColor = NewNormalItemHoverColor
   PropertyChanged "NormalItemHoverColor"
   
   Call Refresh

End Property

Public Property Get OpenOneGroupOnly() As Boolean
Attribute OpenOneGroupOnly.VB_Description = "Determins whether one groups is open at a time, except fixed groups."

   OpenOneGroupOnly = m_OpenOneGroupOnly

End Property

Public Property Let OpenOneGroupOnly(ByVal NewOpenOneGroupOnly As Boolean)

   m_OpenOneGroupOnly = NewOpenOneGroupOnly
   PropertyChanged "OpenOneGroupOnly"
   
   Call Refresh

End Property

Public Property Get ShowBorder() As Boolean
Attribute ShowBorder.VB_Description = "Determins whether the object border will be showed."

   ShowBorder = shpBorder.Visible

End Property

Public Property Let ShowBorder(ByVal NewShowBorder As Boolean)

   shpBorder.Visible = NewShowBorder

End Property

Public Property Get SoundGroupClicked() As Boolean
Attribute SoundGroupClicked.VB_Description = "Determins whether a sound will be played when a group is clicked."

   SoundGroupClicked = m_SoundGroupClicked

End Property

Public Property Let SoundGroupClicked(ByVal NewSoundGroupClicked As Boolean)

   m_SoundGroupClicked = NewSoundGroupClicked
   PropertyChanged "SoundGroupClicked"

End Property

Public Property Get SoundItemClicked() As Boolean
Attribute SoundItemClicked.VB_Description = "Determins whether a sound will be played when a item is clicked."

   SoundItemClicked = m_SoundItemClicked

End Property

Public Property Let SoundItemClicked(ByVal NewSoundItemClicked As Boolean)

   m_SoundItemClicked = NewSoundItemClicked
   PropertyChanged "SoundItemClicked"

End Property

Public Property Get SpecialArrowDownColor() As OLE_COLOR
Attribute SpecialArrowDownColor.VB_Description = "Returns/sets the arrow color when an special header is up."

   SpecialArrowDownColor = m_SpecialArrowDownColor

End Property

Public Property Let SpecialArrowDownColor(ByVal NewSpecialArrowDownColor As OLE_COLOR)

   m_SpecialArrowDownColor = NewSpecialArrowDownColor
   PropertyChanged "SpecialArrowDownColor"
   
   Call Refresh

End Property

Public Property Get SpecialArrowHoverColor() As OLE_COLOR
Attribute SpecialArrowHoverColor.VB_Description = "Returns/sets the arrow color when the mouse is hover an special header."

   SpecialArrowHoverColor = m_SpecialArrowHoverColor

End Property

Public Property Let SpecialArrowHoverColor(ByVal NewSpecialArrowHoverColor As OLE_COLOR)

   m_SpecialArrowHoverColor = NewSpecialArrowHoverColor
   PropertyChanged "SpecialArrowHoverColor"
   
   Call Refresh

End Property

Public Property Get SpecialArrowUpColor() As OLE_COLOR
Attribute SpecialArrowUpColor.VB_Description = "Returns/sets the arrow color when an special header is up."

   SpecialArrowUpColor = m_SpecialArrowUpColor

End Property

Public Property Let SpecialArrowUpColor(ByVal NewSpecialArrowUpColor As OLE_COLOR)

   m_SpecialArrowUpColor = NewSpecialArrowUpColor
   PropertyChanged "SpecialArrowUpColor"
   
   Call Refresh

End Property

Public Property Get SpecialButtonBackColor() As OLE_COLOR
Attribute SpecialButtonBackColor.VB_Description = "Returns/sets the background color for an special header button."

   SpecialButtonBackColor = m_SpecialButtonBackColor

End Property

Public Property Let SpecialButtonBackColor(ByVal NewSpecialButtonBackColor As OLE_COLOR)

   m_SpecialButtonBackColor = NewSpecialButtonBackColor
   PropertyChanged "SpecialButtonBackColor"
   
   Call Refresh

End Property

Public Property Get SpecialButtonDownColor() As OLE_COLOR
Attribute SpecialButtonDownColor.VB_Description = "Returns/sets the color for an special header button when it is down."

   SpecialButtonDownColor = m_SpecialButtonDownColor

End Property

Public Property Let SpecialButtonDownColor(ByVal NewSpecialButtonDownColor As OLE_COLOR)

   m_SpecialButtonDownColor = NewSpecialButtonDownColor
   PropertyChanged "SpecialButtonDownColor"
   
   Call Refresh

End Property

Public Property Get SpecialButtonHoverColor() As OLE_COLOR
Attribute SpecialButtonHoverColor.VB_Description = "Returns/sets the color for an special header button when the mouse is hover the header."

   SpecialButtonHoverColor = m_SpecialButtonHoverColor

End Property

Public Property Let SpecialButtonHoverColor(ByVal NewSpecialButtonHoverColor As OLE_COLOR)

   m_SpecialButtonHoverColor = NewSpecialButtonHoverColor
   PropertyChanged "SpecialButtonHoverColor"
   
   Call Refresh

End Property

Public Property Get SpecialButtonPictureDown() As IPictureDisp
Attribute SpecialButtonPictureDown.VB_Description = "Returns/sets a graphic for an special header button when it is down."

   Set SpecialButtonPictureDown = m_ButtonPicture(5)

End Property

Public Property Let SpecialButtonPictureDown(ByRef NewSpecialButtonPictureDown As IPictureDisp)

   Set SpecialButtonPictureDown = NewSpecialButtonPictureDown

End Property

Public Property Set SpecialButtonPictureDown(ByRef NewSpecialButtonPictureDown As IPictureDisp)

   Call SetButtonPicture(5, NewSpecialButtonPictureDown, "SpecialButtonPictureDown", True)

End Property

Public Property Get SpecialButtonPictureHover() As IPictureDisp
Attribute SpecialButtonPictureHover.VB_Description = "Returns/sets a graphic for an special header button when the mouse is hover the header."

   Set SpecialButtonPictureHover = m_ButtonPicture(4)

End Property

Public Property Let SpecialButtonPictureHover(ByRef NewSpecialButtonPictureHover As IPictureDisp)

   Set SpecialButtonPictureHover = NewSpecialButtonPictureHover

End Property

Public Property Set SpecialButtonPictureHover(ByRef NewSpecialButtonPictureHover As IPictureDisp)

   Call SetButtonPicture(4, NewSpecialButtonPictureHover, "SpecialButtonPictureHover", True)

End Property

Public Property Get SpecialButtonPictureUp() As IPictureDisp
Attribute SpecialButtonPictureUp.VB_Description = "Returns/sets a graphic for an special header button when it is up."

   Set SpecialButtonPictureUp = m_ButtonPicture(3)

End Property

Public Property Let SpecialButtonPictureUp(ByRef NewSpecialButtonPictureUp As IPictureDisp)

   Set SpecialButtonPictureUp = NewSpecialButtonPictureUp

End Property

Public Property Set SpecialButtonPictureUp(ByRef NewSpecialButtonPictureUp As IPictureDisp)

   Call SetButtonPicture(3, NewSpecialButtonPictureUp, "SpecialButtonPictureUp", True)

End Property

Public Property Get SpecialButtonUpColor() As OLE_COLOR
Attribute SpecialButtonUpColor.VB_Description = "Returns/sets the color for an special header button when it is up."

   SpecialButtonUpColor = m_SpecialButtonUpColor

End Property

Public Property Let SpecialButtonUpColor(ByVal NewSpecialButtonUpColor As OLE_COLOR)

   m_SpecialButtonUpColor = NewSpecialButtonUpColor
   PropertyChanged "SpecialButtonUpColor"
   
   Call Refresh

End Property

Public Property Get SpecialHeaderBackColor() As OLE_COLOR
Attribute SpecialHeaderBackColor.VB_Description = "Returns/sets the background color for an special header."

   SpecialHeaderBackColor = m_SpecialHeaderBackColor

End Property

Public Property Let SpecialHeaderBackColor(ByVal NewSpecialHeaderBackColor As OLE_COLOR)

   m_SpecialHeaderBackColor = NewSpecialHeaderBackColor
   PropertyChanged "SpecialHeaderBackColor"
   
   Call Refresh

End Property

Public Property Get SpecialHeaderForeColor() As OLE_COLOR
Attribute SpecialHeaderForeColor.VB_Description = "Returns/sets the foreground color for an special header."

   SpecialHeaderForeColor = m_SpecialHeaderForeColor

End Property

Public Property Let SpecialHeaderForeColor(ByVal NewSpecialHeaderForeColor As OLE_COLOR)

   m_SpecialHeaderForeColor = NewSpecialHeaderForeColor
   PropertyChanged "SpecialHeaderForeColor"
   
   Call Refresh

End Property

Public Property Get SpecialHeaderHoverColor() As OLE_COLOR
Attribute SpecialHeaderHoverColor.VB_Description = "Returns/sets the color for an special header when the mouse is hover the header."

   SpecialHeaderHoverColor = m_SpecialHeaderHoverColor

End Property

Public Property Let SpecialHeaderHoverColor(ByVal NewSpecialHeaderHoverColor As OLE_COLOR)

   m_SpecialHeaderHoverColor = NewSpecialHeaderHoverColor
   PropertyChanged "SpecialHeaderHoverColor"
   
   Call Refresh

End Property

Public Property Get SpecialItemBackColor() As OLE_COLOR
Attribute SpecialItemBackColor.VB_Description = "Returns/sets the background color for an special item group."

   SpecialItemBackColor = m_SpecialItemBackColor

End Property

Public Property Let SpecialItemBackColor(ByVal NewSpecialItemBackColor As OLE_COLOR)

   m_SpecialItemBackColor = NewSpecialItemBackColor
   PropertyChanged "SpecialItemBackColor"
   
   Call Refresh

End Property

Public Property Get SpecialItemBorderColor() As OLE_COLOR
Attribute SpecialItemBorderColor.VB_Description = "Returns/sets the border color for an special item group."

   SpecialItemBorderColor = m_SpecialItemBorderColor

End Property

Public Property Let SpecialItemBorderColor(ByVal NewSpecialItemBorderColor As OLE_COLOR)

   m_SpecialItemBorderColor = NewSpecialItemBorderColor
   PropertyChanged "SpecialItemBorderColor"
   
   Call Refresh

End Property

Public Property Get SpecialItemForeColor() As OLE_COLOR
Attribute SpecialItemForeColor.VB_Description = "Returns/sets the foreground color for an special item group."

   SpecialItemForeColor = m_SpecialItemForeColor

End Property

Public Property Let SpecialItemForeColor(ByVal NewSpecialItemForeColor As OLE_COLOR)

   m_SpecialItemForeColor = NewSpecialItemForeColor
   PropertyChanged "SpecialItemForeColor"
   
   Call Refresh

End Property

Public Property Get SpecialItemHoverColor() As OLE_COLOR
Attribute SpecialItemHoverColor.VB_Description = "Returns/sets the color for an special item group when the mouse is hover the item."

   SpecialItemHoverColor = m_SpecialItemHoverColor

End Property

Public Property Let SpecialItemHoverColor(ByVal NewSpecialItemHoverColor As OLE_COLOR)

   m_SpecialItemHoverColor = NewSpecialItemHoverColor
   PropertyChanged "SpecialItemHoverColor"
   
   Call Refresh

End Property

Public Property Get UseAlphaBlend() As Boolean
Attribute UseAlphaBlend.VB_Description = "Determins whether alphablend will be used by animations."

   UseAlphaBlend = m_UseAlphaBlend

End Property

Public Property Let UseAlphaBlend(ByVal NewUseAlphaBlend As Boolean)

   m_UseAlphaBlend = NewUseAlphaBlend
   PropertyChanged "UseAlphaBlend"

End Property

Public Property Get UseWindowTheme() As Boolean
Attribute UseWindowTheme.VB_Description = "Determins whether Windows themes will be used."

   UseWindowTheme = m_UseWindowTheme

End Property

Public Property Let UseWindowTheme(ByVal NewUseWindowTheme As Boolean)

   m_UseWindowTheme = NewUseWindowTheme
   PropertyChanged "UseWindowTheme"
   
   Call Refresh

End Property

Public Function AddDetailGroup(ByVal Title As String, ByVal DetailTitle As String, ByVal DetailCaption As String, Optional ByVal ToolTipText As String, Optional ByRef DetailPicture As IPictureDisp, Optional ByVal WindowState As WindowStates, Optional ByRef BigIcon As IPictureDisp, Optional ByRef ItemsBackground As IPictureDisp) As Integer

   AddDetailGroup = AddGroup(Title, ToolTipText, WindowState, BigIcon, ItemsBackground, , True, DetailTitle, DetailCaption, DetailPicture)

End Function

Public Function AddItem(ByVal Index As Integer, ByVal Caption As String, Optional ByVal Tag As String, Optional ByVal ToolTipText As String, Optional IsHyperlink As Boolean = True, Optional ByRef SmallIcon As IPictureDisp, Optional ByVal Active As Boolean) As Integer

   With Groups(Index)
      If .MenuType = MENU_DETAILS Then Exit Function
      
      ReDim Preserve .Items(.ItemCount) As GroupItemType
      
      With .Items(.ItemCount)
         .Caption = Caption
         .Tag = Tag
         .Hyperlink = IsHyperlink
         .Active = Active
      End With
      
      If IsHyperlink Then .Items(.ItemCount).ToolTipText = ToolTipText
      If Not SmallIcon Is Nothing Then Set .Items(.ItemCount).SmallIcon = SmallIcon
      
      .Items(.ItemCount).State = STATE_NORMAL
      AddItem = .ItemCount
      .ItemCount = .ItemCount + 1
      
      Call Refresh
   End With

End Function

Public Function AddNormalGroup(ByVal Title As String, Optional ByVal ToolTipText As String, Optional ByVal WindowState As WindowStates, Optional ByRef BigIcon As IPictureDisp, Optional ByRef ItemsBackground As IPictureDisp) As Integer

   AddNormalGroup = AddGroup(Title, ToolTipText, WindowState, BigIcon, ItemsBackground)

End Function

Public Function AddSpecialGroup(ByVal Title As String, Optional ByVal ToolTipText As String, Optional ByVal WindowState As WindowStates, Optional ByRef BigIcon As IPictureDisp, Optional ByRef ItemsBackground As IPictureDisp) As Integer

   AddSpecialGroup = AddGroup(Title, ToolTipText, WindowState, BigIcon, ItemsBackground, True)

End Function

Public Function DeleteGroup(ByVal Index As Integer) As Integer

Dim intCount As Integer

   DeleteGroup = -1
   
   If Not CheckIndex(Index) Then Exit Function
   
   For intCount = Index To UBound(Groups) - 1
      Groups(intCount) = Groups(intCount + 1)
   Next 'intCount
   
   If intCount > 0 Then
      ReDim Preserve Groups(intCount - 1) As GroupType
      
   Else
      Erase Groups
   End If
   
   GroupCount = GroupCount - 1
   DeleteGroup = GroupCount
   
   If GroupCount Then
      intCount = picGroup.Count - 1
      
      Call Subclass_DelMsg(picGroup.Item(intCount).hWnd, WM_MOUSEWHEEL)
      Call Subclass_Stop(picGroup.Item(intCount).hWnd)
      
      Unload picGroup.Item(picGroup.Count - 1)
      intCount = picItems.Count - 1
      
      Call Subclass_DelMsg(picItems.Item(intCount).hWnd, WM_MOUSEWHEEL)
      Call Subclass_Stop(picItems.Item(intCount).hWnd)
      
      Unload picItems.Item(picItems.Count - 1)
   End If
   
   Call Refresh

End Function

Public Function DeleteItem(ByVal Index As Integer, ByVal Item As Integer) As Integer

Dim intItem As Integer

   DeleteItem = -1
   
   If Not CheckIndex(Index) Then Exit Function
   
   With Groups(Index)
      If Not CheckItem(Index, Item) Or (.MenuType = MENU_DETAILS) Then Exit Function
      
      For intItem = Item To UBound(.Items) - 1
         .Items(intItem) = .Items(intItem + 1)
      Next 'intItem
      
      If intItem > 0 Then
         ReDim Preserve .Items(intItem - 1) As GroupItemType
         
      Else
         Erase .Items
      End If
      
      .ItemCount = .ItemCount - 1
      DeleteItem = .ItemCount
      
      Call Refresh
   End With

End Function

Public Function FullTextShowed(ByVal Index As Integer, Optional Item As Integer = -1) As Boolean

   If Not CheckIndex(Index) Then Exit Function
   
   If Item > -1 Then
      If Not CheckItem(Index, Item) Or (Groups(Index).MenuType = MENU_DETAILS) Then Exit Function
      
      FullTextShowed = Groups(Index).Items(Item).FullTextShowed
      
   Else
      FullTextShowed = Groups(Index).FullTextShowed
   End If

End Function

Public Function GetGroupTitle(ByVal Index As Integer) As String

   If Not CheckIndex(Index) Then Exit Function
   
   GetGroupTitle = Groups(Index).Title

End Function

Public Function GetDetailCaption(ByVal Index As Integer) As String

   If Not CheckIndex(Index) Or (Groups(Index).MenuType <> MENU_DETAILS) Then Exit Function

End Function

Public Function GetGroupCount() As Integer

   GetGroupCount = GroupCount

End Function

Public Function GetGroupIcon(ByVal Index As Integer) As IPictureDisp

   If Not CheckIndex(Index) Then Exit Function
   
   Set GetGroupIcon = Groups(Index).BigIcon

End Function

Public Function GetItemActive(ByVal Index As Integer, ByVal Item As Integer) As Boolean

   If Not CheckIndex(Index) Or Not CheckItem(Index, Item) Or (Groups(Index).MenuType = MENU_DETAILS) Then Exit Function
   
   GetItemActive = Groups(Index).Items(Item).Active

End Function

Public Function GetItemCaption(ByVal Index As Integer, ByVal Item As Integer, Optional ByVal StripTab As Boolean) As String

Dim strCaption As String

   If Not CheckIndex(Index) Or Not CheckItem(Index, Item) Or (Groups(Index).MenuType = MENU_DETAILS) Then Exit Function
   
   strCaption = Groups(Index).Items(Item).Caption
   
   If StripTab And InStr(strCaption, vbTab) Then strCaption = Replace(strCaption, vbTab, " ", 1)
   
   GetItemCaption = strCaption

End Function

Public Function GetItemCount(ByVal Index As Integer) As Long

   GetItemCount = -1
   
   If Not CheckIndex(Index) Or (Groups(Index).MenuType = MENU_DETAILS) Then Exit Function
   
   GetItemCount = Groups(Index).ItemCount

End Function

Public Function GetItemHyperlink(ByVal Index As Integer, ByVal Item As Integer) As Boolean

   If Not CheckIndex(Index) Or Not CheckItem(Index, Item) Or (Groups(Index).MenuType = MENU_DETAILS) Then Exit Function
   
   GetItemHyperlink = Groups(Index).Items(Item).Hyperlink

End Function

Public Function GetItemIcon(ByVal Index As Integer, ByVal Item As Integer) As IPictureDisp

   If Not CheckIndex(Index) Or Not CheckItem(Index, Item) Or (Groups(Index).MenuType = MENU_DETAILS) Then Exit Function
   
   Set GetItemIcon = Groups(Index).Items(Item).SmallIcon

End Function

Public Function GetItemTag(ByVal Index As Integer, ByVal Item As Integer) As String

   If Not CheckIndex(Index) Or Not CheckItem(Index, Item) Or (Groups(Index).MenuType = MENU_DETAILS) Then Exit Function
   
   GetItemTag = Groups(Index).Items(Item).Tag

End Function

Public Function GetWindowState(ByVal Index As Integer) As WindowStates

   GetWindowState = -1
   
   If Not CheckIndex(Index) Then Exit Function
   
   GetWindowState = Groups(Index).WindowState

End Function

Public Function SetDetailCaption(ByVal Index As Integer, ByVal NewCaption As String) As Boolean

   SetDetailCaption = SetGroupObject(IsDetailCaption, Index, , NewCaption)

End Function

Public Function SetDetailPicture(ByVal Index As Integer, Optional ByRef NewPicture As IPictureDisp) As Boolean

   SetDetailPicture = SetGroupObject(IsDetailPicture, Index, , , , NewPicture)

End Function

Public Function SetDetailTitle(ByVal Index As Integer, ByVal NewTitle As String) As Boolean

   SetDetailTitle = SetGroupObject(IsDetailTitle, Index, , NewTitle)

End Function

Public Function SetItemActive(ByVal Index As Integer, ByVal Item As Integer, Optional ByVal NewActive As Boolean) As Boolean

   SetItemActive = SetGroupObject(IsItemActive, Index, Item, , , , , NewActive)

End Function

Public Function SetItemBackgroundPicture(ByVal Index As Integer, Optional ByRef NewPicture As IPictureDisp) As Boolean

   SetItemBackgroundPicture = SetGroupObject(IsItemBackgroundPicture, Index, , , , NewPicture)

End Function

Public Function SetItemCaption(ByVal Index As Integer, ByVal Item As Integer, Optional ByVal NewCaption As String) As Boolean

   SetItemCaption = SetGroupObject(IsItemCaption, Index, Item, NewCaption)

End Function

Public Function SetItemHyperlink(ByVal Index As Integer, ByVal Item As Integer, Optional ByVal NewHypelink As Boolean) As Boolean

   SetItemHyperlink = SetGroupObject(IsItemHyperlink, Index, Item, , , , NewHypelink)

End Function

Public Function SetItemSmallIcon(ByVal Index As Integer, ByVal Item As Integer, Optional ByRef NewIcon As IPictureDisp) As Boolean

   SetItemSmallIcon = SetGroupObject(IsItemSmallIcon, Index, Item, , , NewIcon)

End Function

Public Function SetItemTag(ByVal Index As Integer, ByVal Item As Integer, Optional ByRef NewTag As String) As Boolean

   SetItemTag = SetGroupObject(IsItemTag, Index, Item, NewTag)

End Function

Public Function SetGroupBigIcon(ByVal Index As Integer, Optional ByRef NewIcon As IPictureDisp) As Boolean

   SetGroupBigIcon = SetGroupObject(IsGroupBigIcon, Index, , , , NewIcon)

End Function

Public Function SetGroupTitle(ByVal Index As Integer, Optional ByVal NewTitle As String) As Boolean

   SetGroupTitle = SetGroupObject(IsGroupTitle, Index, , NewTitle)

End Function

Public Function SetToolTipText(ByVal Index As Integer, Optional Item As Integer = -1, Optional ByVal ToolTipText As String) As Boolean

   SetToolTipText = SetGroupObject(IsToolTipText, Index, Item, ToolTipText)

End Function

Public Function SetWindowState(ByVal Index As Integer, Optional ByVal NewWindowState As WindowStates) As Boolean

   If (NewWindowState < Expanded) Or (NewWindowState > Fixed) Then Exit Function
   
   Do While tmrAnimation.Enabled
      DoEvents
   Loop
   
   SetWindowState = SetGroupObject(IsWindowState, Index, , , NewWindowState)

End Function

Public Sub ClearAllGroupItems(ByVal Index As Integer)

   If Not CheckIndex(Index) Then Exit Sub
   
   With Groups(Index)
      If .MenuType <> MENU_DETAILS Then
         .ItemCount = 0
         
         ReDim .Items(.ItemCount) As GroupItemType
         
         Call Refresh
      End If
   End With

End Sub

Public Sub ClearAllGroups()

   GroupCount = 0
   StateNormalGroup = False
   StateSpecialGroup = False
   
   ReDim Groups(GroupCount) As GroupType
   
   Call Refresh

End Sub

Public Sub Refresh()

Dim blnBold As Boolean
Dim lngMaxY As Long
Dim sngSize As Single

   If m_Locked Or tmrAnimation.Enabled Then Exit Sub
   
   Call DrawBackground
   
   If Ambient.UserMode Then
      lngMaxY = DrawGroups
      
      With tsbVertical
         If lngMaxY > ScaleHeight Then
            If Not .Visible Then
               .Top = 0
               .Left = ScaleWidth - .Width
               .Height = ScaleHeight
               .Max = GetMaxY(lngMaxY)
               .Visible = True
               DrawGroups
               
            ElseIf ScaleHeight <> .Height Then
               .Height = ScaleHeight
               .Max = GetMaxY(lngMaxY)
               
            ElseIf GetMaxY(lngMaxY) <> .Max Then
               .Height = ScaleHeight
               .Max = GetMaxY(lngMaxY)
            End If
            
         ElseIf .Visible Then
            .Visible = False
            .Left = ScaleWidth
            DrawGroups
         End If
      End With
      
   Else
      blnBold = FontBold
      sngSize = FontSize
      FontBold = True
      FontSize = 10
      ForeColor = &HFFFFFF
      CurrentX = (ScaleWidth - TextWidth(THEME_NAME)) / 2
      CurrentY = (ScaleHeight - TextHeight(THEME_NAME)) / 2
      Print THEME_NAME
      FontBold = blnBold
      FontSize = sngSize
   End If

End Sub

Private Function AddGroup(ByVal Title As String, ByVal ToolTipText As String, ByVal WindowState As WindowStates, ByRef BigIcon As IPictureDisp, ByRef ItemsBackground As IPictureDisp, Optional ByVal SpecialGroup As Boolean, Optional ByVal DetailGroup As Boolean, Optional ByVal DetailTitle As String, Optional ByVal DetailCaption As String, Optional ByRef DetailPicture As IPictureDisp) As Integer

Dim lngMenuType As Long

   If SpecialGroup Then
      lngMenuType = MENU_SPECIAL
      
      If WindowState = Expanded Then StateSpecialGroup = True
      
   Else
      lngMenuType = MENU_NORMAL
      
      If m_OpenOneGroupOnly And (WindowState = Expanded) Then
         If StateSpecialGroup Or StateNormalGroup Then
            WindowState = Collapsed
            
         Else
            StateNormalGroup = True
         End If
      End If
   End If
   
   ReDim Preserve Groups(GroupCount) As GroupType
   
   With Groups(GroupCount)
      ReDim .Items(0) As GroupItemType
      
      If DetailGroup Then
         .DetailTitle = DetailTitle
         .DetailCaption = DetailCaption
         lngMenuType = MENU_DETAILS
         
         If Not DetailPicture Is Nothing Then Set .DetailPicture = DetailPicture
      End If
      
      If Not BigIcon Is Nothing Then Set .BigIcon = BigIcon
      If Not ItemsBackground Is Nothing Then Set .ItemsBackgroundPicture = ItemsBackground
      
      If GroupCount Then
         Load picGroup.Item(GroupCount)
         Load picItems.Item(GroupCount)
         
         With picGroup.Item(GroupCount)
            .Font = UserControl.Font
            
            Call Subclass_Initialize(.hWnd)
            Call Subclass_AddMsg(.hWnd, WM_MOUSEWHEEL)
         End With
         
         With picItems.Item(GroupCount)
            .Font = UserControl.Font
            
            Call Subclass_Initialize(.hWnd)
            Call Subclass_AddMsg(.hWnd, WM_MOUSEWHEEL)
         End With
      End If
      
      .Title = Title
      .ToolTipText = ToolTipText
      .WindowState = WindowState
      .MenuType = lngMenuType
      .State = STATE_NORMAL
      AddGroup = GroupCount
      GroupCount = GroupCount + 1
   End With
   
   Call Refresh

End Function

Private Function CheckIndex(ByVal Index As Integer) As Boolean

   If (Index >= 0) And (Index <= UBound(Groups)) Then CheckIndex = True

End Function

Private Function CheckItem(ByVal Index As Integer, ByVal Item As Integer) As Boolean

   If (Item >= 0) And (Item <= UBound(Groups(Index).Items)) Then CheckItem = True

End Function

Private Function CountButtonPictures() As Boolean

Dim intCount As Integer

   For intCount = 0 To 5
      If m_ButtonPicture(intCount) Is Nothing Then Exit Function
   Next 'intCount
   
   CountButtonPictures = True

End Function

Private Function DrawDetails(ByVal Index As Integer, ByVal Y As Long) As Long

Dim lngBaseY       As Long
Dim lngPictureSize As Long
Dim lngY           As Long
Dim rctText        As Rect
Dim strMessage     As String

   With picItems.Item(Index)
      If Not Groups(Index).DetailPicture Is Nothing Then lngPictureSize = 100
      
      lngBaseY = 8
      strMessage = MakeMessage(Groups(Index).DetailCaption, tsbVertical.Left - 40)
      .Cls
      
      If Len(strMessage) Then
         .ForeColor = m_DetailsForeColor
         .FontUnderline = False
         .Top = picGroup.Item(Index).Top + picGroup.Item(Index).Height
         .Left = 10
         .Width = tsbVertical.Left - .Left * 2 + 1
         .Height = TextHeight(strMessage) + 28 + Y + (110 And (lngPictureSize > 0)) - .Top
      End If
      
      Call DrawThemePart(.hDC, EBP_NORMALGROUPBACKGROUND, STATE_NORMAL, ObjectRect(picItems.Item(Index)))
      
      If Not HasTheme Then Call DrawWindow(Index, m_NormalItemBorderColor, m_NormalItemBackColor, m_GradientNormalItemBackColor)
      If Not Groups(Index).ItemsBackgroundPicture Is Nothing Then .PaintPicture Groups(Index).ItemsBackgroundPicture, 1, 1, .ScaleWidth - 1, .ScaleHeight - 1
      
      If Not Groups(Index).DetailPicture Is Nothing Then
         If lngPictureSize Then
            On Error Resume Next
            .PaintPicture Groups(Index).DetailPicture, (.ScaleWidth - lngPictureSize) / 2, lngBaseY, lngPictureSize, lngPictureSize, , , , , vbSrcAnd
            lngBaseY = lngBaseY + lngPictureSize + 10
            On Error GoTo 0
         End If
      End If
      
      If Len(Groups(Index).DetailTitle) Then
         rctText.Top = lngBaseY
         rctText.Left = 10
         rctText.Right = .ScaleWidth - 10
         rctText.Bottom = lngBaseY + TextHeight(Groups(Index).DetailTitle)
         .FontBold = True
         DrawText .hDC, Groups(Index).DetailTitle, -1, rctText, DT_LEFT Or DT_WORD_ELLIPSIS
      End If
      
      If Len(strMessage) Then
         rctText.Top = lngBaseY + 20
         rctText.Bottom = .ScaleHeight
         .FontBold = False
         DrawText .hDC, strMessage, -1, rctText, DT_LEFT
      End If
   End With
   
   lngY = picItems.Item(Index).Height
   
   If lngY > 6 Then
      Call SetAnimation(Index)
      
   Else
      lngY = 0
   End If
   
   DrawDetails = Y + lngY

End Function

Private Function DrawGroup(ByVal Index As Integer, ByVal Y As Long) As Long

Const EBP_NORMALGROUPHEAD  As Integer = 8
Const EBP_SPECIALGROUPHEAD As Integer = 12

Dim intHeight              As Integer
Dim lngPartID              As Long
Dim objGroup               As Object
Dim rctFrame               As Rect

   ReDim lngColor(2) As Long
   
   GroupIndex = Index
   intHeight = 32 + (6 And (m_HeaderHeight = High))
   Set objGroup = picGroup.Item(Index)
   
   If Groups(Index).MenuType = MENU_SPECIAL Then
      lngColor(0) = m_GradientSpecialHeaderBackColor
      lngColor(1) = m_SpecialHeaderBackColor
      lngColor(2) = lngColor(1)
      lngPartID = EBP_SPECIALGROUPHEAD
      
   Else
      lngColor(0) = m_NormalHeaderBackColor
      lngColor(1) = m_GradientNormalHeaderBackColor
      lngColor(2) = lngColor(0)
      lngPartID = EBP_NORMALGROUPHEAD
   End If
   
   With objGroup
      .Top = Y - 6
      .Left = 10
      .Width = tsbVertical.Left - .Left * 2 + 1
      .Height = intHeight
      
      If .Top > -1 Then .PaintPicture Picture, 0, 0, .ScaleWidth, .ScaleHeight, .Left, .Top, .ScaleWidth, .ScaleHeight, vbSrcCopy
      
      .Visible = True
      rctFrame.Top = 8 - (6 And (m_HeaderHeight = High))
      rctFrame.Left = 0
      rctFrame.Right = .Width
      rctFrame.Bottom = .Height
      
      Call DrawThemePart(.hDC, lngPartID, STATE_NORMAL, rctFrame)
   End With
   
   If Not HasTheme Then
      With rctFrame
         .Top = 6
         .Left = 0
         .Right = tsbVertical.Left - 19
         .Bottom = .Top + intHeight
         objGroup.Line (.Left, .Top)-(.Right - 1, .Bottom - 1), lngColor(2), BF
         
         Call DrawGradient(objGroup.hDC, rctFrame, lngColor(0), lngColor(1), Groups(Index).MenuType <> MENU_SPECIAL)
         
         .Right = tsbVertical.Left - 20
         objGroup.PSet (.Left, 6), Point(.Left, Y - 1)
         objGroup.PSet (.Left + 1, 6), Point(.Left + 1, Y - 1)
         objGroup.PSet (.Left, 7), Point(.Left, Y)
         objGroup.PSet (.Right, 6), Point(.Right, Y - 1)
         objGroup.PSet (.Right - 1, 6), Point(.Right - 1, Y - 1)
         objGroup.PSet (.Right, 7), Point(.Right, Y)
      End With
   End If
   
   Call DrawGroupTitle(Index)
   Call DrawButton(Index)
   
   DrawGroup = Y + intHeight
   Set objGroup = Nothing
   Erase lngColor

End Function

Private Function DrawGroups() As Long

Dim intIndex As Integer
Dim lngY     As Long

   lngY = 15 - ((tsbVertical.Value * 10) And tsbVertical.Visible)
   
   For intIndex = 0 To UBound(Groups)
      With Groups(intIndex)
         lngY = DrawGroup(intIndex, lngY)
         
         If (.MenuType = MENU_DETAILS) And (.WindowState <> Collapsed) Then
            lngY = DrawDetails(intIndex, lngY)
            
         ElseIf .WindowState <> Collapsed Then
            lngY = DrawItems(intIndex, lngY)
         End If
      End With
      
      lngY = lngY + 11 - (2 And HasTheme)
   Next 'intIndex
   
   DrawGroups = lngY + ((tsbVertical.Value * 10) And tsbVertical.Visible)

End Function

Private Function DrawItems(ByVal Index As Integer, ByVal Y As Long) As Long

Const EBP_SPECIALGROUPBACKGROUND As Integer = 9
Const DT_RIGHT                   As Long = &H2

Dim intItem                      As Integer
Dim lngBackColor                 As Long
Dim lngBorderColor               As Long
Dim lngGradientColor             As Long
Dim lngPartID                    As Long
Dim lngX                         As Long
Dim lngY                         As Long
Dim objItems                     As Object
Dim rctText                      As Rect
Dim strCaption()                 As String

   lngY = 8
   Set objItems = picItems.Item(Index)
   
   ReDim lngColor(1) As Long
   
   With objItems
      If Groups(Index).MenuType = MENU_SPECIAL Then
         lngBackColor = m_SpecialItemBackColor
         lngBorderColor = m_SpecialItemBorderColor
         lngGradientColor = m_GradientSpecialItemBackColor
         lngColor(0) = m_SpecialItemForeColor
         lngColor(1) = m_SpecialItemHoverColor
         lngPartID = EBP_SPECIALGROUPBACKGROUND
         
      Else
         lngBackColor = m_NormalItemBackColor
         lngBorderColor = m_NormalItemBorderColor
         lngGradientColor = m_GradientNormalItemBackColor
         lngColor(0) = m_NormalItemForeColor
         lngColor(1) = m_NormalItemHoverColor
         lngPartID = EBP_NORMALGROUPBACKGROUND
      End If
      
      .Cls
      .FontBold = False
      .Top = picGroup.Item(Index).Top + picGroup.Item(Index).Height
      .Left = 10
      .Width = tsbVertical.Left - .Left * 2 + 1
      .Height = Groups(Index).ItemCount * 24 + Y - .Top
      
      Call DrawThemePart(.hDC, lngPartID, STATE_NORMAL, ObjectRect(picItems.Item(Index)))
      
      If Not HasTheme Then Call DrawWindow(Index, lngBorderColor, lngBackColor, lngGradientColor)
      If Not Groups(Index).ItemsBackgroundPicture Is Nothing Then .PaintPicture Groups(Index).ItemsBackgroundPicture, 1, 1, .ScaleWidth - 2, .ScaleHeight - 2
   End With
   
   If Groups(Index).ItemCount > 0 Then
      For intItem = 0 To UBound(Groups(Index).Items)
         With Groups(Index).Items(intItem)
            If Not .SmallIcon Is Nothing Then
               lngX = 36
               objItems.PaintPicture .SmallIcon, 10, lngY, 16, 16
               
            Else
               lngX = 10
            End If
            
            If .State = STATE_NORMAL Then
               objItems.ForeColor = lngColor(0)
               objItems.FontUnderline = False
               
            ElseIf .State = STATE_HOT Then
               objItems.ForeColor = lngColor(1)
               objItems.FontUnderline = .Hyperlink
            End If
            
            If .Active Then
               objItems.FontBold = True
               
            Else
               objItems.FontBold = False
            End If
            
            rctText.Top = lngY + (16 - TextHeight(.Caption)) \ 2
            rctText.Bottom = rctText.Top + TextHeight(.Caption)
            .FullTextShowed = True
            
            If InStr(.Caption, vbTab) Then
               strCaption = Split(.Caption, vbTab, 2)
               
               With rctText
                  .Left = lngX
                  FontBold = objItems.FontBold
                  .Right = (objItems.ScaleWidth - 10) / 2 - 2
                  DrawText objItems.hDC, strCaption(0), -1, rctText, DT_LEFT Or DT_WORD_ELLIPSIS
                  
                  If Len(strCaption(0)) And (objItems.TextWidth(strCaption(0)) > rctText.Right - rctText.Left) Then Groups(Index).Items(intItem).FullTextShowed = False
                  
                  .Left = .Right + 2
                  .Right = objItems.ScaleWidth - 10
                  DrawText objItems.hDC, strCaption(1), -1, rctText, DT_RIGHT Or DT_WORD_ELLIPSIS
                  lngX = .Right
                  
                  If Len(strCaption(1)) And (objItems.TextWidth(strCaption(1)) > .Right - .Left) Then Groups(Index).Items(intItem).FullTextShowed = False
               End With
               
            Else
               rctText.Left = lngX
               rctText.Right = objItems.ScaleWidth - 10
               DrawText objItems.hDC, .Caption, -1, rctText, DT_LEFT Or DT_WORD_ELLIPSIS
               lngX = lngX + TextWidth(.Caption)
               
               If Len(.Caption) And (objItems.TextWidth(.Caption) > rctText.Right - rctText.Left) Then .FullTextShowed = False
            End If
            
            FontBold = False
            .Rect.Top = lngY
            .Rect.Left = 10
            .Rect.Right = lngX
            .Rect.Bottom = .Rect.Top + TextHeight(.Caption)
            lngY = lngY + 24
         End With
      Next 'intItem
      
      Call SetAnimation(Index)
      
      lngY = objItems.Height
      
   Else
      lngY = 0
   End If
   
   DrawItems = Y + lngY
   Set objItems = Nothing
   Erase lngColor, strCaption

End Function

Private Function GetColor(ByVal IsColor As Integer) As Integer

   GetColor = val("&H" & Hex((IsColor / &HFF&) * &HFFFF&))

End Function

Private Function GetMaxY(ByVal Y As Long)

   GetMaxY = (Y - ScaleHeight + (2 And HasTheme)) \ 10

End Function

Private Function IsFunctionSupported(ByVal sFunction As String, ByVal sModule As String) As Boolean

Dim lngModule As Long

   lngModule = GetModuleHandle(sModule)
   
   If lngModule = 0 Then lngModule = LoadLibrary(sModule)
   
   If lngModule Then
      IsFunctionSupported = GetProcAddress(lngModule, sFunction)
      FreeLibrary lngModule
   End If

End Function

Private Function MakeMessage(ByVal Message As String, ByVal Width As Long, Optional ByRef LinesCount As Integer) As String

Dim intLines    As Integer
Dim intPointer  As Integer
Dim lngCount    As Long
Dim strBuffer() As String
Dim strLine     As String
Dim strText     As String
Dim strWord     As String

   strBuffer = Split(Message)
   
   For lngCount = 0 To UBound(strBuffer)
      strWord = strBuffer(lngCount)
      
      If InStr(strWord, vbCrLf) Then
         intPointer = InStr(strWord, vbCrLf)
         strBuffer(lngCount) = Mid(strWord, intPointer)
         strWord = Left(strWord, intPointer - 1)
      End If
      
      If TextWidth(strText & " " & strWord) >= Width Then
         strLine = strLine & strText & vbCrLf
         intLines = intLines + 1
         strText = strWord
         
      Else
         strText = LTrim(strText & " " & strWord)
      End If
      
      If intPointer Then
         strLine = strLine & strText
         intPointer = 0
         
         Do
            strBuffer(lngCount) = Mid(strBuffer(lngCount), 3)
            strLine = strLine & vbCrLf
            intLines = intLines + 1
            
            If Left(strBuffer(lngCount), 2) <> vbCrLf Then
               lngCount = lngCount - 1
               strText = ""
               Exit Do
            End If
         Loop
      End If
   Next 'lngCount
   
   LinesCount = intLines + 1
   MakeMessage = strLine & strText

End Function

Private Function ObjectRect(ByRef Box As Object, Optional Top As Long) As Rect

   With Box
      ObjectRect.Top = Top
      ObjectRect.Left = 0
      ObjectRect.Right = .ScaleWidth
      ObjectRect.Bottom = .ScaleHeight
   End With

End Function

Private Function SetGroupObject(ByVal GroupObject As GroupObjects, ByVal Index As Integer, Optional ByVal Item As Integer, Optional ByVal NewText As String, Optional ByVal NewWindowState As WindowStates, Optional ByRef NewPicture As IPictureDisp, Optional ByVal NewHyperlink As Boolean, Optional ByVal NewActive As Boolean) As Boolean

Dim blnRefresh As Boolean

   On Local Error GoTo ExitFunction
   blnRefresh = True
   
   With Groups(Index)
      Select Case GroupObject
         Case IsItemCaption
            .Items(Item).Caption = NewText
            
         Case IsItemBackgroundPicture
            Set .ItemsBackgroundPicture = NewPicture
            
         Case IsItemHyperlink
            .Items(Item).Hyperlink = NewHyperlink
            
         Case IsItemActive
            .Items(Item).Active = NewActive
            
         Case IsItemSmallIcon
            Set .Items(Item).SmallIcon = NewPicture
            
         Case IsItemTag
            .Items(Item).Tag = NewText
            
         Case IsGroupBigIcon
            Set .BigIcon = NewPicture
            
         Case IsGroupTitle
            .Title = NewText
            
         Case IsToolTipText
            blnRefresh = False
            
            If Item > -1 Then
               .Items(Item).ToolTipText = NewText
               
            Else
               .ToolTipText = NewText
            End If
            
         Case IsWindowState
            AnimationIndex = Index
            
            If NewWindowState = Collapsed Then
               .State = STATE_HOT
               
               Call SetCollapseAnimation(Index)
               
            Else
               .State = STATE_NORMAL
            End If
            
            .WindowState = NewWindowState
            
            If .WindowState = Expanded Then Call CheckOpenGroups(Index)
            
         Case Else
            If .MenuType <> MENU_DETAILS Then GoTo ExitFunction
            
            Select Case GroupObject
               Case IsDetailCaption
                  .DetailCaption = NewText
                  
               Case IsDetailPicture
                  Set .DetailPicture = NewPicture
                  
               Case IsDetailTitle
                  .DetailTitle = NewText
            End Select
      End Select
   End With
   
   SetGroupObject = True
   
   If blnRefresh Then Call Refresh
   
ExitFunction:
   On Local Error GoTo 0

End Function

Private Function TranslateColor(ByVal Colors As OLE_COLOR, Optional ByVal Palette As Long) As Long

   If OleTranslateColor(Colors, Palette, TranslateColor) Then TranslateColor = -1

End Function

Private Function TranslateRGB(ByVal ColorVal As Long, ByVal Part As Long) As Long

Dim strHex As String

   strHex = Trim(Hex(ColorVal))
   TranslateRGB = val("&H" + UCase(Mid(Right("000000", 6 - Len(strHex)) & strHex, 5 - Part * 2, 2)))

End Function

Private Sub CheckOpenGroups(ByVal OpenGroup As Integer)

Dim intIndex As Integer

   If Not m_OpenOneGroupOnly Or (Groups(OpenGroup).WindowState = Fixed) Then Exit Sub
   
   For intIndex = 0 To UBound(Groups)
      If intIndex <> OpenGroup Then
         If Groups(intIndex).WindowState = Expanded Then
            Groups(intIndex).WindowState = Collapsed
            picItems.Item(intIndex).Visible = False
         End If
      End If
   Next 'intIndex

End Sub

Private Sub DoAlphaBlend()

Const AC_SRC_OVER    As Long = &H0

Dim blfAlpha         As BlendFunction
Dim lngBlendFunction As Long

   With blfAlpha
      .BlendOp = AC_SRC_OVER
      .BlendFlags = 0
      .AlphaFormat = 0
   End With
   
   With picItems.Item(AnimationIndex)
      blfAlpha.SourceConstantAlpha = AlphaPercent
      
      Call CopyMemory(lngBlendFunction, blfAlpha, 4)
      
      AlphaBlend .hDC, 0, 0, .ScaleWidth, .ScaleHeight, UserControl.hDC, 0, 0, .ScaleWidth, .ScaleHeight, lngBlendFunction
   End With

End Sub

Private Sub DrawArrow(ByVal Index As Integer, ByVal X As Long, ByVal Y As Long, ByVal IsWindowState As WindowStates, ByVal Color As Long)

Dim objGroup As Object

   Set objGroup = picGroup.Item(Index)
   
   If IsWindowState = Collapsed Then
      objGroup.Line (X + 1, Y + 11)-(X + 4, Y + 14), Color
      objGroup.Line (X + 4, Y + 12)-(X + 6, Y + 10), Color
      objGroup.Line (X, Y + 11)-(X + 4, Y + 15), Color
      objGroup.Line (X + 4, Y + 13)-(X + 7, Y + 10), Color
      objGroup.Line (X + 1, Y + 15)-(X + 4, Y + 18), Color
      objGroup.Line (X + 4, Y + 16)-(X + 6, Y + 14), Color
      objGroup.Line (X, Y + 15)-(X + 4, Y + 19), Color
      objGroup.Line (X + 4, Y + 17)-(X + 7, Y + 14), Color
      
   Else
      objGroup.Line (X + 1, Y + 13)-(X + 4, Y + 10), Color
      objGroup.Line (X + 4, Y + 12)-(X + 6, Y + 14), Color
      objGroup.Line (X, Y + 13)-(X + 4, Y + 9), Color
      objGroup.Line (X + 4, Y + 11)-(X + 7, Y + 14), Color
      objGroup.Line (X + 1, Y + 17)-(X + 4, Y + 14), Color
      objGroup.Line (X + 4, Y + 16)-(X + 6, Y + 18), Color
      objGroup.Line (X, Y + 17)-(X + 4, Y + 13), Color
      objGroup.Line (X + 4, Y + 15)-(X + 7, Y + 18), Color
   End If
   
   Set objGroup = Nothing

End Sub

Private Sub DrawBackground()

Const EBP_HEADERBACKGROUND As Integer = 1

Dim rctFrame               As Rect

   With rctFrame
      .Top = 0
      .Left = 0
      .Right = ScaleWidth
      .Bottom = ScaleHeight
   End With
   
   UserControl.Cls
   
   Call DrawThemePart(hDC, EBP_HEADERBACKGROUND, STATE_NORMAL, rctFrame)
   
   If Not HasTheme Then Call DrawGradient(hDC, rctFrame, m_GradientBackColor, m_BackColor)
   
   Picture = Image

End Sub

Private Sub DrawButton(ByVal Index As Integer)

Const EBP_NORMALGROUPCOLLAPSE  As Integer = 6
Const EBP_NORMALGROUPEXPAND    As Integer = 7
Const EBP_SPECIALGROUPCOLLAPSE As Integer = 10
Const EBP_SPECIALGROUPEXPAND   As Integer = 11

Dim intHeight                  As Integer
Dim lngCollapse                As Long
Dim lngColor(2)                As Long
Dim lngWindowState             As Long
Dim objGroup                   As Object
Dim rctButton                  As Rect

   Set objGroup = picGroup.Item(Index)
   intHeight = (4 And (m_HeaderHeight = High))
   objGroup.FillStyle = vbFSSolid
   
   With rctButton
      .Top = 9 + intHeight
      .Left = tsbVertical.Left - 46
      .Right = .Left + 24
      .Bottom = .Top + 24
   End With
   
   With Groups(Index)
      If .MenuType = MENU_SPECIAL Then
         lngCollapse = EBP_SPECIALGROUPCOLLAPSE
         lngWindowState = EBP_SPECIALGROUPEXPAND
         lngColor(0) = m_SpecialButtonUpColor
         lngColor(1) = m_SpecialButtonBackColor
         lngColor(2) = m_SpecialArrowUpColor
         
      Else
         lngCollapse = EBP_NORMALGROUPCOLLAPSE
         lngWindowState = EBP_NORMALGROUPEXPAND
         lngColor(0) = m_NormalButtonUpColor
         lngColor(1) = m_NormalButtonBackColor
         lngColor(2) = m_NormalArrowUpColor
      End If
      
      If .WindowState <> Fixed Then
         If .WindowState <> Collapsed Then lngWindowState = lngCollapse
         
         Call DrawThemePart(objGroup.hDC, lngWindowState, .State, rctButton)
         
         If Not HasTheme Then
            If .State > STATE_NORMAL Then
               If .State = STATE_PRESSED Then FillStyle = vbFSSolid
               
               If .MenuType = MENU_SPECIAL Then
                  If .State = STATE_PRESSED Then
                     lngColor(0) = m_SpecialButtonHoverColor
                     lngColor(1) = m_SpecialButtonDownColor
                     lngColor(2) = m_SpecialArrowDownColor
                     
                  Else
                     lngColor(0) = m_SpecialButtonHoverColor
                     lngColor(1) = m_SpecialButtonBackColor
                     lngColor(2) = m_SpecialArrowHoverColor
                  End If
                  
               Else
                  If .State = STATE_PRESSED Then
                     lngColor(0) = m_NormalButtonHoverColor
                     lngColor(1) = m_NormalButtonDownColor
                     lngColor(2) = m_NormalArrowDownColor
                     
                  Else
                     lngColor(0) = m_NormalButtonHoverColor
                     lngColor(2) = m_NormalArrowHoverColor
                  End If
               End If
            End If
            
            If .State Then
               If UseButtonPictures Then
                  objGroup.PaintPicture picButtonMasker, tsbVertical.Left - 45, 9 + intHeight, 19, 19, , , , , vbSrcAnd
                  objGroup.PaintPicture m_ButtonPicture(.State - 1 + (3 And (.MenuType = MENU_SPECIAL))), tsbVertical.Left - 45, 9 + intHeight, 19, 19, , , , , vbSrcPaint
                  
               Else
                  objGroup.FillColor = lngColor(1)
                  objGroup.Circle (tsbVertical.Left - 36, 18 + intHeight), 9, lngColor(0)
               End If
            End If
            
            objGroup.FillStyle = vbFSTransparent
            
            Call DrawArrow(Index, tsbVertical.Left - 39, 4 + intHeight, .WindowState, lngColor(2))
         End If
      End If
   End With
   
   Set objGroup = Nothing

End Sub

Private Sub DrawGradient(ByVal hDC As Long, ByRef picRect As Rect, ByVal GradientColor As Long, ByVal BaseColor As Long, Optional ByVal IsNormalHeader As Boolean)

Dim lngRGB         As Long
Dim rctGradient    As GradientRect
Dim tvxGradient(1) As TriVertex

   If m_GradientSwap Then
      GradientColor = GradientColor Xor BaseColor
      BaseColor = GradientColor Xor BaseColor
      GradientColor = GradientColor Xor BaseColor
   End If
   
   lngRGB = TranslateColor(GradientColor)
   
   With tvxGradient(0)
      If IsNormalHeader Then
         If m_GradientSwap Then
            .X = picRect.Left
            picRect.Right = picRect.Left + 160
            
         Else
            .X = picRect.Right - 160
         End If
         
      Else
         .X = picRect.Left
      End If
      
      .Y = picRect.Top
      .Red = GetColor(TranslateRGB(lngRGB, IsRed))
      .Green = GetColor(TranslateRGB(lngRGB, IsGreen))
      .Blue = GetColor(TranslateRGB(lngRGB, IsBlue))
   End With
   
   lngRGB = TranslateColor(BaseColor)
   
   With tvxGradient(1)
      .X = picRect.Right
      .Y = picRect.Bottom
      .Red = GetColor(TranslateRGB(lngRGB, IsRed))
      .Green = GetColor(TranslateRGB(lngRGB, IsGreen))
      .Blue = GetColor(TranslateRGB(lngRGB, IsBlue))
   End With
   
   rctGradient.UpperLeft = 1
   rctGradient.LowerRight = 0
   GradientFill hDC, tvxGradient(0), 2, rctGradient, 1, 0
   Erase tvxGradient

End Sub

Private Sub DrawGroupTitle(ByVal Index As Integer)

Dim rctText  As Rect
Dim strTitle As String

   With picGroup.Item(Index)
      If Groups(Index).State = STATE_NORMAL Then
         If Groups(Index).MenuType = MENU_SPECIAL Then
            .ForeColor = m_SpecialHeaderForeColor
            
         Else
            .ForeColor = m_NormalHeaderForeColor
         End If
         
      ElseIf Groups(Index).MenuType = MENU_SPECIAL Then
         .ForeColor = m_SpecialHeaderHoverColor
         
      Else
         .ForeColor = m_NormalHeaderHoverColor
      End If
      
      .FontBold = True
      .FontUnderline = False
      strTitle = Groups(Index).Title
      rctText.Top = 3 + (.ScaleHeight - TextHeight(strTitle)) \ 2
      rctText.Left = 40 - (30 And Groups(Index).BigIcon Is Nothing)
      rctText.Right = tsbVertical.Left - 32 - (18 And (Groups(Index).WindowState <> Fixed))
      rctText.Bottom = rctText.Top + TextHeight(strTitle)
      Groups(Index).FullTextShowed = True
      
      If Len(strTitle) And (.TextWidth(strTitle) > rctText.Right - rctText.Left) Then Groups(Index).FullTextShowed = False
      If Not Groups(Index).BigIcon Is Nothing Then .PaintPicture Groups(Index).BigIcon, 6, (6 And (m_HeaderHeight = High)), 32, 32
      
      DrawText .hDC, strTitle, -1, rctText, DT_LEFT Or DT_WORD_ELLIPSIS
   End With

End Sub

Private Sub DrawThemePart(ByVal hDC As Long, ByVal lPartID As Long, ByVal lState As Long, ByRef rRect As Rect)

Dim lngTheme As Long

   If Not m_UseWindowTheme Then GoTo ErrorTheme
   
   On Local Error GoTo ErrorTheme
   lngTheme = OpenThemeData(hWnd, StrPtr(THEME_NAME))
   
   If lngTheme Then
      HasTheme = Not DrawThemeBackground(lngTheme, hDC, lPartID, lState, rRect, rRect)
      CloseThemeData lngTheme
      
      GoTo ExitFunction
   End If
   
ErrorTheme:
   HasTheme = False
   
ExitFunction:
   On Local Error GoTo 0

End Sub

Private Sub DrawWindow(ByVal Index As Integer, ByVal IsBorderColor As Long, ByVal IsBackColor As Long, ByVal IsGradientColor As Long)

Dim rctFrame As Rect

   ReDim lngColor(1) As Long
   
   With rctFrame
      .Top = 1
      .Left = 1
      .Right = picItems.Item(Index).ScaleWidth - 2
      .Bottom = picItems.Item(Index).ScaleHeight - 2
      picItems.Item(Index).Line (0, 0)-(.Right + 1, .Bottom + 1), IsBorderColor, B
   End With
   
   If Groups(Index).MenuType = MENU_SPECIAL Then
      lngColor(1) = IsGradientColor
      lngColor(0) = IsBackColor
      
   Else
      lngColor(0) = IsGradientColor
      lngColor(1) = IsBackColor
   End If
   
   Call DrawGradient(picItems.Item(Index).hDC, rctFrame, lngColor(0), lngColor(1))
   
   Erase lngColor

End Sub

Private Sub MouseMoveGroup(ByVal Index As Integer, ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single, Optional ByVal MouseMove As Boolean)

Dim blnChanged  As Boolean
Dim blnNewMouse As Boolean

   With Groups(Index)
      If PtInRect(ObjectRect(picGroup(Index), 7 - (3 And (m_HeaderHeight = High))), CLng(X), CLng(Y)) = 0 Then
         picGroup.Item(Index).ToolTipText = ""
         .State = STATE_NORMAL
         blnChanged = True
         
      Else
         picGroup.Item(Index).ToolTipText = Replace(.ToolTipText, vbTab, " ")
         GroupIndex = Index
         
         If Button = vbDefault Then
            If .State <> STATE_HOT Then
               .State = STATE_HOT
               blnChanged = True
            End If
            
         ElseIf Button = vbLeftButton Then
            If .State <> STATE_PRESSED Then
               .State = STATE_PRESSED
               blnChanged = True
            End If
         End If
         
         blnNewMouse = True
      End If
      
      picGroup.Item(Index).MousePointer = (vbCustom And blnNewMouse)
      
      If blnChanged Then
         If MouseMove Then
            DrawGroup Index, picGroup.Item(Index).Top + 6
            
         Else
            Call Refresh
            
            If Groups(Index).WindowState = Collapsed Then
               RaiseEvent Collapse(Index)
               
            ElseIf Groups(Index).WindowState = Expanded Then
               RaiseEvent Expand(Index)
            End If
         End If
      End If
   End With

End Sub

Private Sub MouseMoveItem(ByVal Index As Integer, ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single, Optional ByVal MouseMove As Boolean)

Dim blnChanged  As Boolean
Dim blnNewMouse As Boolean
Dim intItem     As Integer

   GroupIndex = Index
   
   With Groups(Index)
      Call ResetGroup
      
      picItems.Item(Index).ToolTipText = ""
      
      If .MenuType <> MENU_DETAILS Then
         If .WindowState <> Collapsed Then
            For intItem = 0 To UBound(.Items)
               With .Items(intItem)
                  If .Hyperlink And PtInRect(.Rect, CLng(X), CLng(Y)) Then
                     If .State <> STATE_HOT Then
                        .State = STATE_HOT
                        blnChanged = True
                     End If
                     
                     If picItems.Item(Index).MousePointer <> vbCustom Then picItems.Item(Index).MousePointer = vbCustom
                     If Len(.ToolTipText) Then picItems.Item(Index).ToolTipText = Replace(.ToolTipText, vbTab, " ")
                     
                     blnNewMouse = True
                     
                  ElseIf PtInRect(.Rect, CLng(X), CLng(Y)) Then
                     If Len(.ToolTipText) Then picItems.Item(Index).ToolTipText = Replace(.ToolTipText, vbTab, " ")
                     
                  ElseIf .State <> STATE_NORMAL Then
                     .State = STATE_NORMAL
                     blnChanged = True
                  End If
               End With
            Next 'intItem
         End If
      End If
   End With
   
   picItems.Item(Index).MousePointer = (vbCustom And blnNewMouse)
   
   If blnChanged Then
      If MouseMove Then
         DrawItems Index, picGroup.Item(Index).Top + picGroup.Item(Index).Height + 6
         
      Else
         Call Refresh
      End If
   End If

End Sub

Private Sub MoveAll()

Dim intIndex As Integer
Dim lngY     As Long

   lngY = 9 - ((tsbVertical.Value * 10) And tsbVertical.Visible)
   
   For intIndex = 0 To UBound(Groups)
      With picGroup.Item(intIndex)
         .Top = lngY
         lngY = lngY + .Height
         
         If picItems.Item(intIndex).Visible Then
            picItems.Item(intIndex).Top = .Top + .Height
            lngY = lngY + picItems.Item(intIndex).Height
         End If
      End With
      
      lngY = lngY + 11 - (2 And HasTheme)
   Next 'intIndex

End Sub

Private Sub MoveWindow(ByVal Lines As Integer)

Dim intIndex As Integer
Dim lngTop   As Long

   With picItems.Item(AnimationIndex)
      AlphaPercent = AlphaPercent + BlendStep
      
      If AlphaPercent > 255 Then AlphaPercent = 255
      If AlphaPercent < 0 Then AlphaPercent = 0
      
      If Lines < 0 Then
         If m_UseAlphaBlend Then Call DoAlphaBlend
         
         BitBlt .hDC, 0, 0, .ScaleWidth, .ScaleHeight - Lines, .hDC, 0, -Lines, vbSrcCopy
      End If
      
      .Height = .Height + Lines
      lngTop = .Top + .Height
      
      If Lines > 0 Then
         BitBlt .hDC, 0, 0, .ScaleWidth, .ScaleHeight, picAnimation.hDC, 0, ItemHeight - .ScaleHeight, vbSrcCopy
         
         If m_UseAlphaBlend Then Call DoAlphaBlend
      End If
      
      .Refresh
   End With
   
   For intIndex = AnimationIndex + 1 To UBound(Groups)
      With picGroup.Item(intIndex)
         .Top = lngTop + 11 - (2 And HasTheme)
         picItems.Item(intIndex).Top = .Top + .Height
         
         With picItems.Item(intIndex)
            If .Visible Then
               lngTop = .Top + .Height
               
            Else
               lngTop = .Top
            End If
         End With
         
         DoEvents
      End With
   Next 'intIndex

End Sub

Private Sub PlaySound(ByVal Index As Integer)

Const SND_ASYNC     As Long = &H1
Const SND_MEMORY    As Long = &H4
Const SND_NODEFAULT As Long = &H2

Dim strSoundBuffer  As String

   On Local Error Resume Next
   strSoundBuffer = StrConv(LoadResData(Index, "Sounds"), vbUnicode)
   SoundPlay strSoundBuffer, SND_ASYNC Or SND_NODEFAULT Or SND_MEMORY
   On Local Error GoTo 0

End Sub

Private Sub ResetGroup()

   Groups(GroupIndex).State = STATE_NORMAL
   DrawGroup GroupIndex, picGroup.Item(GroupIndex).Top + 6

End Sub

Private Sub SetAlignment()

   If Extender.Align < vbAlignLeft Then
      DoAlignment = True
      Extender.Align = vbAlignLeft
      
      If Width > 3252 Then Width = 3252
   End If
   
   ' if alignment = vbAlignRight
   If DoAlignment And (Width > 3252) Then
      Width = 3252
      Extender.Align = vbAlignLeft
      DoAlignment = False
   End If

End Sub

Private Sub SetAnimation(ByVal Index As Integer)

   With picItems.Item(Index)
      If AnimationIndex = Index Then
         ItemHeight = .Height
         AlphaPercent = 255
         
         If m_Animation Then
            BlendStep = -255 \ (ItemHeight \ MoveLines)
            
            If m_Animation > Slow Then BlendStep = BlendStep / 1.5
         End If
         
         If Not .Visible Then
            .Height = 0
            .Visible = True
            
            Call ToggleAnimation(True)
         End If
         
      Else
         .Visible = True
      End If
   End With

End Sub

Private Sub SetButtonPicture(ByVal Index As Integer, ByRef NewPicture As IPictureDisp, ByVal PropertieSet As String, ByVal SpecialGroup As Boolean)

   Set m_ButtonPicture(Index) = NewPicture
   UseButtonPictures = CountButtonPictures
   PropertyChanged PropertieSet
   
   Call Refresh

End Sub

Private Sub SetCollapseAnimation(ByVal Index As Integer)

   DrawGroup Index, picGroup.Item(Index).Top + 6
   AlphaPercent = 0
   
   If m_Animation Then BlendStep = picGroup.Item(Index).ScaleHeight \ MoveLines
   
   If m_Animation = Slow Then
      BlendStep = BlendStep \ 4
      
   ElseIf m_Animation = Medium Then
      BlendStep = BlendStep * 1.2
      
   ElseIf m_Animation = Fast Then
      BlendStep = BlendStep * 3.5
   End If
   
   Call ToggleAnimation(True)

End Sub

Private Sub ToggleAnimation(ByVal State As Boolean)

   If m_Animation Then
      tmrAnimation.Enabled = State
      
   Else
      picItems.Item(GroupIndex).Visible = False
      State = False
   End If
   
   If Not State Then
      AnimationIndex = -1
      
      Call Refresh
   End If

End Sub

Private Sub TrackMouseLeave(ByVal lhWnd As Long)

Const TME_LEAVE   As Long = &H2&

Dim tmeMouseTrack As TrackMouseEventStruct

   With tmeMouseTrack
      .cbSize = Len(tmeMouseTrack)
      .dwFlags = TME_LEAVE
      .hwndTrack = lhWnd
   End With
   
   If TrackUser32 Then
      TrackMouseEvent tmeMouseTrack
      
   Else
      TrackMouseEventComCtl tmeMouseTrack
   End If

End Sub

Private Sub picGroup_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)

   RaiseEvent MouseDown(Button, Shift, X, Y)
   
   Call MouseMoveGroup(Index, Button, Shift, X, Y)

End Sub

Private Sub picGroup_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)

   picGroup.Item(Index).SetFocus
   RaiseEvent MouseMove(Button, Shift, X, Y)
   
   Call MouseMoveGroup(Index, Button, Shift, X, Y, True)

End Sub

Private Sub picGroup_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)

Const SOUND_GROUP_CLICKED As Integer = 1

   If (Groups(Index).ItemCount = 0) And (Groups(Index).MenuType <> MENU_DETAILS) Then Exit Sub
   
   If PtInRect(ObjectRect(picGroup(Index), 7 - (3 And (m_HeaderHeight = High))), CLng(X), CLng(Y)) Then
      RaiseEvent MouseUp(Button, Shift, X, Y)
      
   Else
      Groups(Index).State = STATE_NORMAL
      
      Call MouseMoveGroup(Index, 0, 0, X, Y)
      
      Exit Sub
   End If
   
   With Groups(Index)
      If Button = vbLeftButton Then
         If .WindowState <> Fixed Then
            AnimationIndex = Index
            
            If .WindowState = Expanded Then
               .State = STATE_HOT
               .WindowState = Collapsed
               
               Call SetCollapseAnimation(Index)
               
            Else
               .State = STATE_NORMAL
               .WindowState = Expanded
               
               Call CheckOpenGroups(Index)
            End If
         End If
         
         RaiseEvent GroupClick(Index, .WindowState)
         
         If m_SoundGroupClicked And (Groups(Index).WindowState <> Fixed) Then Call PlaySound(SOUND_GROUP_CLICKED)
         
         Call MouseMoveGroup(Index, 0, 0, X, Y)
      End If
   End With

End Sub

Private Sub picItems_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)

   RaiseEvent MouseDown(Button, Shift, X, Y)
   
   Call MouseMoveItem(Index, Button, Shift, X, Y)

End Sub

Private Sub picItems_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)

   picItems.Item(Index).SetFocus
   RaiseEvent MouseMove(Button, Shift, X, Y)
   
   Call MouseMoveItem(Index, Button, Shift, X, Y, True)

End Sub

Private Sub picItems_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)

Const SOUND_ITEM_CLICKED As Integer = 2

Dim intItem              As Integer

   RaiseEvent MouseUp(Button, Shift, X, Y)
   
   With Groups(Index)
      If .MenuType <> MENU_DETAILS Then
         If .WindowState <> Collapsed Then
            For intItem = 0 To UBound(.Items)
               With .Items(intItem)
                  If .Hyperlink And PtInRect(.Rect, CLng(X), CLng(Y)) Then
                     If Button = vbLeftButton Then
                        RaiseEvent ItemClick(Index, intItem)
                        
                        If m_SoundItemClicked Then Call PlaySound(SOUND_ITEM_CLICKED)
                        
                        Call MouseMoveItem(Index, 0, 0, X, Y)
                     End If
                     
                     Exit For
                  End If
               End With
            Next 'intItem
         End If
      End If
   End With

End Sub

Private Sub tmrAnimation_Timer()

   If AnimationIndex = -1 Then
      tmrAnimation.Enabled = False
      Exit Sub
   End If
   
   With picItems.Item(AnimationIndex)
      If .Height < MoveLines Then
         With picAnimation
            .Cls
            .Width = picItems.Item(AnimationIndex).Width
            .Height = ItemHeight
            BitBlt .hDC, 0, 0, .ScaleWidth, ItemHeight, picItems.Item(AnimationIndex).hDC, 0, 0, vbSrcCopy
         End With
      End If
      
      If Groups(AnimationIndex).WindowState = Collapsed Then
         If .Height - MoveLines > 0 Then
            Call MoveWindow(-MoveLines)
            
         Else
            .Visible = False
            
            Call ToggleAnimation(False)
         End If
         
      Else
         If .Height + MoveLines < ItemHeight Then
            Call MoveWindow(MoveLines)
            
         Else
            .Height = ItemHeight
            
            Call ToggleAnimation(False)
         End If
      End If
   End With

End Sub

Private Sub tsbVertical_Change()

   Call MoveAll

End Sub

Private Sub tsbVertical_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

   tsbVertical.SetFocus

End Sub

Private Sub tsbVertical_MouseWheel(ScrollLines As Integer)

   tsbVertical.Value = tsbVertical.Value + ScrollLines

End Sub

Private Sub tsbVertical_Scroll()

   Call MoveAll

End Sub

Private Sub UserControl_AmbientChanged(PropertyName As String)

   Call Refresh

End Sub

Private Sub UserControl_Initialize()

   ReDim Groups(0) As GroupType
   ReDim Groups(0).Items(0) As GroupItemType
   
   AnimationIndex = -1
   m_Animation = Medium
   m_BackColor = &HE6AA8C
   m_DetailsForeColor = &H0&
   m_GradientBackColor = &HCC6633
   m_GradientNormalHeaderBackColor = &HF0D2C5
   m_GradientNormalItemBackColor = &HF7DFD6
   m_GradientSpecialHeaderBackColor = &HB24801
   m_GradientSpecialItemBackColor = &HF7DFD6
   m_NormalArrowDownColor = &HC65D21
   m_NormalArrowHoverColor = &HC65D21
   m_NormalArrowUpColor = &HFF8E42
   m_NormalButtonBackColor = &HFFFFFF
   m_NormalButtonDownColor = &HF0CDC0
   m_NormalButtonHoverColor = &HF09D90
   m_NormalButtonUpColor = &HF0CDC0
   m_NormalHeaderBackColor = &HFFFFFF
   m_NormalHeaderForeColor = &HC65D21
   m_NormalHeaderHoverColor = &HFF8E42
   m_NormalItemBackColor = &HF7DFD6
   m_NormalItemBorderColor = &HFFFFFF
   m_NormalItemForeColor = &HC65D21
   m_NormalItemHoverColor = &HFF8E42
   m_SpecialArrowDownColor = &HFF8E42
   m_SpecialArrowHoverColor = &HFFFFFF
   m_SpecialArrowUpColor = &HFFFFFF
   m_SpecialButtonBackColor = &HBC5215
   m_SpecialButtonDownColor = &HB24801
   m_SpecialButtonHoverColor = &HC6AD71
   m_SpecialButtonUpColor = &HC67D41
   m_SpecialHeaderBackColor = &HBC5215
   m_SpecialHeaderForeColor = &HFFFFFF
   m_SpecialHeaderHoverColor = &HFF8E42
   m_SpecialItemBackColor = &HF7DFD6
   m_SpecialItemBorderColor = &HFFFFFF
   m_SpecialItemForeColor = &HC65D21
   m_SpecialItemHoverColor = &HFF8E42
   m_UseAlphaBlend = True
   m_UseWindowTheme = True

End Sub

Private Sub UserControl_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

   Call ResetGroup

End Sub

Private Sub UserControl_Paint()

   Call SetAlignment
   Call Refresh

End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)

Const SPI_GETWHEELSCROLLLINES As Long = 104

Dim blnTrackMouse             As Boolean

   With PropBag
      m_Animation = .ReadProperty("Animation", Medium)
      m_BackColor = .ReadProperty("BackColor", &HE6AA8C)
      shpBorder.BorderColor = .ReadProperty("BorderColor", &HFFFFFF)
      m_DetailsForeColor = .ReadProperty("DetailsForeColor", &H0&)
      Set Font = .ReadProperty("Font", UserControl.Font)
      m_GradientBackColor = .ReadProperty("GradientBackColor", &HCC6633)
      m_GradientNormalHeaderBackColor = .ReadProperty("GradientNormalHeaderBackColor", &HF0D2C5)
      m_GradientNormalItemBackColor = .ReadProperty("GradientNormalItemBackColor", &HF7DFD6)
      m_GradientSpecialHeaderBackColor = .ReadProperty("GradientSpecialHeaderBackColor", &HB24801)
      m_GradientSpecialItemBackColor = .ReadProperty("GradientSpecialItemBackColor", &HF7DFD6)
      m_GradientSwap = .ReadProperty("GradientSwap", False)
      m_HeaderHeight = .ReadProperty("HeaderHeight", Low)
      m_Locked = .ReadProperty("Locked", False)
      m_NormalArrowDownColor = .ReadProperty("NormalArrowDownColor", &HC65D21)
      m_NormalArrowHoverColor = .ReadProperty("NormalArrowHoverColor", &HC65D21)
      m_NormalArrowUpColor = .ReadProperty("NormalArrowUpColor", &HFF8E42)
      m_NormalButtonBackColor = .ReadProperty("NormalButtonBackColor", &HFFFFFF)
      m_NormalButtonDownColor = .ReadProperty("NormalButtonDownColor", &HF0CDC0)
      m_NormalButtonHoverColor = .ReadProperty("NormalButtonHoverColor", &HF09D90)
      Set m_ButtonPicture(2) = .ReadProperty("NormalButtonPictureDown", Nothing)
      Set m_ButtonPicture(1) = .ReadProperty("NormalButtonPictureHover", Nothing)
      Set m_ButtonPicture(0) = .ReadProperty("NormalButtonPictureUp", Nothing)
      m_NormalButtonUpColor = .ReadProperty("NormalButtonUpColor", &HF0CDC0)
      m_NormalHeaderBackColor = .ReadProperty("NormalHeaderBackColor", &HFFFFFF)
      m_NormalHeaderForeColor = .ReadProperty("NormalHeaderForeColor", &HC65D21)
      m_NormalHeaderHoverColor = .ReadProperty("NormalHeaderHoverColor ", &HFF8E42)
      m_NormalItemBackColor = .ReadProperty("NormalItemBackColor", &HF7DFD6)
      m_NormalItemBorderColor = .ReadProperty("NormalItemBorderColor", &HFFFFFF)
      m_NormalItemForeColor = .ReadProperty("NormalItemForeColor", &HC65D21)
      m_NormalItemHoverColor = .ReadProperty("NormalItemHoverColor", &HFF8E42)
      m_OpenOneGroupOnly = .ReadProperty("OpenOneGroupOnly", False)
      shpBorder.Visible = .ReadProperty("ShowBorder", False)
      m_SoundGroupClicked = .ReadProperty("SoundGroupClicked", False)
      m_SoundItemClicked = .ReadProperty("SoundItemClicked", False)
      m_SpecialArrowDownColor = .ReadProperty("SpecialArrowDownColor", &HFF8E42)
      m_SpecialArrowHoverColor = .ReadProperty("SpecialArrowHoverColor", &HFFFFFF)
      m_SpecialArrowUpColor = .ReadProperty("SpecialArrowUpColor", &HFFFFFF)
      m_SpecialButtonBackColor = .ReadProperty("SpecialButtonBackColor", &HBC5215)
      m_SpecialButtonDownColor = .ReadProperty("SpecialButtonDownColor", &HB24801)
      m_SpecialButtonHoverColor = .ReadProperty("SpecialButtonHoverColor", &HC6AD71)
      Set m_ButtonPicture(5) = .ReadProperty("SpecialButtonPictureDown", Nothing)
      Set m_ButtonPicture(4) = .ReadProperty("SpecialButtonPictureHover", Nothing)
      Set m_ButtonPicture(3) = .ReadProperty("SpecialButtonPictureUp", Nothing)
      m_SpecialButtonUpColor = .ReadProperty("SpecialButtonUpColor", &HC67D41)
      m_SpecialHeaderBackColor = .ReadProperty("SpecialHeaderBackColor", &HBC5215)
      m_SpecialHeaderForeColor = .ReadProperty("SpecialHeaderForeColor", &HFFFFFF)
      m_SpecialHeaderHoverColor = .ReadProperty("SpecialHeaderHoverColor", &HFF8E42)
      m_SpecialItemBackColor = .ReadProperty("SpecialItemBackColor", &HF7DFD6)
      m_SpecialItemBorderColor = .ReadProperty("SpecialItemBorderColor", &HFFFFFF)
      m_SpecialItemForeColor = .ReadProperty("SpecialItemForeColor", &HC65D21)
      m_SpecialItemHoverColor = .ReadProperty("SpecialItemHoverColor", &HFF8E42)
      m_UseAlphaBlend = .ReadProperty("UseAlphaBlend", True)
      m_UseWindowTheme = .ReadProperty("UseWindowTheme", True)
      UseButtonPictures = CountButtonPictures
      AnimationIndex = -1
      MoveLines = m_Animation * 3
   End With
   
   Call Refresh
   
   SystemParametersInfo SPI_GETWHEELSCROLLLINES, 0, ScrollLines, 0
   ScrollLines = ScrollLines + (1 And (ScrollLines = 0))
   
   If Ambient.UserMode Then
      Call SetAlignment
      
      TrackUser32 = IsFunctionSupported("TrackMouseEvent", "User32")
      
      If Not TrackUser32 Then blnTrackMouse = IsFunctionSupported("_TrackMouseEvent", "ComCtl32")
      
      With UserControl
         Call Subclass_Initialize(.hWnd)
         Call Subclass_AddMsg(.hWnd, WM_MOUSELEAVE)
         Call Subclass_AddMsg(.hWnd, WM_MOUSEMOVE)
         Call Subclass_AddMsg(.hWnd, WM_CTLCOLORSCROLLBAR)
         Call Subclass_AddMsg(.hWnd, WM_SYSCOLORCHANGE)
         Call Subclass_AddMsg(.hWnd, WM_THEMECHANGED)
      End With
      
      Call Subclass_Initialize(picGroup.Item(0).hWnd)
      Call Subclass_AddMsg(picGroup.Item(0).hWnd, WM_MOUSEWHEEL)
      Call Subclass_Initialize(picItems.Item(0).hWnd)
      Call Subclass_AddMsg(picItems.Item(0).hWnd, WM_MOUSEWHEEL)
   End If

End Sub

Private Sub UserControl_Resize()

   If Width < 201 * Screen.TwipsPerPixelX Then Width = 201 * Screen.TwipsPerPixelX
   
   With tsbVertical
      .Left = ScaleWidth - (.Width And .Visible)
   End With
   
   shpBorder.Move ScaleLeft, ScaleTop, ScaleWidth, ScaleHeight
   
   If Not DoAlignment Then
      Call SetAlignment
      Call Refresh
      
   ElseIf DoAlignment And (Width > 3252) Then
      Width = 3252
      Extender.Align = vbAlignLeft
      
   Else
      Call Refresh
   End If

End Sub

Private Sub UserControl_Show()

   Call Refresh

End Sub

Private Sub UserControl_Terminate()

Dim intCount As Integer

   On Local Error GoTo ExitSub
   
   Call Subclass_Terminate
   
   For intCount = 0 To 5
      Set m_ButtonPicture(intCount) = Nothing
   Next 'intCount
   
ExitSub:
   On Local Error GoTo 0

End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)

   With PropBag
      .WriteProperty "Animation", m_Animation, Medium
      .WriteProperty "BackColor", m_BackColor, &HE6AA8C
      .WriteProperty "BorderColor", shpBorder.BorderColor, &HFFFFFF
      .WriteProperty "DetailsForeColor", m_DetailsForeColor, &H0&
      .WriteProperty "Font", UserControl.Font
      .WriteProperty "GradientBackColor", m_GradientBackColor, &HCC6633
      .WriteProperty "GradientNormalHeaderBackColor", m_GradientNormalHeaderBackColor, &HF0D2C5
      .WriteProperty "GradientNormalItemBackColor", m_GradientNormalItemBackColor, &HF7DFD6
      .WriteProperty "GradientSpecialHeaderBackColor", m_GradientSpecialHeaderBackColor, &HB24801
      .WriteProperty "GradientSpecialItemBackColor", m_GradientSpecialItemBackColor, &HF7DFD6
      .WriteProperty "GradientSwap", m_GradientSwap, False
      .WriteProperty "HeaderHeight", m_HeaderHeight, Low
      .WriteProperty "Locked", m_Locked, False
      .WriteProperty "NormalArrowDownColor", m_NormalArrowDownColor, &HC65D21
      .WriteProperty "NormalArrowHoverColor", m_NormalArrowHoverColor, &HC65D21
      .WriteProperty "NormalArrowUpColor", m_NormalArrowUpColor, &HFF8E42
      .WriteProperty "NormalButtonBackColor", m_NormalButtonBackColor, &HFFFFFF
      .WriteProperty "NormalButtonDownColor", m_NormalButtonDownColor, &HF0CDC0
      .WriteProperty "NormalButtonHoverColor", m_NormalButtonHoverColor, &HF09D90
      .WriteProperty "NormalButtonPictureDown", m_ButtonPicture(2), Nothing
      .WriteProperty "NormalButtonPictureHover", m_ButtonPicture(1), Nothing
      .WriteProperty "NormalButtonPictureUp", m_ButtonPicture(0), Nothing
      .WriteProperty "NormalButtonUpColor", m_NormalButtonUpColor, &HF0CDC0
      .WriteProperty "NormalHeaderBackColor", m_NormalHeaderBackColor, &HFFFFFF
      .WriteProperty "NormalHeaderForeColor", m_NormalHeaderForeColor, &HC65D21
      .WriteProperty "NormalHeaderHoverColor", m_NormalHeaderHoverColor, &HFF8E42
      .WriteProperty "NormalItemBackColor", m_NormalItemBackColor, &HF7DFD6
      .WriteProperty "NormalItemBorderColor", m_NormalItemBorderColor, &HFFFFFF
      .WriteProperty "NormalItemForeColor", m_NormalItemForeColor, &HC65D21
      .WriteProperty "NormalItemHoverColor", m_NormalItemHoverColor, &HFF8E42
      .WriteProperty "OpenOneGroupOnly", m_OpenOneGroupOnly, False
      .WriteProperty "ShowBorder", shpBorder.Visible, False
      .WriteProperty "SoundGroupClicked", m_SoundGroupClicked, False
      .WriteProperty "SoundItemClicked", m_SoundItemClicked, False
      .WriteProperty "SpecialArrowDownColor", m_SpecialArrowDownColor, &HFF8E42
      .WriteProperty "SpecialArrowHoverColor", m_SpecialArrowHoverColor, &HFFFFFF
      .WriteProperty "SpecialArrowUpColor", m_SpecialArrowUpColor, &HFFFFFF
      .WriteProperty "SpecialButtonBackColor", m_SpecialButtonBackColor, &HBC5215
      .WriteProperty "SpecialButtonDownColor", m_SpecialButtonDownColor, &HB24801
      .WriteProperty "SpecialButtonHoverColor", m_SpecialButtonHoverColor, &HC6AD71
      .WriteProperty "SpecialButtonPictureDown", m_ButtonPicture(5), Nothing
      .WriteProperty "SpecialButtonPictureHover", m_ButtonPicture(4), Nothing
      .WriteProperty "SpecialButtonPictureUp", m_ButtonPicture(3), Nothing
      .WriteProperty "SpecialButtonUpColor", m_SpecialButtonUpColor, &HC67D41
      .WriteProperty "SpecialHeaderBackColor", m_SpecialHeaderBackColor, &HBC5215
      .WriteProperty "SpecialHeaderForeColor", m_SpecialHeaderForeColor, &HFFFFFF
      .WriteProperty "SpecialHeaderHoverColor", m_SpecialHeaderHoverColor, &HFF8E42
      .WriteProperty "SpecialItemBackColor", m_SpecialItemBackColor, &HF7DFD6
      .WriteProperty "SpecialItemBorderColor", m_SpecialItemBorderColor, &HFFFFFF
      .WriteProperty "SpecialItemForeColor", m_SpecialItemForeColor, &HC65D21
      .WriteProperty "SpecialItemHoverColor", m_SpecialItemHoverColor, &HFF8E42
      .WriteProperty "UseAlphaBlend", m_UseAlphaBlend, True
      .WriteProperty "UseWindowTheme", m_UseWindowTheme, True
   End With

End Sub

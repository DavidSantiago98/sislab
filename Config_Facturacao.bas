Attribute VB_Name = "Config_Facturacao"
Option Explicit

' Actualiza��o : 02/04/2002
' T�cnico Paulo Costa

Public Function Configura_Sistema_Faturacao() As Integer

    On Error GoTo ErrorHandler
    MDIFormInicio.IDM_MENU_FACTURACAO.Item(4).Visible = False
    MDIFormInicio.IDM_MENU_FACTURACAO.Item(4).Enabled = False
    If gTipoInstituicao = "HOSPITALAR" Then
        MDIFormInicio.IDM_MENU_FACTURACAO(2).Visible = False
        MDIFormInicio.IDM_MENU_FACTURACAO(7).Visible = False
        ' pferreira 2009.09.14
        MDIFormInicio.IDM_MENU_FACTURACAO(9).Visible = False
    End If
    
    If gLAB <> "HSJ" Then
        MDIFormInicio.IDM_MENU_FACTURACAO(8).Visible = False
    End If
    
    Select Case gSISTEMA_FACTURACAO
    
        Case cFACTURACAO_FACTUS, cFACTURACAO_GH
            
            ' Liga��o ao FACTUS ou a Gest�o Hospitalar.
            
            ' Tratamento de Devolvidos da factura��o
            'MDIFormInicio.IDM_DEVOLVIDOS_SONHO.Visible = False
            'MDIFormInicio.IDM_DEVOLVIDOS_SONHO.Enabled = False
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(4).Visible = True
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(4).Enabled = True
       
        Case cFACTURACAO_SONHO
        
            ' Liga��o ao SONHO
            
            ' MENU 04.10
            ' Estat�ticas da factura��o.
            'MDIFormInicio.IDM_ESTAT_FACTUS.Visible = False
            'MDIFormInicio.IDM_ESTAT_FACTUS.Enabled = False
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(3).Visible = False
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(3).Enabled = False
            
            ' Tratamento de Rejeitados do FACTUS
            'MDIFormInicio.IDM_TRATA_REJEIT_FACTUS.Visible = False
            'MDIFormInicio.IDM_TRATA_REJEIT_FACTUS.Enabled = False
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(2).Visible = False
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(2).Enabled = False
            
        Case "SISBIT"
        
            ' Liga��o ao SONHO
            
            ' MENU 04.10
            ' Estat�ticas da factura��o.
            'MDIFormInicio.IDM_ESTAT_FACTUS.Visible = False
            'MDIFormInicio.IDM_ESTAT_FACTUS.Enabled = False
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(3).Visible = False
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(3).Enabled = False
            
            ' Tratamento de Rejeitados do FACTUS
            'MDIFormInicio.IDM_TRATA_REJEIT_FACTUS.Visible = False
            'MDIFormInicio.IDM_TRATA_REJEIT_FACTUS.Enabled = False
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(2).Visible = True
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(2).Enabled = True
            

        Case cFACTURACAO_NOVAHIS
        
            ' Liga��o ao SONHO
            
            ' MENU 04.10
            ' Estat�ticas da factura��o.
            'MDIFormInicio.IDM_ESTAT_FACTUS.Visible = False
            'MDIFormInicio.IDM_ESTAT_FACTUS.Enabled = False
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(3).Visible = False
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(3).Enabled = False
            
            ' Tratamento de Rejeitados do FACTUS
            'MDIFormInicio.IDM_TRATA_REJEIT_FACTUS.Visible = False
            'MDIFormInicio.IDM_TRATA_REJEIT_FACTUS.Enabled = False
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(2).Visible = False
            MDIFormInicio.IDM_MENU_FACTURACAO.Item(2).Enabled = False
            
        Case Else
            
            ' Esconde Liga��o � Factura��o

            ' Menu de Factura��o
            MDIFormInicio.IDM_MENU_FACTURACAO_PR.Visible = False
            MDIFormInicio.IDM_MENU_FACTURACAO_PR.Enabled = False
            
'            ' MENU 04.08
'            MDIFormInicio.IDM_CRIAR_SEQ_FACTURACAO.Visible = False
'            MDIFormInicio.IDM_CRIAR_SEQ_FACTURACAO.Enabled = False
'
'            ' MENU 04.09
'            MDIFormInicio.IDM_ENV_FACT.Visible = False
'            MDIFormInicio.IDM_ENV_FACT.Enabled = False
'
'            ' MENU 04.10
'            MDIFormInicio.IDM_ESTAT_FACTUS.Visible = False
'            MDIFormInicio.IDM_ESTAT_FACTUS.Enabled = False
    
    End Select
    
    Configura_Sistema_Faturacao = 1
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Configura_Sistema_Faturacao (Config_Facturacao) -> " & Err.Description)
            Configura_Sistema_Faturacao = -1
            Exit Function
    End Select
End Function





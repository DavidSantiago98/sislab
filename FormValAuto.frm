VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form FormValAuto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormValAuto"
   ClientHeight    =   11610
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10020
   Icon            =   "FormValAuto.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   11610
   ScaleWidth      =   10020
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcObsFalse2 
      Height          =   285
      Left            =   8160
      TabIndex        =   90
      Top             =   11160
      Width           =   375
   End
   Begin VB.TextBox EcObsTrue2 
      Height          =   285
      Left            =   8160
      TabIndex        =   88
      Top             =   10800
      Width           =   375
   End
   Begin VB.Frame Frame6 
      Caption         =   "Se a f�rmula N�O se verificar"
      Height          =   1575
      Left            =   120
      TabIndex        =   79
      Top             =   6600
      Width           =   9735
      Begin VB.CheckBox CkObsFalse 
         Caption         =   "Observa��o"
         Height          =   195
         Left            =   5160
         TabIndex        =   83
         Top             =   480
         Width           =   1335
      End
      Begin VB.ComboBox CbFormulaFalse 
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   80
         Top             =   375
         Width           =   2775
      End
      Begin RichTextLib.RichTextBox EcObsFalse 
         Height          =   495
         Left            =   1200
         TabIndex        =   85
         Top             =   960
         Width           =   7455
         _ExtentX        =   13150
         _ExtentY        =   873
         _Version        =   393217
         TextRTF         =   $"FormValAuto.frx":000C
      End
      Begin VB.Label Label5 
         Caption         =   "Obsev."
         Height          =   255
         Index           =   2
         Left            =   480
         TabIndex        =   86
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label5 
         Caption         =   "Ac��o"
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   81
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.TextBox EcPesqRapInterf 
      Height          =   285
      Left            =   8160
      TabIndex        =   77
      Top             =   10440
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapidaAna 
      Height          =   285
      Left            =   1680
      TabIndex        =   73
      Top             =   11160
      Width           =   615
   End
   Begin VB.TextBox EcPesqRapSexo 
      Height          =   285
      Left            =   8160
      TabIndex        =   61
      Top             =   10080
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapGrupo 
      Height          =   285
      Left            =   8160
      TabIndex        =   58
      Top             =   9720
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapProv 
      Height          =   285
      Left            =   6240
      TabIndex        =   56
      Top             =   10800
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcPesqRapAux 
      Height          =   285
      Left            =   4080
      TabIndex        =   54
      Top             =   10800
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcOperValorVerif 
      Height          =   285
      Left            =   1680
      TabIndex        =   52
      Top             =   10800
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Frame Frame5 
      Height          =   1095
      Left            =   120
      TabIndex        =   48
      Top             =   720
      Width           =   9735
      Begin VB.TextBox EcCodAnaS 
         Height          =   285
         Left            =   2160
         TabIndex        =   3
         Top             =   270
         Width           =   975
      End
      Begin VB.TextBox EcDescrAnaS 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   285
         Left            =   3120
         TabIndex        =   4
         Top             =   270
         Width           =   4815
      End
      Begin VB.CommandButton EcPesqAnaS2 
         Height          =   375
         Left            =   8040
         Picture         =   "FormValAuto.frx":008E
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Pesquisa R�pida �s An�lises Simples"
         Top             =   240
         Width           =   375
      End
      Begin VB.ComboBox EcOperador 
         Height          =   315
         Left            =   2170
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   630
         Width           =   855
      End
      Begin VB.TextBox EcValorVerif 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "99,999"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   3360
         TabIndex        =   7
         Top             =   630
         Width           =   855
      End
      Begin VB.Label Label12 
         Caption         =   "Efectuar valida��o auto. �"
         Height          =   255
         Left            =   120
         TabIndex        =   51
         Top             =   270
         Width           =   1935
      End
      Begin VB.Label Label11 
         Caption         =   "cujo resultado tenha valores"
         Height          =   255
         Left            =   120
         TabIndex        =   50
         Top             =   630
         Width           =   2055
      End
      Begin VB.Label Label4 
         Caption         =   "a"
         Height          =   270
         Left            =   3165
         TabIndex        =   49
         Top             =   630
         Width           =   135
      End
   End
   Begin VB.TextBox EcPesqRapDC 
      Height          =   285
      Left            =   1680
      TabIndex        =   46
      Top             =   10440
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcPesqRapVariacoes 
      Height          =   285
      Left            =   4080
      TabIndex        =   44
      Top             =   10440
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapTeraMed 
      Height          =   285
      Left            =   6240
      TabIndex        =   42
      Top             =   10440
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapDiag 
      Height          =   285
      Left            =   6240
      TabIndex        =   40
      Top             =   10080
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Frame Frame3 
      Caption         =   " Se a f�rmula se verificar"
      Height          =   1335
      Left            =   120
      TabIndex        =   39
      Top             =   5160
      Width           =   9735
      Begin RichTextLib.RichTextBox EcObsTrue 
         Height          =   495
         Left            =   1200
         TabIndex        =   84
         Top             =   720
         Width           =   7455
         _ExtentX        =   13150
         _ExtentY        =   873
         _Version        =   393217
         TextRTF         =   $"FormValAuto.frx":0618
      End
      Begin VB.CheckBox CkObsTrue 
         Caption         =   "Observa��o"
         Height          =   195
         Left            =   5160
         TabIndex        =   82
         Top             =   360
         Width           =   1335
      End
      Begin VB.ComboBox CbFormulaTrue 
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   75
         Top             =   360
         Width           =   2775
      End
      Begin VB.Label Label5 
         Caption         =   "Obsev."
         Height          =   255
         Index           =   3
         Left            =   480
         TabIndex        =   87
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label5 
         Caption         =   "Ac��o"
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   62
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   " F�rmula da Valida��o Autom�tica"
      Height          =   3255
      Left            =   120
      TabIndex        =   38
      Top             =   1920
      Width           =   9735
      Begin VB.CommandButton BtInterferencias 
         Caption         =   "Interfer�ncias"
         Height          =   975
         Left            =   8400
         Picture         =   "FormValAuto.frx":069A
         Style           =   1  'Graphical
         TabIndex        =   76
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton BtAnalises 
         Caption         =   "An�lise Simples"
         Height          =   975
         Left            =   1920
         Picture         =   "FormValAuto.frx":2364
         Style           =   1  'Graphical
         TabIndex        =   72
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton BtAjuda 
         Caption         =   "&Ajuda"
         Height          =   975
         Left            =   8400
         Picture         =   "FormValAuto.frx":402E
         Style           =   1  'Graphical
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "Ajuda"
         Top             =   2040
         Width           =   1215
      End
      Begin VB.CommandButton BtSexo 
         Caption         =   "Sexo"
         Height          =   975
         Left            =   4560
         Picture         =   "FormValAuto.frx":5CF8
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   840
         Width           =   1095
      End
      Begin VB.CommandButton BtProven 
         Caption         =   "Proveni�ncia"
         Height          =   975
         Left            =   3240
         Picture         =   "FormValAuto.frx":79C2
         Style           =   1  'Graphical
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Proveni�ncias dos pedidos Analiticos"
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton EcIncompRes 
         Caption         =   "&Incompatibilidade de Resultados"
         Enabled         =   0   'False
         Height          =   975
         Left            =   4560
         Picture         =   "FormValAuto.frx":5024C
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   2040
         Width           =   1095
      End
      Begin VB.CommandButton BtValRef 
         Caption         =   "Valores Ref."
         Height          =   975
         Left            =   5760
         Picture         =   "FormValAuto.frx":50556
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton BtVariacoes 
         Caption         =   "Varia��es Res."
         Height          =   975
         Left            =   7080
         Picture         =   "FormValAuto.frx":52220
         Style           =   1  'Graphical
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida aos Delta Checks"
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton BtTeraMed 
         Caption         =   "&Terap�uticas e Medica��o"
         Enabled         =   0   'False
         Height          =   975
         Left            =   5760
         Picture         =   "FormValAuto.frx":53EEA
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida �s Terap�uticas e Medica��o"
         Top             =   2040
         Width           =   1215
      End
      Begin VB.CommandButton BtDiagnosticos 
         Caption         =   "&Diagn�sticos"
         Enabled         =   0   'False
         Height          =   975
         Left            =   3240
         Picture         =   "FormValAuto.frx":541F4
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida aos Diagn�sticos"
         Top             =   2040
         Width           =   1215
      End
      Begin VB.CommandButton BtGruposDiag 
         Caption         =   "&Grupos Diag."
         Enabled         =   0   'False
         Height          =   975
         Left            =   1920
         Picture         =   "FormValAuto.frx":544FE
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   2040
         Width           =   1215
      End
      Begin VB.TextBox EcFormula 
         Appearance      =   0  'Flat
         Height          =   495
         Left            =   240
         MultiLine       =   -1  'True
         TabIndex        =   8
         Top             =   240
         Width           =   9360
      End
      Begin VB.Frame Frame8 
         Caption         =   "Operadores"
         Height          =   2295
         Left            =   120
         TabIndex        =   63
         Top             =   720
         Width           =   1695
         Begin VB.CommandButton Bt_e 
            Caption         =   "&&"
            Height          =   375
            Left            =   120
            TabIndex        =   71
            TabStop         =   0   'False
            ToolTipText     =   "e"
            Top             =   360
            Width           =   615
         End
         Begin VB.CommandButton bt_ou 
            Caption         =   "|"
            Height          =   375
            Left            =   960
            TabIndex        =   70
            TabStop         =   0   'False
            ToolTipText     =   "ou"
            Top             =   360
            Width           =   615
         End
         Begin VB.CommandButton bt_nao 
            Caption         =   "!"
            Height          =   375
            Left            =   120
            TabIndex        =   69
            TabStop         =   0   'False
            ToolTipText     =   "Nega��o"
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton bt_abreParenteses 
            Caption         =   "("
            Height          =   375
            Left            =   960
            TabIndex        =   68
            TabStop         =   0   'False
            ToolTipText     =   "Parentesis Esquerdo"
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton bt_FechaParenteses 
            Caption         =   ")"
            Height          =   375
            Left            =   120
            TabIndex        =   67
            TabStop         =   0   'False
            ToolTipText     =   "Parentesis Direito"
            Top             =   1320
            Width           =   615
         End
         Begin VB.CommandButton Bt_menor 
            Caption         =   "<"
            Height          =   375
            Left            =   960
            TabIndex        =   66
            TabStop         =   0   'False
            ToolTipText     =   "Parentesis Direito"
            Top             =   1320
            Width           =   615
         End
         Begin VB.CommandButton Bt_Maior 
            Caption         =   ">"
            Height          =   375
            Left            =   120
            TabIndex        =   65
            TabStop         =   0   'False
            ToolTipText     =   "Parentesis Direito"
            Top             =   1800
            Width           =   615
         End
         Begin VB.CommandButton Bt_Igual 
            Caption         =   "="
            Height          =   375
            Left            =   960
            TabIndex        =   64
            TabStop         =   0   'False
            ToolTipText     =   "Parentesis Direito"
            Top             =   1800
            Width           =   615
         End
      End
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   120
      TabIndex        =   35
      Top             =   0
      Width           =   9735
      Begin VB.CommandButton BtPesqRapDC 
         Height          =   375
         Left            =   8040
         Picture         =   "FormValAuto.frx":54DC8
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox EcDescricao 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   3000
         TabIndex        =   1
         Top             =   240
         Width           =   4935
      End
      Begin VB.TextBox EcCodigo 
         Height          =   285
         Left            =   960
         TabIndex        =   0
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Descri��o"
         Height          =   255
         Left            =   2040
         TabIndex        =   37
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "C�digo"
         Height          =   255
         Left            =   240
         TabIndex        =   36
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4080
      TabIndex        =   30
      Top             =   9720
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   29
      Top             =   9720
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4080
      TabIndex        =   28
      Top             =   10080
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   27
      Top             =   10080
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Frame Frame4 
      Height          =   825
      Left            =   120
      TabIndex        =   20
      Top             =   8160
      Width           =   9735
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   26
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   25
         Top             =   195
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   24
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   23
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label13 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label14 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6240
      TabIndex        =   19
      Top             =   9720
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Label Label21 
      Caption         =   "EcObsFalse2"
      Height          =   255
      Index           =   3
      Left            =   6840
      TabIndex        =   91
      Top             =   11160
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label21 
      Caption         =   "EcObsTrue2"
      Height          =   255
      Index           =   2
      Left            =   6840
      TabIndex        =   89
      Top             =   10800
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label7 
      Caption         =   "EcPesqRapInterf"
      Height          =   255
      Left            =   6720
      TabIndex        =   78
      Top             =   10440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label21 
      Caption         =   "EcPesqRapidaAna"
      Height          =   255
      Index           =   1
      Left            =   0
      TabIndex        =   74
      Top             =   11160
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label23 
      Caption         =   "EcPesqRapSexo"
      Height          =   255
      Left            =   6720
      TabIndex        =   60
      Top             =   10080
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label20 
      Caption         =   "EcPesqRapGrupo"
      Height          =   255
      Left            =   6720
      TabIndex        =   59
      Top             =   9720
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label19 
      Caption         =   "EcPesqRapProv"
      Height          =   255
      Left            =   4560
      TabIndex        =   57
      Top             =   10800
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label lb 
      Caption         =   "EcPesqRapAux"
      Height          =   255
      Left            =   2400
      TabIndex        =   55
      Top             =   10800
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label21 
      Caption         =   "EcOperValorVerif"
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   53
      Top             =   10800
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label22 
      Caption         =   "EcPesqRapDC"
      Height          =   255
      Left            =   0
      TabIndex        =   47
      Top             =   10440
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label10 
      Caption         =   "EcPesqRapVariacoes"
      Height          =   255
      Left            =   2400
      TabIndex        =   45
      Top             =   10440
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label9 
      Caption         =   "EcPesqRapTeraMed"
      Height          =   255
      Left            =   4560
      TabIndex        =   43
      Top             =   10440
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label8 
      Caption         =   "EcPesqRapDiag"
      Height          =   255
      Left            =   4560
      TabIndex        =   41
      Top             =   10080
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label15 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2400
      TabIndex        =   34
      Top             =   10080
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label16 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2400
      TabIndex        =   33
      Top             =   9720
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label17 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   0
      TabIndex        =   32
      Top             =   10080
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label18 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   0
      TabIndex        =   31
      Top             =   9720
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcCodSequencial"
      Height          =   255
      Left            =   4560
      TabIndex        =   18
      Top             =   9720
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "FormValAuto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    BL_FimProcessamento Me
    
End Sub





Private Sub BtAnalises_Click()
    FormPesquisaRapida.InitPesquisaRapida "sl_ana_s", _
                        "descr_ana_s", "seq_ana_s", _
                       EcPesqRapidaAna

End Sub






Private Sub CbFormulaFalse_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbFormulaFalse.ListIndex = mediComboValorNull
    End If
End Sub

Private Sub CbFormulatrue_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbFormulaTrue.ListIndex = mediComboValorNull
    End If
End Sub

Private Sub CkObsTrue_Click()
    If CkObsTrue.value = vbChecked Then
        EcObsTrue.Enabled = True
    Else
        EcObsTrue.Enabled = False
        EcObsTrue.Text = ""
        EcObsTrue2.Text = ""
    End If
End Sub

Private Sub CkObsfalse_Click()
    If CkObsFalse.value = vbChecked Then
        EcObsFalse.Enabled = True
    Else
        EcObsFalse.Enabled = False
        EcObsFalse.Text = ""
        EcObsFalse2.Text = ""
    End If
End Sub

Private Sub EcObsFalse2_Change()
    EcObsFalse.Text = EcObsFalse2
End Sub
Private Sub EcObstrue2_Change()
    EcObsTrue.Text = EcObsTrue2
End Sub

Private Sub EcObstrue_LostFocus()
    EcObsTrue2 = EcObsTrue.Text
End Sub

Private Sub EcObsFalse_LostFocus()
    EcObsFalse2 = EcObsFalse.Text
End Sub

Private Sub EcPesqRapidaAna_Change()

    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapidaAna.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_ana_s FROM sl_ana_s WHERE seq_ana_s = " & EcPesqRapidaAna, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da An�lise!", vbExclamation, "Pesquisa r�pida"
        Else
            EcFormula.Text = EcFormula.Text & " $" & Trim(rsCodigo!cod_ana_s) & " "
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
        
        EcPesqRapidaAna.Text = ""
        EcFormula.SetFocus
        Sendkeys ("{END}")
    End If
End Sub



Private Sub BtProven_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_proven", _
                        "descr_proven", "seq_proven", _
                        EcPesqRapProv
End Sub

Private Sub Bt_menor_Click()
    EcFormula.Text = EcFormula.Text & "<"
    EcFormula.SetFocus
End Sub

Private Sub Bt_maior_Click()
    EcFormula.Text = EcFormula.Text & ">"
    EcFormula.SetFocus
End Sub


Private Sub Bt_Igual_Click()
    EcFormula.Text = EcFormula.Text & "="
    EcFormula.SetFocus
End Sub

Private Sub EcCodAnaS_LostFocus()
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodAnaS.Text = UCase(EcCodAnaS.Text)
    If EcCodAnaS.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s='" & Trim(EcCodAnaS.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbExclamation, ""
            EcCodAnaS.Text = ""
            EcDescrAnaS.Text = ""
            EcCodAnaS.SetFocus
        Else
            EcDescrAnaS.Text = Tabela!descr_ana_s
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If
End Sub



Private Sub EcCodigo_LostFocus()
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub


Private Sub BtGruposDiag_Click()
    
    
    FormPesquisaRapida.InitPesquisaRapida "sl_gr_diag", _
                        "descr_gr_diag", "seq_gr_diag", _
                        EcPesqRapGrupo
    
End Sub

Private Sub EcIncompRes_Click()
    
    'FormPesquisaRapida.Show
    'FormPesquisaRapida.InitPesquisaRapida "sl_ana_incomp","cod_ana_s1","seq_ana_incomp", EcPesqRapIncomp,
        
    'Campos do RecordSet
    Dim ChavesPesq(1) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 6) As String
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 6) As Long
    'Cabe�alhos
    Dim Headers(1 To 6) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    Dim resultados(1) As Variant
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    'Query
    ClausulaFrom = "sl_ana_incomp"
    'Crit�rio
    CampoPesquisa1 = "cod_ana_s1"
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "seq_ana_incomp"
    
    Headers(1) = "An�lise"
    CamposEcran(1) = "cod_ana_s1"
    Tamanhos(1) = 1000
    
    Headers(2) = " "
    CamposEcran(2) = "oper_verif1"
    Tamanhos(2) = 500
    
    Headers(3) = " "
    CamposEcran(3) = "res1"
    Tamanhos(3) = 1000
    
    Headers(4) = " "
    CamposEcran(4) = "cod_ana_s2"
    Tamanhos(4) = 1000
    
    Headers(5) = " "
    CamposEcran(5) = "oper_verif2"
    Tamanhos(5) = 500
    
    Headers(6) = " "
    CamposEcran(6) = "res2"
    Tamanhos(6) = 1000
    
    
    'N� de Campos a pesquisar na Classe=>Resultados
    CamposRetorno.InicializaResultados 1

        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Incompatibilidade de Resultados")
    
    mensagem = "N�o foi encontrada nenhuma incompatibilidade de resultados."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcFormula.Text = EcFormula.Text & "N" & resultados(1) & " "
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
        
        
End Sub

Private Sub EcOperador_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcOperador.ListIndex = -1
End Sub


Private Sub EcPesqAnaS2_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_ana_s", _
                        "descr_ana_s", "seq_ana_s", _
                        EcPesqRapAux
End Sub


Private Sub EcPesqRapAux_Change()
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapAux.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_ana_s,descr_ana_s FROM sl_ana_s WHERE seq_ana_s = " & EcPesqRapAux, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da an�lise!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodAnaS.Text = rsCodigo!cod_ana_s
            EcDescrAnaS.Text = rsCodigo!descr_ana_s
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
        EcPesqRapAux.Text = ""
    End If
End Sub

Private Sub EcPesqRapGrupo_Change()
    
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapGrupo.Text <> "" Then
            Set rsCodigo = New ADODB.recordset
            rsCodigo.CursorLocation = adUseServer
            rsCodigo.CursorType = adOpenStatic
            
            rsCodigo.Open "SELECT cod_gr_diag FROM sl_gr_diag WHERE seq_gr_diag = " & EcPesqRapGrupo, gConexao
            If rsCodigo.RecordCount <= 0 Then
                BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do diagn�stico!", vbExclamation, "Pesquisa r�pida"
            Else
                EcFormula.Text = EcFormula.Text & " G" & Trim(rsCodigo!cod_gr_diag) & " "
            End If
            
            rsCodigo.Close
            Set rsCodigo = Nothing
            
            EcPesqRapGrupo.Text = ""
            
    End If
    
End Sub

Private Sub EcPesqRapProv_Change()
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapProv.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_proven FROM sl_proven WHERE seq_proven = " & EcPesqRapProv, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da Proveni�ncia!", vbExclamation, "Pesquisa r�pida"
        Else
            EcFormula.Text = EcFormula.Text & " T=" & Trim(rsCodigo!cod_proven) & " "
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
        
        EcPesqRapProv.Text = ""
    End If
End Sub


Private Sub EcPesqRapSexo_Change()
    
    If EcPesqRapSexo.Text <> "" Then
            EcFormula.Text = EcFormula.Text & " X=" & Trim(EcPesqRapSexo.Text) & " "
            EcPesqRapSexo.Text = ""
    End If
    
End Sub
Private Sub EcPesqRapInterf_change()
    If EcPesqRapInterf.Text <> "" Then
            EcFormula.Text = EcFormula.Text & " F" & Trim(EcPesqRapInterf.Text) & " "
            EcPesqRapInterf.Text = ""
    End If
End Sub

Private Sub BtSexo_Click()
    
    
    FormPesquisaRapida.InitPesquisaRapida "sl_tbf_sexo", _
                        "descr_sexo", "cod_sexo", _
                        EcPesqRapSexo
    
End Sub

Private Sub BtInterferencias_click()
    FormPesquisaRapida.InitPesquisaRapida "sl_interferencias", _
                        "descr_interferencia", "cod_interferencia", _
                        EcPesqRapInterf

End Sub
Private Sub EcValorVerif_LostFocus()
    BG_ValidaTipoCampo_ADO Me, EcValorVerif
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "SL_TBF_T_VAL_AUTO", "cod_t_VAL_AUTO", "descr_t_VAL_AUTO", CbFormulaFalse
    BG_PreencheComboBD_ADO "SL_TBF_T_VAL_AUTO", "cod_t_VAL_AUTO", "descr_t_VAL_AUTO", CbFormulaTrue

    EcOperador.AddItem ">"
    EcOperador.AddItem ">="
    EcOperador.AddItem "<"
    EcOperador.AddItem "<="
    EcOperador.AddItem "="
    
    
    
End Sub


Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            EcOperValorVerif.Text = ""
            EcOperValorVerif.Text = EcOperador.Text
           BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    LimpaCampos
    PreencheCampos

End Sub
Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        EcOperValorVerif.Text = EcOperador.Text
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub BtPesqRapDC_Click()
    
    FormPesquisaRapida.InitPesquisaRapida NomeTabela, _
                        "descr_val_auto", "seq_val_auto", _
                        EcPesqRapDC
End Sub


Private Sub bt_e_Click()
    EcFormula.Text = EcFormula.Text & "& "
    EcFormula.SetFocus
End Sub

Private Sub bt_ou_Click()
    EcFormula.Text = EcFormula.Text & "| "
    EcFormula.SetFocus
End Sub


Private Sub bt_nao_Click()
    EcFormula.Text = EcFormula.Text & "! "
    EcFormula.SetFocus
End Sub


Private Sub bt_abreParenteses_Click()
    EcFormula.Text = EcFormula.Text & "( "
    EcFormula.SetFocus
End Sub


Private Sub bt_fechaParenteses_Click()
    EcFormula.Text = EcFormula.Text & ") "
    EcFormula.SetFocus
End Sub


Private Sub BtAjuda_Click()
    FormAjudaValAuto.Show
End Sub



Private Sub BtDiagnosticos_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_diag", _
                        "descr_diag", "seq_diag", _
                        EcPesqRapDiag
End Sub

Private Sub EcPesqRapDC_Change()
    Dim rsCodigo As ADODB.recordset
    
 If EcPesqRapDC.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_val_auto,descr_val_auto FROM sl_val_auto WHERE seq_val_auto = " & EcPesqRapDC, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da Valida��o Autom�tica!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodigo.Text = rsCodigo!cod_val_auto
            EcDescricao.Text = rsCodigo!descr_val_auto
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
        EcPesqRapDC.Text = ""
    End If
End Sub

Private Sub EcPesqRapDiag_Change()
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapDiag.Text <> "" Then
            Set rsCodigo = New ADODB.recordset
            rsCodigo.CursorLocation = adUseServer
            rsCodigo.CursorType = adOpenStatic
            
            rsCodigo.Open "SELECT cod_diag FROM sl_diag WHERE seq_diag = " & EcPesqRapDiag, gConexao
            If rsCodigo.RecordCount <= 0 Then
                BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do diagn�stico!", vbExclamation, "Pesquisa r�pida"
            Else
                EcFormula.Text = EcFormula.Text & " D" & Trim(rsCodigo!cod_diag) & " "
            End If
            
            rsCodigo.Close
            Set rsCodigo = Nothing
            
            EcPesqRapDiag.Text = ""
    End If
End Sub


Private Sub EcPesqRapTeraMed_Change()
    Dim rsCodigo As ADODB.recordset
    
If EcPesqRapTeraMed.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_tera_med FROM sl_tera_med WHERE seq_tera_med = " & EcPesqRapTeraMed, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da terapeutica\medica��o!", vbExclamation, "Pesquisa r�pida"
        Else
            EcFormula.Text = EcFormula.Text & " M" & Trim(rsCodigo!cod_tera_med) & " "
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
        
        EcPesqRapTeraMed.Text = ""
End If
End Sub


Private Sub EcPesqRapVariacoes_Change()
    Dim rsCodigo As ADODB.recordset

If EcPesqRapVariacoes.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_varia_dc FROM sl_varia_dc WHERE seq_varia_dc = " & EcPesqRapVariacoes, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do Delta Check!", vbExclamation, "Pesquisa r�pida"
        Else
            EcFormula.Text = EcFormula.Text & " Z" & Trim(rsCodigo!cod_varia_dc) & " "
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
        
        EcPesqRapVariacoes.Text = ""
End If
End Sub


Sub FuncaoInserir()

    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            EcOperValorVerif.Text = ""
            EcOperValorVerif.Text = EcOperador.Text
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If
End Sub
Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer
    
    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub BD_Insert()
    
    Dim SQLQuery As String

    gSQLError = 0
    gSQLISAM = 0
    
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_val_auto") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    
End Sub

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY " & ChaveBD & " ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Private Sub BtTeraMed_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_tera_med", _
                        "descr_tera_med", "seq_tera_med", _
                        EcPesqRapTeraMed
End Sub


Private Sub BtValRef_Click()
        
   EcFormula.Text = EcFormula.Text & "R" & " "
    
End Sub

Private Sub BtVariacoes_Click()

    FormPesquisaRapida.InitPesquisaRapida "sl_varia_dc", _
                        "descr_varia_dc", "seq_varia_dc", _
                        EcPesqRapVariacoes
End Sub


Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub

Sub Inicializacoes()

    Me.caption = " Auxiliar de Valida��o"
    Me.left = 540
    Me.top = 450
    Me.Width = 10110
    Me.Height = 8745 ' Normal
    'Me.Height = 8225 ' Campos Extras
    
    NomeTabela = "sl_val_auto"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 17
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    
    ' Campos da Base de Dados
    CamposBD(0) = "seq_val_auto"
    CamposBD(1) = "cod_val_auto"
    CamposBD(2) = "descr_val_auto"
    CamposBD(3) = "formula"
    CamposBD(4) = "user_cri"
    CamposBD(5) = "dt_cri"
    CamposBD(6) = "user_act"
    CamposBD(7) = "dt_act"
    CamposBD(8) = "valor_verif"
    CamposBD(9) = "oper_valor_verif"
    CamposBD(10) = "cod_ana_s"
    CamposBD(11) = "flg_valida_true"
    CamposBD(12) = "flg_valida_false"
    CamposBD(13) = "flg_obs_true"
    CamposBD(14) = "flg_obs_FALSE"
    CamposBD(15) = "obs_true"
    CamposBD(16) = "obs_FALSE"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcFormula
    Set CamposEc(4) = EcUtilizadorCriacao
    Set CamposEc(5) = EcDataCriacao
    Set CamposEc(6) = EcUtilizadorAlteracao
    Set CamposEc(7) = EcDataAlteracao
    Set CamposEc(8) = EcValorVerif
    Set CamposEc(9) = EcOperValorVerif
    Set CamposEc(10) = EcCodAnaS
    Set CamposEc(11) = CbFormulaTrue
    Set CamposEc(12) = CbFormulaFalse
    Set CamposEc(13) = CkObsTrue
    Set CamposEc(14) = CkObsFalse
    Set CamposEc(15) = EcObsTrue2
    Set CamposEc(16) = EcObsFalse2
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo da Valida��o Autom�tica"
    TextoCamposObrigatorios(2) = "Descri��o da Valida��o Autom�tica"
    TextoCamposObrigatorios(3) = "F�rmula"
    TextoCamposObrigatorios(10) = "C�digo da An�lise"
        
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_val_auto"
    Set ChaveEc = EcCodSequencial
    
    
End Sub


Sub LimpaCampos()
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    EcOperValorVerif = ""
    EcOperador.ListIndex = -1
    EcDescrAnaS.Text = ""
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    PreencheValoresDefeito
    EcPesqRapProv.Text = ""
    EcPesqRapTeraMed.Text = ""
    EcPesqRapDiag.Text = ""
    EcPesqRapDC.Text = ""
    EcPesqRapVariacoes.Text = ""
    EcPesqRapAux.Text = ""
    EcPesqRapGrupo.Text = ""
    EcPesqRapSexo.Text = ""
    EcObsTrue.Text = ""
    EcObsFalse.Text = ""
    
    
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        If rs!oper_Valor_verif <> "" Then
            EcOperador.Text = Trim(EcOperValorVerif.Text)
        End If
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
               
            
    End If
    
    
    
End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub DefTipoCampos()
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    CkObsFalse.value = vbGrayed
    CkObsTrue.value = vbGrayed
    EcObsFalse.Enabled = False
    EcObsTrue.Enabled = True
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub
Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormValAuto = Nothing
End Sub


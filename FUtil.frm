VERSION 5.00
Begin VB.Form FormUtilizadores 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionIdUtilizadores"
   ClientHeight    =   5145
   ClientLeft      =   1755
   ClientTop       =   1815
   ClientWidth     =   12195
   Icon            =   "FUtil.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5145
   ScaleWidth      =   12195
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcLogin 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   3960
      TabIndex        =   4
      Top             =   360
      Width           =   3135
   End
   Begin VB.ListBox EcLocais 
      Height          =   960
      Left            =   7320
      Style           =   1  'Checkbox
      TabIndex        =   40
      Top             =   960
      Width           =   4695
   End
   Begin VB.TextBox EcDataRemocao 
      Height          =   285
      Left            =   3360
      TabIndex        =   37
      Top             =   7470
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox EcUtilizadorRemocao 
      Height          =   285
      Left            =   360
      TabIndex        =   36
      Top             =   7470
      Visible         =   0   'False
      Width           =   2775
   End
   Begin VB.TextBox EcUtilizadorOld 
      Enabled         =   0   'False
      Height          =   285
      Left            =   480
      TabIndex        =   2
      Top             =   480
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CheckBox ChRemovidos 
      Caption         =   "Incluir utilizadores desactivados "
      Height          =   195
      Left            =   240
      TabIndex        =   9
      Top             =   2160
      Width           =   2535
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Height          =   285
      Left            =   360
      TabIndex        =   30
      Top             =   6720
      Visible         =   0   'False
      Width           =   2775
   End
   Begin VB.TextBox EcSenha 
      Height          =   285
      Left            =   2040
      TabIndex        =   29
      Top             =   5520
      Width           =   1815
   End
   Begin VB.TextBox EcDataAlteracao 
      Height          =   285
      Left            =   3360
      TabIndex        =   28
      Top             =   6720
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Height          =   285
      Left            =   360
      TabIndex        =   26
      Top             =   6120
      Visible         =   0   'False
      Width           =   2775
   End
   Begin VB.TextBox EcRemovido 
      Height          =   285
      Left            =   1320
      TabIndex        =   24
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcDataCriacao 
      Height          =   285
      Left            =   3360
      TabIndex        =   22
      Top             =   6120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox EcCodUtilizador 
      Height          =   285
      Left            =   360
      TabIndex        =   20
      Top             =   5520
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcDepartamento 
      Height          =   285
      Left            =   1320
      TabIndex        =   8
      Top             =   1680
      Width           =   5775
   End
   Begin VB.TextBox EcFuncao 
      Height          =   285
      Left            =   1320
      TabIndex        =   7
      Top             =   1320
      Width           =   5775
   End
   Begin VB.TextBox EcNome 
      Height          =   285
      Left            =   1320
      TabIndex        =   6
      Top             =   960
      Width           =   5775
   End
   Begin VB.TextBox EcUtilizador 
      Height          =   285
      Left            =   240
      TabIndex        =   1
      Top             =   360
      Width           =   1695
   End
   Begin VB.ComboBox EcGrupo 
      Height          =   315
      Left            =   7320
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   360
      Width           =   3735
   End
   Begin VB.TextBox EcSenhaAux 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   2040
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   360
      Width           =   1815
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Left            =   240
      TabIndex        =   10
      Top             =   2760
      Width           =   11775
   End
   Begin VB.PictureBox PcExistente 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   11400
      Picture         =   "FUtil.frx":000C
      ScaleHeight     =   495
      ScaleWidth      =   585
      TabIndex        =   34
      Top             =   240
      Width           =   585
   End
   Begin VB.PictureBox PcRemovido 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   11400
      Picture         =   "FUtil.frx":0316
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   33
      Top             =   240
      Width           =   495
   End
   Begin VB.Label Label20 
      Caption         =   "Login"
      Height          =   255
      Left            =   3960
      TabIndex        =   44
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label19 
      Caption         =   "Grupo"
      Height          =   255
      Left            =   9720
      TabIndex        =   43
      Top             =   2520
      Width           =   1095
   End
   Begin VB.Label Label18 
      Caption         =   "Departamento"
      Height          =   255
      Left            =   7320
      TabIndex        =   42
      Top             =   2520
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Locais"
      Height          =   255
      Left            =   7320
      TabIndex        =   41
      Top             =   720
      Width           =   855
   End
   Begin VB.Label Label17 
      Caption         =   "data_remocao"
      Height          =   255
      Left            =   3360
      TabIndex        =   39
      Top             =   7230
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label16 
      Caption         =   "utilizador_remocao"
      Height          =   255
      Left            =   360
      TabIndex        =   38
      Top             =   7200
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.Label Senha_Encr 
      Caption         =   "Senha_Encrip"
      Height          =   255
      Left            =   2040
      TabIndex        =   35
      Top             =   5280
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label LaAvisoRemovido 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "desactivado"
      Height          =   195
      Left            =   960
      TabIndex        =   32
      Top             =   0
      Visible         =   0   'False
      Width           =   870
   End
   Begin VB.Label Label15 
      Caption         =   "utilizador_alteracao"
      Height          =   255
      Left            =   360
      TabIndex        =   31
      Top             =   6450
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.Label Label14 
      Caption         =   "Cod. Utilizador"
      Height          =   255
      Left            =   6000
      TabIndex        =   18
      Top             =   2520
      Width           =   1095
   End
   Begin VB.Label Label13 
      Caption         =   "data_ateracao"
      Height          =   255
      Left            =   3360
      TabIndex        =   27
      Top             =   6480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label12 
      Caption         =   "utilizador_criacao"
      Height          =   255
      Left            =   360
      TabIndex        =   25
      Top             =   5880
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label11 
      Caption         =   "removido"
      Height          =   255
      Left            =   1320
      TabIndex        =   23
      Top             =   5280
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label10 
      Caption         =   "data_criacao"
      Height          =   255
      Left            =   3360
      TabIndex        =   21
      Top             =   5880
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "cod_utilizador"
      Height          =   255
      Left            =   240
      TabIndex        =   19
      Top             =   5280
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label9 
      Caption         =   "Grupo"
      Height          =   255
      Left            =   3720
      TabIndex        =   17
      Top             =   2520
      Width           =   855
   End
   Begin VB.Label Label8 
      Caption         =   "Grupo"
      Height          =   255
      Left            =   7320
      TabIndex        =   12
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label7 
      Caption         =   "Departamento"
      Height          =   255
      Left            =   240
      TabIndex        =   15
      Top             =   1680
      Width           =   1095
   End
   Begin VB.Label Label6 
      Caption         =   "Fun��o"
      Height          =   255
      Left            =   240
      TabIndex        =   14
      Top             =   1320
      Width           =   615
   End
   Begin VB.Label Label5 
      Caption         =   "Utilizador"
      Height          =   255
      Left            =   240
      TabIndex        =   16
      Top             =   2520
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Nome"
      Height          =   255
      Left            =   240
      TabIndex        =   13
      Top             =   960
      Width           =   615
   End
   Begin VB.Label LaUtilizador 
      Caption         =   "Utilizador"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "Senha"
      Height          =   255
      Left            =   2040
      TabIndex        =   11
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "FormUtilizadores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim rsTabela As ADODB.recordset


Private Sub AssociaUtilLocais(iCodutilizador As Integer)
    Dim sSQL As String
    Dim i As Integer
    
    sSQL = "DELETE FROM " & "sl_ass_util_locais WHERE cod_util=" & iCodutilizador
    BG_ExecutaQuery_ADO sSQL
    
    If EcLocais.SelCount > 0 Then
        For i = 0 To EcLocais.ListCount - 1
            If EcLocais.Selected(i) = True Then
                sSQL = "INSERT INTO " & " sl_ass_util_locais(cod_util,cod_local,user_cri,dt_cri) VALUES("
                sSQL = sSQL & iCodutilizador & "," & EcLocais.ItemData(i) & ","
                sSQL = sSQL & gCodUtilizador & "," & BL_TrataDataParaBD(Bg_DaData_ADO) & ")"
                BG_ExecutaQuery_ADO sSQL
            End If
        Next i
    End If
End Sub

Private Function DevolveLocaisPesquisa() As String
    Dim sRetorno As String
    Dim i As Integer
    
    For i = 0 To EcLocais.ListCount - 1
        If EcLocais.Selected(i) = True Then
            If sRetorno = "" Then
                sRetorno = "cod_util IN(SELECT DISTINCT(cod_util) FROM " & "sl_ass_util_locais "
                sRetorno = sRetorno & " WHERE cod_local IN(" & EcLocais.ItemData(i) & ","
            Else
                sRetorno = sRetorno & EcLocais.ItemData(i) & ","
            End If
        End If
    Next i
    
    If sRetorno <> "" Then
        sRetorno = Mid(sRetorno, 1, Len(sRetorno) - 1) & "))"
    End If
    
    
    DevolveLocaisPesquisa = sRetorno
End Function

Sub FuncaoActivarRegisto()
    Dim sCondicao As String
    Dim sSQL As String
    Dim MarcaLocal As Variant
       
    gMsgTitulo = "Activar Registo"
    gMsgMsg = "Tem a certeza que quer activar o utilizador?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbNo Then
        Exit Sub
    End If
       
    EcRemovido = gT_Nao
    EcUtilizador = EcUtilizadorOld
    EcUtilizadorOld = ""
    EcDataAlteracao = Bg_DaData_ADO
    EcUtilizadorAlteracao = gCodUtilizador

    sCondicao = ChaveBD & " = " & BG_CvPlica(rsTabela(ChaveBD))
    sSQL = BG_ConstroiCriterio_UPDATE(NomeTabela, CamposBD, CamposEc, sCondicao)
    BG_ExecutaQuery_ADO sSQL
    BG_Mensagem mediMsgBox, "Opera��o efectuada com sucesso.", vbInformation, gMsgTitulo
    
    MarcaLocal = rsTabela.Bookmark
    rsTabela.Requery
        
    If rsTabela.BOF And rsTabela.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, gMsgTitulo
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla EcLista, rsTabela, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rsTabela.RecordCount Then rsTabela.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rsTabela.RecordCount Then rsTabela.Bookmark = MarcaLocal
    Else
        rsTabela.MoveLast
    End If
    
    If rsTabela.EOF Then rsTabela.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub

Private Sub LimpaSeleccionados()
    Dim i As Integer

    For i = 0 To EcLocais.ListCount - 1
        EcLocais.Selected(i) = False
    Next i
End Sub

Private Sub SeleccionaLocaisUtil(iCodutilizador As Integer)
    Dim sSQL As String
    Dim rs As ADODB.recordset
    Dim i As Integer
    
    sSQL = "SELECT * FROM " & "sl_ass_util_locais WHERE cod_util=" & iCodutilizador
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseClient
    rs.Open sSQL, gConexao, adOpenStatic
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            For i = 0 To EcLocais.ListCount - 1
                If EcLocais.ItemData(i) = rs!cod_local Then
                    EcLocais.Selected(i) = True
                    Exit For
                End If
            Next i
            rs.MoveNext
        Loop
    Else
        LimpaSeleccionados
    End If
    rs.Close
    Set rs = Nothing

End Sub

Private Sub ChRemovidos_Click()
    If ListarRemovidos = False Then
        ListarRemovidos = True
        EcUtilizador.Visible = False
        EcUtilizador.TabStop = False
        EcUtilizador = ""
        EcUtilizadorOld.Visible = True
        EcUtilizadorOld.TabStop = True
        EcUtilizadorOld.Enabled = True
    Else
        ListarRemovidos = False
        EcUtilizador.Visible = True
        EcUtilizador.TabStop = True
        EcUtilizadorOld = ""
        EcUtilizadorOld.Visible = False
        EcUtilizadorOld.TabStop = False
        EcUtilizadorOld.Enabled = False
    End If
End Sub

Private Sub EcDepartamento_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDepartamento_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Private Sub EcFuncao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcFuncao_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Private Sub EcGrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    BG_LimpaOpcao EcGrupo, KeyCode
End Sub

Private Sub EcLista_Click()
    If EcLista.ListCount > 0 Then
        rsTabela.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
        
        If EcRemovido.Text = "1" Then
            EcCodUtilizador.Enabled = False
            EcUtilizador.Enabled = False
            EcSenhaAux.Enabled = False
            EcGrupo.Enabled = False
            EcNome.Enabled = False
            EcFuncao.Enabled = False
            EcDepartamento.Enabled = False
            LaAvisoRemovido.Visible = True
            PcRemovido.Visible = True
            PcExistente.Visible = False
            EcUtilizador.Visible = False
            EcUtilizadorOld.Visible = True
            EcUtilizadorOld.Enabled = False
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "ActivarRegisto", "Activo"
        Else
            EcCodUtilizador.Enabled = True
            EcUtilizador.Enabled = True
            EcSenhaAux.Enabled = True
            EcGrupo.Enabled = True
            EcNome.Enabled = True
            EcFuncao.Enabled = True
            EcDepartamento.Enabled = True
            LaAvisoRemovido.Visible = False
            PcRemovido.Visible = False
            PcExistente.Visible = True
            EcUtilizadorOld.Visible = False
            EcUtilizador.Visible = True
            BL_Toolbar_BotaoEstado "Modificar", "Activo"
            BL_Toolbar_BotaoEstado "Remover", "Activo"
            BL_Toolbar_BotaoEstado "ActivarRegisto", "InActivo"
        End If
    End If
End Sub

Private Sub EcNome_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcNome_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Private Sub EcSenha_Change()
    If Trim(EcSenhaAux) = "" Then
        EcSenhaAux = BG_CryptStringDecode(Trim(EcSenha))
    End If
End Sub

Private Sub EcSenhaAux_GotFocus()
    EcSenhaAux.Tag = EcSenha.Tag
    EcSenhaAux.MaxLength = EcSenha.MaxLength
    
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcSenhaAux_LostFocus()
    BG_ValidaTipoCampo Me, EcSenhaAux
    EcSenha = BG_CryptStringEncode(EcSenhaAux)
End Sub

Private Sub EcUtilizador_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcUtilizador_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Private Sub EcUtilizadorOld_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcUtilizadorOld_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.Caption = "Utilizadores"
    Me.Left = 540
    Me.Top = 450
    Me.Width = 12285
    Me.Height = 5580 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    
    NomeTabela = "sl_tbm_util"
    Set CampoDeFocus = EcUtilizador
    
    NumCampos = 16
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "cod_util"
    CamposBD(1) = "util"
    CamposBD(2) = "senha"
    CamposBD(3) = "login"
    CamposBD(4) = "cod_gr_util"
    CamposBD(5) = "nome"
    CamposBD(6) = "funcao"
    CamposBD(7) = "departamento"
    CamposBD(8) = "flg_rem"
    CamposBD(9) = "util_ant"
    CamposBD(10) = "user_cri"
    CamposBD(11) = "dt_cri"
    CamposBD(12) = "user_act"
    CamposBD(13) = "dt_act"
    CamposBD(14) = "user_rem"
    CamposBD(15) = "dt_rem"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodUtilizador
    Set CamposEc(1) = EcUtilizador
    Set CamposEc(2) = EcSenha
    Set CamposEc(3) = EcLogin
    Set CamposEc(4) = EcGrupo
    Set CamposEc(5) = EcNome
    Set CamposEc(6) = EcFuncao
    Set CamposEc(7) = EcDepartamento
    Set CamposEc(8) = EcRemovido
    Set CamposEc(9) = EcUtilizadorOld
    Set CamposEc(10) = EcUtilizadorCriacao
    Set CamposEc(11) = EcDataCriacao
    Set CamposEc(12) = EcUtilizadorAlteracao
    Set CamposEc(13) = EcDataAlteracao
    Set CamposEc(14) = EcUtilizadorRemocao
    Set CamposEc(15) = EcDataRemocao
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Utilizador"
    TextoCamposObrigatorios(4) = "Grupo"
    TextoCamposObrigatorios(5) = "Nome"
    TextoCamposObrigatorios(7) = "Departamento"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_util"
    Set ChaveEc = EcCodUtilizador
    
    ' Definir por defeito n�o listar utilizadores removidos
    ListarRemovidos = False
    PcRemovido.Visible = False
    
    CamposBDparaListBox = Array("util", "cod_gr_util", "cod_util", "departamento", "cod_gr_util")
    NumEspacos = Array(32, 24, 8, 22, 15)
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    
    Estado = 1
    BG_StackJanelas_Push Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rsTabela Is Nothing Then
        rsTabela.Close
        Set rsTabela = Nothing
    End If
    Set FormUtilizadores = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    EcCodUtilizador.Enabled = True
    EcUtilizador.Enabled = True
    EcSenhaAux.Enabled = True
    EcSenhaAux.Text = ""
    EcGrupo.Enabled = True
    EcNome.Enabled = True
    EcFuncao.Enabled = True
    EcDepartamento.Enabled = True
    LaAvisoRemovido.Visible = False
    PcRemovido.Visible = False
    PcExistente.Visible = True
    EcUtilizador.Visible = True
    EcUtilizadorOld.Visible = False
    
    If ListarRemovidos = False Then
        EcUtilizadorOld.Enabled = False
        EcUtilizadorOld.Visible = False
        EcUtilizador.Visible = True
    Else
        EcUtilizadorOld.Enabled = True
        EcUtilizadorOld.Visible = True
        EcUtilizadorOld_GotFocus
    End If
    
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
  
    BG_LimpaCampo_Todos CamposEc
    LimpaSeleccionados
End Sub

Sub DefTipoCampos()
    BG_DefTipoCampoEc NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc NomeTabela, "dt_rem", EcDataRemocao, mediTipoData
    
    BG_DefTipoCampoEc_Todos NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
    EcSenhaAux.Tag = EcSenha.Tag
End Sub

Sub PreencheValoresDefeito()
    Dim sQueryGrupoUtil As String
    
    sQueryGrupoUtil = "SELECT cod_gr_util, descr_gr_util FROM " & "sl_tbm_gr_util"
    BG_PreencheComboBD sQueryGrupoUtil, "cod_gr_util", "descr_gr_util", EcGrupo, cAscComboDesignacao
    
    BG_PreencheComboBD "GR_empr_inst", "cod_empr", "nome_empr", EcLocais, cAscComboCodigo
    
    EcUtilizadorOld.Left = EcUtilizador.Left
    EcUtilizadorOld.Top = EcUtilizador.Top
    LaAvisoRemovido.Top = LaUtilizador.Top
End Sub

Sub PreencheCampos()
    Me.SetFocus
    
    If rsTabela.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos rsTabela, CamposBD, CamposEc
        SeleccionaLocaisUtil rsTabela!cod_util
    End If
End Sub

Sub FuncaoLimpar()
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
End Sub

Sub FuncaoEstadoAnterior()
    If Estado = 2 Then
        Estado = 1
        Met_Toolbar_EstadoN Estado

        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rsTabela Is Nothing Then
            rsTabela.Close
            Set rsTabela = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.Caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    If (EcSenhaAux = "") Or (EcSenhaAux = EcUtilizador) Then
        BG_Mensagem cMsgBox, "A Senha n�o poder� ser vazia ou igual ao nome do utilizador!", vbExclamation, Me.Caption
        ValidaCamposEc = False
        Exit Function
    End If
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    Dim sLocais As String
    
    CriterioTabela = BG_ConstroiCriterio_SELECT(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        If ListarRemovidos = False Then
            CriterioTabela = CriterioTabela & " WHERE flg_rem = 0"
        End If
    Else
        If ListarRemovidos = False Then
            CriterioTabela = CriterioTabela & " AND flg_rem = 0"
        End If
    End If
    
    sLocais = DevolveLocaisPesquisa
    If sLocais <> "" Then
        If InStr(1, UCase(CriterioTabela), "WHERE", vbTextCompare) > 0 Then
            CriterioTabela = CriterioTabela & " AND " & sLocais
        Else
            CriterioTabela = CriterioTabela & " WHERE " & sLocais
        End If
    End If
    
    CriterioTabela = CriterioTabela & " ORDER BY cod_gr_util ASC, util ASC"
    
    ' Estas especifica��es s�o necess�rias para poder usar propriedades
    ' como RecordCount, Bookmark e AbsolutePosition
    Set rsTabela = New ADODB.recordset
    rsTabela.CursorLocation = adUseClient
    rsTabela.CursorType = adOpenStatic
    rsTabela.Open CriterioTabela, gConexao
    
    If rsTabela.RecordCount <= 0 Then
        BG_Mensagem cMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rsTabela.Bookmark
        BG_PreencheListBoxMultipla EcLista, rsTabela, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT", 1
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        Met_Toolbar_EstadoN Estado
        met_Toolbar_BotaoEstado "Remover", "Activo"
    End If
End Sub

Sub FuncaoAnterior()
    rsTabela.MovePrevious
    
    If rsTabela.BOF Then
        rsTabela.MoveNext
        BG_MensagemAnterior
    Else
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    rsTabela.MoveNext
    
    If rsTabela.EOF Then
        rsTabela.MovePrevious
        BG_MensagemSeguinte
    Else
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(cMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
    End If
End Sub

Sub BD_Insert()
    Dim RsVerifUtil As ADODB.recordset
    Dim sSQL As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    ' Inicio: Verifica se j� existe utilizador igual
    Set RsVerifUtil = New ADODB.recordset
    RsVerifUtil.CursorLocation = adUseClient
    RsVerifUtil.CursorType = adOpenStatic
    RsVerifUtil.Source = "SELECT util FROM " & NomeTabela & " WHERE util = '" & EcUtilizador & "'"
    RsVerifUtil.Open , gConexao
    If RsVerifUtil.RecordCount > 0 Then
        BG_Mensagem cMsgBox, "Utilizador j� existente!" & vbCrLf & vbCrLf & "Dados n�o inseridos.", vbExclamation, "Inserir"
        EcUtilizador.SetFocus
        RsVerifUtil.Close
        Set RsVerifUtil = Nothing
        Exit Sub
    End If
    RsVerifUtil.Close
    Set RsVerifUtil = Nothing
    ' Fim: Verifica se j� existe utilizador igual
    
    EcUtilizadorCriacao = Trim(gCodUtilizador)
    EcDataCriacao = BG_DaData
    EcRemovido = gT_Nao
    EcSenha = BG_CryptStringEncode(EcSenhaAux)
    EcCodUtilizador = BG_DaMAX(NomeTabela, "cod_util") + 1
    
    sSQL = BG_ConstroiCriterio_INSERT(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery sSQL
    
    AssociaUtilLocais CInt(EcCodUtilizador)

    BG_TrataErroBD
End Sub
Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(cMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
    End If

End Sub

Sub BD_Update()
    Dim RsVerifUtil As ADODB.recordset
    Dim sSQL As String
    Dim sCondicao As String
    Dim MarcaLocal As Variant
    Dim iCodutil As Integer
    
    gSQLError = 0
    gSQLISAM = 0
    
    'EcUtilizador = rsTabela("util")
    EcDataAlteracao = BG_DaData
    EcUtilizadorAlteracao = gCodUtilizador
    EcSenha = BG_CryptStringEncode(EcSenhaAux)
    
    iCodutil = rsTabela!cod_util

    If EcUtilizador <> rsTabela("util") Then
        Set RsVerifUtil = New ADODB.recordset
        RsVerifUtil.CursorLocation = adUseClient
        RsVerifUtil.CursorType = adOpenStatic
        RsVerifUtil.Source = "SELECT util FROM " & NomeTabela & " WHERE util = '" & EcUtilizador & "'"
        RsVerifUtil.Open , gConexao
        If RsVerifUtil.RecordCount > 0 Then
            BG_Mensagem cMsgBox, "Utilizador j� existente!" & vbCrLf & vbCrLf & "Dados n�o Modificados.", vbExclamation, "Modificar"
            EcUtilizador.SetFocus
            RsVerifUtil.Close
            Set RsVerifUtil = Nothing
            Exit Sub
        End If
        RsVerifUtil.Close
        Set RsVerifUtil = Nothing
    End If
    
    sCondicao = ChaveBD & " = " & BG_CvPlica(rsTabela(ChaveBD))
    sSQL = BG_ConstroiCriterio_UPDATE(NomeTabela, CamposBD, CamposEc, sCondicao)
    BG_ExecutaQuery sSQL
    BG_TrataErroBD
        
    MarcaLocal = rsTabela.Bookmark
    rsTabela.Requery
    
    AssociaUtilLocais iCodutil

    If rsTabela.BOF And rsTabela.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla EcLista, rsTabela, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= rsTabela.RecordCount Then rsTabela.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rsTabela.RecordCount Then rsTabela.Bookmark = MarcaLocal
    
    SeleccionaLocaisUtil iCodutil
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer desactivar o utilizador?"
    gMsgResp = BG_Mensagem(cMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BD_Delete
    End If
End Sub

Sub BD_Delete()
    Dim sCondicao As String
    Dim sSQL As String
    Dim MarcaLocal As Variant
       
    EcRemovido = gT_Sim
    EcUtilizadorOld = EcUtilizador
    EcUtilizador = EcCodUtilizador + " - DESACTIVADO"
    EcDataRemocao = BG_DaData
    EcUtilizadorRemocao = gCodUtilizador

    sCondicao = ChaveBD & " = " & BG_CvPlica(rsTabela(ChaveBD))
    sSQL = BG_ConstroiCriterio_UPDATE(NomeTabela, CamposBD, CamposEc, sCondicao)
    BG_ExecutaQuery sSQL
    BG_Mensagem cMsgBox, "Opera��o efectuada com sucesso.", vbInformation, "Remover"
    
    MarcaLocal = rsTabela.Bookmark
    rsTabela.Requery
        
    If rsTabela.BOF And rsTabela.EOF Then
        BG_Mensagem cMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla EcLista, rsTabela, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rsTabela.RecordCount Then rsTabela.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rsTabela.RecordCount Then rsTabela.Bookmark = MarcaLocal
    Else
        rsTabela.MoveLast
    End If
    
    If rsTabela.EOF Then rsTabela.MovePrevious
    
    LimpaCampos
    PreencheCampos
End Sub


Attribute VB_Name = "SMS"
Option Explicit

' ------------------------------------------------------------------------------------------------

' ENVIA NOVA SMS. INSERE NAS TABELAS DE INTERFACES. POSTERIORMENTE WEBSERVICE ENVIA SMS.

' ------------------------------------------------------------------------------------------------
Public Function SMS_NovaMensagem(n_req As String) As Boolean
    Dim sSql As String
    Dim a_seq_sms As Long
    Dim numTelemovel As Long
    Dim mensagem As String
    Dim rsSMS As New ADODB.recordset
    Dim resSMS As Long
    Dim dataActual As String
    Dim horaActual As String
    On Error GoTo TrataErro
    
    If SMS_VerificaEnvio(n_req) = True Then
        numTelemovel = SMS_RetornaNumTelemovel(n_req)
        mensagem = SMS_RetornaMensagem
        BG_BeginTransaction
        
        ' SEQ_SMS
        a_seq_sms = -1
        sSql = "SELECT SEQ_SMS.nextval prox FROM dual "
        rsSMS.CursorLocation = adUseServer
        rsSMS.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsSMS.Open sSql, gConexao
        If rsSMS.RecordCount >= 1 Then
            a_seq_sms = BL_HandleNull(rsSMS!prox, -1)
        End If
        
        If numTelemovel = -1 Or mensagem = "" Or a_seq_sms = -1 Then
            SMS_NovaMensagem = False
            BG_CommitTransaction
            Exit Function
        End If
        dataActual = Bg_DaData_ADO
        horaActual = Bg_DaHora_ADO
        
        mensagem = Replace(mensagem, "#N_REQ#", n_req)
        ' ---------------------------------------
        ' INSERE NA TABELA SISLAB
        ' ---------------------------------------
        sSql = "INSERT INTO SL_SMS_ENVIO (seq_sms, N_REQ, NUM_TELEMOVEL,MENSAGEM, USER_CRI, DT_CRI, HR_CRI,ESTADO,id_externo ) VALUES ("
        sSql = sSql & a_seq_sms & ", "
        sSql = sSql & n_req & ", "
        sSql = sSql & numTelemovel & ", "
        sSql = sSql & BL_TrataStringParaBD(mensagem) & ", "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        sSql = sSql & BL_TrataDataParaBD(dataActual) & ", "
        sSql = sSql & BL_TrataStringParaBD(horaActual) & ",'WAITING',-1) "
        BG_ExecutaQuery_ADO sSql
        
        resSMS = SMS_EnviaMensagem(a_seq_sms, mensagem, numTelemovel)
        
        ' ---------------------------------------
        ' ALTERA NA TABELA SL_REQUIS
        ' ---------------------------------------
        If resSMS > 0 Then
            sSql = "UPDATE SL_SMS_ENVIO SET id_externo = " & resSMS & " WHERE seq_sms = " & a_seq_sms
            BG_ExecutaQuery_ADO sSql
            
            sSql = "UPDATE sl_requis set user_sms = " & BL_TrataStringParaBD(CStr(gCodUtilizador))
            sSql = sSql & ", dt_sms =" & BL_TrataDataParaBD(dataActual) & ", hr_sms = " & BL_TrataStringParaBD(horaActual)
            sSql = sSql & ", flg_sms_enviada = 1 WHERE n_req = " & n_req
            BG_ExecutaQuery_ADO sSql
        End If
        
    BG_CommitTransaction
    End If
    SMS_NovaMensagem = True
Exit Function
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "SMS_NovaMensagem: " & sSql & " - " & Err.Description, "SMS", "SMS_NovaMensagem", True
    SMS_NovaMensagem = False
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' CARREGA CONFIGURA��ES SOBRE ENVIO DE SMS

' ------------------------------------------------------------------------------------------------
Public Sub SMS_CarregaConfig()
    Dim sSql As String
    Dim rsSMS As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM SL_SMS_CONFIG"
    rsSMS.CursorLocation = adUseServer
    rsSMS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSMS.Open sSql, gConexao
    If rsSMS.RecordCount >= 1 Then
        While Not rsSMS.EOF
            Select Case BL_HandleNull(rsSMS!chave, "")
                Case "ENVIO_TODAS_REQ_CONCLUIDAS"
                    gSMSEnviaTodasReq = BL_HandleNull(rsSMS!flg_activo, 0)
                Case "ENVIO_REQ_CONCLUIDAS_ASSINALADAS"
                    gSMSEnviaReqAssinaladas = BL_HandleNull(rsSMS!flg_activo, 0)
                Case "ENVIO_REQ_CONCLUIDAS_ANTES_PRAZO"
                    gSMSEnviaReqProntasAntesTempo = BL_HandleNull(rsSMS!flg_activo, 0)
            End Select
            rsSMS.MoveNext
        Wend
    End If
    rsSMS.Close
    Set rsSMS = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "SMS_CarregaConfig: " & sSql & " - " & Err.Description, "SMS", "SMS_CarregaConfig", True
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' VERIFICA SE � PARA ENVIAR SMS PARA REQUISICAO EM CAUSA

' ------------------------------------------------------------------------------------------------
Public Function SMS_VerificaEnvio(NReq As String) As Boolean
    On Error GoTo TrataErro
    
    SMS_VerificaEnvio = False
    ' SE ENVIO DE SMS ESTA ACTIVO
    If gEnvioSMSActivo = mediSim Then
        ' SE SMS AINDA NAO FOI ENVIADA
        If SMS_VerificaSMSJaEnviada(NReq) = False Then
            ' SE REQUISICAO ESTA CONCLUIDA
            If SMS_VerificaReqConcluida(NReq) = True Then
                ' SE ENVIO DE SMS ESTA ACTIVO PARA REQUISICOES MARCADAS PARA ENVIAR
                If gSMSEnviaReqAssinaladas = mediSim Then
                    ' SE REQUISICAO FOI MARCADA PARA ENVIAR SMS
                    If SMS_VerificaReqAssinalada(NReq) = True Then
                        SMS_VerificaEnvio = True
                    End If
                ' SE SMS ESTA ACTIVO PARA REQUISICOES PRONTAS ANTES DO TEMPO
                ElseIf gSMSEnviaReqProntasAntesTempo = mediSim Then
                    ' SE REQUISICAO FOI CONCLUIDA ANTES DO PRAZO VALIDADE
                    If SMS_VerificaReqConcluidaAntesPrazo(NReq) = True Then
                        SMS_VerificaEnvio = True
                    End If
                ' SE ENVIO ESTA ACTIVO PARA TODAS REQUISICOES
                ElseIf gSMSEnviaTodasReq = mediSim Then
                    SMS_VerificaEnvio = True
                End If
            End If
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "SMS_VerificaEnvio: " & " - " & Err.Description, "SMS", "SMS_VerificaEnvio", True
    SMS_VerificaEnvio = False
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' VERIFICA SE SMS JA FOI ENVIADA

' ------------------------------------------------------------------------------------------------
Public Function SMS_VerificaSMSJaEnviada(NReq As String) As Boolean
    Dim sSql As String
    Dim rsSMS As New ADODB.recordset
    On Error GoTo TrataErro
    
    SMS_VerificaSMSJaEnviada = True
    sSql = "SELECT flg_sms_enviada FROM sl_requis WHERE n_req = " & NReq
    rsSMS.CursorLocation = adUseServer
    rsSMS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSMS.Open sSql, gConexao
    
    If rsSMS.RecordCount = 1 Then
        If BL_HandleNull(rsSMS!flg_sms_enviada, 0) = 0 Then
            SMS_VerificaSMSJaEnviada = False
        ElseIf BL_HandleNull(rsSMS!flg_sms_enviada, 0) = 1 Then
            SMS_VerificaSMSJaEnviada = True
        End If
    Else
        SMS_VerificaSMSJaEnviada = True
    End If
    rsSMS.Close
    Set rsSMS = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros "SMS_VerificaSMSJaEnviada: " & sSql & " - " & Err.Description, "SMS", "SMS_VerificaSMSJaEnviada", True
    SMS_VerificaSMSJaEnviada = True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' VERIFICA SE REQUISICAO FOI ASSINALADA PARA ENVIAR

' ------------------------------------------------------------------------------------------------
Public Function SMS_VerificaReqAssinalada(NReq As String) As Boolean
    Dim sSql As String
    Dim rsSMS As New ADODB.recordset
    On Error GoTo TrataErro
    
    SMS_VerificaReqAssinalada = False
    sSql = "SELECT flg_aviso_sms FROM sl_requis WHERE n_req = " & NReq
    rsSMS.CursorLocation = adUseServer
    rsSMS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSMS.Open sSql, gConexao
    
    If rsSMS.RecordCount = 1 Then
        If BL_HandleNull(rsSMS!flg_aviso_sms, 0) = 0 Then
            SMS_VerificaReqAssinalada = False
        ElseIf BL_HandleNull(rsSMS!flg_aviso_sms, 0) = 1 Then
            SMS_VerificaReqAssinalada = True
        End If
    Else
        SMS_VerificaReqAssinalada = False
    End If
    rsSMS.Close
    Set rsSMS = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros "SMS_VerificaReqAssinalada: " & sSql & " - " & Err.Description, "SMS", "SMS_VerificaReqAssinalada", True
    SMS_VerificaReqAssinalada = False
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------------------

' VERIFICA SE REQUISICAO ESTA TODA CONCLUIDA

' ------------------------------------------------------------------------------------------------
Public Function SMS_VerificaReqConcluida(NReq As String) As Boolean
    Dim sSql As String
    Dim rsSMS As New ADODB.recordset
    On Error GoTo TrataErro
    
    SMS_VerificaReqConcluida = False
    
    sSql = "SELECT * FROM sl_marcacoes WHERE n_req = " & NReq & " AND cod_ana_s <> 'S99999' "
    rsSMS.CursorLocation = adUseServer
    rsSMS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSMS.Open sSql, gConexao
    If rsSMS.RecordCount >= 1 Then
        SMS_VerificaReqConcluida = False
    Else
        rsSMS.Close
        sSql = "SELECT * FROM sl_realiza WHERE n_req = " & NReq & " AND cod_ana_s <> 'S99999' "
        sSql = sSql & " AND flg_estado NOT IN (" & gEstadoAnaValidacaoMedica & ", " & gEstadoAnaImpressa & ")"
        rsSMS.CursorLocation = adUseServer
        rsSMS.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsSMS.Open sSql, gConexao
        If rsSMS.RecordCount >= 1 Then
            SMS_VerificaReqConcluida = False
        Else
            SMS_VerificaReqConcluida = True
        End If
    End If
    rsSMS.Close
    Set rsSMS = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros "SMS_VerificaReqConcluida: " & sSql & " - " & Err.Description, "SMS", "SMS_VerificaReqConcluida", True
    SMS_VerificaReqConcluida = False
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' VERIFICA SE REQUISICAO CONCLUIDA ANTES DO TEMPO

' ------------------------------------------------------------------------------------------------
Public Function SMS_VerificaReqConcluidaAntesPrazo(NReq As String) As Boolean
    Dim sSql As String
    Dim rsSMS As New ADODB.recordset
    On Error GoTo TrataErro
    
    SMS_VerificaReqConcluidaAntesPrazo = False
    sSql = "SELECT dt_conclusao FROM sl_requis WHERE n_req = " & NReq
    rsSMS.CursorLocation = adUseServer
    rsSMS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSMS.Open sSql, gConexao
    
    If rsSMS.RecordCount = 1 Then
        If BL_HandleNull(rsSMS!dt_conclusao, "") > Bg_DaData_ADO Then
            SMS_VerificaReqConcluidaAntesPrazo = True
        Else
            SMS_VerificaReqConcluidaAntesPrazo = False
        End If
    Else
        SMS_VerificaReqConcluidaAntesPrazo = False
    End If
    rsSMS.Close
    Set rsSMS = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros "SMS_VerificaReqConcluidaAntesPrazo: " & sSql & " - " & Err.Description, "SMS", "SMS_VerificaReqConcluidaAntesPrazo", True
    SMS_VerificaReqConcluidaAntesPrazo = False
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------------------------------------------------

' VERIFICA SE REQUISICAO CONCLUIDA ANTES DO TEMPO

' ------------------------------------------------------------------------------------------------
Public Function SMS_RetornaNumTelemovel(NReq As String) As Long
    Dim sSql As String
    Dim rsSMS As New ADODB.recordset
    On Error GoTo TrataErro
    
    SMS_RetornaNumTelemovel = -1
    sSql = "SELECT telef_ute, telemovel FROM sl_identif WHERE seq_utente = (SELECT seq_utente FROM sl_requis WHERE n_req = " & NReq & ") "
    rsSMS.CursorLocation = adUseServer
    rsSMS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSMS.Open sSql, gConexao
    
    If rsSMS.RecordCount = 1 Then
        If BL_HandleNull(rsSMS!telemovel, "") <> "" Then
            If Mid(rsSMS!telemovel, 1, 2) = "91" Or Mid(rsSMS!telemovel, 1, 2) = "92" Or Mid(rsSMS!telemovel, 1, 2) = "93" Or Mid(rsSMS!telemovel, 1, 2) = "96" Or _
            Mid(rsSMS!telemovel, 1, 2) = "94" Or Mid(rsSMS!telemovel, 1, 2) = "95" Or Mid(rsSMS!telemovel, 1, 2) = "97" Or Mid(rsSMS!telemovel, 1, 2) = "98" Or _
            Mid(rsSMS!telemovel, 1, 2) = "99" Then
                SMS_RetornaNumTelemovel = CLng(BL_HandleNull(rsSMS!telemovel, "-1"))
            Else
                SMS_RetornaNumTelemovel = -1
            End If
        ElseIf BL_HandleNull(rsSMS!telef_ute, "") <> "" Then
             If Mid(rsSMS!telemovel, 1, 2) = "91" Or Mid(rsSMS!telemovel, 1, 2) = "92" Or Mid(rsSMS!telemovel, 1, 2) = "93" Or Mid(rsSMS!telemovel, 1, 2) = "96" Or _
            Mid(rsSMS!telemovel, 1, 2) = "94" Or Mid(rsSMS!telemovel, 1, 2) = "95" Or Mid(rsSMS!telemovel, 1, 2) = "97" Or Mid(rsSMS!telemovel, 1, 2) = "98" Or _
            Mid(rsSMS!telemovel, 1, 2) = "99" Then
                SMS_RetornaNumTelemovel = CLng(BL_HandleNull(rsSMS!telef_ute, "-1"))
            Else
                SMS_RetornaNumTelemovel = -1
            End If
        Else
            SMS_RetornaNumTelemovel = -1
        End If
    End If
    rsSMS.Close
    Set rsSMS = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros "SMS_RetornaNumTelemovel: " & sSql & " - " & Err.Description, "SMS", "SMS_RetornaNumTelemovel", True
    SMS_RetornaNumTelemovel = -1
    Exit Function
    Resume Next
End Function




' ------------------------------------------------------------------------------------------------

' RETORNA MENSAGEM A ENVIAR

' ------------------------------------------------------------------------------------------------
Public Function SMS_RetornaMensagem() As String
    Dim sSql As String
    Dim rsSMS As New ADODB.recordset
    On Error GoTo TrataErro
    
    SMS_RetornaMensagem = ""
    If gSMSEnviaReqAssinaladas = mediSim Then
        sSql = "SELECT * FROM sl_sms_config WHERE chave = 'ENVIO_REQ_CONCLUIDAS_ASSINALADAS'"
    ElseIf gSMSEnviaReqProntasAntesTempo = mediSim Then
        sSql = "SELECT * FROM sl_sms_config WHERE chave = 'ENVIO_REQ_CONCLUIDAS_ANTES_PRAZO'"
    ElseIf gSMSEnviaTodasReq = mediSim Then
        sSql = "SELECT * FROM sl_sms_config WHERE chave = 'ENVIO_TODAS_REQ_CONCLUIDAS'"
    Else
        SMS_RetornaMensagem = ""
        Exit Function
    End If
    rsSMS.CursorLocation = adUseServer
    rsSMS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSMS.Open sSql, gConexao
    If rsSMS.RecordCount = 1 Then
         SMS_RetornaMensagem = BL_HandleNull(rsSMS!mensagem, "")
    End If
    rsSMS.Close
    Set rsSMS = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "SMS_RetornaMensagem: " & " - " & Err.Description, "SMS", "SMS_RetornaMensagem", True
    SMS_RetornaMensagem = ""
    Exit Function
    Resume Next
End Function


Private Function SMS_EnviaMensagem(id_externo As Long, mensagem As String, num_telemovel As Long) As Long
    Dim cmdSMS As New ADODB.Command
    Dim PmtSequence As ADODB.Parameter ' output
    Dim PmtInternalID As ADODB.Parameter
    Dim PmtNumTelemovel As ADODB.Parameter
    Dim PmtMensagem As ADODB.Parameter
    
    Dim resultado As Long
    
    On Error GoTo TrataErro
    
    gSQLError = 0
    gSQLISAM = 0
    
    With cmdSMS
        .ActiveConnection = gConexao
        .CommandText = "SLP_SMS_ENVIO"
        .CommandType = adCmdStoredProc
    End With
    
    Set PmtSequence = cmdSMS.CreateParameter("P_SEQ", adNumeric, adParamOutput, 0)
    PmtSequence.Precision = 12
    
    Set PmtInternalID = cmdSMS.CreateParameter("P_SEQ_INTERNO", adNumeric, adParamInput, 0)
    Set PmtNumTelemovel = cmdSMS.CreateParameter("P_NUM_TELEMOVEL", adVarChar, adParamInput, 30)
    Set PmtMensagem = cmdSMS.CreateParameter("P_MENSAGEM", adVarChar, adParamInput, 300)
    
    cmdSMS.Parameters.Append PmtInternalID
    cmdSMS.Parameters.Append PmtNumTelemovel
    cmdSMS.Parameters.Append PmtMensagem
    cmdSMS.Parameters.Append PmtSequence
    
    cmdSMS.Parameters("P_SEQ_INTERNO") = id_externo
    cmdSMS.Parameters("P_NUM_TELEMOVEL") = CStr(num_telemovel)
    cmdSMS.Parameters("P_MENSAGEM") = mensagem
    cmdSMS.Execute
    
    resultado = Trim(cmdSMS.Parameters.Item("P_SEQ").value)
    If resultado > 0 Then
        SMS_EnviaMensagem = resultado
    Else
        SMS_EnviaMensagem = -1
    End If
    
    Set cmdSMS = Nothing
    Set PmtSequence = Nothing
    Set PmtInternalID = Nothing
    Set PmtNumTelemovel = Nothing
    Set PmtMensagem = Nothing


Exit Function
TrataErro:
    SMS_EnviaMensagem = -1
    BG_LogFile_Erros "ERro ao enviar SMS:" & Err.Number & " - " & Err.Description, "SMS", "SMS_EnviaMensagem", True
    Exit Function
    Resume Next
End Function
  
''''''''''''''''''''''''''''''''''''''''''''''
'RGONCALVES 16.06.2013 ICIL-470
'Envia mensagem com resultado de an�lise
'ICIL-972 - enviar nome, abreviatura, data de nascimento, data da requisicao, t_utente+utente
Public Function SMS_NovaMensagem_Resultado_Analise(ByVal n_req As String, ByVal cod_ana_s As String) As Boolean
    Dim sSql As String
    Dim a_seq_sms As Long
    Dim numTelemovel As Long
    Dim mensagem As String
    Dim rsSMS As New ADODB.recordset
    Dim resSMS As Long
    Dim dataActual As String
    Dim horaActual As String
    Dim seq_realiza As String
    On Error GoTo TrataErro
    
    If SMS_VerificaEnvio_Resultado_Analise(n_req, cod_ana_s) = True Then
        
        'SE J� MANDOU SMS N�O MANDA OUTRA VEZ
        If SMS_VerificaSMSJaEnviada_Resultado_Analise(SMS_Devolve_seq_realiza(n_req, cod_ana_s)) = True Then
            SMS_NovaMensagem_Resultado_Analise = False
            Exit Function
        End If
        
        mensagem = SMS_RetornaMensagem_Resultado_Analise(n_req, cod_ana_s)
        If mensagem = "" Then
            SMS_NovaMensagem_Resultado_Analise = False
            Exit Function
        End If
        
        numTelemovel = SMS_RetornaNumTelemovel(n_req)
        
        ' SEQ_SMS
        a_seq_sms = -1
        sSql = "SELECT SEQ_SMS.nextval prox FROM dual "
        rsSMS.CursorLocation = adUseServer
        rsSMS.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsSMS.Open sSql, gConexao
        If rsSMS.RecordCount >= 1 Then
            a_seq_sms = BL_HandleNull(rsSMS!prox, -1)
        End If
        rsSMS.Close
        
        seq_realiza = SMS_Devolve_seq_realiza(n_req, cod_ana_s)
        
        If numTelemovel = -1 Or mensagem = "" Or a_seq_sms = -1 Then
            SMS_NovaMensagem_Resultado_Analise = False
            Exit Function
        End If
        dataActual = Bg_DaData_ADO
        horaActual = Bg_DaHora_ADO
        
        'ICIL-972
        sSql = "SELECT t_utente, utente, nome_ute, dt_nasc_ute, abrev_ute, n_req, dt_chega FROM sl_identif x1, sl_requis x2 "
        sSql = sSql & " WHERE x1.seq_utente = x2.seq_utente AND x2.n_reQ =" & n_req
        rsSMS.CursorLocation = adUseServer
        rsSMS.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsSMS.Open sSql, gConexao
        If rsSMS.RecordCount >= 1 Then
            mensagem = Replace(mensagem, "{NOME_UTE}", BL_HandleNull(rsSMS!nome_ute, ""))
            mensagem = Replace(mensagem, "{DT_NASC_UTE}", BL_HandleNull(rsSMS!dt_nasc_ute, ""))
            mensagem = Replace(mensagem, "{ABREV_UTE}", BL_HandleNull(rsSMS!abrev_ute, ""))
            mensagem = Replace(mensagem, "{T_UTENTE}", BL_HandleNull(rsSMS!t_utente, ""))
            mensagem = Replace(mensagem, "{UTENTE}", BL_HandleNull(rsSMS!Utente, ""))
            mensagem = Replace(mensagem, "{N_REQ}", BL_HandleNull(rsSMS!n_req, ""))
            mensagem = Replace(mensagem, "{DT_CHEGA}", BL_HandleNull(rsSMS!dt_chega, ""))
        End If
        rsSMS.Close
        Set rsSMS = Nothing
        ' ---------------------------------------
        ' INSERE NA TABELA SISLAB
        ' ---------------------------------------
        sSql = "INSERT INTO SL_SMS_ENVIO (seq_sms, N_REQ, NUM_TELEMOVEL,MENSAGEM, USER_CRI, DT_CRI, HR_CRI,ESTADO,id_externo,seq_realiza ) VALUES ("
        sSql = sSql & a_seq_sms & ", "
        sSql = sSql & n_req & ", "
        sSql = sSql & numTelemovel & ", "
        sSql = sSql & BL_TrataStringParaBD(mensagem) & ", "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        sSql = sSql & BL_TrataDataParaBD(dataActual) & ", "
        sSql = sSql & BL_TrataStringParaBD(horaActual) & ",'WAITING',-1, "
        sSql = sSql & BL_TrataStringParaBD(seq_realiza) & ")"
        BG_ExecutaQuery_ADO sSql
        
        resSMS = SMS_EnviaMensagem(a_seq_sms, mensagem, numTelemovel)
        
        
        If resSMS > 0 Then
            sSql = "UPDATE SL_SMS_ENVIO SET id_externo = " & resSMS & " WHERE seq_sms = " & a_seq_sms
            BG_ExecutaQuery_ADO sSql
'
'            sSql = "UPDATE sl_requis set user_sms = " & BL_TrataStringParaBD(CStr(gCodUtilizador))
'            sSql = sSql & ", dt_sms =" & BL_TrataDataParaBD(dataActual) & ", hr_sms = " & BL_TrataStringParaBD(horaActual)
'            sSql = sSql & ", flg_sms_enviada = 1 WHERE n_req = " & n_req
'            BG_ExecutaQuery_ADO sSql
        End If
        
    End If
    SMS_NovaMensagem_Resultado_Analise = True
Exit Function
TrataErro:
    BG_LogFile_Erros "SMS_NovaMensagem_Resultado_Analise: " & sSql & " - " & Err.Description, "SMS", "SMS_NovaMensagem_Resultado_Analise", True
    SMS_NovaMensagem_Resultado_Analise = False
    Exit Function
    Resume Next
End Function

'RGONCALVES 16.06.2013 ICIL-470
'devolve o texto de SMS associado � an�lise
Private Function SMS_RetornaMensagem_Resultado_Analise(ByVal n_req As String, ByVal cod_ana_s As String) As String

    On Error GoTo TrataErro
    
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim RsResultado As ADODB.recordset
    Dim texto_sms As String
    Dim texto_sms_envio As String
    Dim t_result As String
    Dim flg_envio_sms As String
    Dim cod_ana_resultado As String
    Dim n_resultado As String
    Dim resultado As String
    Dim inicio_cod_ana As Integer
    Dim i As Integer
    Dim caracter As String
    Dim tamanho_formula As Integer
    Dim proximo_caracter As String
    
    cod_ana_s = UCase(Trim(cod_ana_s))
    
    If cod_ana_s = "S99999" Or cod_ana_s = "" Then
        SMS_RetornaMensagem_Resultado_Analise = ""
        Exit Function
    End If
    
    sql = " SELECT flg_envio_sms, texto_sms, t_result FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    
    Set rs = New ADODB.recordset
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    If rs.RecordCount = 1 Then
        flg_envio_sms = CStr(BL_HandleNull(rs!flg_envio_sms, "0"))
        texto_sms = Trim(CStr(BL_HandleNull(rs!texto_sms, "")))
        texto_sms_envio = texto_sms
        t_result = CStr(BL_HandleNull(rs!t_result, ""))
        
        If flg_envio_sms = 1 Then
            'flag de envio activa
            If t_result = gT_Alfanumerico Or _
                t_result = gT_Numerico Or _
                t_result = gT_Auxiliar Then
                
                For i = 1 To Len(texto_sms)
                    caracter = Mid(texto_sms, i, 1)
                    cod_ana_resultado = ""
                    n_resultado = ""
                    If caracter = "$" Or caracter = "@" Then
                        'procurar f�rmula
                        cod_ana_resultado = caracter
                        proximo_caracter = UCase(Mid(texto_sms, i + 1, 1))
                        If proximo_caracter <> "" Then
                            Do While (Asc(proximo_caracter) >= Asc("A") And _
                                Asc(proximo_caracter) <= Asc("Z")) Or _
                                (Asc(proximo_caracter) >= Asc("0") And _
                                Asc(proximo_caracter) <= Asc("9"))

                                cod_ana_resultado = cod_ana_resultado & proximo_caracter
                                i = i + 1
                                proximo_caracter = UCase(Mid(texto_sms, i + 1, 1))
                                
                                If proximo_caracter = "" Then
                                    Exit Do
                                End If
                            Loop
                        End If
                        If caracter = "$" Then
                            n_resultado = 1
                        ElseIf caracter = "@" Then
                            n_resultado = 2
                        Else
                            n_resultado = ""
                        End If
                        
                        If n_resultado <> "" And UCase(Mid(cod_ana_resultado, 2)) = cod_ana_s Then
                            
                            sql = " SELECT i.telemovel, i.telef_ute, a.n_res, a.result " & _
                                " FROM sl_res_alfan a, sl_realiza r, sl_requis req, sl_identif i " & _
                                " WHERE i.seq_utente = Req.seq_utente " & _
                                " AND req.n_req = r.n_req " & _
                                " AND r.seq_realiza = a.seq_realiza " & _
                                " AND r.n_req = " & BL_TrataStringParaBD(n_req) & _
                                " AND r.cod_ana_s = " & BL_TrataStringParaBD(UCase(Mid(cod_ana_resultado, 2))) & _
                                " AND a.n_res = " & BL_TrataStringParaBD(n_resultado) & _
                                " ORDER BY a.seq_realiza "
                            
                            Set RsResultado = New ADODB.recordset
        
                            RsResultado.CursorLocation = adUseServer
                            RsResultado.CursorType = adOpenStatic
                            RsResultado.Open sql, gConexao
                            If RsResultado.RecordCount > 0 Then
                                resultado = CStr(BL_HandleNull(RsResultado!Result, ""))
                                If resultado = cVerObservacoes Or resultado = cVerAnexo Or resultado = cSemResultado Or resultado = cAnaliseEmCurso Then
                                    resultado = ""
                                End If
                                texto_sms_envio = Replace(texto_sms_envio, cod_ana_resultado, resultado)
                            Else
                                texto_sms_envio = Replace(texto_sms_envio, cod_ana_resultado, "")
                            End If
                            RsResultado.Close
                            Set RsResultado = Nothing
                        End If
                    End If
                Next i
            End If
        End If
    End If
    
    rs.Close
    Set rs = Nothing
    SMS_RetornaMensagem_Resultado_Analise = texto_sms_envio
    Exit Function
    
TrataErro:
    SMS_RetornaMensagem_Resultado_Analise = ""
    BG_LogFile_Erros "ERRO ao enviar SMS:" & Err.Number & " - " & Err.Description, "SMS", "SMS_RetornaMensagem_Resultado_Analise", True
    Exit Function
    Resume Next
End Function


'RGONCALVES 16.06.2013 ICIL-470
Private Function SMS_VerificaEnvio_Resultado_Analise(ByVal NReq As String, ByVal cod_ana_s As String) As Boolean
    On Error GoTo TrataErro
    
     SMS_VerificaEnvio_Resultado_Analise = False
    
    ' SE ENVIO DE SMS ESTA ACTIVO
    If gEnvioResultadosSMSActivo = mediSim Then
        'SE � UM C�DIGO QUE N�O � PARA ENVIAR
        If Trim(cod_ana_s) <> "" And Trim(cod_ana_s) <> "S99999" Then
            ' SE REQUISICAO FOI MARCADA PARA ENVIAR SMS
            If SMS_VerificaReqAssinalada(NReq) = True Then
                SMS_VerificaEnvio_Resultado_Analise = True
            End If
        End If
    End If
    
    
        
Exit Function
TrataErro:
    BG_LogFile_Erros "SMS_VerificaEnvio_Resultado_Analise: " & " - " & Err.Description, "SMS", "SMS_VerificaEnvio_Resultado_Analise", True
    SMS_VerificaEnvio_Resultado_Analise = False
    Exit Function
    Resume Next
End Function

'RGONCALVES 16.06.2013 ICIL-470
'verifica se j� tem registo de envio da SMS para o seq_realiza
Private Function SMS_VerificaSMSJaEnviada_Resultado_Analise(ByVal seq_realiza As String) As Boolean
    Dim sSql As String
    Dim rsSMS As New ADODB.recordset
    On Error GoTo TrataErro
    
    If seq_realiza = "" Then
        SMS_VerificaSMSJaEnviada_Resultado_Analise = True
        Exit Function
    End If
    
    sSql = "SELECT seq_realiza FROM sl_sms_envio WHERE seq_realiza = " & seq_realiza
    rsSMS.CursorLocation = adUseServer
    rsSMS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSMS.Open sSql, gConexao
    
    If rsSMS.RecordCount > 0 Then
        SMS_VerificaSMSJaEnviada_Resultado_Analise = True
    Else
        SMS_VerificaSMSJaEnviada_Resultado_Analise = False
    End If
    rsSMS.Close
    Set rsSMS = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros "SMS_VerificaSMSJaEnviada_Resultado_Analise: " & sSql & " - " & Err.Description, "SMS", "SMS_VerificaSMSJaEnviada_Resultado_Analise", True
    SMS_VerificaSMSJaEnviada_Resultado_Analise = True
    Exit Function
    Resume Next
End Function

'RGONCALVES 16.06.2013 ICIL-470
'verifica se j� tem registo de envio da SMS para o seq_realiza
Private Function SMS_Devolve_seq_realiza(ByVal n_req As String, ByVal cod_ana_s As String) As String
    Dim sSql As String
    Dim rs As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT seq_realiza FROM sl_realiza WHERE n_req = " & BL_TrataStringParaBD(n_req) & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s) & " ORDER BY seq_realiza "
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
    
    If rs.RecordCount > 0 Then
        SMS_Devolve_seq_realiza = BL_HandleNull(rs!seq_realiza, "")
    Else
        SMS_Devolve_seq_realiza = ""
    End If
    rs.Close
    Set rs = Nothing
    
Exit Function
TrataErro:
    BG_LogFile_Erros "SMS_Devolve_seq_realiza: " & sSql & " - " & Err.Description, "SMS", "SMS_Devolve_seq_realiza", True
    SMS_Devolve_seq_realiza = ""
    Exit Function
    Resume Next
End Function

'RGONCALVES 16.06.2013 ICIL-470
Public Function SMS_NovaMensagem_Avulso(ByVal numTelemovel As Long, ByVal mensagem As String) As Boolean
    Dim sSql As String
    Dim a_seq_sms As Long
    Dim rsSMS As New ADODB.recordset
    Dim resSMS As Long
    Dim dataActual As String
    Dim horaActual As String
    On Error GoTo TrataErro
    
    If gEnvioSMSAvulsoActivo = mediSim Then
        
        BG_BeginTransaction
        
        ' SEQ_SMS
        a_seq_sms = -1
        sSql = "SELECT SEQ_SMS.nextval prox FROM dual "
        rsSMS.CursorLocation = adUseServer
        rsSMS.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsSMS.Open sSql, gConexao
        If rsSMS.RecordCount >= 1 Then
            a_seq_sms = BL_HandleNull(rsSMS!prox, -1)
        End If
        
        If numTelemovel = -1 Or mensagem = "" Or a_seq_sms = -1 Then
            SMS_NovaMensagem_Avulso = False
            BG_CommitTransaction
            Exit Function
        End If
        dataActual = Bg_DaData_ADO
        horaActual = Bg_DaHora_ADO
        
        ' ---------------------------------------
        ' INSERE NA TABELA SISLAB
        ' ---------------------------------------
        sSql = "INSERT INTO SL_SMS_ENVIO (seq_sms, N_REQ, NUM_TELEMOVEL,MENSAGEM, USER_CRI, DT_CRI, HR_CRI,ESTADO,id_externo ) VALUES ("
        sSql = sSql & a_seq_sms & ", "
        sSql = sSql & "null" & ", "
        sSql = sSql & numTelemovel & ", "
        sSql = sSql & BL_TrataStringParaBD(mensagem) & ", "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        sSql = sSql & BL_TrataDataParaBD(dataActual) & ", "
        sSql = sSql & BL_TrataStringParaBD(horaActual) & ",'WAITING',-1) "
        BG_ExecutaQuery_ADO sSql
        
        resSMS = SMS_EnviaMensagem(a_seq_sms, mensagem, numTelemovel)
        
        ' ---------------------------------------
        ' ALTERA NA TABELA SL_REQUIS
        ' ---------------------------------------
        If resSMS > 0 Then
            sSql = "UPDATE SL_SMS_ENVIO SET id_externo = " & resSMS & " WHERE seq_sms = " & a_seq_sms
            BG_ExecutaQuery_ADO sSql
            
        End If
        
    BG_CommitTransaction
    End If
    SMS_NovaMensagem_Avulso = True
Exit Function
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "SMS_NovaMensagem_Avulso: " & sSql & " - " & Err.Description, "SMS", "SMS_NovaMensagem_Avulso", True
    SMS_NovaMensagem_Avulso = False
    Exit Function
    Resume Next
End Function



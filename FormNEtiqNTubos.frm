VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormNEtiqNTubos 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormNEtiqNTubos"
   ClientHeight    =   5760
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   6060
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5760
   ScaleWidth      =   6060
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcAuxAna 
      Height          =   285
      Left            =   2160
      TabIndex        =   11
      Top             =   5520
      Visible         =   0   'False
      Width           =   1530
   End
   Begin VB.Frame Frame3 
      Caption         =   "Impressora"
      Height          =   735
      Left            =   120
      TabIndex        =   9
      Top             =   4920
      Width           =   5895
      Begin VB.ComboBox CmbPrinters 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   240
         Width           =   5655
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Imprimir etiquetas para"
      Height          =   735
      Left            =   120
      TabIndex        =   7
      Top             =   4200
      Width           =   5895
      Begin VB.ComboBox CmbTipoImp 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   240
         Width           =   5655
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Detalhes das Etiquetas"
      Height          =   4095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5895
      Begin VB.CommandButton BtOkNetiq 
         Height          =   615
         Left            =   1440
         Picture         =   "FormNEtiqNTubos.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Confirmar Impress�o"
         Top             =   3345
         Width           =   975
      End
      Begin VB.CommandButton BtCancNetiq 
         Height          =   615
         Left            =   3120
         Picture         =   "FormNEtiqNTubos.frx":0CCA
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Cancelar Impress�o"
         Top             =   3360
         Width           =   975
      End
      Begin VB.CheckBox CkResumo 
         Caption         =   "Imprimir folha resumo da requisi��o"
         Height          =   195
         Left            =   1320
         TabIndex        =   4
         Top             =   2920
         Width           =   2895
      End
      Begin VB.TextBox EcAdm 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2520
         MaxLength       =   3
         TabIndex        =   2
         Tag             =   "131(3,0)"
         Text            =   "1"
         Top             =   2640
         Width           =   855
      End
      Begin MSFlexGridLib.MSFlexGrid FGTubos 
         Height          =   2055
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   5715
         _ExtentX        =   10081
         _ExtentY        =   3625
         _Version        =   393216
         BackColorBkg    =   -2147483633
         GridLines       =   2
         BorderStyle     =   0
      End
      Begin VB.Label Label1 
         Caption         =   "N� de etiquetas administrativas "
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   2640
         Width           =   2415
      End
   End
   Begin VB.Label LbDataConclusao 
      Height          =   255
      Left            =   1080
      TabIndex        =   12
      Top             =   5760
      Width           =   3735
   End
End
Attribute VB_Name = "FormNEtiqNTubos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Vari�veis Globais para este Form

Option Explicit



Dim estrutAnalises() As DadosAnalises
Dim TotalEstrutAnalises As Integer

Dim LastCol As Integer
Dim LastRow As Integer

'Vari�vel global recebida como parametros do Form Gest�o Requisi��o
Dim NumRequis As String

'Vari�vel de controlo de formata��es da grid de resultados
Dim Flg_ExecutaCodigoFGTubos As Boolean

'Vari�vel de controlo para limpar a grelha
Dim Limpa_Grelha_Combo As Boolean

Private Type DadosAnalises
    NumReq As String
    CodPerfil As String
    CodAnaC As String
    codAnaS As String
    codAgrup As String
    descrAna As String
End Type

Private Sub BtCancNetiq_Click()
    Unload Me
End Sub

Public Sub BtOkNetiq_Click()
    Dim arrReq() As Long
    ReDim arrReq(0)
    Imprime_Etiq
    'BRUNODSANTOS CHUC-7934 - 22.04.2016
    If gImprimirEtiqResumo = mediSim And gImprimeEtiqResumoEcraChegadaTubos = mediSim Then
        Call BL_ImprimeEtiqResumo(eTubos(), PassaParam, False, arrReq())
    End If
    '
    Unload Me
End Sub

 
Private Sub CmbTipoImp_Click()

Limpa_Grelha_Combo = True

    Select Case CmbTipoImp.ListIndex
        Case 0
            'Tubos e Administrativos
            Label1.Visible = True
            EcAdm.Visible = True
            'Activa Grelha
            FGTubos.Enabled = True
            'Limpa Resultados Anteriores
            LimpaEstrutura
            LimpaResultados
            'Carrega dados para a grelha
            TB_ProcuraTubos FGTubos, CLng(NumRequis), False
            AssociaInfoAnalisesAosTubos
            CarregaGrid
        Case 1
            'Administrativos
            Label1.Visible = True
            EcAdm.Visible = True
            'Neste caso desactiva grelha de copias por tubo
            LimpaEstrutura
            LimpaResultados
            'Desactiva grelha de copias por tubo
            FGTubos.Enabled = False
        Case 2
            'Tubos
            Label1.Visible = False
            EcAdm.Visible = False
            'Activa Grelha
            FGTubos.Enabled = True
            'Limpa Resultados Anteriores
            LimpaEstrutura
            LimpaResultados
            'Carrega dados para a grelha
            TB_ProcuraTubos FGTubos, CLng(NumRequis), False
            AssociaInfoAnalisesAosTubos
            CarregaGrid
    End Select
    
Limpa_Grelha_Combo = False

End Sub

Private Sub EcAdm_GotFocus()
    
    EcAdm.SelStart = 0
    EcAdm.SelLength = Len(EcAdm)

End Sub

Private Sub EcAdm_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, EcAdm

End Sub

Private Sub FGTubos_click()
    FGTubos_KeyPress vbKeyReturn
End Sub

Private Sub Form_Activate()
    DoEvents
    TB_ProcuraTubos FGTubos, CLng(NumRequis), False
    PreencheEstruturaAnalises NumRequis
    AssociaInfoAnalisesAosTubos
    CarregaGrid
    If gLAB = "CHVNG" Then
        EcAdm.SetFocus
    Else
        BtOkNetiq.SetFocus
    End If

End Sub

Private Sub Form_Load()
    Dim impress  As String
    Dim impress2  As String
    Dim Index As Long
    Dim i As Integer
    
    Inicializacoes
    
    DefTipoCampos
    
    If BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ResumoRequisicoes") = "1" Then
        CkResumo.Visible = True
        CkResumo.value = 0
    ElseIf BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ResumoRequisicoes") = "2" Then
        CkResumo.Visible = True
        CkResumo.value = 1
    Else
        CkResumo.Visible = False
        CkResumo.value = 0
    End If
    
    Me.caption = " Impress�o de Etiquetas"

    If gF_REQUIS = 1 Then
        LbDataConclusao.caption = "Data prevista de conclus�o: " & DevolveDataValidade(FormGestaoRequisicao.EcNumReq)
    End If

    EcAdm = "1"
    Index = mediComboValorNull
    If Printers.Count = 0 Then
        CmbPrinters.Enabled = False
    Else
        CmbPrinters.Clear
        For i = 0 To Printers.Count - 1
            If InStr(1, UCase(Printers(i).DeviceName), "IN SESSION") > 0 Then
                impress = Mid(Printers(i).DeviceName, 1, InStr(1, UCase(Printers(i).DeviceName), "IN SESSION") - 1)
            ElseIf InStr(1, UCase(Printers(i).DeviceName), "REDIRECTED") > 0 Then
                impress = Mid(Printers(i).DeviceName, 1, InStr(1, UCase(Printers(i).DeviceName), "REDIRECTED") - 1)
            Else
                impress = Printers(i).DeviceName
            End If

            CmbPrinters.AddItem Printers(i).DeviceName
            If gF_REQUIS = 1 Then
                If InStr(1, UCase(FormGestaoRequisicao.EcPrinterEtiq), "IN SESSION") > 0 Then
                    impress2 = Mid(FormGestaoRequisicao.EcPrinterEtiq, 1, InStr(1, UCase(FormGestaoRequisicao.EcPrinterEtiq), "IN SESSION") - 1)
                ElseIf InStr(1, UCase(FormGestaoRequisicao.EcPrinterEtiq), "REDIRECTED") > 0 Then
                    impress2 = Mid(FormGestaoRequisicao.EcPrinterEtiq, 1, InStr(1, UCase(FormGestaoRequisicao.EcPrinterEtiq), "REDIRECTED") - 1)
                Else
                    impress2 = FormGestaoRequisicao.EcPrinterEtiq
                End If
                
                If Trim(impress2) = Trim(impress) Then
                    Index = i
                End If
            ElseIf gF_REQCONS = 1 Then
                If InStr(1, UCase(FormGesReqCons.EcPrinterEtiq), "IN SESSION") > 0 Then
                    impress2 = Mid(FormGesReqCons.EcPrinterEtiq, 1, InStr(1, UCase(FormGesReqCons.EcPrinterEtiq), "IN SESSION") - 1)
                ElseIf InStr(1, UCase(FormGesReqCons.EcPrinterEtiq), "REDIRECTED") > 0 Then
                    impress2 = Mid(FormGesReqCons.EcPrinterEtiq, 1, InStr(1, UCase(FormGesReqCons.EcPrinterEtiq), "REDIRECTED") - 1)
                Else
                    impress2 = FormGesReqCons.EcPrinterEtiq
                End If

                If Trim(impress2) = Trim(impress) Then
                    Index = i
                End If
            ElseIf gF_FILA_ESPERA = 1 Then
                If InStr(1, UCase(FormFilaEspera.EcPrinterEtiq), "IN SESSION") > 0 Then
                    impress2 = Mid(FormFilaEspera.EcPrinterEtiq, 1, InStr(1, UCase(FormFilaEspera.EcPrinterEtiq), "IN SESSION") - 1)
                ElseIf InStr(1, UCase(FormFilaEspera.EcPrinterEtiq), "REDIRECTED") > 0 Then
                    impress2 = Mid(FormFilaEspera.EcPrinterEtiq, 1, InStr(1, UCase(FormFilaEspera.EcPrinterEtiq), "REDIRECTED") - 1)
                Else
                    impress2 = FormFilaEspera.EcPrinterEtiq
                End If
                If Trim(impress2) = Trim(impress) Then
                    Index = i
                End If
            ElseIf gF_REQUIS_PRIVADO = 1 Then
                If InStr(1, UCase(FormGestaoRequisicaoPrivado.EcPrinterEtiq), "IN SESSION") > 0 Then
                    impress2 = Mid(FormGestaoRequisicaoPrivado.EcPrinterEtiq, 1, InStr(1, UCase(FormGestaoRequisicaoPrivado.EcPrinterEtiq), "IN SESSION") - 1)
                ElseIf InStr(1, UCase(FormGestaoRequisicaoPrivado.EcPrinterEtiq), "REDIRECTED") > 0 Then
                    impress2 = Mid(FormGestaoRequisicaoPrivado.EcPrinterEtiq, 1, InStr(1, UCase(FormGestaoRequisicaoPrivado.EcPrinterEtiq), "REDIRECTED") - 1)
                Else
                    impress2 = FormGestaoRequisicaoPrivado.EcPrinterEtiq
                End If

                If Trim(impress2) = Trim(impress) Then
                    Index = i
                End If
            ElseIf gF_REQUIS_VET = 1 Then
                If InStr(1, UCase(FormGestaoRequisicaoVet.EcPrinterEtiq), "IN SESSION") > 0 Then
                    impress2 = Mid(FormGestaoRequisicaoVet.EcPrinterEtiq, 1, InStr(1, UCase(FormGestaoRequisicaoVet.EcPrinterEtiq), "IN SESSION") - 1)
                ElseIf InStr(1, UCase(FormGestaoRequisicaoVet.EcPrinterEtiq), "REDIRECTED") > 0 Then
                    impress2 = Mid(FormGestaoRequisicaoVet.EcPrinterEtiq, 1, InStr(1, UCase(FormGestaoRequisicaoVet.EcPrinterEtiq), "REDIRECTED") - 1)
                Else
                    impress2 = FormGestaoRequisicaoVet.EcPrinterEtiq
                End If

                If Trim(impress2) = Trim(impress) Then
                    Index = i
                End If
            ElseIf gF_REQUIS_AGUAS = 1 Then
                If InStr(1, UCase(FormGestaoRequisicaoAguas.EcPrinterEtiq), "IN SESSION") > 0 Then
                    impress2 = Mid(FormGestaoRequisicaoAguas.EcPrinterEtiq, 1, InStr(1, UCase(FormGestaoRequisicaoAguas.EcPrinterEtiq), "IN SESSION") - 1)
                ElseIf InStr(1, UCase(FormGestaoRequisicaoAguas.EcPrinterEtiq), "REDIRECTED") > 0 Then
                    impress2 = Mid(FormGestaoRequisicaoAguas.EcPrinterEtiq, 1, InStr(1, UCase(FormGestaoRequisicaoAguas.EcPrinterEtiq), "REDIRECTED") - 1)
                Else
                    impress2 = FormGestaoRequisicaoAguas.EcPrinterEtiq
                End If

                If Trim(impress2) = Trim(impress) Then
                    Index = i
                End If
            Else
                impress2 = BL_SelImpressora("Etiqueta.rpt")
                If Trim(impress2) = Trim(impress) Then
                    Index = i
                End If
            End If
        Next i
        CmbPrinters.ListIndex = Index
    End If

    CmbTipoImp.Clear
    CmbTipoImp.AddItem "Tubos e fins administrativos"
    CmbTipoImp.AddItem "Fins administrativos"
    CmbTipoImp.AddItem "Tubos"
    CmbTipoImp.ListIndex = 0
    
    EcAdm.text = 1
    If gLAB = "CHVNG" Then
        EcAdm.text = (FGTubos.rows - 2)
    ElseIf gNumEtiqAdmin > -1 Then
        EcAdm.text = gNumEtiqAdmin
    End If

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    
    BG_LimpaPassaParams
    
    If UnloadMode = 0 Then
        EcAdm = "-1"
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

LimpaEstrutura
LimpaResultados

        If gF_REQUIS = 1 Then
            FormGestaoRequisicao.EcEtqTipo = CmbTipoImp.ListIndex
        ElseIf gF_REQCONS = 1 Then
            FormGesReqCons.EcEtqTipo = CmbTipoImp.ListIndex
        ElseIf gF_FILA_ESPERA = 1 Then
            FormFilaEspera.EcEtqTipo = CmbTipoImp.ListIndex
        ElseIf gF_REQUIS_PRIVADO = 1 Then
            FormGestaoRequisicaoPrivado.EcEtqTipo = CmbTipoImp.ListIndex
        ElseIf gF_REQUIS_VET = 1 Then
            FormGestaoRequisicaoVet.EcEtqTipo = CmbTipoImp.ListIndex
        ElseIf gF_REQUIS_AGUAS = 1 Then
            FormGestaoRequisicaoAguas.EcEtqTipo = CmbTipoImp.ListIndex
        End If
        If CmbPrinters.ListIndex <> -1 Then
            If gF_REQUIS = 1 Then
                FormGestaoRequisicao.EcPrinterEtiq = CmbPrinters.text
            ElseIf gF_REQCONS = 1 Then
                FormGesReqCons.EcPrinterEtiq = CmbPrinters.text
            ElseIf gF_FILA_ESPERA = 1 Then
                FormFilaEspera.EcPrinterEtiq = CmbPrinters.text
            ElseIf gF_REQUIS_PRIVADO = 1 Then
                FormGestaoRequisicaoPrivado.EcPrinterEtiq = CmbPrinters.text
            ElseIf gF_REQUIS_VET = 1 Then
                FormGestaoRequisicaoVet.EcPrinterEtiq = CmbPrinters.text
            ElseIf gF_REQUIS_AGUAS = 1 Then
                FormGestaoRequisicaoAguas.EcPrinterEtiq = CmbPrinters.text
            End If
        End If
    
End Sub


Private Function DevolveDataValidade(n_req As String) As String
    Dim sql As String
    Dim rs As New ADODB.recordset

    BL_VerificaConclusaoRequisicao n_req, Bg_DaData_ADO

    '----------------------------------
    ' DATA VALIDADE ACTUAL
    sql = "SELECT dt_conclusao FROM sl_requis WHERE n_req = " & n_req
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    If rs.RecordCount > 0 Then
        DevolveDataValidade = BL_HandleNull(rs!dt_conclusao, CDate(1))
    End If
    rs.Close
    Set rs = Nothing
    '----------------------------------

End Function

Sub Inicializacoes()

    Me.left = 3200
    Me.top = 900
    
    Flg_ExecutaCodigoFGTubos = True
    LastCol = 0
    LastRow = 0

    
    'Carrega parametros passado do form que pediu a impress�o
    NumRequis = gPassaParams.Param(0)
'    Public arrReq() As Long
'    Me.arrReq() = FormChegadaPorTubosV2.arrReq
    '
End Sub

Sub DefTipoCampos()
    
    'FlexGrid numero de etiquetas por tubo
    With FGTubos
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0

        .ColWidth(0) = 3850
        .Col = 0
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 0) = "Descri��o do Tubo"

        .ColWidth(1) = 1480
        .Col = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 1) = "N�. Etiqueta(s)"

        .row = 1
        .Col = 1
        
        .CellBackColor = vbWhite
    End With
    ReDim reqColh(0)
    TotalReqColh = 0
End Sub
Sub LimpaEstrutura()
    ReDim Preserve gEstruturaTubos(0)
    gTotalTubos = 0
End Sub
Sub LimpaEstruturaAnalises()
    ReDim Preserve estrutAnalises(0)
    TotalEstrutAnalises = 0
End Sub
Private Sub FGTubos_RowColChange()

If Flg_ExecutaCodigoFGTubos = True Then

    Flg_ExecutaCodigoFGTubos = False
         
    If FGTubos.Col = 0 Then
       FGTubos.Col = 1
    End If
        
    MudaCorFundo
        
    LastRow = FGTubos.row
    LastCol = FGTubos.Col
    
    Flg_ExecutaCodigoFGTubos = True
    
End If

End Sub
Private Sub FGTubos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        If FGTubos.Col = 1 Then
            'EcAuxAna.text = FGTubos.TextMatrix(FGTubos.row, FGTubos.col)
            EcAuxAna.left = FGTubos.CellLeft + 245
            EcAuxAna.top = FGTubos.CellTop + 500
            EcAuxAna.Width = FGTubos.CellWidth
            EcAuxAna.Visible = True
            If FGTubos.TextMatrix(FGTubos.row, FGTubos.Col) <> "" Then
                EcAuxAna.text = FGTubos.TextMatrix(FGTubos.row, FGTubos.Col)
                EcAuxAna.SelStart = Len(EcAuxAna.text)
            End If
            EcAuxAna.Enabled = True
            EcAuxAna.SetFocus
            EcAuxAna.SelStart = 0
            EcAuxAna.SelLength = Len(EcAuxAna)
        End If
    Else
        EcAuxAna.SelLength = 0
    End If
End Sub
Sub MudaCorFundo()

    Dim tmpCol As Integer
    Dim tmpRow As Integer
    
    tmpCol = FGTubos.Col
    tmpRow = FGTubos.row
    
    FGTubos.Col = LastCol
    FGTubos.row = LastRow
    If FGTubos.row <> 0 And FGTubos.CellBackColor = vbCyan Then
        FGTubos.CellBackColor = vbWhite
    End If
    
    FGTubos.Col = tmpCol
    FGTubos.row = tmpRow
    
    If FGTubos.row <> 0 And FGTubos.CellBackColor <> vbCyan Then
        FGTubos.CellBackColor = vbCyan
    End If
    
End Sub
Private Sub EcAuxAna_Change()

    EcAuxAna.SelStart = Len(EcAuxAna.text)
    
End Sub
Public Sub EcAuxAna_KeyDown(KeyCode As Integer, Shift As Integer)

   If (KeyCode = 13 And Shift = 0) Then
                FGTubos.TextMatrix(FGTubos.row, FGTubos.Col) = BL_HandleNull(EcAuxAna.text, 0)
                gEstruturaTubos(FGTubos.row).num_copias = BL_HandleNull(EcAuxAna.text, 0)
                EcAuxAna.text = ""
                EcAuxAna.Visible = False
                FGTubos.SetFocus
    Else
        If IsNumeric(Chr(KeyCode)) Or (KeyCode = vbKeyBack) Or (KeyCode = vbKeyDelete) Or (KeyCode = vbKeyDecimal) Then
            EcAuxAna.SetFocus
        ElseIf KeyCode = vbKeyNumpad0 Or KeyCode = vbKeyNumpad1 Or KeyCode = vbKeyNumpad2 Or KeyCode = vbKeyNumpad3 Or KeyCode = vbKeyNumpad4 _
               Or KeyCode = vbKeyNumpad5 Or KeyCode = vbKeyNumpad6 Or KeyCode = vbKeyNumpad7 Or KeyCode = vbKeyNumpad8 Or _
               KeyCode = vbKeyNumpad9 Then
            EcAuxAna.SetFocus
        Else
            BG_Mensagem mediMsgBox, "PF usar apenas o teclado num�rico!", vbExclamation, App.ProductName
            EcAuxAna.text = ""
        End If

    End If

End Sub
Private Sub EcAuxAna_LostFocus()

    EcAuxAna.Visible = False
    If FGTubos.Enabled = True Then FGTubos.SetFocus

End Sub

Private Sub MudaCorFixo(linha As Integer, coluna As Integer)
    
    FGTubos.Col = coluna
    FGTubos.row = linha
    FGTubos.CellBackColor = &HC0FFFF
    
End Sub
Sub LimpaResultados()

    Dim i As Integer
    Dim j As Integer

    If (gTotalTubos > 0) Then
        For i = 1 To gTotalTubos
            gEstruturaTubos(i).num_copias = 0
        Next i
    'Eliminar as linhas - excepto a �ltima
        For j = FGTubos.rows To 3 Step -1
            FGTubos.RemoveItem (j - 1)
        Next j
    End If
    
    'Caso haja altera��es na combo temos de limpar a grid
    If (Limpa_Grelha_Combo = True) Then
        For i = 1 To FGTubos.rows - 1
            FGTubos.TextMatrix(i, 1) = ""
            FGTubos.TextMatrix(i, 0) = ""
        Next i
    'Eliminar as linhas - excepto a �ltima
        For j = FGTubos.rows To 3 Step -1
            FGTubos.RemoveItem (j - 1)
        Next j
    End If
    
End Sub

' pferreira 2010.08.10
' Selecciona campos "abrev_ute" da tabela sl_identif.
Sub Imprime_Etiq()
    On Error GoTo TrataErro
    Dim i As Integer
    
        
    ' -------------------------------------------------------------------------------------------
    ' ETIQUETAS TUBOS
    ' -------------------------------------------------------------------------------------------
    If CmbTipoImp.ListIndex <> 1 Then
        For i = 1 To gTotalTubos
            TB_ImprimeEtiqueta NumRequis, i, mediComboValorNull, "", CmbPrinters.text
         Next i
    End If

    ' -------------------------------------------------------------------------------------------
    ' ETIQUETAS ADMINISTRATIVAS
    ' -------------------------------------------------------------------------------------------
    If CmbTipoImp.ListIndex <> 2 Then
        TB_ImprimeEtiqueta NumRequis, mediComboValorNull, BL_HandleNull(EcAdm, 1), "", CmbPrinters.text
    End If



'    Emanuel Sousa - Tratar Resumo
    If CkResumo.value = vbChecked Then
        ImprimeResumo
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeEtiq: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeEtiq", True
    Exit Sub
    Resume Next
End Sub

Sub PreencheEstruturaAnalises(nRequis As String)
    Dim sql As String
    Dim i As Integer
    Dim rsAnalises As ADODB.recordset
                
'Extrair os dados das analises dada a requisi��o
    sql = "select n_req, cod_perfil, cod_ana_c, cod_ana_s, cod_agrup, descr_ana "
    sql = sql & " FROM sl_marcacoes, slv_analises where n_req =" & nRequis & "  AND sl_marcacoes.cod_Agrup = slv_Analises.cod_ana"
    sql = sql & " UNION select n_req, cod_perfil, cod_ana_c, cod_ana_s, cod_agrup, descr_ana "
    sql = sql & " FROM sl_realiza, slv_analises where n_req =" & nRequis & "  AND sl_Realiza.cod_Agrup = slv_Analises.cod_ana"
    
'Carregar sql para o RecordSet dos tubos
    Set rsAnalises = New ADODB.recordset
    rsAnalises.CursorLocation = adUseServer
    rsAnalises.CursorType = adOpenStatic
    rsAnalises.Open sql, gConexao

'Limpar Resultados e Grid
    LimpaEstruturaAnalises
    
'Preeenche estrutura
    If rsAnalises.RecordCount > 0 Then
    i = 1
        While Not rsAnalises.EOF
            
            TotalEstrutAnalises = TotalEstrutAnalises + 1
            ReDim Preserve estrutAnalises(TotalEstrutAnalises)

            estrutAnalises(TotalEstrutAnalises).NumReq = nRequis
            estrutAnalises(TotalEstrutAnalises).CodAnaC = BL_HandleNull(rsAnalises!cod_ana_c, "")
            estrutAnalises(TotalEstrutAnalises).codAnaS = BL_HandleNull(rsAnalises!cod_ana_s, "")
            estrutAnalises(TotalEstrutAnalises).CodPerfil = BL_HandleNull(rsAnalises!Cod_Perfil, "")
            estrutAnalises(TotalEstrutAnalises).codAgrup = BL_HandleNull(rsAnalises!cod_agrup, "")
            estrutAnalises(TotalEstrutAnalises).descrAna = BL_HandleNull(rsAnalises!descr_ana, "")
            
            i = i + 1
            
            rsAnalises.MoveNext
        Wend
        
    End If
    
    'Fecha RS das analises sem Res
    rsAnalises.Close
    Set rsAnalises = Nothing
    
End Sub
Sub AssociaInfoAnalisesAosTubos()
On Error GoTo TrataErro
    Dim sqlAnaPerfis As String
    Dim sqlAnaSimples As String
    Dim sqlAnaComplexas As String
    Dim i As Integer
    Dim i1, i2, i3, i4, i5, i6 As Integer
    Dim iAnaSimples As Integer
    Dim iAnaComplexas As Integer
    Dim iAnaPerfis As Integer
    Dim RsInfoAnaTuboComplexas As ADODB.recordset
    Dim RsInfoAnaTuboSimples As ADODB.recordset
    Dim RsInfoAnaTuboPerfis As ADODB.recordset
    Dim num_ana As Integer
                     
        For i = 1 To TotalEstrutAnalises
        'Procurar informa��o nos Perfis de Analises
            If (estrutAnalises(i).CodPerfil <> "0") Then
                'Extrair a informa��o dos perfis
                sqlAnaPerfis = "select ANA.cod_perfis cod_ana_s,ana.cod_produto, ana.abr_ana_p abr_ana_s, grupo_ana.descr_gr_ana, tubo.cod_etiq, " & _
                                            " ana.cod_tubo, '' qt_min, tubo.capaci_util, " & _
                                            " tubo.descr_tubo tubo_descr_tubo, '' flg_etiq_prod, '' flg_etiq_ord, '' flg_etiq_abr, inf_complementar,num_copias, num_max_ana, TUBO.NORMA_COLHEITA  " & _
                                            " from sl_perfis ana, sl_tubo tubo, sl_gr_ana grupo_ana " & _
                                            " where ana.gr_ana = grupo_ana.cod_gr_ana(+) and ana.cod_tubo = tubo.cod_tubo and ana.cod_perfis = " & BL_TrataStringParaBD(estrutAnalises(i).CodPerfil) & ""
                
                'Carregar sql para o RecordSet correspondente
                        Set RsInfoAnaTuboPerfis = New ADODB.recordset
                        RsInfoAnaTuboPerfis.CursorLocation = adUseServer
                        RsInfoAnaTuboPerfis.CursorType = adOpenStatic
                        RsInfoAnaTuboPerfis.Open sqlAnaPerfis, gConexao
                               
                    'Adiciona a informa��o � estrutura de tubos
                        If RsInfoAnaTuboPerfis.RecordCount > 0 Then
                        iAnaPerfis = 1
                            While Not RsInfoAnaTuboPerfis.EOF
                                For i1 = 1 To gTotalTubos
                                    If (gEstruturaTubos(i1).CodTubo = RsInfoAnaTuboPerfis!cod_tubo) Then
                                            'verifica se j� tinha informa��o para controlar a coloca��o da virgula
                                            If (gEstruturaTubos(i1).CodProd = "") Then
                                                gEstruturaTubos(i1).CodProd = BL_HandleNull(RsInfoAnaTuboPerfis!cod_produto, "")
                                            Else
                                                gEstruturaTubos(i1).CodProd = gEstruturaTubos(i1).CodProd & "," & BL_HandleNull(RsInfoAnaTuboPerfis!cod_produto, "")
                                            End If
                                            If (gEstruturaTubos(i1).GrAna = "") Then
                                                gEstruturaTubos(i1).GrAna = BL_HandleNull(RsInfoAnaTuboPerfis!descr_gr_ana, "")
                                            Else
                                                gEstruturaTubos(i1).GrAna = gEstruturaTubos(i1).GrAna & "," & BL_HandleNull(RsInfoAnaTuboPerfis!descr_gr_ana, "")
                                            End If
                                            
                                            gEstruturaTubos(i1).num_ana = gEstruturaTubos(i1).num_ana + 1
                                            'RGONCALVES 15.12.2014 - multiplicar por 'num_copias_original' em vez de 'num_copias'
                                            gEstruturaTubos(i1).num_copias = BL_ArredondaCima(gEstruturaTubos(i1).num_ana / gEstruturaTubos(i1).num_max_ana) * gEstruturaTubos(i1).num_copias_original
                                            'imprimir as abreviaturas das analises do tubo
                                            If BL_HandleNull(RsInfoAnaTuboPerfis!flg_etiq_abr, "") = 1 And InStr(gEstruturaTubos(i1).abrAna, BL_HandleNull(RsInfoAnaTuboPerfis!abr_ana_s, "...")) = 0 Then
                                                gEstruturaTubos(i1).abrAna = gEstruturaTubos(i1).abrAna & "," & BL_HandleNull(RsInfoAnaTuboPerfis!abr_ana_s, "")
                                            End If
                                            
                                            If InStr(1, gEstruturaTubos(i1).Descr_etiq_ana, BL_RetornaDescrEtiqAna(BL_HandleNull(RsInfoAnaTuboPerfis!cod_ana_s, ""))) = 0 Then
                                                gEstruturaTubos(i1).Descr_etiq_ana = gEstruturaTubos(i1).Descr_etiq_ana & BL_RetornaDescrEtiqAna(BL_HandleNull(RsInfoAnaTuboPerfis!cod_ana_s, ""))
                                            End If
                                        Exit For
                                    End If
                                Next
                            
                                iAnaPerfis = iAnaPerfis + 1
                                
                                RsInfoAnaTuboPerfis.MoveNext
                            Wend
                        
                        'Fecha RS
                        RsInfoAnaTuboPerfis.Close
                        Set RsInfoAnaTuboPerfis = Nothing
                        
                        'Caso os perfis n�o tenham informa��o vai procurar nas complexas
                        ElseIf (RsInfoAnaTuboPerfis.RecordCount <= 0) Then
                            'Extrair a informa��o das analises complexas
                            sqlAnaComplexas = "select ANA.cod_ana_c cod_ana_s, ana.cod_produto, ana.abr_ana_c abr_ana_s, grupo_ana.descr_gr_ana, tubo.cod_etiq, " & _
                                                " ana.cod_tubo, '' qt_min, tubo.capaci_util, " & _
                                                " tubo.descr_tubo tubo_descr_tubo, '' flg_etiq_prod, '' flg_etiq_ord, flg_etiq_abr, inf_complementar, num_copias, num_max_ana, TUBO.NORMA_COLHEITA  " & _
                                                " from sl_ana_c ana, sl_tubo tubo, sl_gr_ana grupo_ana " & _
                                                " where ana.gr_ana = grupo_ana.cod_gr_ana(+) and ana.cod_tubo = tubo.cod_tubo and ana.cod_ana_c = " & BL_TrataStringParaBD(estrutAnalises(i).CodAnaC) & ""
                        
                            'Carregar sql para o RecordSet correspondente
                            Set RsInfoAnaTuboComplexas = New ADODB.recordset
                            RsInfoAnaTuboComplexas.CursorLocation = adUseServer
                            RsInfoAnaTuboComplexas.CursorType = adOpenStatic
                            RsInfoAnaTuboComplexas.Open sqlAnaComplexas, gConexao
                               
                            'Adiciona a informa��o � estrutura de tubos
                            If RsInfoAnaTuboComplexas.RecordCount > 0 Then
                                iAnaComplexas = 1
                                While Not RsInfoAnaTuboComplexas.EOF
                                
                                    'verifica se j� tinha informa��o para controlar a coloca��o da virgula
                                  For i2 = 1 To gTotalTubos
                                    If (gEstruturaTubos(i2).CodTubo = RsInfoAnaTuboComplexas!cod_tubo) Then
                                            'verifica se j� tinha informa��o para controlar a coloca��o da virgula
                                            If (gEstruturaTubos(i2).CodProd = "") Then
                                                gEstruturaTubos(i2).CodProd = BL_HandleNull(RsInfoAnaTuboComplexas!cod_produto, "")
                                            Else
                                                gEstruturaTubos(i2).CodProd = gEstruturaTubos(i2).CodProd & "," & BL_HandleNull(RsInfoAnaTuboComplexas!cod_produto, "")
                                            End If
                                            If (gEstruturaTubos(i2).GrAna = "") Then
                                                gEstruturaTubos(i2).GrAna = BL_HandleNull(RsInfoAnaTuboComplexas!descr_gr_ana, "")
                                            Else
                                                gEstruturaTubos(i2).GrAna = gEstruturaTubos(i2).GrAna & "," & BL_HandleNull(RsInfoAnaTuboComplexas!descr_gr_ana, "")
                                            End If
                                            
                                            gEstruturaTubos(i2).num_ana = gEstruturaTubos(i2).num_ana + 1
                                            'RGONCALVES 15.12.2014 - multiplicar por 'num_copias_original' em vez de 'num_copias'
                                            gEstruturaTubos(i2).num_copias = BL_ArredondaCima(gEstruturaTubos(i2).num_ana / gEstruturaTubos(i2).num_max_ana) * gEstruturaTubos(i2).num_copias_original
                                            'imprimir as abreviaturas das analises do tubo
                                            If BL_HandleNull(RsInfoAnaTuboComplexas!flg_etiq_abr, "") = 1 And InStr(gEstruturaTubos(i2).abrAna, BL_HandleNull(RsInfoAnaTuboComplexas!abr_ana_s, "...")) = 0 Then
                                                gEstruturaTubos(i2).abrAna = gEstruturaTubos(i2).abrAna & "," & BL_HandleNull(RsInfoAnaTuboComplexas!abr_ana_s, "")
                                            End If
                                            
                                            If InStr(1, gEstruturaTubos(i2).Descr_etiq_ana, BL_RetornaDescrEtiqAna(BL_HandleNull(RsInfoAnaTuboComplexas!cod_ana_s, ""))) = 0 Then
                                                gEstruturaTubos(i2).Descr_etiq_ana = gEstruturaTubos(i2).Descr_etiq_ana & BL_RetornaDescrEtiqAna(BL_HandleNull(RsInfoAnaTuboComplexas!cod_ana_s, ""))
                                            End If
                                        Exit For
                                    End If
                                Next
                                
                                    iAnaComplexas = iAnaComplexas + 1
                                
                                    RsInfoAnaTuboComplexas.MoveNext
                                Wend
                            End If
                        
                            'Fecha RS
                            RsInfoAnaTuboComplexas.Close
                            Set RsInfoAnaTuboComplexas = Nothing
                        'Caso n�o tenha encontrado informa��o nas Complexas procura nas Simples
                        Else
                            'Extrair a informa��o das analises simples
                            sqlAnaSimples = "SELECT ANA.COD_ANA_S, ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, INF_COMPLEMENTAR, num_copias, num_max_ana, TUBO.NORMA_COLHEITA, ANA.descr_etiq  " & _
                                  " FROM  SL_ANA_S ANA, SL_GR_ANA GRUPO_ANA, SL_TUBO TUBO " & _
                                  " WHERE ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA (+) " & _
                                  " AND   ANA.COD_TUBO = TUBO.COD_TUBO " & _
                                  " AND   ANA.COD_ANA_S = " & BL_TrataStringParaBD(estrutAnalises(i).codAnaS) & ""
                        
                            'Carregar sql para o RecordSet correspondente
                                Set RsInfoAnaTuboSimples = New ADODB.recordset
                                RsInfoAnaTuboSimples.CursorLocation = adUseServer
                                RsInfoAnaTuboSimples.CursorType = adOpenStatic
                                RsInfoAnaTuboSimples.Open sqlAnaSimples, gConexao
                               
                            'Adiciona a informa��o � estrutura de tubos
                                If RsInfoAnaTuboSimples.RecordCount > 0 Then
                                    iAnaSimples = 1
                                        While Not RsInfoAnaTuboSimples.EOF
                                
                                                    For i3 = 1 To gTotalTubos
                                                        If (gEstruturaTubos(i3).CodTubo = RsInfoAnaTuboSimples!cod_tubo) Then
                                                                'verifica se j� tinha informa��o para controlar a coloca��o da virgula
                                                                If (gEstruturaTubos(i3).CodProd = "") Then
                                                                    gEstruturaTubos(i3).CodProd = BL_HandleNull(RsInfoAnaTuboSimples!cod_produto, "")
                                                                Else
                                                                    gEstruturaTubos(i3).CodProd = gEstruturaTubos(i3).CodProd & "," & BL_HandleNull(RsInfoAnaTuboSimples!cod_produto, "")
                                                                End If
                                                                If (gEstruturaTubos(i3).GrAna = "") Then
                                                                    gEstruturaTubos(i3).GrAna = BL_HandleNull(RsInfoAnaTuboSimples!descr_gr_ana, "")
                                                                Else
                                                                    gEstruturaTubos(i3).GrAna = gEstruturaTubos(i3).GrAna & "," & BL_HandleNull(RsInfoAnaTuboSimples!descr_gr_ana, "")
                                                                End If
                                                                
                                                                gEstruturaTubos(i3).num_ana = gEstruturaTubos(i3).num_ana + 1
                                                                'RGONCALVES 15.12.2014 - multiplicar por 'num_copias_original' em vez de 'num_copias'
                                                                gEstruturaTubos(i3).num_copias = BL_ArredondaCima(gEstruturaTubos(i3).num_ana / gEstruturaTubos(i3).num_max_ana) * gEstruturaTubos(i3).num_copias_original
                                                                'imprimir as abreviaturas das analises do tubo
                                                                If BL_HandleNull(RsInfoAnaTuboSimples!flg_etiq_abr, "") = 1 And InStr(gEstruturaTubos(i3).abrAna, BL_HandleNull(RsInfoAnaTuboSimples!abr_ana_s, "...")) = 0 Then
                                                                    gEstruturaTubos(i3).abrAna = gEstruturaTubos(i3).abrAna & "," & BL_HandleNull(RsInfoAnaTuboSimples!abr_ana_s, "")
                                                                End If
                                                            
                                                                If InStr(1, gEstruturaTubos(i3).Descr_etiq_ana, BL_HandleNull(RsInfoAnaTuboSimples!descr_etiq, "")) = 0 Then
                                                                    gEstruturaTubos(i3).Descr_etiq_ana = gEstruturaTubos(i3).Descr_etiq_ana & BL_HandleNull(RsInfoAnaTuboSimples!descr_etiq, "")
                                                                End If
                                                            Exit For
                                                        End If
                                                    Next
                                                                        
                                            iAnaSimples = iAnaSimples + 1
                                
                                            RsInfoAnaTuboSimples.MoveNext
                                        Wend
                                End If
                        
                            'Fecha RS
                            RsInfoAnaTuboSimples.Close
                            Set RsInfoAnaTuboSimples = Nothing
                            End If
        ElseIf (estrutAnalises(i).CodAnaC <> "0") Then
            'Extrair a informa��o das analises complexas
                    sqlAnaComplexas = "select ANA.cod_ana_c cod_ana_s, ana.cod_produto, ana.abr_ana_c abr_ana_s, grupo_ana.descr_gr_ana, tubo.cod_etiq, " & _
                                            " ana.cod_tubo, '' qt_min, tubo.capaci_util, " & _
                                            " tubo.descr_tubo tubo_descr_tubo, '' flg_etiq_prod, '' flg_etiq_ord, flg_etiq_abr, inf_complementar, num_copias, num_max_ana, TUBO.NORMA_COLHEITA  " & _
                                            " from sl_ana_c ana, sl_tubo tubo, sl_gr_ana grupo_ana " & _
                                            " where ana.gr_ana = grupo_ana.cod_gr_ana(+) and ana.cod_tubo = tubo.cod_tubo and ana.cod_ana_c = " & BL_TrataStringParaBD(estrutAnalises(i).CodAnaC) & ""
                        
                    'Carregar sql para o RecordSet correspondente
                        Set RsInfoAnaTuboComplexas = New ADODB.recordset
                        RsInfoAnaTuboComplexas.CursorLocation = adUseServer
                        RsInfoAnaTuboComplexas.CursorType = adOpenStatic
                        RsInfoAnaTuboComplexas.Open sqlAnaComplexas, gConexao
                               
                    'Adiciona a informa��o � estrutura de tubos
                        If RsInfoAnaTuboComplexas.RecordCount > 0 Then
                        iAnaComplexas = 1
                            While Not RsInfoAnaTuboComplexas.EOF
                                
                                For i4 = 1 To gTotalTubos
                                    If (gEstruturaTubos(i4).CodTubo = RsInfoAnaTuboComplexas!cod_tubo) Then
                                            'verifica se j� tinha informa��o para controlar a coloca��o da virgula
                                            If (gEstruturaTubos(i4).CodProd = "") Then
                                                gEstruturaTubos(i4).CodProd = BL_HandleNull(RsInfoAnaTuboComplexas!cod_produto, "")
                                            Else
                                                gEstruturaTubos(i4).CodProd = gEstruturaTubos(i4).CodProd & "," & BL_HandleNull(RsInfoAnaTuboComplexas!cod_produto, "")
                                            End If
                                            If (gEstruturaTubos(i4).GrAna = "") Then
                                                gEstruturaTubos(i4).GrAna = BL_HandleNull(RsInfoAnaTuboComplexas!descr_gr_ana, "")
                                            Else
                                                gEstruturaTubos(i4).GrAna = gEstruturaTubos(i4).GrAna & "," & BL_HandleNull(RsInfoAnaTuboComplexas!descr_gr_ana, "")
                                            End If
                                            
                                            gEstruturaTubos(i4).num_ana = gEstruturaTubos(i4).num_ana + 1
                                            'RGONCALVES 15.12.2014 - multiplicar por 'num_copias_original' em vez de 'num_copias'
                                            gEstruturaTubos(i4).num_copias = BL_ArredondaCima(gEstruturaTubos(i4).num_ana / gEstruturaTubos(i4).num_max_ana) * gEstruturaTubos(i4).num_copias_original
                                            'imprimir as abreviaturas das analises do tubo
                                            If BL_HandleNull(RsInfoAnaTuboComplexas!flg_etiq_abr, "") = 1 And InStr(gEstruturaTubos(i4).abrAna, BL_HandleNull(RsInfoAnaTuboComplexas!abr_ana_s, "...")) = 0 Then
                                                gEstruturaTubos(i4).abrAna = gEstruturaTubos(i4).abrAna & "," & BL_HandleNull(RsInfoAnaTuboComplexas!abr_ana_s, "")
                                            End If
                                            
                                            If InStr(1, gEstruturaTubos(i4).Descr_etiq_ana, BL_RetornaDescrEtiqAna(BL_HandleNull(RsInfoAnaTuboComplexas!cod_ana_s, ""))) = 0 Then
                                                gEstruturaTubos(i4).Descr_etiq_ana = gEstruturaTubos(i4).Descr_etiq_ana & BL_RetornaDescrEtiqAna(BL_HandleNull(RsInfoAnaTuboComplexas!cod_ana_s, ""))
                                            End If
                                        Exit For
                                    End If
                                Next
                                
                                iAnaComplexas = iAnaComplexas + 1
                                
                                RsInfoAnaTuboComplexas.MoveNext
                            Wend
                            
                        'Fecha RS
                        RsInfoAnaTuboComplexas.Close
                        Set RsInfoAnaTuboComplexas = Nothing

                        Else
                        'Procura dados nas simples
                            'Extrair a informa��o das analises simples
                            sqlAnaSimples = "SELECT ANA.cod_ana_s, ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, INF_COMPLEMENTAR, num_copias, num_max_ana, TUBO.NORMA_COLHEITA, ANA.descr_etiq  " & _
                                  " FROM  SL_ANA_S ANA, SL_GR_ANA GRUPO_ANA, SL_TUBO TUBO " & _
                                  " WHERE ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA (+) " & _
                                  " AND   ANA.COD_TUBO = TUBO.COD_TUBO " & _
                                  " AND   ANA.COD_ANA_S = " & BL_TrataStringParaBD(estrutAnalises(i).codAnaS) & ""
                        
                            'Carregar sql para o RecordSet correspondente
                            Set RsInfoAnaTuboSimples = New ADODB.recordset
                            RsInfoAnaTuboSimples.CursorLocation = adUseServer
                            RsInfoAnaTuboSimples.CursorType = adOpenStatic
                            RsInfoAnaTuboSimples.Open sqlAnaSimples, gConexao
                               
                        'Adiciona a informa��o � estrutura de tubos
                            If RsInfoAnaTuboSimples.RecordCount > 0 Then
                            iAnaSimples = 1
                                While Not RsInfoAnaTuboSimples.EOF
                                
                                    For i5 = 1 To gTotalTubos
                                    If (gEstruturaTubos(i5).CodTubo = RsInfoAnaTuboSimples!cod_tubo) Then
                                            'verifica se j� tinha informa��o para controlar a coloca��o da virgula
                                            If (gEstruturaTubos(i5).CodProd = "") Then
                                                gEstruturaTubos(i5).CodProd = BL_HandleNull(RsInfoAnaTuboSimples!cod_produto, "")
                                            Else
                                                gEstruturaTubos(i5).CodProd = gEstruturaTubos(i5).CodProd & "," & BL_HandleNull(RsInfoAnaTuboSimples!cod_produto, "")
                                            End If
                                            If (gEstruturaTubos(i5).GrAna = "") Then
                                                gEstruturaTubos(i5).GrAna = BL_HandleNull(RsInfoAnaTuboSimples!descr_gr_ana, "")
                                            Else
                                                gEstruturaTubos(i5).GrAna = gEstruturaTubos(i5).GrAna & "," & BL_HandleNull(RsInfoAnaTuboSimples!descr_gr_ana, "")
                                            End If
                                            
                                            gEstruturaTubos(i5).num_ana = gEstruturaTubos(i5).num_ana + 1
                                            'RGONCALVES 15.12.2014 - multiplicar por 'num_copias_original' em vez de 'num_copias'
                                            gEstruturaTubos(i5).num_copias = BL_ArredondaCima(gEstruturaTubos(i5).num_ana / gEstruturaTubos(i5).num_max_ana) * gEstruturaTubos(i5).num_copias_original
                                            'imprimir as abreviaturas das analises do tubo
                                            If BL_HandleNull(RsInfoAnaTuboSimples!flg_etiq_abr, "") = 1 And InStr(gEstruturaTubos(i5).abrAna, BL_HandleNull(RsInfoAnaTuboSimples!abr_ana_s, "...")) = 0 Then
                                                gEstruturaTubos(i5).abrAna = gEstruturaTubos(i5).abrAna & "," & BL_HandleNull(RsInfoAnaTuboSimples!abr_ana_s, "")
                                            End If
                                            
                                            If InStr(1, gEstruturaTubos(i5).Descr_etiq_ana, BL_HandleNull(RsInfoAnaTuboSimples!descr_etiq, "")) = 0 Then
                                                gEstruturaTubos(i5).Descr_etiq_ana = gEstruturaTubos(i5).Descr_etiq_ana & BL_HandleNull(RsInfoAnaTuboSimples!descr_etiq, "")
                                            End If
                                        Exit For
                                    End If
                                Next
                                
                                    iAnaSimples = iAnaSimples + 1
                                
                                    RsInfoAnaTuboSimples.MoveNext
                                Wend
                            End If
                        
                        'Fecha RS
                        RsInfoAnaTuboSimples.Close
                        Set RsInfoAnaTuboSimples = Nothing
                    End If
                        
            Else
            'Extrair a informa��o das analises simples
                    sqlAnaSimples = "SELECT ANA.cod_ana_s, ANA.COD_PRODUTO, ANA.ABR_ANA_S, GRUPO_ANA.DESCR_GR_ANA, TUBO.COD_ETIQ, ANA.COD_TUBO, ANA.QT_MIN," & _
                                  " TUBO.CAPACI_UTIL, TUBO.DESCR_TUBO TUBO_DESCR_TUBO, ANA.FLG_ETIQ_PROD, ANA.FLG_ETIQ_ORD, ANA.FLG_ETIQ_ABR, INF_COMPLEMENTAR, num_copias, num_max_ana, TUBO.NORMA_COLHEITA, ANA.descr_etiq  " & _
                                  " FROM  SL_ANA_S ANA, SL_GR_ANA GRUPO_ANA, SL_TUBO TUBO " & _
                                  " WHERE ANA.GR_ANA = GRUPO_ANA.COD_GR_ANA (+) " & _
                                  " AND   ANA.COD_TUBO = TUBO.COD_TUBO " & _
                                  " AND   ANA.COD_ANA_S = " & BL_TrataStringParaBD(estrutAnalises(i).codAnaS) & ""
                        
                    'Carregar sql para o RecordSet correspondente
                        Set RsInfoAnaTuboSimples = New ADODB.recordset
                        RsInfoAnaTuboSimples.CursorLocation = adUseServer
                        RsInfoAnaTuboSimples.CursorType = adOpenStatic
                        RsInfoAnaTuboSimples.Open sqlAnaSimples, gConexao
                               
                    'Adiciona a informa��o � estrutura de tubos
                        If RsInfoAnaTuboSimples.RecordCount > 0 Then
                        iAnaSimples = 1
                            While Not RsInfoAnaTuboSimples.EOF
                                
                                For i6 = 1 To gTotalTubos
                                    If (gEstruturaTubos(i6).CodTubo = RsInfoAnaTuboSimples!cod_tubo) Then
                                            'verifica se j� tinha informa��o para controlar a coloca��o da virgula
                                            If (gEstruturaTubos(i6).CodProd = "") Then
                                                gEstruturaTubos(i6).CodProd = BL_HandleNull(RsInfoAnaTuboSimples!cod_produto, "")
                                            Else
                                                gEstruturaTubos(i6).CodProd = gEstruturaTubos(i6).CodProd & "," & BL_HandleNull(RsInfoAnaTuboSimples!cod_produto, "")
                                            End If
                                            If (gEstruturaTubos(i6).GrAna = "") Then
                                                gEstruturaTubos(i6).GrAna = BL_HandleNull(RsInfoAnaTuboSimples!descr_gr_ana, "")
                                            Else
                                                gEstruturaTubos(i6).GrAna = gEstruturaTubos(i6).GrAna & "," & BL_HandleNull(RsInfoAnaTuboSimples!descr_gr_ana, "")
                                            End If
                                            
                                            gEstruturaTubos(i6).num_ana = gEstruturaTubos(i6).num_ana + 1
                                            'RGONCALVES 15.12.2014 - multiplicar por 'num_copias_original' em vez de 'num_copias'
                                            gEstruturaTubos(i6).num_copias = BL_ArredondaCima(gEstruturaTubos(i6).num_ana / gEstruturaTubos(i6).num_max_ana) * gEstruturaTubos(i6).num_copias_original
                                            'imprimir as abreviaturas das analises do tubo
                                            If BL_HandleNull(RsInfoAnaTuboSimples!flg_etiq_abr, "") = 1 And InStr(gEstruturaTubos(i6).abrAna, BL_HandleNull(RsInfoAnaTuboSimples!abr_ana_s, "...")) = 0 Then
                                                gEstruturaTubos(i6).abrAna = gEstruturaTubos(i6).abrAna & "," & BL_HandleNull(RsInfoAnaTuboSimples!abr_ana_s, "")
                                            End If
                                            
                                            If InStr(1, gEstruturaTubos(i6).Descr_etiq_ana, BL_HandleNull(RsInfoAnaTuboSimples!descr_etiq, "")) = 0 Then
                                                gEstruturaTubos(i6).Descr_etiq_ana = gEstruturaTubos(i6).Descr_etiq_ana & BL_HandleNull(RsInfoAnaTuboSimples!descr_etiq, "")
                                            End If
                                        Exit For
                                    End If
                                Next
                                
                                iAnaSimples = iAnaSimples + 1
                                
                                RsInfoAnaTuboSimples.MoveNext
                            Wend
                        End If
                        
                        'Fecha RS
                        RsInfoAnaTuboSimples.Close
                        Set RsInfoAnaTuboSimples = Nothing
            End If
        
        Next i
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

Sub ImprimeResumo()
    On Error GoTo TrataErro
    
    If gImprFolhaResumoParaRecibos <> mediSim Then
        FR_ImprimeResumoCrystal_HOSP NumRequis
    Else
        FR_ImprimeResumoParaImpressoraRecibos NumRequis
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumo: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumo"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' IMPRIME FOLHA DE RESUMO ATRAV�S DO CRYSTAL REPORTS

' -----------------------------------------------------------------
Private Sub ImprimeResumoCrystal()
    Dim continua As Boolean
    Dim ObsUtente As String
    On Error GoTo TrataErro
    
    continua = BL_IniciaReport("FolhaResumo", "Folha de Resumo", crptToPrinter)
    
    If continua = False Then Exit Sub
    BL_MudaCursorRato mediMP_Espera, Me
    
    BL_ApagaRegistosTabelaTemp ("sl_cr_folha_resumo")
    ImprimeResumoProdutos
    ImprimeResumoAnalises
    ImprimeResumoTubos
    
    'soliveira terrugem
    Dim rsOBS As ADODB.recordset
    Set rsOBS = New ADODB.recordset
    rsOBS.CursorLocation = adUseServer
    rsOBS.CursorType = adOpenStatic
    rsOBS.Source = "SELECT x1.obs, x1.nome_ute, x1.dt_nasc_ute, x2.dt_chega, x1.utente, x1.seq_utente, x2.cod_med, x2.descr_medico,"
    rsOBS.Source = rsOBS.Source & " x2.cod_Efr, x2.cod_sala, x2.t_urg, x2.n_req_assoc, x2.cod_proven FROM sl_identif x1, sl_requis x2 "
    rsOBS.Source = rsOBS.Source & " WHERE x2.n_req = " & NumRequis & " AND x1.seq_utente = x2.seq_utente "
    
    If gModoDebug = mediSim Then BG_LogFile_Erros rsOBS.Source
    rsOBS.Open , gConexao
    If rsOBS.RecordCount <= 0 Then
        Exit Sub
    End If
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = ""
    
    'F�rmulas do Report
    Report.formulas(0) = "NumReq=" & BL_TrataStringParaBD("" & NumRequis)
    Report.formulas(1) = "NomeUtente=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!nome_ute, ""))
    Report.formulas(2) = "DtNascUtente=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!dt_nasc_ute, ""))
    Report.formulas(3) = "DtChegada=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!dt_chega, ""))
    Report.formulas(4) = "Processo=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!Utente, ""))
    Report.formulas(5) = "DtValidade=" & BL_TrataStringParaBD("" & DevolveDataValidade(CStr(gRequisicaoActiva)))
    Report.formulas(6) = "NSC=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!Utente, ""))
    Report.formulas(7) = "Servico=''"
    Report.formulas(8) = "SeqUtente=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!seq_utente, ""))
    If BL_HandleNull(rsOBS!cod_med, "") = "" Then
        Report.formulas(9) = "Medico=" & BL_TrataStringParaBD("" & Trim(BL_HandleNull(rsOBS!descr_medico, "")))
    Else
        Report.formulas(9) = "Medico=" & BL_TrataStringParaBD("" & Trim(BL_SelCodigo("SL_MEDICOS", "NOME_MED", "COD_MED", BL_HandleNull(rsOBS!cod_med, ""), "V")))
    End If
    Report.formulas(10) = "Idade=" & BL_TrataStringParaBD("" & BG_CalculaIdade(CDate(BL_HandleNull(rsOBS!dt_nasc_ute, ""))))
    Report.formulas(11) = "EFR=" & BL_TrataStringParaBD("" & BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", BL_HandleNull(rsOBS!cod_efr, ""), "V"))
    Report.formulas(12) = "Sala=" & BL_TrataStringParaBD("" & BL_SelCodigo("SL_COD_SALAS", "DESCR_SALA", "COD_SALA", BL_HandleNull(rsOBS!cod_sala, ""), "V"))
    Report.formulas(13) = "Prioridade=" & BL_TrataStringParaBD("" & BL_SelCodigo("sl_tbf_t_urg", "DESCR_T_URG", "COD_T_URG", BL_HandleNull(rsOBS!t_urg, ""), "V"))
    'soliveira terrugem
    Report.formulas(14) = "ObsUtente=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!obs, ""))
    Report.formulas(15) = "ReqAssoc=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!n_Req_assoc, ""))
    Report.formulas(16) = "ProvReq=" & BL_TrataStringParaBD("" & BL_SelCodigo("SL_PROVEN", "DESCR_PROVEN", "COD_PROVEN", BL_HandleNull(rsOBS!cod_proven, ""), "V"))
    Report.SubreportToChange = "analises"
    Report.SelectionFormula = "{sl_cr_folha_resumo.nome_computador} = '" & gComputador & "' AND trim({sl_cr_folha_resumo.cod_ana})<>'' AND {sl_cr_folha_resumo.n_req} = " & gRequisicaoActiva & " AND {sl_cr_folha_resumo.num_sessao} = " & gNumeroSessao
    Report.Connect = "DSN=" & gDSN
    Report.SubreportToChange = "SubRepTubos"
    Report.SelectionFormula = "{sl_cr_folha_resumo.nome_computador} = '" & BG_SYS_GetComputerName & "' AND trim({sl_cr_folha_resumo.tubo}) <> '' AND {sl_cr_folha_resumo.n_req} = " & gRequisicaoActiva & " AND {sl_cr_folha_resumo.num_sessao} = " & gNumeroSessao
    Report.Connect = "DSN=" & gDSN
    Report.SubreportToChange = ""


    Call BL_ExecutaReport
    BL_MudaCursorRato mediMP_Activo, Me
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoCrystal: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoCrystal"
    Exit Sub
    Resume Next
End Sub


Private Sub CarregaGrid()
    Dim i As Integer
    'Desactiva evento RowColChange
    Flg_ExecutaCodigoFGTubos = False
    For i = 1 To gTotalTubos
        Call MudaCorFixo(i, 0)
        FGTubos.TextMatrix(i, 0) = gEstruturaTubos(i).Designacao_tubo
        FGTubos.TextMatrix(i, 1) = gEstruturaTubos(i).num_copias
        
        FGTubos.ColAlignment(0) = flexAlignLeftCenter
        FGTubos.ColAlignment(1) = flexAlignRightCenter
        FGTubos.AddItem ""
    Next
    'ACTIVA evento RowColChange
    Flg_ExecutaCodigoFGTubos = True
End Sub

' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARIA AS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub ImprimeResumoAnalises()
    Dim i As Integer
    Dim sSql As String
    Dim rsAna As ADODB.recordset
    On Error GoTo TrataErro
    BG_BeginTransaction
    gSQLError = 0
    sSql = "SELECT cod_agrup, descr_ana, ord_marca FROM sl_marcacoes x1, slv_analises x2 WHERE n_Req = " & NumRequis & " AND x1.cod_agrup = x2.cod_ana AND (x2.seq_sinonimo is null or x2.seq_sinonimo <=0) AND x1.ord_marca IS NOT NULL "
    sSql = sSql & " UNION SELECT cod_agrup, descr_ana, ord_marca FROM sl_realiza x1, slv_analises x2 WHERE n_Req = " & NumRequis & " AND x1.cod_agrup = x2.cod_ana AND (x2.seq_sinonimo is null or x2.seq_sinonimo <=0) AND x1.ord_marca IS NOT NULL "
    sSql = sSql & " GROUP by cod_agrup, descr_ana, ord_marca ORDER BY ord_marca "
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    rsAna.Source = sSql
    If gModoDebug = mediSim Then BG_LogFile_Erros rsAna.Source
    rsAna.Open , gConexao
    If rsAna.RecordCount <= 0 Then
        Exit Sub
    End If
    While Not rsAna.EOF
        sSql = "INSERT INTO sl_cr_folha_resumo (nome_computador, n_req,cod_ana,descr_ana, ordem,num_sessao) VALUES("
        sSql = sSql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
        sSql = sSql & gRequisicaoActiva & ", "
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAna!cod_agrup, "")) & ", "
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAna!descr_ana, "")) & "," & BL_HandleNull(rsAna!Ord_Marca, 0) & "," & gNumeroSessao & " )"
        
        BG_ExecutaQuery_ADO sSql
        rsAna.MoveNext
    Wend
    If gSQLError = 0 Then
        BG_CommitTransaction
    Else
        BG_RollbackTransaction
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoAnalises: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoAnalises"
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARAIA OS PRODUTOS DAS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub ImprimeResumoProdutos()
    Dim i As Integer
    Dim rsAna As New ADODB.recordset
    Dim sSql As String
    On Error GoTo TrataErro
    BG_BeginTransaction
    gSQLError = 0
    sSql = "SELECT x1.cod_prod, x2.descr_produto FROM sl_req_prod x1, sl_produto x2 WHERE x1.cod_prod = x2.cod_produto AND x1.n_Req = " & NumRequis
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    rsAna.Source = sSql
    If gModoDebug = mediSim Then BG_LogFile_Erros rsAna.Source
    rsAna.Open , gConexao
    If rsAna.RecordCount <= 0 Then
        Exit Sub
    End If
    
    BG_BeginTransaction
    gSQLError = 0
    While Not rsAna.EOF
        sSql = "INSERT INTO sl_cr_folha_resumo (nome_computador, n_req,produto, num_sessao) VALUES("
        sSql = sSql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
        sSql = sSql & gRequisicaoActiva & ", "
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAna!descr_produto, "")) & "," & gNumeroSessao & " )"

        BG_ExecutaQuery_ADO sSql
        rsAna.MoveNext
    Wend
    If gSQLError = 0 Then
        BG_CommitTransaction
    Else
        BG_RollbackTransaction
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoProdutos: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoProdutos"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARAIA OS TUBOS DAS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub ImprimeResumoTubos()
    Dim i As Integer
    Dim sSql As String
    On Error GoTo TrataErro
        
    BG_BeginTransaction
    gSQLError = 0
    For i = 1 To gTotalTubos
        sSql = "INSERT INTO sl_cr_folha_resumo (nome_computador, n_req,tubo, norma_colheita, num_sessao) VALUES("
        sSql = sSql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
        sSql = sSql & gRequisicaoActiva & ", "
        sSql = sSql & BL_TrataStringParaBD(gEstruturaTubos(i).Designacao_tubo) & ","
        sSql = sSql & BL_TrataStringParaBD(gEstruturaTubos(i).normaTubo) & "," & gNumeroSessao & " )"
        
        BG_ExecutaQuery_ADO sSql
    Next
    If gSQLError = 0 Then
        BG_CommitTransaction
    Else
        BG_RollbackTransaction
    End If
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "ImprimeResumoTubos: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoTubos"
    Exit Sub
    Resume Next
End Sub




Attribute VB_Name = "ModuleTransparency"
Option Explicit

Private Declare Function SetLayeredWindowAttributes Lib "user32" (ByVal hWnd As Long, ByVal crKey As Long, ByVal bAlpha As Byte, ByVal dwFlags As Long) As Long
Private Declare Function UpdateLayeredWindow Lib "user32" (ByVal hWnd As Long, ByVal hdcDst As Long, pptDst As Any, psize As Any, ByVal hdcSrc As Long, pptSrc As Any, crKey As Long, ByVal pblend As Long, ByVal dwFlags As Long) As Long
Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long) As Long
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Private Declare Function CreateRoundRectRgn Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long, ByVal X3 As Long, ByVal Y3 As Long) As Long


Private Declare Function CreateRectRgn Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long


Private Declare Function CreateEllipticRgn Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long


Private Declare Function CombineRgn Lib "gdi32" (ByVal hDestRgn As Long, ByVal hSrcRgn1 As Long, ByVal hSrcRgn2 As Long, ByVal nCombineMode As Long) As Long


Private Declare Function SetWindowRgn Lib "user32" (ByVal hWnd As Long, ByVal hRgn As Long, ByVal bRedraw As Long) As Long
    
Private Const GWL_EXSTYLE = (-20)
Private Const LWA_COLORKEY = &H1
Private Const LWA_ALPHA = &H2
Private Const ULW_COLORKEY = &H1
Private Const ULW_ALPHA = &H2
Private Const ULW_OPAQUE = &H4
Private Const WS_EX_LAYERED = &H80000

Public Function isTransparent(ByVal hWnd As Long) As Boolean
On Error Resume Next
Dim Msg As Long
Msg = GetWindowLong(hWnd, GWL_EXSTYLE)
If (Msg And WS_EX_LAYERED) = WS_EX_LAYERED Then
  isTransparent = True
Else
  isTransparent = False
End If
If Err Then
  isTransparent = False
End If
End Function

Public Function MakeTransparent(ByVal hWnd As Long, Perc As Integer) As Long
Dim Msg As Long
On Error Resume Next
If Perc < 0 Or Perc > 255 Then
  MakeTransparent = 1
Else
  Msg = GetWindowLong(hWnd, GWL_EXSTYLE)
  Msg = Msg Or WS_EX_LAYERED
  SetWindowLong hWnd, GWL_EXSTYLE, Msg
  SetLayeredWindowAttributes hWnd, 0, Perc, LWA_ALPHA
  MakeTransparent = 0
End If
If Err Then
  MakeTransparent = 2
End If
End Function

Public Function MakeOpaque(ByVal hWnd As Long) As Long
Dim Msg As Long
On Error Resume Next
Msg = GetWindowLong(hWnd, GWL_EXSTYLE)
Msg = Msg And Not WS_EX_LAYERED
SetWindowLong hWnd, GWL_EXSTYLE, Msg
SetLayeredWindowAttributes hWnd, 0, 0, LWA_ALPHA
MakeOpaque = 0
If Err Then
  MakeOpaque = 2
End If
End Function




    
    '2 The Function
    ' This should be in the form's code.


Public Function fMakeATranspArea(AreaType As String, pCordinate() As Long) As Boolean
    'Name: fMakeATranpArea
    'Author: Dalin Nie
    'Date: 5/18/98
    'Purpose: Create a Transprarent Area in
    '     a form so that you can see through
    'Input: Areatype : a String indicate wha
    '     t kind of hole shape it would like to ma
    '     ke
    ' PCordinate : the cordinate area needed


    '     for create the shape:
        ' Example: X1, Y1, X2, Y2 for Rectangle
        'OutPut: A boolean
        Const RGN_DIFF = 4
        Dim lOriginalForm As Long
        Dim ltheHole As Long
        Dim lNewForm As Long
        Dim lFwidth As Single
        Dim lFHeight As Single
        Dim lborder_width As Single
        Dim ltitle_height As Single
        On Error GoTo Trap
        lFwidth = gFormActivo.ScaleX(gFormActivo.Width, vbTwips, vbPixels)
        lFHeight = gFormActivo.ScaleY(gFormActivo.Height, vbTwips, vbPixels)
        lOriginalForm = CreateRectRgn(0, 0, lFwidth, lFHeight)
        
        lborder_width = (lFHeight - gFormActivo.ScaleWidth) / 2
        ltitle_height = lFHeight - lborder_width - gFormActivo.ScaleHeight


        Select Case AreaType
            
            Case "Elliptic"
            
            ltheHole = CreateEllipticRgn(pCordinate(1), pCordinate(2), pCordinate(3), pCordinate(4))
            Case "RectAngle"
            
            ltheHole = CreateRectRgn(pCordinate(1), pCordinate(2), pCordinate(3), pCordinate(4))
            
            Case "RoundRect"
            
            ltheHole = CreateRoundRectRgn(pCordinate(1), pCordinate(2), pCordinate(3), pCordinate(4), pCordinate(5), pCordinate(6))
            Case "Circle"
            ltheHole = CreateRoundRectRgn(pCordinate(1), pCordinate(2), pCordinate(3), pCordinate(4), pCordinate(3), pCordinate(4))
            
            Case Else
            MsgBox "Unknown Shape!!"
            Exit Function
        End Select
    lNewForm = CreateRectRgn(0, 0, 0, 0)
    CombineRgn lNewForm, lOriginalForm, _
    ltheHole, RGN_DIFF
    
    SetWindowRgn gFormActivo.hWnd, lNewForm, True
    gFormActivo.Refresh
    fMakeATranspArea = True
    Exit Function
Trap:
    MsgBox "error Occurred. Error # " & Err.Number & ", " & Err.Description
End Function


' 3 How To Call



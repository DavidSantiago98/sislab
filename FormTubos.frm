VERSION 5.00
Begin VB.Form FormTubos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormTubos"
   ClientHeight    =   6780
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6690
   Icon            =   "FormTubos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6780
   ScaleWidth      =   6690
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CbTemposColh 
      Height          =   315
      Left            =   1440
      Style           =   2  'Dropdown List
      TabIndex        =   40
      Top             =   1920
      Width           =   2655
   End
   Begin VB.TextBox EcNumMaxAna 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0;(0)"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2070
         SubFormatType   =   1
      EndProperty
      Height          =   285
      Left            =   5280
      TabIndex        =   38
      Top             =   1560
      Width           =   1215
   End
   Begin VB.TextBox EcNumCopias 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0;(0)"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2070
         SubFormatType   =   1
      EndProperty
      Height          =   285
      Left            =   1440
      TabIndex        =   36
      Top             =   1560
      Width           =   825
   End
   Begin VB.TextBox EcInfComple 
      Height          =   285
      Left            =   1440
      TabIndex        =   35
      Top             =   840
      Width           =   2655
   End
   Begin VB.TextBox EcTampa 
      Height          =   285
      Left            =   5280
      TabIndex        =   33
      Top             =   1200
      Width           =   1215
   End
   Begin VB.TextBox EcProduto 
      Height          =   285
      Left            =   6480
      TabIndex        =   30
      Top             =   6360
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.ComboBox CbProduto 
      Height          =   315
      Left            =   1440
      Style           =   2  'Dropdown List
      TabIndex        =   28
      Top             =   1200
      Width           =   2655
   End
   Begin VB.TextBox EcEtiqueta 
      Height          =   285
      Left            =   5280
      TabIndex        =   26
      Top             =   840
      Width           =   1215
   End
   Begin VB.TextBox EcCapacidade 
      Height          =   315
      Left            =   5280
      TabIndex        =   3
      Top             =   120
      Width           =   1185
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   1440
      TabIndex        =   1
      Top             =   120
      Width           =   1425
   End
   Begin VB.TextBox EcDescricao 
      Height          =   285
      Left            =   1440
      TabIndex        =   2
      Top             =   480
      Width           =   4995
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6360
      TabIndex        =   15
      Top             =   5910
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      ItemData        =   "FormTubos.frx":000C
      Left            =   150
      List            =   "FormTubos.frx":000E
      TabIndex        =   4
      Top             =   2640
      Width           =   6405
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   150
      TabIndex        =   8
      Top             =   4875
      Width           =   6405
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   14
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   13
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   12
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   11
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3720
      TabIndex        =   7
      Top             =   5880
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   6
      Top             =   5910
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      TabIndex        =   5
      Top             =   6360
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   0
      Top             =   6360
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Label Label18 
      Caption         =   "Tempo Colheita"
      Height          =   285
      Index           =   2
      Left            =   240
      TabIndex        =   41
      Top             =   1920
      Width           =   1335
   End
   Begin VB.Label Label18 
      Caption         =   "N� M�x. An�lises"
      Height          =   285
      Index           =   1
      Left            =   4080
      TabIndex        =   39
      Top             =   1560
      Width           =   1335
   End
   Begin VB.Label Label18 
      Caption         =   "N� C�pias"
      Height          =   285
      Index           =   0
      Left            =   240
      TabIndex        =   37
      Top             =   1560
      Width           =   855
   End
   Begin VB.Label Label17 
      Caption         =   "Inf. Complen."
      Height          =   255
      Left            =   240
      TabIndex        =   34
      Top             =   840
      Width           =   975
   End
   Begin VB.Label Label16 
      Caption         =   "Caract.Tampa"
      Height          =   255
      Left            =   4080
      TabIndex        =   32
      Top             =   1200
      Width           =   1215
   End
   Begin VB.Label Label15 
      Caption         =   "Produto"
      Height          =   225
      Left            =   4920
      TabIndex        =   31
      Top             =   6390
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.Label Label14 
      Caption         =   "Produto assoc."
      Height          =   255
      Index           =   0
      Left            =   180
      TabIndex        =   29
      Top             =   1200
      Width           =   2175
   End
   Begin VB.Label Label13 
      Caption         =   "C�digo Etiqueta"
      Height          =   255
      Left            =   4080
      TabIndex        =   27
      Top             =   840
      Width           =   1215
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2520
      TabIndex        =   25
      Top             =   6360
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2520
      TabIndex        =   24
      Top             =   5880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   120
      TabIndex        =   23
      Top             =   6360
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   240
      TabIndex        =   22
      Top             =   5880
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "Capacidade"
      Height          =   285
      Left            =   4320
      TabIndex        =   21
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   180
      TabIndex        =   20
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   180
      TabIndex        =   19
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   4800
      TabIndex        =   18
      Top             =   5940
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   3090
      TabIndex        =   17
      Top             =   2400
      Width           =   855
   End
   Begin VB.Label Label7 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   180
      TabIndex        =   16
      Top             =   2400
      Width           =   645
   End
End
Attribute VB_Name = "FormTubos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset



Private Sub CbProduto_Click()
    BL_ColocaComboTexto "sl_produto", "seq_produto", "cod_produto", EcProduto, CbProduto
End Sub

Private Sub CbProduto_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then CbProduto.ListIndex = -1
End Sub

Private Sub CbTemposColh_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbTemposColh.ListIndex = mediComboValorNull
    End If
End Sub

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub
Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub

Private Sub EcDescricao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDescricao_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub



Private Sub EcProduto_Change()
    BL_ColocaTextoCombo "sl_produto", "seq_produto", "cod_produto", EcProduto, CbProduto
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Tipos de Tubos Prim�rios"
    Me.left = 540
    Me.top = 450
    Me.Width = 7280
    Me.Height = 6240 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_tubo"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 15
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_tubo"
    CamposBD(1) = "cod_tubo"
    CamposBD(2) = "descr_tubo"
    CamposBD(3) = "capaci_util"
    CamposBD(4) = "user_cri"
    CamposBD(5) = "dt_cri"
    CamposBD(6) = "user_act"
    CamposBD(7) = "dt_act"
    CamposBD(8) = "cod_etiq"
    CamposBD(9) = "cod_prod"
    CamposBD(10) = "tampa"
    CamposBD(11) = "inf_complementar"
    CamposBD(12) = "num_copias"
    CamposBD(13) = "num_max_ana"
    CamposBD(14) = "cod_tempo_colheita"

    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcCapacidade
    Set CamposEc(4) = EcUtilizadorCriacao
    Set CamposEc(5) = EcDataCriacao
    Set CamposEc(6) = EcUtilizadorAlteracao
    Set CamposEc(7) = EcDataAlteracao
    Set CamposEc(8) = EcEtiqueta
    Set CamposEc(9) = EcProduto
    Set CamposEc(10) = EcTampa
    Set CamposEc(11) = EcInfComple
    Set CamposEc(12) = EcNumCopias
    Set CamposEc(13) = EcNumMaxAna
    Set CamposEc(14) = CbTemposColh

    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo do Tipo de Tubo"
    TextoCamposObrigatorios(2) = "Descri��o do Tubo"
    TextoCamposObrigatorios(12) = "N�mero de C�pias"
    TextoCamposObrigatorios(13) = "N�mero M�ximo de An�lises"
'    TextoCamposObrigatorios(8) = "C�digo para Etiqueta"
'    TextoCamposObrigatorios(9) = "Produto associado"
        
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_tubo"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_tubo", "descr_tubo")
    NumEspacos = Array(22, 32)
    
    'BRUNODSANTOS LRV-518 - 17.11.2017
    EcNumMaxAna.Enabled = False
    EcNumMaxAna.Text = "99"
    '
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormTubos = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus

    CbProduto.ListIndex = mediComboValorNull
    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
    End If
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        'BRUNODSANTOS LRV-518 17.11.2017
        EcNumMaxAna.Text = "99"
        '
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_tubo ASC,descr_tubo ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'
        
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String

    gSQLError = 0
    gSQLISAM = 0
    
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_tubo") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
        
    
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'

    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    
    LimpaCampos
    PreencheCampos

End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub


Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_produto", "seq_produto", "descr_produto", CbProduto, mediDescComboDesignacao
    BG_PreencheComboBD_ADO "sl_tbf_tempos_colheita", "cod_tempo_colheita", "descr_tempo_colheita", CbTemposColh, mediAscComboCodigo
End Sub



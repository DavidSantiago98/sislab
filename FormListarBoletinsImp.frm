VERSION 5.00
Begin VB.Form FormListarRelatoriosImp 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form"
   ClientHeight    =   3060
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6705
   Icon            =   "FormListarBoletinsImp.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3060
   ScaleWidth      =   6705
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   3015
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6495
      Begin VB.CommandButton BtPesquisaLocalEntrega 
         Height          =   315
         Left            =   5880
         Picture         =   "FormListarBoletinsImp.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   18
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   2040
         Width           =   375
      End
      Begin VB.TextBox EcDescrLocalEntrega 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   2040
         Width           =   3975
      End
      Begin VB.TextBox EcCodLocalEntrega 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   16
         Top             =   2040
         Width           =   735
      End
      Begin VB.TextBox EcCodSala 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   14
         Top             =   1680
         Width           =   735
      End
      Begin VB.TextBox EcDescrSala 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   1680
         Width           =   3975
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   5880
         Picture         =   "FormListarBoletinsImp.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   1680
         Width           =   375
      End
      Begin VB.TextBox EcCodProveniencia 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   10
         Top             =   1320
         Width           =   735
      End
      Begin VB.TextBox EcDescrProveniencia 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   1320
         Width           =   3975
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   5880
         Picture         =   "FormListarBoletinsImp.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1320
         Width           =   375
      End
      Begin VB.CheckBox CkApenasEmail 
         Caption         =   "Apenas Resultados enviados por email"
         Height          =   255
         Left            =   1200
         TabIndex        =   7
         Top             =   2640
         Width           =   3135
      End
      Begin VB.ComboBox CbLocal 
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   840
         Width           =   3495
      End
      Begin VB.TextBox EcDtFim 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3720
         TabIndex        =   2
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox EcDtIni 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1200
         TabIndex        =   1
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "Local Entrega"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   19
         Top             =   2040
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Sala / Posto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   15
         Top             =   1680
         Width           =   1155
      End
      Begin VB.Label Label1 
         Caption         =   "&Proveni�ncia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   11
         Top             =   1320
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Local"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   840
         Width           =   735
      End
      Begin VB.Label lblDataFim 
         Caption         =   "Data final"
         Height          =   495
         Left            =   2640
         TabIndex        =   4
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label lblDataInicio 
         Caption         =   "Data inicial"
         Height          =   495
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   1455
      End
   End
End
Attribute VB_Name = "FormListarRelatoriosImp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 23/04/2003
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Private Sub BtPesquisaSala_Click()
    PA_PesquisaSala EcCodSala, EcDescrSala, ""
End Sub

Public Sub EcCodlocalentrega_Validate(Cancel As Boolean)
    Cancel = PA_ValidateLocalEntrega(EcCodLocalEntrega, EcDescrLocalEntrega)
End Sub

Private Sub BtPesquisaLocalEntrega_Click()
    PA_PesquisaLocalEntrega EcCodLocalEntrega, EcDescrLocalEntrega

End Sub

Public Sub EcCodsala_Validate(Cancel As Boolean)
    Cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodsala_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodsala_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
        
End Sub

Sub Inicializacoes()

    Me.caption = "Listagem de Relat�rios Impressos/Enviados"
    Me.left = 540
    Me.top = 450
    Me.Width = 6750
    Me.Height = 3495 ' Normal
    'Me.Height = 1725 ' Campos Extras
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", CbLocal
    
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Listagem de Relatorios Impressos")
    
    Set FormListarRelatoriosImp = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    EcCodProveniencia = ""
    EcDescrProveniencia = ""
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    CbLocal.ListIndex = mediComboValorNull
    EcCodLocalEntrega.Text = ""
    EcDescrLocalEntrega.Text = ""
    
End Sub

Sub DefTipoCampos()

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"

    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Imprime_Listagem

End Sub

Sub ImprimirVerAntes()
    
    Call Imprime_Listagem

End Sub

Public Sub Imprime_Listagem()
    Dim continua As Boolean
    Dim Query  As String
    Dim sql As String
    Dim RsRel As ADODB.recordset
    Dim rsRec As New ADODB.recordset
    Dim totalRec As Double
    
    sql = "DELETE FROM sl_cr_rel WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sql
    
    '1. Verifica se a Form Preview j� est� aberta
    'If BL_PreviewAberto("Listagem Relat�rios Impressos") = True Then Exit Sub
        
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial para a pesquisa de relat�rios impressos.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final para a pesquisa de relat�rios impressos.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    Set RsRel = New ADODB.recordset
    If CkApenasEmail.value <> vbChecked Then
        sql = "(SELECT Descr_Proven, t_utente || '/ ' || utente utente, nome_ute, n_req, util_imp.utilizador, dt_imp, hr_imp, "
        sql = sql & "util_imp2.utilizador util_imp2, dt_imp2, hr_imp2, " & BL_TrataStringParaBD(gComputador) & " NOME_COMPUTADOR, "
        sql = sql & gNumeroSessao & " NUM_SESSAO , null destino,sl_cod_salas.descr_sala,sl_cod_local_entrega.cod_local_entrega,  "
        sql = sql & " sl_cod_local_entrega.descr_local_entrega "
        sql = sql & " FROM sl_requis LEFT OUTER JOIN sl_Cod_salas ON sl_requis.cod_sala = sl_cod_salas.cod_sala "
        sql = sql & " LEFT OUTER JOIN sl_proven ON sl_requis.cod_proven  = sl_proven.cod_proven "
        sql = sql & " LEFT OUTER JOIN sl_cod_local_entrega ON sl_requis.cod_local_entrega  = sl_cod_local_entrega.cod_local_entrega "
        sql = sql & " LEFT OUTER JOIN sl_idutilizador util_imp2 ON sl_requis.user_imp2 = util_imp2.cod_utilizador, "
        sql = sql & " sl_idutilizador util_imp,sl_identif "
        sql = sql & "WHERE sl_requis.seq_utente = sl_identif.seq_utente AND "
        sql = sql & "sl_requis.user_imp = util_imp.cod_utilizador and "
        sql = sql & "dt_imp BETWEEN " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text)
        'local preenchido?
        If CbLocal.ListIndex <> -1 Then
            sql = sql & " AND sl_requis.cod_local=" & CbLocal.ItemData(CbLocal.ListIndex)
        End If
        If EcCodProveniencia.Text <> "" Then
            sql = sql & " AND sl_requis.cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
        End If
        If EcCodLocalEntrega.Text <> "" Then
            sql = sql & " AND sl_requis.cod_local_entrega=" & BL_TrataStringParaBD(EcCodLocalEntrega.Text)
        End If
        If EcCodSala.Text <> "" Then
            sql = sql & " AND sl_requis.cod_sala=" & BL_TrataStringParaBD(EcCodSala.Text)
        End If
        sql = sql & ") "
    
        sql = sql & " UNION (SELECT Descr_Proven, t_utente || '/ ' || utente utente, nome_ute, n_req, util_imp.utilizador, dt_imp, hr_imp, "
        sql = sql & "util_imp2.utilizador util_imp2, dt_imp2, hr_imp2, " & BL_TrataStringParaBD(gComputador) & " NOME_COMPUTADOR, "
        sql = sql & gNumeroSessao & " NUM_SESSAO, null destino,sl_cod_salas.descr_sala,sl_cod_local_entrega.cod_local_entrega,  "
        sql = sql & " sl_cod_local_entrega.descr_local_entrega "
        sql = sql & "FROM sl_requis LEFT OUTER JOIN sl_Cod_salas ON sl_requis.cod_sala = sl_cod_salas.cod_sala "
        sql = sql & " LEFT OUTER JOIN sl_cod_local_entrega ON sl_requis.cod_local_entrega  = sl_cod_local_entrega.cod_local_entrega "
        sql = sql & " LEFT OUTER JOIN sl_proven ON sl_requis.cod_proven  = sl_proven.cod_proven "
        sql = sql & " LEFT OUTER JOIN sl_idutilizador util_imp2 ON sl_requis.user_imp2 = util_imp2.cod_utilizador, "
        sql = sql & " sl_idutilizador util_imp,sl_identif "
        sql = sql & "WHERE sl_requis.seq_utente = sl_identif.seq_utente and "
        sql = sql & "sl_requis.user_imp = util_imp.cod_utilizador and "
        sql = sql & "dt_imp2 BETWEEN " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text)
        'local preenchido?
        If CbLocal.ListIndex <> -1 Then
            sql = sql & " AND sl_requis.cod_local=" & CbLocal.ItemData(CbLocal.ListIndex)
        End If
        If EcCodProveniencia.Text <> "" Then
            sql = sql & " AND sl_requis.cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
        End If
        If EcCodLocalEntrega.Text <> "" Then
            sql = sql & " AND sl_requis.cod_local_entrega=" & BL_TrataStringParaBD(EcCodLocalEntrega.Text)
        End If
        If EcCodSala.Text <> "" Then
            sql = sql & " AND sl_requis.cod_sala=" & BL_TrataStringParaBD(EcCodSala.Text)
        End If
        sql = sql & ") "
        
        sql = sql & " ORDER BY descr_proven,nome_ute"
    Else
        sql = "SELECT descr_proven, t_utente || '/ ' || utente utente, nome_ute, sl_requis.n_req, utilizador, "
        sql = sql & " sl_email_resultados.dt_cri dt_imp, sl_email_resultados.hr_cri hr_imp, null dt_imp2, "
        sql = sql & " null util_imp2, null hr_imp2, " & BL_TrataStringParaBD(gComputador) & " NOME_COMPUTADOR, "
        sql = sql & gNumeroSessao & " NUM_SESSAO, sl_email_resultados.destino,sl_cod_salas.descr_sala,sl_cod_local_entrega.cod_local_entrega,  "
        sql = sql & " sl_cod_local_entrega.descr_local_entrega "
        sql = sql & " FROM sl_requis LEFT OUTER JOIN sl_Cod_salas ON sl_requis.cod_sala = sl_cod_salas.cod_sala "
        sql = sql & " LEFT OUTER JOIN sl_cod_local_entrega ON sl_requis.cod_local_entrega  = sl_cod_local_entrega.cod_local_entrega "
        sql = sql & " LEFT OUTER JOIN sl_proven ON sl_requis.cod_proven  = sl_proven.cod_proven, "
        sql = sql & " sl_idutilizador, sl_identif,  sl_email_resultados WHERE sl_requis.seq_utente = "
        sql = sql & " sl_identif.seq_utente  AND "
        sql = sql & " sl_email_resultados.user_cri = sl_idutilizador.cod_utilizador AND sl_requis.n_req = "
        sql = sql & " sl_email_resultados.n_req AND sl_email_resultados.dt_cri BETWEEN " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text)
        If EcCodProveniencia.Text <> "" Then
            sql = sql & " AND sl_requis.cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
        End If
        If EcCodLocalEntrega.Text <> "" Then
            sql = sql & " AND sl_requis.cod_local_entrega=" & BL_TrataStringParaBD(EcCodLocalEntrega.Text)
        End If
        If EcCodSala.Text <> "" Then
            sql = sql & " AND sl_requis.cod_sala=" & BL_TrataStringParaBD(EcCodSala.Text)
        End If
        sql = sql & " ORDER BY n_req "
    End If
    
    RsRel.CursorType = adOpenStatic
    RsRel.CursorLocation = adUseServer
    RsRel.Open sql, gConexao
    If RsRel.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo!", vbOKOnly + vbInformation, App.ProductName
        RsRel.Close
        Set RsRel = Nothing
        Exit Sub
    End If
        
    '*****************************************************
        
    '4.Inicia o processo de impress�o do relat�rio
    
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("ListarRequisImp", "Listagem de Relat�rios Impressos", crptToPrinter)
    Else
        continua = BL_IniciaReport("ListarRequisImp", "Listagem de Relat�rios Impressos", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    BL_MudaCursorRato mediMP_Espera, Me
    
    While Not RsRel.EOF
        sql = "SELECT sum(total_pagar) total FROM sl_recibos WHERE n_req = " & BL_HandleNull(RsRel!n_req, "") & " AND estado <> 'A' "
        rsRec.CursorType = adOpenStatic
        rsRec.CursorLocation = adUseServer
        rsRec.Open sql, gConexao
        If rsRec.RecordCount <= 0 Then
            totalRec = 0
        Else
            totalRec = BL_HandleNull(rsRec!total, 0)
        End If
        rsRec.Close
        Set rsRec = Nothing
        
        '3. Insere na tabela tempor�ria as requisi��es impressas perante as datas seleccionadas
        Query = "INSERT INTO SL_CR_REL (DESCR_PROVEN, UTENTE, NOME_UTE, N_REQ, UTIL_IMP, DT_IMP, HR_IMP, " & _
                "UTIL_IMP2, DT_IMP2, HR_IMP2, NOME_COMPUTADOR, NUM_SESSAO, DESTINO, DESCR_SALA, cod_local_entrega, descr_local_entrega, total_rec) VALUES( "
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!descr_proven, "")) & ","
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!Utente, "")) & ","
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!nome_ute, "")) & ","
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!n_req, "")) & ","
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!utilizador, "")) & ","
        Query = Query & BL_TrataDataParaBD(BL_HandleNull(RsRel!dt_imp, "")) & ","
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!hr_imp, "")) & ","
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!util_imp2, "")) & ","
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!dt_imp2, "")) & ","
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!hr_imp2, "")) & ","
        Query = Query & BL_TrataStringParaBD(gComputador) & ","
        Query = Query & gNumeroSessao & ","
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!destino, "")) & ","
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!descr_sala, "")) & ","
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!cod_local_entrega, "")) & ","
        Query = Query & BL_TrataStringParaBD(BL_HandleNull(RsRel!descr_local_entrega, "")) & ","
        Query = Query & Replace(totalRec, ",", ".") & ")"
        BG_ExecutaQuery_ADO Query
        
        RsRel.MoveNext
    Wend
 
    
    '___________________________________________________________________
    
    'Imprime o relat�rio no Crystal Reports
    'Atribui o resto dos dados ao Report=>F�rmulas e Queries
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT * " & _
                      " FROM SL_CR_REL SL_CR_RELIMP " & _
                      " WHERE SL_CR_RELIMP.nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND SL_CR_RELIMP.num_sessao = " & gNumeroSessao & _
                      " ORDER BY DESCR_PROVEN, NOME_UTE"
    
    'F�rmulas do Report
    Report.formulas(0) = "DataIni=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    
    Call BL_ExecutaReport
    
    RsRel.Close
    Set RsRel = Nothing
                
    BL_MudaCursorRato mediMP_Activo, Me
    
    sql = "DELETE FROM sl_cr_rel WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sql
       
End Sub


Private Sub BtPesquisaProveniencia_Click()
    


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    ClausulaWhere = " (flg_invisivel IS NULL or flg_invisivel = 0) "
    If gCodLocal <> -1 Then
        ClausulaWhere = ClausulaWhere & " AND (cod_local IS NULL OR cod_local = " & gCodLocal & ") "
    End If
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.Text = resultados(1)
            EcDescrProveniencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub

Public Sub EcCodProveniencia_Validate(Cancel As Boolean)
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodProveniencia.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_proven FROM sl_proven WHERE cod_proven='" & Trim(EcCodProveniencia.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodProveniencia.Text = ""
            EcDescrProveniencia.Text = ""
        Else
            EcDescrProveniencia.Text = Tabela!descr_proven
                
        End If
        
               
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub


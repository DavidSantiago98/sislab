Attribute VB_Name = "eResults"
Public Sub ER_CancelaEresults_v1(n_req As String, cod_local As String)
    Dim sSql As String
    Dim Rs1 As New ADODB.recordset
    Dim cod_origem As String
    On Error GoTo TrataErro
    
    'RETORNA O COD_ORIGEM PARA O LOCAL EM CAUSA
    sSql = "SELECT * FROM gr_empr_inst WHERE cod_empr = " & cod_local
    Rs1.CursorLocation = adUseServer
    Rs1.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    Rs1.Open sSql, gConexao
    If Rs1.RecordCount >= 1 Then
        cod_origem = BL_HandleNull(Rs1!cod_origem, geResultsCodOrigem)
    Else
        cod_origem = geResultsCodOrigem
    End If
    Rs1.Close
    Set Rs1 = Nothing
    sSql = "delete from er_docs where cod_origem = " & cod_origem & " And codigo_original = " & BL_TrataStringParaBD(n_req)
    gConexaoEresults.Execute sSql
Exit Sub
TrataErro:
    BG_LogFile_Erros "Modulo ER ER_CancelaEresults_v1: " & Err.Description, "BL", "ER_CancelaEresults_v1", True
    Exit Sub
    Resume Next
End Sub


Public Sub ER_CancelaEresults_v2(n_req As String, cod_local As String)
    Dim sSql As String
    Dim Rs1 As New ADODB.recordset
    Dim cod_origem As String
    Dim CmdCancel As New ADODB.Command
    Dim PmtInstCode As ADODB.Parameter
    Dim PmtPlaceCode As ADODB.Parameter
    Dim PmtAppCode As ADODB.Parameter
    Dim PmtDocTypeCode As ADODB.Parameter
    Dim PmtDocId As ADODB.Parameter

    On Error GoTo TrataErro
    
    'RETORNA O COD_ORIGEM PARA O LOCAL EM CAUSA
    sSql = "SELECT * FROM gr_empr_inst WHERE cod_empr = " & cod_local
    Rs1.CursorLocation = adUseServer
    Rs1.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    Rs1.Open sSql, gConexao
    If Rs1.RecordCount >= 1 Then
        cod_origem = BL_HandleNull(Rs1!cod_origem_xml, geResultsCodOrigem)
    Else
        cod_origem = geResultsCodOrigem
    End If
    Rs1.Close
    Set Rs1 = Nothing

    With CmdCancel
        .ActiveConnection = gConexaoEresults
        .CommandText = "pck_documents_document.canceldocument"
        .CommandType = adCmdStoredProc
    End With
    Set PmtInstCode = CmdCancel.CreateParameter("P_INST_CODE", adVarChar, adParamInput, 30)
    Set PmtPlaceCode = CmdCancel.CreateParameter("P_PLACE_CODE", adVarChar, adParamInput, 30)
    Set PmtAppCode = CmdCancel.CreateParameter("P_APP_CODE", adVarChar, adParamInput, 30)
    Set PmtDocTypeCode = CmdCancel.CreateParameter("P_DOCTYPE_CODE", adVarChar, adParamInput, 30)
    Set PmtDocId = CmdCancel.CreateParameter("P_DOC_ID", adVarChar, adParamInput, 30)
    
    CmdCancel.Parameters.Append PmtInstCode
    CmdCancel.Parameters.Append PmtPlaceCode
    CmdCancel.Parameters.Append PmtAppCode
    CmdCancel.Parameters.Append PmtDocTypeCode
    CmdCancel.Parameters.Append PmtDocId

    CmdCancel.Parameters("P_INST_CODE") = gLAB
    CmdCancel.Parameters("P_PLACE_CODE") = gLAB
    CmdCancel.Parameters("P_APP_CODE") = cod_origem
    CmdCancel.Parameters("P_DOCTYPE_CODE") = "1"
    CmdCancel.Parameters("P_DOC_ID") = n_req
    CmdCancel.Execute

Exit Sub
TrataErro:
    BG_LogFile_Erros "Modulo ER ER_CancelaEresults_v2: " & Err.Description, "BL", "ER_CancelaEresults_v2", True
    Exit Sub
    Resume Next
End Sub




Public Sub ER_AbreLinkEResults_V2(n_req As String, cod_local As String, Utente As String, t_utente As String)
    Dim sURL As String
    Dim aplicacao_id As String
    Dim tipo_documento_id As String
    On Error GoTo TrataErro
    sURL = gEresults_Link & "tDoente=" & t_utente & "&doente=" & Utente & "&refDoc=" & n_req
    If gPathIE <> "" And gPathIE <> "-1" Then
        Shell gPathIE & " -new  " & " " & sURL
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Modulo ER ER_AbreLinkEResults_V2: " & Err.Description, "BL", "ER_AbreLinkEResults_V2", True
    Exit Sub
    Resume Next
End Sub
Public Sub ER_AbreLinkEResults_V1(n_req As String, cod_local As String)
    Dim sURL As String
    On Error GoTo TrataErro

Exit Sub
TrataErro:
    BG_LogFile_Erros "Modulo ER ER_AbreLinkEResults_V1: " & Err.Description, "BL", "ER_AbreLinkEResults_V1", True
    Exit Sub
    Resume Next
End Sub
Private Function ER_DevolveCodDocumentoID(n_req As String) As String
    Dim sSql As String
    Dim rsReq As New ADODB.recordset
    Dim Grupo As String
    Dim grupos() As String
    Dim tipoDoc As String
    On Error GoTo TrataErro
    
    ER_DevolveCodDocumentoID = ""
    
    ' GRUPOS DAS REQUISICOES
    sSql = "SELECT gr_ana FROM sl_requis WHERE n_Req = " & n_req
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    rsReq.Open sSql, gConexao
    If rsReq.RecordCount > 0 Then
        If BL_HandleNull(rsReq!gr_ana, "") <> "" Then
            Grupo = BL_HandleNull(rsReq!gr_ana, "")
        End If
    End If
    rsReq.Close
    grupos = Split(Grupo, ";")
    If Grupo = "" Then
        Exit Function
    End If
    'TIPO DE DOCUMENTO PARA O PRIMEIRO GRUPO DA REQ
    sSql = "SELECT tipo_doc FROM sl_gr_ana WHERE cod_gr_ana = " & BL_TrataStringParaBD(grupos(0))
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    rsReq.Open sSql, gConexao
    If rsReq.RecordCount > 0 Then
        tipoDoc = BL_HandleNull(rsReq!tipo_doc, geResultsTipoDoc)
    Else
        tipoDoc = geResultsTipoDoc
    End If
    rsReq.Close
    
    sSql = "SELECT tipo_documento_id FROM er_tipo_documento WHERE sigla = " & BL_TrataStringParaBD(tipoDoc)
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    rsReq.Open sSql, gConexaoEresults
    If rsReq.RecordCount > 0 Then
        ER_DevolveCodDocumentoID = BL_HandleNull(rsReq!tipo_documento_id, "")
    End If
    rsReq.Close
    Set rsReq = Nothing
Exit Function
TrataErro:
    ER_DevolveCodDocumentoID = geResultsTipoDoc
    BG_LogFile_Erros "Modulo ER ER_DevolveCodDocumentoID: " & Err.Description, "BL", "ER_DevolveCodDocumentoID", True
    Exit Function
    Resume Next
End Function


Public Function ER_DevolveAplicacaoId(cod_local As String) As String
    Dim sSql As String
    Dim Rs1 As New ADODB.recordset
    Dim cod_origem_xml As String
    Dim aplicacao_id As String
    On Error GoTo TrataErro
    
    'RETORNA COD_ORIGEM_XML
    sSql = " SELECT * FROM gr_empr_inst WHERE cod_empr = " & cod_local
    Rs1.CursorLocation = adUseServer
    Rs1.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    Rs1.Open sSql, gConexao
    If Rs1.RecordCount >= 1 Then
        cod_origem_xml = BL_HandleNull(Rs1!cod_origem_xml, geResultsCodOrigem)
    Else
        cod_origem_xml = geResultsCodOrigem
    End If
    Rs1.Close
    
    'RETORNA APLICACAO_ID
    aplicacao_id = ""
    sSql = "SELECT * FROM er_aplicacao WHERE codigo = " & BL_TrataStringParaBD(cod_origem_xml)
    Rs1.CursorLocation = adUseServer
    Rs1.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    Rs1.Open sSql, gConexaoEresults
    If Rs1.RecordCount >= 1 Then
        aplicacao_id = BL_HandleNull(Rs1!aplicacao_id, "")
    End If
    ER_DevolveAplicacaoId = aplicacao_id
Exit Function
TrataErro:
    ER_DevolveAplicacaoId = ""
    BG_LogFile_Erros "Modulo ER ER_DevolveAplicacaoId: " & Err.Description, "BL", "ER_DevolveAplicacaoId", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------------

' envia requisi��o por email

' ------------------------------------------------------------------------------------
Public Function EMAIL_EnviaReq(n_req As String) As Boolean
    On Error GoTo TrataErro
    Dim email As String
    Dim sSql As String
    Dim sSqlEmail As String
    Dim rsR As New ADODB.recordset
    Dim pastaEmail As String
    Dim seq_utente As String
    Dim estado_req As String
    Dim nome_ute As String
    Dim flg_html As Integer
    Dim texto_html As String
    Dim flg_recibo As Integer
    Dim flg_cc As Integer
    
    ' VERIFICA SE REQUISI��O EST� MARCADA PARA ENVIAR POR EMAIL
    sSql = "SELECT sl_requis.cod_destino, sl_requis.cod_proven, sl_requis.seq_utente, sl_requis.cod_sala,sl_requis.estado_Req, sl_identif.nome_ute, "
    sSql = sSql & " sl_tbf_t_destino.flg_html, sl_tbf_t_destino.corpo_html, flg_recibo, flg_cc "
    sSql = sSql & " FROM sl_requis, sl_identif, sl_tbf_t_destino WHERE n_Req = " & n_req & " AND sl_Requis.seq_utente = sl_identif.seq_utente "
    sSql = sSql & " AND sl_requis.cod_destino = sl_tbf_t_destino.cod_t_dest"
    rsR.CursorLocation = adUseServer
    rsR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsR.Open sSql, gConexao
    If rsR.RecordCount >= 1 Then
        seq_utente = BL_HandleNull(rsR!seq_utente, "-1")
        estado_req = BL_HandleNull(rsR!estado_req, "")
        nome_ute = BL_HandleNull(rsR!nome_ute, "")
        flg_html = BL_HandleNull(rsR!flg_html, 0)
        texto_html = BL_HandleNull(rsR!corpo_html, "")
        flg_cc = BL_HandleNull(rsR!flg_cc, 0)
        flg_recibo = BL_HandleNull(rsR!flg_recibo, 0)
        
        If BL_HandleNull(rsR!cod_destino, "") = 3 Then
            sSqlEmail = "SELECT email from sl_identif where seq_utente = " & BL_HandleNull(rsR!seq_utente, "")
        ElseIf BL_HandleNull(rsR!cod_destino, "") = 4 Then
            sSqlEmail = "SELECT email from sl_cod_salas where cod_Sala = " & BL_HandleNull(rsR!cod_sala, "")
        ElseIf BL_HandleNull(rsR!cod_destino, "") = 5 Then
            sSqlEmail = "SELECT email from sl_proven where cod_proven= " & BL_HandleNull(rsR!cod_proven, "")
        Else
            BG_Mensagem mediMsgBox, "A requisi��o n�o est� marcada para enviar por email!", vbExclamation, "Email n�o enviado"
            EMAIL_EnviaReq = False
            rsR.Close
            Exit Function
        End If
    Else
        EMAIL_EnviaReq = False
        Exit Function
    End If
    rsR.Close
    
    ' VERIFICA SE EXISTEM ANALISES PENDENTES
    sSql = "SELECT n_Req from sl_marcacoes where n_Req = " & n_req
    sSql = sSql & " UNION select n_req from sl_realiza WHERE n_req = " & n_req & " AND cod_ana_s <> 'S99999' and flg_estado not in (3,4) "
    rsR.CursorLocation = adUseServer
    rsR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsR.Open sSql, gConexao
    If rsR.RecordCount >= 1 Then
        gMsgResp = BG_Mensagem(mediMsgBox, "A requisi��o tem an�lises pendentes, deseja enviar mesmo assim?", vbYesNo + vbDefaultButton2 + vbQuestion, "Envio por Email")
        If gMsgResp <> vbYes Then
            EMAIL_EnviaReq = False
            rsR.Close
            Exit Function
        End If
    End If
    rsR.Close
    
    rsR.CursorLocation = adUseServer
    rsR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsR.Open sSqlEmail, gConexao
    If rsR.RecordCount >= 1 Then
        If BL_HandleNull(rsR!email, "") = "" Then
            BG_Mensagem mediMsgBox, "N�o existe endere�o de email associado!", vbExclamation, "Email n�o enviado"
            EMAIL_EnviaReq = False
            rsR.Close
            Exit Function
        Else
            email = BL_HandleNull(rsR!email, "")
        End If
    End If
    rsR.Close
    
    If BL_FileExists(gDirCliente & "\eMail\") = False Then
        MkDir gDirCliente & "\eMail"
        FileCopy gDirServidor & "\bin\MapaResultadosSimples.rpt", gDirCliente & "\eMail\Teste"
    End If
    pastaEmail = gDirCliente & "\eMail\" & Bg_DaData_ADO & "_" & Replace(time, ":", "")
    If BL_FileExists(pastaEmail) = False Then
        MkDir pastaEmail
    End If
    EMAIL_Imprime pastaEmail, n_req, estado_req, seq_utente, email, nome_ute, flg_html, texto_html, flg_recibo, flg_cc
    
    Set rsR = Nothing
Exit Function
TrataErro:
    EMAIL_EnviaReq = False
    BG_LogFile_Erros "Modulo ER EMAIL_EnviaReq: " & Err.Description, "er", "EMAIL_EnviaReq", True
    Exit Function
    Resume Next
End Function

Private Function EMAIL_Imprime(pastaEmail As String, n_req As String, estado_req As String, _
                               seq_utente As String, email As String, nome_ute As String, flg_html As Integer, _
                               texto_html As String, flg_recibo As Integer, flg_cc As Integer) As Boolean
    Dim i As Long
    Dim j As Long
    Dim sSql As String
    Dim rsEmail As New ADODB.recordset
    Dim mensagem As String
    Dim ficheiros() As String
    Dim assunto As String
    Dim pdfjob As PDFCreator.clsPDFCreator
    Dim pSeqEmail As Long
    Dim dataActual As String
    Dim horaActual As String
    Dim dono As String
    Dim nomeMedico As String
    On Error GoTo TrataErro
    ReDim ficheiros(0)
    EMAIL_Imprime = False
    
    sSql = "SELECT max(seq_email) +1 maximo from sl_email_resultados "
    rsEmail.CursorLocation = adUseServer
    rsEmail.CursorType = adOpenStatic
    rsEmail.Open sSql, gConexao
    If rsEmail.RecordCount > 0 Then
        pSeqEmail = BL_HandleNull(rsEmail!maximo, 0)
    Else
        BG_Mensagem mediMsgBox, "Erro ao gerar sequ�ncial.", vbCritical, " N�o � possivel enviar eMail."
        Exit Function
    End If
    rsEmail.Close
    
    If gTipoInstituicao = gTipoInstituicaoVet Then
        sSql = "SELECT x3.descr_genero, x2.descr_medico, x1.nome_dono FROM sl_identif x1, sl_requis x2,  sl_genero x3 WHERE x1.seq_utente = X2.seq_utente AND x1.cod_genero = x3.cod_genero AND x2.n_Req = " & n_req
        rsEmail.CursorLocation = adUseServer
        rsEmail.CursorType = adOpenStatic
        rsEmail.Open sSql, gConexao
        If rsEmail.RecordCount > 0 Then
            dono = BL_HandleNull(rsEmail!nome_dono, "")
            nomeMedico = BL_HandleNull(rsEmail!descr_medico, "")
        End If
        rsEmail.Close
    End If
    
    mensagem = ""
    If flg_html = mediSim Then
        mensagem = texto_html
    End If
    gImprimirDestino = crptToPrinter

    ' COPIA O RPT
    If gUsaMesmoRelatorioEmail <> mediSim Then
        BL_CopiaRelatorioEmail "MapaResultadosSimplesEmail.rpt", n_req & ".rpt"
    Else
        BL_CopiaRelatorioEmail "MapaResultadosSimples.rpt", n_req & ".rpt"
    End If
    ' IMPRIME O PDF
                
    EMAIL_IniciaPDFCreator pdfjob, gDirCliente & "\eMail\", n_req & ".pdf", "", ""
                
    If IR_ImprimeResultados(False, False, n_req, estado_req, seq_utente, , , , True, , , , , , , False, True, , , , , False) = True Then
        EMAIL_FechaPdfCreator pdfjob
                    
        j = 0
        While BL_IsFileDistilledYet(n_req & ".pdf", gDirCliente & "\eMail\") = False And j < 10
            Sleep 1000
            j = j + 1
        Wend
        If j < 10 Then
            If BL_FileExists(gDirCliente & "\eMail\" & n_req & ".pdf") Then
                If BL_FileExists(pastaEmail & "\" & n_req & ".pdf") Then
                    FileCopy pastaEmail & "\" & n_req & ".pdf", pastaEmail & "\" & n_req & "_" & Bg_DaData_ADO & "_" & Replace(Bg_DaHora_ADO, ":", "") & ".pdf"
                    Kill pastaEmail & "\" & n_req & ".pdf"
                End If
                FileCopy gDirCliente & "\eMail\" & n_req & ".pdf", pastaEmail & "\" & n_req & ".pdf"
                Kill gDirCliente & "\eMail\" & n_req & ".pdf"
            End If
        End If
        ReDim Preserve ficheiros(UBound(ficheiros) + 1)
        ficheiros(UBound(ficheiros)) = n_req & ".pdf"
        dataActual = Bg_DaData_ADO
        horaActual = Bg_DaHora_ADO
        
        ' INSERE REGISTO DO ENVIO NA TABELA
        sSql = "INSERT INTO sl_email_resultados (seq_email,n_req,agrega, destino,assunto,mensagem, localizacao_anexo,estado,user_cri,dt_cri, hr_cri,destino_impr, estado_Req) VALUES ("
        sSql = sSql & pSeqEmail & ", " & n_req & ",0, " & BL_TrataStringParaBD(email) & "," & BL_TrataStringParaBD(assunto) & "," & BL_TrataStringParaBD(Mid(mensagem, 1, 2000)) & ", "
        sSql = sSql & BL_TrataStringParaBD(pastaEmail & "\" & n_req & ".pdf") & ", 0, " & BL_TrataStringParaBD(CStr(gCodUtilizador))
        sSql = sSql & ", " & BL_TrataDataParaBD(dataActual) & ", " & BL_TrataStringParaBD(horaActual) & "," & gImprimirDestino & "," & BL_TrataStringParaBD(estado_req) & " )"
        BG_ExecutaQuery_ADO sSql
        
        EMAIL_ActualizaRequis dataActual, horaActual, n_req
    Else
        EMAIL_FechaPdfCreator pdfjob
        BG_Mensagem mediMsgBox, "Erro ao gerar PDF.", vbCritical, " N�o � possivel enviar eMail."
        EMAIL_Imprime = False
        Exit Function
    End If
    
    If UBound(ficheiros) = 1 Then
        assunto = "Relat�rio(s) de An�lises "
        assunto = "Relat�rio de An�lises de " & " - " & nome_ute
        If gTipoInstituicao = gTipoInstituicaoVet Then
            If dono <> "" Then
                assunto = assunto & " - " & dono
            End If
            
            If nomeMedico <> "" Then
                assunto = assunto & " - " & nomeMedico
            End If
        End If
    End If
    If UBound(ficheiros) > 0 Then
        BL_EnviaEmail email, assunto, mensagem, True, pastaEmail, ficheiros, flg_html, "", "", flg_recibo, flg_cc, "RESULTADOS", ""
    End If
    
    EMAIL_Imprime = True
Exit Function
TrataErro:
    EMAIL_Imprime = False
    BG_LogFile_Erros "Erro ao Gerar Email " & Err.Description, "ER", "EMAIL_Imprime "
    Exit Function
    Resume Next
End Function

Public Sub EMAIL_IniciaPDFCreator(pdfjob As PDFCreator.clsPDFCreator, sPDFPath As String, sPDFName As String, _
                             sMasterPass As String, sUserPass As String)
    
    On Error GoTo TrataErro
    
    '/// Change the output file names and passwords (if security req'd)! ///


    'Check if worksheet is empty and exit if so

    Set pdfjob = New PDFCreator.clsPDFCreator

    With pdfjob
        If .cStart("/NoProcessingAtStartup") = False Then
            pdfjob.cClose
            If .cStart("/NoProcessingAtStartup") = False Then
                MsgBox "Can't initialize PDFCreator.", vbCritical + _
                        vbOKOnly, "PrtPDFCreator"
                Exit Sub
                
            End If
        End If

        'Set details on where to save file to, and flag it automatic
        .cOption("UseAutosave") = 1
        .cOption("UseAutosaveDirectory") = 1
        .cOption("AutosaveDirectory") = sPDFPath
        .cOption("AutosaveFilename") = sPDFName
        .cOption("AutosaveFormat") = 0    ' 0 = PDF
        
        'The following are required to set security of any kind
        .cOption("PDFUseSecurity") = 1
        If sMasterPass <> "" Then
            .cOption("PDFOwnerPass") = 1
            .cOption("PDFOwnerPasswordString") = sMasterPass
        Else
            .cOption("PDFOwnerPass") = 0
            .cOption("PDFOwnerPasswordString") = ""
        End If
        

        'To set individual security options
        .cOption("PDFDisallowCopy") = 1
        .cOption("PDFDisallowModifyContents") = 1
        .cOption("PDFDisallowPrinting") = 1

        'To force a user to enter a password before opening
        If sUserPass <> "" Then
            .cOption("PDFUserPass") = 1
            .cOption("PDFUserPasswordString") = sUserPass
        Else
            .cOption("PDFUserPass") = 0
            .cOption("PDFUserPasswordString") = ""
        End If
        

        'To change to High encryption
        .cOption("PDFHighEncryption") = 1
    
        'Get ready for the print job
        .cClearCache
    End With


Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Parametrizar PDFCreator" & Err.Description, "ER", "EMAIL_IniciaPDFCreator"
    Exit Sub
    Resume Next
End Sub

Public Sub EMAIL_FechaPdfCreator(pdfjob As PDFCreator.clsPDFCreator)
    On Error GoTo TrataErro
    
    'Wait until the print job has entered the print queue
    Do Until pdfjob.cCountOfPrintjobs = 1
        DoEvents
    Loop
    pdfjob.cPrinterStop = False

    'Wait until PDF creator is finished then release the objects
    Do Until pdfjob.cCountOfPrintjobs = 0
        DoEvents
    Loop
    pdfjob.cClose
    Set pdfjob = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Fechar PDFCreator" & Err.Description, "ER", "EMAIL_FechaPdfCreator"
    Exit Sub
    Resume Next
End Sub


Public Sub EMAIL_ActualizaRequis(ByVal dataActual As String, ByVal horaActual As String, ByVal n_req As String)
    Dim sSql As String
    On Error GoTo TrataErro
    'ACTUALIZA TABELA SL_REQUIS
    sSql = "UPDATE sl_requis SET user_email = " & BL_TrataStringParaBD(CStr(gCodUtilizador))
    sSql = sSql & ", dt_email =" & BL_TrataDataParaBD(dataActual) & ", hr_email = " & BL_TrataStringParaBD(horaActual)
    sSql = sSql & " WHERE n_req = " & n_req
    BG_ExecutaQuery_ADO sSql
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro EMAIL_ActualizaRequis" & Err.Description, "ER", "EMAIL_ActualizaRequis"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------------------

' CONSTRU��O DO XML

' ----------------------------------------------------------------------------------------------

Public Function ER_CriacaoUtilizador(ByVal num_sns As String, ByVal email As String, nome As String, profile As String)

    Dim seq_log_rnu As Long
    Dim strSoapAction As String
    Dim strXml As String
    Dim RetornoXML As String
    Dim urleR As String
    Dim password As String
    On Error GoTo TrataErro
    urleR = BL_DevolveURLWebService("ERESULTS")
    
    'gEResultsWebService = "http://srv-bd.coimbralab.local:8661/Glintths.eResults.UserAdministration/UserAdministration.asmx"
    If urleR = "" Then
        Exit Function
    End If
    strSoapAction = "http://tempuri.org/CreateUser"
    
    strXml = ""
    strXml = strXml & "<?xml version=""1.0"" encoding=""utf-8""?>"
    strXml = strXml & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
    strXml = strXml & "     <soap:Body>"
    strXml = strXml & "         <CreateUser xmlns=""http://tempuri.org/"">"
    strXml = strXml & "             <username>" & num_sns & "</username>"
    strXml = strXml & "             <name>" & nome & "</name>"
    strXml = strXml & "             <profile>" & profile & "</profile>"
    strXml = strXml & "             <email>" & email & "</email>"
    strXml = strXml & "         </CreateUser>"
    strXml = strXml & "     </soap:Body>"
    strXml = strXml & "</soap:Envelope>"

    seq_log_rnu = RNU_InsereLog(strXml, urleR)
    If seq_log_rnu > mediComboValorNull Then
        RetornoXML = RNU_PostWebservice(urleR, strSoapAction, strXml)
        RNU_ActualizaLog seq_log_rnu, RetornoXML
        password = RNU_RetornaConteudoTag(RetornoXML, "CreateUserResult")
        REST_webservice "username=" & num_sns & "&password=" & password
    End If
    ER_CriacaoUtilizador = idRNU
Exit Function
TrataErro:
    BG_LogFile_Erros "ER_CriacaoUtilizador: " & Err.Description, "eR", "ER_CriacaoUtilizador", True
    Exit Function
    Resume Next

End Function

'RGONCALVES 29.07.2013 LCC-301
Public Function ER_ResetPasswordUtilizador(ByVal num_sns As String)

    Dim seq_log_rnu As Long
    Dim strSoapAction As String
    Dim strXml As String
    Dim RetornoXML As String
    Dim urleR As String
    
    On Error GoTo TrataErro
    urleR = BL_DevolveURLWebService("ERESULTS")
    
    If urleR = "" Then
        Exit Function
    End If
    strSoapAction = "http://tempuri.org/ResetPassword"
    
    strXml = ""
    strXml = strXml & "<?xml version=""1.0"" encoding=""utf-8""?>"
    strXml = strXml & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
    strXml = strXml & "     <soap:Body>"
    strXml = strXml & "         <ResetPassword xmlns=""http://tempuri.org/"">"
    strXml = strXml & "             <username>" & num_sns & "</username>"
    strXml = strXml & "         </ResetPassword>"
    strXml = strXml & "     </soap:Body>"
    strXml = strXml & "</soap:Envelope>"
    
    seq_log_rnu = RNU_InsereLog(strXml, urleR)
    If seq_log_rnu > mediComboValorNull Then
        RetornoXML = RNU_PostWebservice(urleR, strSoapAction, strXml)
        RNU_ActualizaLog seq_log_rnu, RetornoXML
        password = RNU_RetornaConteudoTag(RetornoXML, "ResetPasswordResult")
        REST_webservice "username=" & num_sns & "&password=" & password
    End If
    ER_ResetPasswordUtilizador = idRNU
Exit Function
TrataErro:
    BG_LogFile_Erros "ER_ResetPasswordUtilizador: " & Err.Description, "eR", "ER_ResetPasswordUtilizador", True
    Exit Function
    Resume Next

End Function


VERSION 5.00
Begin VB.Form FormIncRes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormIncRes"
   ClientHeight    =   5130
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8550
   Icon            =   "FormIncRes.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5130
   ScaleWidth      =   8550
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPesqRapAnaS2 
      Height          =   285
      Left            =   6360
      TabIndex        =   39
      Top             =   3360
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapAnaS1 
      Height          =   285
      Left            =   4200
      TabIndex        =   37
      Top             =   3360
      Width           =   615
   End
   Begin VB.TextBox EcOperVerif2 
      Height          =   285
      Left            =   4200
      TabIndex        =   34
      Top             =   3720
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcOperVerif1 
      Height          =   285
      Left            =   1680
      TabIndex        =   32
      Top             =   3720
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   27
      Top             =   4440
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4200
      TabIndex        =   26
      Top             =   4440
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   25
      Top             =   4080
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4200
      TabIndex        =   24
      Top             =   4080
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.Frame Frame4 
      Height          =   825
      Left            =   120
      TabIndex        =   18
      Top             =   1800
      Width           =   8295
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   40
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label14 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   195
         Width           =   675
      End
      Begin VB.Label Label13 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   480
         Width           =   705
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   21
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   20
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   19
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   1680
      TabIndex        =   16
      Top             =   3360
      Width           =   975
   End
   Begin VB.TextBox EcRes2 
      BeginProperty DataFormat 
         Type            =   0
         Format          =   "99,999"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2070
         SubFormatType   =   0
      EndProperty
      Height          =   285
      Left            =   2400
      TabIndex        =   14
      Top             =   1320
      Width           =   855
   End
   Begin VB.ComboBox EcOperador2 
      Height          =   315
      Left            =   1200
      Style           =   2  'Dropdown List
      TabIndex        =   13
      Top             =   1320
      Width           =   855
   End
   Begin VB.CommandButton BtPesqAnaS2 
      Height          =   375
      Left            =   7920
      Picture         =   "FormIncRes.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Pesquisa R�pida �s An�lises Simples"
      Top             =   920
      Width           =   375
   End
   Begin VB.TextBox EcDescrAnaS2 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2880
      TabIndex        =   10
      Top             =   950
      Width           =   4935
   End
   Begin VB.TextBox EcCodAnaS2 
      Height          =   285
      Left            =   2160
      TabIndex        =   9
      Top             =   950
      Width           =   735
   End
   Begin VB.TextBox EcRes1 
      BeginProperty DataFormat 
         Type            =   0
         Format          =   "99,999"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2070
         SubFormatType   =   0
      EndProperty
      Height          =   285
      Left            =   2385
      TabIndex        =   6
      Top             =   480
      Width           =   855
   End
   Begin VB.ComboBox EcOperador1 
      Height          =   315
      Left            =   1200
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   480
      Width           =   855
   End
   Begin VB.CommandButton BtPesqAnaS1 
      Height          =   375
      Left            =   7800
      Picture         =   "FormIncRes.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Pesquisa R�pida �s An�lises Simples"
      Top             =   105
      Width           =   375
   End
   Begin VB.TextBox EcDescrAnaS1 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2760
      TabIndex        =   2
      Top             =   120
      Width           =   4935
   End
   Begin VB.TextBox EcCodAnaS1 
      Height          =   285
      Left            =   2040
      TabIndex        =   1
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "."
      Height          =   270
      Left            =   3360
      TabIndex        =   42
      Top             =   1320
      Width           =   135
   End
   Begin VB.Label Label11 
      Caption         =   ","
      Height          =   270
      Left            =   3360
      TabIndex        =   41
      Top             =   480
      Width           =   135
   End
   Begin VB.Label Label10 
      Caption         =   "EcPesqRapAnaS2"
      Height          =   255
      Left            =   4920
      TabIndex        =   38
      Top             =   3360
      Width           =   1335
   End
   Begin VB.Label Label9 
      Caption         =   "EcPesqRapAnaS1"
      Height          =   255
      Left            =   2760
      TabIndex        =   36
      Top             =   3360
      Width           =   1335
   End
   Begin VB.Label Label8 
      Caption         =   "EcOperVerif2"
      Height          =   255
      Left            =   2760
      TabIndex        =   35
      Top             =   3720
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label21 
      Caption         =   "EcOperVerif1"
      Height          =   255
      Left            =   120
      TabIndex        =   33
      Top             =   3720
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label18 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   120
      TabIndex        =   31
      Top             =   4080
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label17 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   120
      TabIndex        =   30
      Top             =   4440
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label16 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2760
      TabIndex        =   29
      Top             =   4080
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label15 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2760
      TabIndex        =   28
      Top             =   4440
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "EcCodSequencial"
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   3360
      Width           =   1335
   End
   Begin VB.Label Label6 
      Caption         =   "a"
      Height          =   270
      Left            =   2160
      TabIndex        =   15
      Top             =   1320
      Width           =   135
   End
   Begin VB.Label Label5 
      Caption         =   "n�o pode ser "
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   1320
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "ent�o o resultado da An�lise "
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   960
      Width           =   2295
   End
   Begin VB.Label Label4 
      Caption         =   "a"
      Height          =   270
      Left            =   2160
      TabIndex        =   7
      Top             =   480
      Width           =   135
   End
   Begin VB.Label Label2 
      Caption         =   "atingir valores"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Se o resultado da  An�lise "
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2415
   End
End
Attribute VB_Name = "FormIncRes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

'Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String

    gSQLError = 0
    gSQLISAM = 0
    
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_ana_incomp") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    
End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    LimpaCampos
    PreencheCampos

End Sub

Sub DefTipoCampos()

    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
End Sub
Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    BL_FimProcessamento Me
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormIncRes = Nothing
    
End Sub

Sub FuncaoAnterior()

    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub
Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub
Sub FuncaoInserir()

    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao.Text = gCodUtilizador
        EcDataCriacao.Text = Bg_DaData_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            EcOperVerif1.Text = ""
            EcOperVerif1.Text = EcOperador1.Text
            EcOperVerif2.Text = ""
            EcOperVerif2.Text = EcOperador2.Text
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub
Sub FuncaoModificar()

    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            EcOperVerif1.Text = ""
            EcOperVerif1.Text = EcOperador1.Text
            EcOperVerif2.Text = ""
            EcOperVerif2.Text = EcOperador2.Text
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub
Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY " & ChaveBD & " ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub
Sub FuncaoRemover()

    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        EcOperVerif1.Text = EcOperador1.Text
        EcOperVerif2.Text = EcOperador2.Text
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoSeguinte()

    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Sub LimpaCampos()

    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    
    EcOperVerif1.Text = ""
    EcOperVerif2.Text = ""
    EcOperador1.ListIndex = -1
    EcOperador2.ListIndex = -1
    
    EcDescrAnaS1.Text = ""
    EcDescrAnaS2.Text = ""
    
    EcPesqRapAnaS1.Text = ""
    EcPesqRapAnaS2.Text = ""
    
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    
    PreencheValoresDefeito
    
End Sub

Sub PreencheCampos()
    
    Dim descricao As String
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        If rs!oper_verif1 <> "" Then
            EcOperador1.Text = Trim(EcOperVerif1.Text)
        End If
        If rs!oper_verif2 <> "" Then
            EcOperador2.Text = Trim(EcOperVerif2.Text)
        End If
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        Call ExisteCodAnalise(EcCodAnaS1.Text, descricao)
        EcDescrAnaS1.Text = descricao
        Call ExisteCodAnalise(EcCodAnaS2.Text, descricao)
        EcDescrAnaS2.Text = descricao
    End If
 
End Sub

Sub PreencheValoresDefeito()

    EcOperador1.AddItem ">"
    EcOperador1.AddItem ">="
    EcOperador1.AddItem "<"
    EcOperador1.AddItem "<="
    EcOperador1.AddItem "="
    
    EcOperador2.AddItem ">"
    EcOperador2.AddItem ">="
    EcOperador2.AddItem "<"
    EcOperador2.AddItem "<="
    EcOperador2.AddItem "="
    
End Sub

Function ValidaCamposEc() As Integer

    Dim iRes, i As Integer
    
    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Private Sub BtPesqAnaS1_Click()
    

    FormPesquisaRapida.InitPesquisaRapida "sl_ana_s", _
                        "descr_ana_s", "seq_ana_s", _
                        EcPesqRapAnaS1
    
End Sub

Private Sub BtPesqAnaS2_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_ana_s", _
                        "descr_ana_s", "seq_ana_s", _
                        EcPesqRapAnaS2
    
End Sub


Private Sub EcCodAnaS1_KeyPress(KeyAscii As Integer)
    
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    
End Sub

Private Sub EcCodAnaS1_Validate(Cancel As Boolean)
        
    Dim descricao As String
    
    If EcCodAnaS1.Text <> "" Then
        If ExisteCodAnalise(EcCodAnaS1.Text, descricao) = False Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbExclamation, ""
            Sendkeys ("{HOME}+{END}")
        End If
    End If
    
    EcDescrAnaS1.Text = descricao
    
End Sub


Private Sub EcCodAnaS2_KeyPress(KeyAscii As Integer)
    
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    
End Sub

Private Sub EcCodAnaS2_Validate(Cancel As Boolean)
        
    Dim descricao As String
        
    If EcCodAnaS2.Text <> "" Then
        If ExisteCodAnalise(EcCodAnaS2.Text, descricao) = False Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbExclamation, ""
            Sendkeys ("{HOME}+{END}")
        End If
    End If
    
    EcDescrAnaS2.Text = descricao
    
End Sub



Private Sub EcOperador1_Click()
    
    EcOperVerif1.Text = EcOperador1.Text
    
End Sub

Private Sub EcOperador1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcOperador1.ListIndex = -1
End Sub


Private Sub EcOperador2_Click()
    
    EcOperVerif2.Text = EcOperador2.Text
    
End Sub

Private Sub EcOperador2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcOperador2.ListIndex = -1
End Sub


Private Sub EcPesqRapAnaS1_Change()
    
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapAnaS1.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_ana_s,descr_ana_s FROM sl_ana_s WHERE seq_ana_s = " & EcPesqRapAnaS1.Text, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da an�lise!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodAnaS1.Text = rsCodigo!cod_ana_s
            EcDescrAnaS1.Text = rsCodigo!descr_ana_s
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
        EcPesqRapAnaS1.Text = ""
    End If
    
End Sub

Private Sub EcPesqRapAnaS2_Change()
    
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapAnaS1.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_ana_s,descr_ana_s FROM sl_ana_s WHERE seq_ana_s = " & EcPesqRapAnaS2.Text, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da an�lise!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodAnaS2.Text = rsCodigo!cod_ana_s
            EcDescrAnaS2.Text = rsCodigo!descr_ana_s
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
        EcPesqRapAnaS2.Text = ""
    End If
    
End Sub


Private Sub Form_Activate()
    
    EventoActivate
    
End Sub

Private Sub Form_Load()
    
    EventoLoad
    
End Sub


Sub Inicializacoes()

    Me.caption = " Incompatibilidade de Resultados"
    Me.left = 800
    Me.top = 800
    Me.Width = 8610
    Me.Height = 3105
    
    NomeTabela = "sl_ana_incomp"
    Set CampoDeFocus = EcCodAnaS1
    
    NumCampos = 11
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "seq_ana_incomp"
    
    CamposBD(1) = "cod_ana_s1"
    CamposBD(2) = "oper_verif1"
    CamposBD(3) = "res1"
    
    CamposBD(4) = "cod_ana_s2"
    CamposBD(5) = "oper_verif2"
    CamposBD(6) = "res2"
    
    CamposBD(7) = "user_cri"
    CamposBD(8) = "dt_cri"
    CamposBD(9) = "user_act"
    CamposBD(10) = "dt_act"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodAnaS1
    Set CamposEc(2) = EcOperVerif1
    Set CamposEc(3) = EcRes1
    
    Set CamposEc(4) = EcCodAnaS2
    Set CamposEc(5) = EcOperVerif2
    Set CamposEc(6) = EcRes2
    
    Set CamposEc(7) = EcUtilizadorCriacao
    Set CamposEc(8) = EcDataCriacao
    Set CamposEc(9) = EcUtilizadorAlteracao
    Set CamposEc(10) = EcDataAlteracao
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    
    TextoCamposObrigatorios(1) = "C�digo da An�lise"
    TextoCamposObrigatorios(2) = "Operador"
    TextoCamposObrigatorios(3) = "Valor Resultado"
    
    TextoCamposObrigatorios(4) = "C�digo da An�lise"
    TextoCamposObrigatorios(5) = "Operador"
    TextoCamposObrigatorios(6) = "Valor Resultado"
    
        
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_ana_incomp"
    Set ChaveEc = EcCodSequencial
    
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload
    
End Sub


Private Function ExisteCodAnalise(ByVal codAnaS As String, ByRef DescrAnaS As String) As Boolean
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    Set Tabela = New ADODB.recordset

    sql = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s='" & Trim(codAnaS) & "'"
    Tabela.CursorType = adOpenStatic
    Tabela.CursorLocation = adUseServer
    Tabela.Open sql, gConexao

    If Tabela.RecordCount <= 0 Then
        DescrAnaS = ""
        ExisteCodAnalise = False
    Else
        DescrAnaS = Tabela!descr_ana_s
        ExisteCodAnalise = True
    End If
    Tabela.Close
    Set Tabela = Nothing

    
End Function

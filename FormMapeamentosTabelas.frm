VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormMapeamentosTabelas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormMapeamentosTabelas"
   ClientHeight    =   9225
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   14880
   Icon            =   "FormMapeamentosTabelas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9225
   ScaleWidth      =   14880
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcDescricao 
      Height          =   375
      Left            =   1440
      TabIndex        =   11
      Top             =   8760
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcCodigo 
      Height          =   375
      Left            =   240
      TabIndex        =   10
      Top             =   8880
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox EcCodTipoTabela 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1560
      TabIndex        =   8
      Top             =   600
      Width           =   735
   End
   Begin VB.CommandButton BtPesquisaTipoTabela 
      Height          =   315
      Left            =   6000
      Picture         =   "FormMapeamentosTabelas.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   600
      Width           =   375
   End
   Begin VB.TextBox EcDescrTipoTabela 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      Locked          =   -1  'True
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   600
      Width           =   3735
   End
   Begin VB.TextBox EcCodTipoMapeamento 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1560
      TabIndex        =   4
      Top             =   240
      Width           =   735
   End
   Begin VB.CommandButton BtPesquisaTipoMapeamento 
      Height          =   315
      Left            =   6000
      Picture         =   "FormMapeamentosTabelas.frx":0396
      Style           =   1  'Graphical
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   240
      Width           =   375
   End
   Begin VB.TextBox EcDescrTipoMapeamento 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      Locked          =   -1  'True
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   240
      Width           =   3735
   End
   Begin MSFlexGridLib.MSFlexGrid FgMapeamentos 
      Height          =   6495
      Left            =   120
      TabIndex        =   1
      Top             =   1080
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   11456
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
      Appearance      =   0
   End
   Begin VB.Label Label1 
      Caption         =   "Tipo Tabela"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   600
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Tipo Mapeamento"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   5
      Top             =   240
      Width           =   1335
   End
   Begin VB.Label LbInfoAdicional 
      BackColor       =   &H80000000&
      Height          =   6375
      Left            =   10440
      TabIndex        =   0
      Top             =   1080
      Width           =   4215
   End
End
Attribute VB_Name = "FormMapeamentosTabelas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type tabela_det
    chave As String
    valor As String
End Type

Private Type valor
    seq_mapeamento As Long
    Codigo As String
    descricao As String
    
    seq_tabela As Long
    cod_tabela As String
    descr_tabela As String
    estrutDet() As tabela_det
    totalDet As Integer
End Type

Private Type tipoMapeamento
    Codigo As String
    descricao As String
    Tabela As String
    cod_tabela As String
    descr_tabela As String
    estrutValor() As valor
    totalValor As Integer
End Type
Dim tipoMap As tipoMapeamento
Dim estado As Integer

Private Sub FgMapeamentos_Click()
    Dim i As Integer
    LbInfoAdicional.caption = ""
    If FgMapeamentos.row < FgMapeamentos.rows And FgMapeamentos.row > 0 Then
        If tipoMap.estrutValor(FgMapeamentos.row).totalDet > 0 Then
            For i = 1 To tipoMap.estrutValor(FgMapeamentos.row).totalDet
                LbInfoAdicional.caption = LbInfoAdicional.caption & tipoMap.estrutValor(FgMapeamentos.row).estrutDet(i).chave & ":" & vbCrLf
                LbInfoAdicional.caption = LbInfoAdicional.caption & "        " & tipoMap.estrutValor(FgMapeamentos.row).estrutDet(i).valor & vbCrLf
            Next i
        End If
    End If
End Sub

Private Sub FgMapeamentos_DblClick()
    Dim sSql As String
    Dim RsDet As New ADODB.recordset
    If FgMapeamentos.row < FgMapeamentos.rows Then
        If EcCodTipoMapeamento.Text <> "" And EcCodTipoTabela.Text <> "" Then
        
            EcCodigo.Text = ""
            EcDescricao.Text = ""
            PA_PesquisaGen "cod_tabela", "descr_tabela", "sl_tabelas", EcCodigo, EcDescricao, _
                           "Mapeamentos: " & EcDescrTipoTabela, " cod_tipo_tabela = " & BL_TrataStringParaBD(EcCodTipoTabela.Text)
            If EcCodigo.Text <> "" Then
                tipoMap.estrutValor(FgMapeamentos.row).cod_tabela = EcCodigo.Text
                tipoMap.estrutValor(FgMapeamentos.row).descr_tabela = EcDescricao.Text
                FgMapeamentos.TextMatrix(FgMapeamentos.row, 2) = tipoMap.estrutValor(FgMapeamentos.row).cod_tabela
                FgMapeamentos.TextMatrix(FgMapeamentos.row, 3) = tipoMap.estrutValor(FgMapeamentos.row).descr_tabela
                If tipoMap.estrutValor(FgMapeamentos.row).seq_mapeamento > mediComboValorNull Then
                    AtualizaMapeamento FgMapeamentos.row
                Else
                    InsereMapeamento FgMapeamentos.row
                End If
                tipoMap.estrutValor(FgMapeamentos.row).totalDet = 0
                ReDim tipoMap.estrutValor(FgMapeamentos.row).estrutDet(0)
                If tipoMap.estrutValor(FgMapeamentos.row).cod_tabela <> "" Then
                    sSql = "SELECT * FROM sl_tabelas_det WHERE seq_tabela = (SELECT seQ_tabela FROM sl_tabelas "
                    sSql = sSql & " WHERE cod_tabela =" & BL_TrataStringParaBD(tipoMap.estrutValor(FgMapeamentos.row).cod_tabela) & ")"
                    RsDet.ActiveConnection = gConexao
                    RsDet.CursorType = adOpenStatic
                    RsDet.CursorLocation = adUseServer
                    RsDet.LockType = adLockReadOnly
                    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                    RsDet.Open sSql
                    If RsDet.RecordCount > 0 Then
                        While Not RsDet.EOF
                            tipoMap.estrutValor(FgMapeamentos.row).totalDet = tipoMap.estrutValor(FgMapeamentos.row).totalDet + 1
                            ReDim Preserve tipoMap.estrutValor(FgMapeamentos.row).estrutDet(tipoMap.estrutValor(FgMapeamentos.row).totalDet)
                            tipoMap.estrutValor(FgMapeamentos.row).estrutDet(tipoMap.estrutValor(FgMapeamentos.row).totalDet).chave = BL_HandleNull(RsDet!chave, "")
                            tipoMap.estrutValor(FgMapeamentos.row).estrutDet(tipoMap.estrutValor(FgMapeamentos.row).totalDet).valor = BL_HandleNull(RsDet!valor, "")
                            RsDet.MoveNext
                        Wend
                    End If
                    RsDet.Close
                    Set RsDet = Nothing
                End If
                
            End If
        End If
    End If
End Sub
Private Sub AtualizaMapeamento(indice As Long)
    Dim sSql As String
    sSql = "UPDATE sl_mapeamentos SET seq_tabela = (SELECT seq_tabela FROM sl_tabelas WHERE cod_tipo_tabela = " & BL_TrataStringParaBD(EcCodTipoTabela.Text)
    sSql = sSql & " AND cod_tabela = " & BL_TrataStringParaBD(tipoMap.estrutValor(indice).cod_tabela) & ")"
    sSql = sSql & " WHERE seq_mapeamento = " & tipoMap.estrutValor(indice).seq_mapeamento
    BG_ExecutaQuery_ADO sSql
End Sub

Private Sub InsereMapeamento(indice As Long)
    Dim sSql As String
    sSql = "INSERT INTO sl_mapeamentos (seq_mapeamento,cod_tipo_mapeamento,cod_mapeamento, seq_tabela, user_cri, dt_cri ) VALUES("
    sSql = sSql & " seq_mapeamento.nextval, " & BL_TrataStringParaBD(tipoMap.Codigo) & "," & BL_TrataStringParaBD(tipoMap.estrutValor(indice).Codigo) & ","
    sSql = sSql & " (SELECT seq_tabela FROM sl_tabelas WHERE cod_tipo_tabela = " & BL_TrataStringParaBD(EcCodTipoTabela.Text)
    sSql = sSql & " AND cod_tabela = " & BL_TrataStringParaBD(tipoMap.estrutValor(indice).cod_tabela) & ")," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ",SYSDATE)"
    BG_ExecutaQuery_ADO sSql
End Sub
Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Tabelas de mapeamentos"
    Me.left = 50
    Me.top = 50
    Me.Width = 14970
    Me.Height = 8430 ' Normal
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormMapeamentosTabelas = Nothing
End Sub



Sub DefTipoCampos()
    With FgMapeamentos
        .rows = 2
        .FixedRows = 1
        .Cols = 4
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        
        .ColWidth(0) = 1300
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, 0) = "C�digo"
        
        .ColWidth(1) = 3000
        .Col = 1
        .ColAlignment(1) = flexAlignLeftCenter
        .TextMatrix(0, 1) = "descri��o"
        
        .ColWidth(2) = 1300
        .Col = 2
        .ColAlignment(2) = flexAlignLeftCenter
        .TextMatrix(0, 2) = "C�digo"
        
        .ColWidth(3) = 4000
        .Col = 3
        .ColAlignment(3) = flexAlignLeftCenter
        .TextMatrix(0, 3) = "descri��o"
        .WordWrap = False
        .row = 0
        .Col = 0
    End With

End Sub

Sub PreencheCampos()
    

End Sub


Sub FuncaoLimpar()
    LbInfoAdicional.caption = ""
    EcCodTipoMapeamento.Text = ""
    EcCodTipoMapeamento_Validate False
    EcCodTipoTabela.Text = ""
    EcCodTipotabela_Validate False
End Sub

Sub FuncaoEstadoAnterior()

End Sub



Sub FuncaoProcurar()
    
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()


End Sub



Sub FuncaoModificar()


End Sub



Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub






Private Sub BtPesquisaTipoMapeamento_Click()
PA_PesquisaGen "cod_tipo_mapeamento", "descr_tipo_mapeamento", "sl_tbf_tipos_mapeamento", EcCodTipoMapeamento, EcDescrTipoMapeamento, "Tipos de Mapeamento", ""
ValidaPreRequisitos
End Sub

Private Sub EcCodTipoMapeamento_Validate(Cancel As Boolean)
    Cancel = PA_ValidateGen("cod_tipo_mapeamento", "descr_tipo_mapeamento", "sl_tbf_tipos_mapeamento", EcCodTipoMapeamento, EcDescrTipoMapeamento, "Tipos de Mapeamento")
ValidaPreRequisitos
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodTipoMapeamento_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodTipoMapeamento_Validate"
    Exit Sub
    Resume Next
End Sub
Private Sub BtPesquisaTipotabela_Click()
PA_PesquisaGen "cod_tipo_tabela", "descr_tipo_tabela", "sl_tbf_tipos_tabela", EcCodTipoTabela, EcDescrTipoTabela, "Tipos de tabela", ""
ValidaPreRequisitos
End Sub

Private Sub EcCodTipotabela_Validate(Cancel As Boolean)
    Cancel = PA_ValidateGen("cod_tipo_tabela", "descr_tipo_tabela", "sl_tbf_tipos_tabela", EcCodTipoTabela, EcDescrTipoTabela, "Tipos de tabela")
    ValidaPreRequisitos
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodTipotabela_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodTipotabela_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub ValidaPreRequisitos()
    Dim i As Integer
    tipoMap.cod_tabela = ""
    tipoMap.Codigo = ""
    tipoMap.descr_tabela = ""
    tipoMap.descricao = ""
    tipoMap.Tabela = ""
    tipoMap.totalValor = 0
    ReDim tipoMap.estrutValor(0)
    
    FgMapeamentos.rows = 2
    For i = 0 To FgMapeamentos.Cols - 1
        FgMapeamentos.TextMatrix(1, i) = ""
    Next
    If EcCodTipoMapeamento.Text <> "" And EcCodTipoTabela.Text <> "" Then
        PreencheValores
    End If
End Sub


Private Sub PreencheValores()
    Dim sSql As String
    Dim rsTipoMap As New ADODB.recordset
    Dim rsMape As New ADODB.recordset
    Dim RsDet As New ADODB.recordset
    
    On Error GoTo TrataErro
    sSql = "SELECT * FROM sl_tbf_tipos_mapeamento WHERE cod_tipo_mapeamento = " & BL_TrataStringParaBD(EcCodTipoMapeamento.Text)
   
    rsTipoMap.ActiveConnection = gConexao
    rsTipoMap.CursorType = adOpenStatic
    rsTipoMap.CursorLocation = adUseServer
    rsTipoMap.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTipoMap.Open sSql
    If rsTipoMap.RecordCount = 1 Then
        tipoMap.cod_tabela = BL_HandleNull(rsTipoMap!cod_tabela, "")
        tipoMap.Codigo = BL_HandleNull(rsTipoMap!cod_tipo_mapeamento, "")
        tipoMap.descr_tabela = BL_HandleNull(rsTipoMap!descr_tabela, "")
        tipoMap.descricao = BL_HandleNull(rsTipoMap!descr_tipo_mapeamento, "")
        tipoMap.Tabela = BL_HandleNull(rsTipoMap!Tabela, "")
        tipoMap.totalValor = 0
        ReDim tipoMap.estrutValor(0)
        sSql = "select x1." & tipoMap.cod_tabela & " codigo , x1." & tipoMap.descr_tabela & " descricao, x3.cod_tabela, x3.descR_tabela, x2.seq_mapeamento from "
        sSql = sSql & tipoMap.Tabela & " x1 left outer join sl_mapeamentos x2 on TO_CHAR( x1." & tipoMap.cod_tabela & " ) = x2.cod_mapeamento "
        sSql = sSql & " left outer join sl_tabelas x3 on x3.seq_tabela = x2.seq_tabela order by x1." & tipoMap.descr_tabela
        rsMape.ActiveConnection = gConexao
        rsMape.CursorType = adOpenStatic
        rsMape.CursorLocation = adUseServer
        rsMape.LockType = adLockReadOnly
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsMape.Open sSql
        If rsMape.RecordCount > 0 Then
            FgMapeamentos.Visible = False
            DoEvents
            While Not rsMape.EOF
                tipoMap.totalValor = tipoMap.totalValor + 1
                ReDim Preserve tipoMap.estrutValor(tipoMap.totalValor)
                tipoMap.estrutValor(tipoMap.totalValor).seq_mapeamento = BL_HandleNull(rsMape!seq_mapeamento, mediComboValorNull)
                tipoMap.estrutValor(tipoMap.totalValor).Codigo = BL_HandleNull(rsMape!Codigo, "")
                tipoMap.estrutValor(tipoMap.totalValor).descricao = BL_HandleNull(rsMape!descricao, "")
                tipoMap.estrutValor(tipoMap.totalValor).cod_tabela = BL_HandleNull(rsMape!cod_tabela, "")
                tipoMap.estrutValor(tipoMap.totalValor).descr_tabela = BL_HandleNull(rsMape!descr_tabela, "")
                FgMapeamentos.TextMatrix(tipoMap.totalValor, 0) = tipoMap.estrutValor(tipoMap.totalValor).Codigo
                FgMapeamentos.TextMatrix(tipoMap.totalValor, 1) = tipoMap.estrutValor(tipoMap.totalValor).descricao
                FgMapeamentos.TextMatrix(tipoMap.totalValor, 2) = tipoMap.estrutValor(tipoMap.totalValor).cod_tabela
                FgMapeamentos.TextMatrix(tipoMap.totalValor, 3) = tipoMap.estrutValor(tipoMap.totalValor).descr_tabela
                FgMapeamentos.AddItem ""
                
                tipoMap.estrutValor(tipoMap.totalValor).totalDet = 0
                ReDim tipoMap.estrutValor(tipoMap.totalValor).estrutDet(0)
                If tipoMap.estrutValor(tipoMap.totalValor).cod_tabela <> "" Then
                    sSql = "SELECT * FROM sl_tabelas_det WHERE seq_tabela = (SELECT seQ_tabela FROM sl_tabelas "
                    sSql = sSql & " WHERE cod_tabela =" & BL_TrataStringParaBD(tipoMap.estrutValor(tipoMap.totalValor).cod_tabela) & ")"
                    RsDet.ActiveConnection = gConexao
                    RsDet.CursorType = adOpenStatic
                    RsDet.CursorLocation = adUseServer
                    RsDet.LockType = adLockReadOnly
                    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                    RsDet.Open sSql
                    If RsDet.RecordCount > 0 Then
                        While Not RsDet.EOF
                            tipoMap.estrutValor(tipoMap.totalValor).totalDet = tipoMap.estrutValor(tipoMap.totalValor).totalDet + 1
                            ReDim Preserve tipoMap.estrutValor(tipoMap.totalValor).estrutDet(tipoMap.estrutValor(tipoMap.totalValor).totalDet)
                            tipoMap.estrutValor(tipoMap.totalValor).estrutDet(tipoMap.estrutValor(tipoMap.totalValor).totalDet).chave = BL_HandleNull(RsDet!chave, "")
                            tipoMap.estrutValor(tipoMap.totalValor).estrutDet(tipoMap.estrutValor(tipoMap.totalValor).totalDet).valor = BL_HandleNull(RsDet!valor, "")
                            RsDet.MoveNext
                        Wend
                    End If
                    RsDet.Close
                    Set RsDet = Nothing
                End If
                rsMape.MoveNext
            Wend
            FgMapeamentos.Visible = True
            FgMapeamentos.SetFocus
        End If
        rsMape.Close
        Set rsMape = Nothing
        
    End If
    rsTipoMap.Close
    Set rsTipoMap = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheValores: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheValores"
    Exit Sub
    Resume Next
End Sub

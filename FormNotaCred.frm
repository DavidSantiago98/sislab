VERSION 5.00
Begin VB.Form FormNotaCred 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormNotaCred"
   ClientHeight    =   2490
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   5895
   Icon            =   "FormNotaCred.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2490
   ScaleWidth      =   5895
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtCancelar 
      Height          =   615
      Left            =   120
      Picture         =   "FormNotaCred.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Voltar"
      Top             =   1800
      Width           =   735
   End
   Begin VB.CommandButton BtValidar 
      Height          =   615
      Left            =   960
      Picture         =   "FormNotaCred.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   10
      ToolTipText     =   "Gerar Factura Recibo(s)"
      Top             =   1800
      Width           =   735
   End
   Begin VB.TextBox EcDataDoc 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   4320
      TabIndex        =   9
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcNumDoc 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   2400
      TabIndex        =   8
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcSerieDoc 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   840
      TabIndex        =   7
      Top             =   120
      Width           =   975
   End
   Begin VB.ComboBox CbModo 
      Height          =   315
      Left            =   840
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   1320
      Width           =   2655
   End
   Begin VB.ComboBox CbMotivoNotaCred 
      Height          =   315
      Left            =   840
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   720
      Width           =   4455
   End
   Begin VB.Label Label1 
      Caption         =   "Modo"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   4
      Top             =   1320
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "Motivo"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "Data"
      Height          =   255
      Index           =   2
      Left            =   3720
      TabIndex        =   2
      Top             =   120
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "N� "
      Height          =   255
      Index           =   1
      Left            =   2040
      TabIndex        =   1
      Top             =   120
      Width           =   375
   End
   Begin VB.Label Label1 
      Caption         =   "Serie:"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "FormNotaCred"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Estado As Integer

Private Sub BtCancelar_Click()
    If gPassaRecFactus <> mediSim Then
        FormGestaoRequisicaoPrivado.PreencheFgRecibos
    ElseIf gPassaRecFactus = mediSim Then
        FormGestaoRequisicaoPrivado.AtualizaFgRecibosNew
    End If
    Unload Me
End Sub

Private Sub BtValidar_Click()
    If CbModo.ListIndex > mediComboValorNull And CbMotivoNotaCred.ListIndex > mediComboValorNull Then
        If GSerieNotaCred = "" Then
            BG_Mensagem mediMsgBox, "Necess�rio definir uma serie para notas de cr�dito (Chave) !", vbExclamation, "Nota de cr�dito"
        Else
        
            If FACTUS_CreditaFatura(FormGestaoRequisicaoPrivado.FGRecibos.row, CbMotivoNotaCred.ItemData(CbMotivoNotaCred.ListIndex), _
                                CbModo.ItemData(CbModo.ListIndex)) = True Then
                'LJMANSO-328
                AtualizaEstadoMoviFact
                '
                AtualizaDadosDocumentosDoente
                BG_CommitTransaction
                FormGestaoRequisicaoPrivado.PreencheDadosDocumentos
                FACTUS_PreencheRequis FormGestaoRequisicaoPrivado.EcNumReq.text, ""
                FormGestaoRequisicaoPrivado.AtualizaFgRecibosNew
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "Tem que indicar motivo de nota de cr�dito e modo de devolu��o!", vbExclamation, "Nota de cr�dito"
    End If
End Sub

'LJMANSO-328
'UALIA-908 Adiconada a condi��o flg_estado <> 2
Private Sub AtualizaEstadoMoviFact()
   Dim sSql As String
   sSql = "UPDATE fa_movi_fact a SET flg_estado = 1 WHERE a.t_episodio = 'SISLAB' AND a.flg_estado <> 2 AND a.episodio = " & BL_TrataStringParaBD(FormGestaoRequisicaoPrivado.EcNumReq.text)
   BG_ExecutaQuery_ADO sSql
End Sub

Private Sub AtualizaDadosDocumentosDoente()
    Dim sSql As String
    sSql = "INSERT INTO sl_dados_doc (tipo, serie_doc, n_doc, desconto, dt_pagamento, flg_borla) "
    sSql = sSql & " SELECT tipo, serie_doc,n_doc,0, trunc(sysdate), flg_borla  from slv_documentos_doente WHERE (tipo, serie_doc, n_doc) NOT IN ("
    sSql = sSql & " SELECT tipo, serie_doc, n_doc FROM sl_dados_doc) AND episodio = " & BL_TrataStringParaBD(FormGestaoRequisicaoPrivado.EcNumReq.text)
    BG_ExecutaQuery_ADO sSql
    
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Emiss�o de Notas de Cr�dito"
    Me.left = 50
    Me.top = 50
    Me.Width = 5985
    Me.Height = 2925 ' Normal
    
End Sub

Sub EventoLoad()
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    gF_EMISSAO_NOTA_CRED = 1
    
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = FormGestaoRequisicaoPrivado
    FormGestaoRequisicaoPrivado.Enabled = True
    BL_ToolbarEstadoN 2
    Set FormNotaCred = Nothing
    gF_EMISSAO_NOTA_CRED = 0
End Sub



Sub DefTipoCampos()
    BG_PreencheComboBD_ADO "sl_tbf_motivo_nota_cred", "id_mot", "descr_mot", CbMotivoNotaCred, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "SELECT cod_forma_pag, descr_forma_pag FROM sl_forma_pag WHERE flg_invisivel = 0 ", "cod_forma_pag", "descr_forma_pag", CbModo, mediAscComboDesignacao

End Sub

Sub PreencheCampos()
    

End Sub


Sub FuncaoLimpar()
    

End Sub

Sub FuncaoEstadoAnterior()

End Sub



Sub FuncaoProcurar()
    
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()


End Sub



Sub FuncaoModificar()


End Sub



Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub







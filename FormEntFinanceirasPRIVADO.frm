VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormEntFinanceirasPRIVADO 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEntFinanceirasPRIVADO"
   ClientHeight    =   8145
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   13515
   Icon            =   "FormEntFinanceirasPRIVADO.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8145
   ScaleWidth      =   13515
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkInvisivel 
      Caption         =   "Entidade Desactiva"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   11160
      TabIndex        =   136
      Top             =   480
      Width           =   2055
   End
   Begin VB.TextBox EcLinhaPreco 
      Height          =   285
      Left            =   8640
      TabIndex        =   80
      Top             =   8400
      Width           =   615
   End
   Begin VB.TextBox EcColunaPreco 
      Height          =   285
      Left            =   8640
      TabIndex        =   79
      Top             =   8760
      Width           =   615
   End
   Begin VB.TextBox EcCodEmpresa 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3960
      TabIndex        =   75
      Top             =   9120
      Width           =   855
   End
   Begin VB.TextBox EcNIF 
      Appearance      =   0  'Flat
      BeginProperty DataFormat 
         Type            =   0
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2070
         SubFormatType   =   0
      EndProperty
      Height          =   285
      Left            =   11880
      MaxLength       =   9
      TabIndex        =   2
      Top             =   120
      Width           =   1335
   End
   Begin VB.TextBox EcDtRem 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   6240
      TabIndex        =   55
      Top             =   8760
      Width           =   855
   End
   Begin VB.TextBox EcUserRem 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   6240
      TabIndex        =   53
      Top             =   8400
      Width           =   855
   End
   Begin VB.TextBox EcDtAct 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3960
      TabIndex        =   51
      Top             =   8760
      Width           =   855
   End
   Begin VB.TextBox EcUserAct 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3960
      TabIndex        =   49
      Top             =   8400
      Width           =   855
   End
   Begin VB.TextBox EcUserCri 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      TabIndex        =   47
      Top             =   8400
      Width           =   855
   End
   Begin VB.TextBox EcDtCri 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      TabIndex        =   45
      Top             =   8760
      Width           =   855
   End
   Begin TabDlg.SSTab SSTab 
      Height          =   6375
      Left            =   120
      TabIndex        =   37
      Top             =   960
      Width           =   13215
      _ExtentX        =   23310
      _ExtentY        =   11245
      _Version        =   393216
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Entidades"
      TabPicture(0)   =   "FormEntFinanceirasPRIVADO.frx":000C
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "FgEFR"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Codifica��o"
      TabPicture(1)   =   "FormEntFinanceirasPRIVADO.frx":0028
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Label1(12)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Label1(15)"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Label1(16)"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Label1(18)"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Label3(1)"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "CkImprRelARS"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "EcMorada"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "EcCodPostal"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "EcSeqPostal"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "EcLocalidade"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).Control(10)=   "EcTelefone"
      Tab(1).Control(10).Enabled=   0   'False
      Tab(1).Control(11)=   "Frame1"
      Tab(1).Control(11).Enabled=   0   'False
      Tab(1).Control(12)=   "CkCompartDom"
      Tab(1).Control(12).Enabled=   0   'False
      Tab(1).Control(13)=   "CkObrigaRecibo"
      Tab(1).Control(13).Enabled=   0   'False
      Tab(1).Control(14)=   "CkPrecoEnt"
      Tab(1).Control(14).Enabled=   0   'False
      Tab(1).Control(15)=   "CkValidaNumBenef"
      Tab(1).Control(15).Enabled=   0   'False
      Tab(1).Control(16)=   "CkDescrAnaRec"
      Tab(1).Control(16).Enabled=   0   'False
      Tab(1).Control(17)=   "CkInibeImpAna"
      Tab(1).Control(17).Enabled=   0   'False
      Tab(1).Control(18)=   "Ck65Anos"
      Tab(1).Control(18).Enabled=   0   'False
      Tab(1).Control(19)=   "CkPercentagem"
      Tab(1).Control(19).Enabled=   0   'False
      Tab(1).Control(20)=   "CkObrigaNumBenef"
      Tab(1).Control(20).Enabled=   0   'False
      Tab(1).Control(21)=   "CkSeparaDom"
      Tab(1).Control(21).Enabled=   0   'False
      Tab(1).Control(22)=   "CkInibeDomNaoUrb"
      Tab(1).Control(22).Enabled=   0   'False
      Tab(1).Control(23)=   "CkDomicilioDefeito"
      Tab(1).Control(23).Enabled=   0   'False
      Tab(1).Control(24)=   "CkMostraDivida"
      Tab(1).Control(24).Enabled=   0   'False
      Tab(1).Control(25)=   "CkAvisosPagamento"
      Tab(1).Control(25).Enabled=   0   'False
      Tab(1).Control(26)=   "CkControlaIsentos"
      Tab(1).Control(26).Enabled=   0   'False
      Tab(1).Control(27)=   "CkValFixo"
      Tab(1).Control(27).Enabled=   0   'False
      Tab(1).Control(28)=   "CkInsereCodRubrEFR"
      Tab(1).Control(28).Enabled=   0   'False
      Tab(1).Control(29)=   "CkHemodialise"
      Tab(1).Control(29).Enabled=   0   'False
      Tab(1).Control(30)=   "CkNovaArs"
      Tab(1).Control(30).Enabled=   0   'False
      Tab(1).Control(31)=   "CkAdse"
      Tab(1).Control(31).Enabled=   0   'False
      Tab(1).Control(32)=   "Frame2"
      Tab(1).Control(32).Enabled=   0   'False
      Tab(1).Control(33)=   "CbDestino"
      Tab(1).Control(33).Enabled=   0   'False
      Tab(1).Control(34)=   "FrameNovaPortaria"
      Tab(1).Control(34).Enabled=   0   'False
      Tab(1).ControlCount=   35
      TabCaption(2)   =   "Pre��rios"
      TabPicture(2)   =   "FormEntFinanceirasPRIVADO.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "EcDescrRubrPesq"
      Tab(2).Control(1)=   "EcCodRubrPesq"
      Tab(2).Control(2)=   "BtRemove"
      Tab(2).Control(3)=   "BtAdiciona"
      Tab(2).Control(4)=   "EcAuxPreco"
      Tab(2).Control(5)=   "FgPrecos"
      Tab(2).ControlCount=   6
      Begin VB.Frame FrameNovaPortaria 
         Caption         =   "Nova Portaria"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5775
         Left            =   8640
         TabIndex        =   97
         Top             =   480
         Width           =   12975
         Begin VB.TextBox EcNumCopiasRec 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   9600
            TabIndex        =   130
            Top             =   1440
            Width           =   975
         End
         Begin VB.CheckBox CkGravaPrecosNovaPort 
            Caption         =   "Grava Pre�os da Portaria Associada"
            Height          =   195
            Left            =   600
            TabIndex        =   128
            Top             =   3600
            Width           =   4215
         End
         Begin VB.Frame Frame3 
            Height          =   1575
            Left            =   120
            TabIndex        =   116
            Top             =   1920
            Width           =   12495
            Begin VB.TextBox EcValKNovaPort 
               Appearance      =   0  'Flat
               Height          =   285
               Left            =   11400
               TabIndex        =   126
               Top             =   240
               Width           =   975
            End
            Begin VB.TextBox EcValCNovaPort 
               Appearance      =   0  'Flat
               Height          =   285
               Left            =   9120
               TabIndex        =   124
               Top             =   240
               Width           =   975
            End
            Begin VB.TextBox EcCodGrEfrNovaPort 
               Appearance      =   0  'Flat
               Height          =   285
               Left            =   1440
               TabIndex        =   122
               Top             =   240
               Width           =   735
            End
            Begin VB.TextBox EcDescrGrEfrNovaPort 
               Appearance      =   0  'Flat
               Height          =   285
               Left            =   2280
               TabIndex        =   121
               Top             =   240
               Width           =   4815
            End
            Begin VB.CommandButton BtPesqRapGrupRubrNovaPort 
               Height          =   340
               Left            =   4920
               Picture         =   "FormEntFinanceirasPRIVADO.frx":0060
               Style           =   1  'Graphical
               TabIndex        =   120
               ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
               Top             =   840
               Width           =   375
            End
            Begin VB.TextBox EcCodGrRubrNovaPort 
               Appearance      =   0  'Flat
               Height          =   285
               Left            =   1440
               TabIndex        =   118
               Top             =   840
               Width           =   735
            End
            Begin VB.TextBox EcDescrGrRubrNovaPort 
               Appearance      =   0  'Flat
               Height          =   285
               Left            =   2280
               Locked          =   -1  'True
               TabIndex        =   117
               Top             =   840
               Width           =   2535
            End
            Begin VB.Label Label1 
               Caption         =   "Valor K"
               Height          =   255
               Index           =   40
               Left            =   10440
               TabIndex        =   127
               Top             =   240
               Width           =   1335
            End
            Begin VB.Label Label1 
               Caption         =   "Valor C"
               Height          =   255
               Index           =   39
               Left            =   8160
               TabIndex        =   125
               Top             =   240
               Width           =   1335
            End
            Begin VB.Label Label1 
               Caption         =   "Grupo EFR"
               Height          =   255
               Index           =   38
               Left            =   120
               TabIndex        =   123
               Top             =   240
               Width           =   1215
            End
            Begin VB.Label Label1 
               Caption         =   "Grupo R�bricas"
               Height          =   255
               Index           =   37
               Left            =   120
               TabIndex        =   119
               Top             =   840
               Width           =   1215
            End
         End
         Begin VB.CommandButton BtGravaNovaPort 
            Height          =   495
            Left            =   11040
            Picture         =   "FormEntFinanceirasPRIVADO.frx":03EA
            Style           =   1  'Graphical
            TabIndex        =   115
            ToolTipText     =   "Insere R�brica"
            Top             =   5160
            Width           =   735
         End
         Begin VB.CommandButton BtSairNovaPort 
            Height          =   495
            Left            =   12000
            Picture         =   "FormEntFinanceirasPRIVADO.frx":10B4
            Style           =   1  'Graphical
            TabIndex        =   114
            ToolTipText     =   "Sair Sem Gravar"
            Top             =   5160
            Width           =   735
         End
         Begin VB.TextBox EcUserCriNovaPort 
            Height          =   285
            Left            =   1680
            TabIndex        =   112
            Top             =   5400
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.TextBox EcDtFimNovaPort 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   11640
            TabIndex        =   109
            Top             =   960
            Width           =   975
         End
         Begin VB.TextBox EcDtIniNovaPort 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   9600
            TabIndex        =   108
            Top             =   960
            Width           =   975
         End
         Begin VB.ComboBox CbEmpresaNovaPort 
            Height          =   315
            Left            =   9600
            Style           =   2  'Dropdown List
            TabIndex        =   106
            Top             =   360
            Width           =   3015
         End
         Begin VB.TextBox EcDescrNovaPort 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   3120
            TabIndex        =   103
            Top             =   360
            Width           =   4575
         End
         Begin VB.TextBox EcCodNovaPort 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1200
            TabIndex        =   102
            Top             =   360
            Width           =   855
         End
         Begin VB.CommandButton BtPesqRapEFRRefNovaPort 
            Height          =   340
            Left            =   7320
            Picture         =   "FormEntFinanceirasPRIVADO.frx":1D7E
            Style           =   1  'Graphical
            TabIndex        =   100
            ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
            Top             =   960
            Width           =   375
         End
         Begin VB.TextBox EcDescrEfrRefNovaPort 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2160
            TabIndex        =   99
            Top             =   990
            Width           =   5055
         End
         Begin VB.TextBox EcCodEfrRefNovaPort 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1200
            TabIndex        =   98
            Top             =   990
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "C�pias Recibo"
            Height          =   255
            Index           =   41
            Left            =   8400
            TabIndex        =   129
            Top             =   1440
            Width           =   1335
         End
         Begin VB.Label Label1 
            Caption         =   "EcUserCriNovaPort"
            Height          =   255
            Index           =   36
            Left            =   120
            TabIndex        =   113
            Top             =   5400
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.Label Label1 
            Caption         =   "Data Fim"
            Height          =   255
            Index           =   35
            Left            =   10800
            TabIndex        =   111
            Top             =   960
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Data Inicio"
            Height          =   255
            Index           =   34
            Left            =   8400
            TabIndex        =   110
            Top             =   960
            Width           =   1335
         End
         Begin VB.Label Label1 
            Caption         =   "Empresa"
            Height          =   255
            Index           =   33
            Left            =   8400
            TabIndex        =   107
            Top             =   360
            Width           =   1335
         End
         Begin VB.Label Label1 
            Caption         =   "Descri��o"
            Height          =   255
            Index           =   32
            Left            =   2280
            TabIndex        =   105
            Top             =   360
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo"
            Height          =   255
            Index           =   31
            Left            =   240
            TabIndex        =   104
            Top             =   360
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "EFR Ref."
            Height          =   255
            Index           =   30
            Left            =   240
            TabIndex        =   101
            Top             =   990
            Width           =   735
         End
      End
      Begin VB.ComboBox CbDestino 
         Height          =   315
         Left            =   8400
         Style           =   2  'Dropdown List
         TabIndex        =   137
         Top             =   960
         Width           =   1935
      End
      Begin VB.Frame Frame2 
         Caption         =   "Pre�os por Grupo R�brica"
         Height          =   1455
         Left            =   9240
         TabIndex        =   87
         Top             =   4680
         Width           =   3855
         Begin VB.TextBox EcDescrGrEFR 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2160
            TabIndex        =   93
            Top             =   360
            Width           =   1575
         End
         Begin VB.TextBox EcCodGrEFR 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1440
            TabIndex        =   92
            Top             =   360
            Width           =   735
         End
         Begin VB.TextBox EcValK 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1440
            TabIndex        =   90
            Top             =   1080
            Width           =   735
         End
         Begin VB.TextBox EcValC 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1440
            TabIndex        =   88
            Top             =   720
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo EFR"
            Height          =   255
            Index           =   29
            Left            =   480
            TabIndex        =   94
            Top             =   360
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Valor do K"
            Height          =   255
            Index           =   28
            Left            =   480
            TabIndex        =   91
            Top             =   1080
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Valor do C"
            Height          =   255
            Index           =   27
            Left            =   480
            TabIndex        =   89
            Top             =   720
            Width           =   855
         End
      End
      Begin VB.CheckBox CkAdse 
         Caption         =   "ADSE"
         Height          =   375
         Left            =   6840
         TabIndex        =   135
         Top             =   4680
         Width           =   3375
      End
      Begin VB.CheckBox CkNovaArs 
         Caption         =   "ARS - Nova Factura��o"
         Height          =   375
         Left            =   6840
         TabIndex        =   134
         Top             =   4320
         Width           =   3375
      End
      Begin VB.CheckBox CkHemodialise 
         Caption         =   "Regra Hemodi�lise"
         Height          =   375
         Left            =   6840
         TabIndex        =   133
         Top             =   3960
         Width           =   3375
      End
      Begin VB.CheckBox CkInsereCodRubrEFR 
         Caption         =   "Insere C�digo da R�brica da EFR"
         Height          =   195
         Left            =   6840
         TabIndex        =   131
         Top             =   3360
         Width           =   2775
      End
      Begin VB.CheckBox CkValFixo 
         Caption         =   "Fins de Semana com Valor Fixo"
         Height          =   375
         Left            =   3360
         TabIndex        =   95
         Top             =   5760
         Width           =   3255
      End
      Begin VB.TextBox EcDescrRubrPesq 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   -74280
         TabIndex        =   86
         Top             =   480
         Width           =   2295
      End
      Begin VB.TextBox EcCodRubrPesq 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   -74760
         TabIndex        =   85
         Top             =   480
         Width           =   495
      End
      Begin VB.CommandButton BtRemove 
         Height          =   375
         Left            =   -62640
         Picture         =   "FormEntFinanceirasPRIVADO.frx":2108
         Style           =   1  'Graphical
         TabIndex        =   84
         Top             =   360
         Width           =   375
      End
      Begin VB.CommandButton BtAdiciona 
         Height          =   375
         Left            =   -63120
         Picture         =   "FormEntFinanceirasPRIVADO.frx":2492
         Style           =   1  'Graphical
         TabIndex        =   83
         Top             =   360
         Width           =   375
      End
      Begin VB.TextBox EcAuxPreco 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -73440
         TabIndex        =   78
         Top             =   3240
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.CheckBox CkControlaIsentos 
         Caption         =   "Controla Utentes Isentos (Envio de pre�os)"
         Height          =   375
         Left            =   3360
         TabIndex        =   29
         Top             =   5400
         Width           =   3375
      End
      Begin VB.CheckBox CkAvisosPagamento 
         Caption         =   "Imprime Avisos de Pagamento"
         Height          =   375
         Left            =   3360
         TabIndex        =   28
         Top             =   5040
         Width           =   2775
      End
      Begin VB.CheckBox CkMostraDivida 
         Caption         =   "Mostra D�vida na Entrega de Resultados"
         Height          =   375
         Left            =   3360
         TabIndex        =   27
         Top             =   4680
         Width           =   3375
      End
      Begin VB.CheckBox CkDomicilioDefeito 
         Caption         =   "Domic�lio por Defeito"
         Height          =   375
         Left            =   3360
         TabIndex        =   26
         Top             =   4320
         Width           =   2775
      End
      Begin VB.CheckBox CkInibeDomNaoUrb 
         Caption         =   "Inibe Domicilios N�o Urbanos"
         Height          =   375
         Left            =   3360
         TabIndex        =   25
         Top             =   3960
         Width           =   2775
      End
      Begin VB.CheckBox CkSeparaDom 
         Caption         =   "Separa Factura��o dos Domic�lios"
         Height          =   375
         Left            =   3360
         TabIndex        =   24
         Top             =   3600
         Width           =   2775
      End
      Begin VB.CheckBox CkObrigaNumBenef 
         Caption         =   "Obriga N�mero Benefici�rio"
         Height          =   375
         Left            =   3360
         TabIndex        =   23
         Top             =   3240
         Width           =   2775
      End
      Begin VB.CheckBox CkPercentagem 
         Caption         =   "Permite Factura��o por Percentagem"
         Height          =   375
         Left            =   240
         TabIndex        =   22
         Top             =   5760
         Width           =   3255
      End
      Begin VB.CheckBox Ck65Anos 
         Caption         =   "50% Desconto em Utentes 65 Anos"
         Height          =   375
         Left            =   240
         TabIndex        =   21
         Top             =   5400
         Width           =   3015
      End
      Begin VB.CheckBox CkInibeImpAna 
         Caption         =   "Inibe Impress�o de Resultados"
         Height          =   375
         Left            =   240
         TabIndex        =   20
         Top             =   5040
         Width           =   2775
      End
      Begin VB.CheckBox CkDescrAnaRec 
         Caption         =   "Descrimina An�lises no Recibo"
         Height          =   375
         Left            =   240
         TabIndex        =   19
         Top             =   4680
         Width           =   2775
      End
      Begin VB.CheckBox CkValidaNumBenef 
         Caption         =   "Valida N�mero Benefici�rio"
         Height          =   375
         Left            =   240
         TabIndex        =   18
         Top             =   4320
         Width           =   2775
      End
      Begin VB.CheckBox CkPrecoEnt 
         Caption         =   "Usa Pre�o da Entidade"
         Height          =   375
         Left            =   240
         TabIndex        =   17
         Top             =   3960
         Width           =   2775
      End
      Begin VB.CheckBox CkObrigaRecibo 
         Caption         =   "Obriga Recibo no Envio"
         Height          =   375
         Left            =   240
         TabIndex        =   16
         Top             =   3600
         Width           =   2775
      End
      Begin VB.CheckBox CkCompartDom 
         Caption         =   "Comparticipa Domicilios"
         Height          =   375
         Left            =   240
         TabIndex        =   15
         Top             =   3240
         Width           =   2775
      End
      Begin VB.Frame Frame1 
         Caption         =   "Factura��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1695
         Left            =   120
         TabIndex        =   62
         Top             =   1320
         Width           =   12855
         Begin VB.CommandButton BtNovaPortaria 
            Height          =   375
            Left            =   6720
            Picture         =   "FormEntFinanceirasPRIVADO.frx":281C
            Style           =   1  'Graphical
            TabIndex        =   96
            ToolTipText     =   "Criar Nova Portaria"
            Top             =   960
            Width           =   375
         End
         Begin VB.ComboBox CbEmpresa 
            Height          =   315
            Left            =   9240
            Style           =   2  'Dropdown List
            TabIndex        =   73
            Top             =   840
            Width           =   3495
         End
         Begin VB.TextBox EcCodEFRRef 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1080
            TabIndex        =   14
            Top             =   1230
            Width           =   855
         End
         Begin VB.TextBox EcDescrEFRRef 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2040
            Locked          =   -1  'True
            TabIndex        =   70
            Top             =   1230
            Width           =   4215
         End
         Begin VB.CommandButton BtPesqRapEFRRef 
            Height          =   340
            Left            =   6240
            Picture         =   "FormEntFinanceirasPRIVADO.frx":2BA6
            Style           =   1  'Graphical
            TabIndex        =   69
            ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
            Top             =   1200
            Width           =   375
         End
         Begin VB.CommandButton BtPesqRapPortaria 
            Height          =   340
            Left            =   6240
            Picture         =   "FormEntFinanceirasPRIVADO.frx":2F30
            Style           =   1  'Graphical
            TabIndex        =   67
            ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
            Top             =   810
            Width           =   375
         End
         Begin VB.TextBox EcDescrPortaria 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2040
            Locked          =   -1  'True
            TabIndex        =   66
            Top             =   840
            Width           =   4215
         End
         Begin VB.TextBox EcCodPortaria 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1080
            TabIndex        =   13
            Top             =   840
            Width           =   855
         End
         Begin VB.ComboBox CbTipoFacturacao 
            Height          =   315
            Left            =   9240
            Style           =   2  'Dropdown List
            TabIndex        =   12
            Top             =   360
            Width           =   3495
         End
         Begin VB.ComboBox CbTipoFactura 
            Height          =   315
            Left            =   4560
            Style           =   2  'Dropdown List
            TabIndex        =   11
            Top             =   360
            Width           =   2055
         End
         Begin VB.ComboBox CbTipoEFR 
            Height          =   315
            Left            =   1080
            Style           =   2  'Dropdown List
            TabIndex        =   10
            Top             =   360
            Width           =   1815
         End
         Begin VB.Label Label1 
            Caption         =   "Empresa"
            Height          =   255
            Index           =   25
            Left            =   7920
            TabIndex        =   74
            Top             =   840
            Width           =   1335
         End
         Begin VB.Label Label1 
            Caption         =   "EFR Ref."
            Height          =   255
            Index           =   23
            Left            =   120
            TabIndex        =   71
            Top             =   1230
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Portaria"
            Height          =   255
            Index           =   22
            Left            =   120
            TabIndex        =   68
            Top             =   840
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Tipo Factura��o"
            Height          =   255
            Index           =   21
            Left            =   7920
            TabIndex        =   65
            Top             =   360
            Width           =   1335
         End
         Begin VB.Label Label1 
            Caption         =   "Tipo Factura"
            Height          =   255
            Index           =   20
            Left            =   3480
            TabIndex        =   64
            Top             =   360
            Width           =   975
         End
         Begin VB.Label Label1 
            Caption         =   "Tipo EFR"
            Height          =   255
            Index           =   19
            Left            =   120
            TabIndex        =   63
            Top             =   360
            Width           =   735
         End
      End
      Begin VB.TextBox EcTelefone 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   8400
         TabIndex        =   6
         Top             =   600
         Width           =   1935
      End
      Begin VB.TextBox EcLocalidade 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4680
         TabIndex        =   9
         Top             =   960
         Width           =   2055
      End
      Begin VB.TextBox EcSeqPostal 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2160
         TabIndex        =   8
         Top             =   960
         Width           =   255
      End
      Begin VB.TextBox EcCodPostal 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1200
         TabIndex        =   7
         Top             =   960
         Width           =   855
      End
      Begin VB.TextBox EcMorada 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1200
         TabIndex        =   5
         Top             =   600
         Width           =   5535
      End
      Begin MSFlexGridLib.MSFlexGrid FgEFR 
         Height          =   5415
         Left            =   -74640
         TabIndex        =   38
         Top             =   720
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   9551
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
      End
      Begin MSFlexGridLib.MSFlexGrid FgPrecos 
         Height          =   5535
         Left            =   -74760
         TabIndex        =   77
         Top             =   720
         Width           =   12735
         _ExtentX        =   22463
         _ExtentY        =   9763
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
      End
      Begin VB.CheckBox CkImprRelARS 
         Caption         =   "Imprimir Resultados Relat�rio ARS"
         Height          =   375
         Left            =   6840
         TabIndex        =   132
         Top             =   3600
         Width           =   2775
      End
      Begin VB.Label Label3 
         Caption         =   "Destino"
         Height          =   225
         Index           =   1
         Left            =   7080
         TabIndex        =   138
         Top             =   960
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Telefone"
         Height          =   255
         Index           =   18
         Left            =   7080
         TabIndex        =   61
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Localidade"
         Height          =   255
         Index           =   16
         Left            =   3600
         TabIndex        =   59
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "C�d. Postal"
         Height          =   255
         Index           =   15
         Left            =   240
         TabIndex        =   58
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Morada"
         Height          =   255
         Index           =   12
         Left            =   240
         TabIndex        =   57
         Top             =   600
         Width           =   735
      End
   End
   Begin VB.CommandButton BtPesqRapEFR 
      Height          =   340
      Left            =   10080
      Picture         =   "FormEntFinanceirasPRIVADO.frx":32BA
      Style           =   1  'Graphical
      TabIndex        =   32
      ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
      Top             =   120
      Width           =   375
   End
   Begin VB.TextBox EcDescrFactura 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4320
      TabIndex        =   4
      Top             =   480
      Width           =   5655
   End
   Begin VB.TextBox EcDescricao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4320
      TabIndex        =   1
      Top             =   120
      Width           =   5655
   End
   Begin VB.TextBox EcCodSequencial 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      TabIndex        =   30
      Top             =   9120
      Width           =   855
   End
   Begin VB.TextBox EcAbreviatura 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1200
      TabIndex        =   3
      Top             =   480
      Width           =   855
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2070
         SubFormatType   =   1
      EndProperty
      Height          =   285
      Left            =   1200
      TabIndex        =   0
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcLinhaPreco"
      Height          =   375
      Index           =   50
      Left            =   7680
      TabIndex        =   82
      Top             =   8400
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcColunaPreco"
      Height          =   375
      Index           =   51
      Left            =   7680
      TabIndex        =   81
      Top             =   8760
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcCodEmpresa"
      Height          =   255
      Index           =   26
      Left            =   2640
      TabIndex        =   76
      Top             =   9120
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Empresa"
      Height          =   255
      Index           =   24
      Left            =   7800
      TabIndex        =   72
      Top             =   3240
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "NIF"
      Height          =   255
      Index           =   17
      Left            =   11160
      TabIndex        =   60
      Top             =   120
      Width           =   255
   End
   Begin VB.Label Label1 
      Caption         =   "EcDtRem"
      Height          =   255
      Index           =   14
      Left            =   4920
      TabIndex        =   56
      Top             =   8760
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcUserRem"
      Height          =   255
      Index           =   13
      Left            =   4920
      TabIndex        =   54
      Top             =   8400
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcDtAct"
      Height          =   255
      Index           =   11
      Left            =   2640
      TabIndex        =   52
      Top             =   8760
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcUserAct"
      Height          =   255
      Index           =   10
      Left            =   2640
      TabIndex        =   50
      Top             =   8400
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcUserCri"
      Height          =   255
      Index           =   9
      Left            =   120
      TabIndex        =   48
      Top             =   8400
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcDtCri"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   46
      Top             =   8760
      Width           =   1455
   End
   Begin VB.Label LbRem 
      Height          =   255
      Left            =   9720
      TabIndex        =   44
      Top             =   7560
      Width           =   3135
   End
   Begin VB.Label LbAct 
      Height          =   255
      Left            =   5400
      TabIndex        =   43
      Top             =   7560
      Width           =   3135
   End
   Begin VB.Label LbCri 
      Height          =   255
      Left            =   1200
      TabIndex        =   42
      Top             =   7560
      Width           =   3135
   End
   Begin VB.Label Label1 
      Caption         =   "Remo��o"
      Height          =   255
      Index           =   7
      Left            =   8880
      TabIndex        =   41
      Top             =   7560
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Atera��o"
      Height          =   255
      Index           =   6
      Left            =   4560
      TabIndex        =   40
      Top             =   7560
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Cria��o"
      Height          =   255
      Index           =   5
      Left            =   360
      TabIndex        =   39
      Top             =   7560
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Descri��o para Factura"
      Height          =   255
      Index           =   4
      Left            =   2400
      TabIndex        =   36
      Top             =   480
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Descri��o"
      Height          =   255
      Index           =   3
      Left            =   2400
      TabIndex        =   35
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Abreviatura"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   34
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   33
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "EcCodSequencial"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   31
      Top             =   9120
      Width           =   1455
   End
End
Attribute VB_Name = "FormEntFinanceirasPRIVADO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Const lColEFRCodigo = 0
Const lColEFRDescricao = 1
Const lColEFRPortaria = 2

Const lColPrecosCodRubr = 0
Const lColPrecosDescrRubr = 1
Const lColPrecosCodRubrEFR = 2
Const lColPrecosDescrRubrEFR = 3
Const lColPrecosTxModeradora = 4
Const lColPrecosTipoFact = 5
Const lColPrecosNumK = 6
Const lColPrecosNumC = 7
Const lColPrecosValPagEFR = 8
Const lColPrecosValPagDoe = 9
Const lColPrecosPercDoente = 10
Const lColPrecosPercEnt = 11

Private Type efr
    cod_efr As String
    descr_efr As String
End Type
Dim EstrutEFR() As efr
Dim TotalEstrutEFR As Integer

Private Type Precarios
    cod_rubr As String
    descr_rubr As String
    Portaria As String
    descr_Portaria As String
    cod_rubr_efr As String
    descr_rubr_efr As String
    val_Taxa As String
    t_fac As String
    nr_k As String
    nr_c As String
    val_pag_ent As String
    perc_pag_doe As String
    perc_pag_efr As String
    val_pag_doe As String
    user_cri As String
    dt_cri As String
    user_act As String
    dt_act As String
    empresa_id As String
    
    flg_modificado As Boolean
End Type
Dim EstrutPrec() As Precarios
Dim TotalPrec As Long

Private Sub BtAdiciona_Click()
    If EstrutPrec(TotalPrec).cod_rubr = "" Then
        BG_Mensagem mediMsgBox, "Existe uma linha por preencher.", vbInformation, ""
        Exit Sub
    End If
    TotalPrec = TotalPrec + 1
    ReDim Preserve EstrutPrec(TotalPrec)
    EstrutPrec(TotalPrec).empresa_id = BL_SelCodigo("SL_EMPRESAS", "cod_empresa", "SEQ_EMPRESA", CbEmpresa.ItemData(CbEmpresa.ListIndex))
    EstrutPrec(TotalPrec).flg_modificado = True
    EstrutPrec(TotalPrec).Portaria = EcCodPortaria
    FgPrecos.AddItem ""
    FgPrecos.row = TotalPrec
    FgPrecos.SetFocus
    FgPrecos.topRow = TotalPrec
    FgPrecos.Col = lColPrecosCodRubr
End Sub

Private Sub BtGravaNovaPort_Click()
    GravaDadosNovaPortaria
End Sub

Private Sub BtNovaPortaria_Click()
    If EcCodPortaria <> "" Then
        PreencheDadosNovaPortaria
        FrameNovaPortaria.Visible = True
        FrameNovaPortaria.top = 360
        FrameNovaPortaria.left = 120
    Else
        BG_Mensagem mediMsgBox, "Tem de ter seleccionado uma portaria de refer�ncia.", vbExclamation, "..."
    End If
End Sub

Private Sub BtPesqRapEFRRefNovaPort_Click()
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_efr"
    CamposEcran(1) = "cod_efr"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_efr"
    CamposEcran(2) = "descr_efr"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "fa_efr"
    CampoPesquisa1 = "descr_efr"
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexaoSecundaria, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " descr_efr", " Pesquisar Entidades de Refer�ncia")
    
    mensagem = "N�o foi encontrada nenhuma Entidade de Refer�ncia"
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodEfrRefNovaPort.Text = resultados(1)
            EcDescrEfrRefNovaPort.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    EcCodEFRRef_validate False
End Sub

Private Sub BtPesqRapGrupRubrNovaPort_Click()
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_grupo"
    CamposEcran(1) = "cod_grupo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_grupo"
    CamposEcran(2) = "descr_grupo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "fa_grup_rubr"
    CampoPesquisa1 = "descr_grupo"
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexaoSecundaria, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " descr_grupo", " Pesquisar Grupo R�bricas")
    
    mensagem = "N�o foi encontrada nenhum Grupo de R�brica"
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrRubrNovaPort.Text = resultados(1)
            EcDescrGrRubrNovaPort.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    EcCodGrRubrNovaPort_validate False

End Sub

Private Sub BtRemove_Click()
    RemoveLinha FgPrecos.row
End Sub

Private Sub BtSairNovaPort_Click()
    FrameNovaPortaria.Visible = False
    EcCodEfrRefNovaPort = ""
    EcDescrEfrRefNovaPort = ""
    EcCodNovaPort = ""
    EcDescrNovaPort = ""
    EcDtIniNovaPort = ""
    EcDtFimNovaPort = ""
    CbEmpresaNovaPort.ListIndex = mediComboValorNull
    EcUserCriNovaPort = ""
    EcCodGrEfrNovaPort = ""
    EcDescrGrEfrNovaPort = ""
    EcCodGrRubrNovaPort = ""
    EcDescrGrRubrNovaPort = ""
    EcValCNovaPort = ""
    EcValKNovaPort = ""
    CkGravaPrecosNovaPort.value = vbGrayed
End Sub

Private Sub CbEmpresa_Click()
    If CbEmpresa.ListIndex <> mediComboValorNull Then
        EcCodEmpresa = BL_SelCodigo("SL_EMPRESAS", "cod_empresa", "SEQ_EMPRESA", CbEmpresa.ItemData(CbEmpresa.ListIndex))
    Else
        EcCodEmpresa = ""
    End If
End Sub


Private Sub EcCodEfrRefNovaPort_validate(Cancel As Boolean)
    Dim Tabela As ADODB.recordset
    Dim sql As String
    On Error GoTo TrataErro
    If EcCodEfrRefNovaPort.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_efr FROM fa_efr WHERE  cod_efr=" & BL_TrataStringParaBD(EcCodEfrRefNovaPort)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexaoSecundaria
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodEfrRefNovaPort = ""
            EcDescrEfrRefNovaPort = ""
        Else
            EcCodEfrRefNovaPort = EcCodEfrRefNovaPort
            EcDescrEfrRefNovaPort = Tabela!descr_efr
            Tabela.Close
                    
        End If
     
    Else
        EcDescrEfrRefNovaPort = ""
    End If
Exit Sub
TrataErro:
    EcDescrEfrRefNovaPort = ""
    BG_LogFile_Erros "Erro ao procurar Portaria: " & Err.Description, Me.Name, "EcCodEfrRefNovaPort_validate"
    BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
    Exit Sub
    Resume Next
End Sub

Private Sub EcCodGrRubrNovaPort_validate(Cancel As Boolean)
    Dim Tabela As ADODB.recordset
    Dim sql As String
    On Error GoTo TrataErro
    If EcCodGrRubrNovaPort.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_grupo FROM fa_grup_rubr WHERE  cod_grupo=" & BL_TrataStringParaBD(EcCodGrRubrNovaPort)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexaoSecundaria
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodGrRubrNovaPort = ""
            EcDescrGrRubrNovaPort = ""
        Else
            EcCodGrRubrNovaPort = EcCodGrRubrNovaPort
            EcDescrGrRubrNovaPort = Tabela!descr_grupo
            Tabela.Close
                    
        End If
     
    Else
        EcDescrGrRubrNovaPort = ""
    End If
Exit Sub
TrataErro:
    EcDescrGrRubrNovaPort = ""
    BG_LogFile_Erros "Erro ao procurar Grupo Rubrica: " & Err.Description, Me.Name, "EcCodGrRubrNovaPort_validate", True
    Exit Sub
    Resume Next
End Sub

Private Sub EcCodPortaria_Validate(Cancel As Boolean)
    Dim Tabela As ADODB.recordset
    Dim sql As String
    On Error GoTo TrataErro
    If EcCodPortaria.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_port,cod_efr FROM fa_portarias WHERE  portaria=" & BL_TrataStringParaBD(EcCodPortaria)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexaoSecundaria
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodPortaria.Text = ""
            EcDescrPortaria.Text = ""
            EcCodEFRRef = ""
            EcDescrEFRRef = ""
        Else
            EcDescrPortaria.Text = Tabela!descr_Port
            EcCodEFRRef = Tabela!cod_efr
            EcDescrEFRRef = BL_SelCodigo_Sec("FA_EFR", "DESCR_EFR", "COD_EFR", EcCodEFRRef, "V")
        
        End If
     
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrPortaria.Text = ""
        EcCodEFRRef = ""
        EcDescrEFRRef = ""
    End If
Exit Sub
TrataErro:
    EcCodPortaria.Text = ""
    EcDescrPortaria.Text = ""
    EcCodEFRRef = ""
    EcDescrEFRRef = ""
    BG_LogFile_Erros "Erro ao procurar Portaria: " & Err.Description, Me.Name, "EcCodPortaria_validate"
    BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
    Exit Sub
    Resume Next
End Sub
Private Sub EcCodEFRRef_validate(Cancel As Boolean)
    Dim Tabela As ADODB.recordset
    Dim sql As String
    On Error GoTo TrataErro
    If EcCodEFRRef.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_efr FROM fa_efr WHERE  cod_efr=" & BL_TrataStringParaBD(EcCodEFRRef)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexaoSecundaria
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodPortaria.Text = ""
            EcDescrPortaria.Text = ""
            EcCodEFRRef = ""
            EcDescrEFRRef = ""
        Else
            EcCodEFRRef = EcCodEFRRef
            EcDescrEFRRef = Tabela!descr_efr
            Tabela.Close
            
            sql = "SELECT portaria, descr_port FROM fa_portarias WHERE  cod_efr=" & BL_TrataStringParaBD(EcCodEFRRef)
            sql = sql & " AND dt_fim is null "
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            Tabela.Open sql, gConexaoSecundaria
            If Tabela.RecordCount >= 1 Then
                EcCodPortaria = Tabela!Portaria
                EcDescrPortaria = Tabela!descr_Port
            Else
                EcCodPortaria = ""
                EcDescrPortaria = ""
            End If
        
        End If
     
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrPortaria.Text = ""
        EcCodPortaria = ""
        EcDescrEFRRef = ""
    End If
Exit Sub
TrataErro:
    EcCodPortaria.Text = ""
    EcDescrPortaria.Text = ""
    EcCodPortaria = ""
    EcDescrEFRRef = ""
    BG_LogFile_Erros "Erro ao procurar Portaria: " & Err.Description, Me.Name, "EcCodPortaria_validate"
    BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
    Exit Sub
    Resume Next
End Sub


Private Sub EcCodPostal_validate(Cancel As Boolean)
    
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostal.Text <> "" Then
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal = " & BL_TrataStringParaBD(EcCodPostal.Text)
        
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcLocalidade.Text = ""
            EcCodPostal.SetFocus
        Else
            EcLocalidade.Text = Trim(rsCodigo!descr_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

End Sub

Private Sub EcCodRubrPesq_Change()
    Dim i As Long
    If EcCodRubrPesq = "" Then
        FgPrecos.topRow = 1
    Else
        For i = 1 To TotalPrec
            If UCase(Mid(EstrutPrec(i).cod_rubr, 1, Len(EcCodRubrPesq))) = UCase(EcCodRubrPesq) Then
                FgPrecos.topRow = i
                Exit Sub
            End If
        Next
    End If
End Sub

Private Sub EcDescrRubrPesq_Change()
    Dim i As Long
    If EcDescrRubrPesq = "" Then
        FgPrecos.topRow = 1
    Else
        For i = 1 To TotalPrec
            If UCase(Mid(EstrutPrec(i).descr_rubr, 1, Len(EcDescrRubrPesq))) = UCase(EcDescrRubrPesq) Then
                FgPrecos.topRow = i
                Exit Sub
            End If
        Next
    End If
End Sub

Private Sub EcValC_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcValC)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub
Private Sub EcValk_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcValK)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
End Sub

Private Sub FgEFR_Click()
    
    If TotalEstrutEFR > 0 And FgEFR.row <= TotalEstrutEFR Then
        rs.MoveFirst
        While Not rs.EOF
            If EstrutEFR(FgEFR.row).cod_efr = rs!cod_efr Then
                PreencheCampos
                Exit Sub
            Else
                rs.MoveNext
            End If
        Wend
    End If
End Sub


Private Sub FgPrecos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        FgPrecos_DblClick
    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    BL_Abre_Conexao_Secundaria gSGBD_SECUNDARIA
    Me.caption = " Entidades Financeiras"
    Me.left = 10
    Me.top = 10
    Me.Width = 13605
    Me.Height = 8730 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "fa_efr"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 47
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_efr"
    CamposBD(1) = "cod_efr"
    CamposBD(2) = "descr_efr"
    CamposBD(3) = "out_descr_efr"
    CamposBD(4) = "abr_efr"
    CamposBD(5) = "sigla_e"
    CamposBD(6) = "user_cri"
    CamposBD(7) = "dt_cri"
    CamposBD(8) = "user_act"
    CamposBD(9) = "dt_act"
    CamposBD(10) = "user_rem"
    CamposBD(11) = "dt_rem"
    CamposBD(12) = "n_contrib_efr"
    CamposBD(13) = "mor_efr"
    CamposBD(14) = "cod_postal"
    CamposBD(15) = "n_seq_postal"
    CamposBD(16) = "localidade"
    CamposBD(17) = "telef_efr"
    CamposBD(18) = "tab_utilizada"
    CamposBD(19) = "t_efr"
    CamposBD(20) = "saida_mapa"
    CamposBD(21) = "t_fac"
    CamposBD(22) = "flg_compart_dom"
    CamposBD(23) = "flg_obriga_recibo"
    CamposBD(24) = "flg_usa_preco_ent"
    CamposBD(25) = "valida_num_benef"
    CamposBD(26) = "flg_descrimina_ana_rec"
    CamposBD(27) = "flg_inibe_imp_analises"
    CamposBD(28) = "flg_65anos"
    CamposBD(29) = "flg_perc_facturar"
    CamposBD(30) = "flg_obriga_nr_benef"
    CamposBD(31) = "flg_separa_dom"
    CamposBD(32) = "flg_nao_urbano"
    CamposBD(33) = "flg_dom_defeito"
    CamposBD(34) = "flg_mostra_divida"
    CamposBD(35) = "flg_imp_aviso_pag"
    CamposBD(36) = "flg_controla_isencao"
    CamposBD(37) = "cod_empresa"
    CamposBD(38) = "flg_fds_fixo"
    CamposBD(39) = "num_copias_rec"
    CamposBD(40) = "flg_insere_cod_rubr_efr"
    CamposBD(41) = "flg_imprime_res_ars"
    CamposBD(42) = "flg_hemodialise"
    CamposBD(43) = "flg_nova_ars"
    CamposBD(44) = "flg_adse"
    CamposBD(45) = "flg_invisivel"
    CamposBD(46) = "cod_destino"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcDescrFactura
    Set CamposEc(4) = EcAbreviatura
    Set CamposEc(5) = EcAbreviatura
    Set CamposEc(6) = EcUserCri
    Set CamposEc(7) = EcDtCri
    Set CamposEc(8) = EcUserAct
    Set CamposEc(9) = EcDtAct
    Set CamposEc(10) = EcUserRem
    Set CamposEc(11) = EcDtRem
    Set CamposEc(12) = EcNIF
    Set CamposEc(13) = EcMorada
    Set CamposEc(14) = EcCodPostal
    Set CamposEc(15) = EcSeqPostal
    Set CamposEc(16) = EcLocalidade
    Set CamposEc(17) = EcTelefone
    Set CamposEc(18) = EcCodEFRRef
    Set CamposEc(19) = CbTipoEFR
    Set CamposEc(20) = CbTipoFactura
    Set CamposEc(21) = CbTipoFacturacao
    Set CamposEc(22) = CkCompartDom
    Set CamposEc(23) = CkObrigaRecibo
    Set CamposEc(24) = CkPrecoEnt
    Set CamposEc(25) = CkValidaNumBenef
    Set CamposEc(26) = CkDescrAnaRec
    Set CamposEc(27) = CkInibeImpAna
    Set CamposEc(28) = Ck65Anos
    Set CamposEc(29) = CkPercentagem
    Set CamposEc(30) = CkObrigaNumBenef
    Set CamposEc(31) = CkSeparaDom
    Set CamposEc(32) = CkInibeDomNaoUrb
    Set CamposEc(33) = CkDomicilioDefeito
    Set CamposEc(34) = CkMostraDivida
    Set CamposEc(35) = CkAvisosPagamento
    Set CamposEc(36) = CkControlaIsentos
    Set CamposEc(37) = EcCodEmpresa
    Set CamposEc(38) = CkValFixo
    Set CamposEc(39) = EcNumCopiasRec
    Set CamposEc(40) = CkInsereCodRubrEFR
    Set CamposEc(41) = CkImprRelARS
    Set CamposEc(42) = CkHemodialise
    Set CamposEc(43) = CkNovaArs
    Set CamposEc(44) = CkAdse
    Set CamposEc(45) = CkInvisivel
    Set CamposEc(46) = CbDestino
        
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "Sequ�ncia da Entidade Financeira"
    TextoCamposObrigatorios(1) = "C�digo da Entidade Financeira"
    TextoCamposObrigatorios(2) = "Descri��o da Entidade Financeira"
    TextoCamposObrigatorios(3) = "Descri�ao da Entidade Financeira para Factura"
    TextoCamposObrigatorios(5) = "Abreviatura da Entidade Financeira"
    TextoCamposObrigatorios(12) = "NIF da Entidade Financeira"
    TextoCamposObrigatorios(18) = "Tabela da Entidade Financeira"
    TextoCamposObrigatorios(19) = "Tipo de Entidade Financeira"
    TextoCamposObrigatorios(20) = "Tipo de Factura"
    TextoCamposObrigatorios(21) = "Tipo de Factura��o"
    TextoCamposObrigatorios(37) = "Empresa Associada"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_efr"
    Set ChaveEc = EcCodSequencial
    
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    BL_FimProcessamento Me
End Sub
Sub PreencheValoresDefeito()
    CkNovaArs.value = vbGrayed
    CkCompartDom.value = vbGrayed
    CkObrigaRecibo.value = vbGrayed
    CkPrecoEnt.value = vbGrayed
    CkValidaNumBenef.value = vbGrayed
    CkDescrAnaRec.value = vbGrayed
    CkInibeImpAna.value = vbGrayed
    Ck65Anos.value = vbGrayed
    CkPercentagem.value = vbGrayed
    CkObrigaNumBenef.value = vbGrayed
    CkSeparaDom.value = vbGrayed
    CkInibeDomNaoUrb.value = vbGrayed
    CkDomicilioDefeito.value = vbGrayed
    CkMostraDivida.value = vbGrayed
    CkAvisosPagamento.value = vbGrayed
    CkControlaIsentos.value = vbGrayed
    CkValFixo.value = vbGrayed
    FrameNovaPortaria.Visible = False
    CkGravaPrecosNovaPort.value = vbGrayed
    CkInsereCodRubrEFR.value = vbGrayed
    CkImprRelARS.value = vbGrayed
    CkAdse.value = vbGrayed
    
    SSTab.Tab = 0
    BG_PreencheComboBD_ADO "SL_EMPRESAS", "SEQ_EMPRESA", "NOME_EMPRESA", CbEmpresa
    BG_PreencheComboBD_ADO "SL_EMPRESAS", "SEQ_EMPRESA", "NOME_EMPRESA", CbEmpresaNovaPort
    BG_PreencheComboBDSecundaria_ADO "fa_tbf_t_efr", "cod_t_efr", "descr_t_efr", CbTipoEFR
    BG_PreencheComboBDSecundaria_ADO "fa_tbf_t_fac", "cod_t_fac", "descr_t_fac", CbTipoFacturacao
    BG_PreencheComboBDSecundaria_ADO "fa_tbf_t_fact", "cod_t_fact", "descr_t_fact", CbTipoFactura
    ' PFerreira 04.04.2007
    BG_PreencheComboBD_ADO "sl_tbf_t_destino", "cod_t_dest", "descr_t_dest", CbDestino, mediAscComboCodigo

End Sub
Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormEntFinanceiras = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus
    LbCri = ""
    LbAct = ""
    LbRem = ""
    EcDescrPortaria = ""
    BG_LimpaCampo_Todos CamposEc
    SSTab.Tab = 0
    CkNovaArs.value = vbGrayed
    CkCompartDom.value = vbGrayed
    CkObrigaRecibo.value = vbGrayed
    CkPrecoEnt.value = vbGrayed
    CkValidaNumBenef.value = vbGrayed
    CkDescrAnaRec.value = vbGrayed
    CkInibeImpAna.value = vbGrayed
    Ck65Anos.value = vbGrayed
    CkPercentagem.value = vbGrayed
    CkObrigaNumBenef.value = vbGrayed
    CkSeparaDom.value = vbGrayed
    CkInibeDomNaoUrb.value = vbGrayed
    CkDomicilioDefeito.value = vbGrayed
    CkMostraDivida.value = vbGrayed
    CkAvisosPagamento.value = vbGrayed
    CkControlaIsentos.value = vbGrayed
    CkValFixo.value = vbGrayed
    CkInsereCodRubrEFR.value = vbGrayed
    CkImprRelARS.value = vbGrayed
    CkAdse.value = vbGrayed
    
    FrameNovaPortaria.Visible = False
    EcCodEfrRefNovaPort = ""
    EcDescrEfrRefNovaPort = ""
    EcCodNovaPort = ""
    EcDescrNovaPort = ""
    EcDtIniNovaPort = ""
    EcDtFimNovaPort = ""
    CbEmpresaNovaPort.ListIndex = mediComboValorNull
    EcUserCriNovaPort = ""
    EcCodGrEfrNovaPort = ""
    EcDescrGrEfrNovaPort = ""
    EcCodGrRubrNovaPort = ""
    EcDescrGrRubrNovaPort = ""
    EcValCNovaPort = ""
    EcValKNovaPort = ""
    CkGravaPrecosNovaPort.value = vbGrayed
    CkHemodialise.value = vbGrayed
    
    LimpaPrecos
    EcCodRubrPesq = ""
    EcDescrRubrPesq = ""
    EcValC = ""
    EcValK = ""
    EcCodGrEFR = ""
    EcDescrGrEFR = ""
    CkInvisivel.value = vbGrayed
End Sub

Sub DefTipoCampos()
    With FgPrecos
       .rows = 2
       .Cols = 12
       .FixedRows = 1
       .FixedCols = 0
       .AllowBigSelection = False
       .HighLight = flexHighlightAlways
       .FocusRect = flexFocusHeavy
       .MousePointer = flexDefault
       .FillStyle = flexFillSingle
       .SelectionMode = flexSelectionFree
       .AllowUserResizing = flexResizeColumns
       .GridLinesFixed = flexGridInset
       .GridColorFixed = flexTextInsetLight
       .MergeCells = flexMergeFree
       .PictureType = flexPictureColor
       .RowHeightMin = 280
       .row = 0
       
       .ColWidth(lColPrecosCodRubr) = 500
       .Col = lColPrecosCodRubr
       .Text = "C�d"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosDescrRubr) = 2700
       .Col = lColPrecosDescrRubr
       .Text = "R�brica"
       .ColAlignment(lColPrecosDescrRubr) = flexAlignLeftCenter
       
       .ColWidth(lColPrecosCodRubrEFR) = 800
       .Col = lColPrecosCodRubrEFR
       .Text = "Rubr.EFR"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosDescrRubrEFR) = 2700
       .Col = lColPrecosDescrRubrEFR
       .Text = "Descr. Rubrica EFR"
       .ColAlignment(lColPrecosDescrRubrEFR) = flexAlignLeftCenter
       
       .ColWidth(lColPrecosTxModeradora) = 800
       .Col = lColPrecosTxModeradora
       .Text = "Tx.Modr."
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosTipoFact) = 800
       .Col = lColPrecosTipoFact
       .Text = "Tipo Fact"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosNumC) = 600
       .Col = lColPrecosNumC
       .Text = "N�Cs"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosNumK) = 600
       .Col = lColPrecosNumK
       .Text = "N�Ks"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosValPagEFR) = 700
       .Col = lColPrecosValPagEFR
       .Text = "Val.EFR"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosValPagDoe) = 700
       .Col = lColPrecosValPagDoe
       .Text = "Val.Doe."
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosPercDoente) = 700
       .Col = lColPrecosPercDoente
       .Text = "%Doe"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosPercEnt) = 700
       .Col = lColPrecosPercEnt
       .Text = "%Ent"
       .CellAlignment = flexAlignLeftCenter
       .Col = 0
    End With

    With FgEFR
        .rows = 2
        .Cols = 3
        .FixedRows = 1
        .FixedCols = 0
        .AllowBigSelection = False
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        
        .ColWidth(lColEFRCodigo) = 800
        .Col = lColEFRCodigo
        .TextMatrix(0, lColEFRCodigo) = "C�digo"
        .CellAlignment = flexAlignCenterCenter
        
        .ColWidth(lColEFRDescricao) = 10000
        .Col = lColEFRDescricao
        .TextMatrix(0, lColEFRDescricao) = "Descri��o"
        .CellAlignment = flexAlignCenterCenter
        
        .ColWidth(lColEFRPortaria) = 0
        .Col = lColEFRPortaria
        .TextMatrix(0, lColEFRPortaria) = "Portaria"
        .CellAlignment = flexAlignCenterCenter
        .Col = 0
    End With
    
    EcCodSequencial.Tag = adNumeric
    EcCodigo.Tag = adNumeric
    EcDescricao.Tag = adVarChar
    EcDescrFactura.Tag = adVarChar
    EcAbreviatura.Tag = adVarChar
    EcUserCri.Tag = adVarChar
    EcUserAct.Tag = adVarChar
    EcUserRem.Tag = adVarChar
    EcDtAct.Tag = adDate
    EcDtCri.Tag = adVarChar
    EcDtRem.Tag = adVarChar
    EcMorada.Tag = adVarChar
    EcCodPostal.Tag = adVarChar
    EcSeqPostal.Tag = adNumeric
    EcLocalidade.Tag = adVarChar
    EcTelefone.Tag = adVarChar
    EcNIF.Tag = adVarChar
    CkCompartDom.Tag = adNumeric
    CkObrigaRecibo.Tag = adNumeric
    CkPrecoEnt.Tag = adNumeric
    CkValidaNumBenef.Tag = adNumeric
    CkDescrAnaRec.Tag = adNumeric
    CkInibeImpAna.Tag = adNumeric
    Ck65Anos.Tag = adNumeric
    CkPercentagem.Tag = adNumeric
    CkObrigaNumBenef.Tag = adNumeric
    CkSeparaDom.Tag = adNumeric
    CkInibeDomNaoUrb.Tag = adNumeric
    CkDomicilioDefeito.Tag = adNumeric
    CkMostraDivida.Tag = adNumeric
    CkAvisosPagamento.Tag = adNumeric
    CkControlaIsentos.Tag = adNumeric
    CkValFixo.Tag = adNumeric
    CkInsereCodRubrEFR.Tag = adNumeric
    EcValC.Tag = adNumeric
    EcValK.Tag = adNumeric
    EcCodGrEFR.Tag = adVarChar
    EcDescrGrEFR.Tag = adVarChar
    CkHemodialise.value = vbGrayed
    CkInvisivel.value = vbGrayed
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        LbCri = BL_SelCodigo("SL_IDUTILIZADOR", "UTILIZADOR", "COD_UTILIZADOR", EcUserCri, "V") & " " & EcDtCri
        LbAct = BL_SelCodigo("SL_IDUTILIZADOR", "UTILIZADOR", "COD_UTILIZADOR", EcUserAct, "V") & " " & EcDtAct
        LbRem = BL_SelCodigo("SL_IDUTILIZADOR", "UTILIZADOR", "COD_UTILIZADOR", EcUserRem, "V") & " " & EcDtRem
        If CkInvisivel.value = vbChecked Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
        EcCodEFRRef_validate False
        PreencheEmpresa
        PreenchePrecarios
        PreenchePrGrupRubr
    End If
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        LimpaFgEFR
        CampoDeFocus.SetFocus
    End If
    
End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        LimpaFgEFR
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            
            'rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    On Error GoTo TrataErro
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & " ORDER BY cod_efr ASC,descr_efr ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioTabela
    rs.Open CriterioTabela, gConexaoSecundaria
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        PreencheFgEFR
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao procurar Entidades: " & Err.Description, Me.Name, "FuncaoProcurar"
    BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
    FuncaoLimpar
    Exit Sub
    Resume Next
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        EcCodSequencial = BG_DaMAX_Secund�ria(NomeTabela, "seq_efr") + 1
        BL_InicioProcessamento Me, "A inserir registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
            Dim cod As String
            cod = EcCodigo
            FuncaoLimpar
            EcCodigo = cod
            FuncaoProcurar
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    On Error GoTo TrataErro
    Dim SQLQuery As String

    gSQLError = 0
    gSQLISAM = 0
    'EcCodSequencial = BG_DaMAX(NomeTabela, "seq_efr") + 1
    EcUserCri = gCodUtilizador
    EcDtCri = Bg_DaData_ADO
    gConexaoSecundaria.BeginTrans
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO_SEC(NomeTabela, CamposBD, CamposEc)
    gConexaoSecundaria.Execute SQLQuery
    If GravaPrecarios = False Then
        GoTo TrataErro
    End If
    If GravaPrGrupRubr = False Then
        GoTo TrataErro
    End If
    gConexaoSecundaria.CommitTrans
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Gravar Entidade", Me.Name, "BD_Insert"
    BG_Mensagem mediMsgBox, "Erro ao gravar entidade!", vbExclamation, "Inserir"
    gConexaoSecundaria.RollbackTrans
    Exit Sub
    Resume Next

End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        EcDtCri = Format(CDate(EcDtCri), cFormatoDataBD)
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim reg As Integer
    On Error GoTo TrataErro
    
    gSQLError = 0
    gConexaoSecundaria.BeginTrans
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    gConexaoSecundaria.Execute SQLQuery, reg
    
    If GravaPrecarios = False Then
        GoTo TrataErro
    End If
    If GravaPrGrupRubr = False Then
        GoTo TrataErro
    End If
    
    gConexaoSecundaria.CommitTrans
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    'Era o �ltimo Registo?
    If rs.EOF Then
        If rs.BOF = False Then
            rs.MovePrevious
        Else
            If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
        End If
    Else
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    End If

    
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao gravar alteracoes ", Me.Name, "BD_Update", True
    gConexaoSecundaria.RollbackTrans
    Exit Sub
    Resume Next
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer desactivar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A desactivar registo."
        CkInvisivel.value = vbChecked
        BD_Update
        BL_FimProcessamento Me
    End If
    
End Sub

Private Sub PreencheFgEFR()
    LimpaFgEFR
    While Not rs.EOF
    
        TotalEstrutEFR = TotalEstrutEFR + 1
        ReDim Preserve EstrutEFR(TotalEstrutEFR)
        EstrutEFR(TotalEstrutEFR).cod_efr = rs!cod_efr
        EstrutEFR(TotalEstrutEFR).descr_efr = rs!descr_efr
        
        FgEFR.TextMatrix(TotalEstrutEFR, lColEFRCodigo) = rs!cod_efr
        FgEFR.TextMatrix(TotalEstrutEFR, lColEFRDescricao) = rs!descr_efr
        FgEFR.AddItem ""
        rs.MoveNext
    Wend
    rs.MoveFirst
End Sub

Private Sub LimpaFgEFR()
    Dim i As Integer
    FgEFR.rows = 2
    For i = 0 To FgEFR.Cols - 1
        FgEFR.TextMatrix(1, i) = ""
    Next
    TotalEstrutEFR = 0
    ReDim EstrutEFR(0)
End Sub

Private Sub BtPesqRapEFR_Click()

    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_efr"
    CamposEcran(1) = "cod_efr"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_efr"
    CamposEcran(2) = "descr_efr"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "fa_efr"
    CampoPesquisa1 = "descr_efr"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexaoSecundaria, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " descr_efr", " Pesquisar Entidades")
    
    mensagem = "N�o foi encontrada nenhuma Entidade"
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            FuncaoLimpar
            EcCodigo.Text = resultados(1)
            EcDescricao.Text = resultados(2)
            FuncaoProcurar
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesqRapPortaria_Click()
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "portaria"
    CamposEcran(1) = "portaria"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_port"
    CamposEcran(2) = "descr_port"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "fa_portarias"
    CampoPesquisa1 = "descr_port"
    ClausulaWhere = " dt_fim is null "
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexaoSecundaria, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " descr_port", " Pesquisar Portarias")
    
    mensagem = "N�o foi encontrada nenhuma Portaria"
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodPortaria.Text = resultados(1)
            EcDescrPortaria.Text = resultados(2)
            EcCodEFRRef = BL_SelCodigo_Sec("FA_PORTARIAS", "COD_EFR", "PORTARIA", resultados(1), "V")
            EcDescrEFRRef = BL_SelCodigo_Sec("FA_EFR", "DESCR_EFR", "COD_EFR", EcCodEFRRef, "V")
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesqRapEFRRef_Click()
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_efr"
    CamposEcran(1) = "cod_efr"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_efr"
    CamposEcran(2) = "descr_efr"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "fa_efr"
    CampoPesquisa1 = "descr_efr"
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexaoSecundaria, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " descr_efr", " Pesquisar Entidades de Refer�ncia")
    
    mensagem = "N�o foi encontrada nenhuma Entidade de Refer�ncia"
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodEFRRef.Text = resultados(1)
            EcDescrEFRRef.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    EcCodEFRRef_validate False
End Sub

' ------------------------------------------------------

' PREENCHE COMBO DA EMPRESA

' ------------------------------------------------------
Private Sub PreencheEmpresa()
    Dim i As Long
    Dim seqEmpresa As Long
    On Error GoTo TrataErro
    If EcCodEmpresa <> "" Then
        seqEmpresa = BL_SelCodigo("SL_EMPRESAS", "SEQ_EMPRESA", "cod_empresa", EcCodEmpresa, "V")
        For i = 0 To CbEmpresa.ListCount - 1
            If CbEmpresa.ItemData(i) = seqEmpresa Then
                CbEmpresa.ListIndex = i
                Exit Sub
            End If
        Next
    Else
        CbEmpresa.ListIndex = mediComboValorNull
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher Empresa: " & Err.Description, Me.Name, "PreencheEmpresa"
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------

' PREENCHE COMBO DA EMPRESA

' ------------------------------------------------------
Private Sub PreencheEmpresaNovaPort(codEmpresa As String)
    Dim i As Long
    Dim seqEmpresa As Long
    On Error GoTo TrataErro
    If EcCodEmpresa <> "" Then
        seqEmpresa = BL_SelCodigo("SL_EMPRESAS", "SEQ_EMPRESA", "cod_empresa", codEmpresa, "V")
        For i = 0 To CbEmpresa.ListCount - 1
            If CbEmpresa.ItemData(i) = seqEmpresa Then
                CbEmpresa.ListIndex = i
                Exit Sub
            End If
        Next
    Else
        CbEmpresa.ListIndex = mediComboValorNull
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher Empresa(NovaPortaria): " & Err.Description, Me.Name, "PreencheEmpresaNovaPort"
    Exit Sub
    Resume Next
End Sub



' ------------------------------------------------------

' LIMPA TODOS CAMPOS RELATIVOS AOS VALORES DE REFERENCIA

' ------------------------------------------------------
Private Sub LimpaPrecos()
    Dim i  As Long
    On Error GoTo TrataErro
    
    FgPrecos.rows = 2
    FgPrecos.row = 1
    For i = 0 To FgPrecos.Cols - 1
        FgPrecos.TextMatrix(1, i) = ""
    Next
    TotalPrec = 0
    ReDim EstrutPrec(0)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a limpar FgPrecos ", Me.Name, "LimpaPrecos"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------

' CARREGA PRECARIOS

' ------------------------------------------------------
Private Sub PreenchePrecarios()
    Dim sSql As String
    Dim RsFact As New ADODB.recordset
    Dim rsPrRubr As New ADODB.recordset
    On Error GoTo TrataErro
    If EcCodigo = "" Then Exit Sub
    LimpaPrecos
    sSql = "SELECT * FROM fa_portarias   where portaria = " & EcCodPortaria & " AND DT_FIM IS NULL ORDER BY cod_efr, dt_ini"
    RsFact.CursorType = adOpenStatic
    RsFact.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsFact.Open sSql, gConexaoSecundaria
    If RsFact.RecordCount = 1 Then
        
        
        'BRUNODSANTOS 09.03.2017 - Cedivet-202
        sSql = "SELECT x1.*, x2.descr_rubr  FROM fa_pr_rubr x1, fa_rubr x2 WHERE  x2.cod_grupo in ( " & gGrupoRubricaFactus & " ) AND  x1.portaria = " & BL_HandleNull(RsFact!Portaria, "")
        sSql = sSql & " AND x1.cod_rubr =x2.cod_rubr ORDER BY descr_rubr"
        rsPrRubr.CursorType = adOpenStatic
        rsPrRubr.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsPrRubr.Open sSql, gConexaoSecundaria
        If rsPrRubr.RecordCount >= 1 Then
            While Not rsPrRubr.EOF
                TotalPrec = TotalPrec + 1
                ReDim Preserve EstrutPrec(TotalPrec)
                
                EstrutPrec(TotalPrec).Portaria = BL_HandleNull(RsFact!Portaria, "")
                EstrutPrec(TotalPrec).descr_Portaria = BL_HandleNull(RsFact!descr_Port, "")
                EstrutPrec(TotalPrec).empresa_id = BL_HandleNull(RsFact!empresa_id, "")
                EstrutPrec(TotalPrec).flg_modificado = False
                
                EstrutPrec(TotalPrec).Portaria = BL_HandleNull(rsPrRubr!Portaria, "")
                EstrutPrec(TotalPrec).cod_rubr = BL_HandleNull(rsPrRubr!cod_rubr, "")
                EstrutPrec(TotalPrec).descr_rubr = BL_HandleNull(rsPrRubr!descr_rubr, "")
                EstrutPrec(TotalPrec).cod_rubr_efr = BL_HandleNull(rsPrRubr!cod_rubr_efr, "")
                EstrutPrec(TotalPrec).descr_rubr_efr = BL_HandleNull(rsPrRubr!descr_rubr_efr, "")
                EstrutPrec(TotalPrec).nr_c = BL_HandleNull(rsPrRubr!nr_c, "")
                EstrutPrec(TotalPrec).nr_k = BL_HandleNull(rsPrRubr!nr_k, "")
                EstrutPrec(TotalPrec).perc_pag_doe = BL_HandleNull(rsPrRubr!perc_pag_doe, "")
                'EstrutPrec(TotalPrec).perc_pag_efr = BL_HandleNull(rsPrRubr!perc_pag_ent, "")
                EstrutPrec(TotalPrec).t_fac = BL_HandleNull(rsPrRubr!t_fac, "")
                EstrutPrec(TotalPrec).val_pag_doe = BL_HandleNull(rsPrRubr!val_pag_doe, "")
                EstrutPrec(TotalPrec).val_pag_ent = BL_HandleNull(rsPrRubr!val_pag_ent, "")
                EstrutPrec(TotalPrec).val_Taxa = BL_HandleNull(rsPrRubr!val_Taxa, "")
                EstrutPrec(TotalPrec).user_cri = BL_HandleNull(rsPrRubr!user_cri, "")
                EstrutPrec(TotalPrec).user_act = BL_HandleNull(rsPrRubr!user_act, "")
                EstrutPrec(TotalPrec).dt_cri = BL_HandleNull(rsPrRubr!dt_cri, "")
                EstrutPrec(TotalPrec).dt_act = BL_HandleNull(rsPrRubr!dt_act, "")
                 
                FgPrecos.TextMatrix(TotalPrec, lColPrecosCodRubr) = EstrutPrec(TotalPrec).cod_rubr
                FgPrecos.TextMatrix(TotalPrec, lColPrecosDescrRubr) = EstrutPrec(TotalPrec).descr_rubr
                FgPrecos.TextMatrix(TotalPrec, lColPrecosCodRubrEFR) = EstrutPrec(TotalPrec).cod_rubr_efr
                FgPrecos.TextMatrix(TotalPrec, lColPrecosDescrRubrEFR) = EstrutPrec(TotalPrec).descr_rubr_efr
                FgPrecos.TextMatrix(TotalPrec, lColPrecosNumC) = EstrutPrec(TotalPrec).nr_c
                FgPrecos.TextMatrix(TotalPrec, lColPrecosNumK) = EstrutPrec(TotalPrec).nr_k
                FgPrecos.TextMatrix(TotalPrec, lColPrecosPercDoente) = EstrutPrec(TotalPrec).perc_pag_doe
                FgPrecos.TextMatrix(TotalPrec, lColPrecosPercEnt) = EstrutPrec(TotalPrec).perc_pag_efr
                If BL_HandleNull(rsPrRubr!t_fac, "") = 1 Then
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosTipoFact) = "KS E CS"
                ElseIf BL_HandleNull(rsPrRubr!t_fac, "") = 2 Then
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosTipoFact) = "EUROS"
                Else
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosTipoFact) = ""
                End If
                FgPrecos.TextMatrix(TotalPrec, lColPrecosValPagDoe) = EstrutPrec(TotalPrec).val_pag_doe
                FgPrecos.TextMatrix(TotalPrec, lColPrecosValPagEFR) = EstrutPrec(TotalPrec).val_pag_ent
                FgPrecos.TextMatrix(TotalPrec, lColPrecosTxModeradora) = EstrutPrec(TotalPrec).val_Taxa
                rsPrRubr.MoveNext
                
                FgPrecos.AddItem ""
            Wend
        End If
        rsPrRubr.Close
        RsFact.Close
        Set RsFact = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Carregar Precarios", Me.Name, "PreenchePrecarios"
    Exit Sub
    Resume Next
End Sub

Private Sub FgPrecos_Click()
    EcLinhaPreco = FgPrecos.row
    EcColunaPreco = FgPrecos.Col
End Sub

Private Sub FgPrecos_DblClick()
    EcLinhaPreco = ""
    EcColunaPreco = ""
    If FgPrecos.row > 0 And FgPrecos.row <= TotalPrec Then
        
        EcLinhaPreco = FgPrecos.row
        EcColunaPreco = FgPrecos.Col
        
        ' APENAS DEIXA EDITAR SE J� TIVER RUBRICA ASSOCIADA
        If FgPrecos.Col <> lColPrecosDescrRubr And FgPrecos.Col <> lColPrecosCodRubr And FgPrecos.Col <> lColPrecosTipoFact Then
            If EstrutPrec(EcLinhaPreco).cod_rubr <> "" Then
                If FgPrecos.Col = lColPrecosPercEnt And CkPercentagem.value <> vbChecked Then
                    Exit Sub
                End If
                EcAuxPreco.Visible = True
                EcAuxPreco.left = FgPrecos.CellLeft + FgPrecos.left
                EcAuxPreco.top = FgPrecos.CellTop + FgPrecos.top
                EcAuxPreco.Width = FgPrecos.CellWidth + 10
                EcAuxPreco.SetFocus
            Else
                BG_Mensagem mediMsgBox, "Ter� de indicar primeiro a r�brica!", vbInformation, ""
            End If
            
        ' COLUNA DO T_FAC
        ElseIf FgPrecos.Col = lColPrecosTipoFact Then
            If EstrutPrec(EcLinhaPreco).cod_rubr <> "" Then
                If EstrutPrec(EcLinhaPreco).t_fac = "1" Then
                    EstrutPrec(EcLinhaPreco).t_fac = "2"
                Else
                    EstrutPrec(EcLinhaPreco).t_fac = "1"
                End If
                FgPrecos.TextMatrix(EcLinhaPreco, lColPrecosTipoFact) = BL_SelCodigo_Sec("fa_tbf_t_fac", "DESCR_T_FAC", "COD_T_FAC", EstrutPrec(EcLinhaPreco).t_fac, "V")
            Else
                BG_Mensagem mediMsgBox, "Ter� de indicar primeiro a r�brica!", vbInformation, ""
            End If
        
        ' COLUNA DO CODIGO DA RUBRICA. APENAS PERMITE EDITAR SE FOR UMA LINHA NOVA, E NAO TIVER PREENCHIDA
        ElseIf FgPrecos.Col = lColPrecosCodRubr And EcLinhaPreco = TotalPrec And EstrutPrec(EcLinhaPreco).cod_rubr = "" Then
            EcAuxPreco.Visible = True
            EcAuxPreco.left = FgPrecos.CellLeft + FgPrecos.left
            EcAuxPreco.top = FgPrecos.CellTop + FgPrecos.top
            EcAuxPreco.Width = FgPrecos.CellWidth + 10
            EcAuxPreco.SetFocus
        Else
            Exit Sub
        End If
        
        EstrutPrec(EcLinhaPreco).flg_modificado = True
        If EstrutPrec(EcLinhaPreco).user_cri = "" Then
            EstrutPrec(EcLinhaPreco).user_cri = gCodUtilizador
            EstrutPrec(EcLinhaPreco).dt_cri = Bg_DaData_ADO
        Else
            EstrutPrec(EcLinhaPreco).user_act = gCodUtilizador
            EstrutPrec(EcLinhaPreco).dt_act = Bg_DaData_ADO
        End If
        
        If FgPrecos.Col = lColPrecosCodRubr Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).cod_rubr
        ElseIf FgPrecos.Col = lColPrecosCodRubrEFR Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).cod_rubr_efr
        ElseIf FgPrecos.Col = lColPrecosDescrRubrEFR Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).descr_rubr_efr
        ElseIf FgPrecos.Col = lColPrecosTxModeradora Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).val_Taxa
        ElseIf FgPrecos.Col = lColPrecosNumC Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).nr_c
        ElseIf FgPrecos.Col = lColPrecosNumK Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).nr_k
        ElseIf FgPrecos.Col = lColPrecosValPagEFR Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).val_pag_ent
        ElseIf FgPrecos.Col = lColPrecosValPagDoe Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).val_pag_doe
        ElseIf FgPrecos.Col = lColPrecosPercDoente Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).perc_pag_doe
        ElseIf FgPrecos.Col = lColPrecosPercEnt Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).perc_pag_efr
        End If
    End If
End Sub

Private Sub FgPrecos_GotFocus()
    EcAuxPreco.Visible = False
End Sub

Private Sub EcAuxPreco_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim sSql As String
    Dim rsPrRubr As New ADODB.recordset
    Dim i As Long
    Dim flg_existe As Boolean
    If KeyCode = vbKeyReturn Then
    
        ' COLUNA DA RUBRICA
        If EcLinhaPreco <> "" And EcColunaPreco <> "" Then
            If EcColunaPreco = lColPrecosCodRubr Then
                If EcAuxPreco <> EstrutPrec(EcLinhaPreco).cod_rubr Then
                    sSql = "SELECT * FROM fa_rubr WHERE cod_rubr = " & EcAuxPreco
                    rsPrRubr.CursorType = adOpenStatic
                    rsPrRubr.CursorLocation = adUseServer
                    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                    rsPrRubr.Open sSql, gConexaoSecundaria
                    If rsPrRubr.RecordCount >= 1 Then
                        flg_existe = False
                        For i = 1 To TotalPrec - 1
                            If EstrutPrec(i).cod_rubr = EcAuxPreco Then
                                flg_existe = True
                            End If
                        Next
                        If flg_existe = False Then
                            EstrutPrec(EcLinhaPreco).cod_rubr = EcAuxPreco
                            EstrutPrec(EcLinhaPreco).descr_rubr = BL_HandleNull(rsPrRubr!descr_rubr, "")
                            FgPrecos.Col = lColPrecosCodRubrEFR
                        Else
                            EstrutPrec(EcLinhaPreco).cod_rubr = ""
                            EstrutPrec(EcLinhaPreco).descr_rubr = ""
                            EcAuxPreco = ""
                            BG_Mensagem mediMsgBox, "J� existe essa r�brica codificada para este pre��rio!", vbInformation, ""
                            FgPrecos.Col = lColPrecosCodRubr
                        End If
                    Else
                        EstrutPrec(EcLinhaPreco).cod_rubr = ""
                        EstrutPrec(EcLinhaPreco).descr_rubr = ""
                        EcAuxPreco = ""
                        BG_Mensagem mediMsgBox, "N�o existe r�brica com esse c�digo !", vbInformation, ""
                        FgPrecos.Col = lColPrecosCodRubr
                    End If
                    rsPrRubr.Close
                End If
                
            ElseIf EcColunaPreco = lColPrecosCodRubrEFR Then
                EstrutPrec(EcLinhaPreco).cod_rubr_efr = EcAuxPreco
                FgPrecos.Col = lColPrecosDescrRubrEFR
            ElseIf EcColunaPreco = lColPrecosDescrRubrEFR Then
                FgPrecos.Col = lColPrecosTxModeradora
                EstrutPrec(EcLinhaPreco).descr_rubr_efr = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosTxModeradora Then
                If IsNumeric(EcAuxPreco) Then
                    EstrutPrec(EcLinhaPreco).val_Taxa = EcAuxPreco
                    FgPrecos.Col = lColPrecosTipoFact
                Else
                    EcAuxPreco = EstrutPrec(EcLinhaPreco).val_Taxa
                End If
            ElseIf EcColunaPreco = lColPrecosNumC Then
                If IsNumeric(EcAuxPreco) Then
                    EstrutPrec(EcLinhaPreco).nr_c = EcAuxPreco
                    FgPrecos.Col = lColPrecosValPagEFR
                Else
                    EcAuxPreco = EstrutPrec(EcLinhaPreco).nr_c
                End If
            ElseIf EcColunaPreco = lColPrecosNumK Then
                If IsNumeric(EcAuxPreco) Then
                    EstrutPrec(EcLinhaPreco).nr_k = EcAuxPreco
                    FgPrecos.Col = lColPrecosNumC
                Else
                    EcAuxPreco = EstrutPrec(EcLinhaPreco).nr_k
                End If
            ElseIf EcColunaPreco = lColPrecosPercDoente Then
                If IsNumeric(EcAuxPreco) Then
                    EstrutPrec(EcLinhaPreco).perc_pag_doe = EcAuxPreco
                    FgPrecos.Col = lColPrecosPercEnt
                Else
                    EcAuxPreco = EstrutPrec(EcLinhaPreco).perc_pag_doe
                End If
            ElseIf EcColunaPreco = lColPrecosValPagDoe Then
                If IsNumeric(EcAuxPreco) Then
                    EstrutPrec(EcLinhaPreco).val_pag_doe = EcAuxPreco
                    FgPrecos.Col = lColPrecosPercDoente
                Else
                    EcAuxPreco = EstrutPrec(EcLinhaPreco).val_pag_doe
                End If
            ElseIf EcColunaPreco = lColPrecosPercEnt Then
                If IsNumeric(EcAuxPreco) Then
                    EstrutPrec(EcLinhaPreco).perc_pag_efr = EcAuxPreco
                    FgPrecos.Col = lColPrecosCodRubr
                Else
                    EcAuxPreco = EstrutPrec(EcLinhaPreco).perc_pag_efr
                End If
            ElseIf EcColunaPreco = lColPrecosValPagEFR Then
                If IsNumeric(EcAuxPreco) Then
                    EstrutPrec(EcLinhaPreco).val_pag_ent = EcAuxPreco
                    FgPrecos.Col = lColPrecosValPagDoe
                Else
                    EcAuxPreco = EstrutPrec(EcLinhaPreco).val_pag_ent
                End If
            End If
            
            FgPrecos.TextMatrix(EcLinhaPreco, EcColunaPreco) = EcAuxPreco
            If EcColunaPreco = lColPrecosCodRubr Then
                FgPrecos.TextMatrix(EcLinhaPreco, lColPrecosDescrRubr) = EstrutPrec(EcLinhaPreco).descr_rubr
            End If
            EcAuxPreco = ""
            EcAuxPreco.Visible = False
            FgPrecos.SetFocus
        End If
    End If
End Sub

Private Sub EcAuxPreco_LostFocus()
    EcAuxPreco.Visible = False
    EcAuxPreco = ""
    FgPrecos.SetFocus
End Sub

Private Sub EcAuxPreco_Validate(Cancel As Boolean)
    EcAuxPreco.Visible = False
    EcAuxPreco = ""
    FgPrecos.SetFocus
End Sub

' ------------------------------------------------------

' REMOVE UMA LINHA DA GRID DE PRECOS

' ------------------------------------------------------
Private Sub RemoveLinha(linha As Long)
    Dim i As Long
    Dim sSql As String
    On Error GoTo TrataErro
    If linha > TotalPrec Then
        Exit Sub
    End If
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer Remover estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If gMsgResp = vbYes Then
        
        sSql = "DELETE FROM fa_pr_rubr WHERE portaria = " & EstrutPrec(linha).Portaria & " AND cod_rubr = " & EstrutPrec(linha).cod_rubr
        gConexaoSecundaria.Execute sSql
        
        If linha < TotalPrec Then
            For i = linha To TotalPrec - 1
                MoveEstru i + 1, i
            Next
        End If
        
        TotalPrec = TotalPrec - 1
        ReDim Preserve EstrutPrec(TotalPrec)
        FgPrecos.RemoveItem linha
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a apagar linha", Me.Name, "RemoveLinha"
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------

' MOVE ESTRUTURA

' ------------------------------------------------------
Private Sub MoveEstru(linhaOrigem As Long, linhaDestino As Long)
    EstrutPrec(linhaDestino).cod_rubr = EstrutPrec(linhaOrigem).cod_rubr
    EstrutPrec(linhaDestino).descr_rubr = EstrutPrec(linhaOrigem).descr_rubr
    EstrutPrec(linhaDestino).Portaria = EstrutPrec(linhaOrigem).Portaria
    EstrutPrec(linhaDestino).descr_Portaria = EstrutPrec(linhaOrigem).descr_Portaria
    EstrutPrec(linhaDestino).cod_rubr_efr = EstrutPrec(linhaOrigem).cod_rubr_efr
    EstrutPrec(linhaDestino).descr_rubr_efr = EstrutPrec(linhaOrigem).descr_rubr_efr
    EstrutPrec(linhaDestino).val_Taxa = EstrutPrec(linhaOrigem).val_Taxa
    EstrutPrec(linhaDestino).t_fac = EstrutPrec(linhaOrigem).t_fac
    EstrutPrec(linhaDestino).nr_k = EstrutPrec(linhaOrigem).nr_k
    EstrutPrec(linhaDestino).nr_c = EstrutPrec(linhaOrigem).nr_c
    EstrutPrec(linhaDestino).val_pag_ent = EstrutPrec(linhaOrigem).val_pag_ent
    EstrutPrec(linhaDestino).perc_pag_doe = EstrutPrec(linhaOrigem).perc_pag_doe
    EstrutPrec(linhaDestino).perc_pag_efr = EstrutPrec(linhaOrigem).perc_pag_efr
    EstrutPrec(linhaDestino).val_pag_doe = EstrutPrec(linhaOrigem).val_pag_doe
    EstrutPrec(linhaDestino).user_cri = EstrutPrec(linhaOrigem).user_cri
    EstrutPrec(linhaDestino).dt_cri = EstrutPrec(linhaOrigem).dt_cri
    EstrutPrec(linhaDestino).user_act = EstrutPrec(linhaOrigem).user_act
    EstrutPrec(linhaDestino).dt_act = EstrutPrec(linhaOrigem).dt_act
    EstrutPrec(linhaDestino).empresa_id = EstrutPrec(linhaOrigem).empresa_id
    
    EstrutPrec(linhaDestino).flg_modificado = EstrutPrec(linhaOrigem).flg_modificado

End Sub

' ------------------------------------------------------

' GRAVA DADOS DA TABELA DE PRECOS

' ------------------------------------------------------

Private Function GravaPrecarios() As Boolean
    Dim sSql As String
    Dim i As Long
    On Error GoTo TrataErro
    
    GravaPrecarios = False
    
    For i = 1 To TotalPrec
    
        If EstrutPrec(i).flg_modificado = True And EstrutPrec(i).cod_rubr <> "" Then
            sSql = "DELETE FROM fa_pr_rubr WHERE portaria = " & EstrutPrec(i).Portaria & " AND cod_rubr = " & EstrutPrec(i).cod_rubr
            gConexaoSecundaria.Execute sSql
        
            'If EstrutPrec(i).cod_rubr_efr <> "" Or EstrutPrec(i).nr_c <> "" Or EstrutPrec(i).nr_k <> "" Or EstrutPrec(i).perc_pag_doe <> "" _
                    Or EstrutPrec(i).val_pag_doe <> "" Or EstrutPrec(i).Val_pag_Ent <> "" Or EstrutPrec(i).val_pag_doe <> "" _
                    Or EstrutPrec(i).val_Taxa <> "" Or EstrutPrec(i).perc_pag_doe <> "" Then
                    
                sSql = "INSERT INTO fa_pr_rubr (cod_rubr, portaria, cod_Efr, cod_rubr_efr, descr_rubr_efr, val_taxa, t_fac, nr_k, nr_c, "
                sSql = sSql & " val_pag_ent, perc_pag_doe, val_pag_doe,user_cri, dt_cri, user_act, dt_act ) VALUES ("
                sSql = sSql & EstrutPrec(i).cod_rubr & ", "
                sSql = sSql & EstrutPrec(i).Portaria & ", "
                sSql = sSql & EcCodigo & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).cod_rubr_efr) & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).descr_rubr_efr) & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).val_Taxa, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).t_fac, ",", "."), 2) & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).nr_k, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).nr_c, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).val_pag_ent, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).perc_pag_doe, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).val_pag_doe, ",", "."), "NULL") & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).user_cri) & ", "
                sSql = sSql & BL_TrataDataParaBD(EstrutPrec(i).dt_cri) & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).user_act) & ", "
                sSql = sSql & BL_TrataDataParaBD(EstrutPrec(i).dt_act) & ") "
                
                gConexaoSecundaria.Execute sSql
            'End If
            
        End If
    Next
    GravaPrecarios = True
Exit Function
TrataErro:
    GravaPrecarios = False
    BG_LogFile_Erros "Erro a Gravar Pre�arios:" & Err.Description, Me.Name, "GravaPrecarios"
    Exit Function
    Resume Next
End Function



' ------------------------------------------------------

' CARREGA PRECOS POR GRUPO DE RUBRICAS

' ------------------------------------------------------
Private Sub PreenchePrGrupRubr()

    Dim sSql As String
    Dim RsFact As New ADODB.recordset
    Dim rsPrRubr As New ADODB.recordset
    On Error GoTo TrataErro
    
    EcValC = ""
    EcValK = ""
    EcCodGrEFR = ""
    EcDescrGrEFR = ""
    sSql = "SELECT * FROM fa_pr_grup_rubr where portaria = " & EcCodPortaria & " AND COD_GRUPO in(" & gGrupoRubricaFactus & ")"
    RsFact.CursorType = adOpenStatic
    RsFact.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsFact.Open sSql, gConexaoSecundaria
    If RsFact.RecordCount = 1 Then
        EcValC = BL_HandleNull(RsFact!valor_c, "0")
        EcValK = BL_HandleNull(RsFact!valor_k, "0")
        EcCodGrEFR = BL_HandleNull(RsFact!cod_grupo_efr, "")
        EcDescrGrEFR = BL_HandleNull(RsFact!descr_grupo_efr, "")
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Gravar PRECOS POR GRUPO DE RUBRICAS ", Me.Name, "PreenchePrGrupRubr"
    Exit Sub
    Resume Next
End Sub

  

' ------------------------------------------------------

' GRAVA PRECOS POR GRUPO DE RUBRICAS

' ------------------------------------------------------

Private Function GravaPrGrupRubr() As Boolean
    Dim sSql As String
    Dim i As Long
    On Error GoTo TrataErro
    
    If EcCodPortaria = "" Or EcCodEFRRef = "" Then Exit Function
    GravaPrGrupRubr = False
    
    If EcCodGrEFR = "" Or gLAB = "CEDIVET" Then
        GravaPrGrupRubr = True
        Exit Function
    End If
    
    sSql = "DELETE FROM fa_pr_grup_rubr WHERE portaria = " & EcCodPortaria & " AND cod_grupo in ( " & gGrupoRubricaFactus & ")"
    gConexaoSecundaria.Execute sSql
        
        
    sSql = "INSERT INTO fa_pr_grup_rubr (cod_grupo , portaria, cod_efr, cod_grupo_efr, descr_grupo_efr, valor_k, valor_c,"
    sSql = sSql & "  user_cri, dt_cri ) VALUES ("
    sSql = sSql & gGrupoRubricaFactus & ", "
    sSql = sSql & EcCodPortaria & ", "
    sSql = sSql & EcCodEFRRef & ", "
    sSql = sSql & BL_TrataStringParaBD(EcCodGrEFR) & ", "
    sSql = sSql & BL_TrataStringParaBD(EcDescrGrEFR) & ", "
    sSql = sSql & BL_HandleNull(Replace(EcValK, ",", "."), "NULL") & ", "
    sSql = sSql & BL_HandleNull(Replace(EcValC, ",", "."), "NULL") & ", "
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ") "
    gConexaoSecundaria.Execute sSql
    GravaPrGrupRubr = True
Exit Function
TrataErro:
    GravaPrGrupRubr = False
    BG_LogFile_Erros "Erro a Gravar PRECOS POR GRUPO DE RUBRICAS", Me.Name, "GravaPrGrupRubr"
    gConexaoSecundaria.RollbackTrans
    Exit Function
    Resume Next
End Function



' ------------------------------------------------------

' PREENCHE DADOS DE ECRA PARA INSERIR NOVA PORTARIA

' ------------------------------------------------------
Private Sub PreencheDadosNovaPortaria()
    Dim sSql As String
    Dim rsPort As New ADODB.recordset
    Dim rsGrupRubr As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT * FROM fa_portarias WHERE portaria = " & EcCodPortaria
    rsPort.CursorType = adOpenStatic
    rsPort.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsPort.Open sSql, gConexaoSecundaria
    If rsPort.RecordCount = 1 Then
        EcCodEfrRefNovaPort = EcCodEFRRef
        EcDescrEfrRefNovaPort = EcDescrEFRRef
        EcDescrNovaPort = BL_HandleNull(rsPort!descr_Port, "")
        EcUserCriNovaPort = BL_HandleNull(rsPort!user_cri, "")
        EcDtFimNovaPort = ""
        EcDtIniNovaPort = Bg_DaData_ADO
        PreencheEmpresaNovaPort BL_HandleNull(rsPort!empresa_id, "")
    End If
    rsPort.Close
    Set rsPort = Nothing
    
    sSql = "SELECT * FROM fa_pr_grup_rubr WHERE portaria = " & EcCodPortaria
    rsGrupRubr.CursorType = adOpenStatic
    rsGrupRubr.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsGrupRubr.Open sSql, gConexaoSecundaria
    If rsGrupRubr.RecordCount = 1 Then
        EcCodGrRubrNovaPort = BL_HandleNull(rsGrupRubr!cod_grupo, "")
        EcCodGrRubrNovaPort_validate False
        EcCodGrEfrNovaPort = BL_HandleNull(rsGrupRubr!cod_grupo_efr, "")
        EcDescrGrEfrNovaPort = BL_HandleNull(rsGrupRubr!descr_grupo_efr, "")
        EcValCNovaPort = BL_HandleNull(rsGrupRubr!valor_c, "")
        EcValKNovaPort = BL_HandleNull(rsGrupRubr!valor_k, "")
    End If
    rsGrupRubr.Close
    Set rsGrupRubr = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher dados para a nova portaria: " & Err.Description, Me.Name, "PreencheDadosNovaPortaria"
    gConexaoSecundaria.RollbackTrans
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------

' GRAVA DADOS DA NOVA PORTARIA

' ------------------------------------------------------
Private Sub GravaDadosNovaPortaria()
    Dim sSql As String
    Dim rsPort As New ADODB.recordset
    Dim empresa As String
    On Error GoTo TrataErro
    
    gConexaoSecundaria.BeginTrans
    ' ------------------------------------------------------
    ' VERIFICA SE OS CAMPOS EST�O TODOS PREENCHIDOS.
    ' ------------------------------------------------------
    If EcCodNovaPort = "" Then
        BG_Mensagem mediMsgBox, "O Campo C�digo da Portaria tem de estar preenchido.", vbInformation, ""
        Exit Sub
    End If
    If EcDescrNovaPort = "" Then
        BG_Mensagem mediMsgBox, "O Campo Descricao da Portaria tem de estar preenchido.", vbInformation, ""
        Exit Sub
    End If
    If EcCodEfrRefNovaPort = "" Then
        BG_Mensagem mediMsgBox, "O Campo Entidade de Refer�ncia tem de estar preenchido.", vbInformation, ""
        Exit Sub
    End If
    If EcDtIniNovaPort = "" Then
        BG_Mensagem mediMsgBox, "O Campo Data de inicio tem de estar preenchido.", vbInformation, ""
        Exit Sub
    End If
    If EcCodGrEfrNovaPort = "" Then
        BG_Mensagem mediMsgBox, "O Campo Codigo Grupo de Entidades tem de estar preenchido.", vbInformation, ""
        Exit Sub
    End If
    If EcDescrGrEfrNovaPort = "" Then
        BG_Mensagem mediMsgBox, "O Campo descric��o Grupo de Entidades tem de estar preenchido.", vbInformation, ""
        Exit Sub
    End If
    If EcCodGrRubrNovaPort = "" Then
        BG_Mensagem mediMsgBox, "O Campo  Grupo de R�bricas tem de estar preenchido.", vbInformation, ""
        Exit Sub
    End If
    If CbEmpresaNovaPort.ListIndex = -1 Then
        BG_Mensagem mediMsgBox, "O Campo Empresa tem de estar preenchido.", vbInformation, ""
        Exit Sub
    End If
     
    ' ------------------------------------------------------
    ' VERIFICA SE JA HA ALGUMA PORTARIA COM MESMO CODIGO
    ' ------------------------------------------------------
    sSql = "SELECT * FROM fa_portarias WHERE portaria = " & EcCodNovaPort
    rsPort.CursorType = adOpenStatic
    rsPort.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsPort.Open sSql, gConexaoSecundaria
    If rsPort.RecordCount >= 1 Then
        BG_Mensagem mediMsgBox, "C�digo da portaria j� existe.", vbInformation, ""
        Exit Sub
    End If
    rsPort.Close
    Set rsPort = Nothing
    
    empresa = BL_SelCodigo("SL_EMPRESAS", "cod_empresa", "SEQ_EMPRESA", CbEmpresaNovaPort.ItemData(CbEmpresaNovaPort.ListIndex))
    
    ' ------------------------------------------------------
    ' GRAVA DADOS NA TABELA DE PORTARIAS
    ' ------------------------------------------------------
    sSql = "INSERT INTO fa_portarias (cod_Efr, portaria,descr_port, dt_ini, dt_fim, user_cri, dt_Cri,empresa_id) VALUES( "
    sSql = sSql & EcCodEfrRefNovaPort & ","
    sSql = sSql & EcCodNovaPort & ","
    sSql = sSql & BL_TrataStringParaBD(EcDescrEfrRefNovaPort) & ","
    sSql = sSql & BL_TrataDataParaBD(EcDtIniNovaPort) & ","
    sSql = sSql & BL_TrataDataParaBD(EcDtFimNovaPort) & ","
    sSql = sSql & BL_TrataStringParaBD(EcUserCriNovaPort) & ","
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ","
    sSql = sSql & BL_TrataStringParaBD(empresa) & ")"
    gConexaoSecundaria.Execute sSql
    
    
    ' ------------------------------------------------------
    ' GRAVA DADOS NA TABELA DE PRECOS POR GRUPO DE RUBRICAS
    ' ------------------------------------------------------
    sSql = "INSERT INTO fa_pr_grup_rubr (cod_grupo, portaria,cod_efr, cod_grupo_efr, descr_grupo_efr,valor_k, valor_c,user_Cri, dt_cri, empresa_id) VALUES("
    sSql = sSql & EcCodGrRubrNovaPort & ","
    sSql = sSql & EcCodNovaPort & ","
    sSql = sSql & EcCodEfrRefNovaPort & ","
    sSql = sSql & BL_TrataStringParaBD(EcCodGrEfrNovaPort) & ","
    sSql = sSql & BL_TrataStringParaBD(EcDescrGrEfrNovaPort) & ","
    sSql = sSql & BL_HandleNull(Replace(EcValKNovaPort, ",", "."), "NULL") & ","
    sSql = sSql & BL_HandleNull(Replace(EcValCNovaPort, ",", "."), "NULL") & ","
    sSql = sSql & BL_TrataStringParaBD(EcUserCriNovaPort) & ","
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ","
    sSql = sSql & BL_TrataStringParaBD(empresa) & ")"
    gConexaoSecundaria.Execute sSql
    
    ' ------------------------------------------------------
    ' GRAVA DADOS NA TABELA DE PRECOS POR GRUPO DE RUBRICAS
    ' ------------------------------------------------------
    If CkGravaPrecosNovaPort.value = vbChecked Then
        sSql = "INSERT INTO fa_pr_rubr (cod_rubr, portaria, cod_Efr, cod_rubr_efr, descr_rubr_efr, val_Taxa, t_fac, nr_k, nr_c, val_pag_ent, "
        sSql = sSql & " val_pag_doe, perc_pag_doe, user_cri, dt_cri) "
        sSql = sSql & " SELECT cod_rubr, " & EcCodNovaPort & " portaria, " & EcCodEfrRefNovaPort & " cod_Efr, cod_rubr_efr, descr_rubr_efr, val_taxa,"
        sSql = sSql & " t_fac, nr_k, nr_c, val_pag_ent, val_pag_doe, perc_pag_doe, user_cri, " & BL_TrataDataParaBD(Bg_DaData_ADO) & " dt_cri "
        sSql = sSql & "  FROM fa_pr_rubr WHERE portaria =" & EcCodPortaria
        gConexaoSecundaria.Execute sSql
    End If
    gConexaoSecundaria.CommitTrans
    BtSairNovaPort_Click
Exit Sub
TrataErro:
    gConexaoSecundaria.RollbackTrans
    BG_LogFile_Erros "Erro ao Gravar dados para a nova portaria: " & Err.Description, Me.Name, "GravaDadosNovaPortaria"
    'gConexaoSecundaria.RollbackTrans
    Exit Sub
    Resume Next
 End Sub

' PFerreira 04.04.2007
Private Sub CbDestino_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    BG_LimpaOpcao CbDestino, KeyCode
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbDestino_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbDestino_KeyDown"
    Exit Sub
    Resume Next
End Sub



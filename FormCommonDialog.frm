VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormCommonDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCommonDialog"
   ClientHeight    =   14160
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   9270
   Icon            =   "FormCommonDialog.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   14160
   ScaleWidth      =   9270
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameNovoAnexo 
      Caption         =   "Novo Anexo"
      Height          =   6855
      Left            =   120
      TabIndex        =   0
      Top             =   8520
      Visible         =   0   'False
      Width           =   9015
      Begin VB.DirListBox EcDirectorias 
         Appearance      =   0  'Flat
         Height          =   4140
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   3615
      End
      Begin VB.DriveListBox EcDrives 
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   8655
      End
      Begin VB.FileListBox EcFicheiros 
         Appearance      =   0  'Flat
         Height          =   4320
         Left            =   4320
         TabIndex        =   4
         Top             =   720
         Width           =   4455
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   5400
         Width           =   8775
      End
      Begin VB.CommandButton BtOk 
         Height          =   615
         Left            =   6960
         Picture         =   "FormCommonDialog.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   6120
         Width           =   975
      End
      Begin VB.CommandButton BtCancelar 
         Height          =   615
         Left            =   8040
         Picture         =   "FormCommonDialog.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   6120
         Width           =   855
      End
   End
   Begin VB.CommandButton BtVer 
      Height          =   495
      Left            =   7200
      Picture         =   "FormCommonDialog.frx":0FE0
      Style           =   1  'Graphical
      TabIndex        =   21
      ToolTipText     =   "Consultar Anexo"
      Top             =   6360
      Width           =   495
   End
   Begin MSComctlLib.ImageList IL 
      Left            =   480
      Top             =   6360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormCommonDialog.frx":174A
            Key             =   "PDF"
            Object.Tag             =   "PDF"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormCommonDialog.frx":1AE4
            Key             =   "DOC"
            Object.Tag             =   "DOC"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormCommonDialog.frx":1E7E
            Key             =   "JPG"
            Object.Tag             =   "JPG"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormCommonDialog.frx":2218
            Key             =   "UTE"
            Object.Tag             =   "UTE"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormCommonDialog.frx":2EF2
            Key             =   "ANA"
            Object.Tag             =   "ANA"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormCommonDialog.frx":328C
            Key             =   "REQ"
            Object.Tag             =   "REQ"
         EndProperty
      EndProperty
   End
   Begin VB.TextBox EcUtente 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   20
      Top             =   120
      Width           =   1335
   End
   Begin VB.TextBox EcAnalise 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   3360
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   480
      Width           =   5415
   End
   Begin VB.TextBox EcRequisicao 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   14
      Top             =   480
      Width           =   1335
   End
   Begin VB.TextBox EcNomeUTe 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   3360
      Locked          =   -1  'True
      TabIndex        =   13
      Top             =   120
      Width           =   5415
   End
   Begin VB.CommandButton BtApagarAnexo 
      Height          =   495
      Left            =   7800
      Picture         =   "FormCommonDialog.frx":3F66
      Style           =   1  'Graphical
      TabIndex        =   12
      ToolTipText     =   "Apagar Anexo"
      Top             =   6360
      Width           =   495
   End
   Begin VB.CommandButton BtAnexos 
      Height          =   495
      Left            =   8400
      Picture         =   "FormCommonDialog.frx":46D0
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Novo Anexo"
      Top             =   6360
      Width           =   495
   End
   Begin VB.TextBox EcSeqRealiza 
      Height          =   285
      Left            =   -360
      TabIndex        =   9
      Top             =   7800
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcReq 
      Height          =   285
      Left            =   -360
      TabIndex        =   8
      Top             =   8160
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcseqUtente 
      Height          =   285
      Left            =   -360
      TabIndex        =   7
      Top             =   7440
      Visible         =   0   'False
      Width           =   1215
   End
   Begin MSComctlLib.TreeView TreeViewDoc 
      Height          =   5415
      Left            =   120
      TabIndex        =   10
      Top             =   840
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   9551
      _Version        =   393217
      Style           =   7
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin VB.Label Label1 
      Caption         =   "Requisi��o"
      Height          =   255
      Index           =   3
      Left            =   240
      TabIndex        =   19
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Utente"
      Height          =   255
      Index           =   2
      Left            =   240
      TabIndex        =   18
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Nome"
      Height          =   255
      Index           =   1
      Left            =   2760
      TabIndex        =   17
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "An�lise"
      Height          =   255
      Index           =   0
      Left            =   2760
      TabIndex        =   16
      Top             =   480
      Width           =   615
   End
End
Attribute VB_Name = "FormCommonDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim caminho As String
Dim nome_ficheiro As String
Public rs As ADODB.recordset
Private Type anexo
    seq_anexo As Long
    nome_computador As String
    nome_ficheiro As String
    Origem As String
    user_cri As String
    dt_cri As String
    hr_cri As String
    extensao As String
    indice As Integer
End Type
Private Type analise
    seq_realiza As Long
    cod_ana As String
    descr_ana As String
    totalAnexos As Integer
    anexos() As anexo
    indice As Integer
End Type

Private Type requisicao
    n_req As String
    dt_chega As String
    totalAnalises As Integer
    analises() As analise
    totalAnexos As Integer
    anexos() As anexo
    indice As Integer
End Type

Private Type Utente
    seq_utente As String
    Utente As String
    t_utente As String
    nome_ute As String
    totalAnexos As Integer
    anexos() As anexo
    TotalRequisicoes As Integer
    requisicoes() As requisicao
    indice As Integer
End Type
Dim Utente As Utente

Private Sub BtAnexos_Click()
    FrameNovoAnexo.Visible = True
    FrameNovoAnexo.top = 5
    FrameNovoAnexo.left = 5
    
End Sub

Private Sub BtApagarAnexo_Click()
    If Mid(TreeViewDoc.SelectedItem.Key, 1, 1) = "S" Then
        DesactivaRegisto Mid(TreeViewDoc.SelectedItem.Key, 3)
    End If
End Sub

Private Sub BtVer_Click()

    On Error GoTo TrataErro
    
        If TreeViewDoc.SelectedItem.Index > mediComboValorNull Then
            If Mid(TreeViewDoc.SelectedItem.Key, 1, 1) = "S" Then
                ConsultarRegisto Mid(TreeViewDoc.SelectedItem.Key, 3)
            End If
        End If
        
TrataErro:
    Exit Sub
    Resume Next

End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = " Anexa��o de Documentos"
    Me.left = 0
    Me.top = 0
    Me.Width = 9360
    Me.Height = 7470 ' Normal
    Set CampoDeFocus = TreeViewDoc
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    Dim idxR As Integer
    Dim idxA As Integer
    Dim flg As Integer
    Dim i As Integer
    BG_StackJanelas_Pop
    Set gFormActivo = FormResultadosNovo
    BL_ToolbarEstadoN 0
    

    Set FormCommonDialog = Nothing
    If gF_IDENTIF = mediSim Then
        Set gFormActivo = FormIdentificaUtente
        If Utente.totalAnexos > 0 Then
            For i = 1 To Utente.totalAnexos
                gFormActivo.EcFlgAnexo.Text = "1"
            Next i
        Else
            gFormActivo.EcFlgAnexo.Text = "0"
        End If
        BL_VerificaAnexosUtente gFormActivo, gFormActivo.EcFlgAnexo.Text
    ElseIf gF_IDENTIF_VET = mediSim Then
        Set gFormActivo = FormIdentificaVet
    ElseIf gF_RESULT_NOVO = mediSim Then
        flg = 0
        For idxR = 1 To Utente.TotalRequisicoes
            If Utente.requisicoes(idxR).n_req = EcReq Then
                For idxA = 1 To Utente.requisicoes(idxR).totalAnalises
                    If Utente.requisicoes(idxR).analises(idxA).seq_realiza = EcSeqRealiza Then
                        If Utente.requisicoes(idxR).analises(idxA).totalAnexos > 0 Then
                            flg = 1
                        End If
                    End If
                Next
            End If
        Next
        Set gFormActivo = FormResultadosNovo
        FormResultadosNovo.AnexaDocumento flg
    ElseIf gF_REQUIS = mediSim Then
        flg = 0
        For idxR = 1 To Utente.TotalRequisicoes
            If Utente.requisicoes(idxR).n_req = EcReq Then
                If Utente.requisicoes(idxR).totalAnexos > 0 Then
                    flg = 1
                End If
                
            End If
        Next
        FormGestaoRequisicao.AnexaDocumento flg
        Set gFormActivo = FormGestaoRequisicao
    End If
End Sub

Sub LimpaCampos()
    EcNomeUte = ""
    EcRequisicao = ""
    EcUtente = ""
    EcAnalise = ""
    

End Sub

Sub DefTipoCampos()
    EcFicheiros.Pattern = "*.PDF"
    TreeViewDoc.LineStyle = tvwRootLines
    TreeViewDoc.ImageList = IL
End Sub
Sub PreencheValoresDefeito()
     
    'BRUNODSANTOS 09.03.2017 - Cedivet-206
    If gParamPathAnexo <> "-1" Then
        EcDirectorias.Path = gParamPathAnexo
    Else
         EcDirectorias.Path = EcDrives
    End If
    '
    
    
End Sub

Sub PreencheCampos()

End Sub


Sub FuncaoLimpar()
    

End Sub

Sub FuncaoEstadoAnterior()

End Sub


Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsID As New ADODB.recordset
    If EcSeqRealiza <> "" Then
        sSql = "SELECT DISTINCT x1.seq_utente, x1.nome_ute, x1.utente, x1.t_utente, x2.n_Req, x4.descr_ana "
        sSql = sSql & " FROM sl_identif x1, sl_requis x2, sl_realiza x3, slv_analiseS_apenas x4 "
        sSql = sSql & " WHERE x1.seq_utente =X2.seq_utente AND x2.n_Req = x3.n_req AND x4.cod_ana = x3.cod_agrup "
        sSql = sSql & " AND x1.seq_utente = " & EcSeqUtente
        sSql = sSql & " AND x2.n_req = " & EcReq
        sSql = sSql & " AND x3.seq_realiza = " & EcSeqRealiza
        sSql = sSql & " UNION SELECT DISTINCT x1.seq_utente, x1.nome_ute, x1.utente, x1.t_utente, x2.n_Req, x4.descr_ana "
        sSql = sSql & " FROM sl_identif x1, sl_requis x2, sl_realiza_h x3, slv_analiseS_apenas x4 "
        sSql = sSql & " WHERE x1.seq_utente =X2.seq_utente AND x2.n_Req = x3.n_req AND x4.cod_ana = x3.cod_agrup "
        sSql = sSql & " AND x1.seq_utente = " & EcSeqUtente
        sSql = sSql & " AND x2.n_req = " & EcReq
        sSql = sSql & " AND x3.seq_realiza = " & EcSeqRealiza
    
    ElseIf EcReq <> "" Then
        sSql = "SELECT DISTINCT x1.seq_utente, x1.nome_ute, x1.utente, x1.t_utente, x2.n_Req, null descr_ana "
        sSql = sSql & " FROM sl_identif x1, sl_requis x2 "
        sSql = sSql & " WHERE x1.seq_utente =X2.seq_utente "
        sSql = sSql & " AND x1.seq_utente = " & EcSeqUtente
        sSql = sSql & " AND x2.n_req = " & EcReq
    ElseIf EcSeqUtente <> "" Then
        sSql = "SELECT DISTINCT x1.seq_utente, x1.nome_ute, x1.utente, x1.t_utente, null n_Req, null descr_ana "
        sSql = sSql & " FROM sl_identif x1 "
        sSql = sSql & " WHERE x1.seq_utente = " & EcSeqUtente
    End If
    rsID.CursorLocation = adUseServer
    rsID.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsID.Open sSql, gConexao
    If rsID.RecordCount > 0 Then
        EcUtente = BL_HandleNull(rsID!t_utente, "") & "/" & BL_HandleNull(rsID!Utente, "")
        EcNomeUte = BL_HandleNull(rsID!nome_ute, "")
        EcRequisicao = BL_HandleNull(rsID!n_req, "")
        EcAnalise = BL_HandleNull(rsID!descr_ana, "")
        
        Utente.seq_utente = BL_HandleNull(rsID!seq_utente, mediComboValorNull)
        Utente.nome_ute = BL_HandleNull(rsID!nome_ute, "")
        Utente.t_utente = BL_HandleNull(rsID!t_utente, "")
        Utente.Utente = BL_HandleNull(rsID!Utente, "")
        Utente.indice = 1
        Utente.totalAnexos = 0
        ReDim Utente.anexos(0)
        Utente.TotalRequisicoes = 0
        ReDim Utente.requisicoes(0)
        PreencheAnexos
    End If
    rsID.Close
    Set rsID = Nothing
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()


End Sub



Sub FuncaoModificar()

End Sub


Private Sub BtCancelar_Click()
    FrameNovoAnexo.Visible = False
End Sub

Private Sub BtOk_Click()
    Dim sSql As String
    Dim extensao As String
    Dim seq_anexo As Long
    If EcNome <> "" Then
        extensao = Mid(EcNome, InStrRev(EcNome, "."))
        If UCase(extensao) <> ".PDF" Then
            BG_Mensagem mediMsgBox, "Apenas � permitido anexar PDF's!", vbExclamation, "Anexo"
            Exit Sub
        End If
    End If
    
    If EcSeqRealiza = "" And EcSeqUtente = "" And EcReq = "" Then
        Exit Sub
    End If
    seq_anexo = RB_NovoRegisto(CLng(BL_HandleNull(EcSeqUtente, mediComboValorNull)), CLng(BL_HandleNull(EcReq, mediComboValorNull)), _
                               CLng(BL_HandleNull(EcSeqRealiza, mediComboValorNull)), nome_ficheiro, caminho, extensao)
                               
    If seq_anexo > mediComboValorNull Then
    End If
    FrameNovoAnexo.Visible = False
    PreencheAnexos
End Sub

Private Sub EcDirectorias_Change()
    EcFicheiros.Path = EcDirectorias.Path
    EcNome = EcDirectorias.Path
End Sub

Private Sub EcDirectorias_Click()
    EcFicheiros.Path = EcDirectorias.Path
    EcNome = EcDirectorias.Path
End Sub

Private Sub EcDrives_Change()
On Error Resume Next
   
    EcDirectorias.Path = EcDrives
   
    EcNome = EcDirectorias.Path

End Sub

Private Sub EcFicheiros_Click()
    If EcFicheiros.ListIndex <> mediComboValorNull Then
        EcNome = EcFicheiros.Path & "\" & EcFicheiros.List(EcFicheiros.ListIndex)
        caminho = EcFicheiros.Path
        nome_ficheiro = EcFicheiros.List(EcFicheiros.ListIndex)
    Else
        EcNome = ""
        caminho = ""
        nome_ficheiro = ""
    End If
End Sub

Private Sub PreencheAnexos()
    Dim sSql As String
    Dim rsID As New ADODB.recordset
    Dim root As Node
    limpaUtente
    TreeViewDoc.Nodes.Clear
    Set root = TreeViewDoc.Nodes.Add(, tvwChild, "U_" & Utente.seq_utente, Utente.nome_ute, "UTE")
    root.Expanded = False
    
    sSql = "SELECT * FROM sl_anexos WHERE seq_utente = " & Utente.seq_utente & " AND (flg_invisivel IS NULL OR flg_invisivel = 0) ORDER by seq_utente, n_Req, seq_realiza"
    rsID.CursorLocation = adUseServer
    rsID.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsID.Open sSql, gConexao
    If rsID.RecordCount > 0 Then
        While Not rsID.EOF
            If BL_HandleNull(rsID!seq_realiza, mediComboValorNull) = mediComboValorNull And BL_HandleNull(rsID!n_req, mediComboValorNull) = mediComboValorNull Then
                PreencheAnexoUTente BL_HandleNull(rsID!dt_cri, ""), BL_HandleNull(rsID!extensao, ""), BL_HandleNull(rsID!hr_cri, ""), _
                                    BL_HandleNull(rsID!nome_computador, ""), BL_HandleNull(rsID!nome_ficheiro, ""), BL_HandleNull(rsID!Origem, ""), _
                                    BL_HandleNull(rsID!seq_anexo, ""), BL_HandleNull(rsID!user_cri, "")
            ElseIf BL_HandleNull(rsID!seq_realiza, mediComboValorNull) = mediComboValorNull And BL_HandleNull(rsID!n_req, mediComboValorNull) <> mediComboValorNull Then
                Call PreencheAnexoReq(BL_HandleNull(rsID!n_req, ""), BL_HandleNull(rsID!dt_cri, ""), BL_HandleNull(rsID!extensao, ""), BL_HandleNull(rsID!hr_cri, ""), _
                        BL_HandleNull(rsID!nome_computador, ""), BL_HandleNull(rsID!nome_ficheiro, ""), BL_HandleNull(rsID!Origem, ""), _
                        BL_HandleNull(rsID!seq_anexo, ""), BL_HandleNull(rsID!user_cri, ""))
            ElseIf BL_HandleNull(rsID!seq_realiza, mediComboValorNull) <> mediComboValorNull Then
                Call PreencheAnexoAna(BL_HandleNull(rsID!n_req, ""), BL_HandleNull(rsID!seq_realiza, ""), BL_HandleNull(rsID!dt_cri, ""), BL_HandleNull(rsID!extensao, ""), BL_HandleNull(rsID!hr_cri, ""), _
                        BL_HandleNull(rsID!nome_computador, ""), BL_HandleNull(rsID!nome_ficheiro, ""), BL_HandleNull(rsID!Origem, ""), _
                        BL_HandleNull(rsID!seq_anexo, ""), BL_HandleNull(rsID!user_cri, ""))
            
            End If
            rsID.MoveNext
        Wend
    End If
    rsID.Close
    Set rsID = Nothing

End Sub

Private Sub PreencheAnexoUTente(dt_cri As String, extensao As String, hr_cri As String, nome_computador As String, nome_ficheiro As String, Origem As String, _
                                seq_anexo As String, user_cri As String)
        Utente.totalAnexos = Utente.totalAnexos + 1
        ReDim Preserve Utente.anexos(Utente.totalAnexos)
        
        Utente.anexos(Utente.totalAnexos).dt_cri = dt_cri
        Utente.anexos(Utente.totalAnexos).extensao = extensao
        Utente.anexos(Utente.totalAnexos).hr_cri = hr_cri
        Utente.anexos(Utente.totalAnexos).nome_computador = nome_computador
        Utente.anexos(Utente.totalAnexos).nome_ficheiro = nome_ficheiro
        Utente.anexos(Utente.totalAnexos).Origem = Origem
        Utente.anexos(Utente.totalAnexos).seq_anexo = seq_anexo
        Utente.anexos(Utente.totalAnexos).user_cri = user_cri
        
        Dim root As Node
        Set root = TreeViewDoc.Nodes.Add(Utente.indice, tvwChild, "S_" & Utente.anexos(Utente.totalAnexos).seq_anexo, Utente.anexos(Utente.totalAnexos).nome_ficheiro, "PDF")
        Utente.anexos(Utente.totalAnexos).indice = root.Index
End Sub
Private Sub PreencheAnexoReq(n_req As String, dt_cri As String, extensao As String, hr_cri As String, nome_computador As String, nome_ficheiro As String, Origem As String, _
                                seq_anexo As String, user_cri As String)
    Dim i As Integer
    Dim flg As Boolean
    Dim root As Node
    flg = False
    For i = 1 To Utente.TotalRequisicoes
        If Utente.requisicoes(i).n_req = n_req Then
            flg = True
            Exit For
        End If
    Next
    If flg = False Then
        Utente.TotalRequisicoes = Utente.TotalRequisicoes + 1
        i = Utente.TotalRequisicoes
        ReDim Preserve Utente.requisicoes(i)
        Utente.requisicoes(i).totalAnalises = 0
        Utente.requisicoes(i).totalAnexos = 0
        ReDim Utente.requisicoes(i).analises(0)
        ReDim Utente.requisicoes(i).anexos(0)
        Utente.requisicoes(i).n_req = n_req
        Set root = TreeViewDoc.Nodes.Add(Utente.indice, tvwChild, "R_" & Utente.requisicoes(i).n_req, Utente.requisicoes(i).n_req, "REQ")
        Utente.requisicoes(i).indice = root.Index
    End If
    
    Utente.requisicoes(i).totalAnexos = Utente.requisicoes(i).totalAnexos + 1
    ReDim Preserve Utente.requisicoes(i).anexos(Utente.requisicoes(i).totalAnexos)
    
    Utente.requisicoes(i).anexos(Utente.requisicoes(i).totalAnexos).dt_cri = dt_cri
    Utente.requisicoes(i).anexos(Utente.requisicoes(i).totalAnexos).extensao = extensao
    Utente.requisicoes(i).anexos(Utente.requisicoes(i).totalAnexos).hr_cri = hr_cri
    Utente.requisicoes(i).anexos(Utente.requisicoes(i).totalAnexos).nome_computador = nome_computador
    Utente.requisicoes(i).anexos(Utente.requisicoes(i).totalAnexos).nome_ficheiro = nome_ficheiro
    Utente.requisicoes(i).anexos(Utente.requisicoes(i).totalAnexos).Origem = Origem
    Utente.requisicoes(i).anexos(Utente.requisicoes(i).totalAnexos).seq_anexo = seq_anexo
    Utente.requisicoes(i).anexos(Utente.requisicoes(i).totalAnexos).user_cri = user_cri
    
    Set root = TreeViewDoc.Nodes.Add(Utente.requisicoes(i).indice, tvwChild, "S_" & Utente.requisicoes(i).anexos(Utente.requisicoes(i).totalAnexos).seq_anexo, Utente.requisicoes(i).anexos(Utente.requisicoes(i).totalAnexos).nome_ficheiro, "PDF")
    Utente.requisicoes(i).anexos(Utente.requisicoes(i).totalAnexos).indice = root.Index
End Sub

Private Sub limpaUtente()
    ReDim Utente.anexos(0)
    ReDim Utente.requisicoes(0)
    Utente.totalAnexos = 0
    Utente.TotalRequisicoes = 0
End Sub

Private Sub PreencheAnexoAna(n_req As String, seq_realiza As String, dt_cri As String, extensao As String, hr_cri As String, nome_computador As String, nome_ficheiro As String, Origem As String, _
                                seq_anexo As String, user_cri As String)

                                
    Dim idxR As Integer
    Dim idxA As Integer
    Dim flg As Boolean
    Dim root As Node
    flg = False
    For idxR = 1 To Utente.TotalRequisicoes
        If Utente.requisicoes(idxR).n_req = n_req Then
            flg = True
            Exit For
        End If
    Next
    
    If flg = False Then
        Utente.TotalRequisicoes = Utente.TotalRequisicoes + 1
        idxR = Utente.TotalRequisicoes
        ReDim Preserve Utente.requisicoes(idxR)
        Utente.requisicoes(idxR).totalAnalises = 0
        Utente.requisicoes(idxR).totalAnexos = 0
        ReDim Utente.requisicoes(idxR).analises(0)
        ReDim Utente.requisicoes(idxR).anexos(0)
        Utente.requisicoes(idxR).n_req = n_req
        Set root = TreeViewDoc.Nodes.Add(Utente.indice, tvwChild, "R_" & Utente.requisicoes(idxR).n_req, Utente.requisicoes(idxR).n_req, "REQ")
        Utente.requisicoes(idxR).indice = root.Index
    End If
    
    flg = False
    For idxA = 1 To Utente.requisicoes(idxR).totalAnalises
        If Utente.requisicoes(idxR).analises(idxA).seq_realiza = seq_realiza Then
            flg = True
            Exit For
        End If
    Next
    If flg = False Then
        Utente.requisicoes(idxR).totalAnalises = Utente.requisicoes(idxR).totalAnalises + 1
        ReDim Utente.requisicoes(idxR).analises(Utente.requisicoes(idxR).totalAnalises)
        idxA = Utente.requisicoes(idxR).totalAnalises
        Utente.requisicoes(idxR).analises(idxA).seq_realiza = seq_realiza
        Utente.requisicoes(idxR).analises(idxA).cod_ana = BL_SelCodigo("SL_REALIZA", "COD_ANA_S", "SEQ_REALIZA", Utente.requisicoes(idxR).analises(idxA).seq_realiza)
        Utente.requisicoes(idxR).analises(idxA).descr_ana = BL_SelCodigo("SLV_ANALISES_APENAS", "DESCR_ANA", "COD_ANA", Utente.requisicoes(idxR).analises(idxA).cod_ana, "V")
        Utente.requisicoes(idxR).analises(idxA).totalAnexos = 0
        ReDim Utente.requisicoes(idxR).analises(idxA).anexos(0)
        Set root = TreeViewDoc.Nodes.Add(Utente.requisicoes(idxR).indice, tvwChild, "A_" & Utente.requisicoes(idxR).analises(idxA).seq_realiza, Utente.requisicoes(idxR).analises(idxA).descr_ana, "ANA")
        Utente.requisicoes(idxR).analises(idxA).indice = root.Index
    End If
    Utente.requisicoes(idxR).analises(idxA).totalAnexos = Utente.requisicoes(idxR).analises(idxA).totalAnexos + 1
    ReDim Preserve Utente.requisicoes(idxR).analises(idxA).anexos(Utente.requisicoes(idxR).analises(idxA).totalAnexos)
    
    Utente.requisicoes(idxR).analises(idxA).anexos(Utente.requisicoes(idxR).analises(idxA).totalAnexos).dt_cri = dt_cri
    Utente.requisicoes(idxR).analises(idxA).anexos(Utente.requisicoes(idxR).analises(idxA).totalAnexos).extensao = extensao
    Utente.requisicoes(idxR).analises(idxA).anexos(Utente.requisicoes(idxR).analises(idxA).totalAnexos).hr_cri = hr_cri
    Utente.requisicoes(idxR).analises(idxA).anexos(Utente.requisicoes(idxR).analises(idxA).totalAnexos).nome_computador = nome_computador
    Utente.requisicoes(idxR).analises(idxA).anexos(Utente.requisicoes(idxR).analises(idxA).totalAnexos).nome_ficheiro = nome_ficheiro
    Utente.requisicoes(idxR).analises(idxA).anexos(Utente.requisicoes(idxR).analises(idxA).totalAnexos).Origem = Origem
    Utente.requisicoes(idxR).analises(idxA).anexos(Utente.requisicoes(idxR).analises(idxA).totalAnexos).seq_anexo = seq_anexo
    Utente.requisicoes(idxR).analises(idxA).anexos(Utente.requisicoes(idxR).analises(idxA).totalAnexos).user_cri = user_cri
    
    Set root = TreeViewDoc.Nodes.Add(Utente.requisicoes(idxR).analises(idxA).indice, tvwChild, "S_" & Utente.requisicoes(idxR).analises(idxA).anexos(Utente.requisicoes(idxR).analises(idxA).totalAnexos).seq_anexo, Utente.requisicoes(idxR).analises(idxA).anexos(Utente.requisicoes(idxR).analises(idxA).totalAnexos).nome_ficheiro, "PDF")
    Utente.requisicoes(idxR).anexos(Utente.requisicoes(idxR).totalAnexos).indice = root.Index
    
End Sub


Private Sub DesactivaRegisto(seq_anexo As Long)
    gMsgTitulo = "Eliminar"
    gMsgMsg = "Tem a certeza que quer eliminar o anexo seleccionado?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If gMsgResp = vbYes Then
        If RB_ApagarRegisto(BL_HandleNull(EcSeqUtente, mediComboValorNull), BL_HandleNull(EcReq, mediComboValorNull), BL_HandleNull(EcSeqRealiza, mediComboValorNull), CLng(seq_anexo)) = True Then
            FuncaoProcurar
        End If
    End If
End Sub
Private Sub ConsultarRegisto(seq_anexo As Long)
    Dim caminho
    caminho = RB_RetiraFicheiroBD(seq_anexo)
    If caminho <> "" Then
        gMsgTitulo = "Consultar"
        gMsgMsg = "Ficheiro Gravado em " & caminho & " Deseja abrir ficheiro?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            ShellExecute 1, "Open", caminho, ByVal 0&, 0&, SW_NORMAL
        End If
    End If
End Sub

Private Sub TreeViewDoc_DblClick()
    BtVer_Click
End Sub

VERSION 5.00
Begin VB.Form FormAlertas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Alertas"
   ClientHeight    =   6090
   ClientLeft      =   75
   ClientTop       =   405
   ClientWidth     =   13065
   Icon            =   "FormAlertas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6090
   ScaleWidth      =   13065
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox textAlerta 
      Enabled         =   0   'False
      Height          =   4560
      Left            =   5400
      MultiLine       =   -1  'True
      TabIndex        =   1
      Top             =   240
      Width           =   7575
   End
   Begin VB.ListBox lbAlertas 
      Height          =   4560
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   0
      Top             =   240
      Width           =   5175
   End
   Begin VB.Label lbErros 
      Height          =   615
      Left            =   240
      TabIndex        =   2
      Top             =   5160
      Width           =   12615
   End
End
Attribute VB_Name = "FormAlertas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Type EstruturaAlertas
    codigo As Long
    descricao As String
    dt_alerta As String
    texto As String
    visto As Boolean
    erro As String
End Type
Dim estrutAlertas() As EstruturaAlertas

Dim checked As Boolean

Public Sub LimpaCampos()
    lbAlertas.Clear
    textAlerta.text = ""
    lbErros.caption = ""
    ReDim estrutAlertas(0)
End Sub

Private Sub Form_Load()
 Inicializacoes
 LimpaCampos
 CarregaDados
End Sub

Private Sub Inicializacoes()
    Me.Height = 5460
End Sub
    
Private Sub CarregaDados()
        Dim sql As String
        Dim Rs As New ADODB.recordset
        Dim RsVisto As New ADODB.recordset
        Dim erro As String

        On Error GoTo TrataErro
        
        LimpaCampos
        erro = BL_Executa_Procedimento_Alertas(True)

        sql = "SELECT c.descricao, g.* FROM sl_alertas_globais g, sl_cod_alertas_globais c WHERE g.dt_fim IS NULL AND c.codigo = g.codigo and c.cod_local = g.cod_local AND g.cod_local = " & gCodLocal & " ORDER BY g.dt_alerta "
        Rs.CursorLocation = adUseClient
        Rs.CursorType = adOpenStatic
        Rs.Open sql, gConexao

        While Not Rs.EOF
            ReDim Preserve estrutAlertas(UBound(estrutAlertas) + 1)
            estrutAlertas(UBound(estrutAlertas)).codigo = Rs.Fields("codigo").value
            estrutAlertas(UBound(estrutAlertas)).dt_alerta = BL_HandleNull(Rs.Fields("dt_alerta").value, "")
            estrutAlertas(UBound(estrutAlertas)).texto = BL_HandleNull(Rs.Fields("texto").value, "")
            sql = "SELECT count(*) conta FROM sl_alertas_globais_vistos where codigo = " & estrutAlertas(UBound(estrutAlertas)).codigo & " AND cod_utilizador = " & CStr(gCodUtilizador) & " AND cod_local = " & gCodLocal
            RsVisto.CursorLocation = adUseClient
            RsVisto.CursorType = adOpenStatic
            RsVisto.Open sql, gConexao
            If RsVisto.Fields("conta").value > 0 Then
                estrutAlertas(UBound(estrutAlertas)).visto = True
            Else
                estrutAlertas(UBound(estrutAlertas)).visto = False
            End If
            estrutAlertas(UBound(estrutAlertas)).erro = erro
            estrutAlertas(UBound(estrutAlertas)).descricao = BL_HandleNull(Rs.Fields("descricao").value, "")
            RsVisto.Close
            Rs.MoveNext
       Wend
       PreencheListaAlertas
       checked = True
       Exit Sub
TrataErro:
        BG_LogFile_Erros "FormAlertas - CarregaDados - Erro: " & Err.Description
        Exit Sub
        Resume Next

End Sub

 Private Sub PreencheListaAlertas()
    Dim i As Integer
    
    On Error GoTo TrataErro
    
    checked = False
    lbAlertas.Clear
    
    For i = 1 To UBound(estrutAlertas)
        lbAlertas.AddItem estrutAlertas(i).descricao
        
        If estrutAlertas(i).visto Then
         lbAlertas.Selected(i - 1) = True
        Else
          lbAlertas.Selected(i - 1) = False
        End If
        
        If estrutAlertas(i).erro <> "" Then
           Me.Height = 6495
           lbErros.caption = estrutAlertas(i).erro
        End If
    Next i
    'carregar ao iniciar apenas na TextBox o primeiro alerta
    textAlerta.text = estrutAlertas(1).texto
    Exit Sub
TrataErro:
        BG_LogFile_Erros "FormAlertas - PreencheGrelha - Erro: " & Err.Description
        Exit Sub
        Resume Next

    End Sub

Private Sub Form_Unload(Cancel As Integer)
    ActualizaAlertas
    eAlertas = BL_Devolve_Existem_Alertas_Activos()
End Sub

Private Sub lbAlertas_Click()
 Dim indice As Integer
 indice = lbAlertas.ListIndex + 1
 If indice > 0 And indice <= UBound(estrutAlertas) Then
    textAlerta.text = estrutAlertas(indice).texto
 End If
End Sub

Private Sub lbAlertas_ItemCheck(Item As Integer)
 If checked And lbAlertas.ListCount > 0 Then
  If estrutAlertas(Item + 1).visto = False Then
    estrutAlertas(Item + 1).visto = True
    lbAlertas.Selected(Item) = True
  Else
    estrutAlertas(Item + 1).visto = False
    lbAlertas.Selected(Item) = False
  End If
 End If
End Sub

Private Sub ActualizaAlertas()
    On Error GoTo TrataErro

    Dim i As Integer
    Dim sql As String
    Dim Rs As New ADODB.recordset

    For i = 1 To UBound(estrutAlertas)
        sql = "DELETE FROM sl_alertas_globais_vistos WHERE codigo = " & estrutAlertas(i).codigo & " AND cod_utilizador = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & " AND cod_local = " & gCodLocal
        Rs.CursorLocation = adUseClient
        Rs.CursorType = adOpenStatic
        BG_ExecutaQuery_ADO (sql)
        If estrutAlertas(i).visto = True Then
            sql = "SELECT count(*) conta FROM sl_alertas_globais WHERE codigo = " & estrutAlertas(i).codigo & " AND cod_local = " & gCodLocal
            Rs.Open sql, gConexao
            If Rs.Fields("conta").value > 0 Then
                sql = "INSERT INTO sl_alertas_globais_vistos (codigo,cod_utilizador,cod_local) VALUES (" & estrutAlertas(i).codigo & "," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & gCodLocal & ") "
                BG_ExecutaQuery_ADO (sql)
            End If
            Rs.Close
        End If
    Next
    Exit Sub
TrataErro:
        BG_LogFile_Erros "ActualizaAlertas - Erro: " & Err.Description
        Exit Sub
        Resume Next
End Sub

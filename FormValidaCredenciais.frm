VERSION 5.00
Begin VB.Form FormValidaCredenciais 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8760
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13095
   FillStyle       =   0  'Solid
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8760
   ScaleWidth      =   13095
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   7815
      Left            =   360
      TabIndex        =   0
      Top             =   960
      Width           =   11535
      Begin VB.CommandButton cmdProcessarLista 
         Caption         =   "Processar lista DB"
         Height          =   495
         Left            =   8880
         TabIndex        =   33
         Top             =   2040
         Width           =   1935
      End
      Begin VB.TextBox txtEpisodioEditar 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3960
         TabIndex        =   23
         Top             =   1440
         Width           =   1095
      End
      Begin VB.TextBox txtRubricaEditar 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         TabIndex        =   22
         Top             =   1440
         Width           =   1575
      End
      Begin VB.TextBox txtOrdemEditar 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7080
         TabIndex        =   21
         Top             =   1440
         Width           =   1335
      End
      Begin VB.TextBox txtValorEditar 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5160
         Locked          =   -1  'True
         TabIndex        =   20
         Top             =   1440
         Width           =   1815
      End
      Begin VB.TextBox txtErrosDet 
         Height          =   1335
         Left            =   240
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   16
         Top             =   5640
         Width           =   8175
      End
      Begin VB.ListBox lstErrosLista 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2580
         Left            =   240
         TabIndex        =   15
         Top             =   2640
         Width           =   8175
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Processar Lista.txt"
         Height          =   495
         Left            =   8760
         TabIndex        =   5
         Top             =   480
         Width           =   1695
      End
      Begin VB.TextBox txtCredencialEditar 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   4
         Top             =   1440
         Width           =   2175
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Check1"
         Height          =   375
         Left            =   9120
         TabIndex        =   3
         Top             =   1440
         Value           =   1  'Checked
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   375
         Left            =   9120
         TabIndex        =   2
         Top             =   1080
         Value           =   1  'Checked
         Width           =   255
      End
      Begin VB.TextBox txtCredencialCheckDigit 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   2040
         Width           =   2175
      End
      Begin VB.Label lblOrdemLista 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Ordem"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   7080
         TabIndex        =   32
         Top             =   1080
         Width           =   1365
      End
      Begin VB.Label lblValorLista 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Valor"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   5160
         TabIndex        =   31
         Top             =   1080
         Width           =   1845
      End
      Begin VB.Label lblEpisodioLista 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Requisicao"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   3960
         TabIndex        =   30
         Top             =   1080
         Width           =   1050
      End
      Begin VB.Label lblRubricaLista 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "R�brica"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   2400
         TabIndex        =   29
         Top             =   1080
         Width           =   1575
      End
      Begin VB.Label lblCredencialLista 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Credencial"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   240
         TabIndex        =   28
         Top             =   1080
         Width           =   2130
      End
      Begin VB.Label lblOrdem 
         AutoSize        =   -1  'True
         Caption         =   "Ordem"
         Height          =   195
         Left            =   7560
         TabIndex        =   27
         Top             =   840
         Width           =   465
      End
      Begin VB.Label lblValor 
         AutoSize        =   -1  'True
         Caption         =   "Valor"
         Height          =   195
         Left            =   5160
         TabIndex        =   26
         Top             =   840
         Width           =   360
      End
      Begin VB.Label lblEpisodio 
         AutoSize        =   -1  'True
         Caption         =   "Requisicao"
         Height          =   195
         Left            =   3960
         TabIndex        =   25
         Top             =   840
         Width           =   795
      End
      Begin VB.Label lblRubrica 
         AutoSize        =   -1  'True
         Caption         =   "R�brica"
         Height          =   195
         Left            =   2400
         TabIndex        =   24
         Top             =   840
         Width           =   555
      End
      Begin VB.Label lblErrosDet 
         Caption         =   "Listagem de erros"
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   5400
         Width           =   2775
      End
      Begin VB.Label lblErrosLista 
         Caption         =   "lblErrosLista"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   2400
         Width           =   8175
      End
      Begin VB.Label lblErrosTotal 
         Alignment       =   2  'Center
         Caption         =   "lblErrosTotal"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   5280
         Width           =   8175
      End
      Begin VB.Label lblCredencial 
         AutoSize        =   -1  'True
         Caption         =   "Credencial"
         Height          =   195
         Left            =   240
         TabIndex        =   14
         Top             =   840
         Width           =   750
      End
      Begin VB.Label Label9 
         Alignment       =   2  'Center
         Height          =   195
         Left            =   2760
         TabIndex        =   13
         Top             =   5760
         Width           =   5325
      End
      Begin VB.Label Label8 
         Alignment       =   2  'Center
         Height          =   195
         Left            =   240
         TabIndex        =   12
         Top             =   5760
         Width           =   1995
      End
      Begin VB.Label Label7 
         Height          =   255
         Left            =   2760
         TabIndex        =   11
         Top             =   480
         Width           =   2655
      End
      Begin VB.Label lblCheckDigitRecalculado 
         AutoSize        =   -1  'True
         Caption         =   "Check Digit Recalculado"
         Height          =   195
         Left            =   360
         TabIndex        =   10
         Top             =   1800
         Width           =   1770
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Verificar 04 MCDT"
         Height          =   195
         Left            =   9480
         TabIndex        =   9
         Top             =   1515
         Width           =   1305
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Check digit x = X"
         Height          =   195
         Left            =   9480
         TabIndex        =   8
         Top             =   1155
         Width           =   1200
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Height          =   195
         Left            =   360
         TabIndex        =   7
         Top             =   1680
         Width           =   525
      End
      Begin VB.Label lblPassosGeral 
         Alignment       =   1  'Right Justify
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   5880
         Width           =   2175
      End
   End
End
Attribute VB_Name = "FormValidaCredenciais"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cCol1 = "Credencial"
Private Const cCol2 = "R�brica"
Private Const cCol3 = "Epis�dio"
Private Const cCol4 = "Valor"
Private Const cCol5 = "Ordem"

Private Const cCredValida = "Credencial V�lida"
Private Const cCredInvalida = "Credencial Inv�lida"
Private Const COD_HT = 9

Private Type ExtLinha
    ext0_n_seq_prog As String
    ext1_nr_req_ars As String
    ext2_cod_rubr_efr As String
    ext3_episodio As String
    ext4_val_total As String
    ext5_n_ord_ins As String
    ext6_erros As String
End Type

Private Ext() As ExtLinha
Private ExtErros() As ExtLinha
Private ExtSelecao As ExtLinha

Private Function SumCheckDigit(ByVal str As String) As Integer

    Dim check As Long
    Dim i As Integer
    Dim c As String
    
    check = 0
    For i = 1 To Len(str)
        c = CInt(Mid(str, i, 1))
            
        check = 2 * (check + c)
        
    Next i
    SumCheckDigit = check Mod 11

End Function


Private Function CalculaCheckDigit(str As String) As String

    Dim c As Integer
   
    c = (12 - SumCheckDigit(str)) Mod 11
    CalculaCheckDigit = IIf(c = 10, "X", c)
    
End Function

Private Function VerificaExtruturaCredencial(ByVal str As String) As String

    Dim identifARS As String
    Dim cd As String
    Dim zq As String
    
    str = Trim(str)
    
    If Len(str) <> 19 Then
        If Len(str) = 1 Then
            VerificaExtruturaCredencial = Len(str) & " d�gito"
        Else
            VerificaExtruturaCredencial = Len(str) & " d�gitos"
        End If
        Exit Function
    End If
    If IsNumeric(left(str, 18)) = False Then
        VerificaExtruturaCredencial = "N�o num�rica"
        Exit Function
    End If
    
    identifARS = CInt(left(str, 1))
    If identifARS < 1 Or identifARS > 5 Then
        VerificaExtruturaCredencial = "1� d�gito est� errado"
        Exit Function
    End If

    cd = right(str, 1)
    If FormValidaCredenciais.Check1.Value = vbUnchecked And cd = "x" Then
        VerificaExtruturaCredencial = "Check Digit 'x' min�sculo"
        Exit Function
    End If
    
    zq = Mid(str, 2, 2)
    If FormValidaCredenciais.Check2.Value = vbChecked And zq <> "04" Then
        VerificaExtruturaCredencial = "Posi��es 2 e 3 <> '04'"
        Exit Function
    End If
    
    If IsNumeric(cd) = False And UCase(cd) <> "X" Then
        VerificaExtruturaCredencial = "Check Digit <>('X' ou n�mero)"
        Exit Function
    End If
    
    VerificaExtruturaCredencial = cCredValida

End Function


Private Function DevolveErrosCredencial(ByVal str As String) As String

    Dim identifARS As String
    Dim cd As String
    Dim zq As String
    Dim drt As String
    Dim esq As String
    
    str = Trim(str)
    
verificacao_0:
On Error GoTo verificacao_1
    If Len(str) <> 19 Then
        If Len(str) = 1 Then
            DevolveErrosCredencial = DevolveErrosCredencial & "Credencial : " & Len(str) & " d�gito" & vbCrLf
        Else
            DevolveErrosCredencial = DevolveErrosCredencial & "Credencial : " & Len(str) & " d�gitos" & vbCrLf
        End If
        
    End If
    
verificacao_1:
On Error GoTo verificacao_2
    If IsNumeric(left(str, 18)) = False Then
        DevolveErrosCredencial = DevolveErrosCredencial & "Credencial : " & "N�o num�rica" & vbCrLf
        
    End If
    

verificacao_2:
On Error GoTo verificacao_3
    identifARS = CInt(left(str, 1))
    If identifARS < 1 Or identifARS > 5 Then
        DevolveErrosCredencial = DevolveErrosCredencial & "Credencial : " & "1� d�gito est� errado" & vbCrLf
        
    End If

verificacao_3:
On Error GoTo verificacao_4
    cd = right(str, 1)
    If FormValidaCredenciais.Check1.Value = vbUnchecked And cd = "x" Then
        DevolveErrosCredencial = DevolveErrosCredencial & "Credencial : " & "Check Digit 'x' min�sculo" & vbCrLf
        
    End If

verificacao_4:
On Error GoTo verificacao_5
    zq = Mid(str, 2, 2)
    If FormValidaCredenciais.Check2.Value = vbChecked And zq <> "04" Then
        DevolveErrosCredencial = DevolveErrosCredencial & "Credencial : " & "Posi��es 2 e 3 <> '04'" & vbCrLf
        
    End If

verificacao_5:
On Error GoTo verificacao_6
    If IsNumeric(cd) = False And UCase(cd) <> "X" Then
        DevolveErrosCredencial = DevolveErrosCredencial & "Credencial : " & "Check Digit <>('X' ou n�mero)" & vbCrLf
        
    End If

verificacao_6:
On Error GoTo verificacao_7
    'valida check digit
    If Len(Trim(str)) = 19 Then
        drt = right(Trim(str), 1)
        esq = left(Trim(str), 18)
        If IsNumeric(esq) Then
            cd = CalculaCheckDigit(esq)
            If FormValidaCredenciais.Check1.Value = vbChecked Then
                drt = UCase(drt)
            End If
            If drt <> cd Then
                DevolveErrosCredencial = DevolveErrosCredencial & "Credencial : " & "Check Digit da Credencial - Calculado : '" & right(Trim(str), 1) & "' - '" & cd & "'" & vbCrLf
            End If
        End If
    End If
verificacao_7:

End Function

Private Function DevolveErrosRubrica(ByVal str As String) As String

   
    str = Trim(str)
    
verificacao_0:
On Error GoTo verificacao_1
    If Len(str) < 5 Or Len(str) > 6 Then
        If Len(str) = 1 Then
            DevolveErrosRubrica = DevolveErrosRubrica & "Rubrica : " & Len(str) & " d�gito" & vbCrLf
        Else
            DevolveErrosRubrica = DevolveErrosRubrica & "Rubrica : " & Len(str) & " d�gitos" & vbCrLf
        End If
        
    End If
    
verificacao_1:

End Function

Private Function DevolveErrosEpisodio(ByVal str As String) As String

   
    str = Trim(str)
    
verificacao_0:
On Error GoTo verificacao_1
    If IsNumeric(str) = False Then
        DevolveErrosEpisodio = DevolveErrosEpisodio & "Episodio : " & "N�o num�rico" & vbCrLf
       
    End If
    
verificacao_1:

End Function

Private Function DevolveErrosValor(ByVal str As String) As String

   
    str = Trim(str)
    
verificacao_0:
On Error GoTo verificacao_1
    If IsNumeric(str) = False Then
        DevolveErrosValor = DevolveErrosValor & "Valor : " & "N�o num�rico" & vbCrLf
    ElseIf BL_String2Double(str) <= 0 Then
        DevolveErrosValor = DevolveErrosValor & "Valor : " & "<= 0" & vbCrLf
    End If
    
verificacao_1:

End Function
Private Function DevolveErrosOrdem(ByVal str As String) As String

   
    str = Trim(str)
    
verificacao_0:
On Error GoTo verificacao_1
    If IsNumeric(str) = False Then
        DevolveErrosOrdem = DevolveErrosOrdem & "Ordem : " & "N�o num�rico" & vbCrLf
    ElseIf BL_String2Double(str) < 1 Or BL_String2Double(str) > 6 Then
        DevolveErrosOrdem = DevolveErrosOrdem & "Ordem : " & "Menor que 1 ou maior que 5" & vbCrLf
    End If
    
verificacao_1:

End Function
Private Function DevolveErrosPK_ARS_Episodio(ByVal indice As Long, ByRef str() As ExtLinha) As String

   Dim j As Long
   Dim credencial As String
   Dim credencialAnterior As String
   Dim credencialActual As String
   Dim testeCredencial As String
   Dim episodio() As String
   Dim episodioActual As String
   Dim existeEpisodio As Boolean
   Dim indiceActual As Long
   
   ReDim episodio(0)
   credencial = str(indice).ext1_nr_req_ars
   episodio(0) = str(indice).ext3_episodio
    
verificacao_0:
On Error GoTo verificacao_1
    
    indiceActual = indice
    credencialAnterior = credencial
    While indiceActual > LBound(str) And credencialAnterior = credencial
        indiceActual = indiceActual - 1
        credencialAnterior = str(indiceActual).ext1_nr_req_ars
        If credencialAnterior <> credencial Then
            indiceActual = indiceActual + 1
        End If
    Wend
    
    credencialActual = credencial
    While indiceActual < UBound(str) And credencialActual = credencial
        credencialActual = str(indiceActual).ext1_nr_req_ars
        episodioActual = str(indiceActual).ext3_episodio
        If credencialActual = credencial Then
            existeEpisodio = False
            For j = LBound(episodio) To UBound(episodio)
                If episodio(j) = str(indiceActual).ext3_episodio Then
                    existeEpisodio = True
                End If
            Next j
            If existeEpisodio = False Then 'credencial associada a um epis�dio diferente
                ReDim Preserve episodio(UBound(episodio) + 1)
                episodio(UBound(episodio)) = str(indiceActual).ext3_episodio
                DevolveErrosPK_ARS_Episodio = DevolveErrosPK_ARS_Episodio & "Chave �nica : " & "Credencial '" & credencial & "' associada aos epis�dios '" & episodio(0) & "' e '" & episodio(UBound(episodio)) & "'" & vbCrLf
            End If
        End If
        indiceActual = indiceActual + 1
    Wend
    
    
    
verificacao_1:

End Function
Private Function ProcessaListaDB()

    Dim RsDados As ADODB.recordset
    Dim sql As String
    Dim tColunas(4) As Integer
    Dim i As Long
    Dim iBD_Aberta As Long
    ReDim Ext(0)
    ReDim ExtErros(0)
    
    iBD_Aberta = BL_Abre_Conexao_Secundaria(gSGBD_SECUNDARIA)
    
    If iBD_Aberta = 0 Then
        MsgBox "Imposs�vel abrir liga��o ao Factus !"
        Exit Function
    End If
    
    Set RsDados = New ADODB.recordset
    sql = " SELECT   lf.n_seq_prog, " & _
        "            lf.nr_req_ars, " & _
        "            lf.cod_rubr_efr, " & _
        "            lf.episodio, " & _
        "            lf.val_total, " & _
        "            lf.n_ord_ins " & _
        "     FROM   fa_lin_fact lf " & _
        "    WHERE   serie_fac = 'LOTES2011' AND n_fac = '73' " & _
        " ORDER BY   nr_req_ars, episodio "
        
    With RsDados
        .Source = sql
        .CursorLocation = adUseClient
        .CursorType = adOpenStatic
        .Open , gConexaoSecundaria
    End With
    
    If RsDados.RecordCount > 0 Then
        While Not RsDados.EOF
            If UBound(Ext) > 0 Or Ext(0).ext1_nr_req_ars <> "" Then
                ReDim Preserve Ext(UBound(Ext) + 1)
            End If
            Ext(UBound(Ext)).ext0_n_seq_prog = BL_HandleNull(RsDados!n_seq_prog, "")
            Ext(UBound(Ext)).ext1_nr_req_ars = BL_HandleNull(RsDados!nr_req_ars, "")
            Ext(UBound(Ext)).ext2_cod_rubr_efr = BL_HandleNull(RsDados!cod_rubr_efr, "")
            Ext(UBound(Ext)).ext3_episodio = BL_HandleNull(RsDados!episodio, "")
            Ext(UBound(Ext)).ext4_val_total = BL_HandleNull(RsDados!val_total, "")
            Ext(UBound(Ext)).ext5_n_ord_ins = BL_HandleNull(RsDados!n_ord_ins, "")
            Ext(UBound(Ext)).ext6_erros = ""
            
            RsDados.MoveNext
        Wend
        
        tColunas(0) = Len(cCol1)
        tColunas(1) = Len(cCol2)
        tColunas(2) = Len(cCol3)
        tColunas(3) = Len(cCol4)
        tColunas(4) = Len(cCol5)
        
        'processa extrutura Ext
        If UBound(Ext) > 0 Or Ext(0).ext1_nr_req_ars <> "" Then
            For i = LBound(Ext) To UBound(Ext)
                Ext(i).ext6_erros = DevolveErros(Ext(i))
                If Ext(i).ext6_erros <> "" Then
                    If UBound(ExtErros) > 0 Or ExtErros(0).ext1_nr_req_ars <> "" Then
                        ReDim Preserve ExtErros(UBound(ExtErros) + 1)
                    End If
                    ExtErros(UBound(ExtErros)).ext0_n_seq_prog = Ext(i).ext0_n_seq_prog
                    ExtErros(UBound(ExtErros)).ext1_nr_req_ars = Ext(i).ext1_nr_req_ars
                    ExtErros(UBound(ExtErros)).ext2_cod_rubr_efr = Ext(i).ext2_cod_rubr_efr
                    ExtErros(UBound(ExtErros)).ext3_episodio = Ext(i).ext3_episodio
                    ExtErros(UBound(ExtErros)).ext4_val_total = Ext(i).ext4_val_total
                    ExtErros(UBound(ExtErros)).ext5_n_ord_ins = Ext(i).ext5_n_ord_ins
                    ExtErros(UBound(ExtErros)).ext6_erros = Ext(i).ext6_erros
                    
                    If Len(Ext(i).ext1_nr_req_ars) > tColunas(0) Then tColunas(0) = Len(Ext(i).ext1_nr_req_ars)
                    If Len(Ext(i).ext2_cod_rubr_efr) > tColunas(1) Then tColunas(1) = Len(Ext(i).ext2_cod_rubr_efr)
                    If Len(Ext(i).ext3_episodio) > tColunas(2) Then tColunas(2) = Len(Ext(i).ext3_episodio)
                    If Len(Ext(i).ext4_val_total) > tColunas(3) Then tColunas(3) = Len(Ext(i).ext4_val_total)
                    If Len(Ext(i).ext5_n_ord_ins) > tColunas(4) Then tColunas(4) = Len(Ext(i).ext5_n_ord_ins)
                End If
                
            Next i
            lblErrosLista.caption = left(cCol1 & Space(tColunas(0)), tColunas(0)) & Space(7) & _
                                left(cCol2 & Space(tColunas(1)), tColunas(1)) & Space(7) & _
                                left(cCol3 & Space(tColunas(2)), tColunas(2)) & Space(7) & _
                                left(cCol4 & Space(tColunas(3)), tColunas(3)) & Space(7) & _
                                cCol5
            lblErrosLista.Font = lstErrosLista.Font
            
            
            
            If UBound(ExtErros) > 0 Or ExtErros(0).ext1_nr_req_ars <> "" Then
                For i = LBound(ExtErros) To UBound(ExtErros)
                    lstErrosLista.AddItem left(ExtErros(i).ext1_nr_req_ars & Space(tColunas(0)), tColunas(0)) & "   |   " & _
                                    left(ExtErros(i).ext2_cod_rubr_efr & Space(tColunas(1)), tColunas(1)) & "   |   " & _
                                    left(ExtErros(i).ext3_episodio & Space(tColunas(2)), tColunas(2)) & "   |   " & _
                                    left(ExtErros(i).ext4_val_total & Space(tColunas(3)), tColunas(3)) & "   |   " & _
                                    ExtErros(i).ext5_n_ord_ins
                Next i
            End If
            
            If ExtErros(0).ext1_nr_req_ars <> "" Then
                lblErrosTotal = "Total : " & UBound(ExtErros) - LBound(ExtErros) + 1
            Else
                lblErrosTotal = "Total : 0"
            End If
        End If
    End If
    
    RsDados.Close
    Set RsDados = Nothing
    
End Function

Private Function ProcessaLista()
    Dim f As Integer
    Dim tmp As String
    Dim tmp2 As String
    Dim esq As String
    Dim drt As String
    Dim result As String
    Dim cd As String
    Dim validacaoExtrutura As String
    Dim fich As String
    Dim posSC As String
    Dim linha As String
    Dim tokens() As String
    Dim rv As Integer
    Dim i As Long
    Dim nLinhas As Long
    Dim contadorLinhas As Long
    Dim tColunas(4) As Integer
    Dim percentagem As Integer
    Dim percentagemAnterior As Integer
    Dim credencialAnterior As String
    
    
    credencialAnterior = ""
    
    fich = App.Path & "\lista.txt"
    If Dir(fich, vbArchive) = "" Then
        MsgBox "Ficheiro 'Lista.txt' inexistente !"
        Exit Function
    End If
    f = FreeFile
    
    ReDim Ext(0)
    ReDim ExtErros(0)
    
    nLinhas = 1
    Open fich For Input As #f
    Do
        Line Input #f, linha
        nLinhas = nLinhas + 1
    Loop Until EOF(f)
    Close #f
    f = FreeFile
    Open fich For Input As #f
    Do
        Line Input #f, linha
        
        tmp = linha
        tmp = Replace(tmp, """", "")
        tmp = Replace(tmp, "'", "")
        tmp = Replace(tmp, Chr(COD_HT), ";")
        While InStr(1, tmp, ";;") > 0
            tmp = Replace(tmp, ";;", ";")
        Wend
        
        tmp = Trim(tmp)
        
        While right(tmp, 1) = ";"
            tmp = left(tmp, Len(tmp) - 1)
        Wend
        
        While left(tmp, 1) = ";"
            tmp = right(tmp, Len(tmp) - 1)
        Wend
                
        posSC = InStr(1, tmp, ";")
        
        rv = BL_Tokenize(tmp, ";", tokens)
        
        If tmp <> "" Then
            'verificar credenciais
'            If posSC > 0 Then
'                tmp = left(tmp, posSC - 1)
'            End If
'            If tmp <> credencialAnterior Then
'                validacaoExtrutura = VerificaExtruturaCredencial(tmp)
'                If validacaoExtrutura = cCredValida Then
'                    drt = right(Trim(tmp), 1)
'                    esq = left(Trim(tmp), Len(tmp) - 1)
'                    cd = CalculaCheckDigit(esq)
'                    If FormValidaCredenciais.Check1.Value = vbChecked Then
'                        drt = UCase(drt)
'                    End If
'                    If drt <> cd Then
'                        lstErrosLista.AddItem Trim(tmp) & vbTab & left(cCredInvalida & Space(35), 35) & vbTab & drt & vbTab & cd
'                   End If
'                End If
'            End If
            'verificar tudo
            If UBound(tokens) = 4 Then
                If UBound(Ext) > 0 Or Ext(0).ext1_nr_req_ars <> "" Then
                    ReDim Preserve Ext(UBound(Ext) + 1)
                End If
                Ext(UBound(Ext)).ext0_n_seq_prog = UBound(Ext)
                Ext(UBound(Ext)).ext1_nr_req_ars = Trim(tokens(0))
                Ext(UBound(Ext)).ext2_cod_rubr_efr = Trim(tokens(1))
                Ext(UBound(Ext)).ext3_episodio = Trim(tokens(2))
                Ext(UBound(Ext)).ext4_val_total = Trim(tokens(3))
                Ext(UBound(Ext)).ext5_n_ord_ins = Trim(tokens(4))
                Ext(UBound(Ext)).ext6_erros = ""
            End If
            credencialAnterior = tmp
            
        End If
        
    Loop Until EOF(f)

    Close #f
    
    
    tColunas(0) = Len(cCol1)
    tColunas(1) = Len(cCol2)
    tColunas(2) = Len(cCol3)
    tColunas(3) = Len(cCol4)
    tColunas(4) = Len(cCol5)
    
    'processa extrutura Ext
    If UBound(Ext) > 0 Or Ext(0).ext1_nr_req_ars <> "" Then
        For i = LBound(Ext) To UBound(Ext)
            Ext(i).ext6_erros = DevolveErros(Ext(i))
            If Ext(i).ext6_erros <> "" Then
                If UBound(ExtErros) > 0 Or ExtErros(0).ext1_nr_req_ars <> "" Then
                    ReDim Preserve ExtErros(UBound(ExtErros) + 1)
                End If
                ExtErros(UBound(ExtErros)).ext0_n_seq_prog = UBound(ExtErros)
                ExtErros(UBound(ExtErros)).ext1_nr_req_ars = Ext(i).ext1_nr_req_ars
                ExtErros(UBound(ExtErros)).ext2_cod_rubr_efr = Ext(i).ext2_cod_rubr_efr
                ExtErros(UBound(ExtErros)).ext3_episodio = Ext(i).ext3_episodio
                ExtErros(UBound(ExtErros)).ext4_val_total = Ext(i).ext4_val_total
                ExtErros(UBound(ExtErros)).ext5_n_ord_ins = Ext(i).ext5_n_ord_ins
                ExtErros(UBound(ExtErros)).ext6_erros = Ext(i).ext6_erros
                
                If Len(Ext(i).ext1_nr_req_ars) > tColunas(0) Then tColunas(0) = Len(Ext(i).ext1_nr_req_ars)
                If Len(Ext(i).ext2_cod_rubr_efr) > tColunas(1) Then tColunas(1) = Len(Ext(i).ext2_cod_rubr_efr)
                If Len(Ext(i).ext3_episodio) > tColunas(2) Then tColunas(2) = Len(Ext(i).ext3_episodio)
                If Len(Ext(i).ext4_val_total) > tColunas(3) Then tColunas(3) = Len(Ext(i).ext4_val_total)
                If Len(Ext(i).ext5_n_ord_ins) > tColunas(4) Then tColunas(4) = Len(Ext(i).ext5_n_ord_ins)
            End If
            
        Next i
        lblErrosLista.caption = left(cCol1 & Space(tColunas(0)), tColunas(0)) & Space(7) & _
                            left(cCol2 & Space(tColunas(1)), tColunas(1)) & Space(7) & _
                            left(cCol3 & Space(tColunas(2)), tColunas(2)) & Space(7) & _
                            left(cCol4 & Space(tColunas(3)), tColunas(3)) & Space(7) & _
                            cCol5
        lblErrosLista.Font = lstErrosLista.Font
        
        If UBound(ExtErros) > 0 Or ExtErros(0).ext1_nr_req_ars <> "" Then
            For i = LBound(ExtErros) To UBound(ExtErros)
                lstErrosLista.AddItem left(ExtErros(i).ext1_nr_req_ars & Space(tColunas(0)), tColunas(0)) & "   |   " & _
                                left(ExtErros(i).ext2_cod_rubr_efr & Space(tColunas(1)), tColunas(1)) & "   |   " & _
                                left(ExtErros(i).ext3_episodio & Space(tColunas(2)), tColunas(2)) & "   |   " & _
                                left(ExtErros(i).ext4_val_total & Space(tColunas(3)), tColunas(3)) & "   |   " & _
                                ExtErros(i).ext5_n_ord_ins
                
                
            Next i
            
        
        End If
    
    End If
    

End Function




Private Sub cmdProcessarLista_Click()
    Command2.Enabled = False
    
    Call ProcessaListaDB
   
    Command2.Enabled = True
    
    
End Sub

Private Sub Command2_Click()
    Command2.Enabled = False
    
    Call ProcessaLista
   
    Command2.Enabled = True
End Sub


Private Sub Form_Load()
    FormValidaCredenciais.caption = "Validador de credenciais v" & App.Major & "." & App.Minor & "." & App.Revision

    
End Sub



Private Sub txtCredencialEditar_Change()
    Dim credencial As String
    Dim indice As Integer
    
    indice = lstErrosLista.ListIndex
    credencial = Trim(txtCredencialEditar.text)
    Call PreencheTextboxRecalculaCheckDigit(credencial)
    Call ActualizaTextboxErros
End Sub

Private Sub ActualizaTextboxErros()

    Call PreencheExtSelecao
    txtErrosDet.text = DevolveErros(ExtSelecao)
    Call PreencheExtSelecao
End Sub
Private Sub lstErrosLista_Click()
      
    Dim indice As Integer
    Dim credencial As String
    Dim erros As String
    
    indice = lstErrosLista.ListIndex
    credencial = ExtErros(indice).ext1_nr_req_ars
    erros = ExtErros(indice).ext6_erros
    
    Call PreencheLabelLinha(indice)
    Call PreencheTextboxLinhaEdit(indice)
    Call PreencheTextboxErros(erros)
    Call PreencheExtSelecao
    
End Sub

Private Sub PreencheTextboxErros(ByVal erros As String)
    txtErrosDet.text = erros
End Sub
Private Sub PreencheExtSelecao()
    'ExtSelecao.ext0_n_seq_prog = extTmp.ext0_n_seq_prog
    ExtSelecao.ext1_nr_req_ars = txtCredencialEditar.text
    ExtSelecao.ext2_cod_rubr_efr = txtRubricaEditar.text
    ExtSelecao.ext3_episodio = txtEpisodioEditar.text
    ExtSelecao.ext4_val_total = txtValorEditar.text
    ExtSelecao.ext5_n_ord_ins = txtOrdemEditar.text
    ExtSelecao.ext6_erros = txtErrosDet.text
   
End Sub
Private Sub PreencheLabelLinha(ByVal indice As Integer)
    lblCredencialLista.caption = ExtErros(indice).ext1_nr_req_ars
    lblRubricaLista.caption = ExtErros(indice).ext2_cod_rubr_efr
    lblEpisodioLista.caption = ExtErros(indice).ext3_episodio
    lblValorLista.caption = ExtErros(indice).ext4_val_total
    lblOrdemLista.caption = ExtErros(indice).ext5_n_ord_ins
End Sub
Private Sub PreencheTextboxLinhaEdit(ByVal indice As Integer)
    txtCredencialEditar.text = ExtErros(indice).ext1_nr_req_ars
    txtRubricaEditar.text = ExtErros(indice).ext2_cod_rubr_efr
    txtEpisodioEditar.text = ExtErros(indice).ext3_episodio
    txtValorEditar.text = ExtErros(indice).ext4_val_total
    txtOrdemEditar.text = ExtErros(indice).ext5_n_ord_ins
End Sub
Private Sub PreencheTextboxRecalculaCheckDigit(ByVal credencial As String)

    Dim str As String
    Dim cdOriginal As String
    Dim cdCalculado As String
    
    If VerificaExtruturaCredencial(credencial) = cCredValida Then
        str = left(credencial, 18)
        cdOriginal = right(credencial, 1)
        cdCalculado = CalculaCheckDigit(str)
        txtCredencialCheckDigit.text = str & cdCalculado
    Else
        txtCredencialCheckDigit.text = cCredInvalida
    End If
End Sub

Private Function ValidaCheckDigit(ByVal credencial As String) As Boolean
    Dim str As String
    Dim cd As String
    
    str = left(credencial, 18)
    cd = right(credencial, 1)
    
    If CalculaCheckDigit(str) = cd Then
        ValidaCheckDigit = True
    Else
        ValidaCheckDigit = False
    End If
    

End Function

'Private Sub Text1_Change()
'    Dim esq As String
'    Dim resultado As String
'    Dim total As String
'    Dim pos As Long
'
'    pos = Text1.SelStart
'    Text1.text = Trim(Text1.text)
'    If Text1.text = "" Then
'        txtCredencialCheckDigit.text = ""
'        Label7.caption = ""
'        Exit Sub
'    End If
'
'    resultado = VerificaExtruturaCredencial(Trim(Text1.text))
'    If resultado = cCredValida Then
'        esq = left(Text1.text, 18)
'        total = esq & CalculaCheckDigit(esq)
'        resultado = VerificaExtruturaCredencial(total)
'        If resultado = cCredValida Then
'            txtCredencialCheckDigit.text = total
'            If UCase(Text1.text) = UCase(txtCredencialCheckDigit.text) Then
'                Label7.caption = cCredValida
'                Label7.ForeColor = &HC000&
'            Else
'                Label7.caption = cCredInvalida
'                Label7.ForeColor = &HCFF&
'            End If
'        Else
'            txtCredencialCheckDigit.text = resultado
'        End If
'    Else
'        Label7.caption = resultado
'        Label7.ForeColor = &HFF&
'        txtCredencialCheckDigit.text = ""
'    End If
'
'    Text1.SelStart = pos
'End Sub
'
Private Function DevolveErros(ByRef extTmp As ExtLinha) As String

    Dim str As String
    
    str = DevolveErrosCredencial(extTmp.ext1_nr_req_ars) & _
        DevolveErrosRubrica(extTmp.ext2_cod_rubr_efr) & _
        DevolveErrosEpisodio(extTmp.ext3_episodio) & _
        DevolveErrosValor(extTmp.ext4_val_total) & _
        DevolveErrosOrdem(extTmp.ext5_n_ord_ins) & _
        DevolveErrosPK_ARS_Episodio_Edit(extTmp.ext0_n_seq_prog, extTmp)
        
    DevolveErros = str
End Function
Private Function DevolveErrosPK_ARS_Episodio_Edit(ByVal n_seq_prog As String, ByRef extTmp As ExtLinha)

End Function


Private Sub txtEpisodioEditar_Change()
    
    Call ActualizaTextboxErros
End Sub

Private Sub txtOrdemEditar_Change()
    Call ActualizaTextboxErros
End Sub

Private Sub txtRubricaEditar_Change()
    Call ActualizaTextboxErros
End Sub

Private Sub txtValorEditar_Change()
    Call ActualizaTextboxErros
End Sub

VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form FormEditMessage 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   9780
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   13470
   Icon            =   "FormEditaMensagem.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9780
   ScaleWidth      =   13470
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton CommandButtonSend 
      Caption         =   "&Enviar"
      Height          =   1335
      Left            =   120
      Picture         =   "FormEditaMensagem.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   120
      Width           =   855
   End
   Begin VB.Frame Frame4 
      Height          =   1455
      Left            =   1080
      TabIndex        =   17
      Top             =   0
      Width           =   12255
      Begin VB.CommandButton CommandButtonCC 
         Caption         =   "&Cc..."
         Height          =   285
         Left            =   120
         TabIndex        =   22
         Top             =   600
         Width           =   855
      End
      Begin VB.CommandButton CommandButtonTO 
         Caption         =   "P&ara..."
         Height          =   285
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox TextBoxCC 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   1080
         TabIndex        =   20
         Top             =   600
         Width           =   10935
      End
      Begin VB.TextBox TextBoxSubject 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   19
         Top             =   960
         Width           =   10935
      End
      Begin VB.TextBox TextBoxTo 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   1080
         TabIndex        =   18
         Top             =   240
         Width           =   10935
      End
      Begin VB.Label Label2 
         Caption         =   "A&ssunto:"
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   960
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Height          =   4695
      Left            =   120
      TabIndex        =   11
      Top             =   1560
      Width           =   10095
      Begin VB.CommandButton CommandButtonAttachments 
         Height          =   525
         Left            =   8400
         Picture         =   "FormEditaMensagem.frx":049D
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton CommandButtonPrint 
         Height          =   525
         Left            =   6720
         Picture         =   "FormEditaMensagem.frx":0929
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   240
         Width           =   1575
      End
      Begin VB.Frame Frame1 
         Height          =   735
         Left            =   120
         TabIndex        =   12
         Top             =   120
         Width           =   6495
         Begin VB.ComboBox ComboBoxFontSize 
            Height          =   315
            Left            =   2520
            TabIndex        =   14
            Top             =   240
            Width           =   975
         End
         Begin VB.ComboBox ComboBoxFontType 
            Height          =   315
            ItemData        =   "FormEditaMensagem.frx":0DB7
            Left            =   120
            List            =   "FormEditaMensagem.frx":0DB9
            Sorted          =   -1  'True
            TabIndex        =   13
            Top             =   240
            Width           =   2415
         End
         Begin MSComctlLib.Toolbar ToolbarFormatacao 
            Height          =   390
            Left            =   3600
            TabIndex        =   15
            Top             =   240
            Width           =   2775
            _ExtentX        =   4895
            _ExtentY        =   688
            ButtonWidth     =   609
            ButtonHeight    =   582
            ImageList       =   "ImageListFormatacao"
            DisabledImageList=   "ImageListFormatacao"
            HotImageList    =   "ImageListFormatacao"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   10
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "bold"
                  ImageIndex      =   1
                  Style           =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "italic"
                  ImageIndex      =   2
                  Style           =   1
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "underline"
                  ImageIndex      =   3
                  Style           =   1
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "left"
                  ImageIndex      =   4
                  Style           =   2
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "center"
                  ImageIndex      =   5
                  Style           =   2
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "right"
                  ImageIndex      =   6
                  Style           =   2
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "corrector"
                  Object.ToolTipText     =   "Verificar ortografia"
                  ImageIndex      =   7
               EndProperty
            EndProperty
         End
      End
      Begin RichTextLib.RichTextBox RichTextBoxMessage 
         Height          =   3615
         Left            =   120
         TabIndex        =   16
         Top             =   960
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   6376
         _Version        =   393217
         BackColor       =   16777215
         Enabled         =   -1  'True
         TextRTF         =   $"FormEditaMensagem.frx":0DBB
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   5280
      ScaleHeight     =   315
      ScaleWidth      =   555
      TabIndex        =   1
      Top             =   7800
      Width           =   615
   End
   Begin VB.CommandButton BtImprimir 
      Height          =   855
      Left            =   14880
      Picture         =   "FormEditaMensagem.frx":0E3D
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Imprimir mensagem"
      Top             =   10200
      Width           =   1320
   End
   Begin MSComctlLib.ImageList ImageListFormatacao 
      Left            =   4800
      Top             =   8640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":2B07
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":2C61
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":2DBB
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":2F15
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":306F
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":31C9
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":3323
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog CmdDlgPrint 
      Left            =   3000
      Top             =   7920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6000
      Top             =   8640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":3BB5
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin Crystal.CrystalReport Report 
      Left            =   720
      Top             =   8640
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   5400
      Top             =   8640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":3FA2
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":43D7
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":47E3
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":4AFD
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormEditaMensagem.frx":4F13
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame FrameRecipients 
      Height          =   4695
      Left            =   10320
      TabIndex        =   2
      Top             =   1560
      Width           =   3015
      Begin MSComctlLib.ListView ListViewRecipients 
         Height          =   3255
         Left            =   120
         TabIndex        =   10
         Top             =   960
         Width           =   2535
         _ExtentX        =   4471
         _ExtentY        =   5741
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList2"
         SmallIcons      =   "ImageList2"
         ColHdrIcons     =   "ImageList2"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.CommandButton CommandButtonAddRecipients 
         BackColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         Picture         =   "FormEditaMensagem.frx":5360
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton CommandButtonRemoveAllRecipients 
         Height          =   1095
         Left            =   2640
         Picture         =   "FormEditaMensagem.frx":578F
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Remover Destinat�rios"
         Top             =   3120
         Width           =   255
      End
      Begin VB.CommandButton CommandButtonRemoveRecipient 
         Height          =   1095
         Left            =   2640
         Picture         =   "FormEditaMensagem.frx":5B54
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Remover Destinat�rio Seleccionado"
         Top             =   2040
         Width           =   255
      End
      Begin VB.CommandButton CommandButtonAddRecipient 
         Height          =   1095
         Left            =   2640
         Picture         =   "FormEditaMensagem.frx":5F00
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Adicionar Destinat�rio"
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox CheckBoxHideRecipients 
         Caption         =   "Ocultar destinat�rios"
         Height          =   255
         Left            =   840
         TabIndex        =   9
         Top             =   4320
         Width           =   1815
      End
      Begin VB.ComboBox ComboBoxRecipients 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   600
         Width           =   2775
      End
      Begin VB.OptionButton OptionButtonUsersRecipients 
         Caption         =   "Utilizadores"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.OptionButton OptionButtonListRecipients 
         Caption         =   "Listas"
         Height          =   255
         Left            =   2160
         TabIndex        =   6
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame FrameAttachments 
      Height          =   4695
      Left            =   10320
      TabIndex        =   27
      Top             =   1560
      Width           =   3015
      Begin MSComctlLib.ListView ListViewAttachments 
         Height          =   4335
         Left            =   120
         TabIndex        =   28
         Top             =   240
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   7646
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ColHdrIcons     =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
End
Attribute VB_Name = "FormEditMessage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'     .................................................................
'    .            Form of the application messages system              .
'   .                                                                   .
'   .                    Paulo Ferreira 2009.08.07                      .
'    .                        � 2009 GLINTT-HS                         .
'     .................................................................

Option Explicit

' Constant to define recipients target TO.
Private Const cRecipientsTargetTO = 0

' Constant to define recipients target CC.
Private Const cRecipientsTargetCC = 1

' Constant to define message not read.
Const cMessageNotRead = 4

' Constant to define message read.
Const cMessageRead = 5

' Keep recipients target.
Private iRecipientsTarget As Integer

' Initialize list view of attachments.
Private Sub InitializeListViewAttachments()
    
    On Error GoTo ErrorHandler
    With ListViewAttachments
        .View = lvwSmallIcon
        .ColumnHeaders.Add , , Empty, 350
        .SmallIcons = ImageList1
        .Icons = ImageList1
    End With
    SetListViewColor ListViewAttachments, Picture1, vbWhite, &H8000000F
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Initialize list view of recipients.
Private Sub InitializeListViewRecipients()
    
    On Error GoTo ErrorHandler
    With ListViewRecipients
        .View = lvwReport
        .ColumnHeaders.Add , , Empty, 350
        .ColumnHeaders.Add , , "Tipo", 500
        .ColumnHeaders.Add , , "Descri��o", 1600
        .SmallIcons = ImageList2
        .Icons = ImageList2
    End With
    SetListViewColor ListViewRecipients, Picture1, vbWhite, &H8000000F
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Fill message parameters.
Public Sub FillMessage(�lMessageId� As Long)
    
    Dim msg_lida As Integer
    Dim msg_lida_tooltip As String
    Dim sSql As String
    Dim rMessage As ADODB.recordset
    
    On Error GoTo ErrorHandler
    sSql = "select * from " & gBD_PREFIXO_TAB & "mensagens" & " where id = " & �lMessageId�
    Set rMessage = New ADODB.recordset
    rMessage.CursorLocation = adUseServer
    rMessage.Open sSql, gConexao, adOpenStatic
    If (rMessage.RecordCount = Empty) Then: Exit Sub
    TextBoxSubject.Text = BL_HandleNull(rMessage!assunto, Empty)
    RichTextBoxMessage.TextRTF = BL_HandleNull(rMessage!mensagem, Empty)
    If (BL_HandleNull(rMessage!flg_ocultar, Empty) <> 1) Then: ListViewRecipients.ListItems.Clear: FillRecipients �lMessageId�
    ListViewAttachments.ListItems.Clear
    CommandButtonAttachments.Enabled = False
    FillAttachments �lMessageId�
    If (rMessage.state = adStateOpen) Then: rMessage.Close
    Exit Sub

ErrorHandler:
    Exit Sub

End Sub

' Fill message recipients.
Private Sub FillRecipients(�lMessageId� As Long)
    
    On Error GoTo ErrorHandler
    TextBoxTo.Text = ModuleMessages.GetMessageRecipients(CStr(�lMessageId�))
    TextBoxCC.Text = ModuleMessages.GetMessageRecipients(CStr(�lMessageId�), True)
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Fill message attachments.
Private Sub FillAttachments(�lMessageId� As Long)
    
    Dim rMessage As ADODB.recordset
    Dim sSql As String
    
    On Error GoTo ErrorHandler
    sSql = "select * from " & gBD_PREFIXO_TAB & "mensagens_anexos where id_mensagem = " & �lMessageId�
    Set rMessage = New ADODB.recordset
    rMessage.CursorLocation = adUseServer
    rMessage.Open sSql, gConexao, adOpenStatic
    While (Not rMessage.EOF)
        ListViewAttachments.ListItems.Add , gPathAnexos & �lMessageId� & "_" & rMessage!cod_anexo & "_" & rMessage!anexo, rMessage!anexo, , 1
        rMessage.MoveNext
    Wend
    If (rMessage.state = adStateOpen) Then: rMessage.Close
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Test message font values.
Private Sub TestFontValues()

    Dim sParameter As String
    Dim iItem As Integer
    Dim iIndex As Integer
    
    On Error GoTo ErrorHandler
    iIndex = cr_mediComboValorNull
    sParameter = BG_DaParamAmbiente_NEW(cr_mediAmbitoGeral, "RelatFonte")
    For iItem = 0 To ComboBoxFontType.ListCount
        If (ComboBoxFontType.ItemData(iItem) = sParameter) Then: iIndex = iItem: Exit For
        If (ComboBoxFontType.ItemData(iItem) = "Tahoma") Then: iIndex = iItem
    Next
    ComboBoxFontType.ListIndex = iIndex
    iIndex = cr_mediComboValorNull
    sParameter = BG_DaParamAmbiente_NEW(cr_mediAmbitoGeral, "RelatTamanho")
    For iItem = 0 To ComboBoxFontSize.ListCount
        If (ComboBoxFontSize.ItemData(iItem) = sParameter) Then: iIndex = iItem: Exit For
        If (ComboBoxFontSize.ItemData(iItem) = "12") Then: iIndex = iItem
    Next
    ComboBoxFontSize.ListIndex = iIndex
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' ComboBoxRecipients_KeyDown.
Private Sub ComboBoxRecipients_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo ErrorHandler
    BG_LimpaOpcao ComboBoxRecipients, KeyCode
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' CommandButtonAddRecipient_Click.
Private Sub CommandButtonAddRecipient_Click()
    
    Dim iRecipientType As Integer
    
    On Error GoTo ErrorHandler
    If (ComboBoxRecipients.ListIndex = cr_mediComboValorNull) Then: Exit Sub
    If (OptionButtonUsersRecipients.value = True) Then
        With ListViewRecipients.ListItems.Add(, "U-" & ComboBoxRecipients.ItemData(ComboBoxRecipients.ListIndex), Empty, , cMessageNotRead)
            .ListSubItems.Add , , Empty, 2, "Mensagem n�o lida"
            .ListSubItems.Add , , ComboBoxRecipients.List(ComboBoxRecipients.ListIndex), , "Mensagem n�o lida"
        End With
    Else
        With ListViewRecipients.ListItems.Add(, "L-" & ComboBoxRecipients.ItemData(ComboBoxRecipients.ListIndex), Empty, , cMessageNotRead)
            .ListSubItems.Add , , Empty, 1, "Mensagem n�o lida"
            .ListSubItems.Add , , ComboBoxRecipients.List(ComboBoxRecipients.ListIndex), , "Mensagem n�o lida"
        End With
    End If
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' CommandButtonSend_Click.
Private Sub CommandButtonSend_Click()
    
    On Error GoTo ErrorHandler
    SendMessage
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Clean repeated recipients of the list.
Private Function CleanRepeatedRecipients(�clRecipients� As Collection) As Collection
    
    Dim clCleanCollection As Collection
    Dim item As Variant
    
    On Error GoTo ErrorHandler
    Set clCleanCollection = New Collection
    Set CleanRepeatedRecipients = New Collection
    For Each item In �clRecipients�
        If (Not ContainsUser(clCleanCollection, item)) Then: clCleanCollection.Add item
    Next
    Set CleanRepeatedRecipients = clCleanCollection
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' Check if the list contais the user.
Private Function ContainsUser(�clRecipients� As Collection, �vUser� As Variant) As Boolean

    Dim item As Variant
    
    On Error GoTo ErrorHandler
    For Each item In �clRecipients�
        If (item = �vUser�) Then: ContainsUser = True: Exit Function
    Next
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' Build recipients list.
Private Function BuildRecipientsList() As Collection
    
    Dim sListCode As String
    Dim sListType As String
    Dim rRecipients As ADODB.recordset
    Dim sSql As String
    Dim vItem As Variant
    Dim recipientsList As Collection
    
    On Error GoTo ErrorHandler
    Set recipientsList = New Collection
    For Each vItem In ListViewRecipients.ListItems
        sListType = Mid$(vItem.Key, 1, 2)
        sListCode = Mid$(vItem.Key, 3)
        If (sListType = "U-") Then: recipientsList.Add (sListCode)
        If (sListType = "L-") Then
            sSql = "select cod_lista, cod_membro from " & gBD_PREFIXO_TAB & "mensagens_membros where cod_lista= " & sListCode
            Set rRecipients = New ADODB.recordset
            rRecipients.CursorLocation = adUseServer
            rRecipients.CursorType = adOpenStatic
            rRecipients.Open sSql, gConexao
            While (Not rRecipients.EOF)
                sListCode = rRecipients!cod_membro
                If (BL_HandleNull(rRecipients!cod_membro, Empty) <> Empty) Then: recipientsList.Add sListCode
                rRecipients.MoveNext
            Wend
            If (rRecipients.state = adStateOpen) Then: rRecipients.Close
        End If
    Next
    Set recipientsList = CleanRepeatedRecipients(recipientsList)
    Set BuildRecipientsList = recipientsList
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' CommandButtonPrint_Click.
Private Sub CommandButtonPrint_Click()

    On Error GoTo ErrorHandler
    gImprimirDestino = 0
    FuncaoImprimir
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' CommandButtonRemoveRecipient_Click.
Private Sub CommandButtonRemoveRecipient_Click()
   
    On Error GoTo ErrorHandler
    If (ListViewRecipients.ListItems.Count > 0) Then: ListViewRecipients.ListItems.Remove ListViewRecipients.SelectedItem.Index
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' CommandButtonRemoveAllRecipients.
Private Sub CommandButtonRemoveAllRecipients_Click()
    
    On Error GoTo ErrorHandler
    If (ListViewRecipients.ListItems.Count > 0) Then: ListViewRecipients.ListItems.Clear
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' ComboBoxFontType_Change.
Sub ComboBoxFontType_Change()

    On Error GoTo ErrorHandler
    RichTextBoxMessage.SelFontName = ComboBoxFontType.Text
    Exit Sub
    
ErrorHandler:
    Exit Sub
   
End Sub

' ComboBoxFontType_Click.
Sub ComboBoxFontType_Click()

    On Error GoTo ErrorHandler
    RichTextBoxMessage.SelFontName = ComboBoxFontType.Text
    Exit Sub
    
ErrorHandler:
    Exit Sub
   
End Sub

' CommandButtonAddRecipients_Click.
Private Sub CommandButtonAddRecipients_Click()
  
    Dim vItem As Variant
  
    On Error GoTo ErrorHandler
    If (iRecipientsTarget = cRecipientsTargetTO) Then: TextBoxTo.Text = Empty
    If (iRecipientsTarget = cRecipientsTargetCC) Then: TextBoxCC.Text = Empty
    For Each vItem In BuildRecipientsList
        If (iRecipientsTarget = cRecipientsTargetTO) Then: TextBoxTo.Text = TextBoxTo.Text & CR_BL_DevolveNomeUtilizadorActual(vItem) & "; ": TextBoxTo.Tag = TextBoxTo.Tag & vItem & "; "
        If (iRecipientsTarget = cRecipientsTargetCC) Then: TextBoxCC.Text = TextBoxCC.Text & CR_BL_DevolveNomeUtilizadorActual(vItem) & "; ": TextBoxCC.Tag = TextBoxCC.Tag & vItem & "; "
    Next
    FrameRecipients.Visible = False
    FrameAttachments.Visible = True
    Exit Sub
    
ErrorHandler:
    Exit Sub
   
End Sub

' CommandButtonAttachments_Click.
Private Sub CommandButtonAttachments_Click()

    On Error GoTo ErrorHandler
    Me.CmdDlgPrint.FilterIndex = 1
    Me.CmdDlgPrint.CancelError = True
    Me.CmdDlgPrint.InitDir = gDirCliente & "\"
    Me.CmdDlgPrint.FileName = ""
    Me.CmdDlgPrint.DialogTitle = "Ficheiro"
    Me.CmdDlgPrint.ShowOpen
    If (Me.CmdDlgPrint.FileTitle <> Empty) Then: ListViewAttachments.ListItems.Add , Me.CmdDlgPrint.FileName, Me.CmdDlgPrint.FileTitle, 1, 1
    Exit Sub
    
ErrorHandler:
   Exit Sub
   
End Sub

' CommandButtonCC_Click.
Private Sub CommandButtonCC_Click()
    
    On Error GoTo ErrorHandler
    ListViewRecipients.ListItems.Clear
    FrameRecipients.Visible = True
    FrameAttachments.Visible = False
    iRecipientsTarget = cRecipientsTargetCC
    Exit Sub
    
ErrorHandler:
   Exit Sub
   
End Sub

' CommandButtonTO_Click.
Private Sub CommandButtonTO_Click()

    On Error GoTo ErrorHandler
    ListViewRecipients.ListItems.Clear
    FrameRecipients.Visible = True
    FrameAttachments.Visible = False
    iRecipientsTarget = cRecipientsTargetTO
    Exit Sub
    
ErrorHandler:
   Exit Sub
   
End Sub

' ListViewAttachments_DblClick.
Private Sub ListViewAttachments_DblClick()
    
    Dim aItemKey() As String
    Dim sFileExtension As String
    Dim sFileName As String
        
    On Error GoTo ErrorHandler
    If (ListViewAttachments.ListItems.Count = Empty) Then: Exit Sub
    If (Not ListViewAttachments.Visible) Then: Exit Sub
    If (Dir(ListViewAttachments.SelectedItem.Key) = Empty) Then: BG_Mensagem cr_mediMsgBox, "N�o � poss�vel localizar o anexo selecionado." & vbCrLf & "Pode ter sido apagado." & vbCrLf & "Por favor contacte o administrador da aplica��o.", vbCritical, "..."
    aItemKey = Split(ListViewAttachments.SelectedItem.Key, "\")
    aItemKey = Split(aItemKey(UBound(aItemKey)), ".")
    sFileName = aItemKey(0)
    sFileExtension = aItemKey(1)
    Me.CmdDlgPrint.FilterIndex = 1
    Me.CmdDlgPrint.CancelError = True
    Me.CmdDlgPrint.InitDir = gDirCliente & "\"
    Me.CmdDlgPrint.FileName = sFileName
    Me.CmdDlgPrint.DialogTitle = "Guardar anexo"
    Me.CmdDlgPrint.ShowSave
    If (Me.CmdDlgPrint.FileTitle <> Empty) Then
        Call FileCopy(ListViewAttachments.SelectedItem.Key, Me.CmdDlgPrint.FileName & "." & sFileExtension)
        If (Dir(Me.CmdDlgPrint.FileName & "." & sFileExtension) = Empty) Then: BG_Mensagem cr_mediMsgBox, "N�o foi poss�vel guardar o anexo.", vbCritical, "..."
    End If
    Exit Sub
    
ErrorHandler:
    Exit Sub
   
End Sub

' ListViewAttachments_KeyDown.
Private Sub ListViewAttachments_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo ErrorHandler
    If (ListViewAttachments.ListItems.Count = Empty) Then: Exit Sub
    If (KeyCode = vbKeyDelete And Shift = 0) Then
        If (CommandButtonAttachments.Enabled = True) Then: ListViewAttachments.ListItems.Remove ListViewAttachments.SelectedItem.Index
    End If
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' ComboBoxFontsize_Change.
Sub ComboBoxFontsize_Change()

    On Error GoTo ErrorHandler
    RichTextBoxMessage.SelFontSize = ComboBoxFontSize.Text
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' ComboBoxFontsize_Click.
Sub ComboBoxFontsize_Click()

    On Error GoTo ErrorHandler
    RichTextBoxMessage.SelFontSize = ComboBoxFontSize.Text
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' OptionButtonListRecipients_Click.
Private Sub OptionButtonListRecipients_Click()
    
    Dim sql As String
    
    On Error GoTo ErrorHandler
    sql = "select * from " & gBD_PREFIXO_TAB & "mensagens_listas where flg_publica = 1 or user_cri = " & gCodUtilizador
    If (OptionButtonListRecipients.value = True) Then: CR_BG_PreencheComboBD_ADO sql, "codigo", "descricao", ComboBoxRecipients, cr_mediAscComboDesignacao
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' OptionButtonUsersRecipients_Click.
Private Sub OptionButtonUsersRecipients_Click()
      
    On Error GoTo ErrorHandler
    If (OptionButtonUsersRecipients.value = True) Then: CR_BG_PreencheComboBD_ADO gBD_PREFIXO_TAB & "utilizadores", "codigo", "nome", ComboBoxRecipients, cr_mediAscComboDesignacao
    Exit Sub
    
ErrorHandler:
    Exit Sub
End Sub

' RichTextBoxMessage_GotFocus.
Public Sub RichTextBoxMessage_GotFocus()
    
    Dim ctControl As Control
    
    On Error GoTo ErrorHandler
    For Each ctControl In Controls: ctControl.TabStop = False: Next
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' RichTextBoxMessage_SelChange.
Public Sub RichTextBoxMessage_SelChange()

    On Error GoTo ErrorHandler
    If (IsNull(RichTextBoxMessage.SelFontSize)) Then
        ComboBoxFontSize.Text = Empty
    Else
        ComboBoxFontSize.Text = RichTextBoxMessage.SelFontSize
    End If
    If (RichTextBoxMessage.SelBold) Then
        ToolbarFormatacao.Buttons.item("bold").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("bold").value = tbrUnpressed
    End If
    If (RichTextBoxMessage.SelItalic) Then
        ToolbarFormatacao.Buttons.item("italic").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("italic").value = tbrUnpressed
    End If
    If (RichTextBoxMessage.SelUnderline) Then
        ToolbarFormatacao.Buttons.item("underline").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("underline").value = tbrUnpressed
    End If
    If (IsNull(RichTextBoxMessage.SelAlignment)) Then
        ToolbarFormatacao.Buttons.item("left").value = tbrUnpressed
        ToolbarFormatacao.Buttons.item("center").value = tbrUnpressed
        ToolbarFormatacao.Buttons.item("right").value = tbrUnpressed
    ElseIf (RichTextBoxMessage.SelAlignment = rtfLeft) Then
        ToolbarFormatacao.Buttons.item("left").value = tbrPressed
    ElseIf (RichTextBoxMessage.SelAlignment = rtfCenter) Then
        ToolbarFormatacao.Buttons.item("center").value = tbrPressed
    ElseIf (RichTextBoxMessage.SelAlignment = rtfRight) Then
        ToolbarFormatacao.Buttons.item("right").value = tbrPressed
    End If
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Public Sub Form_Load()
    LoadEvent
End Sub

Public Sub Form_Activate()
    ActivateEvent
End Sub

Public Sub Form_Unload(Cancel As Integer)
    UnloadEvent
End Sub

Private Sub Initializations()
    
    On Error GoTo ErrorHandler
    Me.caption = " Editor de mensagens"
    Me.top = 100
    Me.left = 100
    Me.Width = 13560
    Me.Height = 6795
    InitializeListViewAttachments
    InitializeListViewRecipients
    FrameAttachments.Visible = True
    FrameRecipients.Visible = False
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub LoadEvent()
    
    On Error GoTo ErrorHandler
    Initializations
    SetDefaultValues
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub ActivateEvent()

    On Error GoTo ErrorHandler
    Set gFormActivo = Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub UnloadEvent()
    
    On Error GoTo ErrorHandler
    Set gFormActivo = FormMessages
    FormMessages.CommandButtonRefresh_Click
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub SetDefaultValues()
    
    Dim iItem As Integer
    
    On Error GoTo ErrorHandler
    CheckBoxHideRecipients.value = vbUnchecked
    For iItem = 0 To Screen.FontCount - 1: ComboBoxFontType.AddItem Screen.Fonts(iItem): Next
    For iItem = 8 To 12: ComboBoxFontSize.AddItem iItem: Next
    For iItem = 14 To 28 Step 2: ComboBoxFontSize.AddItem iItem: Next
    ComboBoxFontSize.AddItem 36
    ComboBoxFontSize.AddItem 48
    ComboBoxFontSize.AddItem 72
    ComboBoxRecipients.Clear
    OptionButtonUsersRecipients_Click
    TestFontValues
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' ToolbarFormatacao_ButtonClick.
Private Sub ToolbarFormatacao_ButtonClick(ByVal Button As MSComctlLib.Button)

    On Error GoTo ErrorHandler
    Select Case (UCase(Button.Key))
        Case "BOLD":
            If (Button.value = tbrUnpressed) Then
                If (Not IsNull(RichTextBoxMessage.SelBold)) Then: RichTextBoxMessage.SelBold = False
            Else
                RichTextBoxMessage.SelBold = True
            End If
       Case "ITALIC":
            If (Button.value = tbrUnpressed) Then
                If (Not IsNull(RichTextBoxMessage.SelItalic)) Then RichTextBoxMessage.SelItalic = False
            Else
                RichTextBoxMessage.SelItalic = True
            End If
       Case "UNDERLINE":
            If (Button.value = tbrUnpressed) Then
                If (Not IsNull(RichTextBoxMessage.SelUnderline)) Then RichTextBoxMessage.SelUnderline = False
            Else
                RichTextBoxMessage.SelUnderline = True
            End If
       Case "LEFT":
            If (Button.value <> tbrUnpressed) Then: RichTextBoxMessage.SelAlignment = rtfLeft
       Case "CENTER":
            If (Button.value <> tbrUnpressed) Then: RichTextBoxMessage.SelAlignment = rtfCenter
       Case "RIGHT":
            If (Button.value <> tbrUnpressed) Then: RichTextBoxMessage.SelAlignment = rtfRight
       Case "CORRECTOR":
            If (Me.ActiveControl.Name <> "TextBoxSubject" And Me.ActiveControl.Name <> "RichTextBoxMessage") Then: Exit Sub
            Dim Corrector As Corrector
            Set Corrector = New Corrector
            Corrector.InicializaCorrector gDirServidor & "\bin" & "\Corrector.cor"
            If (Corrector.LocalizacaoCorrector <> Empty) Then: Corrector.VerificaTextoObjecto Me.ActiveControl
            Set Corrector = Nothing
            DoEvents
            Me.SetFocus
    End Select
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Paint a list view with colored bars.
Public Sub SetListViewColor(�lvCtrlListView� As ListView, �pCtrlPictureBox� As PictureBox, �lColorOne� As Long, Optional �lColorTwo� As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo ErrorHandler
    lColor1 = �lColorOne�
    lColor2 = �lColorTwo�
    �lvCtrlListView�.Picture = LoadPicture("")
    �lvCtrlListView�.Refresh
    �pCtrlPictureBox�.Cls
    �pCtrlPictureBox�.AutoRedraw = True
    �pCtrlPictureBox�.BorderStyle = vbBSNone
    �pCtrlPictureBox�.ScaleMode = vbTwips
    �pCtrlPictureBox�.Visible = False
    �lvCtrlListView�.PictureAlignment = lvwTile
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    �pCtrlPictureBox�.top = �lvCtrlListView�.top
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    With �pCtrlPictureBox�.Font
        .Size = �lvCtrlListView�.Font.Size + 2
        .Bold = �lvCtrlListView�.Font.Bold
        .Charset = �lvCtrlListView�.Font.Charset
        .Italic = �lvCtrlListView�.Font.Italic
        .Name = �lvCtrlListView�.Font.Name
        .Strikethrough = �lvCtrlListView�.Font.Strikethrough
        .Underline = �lvCtrlListView�.Font.Underline
        .Weight = �lvCtrlListView�.Font.Weight
    End With
    �pCtrlPictureBox�.Refresh
    lLineHeight = �pCtrlPictureBox�.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = �lvCtrlListView�.Width
    �pCtrlPictureBox�.Height = lBarHeight * 2
    �pCtrlPictureBox�.Width = lBarWidth
    �pCtrlPictureBox�.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    �pCtrlPictureBox�.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    �pCtrlPictureBox�.AutoSize = True
    �lvCtrlListView�.Picture = �pCtrlPictureBox�.Image
    �lvCtrlListView�.Refresh
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' FuncaoImprimir.
Public Sub FuncaoImprimir()
    
    On Error GoTo ErrorHandler
    PrintMessage
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' Print message.
Private Sub PrintMessage()
    
    Dim sSql As String
    
    On Error GoTo ErrorHandler
    
    sSql = "delete from sl_cr_mensagem where nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    
    sSql = "insert into sl_cr_mensagem (nome_computador,user_cri,cod_util_rec,cod_util_cc,assunto,mensagem)  VALUES( "
    sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
    sSql = sSql & BL_TrataStringParaBD(CR_BL_DevolveNomeUtilizadorActual) & ", "
    sSql = sSql & BL_TrataStringParaBD(TextBoxTo.Text) & ", "
    sSql = sSql & BL_TrataStringParaBD(TextBoxCC.Text) & ", "
    sSql = sSql & BL_TrataStringParaBD(TextBoxSubject.Text) & ", "
    sSql = sSql & BL_TrataStringParaBD(RichTextBoxMessage.TextRTF) & ") "
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    If (gSQLError <> Empty) Then: BG_Mensagem mediMsgBox, "Erro ao imprimir mensagem!", vbExclamation, " Erro": Exit Sub
    
    Report.SQLQuery = "select mensagem from sl_cr_mensagem where nome_computador = " & BL_TrataStringParaBD(gComputador)
        
    BG_Mensagem cr_mediMsgStatus, ("A imprimir..."), cr_mediMsgBeep + cr_mediMsgPermanece
    CR_BL_PoeMousePointer cr_mediMP_Espera
    Report.Connect = "DSN=" & gDSN
    Report.ReportFileName = gDirServidor & "\bin\Mensagem_Correio_v2.rpt"
    Report.formulas(0) = "DATE=" & BL_TrataStringParaBD("" & CR_Bg_DaData_ADO & " " & CR_Bg_DaHora_ADO)
    Report.formulas(1) = "USER=" & BL_TrataStringParaBD("" & CR_BL_DevolveNomeUtilizadorActual)
    Report.Destination = gImprimirDestino
    Report.Action = 1
    Report.Reset
    BG_Mensagem cr_mediMsgStatus, "", cr_mediMsgBeep + cr_mediMsgPermanece
    CR_BL_PoeMousePointer cr_mediMP_Activo
    Exit Sub
    
ErrorHandler:
    BG_Mensagem cr_mediMsgStatus, "", cr_mediMsgBeep + cr_mediMsgPermanece
    CR_BL_PoeMousePointer cr_mediMP_Activo
    Exit Sub
    
End Sub

' Send Message.
Private Sub SendMessage()
    
    Dim cmMessage As New ClassModuleMessage
    Dim lMessageID As Long
    
    On Error GoTo ErrorHandler
    CR_BG_BeginTransaction
    If (Not ValidateMessage) Then: Exit Sub
    lMessageID = BG_DaMAX(gBD_PREFIXO_TAB & "mensagens", "id") + 1
    Set cmMessage = MESS_CreateMessage(lMessageID, TextBoxSubject.Text, CLng(gCodUtilizador), GetRecipientsTO, GetRecipientsCC, ModuleMessages.MessageStateUnread, RichTextBoxMessage.TextRTF, GetAttachments)
    If (Not MESS_InsertMessage(cmMessage, CInt(CheckBoxHideRecipients.value))) Then: CR_BG_RollbackTransaction: Unload Me: Exit Sub
    If (Not MESS_InsertRecipients(cmMessage)) Then: CR_BG_RollbackTransaction: Unload Me: Exit Sub
    If (Not MESS_InsertAttachments(cmMessage)) Then: CR_BG_RollbackTransaction: Unload Me: Exit Sub
    CR_BG_CommitTransaction
    Unload Me
    Exit Sub
    
ErrorHandler:
    CR_BG_RollbackTransaction
    Unload Me
    Exit Sub
    
End Sub

' Insert message in data base.
Private Function InsertMessage(�cmMessage� As ClassModuleMessage) As Boolean
    
    Dim sSql As String
    
    On Error GoTo ErrorHandler
    sSql = "insert into " & gBD_PREFIXO_TAB & "mensagens (id,assunto,mensagem,flg_ocultar,user_cri,dt_cri) values (" & _
           �cmMessage�.GetCode & "," & BL_TrataStringParaBD(�cmMessage�.GetSubject) & "," & _
           BL_TrataStringParaBD(�cmMessage�.GetMessage) & "," & CheckBoxHideRecipients.value & "," & _
           �cmMessage�.GetFrom & ",sysdate)"
    CR_BG_ExecutaQuery_ADO sSql: CR_BG_Trata_BDErro
    InsertMessage = (gSQLError = 0)
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' Insert attachments in data base.
Private Function InsertAttachments(�cmMessage� As ClassModuleMessage) As Boolean
    
    On Error GoTo ErrorHandler
    Dim sSql As String
    Dim vItem As Variant
    
    On Error GoTo ErrorHandler
    For Each vItem In �cmMessage�.GetAttachments
        sSql = "insert into " & gBD_PREFIXO_TAB & "mensagens_anexos (id_mensagem,cod_anexo,anexo) values (" & �cmMessage�.GetCode & "," & _
               vItem.Index & "," & BG_VfValor(vItem.Text, "'") & ")"
        CR_BG_ExecutaQuery_ADO sSql: CR_BG_Trata_BDErro
        Call FileCopy(vItem.Key, gPathAnexos & �cmMessage�.GetCode & "_" & vItem.Index & "_" & vItem.Text)
    Next
    InsertAttachments = (gSQLError = 0)
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' Insert recipients in data base.
Private Function InsertRecipients(�cmMessage� As ClassModuleMessage) As Boolean
    
    Dim sSql As String
    Dim vItem As Variant

    On Error GoTo ErrorHandler
    For Each vItem In �cmMessage�.GetTo
        sSql = "insert into " & gBD_PREFIXO_TAB & "mensagens_destinatarios (id_mensagem,cod_util,cod_estado, flg_cc, flg_lida) values (" & �cmMessage�.GetCode & "," & _
               vItem & "," & ModuleMessages.MessageStateUnread & "," & ModuleMessages.RecipientTO & ", 0)"
        CR_BG_ExecutaQuery_ADO sSql: CR_BG_Trata_BDErro
    Next
    InsertRecipients = (gSQLError = 0)
    For Each vItem In �cmMessage�.GetCC
        sSql = "insert into " & gBD_PREFIXO_TAB & "mensagens_destinatarios (id_mensagem,cod_util,cod_estado, flg_cc, flg_lida) values (" & �cmMessage�.GetCode & "," & _
               vItem & "," & ModuleMessages.MessageStateUnread & "," & ModuleMessages.RecipientCC & ", 0)"
        CR_BG_ExecutaQuery_ADO sSql: CR_BG_Trata_BDErro
    Next
    InsertRecipients = (InsertRecipients = gSQLError = 0)
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Private Function ValidateMessage() As Boolean

    On Error GoTo ErrorHandler
    If (TextBoxTo.Text = Empty) Then: BG_Mensagem cr_mediMsgBox, "Campo Obrigat�rio: Destinat�rio", vbInformation, "...": Exit Function
    If (TextBoxSubject.Text = Empty) Then: BG_Mensagem cr_mediMsgBox, "Campo Obrigat�rio: Assunto", vbInformation, "...": Exit Function
    If (Len(TextBoxSubject) > 50) Then: BG_Mensagem cr_mediMsgBox, "O Assunto n�o pode ter mais de 50 caracteres.", vbInformation, "...": Exit Function
    If (TextBoxSubject.Text = Empty) Then
         If (BG_Mensagem(cr_mediMsgBox, "Deseja enviar a sua mensagem sem assunto?", vbYesNo + vbDefaultButton2 + vbQuestion, "...") = vbNo) Then: Exit Function
    End If
    ValidateMessage = True
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' Get collection of message recipients TO.
Private Function GetRecipientsTO() As Collection
    
    Dim vItem As Variant
    
    On Error GoTo ErrorHandler
    Set GetRecipientsTO = New Collection
    For Each vItem In Split(TextBoxTo.Tag, ";")
        If (Trim(vItem) <> Empty) Then: GetRecipientsTO.Add vItem
    Next
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' Get collection of message recipients C.
Private Function GetRecipientsCC() As Collection
    
    Dim vItem As Variant
    
    On Error GoTo ErrorHandler
    Set GetRecipientsCC = New Collection
    For Each vItem In Split(TextBoxCC.Tag, ";")
        If (Trim(vItem) <> Empty) Then: GetRecipientsCC.Add vItem
    Next
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' Get collection of message attachments.
Private Function GetAttachments() As Collection
    
    Dim vItem As Variant
    
    On Error GoTo ErrorHandler
    Set GetAttachments = New Collection
    For Each vItem In ListViewAttachments.ListItems
        If (Trim(vItem) <> Empty) Then: GetAttachments.Add vItem
    Next
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

' Enable commands buttons.
Private Sub EnableCommands(�bEnable� As Boolean)

    On Error GoTo ErrorHandler
    CommandButtonAddRecipient.Enabled = �bEnable�
    CommandButtonAttachments.Enabled = �bEnable�
    CommandButtonSend.Enabled = �bEnable�
    CommandButtonCC.Enabled = �bEnable�
    CommandButtonTO.Enabled = �bEnable�
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

VERSION 5.00
Begin VB.Form FormEstatReqUtil 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   2985
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5400
   Icon            =   "FormEstatReqUtil.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2985
   ScaleWidth      =   5400
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodSala 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   960
      TabIndex        =   12
      Top             =   2160
      Width           =   735
   End
   Begin VB.TextBox EcDescrSala 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   2160
      Width           =   3255
   End
   Begin VB.CommandButton BtPesquisaSala 
      Height          =   315
      Left            =   4920
      Picture         =   "FormEstatReqUtil.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
      Top             =   2160
      Width           =   375
   End
   Begin VB.ComboBox CbGrUtil 
      Height          =   315
      Left            =   960
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   1680
      Width           =   3975
   End
   Begin VB.ComboBox CbUtil 
      Height          =   315
      Left            =   960
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   1200
      Width           =   3975
   End
   Begin VB.CheckBox Flg_DescrRequis 
      Caption         =   "Descriminar Requisi��es"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   2640
      Width           =   2055
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4815
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   840
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   2400
         TabIndex        =   1
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2130
         TabIndex        =   4
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Label Label9 
      Caption         =   "Sala/Posto"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   13
      Top             =   2200
      Width           =   855
   End
   Begin VB.Label Label9 
      Caption         =   "&Grupo"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   8
      Top             =   1800
      Width           =   735
   End
   Begin VB.Label Label9 
      Caption         =   "&Utilizador"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   1200
      Width           =   975
   End
End
Attribute VB_Name = "FormEstatReqUtil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 05/01/2006
' T�cnico Rui Coelho

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim NRegistos As Long

Public rs As ADODB.recordset
'Private Sub BtPesquisaUtil_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_idutilizador", _
'                            "nome", "cod_utilizador", _
'                            EcCodUtil
'End Sub

Private Sub Combo1_Change()

End Sub

'Private Sub EcCodUtil_Change()
'    Dim rsCodigo As ADODB.recordset
'    Dim Sql As String
'
'    If EcCodUtil.Text <> "" Then
'        Set rsCodigo = New ADODB.recordset
'
'        Sql = "SELECT nome FROM sl_idutilizador WHERE cod_utilizador = '" & EcCodUtil & "'"
'
'        rsCodigo.CursorLocation = adUseServer
'        rsCodigo.CursorType = adOpenStatic
'
'        rsCodigo.Open Sql, gConexao
'        If rsCodigo.RecordCount <= 0 Then
'            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do utilizador!", vbExclamation, "Pesquisa r�pida"
'            EcPesqRapMedico.Text = ""
'        Else
'            EcDescrUtil.Text = Trim(rsCodigo!Nome)
'        End If
'
'        rsCodigo.Close
'        Set rsCodigo = Nothing
'    End If
'End Sub
Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Estat�stica de Requisi��es por Utilizador"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 5490
    Me.Height = 3405    'normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtIni
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de Requisi��es por Utilizador")
    
    Set FormEstatReqUtil = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    CbUtil.ListIndex = mediComboValorNull
    EcDtIni.Text = ""
    EcDtFim.Text = ""
    
    
End Sub

Sub DefTipoCampos()

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_idutilizador", "cod_utilizador", "nome", CbUtil
    BG_PreencheComboBD_ADO "sl_gr_util", "cod_gr_util", "descr_gr_util", CbGrUtil
    
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub

Sub Preenche_Estatistica()
    
    Dim sql As String
    Dim continua As Boolean
            
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estat�stica por Laborat�rio") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial do registo da requisi��o.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final do registo da requisi��o.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    'Printer Common Dialog
        If gImprimirDestino = 1 Then
            continua = BL_IniciaReport("EstatisticaReqUtil", "Estat�stica de Requisi��es por Utilizador", crptToPrinter)
        Else
            continua = BL_IniciaReport("EstatisticaReqUtil", "Estat�stica de Requisi��es por Utilizador", crptToWindow)
        End If
        If continua = False Then Exit Sub
    
    
    
    BG_BeginTransaction
    PreencheTabelaTemporaria
    BG_CommitTransaction
    
'    If NAnaTabela = 0 Then
'        Call BG_Mensagem(mediMsgBox, "N�o foram encontradas an�lises requisitadas para iniciar um Relat�rio", vbOKOnly + vbExclamation, App.ProductName)
'        Exit Sub
'    End If
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    If gSGBD = gOracle Then
            Report.SQLQuery = "SELECT * " & _
            " FROM SL_CR_RUTIL, SL_TBF_T_SIT " & _
            " WHERE SL_CR_RUTIL.nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao & _
            " AND SL_CR_RUTIL.T_SIT = SL_TBF_T_SIT.COD_T_SIT(+) ORDER BY COD_UTIL,T_SIT"
    ElseIf gSGBD = gSqlServer Then
        Report.SQLQuery = "SELECT cod_util " & _
            " FROM SL_CR_RUTIL" & gNumeroSessao & " SL_CR_RUTIL LEFT OUTER JOIN SL_TBF_T_SIT ON SL_CR_RUTIL.T_SIT = SL_TBF_T_SIT.COD_T_SIT" & _
            " ORDER BY COD_UTIL,T_SIT"
    End If
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    If CbUtil.ListIndex = -1 Then
        Report.formulas(2) = "Utilizador= " & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(2) = "Utilizador= " & BL_TrataStringParaBD("" & CbUtil.Text)
    End If
    If Flg_DescrRequis.value = 1 Then
        Report.formulas(3) = "DescrRequis=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(3) = "DescrRequis=" & BL_TrataStringParaBD("N")
    End If
    
'    Me.SetFocus
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
 
    
End Sub

Sub PreencheTabelaTemporaria()

    Dim sql As String
    Dim SqlC As String
    
    sql = "DELETE FROM SL_CR_RUTIL WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sql
    
    'verifica os campos preenchidos
    SqlC = " AND r.estado_req <> 'C' "
    
    'Data preenchida
    SqlC = SqlC & " AND r.dt_cri BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    
    'Utilizador preenchida?
    If (CbUtil.ListIndex <> -1) Then
        SqlC = SqlC & " AND r.user_cri= " & CbUtil.ItemData(CbUtil.ListIndex)
    End If
    'Utilizador preenchida?
    If (CbGrUtil.ListIndex <> -1) Then
        SqlC = SqlC & " AND i.cod_gr_util = " & CbGrUtil.ItemData(CbGrUtil.ListIndex)
    End If
    
    'SALA PREENCHIDA
    If EcCodSala <> "" Then
        SqlC = SqlC & " AND R.COD_SALA= " & BL_TrataStringParaBD(EcCodSala)
    End If
    
    '___________________________________________________________________________
    'insere na tabela tempor�ria todos os perfis c/ resultado que satisfazem os campos preenchidos
    sql = "INSERT INTO SL_CR_RUTIL (nome_computador, num_sessao, COD_UTIL, NOME_UTIL,N_REQ,DT_CRI,HR_CRI,T_SIT, dt_pretend, dt_conclusao, cod_Sala) " & _
        " SELECT " & BL_TrataStringParaBD(gComputador) & " nome_computador, " & gNumeroSessao & " num_sessao, COD_UTILIZADOR,NOME,N_REQ,R.DT_CRI,R.HR_CRI,T_SIT, r.dt_pretend, r.dt_conclusao, r.cod_Sala " & _
        " FROM SL_REQUIS R, SL_IDUTILIZADOR I " & _
        " WHERE R.USER_CRI = I.COD_UTILIZADOR "
    sql = sql & SqlC
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    
End Sub

Private Sub BtPesquisaSala_Click()
    On Error GoTo TrataErro
    PA_PesquisaSala EcCodSala, EcDescrSala, ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaSala_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaSala_Click"
    Exit Sub
    Resume Next
End Sub

Public Sub EcCodsala_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    Cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodsala_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodsala_Validate"
    Exit Sub
    Resume Next
End Sub



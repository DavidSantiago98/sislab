VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormReutilColheitas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormReutilColheitas"
   ClientHeight    =   7005
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15525
   Icon            =   "FormReutilColheitas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7005
   ScaleWidth      =   15525
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CbValidacao 
      Height          =   315
      Left            =   8520
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   480
      Width           =   2775
   End
   Begin MSComCtl2.DTPicker EcDtIni 
      Height          =   300
      Left            =   1440
      TabIndex        =   0
      Top             =   480
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   529
      _Version        =   393216
      Format          =   212205569
      CurrentDate     =   39555
   End
   Begin MSComCtl2.DTPicker EcDtFinal 
      Height          =   300
      Left            =   4440
      TabIndex        =   1
      Top             =   480
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   529
      _Version        =   393216
      Format          =   212205569
      CurrentDate     =   39555
   End
   Begin MSFlexGridLib.MSFlexGrid FgReutilColh 
      Height          =   5355
      Left            =   240
      TabIndex        =   6
      Top             =   1200
      Width           =   14865
      _ExtentX        =   26220
      _ExtentY        =   9446
      _Version        =   393216
      BackColorBkg    =   -2147483633
      GridLines       =   0
      BorderStyle     =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Valida��o Final"
      Height          =   255
      Index           =   2
      Left            =   6960
      TabIndex        =   5
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Data Final"
      Height          =   255
      Index           =   1
      Left            =   3360
      TabIndex        =   3
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Data Inicial"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   2
      Top             =   480
      Width           =   975
   End
End
Attribute VB_Name = "FormReutilColheitas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Dim estado As Integer
Dim CamposBDparaListBox
Dim NumEspacos
Dim CampoDeFocus As Object

Dim EstrutReutilColheita() As requisicoes_reutil
Dim TotalDadosColheita As Integer

Const lColNPresc = 0
Const lColProven = 1
Const lColNomeRequis = 2
Const lColDtPedido = 3
Const lColHrPedido = 4
Const lColAnalises = 5
Const lColAnalises2 = 6
Const lColAnalises3 = 7
Const lColAnalises4 = 8
Const lColAnalises5 = 9
Const lColReutil = 10
Const lColDtValidacao = 11
Const lColHrValidacao = 12

Private Type analise
    descr_ana As String
    cod_tubo As String
    descR_tubo As String
    cod_ana As String
    dt_chegada As String
    hr_chegada As String
    dt_colheita As String
    hr_colheita As String
    seq_req_tubo As String
    n_req As String
End Type

Private Type requisicoes_reutil
    n_presc As String
    dt_previ As String
    Proven As String
    med_requis As String
    dt_pedido As String
    hr_pedido As String
    analises() As analise
    dt_val_canc_reutil As String
    hr_val_canc_reutil As String
    user_val_canc_reutil As String
    estado_reutil As String
    flg_reutil As String
    motivo_n_reutil As String
    obs_reutil As String
    colheita_id As String
    cod_local As String
    totalAnalises As Integer
End Type

Private Enum estado_validacao
    Pendente = 0
    Aceite = 1
    Recusado = 2
End Enum


Private Sub FgReutilColh_DblClick()
   Dim i As Integer
   Dim Index As Integer
    If FgReutilColh.row > 0 And FgReutilColh.TextMatrix(FgReutilColh.RowSel, lColNPresc) <> "" Then
            FormReutilColheitas.Enabled = False
            For i = 1 To TotalDadosColheita
                If EstrutReutilColheita(i).n_presc = FgReutilColh.TextMatrix(FgReutilColh.RowSel, lColNPresc) Then
                    Index = i
                End If
            Next i
            FormValReutilColheita.Show
            BG_LimpaPassaParams
           
            FormValReutilColheita.EcNumReq = DevolveNumReq(EstrutReutilColheita(Index).colheita_id)
            gPassaParams.Param(0) = EstrutReutilColheita(Index).estado_reutil
            gPassaParams.Param(1) = EstrutReutilColheita(Index).dt_val_canc_reutil
            gPassaParams.Param(2) = EstrutReutilColheita(Index).hr_val_canc_reutil
            gPassaParams.Param(3) = EstrutReutilColheita(Index).user_val_canc_reutil
            gPassaParams.Param(4) = EstrutReutilColheita(Index).motivo_n_reutil
            gPassaParams.Param(5) = EstrutReutilColheita(Index).obs_reutil
            gPassaParams.Param(6) = EstrutReutilColheita(Index).n_presc
            gPassaParams.Param(7) = EstrutReutilColheita(Index).cod_local
            gPassaParams.Param(8) = EstrutReutilColheita(Index).analises(1).n_req
            FormValReutilColheita.FuncaoProcurar
            'FormValReutilColheita.TrataEstrutura EstrutReutilColheita, Index


    End If
End Sub

'Private Sub BtEntrTubos_Click()
'    If FgReqPend.row > 0 And FgReqPend.row <= TotalDadosRequis Then
'        'FormReqPedElectr.Enabled = False
'
'        ' pferreira 2010.09.29
'        Select Case (gVersaoChegadaTubos)
'            Case ("V1")
'                FormChegadaPorTubos.Show
'                FormChegadaPorTubos.EcNumReq = EstrutDadosRequis(FgReqPend.row).n_req
'                FormChegadaPorTubos.FuncaoProcurar
'            Case ("V2")
'                'NELSONPSILVA 08.03.2018 Glintt-HS-18730
'                If gGestao_Tubos_Default = mediNao Then
'                    gColheitaChegada = gEnumColheitaChegada.ColheitaTubo
'                ElseIf gGestao_Tubos_Default = mediSim Then
'                    gColheitaChegada = gEnumColheitaChegada.ChegadaTubo
'                End If
'                '
'                'NELSONPSILVA 04.04.2018 Glintt-HS-18011 (passagem de vari�vel global para registo de Log RGPD por context_info)
'                'Dim frtubo As New FormChegadaPorTubosV2
'                FormChegadaPorTubosV2.ExternalAccess = True
'                FormChegadaPorTubosV2.Show
'                FormChegadaPorTubosV2.EcNumReq = EstrutDadosRequis(FgReqPend.row).n_req
'                FormChegadaPorTubosV2.FuncaoProcurar
'            Case Else
'                FormChegadaPorTubos.Show
'                FormChegadaPorTubos.EcNumReq = EstrutDadosRequis(FgReqPend.row).n_req
'                FormChegadaPorTubos.FuncaoProcurar
'            End Select
'    End If
'End Sub
'
'' pferreira 2010.11.09
'Private Sub BtGestReq_Click()
'
'    If FgReqPend.row > 0 And FgReqPend.row <= TotalDadosRequis Then
'        If EstrutDadosRequis(FgReqPend.row).tipo_marcacao = lTipoPedidoRequisicao Then
'            FormReutilColheitas.Enabled = False
'            'NELSONPSILVA 04.04.2018 Glintt-HS-18011 (passagem de vari�vel global para registo de Log RGPD por context_info)
'            FormGestaoRequisicao.ExternalAccess = True
'            FormGestaoRequisicao.Show
'            FormGestaoRequisicao.EcNumReq = EstrutDadosRequis(FgReqPend.row).n_req
'            FormGestaoRequisicao.FuncaoProcurar
'            If gLAB = "HCVP" Then
'                FormGestaoRequisicao.EcDataChegada = Bg_DaData_ADO
'            End If
'        ElseIf EstrutDadosRequis(FgReqPend.row).tipo_marcacao = lTipoPedidoMarcacao Then
'            FormReutilColheitas.Enabled = False
'            'NELSONPSILVA 04.04.2018 Glintt-HS-18011 (passagem de vari�vel global para registo de Log RGPD por context_info)
'            FormGesReqCons.ExternalAccess = True
'            FormGesReqCons.Show
'            FormGesReqCons.EcNumReq = EstrutDadosRequis(FgReqPend.row).n_req
'            FormGesReqCons.FuncaoProcurar
'        End If
'    End If
'
'End Sub
'

Private Sub Form_Activate()

    EventoActivate

End Sub

Private Sub Form_Load()

    EventoLoad

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name

    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    'FuncaoProcurar True
    
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"

    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub
'
Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormReutilColheitas = Nothing
'    CkDatas.value = vbUnchecked
'    CkDatas_Click
'    CkReq.value = vbChecked
'    CkMarc.value = vbChecked
End Sub

Sub DefTipoCampos()
    
With FgReutilColh
        .rows = 20
        .FixedRows = 1
        .Cols = 13
        
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .CellAlignment = flexAlignLeftCenter
        .RowHeightMin = 285
        
'        For i = 0 To .Cols - 1
'        .ColAlignment(i) = flexAlignLeftCenter
'        Next i
        
        .ColWidth(lColNPresc) = 900
        .Col = lColNPresc
        .ColAlignment(lColNPresc) = flexAlignLeftCenter
        .TextMatrix(0, lColNPresc) = "Prescri��o"
        
        .ColWidth(lColProven) = 1500
        .Col = lColProven
        .ColAlignment(lColProven) = flexAlignLeftCenter
        .TextMatrix(0, lColProven) = "Proveni�ncia"

        .ColWidth(lColNomeRequis) = 1500
        .Col = lColNomeRequis
        .ColAlignment(lColNomeRequis) = flexAlignLeftCenter
        .TextMatrix(0, lColNomeRequis) = "Requisitante"
        
        .ColWidth(lColDtPedido) = 1200
        .Col = lColDtPedido
        .ColAlignment(lColDtPedido) = flexAlignLeftCenter
        '.TextMatrix(0, lColDtPedido) = "Data Pedido"

        .ColWidth(lColHrPedido) = 500
        .Col = lColHrPedido
        .ColAlignment(lColHrPedido) = flexAlignLeftCenter
        
        .MergeCells = flexMergeRestrictRows
        .TextMatrix(0, lColDtPedido) = "Data Pedido"
        .TextMatrix(0, lColHrPedido) = .TextMatrix(0, lColDtPedido)
        '.MergeRow(lColDtPedido) = True
        .MergeRow(0) = True

        .ColWidth(lColAnalises) = 1000
        .Col = lColAnalises
        .ColAlignment(lColAnalises) = flexAlignLeftCenter
        '.TextMatrix(0, lColAnalises) = "An�lises"

        .ColWidth(lColAnalises2) = 1000
        .Col = lColAnalises2
        .ColAlignment(lColAnalises2) = flexAlignLeftCenter
        '.TextMatrix(0, lColAnalises) = ""

        .ColWidth(lColAnalises3) = 1000
        .Col = lColAnalises3
        .ColAlignment(lColAnalises3) = flexAlignLeftCenter
        '.TextMatrix(0, lColAnalises2) = ""

        .ColWidth(lColAnalises4) = 1000
        .Col = lColAnalises4
        .ColAlignment(lColAnalises4) = flexAlignLeftCenter
        '.TextMatrix(0, lColAnalises4) = ""
        
        .MergeCells = flexMergeFree
        .TextMatrix(0, lColAnalises) = "An�lises"
        .TextMatrix(0, lColAnalises2) = .TextMatrix(0, lColAnalises)
        .TextMatrix(0, lColAnalises3) = .TextMatrix(0, lColAnalises)
        .TextMatrix(0, lColAnalises4) = .TextMatrix(0, lColAnalises)
        .TextMatrix(0, lColAnalises5) = .TextMatrix(0, lColAnalises)
        .MergeRow(0) = True

        .ColWidth(lColReutil) = 1000
        .Col = lColReutil
        .ColAlignment(lColReutil) = flexAlignLeftCenter
        .TextMatrix(0, lColReutil) = "Reutiliza��o"

        .ColWidth(lColDtValidacao) = 1200
        .Col = lColDtValidacao
        .ColAlignment(lColDtValidacao) = flexAlignLeftCenter
        '.TextMatrix(0, lColDtValidacao) = "Data Valida��o"
        
        .ColWidth(lColHrValidacao) = 500
        .Col = lColHrValidacao
        .ColAlignment(lColHrValidacao) = flexAlignLeftCenter
        
        .MergeCells = flexMergeRestrictRows
        .TextMatrix(0, lColDtValidacao) = "Data Decis�o"
        .TextMatrix(0, lColHrValidacao) = .TextMatrix(0, lColDtValidacao)
        '.MergeRow(lColDtPedido) = True
        .MergeRow(0) = True

        '.WordWrap = False
        .row = 1
        .Col = 0
    End With
'    If gNaoRefrescaReqPedElectr = mediSim Then
'        CkRefresh.value = vbUnchecked
'    Else
'        CkRefresh.value = vbChecked
'    End If

End Sub

Sub PreencheValoresDefeito()
    Dim i As Integer
    EcDtIni.value = Bg_DaData_ADO
    EcDtFinal.value = Bg_DaData_ADO

    BG_PreencheComboBD_ADO "sl_tbf_estado_colheita", "cod_estado", "descr_estado", CbValidacao, mediAscComboCodigo
    CbValidacao.ListIndex = estado_validacao.Pendente
    'BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTUtente, mediAscComboCodigo
    'BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", CbLocal, mediAscComboCodigo
   
End Sub

Sub Inicializacoes()

    Me.caption = "Reutiliza��o de Colheitas"
    Me.left = 5
    Me.top = 5
    Me.Width = 13800
    Me.Height = 7500 ' Normal
    'Me.Height = 5505 ' Campos Extras

    'Set CampoDeFocus = EcPrescricao
    'EcEstado.Locked = True
    FgReutilColh.ScrollTrack = True

    'LimpaCampos
End Sub
'
Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsReq As New ADODB.recordset
    'Dim SqlQuery As String
    'Dim rsQuery As New ADODB.recordset
   
    TotalDadosColheita = 0
    'TotalDadosAnalise = 0
    ReDim EstrutReutilColheita(0)
    LimpaFgReutilColh
    
    On Error GoTo TrataErro
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic

    If Me.ActiveControl = Me.EcDtIni Or Me.EcDtFinal And gFormActivo.Name = "FormReutilColheitas" Then
        Sendkeys "{ENTER}", True
        DoEvents
    End If

     sSql = "(select distinct x4.n_prescricao, x5.descr_proven, x4.cod_med, to_date(x4.dt_cri, 'dd/mm/yyyy') dt_cri, x4.hr_cri, x1.dt_val_canc_reutil, x1.hr_val_canc_reutil, "
     sSql = sSql & " x1.user_val_canc_reutil, (select descr_estado from sl_tbf_estado_colheita where nvl(x1.estado_reutil, 0) = cod_estado) estado, x1.flg_reutil, x1.motivo_n_reutil, x1.obs_reutil, x1.seq_colheita, x4.cod_local "
     sSql = sSql & " FROM sl_req_colheitas x1 INNER JOIN (SELECT seq_colheita, COUNT (x2.seq_colheita) FROM sl_req_colheitas x2, sl_requis x3 Where x2.n_req = x3.n_req GROUP BY seq_colheita "
     sSql = sSql & " HAVING COUNT (x2.seq_colheita) > 1) x2 ON x2.seq_colheita = x1.seq_colheita, sl_requis x4 LEFT OUTER JOIN sl_proven x5 ON x5.cod_proven = x4.cod_proven, slv_identif x6, sl_marcacoes x7 "
     sSql = sSql & " WHERE x4.n_req = x1.n_req AND x4.seq_utente = x6.seq_utente AND x4.n_req = x7.n_req "
     sSql = sSql & " AND (x4.dt_cri) BETWEEN TO_DATE (" & BL_TrataDataParaBD(EcDtIni.value) & ", 'dd/mm/yyyy') AND TO_DATE (" & BL_TrataDataParaBD(EcDtFinal.value) & ",  'dd/mm/yyyy') "
     sSql = sSql & " AND NVL (x1.estado_reutil, 0) = " & CbValidacao.ListIndex & " AND x1.flg_reutil = 1 AND x4.n_prescricao IS NOT NULL "
     sSql = sSql & " AND nvl(user_val_canc_reutil,'99999999') not in (select cod_utilizador from sl_idutilizador where utilizador = 'GH'))"
 
     sSql = sSql & " UNION "
     sSql = sSql & "(select distinct x4.n_prescricao, x5.descr_proven, x4.cod_med, to_date(x4.dt_cri, 'dd/mm/yyyy') dt_cri, x4.hr_cri, x1.dt_val_canc_reutil, x1.hr_val_canc_reutil, "
     sSql = sSql & " x1.user_val_canc_reutil, (select descr_estado from sl_tbf_estado_colheita where nvl(x1.estado_reutil, 0) = cod_estado) estado, x1.flg_reutil, x1.motivo_n_reutil, x1.obs_reutil, x1.seq_colheita, x4.cod_local "
     sSql = sSql & " FROM sl_req_colheitas x1 INNER JOIN (SELECT seq_colheita, COUNT (x2.seq_colheita) FROM sl_req_colheitas x2, sl_requis x3 Where x2.n_req = x3.n_req GROUP BY seq_colheita "
     sSql = sSql & " HAVING COUNT (x2.seq_colheita) > 1) x2 ON x2.seq_colheita = x1.seq_colheita, sl_requis x4 LEFT OUTER JOIN sl_proven x5 ON x5.cod_proven = x4.cod_proven, slv_identif x6, sl_realiza x7 "
     sSql = sSql & " WHERE x4.n_req = x1.n_req AND x4.seq_utente = x6.seq_utente AND x4.n_req = x7.n_req "
     sSql = sSql & " AND (x4.dt_cri) BETWEEN TO_DATE (" & BL_TrataDataParaBD(EcDtIni.value) & ", 'dd/mm/yyyy') AND TO_DATE (" & BL_TrataDataParaBD(EcDtFinal.value) & ",  'dd/mm/yyyy') "
     sSql = sSql & " AND NVL (x1.estado_reutil, 0) = " & CbValidacao.ListIndex & " AND x1.flg_reutil = 1 AND x4.n_prescricao IS NOT NULL "
     sSql = sSql & " AND nvl(user_val_canc_reutil,'99999999') not in (select cod_utilizador from sl_idutilizador where utilizador = 'GH'))"
     sSql = sSql & " ORDER BY dt_cri DESC, hr_cri DESC, n_prescricao "

    rsReq.Open sSql, gConexao
    If rsReq.RecordCount > 0 Then
        While Not rsReq.EOF
        PreencheReutilColheita BL_HandleNull(rsReq!n_prescricao, " "), _
                                   BL_HandleNull(rsReq!descr_proven, ""), _
                                   PesqNomeMedico(BL_HandleNull(rsReq!cod_med, "")), _
                                   BL_HandleNull(rsReq!dt_cri, ""), BL_HandleNull(rsReq!hr_cri, ""), _
                                   BL_HandleNull(rsReq!dt_val_canc_reutil, ""), BL_HandleNull(rsReq!hr_val_canc_reutil, ""), _
                                   BL_HandleNull(rsReq!user_val_canc_reutil, ""), BL_HandleNull(rsReq!estado, ""), _
                                   BL_HandleNull(rsReq!flg_reutil, ""), _
                                   BL_HandleNull(rsReq!motivo_n_reutil, ""), BL_HandleNull(rsReq!obs_reutil, ""), _
                                   BL_HandleNull(rsReq!seq_colheita, ""), BL_HandleNull(rsReq!cod_local, "")
        rsReq.MoveNext
'            While Not rsQuery.EOF
'
'            PreencheReutilAnalises BL_HandleNull(rsReq!descr_ana, ""), BL_HandleNull(rsReq!cod_tubo, ""), BL_HandleNull(rsReq!desc_tubo, ""), _
'                                   BL_HandleNull(rsReq!dt_chega, ""), BL_HandleNull(rsReq!hr_chega, ""), BL_HandleNull(rsReq!dt_colheita, ""), _
'                                   BL_HandleNull(rsReq!hr_colheita, "")
'
'            rsReq.MoveNext
        Wend
        FgReutilColh.Visible = False
        PreencheFgReutilColh
        FgReutilColh.Visible = True

        rsReq.Close
    Else
        'LimpaFgReqPend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Procurar Colheitas:" & Err.Description, Me.Name, "FuncaoProcurar", True
    Exit Sub
    Resume Next
End Sub

Sub LimpaCampos()
    Dim i As Integer

    LimpaFgReutilColh
    EcDtIni.value = Bg_DaData_ADO
    EcDtFinal.value = Bg_DaData_ADO
    CbValidacao.ListIndex = estado_validacao.Pendente

    Dim TotalDadosColheita As Integer
    Dim TotalDadosAnalise As Integer
    ReDim EstrutReutilColheita(0)


End Sub


Sub FuncaoLimpar()

    LimpaCampos
    
End Sub
'
Private Sub Form_Unload(Cancel As Integer)

    EventoUnload
End Sub
'
Sub LimpaFgReutilColh()

    Dim j As Long
    Dim i As Long
    j = FgReutilColh.rows - 1
    While j > 0
        'If j > 1 Then
        '    FgReutilColh.RemoveItem j - 1
        'Else
             For i = 0 To FgReutilColh.Cols - 1
                FgReutilColh.TextMatrix(j, i) = ""
            Next
        'End If

        j = j - 1
    Wend
End Sub

'
Private Sub PreencheReutilColheita(n_prescricao As String, descr_proven As String, _
                                 cod_med As String, dt_cri As String, hr_cri As String, _
                                 dt_val_canc_reutil As String, hr_val_canc_reutil As String, user_val_canc_reutil As String, estado As String, _
                                 flg_reutil As String, motivo_n_reutil As String, obs_reutil As String, colheita_id As String, cod_local As String)
    On Error GoTo TrataErro
    Dim i As Long
    
    TotalDadosColheita = TotalDadosColheita + 1
    ReDim Preserve EstrutReutilColheita(TotalDadosColheita)
    EstrutReutilColheita(TotalDadosColheita).n_presc = n_prescricao
    EstrutReutilColheita(TotalDadosColheita).Proven = descr_proven
    EstrutReutilColheita(TotalDadosColheita).med_requis = cod_med
    EstrutReutilColheita(TotalDadosColheita).dt_pedido = dt_cri
    EstrutReutilColheita(TotalDadosColheita).hr_pedido = hr_cri
    EstrutReutilColheita(TotalDadosColheita).dt_val_canc_reutil = dt_val_canc_reutil
    EstrutReutilColheita(TotalDadosColheita).hr_val_canc_reutil = hr_val_canc_reutil
    EstrutReutilColheita(TotalDadosColheita).user_val_canc_reutil = user_val_canc_reutil
    EstrutReutilColheita(TotalDadosColheita).estado_reutil = estado
    EstrutReutilColheita(TotalDadosColheita).flg_reutil = flg_reutil
    EstrutReutilColheita(TotalDadosColheita).motivo_n_reutil = motivo_n_reutil
    EstrutReutilColheita(TotalDadosColheita).obs_reutil = obs_reutil
    EstrutReutilColheita(TotalDadosColheita).colheita_id = colheita_id
    EstrutReutilColheita(TotalDadosColheita).cod_local = cod_local
     
    PreencheReutilAnalises TotalDadosColheita, n_prescricao

Exit Sub

TrataErro:
    BG_LogFile_Erros "Erro ao Procurar An�lises:" & Err.Description, Me.Name, "PreencheReutilColheita", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheReutilAnalises(ByVal indice As Integer, n_presc As String)

    Dim SQLQuery As String
    Dim rsQuery As New ADODB.recordset
    Dim TotalDadosAnalise As Integer
    TotalDadosAnalise = 0
    rsQuery.CursorLocation = adUseServer
    rsQuery.CursorType = adOpenStatic

    On Error GoTo TrataErro
    
    SQLQuery = "select  x2.cod_ana, x2.descr_ana, x3.cod_tubo, x3.descr_tubo, x4.n_req "
    ', x3.descr_tubo, x4.dt_chega, x4.hr_chega, x4.dt_colheita, x4.hr_colheita, x5.seq_req_tubo "
    SQLQuery = SQLQuery & " FROM  sl_marcacoes x1, slv_analises x2, sl_tubo x3, sl_requis x4"
    ', sl_req_tubo x4, sl_marcacoes x5 "
    SQLQuery = SQLQuery & " WHERE "
    SQLQuery = SQLQuery & " x1.cod_agrup = x2.cod_ana AND x2.cod_tubo = x3.cod_tubo AND x1.n_req = x4.n_req"
    'SQLQuery = SQLQuery & " AND x1.n_req = " & BL_TrataStringParaBD(n_req) & " "
    SQLQuery = SQLQuery & " AND x4.n_prescricao = " & BL_TrataStringParaBD(n_presc) & " "
    SQLQuery = SQLQuery & " UNION "
    SQLQuery = SQLQuery & "select  x2.cod_ana, x2.descr_ana, x3.cod_tubo, x3.descr_tubo, x4.n_req "
    ', x3.descr_tubo, x4.dt_chega, x4.hr_chega, x4.dt_colheita, x4.hr_colheita, x5.seq_req_tubo "
    SQLQuery = SQLQuery & " FROM  sl_realiza x1, slv_analises x2, sl_tubo x3, sl_requis x4 "
    ', sl_req_tubo x4, sl_marcacoes x5 "
    SQLQuery = SQLQuery & " WHERE "
    SQLQuery = SQLQuery & " x1.cod_agrup = x2.cod_ana AND x2.cod_tubo = x3.cod_tubo AND x1.n_req = x4.n_req"
    'SQLQuery = SQLQuery & " AND x1.n_req = " & BL_TrataStringParaBD(n_req) & " "
    SQLQuery = SQLQuery & " AND x4.n_prescricao = " & BL_TrataStringParaBD(n_presc) & " "
    
    'sSql = sSql & " AND x1.cod_local = " & BG_DaComboSel(CbLocal)
   
    'SQLQuery = SQLQuery & " ORDER BY dt_colheita DESC, hr_colheita DESC, n_prescricao"
    'Set rsQuery = BG_ExecutaSELECT(SQLQuery)
    SQLQuery = SQLQuery & " ORDER BY cod_ana "
    
    rsQuery.Open SQLQuery, gConexao
    If rsQuery.RecordCount > 0 Then
        While Not rsQuery.EOF
            TotalDadosAnalise = TotalDadosAnalise + 1
            ReDim Preserve EstrutReutilColheita(indice).analises(TotalDadosAnalise)
            EstrutReutilColheita(indice).totalAnalises = EstrutReutilColheita(indice).totalAnalises + 1
            EstrutReutilColheita(indice).analises(TotalDadosAnalise).descr_ana = BL_HandleNull(rsQuery!descr_ana, "")
            EstrutReutilColheita(indice).analises(TotalDadosAnalise).cod_ana = BL_HandleNull(rsQuery!cod_ana, "")
            EstrutReutilColheita(indice).analises(TotalDadosAnalise).cod_tubo = BL_HandleNull(rsQuery!cod_tubo, "")
            EstrutReutilColheita(indice).analises(TotalDadosAnalise).descR_tubo = BL_HandleNull(rsQuery!descR_tubo, "")
            EstrutReutilColheita(indice).analises(TotalDadosAnalise).n_req = BL_HandleNull(rsQuery!n_req, "")
            
            rsQuery.MoveNext
        Wend
        
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Procurar An�lises:" & Err.Description, Me.Name, "PreencheReutilAnalises", True
    Exit Sub
    Resume Next
End Sub


Private Sub PreencheFgReutilColh()
    Dim i As Integer
    Dim j As Integer
    Dim linhaActual As Integer
    Dim z As Integer
    Dim Count As Integer
    linhaActual = 1
   
    On Error GoTo TrataErro
    
    For i = 1 To TotalDadosColheita
        
        FgReutilColh.TextMatrix(linhaActual, lColNPresc) = EstrutReutilColheita(i).n_presc
        FgReutilColh.TextMatrix(linhaActual, lColProven) = EstrutReutilColheita(i).Proven
        FgReutilColh.TextMatrix(linhaActual, lColNomeRequis) = EstrutReutilColheita(i).med_requis
        FgReutilColh.TextMatrix(linhaActual, lColDtPedido) = EstrutReutilColheita(i).dt_pedido
        FgReutilColh.TextMatrix(linhaActual, lColHrPedido) = EstrutReutilColheita(i).hr_pedido
        
        FgReutilColh.TextMatrix(linhaActual, lColReutil) = EstrutReutilColheita(i).estado_reutil
        FgReutilColh.TextMatrix(linhaActual, lColDtValidacao) = EstrutReutilColheita(i).dt_val_canc_reutil
        FgReutilColh.TextMatrix(linhaActual, lColHrValidacao) = EstrutReutilColheita(i).hr_val_canc_reutil

        For j = 1 To EstrutReutilColheita(i).totalAnalises
            
            If Count > 4 Then
                
                linhaActual = linhaActual + 1
                If linhaActual > 20 Then
                    FgReutilColh.AddItem "", linhaActual
                    'z = 1
                End If

                    FgReutilColh.TextMatrix(linhaActual, lColNPresc) = FgReutilColh.TextMatrix(linhaActual - 1, lColNPresc)
                    FgReutilColh.TextMatrix(linhaActual, lColProven) = FgReutilColh.TextMatrix(linhaActual - 1, lColProven)
                    FgReutilColh.TextMatrix(linhaActual, lColNomeRequis) = FgReutilColh.TextMatrix(linhaActual - 1, lColNomeRequis)
                    FgReutilColh.TextMatrix(linhaActual, lColDtPedido) = FgReutilColh.TextMatrix(linhaActual - 1, lColDtPedido)
                    FgReutilColh.TextMatrix(linhaActual, lColHrPedido) = FgReutilColh.TextMatrix(linhaActual - 1, lColHrPedido)
                    
                    FgReutilColh.TextMatrix(linhaActual, lColReutil) = FgReutilColh.TextMatrix(linhaActual - 1, lColReutil)
                    FgReutilColh.TextMatrix(linhaActual, lColDtValidacao) = FgReutilColh.TextMatrix(linhaActual - 1, lColDtValidacao)
                    FgReutilColh.TextMatrix(linhaActual, lColHrValidacao) = FgReutilColh.TextMatrix(linhaActual - 1, lColHrValidacao)
                    
                    z = 0
                    Count = 0
                
            End If
            
            FgReutilColh.TextMatrix(linhaActual, lColAnalises + z) = EstrutReutilColheita(i).analises(j).descr_ana
            FgReutilColh.Col = lColHrPedido + z
            FgReutilColh.row = linhaActual
            Count = Count + 1
            z = z + 1
        Next j
        linhaActual = linhaActual + 1
        z = 0
        Count = 0
      '  FgReutilColh.AddItem ""

      'FgReutilColh.MergeCells = flexMergeRestrictColumns
        FgReutilColh.MergeCol(lColNPresc) = True
        FgReutilColh.MergeCol(lColProven) = True
        FgReutilColh.MergeCol(lColNomeRequis) = True
        'FgReutilColh.MergeCells = flexMergeRestrictolumns
        FgReutilColh.MergeCol(lColDtPedido) = True
        FgReutilColh.MergeCol(lColHrPedido) = True
        FgReutilColh.MergeCol(lColReutil) = True
        FgReutilColh.MergeCol(lColDtValidacao) = True
        FgReutilColh.MergeCol(lColHrValidacao) = True
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Preencher FgReutilColh:" & Err.Description, Me.Name, "PreencheFgReutilColh", True
    Exit Sub
    Resume Next
End Sub

Private Function DevolveNumReq(colheita As String) As String
    Dim sSql As String
    Dim rsNumReq As New ADODB.recordset
    
    On Error GoTo TrataErro
    sSql = "SELECT * FROM sl_req_colheitas where seq_colheita = " & colheita & " and flg_reutil is null and estado_reutil is null"
    rsNumReq.CursorType = adOpenStatic
    rsNumReq.CursorLocation = adUseServer
    rsNumReq.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsNumReq.Open sSql, gConexao
        If rsNumReq.RecordCount > 0 Then
            DevolveNumReq = BL_HandleNull(rsNumReq!n_req, "")
        Else
            DevolveNumReq = ""
        End If
    rsNumReq.Close
    Set rsNumReq = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro no DevolveNumReq:" & Err.Description, Me.Name, "DevolveNumReq", True
    DevolveNumReq = ""
    Exit Function
    Resume Next
End Function


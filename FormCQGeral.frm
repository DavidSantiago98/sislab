VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form FormCQGeral 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCQGeral"
   ClientHeight    =   10095
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   15210
   Icon            =   "FormCQGeral.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   10095
   ScaleWidth      =   15210
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox PicDotDR 
      Appearance      =   0  'Flat
      BackColor       =   &H000000C0&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      Picture         =   "FormCQGeral.frx":000C
      ScaleHeight     =   225
      ScaleWidth      =   225
      TabIndex        =   87
      Top             =   9360
      Width           =   255
   End
   Begin VB.PictureBox PicKo 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1200
      Picture         =   "FormCQGeral.frx":0396
      ScaleHeight     =   255
      ScaleWidth      =   270
      TabIndex        =   83
      Top             =   9720
      Width           =   275
   End
   Begin VB.PictureBox PicOk 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1200
      Picture         =   "FormCQGeral.frx":0720
      ScaleHeight     =   255
      ScaleWidth      =   270
      TabIndex        =   82
      Top             =   9360
      Width           =   275
   End
   Begin VB.PictureBox PicObs 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   480
      Picture         =   "FormCQGeral.frx":0AAA
      ScaleHeight     =   255
      ScaleWidth      =   270
      TabIndex        =   80
      Top             =   9720
      Width           =   275
   End
   Begin VB.PictureBox PicDotR 
      Appearance      =   0  'Flat
      BackColor       =   &H008080FF&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   480
      Picture         =   "FormCQGeral.frx":0E34
      ScaleHeight     =   225
      ScaleWidth      =   225
      TabIndex        =   74
      Top             =   9360
      Width           =   255
   End
   Begin VB.PictureBox PicDotG 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFC0&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   840
      Picture         =   "FormCQGeral.frx":11BE
      ScaleHeight     =   225
      ScaleWidth      =   225
      TabIndex        =   73
      Top             =   9720
      Width           =   255
   End
   Begin VB.PictureBox PicDotY 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   840
      Picture         =   "FormCQGeral.frx":1548
      ScaleHeight     =   225
      ScaleWidth      =   225
      TabIndex        =   72
      Top             =   9360
      Width           =   255
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7215
      Left            =   240
      TabIndex        =   21
      Top             =   1920
      Width           =   14775
      _ExtentX        =   26061
      _ExtentY        =   12726
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "Resultados"
      TabPicture(0)   =   "FormCQGeral.frx":18D2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1(11)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label1(12)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "FgRes"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "EcObs"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "BtGravaObs"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "EcAccao"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "Resumo"
      TabPicture(1)   =   "FormCQGeral.frx":1C6C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "FgResumo"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Gr�fico Levey-Jennings"
      TabPicture(2)   =   "FormCQGeral.frx":2006
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "BtRefresh"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "FrameGraf2"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "FrameGraf1"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "OptMedia(1)"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "OptMedia(0)"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "EcListaGraf"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "Grafico"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "LbAnalise"
      Tab(2).Control(7).Enabled=   0   'False
      Tab(2).Control(8)=   "Label1(4)"
      Tab(2).Control(8).Enabled=   0   'False
      Tab(2).ControlCount=   9
      TabCaption(3)   =   "Listas para Aparelhos"
      TabPicture(3)   =   "FormCQGeral.frx":23A0
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Label1(5)"
      Tab(3).Control(1)=   "Label1(6)"
      Tab(3).Control(2)=   "Label1(7)"
      Tab(3).Control(3)=   "Label1(9)"
      Tab(3).Control(4)=   "Label1(10)"
      Tab(3).Control(5)=   "BtPesquisaApar2"
      Tab(3).Control(5).Enabled=   0   'False
      Tab(3).Control(6)=   "EcDescrApar2"
      Tab(3).Control(7)=   "EcSeqApar2"
      Tab(3).Control(8)=   "EcAnalises"
      Tab(3).Control(9)=   "BtInsere"
      Tab(3).Control(10)=   "EcAnalisesSel"
      Tab(3).Control(11)=   "BtRetira"
      Tab(3).Control(12)=   "BtPesquisaLote2"
      Tab(3).Control(12).Enabled=   0   'False
      Tab(3).Control(13)=   "EcDescrLote2"
      Tab(3).Control(14)=   "EcCodLote2"
      Tab(3).Control(15)=   "FrameAnaMarcadas"
      Tab(3).Control(16)=   "BtGerar"
      Tab(3).Control(17)=   "EcPrefixo"
      Tab(3).ControlCount=   18
      Begin VB.TextBox EcAccao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   615
         Left            =   8160
         TabIndex        =   77
         Top             =   6480
         Width           =   5655
      End
      Begin VB.CommandButton BtGravaObs 
         Height          =   615
         Left            =   13920
         Picture         =   "FormCQGeral.frx":23BC
         Style           =   1  'Graphical
         TabIndex        =   79
         ToolTipText     =   "Guardar Altera��o"
         Top             =   6480
         Width           =   735
      End
      Begin VB.TextBox EcObs 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   615
         Left            =   1080
         TabIndex        =   76
         Top             =   6480
         Width           =   5655
      End
      Begin VB.TextBox EcPrefixo 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -67320
         TabIndex        =   71
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton BtGerar 
         Height          =   615
         Left            =   -66960
         Picture         =   "FormCQGeral.frx":3086
         Style           =   1  'Graphical
         TabIndex        =   69
         ToolTipText     =   "Gerar Lista de Trabalho"
         Top             =   6120
         Width           =   615
      End
      Begin VB.Frame FrameAnaMarcadas 
         Height          =   5055
         Left            =   -74760
         TabIndex        =   66
         Top             =   7320
         Width           =   6735
         Begin VB.ListBox EcAnalisesMarcadas 
            Appearance      =   0  'Flat
            Height          =   4125
            Left            =   120
            MultiSelect     =   2  'Extended
            TabIndex        =   68
            Top             =   720
            Width           =   5775
         End
         Begin VB.Label Label1 
            Caption         =   "An�lises Marcadas"
            Height          =   255
            Index           =   8
            Left            =   240
            TabIndex        =   67
            Top             =   240
            Width           =   1575
         End
      End
      Begin VB.TextBox EcCodLote2 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -74040
         TabIndex        =   63
         Top             =   960
         Width           =   735
      End
      Begin VB.TextBox EcDescrLote2 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -73320
         TabIndex        =   62
         Top             =   960
         Width           =   3855
      End
      Begin VB.CommandButton BtPesquisaLote2 
         Height          =   315
         Left            =   -69480
         Picture         =   "FormCQGeral.frx":3D50
         Style           =   1  'Graphical
         TabIndex        =   61
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Suportes"
         Top             =   960
         Width           =   375
      End
      Begin VB.CommandButton BtRetira 
         Height          =   495
         Left            =   -70440
         Picture         =   "FormCQGeral.frx":40DA
         Style           =   1  'Graphical
         TabIndex        =   60
         ToolTipText     =   " Retirar do Perfil "
         Top             =   3480
         Width           =   495
      End
      Begin VB.ListBox EcAnalisesSel 
         Appearance      =   0  'Flat
         Height          =   4125
         Left            =   -69720
         TabIndex        =   58
         Top             =   1800
         Width           =   3375
      End
      Begin VB.CommandButton BtInsere 
         Height          =   495
         Left            =   -70440
         Picture         =   "FormCQGeral.frx":4464
         Style           =   1  'Graphical
         TabIndex        =   57
         ToolTipText     =   " Inserir no Perfil "
         Top             =   2880
         Width           =   495
      End
      Begin VB.ListBox EcAnalises 
         Appearance      =   0  'Flat
         Height          =   4125
         Left            =   -74040
         MultiSelect     =   2  'Extended
         TabIndex        =   55
         Top             =   1800
         Width           =   3375
      End
      Begin VB.TextBox EcSeqApar2 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -74040
         TabIndex        =   53
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox EcDescrApar2 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -73320
         TabIndex        =   52
         Top             =   600
         Width           =   3855
      End
      Begin VB.CommandButton BtPesquisaApar2 
         Height          =   315
         Left            =   -69480
         Picture         =   "FormCQGeral.frx":47EE
         Style           =   1  'Graphical
         TabIndex        =   51
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Suportes"
         Top             =   600
         Width           =   375
      End
      Begin VB.CommandButton BtRefresh 
         Height          =   495
         Left            =   -61080
         Picture         =   "FormCQGeral.frx":4B78
         Style           =   1  'Graphical
         TabIndex        =   49
         ToolTipText     =   "Actualizar Gr�fico"
         Top             =   480
         Width           =   495
      End
      Begin VB.Frame FrameGraf2 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   4935
         Left            =   -74520
         TabIndex        =   39
         Top             =   1320
         Width           =   495
         Begin VB.TextBox Text16 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   48
            Text            =   "-4DP"
            Top             =   4590
            Width           =   495
         End
         Begin VB.TextBox Text15 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   47
            Text            =   "2DP"
            Top             =   1320
            Width           =   495
         End
         Begin VB.TextBox Text14 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   46
            Text            =   "3DP"
            Top             =   840
            Width           =   495
         End
         Begin VB.TextBox Text13 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   45
            Text            =   "4DP"
            Top             =   360
            Width           =   495
         End
         Begin VB.TextBox Text12 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   44
            Text            =   "-1DP"
            Top             =   3000
            Width           =   495
         End
         Begin VB.TextBox Text11 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   43
            Text            =   "-2DP"
            Top             =   3480
            Width           =   495
         End
         Begin VB.TextBox Text10 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   42
            Text            =   "-3DP"
            Top             =   4080
            Width           =   495
         End
         Begin VB.TextBox Text9 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   41
            Text            =   "1DP"
            Top             =   1885
            Width           =   495
         End
         Begin VB.TextBox Text8 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   40
            Text            =   "M�dia"
            Top             =   2400
            Width           =   495
         End
      End
      Begin VB.Frame FrameGraf1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   4935
         Left            =   -61080
         TabIndex        =   29
         Top             =   1320
         Width           =   495
         Begin VB.TextBox EcMedia 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   38
            Text            =   "M�dia"
            Top             =   2400
            Width           =   495
         End
         Begin VB.TextBox Ec1s 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   37
            Text            =   "1DP"
            Top             =   1925
            Width           =   495
         End
         Begin VB.TextBox Text1 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   36
            Text            =   "-3DP"
            Top             =   4080
            Width           =   495
         End
         Begin VB.TextBox Text2 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   35
            Text            =   "-2DP"
            Top             =   3480
            Width           =   495
         End
         Begin VB.TextBox Text3 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   34
            Text            =   "-1DP"
            Top             =   3000
            Width           =   495
         End
         Begin VB.TextBox Text4 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   33
            Text            =   "4DP"
            Top             =   240
            Width           =   495
         End
         Begin VB.TextBox Text5 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   32
            Text            =   "3DP"
            Top             =   840
            Width           =   495
         End
         Begin VB.TextBox Text6 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   31
            Text            =   "2DP"
            Top             =   1320
            Width           =   495
         End
         Begin VB.TextBox Text7 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   0
            TabIndex        =   30
            Text            =   "-4DP"
            Top             =   4590
            Width           =   495
         End
      End
      Begin VB.OptionButton OptMedia 
         Caption         =   "M�dia Alvo"
         Height          =   255
         Index           =   1
         Left            =   -62400
         TabIndex        =   27
         Top             =   360
         Width           =   1335
      End
      Begin VB.OptionButton OptMedia 
         Caption         =   "M�dia Obtida"
         Height          =   255
         Index           =   0
         Left            =   -63960
         TabIndex        =   26
         Top             =   360
         Width           =   1335
      End
      Begin VB.ListBox EcListaGraf 
         Appearance      =   0  'Flat
         Height          =   705
         Left            =   -73920
         Style           =   1  'Checkbox
         TabIndex        =   24
         Top             =   360
         Width           =   9855
      End
      Begin MSFlexGridLib.MSFlexGrid FgResumo 
         Height          =   5895
         Left            =   -74880
         TabIndex        =   22
         Top             =   360
         Width           =   14295
         _ExtentX        =   25215
         _ExtentY        =   10398
         _Version        =   393216
         BackColorBkg    =   -2147483633
         SelectionMode   =   1
         BorderStyle     =   0
      End
      Begin MSFlexGridLib.MSFlexGrid FgRes 
         Height          =   6015
         Left            =   120
         TabIndex        =   23
         Top             =   360
         Width           =   14535
         _ExtentX        =   25638
         _ExtentY        =   10610
         _Version        =   393216
         BackColorBkg    =   -2147483633
         SelectionMode   =   1
         BorderStyle     =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Century Gothic"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSChart20Lib.MSChart Grafico 
         Height          =   5895
         Left            =   -74880
         OleObjectBlob   =   "FormCQGeral.frx":52E2
         TabIndex        =   28
         Top             =   1080
         Width           =   14385
      End
      Begin VB.Label Label1 
         Caption         =   "Ac��o"
         Height          =   255
         Index           =   12
         Left            =   7560
         TabIndex        =   86
         Top             =   6480
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Observa��o"
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   78
         Top             =   6480
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Prefixo"
         Height          =   255
         Index           =   10
         Left            =   -68040
         TabIndex        =   70
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Lote"
         Height          =   255
         Index           =   9
         Left            =   -74880
         TabIndex        =   64
         Top             =   960
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "An�lises Seleccionadas"
         Height          =   255
         Index           =   7
         Left            =   -69600
         TabIndex        =   59
         Top             =   1560
         Width           =   2295
      End
      Begin VB.Label Label1 
         Caption         =   "An�lises do Aparelho"
         Height          =   255
         Index           =   6
         Left            =   -73920
         TabIndex        =   56
         Top             =   1560
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "Aparelho"
         Height          =   255
         Index           =   5
         Left            =   -74880
         TabIndex        =   54
         Top             =   600
         Width           =   735
      End
      Begin VB.Label LbAnalise 
         Height          =   255
         Left            =   -63840
         TabIndex        =   50
         Top             =   720
         Width           =   2655
      End
      Begin VB.Label Label1 
         Caption         =   "Controlos"
         Height          =   255
         Index           =   4
         Left            =   -74880
         TabIndex        =   25
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame v 
      Height          =   1695
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   14895
      Begin VB.Frame FrameCk 
         Height          =   735
         Left            =   12600
         TabIndex        =   75
         Top             =   840
         Width           =   2175
         Begin VB.OptionButton OptTipo 
            Caption         =   "Resultados Anormais"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   85
            Top             =   360
            Width           =   1935
         End
         Begin VB.OptionButton OptTipo 
            Caption         =   "Todos Resultados"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   84
            Top             =   120
            Width           =   1935
         End
      End
      Begin VB.CommandButton BtPesquisaAna 
         Height          =   315
         Left            =   9840
         Picture         =   "FormCQGeral.frx":7650
         Style           =   1  'Graphical
         TabIndex        =   20
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Suportes"
         Top             =   1320
         Width           =   375
      End
      Begin VB.TextBox EcDescrAna 
         Appearance      =   0  'Flat
         BackColor       =   &H80000000&
         Height          =   285
         Left            =   6840
         TabIndex        =   19
         Top             =   1320
         Width           =   3015
      End
      Begin VB.TextBox EcCodAna 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   6120
         TabIndex        =   18
         Top             =   1320
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaLote 
         Height          =   315
         Left            =   9840
         Picture         =   "FormCQGeral.frx":79DA
         Style           =   1  'Graphical
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Suportes"
         Top             =   840
         Width           =   375
      End
      Begin VB.TextBox EcDescrLote 
         Appearance      =   0  'Flat
         BackColor       =   &H80000000&
         Height          =   285
         Left            =   6840
         TabIndex        =   16
         Top             =   840
         Width           =   3015
      End
      Begin VB.TextBox EcCodLote 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   6120
         TabIndex        =   15
         Top             =   840
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaControlo 
         Height          =   315
         Left            =   4680
         Picture         =   "FormCQGeral.frx":7D64
         Style           =   1  'Graphical
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Suportes"
         Top             =   1320
         Width           =   375
      End
      Begin VB.TextBox EcDescrControlo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000000&
         Height          =   285
         Left            =   1680
         TabIndex        =   13
         Top             =   1320
         Width           =   3015
      End
      Begin VB.TextBox EcCodControlo 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   960
         TabIndex        =   12
         Top             =   1320
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaApar 
         Height          =   315
         Left            =   4680
         Picture         =   "FormCQGeral.frx":80EE
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Suportes"
         Top             =   840
         Width           =   375
      End
      Begin VB.TextBox EcDescrApar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000000&
         Height          =   285
         Left            =   1680
         TabIndex        =   10
         Top             =   840
         Width           =   3015
      End
      Begin VB.TextBox EcSeqApar 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   960
         TabIndex        =   9
         Top             =   840
         Width           =   735
      End
      Begin MSComCtl2.DTPicker EcDtInicial 
         Height          =   255
         Left            =   960
         TabIndex        =   2
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   147128321
         CurrentDate     =   40478
      End
      Begin MSComCtl2.DTPicker EcDtFinal 
         Height          =   255
         Left            =   2640
         TabIndex        =   3
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   147128321
         CurrentDate     =   40478
      End
      Begin VB.Label Label1 
         Caption         =   "An�lise"
         Height          =   255
         Index           =   3
         Left            =   5520
         TabIndex        =   8
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Lote"
         Height          =   255
         Index           =   2
         Left            =   5520
         TabIndex        =   7
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Controlo"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Aparelho"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Datas"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   615
      End
   End
   Begin VB.PictureBox Pic 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      Picture         =   "FormCQGeral.frx":8478
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   0
      Top             =   10680
      Width           =   255
      Begin VB.ListBox EcAnalisesMarcadass 
         Appearance      =   0  'Flat
         Height          =   4125
         Left            =   0
         MultiSelect     =   2  'Extended
         TabIndex        =   65
         Top             =   0
         Width           =   5775
      End
   End
   Begin VB.Label Label3 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Label3"
      ForeColor       =   &H000000FF&
      Height          =   615
      Left            =   3000
      TabIndex        =   81
      Top             =   9480
      Width           =   1455
   End
End
Attribute VB_Name = "FormCQGeral"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim rsAna As New ADODB.recordset

Const cLightGray = &H808080
Const cLightGreen = &HC0FFC0
Const cLightYellow = &H80FFFF
Const cWhite = &HFFFFFF
Const cLightBlue = &HFCE7D8
Const cOrange = &HC0E0FF
Const vermelho = &H8080FF
Const Amarelo = &H80FFFF

Const lColResiApar = 0
Const lColResiLote = 1
Const lColResiAna = 2
Const lColResiRes = 3
Const lColResObs = 4
Const lColResAparelho = 5
Const lColResAna = 6
Const lColResLote = 7
Const lColResNivel = 8
Const lColResData = 9
Const lColResResultado = 10
Const lColResMediaAc = 11
Const lColResDesvioAc = 12
Const lColResMediaO = 13
Const lColResMediaA = 14
Const lColResSigmaAc = 15
Const lColResMenos3 = 16
Const lColResMenos2 = 17
Const lColResMenos1 = 18
Const lColRes0 = 19
Const lColRes1 = 20
Const lColRes2 = 21
Const lColRes3 = 22
Const lColResActivo = 23
Const lColResEstado = 24
Const lColResRegras = 25

Const lColResumoParam = 0
Const lColResumoLote = 1
Const lColResumoNivel = 2
Const lColResumoNumTestes = 3
Const lColResumoMedia = 4
Const lColResumoDesvioPadrao = 5
Const lColResumoCv = 6
Const lColResumoBias = 7
Const lColResumoETC = 8
Const lColResumoID = 9
Const lColResumoMediaAlvo = 10
Const lColResumoDesvioPadraoAlvo = 11
Const lColResumoDesvioPadraoPercentAlvo = 12
Const lColResumoImpr = 13
Const lColResumoInex = 14
Const lColResumoEta = 15

'ESTRUTURA QUE GUARDA SE O LOTE/CONTROLO TEM OU N�O REGRAS CODIFICADAS
Private Type RegrasResultado
    flg_estado As Integer
End Type
Private Type resultado
    seq_resultado As Long
    resultado As String
    flg_excluido As Integer
    obs As String
    accao As String
    estado As Integer
    dt_cri As String
    hr_cri As String
    AvalMedO As Double
    AvalMedA As Double
    media As Double
    desvio As Double
    bias As Double
    cv As Double
    sigma As Double
    cor As Long
    idxAux As Integer
    flg_invisivel As Integer
    
    totalRegras As Integer
    estrutRegras() As RegrasResultado
End Type

Private Type analise
    cod_ana As String
    descr_ana As String
    media_alvo As Double
    desvio_alvo As Double
    num_Amostras As Integer
    media_obt As Double
    desvio_obt As Double
    desvio_obt_perc As Double
    cv As Double
    bias As Double
    etc As Double
    id As Double
    imprecisao As Double
    inexatidao As Double
    eta As Double
    flg_selec As Boolean
    num_dias As Integer
    estrutRes() As resultado
    totalResultado As Integer
End Type

'ESTRUTURA QUE GUARDA SE O LOTE/CONTROLO TEM OU N�O REGRAS CODIFICADAS
Private Type RegrasLote
    flg_activo As Integer
    flg_accao As Integer
    indice As Integer
End Type
Private Type lote
    cod_lote As String
    descr_lote As String
    cod_controlo As String
    descr_controlo As String
    dt_ini As String
    dt_fim As String
    dt_validade As String
    flg_activo As Integer
    nivel As Integer
    descr_nivel As String
    estrutAna() As analise
    totalAna As Integer
    
    estrutRegras() As RegrasLote
    totalRegras As Integer
End Type

'ESTRUTURA AUXILIAR PARA GUARDAR OS RESULTADOS POR APARELHO PARA MELHOR AVALIACAO DAS REGRAS WESTGARD
Private Type AuxAna
    cod_lote As String
    cod_controlo As String
    cod_ana_s As String
    resultado As Double
    flg_excluido As Integer
    estado As Integer
    dt_cri As String
    hr_cri As String
    iLote As Integer
    iAna As Integer
    iRes As Integer
End Type
Private Type aparelho
    seq_apar As Integer
    descr_apar As String
    estrutLote() As lote
    totalLote As Integer
    totalAuxAna As Integer
    estrutAuxAna() As AuxAna
End Type
Dim Estrutura() As aparelho
Dim totalEstrut As Integer

' LISTA DAS LINHAS QUE ESTAO NA ECLISTA
Private Type lista
    iApar As Integer
    iLote As Integer
    iAna As Integer
    indice As Integer
End Type
Dim estrutLista() As lista
Dim totalLista As Integer

'LISTA DE CORES DAS LINHAS DO GRAFICO
Private Type cores
    r As Integer
    g As Integer
    b As Integer
End Type
Dim estrutCores() As cores
Dim multiGraf As Boolean

'PARA PREENCHER A FGRESUMO ORDENADO POR AN�LISE
Dim AnaOrdenada() As String

Private Type AnaSelec
    seq_ana_s As Long
    cod_ana_s As String
    descr_ana_s As String
    prefixo As String
    seq_apar As Integer
    cod_lote As String
End Type
Dim EstrutAnaSelec() As AnaSelec
Dim totalAnaSelec As Integer

'PARA PREENCHER FGRES ORDENADO POR DATA E NAO POR ANALISE
Private Type ordenaData
    iApar As Integer
    iLote As Integer
    iAna As Integer
    iRes As Integer
End Type
Dim EstrutOrderData() As ordenaData
Dim totalOrderData As Integer

Dim colFgRes As Integer
Dim rowFgRes As Integer

' ESTRUTURA COM CODIFICA��O DE REGRAS PREDEFINIDAS
Private Type Regra
    cod_regra As Integer
    descr_regra As String
    abrev_regra As String
End Type
Dim estrutRegras() As Regra
Dim totalRegras As Integer

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub
Sub EventoUnload()
    BG_StackJanelas_Pop
    BL_ToolbarEstadoN 0
    
    Set gFormActivo = MDIFormInicio
    Set FormCQGeral = Nothing
End Sub

Sub Inicializacoes()

    Me.caption = "Controlo de Qualidade "
    Me.left = 5
    Me.top = 5
    Me.Width = 15300
    Me.Height = 9435 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
End Sub
Sub DefTipoCampos()
    With FgRes
        .rows = 2
        .FixedRows = 1
        .Cols = 26
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .MergeCells = flexMergeFree
        '.GridColor = e_LightGray
        
        .ColWidth(lColResiApar) = 0
        .Col = lColResiApar
        .TextMatrix(0, lColResiApar) = "Aparelho"
        .ColWidth(lColResiLote) = 0
        .Col = lColResiLote
        .TextMatrix(0, lColResiLote) = "Lote"
        .ColWidth(lColResiAna) = 0
        .Col = lColResiAna
        .TextMatrix(0, lColResiAna) = "Ana"
        .ColWidth(lColResiRes) = 0
        .Col = lColResiRes
        .TextMatrix(0, lColResiRes) = "Res"

        .ColWidth(lColResObs) = 300
        .Col = lColResObs
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResObs) = ""
        .ColAlignment(lColResObs) = flexAlignLeftCenter
        
        .ColWidth(lColResLote) = 700
        .Col = lColResLote
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResLote) = "Lote"
        .ColAlignment(lColResLote) = flexAlignLeftCenter
        
        .ColWidth(lColResAparelho) = 1000
        .Col = lColResAparelho
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResAparelho) = "Aparelho"
        .ColAlignment(lColResAparelho) = flexAlignLeftCenter
        
        .ColWidth(lColResAna) = 2000
        .Col = lColResAna
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResAna) = "An�lise"
        .ColAlignment(lColResAna) = flexAlignLeftCenter
        
        .ColWidth(lColResNivel) = 500
        .Col = lColResNivel
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResNivel) = "N�vel"
        .ColAlignment(lColResNivel) = flexAlignLeftCenter
        
        .ColWidth(lColResResultado) = 600
        .Col = lColResResultado
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResResultado) = "Valor"
        .ColAlignment(lColResResultado) = flexAlignLeftCenter
        
        .ColWidth(lColResData) = 1400
        .Col = lColResData
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResData) = "Data"
        .ColAlignment(lColResData) = flexAlignLeftCenter
        
        .ColWidth(lColResActivo) = 400
        .Col = lColResActivo
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResActivo) = "Act."
        .ColAlignment(lColResActivo) = flexAlignCenterCenter
        
        .ColWidth(lColResEstado) = 400
        .Col = lColResEstado
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResEstado) = "Est."
        .ColAlignment(lColResEstado) = flexAlignCenterCenter
                
        .ColWidth(lColResMediaA) = 0
        .Col = lColResMediaA
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResMediaA) = " "
        .ColAlignment(lColResMediaA) = flexAlignLeftCenter
        
        .ColWidth(lColResMediaAc) = 800
        .Col = lColResMediaAc
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResMediaAc) = "M.Acum"
        .ColAlignment(lColResMediaAc) = flexAlignLeftCenter
        
        .ColWidth(lColResDesvioAc) = 800
        .Col = lColResDesvioAc
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResDesvioAc) = "DP.Acum"
        .ColAlignment(lColResDesvioAc) = flexAlignLeftCenter
        
        .ColWidth(lColResMediaO) = 600
        .Col = lColResMediaO
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResMediaO) = "ZScore"
        .ColAlignment(lColResMediaO) = flexAlignLeftCenter
        
        .ColWidth(lColResSigmaAc) = 600
        .Col = lColResSigmaAc
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResSigmaAc) = "Sigma"
        .ColAlignment(lColResSigmaAc) = flexAlignLeftCenter
        
        .ColWidth(lColResMenos1) = 300
        .Col = lColResMenos1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResMenos1) = "-1"
        .ColAlignment(lColResMenos1) = flexAlignLeftCenter
        
        .ColWidth(lColResMenos2) = 300
        .Col = lColResMenos2
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResMenos2) = "-2"
        .ColAlignment(lColResMenos2) = flexAlignLeftCenter
        
        .ColWidth(lColResMenos3) = 300
        .Col = lColResMenos3
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResMenos3) = "-3"
        .ColAlignment(lColResMenos3) = flexAlignLeftCenter
        
        
        .ColWidth(lColRes0) = 300
        .Col = lColRes0
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColRes0) = "0"
        .ColAlignment(lColRes0) = flexAlignLeftCenter
        
        .ColWidth(lColRes1) = 300
        .Col = lColRes1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColRes1) = "1"
        .ColAlignment(lColRes1) = flexAlignLeftCenter
        
        .ColWidth(lColRes2) = 300
        .Col = lColRes2
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColRes2) = "2"
        .ColAlignment(lColRes2) = flexAlignLeftCenter
        
        .ColWidth(lColRes3) = 300
        .Col = lColResMenos3
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColRes3) = "3"
        .ColAlignment(lColRes3) = flexAlignLeftCenter
        
        .ColWidth(lColResRegras) = 2000
        .Col = lColResRegras
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, lColResRegras) = "Regras"
        .ColAlignment(lColResRegras) = flexAlignLeftCenter
        
        
        
        .MergeRow(0) = True
        .WordWrap = False
        .row = 0
        .Col = 0
    End With
    
    With FgResumo
        .rows = 3
        .FixedRows = 2
        .Cols = 16
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionByRow
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .MergeCells = flexMergeFree
        
        .ColWidth(lColResumoParam) = 2000
        .Col = lColResumoParam
        .ColAlignment(lColResumoParam) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoParam) = "Par�metro"
        .TextMatrix(0, lColResumoParam) = "Controlo"
        .MergeCol(lColResumoParam) = True
        
        .ColWidth(lColResumoLote) = 1500
        .Col = lColResumoLote
        .ColAlignment(lColResumoLote) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoLote) = "Lote"
        .TextMatrix(0, lColResumoLote) = "Controlo"
        
        .ColWidth(lColResumoNivel) = 800
        .Col = lColResumoNivel
        .ColAlignment(lColResumoNivel) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoNivel) = "N�vel"
        .TextMatrix(0, lColResumoNivel) = "Controlo"
        
        .ColWidth(lColResumoNumTestes) = 700
        .Col = lColResumoNumTestes
        .ColAlignment(lColResumoNumTestes) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoNumTestes) = "n"
        .TextMatrix(0, lColResumoNumTestes) = "Result. Obtidos"
        
        .ColWidth(lColResumoMedia) = 700
        .Col = lColResumoMedia
        .ColAlignment(lColResumoMedia) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoMedia) = "M�dia"
        .TextMatrix(0, lColResumoMedia) = "Result. Obtidos"
        
        .ColWidth(lColResumoDesvioPadrao) = 700
        .Col = lColResumoDesvioPadrao
        .ColAlignment(lColResumoDesvioPadrao) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoDesvioPadrao) = "1SD"
        .TextMatrix(0, lColResumoDesvioPadrao) = "Result. Obtidos"
        
        .ColWidth(lColResumoCv) = 700
        .Col = lColResumoCv
        .ColAlignment(lColResumoCv) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoCv) = "Cv%"
        .TextMatrix(0, lColResumoCv) = "Result. Obtidos"
        
        .ColWidth(lColResumoBias) = 700
        .Col = lColResumoBias
        .ColAlignment(lColResumoBias) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoBias) = "Bias%"
        .TextMatrix(0, lColResumoBias) = "Result. Obtidos"
        
        .ColWidth(lColResumoETC) = 700
        .Col = lColResumoETC
        .ColAlignment(lColResumoETC) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoETC) = "ETc%"
        .TextMatrix(0, lColResumoETC) = "Result. Obtidos"
        
        .ColWidth(lColResumoID) = 700
        .Col = lColResumoID
        .ColAlignment(lColResumoID) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoID) = "ID"
        .TextMatrix(0, lColResumoID) = "Result. Obtidos"
        
        .ColWidth(lColResumoMediaAlvo) = 700
        .Col = lColResumoMediaAlvo
        .ColAlignment(lColResumoMediaAlvo) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoMediaAlvo) = "M�dia"
        .TextMatrix(0, lColResumoMediaAlvo) = "Result. Alvo"
        
        .ColWidth(lColResumoDesvioPadraoAlvo) = 700
        .Col = lColResumoDesvioPadraoAlvo
        .ColAlignment(lColResumoDesvioPadraoAlvo) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoDesvioPadraoAlvo) = "1SD"
        .TextMatrix(0, lColResumoDesvioPadraoAlvo) = "Result. Alvo"
        
        .ColWidth(lColResumoDesvioPadraoPercentAlvo) = 700
        .Col = lColResumoDesvioPadraoPercentAlvo
        .ColAlignment(lColResumoDesvioPadraoPercentAlvo) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoDesvioPadraoPercentAlvo) = "1SD%"
        .TextMatrix(0, lColResumoDesvioPadraoPercentAlvo) = "Result. Alvo"
        
        .ColWidth(lColResumoImpr) = 700
        .Col = lColResumoImpr
        .ColAlignment(lColResumoImpr) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoImpr) = "Imp%"
        .TextMatrix(0, lColResumoImpr) = "Crit. Desempenho"
        
        .ColWidth(lColResumoInex) = 700
        .Col = lColResumoInex
        .ColAlignment(lColResumoInex) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoInex) = "Inex%"
        .TextMatrix(0, lColResumoInex) = "Crit. Desempenho"
        
        .ColWidth(lColResumoEta) = 700
        .Col = lColResumoEta
        .ColAlignment(lColResumoEta) = flexAlignCenterCenter
        .TextMatrix(1, lColResumoEta) = "Eta%"
        .TextMatrix(0, lColResumoEta) = "Crit. Desempenho"
        .MergeRow(0) = True
        .WordWrap = False
        .row = 0
        .Col = 0
    End With
    SSTab1.Tab = 0
    OptMedia(0).value = True
    DefTipoGrafico
    If gModoDebug = mediSim Then
        SSTab1.TabVisible(3) = True
    Else
        SSTab1.TabVisible(3) = False
    End If
    OptTipo(0).value = True
    CarregaRegrasDisponiveis
End Sub
Sub PreencheValoresDefeito()
    EcDtInicial.value = Bg_DaData_ADO - 30
    EcDtFinal.value = Bg_DaData_ADO
End Sub

Private Sub BtGerar_Click()
    GeraListaControlo
End Sub

Private Sub BtGravaObs_Click()
    GravaObs FgRes.TextMatrix(FgRes.row, lColResiApar), FgRes.TextMatrix(FgRes.row, lColResiLote), _
                              FgRes.TextMatrix(FgRes.row, lColResiAna), FgRes.TextMatrix(FgRes.row, lColResiRes), _
                              FgRes.row
End Sub

Private Sub BtPesquisaApar2_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "seq_apar"
    CamposEcran(1) = "seq_apar"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_apar"
    CamposEcran(2) = "descr_apar"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "gc_apar"
    CampoPesquisa1 = "descr_apar"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Aparelhos")
    
    mensagem = "N�o foi encontrada nenhum Aparelho."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcSeqApar2.Text = resultados(1)
            EcDescrApar2.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

    PreencheAnaApar
    PreencheAnaAparMarcadas
End Sub

Private Sub BtRefresh_Click()
    DesenhaGrafico
End Sub



Private Sub CkDP_Click(Index As Integer)
    PreencheFGRes

End Sub

Private Sub eccodlote_Validate(Cancel As Boolean)
    Dim RsDescrCaixa As ADODB.recordset
    
    
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodLote.Text) <> "" Then
        Set RsDescrCaixa = New ADODB.recordset
        
        With RsDescrCaixa
            .Source = "SELECT descr_lote, cod_lote FROM sl_cq_lote WHERE cod_lote= " & BL_TrataStringParaBD(EcCodLote.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrCaixa.RecordCount > 0 Then
            EcCodLote.Text = "" & RsDescrCaixa!cod_lote
            EcDescrLote.Text = "" & RsDescrCaixa!descr_lote
        Else
            Cancel = True
            EcDescrLote.Text = ""
            BG_Mensagem mediMsgBox, "O lote indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrCaixa.Close
        Set RsDescrCaixa = Nothing
    Else
        EcDescrLote.Text = ""
    End If


End Sub
Private Sub ecCodLote2_Validate(Cancel As Boolean)
    Dim RsDescrCaixa As ADODB.recordset
    
    
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodLote2.Text) <> "" Then
        Set RsDescrCaixa = New ADODB.recordset
        
        With RsDescrCaixa
            .Source = "SELECT descr_lote, cod_lote FROM sl_cq_lote WHERE cod_lote= " & BL_TrataStringParaBD(EcCodLote2.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrCaixa.RecordCount > 0 Then
            EcCodLote2.Text = "" & RsDescrCaixa!cod_lote
            EcDescrLote2.Text = "" & RsDescrCaixa!descr_lote
        Else
            Cancel = True
            EcDescrLote2.Text = ""
            BG_Mensagem mediMsgBox, "O lote indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrCaixa.Close
        Set RsDescrCaixa = Nothing
    Else
        EcDescrLote2.Text = ""
    End If
    PreencheAnaApar
    PreencheAnaAparMarcadas
End Sub
Private Sub EcCodana_Validate(Cancel As Boolean)
    Dim RsDescrCaixa As ADODB.recordset
    
    EcCodAna = UCase(EcCodAna)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodAna.Text) <> "" Then
        Set RsDescrCaixa = New ADODB.recordset
        
        With RsDescrCaixa
            .Source = "SELECT descr_ana_S, cod_ana_s FROM sl_ana_s WHERE cod_Ana_S= " & BL_TrataStringParaBD(EcCodAna.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrCaixa.RecordCount > 0 Then
            EcCodAna.Text = "" & RsDescrCaixa!cod_ana_s
            EcDescrAna.Text = "" & RsDescrCaixa!descr_ana_s
        Else
            Cancel = True
            EcDescrAna.Text = ""
            BG_Mensagem mediMsgBox, "A An�lise indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrCaixa.Close
        Set RsDescrCaixa = Nothing
    Else
        EcDescrAna.Text = ""
    End If


End Sub
Private Sub EcCodControlo_Validate(Cancel As Boolean)
    Dim RsDescrCaixa As ADODB.recordset
    
    
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodControlo.Text) <> "" Then
        Set RsDescrCaixa = New ADODB.recordset
        
        With RsDescrCaixa
            .Source = "SELECT descr_controlo, cod_controlo FROM sl_cq_controlo WHERE cod_controlo= " & BL_TrataStringParaBD(EcCodControlo.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrCaixa.RecordCount > 0 Then
            EcCodControlo.Text = "" & RsDescrCaixa!cod_controlo
            EcDescrControlo.Text = "" & RsDescrCaixa!descr_controlo
        Else
            Cancel = True
            EcDescrControlo.Text = ""
            BG_Mensagem mediMsgBox, "O Controlo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrCaixa.Close
        Set RsDescrCaixa = Nothing
    Else
        EcDescrControlo.Text = ""
    End If


End Sub




Private Sub EcSeqApar_Validate(Cancel As Boolean)
    Dim RsDescrCaixa As ADODB.recordset
    
    If EcSeqApar = "" Then Exit Sub
    If Not IsNumeric(EcSeqApar) Then
        BG_Mensagem mediMsgBox, "Campo num�rico!", vbOKOnly + vbExclamation, App.ProductName
        Cancel = True
        Exit Sub
    End If
    
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcSeqApar.Text) <> "" Then
        Set RsDescrCaixa = New ADODB.recordset
        
        With RsDescrCaixa
            .Source = "SELECT seq_apar, descr_apar FROM gc_apar WHERE seq_apar= " & EcSeqApar.Text
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrCaixa.RecordCount > 0 Then
            EcSeqApar.Text = "" & RsDescrCaixa!seq_apar
            EcDescrApar.Text = "" & RsDescrCaixa!descr_apar
        Else
            Cancel = True
            EcDescrApar.Text = ""
            BG_Mensagem mediMsgBox, "O aparelho indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrCaixa.Close
        Set RsDescrCaixa = Nothing
    Else
        EcDescrApar.Text = ""
    End If

End Sub

Private Sub EcSeqApar2_Validate(Cancel As Boolean)
    Dim RsDescrCaixa As ADODB.recordset
    
    If EcSeqApar2 = "" Then Exit Sub
    If Not IsNumeric(EcSeqApar2) Then
        BG_Mensagem mediMsgBox, "Campo num�rico!", vbOKOnly + vbExclamation, App.ProductName
        Cancel = True
        Exit Sub
    End If
    
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcSeqApar2.Text) <> "" Then
        Set RsDescrCaixa = New ADODB.recordset
        
        With RsDescrCaixa
            .Source = "SELECT seq_apar, descr_apar FROM gc_apar WHERE seq_apar= " & EcSeqApar2.Text
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrCaixa.RecordCount > 0 Then
            EcSeqApar2.Text = "" & RsDescrCaixa!seq_apar
            EcDescrApar2.Text = "" & RsDescrCaixa!descr_apar
        Else
            Cancel = True
            EcDescrApar2.Text = ""
            BG_Mensagem mediMsgBox, "O aparelho indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrCaixa.Close
        Set RsDescrCaixa = Nothing
    Else
        EcDescrApar2.Text = ""
    End If
    PreencheAnaApar
    PreencheAnaAparMarcadas
End Sub

Private Sub FGRes_DblClick()
    Dim i As Integer
    If FgRes.TextMatrix(FgRes.row, lColResiApar) <> "" Then
        For i = 0 To EcListaGraf.ListCount - 1
            EcListaGraf.Selected(i) = False
        Next
        For i = 1 To totalLista
            If estrutLista(i).iApar = FgRes.TextMatrix(FgRes.row, lColResiApar) Then
                If estrutLista(i).iLote = FgRes.TextMatrix(FgRes.row, lColResiLote) Then
                    If estrutLista(i).iAna = FgRes.TextMatrix(FgRes.row, lColResiAna) Then
                        If gCQUsaMediaObtida = mediSim Then
                            OptMedia(0).value = True
                        Else
                            OptMedia(1).value = True
                        End If
                        EcListaGraf.Selected(i - 1) = True
                        BtRefresh_Click
                        SSTab1.Tab = 2
                    End If
                End If
            End If
        Next
    End If
End Sub

Private Sub FGRes_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim iApar As Integer
    Dim iLote As Integer
    Dim iAna As Integer
    Dim iRes As Integer
    If FgRes.row > 0 And KeyCode = vbKeyDelete Then
        If FgRes.TextMatrix(FgRes.row, lColResiApar) <> "" Then
            iApar = FgRes.TextMatrix(FgRes.row, lColResiApar)
            iLote = FgRes.TextMatrix(FgRes.row, lColResiLote)
            iAna = FgRes.TextMatrix(FgRes.row, lColResiAna)
            iRes = FgRes.TextMatrix(FgRes.row, lColResiRes)
            gMsgMsg = "Tem a certeza que quer apagar o controlo de qualidade seleccionado? "
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                ApagaControlo iApar, iLote, iAna, iRes
                FuncaoLimpar
            End If
        End If
    End If
End Sub

Private Sub FGRes_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'On Error Resume Next
    
    If FgRes.row > 0 And Button = 2 Then
        If FgRes.TextMatrix(FgRes.row, lColResiApar) <> "" Then
            If FgRes.Col = lColResActivo Then
                If Estrutura(FgRes.TextMatrix(FgRes.row, lColResiApar)).estrutLote(FgRes.TextMatrix(FgRes.row, lColResiLote)).estrutAna(FgRes.TextMatrix(FgRes.row, lColResiAna)).estrutRes(FgRes.TextMatrix(FgRes.row, lColResiRes)).flg_excluido = 0 Then
                    MDIFormInicio.IDM_FGRES(2).Visible = True
                    MDIFormInicio.IDM_FGRES(3).Visible = False
                    MDIFormInicio.IDM_FGRES(0).Visible = False
                    MDIFormInicio.IDM_FGRES(1).Visible = False
                Else
                    MDIFormInicio.IDM_FGRES(3).Visible = True
                    MDIFormInicio.IDM_FGRES(0).Visible = False
                    MDIFormInicio.IDM_FGRES(1).Visible = False
                    MDIFormInicio.IDM_FGRES(2).Visible = False
                End If
                MDIFormInicio.PopupMenu MDIFormInicio.HIDE_CQ_RES
            ElseIf FgRes.Col = lColResEstado Then
                If Estrutura(FgRes.TextMatrix(FgRes.row, lColResiApar)).estrutLote(FgRes.TextMatrix(FgRes.row, lColResiLote)).estrutAna(FgRes.TextMatrix(FgRes.row, lColResiAna)).estrutRes(FgRes.TextMatrix(FgRes.row, lColResiRes)).estado = 0 Then
                    MDIFormInicio.IDM_FGRES(0).Visible = True
                    MDIFormInicio.IDM_FGRES(1).Visible = False
                Else
                    MDIFormInicio.IDM_FGRES(1).Visible = True
                    MDIFormInicio.IDM_FGRES(0).Visible = False
                End If
                MDIFormInicio.IDM_FGRES(2).Visible = False
                MDIFormInicio.IDM_FGRES(3).Visible = False
                MDIFormInicio.PopupMenu MDIFormInicio.HIDE_CQ_RES
            End If
        End If
    End If
End Sub

Private Sub FgRes_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If FgRes.row <= 0 Then Exit Sub
    
    FgRes_DevolveColunaActual X
    FgRes_DevolveLinhaActual Y
    
    If rowFgRes > 0 Then
        If FgRes.TextMatrix(rowFgRes, 0) <> "" Then
            If colFgRes = lColResEstado Then
                FgRes.ToolTipText = "Estado do Resultado (Aceite/Rejeitado) - Entra ou n�o na m�dia"
            ElseIf colFgRes = lColResActivo Then
                FgRes.ToolTipText = "Resultado Activo (Activo/Exclu�do) - Aparece ou n�o no gr�fico"
            Else
                FgRes.ToolTipText = ""
            End If
        Else
            FgRes.ToolTipText = ""
        End If
    Else
        FgRes.ToolTipText = ""
    End If

End Sub

' ----------------------------------------------------------------------

' RETORNA A COLUNA ACTUAL

' ----------------------------------------------------------------------
Private Sub FgRes_DevolveColunaActual(X As Single)
    Dim coluna As Integer
        For coluna = 0 To FgRes.Cols - 1
            If FgRes.ColPos(coluna) <= X And FgRes.ColPos(coluna) + FgRes.ColWidth(coluna) >= X Then
                colFgRes = coluna
                Exit Sub
            End If
        Next
    colFgRes = -1
End Sub


' ----------------------------------------------------------------------

' RETORNA A LINHA ACTUAL

' ----------------------------------------------------------------------
Private Sub FgRes_DevolveLinhaActual(Y As Single)
    Dim linha As Integer
        For linha = 0 To FgRes.rows - 1
            If FgRes.RowPos(linha) <= Y And FgRes.RowPos(linha) + FgRes.RowHeight(linha) >= Y Then
                rowFgRes = linha
                Exit Sub
            End If
        Next
        rowFgRes = -1
End Sub


Private Sub FGRes_RowColChange()
    If FgRes.TextMatrix(FgRes.row, lColResiApar) <> "" And FgRes.row > 1 Then
        EcObs = Estrutura(FgRes.TextMatrix(FgRes.row, lColResiApar)).estrutLote(FgRes.TextMatrix(FgRes.row, lColResiLote)).estrutAna(FgRes.TextMatrix(FgRes.row, lColResiAna)).estrutRes(FgRes.TextMatrix(FgRes.row, lColResiRes)).obs
        EcAccao = Estrutura(FgRes.TextMatrix(FgRes.row, lColResiApar)).estrutLote(FgRes.TextMatrix(FgRes.row, lColResiLote)).estrutAna(FgRes.TextMatrix(FgRes.row, lColResiAna)).estrutRes(FgRes.TextMatrix(FgRes.row, lColResiRes)).accao
    Else
        EcObs = ""
        EcAccao = ""
    End If
End Sub

Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub
Sub FuncaoLimpar()
    
    If estado = 2 Then
        LimpaCampos
    End If

End Sub

Sub FuncaoEstadoAnterior()
End Sub

Sub LimpaCampos()
    EcSeqApar = ""
    EcDescrAna = ""
    EcDescrApar = ""
    EcDescrControlo = ""
    EcDescrLote = ""
    EcCodAna = ""
    EcCodControlo = ""
    EcCodLote = ""
    LimpaFgResumo
    LimpaFgRes
    SSTab1.Tab = 0
    OptMedia(0).value = True
    ReDim AnaOrdenada(0)
    EcSeqApar2 = ""
    EcDescrApar2 = ""
    EcAnalises.Clear
    EcAnalisesSel.Clear
    EcObs = ""
    EcAccao = ""
End Sub
Sub FuncaoInserir()
End Sub

Sub FuncaoModificar()
End Sub

Sub BD_Update()
End Sub


Sub FuncaoProcurar()
    Dim sSql As String
    BL_InicioProcessamento Me, "A pesquisar registos."
    
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    sSql = ControiCriterio
    Set rsAna = New ADODB.recordset
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    
    If rsAna.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaFgResumo
        LimpaFgRes
        
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        
        BL_FimProcessamento Me
    End If
    ReDim AnaOrdenada(0)
End Sub

Sub PreencheCampos()
    Dim flg_apar As Boolean
    Dim flg_lote As Boolean
    Dim flg_ana As Boolean
    Dim iApar As Integer
    Dim iLote As Integer
    Dim iAna As Integer
    Dim iRes As Integer
    Dim i As Integer

    ' LIMPA ESTRUTURAS E FGRESUMO
    totalEstrut = 0
    ReDim Estrutura(0)
    ReDim AnaOrdenada(0)
    totalOrderData = 0
    ReDim EstrutOrderData(0)
    LimpaFgResumo
    
    While Not rsAna.EOF
        flg_apar = False
        For i = 1 To totalEstrut
            If Estrutura(i).seq_apar = BL_HandleNull(rsAna!seq_apar, "-1") Then
                iApar = i
                flg_apar = True
                Exit For
            End If
        Next
        If flg_apar = False Then
            iApar = PreencheCampos_apar(BL_HandleNull(rsAna!seq_apar, "-1"), BL_HandleNull(rsAna!descr_apar, ""))
        End If
        flg_lote = False
        For i = 1 To Estrutura(iApar).totalLote
            If Estrutura(iApar).estrutLote(i).cod_lote = BL_HandleNull(rsAna!cod_lote, "") Then
                flg_lote = True
                iLote = i
                Exit For
            End If
        Next
        If flg_lote = False Then
            iLote = PreencheCampos_lote(iApar, BL_HandleNull(rsAna!cod_controlo, ""), BL_HandleNull(rsAna!cod_lote, ""), BL_HandleNull(rsAna!descr_controlo, ""), _
                                         BL_HandleNull(rsAna!descr_lote, ""), BL_HandleNull(rsAna!dt_fim, ""), BL_HandleNull(rsAna!dt_ini, ""), BL_HandleNull(rsAna!dt_validade, ""), _
                                         BL_HandleNull(rsAna!flg_activo, 0), BL_HandleNull(rsAna!cod_nivel, 0), BL_HandleNull(rsAna!descr_nivel, ""))
        End If
        flg_ana = False
        For i = 1 To Estrutura(iApar).estrutLote(iLote).totalAna
            If Estrutura(iApar).estrutLote(iLote).estrutAna(i).cod_ana = BL_HandleNull(rsAna!cod_ana_s, "") Then
                flg_ana = True
                iAna = i
                Exit For
            End If
        Next
        If flg_ana = False Then
            iAna = PreencheCampos_Ana(iApar, iLote, BL_HandleNull(rsAna!cod_ana_s, ""), BL_HandleNull(rsAna!descr_ana_s, ""), BL_HandleNull(rsAna!desvio_padrao, 0), _
                                        BL_HandleNull(rsAna!media, 0), BL_HandleNull(rsAna!num_dias, 0), BL_HandleNull(rsAna!eta, 0))
        End If
        
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).totalResultado = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).totalResultado + 1
        ReDim Preserve Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).totalResultado)
        iRes = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).totalResultado
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).resultado = BL_Forca_Casas_Decimais(BL_HandleNull(rsAna!resultado, 0), BL_HandleNull(rsAna!casas_dec, 0))
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).dt_cri = BL_HandleNull(rsAna!dt_cri, "")
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).hr_cri = BL_HandleNull(rsAna!hr_cri, "")
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = BL_HandleNull(rsAna!flg_excluido, 0)
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = BL_HandleNull(rsAna!estado, 0)
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).obs = BL_HandleNull(rsAna!obs, "")
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).accao = BL_HandleNull(rsAna!accao, "")
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).seq_resultado = BL_HandleNull(rsAna!seq_resultado, -1)
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_invisivel = BL_HandleNull(rsAna!flg_invisivel, 0)
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).totalRegras = Estrutura(iApar).estrutLote(iLote).totalRegras
        
        'PREENCHE ESTRUTURA AUXILIAR COM OS RESULTADOS TODOS DO APARELHO, SEM SEPARA��O POR EPISODIO
        Estrutura(iApar).totalAuxAna = Estrutura(iApar).totalAuxAna + 1
        ReDim Preserve Estrutura(iApar).estrutAuxAna(Estrutura(iApar).totalAuxAna)
        Estrutura(iApar).estrutAuxAna(Estrutura(iApar).totalAuxAna).cod_ana_s = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cod_ana
        Estrutura(iApar).estrutAuxAna(Estrutura(iApar).totalAuxAna).cod_controlo = Estrutura(iApar).estrutLote(iLote).cod_controlo
        Estrutura(iApar).estrutAuxAna(Estrutura(iApar).totalAuxAna).cod_lote = Estrutura(iApar).estrutLote(iLote).cod_lote
        Estrutura(iApar).estrutAuxAna(Estrutura(iApar).totalAuxAna).dt_cri = BL_HandleNull(rsAna!dt_cri, "")
        Estrutura(iApar).estrutAuxAna(Estrutura(iApar).totalAuxAna).estado = BL_HandleNull(rsAna!estado, 0)
        Estrutura(iApar).estrutAuxAna(Estrutura(iApar).totalAuxAna).flg_excluido = BL_HandleNull(rsAna!flg_excluido, 0)
        Estrutura(iApar).estrutAuxAna(Estrutura(iApar).totalAuxAna).hr_cri = BL_HandleNull(rsAna!hr_cri, "")
        Estrutura(iApar).estrutAuxAna(Estrutura(iApar).totalAuxAna).iAna = iAna
        Estrutura(iApar).estrutAuxAna(Estrutura(iApar).totalAuxAna).iLote = iLote
        Estrutura(iApar).estrutAuxAna(Estrutura(iApar).totalAuxAna).iRes = iRes
        Estrutura(iApar).estrutAuxAna(Estrutura(iApar).totalAuxAna).resultado = BL_HandleNull(rsAna!resultado, 0)
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).idxAux = Estrutura(iApar).totalAuxAna
        
        'PARA JA TODAS AS REGRAS EST�O V�LIDAS
        ReDim Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estrutRegras(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).totalRegras)
        For i = 1 To Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).totalRegras
            Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estrutRegras(i).flg_estado = 0
        Next
        PreencheDadosAcumulados iApar, iLote, iAna, iRes
        
        'PREENCHE ESTRUTURA ORDENADA POR DATA DESCENDENTE, INDEPENDENTE DA ANALISE
        totalOrderData = totalOrderData + 1
        ReDim Preserve EstrutOrderData(totalOrderData)
        EstrutOrderData(totalOrderData).iAna = iAna
        EstrutOrderData(totalOrderData).iApar = iApar
        EstrutOrderData(totalOrderData).iLote = iLote
        EstrutOrderData(totalOrderData).iRes = iRes
        rsAna.MoveNext
    Wend
    PreencheFormulas
    PreencheFormulasAcumuladas
    PreencheFgResumo
    PreencheAvaliacoes
    PreencheFGRes
    PreencheListaGraf
End Sub
Private Function PreencheCampos_apar(seq_apar As Integer, descr_apar As String) As Integer
    totalEstrut = totalEstrut + 1
    ReDim Preserve Estrutura(totalEstrut)
    Estrutura(totalEstrut).descr_apar = descr_apar
    Estrutura(totalEstrut).seq_apar = seq_apar
    Estrutura(totalEstrut).totalLote = 0
    ReDim Estrutura(totalEstrut).estrutLote(0)
    Estrutura(totalEstrut).totalAuxAna = 0
    ReDim Estrutura(totalEstrut).estrutAuxAna(0)
    PreencheCampos_apar = totalEstrut
End Function
Private Function PreencheCampos_lote(iApar As Integer, cod_controlo As String, cod_lote As String, descr_controlo As String, descr_lote As String, _
                                     dt_fim As String, dt_ini As String, dt_validade As String, flg_activo As Integer, nivel As Integer, descr_nivel As String) As Integer
    Dim iLote As Integer
    Dim sSql As String
    Dim rsDados As New ADODB.recordset
    Dim i As Integer
    Estrutura(iApar).totalLote = Estrutura(iApar).totalLote + 1
    ReDim Preserve Estrutura(iApar).estrutLote(Estrutura(iApar).totalLote)
    iLote = Estrutura(iApar).totalLote
    
    Estrutura(iApar).estrutLote(iLote).cod_controlo = cod_controlo
    Estrutura(iApar).estrutLote(iLote).cod_lote = cod_lote
    Estrutura(iApar).estrutLote(iLote).descr_controlo = descr_controlo
    Estrutura(iApar).estrutLote(iLote).descr_lote = descr_lote
    Estrutura(iApar).estrutLote(iLote).dt_fim = dt_fim
    Estrutura(iApar).estrutLote(iLote).dt_ini = dt_ini
    Estrutura(iApar).estrutLote(iLote).dt_validade = dt_validade
    Estrutura(iApar).estrutLote(iLote).flg_activo = flg_activo
    Estrutura(iApar).estrutLote(iLote).nivel = nivel
    Estrutura(iApar).estrutLote(iLote).descr_nivel = descr_nivel
    
    'VERIFICA QUAIS AS REGRAS ASSOCIADAS PARA CADA LOTE/CONTROLO
    Estrutura(iApar).estrutLote(iLote).totalRegras = totalRegras
    ReDim Estrutura(iApar).estrutLote(iLote).estrutRegras(Estrutura(iApar).estrutLote(iLote).totalRegras)
    For i = 1 To Estrutura(iApar).estrutLote(iLote).totalRegras
        Estrutura(iApar).estrutLote(iLote).estrutRegras(i).flg_accao = -1
        Estrutura(iApar).estrutLote(iLote).estrutRegras(i).flg_activo = -1
        Estrutura(iApar).estrutLote(iLote).estrutRegras(i).indice = i
    Next
    sSql = "SELECT * FROM sl_cq_regras WHERE cod_controlo = " & BL_TrataStringParaBD(cod_controlo)
    rsDados.CursorLocation = adUseServer
    rsDados.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDados.Open sSql, gConexao
    If rsDados.RecordCount >= 1 Then
        While Not rsDados.EOF
            For i = 1 To totalRegras
                If estrutRegras(i).cod_regra = BL_HandleNull(rsDados!cod_regra, "") Then
                    Estrutura(iApar).estrutLote(iLote).estrutRegras(i).flg_activo = 1
                    Estrutura(iApar).estrutLote(iLote).estrutRegras(i).flg_accao = BL_HandleNull(rsDados!cod_regra, -1)
                    Exit For
                End If
            Next
            rsDados.MoveNext
        Wend
    End If
    rsDados.Close
    Set rsDados = Nothing
    Estrutura(iApar).estrutLote(iLote).totalAna = 0
    ReDim Estrutura(iApar).estrutLote(iLote).estrutAna(0)
    PreencheCampos_lote = iLote
End Function

Private Function PreencheCampos_Ana(iApar As Integer, iLote As Integer, cod_ana As String, descr_ana As String, desvio_alvo As Double, _
                                     media_alvo As Double, num_dias As Integer, eta As Double) As Integer
    Dim iAna As Integer
    Dim i As Integer
    Estrutura(iApar).estrutLote(iLote).totalAna = Estrutura(iApar).estrutLote(iLote).totalAna + 1
    ReDim Preserve Estrutura(iApar).estrutLote(iLote).estrutAna(Estrutura(iApar).estrutLote(iLote).totalAna)
    iAna = Estrutura(iApar).estrutLote(iLote).totalAna
    
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cod_ana = cod_ana
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).descr_ana = descr_ana
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_alvo = desvio_alvo
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_alvo = media_alvo
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).num_dias = num_dias
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).eta = eta
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).totalResultado = 0
    ReDim Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(0)
    PreencheCampos_Ana = iAna
    For i = 1 To UBound(AnaOrdenada)
        If AnaOrdenada(i) = cod_ana Then
            Exit Function
        End If
    Next
    ReDim Preserve AnaOrdenada(UBound(AnaOrdenada) + 1)
    AnaOrdenada(UBound(AnaOrdenada)) = cod_ana
End Function

Function ValidaCamposEc() As Integer

End Function

Sub FuncaoAnterior()
End Sub
Sub FuncaoSeguinte()
End Sub

Private Function ControiCriterio() As String
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    sSql = "SELECT  x1.seq_apar, x1.cod_lote, x2.descr_lote, x1.cod_ana_s, x3.descr_ana_s, x2.cod_nivel, x4.descr_nivel, "
    sSql = sSql & " x5.media, x5.desvio_padrao, x6.descr_apar, x1.resultado, x1.dt_cri, x1.hr_cri, x7.cod_controlo, x7.descr_controlo,"
    sSql = sSql & " x1.flg_excluido, x2.dt_ini, x2.dt_fim, x2.dt_validade, x2.flg_activo, x5.num_dias,x3.eta,X1.ESTADO, x1.obs, x1.seq_resultado,"
    sSql = sSql & " x1.accao, x1.flg_invisivel, x3.casas_dec "
    sSql = sSql & " FROM sl_cq_res x1, sl_Cq_lote x2, sl_ana_s x3, sl_tbf_nivel x4, sl_cq_ana x5, gc_apar x6, sl_cq_controlo x7 "
    sSql = sSql & " WHERE X1.cod_lote = X2.cod_lote And X1.cod_ana_s = x3.cod_ana_s AND x2.cod_nivel = x4.cod_nivel(+) "
    sSql = sSql & " AND x1.cod_ana_s = x5.cod_ana_s AND x1.cod_lote = x5.cod_lote "
    sSql = sSql & " AND x1.seq_apar = x6.seq_apar "
    sSql = sSql & " AND x2.cod_controlo = x7.cod_controlo "
    sSql = sSql & " AND x1.dt_cri between " & BL_TrataDataParaBD(EcDtInicial.value) & " AND " & BL_TrataDataParaBD(EcDtFinal.value)
    If EcCodAna <> "" Then
        sSql = sSql & " AND x1.cod_ana_s  =" & BL_TrataStringParaBD(EcCodAna)
    End If
    If EcCodLote <> "" Then
        sSql = sSql & " AND x1.cod_lote  =" & BL_TrataStringParaBD(EcCodLote)
    End If
    If EcSeqApar <> "" Then
        sSql = sSql & " AND x1.seq_apar  =" & BL_TrataStringParaBD(EcSeqApar)
    End If
    If EcCodControlo <> "" Then
        sSql = sSql & " AND x7.cod_controlo =" & BL_TrataStringParaBD(EcCodControlo)
    End If
    sSql = sSql & " AND (x1.flg_invisivel IS NULL OR x1.flg_invisivel = 0) "
    sSql = sSql & " ORDER BY  dt_cri ASC, hr_cri ASC   "
    ControiCriterio = sSql
Exit Function
TrataErro:
End Function



Private Sub LimpaFgResumo()
    Dim i As Long
    FgResumo.rows = 3
    FgResumo.row = 2
    For i = 0 To FgResumo.Cols - 1
        FgResumo.Col = i
        Set FgResumo.CellPicture = Nothing
        FgResumo.CellBackColor = vbWhite
        FgResumo.TextMatrix(2, i) = ""
    Next
End Sub

Private Sub LimpaFgRes()
    Dim i As Long
    FgRes.rows = 2
    FgRes.row = 1
    For i = 0 To FgRes.Cols - 1
        FgRes.Col = i
        Set FgRes.CellPicture = Nothing
        FgRes.CellBackColor = vbWhite
        FgRes.TextMatrix(1, i) = ""
        Set FgRes.CellPicture = Nothing
    Next
End Sub

Private Sub PreencheFgResumo()
    Dim iApar As Integer
    Dim iLote As Integer
    Dim iAna As Integer
    Dim i As Integer
    Dim j As Integer
    FgResumo.Visible = False
    i = 0
    For j = 1 To UBound(AnaOrdenada)
        For iApar = 1 To totalEstrut
            For iLote = 1 To Estrutura(iApar).totalLote
                For iAna = 1 To Estrutura(iApar).estrutLote(iLote).totalAna
                    If AnaOrdenada(j) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cod_ana Then
                        i = i + 1
                        FgResumo.row = i + 1
                        FgResumo.TextMatrix(i + 1, lColResumoParam) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).descr_ana
                        FgResumo.TextMatrix(i + 1, lColResumoLote) = Estrutura(iApar).estrutLote(iLote).descr_lote
                        FgResumo.TextMatrix(i + 1, lColResumoNivel) = Estrutura(iApar).estrutLote(iLote).nivel
                        FgResumo.TextMatrix(i + 1, lColResumoNumTestes) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).num_Amostras
                        FgResumo.Col = lColResumoNumTestes
                        FgResumo.CellBackColor = cLightYellow
                        FgResumo.TextMatrix(i + 1, lColResumoMedia) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_obt
                        FgResumo.Col = lColResumoMedia
                        FgResumo.CellBackColor = cLightYellow
                        FgResumo.TextMatrix(i + 1, lColResumoDesvioPadrao) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_obt
                        FgResumo.Col = lColResumoDesvioPadrao
                        FgResumo.CellBackColor = cLightYellow
                        FgResumo.TextMatrix(i + 1, lColResumoCv) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cv
                        FgResumo.Col = lColResumoCv
                        FgResumo.CellBackColor = cOrange
                        FgResumo.TextMatrix(i + 1, lColResumoBias) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).bias
                        FgResumo.Col = lColResumoBias
                        FgResumo.CellBackColor = cOrange
                        FgResumo.TextMatrix(i + 1, lColResumoETC) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).etc
                        FgResumo.Col = lColResumoETC
                        FgResumo.CellBackColor = cOrange
                        FgResumo.TextMatrix(i + 1, lColResumoID) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).id
                        FgResumo.Col = lColResumoID
                        FgResumo.CellBackColor = cOrange
                        FgResumo.TextMatrix(i + 1, lColResumoMediaAlvo) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_alvo
                        FgResumo.Col = lColResumoMediaAlvo
                        FgResumo.CellBackColor = cLightGreen
                        FgResumo.TextMatrix(i + 1, lColResumoDesvioPadraoAlvo) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_alvo
                        FgResumo.Col = lColResumoDesvioPadraoAlvo
                        FgResumo.CellBackColor = cLightGreen
                        FgResumo.TextMatrix(i + 1, lColResumoDesvioPadraoPercentAlvo) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_obt_perc
                        FgResumo.Col = lColResumoDesvioPadraoPercentAlvo
                        FgResumo.CellBackColor = cLightGreen
                        FgResumo.TextMatrix(i + 1, lColResumoImpr) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).imprecisao
                        FgResumo.Col = lColResumoImpr
                        FgResumo.CellBackColor = cLightBlue
                        FgResumo.TextMatrix(i + 1, lColResumoInex) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).inexatidao
                        FgResumo.Col = lColResumoInex
                        FgResumo.CellBackColor = cLightBlue
                        FgResumo.TextMatrix(i + 1, lColResumoEta) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).eta
                        FgResumo.Col = lColResumoEta
                        FgResumo.CellBackColor = cLightBlue
                        FgResumo.AddItem ""
                    End If
                Next
            Next
        Next
    Next
    'FgResumo.RemoveItem totalEstrut + 2
    FgResumo.Visible = True
End Sub

Private Sub PreencheFGRes()
    Dim iApar As Integer
    Dim iLote As Integer
    Dim iAna As Integer
    Dim iRes As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    FgRes.Visible = False
    LimpaFgRes
    i = 1
    For j = totalOrderData To 1 Step -1
        iApar = EstrutOrderData(j).iApar
        iAna = EstrutOrderData(j).iAna
        iLote = EstrutOrderData(j).iLote
        iRes = EstrutOrderData(j).iRes
        If MostraFgRes(iApar, iLote, iAna, iRes) = True Then
            FgRes.row = i
            FgRes.TextMatrix(i, lColResiApar) = iApar
            FgRes.TextMatrix(i, lColResiLote) = iLote
            FgRes.TextMatrix(i, lColResiAna) = iAna
            FgRes.TextMatrix(i, lColResiRes) = iRes
            
            FgRes.TextMatrix(i, lColResLote) = Estrutura(iApar).estrutLote(iLote).cod_lote
            FgRes.TextMatrix(i, lColResAna) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).descr_ana
            FgRes.TextMatrix(i, lColResAparelho) = Estrutura(iApar).descr_apar
            FgRes.TextMatrix(i, lColResResultado) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).resultado
            FgRes.TextMatrix(i, lColResData) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).dt_cri & " " & Mid(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).hr_cri, 1, 6)
            FgRes.Col = lColResActivo
            If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 Then
                Set FgRes.CellPicture = PicOk.Image
            Else
                Set FgRes.CellPicture = PicKo.Image
            End If
            FgRes.Col = lColResEstado
            If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
                Set FgRes.CellPicture = PicOk.Image
            ElseIf Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 1 Then
                Set FgRes.CellPicture = PicKo.Image
            Else
                Set FgRes.CellPicture = Nothing
            End If
            
            FgRes.TextMatrix(i, lColResMediaO) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO
            FgRes.TextMatrix(i, lColResMediaAc) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).media
            FgRes.TextMatrix(i, lColResDesvioAc) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).desvio
            FgRes.TextMatrix(i, lColResSigmaAc) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).sigma
            FgRes.TextMatrix(i, lColResNivel) = Estrutura(iApar).estrutLote(iLote).nivel
            FgRes.TextMatrix(i, lColResRegras) = ""
            For k = 1 To totalRegras
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estrutRegras(k).flg_estado = 1 Then
                    FgRes.TextMatrix(i, lColResRegras) = FgRes.TextMatrix(i, lColResRegras) & estrutRegras(k).abrev_regra & "; "
                End If
            Next
            FgRes.row = i
            FgRes.Col = lColResObs
            If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).obs <> "" Or Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).accao <> "" Then
                Set FgRes.CellPicture = PicObs.Image
            Else
                Set FgRes.CellPicture = Nothing
            End If
            FgRes.Col = lColResResultado
            FgRes.CellBackColor = e_LightGray
            FgRes.CellForeColor = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor
            FgRes.Col = lColResMediaAc
            FgRes.CellBackColor = e_LightGray
            FgRes.CellForeColor = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor
            FgRes.Col = lColResDesvioAc
            FgRes.CellBackColor = e_LightGray
            FgRes.CellForeColor = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor
            FgRes.Col = lColResMediaO
            FgRes.CellBackColor = e_LightGray
            FgRes.CellForeColor = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor
            FgRes.Col = lColResSigmaAc
            FgRes.CellBackColor = e_LightGray
            FgRes.CellForeColor = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor
            
            FgRes.Col = lColResMenos3
            FgRes.CellBackColor = e_Red
            Set FgRes.CellPicture = Nothing
            If gCQUsaMediaObtida = mediSim Then
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO <= -3 Then
                    Set FgRes.CellPicture = PicDotDR.Image
                End If
            Else
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA <= -3 Then
                    Set FgRes.CellPicture = PicDotDR.Image
                End If
            End If
            
            FgRes.Col = lColRes3
            FgRes.CellBackColor = e_Red
            Set FgRes.CellPicture = Nothing
            If gCQUsaMediaObtida = mediSim Then
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO >= 3 Then
                    Set FgRes.CellPicture = PicDotDR.Image
                End If
            Else
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA >= 3 Then
                    Set FgRes.CellPicture = PicDotDR.Image
                End If
            End If
            
            FgRes.Col = lColResMenos2
            FgRes.CellBackColor = e_LightRed
            Set FgRes.CellPicture = Nothing
            If gCQUsaMediaObtida = mediSim Then
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO <= -2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO > -3 Then
                    Set FgRes.CellPicture = PicDotR.Image
                End If
            Else
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA <= -2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA > -3 Then
                    Set FgRes.CellPicture = PicDotR.Image
                End If
            End If
            
            FgRes.Col = lColRes2
            FgRes.CellBackColor = e_LightRed
            If gCQUsaMediaObtida = mediSim Then
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO >= 2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO < 3 Then
                    Set FgRes.CellPicture = PicDotR.Image
                End If
            Else
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA >= 2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA < 3 Then
                    Set FgRes.CellPicture = PicDotR.Image
                End If
            End If
            
            FgRes.Col = lColResMenos1
            FgRes.CellBackColor = e_Yellow
            If gCQUsaMediaObtida = mediSim Then
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO <= -1 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO > -2 Then
                    Set FgRes.CellPicture = PicDotY.Image
                End If
            Else
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA <= -1 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA > -2 Then
                    Set FgRes.CellPicture = PicDotY.Image
                End If
            End If
            
            FgRes.Col = lColRes1
            FgRes.CellBackColor = e_Yellow
            If gCQUsaMediaObtida = mediSim Then
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO >= 1 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO < 2 Then
                    Set FgRes.CellPicture = PicDotY.Image
                End If
            Else
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA >= 1 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA < 2 Then
                    Set FgRes.CellPicture = PicDotY.Image
                End If
            End If
            
            FgRes.Col = lColRes0
            FgRes.CellBackColor = e_LightGreen
            If gCQUsaMediaObtida = mediSim Then
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO > -1 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO < 1 Then
                    Set FgRes.CellPicture = PicDotG.Image
                End If
            Else
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA > -1 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA < 1 Then
                    Set FgRes.CellPicture = PicDotG.Image
                End If
            End If
            FgRes.AddItem ""
            i = i + 1
        End If
    Next
    FgRes.Visible = True
End Sub
Private Sub BtPesquisaApar_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "seq_apar"
    CamposEcran(1) = "seq_apar"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_apar"
    CamposEcran(2) = "descr_apar"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "gc_apar"
    CampoPesquisa1 = "descr_apar"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Aparelhos")
    
    mensagem = "N�o foi encontrada nenhum Aparelho."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcSeqApar.Text = resultados(1)
            EcDescrApar.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


Private Sub BtPesquisaAna_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_ana_s"
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_ana_s"
    CampoPesquisa1 = "descr_ana_s"
    ClausulaWhere = " cod_ana_s = cod_ana_S "
    If EcCodLote <> "" Then
        ClausulaWhere = ClausulaWhere & " AND cod_ana_s in (Select sl_cq_ana.cod_ana_s FROM sl_cq_ana where sl_cq_ana.cod_lote = " & BL_TrataStringParaBD(EcCodLote) & ")"
    End If
    If EcSeqApar <> "" Then
        ClausulaWhere = ClausulaWhere & " AND cod_ana_s in (Select gc_ana_apar.cod_simpl FROM gc_ana_apar where seq_apar = " & (EcSeqApar) & ")"
    End If
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " An�lises")
    
    mensagem = "N�o foi encontrada nenhuma An�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAna.Text = resultados(1)
            EcDescrAna.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub



Private Sub BtPesquisaControlo_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_controlo"
    CamposEcran(1) = "cod_controlo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_controlo"
    CamposEcran(2) = "descr_controlo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_cq_controlo"
    CampoPesquisa1 = "descr_controlo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Controlo")
    
    mensagem = "N�o foi encontrada nenhum Controlo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodControlo.Text = resultados(1)
            EcDescrControlo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub



Private Sub BtPesquisaLote_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_lote"
    CamposEcran(1) = "cod_lote"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_lote"
    CamposEcran(2) = "descr_lote"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_cq_lote"
    CampoPesquisa1 = "descr_lote"
    If EcCodControlo <> "" Then
        ClausulaWhere = " cod_controlo = " & BL_TrataStringParaBD(EcCodControlo)
    End If
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " lote")
    
    mensagem = "N�o foi encontrada nenhum lote."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodLote.Text = resultados(1)
            EcDescrLote.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesquisaLote2_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_lote"
    CamposEcran(1) = "cod_lote"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_lote"
    CamposEcran(2) = "descr_lote"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_cq_lote"
    CampoPesquisa1 = "descr_lote"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " lote")
    
    mensagem = "N�o foi encontrada nenhum lote."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodLote2.Text = resultados(1)
            EcDescrLote2.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    PreencheAnaApar
    PreencheAnaAparMarcadas

End Sub

Private Function CalculaMediaDesvioNumAmostras(iApar As Integer, iLote As Integer, iAna As Integer) As Boolean
    Dim i As Integer
    Dim soma As Double
    Dim aux1 As Double
    On Error GoTo TrataErro
    
    soma = 0
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).num_Amostras = 0
    For i = 1 To Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).totalResultado
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).estado = 0 Then
            soma = soma + Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).resultado
            Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).num_Amostras = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).num_Amostras + 1
        End If
    Next
    If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).num_Amostras > 0 Then
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_obt = Round(soma / Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).num_Amostras, 2)
    End If
    'DESVIO PADRAO
    aux1 = 0
    For i = 1 To Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).totalResultado
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).estado = 0 Then
            aux1 = aux1 + ((Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).resultado - Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_obt) * (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).resultado - Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_obt))
        End If
    Next
    If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).num_Amostras > 1 Then
        aux1 = aux1 / (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).num_Amostras - 1)
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_obt = Round(Sqr(aux1), 2)
    Else
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_obt = 0
    End If
    CalculaMediaDesvioNumAmostras = True
Exit Function
TrataErro:
    CalculaMediaDesvioNumAmostras = False
    BG_LogFile_Erros "ERRO ao calcular media e desvio: " & Err.Number & " - " & Err.Description, Me.Name, "CalculaMediaDesvioNumAmostras", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' DADO DESVIO PADRAO E MEDIA CALCULA COEFICIENTE DE VARIACAO
' SD * 100 / Media
' ------------------------------------------------------------------------------
Private Function CalculaCV(iApar As Integer, iLote As Integer, iAna As Integer) As Boolean
    On Error GoTo TrataErro
    If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_obt = 0 Then
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cv = 0
    Else
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cv = Round(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_obt * 100 / Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_obt, 2)
    End If
    CalculaCV = True
Exit Function
TrataErro:
    CalculaCV = False
    BG_LogFile_Erros "ERRO ao CalculaCV : " & Err.Number & " - " & Err.Description, Me.Name, "CalculaValores", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' DADO DESVIO PADRAO E MEDIA CALCULA COEFICIENTE DE VARIACAO
' SD * 100 / Media
' ------------------------------------------------------------------------------
Private Function CalculaCVAcumulado(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer) As Boolean
    On Error GoTo TrataErro
    If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).media = 0 Then
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cv = 0
    Else
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cv = Round(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).desvio * 100 / Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).media, 2)
    End If
    CalculaCVAcumulado = True
Exit Function
TrataErro:
    CalculaCVAcumulado = False
    BG_LogFile_Erros "ERRO ao CalculaCVAcumulado : " & Err.Number & " - " & Err.Description, Me.Name, "CalculaCVAcumulado", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' ERRO SISTEMATICO
' ((Media - Media Alvo)/Media Alvo) * 100
' ------------------------------------------------------------------------------

Private Function CalculaBias(iApar As Integer, iLote As Integer, iAna As Integer) As Boolean
    On Error GoTo TrataErro
    If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_alvo = 0 Then
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cv = 0
    Else
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).bias = Round(((Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_obt - Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_alvo) / Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_alvo) * 100, 2)
    End If
    CalculaBias = True
Exit Function
TrataErro:
    CalculaBias = False
    BG_LogFile_Erros "ERRO ao CalculaBias : " & Err.Number & " - " & Err.Description, Me.Name, "CalculaBias", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' ERRO SISTEMATICO
' ((Media - Media Alvo)/Media Alvo) * 100
' ------------------------------------------------------------------------------

Private Function CalculaBiasAcumulado(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer) As Boolean
    On Error GoTo TrataErro
    If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).media = 0 Then
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).bias = 0
    Else
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).bias = Round(((Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).media - Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_alvo) / Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).media) * 100, 2)
    End If
    CalculaBiasAcumulado = True
Exit Function
TrataErro:
    CalculaBiasAcumulado = False
    BG_LogFile_Erros "ERRO ao CalculaBiasAcumulado : " & Err.Number & " - " & Err.Description, Me.Name, "CalculaBiasAcumulado", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' SIGMA
' (ETa% - abs(BIAS%))/ CV%
' ------------------------------------------------------------------------------

Private Function CalculaSigmaAcumulado(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer) As Boolean
    On Error GoTo TrataErro
    If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cv = 0 Then
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).sigma = 0
    Else
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).sigma = Round((Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).eta - Abs(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).bias)) / Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cv, 2)
    End If
    CalculaSigmaAcumulado = True
Exit Function
TrataErro:
    CalculaSigmaAcumulado = False
    BG_LogFile_Erros "ERRO ao CalculaSigmaAcumulado : " & Err.Number & " - " & Err.Description, Me.Name, "CalculaSigmaAcumulado", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' ERRO TOTAL
' Abs(BIAS%) + (1.65 * CV%)
' ------------------------------------------------------------------------------

Private Function CalculaETC(iApar As Integer, iLote As Integer, iAna As Integer) As Boolean
    On Error GoTo TrataErro
    If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_alvo = 0 Then
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).etc = 0
    Else
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).etc = Round(Abs(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).bias) + (1.65 * Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cv), 2)
    End If
    CalculaETC = True
Exit Function
TrataErro:
    CalculaETC = False
    BG_LogFile_Erros "ERRO ao CalculaETC : " & Err.Number & " - " & Err.Description, Me.Name, "CalculaETC", True
    Exit Function
    Resume Next
End Function



' ------------------------------------------------------------------------------

' Indice de desvio
' (Abs(Media - Media Alvo)/ desvioPadraoAlvo
' ------------------------------------------------------------------------------
Private Function CalculoID(iApar As Integer, iLote As Integer, iAna As Integer) As Boolean
    On Error GoTo TrataErro
    If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_alvo = 0 Then
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).id = 0
    Else
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).id = Round(Abs(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_obt - Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_alvo) / Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_alvo, 2)
    End If
    CalculoID = True
Exit Function
TrataErro:
    CalculoID = False
    BG_LogFile_Erros "ERRO ao calcular ID: " & Err.Number & " - " & Err.Description, Me.Name, "CalculoID", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' IMPRECISAO
' 0.25 * eta
' ------------------------------------------------------------------------------
Private Function CalculoIMP(iApar As Integer, iLote As Integer, iAna As Integer) As Boolean
    On Error GoTo TrataErro
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).imprecisao = Round(0.25 * Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).eta, 2)
    CalculoIMP = True
Exit Function
TrataErro:
    CalculoIMP = False
    BG_LogFile_Erros "ERRO ao CalculoIMP IMP: " & Err.Number & " - " & Err.Description, Me.Name, "CalculoIMP", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' inexatidao
' 0.5 * eta
' ------------------------------------------------------------------------------
Private Function CalculoINE(iApar As Integer, iLote As Integer, iAna As Integer) As Boolean
    On Error GoTo TrataErro
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).inexatidao = Round(0.5 * Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).eta, 2)
    CalculoINE = True
Exit Function
TrataErro:
    CalculoINE = False
    BG_LogFile_Erros "ERRO ao calcular INE: " & Err.Number & " - " & Err.Description, Me.Name, "CalculoINE", True
    Exit Function
    Resume Next
End Function


Private Function CalculoPercentSD(iApar As Integer, iLote As Integer, iAna As Integer) As Boolean
    On Error GoTo TrataErro
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_obt_perc = Round(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).eta * 0.33, 2)
    CalculoPercentSD = False
Exit Function
TrataErro:
    CalculoPercentSD = False
    BG_LogFile_Erros "ERRO ao calcular SD%: " & Err.Number & " - " & Err.Description, Me.Name, "CalculoPercentSD", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' VERIFICA PARA CADA VALOR QUAL A RELA��O DE DESVIO PARA A MEDIA OBTIDA E ALVO

' ------------------------------------------------------------------------------
Private Sub PreencheAvaliacoes()
    Dim iApar As Integer
    Dim iLote As Integer
    Dim iAna As Integer
    Dim iRes As Integer
    Dim aux As Double
    For iApar = 1 To totalEstrut
        For iLote = 1 To Estrutura(iApar).totalLote
            For iAna = 1 To Estrutura(iApar).estrutLote(iLote).totalAna
                For iRes = 1 To Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).totalResultado
                    If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_alvo <> 0 Then
                        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA = (Round((Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).resultado - Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_alvo) / Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_alvo, 2))
                    Else
                        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA = -3
                    End If
                    If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_obt <> 0 Then
                        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO = (Round((Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).resultado - Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_obt) / Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_obt, 2))
                    Else
                        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO = -3
                    End If
                    If gCQUsaMediaObtida = mediSim Then
                        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO <= -3 Then
                            Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor = e_Red
                        ElseIf Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO >= 3 Then
                            Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor = e_Red
                        ElseIf Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO <= -2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO > -3 Then
                            Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor = e_Red
                        ElseIf Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO >= 2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO < 3 Then
                            Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor = e_Red
                        Else
                            Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor = vbBlack
                        End If
                    Else
                        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA <= -3 Then
                            Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor = e_Red
                        ElseIf Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA >= 3 Then
                            Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor = e_Red
                        ElseIf Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA <= -2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA > -3 Then
                            Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor = e_Red
                        ElseIf Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA >= 2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA < 3 Then
                            Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor = e_Red
                        Else
                            Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).cor = vbBlack
                        End If
                    End If
                    AvaliaRegras iApar, iLote, iAna, iRes
                Next
            Next
        Next
    Next
    
End Sub

' ------------------------------------------------------------------------------

' GRAVA OBSERVA��O NA BD

' ------------------------------------------------------------------------------
Private Function GravaObs(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer, linha As Integer) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim res As Integer
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).obs = Trim(EcObs)
    Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).accao = Trim(EcAccao)
    
    sSql = "UPDATE sl_cq_res SET obs = " & BL_TrataStringParaBD(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).obs)
    sSql = sSql & ", accao = " & BL_TrataStringParaBD(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).accao)
    sSql = sSql & " WHERE seq_resultado = " & Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).seq_resultado
    res = BG_ExecutaQuery_ADO(sSql)
    If res <= 0 Then
        GoTo TrataErro
    End If
    RegistaAlteracao Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).seq_resultado, _
                     Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido, _
                     Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado, _
                     Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).obs, _
                     Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).accao
    
    GravaObs = True
Exit Function
TrataErro:
    GravaObs = False
    BG_LogFile_Erros "ERRO ao Gravar Observa��o: " & sSql & " " & Err.Number & " - " & Err.Description, Me.Name, "GravaObs", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' GRAVA ESTADO NA BD

' ------------------------------------------------------------------------------
Private Function GravaEstado(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer, linha As Integer) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim res As Integer
    
    sSql = "UPDATE sl_cq_res SET estado = " & Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado
    sSql = sSql & " WHERE seq_resultado = " & Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).seq_resultado
    res = BG_ExecutaQuery_ADO(sSql)
    If res <= 0 Then
        GoTo TrataErro
    End If
    GravaEstado = True
Exit Function
TrataErro:
    GravaEstado = False
    BG_LogFile_Erros "ERRO ao Gravar Estado: " & sSql & " " & Err.Number & " - " & Err.Description, Me.Name, "GravaEstado", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' GRAVA EXCLUSAO NA BD

' ------------------------------------------------------------------------------
Private Function GravaExclusao(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer, linha As Integer) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim res As Integer
    
    sSql = "UPDATE sl_cq_res SET flg_excluido = " & Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido
    sSql = sSql & " WHERE seq_resultado = " & Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).seq_resultado
    res = BG_ExecutaQuery_ADO(sSql)
    If res <= 0 Then
        GoTo TrataErro
    End If
    GravaExclusao = True
Exit Function
TrataErro:
    GravaExclusao = False
    BG_LogFile_Erros "ERRO ao Gravar Exclus�o: " & sSql & " " & Err.Number & " - " & Err.Description, Me.Name, "GravaExclusao", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' ALTERA O ESTADO DE CADA CONTROLO

' ------------------------------------------------------------------------------
Public Sub AlteraOpcao(Opcao As Integer)
    Dim iApar As Integer
    Dim iLote As Integer
    Dim iAna As Integer
    Dim iRes As Integer
    
    iApar = FgRes.TextMatrix(FgRes.row, lColResiApar)
    iLote = FgRes.TextMatrix(FgRes.row, lColResiLote)
    iAna = FgRes.TextMatrix(FgRes.row, lColResiAna)
    iRes = FgRes.TextMatrix(FgRes.row, lColResiRes)
    If Opcao = 0 Then
        'excluido
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 1
        FgRes.Col = lColResEstado
        Set FgRes.CellPicture = PicKo.Image
        GravaEstado iApar, iLote, iAna, iRes, FgRes.row
    ElseIf Opcao = 1 Then
        'aceite
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0
        FgRes.Col = lColResEstado
        Set FgRes.CellPicture = PicOk.Image
        GravaEstado iApar, iLote, iAna, iRes, FgRes.row
    ElseIf Opcao = 2 Then
        'excluido
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 1
        GravaExclusao iApar, iLote, iAna, iRes, FgRes.row
        FgRes.Col = lColResActivo
        Set FgRes.CellPicture = PicKo.Image
    ElseIf Opcao = 3 Then
        ' nao excluido
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0
        GravaExclusao iApar, iLote, iAna, iRes, FgRes.row
        FgRes.Col = lColResActivo
        Set FgRes.CellPicture = PicOk.Image
    End If
    RegistaAlteracao Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).seq_resultado, _
                     Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido, _
                     Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado, _
                     Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).obs, _
                     Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).accao
    PreencheFormulas
    PreencheFgResumo
    PreencheAvaliacoes
    PreencheFGRes
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao AlteraEstado: " & " " & Err.Number & " - " & Err.Description, Me.Name, "AlteraEstado", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheListaGraf()
    Dim iApar As Integer
    Dim iLote As Integer
    Dim iAna As Integer
    Dim j As Integer
    On Error GoTo TrataErro
    EcListaGraf.Clear
    totalLista = 0
    ReDim estrutLista(0)
    
    For j = 1 To UBound(AnaOrdenada)
        For iApar = 1 To totalEstrut
            For iLote = 1 To Estrutura(iApar).totalLote
                For iAna = 1 To Estrutura(iApar).estrutLote(iLote).totalAna
                    If AnaOrdenada(j) = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cod_ana Then
                        totalLista = totalLista + 1
                        ReDim Preserve estrutLista(totalLista)
                        estrutLista(totalLista).iAna = iAna
                        estrutLista(totalLista).iApar = iApar
                        estrutLista(totalLista).iLote = iLote
                        EcListaGraf.AddItem Estrutura(iApar).estrutLote(iLote).descr_controlo & " (" & Estrutura(iApar).descr_apar & ") - " & Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).descr_ana
                        EcListaGraf.ItemData(EcListaGraf.NewIndex) = totalLista
                    End If
                Next
            Next
        Next
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao PreencheListaGraf: " & " " & Err.Number & " - " & Err.Description, Me.Name, "PreencheListaGraf", True
    Exit Sub
    Resume Next
End Sub

Private Sub DefTipoGrafico()
    On Error GoTo TrataErro
    
    With Grafico
        With .Plot
            '.AutoLayout = True
            
            With .Backdrop
                With .Frame
                    .Style = VtFrameStyleSingleLine
                    '.FrameColor.Set 0, 0, 0
                    .Width = 20
                End With
                With .Fill
                    .Style = VtFillStyleBrush
                    '.Brush.FillColor.Set 255, 255, 255
                    .Brush.FillColor.Set 245, 255, 216
                    .Brush.Style = VtBrushStyleSolid
                End With
                With .Shadow
                    .Style = VtShadowStyleNull
                    .Brush.Style = VtBrushStylePattern
                    .Offset.Set 50, 50
                    '.Brush.FillColor.Set 120, 120, 120
                End With
            End With
            
            'Para os gr�fico tipo PIE:
            'Sentido em que o gr�fico � desenhado relativamente aos ponteiros do rel�gio
            .Clockwise = True
            .AngleUnit = VtAngleUnitsDegrees
            .Sort = VtSortTypeAscending
            'Posi��o das Labels
            .SubPlotLabelPosition = VtChSubPlotLabelLocationTypeAbove
            
            'PARA OS GR�FICOS 3D:
            'Posi��o inicial dos gr�ficos (CTRL)
            'Lado
            .Projection = VtProjectionTypePerspective
            'Rota��o
            .View3d.Elevation = 10
            
            'Ilumina��o
            With .Light
                '.AmbientIntensity = 0.4
                '.EdgeIntensity = 0.5
                '.EdgeVisible = True
            End With
            
            With .Wall
                
                '.Brush.FillColor.Set 0, 0, 0
                .Brush.Style = VtBrushStyleNull
                '.Brush.PatternColor.Set 255, 255, 255
                'Tipo de linha e cor para desenho da Wall
                With .Pen
                    'Como termina nos cantos
                    .Cap = VtPenCapRound
                    .Join = VtPenJoinRound
                    .Limit = 20
                    .Style = VtPenStyleNull
                    .Width = 24
                    .VtColor.Set 255, 255, 255
                End With
            End With
            
            'Parte do Baixo do Quadro
            With .PlotBase
                
                .Brush.Style = VtBrushStyleSolid
                '.Brush.FillColor.Set 0, 0, 255
                .BaseHeight = 100
            End With
        
        End With
                 
        
        With .Backdrop
            
            With .Frame
                .Style = VtFrameStyleDoubleLine
                .Width = 20
                .SpaceColor.Set 255, 255, 255
            End With
            With .Shadow
                .Style = VtShadowStyleNull
            End With
            With .Fill
                .Style = VtFillStyleBrush
                
            End With
            
        End With
        
        .chartType = VtChChartType2dLine
        
        .ShowLegend = False
              
        'Em gr�ficos de 3D carregando na tecla CTRL roda o gr�fico
        .AllowDynamicRotation = True
    
        'Permite o utilizador seleccionar os objectos do gr�fico
        .AllowSelections = True
    
        'Palete de cores do gr�fico (ajuste dos valores)
        .AllowDithering = False
        
        'Click na Data do Chart n�o selecciona apenas o ponto mas sim toda a S�rie associada
        .AllowSeriesSelection = True
    
        'Sobreposi��o de todas as s�ries
        .Stacking = False
        
    End With
    Grafico.Visible = False
    FrameGraf1.Visible = False
    FrameGraf2.Visible = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao DefTipoGrafico: " & " " & Err.Number & " - " & Err.Description, Me.Name, "DefTipoGrafico", False
    Exit Sub
    Resume Next
End Sub


Private Sub DesenhaGrafico()
    On Error GoTo TrataErro
    Dim iApar As Integer
    Dim iLote As Integer
    Dim iAna As Integer
    Dim iRes As Integer
    Dim j As Integer
    Dim k As Integer
    Dim linhas As Integer
    Dim i As Integer
    Dim mediaT As Double
    Dim desvioT As Double
    Dim primeiraSerie As Integer
    Dim indice As Integer
    Dim TotalLinhas As Integer
    Dim maxNumReg As Integer
    linhas = 1
    PreencheCores
    multiGraf = False
    indice = RetornaIndiceGrafico(TotalLinhas, maxNumReg, mediaT, desvioT)
    If TotalLinhas = 0 Then Exit Sub
    If TotalLinhas > 1 Then
        multiGraf = True
    ElseIf TotalLinhas = 1 Then
        multiGraf = False
    End If
    iApar = estrutLista(indice).iApar
    iLote = estrutLista(indice).iLote
    iAna = estrutLista(indice).iAna
        

    With Grafico
        
        'DADOS:
        .RandomFill = False
        .ColumnCount = 0
        'N� de linhas e colunas da Matriz
        If OptMedia(0).value = True And multiGraf = False Then
            .ColumnCount = TotalLinhas + 1
            primeiraSerie = 2
            .Plot.SeriesCollection.item(1).Pen.VtColor.Set 0, 100, 0
        Else
            .ColumnCount = TotalLinhas
            primeiraSerie = 1
        End If
        .RowCount = maxNumReg
        'Quando se atribui valores ao gr�fico (.data) incrementa a linha na coluna activa
        'ou a coluna se a linha da coluna for a �ltima
        .AutoIncrement = False
        
         
        With .Plot
            
            .Axis(VtChAxisIdY).ValueScale.Auto = False
            .Axis(VtChAxisIdY).ValueScale.Minimum = mediaT - (4 * desvioT)
            .Axis(VtChAxisIdY).ValueScale.Maximum = mediaT + (4 * desvioT)
            .Axis(VtChAxisIdY).ValueScale.MajorDivision = 8
            
            .Axis(VtChAxisIdY).AxisGrid.MajorPen.Style = VtPenStyleNull
            .Axis(VtChAxisIdY).AxisGrid.MajorPen.Width = 1
            .Backdrop.Fill.Style = VtFillStyleNull
            
            
            If multiGraf = True Then
                .Axis(VtChAxisIdY).Labels(1).VtFont.VtColor.Set 255, 255, 255
                FrameGraf2.Visible = True
            Else
                .Axis(VtChAxisIdY).Labels(1).VtFont.VtColor.Set 0, 0, 0
                FrameGraf2.Visible = False
            End If

            '.Axis(VtChAxisIdY2).Labels(1).TextLayout
            .DataSeriesInRow = False
            
            PreencheLinhasGrafico primeiraSerie, Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).descr_ana
            
        End With
        PreencheDadosGrafico primeiraSerie
        If OptMedia(0).value = True And multiGraf = False Then
            DesenhaLinha 1, Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_alvo
        End If
        
        'LABELS (S� DEPOIS DA ATRIBUI��O=>1 n�vel de indenta��o por Label!)
        For i = 1 To .RowCount
            .Column = 1
            .row = i
            .RowLabelCount = 2
            .RowLabelIndex = 1
            .RowLabel = i
            .RowLabelIndex = 2
        Next i
        
        .Column = 1
        .ColumnLabelCount = 1
        .ColumnLabelIndex = 1
        '.ColumnLabel = FGAna.TextMatrix(linha, 0)
        
            
    End With
    Grafico.Visible = True
    FrameGraf1.Visible = True
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao DesenhaGrafico: " & " " & Err.Number & " - " & Err.Description, Me.Name, "DesenhaGrafico", False
    Exit Sub
    Resume Next
End Sub

Private Sub DesenhaMediaDesvios()
    On Error GoTo TrataErro
    'M�dia
    With Grafico.Plot.SeriesCollection.item(1)
        .SeriesMarker.Show = False
        .SeriesMarker.Auto = False
        .ShowLine = True
        With .Pen
            .Style = VtPenStyleSolid
            .Cap = VtPenCapRound
            .Limit = 10
            .Join = VtPenJoinBevel
            .VtColor.Set 184, 184, 184
            .Width = 10
        End With
    End With
    '1 desvio padrao
    With Grafico.Plot.SeriesCollection.item(2)
        .SeriesMarker.Show = False
        .SeriesMarker.Auto = False
        .ShowLine = True
        With .Pen
            .Style = VtPenStyleSolid
            .Cap = VtPenCapRound
            .Limit = 10
            .Join = VtPenJoinBevel
            .VtColor.Set 173, 173, 173
            .Width = 10
        End With
    End With
    '2 desvio padrao
    With Grafico.Plot.SeriesCollection.item(3)
        .SeriesMarker.Show = False
        .SeriesMarker.Auto = False
        .ShowLine = True
        With .Pen
            .Style = VtPenStyleSolid
            .Cap = VtPenCapRound
            .Limit = 10
            .Join = VtPenJoinBevel
            .VtColor.Set 150, 150, 150
            .Width = 10
        End With
    End With
    '3 desvio padrao
    With Grafico.Plot.SeriesCollection.item(4)
        .SeriesMarker.Show = False
        .SeriesMarker.Auto = False
        .ShowLine = True
        With .Pen
            .Style = VtPenStyleSolid
            .Cap = VtPenCapRound
            .Limit = 10
            .Join = VtPenJoinBevel
            .VtColor.Set 125, 125, 125
            .Width = 10
        End With
    End With
    '4 desvio padrao
    With Grafico.Plot.SeriesCollection.item(5)
        .SeriesMarker.Show = False
        .SeriesMarker.Auto = False
        .ShowLine = True
        With .Pen
            .Style = VtPenStyleSolid
            .Cap = VtPenCapRound
            .Limit = 10
            .Join = VtPenJoinBevel
            .VtColor.Set 108, 123, 139
            .Width = 10
        End With
    End With
    '-1 desvio padrao
    With Grafico.Plot.SeriesCollection.item(6)
        .SeriesMarker.Show = False
        .SeriesMarker.Auto = False
        .ShowLine = True
        With .Pen
            .Style = VtPenStyleSolid
            .Cap = VtPenCapRound
            .Limit = 10
            .Join = VtPenJoinBevel
            .VtColor.Set 173, 173, 173
            .Width = 10
        End With
    End With
    '-2 desvio padrao
    With Grafico.Plot.SeriesCollection.item(8)
        .SeriesMarker.Show = False
        .SeriesMarker.Auto = False
        .ShowLine = True
        With .Pen
            .Style = VtPenStyleSolid
            .Cap = VtPenCapRound
            .Limit = 10
            .Join = VtPenJoinBevel
            .VtColor.Set 150, 150, 150
            .Width = 10
        End With
    End With
    '-3 desvio padrao
    With Grafico.Plot.SeriesCollection.item(9)
        .SeriesMarker.Show = False
        .SeriesMarker.Auto = False
        .ShowLine = True
        With .Pen
            .Style = VtPenStyleSolid
            .Cap = VtPenCapRound
            .Limit = 10
            .Join = VtPenJoinRound
            .VtColor.Set 125, 125, 125
            .Width = 10
        End With
    End With
    '-4 desvio padrao
    With Grafico.Plot.SeriesCollection.item(10)
        .SeriesMarker.Show = False
        .SeriesMarker.Auto = False
        .ShowLine = True
        With .Pen
            .Style = VtPenStyleSolid
            .Cap = VtPenCapRound
            .Limit = 10
            .Join = VtPenJoinBevel
            .VtColor.Set 108, 123, 139
            .Width = 10
        End With
    End With
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao DesenhaMediaDesvios: " & " " & Err.Number & " - " & Err.Description, Me.Name, "DesenhaMediaDesvios", False
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------

' DESENHA LINHAS DO GRAFICO

' ------------------------------------------------------------------------------
Private Sub DesenhaLinha(coluna As Integer, valor As Double)
    On Error GoTo TrataErro
    Dim j As Integer
    Grafico.Column = coluna
    For j = 1 To Grafico.RowCount
        
        Grafico.row = j
        Grafico.data = valor
    Next
     
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao DesenhaLinha: " & " " & Err.Number & " - " & Err.Description, Me.Name, "DesenhaLinha", False
    Exit Sub
    Resume Next
End Sub


Private Sub Grafico_PlotSelected(MouseFlags As Integer, Cancel As Integer)
    On Error GoTo TrataErro
    Dim i As Integer
    For i = 1 To Grafico.Plot.SeriesCollection.Count
        Grafico.Plot.SeriesCollection.item(i).DataPoints(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao Grafico_PlotSelected: " & " " & Err.Number & " - " & Err.Description, Me.Name, "Grafico_PlotSelected", False
    Exit Sub
    Resume Next
End Sub

Private Sub Grafico_SeriesSelected(series As Integer, MouseFlags As Integer, Cancel As Integer)
    Dim i As Integer
    For i = 1 To Grafico.Plot.SeriesCollection.Count
        Grafico.Plot.SeriesCollection.item(i).DataPoints(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
    Next
    If series = 1 And OptMedia(1).value = True And multiGraf = False Then
        Grafico.Plot.SeriesCollection.item(series).DataPoints(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
    Else
        Grafico.Plot.SeriesCollection.item(series).DataPoints(-1).DataPointLabel.LocationType = VtChLabelLocationTypeAbovePoint
    End If
    LbAnalise = ""
    For i = 0 To totalLista
        If series = estrutLista(i).indice Then
            LbAnalise = Estrutura(estrutLista(i).iApar).estrutLote(estrutLista(i).iLote).estrutAna(estrutLista(i).iAna).descr_ana
            
            Exit For
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao Grafico_SeriesSelected: " & " " & Err.Number & " - " & Err.Description, Me.Name, "Grafico_SeriesSelected", False
    Exit Sub
    Resume Next
End Sub

Private Sub OptMedia_Click(Index As Integer)
    BtRefresh_Click
End Sub


Private Function RetornaIndiceGrafico(ByRef TotalLinhas As Integer, maxNumReg As Integer, mediaT As Double, desvioT As Double) As Integer
    Dim i As Integer
    Dim k As Integer
    Dim iApar As Integer
    Dim iLote As Integer
    Dim iAna As Integer
    Dim iRes As Integer
    On Error GoTo TrataErro
    
    TotalLinhas = 0
    maxNumReg = 0
    For i = 0 To EcListaGraf.ListCount - 1
        If EcListaGraf.Selected(i) = True Then
            iApar = estrutLista(i + 1).iApar
            iLote = estrutLista(i + 1).iLote
            iAna = estrutLista(i + 1).iAna
            estrutLista(i + 1).indice = TotalLinhas + 1
            k = 0
            For iRes = 1 To Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).totalResultado
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 Then
                    k = k + 1
                End If
            Next
            If k > maxNumReg Then
                maxNumReg = k
            End If
            TotalLinhas = TotalLinhas + 1
            RetornaIndiceGrafico = i + 1
        End If
    Next

    If TotalLinhas > 1 Then
        mediaT = 0
        desvioT = 1
    ElseIf RetornaIndiceGrafico > 0 Then
        iApar = estrutLista(RetornaIndiceGrafico).iApar
        iLote = estrutLista(RetornaIndiceGrafico).iLote
        iAna = estrutLista(RetornaIndiceGrafico).iAna
        
        If OptMedia(0).value = True Then
            mediaT = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_obt
            desvioT = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_obt
        ElseIf OptMedia(0).value = False Then
            mediaT = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).media_alvo
            desvioT = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).desvio_alvo
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "ERRO ao RetornaIndiceGrafico: " & " " & Err.Number & " - " & Err.Description, Me.Name, "RetornaIndiceGrafico", False
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' FORMATA GRAFICO

' ------------------------------------------------------------------------------
Private Sub PreencheDadosGrafico(primeiraSerie As Integer)
    Dim linha As Integer
    Dim iApar As Integer
    Dim iLote As Integer
    Dim iAna As Integer
    Dim iRes As Integer
    Dim serie As Integer
    On Error GoTo TrataErro
    serie = primeiraSerie
    Dim i As Integer
    linha = 1
    Grafico.Column = primeiraSerie
    For i = 0 To EcListaGraf.ListCount - 1
        If EcListaGraf.Selected(i) = True Then
            Grafico.Column = serie
            iApar = estrutLista(i + 1).iApar
            iLote = estrutLista(i + 1).iLote
            iAna = estrutLista(i + 1).iAna
            linha = 1
            For iRes = 1 To Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).totalResultado
                If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 Then
                    Grafico.row = linha
                    
                    If multiGraf = True Then
                        If OptMedia(0).value = True Then
                            Grafico.data = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO
                        ElseIf OptMedia(1).value = True Then
                            Grafico.data = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA
                        End If
                    Else
                        Grafico.data = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).resultado
                    End If
                    
                    Grafico.Plot.SeriesCollection.item(serie).SeriesMarker.Show = True
                    Grafico.Plot.SeriesCollection.item(serie).SeriesMarker.Auto = False
                    Grafico.Plot.SeriesCollection.item(serie).DataPoints(-1).Marker.Visible = True
                    Grafico.Plot.SeriesCollection.item(serie).DataPoints(-1).Marker.Size = 180
                    Grafico.Plot.SeriesCollection.item(serie).DataPoints(-1).Marker.Pen.Width = 25
                    If primeiraSerie = 2 Then
                        Grafico.Plot.SeriesCollection.item(serie).DataPoints(-1).Marker.FillColor.Set estrutCores(serie - 1).r, estrutCores(serie - 1).g, estrutCores(serie - 1).b
                    Else
                        Grafico.Plot.SeriesCollection.item(serie).DataPoints(-1).Marker.FillColor.Set estrutCores(serie).r, estrutCores(serie).g, estrutCores(serie).b
                    End If
                    Grafico.Plot.SeriesCollection.item(serie).DataPoints(-1).Marker.Style = VtMarkerStyle3dBall
                    

                    Grafico.Plot.SeriesCollection.item(serie).DataPoints(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
                    
        
                    
                    linha = linha + 1
                End If
            Next iRes
            serie = serie + 1
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao PreencheDadosGrafico: " & " " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDadosGrafico", False
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------

' PREENCHE CORES DAS LINHAS

' ------------------------------------------------------------------------------
Private Sub PreencheCores()
    On Error GoTo TrataErro
    ReDim estrutCores(5)
    estrutCores(1).r = 30
    estrutCores(1).g = 144
    estrutCores(1).b = 255
    
    estrutCores(2).r = 255
    estrutCores(2).g = 64
    estrutCores(2).b = 64
    
    estrutCores(3).r = 255
    estrutCores(3).g = 12
    estrutCores(3).b = 36
    
    estrutCores(4).r = 255
    estrutCores(4).g = 236
    estrutCores(4).b = 139
    
    estrutCores(5).r = 219
    estrutCores(5).g = 219
    estrutCores(5).b = 112
    
 Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao PreencheCores: " & " " & Err.Number & " - " & Err.Description, Me.Name, "PreencheCores", False
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------

' PREENCHE AS LINHAS DO GRAFICO

' ------------------------------------------------------------------------------
Private Sub PreencheLinhasGrafico(primeiraSerie As Integer, LEGENDA As String)
    Dim i As Integer
    Dim escolhidos As Integer
    On Error GoTo TrataErro
    escolhidos = 0
    For i = 0 To EcListaGraf.ListCount - 1
        If EcListaGraf.Selected(i) = True Then
            With Grafico.Plot.SeriesCollection
                
                With .item(primeiraSerie + escolhidos)
                    
                    .LegendText = LEGENDA
                    
                    .SeriesMarker.Show = False
                    .SeriesMarker.Auto = False
                    
                    .ShowLine = True
                    With .Pen
                        
                        .Style = VtPenStyleNative
                        .Cap = VtPenCapButt
                        .Limit = 25
                        .Join = VtPenJoinMiter
                        .VtColor.Set estrutCores(escolhidos + 1).r, estrutCores(escolhidos + 1).g, estrutCores(escolhidos + 1).b
                        .Width = 25
                    End With
                    
                'DesenhaMediaDesvios
                End With
            End With
            escolhidos = escolhidos + 1
        End If
        
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao PreencheLinhasGrafico: " & " " & Err.Number & " - " & Err.Description, Me.Name, "PreencheLinhasGrafico", False
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------

' CALCULA FORMULAS

' ------------------------------------------------------------------------------
Private Sub PreencheFormulas()
    Dim iApar As Integer
    Dim iLote As Integer
    Dim iAna As Integer
    On Error GoTo TrataErro
    For iApar = 1 To totalEstrut
        For iLote = 1 To Estrutura(iApar).totalLote
            For iAna = 1 To Estrutura(iApar).estrutLote(iLote).totalAna
                CalculaMediaDesvioNumAmostras iApar, iLote, iAna
                CalculaCV iApar, iLote, iAna
                CalculaBias iApar, iLote, iAna
                CalculaETC iApar, iLote, iAna
                CalculoID iApar, iLote, iAna
                CalculoIMP iApar, iLote, iAna
                CalculoINE iApar, iLote, iAna
                CalculoPercentSD iApar, iLote, iAna
            Next
        Next
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao PReencheFormulas: " & " " & Err.Number & " - " & Err.Description, Me.Name, "PReencheFormulas", False
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------

' CALCULA FORMULAS ACUMULADAS - PARA CADA PONTO

' ------------------------------------------------------------------------------
Private Sub PreencheFormulasAcumuladas()
    Dim iApar As Integer
    Dim iLote As Integer
    Dim iAna As Integer
    Dim iRes As Integer
    On Error GoTo TrataErro
    For iApar = 1 To totalEstrut
        For iLote = 1 To Estrutura(iApar).totalLote
            For iAna = 1 To Estrutura(iApar).estrutLote(iLote).totalAna
                For iRes = 1 To Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).totalResultado
                    CalculaCVAcumulado iApar, iLote, iAna, iRes
                    CalculaBiasAcumulado iApar, iLote, iAna, iRes
                    CalculaSigmaAcumulado iApar, iLote, iAna, iRes
                Next
            Next
        Next
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao PReencheFormulas: " & " " & Err.Number & " - " & Err.Description, Me.Name, "PReencheFormulas", False
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------

' REGISTA ALTERACAO DO CONTROLO (OBSERVACAO, EXCLUIDO OU OBS)

' ------------------------------------------------------------------------------
Private Sub RegistaAlteracao(seq_resultado As Long, flg_excluido As Integer, estado As Integer, obs As String, accao As String)
    Dim seq_alteracao As Long
    Dim sSql As String
    Dim iReg As Integer
    On Error GoTo TrataErro
    
    seq_alteracao = BG_DaMAX("sl_cq_reg_alteracoes", "seq_alteracao") + 1
    sSql = "INSERT INTO sl_cq_reg_alteracoes(seq_alteracao, seq_resultado, flg_excluido, estado, obs,accao,user_cri, dt_cri, hr_Cri) VALUES("
    sSql = sSql & seq_alteracao & ", "
    sSql = sSql & seq_resultado & ", "
    sSql = sSql & BL_HandleNull(flg_excluido, "null") & ", "
    sSql = sSql & BL_HandleNull(estado, "null") & ", "
    sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(obs, "")) & ", "
    sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(accao, "")) & ", "
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
    sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ") "
    iReg = BG_ExecutaQuery_ADO(sSql)
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao Registar Alteracao: " & " " & Err.Number & " - " & Err.Description, Me.Name, "RegistaAlteracao", True
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------

' PREENCHE LISTA DAS ANALISES DO APARELHO

' ------------------------------------------------------------------------------
Private Sub PreencheAnaApar()
    Dim sSql As String
    On Error GoTo TrataErro
    totalAnaSelec = 0
    ReDim EstrutAnaSelec(0)
    EcAnalises.Clear
    EcAnalisesSel.Clear
    
    If EcSeqApar2 <> "" And EcCodLote2 <> "" And EcDescrApar2 <> "" And EcDescrLote2 <> "" Then
        sSql = "select distinct seq_ana, cod_simpl, slv_analises.descr_ana from gc_ana_apar, slv_analises  where cod_simpl = cod_ana "
        sSql = sSql & " AND seq_apar = " & EcSeqApar2
        BG_PreencheComboBD_ADO sSql, "seq_ana", "descr_ana", EcAnalises, mediAscComboDesignacao
    Else
        EcAnalises.Clear
        EcAnalisesSel.Clear
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao PreencheAnaApar: " & " " & Err.Number & " - " & Err.Description, Me.Name, "PreencheAnaApar", True
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------

' PREENCHE ANALISES MARCADAS PARA O APARELHO EM CAUSA

' ------------------------------------------------------------------------------
Private Sub PreencheAnaAparMarcadas()
    Dim sSql As String
    On Error GoTo TrataErro
    EcAnalisesMarcadas.Clear
    If EcSeqApar2 <> "" And EcCodLote2 <> "" Then
        sSql = "select distinct x2.seq_ana_s, x1.cod_ana_s, x2.descr_ana_s from sl_cq_listas x1, sl_ana_s x2 where x1.cod_ana_s = x2.cod_ana_S "
        sSql = sSql & " AND x1.seq_apar = " & EcSeqApar2 & " AND flg_apar_trans = 0"
        BG_PreencheComboBD_ADO sSql, "seq_ana_s", "descr_ana_s", EcAnalisesMarcadas, mediAscComboDesignacao
    Else
        EcAnalisesMarcadas.Clear
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao PreencheAnaAparMarcadas: " & " " & Err.Number & " - " & Err.Description, Me.Name, "PreencheAnaAparMarcadas", True
    Exit Sub
    Resume Next
End Sub

Private Sub BtInsere_Click()
    Dim aux As String
    Dim sSql As String
    Dim rsDados As New ADODB.recordset
    Dim j As Integer
    Dim i As Integer
    Dim indice As Integer
    Dim Existe As Boolean
    On Error GoTo TrataErro
    indice = 0
    
    If EcPrefixo = "" Then
        BG_Mensagem mediMsgBox, "Ter� que indicar um prefixo para enviar para aparelho", vbOKOnly + vbExclamation, App.ProductName
        Exit Sub
    End If
    
    For i = 0 To EcAnalises.ListCount - 1
        Existe = False
        If EcAnalises.Selected(i) = True Then
            For j = 1 To totalAnaSelec
                If EstrutAnaSelec(j).seq_apar = EcSeqApar2 Then
                    If EstrutAnaSelec(j).seq_ana_s = EcAnalises.ItemData(i) Then
                       Existe = True
                       Exit For
                    End If
                End If
            Next
            If Existe = False Then
                sSql = "SELECT cod_ana_s, descr_ana_s FROM sl_ana_s "
                sSql = sSql & " WHERE seq_ana_s = " & EcAnalises.ItemData(i)
                rsDados.CursorLocation = adUseServer
                rsDados.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsDados.Open sSql, gConexao
                If rsDados.RecordCount = 1 Then
                    If VerificaListaAna(EcSeqApar2, EcCodLote2, BL_HandleNull(rsDados!cod_ana_s, "")) = False Then
                        
                        totalAnaSelec = totalAnaSelec + 1
                        ReDim Preserve EstrutAnaSelec(totalAnaSelec)
                        EstrutAnaSelec(totalAnaSelec).seq_ana_s = EcAnalises.ItemData(i)
                        EstrutAnaSelec(totalAnaSelec).seq_apar = EcSeqApar2
                        EstrutAnaSelec(totalAnaSelec).cod_ana_s = BL_HandleNull(rsDados!cod_ana_s, "")
                        EstrutAnaSelec(totalAnaSelec).descr_ana_s = BL_HandleNull(rsDados!descr_ana_s, "")
                        EstrutAnaSelec(totalAnaSelec).cod_lote = EcCodLote2
                        EstrutAnaSelec(totalAnaSelec).prefixo = EcPrefixo
                        
                        EcAnalisesSel.AddItem "(" & EcPrefixo & ") " & EstrutAnaSelec(totalAnaSelec).descr_ana_s
                        EcAnalisesSel.ItemData(EcAnalisesSel.NewIndex) = indice
                        indice = indice + 1
                    Else
                        BG_Mensagem mediMsgBox, "An�lise " & BL_HandleNull(rsDados!descr_ana_s, "") & " j� marcada. ", vbOKOnly + vbExclamation, App.ProductName
                    End If
                End If
                rsDados.Close
            End If
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao BtInsere_Click: " & " " & Err.Number & " - " & Err.Description, Me.Name, "BtInsere_Click", True
    Exit Sub
    Resume Next
End Sub

Private Sub BtRetira_Click()
    Dim i As Integer
    Dim j As Integer
    On Error GoTo TrataErro
    If EcAnalisesSel.ListIndex > -1 Then
        j = EcAnalisesSel.ListIndex
        For i = EcAnalisesSel.ListIndex + 1 To totalAnaSelec - 1
            EstrutAnaSelec(i).cod_ana_s = EstrutAnaSelec(i + 1).cod_ana_s
            EstrutAnaSelec(i).cod_lote = EstrutAnaSelec(i + 1).cod_lote
            EstrutAnaSelec(i).descr_ana_s = EstrutAnaSelec(i + 1).descr_ana_s
            EstrutAnaSelec(i).seq_ana_s = EstrutAnaSelec(i + 1).seq_ana_s
            EstrutAnaSelec(i).seq_apar = EstrutAnaSelec(i + 1).seq_apar
            EstrutAnaSelec(i).prefixo = EstrutAnaSelec(i + 1).prefixo
        Next
        totalAnaSelec = totalAnaSelec - 1
        ReDim Preserve EstrutAnaSelec(totalAnaSelec)
        EcAnalisesSel.RemoveItem EcAnalisesSel.ListIndex
        If j < EcAnalisesSel.ListCount Then
            EcAnalisesSel.ListIndex = j
            EcAnalisesSel.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao BtRetira_Click: " & " " & Err.Number & " - " & Err.Description, Me.Name, "BtRetira_Click", True
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------

' VERIFICA SE ANALISE J� ESTA MARCADA PARA ENVIAR PARA APARELHO

' ------------------------------------------------------------------------------
Private Function VerificaListaAna(seq_apar As String, cod_lote As String, cod_ana_s As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsDados As New ADODB.recordset
    sSql = "SELECT * FROM sl_cq_listas WHERE seq_apar = " & seq_apar & " AND cod_lote = " & BL_TrataStringParaBD(cod_lote)
    sSql = sSql & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s) & " AND  flg_apar_trans = 0 "
    rsDados.CursorLocation = adUseServer
    rsDados.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDados.Open sSql, gConexao
    If rsDados.RecordCount >= 1 Then
        VerificaListaAna = True
    Else
        VerificaListaAna = False
    End If
    rsDados.Close
    Set rsDados = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "ERRO ao VerificaListaAna: " & " " & Err.Number & " - " & Err.Description, Me.Name, "VerificaListaAna", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' GERA LISTA DE TRABALHO PARA APARELHO

' ------------------------------------------------------------------------------

Private Sub GeraListaControlo()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim seq_lista As Long
    Dim data As String
    Dim hora As String
    Dim i As Integer
    Dim iReg As Integer
    BG_BeginTransaction
    seq_lista = BG_DaMAX("SL_CQ_LISTAS", "SEQ_LISTA") + 1
    data = Bg_DaData_ADO
    hora = Bg_DaHora_ADO
    For i = 1 To totalAnaSelec
        sSql = "INSERT INTO sl_cq_listas(seq_lista, seq_apar,cod_lote, prefixo,cod_ana_S,flg_apar_trans, user_cri, dt_cri, hr_cri ) VALUES("
        sSql = sSql & seq_lista & ", "
        sSql = sSql & EstrutAnaSelec(i).seq_apar & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutAnaSelec(i).cod_lote) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutAnaSelec(i).prefixo) & ", "
        sSql = sSql & BL_TrataStringParaBD(EstrutAnaSelec(i).cod_ana_s) & ", "
        sSql = sSql & "0, "
        sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
        sSql = sSql & BL_TrataDataParaBD(data) & ", "
        sSql = sSql & BL_TrataStringParaBD(hora) & ") "
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg <> 1 Then
            GoTo TrataErro
        End If
    Next
    BG_CommitTransaction
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "ERRO ao GeraListaControlo: " & sSql & " " & Err.Number & " - " & Err.Description, Me.Name, "GeraListaControlo", True
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------

' PREENCHE DADOS ACUMULADOS (MEDIA E DESVIO PADRAO)

' ------------------------------------------------------------------------------
Private Sub PreencheDadosAcumulados(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer)
    On Error GoTo TrataErro
    Dim i As Integer
    Dim soma As Double
    Dim aux1 As Double
    Dim num_Amostras As Integer
    On Error GoTo TrataErro
    
    soma = 0
    num_Amostras = 0
    For i = 1 To iRes - 1
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).estado = 0 Then
            soma = soma + Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).resultado
            num_Amostras = num_Amostras + 1
        End If
    Next
    If num_Amostras >= 1 Then
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).media = Round(soma / num_Amostras, 2)
    Else
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).media = 0
    End If
    'DESVIO PADRAO
    aux1 = 0
    For i = 1 To iRes - 1
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).estado = 0 Then
            aux1 = aux1 + ((Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).resultado - Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).media) * (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(i).resultado - Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).media))
        End If
    Next
    If num_Amostras > 1 Then
        aux1 = aux1 / (num_Amostras - 1)
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).desvio = Round(Sqr(aux1), 2)
    Else
        Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).desvio = 0
    End If
    
    
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "ERRO ao PreencheDadosAcumulados: " & " " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDadosAcumulados", True
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------

' MOSTRA OU NAO LINHA SE CHECKBOX ESTIVER ACTIVA

' ------------------------------------------------------------------------------
Private Function MostraFgRes(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer) As Boolean
    On Error GoTo TrataErro
    If val(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO) <= -2 Or val(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO) >= 2 Then
        MostraFgRes = True
    Else
        If OptTipo(0).value = True Then
            MostraFgRes = True
        Else
            MostraFgRes = False
        End If
    End If
Exit Function
TrataErro:
    MostraFgRes = False
    BG_LogFile_Erros "ERRO ao MostraFgRes: " & " " & Err.Number & " - " & Err.Description, Me.Name, "MostraFgRes", True
    Exit Function
    Resume Next
End Function

Private Sub OptTipo_Click(Index As Integer)
    PreencheFGRes
End Sub

' ------------------------------------------------------------------------------

' CARREGA A ESTRUTURA COM AS REGRAS PREDEFINIDAS

' ------------------------------------------------------------------------------
Private Function CarregaRegrasDisponiveis() As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsDados As New ADODB.recordset
    CarregaRegrasDisponiveis = False
    totalRegras = 0
    ReDim estrutRegras(0)
    
    sSql = "SELECT * FROM sl_tbf_regras_westgard "
    rsDados.CursorLocation = adUseServer
    rsDados.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDados.Open sSql, gConexao
    If rsDados.RecordCount >= 1 Then
        While Not rsDados.EOF
            totalRegras = totalRegras + 1
            ReDim Preserve estrutRegras(totalRegras)
            estrutRegras(totalRegras).cod_regra = BL_HandleNull(rsDados!cod_regra, "")
            estrutRegras(totalRegras).descr_regra = BL_HandleNull(rsDados!descr_regra, "")
            estrutRegras(totalRegras).abrev_regra = BL_HandleNull(rsDados!abrev_regra, "")
            rsDados.MoveNext
        Wend
    End If
    rsDados.Close
    Set rsDados = Nothing
    CarregaRegrasDisponiveis = True
Exit Function
TrataErro:
    BG_LogFile_Erros "ERRO ao CarregaRegrasDisponiveis: " & " " & Err.Number & " - " & Err.Description, Me.Name, "CarregaRegrasDisponiveis", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' PARA CADA RESULTADO AVALIA SE VIOLA ALGUMA DAS REGRAS MARCADAS

' ------------------------------------------------------------------------------

Private Sub AvaliaRegras(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer)
    On Error GoTo TrataErro
    Dim i As Integer
    For i = 1 To totalRegras
        If Estrutura(iApar).estrutLote(iLote).estrutRegras(i).flg_activo = mediSim Then
            If estrutRegras(i).cod_regra = 1 Then
                Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estrutRegras(i).flg_estado = AvaliaRegra1x2s(iApar, iLote, iAna, iRes)
            ElseIf estrutRegras(i).cod_regra = 2 Then
                Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estrutRegras(i).flg_estado = AvaliaRegra1x3s(iApar, iLote, iAna, iRes)
            ElseIf estrutRegras(i).cod_regra = 3 Then
                Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estrutRegras(i).flg_estado = AvaliaRegra2x2s(iApar, iLote, iAna, iRes)
            ElseIf estrutRegras(i).cod_regra = 4 Then
                Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estrutRegras(i).flg_estado = AvaliaRegraR4s(iApar, iLote, iAna, iRes)
            ElseIf estrutRegras(i).cod_regra = 5 Then
                Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estrutRegras(i).flg_estado = AvaliaRegra4x1s(iApar, iLote, iAna, iRes)
            ElseIf estrutRegras(i).cod_regra = 6 Then
                Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estrutRegras(i).flg_estado = AvaliaRegraX(iApar, iLote, iAna, iRes, 10)
            ElseIf estrutRegras(i).cod_regra = 7 Then
                Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estrutRegras(i).flg_estado = AvaliaRegraX(iApar, iLote, iAna, iRes, 8)
            ElseIf estrutRegras(i).cod_regra = 8 Then
                Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estrutRegras(i).flg_estado = AvaliaRegraX(iApar, iLote, iAna, iRes, 12)
            End If
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao AvaliaRegras: " & " " & Err.Number & " - " & Err.Description, Me.Name, "AvaliaRegras", True
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------

' AVALIA A REGRA 1X3S SE 1 RESULTADO VIOLA 3 OU -3 DP

' ------------------------------------------------------------------------------
Private Function AvaliaRegra1x3s(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer) As Integer
    On Error GoTo TrataErro
    AvaliaRegra1x3s = 0
    If gCQUsaMediaObtida = mediSim Then
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
            If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO <= -3 Or Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO >= 3 Then
                AvaliaRegra1x3s = 1
                Exit Function
            End If
        End If
    Else
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
            If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA <= -3 Or Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA >= 3 Then
                AvaliaRegra1x3s = 1
                Exit Function
            End If
        End If
    End If
Exit Function
TrataErro:
    AvaliaRegra1x3s = 0
    BG_LogFile_Erros "ERRO ao AvaliaRegra1x3s: " & " " & Err.Number & " - " & Err.Description, Me.Name, "AvaliaRegra1x3s", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' AVALIA A REGRA 1X2S SE 1 RESULTADO VIOLA 2 OU -2 DP

' ------------------------------------------------------------------------------
Private Function AvaliaRegra1x2s(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer) As Integer
    On Error GoTo TrataErro
    AvaliaRegra1x2s = 0
    If gCQUsaMediaObtida = mediSim Then
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
            If (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO <= -2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO > -3) Or _
                (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO >= 2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO < 3) Then
                
                AvaliaRegra1x2s = 1
                Exit Function
            End If
        End If
    Else
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
            If (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA <= -2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA > -3) Or _
                (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA >= 2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA < 3) Then
                
                AvaliaRegra1x2s = 1
                Exit Function
            End If
        End If
    End If
Exit Function
TrataErro:
    AvaliaRegra1x2s = 0
    BG_LogFile_Erros "ERRO ao AvaliaRegra1x2s: " & " " & Err.Number & " - " & Err.Description, Me.Name, "AvaliaRegra1x2s", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' AVALIA A REGRA 2X2S SE 2 RESULTADOS VIOLAM 2 OU -2 DP

' ------------------------------------------------------------------------------
Private Function AvaliaRegra2x2s(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer) As Integer
    On Error GoTo TrataErro
    Dim iViolacoes As Integer
    Dim iConseq As Integer
    Dim iApar2 As Integer
    Dim iLote2 As Integer
    Dim iAna2 As Integer
    Dim iRes2 As Integer
    Dim i As Integer
    AvaliaRegra2x2s = 0
    
    
    If gCQUsaMediaObtida = mediSim Then
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
            If (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO <= -2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO > -3) Or _
                (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO >= 2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO < 3) Then
                iViolacoes = 1
                iConseq = 1
                For i = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).idxAux - 1 To 1 Step -1
                    If Estrutura(iApar).estrutAuxAna(i).cod_ana_s = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cod_ana Then
                        If Estrutura(iApar).estrutAuxAna(i).cod_lote = Estrutura(iApar).estrutLote(iLote).cod_lote Then
                            If Estrutura(iApar).estrutAuxAna(i).flg_excluido = 0 And Estrutura(iApar).estrutAuxAna(i).estado = 0 Then
                                iApar2 = iApar
                                iLote2 = Estrutura(iApar).estrutAuxAna(i).iLote
                                iAna2 = Estrutura(iApar).estrutAuxAna(i).iAna
                                iRes2 = Estrutura(iApar).estrutAuxAna(i).iRes
                                If (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO <= -2 And Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO > -3) Or _
                                    (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO >= 2 And Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO < 3) Then
                                    AvaliaRegra2x2s = 1
                                End If
                                Exit Function
                            End If
                        Else
                        End If
                    End If
                Next
            End If
        End If
    Else
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
            If (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA <= -2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA > -3) Or _
                (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA >= 2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA < 3) Then
                iViolacoes = 1
                iConseq = 1
                For i = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).idxAux - 1 To 1 Step -1
                    If Estrutura(iApar).estrutAuxAna(i).cod_ana_s = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cod_ana Then
                        If Estrutura(iApar).estrutAuxAna(i).cod_lote = Estrutura(iApar).estrutLote(iLote).cod_lote Then
                            If Estrutura(iApar).estrutAuxAna(i).flg_excluido = 0 And Estrutura(iApar).estrutAuxAna(i).estado = 0 Then
                                iApar2 = iApar
                                iLote2 = Estrutura(iApar).estrutAuxAna(i).iLote
                                iAna2 = Estrutura(iApar).estrutAuxAna(i).iAna
                                iRes2 = Estrutura(iApar).estrutAuxAna(i).iRes
                                If (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA <= -2 And Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA > -3) Or _
                                    (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA >= 2 And Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA < 3) Then
                                    AvaliaRegra2x2s = 1
                                End If
                                Exit Function
                            End If
                        Else
                        End If
                    End If
                Next
            End If
        End If
    End If
Exit Function
TrataErro:
    AvaliaRegra2x2s = 0
    BG_LogFile_Erros "ERRO ao AvaliaRegra2x2s: " & " " & Err.Number & " - " & Err.Description, Me.Name, "AvaliaRegra2x2s", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' AVALIA A REGRA R(4s)

' ------------------------------------------------------------------------------

Private Function AvaliaRegraR4s(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer) As Integer
    On Error GoTo TrataErro
    Dim iApar2 As Integer
    Dim iLote2 As Integer
    Dim iAna2 As Integer
    Dim iRes2 As Integer
    Dim i As Integer
    Dim conta1 As Integer
    Dim conta2 As Integer
    AvaliaRegraR4s = 0
    
    
    If gCQUsaMediaObtida = mediSim Then
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
            If (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO <= -2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO > -3) Or _
                (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO >= 2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO < 3) Then
                conta1 = val(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO)
                For i = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).idxAux - 1 To 1 Step -1
                    If Estrutura(iApar).estrutAuxAna(i).cod_ana_s = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cod_ana Then
                        If Estrutura(iApar).estrutAuxAna(i).cod_lote = Estrutura(iApar).estrutLote(iLote).cod_lote Then
                            If Estrutura(iApar).estrutAuxAna(i).flg_excluido = 0 And Estrutura(iApar).estrutAuxAna(i).estado = 0 Then
                                iApar2 = iApar
                                iLote2 = Estrutura(iApar).estrutAuxAna(i).iLote
                                iAna2 = Estrutura(iApar).estrutAuxAna(i).iAna
                                iRes2 = Estrutura(iApar).estrutAuxAna(i).iRes
                                If (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO <= -2 And Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO > -3) Or _
                                    (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO >= 2 And Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO < 3) Then
                                    conta2 = val(Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO)
                                    If Abs(conta2 - conta1) = 4 Then
                                        AvaliaRegraR4s = 1
                                    End If
                                End If
                                Exit Function
                            End If
                        Else
                        End If
                    End If
                Next
            End If
        End If
    Else
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
            If (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA <= -2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA > -3) Or _
                (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA >= 2 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA < 3) Then
                conta1 = val(Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA)
                For i = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).idxAux - 1 To 1 Step -1
                    If Estrutura(iApar).estrutAuxAna(i).cod_ana_s = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cod_ana Then
                        If Estrutura(iApar).estrutAuxAna(i).cod_lote = Estrutura(iApar).estrutLote(iLote).cod_lote Then
                            If Estrutura(iApar).estrutAuxAna(i).flg_excluido = 0 And Estrutura(iApar).estrutAuxAna(i).estado = 0 Then
                                iApar2 = iApar
                                iLote2 = Estrutura(iApar).estrutAuxAna(i).iLote
                                iAna2 = Estrutura(iApar).estrutAuxAna(i).iAna
                                iRes2 = Estrutura(iApar).estrutAuxAna(i).iRes
                                If (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA <= -2 And Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA > -3) Or _
                                    (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA >= 2 And Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA < 3) Then
                                    conta2 = val(Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO)
                                    If Abs(conta2 - conta1) = 4 Then
                                        AvaliaRegraR4s = 1
                                    End If
                                End If
                                Exit Function
                            End If
                        Else
                        End If
                    End If
                Next
            End If
        End If
    End If
Exit Function
TrataErro:
    AvaliaRegraR4s = 0
    BG_LogFile_Erros "ERRO ao AvaliaRegraR4s: " & " " & Err.Number & " - " & Err.Description, Me.Name, "AvaliaRegraR4s", True
    Exit Function
    Resume Next
End Function



' ------------------------------------------------------------------------------

' AVALIA A REGRA 4X1S SE 4 RESULTADOS VIOLAM 1 OU -1 DP

' ------------------------------------------------------------------------------
Private Function AvaliaRegra4x1s(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer) As Integer
    On Error GoTo TrataErro
    Dim iViolacoes As Integer
    Dim iConseq As Integer
    Dim iApar2 As Integer
    Dim iLote2 As Integer
    Dim iAna2 As Integer
    Dim iRes2 As Integer
    Dim i As Integer
    AvaliaRegra4x1s = 0
    
    
    If gCQUsaMediaObtida = mediSim Then
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
            If (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO <= -1 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO > -2) Or _
                (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO >= 1 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO < 2) Then
                iViolacoes = 1
                iConseq = 1
                For i = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).idxAux - 1 To 1 Step -1
                    If Estrutura(iApar).estrutAuxAna(i).cod_ana_s = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cod_ana Then
                        If Estrutura(iApar).estrutAuxAna(i).cod_lote = Estrutura(iApar).estrutLote(iLote).cod_lote Then
                            If Estrutura(iApar).estrutAuxAna(i).flg_excluido = 0 And Estrutura(iApar).estrutAuxAna(i).estado = 0 Then
                                iConseq = iConseq + 1
                                iApar2 = iApar
                                iLote2 = Estrutura(iApar).estrutAuxAna(i).iLote
                                iAna2 = Estrutura(iApar).estrutAuxAna(i).iAna
                                iRes2 = Estrutura(iApar).estrutAuxAna(i).iRes
                                If (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO <= -1 And Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO > -2) Or _
                                    (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO >= 1 And Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO < 2) Then
                                    iViolacoes = iViolacoes + 1
                                End If
                                If iConseq > iViolacoes Then
                                    Exit Function
                                ElseIf iConseq = 4 And iViolacoes = 4 Then
                                    AvaliaRegra4x1s = 1
                                    Exit Function
                                End If
                            End If
                        Else
                        End If
                    End If
                Next
            End If
        End If
    Else
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
            If (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA <= -1 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA > -2) Or _
                (Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA >= 1 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA < 2) Then
                iViolacoes = 1
                iConseq = 1
                For i = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).idxAux - 1 To 1 Step -1
                    If Estrutura(iApar).estrutAuxAna(i).cod_ana_s = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cod_ana Then
                        If Estrutura(iApar).estrutAuxAna(i).cod_lote = Estrutura(iApar).estrutLote(iLote).cod_lote Then
                            If Estrutura(iApar).estrutAuxAna(i).flg_excluido = 0 And Estrutura(iApar).estrutAuxAna(i).estado = 0 Then
                                iConseq = iConseq + 1
                                iApar2 = iApar
                                iLote2 = Estrutura(iApar).estrutAuxAna(i).iLote
                                iAna2 = Estrutura(iApar).estrutAuxAna(i).iAna
                                iRes2 = Estrutura(iApar).estrutAuxAna(i).iRes
                                If (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA <= -1 And Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA > -2) Or _
                                    (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA >= 1 And Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA < 2) Then
                                    iViolacoes = iViolacoes + 1
                                End If
                                If iConseq > iViolacoes Then
                                    Exit Function
                                ElseIf iConseq = 4 And iViolacoes = 4 Then
                                    AvaliaRegra4x1s = 1
                                    Exit Function
                                End If
                            End If
                        Else
                        End If
                    End If
                Next
            End If
        End If
    End If
Exit Function
TrataErro:
    AvaliaRegra4x1s = 0
    BG_LogFile_Erros "ERRO ao AvaliaRegra2x2s: " & " " & Err.Number & " - " & Err.Description, Me.Name, "AvaliaRegra2x2s", True
    Exit Function
    Resume Next
End Function



' ------------------------------------------------------------------------------

' AVALIA A REGRA X SE x RESULTADOS ESTAO DO MESMO LADO DA MEDIA

' ------------------------------------------------------------------------------
Private Function AvaliaRegraX(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer, Num As Integer) As Integer
    On Error GoTo TrataErro
    Dim iViolacoes As Integer
    Dim iConseq As Integer
    Dim iApar2 As Integer
    Dim iLote2 As Integer
    Dim iAna2 As Integer
    Dim iRes2 As Integer
    Dim i As Integer
    AvaliaRegraX = 0
    
    
    If gCQUsaMediaObtida = mediSim Then
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
            If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO < 0 Or _
                Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO > 0 Then
                iViolacoes = 1
                iConseq = 1
                For i = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).idxAux - 1 To 1 Step -1
                    If Estrutura(iApar).estrutAuxAna(i).cod_ana_s = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cod_ana Then
                        If Estrutura(iApar).estrutAuxAna(i).cod_lote = Estrutura(iApar).estrutLote(iLote).cod_lote Then
                            If Estrutura(iApar).estrutAuxAna(i).flg_excluido = 0 And Estrutura(iApar).estrutAuxAna(i).estado = 0 Then
                                iConseq = iConseq + 1
                                iApar2 = iApar
                                iLote2 = Estrutura(iApar).estrutAuxAna(i).iLote
                                iAna2 = Estrutura(iApar).estrutAuxAna(i).iAna
                                iRes2 = Estrutura(iApar).estrutAuxAna(i).iRes
                                If (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO < 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO < 0) Or _
                                   (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedO > 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedO > 0) Then
                                        iViolacoes = iViolacoes + 1
                                End If
                                
                                If iConseq > iViolacoes Then
                                    Exit Function
                                ElseIf iConseq = Num And iViolacoes = Num Then
                                    AvaliaRegraX = 1
                                    Exit Function
                                End If
                            End If
                        Else
                        End If
                    End If
                Next
            End If
        End If
    Else
        If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).flg_excluido = 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).estado = 0 Then
            If Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA < 0 Or _
                Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA > 0 Then
                iViolacoes = 1
                iConseq = 1
                For i = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).idxAux - 1 To 1 Step -1
                    If Estrutura(iApar).estrutAuxAna(i).cod_ana_s = Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).cod_ana Then
                        If Estrutura(iApar).estrutAuxAna(i).cod_lote = Estrutura(iApar).estrutLote(iLote).cod_lote Then
                            If Estrutura(iApar).estrutAuxAna(i).flg_excluido = 0 And Estrutura(iApar).estrutAuxAna(i).estado = 0 Then
                                iConseq = iConseq + 1
                                iApar2 = iApar
                                iLote2 = Estrutura(iApar).estrutAuxAna(i).iLote
                                iAna2 = Estrutura(iApar).estrutAuxAna(i).iAna
                                iRes2 = Estrutura(iApar).estrutAuxAna(i).iRes
                                If (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA < 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA < 0) Or _
                                   (Estrutura(iApar2).estrutLote(iLote2).estrutAna(iAna2).estrutRes(iRes2).AvalMedA > 0 And Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).AvalMedA > 0) Then
                                        iViolacoes = iViolacoes + 1
                                End If
                                
                                If iConseq > iViolacoes Then
                                    Exit Function
                                ElseIf iConseq = Num And iViolacoes = Num Then
                                    AvaliaRegraX = 1
                                    Exit Function
                                End If
                            End If
                        Else
                        End If
                    End If
                Next
            End If
        End If
    
    End If
Exit Function
TrataErro:
    AvaliaRegraX = 0
    BG_LogFile_Erros "ERRO ao AvaliaRegraX: " & " " & Err.Number & " - " & Err.Description, Me.Name, "AvaliaRegraX", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' APAGA CONTROLO

' ------------------------------------------------------------------------------
Private Function ApagaControlo(iApar As Integer, iLote As Integer, iAna As Integer, iRes As Integer) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iRegistos As Integer
    ApagaControlo = False
    sSql = "UPDATE sl_cq_res set flg_invisivel = 1 WHERE seq_resultado = " & Estrutura(iApar).estrutLote(iLote).estrutAna(iAna).estrutRes(iRes).seq_resultado
    iRegistos = BG_ExecutaQuery_ADO(sSql)
    If iRegistos <= 0 Then GoTo TrataErro
    
    ApagaControlo = True
Exit Function
TrataErro:
    ApagaControlo = False
    BG_LogFile_Erros "ERRO ao ApagaControlo: " & " " & Err.Number & " - " & Err.Description, Me.Name, "ApagaControlo", True
    Exit Function
    Resume Next
End Function




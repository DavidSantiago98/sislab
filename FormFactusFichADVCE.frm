VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form FormFactusFichADVCE 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFactusFichADVCE"
   ClientHeight    =   7830
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   10980
   Icon            =   "FormFactusFichADVCE.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7830
   ScaleWidth      =   10980
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtAbrir 
      Caption         =   "Abrir"
      Height          =   375
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   855
   End
   Begin VB.CommandButton BtImprimir 
      Caption         =   "Imprimir"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1080
      TabIndex        =   1
      Top             =   0
      Width           =   975
   End
   Begin MSFlexGridLib.MSFlexGrid FgRequisicoes 
      Height          =   735
      Left            =   2160
      TabIndex        =   0
      Top             =   720
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   1296
      _Version        =   393216
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      BorderStyle     =   0
   End
   Begin MSComDlg.CommonDialog NomeFicheiro 
      Left            =   12000
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label LbEntidade 
      Caption         =   "Entidade: "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2640
      TabIndex        =   3
      Top             =   120
      Width           =   3255
   End
End
Attribute VB_Name = "FormFactusFichADVCE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset
Dim sNomeFicheiroI As String

Public Sub Main()

End Sub

Private Sub BtAbrir_Click()
    Dim iFile As Integer
    Dim sStr1 As String
    Dim conteudo() As String

    NomeFicheiro.Filter = "Ficheiro Texto (*.TXT)|*.TXT"

    sNomeFicheiroI = DevolveNomeFicheiro
    If sNomeFicheiroI = "" Then
        Exit Sub
    End If

    BtImprimir.Enabled = True
    
    FormFactusFichADVCE.caption = "AdvanceCare - "
    LbEntidade.caption = ""
    
    FgRequisicoes.rows = 2
    FgRequisicoes.TextMatrix(1, 0) = ""
    FgRequisicoes.TextMatrix(1, 1) = ""
    FgRequisicoes.TextMatrix(1, 2) = ""
    FgRequisicoes.TextMatrix(1, 3) = ""
    
    iFile = FreeFile
    Open sNomeFicheiroI For Input As #iFile
        Do While Not EOF(iFile)
            Line Input #1, sStr1
        
            BL_Tokenize sStr1, "|", conteudo
            If conteudo(0) = "01" Then
                FormFactusFichADVCE.caption = FormFactusFichADVCE.caption & conteudo(3)
                LbEntidade.caption = LbEntidade.caption & conteudo(1)
                                
                FgRequisicoes.TextMatrix(1, 0) = conteudo(4)
                FgRequisicoes.TextMatrix(1, 1) = Mid(conteudo(5), 1, 4) & "/" & Mid(conteudo(5), 5, 2) & "/" & Mid(conteudo(5), 7)
                FgRequisicoes.TextMatrix(1, 2) = CDbl(Replace(conteudo(8), ".", ",")) & " �"
                FgRequisicoes.TextMatrix(1, 3) = conteudo(10)
            End If
        Loop
    Close #iFile

    FgRequisicoes.Row = 1
    FgRequisicoes.Col = 0
    FgRequisicoes.SetFocus
    
End Sub

Private Function DevolveNomeFicheiro() As String
    DevolveNomeFicheiro = ""
    
    NomeFicheiro.FilterIndex = 1
    NomeFicheiro.CancelError = True
    NomeFicheiro.InitDir = "C:\GLINTTHS\"
    NomeFicheiro.FileName = ""
    On Error Resume Next
    NomeFicheiro.DialogTitle = "Nome do ficheiro"
    NomeFicheiro.ShowOpen
    
    DevolveNomeFicheiro = NomeFicheiro.FileName

End Function


Private Sub BtImprimir_Click()
    Dim sNomeFicheiroO As String
    Dim iFile As Integer
    Dim oFile As Integer
    
    Dim sStr1 As String
    Dim conteudo() As String
    Dim Oldconteudo As String
    
    Dim DoeContratado As Double
    Dim DoeDoente As Double
    Dim DoeDesconto As Double
    Dim DoeEntid As Double
    Dim TotContratado As Double
    Dim TotDoente As Double
    Dim TotDesconto As Double
    Dim TotEntid As Double
    
    Oldconteudo = "9999999"
    DoeContratado = 0
    DoeDoente = 0
    DoeDesconto = 0
    DoeEntid = 0
    TotContratado = 0
    TotDoente = 0
    TotEntid = 0

    oFile = FreeFile
    sNomeFicheiroO = "C:\GLINTTHS\ADVCARE_EXTRACTO.TXT"
    Open sNomeFicheiroO For Output As #oFile
    
    iFile = FreeFile
    Open sNomeFicheiroI For Input As #iFile
        Do While Not EOF(iFile)
            Line Input #2, sStr1
            
            BL_Tokenize sStr1, "|", conteudo
            If conteudo(0) = "01" Then
                Print #oFile, " "
                Print #oFile, "                                     " & conteudo(1)
                Print #oFile, " "
                Print #oFile, " --------------------------------------------------------------- "
                Print #oFile, "| N� Linhas | Data do Cheque | Valor a Pagar |    Refer�ncia    |"
                Print #oFile, "|-----------+----------------+---------------+------------------|"
                Print #oFile, "| " & Right("         " & conteudo(4), 9) & " | " & _
                              "  " & Mid(conteudo(5), 1, 4) & "/" & Mid(conteudo(5), 5, 2) & "/" & Mid(conteudo(5), 7) & "  " & " | " & _
                              Right("           " & CDbl(Replace(conteudo(8), ".", ",")) & " �", 13) & " | " & _
                              left(conteudo(10) & "                ", 16) & " |"
                Print #oFile, " --------------------------------------------------------------- "
            ElseIf conteudo(0) = "02" Then
                If Oldconteudo <> conteudo(1) Then
                    If Oldconteudo <> "9999999" Then
                        Print #oFile, "     ---------------------------------+------------+----------+------------+-----------+---------------------- "
                        Print #oFile, "                                      | " & _
                                      Right("        " & DoeContratado, 8) & " �" & " | " & _
                                      Right("      " & DoeDoente, 6) & " �" & " | " & _
                                      Right("        " & DoeDesconto, 8) & " �" & " | " & _
                                      Right("       " & DoeEntid, 7) & " �" & " |"
                        Print #oFile, "                                       " & _
                                      "------------------------------------------------"
                    End If
                    Oldconteudo = conteudo(1)
                    DoeContratado = 0
                    DoeDoente = 0
                    DoeDesconto = 0
                    DoeEntid = 0
                    Print #oFile, " "
                    Print #oFile, " "
                    Print #oFile, " ------------------------------------------------------------ "
                    Print #oFile, "|   N� Benefici�rio    |                Nome                 |"
                    Print #oFile, "|----------------------+-------------------------------------|"
                    Print #oFile, "| " & left(conteudo(1) & "                    ", 20) & " | " & _
                              left(conteudo(2) & "                                   ", 35) & " |"
                    Print #oFile, " ------------------------------------------------------------------------------------------------------------ "
                    Print #oFile, "    |    Data    |   C�digo   | Quant | Contratado |  Doente  |  Desconto  |  Entidade |     Observa��es      |"
                    Print #oFile, "    +------------+------------+-------+------------+----------+------------+-----------+----------------------|"
                End If

                'Retorna a posi��o da string "||" na string sStr1 (0 - n�o existe)
                If InStr(1, sStr1, "||") > 0 Then
                    DoeContratado = Round(DoeContratado + CDbl(Replace(conteudo(9), ".", ",")), 2)
                    DoeDoente = Round(DoeDoente + CDbl(Replace(conteudo(10), ".", ",")), 2)
                    DoeDesconto = Round(DoeDesconto + CDbl(Replace(conteudo(11), ".", ",")), 2)
                    DoeEntid = Round(DoeEntid + CDbl(Replace(conteudo(13), ".", ",")), 2)

                    TotContratado = Round(TotContratado + CDbl(Replace(conteudo(9), ".", ",")), 2)
                    TotDoente = Round(TotDoente + CDbl(Replace(conteudo(10), ".", ",")), 2)
                    TotDesconto = Round(TotDesconto + CDbl(Replace(conteudo(11), ".", ",")), 2)
                    TotEntid = Round(TotEntid + CDbl(Replace(conteudo(13), ".", ",")), 2)

                    Print #oFile, "    | " & Mid(conteudo(5), 1, 4) & "/" & Mid(conteudo(5), 5, 2) & "/" & Mid(conteudo(5), 7) & " | " & _
                        left(conteudo(6) & "          ", 10) & " | " & _
                        Right("     " & CDbl(Replace(conteudo(7), ".", ",")), 5) & " | " & _
                        Right("        " & CDbl(Replace(conteudo(9), ".", ",")), 8) & " �" & " | " & _
                        Right("      " & CDbl(Replace(conteudo(10), ".", ",")), 6) & " �" & " | " & _
                        Right("        " & CDbl(Replace(conteudo(11), ".", ",")), 8) & " �" & " | " & _
                        Right("       " & CDbl(Replace(conteudo(13), ".", ",")), 7) & " �" & " | " & _
                        "                    " & " |"
                Else
                    DoeContratado = Round(DoeContratado + CDbl(Replace(conteudo(10), ".", ",")), 2)
                    DoeDoente = Round(DoeDoente + CDbl(Replace(conteudo(11), ".", ",")), 2)
                    DoeDesconto = Round(DoeDesconto + CDbl(Replace(conteudo(12), ".", ",")), 2)
                    DoeEntid = Round(DoeEntid + CDbl(Replace(conteudo(14), ".", ",")), 2)

                    TotContratado = Round(TotContratado + CDbl(Replace(conteudo(10), ".", ",")), 2)
                    TotDoente = Round(TotDoente + CDbl(Replace(conteudo(11), ".", ",")), 2)
                    TotDesconto = Round(TotDesconto + CDbl(Replace(conteudo(12), ".", ",")), 2)
                    TotEntid = Round(TotEntid + CDbl(Replace(conteudo(14), ".", ",")), 2)

                    Print #oFile, "    | " & Mid(conteudo(5), 1, 4) & "/" & Mid(conteudo(5), 5, 2) & "/" & Mid(conteudo(5), 7) & " | " & _
                        left(conteudo(6) & "          ", 10) & " | " & _
                        Right("     " & CDbl(Replace(conteudo(7), ".", ",")), 5) & " | " & _
                        Right("        " & CDbl(Replace(conteudo(10), ".", ",")), 8) & " �" & " | " & _
                        Right("      " & CDbl(Replace(conteudo(11), ".", ",")), 6) & " �" & " | " & _
                        Right("        " & CDbl(Replace(conteudo(12), ".", ",")), 8) & " �" & " | " & _
                        Right("       " & CDbl(Replace(conteudo(14), ".", ",")), 7) & " �" & " | " & _
                        left(conteudo(8) & "                    ", 20) & " |"
                End If
            End If
        Loop
    Close #iFile
    
    Print #oFile, "     ---------------------------------+------------+----------+------------+-----------+---------------------- "
    Print #oFile, "                                      | " & _
                  Right("        " & DoeContratado, 8) & " �" & " | " & _
                  Right("      " & DoeDoente, 6) & " �" & " | " & _
                  Right("        " & DoeDesconto, 8) & " �" & " | " & _
                  Right("       " & DoeEntid, 7) & " �" & " |"
    Print #oFile, "                                       " & _
                  "------------------------------------------------"
    Print #oFile, " "
    Print #oFile, "                                       " & _
                  "================================================"
    Print #oFile, "                                      | " & _
                  Right("        " & TotContratado, 8) & " �" & " | " & _
                  Right("      " & TotDoente, 6) & " �" & " | " & _
                  Right("        " & TotDesconto, 8) & " �" & " | " & _
                  Right("       " & TotEntid, 7) & " �" & " |"
    Print #oFile, "                                       " & _
                  "================================================"
    Close #oFile
    
    Dim X
    X = Shell("Notepad " & sNomeFicheiroO, vbMaximizedFocus)

End Sub






Private Sub Form_Activate()
    
    Call EventoActivate
    
End Sub

Private Sub Form_Load()
    
    Call EventoLoad
    
End Sub



Private Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    
    Call BL_InicioProcessamento(Me, "Inicializar �cran.")
    
    'Preenche Combos
    Call PreencheValoresDefeito
    
    'Define Campos do ecran e das tabelas
    Call Inicializacoes
    
    'Define o tipo de campos
    Call DefTipoCampos
    
    Call BG_ParametrizaPermissoes_ADO(Me.Name)
    
    Estado = 1
    Call BG_StackJanelas_Push(Me)
    Call BL_FimProcessamento(Me)
    
End Sub

Private Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormFactusFichADVCE = Nothing

End Sub

Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub

Private Sub Inicializacoes()
        
    Me.caption = " ADMG "
    
    Me.left = 800
    Me.top = 800
    Me.Width = 17960
    Me.Height = 8610
    
    
    Set CampoDeFocus = BtAbrir
    
    NomeTabela = ""
    
    NumCampos = 0
        
End Sub
Sub DefTipoCampos()
        With FgRequisicoes
        .Cols = 4
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        
        .ColAlignment(0) = flexAlignCenterCenter
        .ColWidth(0) = 1000
        .TextMatrix(0, 0) = "N� de Linhas"
        
        .ColAlignment(1) = flexAlignCenterCenter
        .ColWidth(1) = 1300
        .TextMatrix(0, 1) = "Data do Cheque"
        
        .ColAlignment(2) = flexAlignCenterCenter
        .ColWidth(2) = 1300
        .TextMatrix(0, 2) = "Valor a Pagar"
        
        .ColAlignment(3) = flexAlignCenterCenter
        .ColWidth(3) = 2000
        .TextMatrix(0, 3) = "Refer�ncia"
    
        .Width = .ColWidth(0) + .ColWidth(1) + .ColWidth(2) + .ColWidth(3) + 500
    End With
End Sub

Public Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
    
End Sub

Public Sub FuncaoInserir()
    
  ' nada

    
End Sub
Private Function ValidaCamposEc() As Integer

' nada


End Function
Public Sub FuncaoProcurar()
    
' nada

End Sub

Public Sub FuncaoModificar()
    
' nada

    
End Sub

Public Sub FuncaoRemover()
' nada

    
End Sub

Public Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoEstadoAnterior()
    
     If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Private Sub PreencheValoresDefeito()
    
' nada
End Sub

Private Sub BD_Insert()
' nada
    

End Sub

Private Sub BD_Delete()
' nada
    

    
End Sub

Private Sub BD_Update()
' nada
    

End Sub

Private Sub LimpaCampos()
' nada

    
End Sub

Private Sub PreencheCampos()
    ' nada

End Sub






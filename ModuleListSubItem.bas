Attribute VB_Name = "ModuleListSubItem"
'      .............................
'     .                             .
'    .   Paulo Ferreira 2009.09.29   .
'     .       � 2009 Glintt-HS      .
'      .............................

Option Explicit

' Declare function lib GetProp.
Private Declare Function GetProp Lib "user32" Alias "GetPropA" (ByVal hWnd As Long, ByVal lpString As String) As Long

' Declare function lib SetProp.
Private Declare Function SetProp Lib "user32" Alias "SetPropA" (ByVal hWnd As Long, ByVal lpString As String, ByVal hData As Long) As Long

' Declare function lib GetCursorPos.
Private Declare Function RemoveProp Lib "user32" Alias "RemovePropA" (ByVal hWnd As Long, ByVal lpString As String) As Long

' Declare function lib SetWindowLong.
Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long

' Declare function lib CallWindowProc.
Private Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal hWnd As Long, ByVal uMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

' Constant to define window handle control destroy.
Private Const WM_DESTROY = &H2

' Constant to define window handle control kill focus.
Private Const WM_KILLFOCUS = &H8

' Constant to define window long index.
Private Const GWL_WNDPROC = (-4)

' Constant to define window property proc.
Private Const OLDWNDPROC = "OldWndProc"

' Set window listview subclass active.
Public Function SubClass(�hWnd� As Long, �lpfnNew� As Long) As Boolean
  
    Dim lpfnOld As Long
    Dim fSuccess As Boolean
  
    If (GetProp(�hWnd�, OLDWNDPROC) = 0) Then
        lpfnOld = SetWindowLong(�hWnd�, GWL_WNDPROC, �lpfnNew�)
        If (lpfnOld) Then: fSuccess = SetProp(�hWnd�, OLDWNDPROC, lpfnOld)
    End If
    If (fSuccess) Then
        SubClass = True
    Else
        If (lpfnOld) Then: Call UnSubClass(�hWnd�)
        ' MsgBox "Unable to successfully subclass &H" & Hex(�hWnd�), vbCritical
    End If
  
End Function

' Set window listview subclass inactive.
Public Function UnSubClass(�hWnd� As Long) As Boolean
  
    Dim lpfnOld As Long
  
    lpfnOld = GetProp(�hWnd�, OLDWNDPROC)
    If (lpfnOld) Then
      If RemoveProp(�hWnd�, OLDWNDPROC) Then: UnSubClass = SetWindowLong(�hWnd�, GWL_WNDPROC, lpfnOld)
    End If

End Function

' Define window listview sub item behaviour.
Public Function WndProc(ByVal �hWnd� As Long, ByVal �uMsg� As Long, ByVal �wParam� As Long, ByVal �lParam� As Long) As Long
  
    Select Case (�uMsg�)
        Case WM_KILLFOCUS:
            Call CallWindowProc(GetProp(�hWnd�, OLDWNDPROC), �hWnd�, �uMsg�, �wParam�, �lParam�)
            If (gFormActivo.Name = "FormControloStock") Then: Call FormControloStock.HideTextBox(True)
            Exit Function
        Case WM_DESTROY:
            Call CallWindowProc(GetProp(�hWnd�, OLDWNDPROC), �hWnd�, �uMsg�, �wParam�, �lParam�)
            Call UnSubClass(�hWnd�)
            Exit Function
    End Select
    WndProc = CallWindowProc(GetProp(�hWnd�, OLDWNDPROC), �hWnd�, �uMsg�, �wParam�, �lParam�)
  
End Function

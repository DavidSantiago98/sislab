VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClassModuleMessage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'     .................................................................
'    .         Class module for application messaging system.          .
'   .                                                                   .
'   .                    Paulo Ferreira 2009.08.07                      .
'    .                       � 2009 GLINTT-HS                          .
'     .................................................................

Option Explicit

' Member to define from parameter code.
Private lCode As Long

' Member to define subject parameter code.
Private sSubject As String

' Member to define from parameter message.
Private lFrom As Integer

' Member to define to parameter message.
Private cTo As Collection

' Member to define CC parameter message.
Private cCC As Collection

' Member to define state parameter message.
Private iState As Integer

' Member to define message parameter message.
Private sMessage As String

' Member to define attachments parameter message.
Private cAttachments As Collection

' Get message lCode parameter.
Public Property Get GetCode() As Long: GetCode = lCode: End Property

' Get message sSubject parameter.
Public Property Get GetSubject() As String: GetSubject = sSubject: End Property

' Get message lFrom parameter.
Public Property Get GetFrom() As Long: GetFrom = lFrom: End Property

' Get message cTo parameter.
Public Property Get GetTo() As Collection: Set GetTo = cTo: End Property

' Get message cCC parameter.
Public Property Get GetCC() As Collection: Set GetCC = cCC: End Property

' Get message sFrom parameter.
Public Property Get GetState() As Integer: GetState = iState: End Property

' Get message sFrom parameter.
Public Property Get GetMessage() As String: GetMessage = sMessage: End Property

' Get message sFrom parameter.
Public Property Get GetAttachments() As Collection: Set GetAttachments = cAttachments: End Property

' Set message lCode parameter.
Public Property Let SetCode(�lCode� As Long): lCode = �lCode�: End Property

' Set message sSubject parameter.
Public Property Let SetSubject(�sSubject� As String): sSubject = �sSubject�: End Property

' Set message sFrom parameter.
Public Property Let SetFrom(�lFrom� As Long): lFrom = �lFrom�: End Property

' Set message cTo parameter.
Public Property Let SetTo(�cTo� As Collection): Set cTo = �cTo�: End Property

' Set message cCC parameter.
Public Property Let SetCC(�cCC� As Collection): Set cCC = �cCC�: End Property

' Set message iState parameter.
Public Property Let SetState(�iState� As Integer): iState = �iState�: End Property

' Set message sMessage parameter.
Public Property Let SetMessage(�sMessage� As String): sMessage = �sMessage�: End Property

' Set message cAttachments parameter.
Public Property Let SetAttachments(�cAttachments� As Collection): Set cAttachments = �cAttachments�: End Property

VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormGeraDados 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionFactGeraDados"
   ClientHeight    =   4935
   ClientLeft      =   2295
   ClientTop       =   3060
   ClientWidth     =   5085
   Icon            =   "FormFACTUSGeraDados.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4935
   ScaleWidth      =   5085
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   2055
      Left            =   240
      TabIndex        =   3
      Top             =   2280
      Width           =   4695
      Begin VB.TextBox EcCodRequisicao 
         Height          =   285
         Left            =   1320
         TabIndex        =   12
         Top             =   240
         Width           =   1335
      End
      Begin VB.Frame Frame1 
         Caption         =   "Data de Chegada"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   3675
         Begin VB.TextBox EcDataFim 
            Height          =   285
            Left            =   2280
            TabIndex        =   7
            Top             =   480
            Width           =   975
         End
         Begin VB.TextBox EcDataInicio 
            Height          =   285
            Left            =   600
            TabIndex        =   6
            Top             =   480
            Width           =   975
         End
         Begin VB.CheckBox EcTodas 
            Caption         =   "Incluir todas as entidades"
            Height          =   255
            Left            =   240
            TabIndex        =   5
            Top             =   840
            Width           =   2415
         End
         Begin MSComCtl2.DTPicker EcEscolhaInicio 
            Height          =   255
            Left            =   1200
            TabIndex        =   8
            Top             =   480
            Width           =   615
            _ExtentX        =   1085
            _ExtentY        =   450
            _Version        =   393216
            Format          =   147324929
            CurrentDate     =   37720
            MinDate         =   2
         End
         Begin MSComCtl2.DTPicker EcEscolhaFim 
            Height          =   255
            Left            =   2880
            TabIndex        =   9
            Top             =   480
            Width           =   615
            _ExtentX        =   1085
            _ExtentY        =   450
            _Version        =   393216
            Format          =   147324929
            CurrentDate     =   37720
            MinDate         =   2
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "De"
            Height          =   195
            Left            =   240
            TabIndex        =   11
            Top             =   480
            Width           =   210
         End
         Begin VB.Label Label2 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            Caption         =   "a"
            Height          =   195
            Left            =   1920
            TabIndex        =   10
            Top             =   480
            Width           =   225
         End
      End
      Begin VB.Label LaNrInscricao 
         AutoSize        =   -1  'True
         Caption         =   "Nr. Requisi��o"
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   1050
      End
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   240
      MultiSelect     =   2  'Extended
      TabIndex        =   0
      Top             =   360
      Width           =   4590
   End
   Begin VB.CommandButton BtFactGeraDados 
      Caption         =   "&Gerar Dados para a Factura��o"
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   4440
      Width           =   4575
   End
   Begin VB.Label LaEntidades 
      AutoSize        =   -1  'True
      Caption         =   "Entidades"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   360
      TabIndex        =   2
      Top             =   120
      Width           =   855
   End
End
Attribute VB_Name = "FormGeraDados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Private Sub EcEscolhaInicio_CloseUp()
    EcDataInicio = EcEscolhaInicio.value
    EcDataInicio.SetFocus
End Sub

Private Sub EcEscolhaFim_CloseUp()
    EcDataFim = EcEscolhaFim.value
    EcDataFim.SetFocus
End Sub
Sub BtFactGeraDados_Click()
'    If gFacturaFechadas <> 1 Then
        If EcCodRequisicao.Text = "" Then
            If (EcDataInicio = "" Or EcDataFim = "") Then
                BG_Mensagem mediMsgBox, "Intervalo de Datas n�o v�lido!", vbExclamation, Me.caption
                Exit Sub
            ElseIf CDate(EcDataInicio) > CDate(EcDataFim) Then
                BG_Mensagem mediMsgBox, "Intervalo de Datas n�o v�lido!", vbExclamation, Me.caption
                Exit Sub
            End If
        End If
'    End If


    If StrComp(UCase(gSISTEMA_FACTURACAO), UCase("SONHO"), vbTextCompare) = 0 Or UCase(gSISTEMA_FACTURACAO) = UCase("GH") Then
        Gera_Dados_Fact
'    ElseIf StrComp(UCase(gSISTEMA_FACTURACAO), UCase("SONHO"), vbTextCompare) = 0 Then
'        Gera_Dados_Fact_SONHO
    ElseIf UCase(gSISTEMA_FACTURACAO) = UCase("FACTUS") Then
        Gera_Dados_Fact_FACTUS
'    ElseIf UCase(gSISTEMA_FACTURACAO) = UCase("GH") Then
'        Gera_Dados_Fact_GH
    End If

End Sub

Sub EcDataFim_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Sub EcDataFim_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Sub EcDataInicio_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Sub EcDataInicio_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Sub EcTodas_Click()
    Dim i As Integer

    If EcTodas.value = 1 Then
        For i = 0 To EcLista.ListCount - 1
            If EcLista.Selected(i) = True Then
                EcLista.Selected(i) = False
            End If
        Next i
        EcLista.Enabled = False

        Exit Sub
    End If

    EcLista.Enabled = True

End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Gera_Dados_Fact_FACTUS()
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim lNrFact As Long, lNrReq As Long, lInscricao As Long
    Dim lCount As Long
    Dim lExame As Long, lEntidade As Long
    Dim sTDoente As String, sTEpisodio As String, sEpisodio As String
    Dim sIsento As String, sBenef As String, sServico As String, sEFR As String
    Dim sExame As String, sSigla As String, sNrIncidencias As String, sFlgAmbInt As String
    Dim iCodNaoIsento As Integer, i As Integer, iRes As Integer, j As Integer
    Dim iBD_Aberta As Integer
    Dim bFacturar As Boolean
    Dim CodAnaFACTUS As String
    Dim l As Integer
    Dim ContaRubFact As Long
    Dim HisAberto As Integer
    Dim ContaReqFact As Long

    On Error GoTo TrataErro
    
    BL_InicioProcessamento Me, "Gera��o a decorrer! "
    DoEvents
    
    
    sSql = "SELECT distinct sl_requis.n_req,sl_requis.dt_chega,sl_requis.n_epis,cod_efr,cod_proven, " & _
         " sl_requis.t_sit,n_benef,estado_req,t_utente,utente,cod_isencao,n_benef, " & _
         " nome_ute,dt_nasc_ute,descr_mor_ute,cod_postal_ute,telef_ute, " & _
         " sl_realiza.seq_realiza, cod_perfil,cod_ana_c,cod_ana_s, cod_agrup, " & _
         " sl_res_micro.n_res,sl_res_micro.cod_micro,sl_res_micro.flg_tsq,sl_res_micro.prova prova, " & _
         " sl_res_micro.cod_gr_antibio,sl_microrg.metodo_tsq, sl_microrg.prova prova_micro " & _
         " FROM sl_requis, sl_identif, sl_realiza, sl_res_micro, sl_microrg " & _
         " WHERE " & _
         " sl_requis.seq_utente = sl_identif.seq_utente and " & _
         " sl_requis.n_req = sl_realiza.n_req and " & _
         " sl_realiza.seq_realiza = sl_res_micro.seq_realiza(+) and " & _
         " sl_res_micro.cod_micro = sl_microrg.cod_microrg(+) and " & _
         " sl_realiza.flg_estado in (3,4) and (sl_realiza.flg_facturado is null or sl_realiza.flg_facturado = 0) and sl_requis.mot_fact_rej is null "


    If EcCodRequisicao <> "" Then
        sSql = sSql & " AND sl_requis.n_req = " & EcCodRequisicao & " "

    Else
        sSql = sSql & " AND sl_requis.dt_chega between " & BL_TrataDataParaBD(EcDataInicio) & " and " & BL_TrataDataParaBD(EcDataFim)

        If EcTodas.value = 0 Then
            If EcLista.SelCount > 400 Then
                BG_Mensagem mediMsgBox, "Demasiadas Entidades seleccionadas!" & vbCrLf & "Pode seleccionar um m�ximo de 400 entidades.", vbOKCancel + vbExclamation, Me.caption
                BL_FimProcessamento Me
                Exit Sub
            ElseIf EcLista.SelCount = 0 Then
                BG_Mensagem mediMsgBox, "Nenhuma Entidades seleccionada!" & vbCrLf & "Tem de seleccionar pelo menos uma entidade.", vbOKCancel + vbExclamation, Me.caption
                BL_FimProcessamento Me
                Exit Sub
            Else
                sSql = sSql & " AND sl_requis.cod_efr IN ("
                For i = 0 To EcLista.ListCount - 1
                    If EcLista.Selected(i) = True Then
                        sSql = sSql & EcLista.ItemData(i) & ", "
                    End If
                Next i
                sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
            End If
        End If
    End If
    
    sSql = sSql & " ORDER BY sl_requis.n_req,sl_realiza.seq_realiza, sl_realiza.cod_perfil, sl_realiza.cod_ana_c, sl_realiza.cod_ana_s "

    If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic

    lCount = 0
    ContaReqFact = 0
    If rs.RecordCount <> 0 Then
    
        iBD_Aberta = BL_Abre_Conexao_Secundaria(gSGBD)
    
        If iBD_Aberta = 0 Then
            BL_FimProcessamento Me
            rs.Close
            Set rs = Nothing
            Exit Sub
        End If
    
        Do While Not rs.EOF

            sTDoente = rs!t_utente
            sTEpisodio = "SISLAB"
            sEpisodio = rs!n_req
            sServico = BL_HandleNull(rs!cod_proven, 0)
            iCodNaoIsento = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "Fact_CodNaoIsento"))
'            If Rs!cod_isencao <> iCodNaoIsento Then
                sIsento = "1"
'            Else
'                sIsento = ""
'            End If
            lNrReq = rs!n_req
            
            'Facturar a identifica��o do microrganismo
            If BL_HandleNull(rs!cod_micro, "") <> "" Then
                
                CodAnaFACTUS = Mapeia_Microrg_para_FACTURACAO(rs!cod_micro, BL_HandleNull(rs!Prova, BL_HandleNull(rs!Prova_micro, "")), rs!n_req)
                
                If ((CodAnaFACTUS <> "-1") And (gInsereAnaFact = True)) Then
                    For l = 1 To gQuantidadeAFact
                        If (FACTUS_Nova_Analises_IN(sTEpisodio, _
                                                    sEpisodio, _
                                                    sTDoente, _
                                                    rs!Utente, _
                                                    CodAnaFACTUS, _
                                                    lNrReq, _
                                                    rs!dt_chega, _
                                                    sServico, _
                                                    sIsento, _
                                                    rs!cod_agrup) = 1) Then
                            
                            'actualiza flg_facturado
                            sSql = "UPDATE sl_realiza SET flg_facturado = 1 " & _
                                  "WHERE seq_realiza = " & rs!seq_realiza
                            BG_ExecutaQuery_ADO sSql
                            BG_Trata_BDErro
                            
                            ContaRubFact = ContaRubFact + 1
                        End If
                            
                        If l < gQuantidadeAFact Then
                            gRsFact.MoveNext
                            CodAnaFACTUS = BL_HandleNull(gRsFact!cod_ana_gh, "")
                        End If
                    Next l
                End If
                gRsFact.Close
                Set gRsFact = Nothing
                    
                If BL_HandleNull(rs!flg_tsq, "") = "S" Then
                        
                    'Facturar o antibiograma de acordo com o(s) grupo(s) de antibioticos registado(s)
                    If BL_HandleNull(rs!Cod_Gr_Antibio, "") <> "" Then
                        Dim PosAux As Integer
                        Dim CodGrAntibio As String
                        Dim CodGrAntibioAux As String
                        Dim UltimaCarta As String
                        CodGrAntibio = rs!Cod_Gr_Antibio
                        PosAux = 1
                        UltimaCarta = ""
                        While Not PosAux = 0
                            If UltimaCarta <> CodGrAntibio Then
                                PosAux = InStr(1, CodGrAntibio, ";")
                                If PosAux <> 0 Then
                                    CodGrAntibioAux = Mid(CodGrAntibio, 1, PosAux - 1)
                                Else
                                    CodGrAntibioAux = CodGrAntibio
                                End If
                                CodAnaFACTUS = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, CodGrAntibioAux, , rs!n_req)
                                
                                If ((CodAnaFACTUS <> "-1") And (gInsereAnaFact = True)) Then
                                    If (FACTUS_Nova_Analises_IN(sTEpisodio, _
                                                               sEpisodio, _
                                                               sTDoente, _
                                                               rs!Utente, _
                                                               CodAnaFACTUS, _
                                                               lNrReq, _
                                                               rs!dt_chega, _
                                                               sServico, _
                                                               sIsento, _
                                                               rs!cod_agrup) = 1) Then
                                            
                                        ContaRubFact = ContaRubFact + 1
                                    End If
                                End If
                                UltimaCarta = CodGrAntibioAux
                                CodGrAntibio = Mid(CodGrAntibio, PosAux + 1)
                            End If
                        Wend
                            
                    'Facturar o antibiograma de acordo com o m�todo da codifica��o de microorganismos
                    ElseIf BL_HandleNull(rs!Metodo_Tsq, "") <> "" And BL_HandleNull(rs!Metodo_Tsq, "") <> "2" And BL_HandleNull(rs!Metodo_Tsq, "") <> "3" Then
                    
                        CodAnaFACTUS = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, , BL_HandleNull(rs!Metodo_Tsq, ""), rs!n_req)
                        
                        If ((CodAnaFACTUS <> "-1") And (gInsereAnaFact = True)) Then
                            If (FACTUS_Nova_Analises_IN(sTEpisodio, _
                                                       sEpisodio, _
                                                       sTDoente, _
                                                       rs!Utente, _
                                                       CodAnaFACTUS, _
                                                       rs!n_req, _
                                                       rs!dt_chega, _
                                                       sServico, _
                                                       sIsento, _
                                                       rs!cod_agrup) = 1) Then
                                
                                ContaRubFact = ContaRubFact + 1
                            End If
                        End If
                    End If
                            
                    'Verificar se existem antibi�ticos registados cujo seu grupo de antibi�ticos tenham o m�todo TSQ "E-TEST" ou "BK"
                    Dim RsFactAntib As ADODB.recordset
                    Set RsFactAntib = New ADODB.recordset
                    sSql = " SELECT sl_res_tsq.cod_antib, cod_metodo from sl_res_tsq, sl_rel_grantib,sl_gr_antibio " & _
                          " WHERE sl_res_tsq.cod_antib = sl_rel_grantib.cod_antibio " & _
                          " and sl_rel_grantib.cod_gr_antibio = sl_gr_antibio.cod_gr_antibio " & _
                          " and seq_realiza = " & rs!seq_realiza & " and n_res = " & rs!N_Res & _
                          " and cod_micro = " & BL_TrataStringParaBD(rs!cod_micro) & "" & _
                          " and cod_metodo in ( 2,3) "
                    RsFactAntib.CursorType = adOpenStatic
                    RsFactAntib.CursorLocation = adUseServer
                    RsFactAntib.Open sSql, gConexao
                    If RsFactAntib.RecordCount > 0 Then
                        CodAnaFACTUS = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, , RsFactAntib!cod_metodo, rs!n_req)
                        If ((CodAnaFACTUS <> "-1") And (gInsereAnaFact = True)) Then
                            If (FACTUS_Nova_Analises_IN(sTEpisodio, _
                                                       sEpisodio, _
                                                       sTDoente, _
                                                       rs!Utente, _
                                                       CodAnaFACTUS, _
                                                       rs!n_req, _
                                                       rs!dt_chega, _
                                                       sServico, _
                                                       sIsento, _
                                                       rs!cod_agrup, _
                                                       RsFactAntib.RecordCount) = 1) Then
                                
                                ContaRubFact = ContaRubFact + 1
                            End If
                        End If
                        RsFactAntib.MoveNext
                    End If
                End If
                
            Else
                    
                CodAnaFACTUS = Mapeia_Analises_para_FACTURACAO(rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!n_req)
                    
                If ((CodAnaFACTUS <> "-1") And (gInsereAnaFact = True)) Then
                        
                    'Facturar a an�lise mapeada
                    BL_InicioProcessamento Me, "A enviar an�lises para o " & HIS.nome
                        
                    'inserir a analise no factus o mm nr de vezes que esta se encontra na tabela sl_ana_facturacao
                    For l = 1 To gQuantidadeAFact
                        If (FACTUS_Nova_Analises_IN(sTEpisodio, _
                                                   sEpisodio, _
                                                   sTDoente, _
                                                   rs!Utente, _
                                                   CodAnaFACTUS, _
                                                   rs!n_req, _
                                                   rs!dt_chega, _
                                                   sServico, _
                                                   sIsento, _
                                                   rs!cod_agrup) = 1) Then
                                
                            sSql = "UPDATE sl_realiza SET flg_facturado = 1 " & _
                                  "WHERE seq_realiza = " & rs!seq_realiza

                            BG_ExecutaQuery_ADO sSql
                            BG_Trata_BDErro

                            ContaRubFact = ContaRubFact + 1
                        
                        End If
                        If l < gQuantidadeAFact Then
                            gRsFact.MoveNext
                            CodAnaFACTUS = BL_HandleNull(gRsFact!cod_ana_gh, "")
                        End If
                    Next l
                            
                    BL_FimProcessamento Me
                    
                    gRsFact.Close
                    Set gRsFact = Nothing
                        
                'An�lise j� facturada ou para n�o facturar (com codigo_GH = Null)
                ElseIf CodAnaFACTUS = "-2" Then
                    sSql = "Update sl_realiza set flg_facturado = 1 where seq_realiza = " & rs!seq_realiza
                    BG_ExecutaQuery_ADO sSql
                    BG_Trata_BDErro
                
                End If
                
            End If
                
            '--------------------------------------------------------------------------------------------------------------------
        
            If lInscricao <> rs!n_req And ((CodAnaFACTUS <> "-1") And (gInsereAnaFact = True)) Then
                
                
                Call FACTUS_Insere_Movi_Resp(rs!n_req, _
                                            sTEpisodio, _
                                            sEpisodio, _
                                            BL_HandleNull(rs!n_benef, ""), _
                                            BL_HandleNull(rs!cod_efr, 0), _
                                            sTDoente, rs!Utente)
                                            
                 
                Call FACTUS_Insere_Doe_Fact_Ext(sTEpisodio, sEpisodio, _
                                                sTDoente, rs!Utente, _
                                                BL_HandleNull(rs!nome_ute, ""), _
                                                BL_HandleNull(rs!dt_nasc_ute, ""), _
                                                BL_HandleNull(rs!descr_mor_ute, ""), _
                                                BL_HandleNull(rs!cod_postal_ute, ""), _
                                                BL_HandleNull(rs!telef_ute, ""))
                      
                lInscricao = rs!n_req
                ContaReqFact = ContaReqFact + 1
                                    
            End If
                        
'             If ((CodAnaFACTUS <> "-1") And (gInsereAnaFact = True)) Then
'
'                sSql = "UPDATE sl_realiza SET flg_facturado = '1' " & _
'                    "WHERE cod_inscricao = " & Rs!n_req & " And Seq_Realiza = " & Rs!seq_realiza
'                If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
'                BG_ExecutaQuery_ADO sSql
'
'                lCount = lCount + 1
'
'            ElseIf CodAnaFACTUS = "-2" Then
'
'                sSql = "UPDATE sl_realiza SET flg_facturado = '1' " & _
'                    "WHERE cod_inscricao = " & Rs!n_req & " And Seq_Realiza = " & Rs!seq_realiza
'                If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
'                BG_ExecutaQuery_ADO sSql
'
'                lCount = lCount + 1
'
'            End If
                    
            rs.MoveNext
        Loop
        BL_Fecha_Conexao_Secundaria
        
    Else
        BG_Mensagem mediMsgBox, "N�o h� registos seleccionados para as condi��es de procura", vbExclamation, Me.caption
        BL_FimProcessamento Me

        rs.Close
        Set rs = Nothing

        Exit Sub
    End If

    BL_FimProcessamento Me

    rs.Close
    Set rs = Nothing

    BG_Mensagem mediMsgBox, "Foram enviados para a Factura��o " & ContaReqFact & " requisi��es. ( " & ContaRubFact & " rubricas)", vbInformation, Me.caption

    Exit Sub

TrataErro:
    BG_LogFile_Erros "Erro nr. " & Err.Number & " - " & Err.Description & " no FormFactGeraDados (Gera_Dados_Fact_FACTUS)"
    Exit Sub
End Sub

Sub Gera_Dados_Fact_SONHO()
    Dim CodAnaGH As String
    Dim CodAnaSONHO As String
    Dim HisAberto As Integer
    Dim sql As String
    Dim SQLQuery As String
    Dim l As Integer, i As Integer
    Dim rs As ADODB.recordset
    Dim CriterioTabela As String
    Dim ContaRubFact As Long
    
    On Error GoTo TrataErro
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = "SELECT distinct sl_requis.n_req,sl_requis.dt_chega,sl_requis.n_epis,cod_efr,cod_proven, " & _
                     " sl_requis.t_sit,n_benef,estado_req,t_utente,utente, " & _
                     " sl_realiza.seq_realiza, cod_perfil,cod_ana_c,cod_ana_s, cod_agrup, sl_realiza.flg_facturado, " & _
                     " sl_res_micro.n_res,sl_res_micro.cod_micro,sl_res_micro.flg_tsq,sl_res_micro.prova prova,sl_res_micro.cod_gr_antibio,sl_microrg.metodo_tsq, sl_microrg.prova prova_micro " & _
                     " FROM sl_requis, sl_identif, sl_realiza, sl_res_micro, sl_microrg " & _
                     " WHERE " & _
                     " sl_requis.seq_utente = sl_identif.seq_utente and " & _
                     " sl_requis.n_req = sl_realiza.n_req and " & _
                     " sl_realiza.seq_realiza = sl_res_micro.seq_realiza(+) and " & _
                     " sl_res_micro.cod_micro = sl_microrg.cod_microrg(+) and sl_requis.n_epis is not null and " & _
                     " sl_realiza.flg_estado in (3,4) and (sl_realiza.flg_facturado is null or sl_realiza.flg_facturado = 0) "
    
    
    If EcCodRequisicao <> "" Then
        CriterioTabela = CriterioTabela & " AND sl_requis.n_req = " & EcCodRequisicao & " "

    Else
        CriterioTabela = CriterioTabela & " AND sl_realiza.dt_val between " & BL_TrataDataParaBD(EcDataInicio) & " and " & BL_TrataDataParaBD(EcDataFim)

'        If EcTodas.Value = 0 Then
'            If EcLista.SelCount > 400 Then
'                BG_Mensagem mediMsgBox, "Demasiadas Entidades seleccionadas!" & vbCrLf & "Pode seleccionar um m�ximo de 400 entidades.", vbOKCancel + vbExclamation, Me.Caption
'                BL_FimProcessamento Me
'                Exit Sub
'            ElseIf EcLista.SelCount = 0 Then
'                BG_Mensagem mediMsgBox, "Nenhuma Entidades seleccionada!" & vbCrLf & "Tem de seleccionar pelo menos uma entidade.", vbOKCancel + vbExclamation, Me.Caption
'                BL_FimProcessamento Me
'                Exit Sub
'            Else
'                CriterioTabela = CriterioTabela & " AND sl_requis.cod_efr IN ("
'                For i = 0 To EcLista.ListCount - 1
'                    If EcLista.Selected(i) = True Then
'                        CriterioTabela = CriterioTabela & EcLista.ItemData(i) & ", "
'                    End If
'                Next i
'                CriterioTabela = Mid(CriterioTabela, 1, Len(CriterioTabela) - 2) & ") "
'            End If
'        End If
    End If
    
    
    CriterioTabela = CriterioTabela & " ORDER BY sl_requis.n_req, sl_realiza.seq_realiza, sl_realiza.cod_perfil, sl_realiza.cod_ana_c, sl_realiza.cod_ana_s "
                       
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation
        FuncaoLimpar
    Else
        
        ContaRubFact = 0
        
        'Envia an�lises para o SONHO
        If gSISTEMA_FACTURACAO = "SONHO" Then
            
            HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
            
            If ((HisAberto = 1) And (UCase(HIS.nome) = UCase("SONHO"))) Then
            
                While Not rs.EOF
                        
                    If BL_HandleNull(rs!cod_micro, "") <> "" Then
                        'Facturar a identifica��o do microrganismo
                        
                        CodAnaSONHO = Mapeia_Microrg_para_FACTURACAO(rs!cod_micro, BL_HandleNull(rs!Prova, BL_HandleNull(rs!Prova_micro, "")), rs!n_req)
                        
                        If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                            For l = 1 To gQuantidadeAFact
                                If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                            BL_HandleNull(rs!n_epis, ""), _
                                                            rs!Utente, _
                                                            rs!cod_proven, _
                                                            CodAnaSONHO, _
                                                            rs!dt_chega, _
                                                            rs!n_req, _
                                                            rs!cod_agrup) = 1) Then
                                                            
                                    'actualiza flg_facturado
                                    sql = "UPDATE " & _
                                          "     sl_realiza " & _
                                          "SET " & _
                                          "     flg_facturado = 1 " & _
                                          "WHERE " & _
                                          "     seq_realiza = " & rs!seq_realiza
                                    
                                    BG_ExecutaQuery_ADO sql
                                    BG_Trata_BDErro
                                    
                                    ContaRubFact = ContaRubFact + 1
                                End If
                                
                                If l < gQuantidadeAFact Then
                                    gRsFact.MoveNext
                                    CodAnaSONHO = BL_HandleNull(gRsFact!cod_ana_gh, "")
                                End If
                            Next l
                        End If
                        gRsFact.Close
                        Set gRsFact = Nothing
                        
                        If BL_HandleNull(rs!flg_tsq, "") = "S" Then
                            
                            'Facturar o antibiograma de acordo com o(s) grupo(s) de antibioticos registado(s)
                            If BL_HandleNull(rs!Cod_Gr_Antibio, "") <> "" Then
                                Dim PosAux As Integer
                                Dim CodGrAntibio As String
                                Dim CodGrAntibioAux As String
                                Dim AntibFacturado As Boolean
                                Dim UltimaCarta As String
                                CodGrAntibio = rs!Cod_Gr_Antibio
                                PosAux = 1
                                UltimaCarta = ""
                                While Not PosAux = 0
                                    If UltimaCarta <> CodGrAntibio Then
                                    PosAux = InStr(1, CodGrAntibio, ";")
                                    If PosAux <> 0 Then
                                        CodGrAntibioAux = Mid(CodGrAntibio, 1, PosAux - 1)
                                    Else
                                        CodGrAntibioAux = CodGrAntibio
                                    End If
                                    CodAnaSONHO = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, CodGrAntibioAux, , rs!n_req)
                                    
                                    If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                                        If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                                    BL_HandleNull(rs!n_epis, ""), _
                                                                    rs!Utente, _
                                                                    rs!cod_proven, _
                                                                    CodAnaSONHO, _
                                                                    rs!dt_chega, _
                                                                    rs!n_req, _
                                                                    rs!cod_agrup) = 1) Then
                                            ContaRubFact = ContaRubFact + 1
                                            
                                        End If
                                    End If
                                    UltimaCarta = CodGrAntibioAux
                                    CodGrAntibio = Mid(CodGrAntibio, PosAux + 1)
                                    End If
                                Wend
                                
                            'Facturar o antibiograma de acordo com o m�todo da codifica��o de microorganismos
                            ElseIf BL_HandleNull(rs!Metodo_Tsq, "") <> "" And BL_HandleNull(rs!Metodo_Tsq, "") <> "2" And BL_HandleNull(rs!Metodo_Tsq, "") <> "3" Then
                            
                                CodAnaSONHO = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, , BL_HandleNull(rs!Metodo_Tsq, ""), rs!n_req)
                                
                                If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                                    If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                                BL_HandleNull(rs!n_epis, ""), _
                                                                rs!Utente, _
                                                                rs!cod_proven, _
                                                                CodAnaSONHO, _
                                                                rs!dt_chega, _
                                                                rs!n_req, _
                                                                rs!cod_agrup) = 1) Then
                                        ContaRubFact = ContaRubFact + 1
                                    End If
                                End If
                                
                            End If
                            
                            'Verificar se existem antibi�ticos registados cujo seu grupo de antibi�ticos tenham o m�todo TSQ "E-TEST" ou "BK"
                            Dim RsFactAntib As ADODB.recordset
                            Set RsFactAntib = New ADODB.recordset
                            sql = " SELECT sl_res_tsq.cod_antib, cod_metodo from sl_res_tsq, sl_rel_grantib,sl_gr_antibio " & _
                                  " WHERE sl_res_tsq.cod_antib = sl_rel_grantib.cod_antibio " & _
                                  " and sl_rel_grantib.cod_gr_antibio = sl_gr_antibio.cod_gr_antibio " & _
                                  " and seq_realiza = " & rs!seq_realiza & " and n_res = " & rs!N_Res & _
                                  " and cod_micro = " & BL_TrataStringParaBD(rs!cod_micro) & "" & _
                                  " and cod_metodo in ( 2,3) "
                            RsFactAntib.CursorType = adOpenStatic
                            RsFactAntib.CursorLocation = adUseServer
                            RsFactAntib.Open sql, gConexao
                            If RsFactAntib.RecordCount > 0 Then
                                While Not RsFactAntib.EOF
                                    CodAnaSONHO = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, , RsFactAntib!cod_metodo, rs!n_req)
                                    If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then

                                        If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                                    BL_HandleNull(rs!n_epis, ""), _
                                                                    rs!Utente, _
                                                                    rs!cod_proven, _
                                                                    CodAnaSONHO, _
                                                                    rs!dt_chega, _
                                                                    rs!n_req, _
                                                                    rs!cod_agrup) = 1) Then
                                            
                                            ContaRubFact = ContaRubFact + 1
                                            
                                        End If
                                    End If
                                    RsFactAntib.MoveNext
                                Wend
                            End If
                        End If
                    
                    Else
                        
                        CodAnaSONHO = Mapeia_Analises_para_FACTURACAO(rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!n_req, rs!cod_agrup)
                        
                        If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                            
                            'Facturar a an�lise mapeada
                            BL_InicioProcessamento Me, "A enviar an�lises para o " & HIS.nome
                            
                            'inserir a analise no sonho o mm nr de vezes que esta se encontra na tabela sl_ana_facturacao
                            For l = 1 To gQuantidadeAFact
                                If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                           BL_HandleNull(rs!n_epis, ""), _
                                                           rs!Utente, _
                                                           rs!cod_proven, _
                                                           CodAnaSONHO, _
                                                           rs!dt_chega, _
                                                           rs!n_req, _
                                                           rs!cod_agrup) = 1) Then
                                    
                                    sql = "UPDATE " & _
                                          "     sl_realiza " & _
                                          "SET " & _
                                          "     flg_facturado = 1 " & _
                                          "WHERE " & _
                                          "     seq_realiza = " & rs!seq_realiza
                                    
                                    BG_ExecutaQuery_ADO sql
                                    BG_Trata_BDErro
                                    
                                    ContaRubFact = ContaRubFact + 1
                                
                                End If
                                If l < gQuantidadeAFact Then
                                    gRsFact.MoveNext
                                    CodAnaSONHO = BL_HandleNull(gRsFact!cod_ana_gh, "")
                                End If
                            Next l
                            
                            BL_FimProcessamento Me
                            
                            gRsFact.Close
                            Set gRsFact = Nothing
                        
                        'An�lise j� facturada ou para n�o facturar (com codigo_GH = Null)
                        ElseIf CodAnaSONHO = "-2" Then
                            sql = "Update sl_realiza set flg_facturado = 1 where seq_realiza = " & rs!seq_realiza
                            BG_ExecutaQuery_ADO sql
                            BG_Trata_BDErro
                        
'                        'An�lise sem correspond�ncia no sonho
'                        ElseIf CodAnaSONHO = "-1" Then
'                            EcListaAna.AddItem Rs!cod_ana_s
                        End If
                    
                    End If
                    rs.MoveNext
                Wend
            End If
            BL_Fecha_conexao_HIS
        End If
        
        BL_FimProcessamento Me
        
        Call BG_Mensagem(mediMsgBox, ContaRubFact & " rubricas enviadas para o " & UCase(gSISTEMA_FACTURACAO) & ".", vbOKOnly + vbInformation, App.ProductName)
        
    End If
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "Erro nr. " & Err.Number & " - " & Err.Description & " no FormFactGeraDados (Gera_Dados_Fact_SONHO)"
    Exit Sub

End Sub

Sub Funcao_DataActual()
    BL_PreencheData CampoActivo, Me.ActiveControl
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = "Factura��o: Gera��o de Dados"
    Me.left = 540 '1500
    Me.top = 50 '450 '900
    Me.Width = 5175 '3750
    Me.Height = 5360 '5445 '4995 '2505

    Set CampoDeFocus = EcCodRequisicao
    
    If gLAB = "HMP" Then
        EcTodas.Visible = False
        EcLista.Visible = False
        LaEntidades.Visible = False
    End If

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito

    estado = 1
    BG_StackJanelas_Push Me

End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormGeraDados = Nothing

End Sub

Sub LimpaCampos()

    Me.SetFocus

    EcDataInicio = ""
    EcDataFim = ""
    EcCodRequisicao.Text = ""

End Sub


Sub DefTipoCampos()

    EcDataInicio.Tag = mediTipoData
    EcDataFim.Tag = mediTipoData
    EcCodRequisicao.Tag = adInteger

End Sub

Sub PreencheValoresDefeito()
    If StrComp(UCase(HIS.nome), UCase("SONHO"), vbTextCompare) <> 0 Then
    'If StrComp(gHIS_Nome, cParamHIS_Sonho, vbTextCompare) <> 0 Then
        BG_PreencheComboBD_ADO "sl_efr", "cod_efr", "descr_efr", EcLista, mediAscComboDesignacao
    Else
        EcLista.List(0) = ""
        EcLista.BackColor = vbInactiveBorder
        LaEntidades.Enabled = False
        EcLista.Enabled = False
        EcTodas.Enabled = False
    End If
    
'    If gFacturaFechadas = 1 Then
        EcEscolhaInicio.value = Bg_DaData_ADO
        EcEscolhaFim.value = Bg_DaData_ADO
'    Else
'        EcDataInicio = Bg_DaData_ADO
'        EcDataFim = Bg_DaData_ADO
'        EcEscolhaInicio.Value = EcDataInicio
'        EcEscolhaFim.Value = EcDataFim
'    End If


End Sub

Sub VerificaEpisodio(sTipoEpisodio As String, sEpisodio As String, ByRef sAmbInt As String)
'    Dim CmdAmbInt As New ADODB.Command
'    Dim PmtTEpisodio As ADODB.Parameter
'    Dim PmtEpisodio As ADODB.Parameter
'    Dim PmtRetAmbInt As ADODB.Parameter
'    Dim RsAmbInt As New ADODB.Recordset
'    Dim PmtRetorno As ADODB.Parameter
'    Dim iBD_Aberta As Integer
''    Dim sFlgAmbInt As String
'
'    On Error GoTo TrataErro
'
'    If sTipoEpisodio = "" Then Exit Sub
'    If sEpisodio = "" Then Exit Sub
'
'    'Chamada das fun��es de liga��o ao GH
'    BG_LimpaPassaParams
'    gPassaParams.ID = "INICIALIZA_DSN_SEC"
'    gPassaParams.Param(0) = BG_DaParamAmbiente(cAmbitoGeral, "HIS_Nome")
'    gPassaParams.Param(1) = BG_DaParamAmbiente(cAmbitoGeral, "HIS_DataSource")
'    gPassaParams.Param(2) = BG_DaParamAmbiente(cAmbitoGeral, "HIS_UID")
'    If BG_DaParamAmbiente(cAmbitoGeral, "HIS_PWD") = "-1" Then
'        gPassaParams.Param(3) = ""
'    Else
'        gPassaParams.Param(3) = BG_DaParamAmbiente(cAmbitoGeral, "HIS_PWD")
'    End If
'    gPassaParams.Param(4) = BG_DaParamAmbiente(cAmbitoGeral, "HIS_LoginTimeout")
'
'    BL_InicializaDSN_Sec
'
'    iBD_Aberta = BL_ConexaoBD_Secundaria_Abre
'
'    If iBD_Aberta = 0 Then
'        Exit Sub
'    End If
'
'    gSGBD_Sec = BG_DaParamAmbiente(cAmbitoGeral, "HIS_SGBD")
'
'    BL_DefineCaracteristicasBD_Secundaria
'
'    Set CmdAmbInt.ActiveConnection = gConexaoSec
'
'    With CmdAmbInt
'        .CommandText = "GH.VERIFICA_AMB_INT_EXT"
'        .CommandType = adCmdStoredProc
'    End With
'
'    Set PmtTEpisodio = CmdAmbInt.CreateParameter("t_episodio_in", adVarChar, adParamInput, 2000)
'
'    Set PmtEpisodio = CmdAmbInt.CreateParameter("episodio_in", adVarChar, adParamInput, 2000)
'
'    Set PmtRetAmbInt = CmdAmbInt.CreateParameter("flg_amb_int_out", adVarChar, adParamInputOutput, 2000)
'
'    CmdAmbInt.Parameters.Append PmtTEpisodio
'    CmdAmbInt.Parameters.Append PmtEpisodio
'    CmdAmbInt.Parameters.Append PmtRetAmbInt
'
'    ' executar o comando e por o valor num recordset
'    CmdAmbInt.Parameters("t_episodio_in") = sTipoEpisodio
'    CmdAmbInt.Parameters("episodio_in") = sEpisodio
'    CmdAmbInt.Parameters("flg_amb_int_out") = Null
'
'    CmdAmbInt.Execute
'
'    BG_LogFile_erros "P0: " & CmdAmbInt.Parameters.Item(0).Value & " P1: " & _
'        CmdAmbInt.Parameters.Item(1).Value & " P2: " & CmdAmbInt.Parameters.Item(2).Value
'
'    sAmbInt = CmdAmbInt.Parameters.Item(2).Value
'
'    Set CmdAmbInt = Nothing
'    Set PmtEpisodio = Nothing
'    Set PmtTEpisodio = Nothing
'    Set PmtRetAmbInt = Nothing
'
'    BL_ConexaoBD_Secundaria_Fecha
'
'    Exit Sub
'
'TrataErro:
'    BG_TrataErro "VerificaEpisodio", Err
'    Resume Next
End Sub

Sub FuncaoLimpar()
    LimpaCampos
    CampoDeFocus.SetFocus
End Sub

Sub FuncaoEstadoAnterior()
    Unload Me
End Sub

Sub Gera_Dados_Fact_GH()
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim lNrFact As Long, lNrReq As Long, lInscricao As Long
    Dim lCount As Long
    Dim lExame As Long, lEntidade As Long
    Dim sTDoente As String, sTEpisodio As String, sEpisodio As String
    Dim sIsento As String, sBenef As String, sServico As String, sEFR As String, sNCred As String, sCodServExec As String
    Dim iCodNaoIsento As Integer, i As Integer, iRes As Integer, j As Integer
    Dim iBD_Aberta As Integer
    Dim bFacturar As Boolean
    Dim CodAnaGH As String, sAnalise As String
    Dim l As Integer
    Dim ContaRubFact As Long
    Dim HisAberto As Integer

    On Error GoTo TrataErro

    BL_InicioProcessamento Me, "Gera��o a decorrer! "
    DoEvents


    sSql = "SELECT distinct sl_requis.n_req,sl_requis.n_req_assoc, " & _
         " sl_requis.req_aux,sl_requis.dt_chega,sl_requis.n_epis,cod_efr,cod_proven,sl_requis.dt_fact, " & _
         " sl_requis.t_sit,n_benef,estado_req,t_utente,utente,cod_isencao,n_benef, " & _
         " nome_ute,dt_nasc_ute,descr_mor_ute,cod_postal_ute,telef_ute, " & _
         " sl_realiza.seq_realiza, cod_perfil,cod_ana_c,cod_ana_s, cod_agrup, " & _
         " sl_res_micro.n_res,sl_res_micro.cod_micro,sl_res_micro.flg_tsq,sl_res_micro.prova prova, " & _
         " sl_res_micro.cod_gr_antibio,sl_microrg.metodo_tsq, sl_microrg.prova prova_micro " & _
         " FROM sl_requis, sl_identif, sl_realiza, sl_res_micro, sl_microrg " & _
         " WHERE " & _
         " sl_requis.seq_utente = sl_identif.seq_utente and " & _
         " sl_requis.n_req = sl_realiza.n_req and " & _
         " sl_realiza.seq_realiza = sl_res_micro.seq_realiza(+) and " & _
         " sl_res_micro.cod_micro = sl_microrg.cod_microrg(+) and " & _
         " sl_realiza.flg_estado in (3,4) and (sl_realiza.flg_facturado is null or sl_realiza.flg_facturado = 0) and sl_requis.mot_fact_rej is null "


    If EcCodRequisicao <> "" Then
        sSql = sSql & " AND sl_requis.n_req = " & EcCodRequisicao & " "

    Else
        Select Case gLAB
            Case "BIO", "GM", "CITO"
                sSql = sSql & " AND sl_requis.dt_fact between " & BL_TrataDataParaBD(EcDataInicio) & " and " & BL_TrataDataParaBD(EcDataFim)
            Case Else
                sSql = sSql & " AND sl_requis.dt_chega between " & BL_TrataDataParaBD(EcDataInicio) & " and " & BL_TrataDataParaBD(EcDataFim)
        End Select

        If EcTodas.value = 0 Then
            If EcLista.SelCount > 400 Then
                BG_Mensagem mediMsgBox, "Demasiadas Entidades seleccionadas!" & vbCrLf & "Pode seleccionar um m�ximo de 400 entidades.", vbOKCancel + vbExclamation, Me.caption
                BL_FimProcessamento Me
                Exit Sub
            ElseIf EcLista.SelCount = 0 Then
                BG_Mensagem mediMsgBox, "Nenhuma Entidades seleccionada!" & vbCrLf & "Tem de seleccionar pelo menos uma entidade.", vbOKCancel + vbExclamation, Me.caption
                BL_FimProcessamento Me
                Exit Sub
            Else
                sSql = sSql & " AND sl_requis.cod_efr IN ("
                For i = 0 To EcLista.ListCount - 1
                    If EcLista.Selected(i) = True Then
                        sSql = sSql & EcLista.ItemData(i) & ", "
                    End If
                Next i
                sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
            End If
        End If
    End If

    sSql = sSql & " ORDER BY sl_requis.n_req,sl_realiza.seq_realiza, sl_realiza.cod_perfil, sl_realiza.cod_ana_c, sl_realiza.cod_ana_s "

    If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic

    lCount = 0
    If rs.RecordCount <> 0 Then
       
        HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
        
        If ((HisAberto = 1) And (UCase(HIS.nome) = UCase("GH"))) Then
        
        'BL_InicioProcessamento Me, "A enviar an�lises para o " & HIS.Nome
        
        Do While Not rs.EOF

            sTDoente = rs!t_utente
            sTEpisodio = rs!t_sit
            sEpisodio = BL_HandleNull(rs!n_epis, "000")
            Select Case gLAB
                Case "CITO"
                    sServico = rs!n_req
                    sNCred = BL_HandleNull(rs!n_Req_assoc, "")
                    sAnalise = rs!req_aux
                    sCodServExec = 2
                Case "BIO"
                    sServico = rs!n_req
                    sNCred = BL_HandleNull(rs!n_Req_assoc, "")
                    sAnalise = rs!cod_agrup
                    sCodServExec = 3
                Case "GM"
                    sServico = rs!n_req
                    sNCred = BL_HandleNull(rs!n_Req_assoc, "")
                    sAnalise = rs!req_aux
                    sCodServExec = 4
                Case Else
                    sServico = BL_HandleNull(rs!cod_proven, 0)
                    sNCred = ""
                    sAnalise = rs!cod_agrup
                    sCodServExec = gCodServExec
            End Select
            lNrReq = rs!n_req
            sEFR = rs!cod_efr
            
            'Facturar a identifica��o do microrganismo
            If BL_HandleNull(rs!cod_micro, "") <> "" Then
                
                CodAnaGH = Mapeia_Microrg_para_FACTURACAO(rs!cod_micro, BL_HandleNull(rs!Prova, BL_HandleNull(rs!Prova_micro, "")), rs!n_req)
                
                If ((CodAnaGH <> "-1") And (gInsereAnaFact = True)) Then
                    For l = 1 To gQuantidadeAFact
                        If (GH_Nova_ANALISES_IN(sTEpisodio, _
                                                    sEpisodio, _
                                                    sTDoente, _
                                                    rs!Utente, _
                                                    CodAnaGH, _
                                                    lNrReq, _
                                                    rs!dt_chega, _
                                                    BL_HandleNull(rs!dt_fact, ""), _
                                                    sServico, _
                                                    sCodServExec, _
                                                    sNCred, _
                                                    sEFR, _
                                                    sAnalise) = 1) Then
                            
                            'actualiza flg_facturado
                            sSql = "UPDATE sl_realiza SET flg_facturado = 1 " & _
                                  "WHERE seq_realiza = " & rs!seq_realiza
                            BG_ExecutaQuery_ADO sSql
                            BG_Trata_BDErro
                            
                            ContaRubFact = ContaRubFact + 1
                        End If
                            
                        If l < gQuantidadeAFact Then
                            gRsFact.MoveNext
                            CodAnaGH = BL_HandleNull(gRsFact!cod_ana_gh, "")
                        End If
                    Next l
                End If
                gRsFact.Close
                Set gRsFact = Nothing
                    
                If BL_HandleNull(rs!flg_tsq, "") = "S" Then
                        
                    'Facturar o antibiograma de acordo com o(s) grupo(s) de antibioticos registado(s)
                    If BL_HandleNull(rs!Cod_Gr_Antibio, "") <> "" Then
                        Dim PosAux As Integer
                        Dim CodGrAntibio As String
                        Dim CodGrAntibioAux As String
                        Dim UltimaCarta As String
                        CodGrAntibio = rs!Cod_Gr_Antibio
                        PosAux = 1
                        UltimaCarta = ""
                        While Not PosAux = 0
                            If UltimaCarta <> CodGrAntibio Then
                                PosAux = InStr(1, CodGrAntibio, ";")
                                If PosAux <> 0 Then
                                    CodGrAntibioAux = Mid(CodGrAntibio, 1, PosAux - 1)
                                Else
                                    CodGrAntibioAux = CodGrAntibio
                                End If
                                CodAnaGH = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, CodGrAntibioAux, , rs!n_req)
                                
                                If ((CodAnaGH <> "-1") And (gInsereAnaFact = True)) Then
                                    If (GH_Nova_ANALISES_IN(sTEpisodio, _
                                                                sEpisodio, _
                                                                sTDoente, _
                                                                rs!Utente, _
                                                                CodAnaGH, _
                                                                lNrReq, _
                                                                rs!dt_chega, _
                                                                BL_HandleNull(rs!dt_fact, ""), _
                                                                sServico, _
                                                                sCodServExec, _
                                                                sNCred, _
                                                                sEFR, _
                                                                sAnalise) = 1) Then
                                            
                                        ContaRubFact = ContaRubFact + 1
                                    End If
                                End If
                                UltimaCarta = CodGrAntibioAux
                                CodGrAntibio = Mid(CodGrAntibio, PosAux + 1)
                            End If
                        Wend
                            
                    'Facturar o antibiograma de acordo com o m�todo da codifica��o de microorganismos
                    ElseIf BL_HandleNull(rs!Metodo_Tsq, "") <> "" And BL_HandleNull(rs!Metodo_Tsq, "") <> "2" And BL_HandleNull(rs!Metodo_Tsq, "") <> "3" Then
                    
                        CodAnaGH = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, , BL_HandleNull(rs!Metodo_Tsq, ""), rs!n_req)
                        
                        If ((CodAnaGH <> "-1") And (gInsereAnaFact = True)) Then
                            If (GH_Nova_ANALISES_IN(sTEpisodio, _
                                                        sEpisodio, _
                                                        sTDoente, _
                                                        rs!Utente, _
                                                        CodAnaGH, _
                                                        lNrReq, _
                                                        rs!dt_chega, _
                                                        BL_HandleNull(rs!dt_fact, ""), _
                                                        sServico, _
                                                        sCodServExec, _
                                                        sNCred, _
                                                        sEFR, _
                                                        sAnalise) = 1) Then
                                
                                ContaRubFact = ContaRubFact + 1
                            End If
                        End If
                    End If
                            
                    'Verificar se existem antibi�ticos registados cujo seu grupo de antibi�ticos tenham o m�todo TSQ "E-TEST" ou "BK"
                    Dim RsFactAntib As ADODB.recordset
                    Set RsFactAntib = New ADODB.recordset
                    sSql = " SELECT sl_res_tsq.cod_antib, cod_metodo from sl_res_tsq, sl_rel_grantib,sl_gr_antibio " & _
                          " WHERE sl_res_tsq.cod_antib = sl_rel_grantib.cod_antibio " & _
                          " and sl_rel_grantib.cod_gr_antibio = sl_gr_antibio.cod_gr_antibio " & _
                          " and seq_realiza = " & rs!seq_realiza & " and n_res = " & rs!N_Res & _
                          " and cod_micro = " & BL_TrataStringParaBD(rs!cod_micro) & "" & _
                          " and cod_metodo in ( 2,3) "
                    RsFactAntib.CursorType = adOpenStatic
                    RsFactAntib.CursorLocation = adUseServer
                    RsFactAntib.Open sSql, gConexao
                    If RsFactAntib.RecordCount > 0 Then
                        CodAnaGH = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, , RsFactAntib!cod_metodo, rs!n_req)
                        If ((CodAnaGH <> "-1") And (gInsereAnaFact = True)) Then
                            If (GH_Nova_ANALISES_IN(sTEpisodio, _
                                                        sEpisodio, _
                                                        sTDoente, _
                                                        rs!Utente, _
                                                        CodAnaGH, _
                                                        lNrReq, _
                                                        rs!dt_chega, _
                                                        BL_HandleNull(rs!dt_fact, ""), _
                                                        sServico, _
                                                        sCodServExec, _
                                                        sNCred, _
                                                        sEFR, _
                                                        sAnalise) = 1) Then
                                
                                ContaRubFact = ContaRubFact + 1
                            End If
                        End If
                        RsFactAntib.MoveNext
                    End If
                End If
                
            Else
                    
                CodAnaGH = Mapeia_Analises_para_FACTURACAO(rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!n_req)
                    
                If ((CodAnaGH <> "-1") And (gInsereAnaFact = True)) Then
                        
                    'Facturar a an�lise mapeada
                    
                        
                    'inserir a analise no factus o mm nr de vezes que esta se encontra na tabela sl_ana_facturacao
                    For l = 1 To gQuantidadeAFact
                        If (GH_Nova_ANALISES_IN(sTEpisodio, _
                                                    sEpisodio, _
                                                    sTDoente, _
                                                    rs!Utente, _
                                                    CodAnaGH, _
                                                    lNrReq, _
                                                    rs!dt_chega, _
                                                    BL_HandleNull(rs!dt_fact, ""), _
                                                    sServico, _
                                                    sCodServExec, _
                                                    sNCred, _
                                                    sEFR, _
                                                    sAnalise) = 1) Then
                                
                            sSql = "UPDATE sl_realiza SET flg_facturado = 1 " & _
                                  "WHERE seq_realiza = " & rs!seq_realiza

                            BG_ExecutaQuery_ADO sSql
                            BG_Trata_BDErro

                            ContaRubFact = ContaRubFact + 1
                        
                        End If
                        If l < gQuantidadeAFact Then
                            gRsFact.MoveNext
                            CodAnaGH = BL_HandleNull(gRsFact!cod_ana_gh, "")
                        End If
                    Next l
                            
                    
                    
                    gRsFact.Close
                    Set gRsFact = Nothing
                        
                'An�lise j� facturada ou para n�o facturar (com codigo_GH = Null)
                ElseIf CodAnaGH = "-2" Then
                    sSql = "Update sl_realiza set flg_facturado = 1 where seq_realiza = " & rs!seq_realiza
                    BG_ExecutaQuery_ADO sSql
                    BG_Trata_BDErro
                
                End If
                
            End If
                
            '--------------------------------------------------------------------------------------------------------------------
                    
            rs.MoveNext
        Loop
        BL_FimProcessamento Me
        BL_Fecha_conexao_HIS
        End If
        
    Else
        BG_Mensagem mediMsgBox, "N�o h� registos seleccionados para as condi��es de procura", vbExclamation, Me.caption
        BL_FimProcessamento Me

        rs.Close
        Set rs = Nothing

        Exit Sub
    End If

    BL_FimProcessamento Me

    rs.Close
    Set rs = Nothing

    BG_Mensagem mediMsgBox, "Foram enviados para a Factura��o " & ContaRubFact & " rubricas.", vbInformation, Me.caption

    Exit Sub

TrataErro:
    BG_LogFile_Erros "Erro nr. " & Err.Number & " - " & Err.Description & " no FormFactGeraDados (Gera_Dados_Fact_GH)"
    Exit Sub
End Sub

Sub Gera_Dados_Fact()
    Dim CodAnaGH As String
    Dim CodAnaSONHO As String
    Dim HisAberto As Integer
    Dim sql As String
    Dim SQLQuery As String
    Dim l As Integer, i As Integer
    Dim rs As ADODB.recordset
    Dim rsReq As ADODB.recordset
    Dim CriterioTabela As String
    Dim ContaRubFact As Long
    Dim ContaReqFact As Long
    Dim SQLReq As String
    Dim SqlUpd As String
    Dim PosAux As Integer
    Dim CodGrAntibio As String
    Dim CodGrAntibioAux As String
    Dim AntibFacturado As Boolean
    Dim UltimaCarta As String
    Dim ContaRubReq As Long
    Dim SqlIns As String

    
    On Error GoTo TrataErro
    
    ContaRubFact = 0
    ContaReqFact = 0
    ContaRubReq = 0
    
    If UCase(HIS.nome) = "SONHO" Then
        HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
    ElseIf UCase(HIS.nome) = "GH" Then
        HisAberto = 1
    End If
    If HisAberto = 1 Then
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rsReq = New ADODB.recordset
    
    'testar
'    SQLReq = "select n_req,utente,n_proc_1, dt_chega, sl_requis.t_sit,sl_requis.n_epis,cod_efr,cod_proven,n_benef,estado_req " & _
'            "from sl_requis, sl_identif " & _
'            "where sl_requis.seq_utente = sl_identif.seq_utente and " & _
'            "(user_fecho is not null or dt_fecho is not null) and dt_fact is null and sl_requis.n_epis is not null and dt_chega between " & BL_TrataDataParaBD("01-01-2006") & " and " & BL_TrataDataParaBD("31-07-2006") & _
'            " "
    
    'final
    SQLReq = "select x1.n_req,t_utente, utente,n_proc_1, x1.dt_chega, x1.t_sit,x1.n_epis,x1.cod_efr,x1.cod_proven,x1.n_benef,x1.estado_req, x3.cod_fact " & _
                " from sl_requis x1, sl_identif x2, gr_empr_inst x3 " & _
                " where x1.seq_utente = x2.seq_utente and " & _
                " x3.cod_empr = x1.cod_local " & _
                " AND dt_fact is null and dt_chega >= " & BL_TrataDataParaBD("01-08-2006")
    
    If gFacturaFechadas = 1 Then
        SQLReq = SQLReq & " and (user_fecho is not null or dt_fecho is not null) "
    End If
    If gFacturaPorProcesso <> 1 Then
        SQLReq = SQLReq & " and x1.n_epis is not null "
    End If

    If EcCodRequisicao <> "" Then
        SQLReq = SQLReq & " AND x1.n_req = " & EcCodRequisicao & " "
    End If
    If EcDataInicio.Text <> "" And EcDataFim.Text <> "" Then
        SQLReq = SQLReq & " AND x1.dt_chega between " & BL_TrataDataParaBD(EcDataInicio) & " and " & BL_TrataDataParaBD(EcDataFim)
    End If
    

    rsReq.CursorType = adOpenStatic
    rsReq.CursorLocation = adUseServer
    rsReq.Open SQLReq, gConexao
    If rsReq.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhuma requisi��o para facturar!", vbExclamation
        FuncaoLimpar
    Else
        While Not rsReq.EOF
            ContaRubReq = 0
            
            Set rs = New ADODB.recordset
            
            'SOLIVEIRA 19.09.2007
            CriterioTabela = "SELECT distinct sl_realiza.n_req, sl_realiza.seq_realiza, cod_perfil,cod_ana_c,cod_ana_s, cod_agrup, " & _
                         " sl_res_micro.n_res,sl_res_micro.cod_micro,sl_res_micro.flg_tsq,sl_res_micro.flg_testes,sl_res_micro.prova prova,sl_res_micro.cod_gr_antibio,sl_microrg.metodo_tsq, sl_microrg.prova prova_micro, sl_realiza.ord_ana_compl, sl_realiza.ord_ana_perf" & _
                         " FROM  sl_realiza, sl_res_micro, sl_microrg " & _
                         " WHERE " & _
                         " sl_realiza.seq_realiza = sl_res_micro.seq_realiza(+) and " & _
                         " sl_res_micro.cod_micro = sl_microrg.cod_microrg(+) and " & _
                         " (sl_realiza.flg_facturado is null or sl_realiza.flg_facturado = 0) and " & _
                         " sl_realiza.n_req = " & rsReq!n_req
            
            If gFacturaFechadas <> 1 Then
                CriterioTabela = CriterioTabela & " and sl_realiza.flg_estado in ('3','4') "
            End If

            'soliveira 19.09.2007
            'CriterioTabela = CriterioTabela & " ORDER BY sl_realiza.seq_realiza, sl_realiza.cod_perfil, sl_realiza.cod_ana_c, sl_realiza.cod_ana_s "
            CriterioTabela = CriterioTabela & " ORDER BY sl_realiza.cod_perfil,sl_realiza.cod_ana_c, sl_realiza.ord_ana_compl, sl_realiza.ord_ana_perf, sl_realiza.cod_ana_s desc "
            
            rs.CursorLocation = adUseServer
            rs.CursorType = adOpenStatic
            rs.Open CriterioTabela, gConexao
            If rs.RecordCount > 0 Then
                    While Not rs.EOF
                        If BL_HandleNull(rs!cod_micro, "") <> "" Then
                            'nova fun��o pela necessidade de facturar mais do q uma rubrica para cada analise
                            'Factura a identifica��o do microrganismo
                            If IF_Factura_Microrg(rsReq!n_req, _
                                                            rsReq!t_sit, _
                                                            rsReq!n_epis, _
                                                            rsReq!dt_chega, _
                                                            BL_HandleNull(rsReq!n_proc_1, ""), _
                                                            BL_HandleNull(rsReq!cod_proven, ""), _
                                                            rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!cod_agrup, _
                                                            rs!cod_micro, _
                                                            BL_HandleNull(rs!Prova, BL_HandleNull(rs!Prova_micro, "")), rsReq!t_utente, rsReq!Utente, BL_HandleNull(rsReq!cod_fact, -1)) = True Then
                                
                                
'                                Sql = "UPDATE sl_realiza SET flg_facturado = 1 WHERE seq_realiza = " & rs!seq_realiza
'                                BG_ExecutaQuery_ADO Sql
'                                BG_Trata_BDErro
                                
                                ContaRubFact = ContaRubFact + 1
                                ContaRubReq = ContaRubReq + 1
                            End If

                            
                        
                            If BL_HandleNull(rs!flg_tsq, "") = "S" Then
                                'Facturar o antibiograma de acordo com o(s) grupo(s) de antibioticos registado(s)
                                If BL_HandleNull(rs!Cod_Gr_Antibio, "") <> "" Then
                                    
                                    CodGrAntibio = rs!Cod_Gr_Antibio
                                    PosAux = 1
                                    UltimaCarta = ""
                                    While Not PosAux = 0
                                        If UltimaCarta <> CodGrAntibio Then
                                            PosAux = InStr(1, CodGrAntibio, ";")
                                            If PosAux <> 0 Then
                                                CodGrAntibioAux = Mid(CodGrAntibio, 1, PosAux - 1)
                                            Else
                                                CodGrAntibioAux = CodGrAntibio
                                            End If
                                            If IF_Factura_Antibiograma(rsReq!n_req, _
                                                                rsReq!t_sit, _
                                                                rsReq!n_epis, _
                                                                rsReq!dt_chega, _
                                                                BL_HandleNull(rsReq!n_proc_1, ""), _
                                                                BL_HandleNull(rsReq!cod_proven, ""), _
                                                                rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!cod_agrup, _
                                                                CodGrAntibioAux, _
                                                                 rs!cod_micro, rs!seq_realiza, rsReq!t_utente, rsReq!Utente, BL_HandleNull(rsReq!cod_fact, -1)) = True Then
                                                                
'                                                Sql = "UPDATE sl_realiza SET flg_facturado = 1 WHERE seq_realiza = " & rs!seq_realiza
'                                                BG_ExecutaQuery_ADO Sql
'                                                BG_Trata_BDErro
                                        
                                                ContaRubFact = ContaRubFact + 1
                                                ContaRubReq = ContaRubReq + 1
                                            End If
                                            UltimaCarta = CodGrAntibioAux
                                            CodGrAntibio = Mid(CodGrAntibio, PosAux + 1)
                                        Else
                                            PosAux = 0
                                        End If
                                    Wend
                                'Facturar a carta de acordo com o m�todo tsq da codifica��o de microorganismos, excepto para metodos 2 ou 3 (facturados no fim)
                                ElseIf BL_HandleNull(rs!Metodo_Tsq, "") <> "" And BL_HandleNull(rs!Metodo_Tsq, "") <> "2" And BL_HandleNull(rs!Metodo_Tsq, "") <> "3" Then
                                
                                    If IF_Factura_Antibiograma(rsReq!n_req, _
                                                        rsReq!t_sit, _
                                                        rsReq!n_epis, _
                                                        rsReq!dt_chega, _
                                                        BL_HandleNull(rsReq!n_proc_1, ""), _
                                                        BL_HandleNull(rsReq!cod_proven, ""), _
                                                        rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!cod_agrup, _
                                                        "", _
                                                         rs!cod_micro, rs!seq_realiza, rsReq!t_utente, rsReq!Utente, BL_HandleNull(rsReq!cod_fact, -1)) = True Then
                                                        
'                                        Sql = "UPDATE sl_realiza SET flg_facturado = 1 WHERE seq_realiza = " & rs!seq_realiza
'                                        BG_ExecutaQuery_ADO Sql
'                                        BG_Trata_BDErro
                                
                                        ContaRubFact = ContaRubFact + 1
                                        ContaRubReq = ContaRubReq + 1
                                    End If
                                    
                                End If
                                
                                'Verifica se tem antibioticos cujo metodo seja 2 ou 3 (e-test e bk) para facturar
                                If IF_Factura_Antibioticos(rsReq!n_req, _
                                                    rsReq!t_sit, _
                                                    rsReq!n_epis, _
                                                    rsReq!dt_chega, _
                                                    BL_HandleNull(rsReq!n_proc_1, ""), _
                                                    BL_HandleNull(rsReq!cod_proven, ""), _
                                                    rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!cod_agrup, _
                                                    CodGrAntibio, _
                                                    "", rs!cod_micro, rs!seq_realiza, rsReq!t_utente, rsReq!Utente, BL_HandleNull(rsReq!cod_fact, -1)) = True Then
                                                    
'                                    Sql = "UPDATE sl_realiza SET flg_facturado = 1 WHERE seq_realiza = " & rs!seq_realiza
'                                    BG_ExecutaQuery_ADO Sql
'                                    BG_Trata_BDErro
                            
                                    ContaRubFact = ContaRubFact + 1
                                    ContaRubReq = ContaRubReq + 1
                                End If
                            End If
                            
                            If BL_HandleNull(rs!flg_testes, "") = "S" Then
                                'factura testes associados ao microrganismo
                                If IF_Factura_Teste(rsReq!n_req, _
                                                    rsReq!t_sit, _
                                                    rsReq!n_epis, _
                                                    rsReq!dt_chega, _
                                                    BL_HandleNull(rsReq!n_proc_1, ""), _
                                                    BL_HandleNull(rsReq!cod_proven, ""), _
                                                    rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!cod_agrup, _
                                                    rs!cod_micro, rs!seq_realiza, rsReq!t_utente, rsReq!Utente, BL_HandleNull(rsReq!cod_fact, -1)) = True Then
                                                    
'                                    Sql = "UPDATE sl_realiza SET flg_facturado = 1 WHERE seq_realiza = " & rs!seq_realiza
'                                    BG_ExecutaQuery_ADO Sql
'                                    BG_Trata_BDErro
                            
                                    ContaRubFact = ContaRubFact + 1
                                    ContaRubReq = ContaRubReq + 1
                                End If
                            End If
                        
                        Else
                            'verifica se tem testes para facturar
                            'para situa��es onde microrganismo = cSemResultado mas com identifica��o de testes
                            If gLAB = "CHVNG" Then
                                If IF_Factura_Teste(rsReq!n_req, _
                                                    rsReq!t_sit, _
                                                    rsReq!n_epis, _
                                                    rsReq!dt_chega, _
                                                    BL_HandleNull(rsReq!n_proc_1, ""), _
                                                    rsReq!cod_proven, _
                                                    rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!cod_agrup, _
                                                    BL_HandleNull(rs!cod_micro, ""), rs!seq_realiza, rsReq!t_utente, rsReq!Utente, BL_HandleNull(rsReq!cod_fact, -1)) = True Then
                                                    
'                                    Sql = "UPDATE sl_realiza SET flg_facturado = 1 WHERE seq_realiza = " & rs!seq_realiza
'                                    BG_ExecutaQuery_ADO Sql
'                                    BG_Trata_BDErro
                            
                                    ContaRubFact = ContaRubFact + 1
                                    ContaRubReq = ContaRubReq + 1
                                End If
                            End If
                            
                            If IF_Factura_Analise(rsReq!n_req, _
                                                    rsReq!cod_efr, _
                                                        rsReq!t_sit, _
                                                        rsReq!n_epis, _
                                                        rsReq!dt_chega, _
                                                        BL_HandleNull(rsReq!n_proc_1, ""), _
                                                        BL_HandleNull(rsReq!cod_proven, ""), _
                                                        rs!Cod_Perfil, _
                                                        rs!cod_ana_c, _
                                                        rs!cod_ana_s, _
                                                        rs!cod_agrup, rs!seq_realiza, rsReq!t_utente, rsReq!Utente, Empty, Empty, Empty, 1, BL_HandleNull(rsReq!cod_med, ""), "", "") = True Then
                                                        
                                
                                
                                sql = "UPDATE sl_realiza SET flg_facturado = 1 WHERE seq_realiza = " & rs!seq_realiza
                                BG_ExecutaQuery_ADO sql
                                BG_Trata_BDErro
                        
                                ContaRubFact = ContaRubFact + 1
                                ContaRubReq = ContaRubReq + 1
                            
                            Else
                                If gAnalise_Ja_Facturada = True Then
                                    sql = "UPDATE sl_realiza SET flg_facturado = 1 WHERE seq_realiza = " & rs!seq_realiza
                                    BG_ExecutaQuery_ADO sql
                                    BG_Trata_BDErro
                                    ContaRubReq = ContaRubReq + 1
                                End If
                            End If
                        
                        End If
                        rs.MoveNext
                    Wend


            End If
            
            'soliveira 19.09.2007
            If gLAB = "CHVNG" Then
                If ContaRubReq > 0 Then
                    SqlUpd = "update sl_requis set dt_fact = " & BL_TrataDataParaBD(Bg_DaData_ADO) & " where n_req = " & rsReq!n_req
                    BG_ExecutaQuery_ADO SqlUpd
                    ContaReqFact = ContaReqFact + 1
                Else
                    SqlIns = "insert into sl_nada_facturadas(n_req) values (" & rsReq!n_req & ")"
                    BG_ExecutaQuery_ADO SqlIns
                End If
            End If
            
            rsReq.MoveNext
        Wend
        BL_FimProcessamento Me
        Call BG_Mensagem(mediMsgBox, ContaRubFact & " rubricas de " & ContaReqFact & " requisi��es enviadas para o " & UCase(gSISTEMA_FACTURACAO) & ".", vbOKOnly + vbInformation, App.ProductName)
    End If
    Else
        BG_Mensagem mediMsgBox, "Erro a abrir conex�o secund�ria!", vbError, "Erro"
    End If
    
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "Erro nr. " & Err.Number & " - " & Err.Description & " no FormFactGeraDados (Gera_Dados_Fact)"
    Exit Sub
    Resume Next
End Sub



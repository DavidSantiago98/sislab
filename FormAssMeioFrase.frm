VERSION 5.00
Begin VB.Form FormAssMeioFrase 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAssMeioFrase"
   ClientHeight    =   4935
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7980
   Icon            =   "FormAssMeioFrase.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4935
   ScaleWidth      =   7980
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodMeio 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H00404040&
      Height          =   285
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   120
      Width           =   705
   End
   Begin VB.TextBox EcDescrMeio 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H00404040&
      Height          =   285
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   120
      Width           =   5985
   End
   Begin VB.CommandButton BtRetira 
      Height          =   435
      Left            =   3720
      Picture         =   "FormAssMeioFrase.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2640
      Width           =   495
   End
   Begin VB.CommandButton BtInsere 
      Height          =   435
      Left            =   3720
      Picture         =   "FormAssMeioFrase.frx":0396
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2040
      Width           =   495
   End
   Begin VB.TextBox EcPesquisa 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   270
      TabIndex        =   2
      Top             =   960
      Width           =   3345
   End
   Begin VB.ListBox EcListaSimples 
      Appearance      =   0  'Flat
      Height          =   3345
      Left            =   270
      TabIndex        =   1
      Top             =   1320
      Width           =   3345
   End
   Begin VB.ListBox EcLista2Simples 
      Appearance      =   0  'Flat
      Height          =   3735
      Left            =   4440
      TabIndex        =   0
      Top             =   960
      Width           =   3345
   End
   Begin VB.Label Label2 
      Caption         =   "Meio"
      Height          =   255
      Index           =   2
      Left            =   360
      TabIndex        =   8
      Top             =   120
      Width           =   3315
   End
   Begin VB.Label Label2 
      Caption         =   "Frases Associadas"
      Height          =   255
      Index           =   1
      Left            =   4440
      TabIndex        =   6
      Top             =   720
      Width           =   3315
   End
   Begin VB.Label Label2 
      Caption         =   "Digite o texto que pretende pesquisar"
      Height          =   255
      Index           =   0
      Left            =   300
      TabIndex        =   5
      Top             =   660
      Width           =   3315
   End
End
Attribute VB_Name = "FormAssMeioFrase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String
Dim CriterioTabelaF As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Public CamposBDparaListBoxF
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim ListaOrigem As Control
Dim ListaDestino As Control
    
Public rs As ADODB.recordset
Public RSListaDestino As ADODB.recordset
Public RSListaOrigem As ADODB.recordset

Private Sub BtInsere_Click()

    Dim i As Integer
            
    Set ListaOrigem = EcListaSimples
    Set ListaDestino = EcLista2Simples
       
    For i = 0 To ListaOrigem.ListCount - 1
        If ListaOrigem.Selected(i) Then
            BG_PassaElementoEntreListas ListaOrigem, ListaDestino, i, False, False
            ListaOrigem.Selected(i) = False
        End If
    Next i
              
End Sub

Private Sub BtRetira_Click()

    Set ListaDestino = EcLista2Simples
    If ListaDestino.ListIndex <> -1 Then
        ListaDestino.RemoveItem (ListaDestino.ListIndex)
    End If

End Sub

Private Sub EcLista2Simples_Click()
    EcLista2Simples.ToolTipText = Trim(EcLista2Simples.List(EcLista2Simples.ListIndex))
End Sub

Private Sub EcListaSimples_Click()
    EcListaSimples.ToolTipText = Trim(EcListaSimples.List(EcListaSimples.ListIndex))
End Sub

Private Sub EcPesquisa_Change()
    BL_RefinaPesquisa EcPesquisa, EcListaSimples, "sl_dicionario", "seq_frase", "descr_frase"
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Associa��o de Frases a Actividades"
    Me.left = 540
    Me.top = 5
    Me.Width = 8070
    Me.Height = 5355 ' Normal
    'Me.Height = 8825 ' Campos Extras
    
    NomeTabela = "sl_ass_meio_frase"
    Set CampoDeFocus = EcCodMeio
    
    NumCampos = 1
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "cod_meio"
        
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodMeio
    
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_meio"
    
    Set ChaveEc = EcCodMeio
    
    CamposBDparaListBoxF = Array("descr_frase")
    NumEspacos = Array(250)
    
End Sub


Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    
    If Not RSListaOrigem Is Nothing Then
        RSListaOrigem.Close
        Set RSListaOrigem = Nothing
    End If
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormAssMeioFrase = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    BG_LimpaCampo_Todos CamposEc
    EcLista2Simples.Clear
End Sub

Sub DefTipoCampos()
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
    EcCodMeio.Tag = adNumeric
End Sub
Sub PreencheValoresDefeito()
    ' nada
End Sub
Sub PreencheCampos()
        
    Me.SetFocus
    
    BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
    

    
    Set RSListaDestino = New ADODB.recordset
    RSListaDestino.CursorType = adOpenStatic
    RSListaDestino.CursorLocation = adUseServer

    CriterioTabelaF = "SELECT sl_dicionario.cod_frase,seq_frase,descr_frase, cod_meio FROM sl_ass_meio_frase ,sl_dicionario "
    CriterioTabelaF = CriterioTabelaF & " WHERE sl_ass_meio_frase.cod_frase = sl_dicionario.cod_frase AND cod_meio = " & EcCodMeio
    CriterioTabelaF = CriterioTabelaF & " ORDER BY descr_frase "
    RSListaDestino.Open CriterioTabelaF, gConexao

    If RSListaDestino.RecordCount > 0 Then
        estado = 2
        ' Inicio do preenchimento de 'EcLista'
        BL_PreencheListBoxMultipla_ADO EcLista2Simples, RSListaDestino, CamposBDparaListBoxF, NumEspacos, CamposBD, CamposEc, "SELECT", , "seq_frase"
    End If
    RSListaDestino.Close
    Set RSListaDestino = Nothing
    
End Sub


Sub FuncaoLimpar()
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista2Simples.Clear
        
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
        
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If
End Sub


Sub FuncaoProcurar()
    Dim cod_meio As String
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    cod_meio = EcCodMeio
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = "SELECT distinct cod_meio FROM sl_ass_meio_frase WHERE cod_meio = " & EcCodMeio
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
        EcCodMeio = cod_meio
    Else
        estado = 2
        LimpaCampos
        EcCodMeio = cod_meio
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
End Sub
Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        BD_Update
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim SQLAux As String
    Dim SQLAux2 As String
    Dim MarcaLocal As Variant
    Dim i As Integer
    Dim rsAux As ADODB.recordset
    Dim RsPerfis As ADODB.recordset
    Dim sql As String
        
    SQLAux2 = "DELETE FROM " & NomeTabela & " WHERE cod_meio=" & EcCodMeio
            
    BG_ExecutaQuery_ADO SQLAux2

    Set rsAux = New ADODB.recordset
    For i = 0 To EcLista2Simples.ListCount - 1
        SQLAux = "SELECT cod_frase FROM sl_dicionario WHERE seq_frase=" & EcLista2Simples.ItemData(i)
        rsAux.Open SQLAux, gConexao, adOpenStatic, adLockReadOnly
        SQLQuery = "INSERT INTO " & NomeTabela & " (cod_meio,cod_frase) VALUES (" & EcCodMeio & ",'" & Trim(rsAux!cod_frase) & "')"
        BG_ExecutaQuery_ADO SQLQuery
        rsAux.Close
    Next i

    Set rsAux = Nothing
   
    'Permite que a FuncaoInserir utilize esta funcao
    If rs Is Nothing Then Exit Sub Else: If rs.RecordCount <= 0 Then Exit Sub
    
    'temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
    
    If MarcaLocal <= rs.RecordCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos
    
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If MarcaLocal <= rs.RecordCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
        
    LimpaCampos
    PreencheCampos

End Sub


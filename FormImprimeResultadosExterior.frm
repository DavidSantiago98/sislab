VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form FormImprimeResultadosExterior 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormImprimeResultadosExterior"
   ClientHeight    =   3420
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7095
   Icon            =   "FormImprimeResultadosExterior.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3420
   ScaleWidth      =   7095
   ShowInTaskbar   =   0   'False
   Begin VB.OptionButton OptAgrupar 
      Caption         =   "Proveni�ncia"
      Height          =   255
      Index           =   2
      Left            =   2880
      TabIndex        =   24
      Top             =   2400
      Width           =   1335
   End
   Begin VB.OptionButton OptAgrupar 
      Caption         =   "Utente"
      Height          =   255
      Index           =   1
      Left            =   4440
      TabIndex        =   23
      Top             =   2400
      Width           =   1215
   End
   Begin VB.OptionButton OptAgrupar 
      Caption         =   "Sala/Posto"
      Height          =   255
      Index           =   0
      Left            =   1440
      TabIndex        =   22
      Top             =   2400
      Width           =   1215
   End
   Begin VB.CommandButton BtPesquisaProveniencia 
      Height          =   315
      Left            =   6360
      Picture         =   "FormImprimeResultadosExterior.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   1320
      Width           =   375
   End
   Begin VB.CommandButton BtPesquisaSala 
      Height          =   315
      Left            =   6360
      Picture         =   "FormImprimeResultadosExterior.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   1800
      Width           =   375
   End
   Begin VB.CommandButton BtImprimir 
      Height          =   495
      Left            =   5160
      Picture         =   "FormImprimeResultadosExterior.frx":0B20
      Style           =   1  'Graphical
      TabIndex        =   13
      ToolTipText     =   "Imprimir"
      Top             =   2760
      Width           =   855
   End
   Begin VB.CommandButton BtSair 
      Height          =   495
      Left            =   6000
      Picture         =   "FormImprimeResultadosExterior.frx":17EA
      Style           =   1  'Graphical
      TabIndex        =   12
      ToolTipText     =   "Sair"
      Top             =   2760
      Width           =   735
   End
   Begin VB.TextBox EcCodProveniencia 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1440
      TabIndex        =   11
      Top             =   1320
      Width           =   975
   End
   Begin VB.TextBox EcDescrProveniencia 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   2400
      Locked          =   -1  'True
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   1320
      Width           =   3975
   End
   Begin VB.TextBox EcDataInicial 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      TabIndex        =   8
      Top             =   600
      Width           =   975
   End
   Begin VB.ComboBox CbSituacao 
      Height          =   315
      Left            =   5400
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   120
      Width           =   1335
   End
   Begin VB.ComboBox CbUrgencia 
      Height          =   315
      Left            =   3360
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   120
      Width           =   1215
   End
   Begin VB.TextBox EcSeqImpr 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1440
      TabIndex        =   5
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcDescrSala 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   2400
      Locked          =   -1  'True
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1800
      Width           =   3975
   End
   Begin VB.TextBox EcCodSala 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1440
      TabIndex        =   3
      Top             =   1800
      Width           =   975
   End
   Begin VB.TextBox EcDataFinal 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3360
      TabIndex        =   2
      Top             =   600
      Width           =   1215
   End
   Begin VB.ComboBox CbGenero 
      Height          =   315
      Left            =   5400
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   600
      Width           =   1335
   End
   Begin RichTextLib.RichTextBox RText 
      Height          =   375
      Left            =   0
      TabIndex        =   25
      Top             =   4440
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"FormImprimeResultadosExterior.frx":20B4
   End
   Begin VB.Label Label9 
      Caption         =   "&Proveni�ncia"
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   1320
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Data Inicial"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   20
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label6 
      Caption         =   "Prioridade"
      Height          =   255
      Left            =   2520
      TabIndex        =   19
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label4 
      Caption         =   "&Situa��o"
      Height          =   255
      Index           =   0
      Left            =   4680
      TabIndex        =   18
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Seq Impress�o"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label LbPosto 
      Caption         =   "Posto"
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   1800
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Data Final"
      Height          =   255
      Index           =   2
      Left            =   2520
      TabIndex        =   15
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label4 
      Caption         =   "Esp�cie"
      Height          =   255
      Index           =   1
      Left            =   4680
      TabIndex        =   14
      Top             =   600
      Width           =   975
   End
End
Attribute VB_Name = "FormImprimeResultadosExterior"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa


Dim CampoActivo As Object

Private Sub BtImprimir_Click()
    ImprimeResultados
End Sub

Private Sub BtPesquisaProveniencia_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", "Pesquisar Proveniencias")
    
    mensagem = "N�o foi encontrada nenhuma proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.Text = resultados(1)
            EcDescrProveniencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
    
    EcCodProveniencia.SetFocus
    
End Sub

Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    BG_LimpaOpcao CbSituacao, KeyCode
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbUrgencia, KeyCode

End Sub

Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodProveniencia.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_proven FROM sl_proven WHERE cod_proven='" & Trim(EcCodProveniencia.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodProveniencia.Text = ""
            EcDescrProveniencia.Text = ""
        Else
            EcDescrProveniencia.Text = Tabela!descr_proven
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub

Private Sub BtSair_Click()
    Unload Me
End Sub


Private Sub EcDataInicial_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDataInicial_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
End Sub

Private Sub EcDataFinal_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDataFinal_Validate(Cancel As Boolean)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
End Sub

Private Sub Form_Activate()
    
    BG_StackJanelas_Actualiza Me
    
    Set gFormActivo = Me
    EcDataInicial.SetFocus
    
End Sub

Private Sub Form_Load()
    gF_IMPRRESULTEXT = 1
    
    Me.caption = "Impress�o de Requisi��es para Exterior"
        
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia
    BG_PreencheComboBD_ADO "sl_genero", "cod_genero", "descr_genero", CbGenero
    BG_StackJanelas_Push Me
    OptAgrupar(0).value = True
    
    'Tipo Data
    EcDataInicial.Tag = "104"
    EcDataFinal.Tag = "104"
    EcDataFinal = Bg_DaData_ADO
    
    If gCodSalaAssocUser <> 0 Then
        EcCodSala = gCodSalaAssocUser
        EcCodsala_Validate False
        EcCodSala.Enabled = False
        BtPesquisaSala.Enabled = False
    Else
        EcCodSala = ""
        EcCodsala_Validate True
        EcCodSala.Enabled = True
        BtPesquisaSala.Enabled = True
    End If
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    gF_IMPRRESULTEXT = 0
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormImprimeResultadosExterior = Nothing
    
End Sub

Private Sub EcCodsala_Validate(Cancel As Boolean)
    Cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
    
End Sub


Private Sub BtPesquisaSala_Click()
    PA_PesquisaSala EcCodSala, EcDescrSala, ""
End Sub



Private Sub CbGenero_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbGenero.ListIndex = -1

End Sub


Private Sub ImprimeResultados()
    Dim sSql As String
    Dim RegReq As New ADODB.recordset
    Dim continua As Boolean
    Dim flg_gerou As Boolean
    Dim rsImpr As New ADODB.recordset
    
    IR_ApagaTabelasResultados
    gImprimirDestino = 1
    
    flg_gerou = False

    sSql = "SELECT sl_Requis.n_req, sl_identif.seq_utente, sl_requis.estado_req "
    sSql = sSql & " FROM sl_identif, sl_requis LEFT OUTER JOIN sl_proven ON sl_requis.cod_proven = sl_proven.cod_proven AND sl_proven.flg_nao_imprimir = 1 "
    sSql = sSql & " LEFT OUTER JOIN sl_cod_salas ON sl_requis.cod_sala = sl_cod_salas.cod_sala, sl_tbf_t_destino "
    sSql = sSql & " WHERE sl_identif.seq_utente = sl_Requis.seq_utente "
    If IsNumeric(EcSeqImpr) Then
        sSql = sSql & " AND sl_requis.n_req IN (SELECT n_req FROM sl_impr_completas WHERE seq_impr = " & EcSeqImpr & ") "
    Else
        sSql = sSql & " AND (sl_requis.estado_req in ( 'D', '3','2' )) "
    End If
    sSql = sSql & " AND n_req not in (select n_req from sl_marcacoes where n_req = sl_requis.n_req and cod_ana_s <> 'S99999') "
    sSql = sSql & " AND n_req not in (select n_req from sl_realiza where n_req = sl_requis.n_req and (flg_estado not in (" & BL_TrataStringParaBD(gEstadoAnaValidacaoMedica) & ", "
    sSql = sSql & BL_TrataStringParaBD(gEstadoAnaImpressa) & ") )) "
    sSql = sSql & " AND sl_requis.cod_destino = sl_tbf_t_destino.cod_t_dest AND flg_impr_completas = 0 AND flg_imprime_listagem = 1 "
    If Trim(EcCodProveniencia.Text) <> "" Then
        sSql = sSql + " AND sl_requis.cod_proven='" + EcCodProveniencia.Text + "'"
    End If
    If EcDataInicial.Text <> "" Then
        sSql = sSql & " AND sl_requis.dt_chega >= " & BL_TrataDataParaBD(EcDataInicial.Text)
    End If
    If EcDataFinal.Text <> "" Then
        sSql = sSql & " AND sl_requis.dt_chega <= " & BL_TrataDataParaBD(EcDataFinal.Text)
    End If
    If EcCodSala.Text <> "" Then
        sSql = sSql & " AND sl_requis.cod_sala = " & BL_TrataStringParaBD(EcCodSala.Text)
    End If
    If CbUrgencia.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND sl_requis.t_urg = " & BL_TrataStringParaBD(CbUrgencia.ItemData(CbUrgencia.ListIndex))
    End If
    
    If CbSituacao.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND sl_requis.t_sit = " & CbSituacao.ItemData(CbSituacao.ListIndex)
    End If
    
    If CbGenero.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND sl_identif.cod_genero = " & CbGenero.ItemData(CbGenero.ListIndex)
    End If
    sSql = sSql & " AND sl_identif.cod_genero IN (SELECT cod_genero FROM sl_genero WHERE flg_impr_completos = 1) "
    sSql = sSql & " AND sl_requis.cod_local = " & gCodLocal
    If gAssinaturaActivo = mediSim Then
        sSql = sSql & " AND sl_requis.n_req in (SELECT sl_req_assinatura.n_Req from sl_req_assinatura)"
    End If
    
    RegReq.CursorType = adOpenStatic
    RegReq.CursorLocation = adUseServer
    RegReq.ActiveConnection = gConexao
    RegReq.Source = sSql
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RegReq.Open
    If RegReq.RecordCount <= 0 Then
        Call BG_Mensagem(mediMsgBox, "N�o existem requisi��es para listar!", vbOKOnly + vbInformation, App.ProductName)
    Else
        If EcSeqImpr = "" Then
            sSql = "SELECT max(seq_impr) maximo FROM sl_impr_completas "
            Set rsImpr = New ADODB.recordset
            rsImpr.CursorType = adOpenStatic
            rsImpr.CursorLocation = adUseServer
            rsImpr.ActiveConnection = gConexao
            rsImpr.Open sSql, gConexao
            EcSeqImpr = BL_HandleNull(rsImpr!maximo + 1, 1)
            flg_gerou = True
            rsImpr.Close
            Set rsImpr = Nothing
        End If
        continua = BL_IniciaReport("MapaResultadosSimplesLista", "Mapa de Resultados", crptToWindow, , , , , , "")
        If continua = False Then
            Call BG_Mensagem(mediMsgBox, "Erro ao abrir report.", vbOKOnly + vbInformation, App.ProductName)
        Else
            While Not RegReq.EOF
                Call IR_ImprimeResultados(False, False, RegReq!n_req, RegReq!estado_req, RegReq!seq_utente, , , , True, "", , "", , , , False, , , False, False, False, False, True)
                BL_RegistaImprimeReq RegReq!n_req, EcSeqImpr
                RegReq.MoveNext
            Wend
            IR_ImprimeResultadosCrystalLista
        End If
    End If
    RegReq.Close
    Set RegReq = Nothing
    IR_ApagaTabelasResultados
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResultados:" & sSql & vbCrLf & Err.Number & "/" & Err.Description, Me.Name, "ImprimeResultados", True
    Exit Sub
    Resume Next
End Sub


Public Function Funcao_DataActual()

    Select Case Me.ActiveControl.Name
        Case "EcDataInicial"
            EcDataInicial.Text = Bg_DaData_ADO
    End Select

End Function


VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormCodAna 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCodAna"
   ClientHeight    =   18480
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   13035
   Icon            =   "FormCodAna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   18480
   ScaleWidth      =   13035
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcTipoUnidade4 
      Height          =   285
      Left            =   9000
      TabIndex        =   159
      Top             =   10080
      Width           =   615
   End
   Begin VB.TextBox EcTipoUnidade3 
      Height          =   285
      Left            =   9000
      TabIndex        =   158
      Top             =   9720
      Width           =   615
   End
   Begin VB.TextBox EcUnidadeUso2Res 
      Height          =   285
      Left            =   1440
      TabIndex        =   157
      Top             =   9600
      Width           =   615
   End
   Begin VB.CommandButton BtCopiaDadosAna 
      Height          =   360
      Left            =   12480
      Picture         =   "FormCodAna.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   146
      ToolTipText     =   "Copiar Dados de Outra An�lise"
      Top             =   600
      Width           =   375
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6255
      Left            =   0
      TabIndex        =   45
      Top             =   1080
      Width           =   12855
      _ExtentX        =   22675
      _ExtentY        =   11033
      _Version        =   393216
      Tabs            =   8
      TabsPerRow      =   8
      TabHeight       =   520
      TabCaption(0)   =   "Dados da An�lise"
      TabPicture(0)   =   "FormCodAna.frx":0396
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1(17)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label1(19)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label1(18)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label1(23)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label1(25)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label1(20)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label1(29)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label1(30)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Label1(28)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Label1(27)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Label1(26)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Label1(33)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Frame8(0)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Frame9"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "EcValorDefeito"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "EcLimiteSup"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "EcLimiteInf"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "EcCasasDecimais"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "EcPeso"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "EcDescrEtiq"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "EcAnaLocais"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "EcCodEstatP"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "EcCodEstatC"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "EcCodEstatistica"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "Frame2"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "EcAnaLocaisExec"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "EcCodSinave"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).ControlCount=   27
      TabCaption(1)   =   "Dados Complem I"
      TabPicture(1)   =   "FormCodAna.frx":03B2
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label1(40)"
      Tab(1).Control(1)=   "Label1(42)"
      Tab(1).Control(2)=   "Label57(4)"
      Tab(1).Control(3)=   "Label57(3)"
      Tab(1).Control(4)=   "Label57(2)"
      Tab(1).Control(5)=   "Label57(0)"
      Tab(1).Control(6)=   "Label57(1)"
      Tab(1).Control(7)=   "Label1(41)"
      Tab(1).Control(8)=   "CkFormLeuc"
      Tab(1).Control(9)=   "CkOpcional"
      Tab(1).Control(10)=   "CkMarcAuto"
      Tab(1).Control(11)=   "CkEtiqueta"
      Tab(1).Control(12)=   "CkInibeDescricao"
      Tab(1).Control(13)=   "CkInibeMarcacao"
      Tab(1).Control(14)=   "CkAcreditacao"
      Tab(1).Control(15)=   "CkExterior"
      Tab(1).Control(16)=   "CkSemHistorico"
      Tab(1).Control(17)=   "CkNegrito"
      Tab(1).Control(18)=   "CkRAST"
      Tab(1).Control(19)=   "CkFacturar"
      Tab(1).Control(20)=   "EcPrazoConc"
      Tab(1).Control(21)=   "CbSexo"
      Tab(1).Control(22)=   "EcValMaxGrafico"
      Tab(1).Control(23)=   "EcValMinGrafico"
      Tab(1).Control(24)=   "EcValGrafico"
      Tab(1).Control(25)=   "CkNaoAvaliaResultados"
      Tab(1).Control(26)=   "CkCutOff"
      Tab(1).Control(27)=   "EcETA"
      Tab(1).Control(28)=   "EcOrdemARS"
      Tab(1).Control(29)=   "EcPrazoVal"
      Tab(1).Control(30)=   "CkTransManual"
      Tab(1).Control(31)=   "CkResAnt"
      Tab(1).Control(32)=   "CkResExterior(0)"
      Tab(1).Control(33)=   "CkEnvioSMS"
      Tab(1).Control(34)=   "EcTextoSMS"
      Tab(1).Control(35)=   "CkAvaliaEpid"
      Tab(1).Control(36)=   "CkCopiaRes"
      Tab(1).Control(37)=   "CkMarcador"
      Tab(1).Control(38)=   "CkResAnt2Res"
      Tab(1).Control(39)=   "CkObrigaTerap"
      Tab(1).Control(40)=   "CkObrigaInfClin"
      Tab(1).Control(41)=   "CkResExterior(1)"
      Tab(1).Control(42)=   "CkResExterior(2)"
      Tab(1).ControlCount=   43
      TabCaption(2)   =   "Dados Complem II"
      TabPicture(2)   =   "FormCodAna.frx":03CE
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "BtSinonimos"
      Tab(2).Control(1)=   "FrameSinon"
      Tab(2).Control(2)=   "EcListaFolhasTrab"
      Tab(2).Control(3)=   "EcInformacao"
      Tab(2).Control(4)=   "EcQuestoes"
      Tab(2).Control(5)=   "Label1(54)"
      Tab(2).Control(6)=   "Label1(58)"
      Tab(2).Control(7)=   "Label1(59)"
      Tab(2).ControlCount=   8
      TabCaption(3)   =   "Coment. e Concl."
      TabPicture(3)   =   "FormCodAna.frx":03EA
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Label1(16)"
      Tab(3).Control(1)=   "RTBComent"
      Tab(3).Control(2)=   "Frame3"
      Tab(3).ControlCount=   3
      TabCaption(4)   =   "Val./Esc. Ref."
      TabPicture(4)   =   "FormCodAna.frx":0406
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Label1(55)"
      Tab(4).Control(1)=   "Label1(52)"
      Tab(4).Control(2)=   "Label1(45)"
      Tab(4).Control(3)=   "Label1(44)"
      Tab(4).Control(4)=   "LbFem"
      Tab(4).Control(5)=   "LbMinF"
      Tab(4).Control(6)=   "LbMaxF"
      Tab(4).Control(7)=   "LbMasc"
      Tab(4).Control(8)=   "LbMinM"
      Tab(4).Control(9)=   "LbMaxM"
      Tab(4).Control(10)=   "LbCmMaxF"
      Tab(4).Control(11)=   "LbCmMinF"
      Tab(4).Control(12)=   "LbCmMaxM"
      Tab(4).Control(13)=   "LbCmMinM"
      Tab(4).Control(14)=   "BtCutOff"
      Tab(4).Control(15)=   "FgValRef"
      Tab(4).Control(16)=   "EcSeqAnaRef"
      Tab(4).Control(17)=   "EcLinhaAnaRef"
      Tab(4).Control(18)=   "EcDtFim"
      Tab(4).Control(19)=   "EcDtIni"
      Tab(4).Control(20)=   "EcObsRef"
      Tab(4).Control(21)=   "BtPesquisaGenero"
      Tab(4).Control(21).Enabled=   0   'False
      Tab(4).Control(22)=   "EcDescrGenero"
      Tab(4).Control(22).Enabled=   0   'False
      Tab(4).Control(23)=   "EcCodGenero"
      Tab(4).Control(24)=   "EcCodReagente"
      Tab(4).Control(25)=   "EcDescrReagente"
      Tab(4).Control(25).Enabled=   0   'False
      Tab(4).Control(26)=   "BtPesquisaReagente"
      Tab(4).Control(26).Enabled=   0   'False
      Tab(4).Control(27)=   "BtPesquisaMetodo"
      Tab(4).Control(27).Enabled=   0   'False
      Tab(4).Control(28)=   "EcDescrMetodo"
      Tab(4).Control(28).Enabled=   0   'False
      Tab(4).Control(29)=   "EcCodMetodo"
      Tab(4).Control(30)=   "OptNRes(1)"
      Tab(4).Control(31)=   "OptNRes(0)"
      Tab(4).Control(32)=   "BtDuplicarAnaRef"
      Tab(4).Control(33)=   "BtNovo"
      Tab(4).Control(34)=   "BtFechar"
      Tab(4).Control(35)=   "BtEscaloes"
      Tab(4).Control(36)=   "BtValRef"
      Tab(4).Control(37)=   "BtZonas"
      Tab(4).Control(38)=   "BtAnular"
      Tab(4).Control(39)=   "BtAdiciona"
      Tab(4).Control(40)=   "BtRemove"
      Tab(4).Control(41)=   "EcLinhaEscRef"
      Tab(4).Control(42)=   "EcColEscRef"
      Tab(4).Control(43)=   "EcMaxFem"
      Tab(4).Control(44)=   "EcMinFem"
      Tab(4).Control(45)=   "EcMaxMasc"
      Tab(4).Control(46)=   "EcMinMasc"
      Tab(4).Control(47)=   "EcComMiF"
      Tab(4).Control(48)=   "EcComMaF"
      Tab(4).Control(49)=   "EcComMiH"
      Tab(4).Control(50)=   "EcComMaH"
      Tab(4).Control(51)=   "FgEscRef"
      Tab(4).Control(52)=   "EcEscRefAux"
      Tab(4).Control(53)=   "CbEscRefAux"
      Tab(4).Control(54)=   "FrCutOff"
      Tab(4).ControlCount=   55
      TabCaption(5)   =   "Interfer�ncias"
      TabPicture(5)   =   "FormCodAna.frx":0422
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "FrameInterferencias"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "Fatura��o"
      TabPicture(6)   =   "FormCodAna.frx":043E
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "Label1(46)"
      Tab(6).Control(1)=   "Label1(53)"
      Tab(6).Control(2)=   "FgPrecos"
      Tab(6).Control(3)=   "BtCopiaPrecario"
      Tab(6).Control(4)=   "EcAuxPreco"
      Tab(6).Control(5)=   "BtGravarAnaFact"
      Tab(6).Control(6)=   "EcRubrSeqAna"
      Tab(6).Control(7)=   "BtAdicionarRubrica"
      Tab(6).Control(7).Enabled=   0   'False
      Tab(6).Control(8)=   "BtPesquisaRubrica"
      Tab(6).Control(8).Enabled=   0   'False
      Tab(6).Control(9)=   "EcDescrRubrica"
      Tab(6).Control(10)=   "EcCodRubrica"
      Tab(6).Control(11)=   "EcQtd"
      Tab(6).Control(12)=   "CkContaMembros"
      Tab(6).Control(13)=   "EcCodAnaRegra"
      Tab(6).Control(14)=   "CkFlgRegra"
      Tab(6).Control(15)=   "CkFlgfacturarAna"
      Tab(6).Control(16)=   "EcCodAnaFacturar"
      Tab(6).Control(17)=   "FrameRubrica"
      Tab(6).ControlCount=   18
      TabCaption(7)   =   "Seg. Resultado"
      TabPicture(7)   =   "FormCodAna.frx":045A
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "FrameSegundoRes"
      Tab(7).Control(1)=   "CkSegundoRes"
      Tab(7).ControlCount=   2
      Begin VB.TextBox EcCodSinave 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8520
         TabIndex        =   335
         Top             =   5520
         Width           =   1455
      End
      Begin VB.CheckBox CkResExterior 
         Caption         =   "Envio N� Convencionado"
         Height          =   255
         Index           =   2
         Left            =   -70440
         TabIndex        =   333
         Top             =   4320
         Width           =   2295
      End
      Begin VB.CheckBox CkResExterior 
         Caption         =   "Envio N�mero SNS"
         Height          =   255
         Index           =   1
         Left            =   -70440
         TabIndex        =   332
         Top             =   3960
         Width           =   1695
      End
      Begin VB.CheckBox CkObrigaInfClin 
         Caption         =   "Obriga Inf. Clinica"
         Height          =   255
         Left            =   -70440
         TabIndex        =   331
         ToolTipText     =   "An�lise sujeita a contagem de marcadores para fatura��o"
         Top             =   3240
         Width           =   3615
      End
      Begin VB.CheckBox CkObrigaTerap 
         Caption         =   "ObrigaTerap�utica "
         Height          =   255
         Left            =   -70440
         TabIndex        =   330
         ToolTipText     =   "An�lise sujeita a contagem de marcadores para fatura��o"
         Top             =   3600
         Width           =   3615
      End
      Begin VB.CheckBox CkResAnt2Res 
         Caption         =   "Imprime resultados anteriores 2� resultado"
         Height          =   255
         Left            =   -70440
         TabIndex        =   329
         ToolTipText     =   "An�lise sujeita a contagem de marcadores para fatura��o"
         Top             =   2880
         Width           =   3615
      End
      Begin VB.Frame FrCutOff 
         Height          =   3015
         Left            =   -74880
         TabIndex        =   231
         Top             =   3120
         Visible         =   0   'False
         Width           =   12615
         Begin VB.TextBox EcComDuv 
            Height          =   285
            Left            =   5400
            TabIndex        =   246
            Top             =   2400
            Width           =   6855
         End
         Begin VB.ComboBox CbDMax 
            Height          =   315
            Left            =   2880
            Style           =   2  'Dropdown List
            TabIndex        =   245
            Top             =   2400
            Width           =   615
         End
         Begin VB.ComboBox CbDMin 
            Height          =   315
            Left            =   1920
            Style           =   2  'Dropdown List
            TabIndex        =   244
            Top             =   2400
            Width           =   615
         End
         Begin VB.TextBox EcCutOffDMax 
            Height          =   285
            Left            =   3600
            TabIndex        =   243
            Top             =   2400
            Width           =   1575
         End
         Begin VB.TextBox EcCutOffDMin 
            Height          =   285
            Left            =   240
            TabIndex        =   242
            Top             =   2400
            Width           =   1575
         End
         Begin VB.TextBox EcComNeg 
            Height          =   285
            Left            =   5400
            TabIndex        =   241
            Top             =   1440
            Width           =   6855
         End
         Begin VB.ComboBox CbNMax 
            Height          =   315
            Left            =   2880
            Style           =   2  'Dropdown List
            TabIndex        =   240
            Top             =   1440
            Width           =   615
         End
         Begin VB.ComboBox CbNMin 
            Height          =   315
            Left            =   1920
            Style           =   2  'Dropdown List
            TabIndex        =   239
            Top             =   1440
            Width           =   615
         End
         Begin VB.TextBox EcCutOffNMax 
            Height          =   285
            Left            =   3600
            TabIndex        =   238
            Top             =   1440
            Width           =   1575
         End
         Begin VB.TextBox EcCutOffNMin 
            Height          =   285
            Left            =   240
            TabIndex        =   237
            Top             =   1440
            Width           =   1575
         End
         Begin VB.TextBox EcComPos 
            Height          =   285
            Left            =   5400
            TabIndex        =   236
            Top             =   600
            Width           =   6855
         End
         Begin VB.ComboBox CbPMax 
            Height          =   315
            Left            =   2880
            Style           =   2  'Dropdown List
            TabIndex        =   235
            Top             =   600
            Width           =   615
         End
         Begin VB.ComboBox CbPMin 
            Height          =   315
            Left            =   1920
            Style           =   2  'Dropdown List
            TabIndex        =   234
            Top             =   600
            Width           =   615
         End
         Begin VB.TextBox EcCutOffPMax 
            Height          =   285
            Left            =   3600
            TabIndex        =   233
            Top             =   600
            Width           =   1575
         End
         Begin VB.TextBox EcCutOffPMin 
            Height          =   285
            Left            =   240
            TabIndex        =   232
            Top             =   600
            Width           =   1575
         End
         Begin VB.Label Label45 
            Caption         =   "X"
            Height          =   255
            Left            =   2640
            TabIndex        =   252
            Top             =   2445
            Width           =   135
         End
         Begin VB.Label Label35 
            Caption         =   "X"
            Height          =   255
            Left            =   2640
            TabIndex        =   251
            Top             =   1485
            Width           =   135
         End
         Begin VB.Label Label41 
            Caption         =   "X"
            Height          =   255
            Left            =   2640
            TabIndex        =   250
            Top             =   645
            Width           =   135
         End
         Begin VB.Label Label1 
            Caption         =   "Com um resultado X, para ser POSITIVO deve-se verificar :"
            Height          =   255
            Index           =   999
            Left            =   480
            TabIndex        =   249
            Top             =   240
            Width           =   8055
         End
         Begin VB.Label Label1 
            Caption         =   "Com um resultado X, para ser NEGATIVO deve-se verificar :"
            Height          =   255
            Index           =   998
            Left            =   480
            TabIndex        =   248
            Top             =   1080
            Width           =   8055
         End
         Begin VB.Label Label1 
            Caption         =   "Com um resultado X, para ser DUVIDOSO deve-se verificar :"
            Height          =   255
            Index           =   997
            Left            =   480
            TabIndex        =   247
            Top             =   2040
            Width           =   8055
         End
      End
      Begin VB.ComboBox CbEscRefAux 
         Height          =   315
         Left            =   -63360
         Style           =   2  'Dropdown List
         TabIndex        =   287
         Top             =   4275
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.TextBox EcEscRefAux 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -64200
         TabIndex        =   288
         Top             =   4275
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.CheckBox CkMarcador 
         Caption         =   "An�lise do tipo Marcador"
         Height          =   255
         Left            =   -70440
         TabIndex        =   328
         ToolTipText     =   "An�lise sujeita a contagem de marcadores para fatura��o"
         Top             =   2520
         Width           =   3135
      End
      Begin VB.CommandButton BtSinonimos 
         Caption         =   "Sin�nimos"
         Height          =   975
         Left            =   -63840
         Picture         =   "FormCodAna.frx":0476
         Style           =   1  'Graphical
         TabIndex        =   327
         ToolTipText     =   "Sin�nimos"
         Top             =   5100
         Width           =   1095
      End
      Begin VB.Frame FrameSinon 
         Caption         =   "Sin�nimos"
         Height          =   5775
         Left            =   -61680
         TabIndex        =   322
         Top             =   360
         Width           =   12495
         Begin VB.CommandButton BtAdicSinonimo 
            Height          =   375
            Left            =   11520
            Picture         =   "FormCodAna.frx":1140
            Style           =   1  'Graphical
            TabIndex        =   326
            ToolTipText     =   "Adicionar Sinonimo"
            Top             =   360
            Width           =   375
         End
         Begin VB.TextBox EcSinonimo 
            Height          =   285
            Left            =   120
            TabIndex        =   325
            Top             =   360
            Width           =   11295
         End
         Begin VB.CommandButton BtFecharSinonimos 
            Height          =   555
            Left            =   11160
            Picture         =   "FormCodAna.frx":14CA
            Style           =   1  'Graphical
            TabIndex        =   324
            ToolTipText     =   "Fechar Sin�nimos"
            Top             =   5040
            Width           =   915
         End
         Begin VB.ListBox EcListaSinonimos 
            Height          =   3960
            Left            =   120
            TabIndex        =   323
            Top             =   840
            Width           =   11895
         End
      End
      Begin VB.ListBox EcListaFolhasTrab 
         Appearance      =   0  'Flat
         Height          =   1605
         Left            =   -73920
         Style           =   1  'Checkbox
         TabIndex        =   318
         Top             =   720
         Width           =   3615
      End
      Begin VB.ListBox EcInformacao 
         Appearance      =   0  'Flat
         Height          =   1605
         Left            =   -67440
         Style           =   1  'Checkbox
         TabIndex        =   317
         Top             =   720
         Width           =   3615
      End
      Begin VB.ListBox EcQuestoes 
         Appearance      =   0  'Flat
         Height          =   1380
         Left            =   -73920
         Style           =   1  'Checkbox
         TabIndex        =   316
         Top             =   2820
         Width           =   3615
      End
      Begin VB.Frame Frame3 
         Caption         =   "Conclus�es"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2655
         Left            =   -74880
         TabIndex        =   307
         Top             =   2940
         Width           =   12495
         Begin VB.CheckBox CkConclRes 
            Caption         =   "Conclus�es substituem o resultado."
            Height          =   255
            Left            =   240
            TabIndex        =   313
            Top             =   240
            Width           =   4575
         End
         Begin VB.TextBox EcConcFormula 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   360
            TabIndex        =   311
            Top             =   720
            Width           =   3975
         End
         Begin VB.TextBox EcConcDescr 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   4440
            TabIndex        =   310
            Top             =   720
            Width           =   7095
         End
         Begin VB.TextBox EcLinhaConc 
            Height          =   285
            Left            =   11760
            TabIndex        =   309
            Top             =   720
            Visible         =   0   'False
            Width           =   615
         End
         Begin VB.CommandButton BtImportarConcRAST 
            Height          =   375
            Left            =   11280
            Picture         =   "FormCodAna.frx":2194
            Style           =   1  'Graphical
            TabIndex        =   308
            ToolTipText     =   "Importar Conclus�es do tipo RAST"
            Top             =   200
            Width           =   375
         End
         Begin MSFlexGridLib.MSFlexGrid FgConc 
            Height          =   1335
            Left            =   360
            TabIndex        =   312
            Top             =   1080
            Width           =   12015
            _ExtentX        =   21193
            _ExtentY        =   2355
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
         End
      End
      Begin MSFlexGridLib.MSFlexGrid FgEscRef 
         Height          =   2415
         Left            =   -74640
         TabIndex        =   282
         Top             =   3720
         Width           =   12015
         _ExtentX        =   21193
         _ExtentY        =   4260
         _Version        =   393216
         Cols            =   8
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
      End
      Begin VB.TextBox EcComMaH 
         Height          =   285
         Left            =   -68400
         TabIndex        =   296
         Top             =   4560
         Width           =   3855
      End
      Begin VB.TextBox EcComMiH 
         Height          =   285
         Left            =   -68400
         TabIndex        =   295
         Top             =   4200
         Width           =   3855
      End
      Begin VB.TextBox EcComMaF 
         Height          =   285
         Left            =   -68400
         TabIndex        =   294
         Top             =   5520
         Width           =   3855
      End
      Begin VB.TextBox EcComMiF 
         Height          =   285
         Left            =   -68400
         TabIndex        =   293
         Top             =   5160
         Width           =   3855
      End
      Begin VB.TextBox EcMinMasc 
         Height          =   285
         Left            =   -72360
         TabIndex        =   292
         Top             =   4200
         Width           =   1695
      End
      Begin VB.TextBox EcMaxMasc 
         Height          =   285
         Left            =   -72360
         TabIndex        =   291
         Top             =   4560
         Width           =   1695
      End
      Begin VB.TextBox EcMinFem 
         Height          =   285
         Left            =   -72360
         TabIndex        =   290
         Top             =   5160
         Width           =   1695
      End
      Begin VB.TextBox EcMaxFem 
         Height          =   285
         Left            =   -72360
         TabIndex        =   289
         Top             =   5520
         Width           =   1695
      End
      Begin VB.TextBox EcColEscRef 
         Height          =   285
         Left            =   -63120
         TabIndex        =   286
         Top             =   4800
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.TextBox EcLinhaEscRef 
         Height          =   285
         Left            =   -63120
         TabIndex        =   285
         Top             =   3840
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton BtRemove 
         Height          =   375
         Left            =   -62760
         Picture         =   "FormCodAna.frx":251E
         Style           =   1  'Graphical
         TabIndex        =   284
         Top             =   3360
         Width           =   375
      End
      Begin VB.CommandButton BtAdiciona 
         Height          =   375
         Left            =   -63240
         Picture         =   "FormCodAna.frx":28A8
         Style           =   1  'Graphical
         TabIndex        =   283
         Top             =   3360
         Width           =   375
      End
      Begin VB.CommandButton BtAnular 
         Height          =   825
         Left            =   -65400
         Picture         =   "FormCodAna.frx":2C32
         Style           =   1  'Graphical
         TabIndex        =   280
         ToolTipText     =   "Apagar o registo ainda aberto"
         Top             =   1560
         Width           =   840
      End
      Begin VB.CommandButton BtZonas 
         Height          =   810
         Left            =   -63720
         Picture         =   "FormCodAna.frx":34FC
         Style           =   1  'Graphical
         TabIndex        =   279
         ToolTipText     =   "Zonas de Refer�ncia"
         Top             =   1560
         Width           =   840
      End
      Begin VB.CommandButton BtValRef 
         Height          =   825
         Left            =   -62880
         Picture         =   "FormCodAna.frx":3DC6
         Style           =   1  'Graphical
         TabIndex        =   278
         ToolTipText     =   "Valores de Ref�rencia"
         Top             =   720
         Width           =   720
      End
      Begin VB.CommandButton BtEscaloes 
         Height          =   825
         Left            =   -63720
         Picture         =   "FormCodAna.frx":4C90
         Style           =   1  'Graphical
         TabIndex        =   277
         ToolTipText     =   "Escal�es de Refer�ncia"
         Top             =   735
         Width           =   840
      End
      Begin VB.CommandButton BtFechar 
         Height          =   825
         Left            =   -64560
         Picture         =   "FormCodAna.frx":5B5A
         Style           =   1  'Graphical
         TabIndex        =   276
         ToolTipText     =   "Fechar o registo ainda aberto."
         Top             =   735
         Width           =   840
      End
      Begin VB.CommandButton BtNovo 
         Height          =   825
         Left            =   -65400
         Picture         =   "FormCodAna.frx":699C
         Style           =   1  'Graphical
         TabIndex        =   275
         ToolTipText     =   "Inserir "
         Top             =   735
         Width           =   840
      End
      Begin VB.CommandButton BtDuplicarAnaRef 
         Height          =   825
         Left            =   -64560
         Picture         =   "FormCodAna.frx":7266
         Style           =   1  'Graphical
         TabIndex        =   274
         ToolTipText     =   "Duplicar o registo ainda aberto"
         Top             =   1560
         Width           =   840
      End
      Begin VB.OptionButton OptNRes 
         Caption         =   "1� Resultado"
         Height          =   195
         Index           =   0
         Left            =   -68880
         TabIndex        =   273
         Top             =   480
         Width           =   1335
      End
      Begin VB.OptionButton OptNRes 
         Caption         =   "2� Resultado"
         Height          =   195
         Index           =   1
         Left            =   -67320
         TabIndex        =   272
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox EcCodMetodo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -69480
         TabIndex        =   267
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox EcDescrMetodo 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -68880
         Locked          =   -1  'True
         TabIndex        =   266
         TabStop         =   0   'False
         Top             =   1080
         Width           =   3015
      End
      Begin VB.CommandButton BtPesquisaMetodo 
         Height          =   315
         Left            =   -65880
         Picture         =   "FormCodAna.frx":7F30
         Style           =   1  'Graphical
         TabIndex        =   265
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   1080
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaReagente 
         Height          =   315
         Left            =   -65880
         Picture         =   "FormCodAna.frx":82BA
         Style           =   1  'Graphical
         TabIndex        =   264
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   1560
         Width           =   375
      End
      Begin VB.TextBox EcDescrReagente 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -68880
         Locked          =   -1  'True
         TabIndex        =   263
         TabStop         =   0   'False
         Top             =   1560
         Width           =   3015
      End
      Begin VB.TextBox EcCodReagente 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -69480
         TabIndex        =   262
         Top             =   1560
         Width           =   615
      End
      Begin VB.TextBox EcCodGenero 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -69480
         TabIndex        =   261
         Top             =   2040
         Width           =   615
      End
      Begin VB.TextBox EcDescrGenero 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -68880
         Locked          =   -1  'True
         TabIndex        =   260
         TabStop         =   0   'False
         Top             =   2040
         Width           =   3015
      End
      Begin VB.CommandButton BtPesquisaGenero 
         Height          =   315
         Left            =   -65880
         Picture         =   "FormCodAna.frx":8644
         Style           =   1  'Graphical
         TabIndex        =   259
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   2040
         Width           =   375
      End
      Begin VB.TextBox EcObsRef 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   545
         Left            =   -69480
         MultiLine       =   -1  'True
         TabIndex        =   258
         Top             =   2460
         Width           =   7095
      End
      Begin VB.TextBox EcDtIni 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -74040
         TabIndex        =   256
         Top             =   840
         Width           =   1215
      End
      Begin VB.TextBox EcDtFim 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -72600
         TabIndex        =   255
         Top             =   840
         Width           =   1215
      End
      Begin VB.TextBox EcLinhaAnaRef 
         Height          =   285
         Left            =   -74760
         TabIndex        =   254
         Top             =   840
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox EcSeqAnaRef 
         Height          =   285
         Left            =   -71400
         TabIndex        =   253
         Top             =   840
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Frame FrameInterferencias 
         Caption         =   "Interfer�ncias"
         Height          =   5295
         Left            =   -74880
         TabIndex        =   222
         Top             =   360
         Width           =   12375
         Begin VB.TextBox EcInterfSeq 
            Height          =   285
            Left            =   8760
            TabIndex        =   228
            Top             =   3480
            Visible         =   0   'False
            Width           =   615
         End
         Begin VB.TextBox EcObsDesblInterferencia 
            Height          =   285
            Left            =   8520
            TabIndex        =   227
            Top             =   840
            Width           =   3615
         End
         Begin VB.TextBox EcObsInterferencia 
            Height          =   285
            Left            =   4320
            TabIndex        =   226
            Top             =   840
            Width           =   4095
         End
         Begin VB.TextBox EcFormulaInterf 
            Height          =   285
            Left            =   120
            TabIndex        =   225
            Top             =   840
            Width           =   3015
         End
         Begin VB.TextBox EcAuxInterferencia 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   5040
            TabIndex        =   224
            Top             =   2400
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.ComboBox CbAccaoInterferencia 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   3240
            Style           =   2  'Dropdown List
            TabIndex        =   223
            Top             =   840
            Width           =   975
         End
         Begin MSFlexGridLib.MSFlexGrid FgInterferencias 
            Height          =   1620
            Left            =   240
            TabIndex        =   229
            Top             =   1200
            Width           =   9855
            _ExtentX        =   17383
            _ExtentY        =   2858
            _Version        =   393216
            Cols            =   3
            FixedCols       =   2
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
         End
         Begin VB.Label LbLabel 
            Height          =   375
            Left            =   360
            TabIndex        =   230
            Top             =   3120
            Width           =   9735
         End
      End
      Begin VB.Frame FrameRubrica 
         Caption         =   "Cria��o de R�brica"
         Height          =   5535
         Left            =   -74880
         TabIndex        =   193
         Top             =   360
         Width           =   12615
         Begin VB.TextBox EcRubrDescr 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   3120
            TabIndex        =   201
            Top             =   480
            Width           =   3735
         End
         Begin VB.TextBox EcRubrCodigo 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   960
            TabIndex        =   200
            Top             =   480
            Width           =   855
         End
         Begin VB.ComboBox CbRubrGrupo 
            Height          =   315
            Left            =   960
            Style           =   2  'Dropdown List
            TabIndex        =   199
            Top             =   1200
            Width           =   5895
         End
         Begin VB.CommandButton BtGravaRubr 
            Height          =   495
            Left            =   10680
            Picture         =   "FormCodAna.frx":89CE
            Style           =   1  'Graphical
            TabIndex        =   198
            ToolTipText     =   "Insere R�brica"
            Top             =   4920
            Width           =   735
         End
         Begin VB.CommandButton BtSairRubr 
            Height          =   495
            Left            =   11640
            Picture         =   "FormCodAna.frx":9698
            Style           =   1  'Graphical
            TabIndex        =   197
            ToolTipText     =   "Sair Sem Gravar"
            Top             =   4920
            Width           =   735
         End
         Begin VB.TextBox EcRubrDtCri 
            Height          =   285
            Left            =   960
            TabIndex        =   196
            Top             =   1800
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox EcRubrUserCri 
            Height          =   285
            Left            =   1800
            TabIndex        =   195
            Top             =   1800
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox EcRubrFlgExecHosp 
            Height          =   285
            Left            =   2760
            TabIndex        =   194
            Top             =   1800
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo"
            Height          =   255
            Index           =   47
            Left            =   240
            TabIndex        =   204
            Top             =   480
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Descri��o"
            Height          =   255
            Index           =   48
            Left            =   2280
            TabIndex        =   203
            Top             =   480
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo"
            Height          =   255
            Index           =   49
            Left            =   240
            TabIndex        =   202
            Top             =   1200
            Width           =   615
         End
      End
      Begin VB.TextBox EcCodAnaFacturar 
         Height          =   285
         Left            =   -65160
         TabIndex        =   220
         Top             =   495
         Width           =   735
      End
      Begin VB.CheckBox CkFlgfacturarAna 
         Caption         =   "Pergunta se pretende facturar an�lise:"
         Height          =   195
         Left            =   -68160
         TabIndex        =   219
         Top             =   495
         Width           =   3015
      End
      Begin VB.CheckBox CkFlgRegra 
         Caption         =   "S� factura se n�o marcada a an�lise:"
         Height          =   195
         Left            =   -68160
         TabIndex        =   218
         Top             =   855
         Width           =   3015
      End
      Begin VB.TextBox EcCodAnaRegra 
         Height          =   285
         Left            =   -65160
         TabIndex        =   217
         Top             =   855
         Width           =   735
      End
      Begin VB.CheckBox CkContaMembros 
         Caption         =   "Envia Membros para FACTUS"
         Height          =   255
         Left            =   -68160
         TabIndex        =   216
         Top             =   1200
         Width           =   2535
      End
      Begin VB.TextBox EcQtd 
         Height          =   285
         Left            =   -63120
         TabIndex        =   215
         Top             =   480
         Width           =   735
      End
      Begin VB.TextBox EcCodRubrica 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -73680
         TabIndex        =   213
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox EcDescrRubrica 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -72840
         TabIndex        =   212
         Top             =   600
         Width           =   3735
      End
      Begin VB.CommandButton BtPesquisaRubrica 
         Height          =   315
         Left            =   -69120
         Picture         =   "FormCodAna.frx":A362
         Style           =   1  'Graphical
         TabIndex        =   211
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida R�bricas"
         Top             =   600
         Width           =   375
      End
      Begin VB.CommandButton BtAdicionarRubrica 
         Height          =   315
         Left            =   -74880
         Picture         =   "FormCodAna.frx":A6EC
         Style           =   1  'Graphical
         TabIndex        =   210
         TabStop         =   0   'False
         ToolTipText     =   "Adicionar R�brica ao FACTUS"
         Top             =   1200
         Width           =   375
      End
      Begin VB.TextBox EcRubrSeqAna 
         Height          =   285
         Left            =   -66000
         TabIndex        =   209
         Top             =   1800
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton BtGravarAnaFact 
         Height          =   315
         Left            =   -73680
         Picture         =   "FormCodAna.frx":AA76
         Style           =   1  'Graphical
         TabIndex        =   208
         ToolTipText     =   "Gravar Mapeamento Factura��o"
         Top             =   1200
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox EcAuxPreco 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -72840
         TabIndex        =   207
         Top             =   2160
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.CommandButton BtCopiaPrecario 
         Height          =   315
         Left            =   -74400
         Picture         =   "FormCodAna.frx":AE00
         Style           =   1  'Graphical
         TabIndex        =   206
         ToolTipText     =   "Copiar Tabela de Outra An�lise"
         Top             =   1200
         Width           =   375
      End
      Begin VB.CheckBox CkSegundoRes 
         Caption         =   "Segundo Resultado Disponivel"
         Height          =   195
         Left            =   -74880
         TabIndex        =   192
         Top             =   480
         Width           =   3375
      End
      Begin VB.Frame FrameSegundoRes 
         Height          =   5415
         Left            =   -75000
         TabIndex        =   165
         Top             =   720
         Width           =   12495
         Begin VB.TextBox EcLimiteSup2Res 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2040
            TabIndex        =   184
            Top             =   1560
            Width           =   1455
         End
         Begin VB.TextBox EcLimiteInf2Res 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2040
            TabIndex        =   183
            Top             =   1200
            Width           =   1455
         End
         Begin VB.TextBox EcValorDefeito2Res 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2040
            TabIndex        =   182
            Top             =   840
            Width           =   1455
         End
         Begin VB.CommandButton BtVerificaFormula 
            Height          =   375
            Index           =   3
            Left            =   5280
            Picture         =   "FormCodAna.frx":B18A
            Style           =   1  'Graphical
            TabIndex        =   181
            ToolTipText     =   " Ajuda na constru��o da f�rmula "
            Top             =   2640
            Width           =   400
         End
         Begin VB.CommandButton BtVerificaFormula 
            Height          =   375
            Index           =   2
            Left            =   5280
            Picture         =   "FormCodAna.frx":B2D4
            Style           =   1  'Graphical
            TabIndex        =   180
            ToolTipText     =   " Ajuda na constru��o da f�rmula "
            Top             =   2280
            Width           =   400
         End
         Begin VB.TextBox EcFormula2Res 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2040
            TabIndex        =   179
            Top             =   2280
            Width           =   3280
         End
         Begin VB.TextBox EcCasasDecimais2Res 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2040
            TabIndex        =   178
            Top             =   1920
            Width           =   1455
         End
         Begin VB.TextBox EcFormulaFem2Res 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2040
            TabIndex        =   177
            Top             =   2640
            Width           =   3285
         End
         Begin VB.ComboBox EcTipoResultado2Res 
            Height          =   315
            Left            =   2040
            Style           =   2  'Dropdown List
            TabIndex        =   176
            Top             =   360
            Width           =   1695
         End
         Begin VB.Frame Frame8 
            Caption         =   "Unidades"
            Height          =   1575
            Index           =   1
            Left            =   6960
            TabIndex        =   167
            Top             =   240
            Width           =   5055
            Begin VB.OptionButton OptUnidade2Res2 
               Caption         =   "Unidade Medida 2"
               Height          =   255
               Left            =   120
               TabIndex        =   174
               Top             =   720
               Width           =   1935
            End
            Begin VB.TextBox EcUnidade2Res2 
               Appearance      =   0  'Flat
               Height          =   285
               Left            =   2160
               TabIndex        =   173
               Top             =   720
               Width           =   1215
            End
            Begin VB.ComboBox EcTipoUnidadeAux4 
               Height          =   315
               Left            =   3360
               Style           =   2  'Dropdown List
               TabIndex        =   172
               Top             =   720
               Width           =   1455
            End
            Begin VB.OptionButton OptUnidade1Res2 
               Caption         =   "Unidade Medida 1"
               Height          =   195
               Left            =   120
               TabIndex        =   171
               Top             =   240
               Width           =   1935
            End
            Begin VB.TextBox EcUnidade1Res2 
               Appearance      =   0  'Flat
               Height          =   285
               Left            =   2160
               TabIndex        =   170
               Top             =   240
               Width           =   1215
            End
            Begin VB.ComboBox EcTipoUnidadeAux3 
               Height          =   315
               Left            =   3360
               Style           =   2  'Dropdown List
               TabIndex        =   169
               Top             =   240
               Width           =   1455
            End
            Begin VB.TextBox EcFactorConv2Res 
               Appearance      =   0  'Flat
               Height          =   285
               Left            =   2160
               TabIndex        =   168
               Top             =   1200
               Width           =   1215
            End
            Begin VB.Label Label1 
               Caption         =   "Factor de Convers�o"
               Height          =   255
               Index           =   60
               Left            =   120
               TabIndex        =   175
               Top             =   1200
               Width           =   1575
            End
         End
         Begin VB.CheckBox CkFormLeuc2Res 
            Caption         =   "F�rmula Leucocit�ria"
            Height          =   255
            Left            =   6960
            TabIndex        =   166
            Top             =   1920
            Width           =   2000
         End
         Begin VB.Label Label1 
            Caption         =   "Limite Superior"
            Height          =   255
            Index           =   64
            Left            =   240
            TabIndex        =   191
            Top             =   1560
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Limite Inferior"
            Height          =   255
            Index           =   63
            Left            =   240
            TabIndex        =   190
            Top             =   1200
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Valor por Defeito"
            Height          =   255
            Index           =   61
            Left            =   240
            TabIndex        =   189
            Top             =   840
            Width           =   1335
         End
         Begin VB.Label Label1 
            Caption         =   "Casas Decimais"
            Height          =   255
            Index           =   67
            Left            =   240
            TabIndex        =   188
            Top             =   1920
            Width           =   1335
         End
         Begin VB.Label Label1 
            Caption         =   "F�rmula Masc."
            Height          =   255
            Index           =   65
            Left            =   240
            TabIndex        =   187
            Top             =   2280
            Width           =   1335
         End
         Begin VB.Label Label1 
            Caption         =   "F�rmula Fem."
            Height          =   255
            Index           =   66
            Left            =   240
            TabIndex        =   186
            Top             =   2640
            Width           =   1335
         End
         Begin VB.Label Label5 
            Caption         =   "Tipo de Resultado"
            Height          =   255
            Left            =   240
            TabIndex        =   185
            Top             =   360
            Width           =   1455
         End
      End
      Begin VB.CheckBox CkCopiaRes 
         Caption         =   "Copiar Resultados dentro Requisi��o"
         Height          =   255
         Left            =   -70440
         TabIndex        =   164
         Top             =   2160
         Width           =   3135
      End
      Begin VB.CheckBox CkAvaliaEpid 
         Caption         =   "Avalia��o Epidemiol�gica"
         Height          =   255
         Left            =   -70440
         TabIndex        =   163
         Top             =   840
         Width           =   3375
      End
      Begin VB.TextBox EcTextoSMS 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   -70200
         MultiLine       =   -1  'True
         TabIndex        =   162
         Top             =   1440
         Width           =   2175
      End
      Begin VB.CheckBox CkEnvioSMS 
         Caption         =   "Envio de resultados por SMS"
         Height          =   255
         Left            =   -70440
         TabIndex        =   161
         ToolTipText     =   "Utilizar $cod_analise para o 1� resultado e @cod_analise para o 2� resultado"
         Top             =   1200
         Width           =   2655
      End
      Begin VB.CheckBox CkResExterior 
         Caption         =   "Envio de Resultados para Exterior"
         Height          =   255
         Index           =   0
         Left            =   -70440
         TabIndex        =   155
         Top             =   480
         Width           =   3375
      End
      Begin VB.CheckBox CkResAnt 
         Caption         =   "Restringir Pesquisa Resultados Anteriores"
         Height          =   255
         Left            =   -74640
         TabIndex        =   154
         Top             =   5760
         Width           =   3375
      End
      Begin VB.CheckBox CkTransManual 
         Caption         =   "Transmiss�o Manual"
         Height          =   255
         Left            =   -74640
         TabIndex        =   149
         Top             =   5420
         Width           =   2055
      End
      Begin VB.TextBox EcPrazoVal 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -63960
         TabIndex        =   147
         Top             =   1320
         Width           =   1335
      End
      Begin VB.TextBox EcOrdemARS 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0,00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   -63960
         TabIndex        =   144
         Top             =   3240
         Width           =   1335
      End
      Begin VB.TextBox EcETA 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0,00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   -63960
         TabIndex        =   142
         Top             =   2880
         Width           =   1335
      End
      Begin VB.CheckBox CkCutOff 
         Caption         =   "Usa Cut-Off"
         Height          =   255
         Left            =   -74640
         TabIndex        =   141
         Top             =   5100
         Width           =   2055
      End
      Begin VB.CheckBox CkNaoAvaliaResultados 
         Caption         =   "N�o avaliar Resultados"
         Height          =   255
         Left            =   -74640
         TabIndex        =   140
         Top             =   4740
         Width           =   2055
      End
      Begin VB.ListBox EcAnaLocaisExec 
         Appearance      =   0  'Flat
         Height          =   930
         Left            =   1680
         Style           =   1  'Checkbox
         TabIndex        =   124
         Top             =   5220
         Width           =   4455
      End
      Begin VB.Frame Frame2 
         Height          =   3615
         Left            =   240
         TabIndex        =   99
         Top             =   540
         Width           =   6135
         Begin VB.CommandButton BtPesquisaGrImpr 
            Height          =   315
            Left            =   5520
            Picture         =   "FormCodAna.frx":B41E
            Style           =   1  'Graphical
            TabIndex        =   153
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Grupos de An�lises"
            Top             =   1635
            Width           =   375
         End
         Begin VB.TextBox EcDescrGrImpr 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   152
            TabStop         =   0   'False
            Top             =   1635
            Width           =   3375
         End
         Begin VB.TextBox EcCodGrImpr 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   151
            Top             =   1635
            Width           =   735
         End
         Begin VB.TextBox EcCodProd 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   120
            Top             =   2160
            Width           =   735
         End
         Begin VB.TextBox EcDescrProduto 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   119
            TabStop         =   0   'False
            Top             =   2160
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaProduto 
            Height          =   315
            Left            =   5520
            Picture         =   "FormCodAna.frx":B7A8
            Style           =   1  'Graphical
            TabIndex        =   118
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Produtos"
            Top             =   2160
            Width           =   375
         End
         Begin VB.TextBox EcCodTuboP 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   117
            Top             =   2640
            Width           =   735
         End
         Begin VB.TextBox EcDescrTuboP 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   116
            TabStop         =   0   'False
            Top             =   2640
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaTuboP 
            Height          =   315
            Left            =   5520
            Picture         =   "FormCodAna.frx":BB32
            Style           =   1  'Graphical
            TabIndex        =   115
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida Tubos Prim�rios"
            Top             =   2640
            Width           =   375
         End
         Begin VB.TextBox EcCodTuboS 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   114
            Top             =   3120
            Width           =   735
         End
         Begin VB.TextBox EcDescrTuboS 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   113
            TabStop         =   0   'False
            Top             =   3120
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaTuboS 
            Height          =   315
            Left            =   5520
            Picture         =   "FormCodAna.frx":BEBC
            Style           =   1  'Graphical
            TabIndex        =   112
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Tubos Secund�rios"
            Top             =   3120
            Width           =   375
         End
         Begin VB.TextBox EcCodGrupo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   108
            Top             =   240
            Width           =   735
         End
         Begin VB.TextBox EcDescrGrupo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   107
            TabStop         =   0   'False
            Top             =   240
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaGrupo 
            Height          =   315
            Left            =   5520
            Picture         =   "FormCodAna.frx":C246
            Style           =   1  'Graphical
            TabIndex        =   106
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Grupos de An�lises"
            Top             =   240
            Width           =   375
         End
         Begin VB.TextBox EcCodSubGrupo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   105
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox EcDescrSubGrupo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   104
            TabStop         =   0   'False
            Top             =   720
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaSubGrupo 
            Height          =   315
            Left            =   5520
            Picture         =   "FormCodAna.frx":C5D0
            Style           =   1  'Graphical
            TabIndex        =   103
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Sub Grupos"
            Top             =   720
            Width           =   375
         End
         Begin VB.TextBox EcCodClasse 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1440
            TabIndex        =   102
            Top             =   1200
            Width           =   735
         End
         Begin VB.TextBox EcDescrClasse 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   101
            TabStop         =   0   'False
            Top             =   1200
            Width           =   3375
         End
         Begin VB.CommandButton BtPesquisaClasse 
            Height          =   315
            Left            =   5520
            Picture         =   "FormCodAna.frx":C95A
            Style           =   1  'Graphical
            TabIndex        =   100
            TabStop         =   0   'False
            ToolTipText     =   "Pesquisa R�pida de Classes"
            Top             =   1200
            Width           =   375
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo Impress."
            Height          =   255
            Index           =   62
            Left            =   240
            TabIndex        =   150
            Top             =   1680
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Produto"
            Height          =   255
            Index           =   7
            Left            =   240
            TabIndex        =   123
            Top             =   2160
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Tubo Prim�rio"
            Height          =   255
            Index           =   8
            Left            =   240
            TabIndex        =   122
            Top             =   2640
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "Tubo Aliquota"
            Height          =   255
            Index           =   9
            Left            =   240
            TabIndex        =   121
            Top             =   3120
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo"
            Height          =   255
            Index           =   4
            Left            =   240
            TabIndex        =   111
            Top             =   240
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "SubGrupo"
            Height          =   255
            Index           =   5
            Left            =   240
            TabIndex        =   110
            Top             =   720
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Classe"
            Height          =   255
            Index           =   6
            Left            =   240
            TabIndex        =   109
            Top             =   1200
            Width           =   855
         End
      End
      Begin VB.TextBox EcValGrafico 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0;(0)"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   -63960
         TabIndex        =   38
         Top             =   1740
         Width           =   1335
      End
      Begin VB.TextBox EcValMinGrafico 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0;(0)"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   -63960
         TabIndex        =   39
         Top             =   2100
         Width           =   1335
      End
      Begin VB.TextBox EcValMaxGrafico 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0;(0)"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   -63960
         TabIndex        =   40
         Top             =   2460
         Width           =   1335
      End
      Begin VB.ComboBox CbSexo 
         Height          =   315
         Left            =   -63960
         Style           =   2  'Dropdown List
         TabIndex        =   36
         Top             =   540
         Width           =   1335
      End
      Begin VB.TextBox EcPrazoConc 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -63960
         TabIndex        =   37
         Top             =   900
         Width           =   1335
      End
      Begin VB.TextBox EcCodEstatistica 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11520
         TabIndex        =   21
         Top             =   4800
         Width           =   735
      End
      Begin VB.TextBox EcCodEstatC 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11520
         TabIndex        =   22
         Top             =   5160
         Width           =   735
      End
      Begin VB.TextBox EcCodEstatP 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11520
         TabIndex        =   23
         Top             =   5520
         Width           =   735
      End
      Begin VB.ListBox EcAnaLocais 
         Appearance      =   0  'Flat
         Height          =   930
         Left            =   1680
         Style           =   1  'Checkbox
         TabIndex        =   5
         Top             =   4260
         Width           =   4455
      End
      Begin VB.TextBox EcDescrEtiq 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8520
         TabIndex        =   18
         Top             =   5160
         Width           =   1455
      End
      Begin VB.TextBox EcPeso 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11520
         TabIndex        =   20
         Top             =   4440
         Width           =   735
      End
      Begin VB.TextBox EcCasasDecimais 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11520
         TabIndex        =   19
         Top             =   4080
         Width           =   735
      End
      Begin VB.TextBox EcLimiteInf 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8520
         TabIndex        =   16
         Top             =   4455
         Width           =   1455
      End
      Begin VB.TextBox EcLimiteSup 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8520
         TabIndex        =   17
         Top             =   4800
         Width           =   1455
      End
      Begin VB.TextBox EcValorDefeito 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8520
         TabIndex        =   15
         Top             =   4080
         Width           =   1455
      End
      Begin VB.CheckBox CkFacturar 
         Caption         =   "An�lise a facturar"
         Height          =   255
         Left            =   -74640
         TabIndex        =   34
         Top             =   3960
         Width           =   2055
      End
      Begin VB.CheckBox CkRAST 
         Caption         =   "An�lise do tipo RAST"
         Height          =   255
         Left            =   -74640
         TabIndex        =   35
         Top             =   4320
         Width           =   2055
      End
      Begin VB.CheckBox CkNegrito 
         Caption         =   "An�lise a Negrito"
         Height          =   255
         Left            =   -74640
         TabIndex        =   33
         Top             =   3600
         Width           =   2055
      End
      Begin VB.CheckBox CkSemHistorico 
         Caption         =   "An�lise N�o Usa Hist�rico"
         Height          =   255
         Left            =   -74640
         TabIndex        =   32
         Top             =   3240
         Width           =   2415
      End
      Begin VB.CheckBox CkExterior 
         Caption         =   "An�lise Efectuada no Exterior"
         Height          =   255
         Left            =   -74640
         TabIndex        =   31
         Top             =   2880
         Width           =   2415
      End
      Begin VB.CheckBox CkAcreditacao 
         Caption         =   "An�lise Acreditada"
         Height          =   255
         Left            =   -74640
         TabIndex        =   30
         Top             =   2520
         Width           =   2415
      End
      Begin VB.CheckBox CkInibeMarcacao 
         Caption         =   "Inibe marca��o de an�lise"
         Height          =   255
         Left            =   -74640
         TabIndex        =   29
         Top             =   2160
         Width           =   2175
      End
      Begin VB.CheckBox CkInibeDescricao 
         Caption         =   "Inibe designa��o no relat�rio"
         Height          =   255
         Left            =   -74640
         TabIndex        =   28
         Top             =   1800
         Width           =   2415
      End
      Begin VB.CheckBox CkEtiqueta 
         Caption         =   "Imprimir abreviatura da an�lise na etiqueta"
         Height          =   375
         Left            =   -74640
         TabIndex        =   26
         Top             =   1080
         Width           =   3915
      End
      Begin VB.CheckBox CkMarcAuto 
         Caption         =   "Marca��o autom�tica dentro de complexas"
         Height          =   255
         Left            =   -74640
         TabIndex        =   24
         Top             =   480
         Width           =   3500
      End
      Begin VB.CheckBox CkOpcional 
         Caption         =   "Opcional dentro de complexas"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -74640
         TabIndex        =   25
         Top             =   840
         Width           =   2535
      End
      Begin VB.CheckBox CkFormLeuc 
         Caption         =   "Avaliar 100%"
         Height          =   255
         Left            =   -74640
         TabIndex        =   27
         Top             =   1485
         Width           =   1275
      End
      Begin VB.Frame Frame9 
         Caption         =   "F�rmula"
         Height          =   1215
         Left            =   7200
         TabIndex        =   50
         Top             =   2400
         Width           =   5055
         Begin VB.CommandButton BtVerificaFormula 
            Height          =   375
            Index           =   1
            Left            =   3660
            Picture         =   "FormCodAna.frx":CCE4
            Style           =   1  'Graphical
            TabIndex        =   156
            ToolTipText     =   " Ajuda na constru��o da f�rmula "
            Top             =   720
            Width           =   400
         End
         Begin VB.CommandButton BtVerificaFormula 
            Height          =   375
            Index           =   0
            Left            =   3660
            Picture         =   "FormCodAna.frx":CE2E
            Style           =   1  'Graphical
            TabIndex        =   52
            ToolTipText     =   " Ajuda na constru��o da f�rmula "
            Top             =   240
            Width           =   400
         End
         Begin VB.CommandButton EcAjuda 
            Height          =   855
            Left            =   4065
            Picture         =   "FormCodAna.frx":CF78
            Style           =   1  'Graphical
            TabIndex        =   51
            ToolTipText     =   " Ajuda na constru��o da f�rmula "
            Top             =   240
            Width           =   400
         End
         Begin VB.TextBox EcFormula 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   600
            TabIndex        =   13
            Top             =   255
            Width           =   3075
         End
         Begin VB.TextBox EcFormulaFem 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   600
            TabIndex        =   14
            Top             =   720
            Width           =   3045
         End
         Begin VB.Label Label1 
            Caption         =   "Masc"
            Height          =   255
            Index           =   14
            Left            =   120
            TabIndex        =   54
            Top             =   240
            Width           =   375
         End
         Begin VB.Label Label1 
            Caption         =   "Fem"
            Height          =   255
            Index           =   15
            Left            =   120
            TabIndex        =   53
            Top             =   720
            Width           =   375
         End
      End
      Begin VB.Frame Frame8 
         Caption         =   "Unidades"
         Height          =   1575
         Index           =   0
         Left            =   7200
         TabIndex        =   46
         Top             =   540
         Width           =   5055
         Begin VB.TextBox EcFactorConv 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2280
            TabIndex        =   12
            Top             =   1200
            Width           =   1215
         End
         Begin VB.OptionButton OptUnidade1 
            Height          =   195
            Left            =   240
            TabIndex        =   48
            Top             =   240
            Width           =   375
         End
         Begin VB.OptionButton OptUnidade2 
            Height          =   255
            Left            =   240
            TabIndex        =   47
            Top             =   720
            Width           =   375
         End
         Begin VB.TextBox EcUnidade1 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2280
            TabIndex        =   7
            Top             =   240
            Width           =   1215
         End
         Begin VB.ComboBox EcTipoUnidadeAux 
            Height          =   315
            Left            =   3480
            Style           =   2  'Dropdown List
            TabIndex        =   8
            Top             =   240
            Width           =   1455
         End
         Begin VB.TextBox EcUnidade2 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2280
            TabIndex        =   10
            Top             =   720
            Width           =   1215
         End
         Begin VB.ComboBox EcTipoUnidadeAux2 
            Height          =   315
            Left            =   3480
            Style           =   2  'Dropdown List
            TabIndex        =   11
            Top             =   720
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Factor de Convers�o"
            Height          =   255
            Index           =   13
            Left            =   600
            TabIndex        =   49
            Top             =   1200
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Unidade Medida 1"
            Height          =   255
            Index           =   11
            Left            =   600
            TabIndex        =   6
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Unidade Medida 2"
            Height          =   255
            Index           =   12
            Left            =   600
            TabIndex        =   9
            Top             =   720
            Width           =   1455
         End
      End
      Begin MSFlexGridLib.MSFlexGrid FgPrecos 
         Height          =   4095
         Left            =   -74880
         TabIndex        =   205
         Top             =   1560
         Width           =   12615
         _ExtentX        =   22251
         _ExtentY        =   7223
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
      End
      Begin MSFlexGridLib.MSFlexGrid FgValRef 
         Height          =   1455
         Left            =   -74880
         TabIndex        =   257
         Top             =   1200
         Width           =   4095
         _ExtentX        =   7223
         _ExtentY        =   2566
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
      End
      Begin VB.CommandButton BtCutOff 
         Height          =   825
         Left            =   -63720
         Picture         =   "FormCodAna.frx":D502
         Style           =   1  'Graphical
         TabIndex        =   281
         ToolTipText     =   "Inserir dados de Cut-Off"
         Top             =   1560
         Visible         =   0   'False
         Width           =   840
      End
      Begin RichTextLib.RichTextBox RTBComent 
         Height          =   2055
         Left            =   -73800
         TabIndex        =   314
         Top             =   540
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   3625
         _Version        =   393217
         TextRTF         =   $"FormCodAna.frx":D80C
      End
      Begin VB.Label Label1 
         Caption         =   "Cod. An�lise SINAVE"
         Height          =   375
         Index           =   33
         Left            =   7200
         TabIndex        =   334
         Top             =   5520
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Folhas Trab."
         Height          =   255
         Index           =   54
         Left            =   -74880
         TabIndex        =   321
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Informa��es"
         Height          =   255
         Index           =   58
         Left            =   -68400
         TabIndex        =   320
         Top             =   780
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Quest�es"
         Height          =   255
         Index           =   59
         Left            =   -74880
         TabIndex        =   319
         Top             =   2820
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Coment�rio"
         Height          =   255
         Index           =   16
         Left            =   -74880
         TabIndex        =   315
         Top             =   480
         Width           =   975
      End
      Begin VB.Label LbCmMinM 
         Caption         =   "Coment�rio :"
         Height          =   255
         Left            =   -69360
         TabIndex        =   306
         Top             =   4200
         Width           =   1095
      End
      Begin VB.Label LbCmMaxM 
         Caption         =   "Coment�rio :"
         Height          =   255
         Left            =   -69360
         TabIndex        =   305
         Top             =   4560
         Width           =   975
      End
      Begin VB.Label LbCmMinF 
         Caption         =   "Coment�rio :"
         Height          =   255
         Left            =   -69360
         TabIndex        =   304
         Top             =   5160
         Width           =   975
      End
      Begin VB.Label LbCmMaxF 
         Caption         =   "Coment�rio :"
         Height          =   255
         Left            =   -69360
         TabIndex        =   303
         Top             =   5520
         Width           =   975
      End
      Begin VB.Label LbMaxM 
         Caption         =   "M�ximo :"
         Height          =   255
         Left            =   -73680
         TabIndex        =   302
         Top             =   4560
         Width           =   735
      End
      Begin VB.Label LbMinM 
         Caption         =   "Minimo :"
         Height          =   255
         Left            =   -73680
         TabIndex        =   301
         Top             =   4200
         Width           =   735
      End
      Begin VB.Label LbMasc 
         Caption         =   "Masculino"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74640
         TabIndex        =   300
         Top             =   4320
         Width           =   855
      End
      Begin VB.Label LbMaxF 
         Caption         =   "M�ximo :"
         Height          =   255
         Left            =   -73680
         TabIndex        =   299
         Top             =   5520
         Width           =   735
      End
      Begin VB.Label LbMinF 
         Caption         =   "Minimo :"
         Height          =   255
         Left            =   -73680
         TabIndex        =   298
         Top             =   5160
         Width           =   735
      End
      Begin VB.Label LbFem 
         Caption         =   "Feminino"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74640
         TabIndex        =   297
         Top             =   5280
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "M�todo"
         Height          =   255
         Index           =   44
         Left            =   -70200
         TabIndex        =   271
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Reagente"
         Height          =   255
         Index           =   45
         Left            =   -70200
         TabIndex        =   270
         Top             =   1560
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Esp�cie"
         Height          =   255
         Index           =   52
         Left            =   -70200
         TabIndex        =   269
         Top             =   2040
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Obs."
         Height          =   255
         Index           =   55
         Left            =   -70200
         TabIndex        =   268
         Top             =   2460
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Quantidade:"
         Height          =   255
         Index           =   53
         Left            =   -64080
         TabIndex        =   221
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Rubrica"
         Height          =   255
         Index           =   46
         Left            =   -74640
         TabIndex        =   214
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Validade"
         Height          =   255
         Index           =   41
         Left            =   -65760
         TabIndex        =   148
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label Label57 
         Caption         =   "Ordem ARS"
         Height          =   255
         Index           =   1
         Left            =   -65760
         TabIndex        =   145
         Top             =   3240
         Width           =   1815
      End
      Begin VB.Label Label57 
         Caption         =   "ETa%"
         Height          =   255
         Index           =   0
         Left            =   -65760
         TabIndex        =   143
         Top             =   2880
         Width           =   1815
      End
      Begin VB.Label Label57 
         Caption         =   "Valor eixo X"
         Height          =   255
         Index           =   2
         Left            =   -65760
         TabIndex        =   94
         Top             =   1740
         Width           =   1815
      End
      Begin VB.Label Label57 
         Caption         =   "Val Minimo"
         Height          =   255
         Index           =   3
         Left            =   -65760
         TabIndex        =   93
         Top             =   2100
         Width           =   1815
      End
      Begin VB.Label Label57 
         Caption         =   "Val Maximo"
         Height          =   255
         Index           =   4
         Left            =   -65760
         TabIndex        =   92
         Top             =   2460
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "An�lise S� marcada em:"
         Height          =   255
         Index           =   42
         Left            =   -65760
         TabIndex        =   91
         Top             =   540
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Tempo Conclus�o"
         Height          =   255
         Index           =   40
         Left            =   -65760
         TabIndex        =   90
         Top             =   900
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "C�d. Estatistica"
         Height          =   255
         Index           =   26
         Left            =   10200
         TabIndex        =   66
         Top             =   4845
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "C�d. Estat Compl"
         Height          =   255
         Index           =   27
         Left            =   10200
         TabIndex        =   65
         Top             =   5160
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "C�d. Estat Perfil"
         Height          =   255
         Index           =   28
         Left            =   10200
         TabIndex        =   64
         Top             =   5520
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Locais de Marc."
         Height          =   255
         Index           =   30
         Left            =   240
         TabIndex        =   63
         Top             =   4260
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "Local Exec."
         Height          =   255
         Index           =   29
         Left            =   240
         TabIndex        =   62
         Top             =   5340
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Design. Etiqueta"
         Height          =   255
         Index           =   20
         Left            =   7200
         TabIndex        =   61
         Top             =   5180
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Peso estat�stico"
         Height          =   255
         Index           =   25
         Left            =   10200
         TabIndex        =   60
         Top             =   4440
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Casas decimais"
         Height          =   255
         Index           =   23
         Left            =   10200
         TabIndex        =   59
         Top             =   4140
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Limite Inferior"
         Height          =   255
         Index           =   18
         Left            =   7200
         TabIndex        =   58
         Top             =   4500
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Limite Superior"
         Height          =   255
         Index           =   19
         Left            =   7200
         TabIndex        =   57
         Top             =   4860
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "Valor por Defeito"
         Height          =   255
         Index           =   17
         Left            =   7200
         TabIndex        =   56
         Top             =   4140
         Width           =   1335
      End
   End
   Begin VB.TextBox EcQuantMinima 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   10080
      TabIndex        =   138
      Tag             =   "131(12,4)"
      Top             =   9360
      Width           =   1455
   End
   Begin VB.TextBox EcLinhasBrancoImp 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   5640
      TabIndex        =   134
      Top             =   9360
      Width           =   735
   End
   Begin VB.CheckBox CkConclRAST 
      Caption         =   "Inserir Estas conclus�es em todas an�lises do tipo Rast"
      Height          =   255
      Left            =   0
      TabIndex        =   133
      Top             =   9360
      Width           =   4815
   End
   Begin VB.TextBox EcColunaPreco 
      Height          =   285
      Left            =   10920
      TabIndex        =   131
      Top             =   8880
      Width           =   615
   End
   Begin VB.TextBox EcLinhaPreco 
      Height          =   285
      Left            =   10920
      TabIndex        =   129
      Top             =   8520
      Width           =   615
   End
   Begin VB.TextBox EcTipoUnidade 
      Height          =   285
      Left            =   9360
      TabIndex        =   126
      Top             =   8520
      Width           =   615
   End
   Begin VB.TextBox EcTipoUnidade2 
      Height          =   285
      Left            =   9360
      TabIndex        =   125
      Top             =   8880
      Width           =   615
   End
   Begin VB.TextBox EcCodLocal 
      Height          =   285
      Left            =   7440
      TabIndex        =   97
      Top             =   8880
      Width           =   615
   End
   Begin VB.CommandButton BtPesqRapAna 
      Height          =   340
      Left            =   9240
      Picture         =   "FormCodAna.frx":D88E
      Style           =   1  'Graphical
      TabIndex        =   96
      ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
      Top             =   120
      Width           =   375
   End
   Begin VB.CommandButton BtCopiaDescr 
      Height          =   360
      Left            =   9240
      Picture         =   "FormCodAna.frx":DC18
      Style           =   1  'Graphical
      TabIndex        =   95
      ToolTipText     =   "Copiar Descri��o"
      Top             =   575
      Width           =   375
   End
   Begin VB.CheckBox CkInvisivel 
      Caption         =   "An�lise Cancelada"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   9720
      TabIndex        =   89
      Top             =   600
      Width           =   2055
   End
   Begin VB.TextBox EcOrdem 
      Height          =   285
      Left            =   7440
      TabIndex        =   87
      Top             =   8520
      Width           =   615
   End
   Begin VB.TextBox EcDataAlteracao 
      Height          =   285
      Left            =   5760
      TabIndex        =   85
      Top             =   8880
      Width           =   615
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Height          =   285
      Left            =   5760
      TabIndex        =   83
      Top             =   8520
      Width           =   615
   End
   Begin VB.TextBox EcDataCriacao 
      Height          =   285
      Left            =   3720
      TabIndex        =   81
      Top             =   8880
      Width           =   615
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Height          =   285
      Left            =   3720
      TabIndex        =   79
      Top             =   8520
      Width           =   615
   End
   Begin VB.TextBox EcUnidadeUso 
      Height          =   285
      Left            =   1440
      TabIndex        =   77
      Top             =   8880
      Width           =   615
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   1440
      TabIndex        =   76
      Top             =   8520
      Width           =   615
   End
   Begin VB.Frame Frame1 
      Height          =   855
      Index           =   0
      Left            =   240
      TabIndex        =   67
      Top             =   7320
      Width           =   12615
      Begin VB.Label LbHrAct 
         Height          =   255
         Left            =   3600
         TabIndex        =   75
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataAlteracao 
         Height          =   255
         Left            =   2400
         TabIndex        =   74
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Altera��o"
         Height          =   255
         Index           =   32
         Left            =   120
         TabIndex        =   73
         Top             =   480
         Width           =   735
      End
      Begin VB.Label LaUtilAlteracao 
         Height          =   255
         Left            =   960
         TabIndex        =   72
         Top             =   480
         Width           =   1575
      End
      Begin VB.Label LbHrCri 
         Height          =   255
         Left            =   3600
         TabIndex        =   71
         Top             =   120
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Height          =   255
         Left            =   2400
         TabIndex        =   70
         Top             =   120
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Cria��o"
         Height          =   255
         Index           =   31
         Left            =   120
         TabIndex        =   69
         Top             =   120
         Width           =   615
      End
      Begin VB.Label LaUtilCriacao 
         Height          =   255
         Left            =   960
         TabIndex        =   68
         Top             =   120
         Width           =   1575
      End
   End
   Begin VB.ComboBox EcTipoResultado 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   11280
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   120
      Width           =   1695
   End
   Begin VB.TextBox EcAbreviatura 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      Height          =   285
      Left            =   1080
      TabIndex        =   1
      Top             =   600
      Width           =   1095
   End
   Begin VB.TextBox EcDescrRelatorio 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      Height          =   285
      Left            =   3840
      TabIndex        =   3
      Top             =   600
      Width           =   5295
   End
   Begin VB.TextBox EcDesignacao 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      Height          =   285
      Left            =   3840
      TabIndex        =   2
      Top             =   120
      Width           =   5295
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      Height          =   285
      Left            =   1080
      TabIndex        =   0
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "EcTipoUnidade4"
      Height          =   375
      Index           =   70
      Left            =   7920
      TabIndex        =   160
      Top             =   9840
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Qtd. M�nima"
      Height          =   255
      Index           =   21
      Left            =   8760
      TabIndex        =   139
      Top             =   9360
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "Capacidade Tubo"
      Height          =   255
      Index           =   22
      Left            =   6360
      TabIndex        =   137
      Top             =   9360
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label LbCapacidadeUtil 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   7680
      TabIndex        =   136
      Top             =   9360
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Linhas em Branco"
      Height          =   255
      Index           =   24
      Left            =   4320
      TabIndex        =   135
      Top             =   9405
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcColunaPreco"
      Height          =   375
      Index           =   51
      Left            =   9960
      TabIndex        =   132
      Top             =   8880
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcLinhaPreco"
      Height          =   375
      Index           =   50
      Left            =   9960
      TabIndex        =   130
      Top             =   8520
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcTipoUnidade"
      Height          =   375
      Index           =   57
      Left            =   8400
      TabIndex        =   128
      Top             =   8520
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcTipoUnidade2"
      Height          =   375
      Index           =   56
      Left            =   8400
      TabIndex        =   127
      Top             =   8880
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcCodLocal"
      Height          =   375
      Index           =   43
      Left            =   6480
      TabIndex        =   98
      Top             =   8880
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcOrdem"
      Height          =   375
      Index           =   39
      Left            =   6480
      TabIndex        =   88
      Top             =   8520
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcDataAlteracao"
      Height          =   375
      Index           =   38
      Left            =   4440
      TabIndex        =   86
      Top             =   8880
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   375
      Index           =   37
      Left            =   4440
      TabIndex        =   84
      Top             =   8520
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcDataCriacao"
      Height          =   375
      Index           =   36
      Left            =   2400
      TabIndex        =   82
      Top             =   8880
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   375
      Index           =   35
      Left            =   2400
      TabIndex        =   80
      Top             =   8520
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcUnidadeUso"
      Height          =   375
      Index           =   34
      Left            =   120
      TabIndex        =   78
      Top             =   8880
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Tipo de Resultado"
      Height          =   255
      Index           =   10
      Left            =   9720
      TabIndex        =   55
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Abreviatura"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   44
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Desc. Relat�rio"
      Height          =   255
      Index           =   2
      Left            =   2520
      TabIndex        =   43
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Descri��o"
      Height          =   255
      Index           =   1
      Left            =   2520
      TabIndex        =   42
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   41
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "FormCodAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String
Dim TextoCamposObrigatoriosFactus() As String

Dim ChaveBD As String
Dim ChaveEc As Object


Dim NomeTabela As String
Dim CriterioTabela As String

Dim NomeTabelaFactus As String
Dim NumCamposFACTUS As Integer
Dim CamposFactusBD() As String
Dim CamposFactusEc() As Object
Dim ChaveBDFactus As String
Dim ChaveEcFactus As Object

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset


'Vari�vel para controlar se no Update o c�digo da an�lise foi modificado!
Dim sValCodAnalise As String
Dim Max As Integer
Dim Ind As Integer

Dim linhaActual As Integer
Dim colunaActual As Integer
' --------------------------------------
' Estrutura de Sinonimos
' --------------------------------------
Private Type Sinonimos
    seq_sinonimo As Long
    descr_sinonimo As String
    flg_original As String
End Type
Dim EstrutSinonimos() As Sinonimos
Dim TotalEstrutSinonimos As Integer

' --------------------------------------
' Estrutura de INTERFERENCIAS
' --------------------------------------
Private Type Interferencias
    formula As String
    cod_accao As Integer
    descr_accao As String
    obs As String
    obs_desbloq As String
    seq_interf As Long
End Type
Dim EstrutInterf() As Interferencias
Dim TotalEstrutinterf As Integer

Const lColValRefDataInf = 1
Const lColValRefDataSup = 2
Const lColValRefTipo = 0

Const lColConcFormula = 0
Const lColConcDescr = 1

Const lColEscRefEscInf = 0
Const lColEscRefTEscInf = 1
Const lColEscRefEscSup = 2
Const lColEscRefTEscSup = 3
Const lColEscRefHRefMin = 4
Const lColEscRefHRefMax = 5
Const lColEscRefFRefMin = 6
Const lColEscRefFRefMax = 7

Const lColPrecosCodPrecario = 0
Const lColPrecosDescrPrecario = 1
Const lColPrecosCodRubrEFR = 2
Const lColPrecosDescrRubrEFR = 3
Const lColPrecosTxModeradora = 4
Const lColPrecosTipoFact = 5
Const lColPrecosNumK = 6
Const lColPrecosNumC = 7
Const lColPrecosValPagEFR = 8
Const lColPrecosValPagDoe = 9
Const lColPrecosPercDoente = 10

' --------------------------------------
' Estrutura de Valores/Escaloes Ref
' --------------------------------------
Private Type cutoff
    cod_ana_ref As Long
    seq_cut_off As Long
    val_neg_min As String
    val_neg_max As String
    t_comp_nmin As String
    t_comp_nmax As String
    val_pos_min As String
    val_pos_max As String
    t_comp_pmin As String
    t_comp_pmax As String
    val_duv_min As String
    val_duv_max As String
    t_comp_dmin As String
    t_comp_dmax As String
    com_neg As String
    com_pos As String
    com_duv As String
End Type
Private Type EscRef
    seq_esc_ref As String
    t_esc_inf As String
    esc_inf As String
    esc_inf_dias As String
    t_esc_sup As String
    esc_sup As String
    esc_sup_dias As String
    h_ref_min As String
    h_ref_max As String
    f_ref_min As String
    f_ref_max As String
End Type
Private Type ValRef
    h_ref_min As String
    h_ref_max As String
    f_ref_min As String
    f_ref_max As String
    com_mah As String
    com_mih As String
    com_maf As String
    com_mif As String
End Type
Private Type AnaRef
    seq_ana_ref As String
    N_Res As Integer
    dt_valid_inf As String
    dt_valid_sup As String
    cod_reag As String
    cod_genero As String
    cod_metodo As String
    t_ref As String
    valoresRef As ValRef
    EscRef() As EscRef
    totalEscRef As Long
    cutoff As cutoff
    obs_ref As String
End Type
Private Type nResultado
    nres As Integer
    EstrutAnaRef() As AnaRef
    TotalAnaRef As Long
End Type
Dim nres(2) As nResultado
Dim nResActivo As Integer

Private Type Concl
    formula As String
    conclusao As String
End Type
Dim EstrutConcl() As Concl
Dim TotalConcl As Long

Private Type Precarios
    cod_rubr As String
    Portaria As String
    descr_Portaria As String
    cod_efr As String
    cod_rubr_efr As String
    descr_rubr_efr As String
    val_Taxa As String
    t_fac As String
    nr_k As String
    nr_c As String
    val_pag_ent As String
    perc_pag_doe As String
    val_pag_doe As String
    user_cri As String
    dt_cri As String
    user_act As String
    dt_act As String
    empresa_id As String
    valor_c As String
    
    flg_modificado As Boolean
End Type
Dim EstrutPrec() As Precarios
Dim TotalPrec As Long

Const FrameValRef_top = 3120
Const FrameValRef_left = 240

Const lCodDias = 0
Const lCodMeses = 1
Const lCodAnos = 2
Const lCodCrianca = 3
Const lCodAdulto = 4
' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties

Const lTabDadosAna = 0
Const lTabDadosCompl = 1
Const lTabDadosCompl2 = 2
Const lTabComConc = 3
Const lTabValRef = 4
Const lTabinterf = 5
Const lTabFact = 6
Const lTabSegRes = 7

Dim NumCampos2Res As Integer
Dim CamposBD2Res() As String
Dim CamposEc2Res() As Object
Dim TextoCamposObrigatorios2Res() As String
Dim ChaveBD2Res As String
Dim ChaveEc2Res As Object
Dim NomeTabela2Res As String
Dim CriterioTabela2Res As String
Dim Rs2Res As New ADODB.recordset

Sub FuncaoRemover()

    Dim sql As String
        
    gMsgTitulo = " Remover"
    gMsgMsg = "Tem a certeza que deseja cancelar esta an�lise ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    'Para os valores de refer�ncia
    BG_RollbackTransaction
    BG_BeginTransaction
        
    'Apaga o registo da an�lise
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    'SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    SQLQuery = "UPDATE sl_ana_s SET flg_invisivel = 1, " & _
                " user_act = '" & gCodUtilizador & "'," & _
                " dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & _
                "  WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    
    'Confirma a elimina��o
    BG_BeginTransaction
    
    'Temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
   
     'A Tabela Ficou Vazia?
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    
    'Era o �ltimo Registo?
    If rs.EOF Then
        rs.MovePrevious
    Else
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    End If
    
    LimpaCampos
    PreencheCampos

End Sub

Sub EventoActivate()
    
    SSTab1.Tab = lTabDadosAna
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub FuncaoModificar()

    Dim iRes As Integer
    Dim iRes2 As Integer
    gMsgTitulo = " Modificar"
    gMsgMsg = "Tem a certeza que deseja validar as altera��es efectuadas ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        EcCodigo.SetFocus
        BL_InicioProcessamento Me, "A modificar registo."
        
        If Trim(EcPeso.text) = "" Then
            EcPeso.text = "1"
        End If
        
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        If CkEtiqueta.value = 2 Then CkEtiqueta.value = 0
        If CkMarcAuto.value = 2 Then CkMarcAuto.value = 0
        If CkFormLeuc.value = 2 Then CkFormLeuc.value = 0
        iRes = ValidaCamposEc
        If iRes = True Then
            If CkSegundoRes.value = vbChecked Then
                iRes2 = ValidaCamposEc2Res
            Else
                iRes2 = True
            End If
            If iRes2 = True Then
                BD_Update
            End If
        End If
        
        BL_FimProcessamento Me
        
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim erro As String
    Dim iReg As Integer
    On Error GoTo Trata_Erro
    
    BG_BeginTransaction
    MarcaLocal = rs.Bookmark
    
    gSQLError = 0
    condicao = ChaveBD & " = " & EcCodSequencial.text
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    
    iReg = BG_ExecutaQuery_ADO(SQLQuery)
    
'    BG_LogFile_Erros "-- " & SQLQuery & "--"
        
    If iReg = 1 Then
        'Verifica se mudou a chave da an�lise
        If sValCodAnalise <> EcCodigo.text Then
            
            BL_InicioProcessamento Me, "A actualizar Intervalos de refer�ncia."
            'Apaga as Intervalos com o valor da an�lise antigo para os 2 tipos de resultados
            gSQLError = 0
            BG_ExecutaQuery_ADO "DELETE FROM sl_ana_ref WHERE cod_ana_s ='" & sValCodAnalise & "'"
            
            
            BL_FimProcessamento Me
            BL_InicioProcessamento Me, "A actualizar segundo resultado."
            'Faz o Update de 2�s Resultados
            gSQLError = 0
            BG_ExecutaQuery_ADO "UPDATE sl_segundo_res SET cod_ana_s='" & EcCodigo.text & "' WHERE cod_ana_s='" & sValCodAnalise & "'"
            
            
            BL_FimProcessamento Me
            BL_InicioProcessamento Me, "A actualizar membros das complexas."
            'Faz o Update das An�lises Complexas
            gSQLError = 0
            BG_ExecutaQuery_ADO "UPDATE sl_membro SET cod_membro='" & EcCodigo.text & "' WHERE cod_membro='" & sValCodAnalise & "' AND T_membro='A'"
            
            
            BL_FimProcessamento Me
            BL_InicioProcessamento Me, "A actualizar membros dos perfis."
            'Faz o Update dos Perfis
            gSQLError = 0
            BG_ExecutaQuery_ADO "UPDATE sl_ana_perfis SET cod_analise='" & EcCodigo.text & "' WHERE cod_analise='" & sValCodAnalise & "'"
            
        End If
        
        If BL_GravaLocaisAna(EcCodigo, Me) = False Then
            GoTo Trata_Erro
        End If
        If BL_GravaLocaisAnaExec(EcCodigo, Me) = False Then
            GoTo Trata_Erro
        End If
        If BL_GravaFolhasTrab(EcCodigo, Me) = False Then
            GoTo Trata_Erro
        End If
        If BL_GravaDadosQuestoes(Me, EcCodigo) = False Then
            GoTo Trata_Erro
        End If
        
        If GravaConclRAST = False Then
            GoTo Trata_Erro
        End If
        
        If GravaAnaRef(mediComboValorNull) = False Then
            GoTo Trata_Erro
        End If
        
        If GravaConcl = False Then
            GoTo Trata_Erro
        End If
        If gUsaInterferencia = mediSim Then
            If GravaInterferencias = False Then
                GoTo Trata_Erro
            End If
        End If
        
        If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
            If GravaFacturacao = False Then
                GoTo Trata_Erro
            End If
            GravaPrecarios
        End If
        If GravaSinonimo = False Then
            GoTo Trata_Erro
        End If
        If BL_GravaDadosInfAna(Me, EcCodigo) = False Then
            GoTo Trata_Erro
        End If
        If GravaSegundoResultado = False Then
            GoTo Trata_Erro
        End If
        
        
    End If
    BG_CommitTransaction
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark

    If sValCodAnalise = EcCodigo.text Then
        MarcaLocal = rs.Bookmark
        rs.Requery
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        FuncaoProcurar
    End If

    LimpaCampos
    PreencheCampos
     
    Exit Sub
Trata_Erro:
    BG_RollbackTransaction
    BG_LogFile_Erros "FormAnaSimples (BD_UPDATE):" & Err.Number & "-" & Err.Description & "(" & erro & ")", Me.Name, "BD_Update", True
    Resume Next
End Sub

Sub LimpaCampos()
    Dim i As Integer
    Me.SetFocus
    
    BG_LimpaCampo_Todos CamposEc
    BG_LimpaCampo_Todos CamposEc2Res
    BG_LimpaCampo_Todos CamposFactusEc
    
    RTBComent.text = ""
    LimpaLabels
    OptUnidade1.value = False
    OptUnidade2.value = False
    TotalEstrutSinonimos = 0
    ReDim EstrutSinonimos(0)
    TotalEstrutinterf = 0
    ReDim EstrutInterf(0)
    EcFormulaInterf = ""
    CbAccaoInterferencia.ListIndex = mediComboValorNull
    EcObsInterferencia = ""
    EcObsDesblInterferencia = ""
    EcInterfSeq = ""
    EcDescrClasse = ""
    EcDescrGrupo = ""
    EcDescrGrImpr = ""
    EcDescrProduto = ""
    EcDescrSubGrupo = ""
    EcDescrTuboP = ""
    EcDescrTuboS = ""
    FrameSinon.Visible = False
    FrCutOff.Visible = False
    For i = 0 To EcAnaLocais.ListCount - 1
        EcAnaLocais.Selected(i) = False
    Next
    For i = 0 To EcAnaLocais.ListCount - 1
        EcAnaLocaisExec.Selected(i) = False
    Next
    For i = 0 To EcListaFolhasTrab.ListCount - 1
        EcListaFolhasTrab.Selected(i) = False
    Next
    LimpaAnaRef
    LimpaConcl
    LimpaPrecos
    SSTab1.Tab = lTabDadosAna
    EcCodigo.SetFocus
    FrameSinon.Visible = False
    
    CkFacturar.value = vbGrayed
    CkContaMembros.value = vbGrayed
    CkFlgRegra.value = vbGrayed
    EcCodAnaRegra = ""
    CkFlgfacturarAna.value = vbGrayed
    CkNaoAvaliaResultados.value = vbGrayed
    CkCutOff.value = vbGrayed
    CkTransManual.value = vbGrayed
    CkResExterior(0).value = vbGrayed
    EcCodAnaFacturar = ""
    EcCodRubrica = ""
    EcCodRubrica_Validate False
    EcRubrSeqAna = ""
    EcAuxPreco = ""
    EcAuxPreco.Visible = False
    EcColunaPreco = ""
    EcLinhaPreco = ""
    EcLinhaEscRef = ""
    EcColEscRef = ""
    nres(1).TotalAnaRef = 0
    ReDim nres(1).EstrutAnaRef(0)
    nres(2).TotalAnaRef = 0
    ReDim nres(2).EstrutAnaRef(0)
    OptNRes(0).value = True
    OptNRes_Click 0
    EcQtd = ""
    CkResAnt.value = vbGrayed
    BL_LimpaDadosInfAna Me
    BL_LimpaDadosQuestoes Me
    FgInterferencias.rows = 2
    For i = 0 To FgInterferencias.Cols - 1
        FgInterferencias.TextMatrix(1, i) = ""
    Next
    LbLabel.caption = ""
    CkSegundoRes.value = vbGrayed
    EcTipoUnidadeAux3.ListIndex = mediComboValorNull
    EcTipoUnidadeAux4.ListIndex = mediComboValorNull
    'RGONCALVES 16.06.2013 ICIL-470
    CkEnvioSMS.value = vbGrayed
    CkAvaliaEpid.value = vbGrayed
    EcTextoSMS.text = ""
    CkCopiaRes.value = vbGrayed
    CkMarcador.value = vbGrayed
    CkResAnt2Res.value = vbGrayed
    CkObrigaInfClin.value = vbGrayed
    CkObrigaTerap.value = vbGrayed
    
    'BRUNODSANTOS 22.02.2017 - Glintt-HS-14510
    CkResExterior(1).value = vbGrayed
    CkResExterior(2).value = vbGrayed
    EcCodSinave.text = ""
    '
End Sub

Sub FuncaoInserir()
    Dim iRes2 As Integer
    Dim iRes As Integer
    Dim codigo As String
    gMsgTitulo = " Inserir"
    gMsgMsg = "Tem a certeza que deseja Inserir estes dados ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        EcCodigo.SetFocus
        If EcCodigo = "" Then
            gMsgTitulo = " C�digo"
            gMsgMsg = "Quer gerar um novo c�digo?   "
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            
            If gMsgResp = vbYes Then
                codigo = BL_GeraCodigoAnalise
                If codigo <> "" Then
                    gMsgTitulo = " C�digo"
                    gMsgMsg = "Foi gerado o c�digo: S" & codigo & ". Aceitar?"
                    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                    If gMsgResp = vbYes Then
                        EcCodigo = "S" & codigo
                        EcCodEstatistica = EcCodigo
                    Else
                        Exit Sub
                    End If
                End If
            End If
        Else
            If BL_VerificaCodigoExiste(EcCodigo) = True Then
                gMsgTitulo = " C�digo"
                gMsgMsg = "J� existe uma an�lise com esse c�digo. Quer continuar? "
                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                If gMsgResp <> vbYes Then
                    Exit Sub
                End If
            End If
        End If
        If Trim(EcPeso.text) = "" Then
            EcPeso.text = "1"
        End If
    
        BL_InicioProcessamento Me, "A inserir registo"
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        If CkEtiqueta.value = 2 Then CkEtiqueta.value = 0
        'If CkEtiqOrd.Value = 2 Then CkEtiqOrd.Value = 0
        If CkMarcAuto.value = 2 Then CkMarcAuto.value = 0
        If CkFormLeuc.value = 2 Then CkFormLeuc.value = 0
        iRes = ValidaCamposEc
        
        If iRes = True Then
            If CkSegundoRes.value = vbChecked Then
                iRes2 = ValidaCamposEc2Res
            Else
                iRes2 = True
            End If
            If iRes2 = True Then
                BD_Insert
                Dim cod As String
                cod = EcCodigo
                FuncaoLimpar
                EcCodigo = cod
                FuncaoProcurar
            End If
        End If
        
        BL_FimProcessamento Me
        
        If iRes = True Then
            ' PERGUNTA SE UTILIZADOR QUER INTRODUZIR NOVA ANALISE
            gMsgTitulo = " C�digo"
            gMsgMsg = "Quer inserir nova an�lise? "
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                EcCodigo = ""
                EcCodEstatistica = ""
                EcUtilizadorCriacao = ""
                EcDataCriacao = ""
                EcOrdem.text = ""
                EcCodSequencial = ""
                EcCodigo.locked = False
                Estado = 1
                BL_ToolbarEstadoN Estado
                
                CampoDeFocus.SetFocus
                If Not rs Is Nothing Then
                    rs.Close
                    Set rs = Nothing
                End If
            End If
        End If
        
    End If

End Sub


Function ValidaCamposEc() As Integer

    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function
Function ValidaCamposEc2Res() As Integer

    Dim iRes, i As Integer

    For i = 0 To NumCampos2Res - 1
        If TextoCamposObrigatorios2Res(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc2Res(i), TextoCamposObrigatorios2Res(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc2Res = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc2Res = True

End Function

Sub BD_Insert()
    
    Dim SQLQuery As String
    Dim erro As String
    
    On Error GoTo Trata_Erro
    BG_BeginTransaction
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcOrdem.text = BG_DaMAX(NomeTabela, "ordem") + 1
    
    EcCodSequencial = BG_DaMAX("SLV_ANALISES_APENAS", "seq_ana") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    If gSQLError = 0 Then
        If BL_GravaLocaisAna(EcCodigo, Me) = False Then
            GoTo Trata_Erro
        End If
        If BL_GravaLocaisAnaExec(EcCodigo, Me) = False Then
            GoTo Trata_Erro
        End If
        If BL_GravaDadosInfAna(Me, EcCodigo) = False Then
            GoTo Trata_Erro
        End If
        If BL_GravaDadosQuestoes(Me, EcCodigo) = False Then
            GoTo Trata_Erro
        End If
        If GravaConclRAST = False Then
            GoTo Trata_Erro
        End If
        If GravaAnaRef(mediComboValorNull) = False Then
            GoTo Trata_Erro
        End If
        If gUsaInterferencia = mediSim Then
            If GravaInterferencias = False Then
                GoTo Trata_Erro
            End If
        End If
        If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
            If GravaFacturacao = False Then
                GoTo Trata_Erro
            End If
            GravaPrecarios
        End If
        If GravaSinonimo = False Then
            GoTo Trata_Erro
        End If
        
        ValidaBotoes
        If BL_GravaLocaisAna(EcCodigo, Me) = False Then
            GoTo Trata_Erro
        End If
        If BL_GravaLocaisAnaExec(EcCodigo, Me) = False Then
            GoTo Trata_Erro
        End If
        If BL_GravaFolhasTrab(EcCodigo, Me) = False Then
            GoTo Trata_Erro
        End If
        If GravaSegundoResultado = False Then
            GoTo Trata_Erro
        End If
    End If
    BG_CommitTransaction
    Exit Sub
Trata_Erro:
    BG_RollbackTransaction
    BG_LogFile_Erros "FormAnaSimples (BD_INSERT):" & Err.Number & "-" & Err.Description & "(" & erro & ")", Me.Name, "BD_Insert", True
    Exit Sub
    Resume Next
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Set CampoDeFocus = EcCodigo
    Inicializacoes
    Inicializacoes2Res
    DefTipoCampos
    
    Max = -1
    ReDim FOPropertiesTemp(0)
    
    LimpaLabels
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    
    'Para os valores de refer�ncia
    BG_BeginTransaction
    
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()


    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        BL_Abre_Conexao_Secundaria gSGBD_SECUNDARIA
    End If
    
    Me.caption = " An�lises Simples"
    Me.left = 540
    Me.top = 100
    Me.Width = 13125
    Me.Height = 8730
    
    NomeTabela = "sl_ana_s"
    NomeTabelaFactus = "fa_rubr"
    Set CampoDeFocus = EcCodigo
    
    
    'CHVNG-Espinho
    'NumCampos = 74
    'BRUNODSANTOS 22.02.2017 - Glintt-HS-14510
    NumCampos = 77
    '
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "seq_ana_s"
    CamposBD(1) = "cod_ana_s"
    CamposBD(2) = "abr_ana_s"
    CamposBD(3) = "descr_ana_s"
    CamposBD(4) = "gr_ana"
    CamposBD(5) = "sgr_ana"
    CamposBD(6) = "t_result"
    CamposBD(7) = "unid_1"
    CamposBD(8) = "t_unid_1"
    CamposBD(9) = "unid_2"
    CamposBD(10) = "t_unid_2"
    CamposBD(11) = "unid_uso"
    CamposBD(12) = "fac_conv_unid"
    CamposBD(13) = "descr_formula"
    CamposBD(14) = "val_defeito"
    CamposBD(15) = "casas_dec"
    CamposBD(16) = "lim_inf"
    CamposBD(17) = "lim_sup"
    CamposBD(18) = "peso"
    CamposBD(19) = "flg_etiq_abr"
    CamposBD(20) = "user_cri"
    CamposBD(21) = "dt_cri"
    CamposBD(22) = "user_act"
    CamposBD(23) = "dt_act"
    CamposBD(24) = "coment"
    CamposBD(25) = "flg_concl_res"
    CamposBD(26) = "cod_produto"
    CamposBD(27) = "cod_tubo"
    CamposBD(28) = "qt_min"
    CamposBD(29) = "flg_marc_auto"
    CamposBD(30) = "flg_opcional"
    CamposBD(31) = "flg_formleuc"
    CamposBD(32) = "classe_ana"
    CamposBD(33) = "ordem"
    CamposBD(34) = "Linhas_Branco_Imp"
    CamposBD(35) = "cod_tubo_sec"
    CamposBD(36) = "flg_invisivel"
    CamposBD(37) = "cod_estatistica"
    CamposBD(38) = "flg_inibe_descr"
    CamposBD(39) = "prazo_conc"
    CamposBD(40) = "inibe_marcacao"
    CamposBD(41) = "ana_acreditada"
    CamposBD(42) = "ana_exterior"
    CamposBD(43) = "sem_historico"
    CamposBD(44) = "negrito"
    CamposBD(45) = "rast"
    CamposBD(46) = "descr_etiq"
    CamposBD(47) = "flg_facturar"
    CamposBD(48) = "flg_sexo"
    CamposBD(49) = "valor_grafico"
    CamposBD(50) = "val_min_grafico"
    CamposBD(51) = "val_max_grafico"
    CamposBD(52) = "descr_formula_f"
    CamposBD(53) = "COD_ESTAT_C"
    CamposBD(54) = "COD_ESTAT_P"
    CamposBD(55) = "descr_relatorio"
    CamposBD(56) = "flg_nao_avaliar"
    CamposBD(57) = "flg_cut_off"
    CamposBD(58) = "eta"
    CamposBD(59) = "ordem_ars"
    CamposBD(60) = "prazo_val"
    CamposBD(61) = "flg_transmanual"
    CamposBD(62) = "gr_impr"
    CamposBD(63) = "flg_resanter"
    CamposBD(64) = "flg_envio_ext"
    CamposBD(65) = "flg_segundo_Res"
    'RGONCALVES 16.06.2013 ICIL-470
    CamposBD(66) = "flg_envio_sms"
    CamposBD(67) = "texto_sms"
    CamposBD(68) = "flg_vigilancia_epid"
    CamposBD(69) = "flg_copia_resultados"
    CamposBD(70) = "flg_marcador"
    CamposBD(71) = "flg_res_ant_2"
    CamposBD(72) = "flg_obriga_infocli"
    CamposBD(73) = "flg_obriga_terap"
    'BRUNODSANTOS 22.02.2017 - Glintt-HS-14510
    CamposBD(74) = "flg_envio_num_sns"
    CamposBD(75) = "flg_envio_cod_conv"
    CamposBD(76) = "cod_ana_sinave"
    '
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcAbreviatura
    Set CamposEc(3) = EcDesignacao
    Set CamposEc(4) = EcCodGrupo
    Set CamposEc(5) = EcCodSubGrupo
    Set CamposEc(6) = EcTipoResultado
    Set CamposEc(7) = EcUnidade1
    Set CamposEc(8) = EcTipoUnidade
    Set CamposEc(9) = EcUnidade2
    Set CamposEc(10) = EcTipoUnidade2
    Set CamposEc(11) = EcUnidadeUso
    Set CamposEc(12) = EcFactorConv
    Set CamposEc(13) = EcFormula
    Set CamposEc(14) = EcValorDefeito
    Set CamposEc(15) = EcCasasDecimais
    Set CamposEc(16) = EcLimiteInf
    Set CamposEc(17) = EcLimiteSup
    Set CamposEc(18) = EcPeso
    Set CamposEc(19) = CkEtiqueta
    Set CamposEc(20) = EcUtilizadorCriacao
    Set CamposEc(21) = EcDataCriacao
    Set CamposEc(22) = EcUtilizadorAlteracao
    Set CamposEc(23) = EcDataAlteracao
    Set CamposEc(24) = RTBComent
    Set CamposEc(25) = CkConclRes
    Set CamposEc(26) = EcCodProd
    Set CamposEc(27) = EcCodTuboP
    Set CamposEc(28) = EcQuantMinima
    Set CamposEc(29) = CkMarcAuto
    Set CamposEc(30) = CkOpcional
    Set CamposEc(31) = CkFormLeuc
    Set CamposEc(32) = EcCodClasse
    Set CamposEc(33) = EcOrdem
    Set CamposEc(34) = EcLinhasBrancoImp
    Set CamposEc(35) = EcCodTuboS
    Set CamposEc(36) = CkInvisivel
    Set CamposEc(37) = EcCodEstatistica
    Set CamposEc(38) = CkInibeDescricao
    Set CamposEc(39) = EcPrazoConc
    Set CamposEc(40) = CkInibeMarcacao
    Set CamposEc(41) = CkAcreditacao
    Set CamposEc(42) = CkExterior
    Set CamposEc(43) = CkSemHistorico
    Set CamposEc(44) = CkNegrito
    Set CamposEc(45) = CkRAST
    Set CamposEc(46) = EcDescrEtiq
    Set CamposEc(47) = CkFacturar
    Set CamposEc(48) = CbSexo
    Set CamposEc(49) = EcValGrafico
    Set CamposEc(50) = EcValMinGrafico
    Set CamposEc(51) = EcValMaxGrafico
    Set CamposEc(52) = EcFormulaFem
    Set CamposEc(53) = EcCodEstatC
    Set CamposEc(54) = EcCodEstatP
    Set CamposEc(55) = EcDescrRelatorio
    Set CamposEc(56) = CkNaoAvaliaResultados
    Set CamposEc(57) = CkCutOff
    Set CamposEc(58) = EcETA
    Set CamposEc(59) = EcOrdemARS
    Set CamposEc(60) = EcPrazoVal
    Set CamposEc(61) = CkTransManual
    Set CamposEc(62) = EcCodGrImpr
    Set CamposEc(63) = CkResAnt
    Set CamposEc(64) = CkResExterior(0)
    Set CamposEc(65) = CkSegundoRes
    'RGONCALVES 16.06.2013 ICIL-470
    Set CamposEc(66) = CkEnvioSMS
    Set CamposEc(67) = EcTextoSMS
    Set CamposEc(68) = CkAvaliaEpid
    Set CamposEc(69) = CkCopiaRes
    Set CamposEc(70) = CkMarcador
    Set CamposEc(71) = CkResAnt2Res
    Set CamposEc(72) = CkObrigaInfClin
    Set CamposEc(73) = CkObrigaTerap
    '
    'BRUNODSANTOS 22.02.2017 - Glintt-HS-14510
    Set CamposEc(74) = CkResExterior(1)
    Set CamposEc(75) = CkResExterior(2)
    Set CamposEc(76) = EcCodSinave
    '
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo An�lise"
    TextoCamposObrigatorios(3) = "Designa��o da An�lise"
    TextoCamposObrigatorios(6) = "Tipo de Resultado"
    'TextoCamposObrigatorios(19) = "Imprimir abreviatura da an�lise na etiqueta"
    TextoCamposObrigatorios(37) = "C�digo Estat�stica"
    TextoCamposObrigatorios(47) = "An�lise a Facturar"
    TextoCamposObrigatorios(55) = "Descri��o Rela�rio"
    TextoCamposObrigatorios(65) = "Segundo Resultado"
    
    ChaveBD = "seq_ana_s"
    Set ChaveEc = EcCodSequencial
    
    NumCamposFACTUS = 6
    ReDim CamposFactusBD(0 To NumCamposFACTUS - 1)
    ReDim CamposFactusEc(0 To NumCamposFACTUS - 1)
    ReDim TextoCamposObrigatoriosFactus(0 To NumCamposFACTUS - 1)
    
    ' Campos da Base de Dados
    CamposFactusBD(0) = "cod_grupo"
    CamposFactusBD(1) = "cod_rubr"
    CamposFactusBD(2) = "descr_rubr"
    CamposFactusBD(3) = "user_cri"
    CamposFactusBD(4) = "dt_cri"
    CamposFactusBD(5) = "flg_exec_hosp"

    ' Campos do Ecr�
    Set CamposFactusEc(0) = CbRubrGrupo
    Set CamposFactusEc(1) = EcRubrCodigo
    Set CamposFactusEc(2) = EcRubrDescr
    Set CamposFactusEc(3) = EcRubrUserCri
    Set CamposFactusEc(4) = EcRubrDtCri
    Set CamposFactusEc(5) = EcRubrFlgExecHosp

    
    CamposBDparaListBox = Array("descr_produto")
    NumEspacos = Array(22)
    
    Call BL_FormataCodigo(EcCodigo)
    EcDesignacao.ToolTipText = cMsgWilcards
End Sub

Sub Inicializacoes2Res()

    NomeTabela2Res = "sl_segundo_Res"
    
    
    'CHVNG-Espinho
    NumCampos2Res = 15
    ReDim CamposBD2Res(0 To NumCampos2Res - 1)
    ReDim CamposEc2Res(0 To NumCampos2Res - 1)
    ReDim TextoCamposObrigatorios2Res(0 To NumCampos2Res - 1)
    
    ' Campos da Base de Dados
    CamposBD2Res(0) = "cod_ana_s"
    CamposBD2Res(1) = "t_result"
    CamposBD2Res(2) = "val_defeito"
    CamposBD2Res(3) = "lim_inf"
    CamposBD2Res(4) = "lim_sup"
    CamposBD2Res(5) = "casas_dec"
    CamposBD2Res(6) = "descr_formula"
    CamposBD2Res(7) = "descr_formula_f"
    CamposBD2Res(8) = "unid_1"
    CamposBD2Res(9) = "unid_2"
    CamposBD2Res(10) = "unid_uso"
    CamposBD2Res(11) = "fac_conv_unid"
    CamposBD2Res(12) = "flg_formleuc"
    CamposBD2Res(13) = "t_unid_1"
    CamposBD2Res(14) = "t_unid_2"
    
    ' Campos do Ecr�
    Set CamposEc2Res(0) = EcCodigo
    Set CamposEc2Res(1) = EcTipoResultado2Res
    Set CamposEc2Res(2) = EcValorDefeito2Res
    Set CamposEc2Res(3) = EcLimiteInf2Res
    Set CamposEc2Res(4) = EcLimiteSup2Res
    Set CamposEc2Res(5) = EcCasasDecimais2Res
    Set CamposEc2Res(6) = EcFormula2Res
    Set CamposEc2Res(7) = EcFormulaFem2Res
    Set CamposEc2Res(8) = EcUnidade1Res2
    Set CamposEc2Res(9) = EcUnidade2Res2
    Set CamposEc2Res(10) = EcUnidadeUso2Res
    Set CamposEc2Res(11) = EcFactorConv2Res
    Set CamposEc2Res(12) = CkFormLeuc2Res
    Set CamposEc2Res(13) = EcTipoUnidade3
    Set CamposEc2Res(14) = EcTipoUnidade4
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios2Res(0) = "C�digo An�lise"
    TextoCamposObrigatorios2Res(1) = "Tipo de Resultado (Segundo Resultado)"
    
    ChaveBD2Res = "cod_Ana_s"
    Set ChaveEc2Res = EcCodigo
    
End Sub


Sub DefTipoCampos()
    EcCodigo.Tag = adVarChar
    EcAbreviatura.Tag = adVarChar
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        BG_DefTipoCampoEc_ADO NomeTabelaFactus, "dt_cri", EcRubrDtCri, mediTipoData
    End If
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        If gSGBD = gOracle Then
            BG_DefTipoCampoEc_Todos_ADO NomeTabelaFactus, CamposFactusBD, CamposFactusEc, mediTipoDefeito
        End If
    End If
    
    RTBComent.MaxLength = 4000
    CkConclRes.value = vbGrayed
    CkOpcional.value = vbGrayed
    CkFormLeuc.value = vbGrayed
    CkInvisivel.value = vbGrayed
    CkInibeMarcacao.value = vbGrayed
    CkInibeDescricao.value = vbGrayed
    CkAcreditacao.value = vbGrayed
    CkExterior.value = vbGrayed
    CkSemHistorico.value = vbGrayed
    CkNegrito.value = vbGrayed
    CkRAST.value = vbGrayed
    CkFacturar.value = vbGrayed
    CkContaMembros.value = vbGrayed
    CkFlgRegra.value = vbGrayed
    EcCodAnaRegra = ""
    CkFlgfacturarAna.value = vbGrayed
    CkNaoAvaliaResultados.value = vbGrayed
    EcCodAnaFacturar = ""
    EcCodRubrica = ""
    EcCodRubrica_Validate False
    EcRubrSeqAna = ""
    CkCutOff.value = vbGrayed
    CkTransManual.value = vbGrayed
    CkResAnt.value = vbGrayed
    CkResExterior(0).value = vbGrayed
    EcDescrEtiq.MaxLength = 10
    CkCopiaRes.value = vbGrayed
    CkMarcador.value = vbGrayed
    CkResAnt2Res.value = vbGrayed
    CkObrigaInfClin.value = vbGrayed
    CkObrigaTerap.value = vbGrayed
    With FgPrecos
       .rows = 2
       .Cols = 11
       .FixedRows = 1
       .FixedCols = 0
       .AllowBigSelection = False
       .HighLight = flexHighlightAlways
       .FocusRect = flexFocusHeavy
       .MousePointer = flexDefault
       .FillStyle = flexFillSingle
       .SelectionMode = flexSelectionFree
       .AllowUserResizing = flexResizeColumns
       .GridLinesFixed = flexGridInset
       .GridColorFixed = flexTextInsetLight
       .MergeCells = flexMergeFree
       .PictureType = flexPictureColor
       .RowHeightMin = 280
       .row = 0
       
       .ColWidth(lColPrecosCodPrecario) = 500
       .Col = lColPrecosCodPrecario
       .text = "C�d"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosDescrPrecario) = 2800
       .Col = lColPrecosDescrPrecario
       .text = "Pre��rio"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosCodRubrEFR) = 1000
       .Col = lColPrecosCodRubrEFR
       .text = "C�d.Rubr."
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosDescrRubrEFR) = 2800
       .Col = lColPrecosDescrRubrEFR
       .text = "Descr. Rubrica"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosTxModeradora) = 800
       .Col = lColPrecosTxModeradora
       .text = "Tx.Modr."
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosTipoFact) = 800
       .Col = lColPrecosTipoFact
       .text = "Tipo Fact"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosNumC) = 700
       .Col = lColPrecosNumC
       .text = "N�Cs"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosNumK) = 700
       .Col = lColPrecosNumK
       .text = "N�Ks"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosValPagEFR) = 700
       .Col = lColPrecosValPagEFR
       .text = "Val.EFR"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosValPagDoe) = 700
       .Col = lColPrecosValPagDoe
       .text = "Val.Doe."
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(lColPrecosPercDoente) = 700
       .Col = lColPrecosPercDoente
       .text = "%Doe"
       .CellAlignment = flexAlignLeftCenter
       
       .Col = 0
    End With
    
    With FgInterferencias
        .rows = 2
        .Cols = 5
        .FixedRows = 1
        .FixedCols = 0
        .AllowBigSelection = False
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        .ColWidth(0) = 3000
        .Col = 0
        .text = "Interfer�ncia"
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(1) = 0
        .Col = 1
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(2) = 800
        .Col = 2
        .text = "Ac��o"
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(3) = 3000
        .Col = 3
        .text = "Observa��o"
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(4) = 3000
        .Col = 4
        .text = "Obs Desbloqueio"
        .CellAlignment = flexAlignCenterCenter
        .Col = 0
    End With
    
    With FgValRef
        .rows = 2
        .Cols = 3
        .FixedRows = 1
        .FixedCols = 0
        .AllowBigSelection = False
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        .ColWidth(lColValRefTipo) = 800
        .Col = lColValRefTipo
        .TextMatrix(0, lColValRefTipo) = "Tipo"
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(lColValRefDataInf) = 1500
        .Col = lColValRefDataInf
        .TextMatrix(0, lColValRefDataInf) = "Data Inicial"
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(lColValRefDataSup) = 1500
        .Col = lColValRefDataSup
        .TextMatrix(0, lColValRefDataSup) = "Data Superior"
        .CellAlignment = flexAlignCenterCenter
        .Col = 0
    End With
    
    With FgEscRef
        .rows = 2
        .Cols = 8
        .FixedRows = 1
        .FixedCols = 0
        .AllowBigSelection = False
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        
        .ColWidth(lColEscRefEscInf) = 800
        .Col = lColEscRefEscInf
        .TextMatrix(0, lColEscRefEscInf) = "N�Inf."
        .ColAlignment(lColEscRefEscInf) = flexAlignLeftCenter
        
        .ColWidth(lColEscRefTEscInf) = 1000
        .Col = lColEscRefTEscInf
        .TextMatrix(0, lColEscRefTEscInf) = "Tipo Inf."
        .ColAlignment(lColEscRefTEscInf) = flexAlignLeftCenter
        
        .ColWidth(lColEscRefEscSup) = 800
        .Col = lColEscRefEscSup
        .TextMatrix(0, lColEscRefEscSup) = "N�Sup."
        .ColAlignment(lColEscRefEscSup) = flexAlignLeftCenter
        
        .ColWidth(lColEscRefTEscSup) = 1000
        .Col = lColEscRefTEscSup
        .TextMatrix(0, lColEscRefTEscSup) = "Tipo Sup."
        .ColAlignment(lColEscRefTEscSup) = flexAlignLeftCenter
        
        .ColWidth(lColEscRefHRefMin) = 2000
        .Col = lColEscRefHRefMin
        .TextMatrix(0, lColEscRefHRefMin) = "Min. Masc"
        .ColAlignment(lColEscRefHRefMin) = flexAlignLeftCenter
        
        .ColWidth(lColEscRefHRefMax) = 2000
        .Col = lColEscRefHRefMax
        .TextMatrix(0, lColEscRefHRefMax) = "Max. Masc"
        .ColAlignment(lColEscRefHRefMax) = flexAlignLeftCenter
        
        .ColWidth(lColEscRefFRefMin) = 2000
        .Col = lColEscRefFRefMin
        .TextMatrix(0, lColEscRefFRefMin) = "Min. Fem."
        .ColAlignment(lColEscRefFRefMin) = flexAlignLeftCenter
        
        .ColWidth(lColEscRefFRefMax) = 2000
        .Col = lColEscRefFRefMax
        .TextMatrix(0, lColEscRefFRefMax) = "Max. Fem."
        .ColAlignment(lColEscRefFRefMax) = flexAlignLeftCenter
        .Col = 0
    End With
    
    With FgConc
        .rows = 2
        .Cols = 2
        .FixedRows = 1
        .FixedCols = 0
        .AllowBigSelection = False
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        
        .ColWidth(lColConcFormula) = 4000
        .Col = lColConcFormula
        .TextMatrix(0, lColConcFormula) = "F�rmula"
        .CellAlignment = flexAlignCenterCenter
        
        .ColWidth(lColConcDescr) = 7200
        .Col = lColConcDescr
        .TextMatrix(0, lColConcDescr) = "Conclus�o"
        .CellAlignment = flexAlignCenterCenter
    End With
    EcDtIni.Tag = adDate
    EcDtFim.Tag = adDate
    GereValRef False
    GereEscRef False
    
    FrameSinon.Visible = False
    FrCutOff.Visible = False
    FrameRubrica.Visible = False
    CbRubrGrupo.Tag = adNumeric
    EcRubrDtCri.Tag = adDate
    EcRubrUserCri.Tag = adVarChar
    If gTipoInstituicao = "HOSPITALAR" Then
        SSTab1.TabVisible(lTabFact) = False
    End If
    If gUsaInterferencia <> mediSim Then
        SSTab1.TabVisible(lTabinterf) = False
    End If
    CkSegundoRes.value = vbGrayed
    'RGONCALVES 16.06.2013 ICIL-470
    CkEnvioSMS.value = vbGrayed
    CkAvaliaEpid.value = vbGrayed
    EcTextoSMS.text = ""
    '
    'BRUNODSANTOS 22.02.2017 - Glintt-HS-14510
    CkResExterior(1).value = vbGrayed
    CkResExterior(2).value = vbGrayed
    '
End Sub

Sub LimpaLabels()
    
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""

End Sub


 Sub PreencheComboTipoUnidade()
    Dim i As Integer
    Select Case UCase(rs!t_unid_1)
        Case "I"
            For i = 0 To EcTipoUnidadeAux.ListCount - 1
                If EcTipoUnidadeAux.List(i) = "Internacional" Then
                    EcTipoUnidadeAux.ListIndex = i
                    Exit For
                End If
            Next
            EcTipoUnidadeAux.text = "Internacional"
        Case "C"
            For i = 0 To EcTipoUnidadeAux.ListCount - 1
                If EcTipoUnidadeAux.List(i) = "Convencional" Then
                    EcTipoUnidadeAux.ListIndex = i
                    Exit For
                End If
            Next
        End Select
    Select Case UCase(rs!t_unid_2)
        Case "I"
            EcTipoUnidadeAux2.text = "Internacional"
        Case "C"
            EcTipoUnidadeAux2.text = "Convencional"
    End Select
 
 End Sub

Sub PreencheComboTipoUnidadeDef()
    
    EcTipoUnidadeAux.Clear
    EcTipoUnidadeAux.AddItem ("Internacional")
    EcTipoUnidadeAux.AddItem ("Convencional")
    EcTipoUnidadeAux2.Clear
    EcTipoUnidadeAux2.AddItem ("Internacional")
    EcTipoUnidadeAux2.AddItem ("Convencional")
    EcTipoUnidadeAux3.Clear
    EcTipoUnidadeAux3.AddItem ("Internacional")
    EcTipoUnidadeAux3.AddItem ("Convencional")
    EcTipoUnidadeAux4.Clear
    EcTipoUnidadeAux4.AddItem ("Internacional")
    EcTipoUnidadeAux4.AddItem ("Convencional")

End Sub

Sub PreencheCampos()
    Dim aux As String
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        PreencheComboTipoUnidade
        Call EcCodTubop_LostFocus
        Call EcCodTuboS_LostFocus
        UnidadeUso
        EcCodGrupo_Validate False
        EcCodgrImpr_Validate False
        EcCodclasse_Validate False
        EcCodProd_Validate False
        EcCodsubGrupo_Validate False
        EcCodtubop_Validate False
        EcCodtuboS_Validate False

        ValidaBotoes
        
        'Guarda o valor da Chave
        sValCodAnalise = Trim(UCase(EcCodigo.text))
        
        If CkInvisivel.value = 1 Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
        If gUsaInterferencia = mediSim Then
            PreencheInterferencias
        End If
        PreencheAnaRef
        OptNRes_Click 0
        PreencheConcl
        If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
            PreencheFacturacao
            PreenchePrecarios
        End If
        BL_CarregaLocaisAna EcCodigo, Me
        BL_CarregaLocaisAnaExec EcCodigo, Me
        BL_CarregaFolhasTrab EcCodigo, Me
        BL_PreencheDadosInfAna Me, EcCodigo
        BL_PreencheDadosQuestoes Me, EcCodigo
        
        aux = EcCodigo
        BG_LimpaCampo_Todos CamposEc2Res
        EcCodigo = aux
        If CkSegundoRes.value = vbChecked Then
            FuncaoProcurar2Res
        End If
    End If
    
End Sub

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    'Para os valores de refer�ncia
    BG_RollbackTransaction
    BG_BeginTransaction
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_ana_s ASC"
    Else
    End If
    
    CriterioTabela = BL_Upper_Campo(CriterioTabela, _
                                    "descr_ana_s", _
                                    True)
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioTabela
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        LimpaCampos
        PreencheCampos
                
        BL_ToolbarEstadoN Estado
        'BL_Toolbar_BotaoEstado "Remover", "InActivo"
        Me.EcCodigo.locked = True
        DoEvents
        
        BL_FimProcessamento Me
    End If
        
End Sub

Sub FuncaoProcurar2Res()
    Dim i As Integer
    CriterioTabela2Res = BG_ConstroiCriterio_SELECT_ADO(NomeTabela2Res, CamposBD2Res, CamposEc2Res, False)
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    Set Rs2Res = New ADODB.recordset
    Rs2Res.CursorType = adOpenStatic
    Rs2Res.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioTabela
    Rs2Res.Open CriterioTabela2Res, gConexao
    
    If Rs2Res.RecordCount <= 0 Then

    Else
        BG_PreencheCampoEc_Todos_ADO Rs2Res, CamposBD2Res, CamposEc2Res
        
        ' UNIDADE SELECCIONADA
        OptUnidade1Res2.value = False
        OptUnidade2Res2.value = False
        Select Case Rs2Res!unid_uso
            Case 1
                OptUnidade1Res2.value = True
            Case 2
                OptUnidade2Res2.value = True
        End Select
                
        Select Case UCase(Rs2Res!t_unid_1)
            Case "I"
                For i = 0 To EcTipoUnidadeAux3.ListCount - 1
                    If EcTipoUnidadeAux3.List(i) = "Internacional" Then
                        EcTipoUnidadeAux3.ListIndex = i
                        Exit For
                    End If
                Next
            Case "C"
                For i = 0 To EcTipoUnidadeAux3.ListCount - 1
                    If EcTipoUnidadeAux3.List(i) = "Convencional" Then
                        EcTipoUnidadeAux3.ListIndex = i
                        Exit For
                    End If
                Next
            End Select
        Select Case UCase(Rs2Res!t_unid_2)
            Case "I"
                For i = 0 To EcTipoUnidadeAux4.ListCount - 1
                    If EcTipoUnidadeAux4.List(i) = "Internacional" Then
                        EcTipoUnidadeAux4.ListIndex = i
                        Exit For
                    End If
                Next
            Case "C"
                For i = 0 To EcTipoUnidadeAux4.ListCount - 1
                    If EcTipoUnidadeAux4.List(i) = "Convencional" Then
                        EcTipoUnidadeAux4.ListIndex = i
                        Exit For
                    End If
                Next
        End Select
                
    End If
    Rs2Res.Close
    Set Rs2Res = Nothing
End Sub

Sub FuncaoSeguinte()

    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        'Para os valores de refer�ncia
        BG_RollbackTransaction
        BG_BeginTransaction
        
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
    
End Sub

Sub FuncaoAnterior()

    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        'Para os valores de refer�ncia
        BG_RollbackTransaction
        BG_BeginTransaction
        
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
    
End Sub

Sub FuncaoLimpar()
            
    'Para os valores de refer�ncia
    BG_RollbackTransaction
    BG_BeginTransaction
            
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
    
    Me.EcCodigo.locked = False
    
End Sub

Sub FuncaoEstadoAnterior()

    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    
    Dim i As Integer
    
    
    
    CkEtiqueta.value = 2
'    CkEtiqOrd.Value = 2
    CkMarcAuto.value = 2
    
    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", CbSexo, mediAscComboCodigo
    BG_PreencheComboBD_ADO "SL_TBF_T_DATA", "COD_T_DATA", "DESCR_T_DATA", CbEscRefAux, mediAscComboCodigo
    BG_PreencheComboBD_ADO "sl_tbf_SINAIS", "COD_SINAL", "DESCR_SINAL", CbNMin, mediAscComboCodigo
    BG_PreencheComboBD_ADO "sl_tbf_SINAIS", "COD_SINAL", "DESCR_SINAL", CbNMax, mediAscComboCodigo
    BG_PreencheComboBD_ADO "sl_tbf_SINAIS", "COD_SINAL", "DESCR_SINAL", CbPMin, mediAscComboCodigo
    BG_PreencheComboBD_ADO "sl_tbf_SINAIS", "COD_SINAL", "DESCR_SINAL", CbPMax, mediAscComboCodigo
    BG_PreencheComboBD_ADO "sl_tbf_SINAIS", "COD_SINAL", "DESCR_SINAL", CbDMin, mediAscComboCodigo
    BG_PreencheComboBD_ADO "sl_tbf_SINAIS", "COD_SINAL", "DESCR_SINAL", CbDMax, mediAscComboCodigo
    

    ' Locais
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcAnaLocais, mediAscComboCodigo
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcAnaLocaisExec, mediAscComboCodigo
    
    BL_IniFolhasTrab Me
    BL_Informacao Me
    BL_Questoes Me
    
    BG_PreencheComboBD_ADO "sl_accoes_interferencias", "cod_accao", "descr_accao", CbAccaoInterferencia
    BG_PreencheComboBD_ADO "sl_tbf_t_res", "cod_t_res", "descr_t_res", EcTipoResultado
    BG_PreencheComboBD_ADO "sl_tbf_t_res", "cod_t_res", "descr_t_res", EcTipoResultado2Res
    If gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        BG_PreencheComboBDSecundaria_ADO "FA_GRUP_RUBR", "cod_grupo", "descr_grupo", CbRubrGrupo
    End If
    PreencheComboTipoUnidadeDef
    EcRubrFlgExecHosp = "S"
    
End Sub

Sub UnidadeUso()

    OptUnidade1.value = False
    OptUnidade2.value = False
    Select Case rs!unid_uso
        Case 1
            OptUnidade1.value = True
        Case 2
            OptUnidade2.value = True
    End Select
    
End Sub

Private Sub BtAdiciona_Click()
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        
        nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).totalEscRef = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).totalEscRef + 1
        ReDim Preserve nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).totalEscRef)
        FgEscRef.AddItem ""
        EcLinhaEscRef = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).totalEscRef
        FgEscRef.row = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).totalEscRef
        FgEscRef_Click
        FgEscRef.SetFocus
    End If

End Sub

Private Sub BtAdicionarRubrica_Click()
    FrameRubrica.Visible = True
    FrameRubrica.top = 360
    FrameRubrica.left = 50
End Sub

Private Sub BtAnular_Click()
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If EcLinhaAnaRef <= nres(nResActivo).TotalAnaRef Then
            If Trim(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).dt_valid_sup) = "" Then
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).t_ref = ""
                FgValRef.TextMatrix(EcLinhaAnaRef, lColValRefTipo) = ""
                FgValRef.row = EcLinhaAnaRef
                FgValRef_Click
                FgValRef.SetFocus
            End If
        End If
    End If
End Sub

Private Sub BtCopiaDadosAna_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1) As Variant
    Dim PesqRapida As Boolean
    
    ' SE J� ESTIVER PREENCHIDO N�O FAZ NADA.
    If EcCodSequencial <> "" Then
        BG_Mensagem mediMsgBox, "S� � permitido copiar nas novas an�lises.", vbExclamation, "ATEN��O"
        Exit Sub
    End If
    

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3900
    Headers(2) = "Descri��o"
    
    If gLAB = "ENZ" Then
        CamposEcran(3) = "descr_material"
    Else
        CamposEcran(3) = "descr_produto"
    End If
    Tamanhos(3) = 2000
    Headers(3) = "Produto"
    
    CamposRetorno.InicializaResultados 1
    
    If gLAB = "ENZ" Then
        CFrom = "sl_ana_s, sl_materiais"
        CWhere = "sl_ana_s.cod_produto = sl_materiais.cod_material(+) and (flg_invisivel is null or flg_invisivel <> 1)"
    Else
        If gSGBD = gOracle Then
            CFrom = "sl_ana_s, sl_produto"
            CWhere = "sl_ana_s.cod_produto = sl_produto.cod_produto(+) and (flg_invisivel is null or flg_invisivel <> 1)"
        ElseIf gSGBD = gSqlServer Then
            CFrom = "sl_ana_s LEFT OUTER JOIN sl_produto ON sl_ana_s.cod_produto = sl_produto.cod_produto "
            CWhere = " (flg_invisivel is null or flg_invisivel <> 1)"
        End If
    End If
    CampoPesquisa = "descr_ana_s"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, _
                                                                   ChavesPesq, _
                                                                   CamposEcran, _
                                                                   CamposRetorno, _
                                                                   Tamanhos, _
                                                                   Headers, _
                                                                   CWhere, _
                                                                   CFrom, _
                                                                   "", _
                                                                   CampoPesquisa, _
                                                                   " ORDER BY descr_ana_s ", _
                                                                   " Pesquisa R�pida de An�lises Simples")
                                                                   

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            Call LimpaCampos
            EcCodigo.text = Resultados(1)
            EcCodigo.SetFocus
            Call FuncaoProcurar
            EcCodSequencial = ""
            EcCodigo = ""
            EcCodigo.locked = False
            EcDesignacao = ""
            EcDescrRelatorio = ""
            EcCodEstatC = ""
            EcCodEstatistica = ""
            EcCodEstatP = ""
            EcUtilizadorCriacao = ""
            EcUtilizadorAlteracao = ""
            EcDataAlteracao = ""
            EcDataCriacao = ""
            EcAbreviatura = ""
            LaDataAlteracao = ""
            LaDataCriacao = ""
            LbHrAct = ""
            LbHrCri = ""
            LaUtilAlteracao = ""
            LaUtilCriacao = ""
            
            ApagaDadosAna
            Estado = 1
            BL_ToolbarEstadoN Estado
            
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises Simples", vbExclamation, "ATEN��O"
        EcCodigo.SetFocus
    End If
    
End Sub

Private Sub BtCopiaDescr_Click()
    EcDescrRelatorio = EcDesignacao
End Sub


Private Sub BtCutOff_Click()
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If EcLinhaAnaRef <= nres(nResActivo).TotalAnaRef Then
            If Trim(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).t_ref) = "" Then
                FrCutOff.Visible = True
                FrCutOff.top = 2880
                FrCutOff.left = 120
                
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).t_ref = "C"
                FgValRef.TextMatrix(EcLinhaAnaRef, lColValRefTipo) = "C"
                FgValRef.row = EcLinhaAnaRef
                FgValRef_Click
            End If
        End If
    End If
End Sub

Private Sub BtDuplicarAnaRef_Click()
    Dim i As Long
    Dim j As Integer
    Dim rsZona As New ADODB.recordset
    Dim sSql As String
    Dim seqZonaRef As Long
    
    BG_BeginTransaction
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If EcLinhaAnaRef <= nres(nResActivo).TotalAnaRef Then
            gMsgTitulo = " Duplica��o de Registo"
            gMsgMsg = "Tem a certeza que deseja duplicar registo? Todos intervalos abertos ser�o fechados."
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                For i = 1 To nres(nResActivo).TotalAnaRef
                    If nres(nResActivo).EstrutAnaRef(i).dt_valid_sup = "" Then
                        nres(nResActivo).EstrutAnaRef(i).dt_valid_sup = Bg_DaData_ADO
                        FgValRef.TextMatrix(i, lColValRefDataSup) = nres(nResActivo).EstrutAnaRef(i).dt_valid_sup
                        GravaAnaRef i
                    End If
                Next
                nres(nResActivo).TotalAnaRef = nres(nResActivo).TotalAnaRef + 1
                ReDim Preserve nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef)
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).cod_genero = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef.text).cod_genero
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).cod_metodo = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef.text).cod_metodo
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).cod_reag = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef.text).cod_reag
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).cutoff = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef.text).cutoff
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).cutoff.cod_ana_ref = mediComboValorNull
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).dt_valid_inf = Bg_DaData_ADO
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).dt_valid_sup = ""
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).EscRef = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef.text).EscRef
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).N_Res = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef.text).N_Res
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).obs_ref = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef.text).obs_ref
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).t_ref = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef.text).t_ref
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).totalEscRef = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef.text).totalEscRef
                For j = 1 To nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).totalEscRef
                    nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).EscRef(j).seq_esc_ref = ""
                Next j
                nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).valoresRef = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef.text).valoresRef
                If GravaAnaRef(nres(nResActivo).TotalAnaRef) = True Then
                    FgValRef.TextMatrix(nres(nResActivo).TotalAnaRef, lColValRefTipo) = nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).t_ref
                    FgValRef.TextMatrix(nres(nResActivo).TotalAnaRef, lColValRefDataInf) = nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).dt_valid_inf
                    FgValRef.TextMatrix(nres(nResActivo).TotalAnaRef, lColValRefDataSup) = nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).dt_valid_sup
                    FgValRef.AddItem ""
                    
                    'ZONAS
                    sSql = "SELECT * FROM sl_zona_ref WHERE cod_ana_ref = " & nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef.text).seq_ana_ref
                    rsZona.CursorType = adOpenStatic
                    rsZona.CursorLocation = adUseClient
                    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                    rsZona.Open sSql, gConexao
                    If rsZona.RecordCount >= 1 Then
                        While Not rsZona.EOF
                            seqZonaRef = BG_DaMAX("SL_ZONA_REF", "seq_zona_ref") + 1
                            sSql = "INSERT INTO sl_zona_ref(seq_zona_ref, cod_ana_ref, n_Res, sexo, idade_inf, t_idade_inf, idade_inf_dias, idade_sup, t_idade_sup, "
                            sSql = sSql & " idade_sup_dias, cod_diag, cod_or_zonas,ref_min, ref_max, txt_livre, s_gestacao, flg_inibe_val) VALUES ("
                            sSql = sSql & seqZonaRef & ","
                            sSql = sSql & nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).seq_ana_ref & ","
                            sSql = sSql & nResActivo & ","
                            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsZona!Sexo, "")) & ","
                            sSql = sSql & BL_HandleNull(rsZona!idade_inf, "NULL") & ","
                            sSql = sSql & BL_HandleNull(rsZona!t_idade_inf, "NULL") & ","
                            sSql = sSql & BL_HandleNull(rsZona!idade_inf_dias, "NULL") & ","
                            sSql = sSql & BL_HandleNull(rsZona!idade_sup, "NULL") & ","
                            sSql = sSql & BL_HandleNull(rsZona!t_idade_sup, "NULL") & ","
                            sSql = sSql & BL_HandleNull(rsZona!idade_sup_dias, "NULL") & ","
                            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsZona!cod_diag, "")) & ","
                            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsZona!cod_or_zonas, "")) & ","
                            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsZona!ref_min, "")) & ","
                            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsZona!ref_max, "")) & ","
                            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsZona!txt_livre, "")) & ","
                            sSql = sSql & BL_HandleNull(rsZona!s_gestacao, "NULL") & ","
                            sSql = sSql & BL_HandleNull(rsZona!flg_inibe_val, "NULL") & ")"
                            BG_ExecutaQuery_ADO sSql
                            rsZona.MoveNext
                        Wend
                    End If
                    rsZona.Close
                    Set rsZona = Nothing
                End If
            End If
            
        End If
    End If
    BG_CommitTransaction
End Sub

Private Sub BtEscaloes_Click()
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If EcLinhaAnaRef <= nres(nResActivo).TotalAnaRef Then
            If Trim(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).t_ref) = "" Then
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).t_ref = "E"
                FgValRef.TextMatrix(EcLinhaAnaRef, lColValRefTipo) = "E"
                FgValRef.row = EcLinhaAnaRef
                FgValRef_Click
                FgEscRef.row = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).totalEscRef + 1
                FgEscRef_Click
                FgEscRef.SetFocus
            ElseIf Trim(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).t_ref) = "E" Then
            End If
        End If
    End If

End Sub

Private Sub BtFechar_Click()
    Dim i As Long
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If EcLinhaAnaRef <= nres(nResActivo).TotalAnaRef Then
            If nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).dt_valid_sup = "" And nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).dt_valid_inf <> "" Then
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).dt_valid_sup = Bg_DaData_ADO
                FgValRef.TextMatrix(EcLinhaAnaRef, lColValRefDataSup) = Bg_DaData_ADO
            End If
        End If
    End If
End Sub

Private Sub BtGravarAnaFact_Click()
    GravaFacturacao
End Sub

Private Sub BtGravaRubr_Click()
    Dim SQLQuery As String
    Dim RsFact As New ADODB.recordset
    On Error GoTo TrataErro
    
    SQLQuery = "SELECT * from fa_rubr WHERE cod_rubr = " & EcRubrCodigo
    RsFact.CursorType = adOpenStatic
    RsFact.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLQuery
    RsFact.Open SQLQuery, gConexaoSecundaria
    If RsFact.RecordCount >= 1 Then
        BG_Mensagem mediMsgBox, "R�brica indicada j� existe.", vbExclamation, "ATEN��O"
        Exit Sub
    End If
    On Error GoTo TrataErro
    EcRubrDtCri = Bg_DaData_ADO
    EcRubrUserCri = gCodUtilizador
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabelaFactus, CamposFactusBD, CamposFactusEc)
    gConexaoSecundaria.Execute SQLQuery
    BG_Mensagem mediMsgBox, "R�brica Inserida.", vbExclamation, "ATEN��O"

    FrameRubrica.Visible = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Gravar Nova Rubrica ", Me.Name, "BtGravaRubr_Click"
    BG_RollbackTransaction
    Exit Sub
    Resume Next
End Sub

Private Sub BtImportarConcRAST_Click()
    Dim sSql As String
    Dim rsConc As New ADODB.recordset
    On Error GoTo TrataErro
    TotalConcl = 0
    ReDim EstrutConcl(0)
    FgConc.rows = 2
    
    sSql = "SELECT * FROM SL_CONC_RAST "
    rsConc.CursorType = adOpenStatic
    rsConc.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsConc.Open sSql, gConexao
    If rsConc.RecordCount > 0 Then
        While Not rsConc.EOF
            TotalConcl = TotalConcl + 1
            ReDim Preserve EstrutConcl(TotalConcl)
            EstrutConcl(TotalConcl).formula = Replace(BL_HandleNull(rsConc!formula, ""), "$", "$" & EcCodigo)
            EstrutConcl(TotalConcl).conclusao = BL_HandleNull(rsConc!conclusao, "")
            
            FgConc.TextMatrix(TotalConcl, lColConcFormula) = EstrutConcl(TotalConcl).formula
            FgConc.TextMatrix(TotalConcl, lColConcDescr) = EstrutConcl(TotalConcl).conclusao
            FgConc.AddItem ""
            
            rsConc.MoveNext
        Wend
        FgConc.row = 1
    End If
    rsConc.Close
    Set rsConc = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Importar Conclus�es Rast", Me.Name, "BtImportarConcRAST_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtNovo_Click()
    Dim i As Long
    If nResActivo > mediComboValorNull Then
        For i = 1 To nres(nResActivo).TotalAnaRef
            If nres(nResActivo).EstrutAnaRef(i).dt_valid_sup = "" And nres(nResActivo).EstrutAnaRef(i).dt_valid_inf <> "" And nres(nResActivo).EstrutAnaRef(i).cod_genero = EcCodGenero Then
                BG_Mensagem mediMsgBox, "N�o � poss�vel abrir novo intervalo.", vbExclamation, "ATEN��O"
                Exit Sub
            End If
        Next
        nres(nResActivo).TotalAnaRef = nres(nResActivo).TotalAnaRef + 1
        ReDim Preserve nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef)
        nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).dt_valid_inf = Bg_DaData_ADO
        nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).N_Res = nResActivo
        If CkCutOff.value = vbChecked And nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).t_ref = "" Then
            nres(nResActivo).EstrutAnaRef(nres(nResActivo).TotalAnaRef).t_ref = "C"
            
        End If
        FgValRef.row = nres(nResActivo).TotalAnaRef
        FgValRef.TextMatrix(nres(nResActivo).TotalAnaRef, lColValRefDataInf) = Bg_DaData_ADO
        FgValRef_Click
    End If
End Sub

Private Sub BtPesqRapAna_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1) As Variant
    Dim PesqRapida As Boolean
    Dim campoPesquisaCod As String
    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3900
    Headers(2) = "Descri��o"
    
    If gLAB = "ENZ" Then
        CamposEcran(3) = "descr_material"
    Else
        CamposEcran(3) = "descr_produto"
    End If
    Tamanhos(3) = 2000
    Headers(3) = "Produto"
    
    CamposRetorno.InicializaResultados 1
    
    If gLAB = "ENZ" Then
        CFrom = "sl_ana_s, sl_materiais"
        CWhere = "sl_ana_s.cod_produto = sl_materiais.cod_material(+) and (flg_invisivel is null or flg_invisivel <> 1)"
    Else
        If gSGBD = gOracle Then
            CFrom = "sl_ana_s, sl_produto"
            CWhere = "sl_ana_s.cod_produto = sl_produto.cod_produto(+) and (flg_invisivel is null or flg_invisivel <> 1)"
        ElseIf gSGBD = gSqlServer Then
            CFrom = "sl_ana_s LEFT OUTER JOIN sl_produto ON sl_ana_s.cod_produto = sl_produto.cod_produto "
            CWhere = " (flg_invisivel is null or flg_invisivel <> 1)"
        End If
    End If
    CampoPesquisa = "descr_ana_s"
    campoPesquisaCod = "cod_ana_s"
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, _
                                                                   ChavesPesq, _
                                                                   CamposEcran, _
                                                                   CamposRetorno, _
                                                                   Tamanhos, _
                                                                   Headers, _
                                                                   CWhere, _
                                                                   CFrom, _
                                                                   "", _
                                                                   CampoPesquisa, _
                                                                   " ORDER BY descr_ana_s ", _
                                                                   " Pesquisa R�pida de An�lises Simples", campoPesquisaCod)
                                                                   

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            Call LimpaCampos
            EcCodigo.text = Resultados(1)
            EcCodigo.SetFocus
            Call FuncaoProcurar
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises Simples", vbExclamation, "ATEN��O"
        EcCodigo.SetFocus
    End If

End Sub

Private Sub BtPesqRapAnaS_Click()

End Sub

Private Sub BtPesquisaRubrica_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_rubr"
    CamposEcran(1) = "cod_rubr"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_rubr"
    CamposEcran(2) = "descr_rubr"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "fa_rubr"
    CampoPesquisa1 = "descr_rubr"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexaoSecundaria, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " descr_rubr", " Pesquisar R�bricas")
    
    mensagem = "N�o foi encontrada nenhuma R�brica"
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodRubrica.text = Resultados(1)
            EcDescrRubrica.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtRemove_Click()
    Dim i As Long
    If EcLinhaAnaRef <> "" And EcLinhaEscRef <> "" And nResActivo > mediComboValorNull Then
        If EcLinhaEscRef <= nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).totalEscRef Then
            gMsgTitulo = " Acrescentar"
            gMsgMsg = "Deseja apagar escal�o?   "
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                For i = FgEscRef.row To nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).totalEscRef - 1
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i).esc_inf = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i + 1).esc_inf
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i).esc_inf_dias = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i + 1).esc_inf_dias
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i).esc_sup = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i + 1).esc_sup
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i).esc_sup_dias = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i + 1).esc_sup_dias
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i).f_ref_max = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i + 1).f_ref_max
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i).f_ref_min = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i + 1).f_ref_min
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i).h_ref_max = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i + 1).h_ref_max
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i).h_ref_min = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i + 1).h_ref_min
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i).seq_esc_ref = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i + 1).seq_esc_ref
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i).t_esc_inf = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i + 1).t_esc_inf
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i).t_esc_sup = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(i + 1).t_esc_sup
                Next
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).totalEscRef = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).totalEscRef - 1
                ReDim Preserve nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).totalEscRef)
                FgEscRef.RemoveItem EcLinhaEscRef
            End If
        End If
    End If
End Sub

Private Sub BtSairRubr_Click()
    FrameRubrica.Visible = False
End Sub

Private Sub BtValRef_Click()
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If EcLinhaAnaRef <= nres(nResActivo).TotalAnaRef Then
            If Trim(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).t_ref) = "" Then
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).t_ref = "V"
                FgValRef.TextMatrix(EcLinhaAnaRef, lColValRefTipo) = "V"
                FgValRef.row = EcLinhaAnaRef
                FgValRef_Click
                EcMinMasc.SetFocus
            ElseIf Trim(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).t_ref) = "V" Then
            End If
        End If
    End If
End Sub

Private Sub BtVerificaFormula_Click(Index As Integer)
    
    Dim s As String
    If Index = 0 Then
        If EcFormula.text <> "" Then
            s = BL_FormulaResolve(EcFormula.text, True)
            If Trim(s) = "" Then
                BG_Mensagem mediMsgBox, "F�rmula mal definida", vbExclamation + vbOKOnly, "F�rmula."
                EcFormula.SetFocus
            End If
        End If
    ElseIf Index = 1 Then
        If EcFormulaFem.text <> "" Then
            s = BL_FormulaResolve(EcFormulaFem.text, True)
            If Trim(s) = "" Then
                BG_Mensagem mediMsgBox, "F�rmula mal definida", vbExclamation + vbOKOnly, "F�rmula."
                EcFormulaFem.SetFocus
            End If
        End If
    ElseIf Index = 2 Then
        If EcFormula2Res.text <> "" Then
            s = BL_FormulaResolve(EcFormula2Res.text, True)
            If Trim(s) = "" Then
                BG_Mensagem mediMsgBox, "F�rmula mal definida", vbExclamation + vbOKOnly, "F�rmula."
                EcFormula2Res.SetFocus
            End If
        End If
    ElseIf Index = 3 Then
        If EcFormulaFem2Res.text <> "" Then
            s = BL_FormulaResolve(EcFormulaFem2Res.text, True)
            If Trim(s) = "" Then
                BG_Mensagem mediMsgBox, "F�rmula mal definida", vbExclamation + vbOKOnly, "F�rmula."
                EcFormulaFem2Res.SetFocus
            End If
        End If
    
    End If
    
End Sub




Private Sub BtZonas_Click()
    Dim iRes As Integer
    Dim iRes2 As Integer
    Dim seqanaref As String
    seqanaref = EcSeqAnaRef
    iRes = BG_Mensagem(mediMsgBox, "Entrar nas Zonas de Refer�ncia da an�lise implica gravar as altera��es � mesma." & Chr(13) & "Confirma a grava��o ?", vbQuestion + vbYesNo, "Zonas de Refer�ncia")
    
    If iRes = 6 Then
        BL_InicioProcessamento Me, "A registar altera��es."
        iRes = ValidaCamposEc
        If iRes = True Then
            If CkSegundoRes.value = vbChecked Then
                iRes2 = ValidaCamposEc2Res
            Else
                iRes2 = True
            End If
            If iRes2 = True Then
                If Estado = 2 Then
                    BD_Update
                Else
                    BD_Insert
                End If
                EcSeqAnaRef = seqanaref
                If gEstadoTransacao = True Then
                    BG_CommitTransaction
                    gEstadoTransacao = False
                End If
    
                If EcSeqAnaRef = "" Then Exit Sub
                Max = UBound(gFieldObjectProperties)
        
                ' Guardar as propriedades dos campos do form
                For Ind = 0 To Max
                    ReDim Preserve FOPropertiesTemp(Ind)
                    FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
                    FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
                    FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
                    FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
                    FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
                    FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
                Next Ind
                            
                FormCodAna.Enabled = False
                Set gFormActivo = FormZonasRef
                FormZonasRef.Show
                
            End If
        End If
        BL_FimProcessamento Me
    End If

End Sub


Private Sub CbAccaoInterferencia_KeyDown(KeyCode As Integer, Shift As Integer)
    If CbAccaoInterferencia.ListIndex <> mediComboValorNull And KeyCode = vbKeyReturn Then
        EcObsInterferencia.SetFocus
    End If
End Sub

Private Sub CbEscRefAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        CbEscRefAux_validate False
    End If
End Sub

Private Sub CbEscRefAux_validate(Cancel As Boolean)
    If nResActivo > mediComboValorNull Then
        If FgValRef.row <= nres(nResActivo).TotalAnaRef And FgValRef.row > 0 Then
            If FgEscRef.row <= nres(nResActivo).EstrutAnaRef(FgValRef.row).totalEscRef And FgEscRef.row > 0 Then
                If FgEscRef.Col = 1 Or FgEscRef.Col = 3 Then
                    If EcColEscRef = lColEscRefTEscInf Then
                        If CbEscRefAux.ListIndex = mediComboValorNull Then
                            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).t_esc_inf = ""
                            FgEscRef.TextMatrix(EcLinhaEscRef, EcColEscRef) = ""
                        Else
                            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).t_esc_inf = CbEscRefAux.ItemData(CbEscRefAux.ListIndex)
                            FgEscRef.TextMatrix(EcLinhaEscRef, EcColEscRef) = CbEscRefAux
                        End If
                        nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_inf_dias = BL_Converte_Para_Dias(CInt(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_inf, 0)), nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).t_esc_inf)
                        If AvaliaIntervaloEscRef = False Then
                            CbEscRefAux.SetFocus
                            Exit Sub
                        End If
                    ElseIf EcColEscRef = lColEscRefTEscSup Then
                        If CbEscRefAux.ListIndex = mediComboValorNull Then
                            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).t_esc_sup = ""
                            FgEscRef.TextMatrix(EcLinhaEscRef, EcColEscRef) = ""
                        Else
                            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).t_esc_sup = CbEscRefAux.ItemData(CbEscRefAux.ListIndex)
                            FgEscRef.TextMatrix(EcLinhaEscRef, EcColEscRef) = CbEscRefAux
                        End If
                        nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_sup_dias = BL_Converte_Para_Dias(CInt(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_sup, 0)), nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).t_esc_sup)
                        If AvaliaIntervaloEscRef = False Then
                            CbEscRefAux.SetFocus
                            Exit Sub
                        End If
                    End If
                End If
            End If
        End If
        CbEscRefAux.ListIndex = mediComboValorNull
        CbEscRefAux.Visible = False
        FgEscRef.SetFocus
        FgEscRef.Col = FgEscRef.Col + 1
        FgEscRef_Click
    End If

End Sub

Private Sub BtCopiaPrecario_Click()
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_rubr"
    CamposEcran(1) = "cod_rubr"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_rubr"
    CamposEcran(2) = "descr_rubr"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "fa_rubr"
    CampoPesquisa1 = "descr_rubr"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexaoSecundaria, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, " descr_rubr", " Pesquisar R�bricas")
    
    mensagem = "N�o foi encontrada nenhuma R�brica"
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            gMsgTitulo = " Remover"
            gMsgMsg = "Tem a certeza que deseja copiar os pre�os da r�brica " & Resultados(1) & " - " & Resultados(2) & "?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
            If gMsgResp = vbYes Then
                CopiaRubrica CStr(Resultados(1))
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


Private Sub CbPMin_click()
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If CInt(EcLinhaAnaRef) <= nres(nResActivo).TotalAnaRef Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.t_comp_pmin = CbPMin
        End If
    End If
End Sub

Private Sub CbPMax_click()
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If CInt(EcLinhaAnaRef) <= nres(nResActivo).TotalAnaRef Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.t_comp_pmax = CbPMax
        End If
    End If
End Sub
Private Sub CbnMin_click()
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If CInt(EcLinhaAnaRef) <= nres(nResActivo).TotalAnaRef Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.t_comp_nmin = CbNMin
        End If
    End If
End Sub

Private Sub CbnMax_click()
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If CInt(EcLinhaAnaRef) <= nres(nResActivo).TotalAnaRef Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.t_comp_nmax = CbNMax
        End If
    End If
End Sub

Private Sub CbdMin_click()
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If CInt(EcLinhaAnaRef) <= nres(nResActivo).TotalAnaRef Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.t_comp_dmin = CbDMin
        End If
    End If
End Sub

Private Sub CbdMax_click()
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If CInt(EcLinhaAnaRef) <= nres(nResActivo).TotalAnaRef Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.t_comp_dmax = CbDMax
        End If
    End If
End Sub

Private Sub CbSexo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbSexo.ListIndex = mediComboValorNull
    End If
End Sub


Private Sub CkCutOff_Click()
    Dim msg As String
    Dim i As Long
    If nResActivo > mediComboValorNull Then
        If CkCutOff.value = vbChecked Then
            If nres(nResActivo).TotalAnaRef >= 1 Then
                For i = 1 To nres(nResActivo).TotalAnaRef
                    If nres(nResActivo).EstrutAnaRef(i).t_ref = "E" And nres(nResActivo).EstrutAnaRef(i).dt_valid_inf = "" Then
                        msg = "Existem dados de escal�es de refer�ncia codificados! Ter� que elimin�-los primeiro."
                        BG_Mensagem mediMsgBox, msg, vbOKOnly + vbInformation, "Cut-Off"
                        CkCutOff.value = vbUnchecked
                        Exit Sub
                    ElseIf nres(nResActivo).EstrutAnaRef(i).t_ref = "V" And nres(nResActivo).EstrutAnaRef(i).dt_valid_inf = "" Then
                        msg = "Existem dados de valores de refer�ncia codificados! Ter� que elimin�-los primeiro."
                        BG_Mensagem mediMsgBox, msg, vbOKOnly + vbInformation, "Cut-Off"
                        CkCutOff.value = vbUnchecked
                        Exit Sub
                    End If
                Next
            End If
            BtZonas.Visible = False
            BtEscaloes.Visible = False
            BtValRef.Visible = False
            BtCutOff.Visible = True
            Exit Sub
        ElseIf CkCutOff.value = vbUnchecked Then
            If nres(nResActivo).TotalAnaRef >= 1 Then
                For i = 1 To nres(nResActivo).TotalAnaRef
                    If nres(nResActivo).EstrutAnaRef(i).t_ref = "C" Then
                        BG_Mensagem mediMsgBox, "Existem dados de cut-off codificados! Ter� que elimin�-los primeiro.", vbOKOnly + vbInformation, "Cut-Off"
                        CkCutOff.value = vbChecked
                        Exit Sub
                    End If
                Next
            End If
            BtZonas.Visible = True
            BtEscaloes.Visible = True
            BtValRef.Visible = True
            BtCutOff.Visible = False
        End If
    End If

End Sub





Private Sub CkSegundoRes_Click()
    If CkSegundoRes.value = vbChecked Then
        FrameSegundoRes.Enabled = True
    Else
        FrameSegundoRes.Enabled = False
        'BG_LimpaCampo_Todos CamposEc2Res
        
    End If
End Sub

Private Sub EcAjuda_Click()
  FormAjudaFormulaAna.Show

End Sub


Private Sub EcAjudaFem_Click()
    FormAjudaFormulaAna.Show
End Sub

Private Sub EcAuxInterferencia_GotFocus()
    linhaActual = FgInterferencias.row
    colunaActual = FgInterferencias.Col
End Sub

Private Sub EcAuxInterferencia_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 13 Then
    EcAuxInterferencia_LostFocus
End If
End Sub

Private Sub EcAuxInterferencia_LostFocus()
    FgInterferencias.TextMatrix(linhaActual, colunaActual) = EcAuxInterferencia
    FgInterferencias.SetFocus
    EcAuxInterferencia.Visible = False
    If colunaActual <> FgInterferencias.Cols - 1 Then
        FgInterferencias.Col = colunaActual + 1
    End If
End Sub



Private Sub EcAuxPreco_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    If KeyCode = vbKeyReturn Then
        If EcLinhaPreco <> "" And EcColunaPreco <> "" Then
            If EcColunaPreco = lColPrecosCodRubrEFR Then
                EstrutPrec(EcLinhaPreco).cod_rubr_efr = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosDescrRubrEFR Then
                EstrutPrec(EcLinhaPreco).descr_rubr_efr = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosTxModeradora Then
                EstrutPrec(EcLinhaPreco).val_Taxa = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosNumC Then
                EstrutPrec(EcLinhaPreco).nr_c = EcAuxPreco
                EstrutPrec(EcLinhaPreco).val_pag_ent = Round(CDbl(EstrutPrec(EcLinhaPreco).nr_c) * CDbl(EstrutPrec(EcLinhaPreco).valor_c), 2)
                FgPrecos.TextMatrix(EcLinhaPreco, lColPrecosValPagEFR) = EstrutPrec(EcLinhaPreco).val_pag_ent
            ElseIf EcColunaPreco = lColPrecosNumK Then
                EstrutPrec(EcLinhaPreco).nr_k = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosPercDoente Then
                EstrutPrec(EcLinhaPreco).perc_pag_doe = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosValPagDoe Then
                EstrutPrec(EcLinhaPreco).val_pag_doe = EcAuxPreco
            ElseIf EcColunaPreco = lColPrecosValPagEFR Then
                EstrutPrec(EcLinhaPreco).val_pag_ent = EcAuxPreco
            End If
            FgPrecos.TextMatrix(EcLinhaPreco, EcColunaPreco) = EcAuxPreco
            EcAuxPreco = ""
            EcAuxPreco.Visible = False
            FgPrecos.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro EcAuxPreco_KeyDown", Me.Name, "EcAuxPreco_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxPreco_LostFocus()
    EcAuxPreco.Visible = False
    EcAuxPreco = ""
End Sub

Private Sub EcAuxPreco_Validate(Cancel As Boolean)
    EcAuxPreco.Visible = False
    EcAuxPreco = ""
End Sub

Private Sub EcCasasDecimais_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, EcCasasDecimais

End Sub

Private Sub EcCodgrImpr_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodGrImpr)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrImpr.text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_gr_impr, descr_gr_impr FROM sl_gr_impr WHERE upper(cod_gr_impr)= " & BL_TrataStringParaBD(UCase(EcCodGrImpr.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodGrImpr.text = "" & RsDescrGrupo!cod_gr_impr
            EcDescrGrImpr.text = "" & RsDescrGrupo!descr_gr_impr
        Else
            Cancel = True
            EcDescrGrImpr.text = ""
            BG_Mensagem mediMsgBox, "O grupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrGrImpr.text = ""
    End If

End Sub

Private Sub EcCodGrupo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodGrupo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrupo.text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_gr_ana, descr_gr_ana FROM sl_gr_ana WHERE upper(cod_gr_ana)= " & BL_TrataStringParaBD(UCase(EcCodGrupo.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodGrupo.text = "" & RsDescrGrupo!cod_gr_ana
            EcDescrGrupo.text = "" & RsDescrGrupo!descr_gr_ana
        Else
            Cancel = True
            EcDescrGrupo.text = ""
            BG_Mensagem mediMsgBox, "O grupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrGrupo.text = ""
    End If

End Sub

Private Sub EcCodigo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcCodigo_Validate False
        FuncaoProcurar
    End If
End Sub



Private Sub EcCodRubrica_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    On Error GoTo TrataErro
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodRubrica.text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_rubr, descr_rubr FROM fa_rubr WHERE upper(cod_rubr)= " & BL_TrataStringParaBD(UCase(EcCodRubrica.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexaoSecundaria
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodRubrica.text = "" & RsDescrGrupo!cod_rubr
            EcDescrRubrica.text = "" & RsDescrGrupo!descr_rubr
        
        Else
            Cancel = True
            EcCodRubrica = ""
            EcDescrRubrica.text = ""
            BG_Mensagem mediMsgBox, "A R�brica indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            'SendKeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrRubrica.text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao obter descri��o da R�brica: " & Err.Description, Me.Name, "EcCodRubrica_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcCodsubGrupo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodGrupo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodSubGrupo.text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_sgr_ana, descr_sgr_ana FROM sl_sgr_ana WHERE upper(cod_sgr_ana)= " & BL_TrataStringParaBD(UCase(EcCodSubGrupo.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodSubGrupo.text = "" & RsDescrGrupo!cod_sgr_ana
            EcDescrSubGrupo.text = "" & RsDescrGrupo!descr_sgr_ana
        Else
            Cancel = True
            EcDescrSubGrupo.text = ""
            BG_Mensagem mediMsgBox, "O Subgrupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrSubGrupo.text = ""
    End If

End Sub

Private Sub EcCodclasse_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodClasse.text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_classe_ana, descr_classe_ana FROM sl_classe_ana WHERE upper(cod_classe_ana)= " & BL_TrataStringParaBD(UCase(EcCodClasse.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodClasse.text = "" & RsDescrGrupo!cod_classe_ana
            EcDescrClasse.text = "" & RsDescrGrupo!descr_classe_ana
        Else
            Cancel = True
            EcDescrClasse.text = ""
            BG_Mensagem mediMsgBox, "A Classe indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrClasse.text = ""
    End If

End Sub

Private Sub EcCodProd_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProd.text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_produto, descr_produto FROM sl_produto WHERE upper(cod_produto)= " & BL_TrataStringParaBD(UCase(EcCodProd.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodProd.text = "" & RsDescrGrupo!cod_produto
            EcDescrProduto.text = "" & RsDescrGrupo!descr_produto
        Else
            Cancel = True
            EcDescrProduto.text = ""
            BG_Mensagem mediMsgBox, "O Produto indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrProduto.text = ""
    End If

End Sub

Private Sub EcCodtubop_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodTuboP.text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_tubo, descr_tubo FROM sl_tubo WHERE upper(cod_tubo)= " & BL_TrataStringParaBD(UCase(EcCodTuboP.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodTuboP.text = "" & RsDescrGrupo!cod_tubo
            EcDescrTuboP.text = "" & RsDescrGrupo!descR_tubo
        Else
            Cancel = True
            EcDescrTuboP.text = ""
            BG_Mensagem mediMsgBox, "O Tubo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrTuboP.text = ""
    End If
End Sub
Private Sub EcCodTuboS_LostFocus()
    
    EcCodTuboS.text = UCase(EcCodTuboS.text)
    

End Sub
Private Sub EcCodtuboS_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodTuboS.text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_tubo, descr_tubo FROM sl_Tubo_sec WHERE upper(cod_tubo)= " & BL_TrataStringParaBD(UCase(EcCodTuboS.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodTuboS.text = "" & RsDescrGrupo!cod_tubo
            EcDescrTuboS.text = "" & RsDescrGrupo!descR_tubo
        Else
            Cancel = True
            EcDescrTuboS.text = ""
            BG_Mensagem mediMsgBox, "O Tubo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            'SendKeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrTuboS.text = ""
    End If

End Sub
Private Sub EcCodMetodo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodMetodo.text) <> "" And nResActivo > mediComboValorNull Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_metodo, descr_metodo FROM sl_metodo WHERE upper(cod_metodo)= " & BL_TrataStringParaBD(UCase(EcCodMetodo.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodMetodo.text = "" & RsDescrGrupo!cod_metodo
            EcDescrMetodo.text = "" & RsDescrGrupo!descr_metodo
            If EcLinhaAnaRef <> "" And IsNumeric(EcLinhaAnaRef) Then
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cod_metodo = EcCodMetodo
            End If
        Else
            Cancel = True
            EcDescrMetodo.text = ""
            BG_Mensagem mediMsgBox, "O M�todo indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrMetodo.text = ""
        If EcLinhaAnaRef <> "" And IsNumeric(EcLinhaAnaRef) Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cod_metodo = EcCodMetodo
        End If
    End If

End Sub
Private Sub EcCodReagente_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodReagente.text) <> "" And nResActivo > mediComboValorNull Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_reagent, descr_reagent FROM sl_Reagent WHERE upper(cod_reagent)= " & BL_TrataStringParaBD(UCase(EcCodReagente.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodReagente.text = "" & RsDescrGrupo!cod_reagent
            EcDescrReagente.text = "" & RsDescrGrupo!descr_reagent
            If EcLinhaAnaRef <> "" And IsNumeric(EcLinhaAnaRef) Then
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cod_reag = EcCodReagente
            End If
        Else
            Cancel = True
            EcDescrReagente.text = ""
            BG_Mensagem mediMsgBox, "O M�todo indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrReagente.text = ""
        If EcLinhaAnaRef <> "" And IsNumeric(EcLinhaAnaRef) Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cod_reag = EcCodReagente
        End If
    End If

End Sub
Private Sub EcCodGenero_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodClasse)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGenero.text) <> "" And nResActivo > mediComboValorNull Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_genero, descr_genero FROM sl_genero WHERE upper(cod_genero)= " & BL_TrataStringParaBD(UCase(EcCodGenero.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            'If VerificaPodeAlterarGenero(RsDescrGrupo!cod_genero) = True Then
                EcCodGenero.text = "" & RsDescrGrupo!cod_genero
                EcDescrGenero.text = "" & RsDescrGrupo!descr_genero
                If EcLinhaAnaRef <> "" And IsNumeric(EcLinhaAnaRef) Then
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cod_genero = EcCodGenero
                End If
            'Else
            '    Cancel = True
            '    EcDescrGenero.text = ""
            '    BG_Mensagem mediMsgBox, "J� existe um Intervalo para esse g�nero!", vbOKOnly + vbExclamation, App.ProductName
            '    SendKeys ("{HOME}+{END}")
            'End If
        Else
            Cancel = True
            EcDescrGenero.text = ""
            BG_Mensagem mediMsgBox, "O G�nero indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrGenero.text = ""
        If EcLinhaAnaRef <> "" And IsNumeric(EcLinhaAnaRef) Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cod_genero = EcCodGenero
        End If
    End If

End Sub
Private Sub EcCodigo_Validate(Cancel As Boolean)
    
    EcCodigo.text = UCase(EcCodigo.text)
    If Trim(EcCodigo.text) = "S" Then
        BG_Mensagem mediMsgBox, "O [S] por si s�, n�o pode ser c�digo da an�lise, pois � identificativo do tipo de an�lise!", vbExclamation, "C�digo da Simples"
        Cancel = True
    ElseIf left(Trim(EcCodigo.text), 1) <> "S" And Trim(EcCodigo.text) <> "" Then
        EcCodigo.text = "S" & EcCodigo.text
    ElseIf Trim(EcCodigo.text) <> "" Then
        EcCodigo.text = left(Trim(EcCodigo.text), 1) & Mid(EcCodigo.text, 2, Len(EcCodigo.text) - 1)
    End If
    
End Sub


Private Sub EcCodTubop_LostFocus()

    Dim sql As String

    Dim Tabela As ADODB.recordset

    EcCodTuboP.text = UCase(EcCodTuboP.text)
    If Trim(EcCodTuboP.text) <> "" Then

        Set Tabela = New ADODB.recordset

        sql = "SELECT capaci_util " & _
              "FROM sl_tubo " & _
              "WHERE cod_tubo=" & BL_TrataStringParaBD(EcCodTuboP)
        
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
        
        If Tabela.RecordCount > 0 Then
            LbCapacidadeUtil.caption = BL_HandleNull(Tabela!capaci_util, "")
        End If
        
        
        Tabela.Close
        Set Tabela = Nothing

    Else
        LbCapacidadeUtil.caption = ""
    End If

End Sub




Private Sub EcConcDescr_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcConcdescr_validate False
        EcConcFormula.SetFocus
    End If
End Sub

Private Sub EcConcFormula_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcConcFormula_validate False
        EcConcDescr.SetFocus
    End If
End Sub

Private Sub EcCutOffPMin_lostfocus()
    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.val_pos_min = EcCutOffPMin
End Sub
Private Sub EcCutOffPMax_lostfocus()
    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.val_pos_max = EcCutOffPMax
End Sub
Private Sub EcComPos_lostfocus()
    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.com_pos = EcComPos
End Sub


Private Sub EcCutOffnMin_lostfocus()
    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.val_neg_min = EcCutOffNMin
End Sub
Private Sub EcCutOffnMax_lostfocus()
    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.val_neg_max = EcCutOffNMax
End Sub
Private Sub EcComneg_lostfocus()
    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.com_neg = EcComNeg
End Sub

Private Sub EcCutOffdMin_lostfocus()
    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.val_duv_min = EcCutOffDMin
End Sub
Private Sub EcCutOffDMax_lostfocus()
    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.val_duv_max = EcCutOffDMax
End Sub
Private Sub EcComDuv_lostfocus()
    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cutoff.com_duv = EcComDuv
End Sub

Private Sub EcEscRefAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcEscRefAux_validate False
        If FgEscRef.Col < FgEscRef.Cols - 1 Then
            FgEscRef.Col = FgEscRef.Col + 1
        End If
    End If
End Sub

Private Sub EcEscRefAux_LostFocus()
    FgEscRef.Enabled = True
    FgEscRef.SetFocus
End Sub

Private Sub EcEscRefAux_validate(Cancel As Boolean)
    If EcEscRefAux = "" Then
        EcEscRefAux = "0"
    End If
    If FgValRef.row <= nres(nResActivo).TotalAnaRef And FgValRef.row > 0 And nResActivo > mediComboValorNull Then
        If FgEscRef.row <= nres(nResActivo).EstrutAnaRef(FgValRef.row).totalEscRef And FgEscRef.row > 0 Then
            If FgEscRef.Col <> 1 And FgEscRef.Col <> 3 Then
                If FgEscRef.Col = lColEscRefEscInf Then
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_inf = BL_HandleNull(EcEscRefAux, "")
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_inf_dias = BL_Converte_Para_Dias(CInt(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_inf), nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).t_esc_inf)
                    If AvaliaIntervaloEscRef = False Then
                        EcEscRefAux.SetFocus
                        Exit Sub
                    End If
                ElseIf FgEscRef.Col = lColEscRefEscSup Then
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_sup = BL_HandleNull(EcEscRefAux, "")
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_sup_dias = BL_Converte_Para_Dias(CInt(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_sup), nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).t_esc_sup)
                    If AvaliaIntervaloEscRef = False Then
                        EcEscRefAux.SetFocus
                        Exit Sub
                    End If
                ElseIf FgEscRef.Col = lColEscRefFRefMax Then
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).f_ref_max = BL_HandleNull(EcEscRefAux, "")
                    If AvaliaValoresEscRef("F") = False Then
                        EcEscRefAux.SetFocus
                        Exit Sub
                    End If
                ElseIf FgEscRef.Col = lColEscRefFRefMin Then
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).f_ref_min = BL_HandleNull(EcEscRefAux, "")
                    If AvaliaValoresEscRef("F") = False Then
                        EcEscRefAux.SetFocus
                        Exit Sub
                    End If
                ElseIf FgEscRef.Col = lColEscRefHRefMax Then
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).h_ref_max = BL_HandleNull(EcEscRefAux, "")
                    If AvaliaValoresEscRef("H") = False Then
                        EcEscRefAux.SetFocus
                        Exit Sub
                    End If
                ElseIf FgEscRef.Col = lColEscRefHRefMin Then
                    nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).h_ref_min = BL_HandleNull(EcEscRefAux, "")
                    If AvaliaValoresEscRef("H") = False Then
                        EcEscRefAux.SetFocus
                        Exit Sub
                    End If
                End If
                FgEscRef.TextMatrix(EcLinhaEscRef, EcColEscRef) = BL_HandleNull(EcEscRefAux, "")
            End If
        End If
    End If
    EcEscRefAux = ""
    EcEscRefAux.Visible = False
    FgEscRef.Enabled = True
    FgEscRef.SetFocus
    
End Sub


Private Sub EcFormula_LostFocus()
    
    Dim s As String

    If EcFormula.text <> "" Then
        s = BL_FormulaResolve(EcFormula.text, True)
        If Trim(s) = "" Then
            BG_Mensagem mediMsgBox, "F�rmula mal definida", vbExclamation + vbOKOnly, "F�rmula."
            EcFormula.SetFocus
        End If
    End If
    
End Sub
Private Sub EcFormulaFem_LostFocus()
    
    Dim s As String

    If EcFormulaFem.text <> "" Then
        s = BL_FormulaResolve(EcFormulaFem.text, True)
        If Trim(s) = "" Then
            BG_Mensagem mediMsgBox, "F�rmula mal definida", vbExclamation + vbOKOnly, "F�rmula."
            EcFormulaFem.SetFocus
        End If
    End If
    
End Sub

Private Sub EcFormulaInterf_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn And EcFormulaInterf <> "" Then
        CbAccaoInterferencia.SetFocus
    End If
End Sub

Private Sub EcLinhasBrancoImp_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcLinhasBrancoImp)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    Else
        If EcLinhasBrancoImp.text <> "" Then
            If CLng(EcLinhasBrancoImp.text) > 10 Then
                BG_Mensagem mediMsgBox, "N� fora do limite", vbExclamation, App.ProductName
                Cancel = True
                Sendkeys ("{HOME}+{END}")
            End If
        End If
    End If

End Sub

Private Sub EcMinMasc_validate(Cancel As Boolean)
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If IsNumeric(EcLinhaAnaRef) Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).valoresRef.h_ref_min = EcMinMasc
        End If
    End If
End Sub
Private Sub EcMaxMasc_validate(Cancel As Boolean)
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If IsNumeric(EcLinhaAnaRef) Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).valoresRef.h_ref_max = EcMaxMasc
        End If
    End If
End Sub
Private Sub EcMinFem_validate(Cancel As Boolean)
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If IsNumeric(EcLinhaAnaRef) Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).valoresRef.f_ref_min = EcMinFem
        End If
    End If
End Sub
Private Sub EcMaxFem_validate(Cancel As Boolean)
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If IsNumeric(EcLinhaAnaRef) Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).valoresRef.f_ref_max = EcMaxFem
        End If
    End If
End Sub
Private Sub EcComMaF_validate(Cancel As Boolean)
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If IsNumeric(EcLinhaAnaRef) Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).valoresRef.com_maf = EcComMaF
        End If
    End If
End Sub
Private Sub EcComMaH_validate(Cancel As Boolean)
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If IsNumeric(EcLinhaAnaRef) Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).valoresRef.com_mah = EcComMaH
        End If
    End If
End Sub
Private Sub EcComMiF_validate(Cancel As Boolean)
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If IsNumeric(EcLinhaAnaRef) Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).valoresRef.com_mif = EcComMiF
        End If
    End If
End Sub
Private Sub EcComMiH_validate(Cancel As Boolean)
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If IsNumeric(EcLinhaAnaRef) Then
            nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).valoresRef.com_mih = EcComMiH
        End If
    End If
End Sub
Private Sub EcObsDesblInterferencia_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyReturn And EcFormulaInterf <> "" And CbAccaoInterferencia.ListIndex <> mediComboValorNull And EcObsInterferencia <> "" Then
    AdicionaInterferencia EcFormulaInterf, BG_DaComboSel(CbAccaoInterferencia), EcObsInterferencia, EcObsDesblInterferencia, BL_HandleNull(EcInterfSeq, -1)
    EcObsInterferencia = ""
    EcObsDesblInterferencia = ""
    CbAccaoInterferencia.ListIndex = mediComboValorNull
    EcFormulaInterf = ""
End If

End Sub

Private Sub EcObsInterferencia_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EcObsDesblInterferencia.SetFocus
    End If
End Sub



Private Sub EcObsRef_Validate(Cancel As Boolean)
    If EcLinhaAnaRef <> "" And IsNumeric(EcLinhaAnaRef) And nResActivo > mediComboValorNull Then
        nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).obs_ref = BL_HandleNull(EcObsRef, "")
    End If
End Sub

Private Sub EcOrdemARS_Validate(Cancel As Boolean)
    If EcOrdemARS = "" Then Exit Sub
    If Not IsNumeric(EcOrdemARS) Then
        BG_Mensagem mediMsgBox, "Valor tem que ser num�rico.", vbExclamation + vbOKOnly, "Ordem ARS."
        EcOrdemARS.SetFocus
    End If
End Sub

Private Sub EcPeso_Change()
    
    BG_ValidaTipoCampo_ADO Me, EcPeso

End Sub


Private Sub EcQuantMinima_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcQuantMinima)

End Sub




Private Sub EcTipoResultado_Click()
    If EcTipoResultado.ListIndex > mediComboValorNull Then
        If EcTipoResultado.ItemData(EcTipoResultado.ListIndex) = gT_Relatorio Then
        
        End If
    End If
End Sub

Private Sub EcTipoResultado_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcTipoResultado.ListIndex = -1

End Sub

Private Sub EcTipoUnidadeAux_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcTipoUnidadeAux.ListIndex = -1

End Sub
Private Sub EcTipoUnidadeAux_Click()
    
    If EcUnidade1.text = "" Then
        EcTipoUnidadeAux.ListIndex = -1
    Else
        Select Case EcTipoUnidadeAux.ListIndex
            Case 0
                EcTipoUnidade.text = "I"
            Case 1
                EcTipoUnidade.text = "C"
        End Select
        OptUnidade1.value = True
    End If

End Sub

Private Sub EcTipoUnidadeAux_LostFocus()
    
    If EcUnidade1.text <> "" And EcTipoUnidadeAux.ListIndex = -1 Then
        EcTipoUnidadeAux.SetFocus
    End If

End Sub

Private Sub EcTipoUnidadeAux2_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcTipoUnidadeAux2.ListIndex = -1

End Sub

Private Sub EcTipoUnidadeAux2_LostFocus()
    
    If EcUnidade2.text <> "" And EcTipoUnidadeAux2.ListIndex = -1 Then
        EcTipoUnidadeAux2.SetFocus
    End If

End Sub

Private Sub EcTipoUnidadeAux3_click()
    If EcUnidade1Res2.text = "" Then
        EcTipoUnidadeAux3.ListIndex = -1
    Else
        Select Case EcTipoUnidadeAux3.ListIndex
            Case 0
                EcTipoUnidade3.text = "I"
            Case 1
                EcTipoUnidade3.text = "C"
        End Select
        OptUnidade1Res2.value = True
    End If

End Sub
Private Sub EcTipoUnidadeAux4_click()
    If EcUnidade2Res2.text = "" Then
        EcTipoUnidadeAux4.ListIndex = -1
    Else
        Select Case EcTipoUnidadeAux4.ListIndex
            Case 0
                EcTipoUnidade4.text = "I"
            Case 1
                EcTipoUnidade4.text = "C"
        End Select
        OptUnidade2Res2.value = True
    End If

End Sub

Private Sub EcUnidade1_Change()
    
    If EcUnidade1.text = "" Then
        EcTipoUnidadeAux.ListIndex = -1
    Else
        If OptUnidade2.value = False And OptUnidade1.value = False Then
            OptUnidade1.value = True
        End If
    End If

End Sub

Private Sub EcUnidade2_Change()
    
    If EcUnidade2.text = "" Then
        EcTipoUnidadeAux2.ListIndex = -1
    Else
        If OptUnidade2.value = False And OptUnidade1.value = False Then
            OptUnidade2.value = True
        End If
    End If

End Sub

Private Sub EcValGrafico_LostFocus()
    If Not IsNumeric(EcValGrafico) And EcValGrafico <> "" Then
        BG_Mensagem mediMsgBox, "Valor para Gr�fico mal definido.", vbExclamation + vbOKOnly, "Gr�fico."
        EcValGrafico.SetFocus
    End If
End Sub

Private Sub FgConc_Click()
    If FgConc.row <= TotalConcl And FgConc.row > 0 Then
        EcLinhaConc = FgConc.row
        EcConcFormula = EstrutConcl(FgConc.row).formula
        EcConcDescr = EstrutConcl(FgConc.row).conclusao
    Else
        EcLinhaConc = ""
        EcConcFormula = ""
        EcConcDescr = ""
    End If
End Sub

Private Sub FgConc_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim linha As Long
    If KeyCode = vbKeyDelete Then
        FgConc.RemoveItem FgConc.row
        For linha = FgConc.row To TotalConcl - 1
            EstrutConcl(linha).conclusao = EstrutConcl(linha + 1).conclusao
            EstrutConcl(linha).formula = EstrutConcl(linha + 1).formula
        Next
        TotalConcl = TotalConcl - 1
        ReDim Preserve EstrutConcl(TotalConcl)
        EcConcDescr = ""
        EcConcFormula = ""
    End If
End Sub

Private Sub FgConc_RowColChange()
    FgConc_Click
End Sub

Private Sub FgEscRef_Click()
    Dim i As Long
    If FgValRef.row <= nres(nResActivo).TotalAnaRef And FgValRef.row > 0 And nResActivo > mediComboValorNull Then
        If FgEscRef.row <= nres(nResActivo).EstrutAnaRef(FgValRef.row).totalEscRef And FgEscRef.row > 0 Then
            EcLinhaEscRef = FgEscRef.row
            EcColEscRef = FgEscRef.Col
        End If
    End If
End Sub

Private Sub FgEscRef_DblClick()
    Dim i As Integer
    If FgValRef.row <= nres(nResActivo).TotalAnaRef And FgValRef.row > 0 And nResActivo > mediComboValorNull Then
        If FgEscRef.row <= nres(nResActivo).EstrutAnaRef(FgValRef.row).totalEscRef And FgEscRef.row > 0 Then
            If FgEscRef.Col <> 1 And FgEscRef.Col <> 3 Then
                EcLinhaEscRef = FgEscRef.row
                EcColEscRef = FgEscRef.Col
                If FgEscRef.Col = lColEscRefEscInf Then
                    EcEscRefAux = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_inf
                ElseIf FgEscRef.Col = lColEscRefEscSup Then
                    EcEscRefAux = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_sup
                ElseIf FgEscRef.Col = lColEscRefFRefMax Then
                    EcEscRefAux = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).f_ref_max
                ElseIf FgEscRef.Col = lColEscRefFRefMin Then
                    EcEscRefAux = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).f_ref_min
                ElseIf FgEscRef.Col = lColEscRefHRefMax Then
                    EcEscRefAux = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).h_ref_max
                ElseIf FgEscRef.Col = lColEscRefHRefMin Then
                    EcEscRefAux = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).h_ref_min
                End If
                EcEscRefAux.top = FgEscRef.CellTop + FgEscRef.top
                EcEscRefAux.left = FgEscRef.CellLeft + FgEscRef.left
                EcEscRefAux.Width = FgEscRef.CellWidth + 10
                EcEscRefAux.Visible = True
                EcEscRefAux.SetFocus
            ElseIf FgEscRef.Col = 1 Or FgEscRef.Col = 3 Then
                If FgEscRef.Col = lColEscRefTEscInf Then
                    If nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).t_esc_inf = "" Then
                        CbEscRefAux.ListIndex = mediComboValorNull
                    Else
                        For i = 0 To CbEscRefAux.ListCount - 1
                            If nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).t_esc_inf = CbEscRefAux.ItemData(i) Then
                                CbEscRefAux.ListIndex = i
                                Exit For
                            End If
                        Next
                    End If
                ElseIf FgEscRef.Col = lColEscRefTEscSup Then
                    If nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).t_esc_sup = "" Then
                        CbEscRefAux.ListIndex = mediComboValorNull
                    Else
                        For i = 0 To CbEscRefAux.ListCount - 1
                            If nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).t_esc_sup = CbEscRefAux.ItemData(i) Then
                                CbEscRefAux.ListIndex = i
                                Exit For
                            End If
                        Next
                    End If
                End If
                CbEscRefAux.top = FgEscRef.CellTop + FgEscRef.top
                CbEscRefAux.left = FgEscRef.CellLeft + FgEscRef.left
                CbEscRefAux.Width = FgEscRef.CellWidth + 10
                CbEscRefAux.Visible = True
                CbEscRefAux.SetFocus
            End If
        End If
    End If
End Sub

Private Sub FgEscRef_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        FgEscRef_DblClick
    End If
End Sub

Private Sub FgEscRef_RowColChange()
    FgEscRef_Click
End Sub

Private Sub FgInterferencias_Click()
    LbLabel.caption = FgInterferencias.TextMatrix(FgInterferencias.row, FgInterferencias.Col)
End Sub

Private Sub FgInterferencias_DblClick()
    Dim i As Integer
    If FgInterferencias.row <= TotalEstrutinterf Then
        EcInterfSeq = EstrutInterf(FgInterferencias.row).seq_interf
        EcFormulaInterf = EstrutInterf(FgInterferencias.row).formula
        For i = 0 To CbAccaoInterferencia.ListCount - 1
            If CbAccaoInterferencia.ItemData(i) = EstrutInterf(FgInterferencias.row).cod_accao Then
                CbAccaoInterferencia.ListIndex = i
            End If
        Next
        EcObsInterferencia = EstrutInterf(FgInterferencias.row).obs
        EcObsDesblInterferencia = EstrutInterf(FgInterferencias.row).obs_desbloq
        
        For i = FgInterferencias.row To FgInterferencias.rows - 3
            EstrutInterf(i).formula = EstrutInterf(i + 1).formula
            EstrutInterf(i).cod_accao = EstrutInterf(i + 1).cod_accao
            EstrutInterf(i).descr_accao = EstrutInterf(i + 1).descr_accao
            EstrutInterf(i).obs = EstrutInterf(i + 1).obs
            EstrutInterf(i).obs_desbloq = EstrutInterf(i + 1).obs_desbloq
            EstrutInterf(i).seq_interf = EstrutInterf(i + 1).seq_interf
        Next
        EstrutInterf(TotalEstrutinterf).formula = ""
        EstrutInterf(TotalEstrutinterf).cod_accao = -1
        EstrutInterf(TotalEstrutinterf).descr_accao = ""
        EstrutInterf(TotalEstrutinterf).obs = ""
        EstrutInterf(TotalEstrutinterf).obs_desbloq = ""
        EstrutInterf(TotalEstrutinterf).seq_interf = -1
        
        TotalEstrutinterf = TotalEstrutinterf - 1
        ReDim Preserve EstrutInterf(TotalEstrutinterf)
        FgInterferencias.RemoveItem FgInterferencias.row
    End If
    
'    Dim i As Integer
'
'    EcAuxInterferencia = ""
'    If FgInterferencias.Row > 0 And FgInterferencias.Col > 1 And FgInterferencias.Col <> 4 And FgInterferencias.TextMatrix(FgInterferencias.Row, 0) <> "" Then
'        EcAuxInterferencia.Visible = True
'        EcAuxInterferencia.Width = FgInterferencias.ColWidth(FgInterferencias.Col)
'        EcAuxInterferencia.Left = FgInterferencias.Left + FgInterferencias.ColPos(FgInterferencias.Col) + 40
'        EcAuxInterferencia.Top = FgInterferencias.Top + FgInterferencias.RowPos(FgInterferencias.Row) + 40
'        EcAuxInterferencia = FgInterferencias.TextMatrix(FgInterferencias.Row, FgInterferencias.Col)
'        EcAuxInterferencia.SetFocus
'    ElseIf FgInterferencias.Row > 0 And FgInterferencias.Col = 4 And FgInterferencias.TextMatrix(FgInterferencias.Row, 0) <> "" Then
'        CbAuxInterferencia.Width = FgInterferencias.ColWidth(FgInterferencias.Col)
'        CbAuxInterferencia.Left = FgInterferencias.Left + FgInterferencias.ColPos(FgInterferencias.Col) + 40
'        CbAuxInterferencia.Top = FgInterferencias.Top + FgInterferencias.RowPos(FgInterferencias.Row) + 40
'        If FgInterferencias.TextMatrix(FgInterferencias.Row, 5) <> "" Then
'            For i = 0 To CbAuxInterferencia.ListCount - 1
'                If CbAuxInterferencia.ItemData(i) = FgInterferencias.TextMatrix(FgInterferencias.Row, 5) Then
'                    CbAuxInterferencia.ListIndex = i
'                End If
'            Next
'        Else
'            CbAuxInterferencia.ListIndex = -1
'        End If
'        CbAuxInterferencia.Visible = True
'        CbAuxInterferencia.SetFocus
'    End If
End Sub

Private Sub FgInterferencias_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        If FgInterferencias.TextMatrix(FgInterferencias.row, 0) <> "" And FgInterferencias.row > 0 Then
            FgInterferencias.RemoveItem FgInterferencias.row
        End If
    End If
End Sub



Private Sub FgPrecos_Click()
    EcLinhaPreco = FgPrecos.row
    EcColunaPreco = FgPrecos.Col
End Sub

Private Sub FgPrecos_DblClick()
    EcLinhaPreco = ""
    EcColunaPreco = ""
    If FgPrecos.row > 0 And FgPrecos.row <= TotalPrec Then
        EcLinhaPreco = FgPrecos.row
        EcColunaPreco = FgPrecos.Col
        If FgPrecos.Col <> lColPrecosCodPrecario And FgPrecos.Col <> lColPrecosDescrPrecario And FgPrecos.Col <> lColPrecosTipoFact Then
            EcAuxPreco.Visible = True
            EcAuxPreco.left = FgPrecos.CellLeft + FgPrecos.left
            EcAuxPreco.top = FgPrecos.CellTop + FgPrecos.top
            EcAuxPreco.Width = FgPrecos.CellWidth + 10
            EcAuxPreco.SetFocus
        ElseIf FgPrecos.Col = lColPrecosTipoFact Then
            If EstrutPrec(EcLinhaPreco).t_fac = "1" Then
                EstrutPrec(EcLinhaPreco).t_fac = "2"
            Else
                EstrutPrec(EcLinhaPreco).t_fac = "1"
            End If
            If EstrutPrec(EcLinhaPreco).t_fac = "1" Then
                FgPrecos.TextMatrix(EcLinhaPreco, lColPrecosTipoFact) = "KS E CS"
            ElseIf EstrutPrec(EcLinhaPreco).t_fac = "2" Then
                FgPrecos.TextMatrix(EcLinhaPreco, lColPrecosTipoFact) = "EUROS"
            End If
    
        Else
            Exit Sub
        End If
        
        EstrutPrec(EcLinhaPreco).flg_modificado = True
        If EstrutPrec(EcLinhaPreco).user_cri = "" Then
            EstrutPrec(EcLinhaPreco).user_cri = gCodUtilizador
            EstrutPrec(EcLinhaPreco).dt_cri = Bg_DaData_ADO
        Else
            EstrutPrec(EcLinhaPreco).user_act = gCodUtilizador
            EstrutPrec(EcLinhaPreco).dt_act = Bg_DaData_ADO
        End If
        
        If FgPrecos.Col = lColPrecosCodRubrEFR Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).cod_rubr_efr
        ElseIf FgPrecos.Col = lColPrecosDescrRubrEFR Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).descr_rubr_efr
        ElseIf FgPrecos.Col = lColPrecosTxModeradora Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).val_Taxa
        ElseIf FgPrecos.Col = lColPrecosNumC Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).nr_c
        ElseIf FgPrecos.Col = lColPrecosNumK Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).nr_k
        ElseIf FgPrecos.Col = lColPrecosValPagEFR Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).val_pag_ent
        ElseIf FgPrecos.Col = lColPrecosValPagDoe Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).val_pag_doe
        ElseIf FgPrecos.Col = lColPrecosPercDoente Then
            EcAuxPreco = EstrutPrec(EcLinhaPreco).perc_pag_doe
        End If
    End If
End Sub

Private Sub FgPrecos_GotFocus()
    EcAuxPreco.Visible = False
End Sub

Private Sub FgPrecos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        FgPrecos_DblClick
    End If
End Sub

Private Sub FgValRef_Click()
    Dim i As Long
    Dim linha As Long
    Dim coluna As Long
    Dim l As Long
    Dim c As Long
    l = FgValRef.row
    c = FgValRef.Col
    If nResActivo > mediComboValorNull Then
        If FgValRef.row <= nres(nResActivo).TotalAnaRef And FgValRef.row > 0 And nResActivo > mediComboValorNull Then
            EcLinhaAnaRef = FgValRef.row
            EcSeqAnaRef = nres(nResActivo).EstrutAnaRef(FgValRef.row).seq_ana_ref
            EcDtIni = nres(nResActivo).EstrutAnaRef(FgValRef.row).dt_valid_inf
            EcDtFim = nres(nResActivo).EstrutAnaRef(FgValRef.row).dt_valid_sup
            EcCodMetodo = nres(nResActivo).EstrutAnaRef(FgValRef.row).cod_metodo
            EcCodMetodo_Validate False
            EcCodReagente = nres(nResActivo).EstrutAnaRef(FgValRef.row).cod_reag
            EcCodReagente_Validate False
            EcCodGenero = nres(nResActivo).EstrutAnaRef(FgValRef.row).cod_genero
            EcCodGenero_Validate False
            EcObsRef = nres(nResActivo).EstrutAnaRef(FgValRef.row).obs_ref
            If nres(nResActivo).EstrutAnaRef(FgValRef.row).t_ref = "V" Then
                GereValRef True
                GereEscRef False
                FrCutOff.Visible = False
                PreencheValRef nResActivo, FgValRef.row
                BtCutOff.Visible = False
                BtValRef.Visible = True
                BtEscaloes.Visible = True
                BtZonas.Visible = True
            ElseIf nres(nResActivo).EstrutAnaRef(FgValRef.row).t_ref = "E" Then
                GereValRef False
                GereEscRef True
                FrCutOff.Visible = False
                PreencheEscRef nResActivo, FgValRef.row
                BtCutOff.Visible = False
                BtValRef.Visible = True
                BtEscaloes.Visible = True
                BtZonas.Visible = True
            ElseIf nres(nResActivo).EstrutAnaRef(FgValRef.row).t_ref = "C" Then
                GereValRef False
                GereEscRef False
                FrCutOff.Visible = True
                BtCutOff.Visible = True
                BtValRef.Visible = False
                BtEscaloes.Visible = False
                BtZonas.Visible = False
                PreencheCutOff nResActivo, FgValRef.row
            Else
                GereValRef False
                GereEscRef False
            End If
        Else
            EcLinhaAnaRef = ""
            EcSeqAnaRef = ""
            EcDtIni = ""
            EcDtFim = ""
            EcCodMetodo = ""
            EcCodMetodo_Validate False
            EcCodReagente = ""
            EcCodReagente_Validate False
            EcCodGenero = ""
            EcCodGenero_Validate False
            GereValRef False
            GereEscRef False
            EcObsRef = ""
        End If
    End If
End Sub

Private Sub FgValRef_RowColChange()
    FgValRef_Click
End Sub

Private Sub Form_Activate()

    EventoActivate
    If Max <> -1 Then
        BG_BeginTransaction
        
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If
           
End Sub

Private Sub Form_Load()

    EventoLoad

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormCodAna = Nothing
    
    'Para os valores de refer�ncia
    BG_RollbackTransaction
   
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload
    
End Sub

Private Sub OptNRes_Click(Index As Integer)
    If EcCodigo <> "" Then
        nResActivo = Index + 1
    Else
        nResActivo = mediComboValorNull
    End If
    PreencheFgAnaRef nResActivo
End Sub

Private Sub OptUnidade1_Click()
    
    EcUnidadeUso.text = 1

End Sub

Private Sub OptUnidade1Res2_Click()
    EcUnidadeUso2Res.text = 1
End Sub
Private Sub OptUnidade2Res2_Click()
    EcUnidadeUso2Res.text = 2
End Sub
Private Sub OptUnidade2_Click()
    
    EcUnidadeUso.text = 2

End Sub

Private Sub ValidaBotoes()
    
    
End Sub

Private Function VerificaValRef(Cod_Zona As String, TRef As String) As Boolean

    Dim RsVR As ADODB.recordset
    Dim ret As Boolean
    Dim sql As String
    
    ret = False
    
    If TRef = "V" Then
        sql = "SELECT COUNT(*) AS conta FROM sl_val_ref" & _
        " Where cod_ana_ref = " & Cod_Zona & " and n_res=1"
    ElseIf TRef = "E" Then
        sql = "SELECT COUNT(*) AS conta FROM sl_esc_ref WHERE cod_ana_ref = " & Cod_Zona & " and n_res=1"
    ElseIf TRef = "C" Then
        sql = "SELECT COUNT(*) AS conta FROM sl_cut_off WHERE cod_ana_ref = " & Cod_Zona & " and n_res=1"
    Else
        VerificaValRef = ret
        Exit Function
    End If
    
    Set RsVR = New ADODB.recordset
    
    RsVR.CursorLocation = adUseClient
    RsVR.CursorType = adOpenStatic
    RsVR.LockType = adLockReadOnly
    
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsVR.Open sql, gConexao
    
    If RsVR.RecordCount > 0 Then
        If RsVR!conta > 0 Then
            ret = True
        Else
            ret = False
        End If
    Else
        ret = False
    End If
    
    RsVR.Close
    Set RsVR = Nothing

    VerificaValRef = ret

End Function


Private Function ExisteMembros(ByVal codAnalise As String) As Boolean
    
    Dim Registo As New ADODB.recordset
    
    
    Registo.CursorLocation = adUseClient
    Registo.CursorType = adOpenForwardOnly
    Registo.LockType = adLockReadOnly
    Registo.ActiveConnection = gConexao
    Registo.Source = "SELECT Cod_ana_c FROM sl_membro WHERE cod_membro='" & codAnalise & "' AND T_membro='A'"
    If gModoDebug = mediSim Then BG_LogFile_Erros Registo.Source
    Registo.Open
    
    If Registo.RecordCount <> 0 Then
        ExisteMembros = True
    Else
        ExisteMembros = False
    End If
    
    Registo.Close
    Set Registo = Nothing
    
End Function

Private Function ExistePerfis(ByVal codAnalise As String) As Boolean
    
    Dim Registo As New ADODB.recordset
    
    Registo.CursorLocation = adUseClient
    Registo.CursorType = adOpenForwardOnly
    Registo.LockType = adLockReadOnly
    Registo.ActiveConnection = gConexao
    Registo.Source = "SELECT Cod_perfis FROM sl_ana_perfis WHERE cod_analise='" & codAnalise & "'"
    If gModoDebug = mediSim Then BG_LogFile_Erros Registo.Source
    Registo.Open
    
    If Registo.RecordCount <> 0 Then
        ExistePerfis = True
    Else
        ExistePerfis = False
    End If
    
    Registo.Close
    Set Registo = Nothing
    
End Function


Private Sub PreencheInterferencias()
    Dim sSql As String
    Dim rsInter As New ADODB.recordset
    Dim linha As Integer
        
    sSql = "SELECT * FROM sl_ana_interferencias WHERE cod_ana_s = " & BL_TrataStringParaBD(EcCodigo)
    rsInter.CursorType = adOpenStatic
    rsInter.CursorLocation = adUseClient
    rsInter.Open sSql, gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    If rsInter.RecordCount > 0 Then
    
        While Not rsInter.EOF
            AdicionaInterferencia BL_HandleNull(rsInter!formula, ""), BL_HandleNull(rsInter!cod_accao, ""), _
            BL_HandleNull(rsInter!obs, ""), BL_HandleNull(rsInter!obs_desbloqueio, ""), BL_HandleNull(rsInter!seq_interferencia, -1)
            rsInter.MoveNext
        Wend
    End If
    rsInter.Close
    Set rsInter = Nothing
End Sub

Private Function GravaInterferencias() As Boolean
    Dim sSql As String
    Dim linha As Integer
    On Error GoTo TrataErro
    GravaInterferencias = False
    
    sSql = "DELETE FROM sl_ana_interferencias WHERE cod_ana_s = " & BL_TrataStringParaBD(EcCodigo)
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    If gSQLError <> 0 Then
        GoTo TrataErro
    End If

    For linha = 1 To TotalEstrutinterf
        If gSGBD = gOracle Then
            sSql = " INSERT INTO sl_ana_interferencias(cod_ana_s,formula, cod_accao, obs, obs_desbloqueio, seq_interferencia) VALUES(" & BL_TrataStringParaBD(EcCodigo) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutInterf(linha).formula) & ", " & EstrutInterf(linha).cod_accao & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutInterf(linha).obs) & ","
            sSql = sSql & BL_TrataStringParaBD(EstrutInterf(linha).obs_desbloq) & ","
            If EstrutInterf(linha).seq_interf = -1 Then
                sSql = sSql & " seq_interferencia.nextval )"
            Else
                sSql = sSql & EstrutInterf(linha).seq_interf & "  )"
            End If
        Else
            sSql = " INSERT INTO sl_ana_interferencias(cod_ana_s,formula, cod_accao, obs, obs_desbloqueio) VALUES(" & BL_TrataStringParaBD(EcCodigo) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutInterf(linha).formula) & ", " & EstrutInterf(linha).cod_accao & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutInterf(linha).obs) & ","
            sSql = sSql & BL_TrataStringParaBD(EstrutInterf(linha).obs_desbloq) & ")"
        End If
        BG_ExecutaQuery_ADO sSql
        BG_Trata_BDErro
        If gSQLError <> 0 Then
            GoTo TrataErro
        End If
    Next
    GravaInterferencias = True
Exit Function
TrataErro:
    GravaInterferencias = False
    BG_LogFile_Erros "Erro ao gravar Interferencias", Me.Name, "GravaInterferencias", True
    Exit Function
    Resume Next
End Function


Private Sub BtFecharSinonimos_Click()
    EcListaSinonimos.Clear
    FrameSinon.Visible = False
End Sub

Private Sub BtSinonimos_Click()
    FrameSinon.Visible = True
    FrameSinon.left = 240
    FrameSinon.top = 480
    FrameSinon.ZOrder (0)
    FuncaoProcurarSinonimos
End Sub

Private Sub FuncaoProcurarSinonimos()
    Dim sSql As String
    Dim rsSin As New ADODB.recordset
    sSql = "SELECT * FROM sl_ana_sinonimos WHERE cod_ana = " & BL_TrataStringParaBD(EcCodigo)
    sSql = sSql & " AND (flg_original IS NULL OR flg_original = 0) "
    rsSin.CursorType = adOpenStatic
    rsSin.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsSin.Open sSql, gConexao
    TotalEstrutSinonimos = 0
    ReDim EstrutSinonimos(0)
    EcListaSinonimos.Clear
    If rsSin.RecordCount > 0 Then
        While Not rsSin.EOF
            TotalEstrutSinonimos = TotalEstrutSinonimos + 1
            ReDim Preserve EstrutSinonimos(TotalEstrutSinonimos)
            
            EstrutSinonimos(TotalEstrutSinonimos).seq_sinonimo = rsSin!seq_sinonimo
            EstrutSinonimos(TotalEstrutSinonimos).descr_sinonimo = BL_HandleNull(rsSin!descr_sinonimo, "")
            EstrutSinonimos(TotalEstrutSinonimos).flg_original = 0
            EcListaSinonimos.AddItem EstrutSinonimos(TotalEstrutSinonimos).descr_sinonimo
            rsSin.MoveNext
        Wend
    End If
    rsSin.Close
    Set rsSin = Nothing
End Sub



Private Sub EcListaSinonimos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        ApagaSinonimo
    End If
End Sub



Private Sub EcSinonimo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        BtAdicSinonimo_Click
    End If
End Sub

Private Sub ApagaSinonimo()
    Dim sSql As String
    sSql = "DELETE FROM sl_ana_sinonimos WHERE seq_sinonimo =  " & EstrutSinonimos(EcListaSinonimos.ListIndex + 1).seq_sinonimo
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    
    FuncaoProcurarSinonimos
    EcSinonimo = ""
    EcSinonimo.SetFocus

End Sub

Private Sub BtAdicSinonimo_Click()
    Dim sSql As String
    On Error GoTo TrataErro
    
    If gSGBD = gOracle Then
        sSql = "INSERT INTO sl_ana_sinonimos (seq_sinonimo, cod_ana, descr_sinonimo, flg_original) VALUES (seq_sinonimo.nextval, "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", " & BL_TrataStringParaBD(EcSinonimo) & ",0)"
    ElseIf gSGBD = gSqlServer Then
        sSql = "INSERT INTO sl_ana_sinonimos ( cod_ana, descr_sinonimo, flg_original) VALUES ( "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", " & BL_TrataStringParaBD(EcSinonimo) & ",0)"
    End If
    
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    FuncaoProcurarSinonimos
    EcSinonimo = ""
    EcSinonimo.SetFocus
TrataErro:
    BL_LogFile_BD Me.Name, "BtAdicSinonimo_click", Err.Number, Err.Description, ""
    Exit Sub
    Resume Next
End Sub

Function GravaConclRAST() As Boolean
    Dim sql As String
    Dim RsRAST As ADODB.recordset
    On Error GoTo TrataErro
    GravaConclRAST = False
    If CkConclRAST.value = 1 Then
        sql = "Delete from sl_ana_conc where cod_ana_s in (select cod_ana_s from sl_ana_s where rast = 1 and cod_ana_s <> " & BL_TrataStringParaBD(EcCodigo.text) & ")"
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
        If gSQLError <> 0 Then
            GoTo TrataErro
        End If
        
        Set RsRAST = New ADODB.recordset
        sql = "select cod_ana_s from sl_ana_s where rast = 1 and cod_ana_s <> " & BL_TrataStringParaBD(EcCodigo.text)
        RsRAST.CursorType = adOpenStatic
        RsRAST.CursorLocation = adUseClient
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsRAST.Open sql, gConexao
        While Not RsRAST.EOF
            sql = "insert into sl_ana_conc(cod_ana_s,descr_formula,descr_conc) " & _
                    "select " & BL_TrataStringParaBD(RsRAST!cod_ana_s) & ", replace(descr_formula," & BL_TrataStringParaBD(EcCodigo.text) & "," & BL_TrataStringParaBD(RsRAST!cod_ana_s) & "), descr_conc " & _
                    "from sl_ana_conc where cod_ana_s = " & BL_TrataStringParaBD(EcCodigo.text)
            BG_ExecutaQuery_ADO sql
            BG_Trata_BDErro
            
            RsRAST.MoveNext
        Wend
    End If
    GravaConclRAST = True
    Exit Function
TrataErro:
    GravaConclRAST = False
    BG_LogFile_Erros "Erro a gravar conclus�es de RASTS da an�lise " & EcCodigo.text & "!" & Err.Number & " - " & Err.Description
    Exit Function
End Function


Private Sub AdicionaInterferencia(formula As String, cod_accao As Integer, obs As String, obs_desbloqueio As String, seq_interferencia As Long)
    Dim i As Integer
    TotalEstrutinterf = TotalEstrutinterf + 1
    ReDim Preserve EstrutInterf(TotalEstrutinterf)
    
    EstrutInterf(TotalEstrutinterf).formula = formula
    EstrutInterf(TotalEstrutinterf).cod_accao = cod_accao
    EstrutInterf(TotalEstrutinterf).descr_accao = BL_SelCodigo("sl_accoes_interferencias", "DESCR_ACCAO", "COD_ACCAO", cod_accao, "V")
    EstrutInterf(TotalEstrutinterf).obs = obs
    EstrutInterf(TotalEstrutinterf).obs_desbloq = obs_desbloqueio
    EstrutInterf(TotalEstrutinterf).seq_interf = seq_interferencia
    
    FgInterferencias.TextMatrix(TotalEstrutinterf, 0) = EstrutInterf(TotalEstrutinterf).formula
    FgInterferencias.TextMatrix(TotalEstrutinterf, 1) = EstrutInterf(TotalEstrutinterf).seq_interf
    FgInterferencias.TextMatrix(TotalEstrutinterf, 2) = EstrutInterf(TotalEstrutinterf).descr_accao
    FgInterferencias.TextMatrix(TotalEstrutinterf, 3) = EstrutInterf(TotalEstrutinterf).obs
    FgInterferencias.TextMatrix(TotalEstrutinterf, 4) = EstrutInterf(TotalEstrutinterf).obs_desbloq
    
    FgInterferencias.AddItem ""
End Sub


Private Sub BtPesquisaGrupo_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    If EcCodLocal <> "" Then
        ClausulaWhere = "cod_local = " & gCodLocal
    End If
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupo de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrupo.text = Resultados(1)
            EcDescrGrupo.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub
Private Sub BtPesquisaGrImpr_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_impr"
    CamposEcran(1) = "seq_gr_impr"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_impr"
    CamposEcran(2) = "descr_gr_impr"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_impr"
    CampoPesquisa1 = "descr_gr_impr"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupo de Impress�o")
    
    mensagem = "N�o foi encontrada nenhum Grupo de Impress�o."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrImpr.text = Resultados(1)
            EcDescrGrImpr.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesquisasubGrupo_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_sgr_ana"
    CamposEcran(1) = "cod_sgr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_sgr_ana"
    CamposEcran(2) = "descr_sgr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_sgr_ana"
    CampoPesquisa1 = "descr_sgr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar SubGrupo de An�lises")
    
    mensagem = "N�o foi encontrada nenhum SubGrupo de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodSubGrupo.text = Resultados(1)
            EcDescrSubGrupo.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub



Private Sub BtPesquisaclasse_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_classe_ana"
    CamposEcran(1) = "cod_classe_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_classe_ana"
    CamposEcran(2) = "descr_classe_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_classe_ana"
    CampoPesquisa1 = "descr_classe_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Classe de An�lises")
    
    mensagem = "N�o foi encontrada nenhuma classe de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodClasse.text = Resultados(1)
            EcDescrClasse.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesquisaProduto_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_produto"
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_produto"
    CampoPesquisa1 = "descr_produto"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Produto")
    
    mensagem = "N�o foi encontrado nenhum produto."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProd.text = Resultados(1)
            EcDescrProduto.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub


Private Sub BtPesquisaTubop_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_Tubo"
    CamposEcran(1) = "cod_Tubo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_Tubo"
    CamposEcran(2) = "descr_Tubo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_Tubo"
    CampoPesquisa1 = "descr_Tubo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tubo")
    
    mensagem = "N�o foi encontrado nenhum Tubo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodTuboP.text = Resultados(1)
            EcDescrTuboP.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub



Private Sub BtPesquisaTubos_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_Tubo"
    CamposEcran(1) = "cod_Tubo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_Tubo"
    CamposEcran(2) = "descr_Tubo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_Tubo_sec"
    CampoPesquisa1 = "descr_Tubo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Tubo")
    
    mensagem = "N�o foi encontrado nenhum Tubo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodTuboS.text = Resultados(1)
            EcDescrTuboS.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub
Private Sub BtPesquisametodo_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_metodo"
    CamposEcran(1) = "cod_metodo"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_metodo"
    CamposEcran(2) = "descr_metodo"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_metodo"
    CampoPesquisa1 = "descr_metodo"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar M�todo")
    
    mensagem = "N�o foi encontrado nenhum M�todo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMetodo.text = Resultados(1)
            EcDescrMetodo.text = Resultados(2)
            If EcLinhaAnaRef <> "" And IsNumeric(EcLinhaAnaRef) And nResActivo > mediComboValorNull Then
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cod_metodo = EcCodMetodo
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesquisaReagente_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_reagent"
    CamposEcran(1) = "cod_reagent"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_reagent"
    CamposEcran(2) = "descr_reagent"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_reagent"
    CampoPesquisa1 = "descr_reagent"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Reagente")
    
    mensagem = "N�o foi encontrado nenhum Reagente."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodReagente.text = Resultados(1)
            EcDescrReagente.text = Resultados(2)
            If EcLinhaAnaRef <> "" And IsNumeric(EcLinhaAnaRef) And nResActivo > mediComboValorNull Then
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cod_reag = EcCodReagente
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub
Private Sub BtPesquisagenero_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_genero"
    CamposEcran(1) = "cod_genero"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_genero"
    CamposEcran(2) = "descr_genero"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_genero"
    CampoPesquisa1 = "descr_genero"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar G�neros")
    
    mensagem = "N�o foi encontrado nenhum G�nero."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGenero.text = Resultados(1)
            EcDescrGenero.text = Resultados(2)
            If EcLinhaAnaRef <> "" And IsNumeric(EcLinhaAnaRef) And nResActivo > mediComboValorNull Then
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).cod_genero = EcCodGenero
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

' ------------------------------------------------------

' LIMPA TODOS CAMPOS RELATIVOS AOS VALORES DE REFERENCIA

' ------------------------------------------------------
Private Sub LimpaAnaRef()
    Dim i  As Long
    On Error GoTo TrataErro
    
    FgValRef.rows = 2
    FgValRef.row = 1
    For i = 0 To FgValRef.Cols - 1
        FgValRef.TextMatrix(1, i) = ""
    Next
    EcLinhaAnaRef = ""
    EcSeqAnaRef = ""
    EcDtFim = ""
    EcDtIni = ""
    EcCodMetodo = ""
    EcDescrMetodo = ""
    EcCodReagente = ""
    EcDescrReagente = ""
    FgValRef.row = 1
    GereValRef False
    GereEscRef False
    FgEscRef.rows = 2
    FgEscRef.row = 1
    For i = 0 To FgEscRef.Cols - 1
        FgEscRef.TextMatrix(1, i) = ""
    Next
    EcCodMetodo = ""
    EcDescrMetodo = ""
    EcObsRef = ""
    EcCodGenero = ""
    EcCodGenero_Validate False
    EcCutOffPMax.text = ""
    EcCutOffPMin.text = ""
    EcCutOffNMax.text = ""
    EcCutOffNMin.text = ""
    EcCutOffDMax.text = ""
    EcCutOffDMin.text = ""
    CbPMax.ListIndex = -1
    CbPMin.ListIndex = -1
    CbNMin.ListIndex = -1
    CbNMax.ListIndex = -1
    CbDMin.ListIndex = -1
    CbDMax.ListIndex = -1
    EcComPos.text = ""
    EcComNeg.text = ""
    EcComDuv.text = ""
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a limpar AnaRef ", Me.Name, "LimpaAnaRef"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------

' LIMPA TODOS CAMPOS RELATIVOS AOS VALORES DE REFERENCIA

' ------------------------------------------------------
Private Sub LimpaPrecos()
    Dim i  As Long
    On Error GoTo TrataErro
    
    FgPrecos.rows = 2
    FgPrecos.row = 1
    For i = 0 To FgPrecos.Cols - 1
        FgPrecos.TextMatrix(1, i) = ""
    Next
    TotalPrec = 0
    ReDim EstrutPrec(0)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a limpar FgPrecos ", Me.Name, "LimpaPrecos"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------

' LIMPA CONCLUSOES

' ------------------------------------------------------
Private Sub LimpaConcl()
    Dim i  As Long
    On Error GoTo TrataErro
    
    FgConc.rows = 2
    FgConc.row = 1
    For i = 0 To FgConc.Cols - 1
        FgConc.TextMatrix(1, i) = ""
    Next
    FgConc.row = 1
    TotalConcl = 0
    ReDim EstrutConcl(0)
    EcConcDescr = ""
    EcConcFormula = ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a limpar Conclus�es ", Me.Name, "LimpaConcl"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------

' PREENCHE DADOS RELATIVOS A TABLEA SL_ANA_REF

' ------------------------------------------------------
Private Sub PreencheAnaRef()
    Dim sSql As String
    Dim rsAnaRef As New ADODB.recordset
    Dim indiceNRes As Integer
    
    nres(1).TotalAnaRef = 0
    ReDim nres(1).EstrutAnaRef(0)
    nres(2).TotalAnaRef = 0
    ReDim nres(2).EstrutAnaRef(0)
    OptNRes(0).value = True
    OptNRes_Click 0
    LimpaAnaRef
    sSql = "SELECT * FROM sl_ana_ref WHERE  cod_ana_s =" & BL_TrataStringParaBD(EcCodigo)
    rsAnaRef.CursorType = adOpenStatic
    rsAnaRef.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaRef.Open sSql, gConexao
    GereEscRef False
    If rsAnaRef.RecordCount > 0 Then
        While Not rsAnaRef.EOF
            indiceNRes = BL_HandleNull(rsAnaRef!N_Res, 1)
            
            nres(indiceNRes).TotalAnaRef = nres(indiceNRes).TotalAnaRef + 1
            ReDim Preserve nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef)
            nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).cod_metodo = BL_HandleNull(rsAnaRef!cod_metod, "")
            nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).cod_reag = BL_HandleNull(rsAnaRef!cod_reag, "")
            nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).cod_genero = BL_HandleNull(rsAnaRef!cod_genero, "")
            nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).dt_valid_inf = BL_HandleNull(rsAnaRef!dt_valid_inf, "")
            nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).dt_valid_sup = BL_HandleNull(rsAnaRef!dt_valid_sup, "")
            nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).N_Res = BL_HandleNull(rsAnaRef!N_Res, "1")
            nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).seq_ana_ref = BL_HandleNull(rsAnaRef!seq_ana_ref, "")
            nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).t_ref = BL_HandleNull(rsAnaRef!t_ref, "")
            nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).obs_ref = BL_HandleNull(rsAnaRef!obs_ref, "")
            nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).totalEscRef = 0
            ReDim nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).EscRef(0)
            
            If nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).t_ref = "V" Then
                PreencheValRef indiceNRes, nres(indiceNRes).TotalAnaRef
            ElseIf nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).t_ref = "E" Then
                PreencheEscRef indiceNRes, nres(indiceNRes).TotalAnaRef
            ElseIf nres(indiceNRes).EstrutAnaRef(nres(indiceNRes).TotalAnaRef).t_ref = "C" Then
                PreencheCutOff indiceNRes, nres(indiceNRes).TotalAnaRef
            Else
            End If
            
            
            rsAnaRef.MoveNext
        Wend
        GereEscRef False
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Preencher Val/Esc Refer�ncia ", Me.Name, "PreencheAnaRef"
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheFgAnaRef(ByVal indiceNRes As Integer)
    On Error GoTo TrataErro
    Dim i As Integer
    LimpaAnaRef
    If indiceNRes > 0 Then
        For i = 1 To nres(indiceNRes).TotalAnaRef
            FgValRef.TextMatrix(i, lColValRefDataInf) = nres(indiceNRes).EstrutAnaRef(i).dt_valid_inf
            FgValRef.TextMatrix(i, lColValRefDataSup) = nres(indiceNRes).EstrutAnaRef(i).dt_valid_sup
            FgValRef.TextMatrix(i, lColValRefTipo) = nres(indiceNRes).EstrutAnaRef(i).t_ref
            FgValRef.AddItem ""
        Next
        If nres(indiceNRes).TotalAnaRef > 0 Then
            FgValRef.row = nres(indiceNRes).TotalAnaRef
            FgValRef_Click
        Else
            FgValRef.row = 1
        End If
    End If
        

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Preencher PreencheFgAnaRef ", Me.Name, "PreencheFgAnaRef"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------

' PREENCHE DADOS RELATIVOS AS CONCLUSOES

' ------------------------------------------------------
Private Sub PreencheConcl()
    Dim sSql As String
    Dim rsAnaConc As New ADODB.recordset
    LimpaConcl
    sSql = "SELECT * FROM sl_ana_conc WHERE cod_ana_s =" & BL_TrataStringParaBD(EcCodigo)
    rsAnaConc.CursorType = adOpenStatic
    rsAnaConc.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaConc.Open sSql, gConexao
    If rsAnaConc.RecordCount > 0 Then
        While Not rsAnaConc.EOF
            TotalConcl = TotalConcl + 1
            ReDim Preserve EstrutConcl(TotalConcl)
            EstrutConcl(TotalConcl).formula = BL_HandleNull(rsAnaConc!descr_formula, "")
            EstrutConcl(TotalConcl).conclusao = BL_HandleNull(rsAnaConc!descr_conc, "")
            
            FgConc.TextMatrix(TotalConcl, lColConcFormula) = EstrutConcl(TotalConcl).formula
            FgConc.TextMatrix(TotalConcl, lColConcDescr) = EstrutConcl(TotalConcl).conclusao
            FgConc.AddItem ""
            
            rsAnaConc.MoveNext
        Wend
        FgConc.row = 1
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Preencher Conclus�es ", Me.Name, "PreencheConcl"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------

' PREENCHE DADOS RELATIVOS AS CONCLUSOES

' ------------------------------------------------------
Private Function GravaConcl() As Boolean
    Dim sSql As String
    Dim i As Long
    On Error GoTo TrataErro
    GravaConcl = False
    sSql = "DELETE FROM sl_ana_conc WHERE cod_ana_s = " & BL_TrataStringParaBD(EcCodigo)
    BG_ExecutaQuery_ADO sSql
    For i = 1 To TotalConcl
        sSql = "INSERT INTO sl_ana_conc (cod_ana_s, descr_formula, descr_conc) VALUES("
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ","
        sSql = sSql & BL_TrataStringParaBD(EstrutConcl(i).formula) & ","
        sSql = sSql & BL_TrataStringParaBD(EstrutConcl(i).conclusao) & ")"
        BG_ExecutaQuery_ADO sSql
    Next
    GravaConcl = True
Exit Function
TrataErro:
    GravaConcl = False
    BG_LogFile_Erros "Erro a Gravar Conclus�es ", Me.Name, "GravaConcl"
    Exit Function
    Resume Next
End Function

Sub Funcao_DataActual()
    
    If Me.ActiveControl.Name = EcDtIni.Name Then
        BL_PreencheData EcDtIni, Me.ActiveControl
        EcDtini_Validate False
    Else
        If Me.ActiveControl.Name = EcDtFim.Name Then
            BL_PreencheData EcDtFim, Me.ActiveControl
            EcDtFim_Validate False
        End If
    End If
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtIni)
    If Cancel Or EcDtIni = "" Then
        EcDtIni = ""
        Sendkeys ("{HOME}+{END}")
        EcDtIni.SetFocus
    Else

        If FgValRef.row <= nres(nResActivo).TotalAnaRef Then
            If EcLinhaAnaRef <> "" And IsNumeric(EcLinhaAnaRef) Then
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).dt_valid_inf = EcDtIni
                FgValRef.TextMatrix(EcLinhaAnaRef, lColValRefDataInf) = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).dt_valid_inf
            End If
        End If
    End If

End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtFim)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    Else
        If FgValRef.row <= nres(nResActivo).TotalAnaRef Then
            If EcLinhaAnaRef <> "" And IsNumeric(EcLinhaAnaRef) Then
                nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).dt_valid_sup = EcDtFim
                FgValRef.TextMatrix(EcLinhaAnaRef, lColValRefDataSup) = nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).dt_valid_sup
            End If
        End If
    End If

End Sub

' ------------------------------------------------------

' GRAVA VALORES NA TABELA SL_ANA_REF

' ------------------------------------------------------
Private Function GravaAnaRef(indice As Long) As Boolean
    Dim i As Integer
    Dim j As Integer
    Dim sSql As String
    Dim Max As Long
    On Error GoTo TrataErro
    GravaAnaRef = False
    For j = 1 To 2
        For i = 1 To nres(j).TotalAnaRef
            If indice > 0 Then
                i = indice
            End If
            
            If nres(j).EstrutAnaRef(i).seq_ana_ref <> "" Then
                sSql = "DELETE FROM SL_ANA_REF WHERE cod_ana_S = " & BL_TrataStringParaBD(EcCodigo)
                sSql = sSql & " AND seq_ana_ref =" & nres(j).EstrutAnaRef(i).seq_ana_ref
                BG_ExecutaQuery_ADO sSql
            Else
                Max = BG_DaMAX("SL_ANA_REF", "SEQ_ANA_REF") + 1
                nres(j).EstrutAnaRef(i).seq_ana_ref = Max
            End If
            sSql = "INSERT INTO SL_ANA_REF (seq_ana_ref, cod_ana_S,n_res,dt_valid_inf, dt_valid_sup,cod_reag,cod_metod,t_ref, cod_genero, OBS_REF) VALUES("
            sSql = sSql & nres(j).EstrutAnaRef(i).seq_ana_ref & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", "
            sSql = sSql & BL_HandleNull(nres(j).EstrutAnaRef(i).N_Res, 1) & ", "
            sSql = sSql & BL_TrataDataParaBD(nres(j).EstrutAnaRef(i).dt_valid_inf) & ", "
            sSql = sSql & BL_TrataDataParaBD(nres(j).EstrutAnaRef(i).dt_valid_sup) & ", "
            sSql = sSql & BL_TrataStringParaBD(nres(j).EstrutAnaRef(i).cod_reag) & ", "
            sSql = sSql & BL_TrataStringParaBD(nres(j).EstrutAnaRef(i).cod_metodo) & ", "
            sSql = sSql & BL_TrataStringParaBD(nres(j).EstrutAnaRef(i).t_ref) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(nres(j).EstrutAnaRef(i).cod_genero, gCodGeneroDefeito)) & ", "
            sSql = sSql & BL_TrataStringParaBD(nres(j).EstrutAnaRef(i).obs_ref) & ") "
            BG_ExecutaQuery_ADO sSql
            If nres(j).EstrutAnaRef(i).t_ref = "V" Then
                If GravaValRef(j, i) = False Then
                    GoTo TrataErro
                End If
            ElseIf nres(j).EstrutAnaRef(i).t_ref = "E" Then
                If GravaEscRef(j, i) = False Then
                    GoTo TrataErro
                End If
            ElseIf nres(j).EstrutAnaRef(i).t_ref = "C" Then
                If GravaCutOff(j, i) = False Then
                    GoTo TrataErro
                End If
            Else
                sSql = "DELETE FROM SL_VAL_REF WHERE COD_ANA_REF = " & nres(j).EstrutAnaRef(i).seq_ana_ref
                BG_ExecutaQuery_ADO sSql
                sSql = "DELETE FROM SL_ESC_REF WHERE COD_ANA_REF = " & nres(j).EstrutAnaRef(i).seq_ana_ref
                BG_ExecutaQuery_ADO sSql
            End If
            If indice > 0 Then
                Exit For
            End If
        Next i
    Next j
    GravaAnaRef = True
Exit Function
TrataErro:
    GravaAnaRef = False
    BG_LogFile_Erros "Erro a Gravar Ana Refer�ncia ", Me.Name, "GravaAnaRef"
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------

' GRAVA VALORES NA TABELA SL_VAL_REF

' ------------------------------------------------------
Private Function GravaValRef(indiceNRes As Integer, indiceAnaRef As Integer) As Boolean
    Dim i As Long
    Dim sSql As String
    Dim Max As Long
    On Error GoTo TrataErro
    GravaValRef = False
    If nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref <> "" Then
        sSql = "DELETE FROM SL_VAL_REF WHERE COD_ANA_REF = " & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref
        BG_ExecutaQuery_ADO sSql
        sSql = "DELETE FROM SL_ESC_REF WHERE COD_ANA_REF = " & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref
        BG_ExecutaQuery_ADO sSql
    End If
    sSql = "INSERT INTO SL_VAL_REF (cod_ana_ref, n_res,h_ref_min,h_ref_max,f_ref_min,f_ref_max,com_mah, com_mih, com_maf, com_mif) VALUES("
    sSql = sSql & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref & ", "
    sSql = sSql & BL_HandleNull(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).N_Res, 1) & ", "
    sSql = sSql & BL_TrataStringParaBD(Trim(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.h_ref_min)) & ", "
    sSql = sSql & BL_TrataStringParaBD(Trim(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.h_ref_max)) & ", "
    sSql = sSql & BL_TrataStringParaBD(Trim(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.f_ref_min)) & ", "
    sSql = sSql & BL_TrataStringParaBD(Trim(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.f_ref_max)) & ", "
    sSql = sSql & BL_TrataStringParaBD(Trim(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.com_mah)) & ", "
    sSql = sSql & BL_TrataStringParaBD(Trim(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.com_mih)) & ", "
    sSql = sSql & BL_TrataStringParaBD(Trim(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.com_maf)) & ", "
    sSql = sSql & BL_TrataStringParaBD(Trim(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.com_mif)) & ")"
    BG_ExecutaQuery_ADO sSql
    GravaValRef = True
Exit Function
TrataErro:
    GravaValRef = False
    BG_LogFile_Erros "Erro a Gravar Valores Refer�ncia ", Me.Name, "GravaValRef"
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------

' GRAVA VALORES NA TABELA SL_CUT_OFF

' ------------------------------------------------------
Private Function GravaCutOff(indiceNRes As Integer, indiceAnaRef As Integer) As Boolean
    Dim i As Long
    Dim sSql As String
    Dim Max As Long
    On Error GoTo TrataErro
    GravaCutOff = False
    If nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref <> "" Then
        sSql = "DELETE FROM sl_cut_off WHERE cod_ana_ref=" & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref
        BG_ExecutaQuery_ADO sSql
    End If
    
    sSql = "INSERT INTO sl_cut_off (cod_ana_ref, seq_cut_off, val_neg_min, val_neg_max, t_comp_nmin, t_comp_nmax, val_pos_min, val_pos_max, "
    sSql = sSql & " t_comp_pmin, t_comp_pmax, val_duv_min, val_duv_max, t_comp_dmin, t_comp_dmax, com_neg, com_pos, com_duv)  VALUES( "
    sSql = sSql & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref & ", "
    sSql = sSql & BG_DaMAX("sl_cut_off", "seq_cut_off") + 1 & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.val_neg_min) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.val_neg_max) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.t_comp_nmin) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.t_comp_nmax) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.val_pos_min) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.val_pos_max) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.t_comp_pmin) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.t_comp_pmax) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.val_duv_min) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.val_duv_max) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.t_comp_dmin) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.t_comp_dmax) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.com_neg) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.com_pos) & ", "
    sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.com_duv) & ") "
    BG_ExecutaQuery_ADO sSql
    GravaCutOff = True

Exit Function
TrataErro:
    GravaCutOff = False
    BG_LogFile_Erros "Erro a Gravar CutOff ", Me.Name, "GravaCutOff"
    Exit Function
    Resume Next
End Function


' ------------------------------------------------------

' PREENCHE DADOS RELATIVOS A TABLEA SL_VAL_REF

' ------------------------------------------------------
Private Sub PreencheValRef(indiceNRes As Integer, indiceAnaRef As Long)
    Dim sSql As String
    Dim rsAnaRef As New ADODB.recordset
    If nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref = "" Then Exit Sub
    
    sSql = "SELECT * FROM sl_val_ref WHERE n_res = " & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).N_Res & "  AND cod_ana_ref =" & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref
    rsAnaRef.CursorType = adOpenStatic
    rsAnaRef.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaRef.Open sSql, gConexao
    If rsAnaRef.RecordCount = 1 Then
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.com_maf = BL_HandleNull(rsAnaRef!com_maf, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.com_mah = BL_HandleNull(rsAnaRef!com_mah, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.com_mif = BL_HandleNull(rsAnaRef!com_mif, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.com_mih = BL_HandleNull(rsAnaRef!com_mih, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.f_ref_max = BL_HandleNull(rsAnaRef!f_ref_max, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.f_ref_min = BL_HandleNull(rsAnaRef!f_ref_min, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.h_ref_max = BL_HandleNull(rsAnaRef!h_ref_max, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.h_ref_min = BL_HandleNull(rsAnaRef!h_ref_min, "")
        
        EcMinFem = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.f_ref_min
        EcMinMasc = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.h_ref_min
        EcMaxFem = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.f_ref_max
        EcMaxMasc = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.h_ref_max
        EcComMaF = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.com_maf
        EcComMaH = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.com_mah
        EcComMiF = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.com_mif
        EcComMiH = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).valoresRef.com_mih
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Preencher Valores Refer�ncia ", Me.Name, "PreencheValRef"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------

' PREENCHE DADOS RELATIVOS A TABLEA SL_CUT_OFF

' ------------------------------------------------------
Private Sub PreencheCutOff(indiceNRes As Integer, indiceAnaRef As Long)
    Dim sSql As String
    Dim rsAnaRef As New ADODB.recordset
    If nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref = "" Then Exit Sub
    
    sSql = "SELECT * FROM sl_cut_off WHERE cod_ana_ref =" & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref
    rsAnaRef.CursorType = adOpenStatic
    rsAnaRef.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaRef.Open sSql, gConexao
    If rsAnaRef.RecordCount = 1 Then
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.cod_ana_ref = BL_HandleNull(rsAnaRef!cod_ana_ref, -1)
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.com_duv = BL_HandleNull(rsAnaRef!com_duv, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.com_neg = BL_HandleNull(rsAnaRef!com_neg, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.com_pos = BL_HandleNull(rsAnaRef!com_pos, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.seq_cut_off = BL_HandleNull(rsAnaRef!seq_cut_off, -1)
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.t_comp_dmax = BL_HandleNull(rsAnaRef!t_comp_dmax, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.t_comp_dmin = BL_HandleNull(rsAnaRef!t_comp_dmin, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.t_comp_nmax = BL_HandleNull(rsAnaRef!t_comp_nmax, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.t_comp_nmin = BL_HandleNull(rsAnaRef!t_comp_nmin, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.t_comp_pmax = BL_HandleNull(rsAnaRef!t_comp_pmax, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.t_comp_pmin = BL_HandleNull(rsAnaRef!t_comp_pmin, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.val_duv_max = BL_HandleNull(rsAnaRef!val_duv_max, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.val_duv_min = BL_HandleNull(rsAnaRef!val_duv_min, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.val_neg_max = BL_HandleNull(rsAnaRef!val_neg_max, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.val_neg_min = BL_HandleNull(rsAnaRef!val_neg_min, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.val_pos_max = BL_HandleNull(rsAnaRef!val_pos_max, "")
        nres(indiceNRes).EstrutAnaRef(indiceAnaRef).cutoff.val_pos_min = BL_HandleNull(rsAnaRef!val_pos_min, "")
        
        EcCutOffPMin.text = BL_HandleNull(rsAnaRef!val_pos_min, "")
        EcCutOffPMax.text = BL_HandleNull(rsAnaRef!val_pos_max, "")
        EcCutOffNMin.text = BL_HandleNull(rsAnaRef!val_neg_min, "")
        EcCutOffNMax.text = BL_HandleNull(rsAnaRef!val_neg_max, "")
        EcCutOffDMin.text = BL_HandleNull(rsAnaRef!val_duv_min, "")
        EcCutOffDMax.text = BL_HandleNull(rsAnaRef!val_duv_max, "")
        If BL_HandleNull(rsAnaRef!t_comp_pmin, "") <> "" Then
            CbPMin = rsAnaRef!t_comp_pmin
        Else
            CbPMin.ListIndex = -1
        End If
        If BL_HandleNull(rsAnaRef!t_comp_pmax, "") <> "" Then
            CbPMax = rsAnaRef!t_comp_pmax
        Else
            CbPMax.ListIndex = -1
        End If
        If BL_HandleNull(rsAnaRef!t_comp_nmin, "") <> "" Then
            CbNMin = rsAnaRef!t_comp_nmin
        Else
            CbNMin.ListIndex = -1
        End If
        If BL_HandleNull(rsAnaRef!t_comp_nmax, "") <> "" Then
            CbNMax = rsAnaRef!t_comp_nmax
        Else
            CbNMax.ListIndex = -1
        End If
        If BL_HandleNull(rsAnaRef!t_comp_dmin, "") <> "" Then
            CbDMin = rsAnaRef!t_comp_dmin
        Else
            CbDMin.ListIndex = -1
        End If
        If BL_HandleNull(rsAnaRef!t_comp_dmax, "") <> "" Then
            CbDMax = rsAnaRef!t_comp_dmax
        Else
            CbDMax.ListIndex = -1
        End If
        EcComPos.text = BL_HandleNull(rsAnaRef!com_pos, "")
        EcComNeg.text = BL_HandleNull(rsAnaRef!com_neg, "")
        EcComDuv.text = BL_HandleNull(rsAnaRef!com_duv, "")
    Else
        EcCutOffPMax.text = ""
        EcCutOffPMin.text = ""
        EcCutOffNMax.text = ""
        EcCutOffNMin.text = ""
        EcCutOffDMax.text = ""
        EcCutOffDMin.text = ""
        CbPMax.ListIndex = -1
        CbPMin.ListIndex = -1
        CbNMin.ListIndex = -1
        CbNMax.ListIndex = -1
        CbDMin.ListIndex = -1
        CbDMax.ListIndex = -1
        EcComPos.text = ""
        EcComNeg.text = ""
        EcComDuv.text = ""
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Preencher CutOff ", Me.Name, "PreencheCutOff"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------

' PREENCHE DADOS RELATIVOS A TABLEA SL_ESC_REF

' ------------------------------------------------------
Private Sub PreencheEscRef(indiceNRes As Integer, indiceAnaRef As Long)
    Dim sSql As String
    Dim total As Long
    Dim i As Long
    Dim rsAnaRef As New ADODB.recordset
    FgEscRef.rows = 2
    FgEscRef.row = 1
    For i = 0 To FgEscRef.Cols - 1
        FgEscRef.TextMatrix(1, i) = ""
    Next
    
    If nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref = "" Then Exit Sub
    nres(indiceNRes).EstrutAnaRef(indiceAnaRef).totalEscRef = 0
    ReDim Preserve nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total)
    
    sSql = "SELECT * FROM sl_esc_ref WHERE cod_ana_ref =" & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref
    sSql = sSql & " ORDER BY esc_inf_dias ASC, seq_esc_ref ASC "
    rsAnaRef.CursorType = adOpenStatic
    rsAnaRef.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaRef.Open sSql, gConexao
    If rsAnaRef.RecordCount >= 1 Then
        While Not rsAnaRef.EOF
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).totalEscRef = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).totalEscRef + 1
            total = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).totalEscRef
            ReDim Preserve nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total)
            
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).seq_esc_ref = BL_HandleNull(rsAnaRef!seq_esc_ref, "")
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_inf = BL_HandleNull(rsAnaRef!t_esc_inf, "")
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).esc_inf = BL_HandleNull(rsAnaRef!esc_inf, "")
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).esc_inf_dias = BL_HandleNull(rsAnaRef!esc_inf_dias, "")
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_sup = BL_HandleNull(rsAnaRef!t_esc_sup, "")
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).esc_sup = BL_HandleNull(rsAnaRef!esc_sup, "")
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).esc_sup_dias = BL_HandleNull(rsAnaRef!esc_sup_dias, "")
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).h_ref_min = BL_HandleNull(rsAnaRef!h_ref_min, "")
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).h_ref_max = BL_HandleNull(rsAnaRef!h_ref_max, "")
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).f_ref_min = BL_HandleNull(rsAnaRef!f_ref_min, "")
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).f_ref_max = BL_HandleNull(rsAnaRef!f_ref_max, "")
            
            FgEscRef.TextMatrix(total, lColEscRefEscInf) = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).esc_inf
            
            If nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_inf = lCodDias Then
                FgEscRef.TextMatrix(total, lColEscRefTEscInf) = "Dias"
            ElseIf nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_inf = lCodMeses Then
                FgEscRef.TextMatrix(total, lColEscRefTEscInf) = "Meses"
            ElseIf nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_inf = lCodAnos Then
                FgEscRef.TextMatrix(total, lColEscRefTEscInf) = "Anos"
            ElseIf nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_inf = lCodCrianca Then
                FgEscRef.TextMatrix(total, lColEscRefTEscInf) = "Crianca"
            ElseIf nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_inf = lCodAdulto Then
                FgEscRef.TextMatrix(total, lColEscRefTEscInf) = "Adulto"
            End If
            'FgEscRef.TextMatrix(total, lColEscRefTEscInf) = BL_SelCodigo("SL_TBF_T_DATA", "DESCR_T_DATA", "COD_T_DATA", nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_inf, "V")
            FgEscRef.TextMatrix(total, lColEscRefEscSup) = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).esc_sup
            If nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_sup = lCodDias Then
                FgEscRef.TextMatrix(total, lColEscRefTEscSup) = "Dias"
            ElseIf nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_sup = lCodMeses Then
                FgEscRef.TextMatrix(total, lColEscRefTEscSup) = "Meses"
            ElseIf nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_sup = lCodAnos Then
                FgEscRef.TextMatrix(total, lColEscRefTEscSup) = "Anos"
            ElseIf nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_sup = lCodCrianca Then
                FgEscRef.TextMatrix(total, lColEscRefTEscSup) = "Crianca"
            ElseIf nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_sup = lCodAdulto Then
                FgEscRef.TextMatrix(total, lColEscRefTEscSup) = "Adulto"
            End If
            'FgEscRef.TextMatrix(total, lColEscRefTEscSup) = BL_SelCodigo("SL_TBF_T_DATA", "DESCR_T_DATA", "COD_T_DATA", nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).t_esc_sup, "V")
            FgEscRef.TextMatrix(total, lColEscRefHRefMin) = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).h_ref_min
            FgEscRef.TextMatrix(total, lColEscRefHRefMax) = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).h_ref_max
            FgEscRef.TextMatrix(total, lColEscRefFRefMin) = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).f_ref_min
            FgEscRef.TextMatrix(total, lColEscRefFRefMax) = nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(total).f_ref_max
            
            FgEscRef.AddItem ""
            rsAnaRef.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Preencher Escal�es Refer�ncia ", Me.Name, "PreencheEscRef"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------

' GRAVA VALORES NA TABELA SL_ESC_REF

' ------------------------------------------------------
Private Function GravaEscRef(indiceNRes As Integer, indiceAnaRef As Integer) As Boolean
    Dim i As Long
    Dim sSql As String
    Dim Max As Long
    On Error GoTo TrataErro
    GravaEscRef = False
    
    sSql = "DELETE FROM SL_VAL_REF WHERE COD_ANA_REF = " & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref
    BG_ExecutaQuery_ADO sSql
    sSql = "DELETE FROM SL_ESC_REF WHERE cod_ana_ref IN (select seq_ana_ref from sl_ana_ref where seq_ana_ref = " & (nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref) & ")"
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To nres(indiceNRes).EstrutAnaRef(indiceAnaRef).totalEscRef
        If nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).seq_esc_ref = "" Then
            Max = BG_DaMAX("SL_ESC_REF", "SEQ_ESC_REF") + 1
            nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).seq_esc_ref = Max
        End If
        
        sSql = "INSERT INTO SL_ESC_REF (seq_esc_ref, cod_ana_ref, n_res, t_esc_inf,esc_inf, esc_inf_dias ,t_esc_sup ,esc_sup, esc_sup_dias ,h_ref_min,h_ref_max ,f_ref_min ,f_ref_max ) VALUES("
        sSql = sSql & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).seq_esc_ref & ","
        sSql = sSql & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).seq_ana_ref & ", "
        sSql = sSql & BL_HandleNull(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).N_Res, 1) & ", "
        sSql = sSql & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).t_esc_inf & ","
        sSql = sSql & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).esc_inf & ","
        sSql = sSql & CLng(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).esc_inf_dias) & ","
        sSql = sSql & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).t_esc_sup & ","
        sSql = sSql & nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).esc_sup & ","
        sSql = sSql & CLng(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).esc_sup_dias) & ","
        sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).h_ref_min) & ", "
        sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).h_ref_max) & ", "
        sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).f_ref_min) & ", "
        sSql = sSql & BL_TrataStringParaBD(nres(indiceNRes).EstrutAnaRef(indiceAnaRef).EscRef(i).f_ref_max) & ") "
        BG_ExecutaQuery_ADO sSql
    Next
    GravaEscRef = True
Exit Function
TrataErro:
    GravaEscRef = False
    BG_LogFile_Erros "Erro a Gravar Escal�es Refer�ncia ", Me.Name, "GravaEscRef"
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------

' COLOCA VISIVEL/INVISIVEL GRELHA DOS ESCALOES REF

' ------------------------------------------------------
Private Sub GereEscRef(EscRef As Boolean)
    FgEscRef.Visible = EscRef
    
    BtAdiciona.Visible = EscRef
    BtRemove.Visible = EscRef

End Sub

' ------------------------------------------------------

' COLOCA VISIVEL/INVISIVEL CAMPOS DOS VALORES REF

' ------------------------------------------------------
Private Sub GereValRef(ValRef As Boolean)
    LbMasc.Visible = ValRef
    LbFem.Visible = ValRef
    LbMinF.Visible = ValRef
    LbMinM.Visible = ValRef
    LbMaxF.Visible = ValRef
    LbMaxM.Visible = ValRef
    LbCmMaxF.Visible = ValRef
    LbCmMaxM.Visible = ValRef
    LbCmMinF.Visible = ValRef
    LbCmMinM.Visible = ValRef
    EcMinFem.Visible = ValRef
    EcMinMasc.Visible = ValRef
    EcMaxFem.Visible = ValRef
    EcMaxMasc.Visible = ValRef
    EcComMaF.Visible = ValRef
    EcComMaH.Visible = ValRef
    EcComMiF.Visible = ValRef
    EcComMiH.Visible = ValRef
    If ValRef = False Then
        EcMinFem = ""
        EcMinMasc = ""
        EcMaxFem = ""
        EcMaxMasc = ""
        EcComMaF = ""
        EcComMaH = ""
        EcComMiF = ""
        EcComMiH = ""
    End If
End Sub


Private Sub EcConcFormula_validate(Cancel As Boolean)
    If EcLinhaConc <> "" Then
        If IsNumeric(EcLinhaConc) Then
            If EcLinhaConc > TotalConcl Then
                TotalConcl = TotalConcl + 1
                ReDim Preserve EstrutConcl(EcLinhaConc)
                FgConc.AddItem ""
            End If
            EstrutConcl(EcLinhaConc).formula = EcConcFormula
            FgConc.TextMatrix(EcLinhaConc, lColConcFormula) = EcConcFormula
        End If
    Else
        EcLinhaConc = TotalConcl + 1
        If EcLinhaConc > TotalConcl Then
            TotalConcl = TotalConcl + 1
            ReDim Preserve EstrutConcl(EcLinhaConc)
            FgConc.AddItem ""
        End If
        EstrutConcl(EcLinhaConc).formula = EcConcFormula
        FgConc.TextMatrix(EcLinhaConc, lColConcFormula) = EcConcFormula
    End If

End Sub
Private Sub EcConcdescr_validate(Cancel As Boolean)
    If EcLinhaConc <> "" Then
        If IsNumeric(EcLinhaConc) Then
            If EcLinhaConc > TotalConcl Then
                TotalConcl = TotalConcl + 1
                ReDim Preserve EstrutConcl(EcLinhaConc)
                FgConc.AddItem ""
            End If
            EstrutConcl(EcLinhaConc).conclusao = EcConcDescr
            FgConc.TextMatrix(EcLinhaConc, lColConcDescr) = EcConcDescr
        End If
    Else
        EcLinhaConc = TotalConcl + 1
        If EcLinhaConc > TotalConcl Then
            TotalConcl = TotalConcl + 1
            ReDim Preserve EstrutConcl(EcLinhaConc)
            FgConc.AddItem ""
        End If
        EstrutConcl(EcLinhaConc).formula = EcConcFormula
        FgConc.TextMatrix(EcLinhaConc, lColConcFormula) = EcConcFormula
    End If
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    If SSTab1.Tab <> lTabValRef Then
        GereEscRef False
        GereValRef False
    ElseIf SSTab1 = lTabValRef Then
        If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
            If IsNumeric(EcLinhaAnaRef) Then
                If nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).t_ref = "V" Then
                    GereValRef True
                ElseIf nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).t_ref = "E" Then
                    GereEscRef True
                End If
            End If
        End If
    End If
        
End Sub

' ------------------------------------------------------

' PREENCHE DADOS DE FACTURACAO

' ------------------------------------------------------
Private Sub PreencheFacturacao()
    Dim sSql As String
    Dim RsFact As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT * FROM sl_ana_facturacao WHERE cod_ana = " & BL_TrataStringParaBD(EcCodigo) & " AND cod_efr IS NULL "
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    RsFact.CursorType = adOpenStatic
    RsFact.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsFact.Open sSql, gConexao
    If RsFact.RecordCount >= 1 Then
        EcRubrSeqAna = BL_HandleNull(RsFact!seq_ana, "")
        EcCodRubrica = BL_HandleNull(RsFact!cod_ana_gh, "")
        EcCodRubrica_Validate False
        
        If BL_HandleNull(RsFact!flg_conta_membros, "0") = mediSim Then
            CkContaMembros.value = vbChecked
        Else
            CkContaMembros.value = vbUnchecked
        End If
        
        If BL_HandleNull(RsFact!flg_ana_facturar, "0") = mediSim Then
            CkFlgfacturarAna.value = vbChecked
        Else
            CkFlgfacturarAna.value = vbUnchecked
        End If
        EcCodAnaFacturar = BL_HandleNull(RsFact!cod_ana_facturar, "")
        
        If BL_HandleNull(RsFact!Flg_Regra, "0") = mediSim Then
            CkFlgRegra.value = vbChecked
        Else
            CkFlgRegra.value = vbUnchecked
        End If
        EcCodAnaRegra = BL_HandleNull(RsFact!Cod_Ana_Regra, "")
        EcQtd = BL_HandleNull(RsFact!qtd, "")
    End If
    RsFact.Close
    Set RsFact = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a  Preencher Factura��o ", Me.Name, "PreencheFacturacao"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------

' GRAVA DADOS DE FACTURA��O

' ------------------------------------------------------

Private Function GravaFacturacao() As Boolean
    Dim sSql As String
    Dim FlgfacturarAna As Integer
    Dim flgRegra As Integer
    Dim ContaMembros As Integer
    Dim rsPortaria As New ADODB.recordset
    Dim portariaAtiba As Integer
    On Error GoTo TrataErro
    
    GravaFacturacao = False
    If CkFlgfacturarAna.value = vbChecked Then
        FlgfacturarAna = 1
    Else
        FlgfacturarAna = 0
    End If
    If CkFlgRegra.value = vbChecked Then
        flgRegra = 1
    Else
        flgRegra = 0
    End If
    If CkContaMembros.value = vbChecked Then
        ContaMembros = 1
    Else
        ContaMembros = 0
    End If
    
    If EcRubrSeqAna <> "" And EcCodRubrica.text = "" Then
        sSql = "DELETE FROM sl_ana_facturacao WHERE seq_ana = " & EcRubrSeqAna
        BG_ExecutaQuery_ADO sSql
    ElseIf EcCodRubrica <> "" Then
        sSql = " SELECT x2.cod_portaria FROM sl_portarias x2 Where " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN dt_ini"
        sSql = sSql & " AND NVL (dt_fim,'31-12-2099')"
        rsPortaria.CursorType = adOpenStatic
        rsPortaria.CursorLocation = adUseClient
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsPortaria.Open sSql, gConexao
        If rsPortaria.RecordCount <> 1 Then
            GravaFacturacao = False
            Exit Function
        Else
            portariaAtiba = BL_HandleNull(rsPortaria!cod_portaria, mediComboValorNull)
        End If
        rsPortaria.Close
        Set rsPortaria = Nothing
        If EcRubrSeqAna.text = "" Then
            EcRubrSeqAna = BG_DaMAX("sl_ana_facturacao", "seq_ana") + 1
            sSql = "INSERT INTO Sl_ana_facturacao (seq_ana, cod_ana, descr_ana,cod_ana_gh, descr_ana_Facturacao,flg_regra, "
            sSql = sSql & " cod_ana_regra, flg_conta_membros, flg_ana_facturar, cod_ana_facturar,qtd, cod_portaria, user_cri, dt_Cri, hr_cri  ) VALUES( "
            sSql = sSql & EcRubrSeqAna & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", "
            sSql = sSql & BL_TrataStringParaBD(EcDesignacao) & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodRubrica) & ", "
            sSql = sSql & BL_TrataStringParaBD(EcDescrRubrica) & ", "
            sSql = sSql & flgRegra & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodAnaRegra) & ", "
            sSql = sSql & ContaMembros & ", "
            sSql = sSql & FlgfacturarAna & ", "
            sSql = sSql & BL_TrataStringParaBD(EcCodAnaFacturar) & ","
            sSql = sSql & BL_HandleNull(EcQtd, "NULL") & ", "
            sSql = sSql & portariaAtiba & ","
            sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ") "
            BG_ExecutaQuery_ADO sSql
        Else
            sSql = "UPDATE Sl_ana_facturacao SET cod_ana_gh  = " & BL_TrataStringParaBD(EcCodRubrica) & ","
            sSql = sSql & " flg_regra  = " & flgRegra & ", "
            sSql = sSql & " cod_ana_regra  = " & BL_TrataStringParaBD(EcCodAnaRegra) & ", "
            sSql = sSql & " flg_conta_membros  = " & ContaMembros & ", "
            sSql = sSql & " flg_ana_facturar  = " & FlgfacturarAna & ", "
            sSql = sSql & " cod_ana_facturar  = " & BL_TrataStringParaBD(EcCodAnaFacturar.text) & ", "
            sSql = sSql & " qtd  = " & BL_HandleNull(EcQtd, "NULL") & ", "
            sSql = sSql & " cod_portaria  = " & portariaAtiba & ", "
            sSql = sSql & " user_act  = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ", "
            sSql = sSql & " dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", "
            sSql = sSql & " hr_act = " & BL_TrataStringParaBD(Bg_DaHora_ADO) & " WHERE  seq_ana = " & EcRubrSeqAna.text
            BG_ExecutaQuery_ADO sSql
        End If
    End If
    GravaFacturacao = True
Exit Function
TrataErro:
    GravaFacturacao = False
    BG_LogFile_Erros "Erro a Gravar Factura��o ", Me.Name, "GravaFacturacao"
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------

' CARREGA PRECARIOS

' ------------------------------------------------------
Private Sub PreenchePrecarios()
    Dim sSql As String
    Dim RsFact As New ADODB.recordset
    Dim rsPrRubr As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM fa_portarias where DT_FIM IS NULL ORDER BY cod_efr, dt_ini"
    RsFact.CursorType = adOpenStatic
    RsFact.CursorLocation = adUseClient
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsFact.Open sSql, gConexaoSecundaria
    If RsFact.RecordCount >= 1 Then
        While Not RsFact.EOF
            TotalPrec = TotalPrec + 1
            ReDim Preserve EstrutPrec(TotalPrec)
            
            EstrutPrec(TotalPrec).cod_rubr = EcCodRubrica
            EstrutPrec(TotalPrec).cod_efr = BL_HandleNull(RsFact!cod_efr, "")
            EstrutPrec(TotalPrec).Portaria = BL_HandleNull(RsFact!Portaria, "")
            EstrutPrec(TotalPrec).descr_Portaria = BL_HandleNull(RsFact!descr_Port, "")
            EstrutPrec(TotalPrec).empresa_id = BL_HandleNull(RsFact!empresa_id, "")
            EstrutPrec(TotalPrec).flg_modificado = False
            EstrutPrec(TotalPrec).valor_c = IF_RetornaValorC(EstrutPrec(TotalPrec).Portaria)
            
            FgPrecos.TextMatrix(TotalPrec, lColPrecosCodPrecario) = EstrutPrec(TotalPrec).Portaria
            FgPrecos.TextMatrix(TotalPrec, lColPrecosDescrPrecario) = EstrutPrec(TotalPrec).descr_Portaria
            
            If EcCodRubrica <> "" Then
                sSql = "SELECT * FROM fa_pr_rubr WHERE cod_rubr = " & EcCodRubrica & " AND portaria = " & EstrutPrec(TotalPrec).Portaria

                rsPrRubr.CursorType = adOpenStatic
                rsPrRubr.CursorLocation = adUseClient
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsPrRubr.Open sSql, gConexaoSecundaria
                If rsPrRubr.RecordCount = 1 Then
                    EstrutPrec(TotalPrec).cod_rubr_efr = BL_HandleNull(rsPrRubr!cod_rubr_efr, "")
                    EstrutPrec(TotalPrec).descr_rubr_efr = BL_HandleNull(rsPrRubr!descr_rubr_efr, "")
                    EstrutPrec(TotalPrec).nr_c = BL_HandleNull(rsPrRubr!nr_c, "")
                    EstrutPrec(TotalPrec).nr_k = BL_HandleNull(rsPrRubr!nr_k, "")
                    EstrutPrec(TotalPrec).perc_pag_doe = BL_HandleNull(rsPrRubr!perc_pag_doe, "")
                    EstrutPrec(TotalPrec).t_fac = BL_HandleNull(rsPrRubr!t_fac, "")
                    EstrutPrec(TotalPrec).val_pag_doe = BL_HandleNull(rsPrRubr!val_pag_doe, "")
                    EstrutPrec(TotalPrec).val_pag_ent = BL_HandleNull(rsPrRubr!val_pag_ent, "")
                    EstrutPrec(TotalPrec).val_Taxa = BL_HandleNull(rsPrRubr!val_Taxa, "")
                    EstrutPrec(TotalPrec).user_cri = BL_HandleNull(rsPrRubr!user_cri, "")
                    EstrutPrec(TotalPrec).user_act = BL_HandleNull(rsPrRubr!user_act, "")
                    EstrutPrec(TotalPrec).dt_cri = BL_HandleNull(rsPrRubr!dt_cri, "")
                    EstrutPrec(TotalPrec).dt_act = BL_HandleNull(rsPrRubr!dt_act, "")
                    
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosCodRubrEFR) = EstrutPrec(TotalPrec).cod_rubr_efr
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosDescrRubrEFR) = EstrutPrec(TotalPrec).descr_rubr_efr
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosNumC) = EstrutPrec(TotalPrec).nr_c
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosNumK) = EstrutPrec(TotalPrec).nr_k
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosPercDoente) = EstrutPrec(TotalPrec).perc_pag_doe
                    
                    If EstrutPrec(TotalPrec).t_fac = "1" Then
                        FgPrecos.TextMatrix(TotalPrec, lColPrecosTipoFact) = "KS E CS"
                    ElseIf EstrutPrec(TotalPrec).t_fac = "2" Then
                        FgPrecos.TextMatrix(TotalPrec, lColPrecosTipoFact) = "EUROS"
                    End If
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosValPagDoe) = EstrutPrec(TotalPrec).val_pag_doe
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosValPagEFR) = EstrutPrec(TotalPrec).val_pag_ent
                    FgPrecos.TextMatrix(TotalPrec, lColPrecosTxModeradora) = EstrutPrec(TotalPrec).val_Taxa
                End If
                rsPrRubr.Close
            End If
            FgPrecos.AddItem ""
            RsFact.MoveNext
        Wend
        RsFact.Close
        Set RsFact = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Carregar Precarios", Me.Name, "PreenchePrecarios"
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------

' GRAVA DADOS DA TABELA DE PRECOS

' ------------------------------------------------------

Private Sub GravaPrecarios()
    Dim sSql As String
    Dim i As Long
    On Error GoTo TrataErro
    
    gConexaoSecundaria.BeginTrans
    
    For i = 1 To TotalPrec
    
        If EstrutPrec(i).flg_modificado = True And EstrutPrec(i).cod_rubr <> "" Then
            sSql = "DELETE FROM fa_pr_rubr WHERE portaria = " & EstrutPrec(i).Portaria & " AND cod_rubr = " & EstrutPrec(i).cod_rubr
            gConexaoSecundaria.Execute sSql
        
            If EstrutPrec(i).cod_rubr_efr <> "" Or EstrutPrec(i).nr_c <> "" Or EstrutPrec(i).nr_k <> "" Or EstrutPrec(i).perc_pag_doe <> "" _
                    Or EstrutPrec(i).val_pag_doe <> "" Or EstrutPrec(i).val_pag_ent <> "" Or EstrutPrec(i).val_pag_doe <> "" _
                    Or EstrutPrec(i).val_Taxa <> "" Or EstrutPrec(i).perc_pag_doe <> "" Then
                    
                sSql = "INSERT INTO fa_pr_rubr (cod_rubr, portaria, cod_Efr, cod_rubr_efr, descr_rubr_efr, val_taxa, t_fac, nr_c, nr_k, "
                sSql = sSql & " val_pag_ent, perc_pag_doe, val_pag_doe,user_cri, dt_cri, user_act, dt_act ) VALUES ("
                sSql = sSql & EstrutPrec(i).cod_rubr & ", "
                sSql = sSql & EstrutPrec(i).Portaria & ", "
                sSql = sSql & EstrutPrec(i).cod_efr & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).cod_rubr_efr) & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).descr_rubr_efr) & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).val_Taxa, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).t_fac, ",", "."), 2) & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).nr_c, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).nr_k, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).val_pag_ent, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).perc_pag_doe, ",", "."), "NULL") & ", "
                sSql = sSql & BL_HandleNull(Replace(EstrutPrec(i).val_pag_doe, ",", "."), "NULL") & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).user_cri) & ", "
                sSql = sSql & BL_TrataDataParaBD(EstrutPrec(i).dt_cri) & ", "
                sSql = sSql & BL_TrataStringParaBD(EstrutPrec(i).user_act) & ", "
                sSql = sSql & BL_TrataDataParaBD(EstrutPrec(i).dt_act) & ") "
                gConexaoSecundaria.Execute sSql
            End If
            
        End If
    Next
    gConexaoSecundaria.CommitTrans
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a Gravar Pre�arios ", Me.Name, "GravaPrecarios"
    gConexaoSecundaria.RollbackTrans
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' INDICA SE O NUM DIAS DO ESCAL�O REF SUPERIORES SAO MAIORES Q OS INF

' ----------------------------------------------------------------------
Private Function AvaliaIntervaloEscRef() As Boolean
    On Error GoTo TrataErro
    AvaliaIntervaloEscRef = True
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If CDbl(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_inf_dias, 0)) >= CDbl(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_sup_dias, 0)) Then
            If CDbl(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_inf_dias, 0)) >= 1 Then
                If CDbl(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).esc_sup_dias, 0)) >= 1 Then
                    BG_Mensagem mediMsgBox, "Intervalo Superior deve ser maior que o Infervalo Inferior.", vbExclamation, "Escal�o de Refer�ncia"
                    AvaliaIntervaloEscRef = False
                End If
            End If
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro a avaliar intervalo Esc. Ref. ", Me.Name, "AvaliaIntervaloEscRef"
    AvaliaIntervaloEscRef = True
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' INDICA SE Valores do  ESCAL�O REF SUPERIORES SAO MAIORES Q OS INF

' ----------------------------------------------------------------------
Private Function AvaliaValoresEscRef(tipo As String) As Boolean
    On Error GoTo TrataErro
    AvaliaValoresEscRef = True
    If EcLinhaAnaRef <> "" And nResActivo > mediComboValorNull Then
        If tipo = "H" Then
            If CDbl(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).h_ref_min, 0)) >= CDbl(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).h_ref_max, 0)) Then
                If CDbl(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).h_ref_min, 0)) >= 1 Then
                    If CDbl(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).h_ref_max, 0)) >= 1 Then
                        BG_Mensagem mediMsgBox, "Valor Superior deve ser maior que o valor Inferior.", vbExclamation, "Escal�o de Refer�ncia"
                        AvaliaValoresEscRef = False
                    End If
                End If
            End If
        ElseIf tipo = "F" Then
            If CDbl(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).f_ref_min, 0)) >= CDbl(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).f_ref_max, 0)) Then
                If CDbl(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).f_ref_min, 0)) >= 1 Then
                    If CDbl(BL_HandleNull(nres(nResActivo).EstrutAnaRef(EcLinhaAnaRef).EscRef(EcLinhaEscRef).f_ref_max, 0)) >= 1 Then
                        BG_Mensagem mediMsgBox, "Valor Superior deve ser maior que o valor Inferior.", vbExclamation, "Escal�o de Refer�ncia"
                        AvaliaValoresEscRef = False
                    End If
                End If
            End If
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Erro a avaliar valores Esc. Ref. ", Me.Name, "AvaliaValoresEscRef"
    AvaliaValoresEscRef = True
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' COPIA CODIFICA��ES DE OUTRA AN�LISE

' ----------------------------------------------------------------------
Private Sub CopiaRubrica(rubrica As String)
    Dim sSql As String
    Dim rsPrRubr As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    
    If EcCodRubrica <> "" Then
    
        ' APAGA AS PARAMETRIZACOES EXISTENTES
        For i = 1 To TotalPrec
           EstrutPrec(i).cod_rubr_efr = -1
           EstrutPrec(i).descr_rubr_efr = ""
           EstrutPrec(i).nr_c = ""
           EstrutPrec(i).nr_k = ""
           EstrutPrec(i).perc_pag_doe = ""
           EstrutPrec(i).t_fac = ""
           EstrutPrec(i).val_pag_doe = ""
           EstrutPrec(i).val_pag_ent = ""
           EstrutPrec(i).val_Taxa = ""
           EstrutPrec(i).user_cri = ""
           EstrutPrec(i).user_act = ""
           EstrutPrec(i).dt_cri = ""
           EstrutPrec(i).dt_act = ""
           EstrutPrec(i).flg_modificado = True
    
           FgPrecos.TextMatrix(i, lColPrecosCodRubrEFR) = EstrutPrec(i).cod_rubr_efr
           FgPrecos.TextMatrix(i, lColPrecosDescrRubrEFR) = EstrutPrec(i).descr_rubr_efr
           FgPrecos.TextMatrix(i, lColPrecosNumC) = EstrutPrec(i).nr_c
           FgPrecos.TextMatrix(i, lColPrecosNumK) = EstrutPrec(i).nr_k
           FgPrecos.TextMatrix(i, lColPrecosPercDoente) = EstrutPrec(i).perc_pag_doe
           FgPrecos.TextMatrix(i, lColPrecosTipoFact) = ""
           FgPrecos.TextMatrix(i, lColPrecosValPagDoe) = EstrutPrec(i).val_pag_doe
           FgPrecos.TextMatrix(i, lColPrecosValPagEFR) = EstrutPrec(i).val_pag_ent
           FgPrecos.TextMatrix(i, lColPrecosTxModeradora) = EstrutPrec(i).val_Taxa
           Exit For
        Next
        sSql = "SELECT * FROM fa_pr_rubr WHERE cod_rubr = " & rubrica
        rsPrRubr.CursorType = adOpenStatic
        rsPrRubr.CursorLocation = adUseClient
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsPrRubr.Open sSql, gConexaoSecundaria
        If rsPrRubr.RecordCount >= 1 Then
            While Not rsPrRubr.EOF
                For i = 1 To TotalPrec
                    If BL_HandleNull(rsPrRubr!Portaria, "") = EstrutPrec(i).Portaria Then
                        EstrutPrec(i).cod_rubr_efr = BL_HandleNull(rsPrRubr!cod_rubr_efr, "")
                        EstrutPrec(i).descr_rubr_efr = EcDescrRubrica
                        EstrutPrec(i).nr_c = BL_HandleNull(rsPrRubr!nr_c, "")
                        EstrutPrec(i).nr_k = BL_HandleNull(rsPrRubr!nr_k, "")
                        EstrutPrec(i).perc_pag_doe = BL_HandleNull(rsPrRubr!perc_pag_doe, "")
                        EstrutPrec(i).t_fac = BL_HandleNull(rsPrRubr!t_fac, "")
                        EstrutPrec(i).val_pag_doe = BL_HandleNull(rsPrRubr!val_pag_doe, "")
                        EstrutPrec(i).val_pag_ent = BL_HandleNull(rsPrRubr!val_pag_ent, "")
                        EstrutPrec(i).val_Taxa = BL_HandleNull(rsPrRubr!val_Taxa, "")
                        EstrutPrec(i).user_cri = BL_HandleNull(rsPrRubr!user_cri, "")
                        EstrutPrec(i).user_act = BL_HandleNull(rsPrRubr!user_act, "")
                        EstrutPrec(i).dt_cri = BL_HandleNull(rsPrRubr!dt_cri, "")
                        EstrutPrec(i).dt_act = BL_HandleNull(rsPrRubr!dt_act, "")
                        EstrutPrec(i).flg_modificado = True
                 
                        FgPrecos.TextMatrix(i, lColPrecosCodRubrEFR) = EstrutPrec(i).cod_rubr_efr
                        FgPrecos.TextMatrix(i, lColPrecosDescrRubrEFR) = EstrutPrec(i).descr_rubr_efr
                        FgPrecos.TextMatrix(i, lColPrecosNumC) = EstrutPrec(i).nr_c
                        FgPrecos.TextMatrix(i, lColPrecosNumK) = EstrutPrec(i).nr_k
                        FgPrecos.TextMatrix(i, lColPrecosPercDoente) = EstrutPrec(i).perc_pag_doe
                        If BL_HandleNull(rsPrRubr!t_fac, "") = "1" Then
                            FgPrecos.TextMatrix(i, lColPrecosTipoFact) = "KS E CS"
                        ElseIf BL_HandleNull(rsPrRubr!t_fac, "") = "2" Then
                            FgPrecos.TextMatrix(i, lColPrecosTipoFact) = "EUROS"
                        End If
                        FgPrecos.TextMatrix(i, lColPrecosValPagDoe) = EstrutPrec(i).val_pag_doe
                        FgPrecos.TextMatrix(i, lColPrecosValPagEFR) = EstrutPrec(i).val_pag_ent
                        FgPrecos.TextMatrix(i, lColPrecosTxModeradora) = EstrutPrec(i).val_Taxa
                        Exit For
                    End If
                Next
                rsPrRubr.MoveNext
            Wend
        End If
        rsPrRubr.Close
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Copiar R�brica", Me.Name, "CopiaRubrica"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' APAGA DADOS QUE N�O INTERESSA IMPORTAR DE OUTRAS ANALISES

' ----------------------------------------------------------------------
Private Sub ApagaDadosAna()
    On Error GoTo TrataErro
    ApagaDadosAnaFacturacao
    ApagaDadosAnaInterferencias
    ApagaDadosAnaValRef
    ApagaDadosAnaFolhasTrab
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Apagar Dados de An�lise", Me.Name, "ApagaDadosAna"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' APAGA DADOS QUE N�O INTERESSA IMPORTAR DE OUTRAS ANALISES FACT

' ----------------------------------------------------------------------
Private Sub ApagaDadosAnaFacturacao()
    On Error GoTo TrataErro
    EcRubrSeqAna = ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Apagar Dados de An�lise Factura��o", Me.Name, "ApagaDadosAnaFacturacao"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' APAGA DADOS QUE N�O INTERESSA IMPORTAR DE OUTRAS ANALISES INTERF

' ----------------------------------------------------------------------
Private Sub ApagaDadosAnaInterferencias()
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To TotalEstrutinterf
        EstrutInterf(i).seq_interf = -1
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Apagar Dados de An�lise Interf", Me.Name, "ApagaDadosAnaInterferencias"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' APAGA DADOS QUE N�O INTERESSA IMPORTAR DE OUTRAS ANALISES VAL REF

' ----------------------------------------------------------------------
Private Sub ApagaDadosAnaValRef()
    Dim i As Integer
    Dim j As Integer
    On Error GoTo TrataErro
    
    For i = 1 To nres(nResActivo).TotalAnaRef
        nres(nResActivo).EstrutAnaRef(i).seq_ana_ref = ""
        For j = 1 To nres(nResActivo).EstrutAnaRef(i).totalEscRef
            nres(nResActivo).EstrutAnaRef(i).EscRef(j).seq_esc_ref = ""
        Next
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Apagar Dados de An�lise Val Ref", Me.Name, "ApagaDadosAnaValRef"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' APAGA DADOS QUE N�O INTERESSA IMPORTAR DE OUTRAS ANALISES - FOLHAS TRAB

' ----------------------------------------------------------------------
Private Sub ApagaDadosAnaFolhasTrab()
    Dim i As Integer
    Dim j As Integer
    On Error GoTo TrataErro
    
    For i = 0 To EcListaFolhasTrab.ListCount - 1
        If EcListaFolhasTrab.Selected(i) = True Then
            gFolhasTrab(i + 1).ordem = -1
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro ao Apagar Dados de An�lise Folhas Trab", Me.Name, "ApagaDadosAnaFolhasTrab"
    Exit Sub
    Resume Next
End Sub


' ----------------------------------------------------------------------

' GRAVA SINONIMO IGUAL A DESCRICAO

' ----------------------------------------------------------------------
Private Function GravaSinonimo() As Boolean
    Dim sSql As String
    On Error GoTo TrataErro
    GravaSinonimo = False
    sSql = "DELETE FROM sl_ana_sinonimos WHERE cod_ana = " & BL_TrataStringParaBD(EcCodigo) & " AND flg_original = 1 "
    BG_ExecutaQuery_ADO sSql
    
    If gSGBD = gOracle Then
        sSql = "INSERT INTO sl_ana_sinonimos (seq_sinonimo, cod_ana, descr_sinonimo, flg_original) VALUES (seq_sinonimo.nextval, "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", " & BL_TrataStringParaBD(EcDesignacao) & ",1)"
    ElseIf gSGBD = gSqlServer Then
        sSql = "INSERT INTO sl_ana_sinonimos ( cod_ana, descr_sinonimo, flg_original) VALUES ( "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", " & BL_TrataStringParaBD(EcDesignacao) & ",1)"
    End If
    
    BG_ExecutaQuery_ADO sSql
    GravaSinonimo = True
Exit Function
TrataErro:
    GravaSinonimo = False
    BG_LogFile_Erros "Erro ao Gravar Sinonimos", Me.Name, "GravaSinonimo"
    Exit Function
    Resume Next
End Function


Private Sub CbPMin_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbPMin.ListIndex = mediComboValorNull
    End If
End Sub
Private Sub CbPMax_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbPMax.ListIndex = mediComboValorNull
    End If
End Sub
Private Sub CbnMin_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbNMin.ListIndex = mediComboValorNull
    End If
End Sub
Private Sub CbnMax_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbNMax.ListIndex = mediComboValorNull
    End If
End Sub
Private Sub CbdMin_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbDMin.ListIndex = mediComboValorNull
    End If
End Sub
Private Sub CbdMax_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CbDMax.ListIndex = mediComboValorNull
    End If
End Sub


Private Function GravaSegundoResultado() As Boolean
    On Error GoTo TrataErro
    Dim iReg As Integer
    Dim sSql As String
    GravaSegundoResultado = False
    
    
    sSql = "DELETE FROM " & NomeTabela2Res & " WHERE cod_ana_s =" & BL_TrataStringParaBD(EcCodigo.text)
    BG_ExecutaQuery_ADO sSql
    
    If CkSegundoRes.value = vbChecked Then
        sSql = BG_ConstroiCriterio_INSERT_ADO(NomeTabela2Res, CamposBD2Res, CamposEc2Res)
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg <= 0 Then
            GoTo TrataErro
        End If
    End If
    GravaSegundoResultado = True
Exit Function
TrataErro:
    GravaSegundoResultado = False
    BG_LogFile_Erros "Erro ao Gravar Segundo Resultado", Me.Name, "GravaSegundoResultado", True
    Exit Function
    Resume Next
End Function

VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form FormEstatTempoColheita 
   BackColor       =   &H00FEC6BA&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEstatTempoColheita"
   ClientHeight    =   7920
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   8805
   Icon            =   "FormEstatTempoColheita.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7920
   ScaleWidth      =   8805
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameImpr 
      BackColor       =   &H00FEC6BA&
      Height          =   855
      Left            =   240
      TabIndex        =   22
      Top             =   6960
      Width           =   8415
      Begin VB.CheckBox CkSupDetalhes 
         BackColor       =   &H00FEC6BA&
         Caption         =   "Suprimir Detalhes"
         Height          =   255
         Left            =   5400
         TabIndex        =   26
         Top             =   240
         Width           =   1695
      End
      Begin VB.CheckBox CkAgruparPrioridade 
         BackColor       =   &H00FEC6BA&
         Caption         =   "Agrupar Prioridade"
         Height          =   255
         Left            =   3480
         TabIndex        =   25
         Top             =   240
         Width           =   1695
      End
      Begin VB.CheckBox CkAgruparSala 
         BackColor       =   &H00FEC6BA&
         Caption         =   "Agrupar Sala"
         Height          =   255
         Left            =   2040
         TabIndex        =   24
         Top             =   240
         Width           =   1695
      End
      Begin VB.CheckBox CkAgruparOperador 
         BackColor       =   &H00FEC6BA&
         Caption         =   "Agrupar Operador"
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.OptionButton OptTipo 
      BackColor       =   &H00FEC6BA&
      Caption         =   "Quantidade Colheitas"
      Height          =   375
      Index           =   1
      Left            =   3840
      TabIndex        =   21
      Top             =   720
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.OptionButton OptTipo 
      BackColor       =   &H00FEC6BA&
      Caption         =   "Tempo M�dio Colheitas"
      Height          =   375
      Index           =   0
      Left            =   1080
      TabIndex        =   20
      Top             =   720
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Frame FrameAgrup 
      BackColor       =   &H00FEC6BA&
      Height          =   1935
      Left            =   7200
      TabIndex        =   15
      Top             =   0
      Width           =   1575
      Begin VB.OptionButton OptAgrupamento 
         BackColor       =   &H00FEC6BA&
         Caption         =   "Por dia"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   19
         Top             =   240
         Width           =   975
      End
      Begin VB.OptionButton OptAgrupamento 
         BackColor       =   &H00FEC6BA&
         Caption         =   "Por M�s"
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   18
         Top             =   960
         Width           =   975
      End
      Begin VB.OptionButton OptAgrupamento 
         BackColor       =   &H00FEC6BA&
         Caption         =   "Por ano"
         Height          =   195
         Index           =   2
         Left            =   240
         TabIndex        =   17
         Top             =   1320
         Width           =   975
      End
      Begin VB.OptionButton OptAgrupamento 
         BackColor       =   &H00FEC6BA&
         Caption         =   "Por Hora"
         Height          =   195
         Index           =   3
         Left            =   240
         TabIndex        =   16
         Top             =   600
         Width           =   975
      End
   End
   Begin VB.CommandButton BtPesquisaOperador 
      Height          =   315
      Left            =   6720
      Picture         =   "FormEstatTempoColheita.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
      Top             =   1320
      Width           =   375
   End
   Begin VB.CommandButton BtPesquisaSala 
      Height          =   315
      Left            =   6720
      Picture         =   "FormEstatTempoColheita.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   13
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
      Top             =   1800
      Width           =   375
   End
   Begin VB.ComboBox CbPrioridades 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1200
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   2160
      Width           =   1935
   End
   Begin VB.TextBox EcDescrSala 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1920
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   1800
      Width           =   4575
   End
   Begin VB.TextBox EcDescrOperador 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1920
      Locked          =   -1  'True
      TabIndex        =   10
      Top             =   1320
      Width           =   4575
   End
   Begin VB.TextBox EcCodSala 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      Height          =   285
      Left            =   1200
      TabIndex        =   9
      Top             =   1800
      Width           =   735
   End
   Begin VB.TextBox EcCodOperador 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      Height          =   285
      Left            =   1200
      TabIndex        =   8
      Top             =   1320
      Width           =   735
   End
   Begin MSChart20Lib.MSChart Grafico 
      Height          =   4335
      Left            =   240
      OleObjectBlob   =   "FormEstatTempoColheita.frx":0B20
      TabIndex        =   0
      Top             =   2640
      Width           =   8415
   End
   Begin MSComCtl2.DTPicker EcDtInicio 
      Height          =   255
      Left            =   2040
      TabIndex        =   1
      Top             =   240
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   450
      _Version        =   393216
      Format          =   145620993
      CurrentDate     =   39694
   End
   Begin MSComCtl2.DTPicker EcDtFim 
      Height          =   255
      Left            =   4680
      TabIndex        =   2
      Top             =   240
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   450
      _Version        =   393216
      Format          =   145620993
      CurrentDate     =   39694
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FEC6BA&
      Caption         =   "Prioridade"
      Height          =   255
      Index           =   4
      Left            =   360
      TabIndex        =   7
      Top             =   2160
      Width           =   735
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FEC6BA&
      Caption         =   "Sala"
      Height          =   255
      Index           =   3
      Left            =   360
      TabIndex        =   6
      Top             =   1800
      Width           =   735
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FEC6BA&
      Caption         =   "Operador"
      Height          =   255
      Index           =   2
      Left            =   360
      TabIndex        =   5
      Top             =   1320
      Width           =   735
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FEC6BA&
      Caption         =   "Data Final"
      Height          =   255
      Index           =   1
      Left            =   3720
      TabIndex        =   4
      Top             =   240
      Width           =   855
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FEC6BA&
      Caption         =   "Data Inicial"
      Height          =   255
      Index           =   0
      Left            =   1080
      TabIndex        =   3
      Top             =   240
      Width           =   855
   End
End
Attribute VB_Name = "FormEstatTempoColheita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Private Type DadosReq
    n_req As String
    cod_sala As String
    prioridade As String
    dt_cri As String
    hr_cri As String
    dt_act As String
    hr_act As String
    user_cri As String
    user_act As String
End Type

Dim estrutRequis() As DadosReq
Dim totalRequis As Long

Private Type dados
    Label As String
    soma As Double
    media As Double
    NReq As Long
End Type
Dim estrutGrafico() As dados
Dim TotalGrafico As Long

Const lAgrupDia = 0
Const lAgrupHora = 3
Const lAgrupMes = 1
Const lAgrupAno = 2


Private Sub BtPesquisaSala_Click()
    PA_PesquisaSala EcCodSala, EcDescrSala, ""
End Sub

Private Sub BtPesquisaOperador_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_idutilizador", _
                        "nome", "cod_utilizador", _
                        EcCodOperador, EcDescrOperador
End Sub

Public Sub EcCodsala_Validate(Cancel As Boolean)
        
    Cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
End Sub
Public Sub EcCodOperador_Validate(Cancel As Boolean)
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodOperador.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT nome FROM sl_idutilizador WHERE cod_utilizador='" & Trim(EcCodOperador.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodOperador.Text = ""
            EcDescrOperador.Text = ""
        Else
            EcDescrOperador.Text = Tabela!nome
        End If
       
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrOperador.Text = ""
    End If
    
End Sub
Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = "Estat�stica de Colheitas"
    Me.left = 5
    Me.top = 5
    Me.Width = 9795
    Me.Height = 8310
    Set CampoDeFocus = EcDtInicio
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito

    BG_ParametrizaPermissoes_ADO Me.Name

    estado = 0
    BG_StackJanelas_Push Me

End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "InActivo"
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"

    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0


    Set FormEstatTempoColheita = Nothing

End Sub

Sub LimpaCampos()
    Me.SetFocus

    EcDtInicio.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    Grafico.Visible = False
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
End Sub

Sub DefTipoCampos()

    totalRequis = 0
    ReDim estrutRequis(totalRequis)
    Grafico.Visible = False
End Sub

Sub PreencheCampos()
    'nada
End Sub

Sub FuncaoLimpar()
    LimpaCampos
End Sub

Sub FuncaoEstadoAnterior()
    'nada
End Sub


Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsFE As New ADODB.recordset
    
    ReDim estrutGrafico(0)
    ReDim estrutRequis(0)
    TotalGrafico = 0
    totalRequis = 0
    
    If EcDtInicio.value > EcDtFim.value Then
        Call BG_Mensagem(mediMsgBox, "Data final tem de ser igual ou superior � data inicial.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If

    sSql = "SELECT * FROM Sl_fila_espera WHERE dt_cri BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
    sSql = sSql & " AND dt_act IS NOT NULL AND hr_act IS NOT NULL "
    If EcCodOperador <> "" Then
        sSql = sSql & " AND user_act = " & BL_TrataStringParaBD(EcCodOperador)
    End If
    If EcCodSala <> "" Then
        sSql = sSql & " AND cod_sala = " & BL_TrataStringParaBD(EcCodSala)
    End If
    If CbPrioridades.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND prioridade = " & BG_DaComboSel(CbPrioridades)
    End If
    
    If OptAgrupamento(lAgrupHora).value = True Then
        sSql = sSql & " ORDER by  hr_act ASC "
    Else
        sSql = sSql & " ORDER by  n_req ASC "
    End If
    Set rsFE = New ADODB.recordset
    rsFE.CursorLocation = adUseServer
    rsFE.CursorType = adOpenStatic
    rsFE.Open sSql, gConexao
    
    If rsFE.RecordCount > 0 Then
        While Not rsFE.EOF
            PreencheEstrutReq rsFE!n_req, BL_HandleNull(rsFE!cod_sala, "0"), BL_HandleNull(rsFE!prioridade, "0"), BL_HandleNull(rsFE!dt_cri, ""), _
                              BL_HandleNull(rsFE!hr_cri, ""), BL_HandleNull(rsFE!dt_act, ""), BL_HandleNull(rsFE!hr_act, ""), BL_HandleNull(rsFE!user_cri, ""), _
                              BL_HandleNull(rsFE!user_act, "")
            rsFE.MoveNext
        Wend
        
        TotalGrafico = 0
        ReDim estrutGrafico(0)
        GeraGrafico
        BL_Toolbar_BotaoEstado "Imprimir", "Activo"
        BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
        
    Else
        BG_Mensagem mediMsgBox, "N�o existem registos para abrir a estat�stica!", vbInformation, "Estat�stica de Tempo M�dio de colheitas"
        BL_FimProcessamento Me
    
    End If
    
    rsFE.Close
    Set rsFE = Nothing
End Sub

Private Sub PreencheEstrutReq(n_req As String, cod_sala As String, prioridade As String, dt_cri As String, hr_cri As String, dt_act As String, _
                                 hr_act As String, user_cri As String, user_act As String)
    Dim i As Integer
    
    For i = 1 To totalRequis
        If n_req = estrutRequis(i).n_req Then
            Exit Sub
        End If
    Next
                       
    totalRequis = totalRequis + 1
    ReDim Preserve estrutRequis(totalRequis)
    estrutRequis(totalRequis).n_req = n_req
    estrutRequis(totalRequis).cod_sala = cod_sala
    estrutRequis(totalRequis).prioridade = prioridade
    estrutRequis(totalRequis).dt_cri = dt_cri
    estrutRequis(totalRequis).dt_act = dt_act
    estrutRequis(totalRequis).hr_cri = hr_cri
    estrutRequis(totalRequis).hr_act = hr_act
    estrutRequis(totalRequis).user_cri = user_cri
    estrutRequis(totalRequis).user_act = user_act
                                 
End Sub

Sub FuncaoAnterior()
    'nada
End Sub

Sub FuncaoSeguinte()
    'nada
End Sub

Sub FuncaoInserir()
    'nada
End Sub

Sub BD_Insert()
    'nada
End Sub

Sub FuncaoModificar()
    'nada
End Sub

Sub BD_Update()
    'nada
End Sub

Sub FuncaoRemover()
    'nada
End Sub

Sub BD_Delete()
    'nada
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "SL_COD_PRIORIDADES", "cod_prio", "nome_prio", CbPrioridades
    OptAgrupamento(0).value = True
    EcDtInicio.value = Bg_DaData_ADO
    EcDtFim.value = Bg_DaData_ADO
    OptTipo(0).value = True
End Sub

' DADO DETERMINADO TEMPO EM MINUTOS TRANSFORMA ESSE TEMPO NUMA STRING

Private Function DevolveTempo(minutos As String) As String
    Dim tempo As String
    Dim aux As String

    ' CALCULA STRING  TEMPOS
    If minutos < 60 Then
        tempo = minutos & " m"
    ElseIf minutos < 24 * 60 Then
        tempo = Mid((minutos / 60), 1, IIf(InStr(1, (minutos / 60), gSimboloDecimal) = 0, Len(CStr(minutos / 60)), InStr(1, (minutos / 60), gSimboloDecimal) - 1)) & " h "
        tempo = tempo & minutos Mod 60 & " m"
    Else
        ' VERIFICA SE TEM CASAS DECIMAIS
        If InStr(1, minutos / (24 * 60), gSimboloDecimal) <> 0 Then
            tempo = Mid(minutos / (24 * 60), 1, InStr(1, minutos / (24 * 60), gSimboloDecimal) - 1) & " d "
        Else
            tempo = minutos / (24 * 60) & " d "
        End If

        minutos = minutos Mod (24 * 60)
        If InStr(1, minutos / 60, gSimboloDecimal) <> 0 Then
            tempo = tempo & Mid(minutos / 60, 1, InStr(1, minutos / 60, gSimboloDecimal) - 1) & " h "
            tempo = tempo & minutos Mod 60 & " m"
        Else
            tempo = tempo & minutos / 60 & "h 0 m"
        End If
    End If
    DevolveTempo = tempo
End Function

Private Sub GeraGrafico()
    Dim modo As Integer
    Dim i As Long
    Dim Label As String
    
    If OptAgrupamento(lAgrupAno).value = True Then
        modo = lAgrupAno
    ElseIf OptAgrupamento(lAgrupMes).value = True Then
        modo = lAgrupMes
    ElseIf OptAgrupamento(lAgrupDia).value = True Then
        modo = lAgrupDia
    ElseIf OptAgrupamento(lAgrupHora).value = True Then
        modo = lAgrupHora
    End If
    
    For i = 1 To totalRequis
        If modo = lAgrupAno Then
            Label = DatePart("YYYY", CDate(estrutRequis(i).dt_cri))
        ElseIf modo = lAgrupMes Then
            Label = DatePart("m", CDate(estrutRequis(i).dt_cri)) & "-" & DatePart("YYYY", CDate(estrutRequis(i).dt_cri))
        ElseIf modo = lAgrupDia Then
            Label = CDate(estrutRequis(i).dt_cri)
        ElseIf modo = lAgrupHora Then
            Label = DatePart("h", estrutRequis(i).hr_act)
        End If
        
        PreencheEstrutGrafico Label, estrutRequis(i).hr_cri, estrutRequis(i).hr_act
    Next
    
    With Grafico
        
        .RowCount = TotalGrafico
        .ColumnCount = 1
        
        'Quando se atribui valores ao gr�fico (.data) incrementa a linha na coluna activa
        'ou a coluna se a linha da coluna for a �ltima
        .AutoIncrement = False
    
        .Column = 1
        For i = 1 To TotalGrafico
            .row = i
            If OptTipo(0).value = True Then
                .data = BL_String2Double(estrutGrafico(i).media)
            ElseIf OptTipo(1).value = True Then
                If OptAgrupamento(lAgrupHora).value Then
                    .data = BL_String2Double(estrutGrafico(i).NReq / (EcDtFim.value - EcDtInicio.value))
                Else
                    .data = BL_String2Double(estrutGrafico(i).NReq)
                End If
            End If
        Next i
        '.ChartData = Matriz
        
        'LABELS (S� DEPOIS DA ATRIBUI��O=>1 n�vel de indenta��o por Label!)
        For i = 1 To TotalGrafico
            .row = i
            .RowLabelCount = 2
            .RowLabelIndex = 1
            .RowLabel = estrutGrafico(i).Label
            .RowLabelIndex = 2
            If OptAgrupamento(lAgrupDia) Then
                .RowLabel = "Dias"
            ElseIf OptAgrupamento(lAgrupHora) Then
                .RowLabel = "Horas"
            ElseIf OptAgrupamento(lAgrupMes) Then
                .RowLabel = "Meses"
            ElseIf OptAgrupamento(lAgrupAno) Then
                .RowLabel = "Anos"
            End If
        Next i
        
        .Column = 1
        .ColumnLabelCount = 1
        .ColumnLabelIndex = 1
        
        
        .title.Text = "Tempo M�dio de Colheita"
                     
    End With
    Grafico.Visible = True
End Sub

Private Sub PreencheEstrutGrafico(Label As String, hr_cri As String, hr_act As String)
    Dim i As Long
    Dim indice As Long
    If DateDiff("n", hr_cri, hr_act) < 0 Then
        Exit Sub
    End If
    indice = -1
    For i = 1 To TotalGrafico
        If estrutGrafico(i).Label = Label Then
            indice = i
            Exit For
        End If
    Next
    If indice = -1 Then
        TotalGrafico = TotalGrafico + 1
        ReDim Preserve estrutGrafico(TotalGrafico)
        estrutGrafico(TotalGrafico).NReq = 0
        estrutGrafico(TotalGrafico).soma = 0
        indice = TotalGrafico
    End If
    estrutGrafico(indice).Label = Label
    estrutGrafico(indice).NReq = estrutGrafico(indice).NReq + 1
    estrutGrafico(indice).soma = estrutGrafico(indice).soma + DateDiff("n", hr_cri, hr_act)
    estrutGrafico(indice).media = estrutGrafico(indice).soma / estrutGrafico(indice).NReq
End Sub

Sub FuncaoImprimir()

    
    Dim sSql As String
    Dim continua As Boolean
    Dim Label As String
    Dim tempo As Double
    Dim aux As String
    Dim i As Long
    
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estat�stica de Colheitas") = True Then Exit Sub
    
    If TotalGrafico <= 0 Then Exit Sub
    
    'Printer Common Dialog
    gImprimirDestino = 0
    If CkAgruparOperador.value = vbChecked Then
        continua = BL_IniciaReport("EstatisticaColheita_Operador", "Estat�stica de Colheitas", crptToWindow)
    ElseIf CkAgruparSala.value = vbChecked Then
        continua = BL_IniciaReport("EstatisticaColheita_sala", "Estat�stica de Colheitas", crptToWindow)
    Else
        continua = BL_IniciaReport("EstatisticaColheita", "Estat�stica de Colheitas", crptToWindow)
    End If
    If continua = False Then Exit Sub
        
    sSql = "DELETE FROM sl_cr_estat_colheita WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    
    
    For i = 1 To totalRequis
        Label = ""
        tempo = 0
        If DateDiff("n", estrutRequis(i).hr_cri, estrutRequis(i).hr_act) >= 0 Then
            tempo = DateDiff("n", estrutRequis(i).hr_cri, estrutRequis(i).hr_act)
            If OptAgrupamento(lAgrupAno).value = True Then
                Label = DatePart("YYYY", CDate(estrutRequis(i).dt_cri))
            ElseIf OptAgrupamento(lAgrupMes).value = True Then
                Label = DatePart("m", CDate(estrutRequis(i).dt_cri)) & "-" & DatePart("YYYY", CDate(estrutRequis(i).dt_cri))
            ElseIf OptAgrupamento(lAgrupDia).value = True Then
                Label = CDate(estrutRequis(i).dt_cri)
            ElseIf OptAgrupamento(lAgrupHora).value = True Then
                Label = DatePart("h", estrutRequis(i).hr_act)
            End If
            
            sSql = "INSERT INTO sl_cr_estat_colheita(nome_computador, num_sessao,n_req, cod_sala, prioridade, dt_cri, "
            sSql = sSql & "hr_cri, dt_act, hr_act, user_cri, user_act, label, tempo ) VALUES ( "
            sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
            sSql = sSql & gNumeroSessao & ", "
            sSql = sSql & estrutRequis(i).n_req & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutRequis(i).cod_sala) & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutRequis(i).prioridade) & ", "
            sSql = sSql & BL_TrataDataParaBD(estrutRequis(i).dt_cri) & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutRequis(i).hr_cri) & ", "
            sSql = sSql & BL_TrataDataParaBD(estrutRequis(i).dt_act) & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutRequis(i).hr_act) & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutRequis(i).user_cri) & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutRequis(i).user_act) & ", "
            sSql = sSql & BL_TrataStringParaBD(Label) & ", "
            sSql = sSql & tempo & ")"
            BG_ExecutaQuery_ADO sSql
        End If
    Next
    
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    Report.SQLQuery = "SELECT sl_cr_estat_colheita.nome_computador, sl_cr_estat_colheita.num_sessao, sl_cr_estat_colheita.n_req, "
    Report.SQLQuery = Report.SQLQuery & " sl_cr_estat_colheita.cod_sala , sl_cr_estat_colheita.prioridade, sl_cr_estat_colheita.dt_cri, "
    Report.SQLQuery = Report.SQLQuery & " sl_cr_estat_colheita.hr_cri, sl_cr_estat_colheita.dt_act, sl_cr_estat_colheita.hr_act, sl_cr_estat_colheita.user_cri, "
    Report.SQLQuery = Report.SQLQuery & "  sl_cr_estat_colheita.user_act, sl_cr_estat_colheita.label, sl_cr_estat_colheita.tempo, sl_idutilizador.nome, sl_Cod_salas.descr_sala "
    Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_estat_colheita, sl_idutilizador, sl_cod_salas  WHERE sl_cr_estat_colheita.nome_computador =  " & BL_TrataStringParaBD(BG_SYS_GetComputerName)
    Report.SQLQuery = Report.SQLQuery & " AND sl_cr_estat_colheita.num_Sessao = " & gNumeroSessao & " AND sl_cr_estat_colheita.cod_sala = sl_cod_salas.cod_sala"
    Report.SQLQuery = Report.SQLQuery & " AND sl_idutilizador.cod_utilizador = sl_cr_estat_colheita.user_act "
    If OptAgrupamento(lAgrupHora) Then
        aux = " convert(int,label) "
    Else
        aux = " label "
    End If
    
    If CkAgruparOperador.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY sl_cr_estat_colheita.user_act ASC, " & aux & " ASC, n_req ASC "
    ElseIf CkAgruparSala.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY sl_cr_estat_colheita.cod_sala ASC, " & aux & "  ASC, n_req ASC "
    Else
        Report.SQLQuery = Report.SQLQuery & " ORDER BY " & aux & "  ASC, n_req ASC "
    End If
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtInicio.value)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.value)
    If OptTipo(0).value = True Then
        Report.formulas(2) = "TipoEstat=" & BL_TrataStringParaBD("Tempo M�dio Colheitas")
    Else
        Report.formulas(2) = "TipoEstat=" & BL_TrataStringParaBD("Quantidade Colheitas")
    End If
    
    If OptAgrupamento(lAgrupHora).value = True Then
        Report.formulas(3) = "Agrup=" & BL_TrataStringParaBD("Horas")
    ElseIf OptAgrupamento(lAgrupDia).value = True Then
        Report.formulas(3) = "Agrup=" & BL_TrataStringParaBD("Dias")
    ElseIf OptAgrupamento(lAgrupMes).value = True Then
        Report.formulas(3) = "Agrup=" & BL_TrataStringParaBD("Meses")
    ElseIf OptAgrupamento(lAgrupAno).value = True Then
        Report.formulas(3) = "Agrup=" & BL_TrataStringParaBD("Anos")
    End If
    
    If CkAgruparOperador.value = vbChecked Then
        Report.formulas(4) = "AgrupOperador=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(4) = "AgrupOperador=" & BL_TrataStringParaBD("N")
    End If
    
    If CkAgruparPrioridade.value = vbChecked Then
        Report.formulas(5) = "AgrupPrior=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(5) = "AgrupPrior=" & BL_TrataStringParaBD("N")
    End If
    
    If CkAgruparSala.value = vbChecked Then
        Report.formulas(6) = "AgrupSala=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(6) = "AgrupSala=" & BL_TrataStringParaBD("N")
    End If
    
    If CkSupDetalhes.value = vbChecked Then
        Report.formulas(7) = "SupDetalhes=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(7) = "SupDetalhes=" & BL_TrataStringParaBD("N")
    End If
    
    Call BL_ExecutaReport
    
    sSql = "DELETE FROM sl_cr_estat_colheita WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    
End Sub



VERSION 5.00
Begin VB.Form FormInventarioStock 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   4665
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   8205
   Icon            =   "FormInventarioStock.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4665
   ScaleWidth      =   8205
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   1455
      Left            =   120
      TabIndex        =   18
      Top             =   0
      Width           =   7935
      Begin VB.ComboBox EcActivo 
         Height          =   315
         Left            =   3960
         Style           =   2  'Dropdown List
         TabIndex        =   30
         Top             =   960
         Width           =   1215
      End
      Begin VB.TextBox EcCodigo 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   23
         Top             =   240
         Width           =   1365
      End
      Begin VB.TextBox EcDescricao 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3960
         TabIndex        =   22
         Top             =   270
         Width           =   3765
      End
      Begin VB.TextBox EcStockMinimo 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1320
         TabIndex        =   21
         Top             =   600
         Width           =   1365
      End
      Begin VB.TextBox EcStockActual 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   20
         Top             =   960
         Width           =   1365
      End
      Begin VB.ComboBox CbLocal 
         Height          =   315
         Left            =   3960
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   600
         Width           =   3735
      End
      Begin VB.Label Label9 
         Caption         =   "Local"
         Height          =   255
         Left            =   3000
         TabIndex        =   29
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "C�digo "
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   270
         Width           =   615
      End
      Begin VB.Label Label6 
         Caption         =   "Descri��o "
         Height          =   255
         Left            =   3000
         TabIndex        =   27
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Stock M�nimo"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "Stock Actual"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   960
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "Activo"
         Height          =   255
         Left            =   3000
         TabIndex        =   24
         Top             =   960
         Width           =   615
      End
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   12
      Top             =   6030
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4080
      TabIndex        =   11
      Top             =   6000
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   10
      Top             =   5640
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4080
      TabIndex        =   9
      Top             =   5640
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6360
      TabIndex        =   8
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Frame Frame1 
      Height          =   795
      Left            =   120
      TabIndex        =   1
      Top             =   3720
      Width           =   7965
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   195
         Width           =   615
      End
      Begin VB.Label Label7 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   480
         Width           =   765
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   5
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   4
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   3
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   2
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2130
      Left            =   120
      TabIndex        =   0
      Top             =   1560
      Width           =   7965
   End
   Begin VB.Label Label14 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2760
      TabIndex        =   17
      Top             =   6000
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label13 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2880
      TabIndex        =   16
      Top             =   5640
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label12 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label11 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   5640
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   4800
      TabIndex        =   13
      Top             =   5520
      Visible         =   0   'False
      Width           =   1485
   End
End
Attribute VB_Name = "FormInventarioStock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'      .............................
'     .                             .
'    .   Paulo Ferreira 2010.02.18   .
'     .       � 2010 Glintt-HS      .
'      .............................

' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim CriterioBase As String 'usado para os botoes de ordenacao
Dim CriterioAnterior As String 'usado para os botoes de ordenacao

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public obTabela As ADODB.recordset

Private Sub EcLista_Click()
    If EcLista.ListCount > 0 Then
        obTabela.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
End Sub
Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, EcCodigo
End Sub

Private Sub EcDescricao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDescricao_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = " Invent�rio de Stocks"
    Me.left = 500
    Me.top = 450
    Me.Width = 8295
    Me.Height = 5055 ' Normal
    'Me.Height = 4500 ' Campos Extras
    
    NomeTabela = "sl_stock_inventario"
    Set CampoDeFocus = EcDescricao
    
    NumCampos = 10
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "cod_stock"
    CamposBD(1) = "descr_stock"
    CamposBD(2) = "stock_minimo"
    CamposBD(3) = "stock_actual"
    CamposBD(4) = "flg_activo"
    CamposBD(5) = "user_cri"
    CamposBD(6) = "dt_cri"
    CamposBD(7) = "user_act"
    CamposBD(8) = "dt_act"
    CamposBD(9) = "cod_local"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcDescricao
    Set CamposEc(2) = EcStockMinimo
    Set CamposEc(3) = EcStockActual
    Set CamposEc(4) = EcActivo
    Set CamposEc(5) = EcUtilizadorCriacao
    Set CamposEc(6) = EcDataCriacao
    Set CamposEc(7) = EcUtilizadorAlteracao
    Set CamposEc(8) = EcDataAlteracao
    Set CamposEc(9) = CbLocal
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo"
    TextoCamposObrigatorios(2) = "Descri��o"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_stock"
    Set ChaveEc = EcCodigo
    
    CamposBDparaListBox = Array("cod_stock", "descr_stock", "cod_local")
    NumEspacos = Array(7, 20, 30)
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes
    
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    PreencheValoresDefeito
    estado = 1
    BG_StackJanelas_Push Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub PreencheValoresDefeito()
    
    BG_PreencheComboBD_ADO "sl_tbf_simnao", "cod_simnao", "descr_simnao", EcActivo, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", CbLocal, mediDescComboCodigo
   
End Sub

Private Sub CbLocal_KeyDown(KeyCode As Integer, Shift As Integer)
        
    BG_LimpaOpcao CbLocal, KeyCode
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not obTabela Is Nothing Then
        obTabela.Close
        Set obTabela = Nothing
    End If
    
    Set FormInventarioStock = Nothing
    
End Sub

Sub LimpaCampos()

    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

Sub PreencheCampos()
    Me.SetFocus
    
    If obTabela.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO obTabela, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
    End If
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        
        If Not obTabela Is Nothing Then
            obTabela.Close
            Set obTabela = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos..."
    Set obTabela = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    CriterioTabela = CriterioTabela & "  ORDER BY cod_stock ASC, descr_stock ASC"
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    obTabela.CursorType = adOpenStatic
    obTabela.CursorLocation = adUseServer
    
    obTabela.Open CriterioTabela, gConexao
    
    If obTabela.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo", vbExclamation, " Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = obTabela.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, obTabela, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        
        BL_FimProcessamento Me
    End If
End Sub
Sub FuncaoAnterior()
    obTabela.MovePrevious
    
    If obTabela.BOF Then
        obTabela.MoveNext
        BG_MensagemAnterior
    Else
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    obTabela.MoveNext
    
    If obTabela.EOF Then
        obTabela.MovePrevious
        BG_MensagemSeguinte
    Else
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    Dim sql As String
    Dim rs As ADODB.recordset
    
    EcCodigo = BG_DaMAX(NomeTabela, "cod_stock") + 1
    
    gMsgTitulo = " Inserir"
    gMsgMsg = "Tem a certeza que quer inserir este registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, (gMsgMsg), vbYesNo + vbDefaultButton2 + vbQuestion, (gMsgTitulo))
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        sql = "SELECT * FROM " & NomeTabela & " WHERE cod_stock = " & BL_TrataStringParaBD(EcCodigo)
        Set rs = New ADODB.recordset

        rs.CursorType = adOpenStatic
        rs.CursorLocation = adUseServer
        rs.Open sql, gConexao

        If rs.RecordCount > 0 Then
            BG_Mensagem mediMsgBox, "Registo j� existe", vbExclamation, " Inserir"
            Exit Sub
        End If
    
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
    End If

End Sub

Sub BD_Insert()
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    BG_Trata_BDErro
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = " Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es?"
    gMsgResp = BG_Mensagem(mediMsgBox, (gMsgMsg), vbYesNo + vbDefaultButton2 + vbQuestion, (gMsgTitulo))
    
    If gMsgResp = vbYes Then
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Update
        End If
    End If

End Sub

Sub BD_Update()
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(obTabela(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    BG_Trata_BDErro
    
    'propriedades precisam de ser estas pois � necessario utilizar o Bookmark
    MarcaLocal = obTabela.Bookmark
    obTabela.Requery
    
    If obTabela.BOF And obTabela.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, obTabela, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= obTabela.RecordCount Then
        obTabela.Move EcLista.ListIndex, MarcaLocal
        obTabela.Bookmark = MarcaLocal
    End If
    
End Sub

Sub FuncaoRemover()
    gMsgTitulo = " Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, (gMsgMsg), vbYesNo + vbDefaultButton2 + vbQuestion, (gMsgTitulo))
    
    If gMsgResp = vbYes Then
        BD_Delete
    End If
    
End Sub

Sub BD_Delete()
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    condicao = ChaveBD & " = '" & BG_CvPlica(obTabela(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE cod_stock = " & EcCodigo
    BG_ExecutaQuery_ADO SQLQuery

    'Temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = obTabela.Bookmark
    obTabela.Requery
        
    If obTabela.BOF And obTabela.EOF Then
        BG_Mensagem mediMsgBox, "O registo removido era �nico nesta pesquisa !", vbExclamation, " Remover"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, obTabela, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= obTabela.RecordCount Then
            obTabela.Move EcLista.ListIndex, MarcaLocal
            obTabela.Bookmark = MarcaLocal
        End If
    Else
        obTabela.MoveLast
    End If
    
    If obTabela.EOF Then obTabela.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub

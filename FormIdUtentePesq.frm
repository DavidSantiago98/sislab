VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormIdUtentePesq 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Pesquisa Utente"
   ClientHeight    =   8835
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   20400
   Icon            =   "FormIdUtentePesq.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8835
   ScaleWidth      =   20400
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcTutente 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5640
      TabIndex        =   52
      TabStop         =   0   'False
      Top             =   8280
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   120
      TabIndex        =   15
      Top             =   6600
      Width           =   12195
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   21
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   20
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   19
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   195
         Width           =   675
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   16
         Top             =   480
         Width           =   1455
      End
   End
   Begin VB.Frame FrameLista 
      BorderStyle     =   0  'None
      Height          =   4815
      Left            =   120
      TabIndex        =   47
      Top             =   1920
      Width           =   12255
      Begin VB.ListBox EcLista 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4440
         ItemData        =   "FormIdUtentePesq.frx":000C
         Left            =   0
         List            =   "FormIdUtentePesq.frx":000E
         TabIndex        =   48
         Top             =   240
         Width           =   12135
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Data Nascimento"
         Height          =   195
         Left            =   6360
         TabIndex        =   51
         Top             =   0
         Width           =   1230
      End
      Begin VB.Label Label3 
         Caption         =   "Nome"
         Height          =   255
         Left            =   1320
         TabIndex        =   50
         Top             =   0
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "N�mero"
         Height          =   255
         Left            =   0
         TabIndex        =   49
         Top             =   0
         Width           =   615
      End
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   1440
      TabIndex        =   33
      Top             =   9960
      Width           =   1095
   End
   Begin VB.TextBox EcEFR 
      Height          =   285
      Left            =   4560
      TabIndex        =   29
      Top             =   8280
      Width           =   615
   End
   Begin VB.TextBox EcSexo 
      Height          =   285
      Left            =   3840
      TabIndex        =   28
      Top             =   8280
      Width           =   375
   End
   Begin RichTextLib.RichTextBox EcNBenf1 
      Height          =   285
      Left            =   7920
      TabIndex        =   5
      Top             =   960
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   503
      _Version        =   393217
      Appearance      =   0
      TextRTF         =   $"FormIdUtentePesq.frx":0010
   End
   Begin VB.TextBox EcUtente 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2400
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   8280
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.Frame Frame3 
      Height          =   1815
      Left            =   10680
      TabIndex        =   26
      Top             =   0
      Width           =   1575
      Begin VB.CommandButton BtAdicionar 
         Height          =   615
         Left            =   120
         Picture         =   "FormIdUtentePesq.frx":0092
         Style           =   1  'Graphical
         TabIndex        =   31
         ToolTipText     =   "Novo Utente"
         Top             =   960
         Width           =   615
      End
      Begin VB.CommandButton BtProcurar 
         Height          =   615
         Left            =   840
         Picture         =   "FormIdUtentePesq.frx":0D5C
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Procurar Utentes"
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton BtEscolher 
         Height          =   615
         Left            =   120
         Picture         =   "FormIdUtentePesq.frx":1A26
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Escolher Utente"
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   8280
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   240
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   8280
      Visible         =   0   'False
      Width           =   795
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2040
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   7920
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   7920
      Visible         =   0   'False
      Width           =   795
   End
   Begin VB.TextBox EcSeqUtente 
      Height          =   315
      Left            =   240
      TabIndex        =   14
      Top             =   7920
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Frame Frame1 
      Height          =   1815
      Left            =   120
      TabIndex        =   9
      Top             =   0
      Width           =   10455
      Begin VB.TextBox EcNumInt 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1320
         TabIndex        =   46
         Top             =   1320
         Width           =   1815
      End
      Begin VB.ComboBox EcDescrIsencao 
         Height          =   315
         Left            =   7800
         Style           =   2  'Dropdown List
         TabIndex        =   44
         Top             =   1320
         Width           =   1815
      End
      Begin VB.ComboBox EcDescrSexo 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   7800
         Style           =   2  'Dropdown List
         TabIndex        =   42
         Top             =   600
         Width           =   1815
      End
      Begin VB.CommandButton BtPesquisaRNU 
         BackColor       =   &H00FFC0C0&
         Height          =   300
         Left            =   6000
         Picture         =   "FormIdUtentePesq.frx":26F0
         Style           =   1  'Graphical
         TabIndex        =   41
         ToolTipText     =   "  Pesquisa Utente (RNU)"
         Top             =   1320
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapEspecie 
         Height          =   300
         Left            =   6000
         Picture         =   "FormIdUtentePesq.frx":2A7A
         Style           =   1  'Graphical
         TabIndex        =   40
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox EcDescrEspecie 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   960
         Width           =   3975
      End
      Begin VB.TextBox EcCodEspecie 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   2
         Top             =   960
         Width           =   735
      End
      Begin VB.CommandButton BtPesqRapEFR 
         Height          =   300
         Left            =   6000
         Picture         =   "FormIdUtentePesq.frx":2E04
         Style           =   1  'Graphical
         TabIndex        =   37
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox EcDescrEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   35
         TabStop         =   0   'False
         Top             =   600
         Width           =   3975
      End
      Begin VB.TextBox EcCodEFR 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   1
         Top             =   600
         Width           =   735
      End
      Begin VB.CommandButton BtPesqRapNome 
         Height          =   300
         Left            =   6000
         Picture         =   "FormIdUtentePesq.frx":318E
         Style           =   1  'Graphical
         TabIndex        =   34
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox EcCartaoUtente 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4440
         TabIndex        =   3
         Top             =   1320
         Width           =   1575
      End
      Begin VB.TextBox EcNBenf 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7800
         TabIndex        =   30
         Top             =   960
         Width           =   1815
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   285
         Left            =   9360
         TabIndex        =   6
         Top             =   240
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   503
         _Version        =   393216
         Format          =   199884801
         CurrentDate     =   39182
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   7800
         TabIndex        =   4
         Tag             =   "7"
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1320
         TabIndex        =   0
         Top             =   240
         Width           =   4695
      End
      Begin VB.Label LbIsencao 
         AutoSize        =   -1  'True
         Caption         =   "I&sen��o"
         Height          =   195
         Left            =   6600
         TabIndex        =   45
         Top             =   1320
         Width           =   570
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Se&xo"
         Height          =   195
         Left            =   6600
         TabIndex        =   43
         Top             =   600
         Width           =   360
      End
      Begin VB.Label LbEspecie 
         Caption         =   "Esp�cie"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   38
         Top             =   960
         Width           =   735
      End
      Begin VB.Label Label10 
         Caption         =   "E.&F.R."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   600
         Width           =   495
      End
      Begin VB.Label LbCartao 
         AutoSize        =   -1  'True
         Caption         =   "N�Cart�o Utente"
         Height          =   195
         Left            =   3240
         TabIndex        =   32
         Top             =   1350
         Width           =   1290
      End
      Begin VB.Label LbDataNasc 
         AutoSize        =   -1  'True
         Caption         =   "Dt.Nascimento"
         Height          =   255
         Left            =   6600
         TabIndex        =   13
         Top             =   240
         Width           =   1050
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   660
      End
      Begin VB.Label LbNBenf 
         AutoSize        =   -1  'True
         Caption         =   "Nr- Benefici�rio"
         Height          =   315
         Left            =   6600
         TabIndex        =   11
         Top             =   960
         Width           =   1080
      End
      Begin VB.Label LbNInt 
         AutoSize        =   -1  'True
         Caption         =   "Nr.Interno"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1350
         Width           =   705
      End
   End
End
Attribute VB_Name = "FormIdUtentePesq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

''''''''''''''''''''''''''''''''''''''''''''
' Actualiza��o: 04/04/2007 '''''''''''''''''
' T�cnico: Paulo Ferreira ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''

''''''''''''''''''''''''''''''''''''''''''''
' Constantes de indices dos campos '''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Const CI_NBENEF = 0
Private Const CI_NINT = 1
Private Const CI_NOME = 2
Private Const CI_DTNASC = 3
Private Const CI_USERCRI = 4
Private Const CI_USERACT = 5
Private Const CI_DTCRI = 6
Private Const CI_DTACT = 7
Private Const CI_UTENTE = 8
Private Const CI_CARTAO_UTENTE = 9
Private Const CI_SEQ_UTENTE = 10
Private Const CI_COD_EFR = 11
Private Const CI_COD_ESPECIE = 12
Private Const CI_sexo = 13
Private Const CI_Isencao = 14
Private Const CI_TUTENTE = 15

''''''''''''''''''''''''''''''''''''''''''''
' Constantes de estado do ecra '''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Const CS_INACTIVE = 0
Private Const CS_ACTIVE = 1
Private Const CS_WAIT = 2

''''''''''''''''''''''''''''''''''''''''''''
' Variaveis de base de dados '''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Dim NumCampos As Integer
Dim CamposBD() As String
Dim CamposEc() As Object
Dim ChaveBD As String
Dim ChaveEc As Object
Dim NomeTabela As String
Dim CriterioTabela As String
Public rs As ADODB.recordset

''''''''''''''''''''''''''''''''''''''''''''
' Estado do ecra '''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Dim estado As Integer

''''''''''''''''''''''''''''''''''''''''''''
' Variaveis de ecra
''''''''''''''''''''''''''''''''''''''''''''
Dim CampoDeFocus As Object
Dim MarcaInicial As Variant
Dim CamposBDparaListBox As Variant
Dim NumEspacos

''''''''''''''''''''''''''''''''''''''''''''
' Variavel que valida campo data '''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Dim bDtValidate As Boolean

''''''''''''''''''''''''''''''''''''''''''''
' Evento inicial do ecra '''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    ' Padrao
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    BL_LimpaDadosUtente
    BG_ParametrizaPermissoes_ADO Me.Name
    estado = CS_INACTIVE
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    gF_IDENTIF_PESQ = 1
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de ecra activo ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EventoActivate()
    
    ' Padrao
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "EstadoAnterior", "Activo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    FuncaoLimpar
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que passa para o registo sequinte '
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoSeguinte()
    
    ' Se o recordset nao estiver aberto sair da funcao
    If (rs Is Nothing) Then: Exit Sub
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    ' Move a linha seleccionada para baixo
    If (EcLista.ListIndex < EcLista.ListCount - 1) Then: EcLista.Selected(EcLista.ListIndex + 1) = True
    If (EcLista.ListIndex < EcLista.ListCount - 1) Then: EcLista.Selected(EcLista.ListIndex - 1) = False

    ' Se o recordset = EOF
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    
    ' Senao passa o registo sequinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que passa para o registo anterior '
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoAnterior()
    
    ' Se o recordset nao estiver aberto sair da funcao
    If (rs Is Nothing) Then: Exit Sub
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    ' Move a linha seleccionada para cima
    If (EcLista.ListIndex > 0) Then: EcLista.Selected(EcLista.ListIndex - 1) = True
    If (EcLista.ListIndex > 0) Then: EcLista.Selected(EcLista.ListIndex + 1) = False
    
    ' Se o recordset = BOF
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
        
    ' Senao passa o registo anterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''
' Evento de saida do ecra ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub EventoUnload()

    BG_StackJanelas_Pop
    If gF_FACTUSENVIO = 1 Then
        FormFactusEnvio.Enabled = True
        Set gFormActivo = FormFactusEnvio
    ElseIf gF_EMISSAO_RECIBOS = 1 Then
        FormEmissaoRecibos.Enabled = True
        Set gFormActivo = FormEmissaoRecibos
    Else
        Set gFormActivo = MDIFormInicio
    End If
    BL_ToolbarEstadoN 0

    Set FormIdUtentePesq = Nothing
    gF_IDENTIF_PESQ = 0
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Define tipo de campos utilizados no ecra '
''''''''''''''''''''''''''''''''''''''''''''
Private Sub DefTipoCampos()
        
    ' Padrao
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_nasc_ute", EcDataNasc, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    BG_PreencheComboBD_ADO "sl_isencao", "seq_isencao", "descr_isencao", EcDescrIsencao
    EcDataNasc.Tag = adDate
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Inicializacoes do ecra '''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Inicializacoes()
        
    ' Definicoes do ecra
    Me.caption = " Pesquisa Utentes"
    Me.left = 440
    Me.top = 350
    Me.Width = 12525
    Me.Height = 7980
    
    ' Definicoes da base de dados
    NomeTabela = "sl_identif"
    Set CampoDeFocus = EcNome
    NumCampos = 16
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
        
    ' Campos da Base de Dados
    CamposBD(CI_NBENEF) = "n_benef_ute"
    CamposBD(CI_NINT) = "utente"
    CamposBD(CI_NOME) = "nome_ute"
    CamposBD(CI_DTNASC) = "dt_nasc_ute"
    CamposBD(CI_USERCRI) = "user_cri"
    CamposBD(CI_USERACT) = "user_act"
    CamposBD(CI_DTCRI) = "dt_cri"
    CamposBD(CI_DTACT) = "dt_act"
    CamposBD(CI_UTENTE) = "utente"
    CamposBD(CI_CARTAO_UTENTE) = "n_cartao_ute"
    CamposBD(CI_SEQ_UTENTE) = "seq_utente"
    CamposBD(CI_COD_EFR) = "cod_efr_ute"
    CamposBD(CI_COD_ESPECIE) = "cod_genero"
    CamposBD(CI_sexo) = "sexo_ute"
    CamposBD(CI_Isencao) = "cod_isencao_ute"
    CamposBD(CI_TUTENTE) = "t_utente"
    
    ' Campos do Ecr�
    Set CamposEc(CI_NBENEF) = EcNBenf
    Set CamposEc(CI_NINT) = EcNumInt
    Set CamposEc(CI_NOME) = EcNome
    Set CamposEc(CI_DTNASC) = EcDataNasc
    Set CamposEc(CI_USERCRI) = EcUtilizadorCriacao
    Set CamposEc(CI_USERACT) = EcUtilizadorAlteracao
    Set CamposEc(CI_DTCRI) = EcDataCriacao
    Set CamposEc(CI_DTACT) = EcDataAlteracao
    Set CamposEc(CI_UTENTE) = EcUtente
    Set CamposEc(CI_CARTAO_UTENTE) = EcCartaoUtente
    Set CamposEc(CI_SEQ_UTENTE) = EcSeqUtente
    Set CamposEc(CI_COD_EFR) = EcCodEFR
    Set CamposEc(CI_COD_ESPECIE) = EcCodEspecie
    Set CamposEc(CI_sexo) = EcDescrSexo
    Set CamposEc(CI_Isencao) = EcDescrIsencao
    Set CamposEc(CI_TUTENTE) = EcTutente
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_utente"
    Set ChaveEc = EcSeqUtente
    
    ' Inicializacoes da lista
    NumEspacos = Array(11, 43, 10)
    CamposBDparaListBox = Array("n_benef_ute", "nome_ute", "dt_nasc_ute")
    
    ' Inicializa variavel de validacao da data
    bDtValidate = False
End Sub

Private Sub BtAdicionar_Click()
    FormIdentificaUtente.Show
    Set gFormActivo = FormIdentificaUtente
End Sub

Private Sub BtPesqRapEFR_Click()
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
End Sub



Private Sub BtPesqRapNome_Click()
    If EcNome = "" And EcDataNasc = "" And EcCodEFR = "" And EcCodEspecie = "" And EcNumInt = "" And EcNBenf = "" Then
        Exit Sub
    End If
    
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "seq_utente"
    CamposEcran(1) = "seq_utente"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "nome_ute"
    CamposEcran(2) = "nome_ute"
    Tamanhos(2) = 3000
    
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2
    
    'Query
    ClausulaFrom = "sl_identif, sl_tbf_sexo, sl_genero"
    CampoPesquisa1 = "nome_ute"
    ClausulaWhere = " sl_identif.sexo_ute = sl_tbf_sexo.cod_sexo AND sl_identif.cod_genero = sl_genero.cod_genero"
    ClausulaWhere = ClausulaWhere & " AND seq_utente = seq_utente " & ConstroiCriterioPesquisa
        
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        ClausulaFrom = "slv_identif, sl_tbf_sexo, sl_genero"
        ClausulaWhere = " slv_identif.sexo_ute = sl_tbf_sexo.cod_sexo AND slv_identif.cod_genero = sl_genero.cod_genero"
        ClausulaWhere = ClausulaWhere & " AND seq_utente = seq_utente " & ConstroiCriterioPesquisa
    End If
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Nomes")
    
    mensagem = "N�o foi encontrada nenhum nome."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcSeqUtente.Text = resultados(1)
            EcNome.Text = resultados(2)
            FuncaoProcurar
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesqRapNome_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesqRapNome_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaRNU_Click()
    Dim identif As identifRNU
    Dim i As Integer
    If EcCartaoUtente <> "" Then
        If IsNumeric(EcCartaoUtente) Then
            identif = RNU_GetPatientBySNS(EcCartaoUtente)
            If identif.identificacao.NomeCompleto <> "" Then
                EcNome = identif.identificacao.NomeCompleto
                EcDataNasc = identif.identificacao.DataNascimento
                For i = 0 To EcDescrSexo.ListCount - 1
                    If EcDescrSexo.ItemData(i) = identif.identificacao.Sexo Then
                        EcDescrSexo.ListIndex = i
                        Exit For
                    End If
                Next i
                FuncaoProcurar
            Else
                BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo!", vbInformation, "Procurar"
            End If
        Else
            BG_Mensagem mediMsgBox, "N�mero Inv�lido!", vbInformation, "Procurar"
        End If
    Else
        BG_Mensagem mediMsgBox, "Cart�o de Utente n�o preenchido!", vbInformation, "Procurar"
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento do botao registo anterior '''''''''
''''''''''''''''''''''''''''''''''''''''''''


''''''''''''''''''''''''''''''''''''''''''''
' Evento do botao seguinte registo '''''''''
''''''''''''''''''''''''''''''''''''''''''''

''''''''''''''''''''''''''''''''''''''''''''
' Evento do botao procurar utente ''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BtProcurar_Click()
    FuncaoProcurar
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento do botao escolher utente ''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub BtEscolher_Click()
    EcLista_DblClick
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento alteracao do calendario '''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub DTPicker1_Change()
    EcDataNasc.Text = DTPicker1.value
    EcDataNasc_Validate True
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de alteracao do campo 'EcDataNasc''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcDataNasc_lostfocus()
     EcDataNasc_Validate True
    If (EcDataNasc.Text <> "" And bDtValidate) Then: DTPicker1.value = EcDataNasc.Text
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de validacao do campo data nasc '''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcDataNasc_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataNasc)
    If (Cancel) Then: Sendkeys ("{HOME}+{END}")
    bDtValidate = Not Cancel
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao limpar do ecra ''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoLimpar()
        
    ' Padrao
    EcLista.Clear
    LimpaCampos
    CampoDeFocus.SetFocus
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao procurar do ecra ''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoProcurar()
    
    Dim SelTotal As Boolean
    ' Constroi criterio para procura
    If EcNBenf1.Text <> "" And EcNBenf = "" Then
        EcNBenf = EcNBenf1.Text
    End If
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, True)
    
    ' Se o utilizador nao colocar dados para procura e despoletado um aviso
    If (EcNBenf.Text = "" And EcNumInt.Text = "" And EcNome.Text = "" And EcDataNasc.Text = "" And EcCartaoUtente = "" And EcSeqUtente = "") Then
        If BG_Mensagem(mediMsgBox, "N�o colocou crit�rios de pesquisa, esta opera��o pode demorar " & _
                        "algum tempo." & vbNewLine & "Confirma a selec��o de todos os utentes ?", _
                        vbQuestion + vbYesNo + vbDefaultButton1, App.ProductName) = vbNo Then: Exit Sub
    End If
      
    ' Definicao de criterio terminada
    CriterioTabela = CriterioTabela & " order by nome_ute"
    CriterioTabela = BL_Upper_Campo(CriterioTabela, "nome_ute", True)
        
    ' Define conexao
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    BL_InicioProcessamento Me, "A pesquisar SISLAB."
    
    ' Abre conexao
    rs.Open CriterioTabela, gConexao

    ' Se a consulta nao retornar dados nao faz nada
    If rs.RecordCount <= 0 Then
        
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo!", vbInformation, "Procurar"
        FormIdentificaUtente.Show
        Set gFormActivo = FormIdentificaUtente
        FormIdentificaUtente.EcNome = EcNome
        FormIdentificaUtente.EcDataNasc = EcDataNasc
        FormIdentificaUtente.EcCodEntFinR = EcEFR
        FormIdentificaUtente.EcNrBenef = EcNBenf
        FormIdentificaUtente.EcCodEntFinR_Validate True
        If EcDescrSexo.ListIndex > mediComboValorNull Then
            FormIdentificaUtente.EcSexo = EcDescrSexo.ItemData(EcDescrSexo.ListIndex)
        Else
            FormIdentificaUtente.EcSexo = ""
        End If
        If EcDescrSexo <> "" Then
            FormIdentificaUtente.EcDescrSexo = EcDescrSexo
        End If
        FormIdentificaUtente.EcDescrIsencao.ListIndex = EcDescrIsencao.ListIndex
        If EcDescrIsencao.ListIndex > mediComboValorNull Then
            FormIdentificaUtente.EcCodIsencao = EcDescrIsencao.ItemData(EcDescrIsencao.ListIndex)
        Else
            FormIdentificaUtente.EcCodIsencao = ""
        End If
        BL_ToolbarEstadoN 1
    ' Senao preenche os campos e lista
    Else
        LimpaCampos
        MarcaInicial = 1
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        'PreencheCampos
        BL_Toolbar_BotaoEstado "Anterior", "Activo"
        BL_Toolbar_BotaoEstado "Seguinte", "Activo"
        BL_FimProcessamento Me
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que retrocede o estado do ecra ''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub FuncaoEstadoAnterior()
    
    ' Se ecra estiver no estado de ocupado
    If (estado = CS_WAIT) Then
        estado = CS_ACTIVE
        BL_ToolbarEstadoN estado
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If (Not rs Is Nothing) Then: rs.Close: Set rs = Nothing
    
    ' Senao fecha o ecra
    Else
        Unload Me
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Funcao que preenche campos do ecra '''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub PreencheCampos()
        
    ' Se recordset estiver vazio nao preenche nada
    If (rs.RecordCount = 0) Then
        FuncaoLimpar
    
    ' Senao preenche todos os campos
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        EcCodEFR_Validate False
        EcCodespecie_Validate False
        EcNBenf1.Visible = False
        EcLista.SetFocus
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Limpa todos os campos do ecra ''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Public Sub LimpaCampos()
    
    BG_LimpaCampo_Todos CamposEc
    DTPicker1.value = Bg_DaData_ADO
    EcNBenf1.Visible = True
    EcEFR = ""
    EcSexo = ""
    EcNBenf1 = ""
    EcNBenf = ""
    EcDescrEFR = ""
    EcDescrEspecie = ""
End Sub


''''''''''''''''''''''''''''''''''''''''''''
' Evento de click na lista '''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcLista_Click()

    ' Se a lista contiver dados preenche campos respectivos da linha
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If

End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de duplo click na lista '''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcLista_DblClick()
    Dim i As Integer
    ' Se nao foi seleccionado nenhum utente sai da funcao
    If (EcNBenf.Text = "" And EcNumInt.Text = "" And EcNome.Text = "" And EcCartaoUtente = "") Then
        BG_Mensagem mediMsgBox, "Dever� seleccionar um utente! ", vbInformation, " Aten��o"
        Exit Sub
    End If
    
    If gTipoInstituicao = gTipoInstituicaoVet Then
        BL_LimpaDadosUtente
        gDUtente.seq_utente = BL_String2Double(EcSeqUtente)
        gEpisodioActivo = ""
        gSituacaoActiva = -1
        FormGestaoRequisicaoVet.Show
    ElseIf gF_EMISSAO_RECIBOS = mediSim Then
        If EcSeqUtente <> "" Then
            If gPassaRecFactus <> mediSim Then
                FormEmissaoRecibos.AlteraUtenteRecibo_OLD EcSeqUtente, FormEmissaoRecibos.FgEntidades.row, EcNome
            ElseIf gPassaRecFactus = mediSim Then
                FormEmissaoRecibos.AlteraUtenteRecibo_new EcSeqUtente, FormEmissaoRecibos.FgEntidades.row, EcNome
            End If
            Unload Me
        End If
    Else
        FormIdentificaUtente.Show
        Set gFormActivo = FormIdentificaUtente
        FormIdentificaUtente.EcDescrTipoUtente = EcTutente
        FormIdentificaUtente.EcUtente = EcUtente
        FormIdentificaUtente.FuncaoProcurar
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de enter na lista '''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcLista_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        Call EcLista_DblClick
    End If
    
End Sub




Private Sub EcNBenf_LostFocus()
    EcNBenf = UCase(EcNBenf)
End Sub

Private Sub EcNBenf1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        EcNBenf1.Text = Trim(EcNBenf1.Text)
        EcNBenf1_LostFocus
        'FuncaoProcurar
    End If
End Sub

Private Sub EcNBenf1_LostFocus()
    Dim conteudo() As String
    ReDim conteudo(1)
    EcNBenf1 = UCase(EcNBenf1)
    If EcNBenf1.Text = "" Then Exit Sub
    BL_Tokenize EcNBenf1.Text, vbCrLf, conteudo
    
    If Mid(conteudo(0), 1, 1) = "%" Then
        If UBound(conteudo) >= 2 Then
            EcNome = UCase(Trim(Mid(conteudo(0), 2, 65)))
            EcSexo = Mid(conteudo(2), 27, 1)
            EcDataNasc = Mid(conteudo(2), 25, 2) & "-" & Mid(conteudo(2), 23, 2) & "-" & Mid(conteudo(2), 19, 4)
            EcEFR = BL_SelCodigo("SL_EFR_MAP", "COD_EFR", "COD_ANT", Mid(conteudo(2), 38, 6))
            EcNBenf = Mid(conteudo(1), 3, 9)
            EcNBenf1.Text = ""
            FuncaoProcurar
        Else
            EcNBenf = EcNBenf1.Text
            EcNBenf1.Text = ""
            FuncaoProcurar
        End If
    Else
        If UBound(conteudo) <= 1 Then
            'Rui Coelho
            If Len(conteudo(0)) = 13 Then
                If Mid(conteudo(0), 10, 4) = "0000" Or Mid(conteudo(0), 10, 4) = "0001" Or _
                   Mid(conteudo(0), 10, 4) = "0010" Or Mid(conteudo(0), 10, 4) = "0011" Or _
                   Mid(conteudo(0), 10, 4) = "0100" Or Mid(conteudo(0), 10, 4) = "0101" Or _
                   Mid(conteudo(0), 10, 4) = "0110" Or Mid(conteudo(0), 10, 4) = "0111" Or _
                   Mid(conteudo(0), 10, 4) = "1000" Or Mid(conteudo(0), 10, 4) = "1001" Or _
                   Mid(conteudo(0), 10, 4) = "1010" Or Mid(conteudo(0), 10, 4) = "1011" Or _
                   Mid(conteudo(0), 10, 4) = "1100" Or Mid(conteudo(0), 10, 4) = "1101" Or _
                   Mid(conteudo(0), 10, 4) = "1110" Or Mid(conteudo(0), 10, 4) = "1111" Then
                    EcNBenf = Mid(conteudo(0), 1, 9)
                    FuncaoProcurar
                Else
                    EcNBenf = EcNBenf1.Text
                    EcNBenf1.Text = ""
                    FuncaoProcurar
                End If
            Else
                EcNBenf = EcNBenf1.Text
                EcNBenf1.Text = ""
                FuncaoProcurar
            End If
        Else
            EcNBenf = EcNBenf1.Text
            EcNBenf1.Text = ""
            FuncaoProcurar
        End If
    End If
    EcNBenf1.Visible = False
End Sub

Private Sub EcNome_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 And EcNome <> "" Then
        If gNomeCapitalizado = 1 Then
            EcNome.Text = StrConv(EcNome.Text, vbProperCase)
        Else
            EcNome.Text = UCase(EcNome.Text)
        End If
        FuncaoProcurar
    End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Evento de perda de focus do campos nome ''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub EcNome_LostFocus()
    
    If gNomeCapitalizado = 1 Then
        EcNome.Text = StrConv(EcNome.Text, vbProperCase)
        
    Else
        EcNome.Text = UCase(EcNome.Text)
    End If
End Sub


Private Sub EcNumInt_GotFocus()
    cmdOK.Default = True
End Sub

Private Sub EcNumInt_Validate(Cancel As Boolean)
    EcNumInt = Trim(EcNumInt)
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Activacao do ecra ''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Form_Activate()
    EventoActivate
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Abertura do ecra '''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Form_Load()
    EventoLoad
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Primeiro evento de unload do ecra ''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    ' Se o utilizador fechou o form na caixa de comando abre o ecra 'FormIdentificaUtente'
    'If (UnloadMode = vbFormControlMenu) Then: FormIdentificaUtente.Show: Set gFormActivo = FormIdentificaUtente
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Unload do ecra '''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

'Private Sub EcDataNasc_Validate(Cancel As Boolean)
'
'    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataNasc)
'    If Cancel Then
'        SendKeys ("{HOME}+{END}")
'    End If
'
'End Sub

Private Sub cmdOK_Click()
    On Error GoTo TrataErro
    EcNumInt = Trim(EcNumInt)
    FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "cmdOK_Click: " & Err.Number & " - " & Err.Description, Me.Name, "cmdOK_Click"
    Exit Sub
    Resume Next
End Sub

Public Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, "")
End Sub


Private Sub EcCodespecie_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodEspecie.Text = UCase(EcCodEspecie.Text)
    If EcCodEspecie.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_genero FROM sl_genero WHERE cod_genero=" & BL_TrataStringParaBD(Trim(EcCodEspecie.Text))
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Pa�s inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodEspecie.Text = ""
            EcDescrEspecie.Text = ""
        Else
            EcDescrEspecie.Text = BL_HandleNull(Tabela!descr_genero)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrEspecie.Text = ""
    End If
End Sub

' --------------------------------------------------------------------------

' CONSTROI CRITERIO DE PESQUISA R�PIDA

' --------------------------------------------------------------------------
Private Function ConstroiCriterioPesquisa() As String
    On Error GoTo TrataErro
    ConstroiCriterioPesquisa = ""
        If EcNome <> "" Then
            ConstroiCriterioPesquisa = ConstroiCriterioPesquisa & " AND upper(nome_ute) like '%" & UCase(Replace(EcNome, "*", "%")) & "%'"
        End If
        
        If EcCodEFR <> "" Then
            ConstroiCriterioPesquisa = ConstroiCriterioPesquisa & " AND cod_efr_ute = " & EcCodEFR
        End If
        If EcCodEspecie <> "" Then
            ConstroiCriterioPesquisa = ConstroiCriterioPesquisa & " AND cod_genero = " & EcCodEspecie
        End If
        If EcDataNasc <> "" Then
            ConstroiCriterioPesquisa = ConstroiCriterioPesquisa & " AND dt_nasc_ute = " & BL_TrataDataParaBD(EcDataNasc)
        End If
        If EcNBenf <> "" Then
            ConstroiCriterioPesquisa = ConstroiCriterioPesquisa & " AND n_benef_ute = " & BL_TrataStringParaBD(EcNBenf)
        End If
        If EcNumInt <> "" Then
            ConstroiCriterioPesquisa = ConstroiCriterioPesquisa & " AND utente = " & (EcNumInt)
        End If
        
Exit Function
TrataErro:
    BG_LogFile_Erros "ConstroiCriterioPesquisa: " & Err.Number & " - " & Err.Description, Me.Name, "ConstroiCriterioPesquisa"
    Exit Function
    Resume Next
End Function


Private Sub BtPesqRapEspecie_Click()
    


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_genero"
    CamposEcran(1) = "cod_genero"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_genero"
    CamposEcran(2) = "descr_genero"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_genero"
    CampoPesquisa1 = "descr_genero"
    ClausulaWhere = ""
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Esp�cies")
    
    mensagem = "N�o foi encontrada nenhuma Esp�cie."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodEspecie.Text = resultados(1)
            EcDescrEspecie.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesqRapEspecie_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesqRapEspecie_Click"
    Exit Sub
    Resume Next
End Sub





VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Begin VB.Form FormRegistoAcessos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionRegistoAcessos"
   ClientHeight    =   7125
   ClientLeft      =   540
   ClientTop       =   2295
   ClientWidth     =   10605
   Icon            =   "FormRegAce.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7125
   ScaleWidth      =   10605
   ShowInTaskbar   =   0   'False
   Begin MSDataGridLib.DataGrid DBGrid1 
      Bindings        =   "FormRegAce.frx":000C
      Height          =   4305
      Left            =   150
      TabIndex        =   20
      Top             =   870
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   7594
      _Version        =   393216
      AllowUpdate     =   -1  'True
      Appearance      =   0
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc Data_RegistoAcessos 
      Height          =   330
      Left            =   7080
      Top             =   5640
      Visible         =   0   'False
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   -1
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Data_RegistoAcessos"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc Data_IdUtilizadores 
      DragMode        =   1  'Automatic
      Height          =   330
      Left            =   4080
      Top             =   5640
      Visible         =   0   'False
      Width           =   2925
      _ExtentX        =   5159
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   -1
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Data_IdUtilizadores"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataListLib.DataCombo EcUtilizador 
      Bindings        =   "FormRegAce.frx":002E
      DataSource      =   "Data_RegistoAcessos"
      Height          =   315
      Left            =   3240
      TabIndex        =   3
      Top             =   360
      Width           =   2325
      _ExtentX        =   4101
      _ExtentY        =   556
      _Version        =   393216
      Style           =   2
      OLEDropMode     =   1
      ListField       =   ""
      BoundColumn     =   ""
      Text            =   "DataCombo1"
   End
   Begin VB.OptionButton OpNomeComputador 
      Caption         =   "Computador"
      Height          =   255
      Left            =   7800
      TabIndex        =   10
      Top             =   5250
      Width           =   1215
   End
   Begin VB.OptionButton OpUtilizador 
      Caption         =   "Utilizador"
      Height          =   255
      Left            =   9120
      TabIndex        =   11
      Top             =   5280
      Width           =   1095
   End
   Begin VB.OptionButton OpCodAcesso 
      Caption         =   "N� Acesso"
      Height          =   255
      Left            =   6600
      TabIndex        =   9
      Top             =   5250
      Value           =   -1  'True
      Width           =   1095
   End
   Begin VB.CheckBox ChTipoRes 
      Caption         =   "Computadores com a aplica��o aberta"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   5280
      Width           =   3135
   End
   Begin VB.TextBox EcCodUtilizador 
      DataSource      =   "Data_RegistoAcessos"
      Height          =   285
      Left            =   4560
      TabIndex        =   15
      Top             =   120
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox EcHoraSaida 
      DataSource      =   "Data_RegistoAcessos"
      Height          =   285
      Left            =   9240
      TabIndex        =   7
      Top             =   360
      Width           =   855
   End
   Begin VB.TextBox EcHoraEntrada 
      DataSource      =   "Data_RegistoAcessos"
      Height          =   285
      Left            =   6840
      TabIndex        =   5
      Top             =   360
      Width           =   1095
   End
   Begin VB.TextBox EcDataSaida 
      DataSource      =   "Data_RegistoAcessos"
      Height          =   285
      Left            =   8040
      TabIndex        =   6
      Top             =   360
      Width           =   1095
   End
   Begin VB.TextBox EcDataEntrada 
      DataSource      =   "Data_RegistoAcessos"
      Height          =   285
      Left            =   5640
      TabIndex        =   4
      Top             =   360
      Width           =   1095
   End
   Begin VB.TextBox EcNomeComputador 
      DataSource      =   "Data_RegistoAcessos"
      Height          =   285
      Left            =   1200
      TabIndex        =   2
      Top             =   360
      Width           =   1935
   End
   Begin VB.TextBox EcCodAcesso 
      DataSource      =   "Data_RegistoAcessos"
      Height          =   285
      Left            =   165
      TabIndex        =   1
      Top             =   360
      Width           =   900
   End
   Begin VB.Label Label8 
      Caption         =   "Ordenado por:"
      Height          =   255
      Left            =   5400
      TabIndex        =   12
      Top             =   5250
      Width           =   1095
   End
   Begin VB.Label Label7 
      Caption         =   "Computador"
      Height          =   255
      Left            =   1200
      TabIndex        =   13
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label6 
      Caption         =   "Hora Sa�da"
      Height          =   255
      Left            =   9240
      TabIndex        =   19
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label5 
      Caption         =   "Data Sa�da"
      Height          =   255
      Left            =   8040
      TabIndex        =   18
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "Hora Entrada"
      Height          =   255
      Left            =   6840
      TabIndex        =   17
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "Data Entrada"
      Height          =   255
      Left            =   5640
      TabIndex        =   16
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Utilizador"
      Height          =   255
      Left            =   3240
      TabIndex        =   14
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "N� Acesso"
      Height          =   255
      Left            =   195
      TabIndex        =   0
      Top             =   120
      Width           =   855
   End
End
Attribute VB_Name = "FormRegistoAcessos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim NomeView As String
Dim CriterioTabela As String

Dim obTabela As Recordset
Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Function EstadoCamposEc(Tipo As Integer)

If Tipo = 0 Then

    EcCodAcesso.Visible = False
    EcNomeComputador.Visible = False
    EcUtilizador.Visible = False
    EcDataEntrada.Visible = False
    EcHoraEntrada.Visible = False
    EcDataSaida.Visible = False
    EcHoraSaida.Visible = False
    Label1.Visible = False
    Label2.Visible = False
    Label3.Visible = False
    Label4.Visible = False
    Label5.Visible = False
    Label6.Visible = False
    Label7.Visible = False
    
ElseIf Tipo = 1 Then
    EcCodAcesso.Visible = True
    EcNomeComputador.Visible = True
    EcUtilizador.Visible = True
    EcDataEntrada.Visible = True
    EcHoraEntrada.Visible = True
    EcDataSaida.Visible = True
    EcHoraSaida.Visible = True
    Label1.Visible = True
    Label2.Visible = True
    Label3.Visible = True
    Label4.Visible = True
    Label5.Visible = True
    Label6.Visible = True
    Label7.Visible = True
End If

End Function

Private Sub ChTipoRes_Click()
    
    FuncaoLimpar
    AjustaGrid
    If ChTipoRes = 0 Then
        EstadoCamposEc (1)
        BL_Toolbar_BotaoEstado "Limpar", "Activo"
        BL_Toolbar_BotaoEstado "EstadoAnterior", "Activo"
        OpUtilizador.Enabled = True
        
    Else
        EstadoCamposEc (0)
        If OpUtilizador.Value = True Then
            OpCodAcesso.Value = True
        End If
        
        FuncaoProcurar
        AjustaGrid
        
        BL_Toolbar_BotaoEstado "Limpar", "InActivo"
        BL_Toolbar_BotaoEstado "EstadoAnterior", "InActivo"
        
        OpUtilizador.Enabled = False
    End If

End Sub

Private Sub EcCodAcesso_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcCodAcesso_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Private Sub EcDataEntrada_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDataEntrada_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Private Sub EcDataSaida_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDataSaida_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Private Sub EcHoraEntrada_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcHoraEntrada_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Private Sub EcHoraSaida_GotFocus()
    
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcHoraSaida_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Private Sub EcUtilizador_Click(Area As Integer)
    
    EcCodUtilizador = EcUtilizador.BoundText

End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Funcao_DataActual()
    
    BL_PreencheData EcDataEntrada, Me.ActiveControl
    BL_PreencheData EcDataSaida, Me.ActiveControl

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()

    Me.Caption = " Registo de Acessos"
    Me.Left = 540
    Me.Top = 450
    Me.Width = 10350
    Me.Height = 5985
    
    NomeTabela = "slv_registoacessos"
    Set CampoDeFocus = EcUtilizador
    
    NumCampos = 7
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "cod_acesso"
    'CamposBD(1) = "nome_computador"
    CamposBD(1) = "descr_computador"
    CamposBD(2) = "cod_utilizador"
    CamposBD(3) = "dt_entrada"
    CamposBD(4) = "hr_entrada"
    CamposBD(5) = "dt_saida"
    CamposBD(6) = "hr_saida"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodAcesso
    Set CamposEc(1) = EcNomeComputador
    Set CamposEc(2) = EcCodUtilizador
    Set CamposEc(3) = EcDataEntrada
    Set CamposEc(4) = EcHoraEntrada
    Set CamposEc(5) = EcDataSaida
    Set CamposEc(6) = EcHoraSaida
    
'-----------------------------------------------------------------------------------

    ' Definir Data_IdUtilizadores ...
    Data_IdUtilizadores.ConnectionString = gConexao.ConnectionString
    Data_IdUtilizadores.CommandType = adCmdText
    Data_IdUtilizadores.RecordSource = "SELECT cod_utilizador,utilizador FROM sl_idutilizador WHERE flg_removido = 0 ORDER BY utilizador Asc"
    
    ' Definir Data_RegistoAcessos ...
    Data_RegistoAcessos.ConnectionString = gConexao.ConnectionString
    Data_RegistoAcessos.CommandType = adCmdText
    
    Data_RegistoAcessos.RecordSource = "SELECT * FROM slv_registoacessos ORDER BY cod_acesso DESC"
      
    BL_InicioProcessamento Me, "A pesquisar registos."
    Data_RegistoAcessos.Refresh
    Data_IdUtilizadores.Refresh
    BL_FimProcessamento Me
    
    ' Definir EcCodAcesso ...
    EcCodAcesso.DataField = "cod_acesso"
    
    ' Definir EcNomeComputador ...
    EcNomeComputador.DataField = "descr_computador"
    
    ' Definir EcCodUtilizador ...
    EcCodUtilizador.DataField = "cod_utilizador"
    
    ' Definir EcDataEntrada ...
    EcDataEntrada.DataField = "dt_entrada"
    
    ' Definir EcHoraEntrada ...
    EcHoraEntrada.DataField = "hr_entrada"
    
    ' Definir EcDataSaida ...
    EcDataSaida.DataField = "dt_saida"
    
    ' Definir EcHoraSaida ...
    EcHoraSaida.DataField = "hr_saida"
    
    ' Definir EcUtilizador ...
    EcUtilizador.DataField = "cod_utilizador"
    
    EcUtilizador.ListField = "utilizador"
    EcUtilizador.BoundColumn = "cod_utilizador"
    AjustaGrid
    
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    
    Estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormRegistoAcessos = Nothing

End Sub

Sub LimpaCampos()
    
    On Error Resume Next
    
    Me.SetFocus
    
    Data_RegistoAcessos.RecordSource = "SELECT * FROM " & NomeTabela & " WHERE cod_acesso = " & "-1"
    EcNomeComputador.Text = ""
    Data_RegistoAcessos.Refresh

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_entrada", EcDataEntrada, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_saida", EcDataSaida, mediTipoData
    
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_entrada", EcHoraEntrada, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_saida", EcHoraSaida, mediTipoHora
    
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheValoresDefeito()
    
    ChTipoRes.Value = 0
    
    DBGrid1.AllowAddNew = False
    DBGrid1.AllowDelete = False
    DBGrid1.AllowUpdate = False
    DBGrid1.AllowRowSizing = False
    DBGrid1.BorderStyle = 0
    DBGrid1.ColumnHeaders = False

End Sub

Public Sub AjustaGrid()
    
    ' Esta Rotina deve ser invocada:
    ' - No evento 'Reposition' do Data Control
    '
    ' Algumas Propriedades:
    ' - Alignment
    ' - AllowSizing
    ' - Caption
    ' - Visible
    ' - Width
    '
    ' Exemplo:
    '   DBGrid1.Columns(0).Caption = "T�tulo"
    
    If ChTipoRes = 0 Then
        DBGrid1.Top = 960
        DBGrid1.Height = 4095
        
        DBGrid1.ColumnHeaders = False
        
        DBGrid1.Columns(2).Visible = False
        
        DBGrid1.Columns(0).Alignment = vbLeftJustify
        DBGrid1.Columns(4).Alignment = vbLeftJustify
        DBGrid1.Columns(5).Alignment = vbLeftJustify
        DBGrid1.Columns(6).Alignment = vbLeftJustify
        DBGrid1.Columns(7).Alignment = vbLeftJustify
        
        DBGrid1.Columns(0).Width = 1200
        DBGrid1.Columns(1).Width = 2060
        DBGrid1.Columns(2).Width = 0
        DBGrid1.Columns(3).Width = 2500
        DBGrid1.Columns(4).Width = 1200
        DBGrid1.Columns(5).Width = 1100
        DBGrid1.Columns(6).Width = 1200
        DBGrid1.Columns(7).Width = 850
        
        DBGrid1.Columns(0).Caption = "N� de Acesso"
        DBGrid1.Columns(1).Caption = "Computador"
        DBGrid1.Columns(2).Caption = "Data Entrada"
        DBGrid1.Columns(3).Caption = "Hora Entrada"
        DBGrid1.Columns(4).Caption = "Data Sa�da"
        DBGrid1.Columns(5).Caption = "Hora Sa�da"
        
    Else
        DBGrid1.Top = 240
        DBGrid1.Height = 4815
        
        DBGrid1.ColumnHeaders = True
        
        DBGrid1.Columns(2).Visible = True
        DBGrid1.Columns(0).Alignment = vbLeftJustify
        DBGrid1.Columns(1).Alignment = vbLeftJustify
        DBGrid1.Columns(2).Alignment = vbLeftJustify
        DBGrid1.Columns(3).Alignment = vbLeftJustify
        DBGrid1.Columns(4).Alignment = vbLeftJustify
        DBGrid1.Columns(5).Alignment = vbLeftJustify
        
        DBGrid1.Columns(0).Width = 1300
        DBGrid1.Columns(1).Width = 2060
        DBGrid1.Columns(2).Width = 1200
        DBGrid1.Columns(3).Width = 1100
        DBGrid1.Columns(4).Width = 1200
        DBGrid1.Columns(5).Width = 1100
        
        DBGrid1.Columns(0).Caption = "N� de Acesso"
        DBGrid1.Columns(1).Caption = "Computador"
        DBGrid1.Columns(2).Caption = "Data Entrada"
        DBGrid1.Columns(3).Caption = "Hora Entrada"
        DBGrid1.Columns(4).Caption = "Data Sa�da"
        DBGrid1.Columns(5).Caption = "Hora Sa�da"
    End If

End Sub

Sub FuncaoLimpar()

    EstadoCamposEc (1)
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        LimpaCampos
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.Caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
    
    Dim SelTotal As Boolean
    Dim sOrdenacao As String

    If ChTipoRes.Value = 0 Then
        CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    Else
        CriterioTabela = "SELECT * FROM slv_RegAcess_abert WHERE dt_saida IS null"
    End If

    If OpCodAcesso.Value = True Then
        sOrdenacao = " ORDER BY cod_acesso Desc"
    ElseIf OpNomeComputador.Value = True Then
        sOrdenacao = " ORDER BY descr_computador ASC, cod_acesso Desc"
    ElseIf OpUtilizador.Value = True Then
        sOrdenacao = " ORDER BY cod_utilizador ASC, cod_acesso Desc"
    End If
    
    If SelTotal = True Then
        gMsgTitulo = "Procurar"
        gMsgMsg = "Essa opera��o pode demorar algum tempo !"
        gMsgMsg = gMsgMsg & vbCrLf & vbCrLf & "Tem a certeza que quer seleccionar a tabela toda?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbNo Then
            Exit Sub
        End If
    End If

    BL_InicioProcessamento Me, "A pesquisar registos."
    Data_RegistoAcessos.RecordSource = CriterioTabela & sOrdenacao
    Data_RegistoAcessos.Refresh
    AjustaGrid
    
    If Data_RegistoAcessos.Recordset.RecordCount = 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        BL_ToolbarEstadoN Estado
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    Data_RegistoAcessos.Recordset.MovePrevious
    
    If Data_RegistoAcessos.Recordset.BOF Then
        Data_RegistoAcessos.Recordset.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    Data_RegistoAcessos.Recordset.MoveNext
    
    If Data_RegistoAcessos.Recordset.EOF Then
        Data_RegistoAcessos.Recordset.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoInserir()
    
    BG_Mensagem mediMsgBox, "N�o � poss�vel efectuar a opera��o neste Form!", vbInformation

End Sub

Sub FuncaoModificar()
    
    BG_Mensagem mediMsgBox, "N�o � poss�vel efectuar a opera��o neste Form!", vbInformation

End Sub

Sub FuncaoRemover()
    
    BG_Mensagem mediMsgBox, "N�o � poss�vel efectuar a opera��o neste Form!", vbInformation

End Sub

Private Sub OpCodAcesso_Click()
    
    Dim sOrdenacao As String
    Dim SelTotal As Boolean
     
    If Estado = 1 Then
        LimpaCampos
        EcCodUtilizador = ""
        If ChTipoRes.Value = 0 Then
            CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
        Else
            CriterioTabela = "SELECT * FROM slv_RegAcess_abert WHERE dt_saida IS null"
        End If
        Data_RegistoAcessos.RecordSource = CriterioTabela & " ORDER BY cod_acesso DESC"
        Data_RegistoAcessos.Refresh
    End If
    
    If Estado = 2 Then
        If OpCodAcesso.Value = True Then
            sOrdenacao = " ORDER BY cod_acesso Desc"
        ElseIf OpNomeComputador.Value = True Then
            sOrdenacao = " ORDER BY cod_computador ASC, cod_acesso Desc"
        ElseIf OpUtilizador.Value = True Then
            sOrdenacao = " ORDER BY cod_utilizador ASC, cod_acesso Desc"
        End If
        
        BL_MudaCursorRato mediMP_Espera, MDIFormInicio
        Data_RegistoAcessos.RecordSource = CriterioTabela & sOrdenacao
        Data_RegistoAcessos.Refresh
        BL_MudaCursorRato mediMP_Activo, MDIFormInicio
    End If
    AjustaGrid

End Sub

Private Sub OpNomeComputador_Click()
    
    Dim sOrdenacao As String
    Dim SelTotal As Boolean
    
    If Estado = 1 Then
        LimpaCampos
        EcCodUtilizador = ""
        If ChTipoRes.Value = 0 Then
            CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
        Else
            CriterioTabela = "SELECT * FROM slv_RegAcess_abert WHERE dt_saida IS null"
        End If
        Data_RegistoAcessos.RecordSource = CriterioTabela & " ORDER BY descr_computador"
        Data_RegistoAcessos.Refresh
    End If
    If Estado = 2 Then
        If OpCodAcesso.Value = True Then
            sOrdenacao = " ORDER BY cod_acesso Desc"
        ElseIf OpNomeComputador.Value = True Then
            sOrdenacao = " ORDER BY descr_computador ASC, cod_acesso Desc"
        ElseIf OpUtilizador.Value = True Then
            sOrdenacao = " ORDER BY cod_utilizador ASC, cod_acesso Desc"
        End If
        
        BL_MudaCursorRato mediMP_Espera, MDIFormInicio
        Data_RegistoAcessos.RecordSource = CriterioTabela & sOrdenacao
        Data_RegistoAcessos.Refresh
        BL_MudaCursorRato mediMP_Activo, MDIFormInicio
    End If
    AjustaGrid
    
End Sub

Private Sub OpUtilizador_Click()
    
    Dim sOrdenacao As String
    Dim SelTotal As Boolean
       
    If Estado = 1 Then
        LimpaCampos
        EcCodUtilizador = ""
        If ChTipoRes.Value = 0 Then
            CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
        Else
            CriterioTabela = "SELECT * FROM slv_RegAcess_abert WHERE dt_saida IS null"
        End If
        Data_RegistoAcessos.RecordSource = CriterioTabela & " ORDER BY cod_utilizador"
        Data_RegistoAcessos.Refresh
    End If
    If Estado = 2 Then
        If OpCodAcesso.Value = True Then
            sOrdenacao = " ORDER BY cod_acesso Desc"
        ElseIf OpNomeComputador.Value = True Then
            sOrdenacao = " ORDER BY cod_computador ASC, cod_acesso Desc"
        ElseIf OpUtilizador.Value = True Then
            sOrdenacao = " ORDER BY cod_utilizador ASC, cod_acesso Desc"
        End If
        BL_MudaCursorRato mediMP_Espera, MDIFormInicio
        Data_RegistoAcessos.RecordSource = CriterioTabela & sOrdenacao
        Data_RegistoAcessos.Refresh
        BL_MudaCursorRato mediMP_Activo, MDIFormInicio
    End If
    AjustaGrid

End Sub


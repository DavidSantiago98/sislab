VERSION 5.00
Begin VB.Form FormTransPermissoes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormTransPermissoes"
   ClientHeight    =   2295
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5640
   Icon            =   "FormTransPermissoes.frx":0000
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2295
   ScaleWidth      =   5640
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Caption         =   "Destino "
      Height          =   1335
      Left            =   2880
      TabIndex        =   10
      Top             =   120
      Width           =   2655
      Begin VB.OptionButton OpDG 
         Caption         =   "Grupos"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Value           =   -1  'True
         Width           =   2175
      End
      Begin VB.OptionButton OpDU 
         Caption         =   "Utilizadores"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   480
         Width           =   2175
      End
      Begin VB.ComboBox CbDestU 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   840
         Visible         =   0   'False
         Width           =   2415
      End
      Begin VB.ComboBox CbDestG 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   840
         Width           =   2415
      End
   End
   Begin VB.PictureBox GG 
      Height          =   495
      Left            =   2160
      Picture         =   "FormTransPermissoes.frx":000C
      ScaleHeight     =   435
      ScaleWidth      =   435
      TabIndex        =   4
      Top             =   2520
      Width           =   495
   End
   Begin VB.PictureBox UU 
      Height          =   495
      Left            =   960
      Picture         =   "FormTransPermissoes.frx":08D6
      ScaleHeight     =   435
      ScaleWidth      =   435
      TabIndex        =   3
      Top             =   2520
      Width           =   495
   End
   Begin VB.PictureBox UG 
      Height          =   495
      Left            =   360
      Picture         =   "FormTransPermissoes.frx":11A0
      ScaleHeight     =   435
      ScaleWidth      =   435
      TabIndex        =   2
      Top             =   2520
      Width           =   495
   End
   Begin VB.PictureBox GU 
      Height          =   495
      Left            =   1560
      Picture         =   "FormTransPermissoes.frx":1A6A
      ScaleHeight     =   435
      ScaleWidth      =   435
      TabIndex        =   1
      Top             =   2520
      Width           =   495
   End
   Begin VB.CommandButton BtTransf 
      Height          =   615
      Left            =   2280
      Picture         =   "FormTransPermissoes.frx":2334
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   1560
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Caption         =   "Origem "
      Height          =   1335
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   2655
      Begin VB.OptionButton OpOG 
         Caption         =   "Grupos"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   240
         Value           =   -1  'True
         Width           =   2175
      End
      Begin VB.OptionButton OpOU 
         Caption         =   "Utilizadores"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   480
         Width           =   2175
      End
      Begin VB.ComboBox CbOrigU 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   840
         Visible         =   0   'False
         Width           =   2415
      End
      Begin VB.ComboBox CbOrigG 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   840
         Width           =   2415
      End
   End
End
Attribute VB_Name = "FormTransPermissoes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualização : 21/02/2002
' Técnico Paulo Costa

' Variáveis Globais para este Form.

Sub PreencheValoresDefeito()
    
    Dim sql As String
    Dim RsGU As ADODB.recordset
    Dim i As Integer
    
    BG_PreencheComboBD_ADO "sl_idutilizador", "cod_utilizador", "utilizador", CbDestU, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "sl_gr_util", "cod_gr_util", "descr_gr_util", CbDestG, mediAscComboDesignacao

    sql = "Select DISTINCT cod_grupo, descr_gr_util from " & cParamAcessos & ", sl_gr_util where " & cParamAcessos & ".cod_grupo = sl_gr_util.cod_gr_util and cod_grupo is not null"
    
    Set RsGU = New ADODB.recordset
    With RsGU
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .Open , gConexao
    End With
    
    i = 0
    While Not RsGU.EOF
        CbOrigG.AddItem BL_HandleNull(RsGU!descr_gr_util, " "), i
        CbOrigG.ItemData(i) = BL_HandleNull(RsGU!cod_grupo, 0)
        i = i + 1
        RsGU.MoveNext
    Wend
    RsGU.Close
    
    sql = "Select DISTINCT cod_util, utilizador from " & cParamAcessos & ", sl_idutilizador where cod_util = sl_idutilizador.cod_utilizador and cod_util is not null"
    With RsGU
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .Open , gConexao
    End With
    
    i = 0
    While Not RsGU.EOF
        CbOrigU.AddItem BL_HandleNull(RsGU!utilizador, " "), i
        CbOrigU.ItemData(i) = BL_HandleNull(RsGU!cod_util, 0)
        i = i + 1
        RsGU.MoveNext
    Wend
    RsGU.Close
    
    Set RsGU = Nothing
    
End Sub

Sub Inicializacoes()
    
    Me.caption = " Transferência de Parametrizações de Acessos"
    Me.left = 540
    Me.top = 450
    Me.Height = 2670
    Me.Width = 5730
    
    PreencheValoresDefeito

End Sub

Private Sub BtTransf_Click()
    
    Dim SelSQL As String
    Dim InsSQL As String
    Dim ValuesSQL As String
    Dim RsTransf As ADODB.recordset
    Dim SeqParam As Long
    Dim CodUtil As String
    Dim CodGrup As String
    
    SelSQL = "SELECT nome_obj, nome_form, descr_prop, val_prop FROM " & cParamAcessos & " WHERE "
    InsSQL = "INSERT INTO " & cParamAcessos & " ( seq_param_acess, cod_util, cod_grupo, nome_obj, nome_form, descr_prop, val_prop, user_cri, dt_cri ) VALUES "
    
    If OpOG.value = True And OpDG.value = True Then
        'Grupo  -> Grupo
        
        If CbOrigG.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Deve escolher o grupo de origem!", vbOKOnly + vbExclamation, "Transferência"
            Exit Sub
        End If
        If CbDestG.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Deve escolher o grupo de destino!", vbOKOnly + vbExclamation, "Transferência"
            Exit Sub
        End If
        If CbDestG.ItemData(CbDestG.ListIndex) = CbOrigG.ItemData(CbOrigG.ListIndex) Then
            BG_Mensagem mediMsgBox, "O grupo de destino deve ser diferente do grupo de origem!", vbOKOnly + vbExclamation, "Transferência"
            Exit Sub
        End If
        
        SelSQL = SelSQL & " cod_grupo = " & CbOrigG.ItemData(CbOrigG.ListIndex)
        CodUtil = "null"
        CodGrup = CbDestG.ItemData(CbDestG.ListIndex)
    
    ElseIf OpOU.value = True And OpDG.value = True Then
        'Utilizador -> Grupo
        
        If CbOrigU.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Deve escolher o utilizador de origem!", vbOKOnly + vbExclamation, "Transferência"
            Exit Sub
        End If
        If CbDestG.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Deve escolher o grupo de destino!", vbOKOnly + vbExclamation, "Transferência"
            Exit Sub
        End If
        
        SelSQL = SelSQL & " cod_util = " & CbOrigU.ItemData(CbOrigU.ListIndex)
        CodUtil = "null"
        CodGrup = CbDestG.ItemData(CbDestG.ListIndex)
        
    ElseIf OpOU.value = True And OpDU.value = True Then
        'Utilizador -> Utilizador
        
        If CbOrigU.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Deve escolher o utilizador de origem!", vbOKOnly + vbExclamation, "Transferência"
            Exit Sub
        End If
        If CbDestU.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Deve escolher o utilizador de destino!", vbOKOnly + vbExclamation, "Transferência"
            Exit Sub
        End If
        If CbDestU.ItemData(CbDestU.ListIndex) = CbOrigU.ItemData(CbOrigU.ListIndex) Then
            BG_Mensagem mediMsgBox, "O utilizador de destino deve ser diferente do utilizador de origem!", vbOKOnly + vbExclamation, "Transferência"
            Exit Sub
        End If
        
        SelSQL = SelSQL & " cod_util = " & CbOrigU.ItemData(CbOrigU.ListIndex)
        CodUtil = CbDestU.ItemData(CbDestU.ListIndex)
        CodGrup = "null"
        
    ElseIf OpOG.value = True And OpDU.value = True Then
        'Grupo -> Utilizador
        
        If CbOrigG.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Deve escolher o grupo de origem!", vbOKOnly + vbExclamation, "Transferência"
            Exit Sub
        End If
        If CbDestU.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Deve escolher o utilizador de destino!", vbOKOnly + vbExclamation, "Transferência"
            Exit Sub
        End If
        
        SelSQL = SelSQL & " cod_grupo = " & CbOrigG.ItemData(CbOrigG.ListIndex)
        CodUtil = CbDestU.ItemData(CbDestU.ListIndex)
        CodGrup = "null"
        
    End If
    
    BL_InicioProcessamento Me, "A transferir parametrizações de acesso..."
    
    Set RsTransf = New ADODB.recordset
    
    With RsTransf
        .Source = SelSQL
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .Open , gConexao
    End With
        
    While Not RsTransf.EOF
        SeqParam = BG_DaMAX(cParamAcessos, "seq_param_acess") + 1
        
        ValuesSQL = ""
        ValuesSQL = " (" & SeqParam & "," & CodUtil & "," & CodGrup & "," & _
             BL_TrataStringParaBD(BL_HandleNull(RsTransf!nome_obj, " ")) & "," & _
             BL_TrataStringParaBD(BL_HandleNull(RsTransf!nome_form, " ")) & "," & _
             BL_TrataStringParaBD(BL_HandleNull(RsTransf!descr_prop, " ")) & "," & _
             BL_TrataStringParaBD(BL_HandleNull(RsTransf!val_prop, " ")) & "," & _
             BL_TrataStringParaBD(gIdUtilizador) & "," & _
             BL_TrataDataParaBD(Bg_DaData_ADO) & " ) "

        gSQLError = 0
        gSQLISAM = 0
        BG_ExecutaQuery_ADO InsSQL & ValuesSQL
        
        
        RsTransf.MoveNext
    Wend
    
    RsTransf.Close
    Set RsTransf = Nothing
    
    BL_FimProcessamento Me, "Parametrizações de acesso tranferidas."

End Sub

Private Sub CbDestG_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbDestG.ListIndex = -1

End Sub

Private Sub CbDestU_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbDestU.ListIndex = -1

End Sub

Private Sub CbOrigG_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbOrigG.ListIndex = -1

End Sub

Private Sub CbOrigU_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbOrigU.ListIndex = -1

End Sub

Private Sub Form_Load()
    
    Inicializacoes

End Sub

Private Sub OpDG_Click()
    
    CbDestG.Visible = True
    CbDestU.Visible = False
    
    If CbOrigG.Visible = True Then
        BtTransf.Picture = GG.Picture
    Else
        BtTransf.Picture = UG.Picture
    End If

End Sub

Private Sub OpDU_Click()
    
    CbDestU.Visible = True
    CbDestG.Visible = False
    
    If CbOrigG.Visible = True Then
        BtTransf.Picture = GU.Picture
    Else
        BtTransf.Picture = UU.Picture
    End If

End Sub

Private Sub OpOG_Click()
    
    CbOrigG.Visible = True
    CbOrigU.Visible = False
    
    If CbDestG.Visible = True Then
        BtTransf.Picture = GG.Picture
    Else
        BtTransf.Picture = GU.Picture
    End If

End Sub

Private Sub OpOU_Click()
    
    CbOrigU.Visible = True
    CbOrigG.Visible = False
    
    If CbDestG.Visible = True Then
        BtTransf.Picture = UG.Picture
    Else
        BtTransf.Picture = UU.Picture
    End If

End Sub



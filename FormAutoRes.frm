VERSION 5.00
Begin VB.Form FormAutoRes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAutoRes"
   ClientHeight    =   4935
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7155
   Icon            =   "FormAutoRes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4935
   ScaleWidth      =   7155
   ShowInTaskbar   =   0   'False
   Begin VB.ListBox EcListaAnalises 
      Height          =   645
      Left            =   1200
      TabIndex        =   26
      Top             =   1200
      Width           =   4695
   End
   Begin VB.CommandButton BtPesquisaAnalises 
      Height          =   315
      Left            =   5880
      Picture         =   "FormAutoRes.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   25
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   1200
      Width           =   375
   End
   Begin VB.CommandButton BtRelaAR 
      Caption         =   "Atribuir Resultados"
      Height          =   1095
      Left            =   5880
      Picture         =   "FormAutoRes.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   1230
      TabIndex        =   1
      Top             =   180
      Width           =   1755
   End
   Begin VB.TextBox EcDescricao 
      Height          =   285
      Left            =   1230
      TabIndex        =   2
      Top             =   600
      Width           =   4575
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   1800
      TabIndex        =   14
      Top             =   5760
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   120
      TabIndex        =   3
      Top             =   2280
      Width           =   6885
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   120
      TabIndex        =   7
      Top             =   4050
      Width           =   6885
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   13
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   12
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   11
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   10
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4320
      TabIndex        =   6
      Top             =   5040
      Visible         =   0   'False
      Width           =   885
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   5
      Top             =   5040
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4320
      TabIndex        =   4
      Top             =   5400
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   0
      Top             =   5400
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.Label Label6 
      Caption         =   "Ana.Apagar"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   27
      Top             =   1200
      Width           =   855
   End
   Begin VB.Label Label13 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   3000
      TabIndex        =   23
      Top             =   5400
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   3000
      TabIndex        =   22
      Top             =   5040
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   240
      TabIndex        =   21
      Top             =   5400
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   240
      TabIndex        =   20
      Top             =   5040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   3000
      TabIndex        =   19
      Top             =   2040
      Width           =   1065
   End
   Begin VB.Label Label7 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   150
      TabIndex        =   18
      Top             =   2070
      Width           =   795
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo "
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   210
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o "
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   16
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   240
      TabIndex        =   15
      Top             =   5760
      Visible         =   0   'False
      Width           =   1485
   End
End
Attribute VB_Name = "FormAutoRes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind, Max  As Integer
    

Private Sub BtPesquisaAnalises_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_s"
    CWhere = "  (flg_invisivel is null or flg_invisivel = 0) "
    CampoPesquisa = "descr_ana_s"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana_s ", _
                                                                           " Simples")
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaAnalises.ListCount = 0 Then
                EcListaAnalises.AddItem BL_SelCodigo("sl_ana_s", "descr_ana_s", "seq_ana_s", resultados(i))
                EcListaAnalises.ItemData(0) = resultados(i)
            Else
                EcListaAnalises.AddItem BL_SelCodigo("sl_ana_s", "descr_ana_s", "seq_ana_s", resultados(i))
                EcListaAnalises.ItemData(EcListaAnalises.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub

Private Sub BtRelaAR_Click()

    If EcCodigo <> "" And EcDescricao <> "" Then
        Max = UBound(gFieldObjectProperties)
    
        ' Guardar as propriedades dos campos do form
        For Ind = 0 To Max
            ReDim Preserve FOPropertiesTemp(Ind)
            FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
            FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
            FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
            FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
            FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
            FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
        Next Ind
            
        FormRelAutRes.Show
    Else
        BG_Mensagem mediMsgBox, "N�o � possivel abrir a atribui��o sem primeiro escolher um c�digo de automatiza��o."
    End If
End Sub

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
    
End Sub
Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub

Private Sub EcDescricao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDescricao_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Private Sub Eclistaanalises_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And EcListaAnalises.ListCount > 0 Then     'Delete
        If EcListaAnalises.ListIndex > mediComboValorNull Then
            EcListaAnalises.RemoveItem (EcListaAnalises.ListIndex)
        End If
    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = " Automatiza��o de Resultados"
    Me.left = 540
    Me.top = 450
    Me.Width = 7290
    Me.Height = 5355 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_auto_res"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 7
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_auto_res"
    CamposBD(1) = "cod_auto_res"
    CamposBD(2) = "descr_auto_res"
    CamposBD(3) = "user_cri"
    CamposBD(4) = "dt_cri"
    CamposBD(5) = "user_act"
    CamposBD(6) = "dt_act"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcUtilizadorCriacao
    Set CamposEc(4) = EcDataCriacao
    Set CamposEc(5) = EcUtilizadorAlteracao
    Set CamposEc(6) = EcDataAlteracao
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    'TextoCamposObrigatorios(0) = "Codigo Sequencial"
    TextoCamposObrigatorios(1) = "C�digo da Automatiza��o"
    TextoCamposObrigatorios(2) = "Descri��o da Automatiza��o"
        
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_auto_res"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_auto_res", "descr_auto_res")
    NumEspacos = Array(22, 32)
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    gF_AUTORES = 1
    Inicializacoes

    DefTipoCampos
    Max = -1
    ReDim FOPropertiesTemp(0)

    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    gF_AUTORES = 0
    Set FormAutoRes = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    EcListaAnalises.Clear
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        PreencheAnalises
    End If
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_auto_res ASC,descr_auto_res ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'
        
        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        EcLista.ListIndex = EcLista.ListIndex + 1
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String

    gSQLError = 0
    gSQLISAM = 0
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_auto_res") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    GravaAnalises
    
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    GravaAnalises
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'

    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    
    LimpaCampos
    PreencheCampos

End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub

Private Sub GravaAnalises()
    Dim i As Integer
    On Error GoTo TrataErro
    Dim sSql As String
    
    sSql = "DELETE FROM sl_auto_res_ana_apagar WHERE cod_auto_res = " & BL_TrataStringParaBD(EcCodigo)
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To EcListaAnalises.ListCount - 1
        sSql = "INSERT INTO sl_auto_res_ana_apagar (cod_auto_res, cod_ana_s) VALUES( "
        sSql = sSql & BL_TrataStringParaBD(EcCodigo) & ", "
        sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("sl_ana_s", "cod_ana_s", "seq_ana_s", EcListaAnalises.ItemData(i)))
        sSql = sSql & ")"
        BG_ExecutaQuery_ADO sSql
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao GravaAnalises: " & Err.Description, Me.Name, "GravaAnalises", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheAnalises()
    Dim i As Integer
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    EcListaAnalises.Clear
    
    sSql = "SELECT x1.cod_auto_res, x1.cod_ana_s, x2.DESCR_ANA_S, x2.seq_ana_s"
    sSql = sSql & " FROM sl_auto_res_ana_apagar x1, sl_ana_s x2 "
    sSql = sSql & " WHERE cod_auto_res = " & BL_TrataStringParaBD(EcCodigo)
    sSql = sSql & " AND x1.cod_ana_s = x2.cod_ana_s "
    sSql = sSql & " ORDER BY ordem "
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = 1 Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        While Not rsAna.EOF
            EcListaAnalises.AddItem rsAna!descr_ana_s
            EcListaAnalises.ItemData(EcListaAnalises.NewIndex) = rsAna!seq_ana_s
            rsAna.MoveNext
        Wend
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao PreencheAnalises: " & Err.Description, Me.Name, "PreencheAnalises", True
    Exit Sub
    Resume Next
End Sub




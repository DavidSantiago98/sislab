VERSION 5.00
Begin VB.Form FormCorreccaoRequis 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   4575
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6150
   Icon            =   "FormCorreccaoRequis.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4575
   ScaleWidth      =   6150
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcSeqUtente 
      Height          =   285
      Left            =   1080
      TabIndex        =   15
      Top             =   3120
      Width           =   1215
   End
   Begin VB.CommandButton BtCorrige 
      Enabled         =   0   'False
      Height          =   800
      Left            =   5040
      Picture         =   "FormCorreccaoRequis.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   13
      ToolTipText     =   "Efectuar Correc��o"
      Top             =   1850
      Width           =   975
   End
   Begin VB.Frame Frame2 
      Caption         =   " N�mero corrigido"
      Height          =   855
      Left            =   120
      TabIndex        =   10
      Top             =   1800
      Width           =   4695
      Begin VB.TextBox EcReqDestino 
         Height          =   285
         Left            =   1200
         TabIndex        =   11
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Requisi��o :"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Requisi��o a corrigir"
      Height          =   1575
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5895
      Begin VB.TextBox EcEstado 
         BackColor       =   &H80000016&
         Enabled         =   0   'False
         Height          =   285
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   360
         Width           =   3255
      End
      Begin VB.TextBox EcNome 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   1080
         Width           =   4455
      End
      Begin VB.TextBox EcUtente 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2400
         TabIndex        =   5
         Top             =   720
         Width           =   1215
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4560
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   720
         Width           =   1095
      End
      Begin VB.ComboBox CbTipoUtente 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   720
         Width           =   1215
      End
      Begin VB.TextBox EcReqOrigem 
         Height          =   285
         Left            =   1200
         TabIndex        =   1
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   9
         Top             =   1080
         Width           =   405
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   8
         Top             =   720
         Width           =   465
      End
      Begin VB.Label LbData 
         AutoSize        =   -1  'True
         Caption         =   "Data Nasc."
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   3720
         TabIndex        =   7
         ToolTipText     =   "Data de Nascimento"
         Top             =   720
         Width           =   795
      End
      Begin VB.Label Label1 
         Caption         =   "Requisi��o :"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Label Label3 
      Caption         =   "Seq Utente :"
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   3120
      Width           =   1215
   End
End
Attribute VB_Name = "FormCorreccaoRequis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/11/2008
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim FlagLoad As Boolean
Dim TipoInf As String

Dim Estado As Integer

Private Sub DefTipoCampos()

    'Tipo VarChar
    EcReqOrigem.Tag = "200"
    EcReqDestino.Tag = "200"

    EcReqOrigem.MaxLength = 7
    EcReqDestino.MaxLength = 7
    
    If gNReqPreImpressa <> 1 Then
        BG_Mensagem mediMsgBox, "Op��o s� dispon�vel para etiquetas pr�-impressas!", vbInformation, "Correc��o de N�meros de Requisi��o"
    End If

End Sub

Public Sub FuncaoLimpar()

    LimpaCampos

End Sub


Private Sub EcReqDestino_Validate(Cancel As Boolean)
    
    If EcReqDestino.text <> "" Then
        VerificaRequisicao EcReqDestino
    End If

End Sub

Private Sub EcReqOrigem_Validate(Cancel As Boolean)

    Dim nome As String
    Dim seq As String

    If Trim(EcReqOrigem.text) <> "" Then
        Call PreencheDadosRequis(EcReqOrigem)
    End If

End Sub

Private Sub BtCorrige_Click()
    EfectuarCorrecao
End Sub

Private Sub Form_Activate()

    EventoActivate

End Sub

Private Sub Form_Deactivate()

    Call BL_FimProcessamento(Screen.ActiveForm, "")

End Sub

Private Sub Form_Load()

    EventoLoad

End Sub

Private Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN 0
    BL_Toolbar_BotaoEstado "Limpar", "Activo"

    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    PreencheValoresDefeito
    DefTipoCampos
    Set CampoActivo = Me.ActiveControl

    BG_ParametrizaPermissoes_ADO Me.Name

    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Private Sub Inicializacoes()

    Me.caption = " Correc��o de N�meros de Requisi��es"
    Me.left = 540
    Me.top = 450
    Me.Width = 6720
    Me.Height = 3510 ' Normal
    'Me.Height = 8330 ' Campos Extras

    Set CampoDeFocus = EcReqOrigem

End Sub

Private Sub LimpaCampos()

    Me.SetFocus

    CbTipoUtente.ListIndex = mediComboValorNull
    EcUtente.text = ""
    EcDataNasc.text = ""
    EcNome.text = ""
    EcSeqUtente.text = ""

    EcReqOrigem.text = ""
    EcReqDestino.text = ""
    EcEstado.text = ""
    BtCorrige.Enabled = False


    EcReqOrigem.SetFocus

End Sub

Private Sub Form_Unload(Cancel As Integer)

    BG_StackJanelas_Pop

    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormCorreccaoRequis = Nothing

End Sub

Private Sub PreencheValoresDefeito()

    BL_InicioProcessamento Me, "Carregar valores de defeito..."
    Call BG_PreencheComboBD_ADO("sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente)
    BL_FimProcessamento Me, ""

End Sub

Private Sub PreencheDadosRequis(NReq As String)

    Dim RegUtente As ADODB.recordset
    Dim sql As String
    Dim i As Integer
    
    If NReq <> "" Then

        Set RegUtente = New ADODB.recordset
        RegUtente.CursorLocation = adUseServer
        RegUtente.LockType = adLockReadOnly
        RegUtente.CursorType = adOpenForwardOnly
        RegUtente.ActiveConnection = gConexao
        sql = "SELECT sl_requis.n_req,sl_requis.seq_utente,t_utente, utente,Nome_ute,dt_nasc_ute, estado_req, descr_estado " & _
                " From sl_requis, sl_identif, sl_tbf_estado_req " & _
                " Where sl_requis.seq_utente = sl_identif.seq_utente " & _
                " And sl_requis.estado_req = sl_tbf_estado_req.cod_estado " & _
                " And n_req = " & Trim(EcReqOrigem)
        RegUtente.Source = sql
        RegUtente.Open
    
        If RegUtente.RecordCount = 0 Then
            BG_Mensagem mediMsgBox, "Requisi��o Inexistente!", vbInformation
            LimpaCampos
        Else
            CbTipoUtente.text = RegUtente.Fields("t_utente").value
            EcUtente.text = RegUtente.Fields("utente").value
            EcNome.text = RegUtente.Fields("nome_ute").value
            EcDataNasc.text = RegUtente.Fields("dt_nasc_ute").value
            EcEstado.text = RegUtente.Fields("descr_estado").value
            EcSeqUtente.text = RegUtente.Fields("seq_utente").value
            
            BtCorrige.Enabled = True
        End If
    
        RegUtente.Close
        Set RegUtente = Nothing
        
    End If

End Sub

Private Sub EfectuarCorrecao()
    Dim sql As String
    Dim rsReq As ADODB.recordset
    On Error GoTo TrataErro
    
    If EcReqDestino.text <> "" Then
        If VerificaRequisicao(EcReqOrigem) = True Then
            'Actualiza os Registos das Requisi��es,Diagn�sticos e Resultados associados
            BG_BeginTransaction
            BL_InicioProcessamento Me, "A transferir registos..."
            gSQLError = 0
            
            'edgar.parada LJMANSO-257 13.04.2018
            sql = "update sl_req_colheitas set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            '
            'Actualiza sl_marcacoes
            sql = "update sl_marcacoes set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza produtos da requisi��o
            sql = "update sl_req_prod set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza tubos da requisi��o
            sql = "update sl_req_tubo set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza recibos da requisi��o
            sql = "update sl_recibos set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza detalhe dos recibos da requisi��o
            sql = "update sl_recibos_det set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza recibos cancelados da requisi��o
            sql = "update sl_recibos_det_canc set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            
            'Actualiza diagn�sticos secund�rios da requisi��o
            sql = "update sl_diag_sec set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza terapeuticas e medica��es da requisi��o
            sql = "update sl_tm_ute set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza motivos da requisi��o
            sql = "update sl_motivo_req set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza m�dicos da requisi��o
            sql = "update sl_medicos_req set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza informa��o clinica da requisi��o
            sql = "update sl_inf_clinica set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza tubos da requisi��o
            sql = "update sl_falt_req set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza an�lises eliminadas da requisi��o
            sql = "update sl_ana_eliminadas set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza fila de espera da requisi��o
            sql = "update sl_fila_espera set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza observa��o do registo na requisi��o
            sql = "update sl_obs_ana_req set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            
            'Actualiza n�mero de requisi��o
            sql = "update sl_requis set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            
            'Actualiza n�mero de requisi��o
            sql = "update sl_ana_acrescentadas set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update sl_ana_alteracoes set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update sl_analises_in set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update sl_dados_fact set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update SL_CM_FIN set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update sl_cm_interf set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update sl_cm_tec set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update SL_ERESULTS set cod_original = " & EcReqDestino & " where cod_original = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update SL_IMPR_COMPLETAS set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update sl_obs_auto set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update sl_recibos_canc set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update sl_repeticoes set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update SL_REQ_ASSINATURA set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update SL_REQ_CANC set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update SL_REQ_EMAIL set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update sl_res_grafico set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            'Actualiza n�mero de requisi��o
            sql = "update sl_res_grafico_im set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza n�mero de requisi��o
            sql = "update sl_res_grafico_banda set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza n�mero de requisi��o
            sql = "update sl_obs_ana_req set n_req = " & EcReqDestino & " where n_req = " & EcReqOrigem
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Actualiza n�mero de requisi��o
            sql = "update fa_movi_fact  set episodio = " & BL_TrataStringParaBD(EcReqDestino) & " where episodio = " & BL_TrataStringParaBD(EcReqOrigem)
            
            BG_ExecutaQuery_ADO sql
            sql = "update fa_movi_resp  set episodio = " & BL_TrataStringParaBD(EcReqDestino) & " where episodio = " & BL_TrataStringParaBD(EcReqOrigem)
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'NELSONPAILVA UALIA-878
            sql = "update sl_req_ana_movi_fact set n_req = " & BL_TrataStringParaBD(EcReqDestino) & " where n_req = " & BL_TrataStringParaBD(EcReqOrigem)
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            '
            'Registo correc��o
            sql = "insert into sl_requis_corrigidas(n_req_origem, n_req_destino,seq_utente,user_cri, dt_cri, hr_cri) values (" & _
                    EcReqOrigem & "," & EcReqDestino & "," & EcSeqUtente & ",'" & gCodUtilizador & "'," & BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
            BG_ExecutaQuery_ADO sql
            If gSQLError <> 0 Then
                GoTo TrataErro
            End If
            
            'Se n�o deu nenhum erro!
            BG_CommitTransaction
            BL_FimProcessamento Me, ""
        
            'Limpa os Campos
            BG_Mensagem mediMsgBox, "Correc��o bem sucedida!", vbOKOnly + vbInformation, App.ProductName
            LimpaCampos
            
        End If
    
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro a efectuar a correc��o: " & sql & " - " & Err.Description, Me.Name, "EfectuarCorrecao", True
    BG_RollbackTransaction
    BL_FimProcessamento Me, ""
    Exit Sub
End Sub


Function VerificaRequisicao(NReq As String) As Boolean
    Dim RsVf As ADODB.recordset
    Dim sql As String
    
    VerificaRequisicao = False
    
    Set RsVf = New ADODB.recordset
    RsVf.CursorLocation = adUseServer
    RsVf.CursorType = adOpenStatic
    
    sql = "select n_req from sl_requis where n_req = " & EcReqDestino
    RsVf.Open sql, gConexao
    
    If RsVf.RecordCount = 0 Then
        Set RsVf = New ADODB.recordset
        RsVf.CursorLocation = adUseServer
        RsVf.CursorType = adOpenStatic
        
        sql = "select n_req from sl_realiza where n_req = " & EcReqOrigem
        RsVf.Open sql, gConexao
        If RsVf.RecordCount > 0 Then
            BG_Mensagem mediMsgBox, "A requisi��o de origem j� tem resultados!" & vbCrLf & " Deve primeiro apagar os resultados da requisi��o " & EcReqOrigem, vbInformation, gFormActivo.caption
            FuncaoLimpar
        Else
            RsVf.Close
            If gPassaRecFactus = mediSim Then
                sql = "SELECT * FROM fa_lin_fact WHERE episodio = " & BL_TrataStringParaBD(EcReqOrigem.text)
                RsVf.CursorLocation = adUseServer
                RsVf.CursorType = adOpenStatic
                RsVf.Open sql, gConexao
                If RsVf.RecordCount > 0 Then
                    BG_Mensagem mediMsgBox, "A requisi��o de origem j� tem dados faturados!" & vbCrLf & EcReqOrigem, vbInformation, gFormActivo.caption
                    FuncaoLimpar
                Else
                    VerificaRequisicao = True
                End If
            Else
                VerificaRequisicao = True
            End If
        End If
    Else
        MsgBox "Requisi��o j� existente! N�o � poss�vel efectuar a correc��o! " & vbCrLf & "Deve actualizar a requisi��o " & EcReqDestino & ""
        EcReqDestino.text = ""
    End If
    RsVf.Close
    Set RsVf = Nothing
    
End Function


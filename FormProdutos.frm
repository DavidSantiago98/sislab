VERSION 5.00
Begin VB.Form FormProdutos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormProdutos"
   ClientHeight    =   6360
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7125
   Icon            =   "FormProdutos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6360
   ScaleWidth      =   7125
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodSinave 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   5760
      TabIndex        =   40
      Top             =   840
      Width           =   975
   End
   Begin VB.TextBox EcPrazoVE 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   5760
      TabIndex        =   37
      Top             =   1200
      Width           =   975
   End
   Begin VB.CheckBox CkProduto 
      Caption         =   "Permite Introdu��o Volume"
      Height          =   255
      Left            =   360
      TabIndex        =   36
      Top             =   1560
      Width           =   2535
   End
   Begin VB.TextBox EcOrdem 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3240
      TabIndex        =   35
      Top             =   120
      Width           =   615
   End
   Begin VB.TextBox EcEspecifObrig 
      Height          =   285
      Left            =   6240
      TabIndex        =   33
      Top             =   6120
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CheckBox ChEspecifObrig 
      Caption         =   "Obrigat�rio indicar especifica��o"
      Height          =   255
      Left            =   4440
      TabIndex        =   31
      Top             =   1560
      Width           =   2655
   End
   Begin VB.TextBox EcEspecif 
      Height          =   285
      Left            =   6240
      TabIndex        =   28
      Top             =   5760
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ComboBox CbEspecif 
      Height          =   315
      Left            =   360
      Style           =   2  'Dropdown List
      TabIndex        =   27
      Top             =   1200
      Width           =   3135
   End
   Begin VB.TextBox EcEtiqueta 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   5760
      TabIndex        =   2
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      TabIndex        =   0
      Top             =   120
      Width           =   1425
   End
   Begin VB.TextBox EcDescricao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      TabIndex        =   1
      Top             =   480
      Width           =   5715
   End
   Begin VB.TextBox EcCodSequencial 
      Height          =   285
      Left            =   6480
      TabIndex        =   15
      Top             =   5400
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ListBox EcLista 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2130
      Left            =   120
      TabIndex        =   3
      Top             =   2040
      Width           =   6885
   End
   Begin VB.Frame Frame2 
      Height          =   825
      Left            =   120
      TabIndex        =   8
      Top             =   4200
      Width           =   6885
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   14
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   13
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   12
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   11
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4200
      TabIndex        =   7
      Top             =   5400
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   6
      Top             =   5400
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4200
      TabIndex        =   5
      Top             =   5760
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   4
      Top             =   5760
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Label Label13 
      Caption         =   "Cod. SINAVE"
      Height          =   255
      Index           =   3
      Left            =   4080
      TabIndex        =   39
      Top             =   960
      Width           =   1575
   End
   Begin VB.Label Label13 
      Caption         =   "Prazo Restri��o VE"
      Height          =   255
      Index           =   2
      Left            =   4080
      TabIndex        =   38
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label Label13 
      Caption         =   "Ordem"
      Height          =   255
      Index           =   1
      Left            =   2640
      TabIndex        =   34
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label16 
      Caption         =   "EspecifObrig"
      Height          =   255
      Left            =   4920
      TabIndex        =   32
      Top             =   6120
      Width           =   1095
   End
   Begin VB.Label Label15 
      Caption         =   "Especifica��o"
      Height          =   255
      Left            =   5520
      TabIndex        =   30
      Top             =   1800
      Width           =   1095
   End
   Begin VB.Label Label14 
      Caption         =   "Cod. Especifica. :"
      Height          =   225
      Left            =   4920
      TabIndex        =   29
      Top             =   5760
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.Label Label2 
      Caption         =   "Especifica��o prefer�ncial "
      Height          =   255
      Left            =   120
      TabIndex        =   26
      Top             =   960
      Width           =   2175
   End
   Begin VB.Label Label13 
      Caption         =   "C�digo para etiqueta"
      Height          =   255
      Index           =   0
      Left            =   4080
      TabIndex        =   25
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   2760
      TabIndex        =   24
      Top             =   5760
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2760
      TabIndex        =   23
      Top             =   5400
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   120
      TabIndex        =   22
      Top             =   5760
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   5400
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo "
      Height          =   255
      Left            =   120
      TabIndex        =   20
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "Descri��o "
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Sequencial : "
      Height          =   225
      Left            =   4920
      TabIndex        =   18
      Top             =   5400
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.Label Label9 
      Caption         =   "Descri��o"
      Height          =   255
      Left            =   2880
      TabIndex        =   17
      Top             =   1800
      Width           =   855
   End
   Begin VB.Label Label7 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   1800
      Width           =   645
   End
End
Attribute VB_Name = "FormProdutos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim Clicking As Boolean

Public rs As ADODB.recordset

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind, Max  As Integer

Private Sub ChRemovidos_Click()
    If ListarRemovidos = False Then
        ListarRemovidos = True
    Else
        ListarRemovidos = False
    End If
End Sub

Private Sub CbEspecif_Click()
    BL_ColocaComboTexto "sl_especif", "seq_especif", "cod_especif", EcEspecif, CbEspecif
End Sub

Private Sub CbEspecif_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then CbEspecif.ListIndex = -1
End Sub

Private Sub ChEspecifObrig_Click()
    If Clicking = True Then Exit Sub
    Clicking = True
    If ChEspecifObrig.value = 0 Then
        EcEspecifObrig = ""
    Else
        EcEspecifObrig = "S"
    End If
    Clicking = False
End Sub

Private Sub EcCodigo_LostFocus()
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub

Private Sub EcEspecif_Change()
    BL_ColocaTextoCombo "sl_especif", "seq_especif", "cod_especif", EcEspecif, CbEspecif
End Sub

Private Sub EcEspecifObrig_Change()
    If EcEspecifObrig = "" Then
        ChEspecifObrig.value = 0
    Else
        ChEspecifObrig.value = 1
    End If
End Sub

Private Sub EcEtiqueta_LostFocus()
    BG_ValidaTipoCampo_ADO Me, EcEtiqueta
End Sub

Private Sub EcLista_Click()
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
End Sub

Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDescricao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub ChEspecifObrig_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDescricao_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Produtos"
    Me.left = 540
    Me.top = 450
    Me.Width = 7215
    Me.Height = 5490 ' Normal
    
    NomeTabela = "sl_produto"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 14
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_produto"
    CamposBD(1) = "cod_produto"
    CamposBD(2) = "descr_produto"
    CamposBD(3) = "cod_etiq"
    CamposBD(4) = "user_cri"
    CamposBD(5) = "dt_cri"
    CamposBD(6) = "user_act"
    CamposBD(7) = "dt_act"
    CamposBD(8) = "cod_especif"
    CamposBD(9) = "especif_obrig"
    CamposBD(10) = "ordem"
    CamposBD(11) = "permite_volume"
    CamposBD(12) = "prazo_ve"
    'BRUNODSANTOS 22.02.2017 - Glintt-HS-14510
    CamposBD(13) = "cod_sinave"
    '
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSequencial
    Set CamposEc(1) = EcCodigo
    Set CamposEc(2) = EcDescricao
    Set CamposEc(3) = EcEtiqueta
    Set CamposEc(4) = EcUtilizadorCriacao
    Set CamposEc(5) = EcDataCriacao
    Set CamposEc(6) = EcUtilizadorAlteracao
    Set CamposEc(7) = EcDataAlteracao
    Set CamposEc(8) = EcEspecif
    Set CamposEc(9) = EcEspecifObrig
    Set CamposEc(10) = EcOrdem
    Set CamposEc(11) = CkProduto
    Set CamposEc(12) = EcPrazoVE
    'BRUNODSANTOS 22.02.2017 - Glintt-HS-14510
    Set CamposEc(13) = EcCodSinave
    '
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo do Produto"
    TextoCamposObrigatorios(2) = "Descri��o do Produto"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_produto"
    Set ChaveEc = EcCodSequencial
    
    CamposBDparaListBox = Array("cod_produto", "descr_produto", "cod_especif")
    NumEspacos = Array(8, 40, 10)
    CkProduto.value = vbGrayed
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    gF_PRODUTOS = 1
    
    Max = -1
    ReDim FOPropertiesTemp(0)
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormProdutos = Nothing
    gF_PRODUTOS = 0
End Sub

Sub LimpaCampos()
    Me.SetFocus

    CbEspecif.ListIndex = -1
    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    CkProduto.value = vbGrayed
End Sub

Sub DefTipoCampos()
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_especif", "seq_especif", "descr_especif", CbEspecif, mediDescComboDesignacao
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
    End If
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY cod_produto ASC, descr_produto ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    Dim SQLQuery As String
    Dim sSql As String
    Dim RsOrdem As New ADODB.recordset
    
    gSQLError = 0
    gSQLISAM = 0
    
    If Trim(EcOrdem) = "" Then
        gMsgTitulo = "Ordem do Produto"
        gMsgMsg = "Campo ORDEM n�o pode ser vazio"
        BG_Mensagem mediMsgBox, gMsgMsg, , gMsgTitulo
        Exit Sub
    Else
        sSql = "SELECT descr_produto FROM sl_produto where ordem = " & EcOrdem
        RsOrdem.CursorType = adOpenStatic
        RsOrdem.CursorLocation = adUseServer
        RsOrdem.Open sSql, gConexao
        If RsOrdem.RecordCount > 0 Then
            gMsgTitulo = "Ordem do Produto"
            gMsgMsg = "O produto " & BL_HandleNull(RsOrdem!descr_produto, "") & " j� tem essa ordem. Ter� que escolher uma que n�o exista."
            BG_Mensagem mediMsgBox, gMsgMsg, , gMsgTitulo
            RsOrdem.Close
            Set RsOrdem = Nothing
            Exit Sub
        End If
        RsOrdem.Close
        Set RsOrdem = Nothing
    End If
    
    EcCodSequencial = BG_DaMAX(NomeTabela, "seq_produto") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()
    Dim sSql As String
    Dim RsOrdem As New ADODB.recordset
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    If Trim(EcOrdem) = "" Then
        gMsgTitulo = "Ordem do Produto"
        gMsgMsg = "Campo ORDEM n�o pode ser vazio"
        BG_Mensagem mediMsgBox, gMsgMsg, , gMsgTitulo
        Exit Sub
    Else
        sSql = "SELECT descr_produto FROM sl_produto where ordem = " & EcOrdem & " AND cod_produto <> " & BL_TrataStringParaBD(EcCodigo)
        RsOrdem.CursorType = adOpenStatic
        RsOrdem.CursorLocation = adUseServer
        RsOrdem.Open sSql, gConexao
        If RsOrdem.RecordCount > 0 Then
            gMsgTitulo = "Ordem do Produto"
            gMsgMsg = "O produto " & BL_HandleNull(RsOrdem!descr_produto, "") & " j� tem essa ordem. Ter� que escolher uma que n�o exista."
            BG_Mensagem mediMsgBox, gMsgMsg, , gMsgTitulo
            RsOrdem.Close
            Set RsOrdem = Nothing
            Exit Sub
        End If
        RsOrdem.Close
        Set RsOrdem = Nothing
    End If
    
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Static para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub



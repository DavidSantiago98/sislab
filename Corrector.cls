VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Corrector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"

'variaveis privadas
Dim sPalavrasPt() As String
Dim sPalavrasCod() As String
Public LocalizacaoCorrector As String


Public Function VerificaTexto(TextoAVerificar As String) As String
    Dim sTexto As String
    Dim sRetorno As String
    
    On Error GoTo TrataErro
    
    sRetorno = TextoAVerificar
    
    If TextoAVerificar = "" Then
        Exit Function
    End If
    
    sTexto = TextoAVerificar
    sRetorno = VerificaOrtografia(sTexto)
    
    VerificaTexto = sRetorno
    
    Exit Function
TrataErro:
    MsgBox "Erro: " & Err.Number & " - " & Err.Description, vbInformation
    VerificaTexto = sRetorno
End Function


Private Function VerificaOrtografia(sTexto As String) As String
    'ID do Portugu�s
    Const Idioma_Pt = 2070
    Dim iResp As Integer

    Dim sRetorno As String
    Dim Documento As Variant
    Dim bPassouWordPt As Boolean
    Dim bPassouIdiomaPt As Boolean

    sRetorno = sTexto

    On Error GoTo Trata_Erro
    
    Set Documento = CreateObject("Word.Application")     'Criar um objecto word
    Documento.Visible = False                            'Esconder o objecto
    Documento.Documents.Add                              'Adicionar um documento
    'Testar se o word est� em Portugu�s
    
    If Documento.Language <> Idioma_Pt Then
        GoTo Trata_Erro
    Else
        bPassouWordPt = True
    End If

    If Documento.ActiveDocument.AttachedTemplate.LanguageID <> Idioma_Pt Then
          Documento.ActiveDocument.AttachedTemplate.LanguageID = Idioma_Pt
          Documento.ActiveDocument.AttachedTemplate.NoProofing = False
          Documento.Selection.LanguageID = Idioma_Pt
          Documento.Selection.NoProofing = False
          Documento.Application.CheckLanguage = True
    End If
    bPassouIdiomaPt = True
    
    Clipboard.Clear                                      'Limpar o Clipboard
    Clipboard.SetText sTexto                             'Copiar texto para o Clipboard
    Documento.Selection.Paste                            'Copiar texto do Clipboard
    Clipboard.Clear                                      'Limpar o Clipboard
    Documento.ActiveDocument.CheckSpelling               'Chamar o corrector ortogr�fico
    Documento.Visible = False                            'esconder o objecto
    Documento.ActiveDocument.Select                      'Selecionar o texto
    Documento.Selection.Cut                              'coloca-lo no clipboard
    Documento.Visible = False                            'esconder o objecto
    sRetorno = Clipboard.GetText                         'retirar o texto do clippoard
    Clipboard.Clear                                      'Limpar o Clipboard
    Documento.ActiveDocument.Close SaveChanges:= _
    wdDoNotSaveChanges                                   'fechar o objecto sem gravar
    Documento.Quit
    Set Documento = Nothing                              'libertar referencia
    
    VerificaOrtografia = sRetorno
    
    Exit Function

Trata_Erro:

    If bPassouWordPt = False Then
        iResp = MsgBox("O idioma do Word n�o � Portugu�s." & vbCrLf & vbCrLf & "Deseja prosseguir?", vbQuestion + vbYesNo)
        If iResp = vbYes Then
            Resume Next
        End If
    End If

    If bPassouIdiomaPt = False Then
        iResp = MsgBox("N�o foi possivel definir o idioma para Portugu�s." & vbCrLf & vbCrLf & "Deseja prosseguir?", vbQuestion + vbYesNo)
        If iResp = vbYes Then
            Resume Next
        End If
    End If

    If Not Documento Is Nothing Then
        Documento.ActiveDocument.Close SaveChanges:= _
         wdDoNotSaveChanges                                   'fechar o objecto sem gravar
        Documento.Quit
        Set Documento = Nothing
    End If
    
    VerificaOrtografia = sRetorno
End Function

Public Sub VerificaTextoObjecto(ObjectoAVerificar As Object, Optional TextoSelecionado As Boolean = False)
    Dim sTexto As String
    Dim sTextoCorrigido As String
    Dim sTextoAnt() As String
    Dim sTextoNovo() As String
    Dim iContador As Integer
    Dim jContador As Integer
    
    On Error GoTo TrataErro
        
    If TypeOf ObjectoAVerificar Is TextBox Then
        If ObjectoAVerificar.Text <> "" Then
            sTexto = ObjectoAVerificar.Text
            sTextoCorrigido = VerificaOrtografia(sTexto)
        Else
            Exit Sub
        End If
    ElseIf TypeOf ObjectoAVerificar Is RichTextBox Then
        If LocalizacaoCorrector = "" Then
            MsgBox "Primeiro ter� de inicializar o corrector.", vbInformation
            Exit Sub
        Else
            If TextoSelecionado = False Then
                sTexto = ObjectoAVerificar.Text
            Else
                sTexto = ObjectoAVerificar.SelText
            End If
            
            If sTexto <> "" Then
                sTexto = Replace(sTexto, vbCrLf, " ")
                sTexto = Replace(sTexto, vbTab, " ")
                sTexto = Trim(sTexto)
                sTextoCorrigido = VerificaOrtografia(Trim(sTexto))
            Else
                Exit Sub
            End If
        End If
    Else
        MsgBox "Objecto inv�lido.", vbCritical
        Exit Sub
    End If
    

    If sTexto = sTextoCorrigido Then
        'N�o h� diferen�as
        MsgBox "Verifica��o ortogr�fica conclu�da.", vbInformation
        Exit Sub
    End If
    
    If TypeOf ObjectoAVerificar Is TextBox Then
        'o objecto � uma textBox
        ObjectoAVerificar.Text = Replace(ObjectoAVerificar.Text, sTexto, sTextoCorrigido)
    Else
        'O objecto � uma RichTextBox
        Dim bDiferencas As Boolean
        
        sTextoAnt = Split(sTexto, " ")
        sTextoNovo = Split(sTextoCorrigido, " ")
        
        If UBound(sTextoAnt) <> UBound(sTextoNovo) Then
            'existem diferen�as entre o numero de palavras
            bDiferencas = True
        End If
        
        jContador = 0
        For iContador = 0 To UBound(sTextoAnt)
            
            If sTextoAnt(iContador) <> sTextoNovo(iContador) Then
                If (UBound(sTextoAnt) - iContador) - (UBound(sTextoNovo) - jContador) = 0 Then
                    bDiferencas = False
                End If
                
                
                If bDiferencas = True Then
                    'Numero de palavras diferente
                    If Len(Trim(sTextoAnt(iContador))) = Len(Trim(sTextoNovo(jContador))) + Len(Trim(sTextoNovo(jContador + 1))) Then
                        'a palavra anterior foi separada em duas
                        sTextoAnt(iContador) = TestaTexto(sTextoAnt(iContador))
                        sTextoNovo(jContador) = TestaTexto(sTextoNovo(jContador)) & " " & TestaTexto(sTextoNovo(jContador + 1))
                        
                        ObjectoAVerificar.TextRTF = Replace(ObjectoAVerificar.TextRTF, sTextoAnt(iContador), sTextoNovo(jContador))
                        jContador = jContador + 1
                    Else
                        sTextoAnt(iContador) = TestaTexto(sTextoAnt(iContador))
                        sTextoNovo(jContador) = TestaTexto(sTextoNovo(jContador))
            
                        ObjectoAVerificar.TextRTF = Replace(ObjectoAVerificar.TextRTF, sTextoAnt(iContador), sTextoNovo(jContador))
                    End If
                Else
                    sTextoAnt(iContador) = TestaTexto(sTextoAnt(iContador))
                    sTextoNovo(jContador) = TestaTexto(sTextoNovo(jContador))
        
                    ObjectoAVerificar.TextRTF = Replace(ObjectoAVerificar.TextRTF, sTextoAnt(iContador), sTextoNovo(jContador))
                End If

            End If
            jContador = jContador + 1
        Next iContador
    End If
    
    Exit Sub
TrataErro:
    MsgBox "Erro: " & Err.Number & " - " & Err.Description, vbInformation
End Sub

Private Function TestaTexto(sTexto As String) As String
    Dim i As Integer
    Dim sRetorno As String
    
    On Error GoTo Trata_Erro
    
    sRetorno = sTexto
    
    For i = 0 To UBound(sPalavrasPt)
        If InStr(1, sTexto, sPalavrasPt(i), vbTextCompare) > 0 Then
            sTexto = Replace(sTexto, sPalavrasPt(i), sPalavrasCod(i))
        End If
    Next i
    
    If sTexto <> sRetorno Then
        sRetorno = sTexto
    End If
    
    TestaTexto = sRetorno
    Exit Function
Trata_Erro:
    TestaTexto = sRetorno
End Function


Public Sub InicializaCorrector(LocalFicheiro As String)
    If LocalFicheiro = "" And LocalizacaoCorrector = "" Then
        MsgBox "Ter� de indicar a localiza��o do ficheiro!", vbInformation
        Exit Sub
    End If
    
    If Dir(LocalFicheiro) = "" Then
        MsgBox "Ficheiro de formata��es n�o encontrado.", vbCritical
        Exit Sub
    End If
       
    
    Dim i As Integer
    Dim iFile As Integer
    Dim sSeparador() As String
    Dim sLinha As String

    On Error GoTo Trata_Erro
    
    iFile = FreeFile
    Open LocalFicheiro For Input As #iFile
    
    i = 1
    Do While Not EOF(1)
        Line Input #1, sLinha
        sSeparador = Split(sLinha, "=")
        If UBound(sSeparador) = 1 Then
            ReDim Preserve sPalavrasPt(i - 1)
            ReDim Preserve sPalavrasCod(i - 1)
            
            sPalavrasPt(i - 1) = sSeparador(0)
            sPalavrasCod(i - 1) = sSeparador(1)
            
            i = i + 1
        End If
    Loop
    
    Close #1
    
    LocalizacaoCorrector = LocalFicheiro
    
    Exit Sub
Trata_Erro:
    If Not FolhaExcel Is Nothing Then
        Set FolhaExcel = Nothing
    End If
    
    If Not xl Is Nothing Then
        Set xl = Nothing
    End If
       
End Sub


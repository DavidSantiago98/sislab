Attribute VB_Name = "ControloQualidade"
Option Explicit
Public Type ResultadosCQ
    seq_apar As Integer
    descr_apar As String
    cod_lote As String
    descr_lote As String
    cod_ana_s As String
    descr_ana_s As String
    cod_nivel As String
    descr_nivel As String
    num_testes As Integer
    media As Double
    desvio_padrao As Double
    media_lote As Double
    desvio_padrao_lote As Double
    cv As Double
    bias As Double
    etc As Double
    id As Double
    desvio_padrao_percent_lote As Double
    imprecisao As Double
    inexatidao As Double
    eta As Double
    flg_selec As Boolean
End Type
Public CQestrutRes() As ResultadosCQ
Public CQtotalRes As Long

' ------------------------------------------------------------------------------

' DADO UM LOTE CALCULA MEDIA, DESVIO PADRAO E NUMERO DE AMOSTRAS
' desvio padrao = Sqrt(E((Xn - Media) ^ 2))/n-1)
' media = E(Controlos aceites/n)
' ------------------------------------------------------------------------------
Public Sub CQ_CalculaValores(codLote As String, cod_ana_s As String, ByRef numAmostras As Integer, ByRef media As Double, _
                             ByRef desvioPadrao As Double, dt_ini As String, dt_fim As String)
    Dim sSql As String
    Dim rsLote As New ADODB.recordset
    Dim soma As Double
    Dim valores() As Double
    Dim i As Integer
    Dim aux1 As Double
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_cq_res WHERE cod_lote = " & BL_TrataStringParaBD(codLote) & " AND cod_ana_S = " & BL_TrataStringParaBD(cod_ana_s)
    sSql = sSql & " AND (flg_excluido IS NULL OR flg_excluido = 0) "
    sSql = sSql & " AND dt_cri between " & BL_TrataDataParaBD(dt_ini) & " AND " & BL_TrataDataParaBD(dt_fim)
    rsLote.CursorLocation = adUseServer
    rsLote.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsLote.Open sSql, gConexao
    If rsLote.RecordCount > 0 Then
        soma = 0
        numAmostras = 0
        ReDim valores(rsLote.RecordCount)
        While Not rsLote.EOF
            soma = soma + BL_HandleNull(rsLote!resultado, 0)
            numAmostras = numAmostras + 1
            valores(numAmostras) = BL_HandleNull(rsLote!resultado, 0)
            rsLote.MoveNext
        Wend
        media = Round(soma / numAmostras, 2)
        
        'DESVIO PADRAO
        aux1 = 0
        For i = 1 To rsLote.RecordCount
            aux1 = aux1 + ((valores(i) - media) * (valores(i) - media))
        Next
        If numAmostras > 1 Then
            aux1 = aux1 / (numAmostras - 1)
            desvioPadrao = Round(Sqr(aux1), 2)
        Else
            desvioPadrao = 0
        End If
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ERRO ao calcular valores: " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_CalculaValores", True
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------

' DADO DESVIO PADRAO E MEDIA CALCULA COEFICIENTE DE VARIACAO
' SD * 100 / Media
' ------------------------------------------------------------------------------
Public Function CQ_CalculoCV(media As Double, desvioPadrao As Double) As Double
    On Error GoTo TrataErro
    If media = 0 Then
        CQ_CalculoCV = 0
        Exit Function
    End If
    CQ_CalculoCV = desvioPadrao * 100 / media
Exit Function
TrataErro:
    CQ_CalculoCV = 0
    BG_LogFile_Erros "ERRO ao calcular CV: " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_CalculaValores", True
    Exit Function
    Resume Next
End Function



' ------------------------------------------------------------------------------

' ERRO SISTEMATICO
' ((Media - Media Alvo)/Media Alvo) * 100
' ------------------------------------------------------------------------------
Public Function CQ_CalculoBIAS(media As Double, mediaAlvo As Double) As Double
    On Error GoTo TrataErro
    If mediaAlvo = 0 Then
        CQ_CalculoBIAS = 0
        Exit Function
    End If
    CQ_CalculoBIAS = ((media - mediaAlvo) / mediaAlvo) * 100
Exit Function
TrataErro:
    CQ_CalculoBIAS = 0
    BG_LogFile_Erros "ERRO ao calcular BIAS: " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_CalculoBIAS", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' ERRO TOTAL
' Abs(BIAS%) + (1.65 * CV%)
' ------------------------------------------------------------------------------
Public Function CQ_CalculoETC(bias As Double, cv As Double) As Double
    On Error GoTo TrataErro
    CQ_CalculoETC = Abs(bias) + (1.65 * cv)
Exit Function
TrataErro:
    CQ_CalculoETC = 0
    BG_LogFile_Erros "ERRO ao calcular ETC: " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_CalculoETC", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' Indice de desvio
' (Abs(Media - Media Alvo)/ desvioPadraoAlvo
' ------------------------------------------------------------------------------
Public Function CQ_CalculoID(media As Double, mediaAlvo As Double, desvioPadraoAlvo As Double) As Double
    On Error GoTo TrataErro
    If desvioPadraoAlvo = 0 Then
        CQ_CalculoID = 0
        Exit Function
    End If
    CQ_CalculoID = Abs(media - mediaAlvo) / desvioPadraoAlvo
Exit Function
TrataErro:
    CQ_CalculoID = 0
    BG_LogFile_Erros "ERRO ao calcular ID: " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_CalculoID", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' IMPRECISAO
' 0.25 * eta
' ------------------------------------------------------------------------------
Public Function CQ_CalculoIMP(eta As Double) As Double
    On Error GoTo TrataErro
    CQ_CalculoIMP = 0.25 * eta
Exit Function
TrataErro:
    CQ_CalculoIMP = 0
    BG_LogFile_Erros "ERRO ao calcular IMP: " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_CalculoIMP", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

' inexatidao
' 0.5 * eta
' ------------------------------------------------------------------------------
Public Function CQ_CalculoINE(eta As Double) As Double
    On Error GoTo TrataErro
    CQ_CalculoINE = 0.5 * eta
Exit Function
TrataErro:
    CQ_CalculoINE = 0
    BG_LogFile_Erros "ERRO ao calcular INE: " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_CalculoINE", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------


' ------------------------------------------------------------------------------
Public Function CQ_CalculoPercentSD(eta As Double) As Double
    On Error GoTo TrataErro
    CQ_CalculoPercentSD = eta * 0.33
Exit Function
TrataErro:
    CQ_CalculoPercentSD = 0
    BG_LogFile_Erros "ERRO ao calcular SD%: " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_CalculoPercentSD", True
    Exit Function
    Resume Next
End Function

' ------------------------------------------------------------------------------

'  DEVOLVE VALOR ECA CODIFICADO PARA A ANLAISE

' ------------------------------------------------------------------------------
Public Function CQ_RetornaETa(codAnaS As String) As Double
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    CQ_RetornaETa = 0
    
    sSql = "SELECT eta FROM sl_ana_s WHERE cod_ana_S = " & BL_TrataStringParaBD(codAnaS)
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount > 0 Then
        CQ_RetornaETa = BL_HandleNull(rsAna!eta, 0)
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "ERRO ao retornar ETA: " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_RetornaETa", True
    CQ_RetornaETa = 0
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------------------------------------------------------

' 1(2s) - Quando 1 dos valores do controle calculado ultrapassa +2DP ou -2DP

' -------------------------------------------------------------------------------------------------------------------
Public Function CQ_Calcula12s(rsLote As ADODB.recordset, cod_ana_s As String, cod_lote As String, media As Double, desvio As Double) As Boolean
    Dim desvios2 As Double
    On Error GoTo TrataErro
    desvios2 = 2 * desvio
    rsLote.MoveFirst
    While Not rsLote.EOF
        If BL_HandleNull(rsLote!resultado, 0) > media + desvios2 Then
            CQ_Calcula12s = False
            Exit Function
        ElseIf BL_HandleNull(rsLote!resultado, 0) < (media - desvios2) Then
            CQ_Calcula12s = False
            Exit Function
        End If
        rsLote.MoveNext
    Wend
    CQ_Calcula12s = True
Exit Function
TrataErro:
    CQ_Calcula12s = False
    BG_LogFile_Erros "ERRO ao calcular 1(2s): " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_Calcula12s", True
    Exit Function
    Resume Next
End Function


' -------------------------------------------------------------------------------------------------------------------

' 1(3s) - Quando 1 dos valores do controle calculado ultrapassa +3DP ou -3DP

' -------------------------------------------------------------------------------------------------------------------
Public Function CQ_Calcula13s(rsLote As ADODB.recordset, cod_ana_s As String, cod_lote As String, media As Double, desvio As Double) As Boolean
    Dim desvios2 As Double
    On Error GoTo TrataErro
    desvios2 = 3 * desvio
    rsLote.MoveFirst
    While Not rsLote.EOF
        If BL_HandleNull(rsLote!resultado, 0) > media + desvios2 Then
            CQ_Calcula13s = False
            Exit Function
        ElseIf BL_HandleNull(rsLote!resultado, 0) < (media - desvios2) Then
            CQ_Calcula13s = False
            Exit Function
        End If
        rsLote.MoveNext
    Wend
    CQ_Calcula13s = True
Exit Function
TrataErro:
    CQ_Calcula13s = False
    BG_LogFile_Erros "ERRO ao calcular 1(3s): " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_Calcula13s", True
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------------------------------------------------------

' 2(2s) - Quando 2 dos valores consecutivos do controle calculado ultrapassam +2DP ou -2DP

' -------------------------------------------------------------------------------------------------------------------
Public Function CQ_Calcula22s(rsLote As ADODB.recordset, cod_ana_s As String, cod_lote As String, media As Double, desvio As Double) As Boolean
    Dim desvios As Double
    Dim Positivos As Integer
    Dim negativos As Integer
    On Error GoTo TrataErro
    desvios = 2 * desvio
    Positivos = 0
    negativos = 0
    rsLote.MoveFirst
    While Not rsLote.EOF
        If BL_HandleNull(rsLote!resultado, 0) > media + desvios Then
            Positivos = Positivos + 1
            negativos = 0
        ElseIf BL_HandleNull(rsLote!resultado, 0) < (media - desvios) Then
            Positivos = 0
            negativos = negativos + 1
        Else
            Positivos = 0
            negativos = 0
        End If
        If Positivos >= 2 Or negativos >= 2 Then
            CQ_Calcula22s = False
            Exit Function
        End If
        rsLote.MoveNext
    Wend
    
    CQ_Calcula22s = True
Exit Function
TrataErro:
    CQ_Calcula22s = False
    BG_LogFile_Erros "ERRO ao calcular 2(2s): " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_Calcula22s", True
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------------------------------------------------------

' R(4s) - Quando 1 valor do controle ultrapassa +2DP e outro consecutivo ultrapassa o -2DP

' -------------------------------------------------------------------------------------------------------------------
Public Function CQ_CalculaR4s(rsLote As ADODB.recordset, cod_ana_s As String, cod_lote As String, media As Double, desvio As Double) As Boolean
    Dim desvios As Double
    Dim Positivos As Integer
    Dim negativos As Integer
    On Error GoTo TrataErro
    desvios = 2 * desvio
    Positivos = 0
    negativos = 0
    rsLote.MoveFirst
    While Not rsLote.EOF
        If BL_HandleNull(rsLote!resultado, 0) > media + desvios Then
            Positivos = Positivos + 1
        ElseIf BL_HandleNull(rsLote!resultado, 0) < (media - desvios) Then
            negativos = negativos + 1
        Else
            Positivos = 0
            negativos = 0
        End If
        If Positivos >= 1 And negativos >= 1 Then
            CQ_CalculaR4s = False
            Exit Function
        End If
        rsLote.MoveNext
    Wend
    CQ_CalculaR4s = True
Exit Function
TrataErro:
    CQ_CalculaR4s = False
    BG_LogFile_Erros "ERRO ao calcular R(2s): " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_CalculaR4s", True
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------------------------------------------------------

' 4(1s) - Quando 4 dos valores consecutivos do controle calculado ultrapassam +1DP ou -1DP

' -------------------------------------------------------------------------------------------------------------------
Public Function CQ_Calcula41s(rsLote As ADODB.recordset, cod_ana_s As String, cod_lote As String, media As Double, desvio As Double) As Boolean
    Dim desvios As Double
    Dim Positivos As Integer
    Dim negativos As Integer
    On Error GoTo TrataErro
    desvios = desvio
    Positivos = 0
    negativos = 0
    rsLote.MoveFirst
    While Not rsLote.EOF
        If BL_HandleNull(rsLote!resultado, 0) > media + desvios Then
            Positivos = Positivos + 1
            negativos = 0
        ElseIf BL_HandleNull(rsLote!resultado, 0) < (media - desvios) Then
            Positivos = 0
            negativos = negativos + 1
        Else
            Positivos = 0
            negativos = 0
        End If
        If Positivos >= 4 Or negativos >= 4 Then
            CQ_Calcula41s = False
            Exit Function
        End If
        rsLote.MoveNext
    Wend
    CQ_Calcula41s = True
Exit Function
TrataErro:
    CQ_Calcula41s = False
    BG_LogFile_Erros "ERRO ao calcular 4(1s): " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_Calcula41s", True
    Exit Function
    Resume Next
End Function



' -------------------------------------------------------------------------------------------------------------------

' 10x 8x, 12x -Quando 10,8 ou 12 dos valores consecutivos do controle calculado est�o do mesmo lado em rela��o � m�dia

' -------------------------------------------------------------------------------------------------------------------
Public Function CQ_CalculaX(rsLote As ADODB.recordset, cod_ana_s As String, cod_lote As String, media As Double, desvio As Double, numero As Integer) As Boolean
    Dim desvios As Double
    Dim Positivos As Integer
    Dim negativos As Integer
    On Error GoTo TrataErro
    desvios = desvio
    Positivos = 0
    negativos = 0
    rsLote.MoveFirst
    While Not rsLote.EOF
        If BL_HandleNull(rsLote!resultado, 0) > media Then
            Positivos = Positivos + 1
            negativos = 0
        ElseIf BL_HandleNull(rsLote!resultado, 0) < (media) Then
            Positivos = 0
            negativos = negativos + 1
        Else
            Positivos = 0
            negativos = 0
        End If
        If Positivos >= numero Or negativos >= numero Then
            CQ_CalculaX = False
            Exit Function
        End If
        rsLote.MoveNext
    Wend
    CQ_CalculaX = True
Exit Function
TrataErro:
    CQ_CalculaX = False
    BG_LogFile_Erros "ERRO ao calcular X: " & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_CalculaX", True
    Exit Function
    Resume Next
End Function


Public Function CQ_DevolveRecordSet(ByRef rsLote As ADODB.recordset, cod_ana_s As String, cod_lote As String, dt_ini As String, dt_fim As String) As Boolean
    Dim sSql As String
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_cq_res WHERE cod_lote = " & BL_TrataStringParaBD(cod_lote) & " AND cod_ana_S = " & BL_TrataStringParaBD(cod_ana_s)
    sSql = sSql & " AND (flg_excluido IS NULL OR flg_excluido = 0) "
    sSql = sSql & " AND (flg_invisivel IS NULL OR flg_invisivel = 0) "
    sSql = sSql & " AND dt_cri between " & BL_TrataDataParaBD(dt_ini) & " AND " & BL_TrataDataParaBD(dt_fim)
    rsLote.CursorLocation = adUseServer
    rsLote.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsLote.Open sSql, gConexao
    If rsLote.RecordCount > 1 Then
        CQ_DevolveRecordSet = True
    Else
        CQ_DevolveRecordSet = False
    End If
Exit Function
TrataErro:
    CQ_DevolveRecordSet = False
    BG_LogFile_Erros "ERRO ao abrir RecordSet" & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_DevolveRecordSet", True
    Exit Function
    Resume Next
End Function

Private Function CQ_ConstroiCriterio(cod_ana_s As String, dt_ini As String, dt_fim As String) As String
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    sSql = "SELECT DISTINCT x1.seq_apar, x1.cod_lote, x2.descr_lote, x1.cod_ana_s, x3.descr_ana_s, x2.cod_nivel, x4.descr_nivel, "
    sSql = sSql & " x5.media, x5.desvio_padrao, x6.descr_apar "
    sSql = sSql & " FROM sl_cq_res x1, sl_Cq_lote x2, sl_ana_s x3, sl_tbf_nivel x4, sl_cq_ana x5, gc_apar x6 "
    sSql = sSql & " WHERE X1.cod_lote = X2.cod_lote And X1.cod_ana_s = x3.cod_ana_s AND x2.cod_nivel = x4.cod_nivel(+) "
    sSql = sSql & " AND x1.cod_ana_s = x5.cod_ana_s AND x1.cod_lote = x5.cod_lote "
    sSql = sSql & " AND x1.seq_apar = x6.seq_apar "
    sSql = sSql & " AND x1.dt_cri between " & BL_TrataDataParaBD(dt_ini) & " AND " & BL_TrataDataParaBD(dt_fim)
    sSql = sSql & " AND x1.cod_ana_s =  " & BL_TrataStringParaBD(cod_ana_s)
    sSql = sSql & " ORDER BY descr_ana_s ASC, cod_nivel ASC, cod_lote ASC "
    CQ_ConstroiCriterio = sSql
Exit Function
TrataErro:
    CQ_ConstroiCriterio = ""
    BG_LogFile_Erros "ERRO ao CQ_ConstroiCriterio" & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_ConstroiCriterio", True
    Exit Function
    Resume Next
End Function

Public Function CQ_PreencheEstrut(cod_ana_s As String, dt_ini As String, dt_fim As String) As Boolean
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = CQ_ConstroiCriterio(cod_ana_s, dt_ini, dt_fim)
    Set rsAna = New ADODB.recordset
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount <= 0 Then
        CQ_PreencheEstrut = False
    Else
        CQtotalRes = 0
        ReDim CQestrutRes(0)
        While Not rsAna.EOF
            CQtotalRes = CQtotalRes + 1
            ReDim Preserve CQestrutRes(CQtotalRes)
            CQestrutRes(CQtotalRes).flg_selec = False
            CQestrutRes(CQtotalRes).cod_ana_s = BL_HandleNull(rsAna!cod_ana_s, "")
            CQestrutRes(CQtotalRes).cod_lote = BL_HandleNull(rsAna!cod_lote, "")
            CQestrutRes(CQtotalRes).cod_nivel = BL_HandleNull(rsAna!cod_nivel, -1)
            CQestrutRes(CQtotalRes).descr_ana_s = BL_HandleNull(rsAna!descr_ana_s, "")
            CQestrutRes(CQtotalRes).descr_lote = BL_HandleNull(rsAna!descr_lote, "")
            CQestrutRes(CQtotalRes).descr_nivel = BL_HandleNull(rsAna!descr_nivel, "")
            CQestrutRes(CQtotalRes).desvio_padrao_lote = BL_HandleNull(rsAna!desvio_padrao, -1)
            CQestrutRes(CQtotalRes).desvio_padrao_percent_lote = 0
            CQestrutRes(CQtotalRes).media_lote = BL_HandleNull(rsAna!media, -1)
            CQestrutRes(CQtotalRes).seq_apar = BL_HandleNull(rsAna!seq_apar, -1)
            CQestrutRes(CQtotalRes).descr_apar = BL_HandleNull(rsAna!descr_apar, "")
            CQ_CalculaValores CQestrutRes(CQtotalRes).cod_lote, CQestrutRes(CQtotalRes).cod_ana_s, CQestrutRes(CQtotalRes).num_testes, _
                              CQestrutRes(CQtotalRes).media, CQestrutRes(CQtotalRes).desvio_padrao, dt_ini, dt_fim
            CQestrutRes(CQtotalRes).cv = Round(CQ_CalculoCV(CQestrutRes(CQtotalRes).media, CQestrutRes(CQtotalRes).desvio_padrao), 2)
            CQestrutRes(CQtotalRes).bias = Round(CQ_CalculoBIAS(CQestrutRes(CQtotalRes).media, CQestrutRes(CQtotalRes).media_lote), 2)
            CQestrutRes(CQtotalRes).etc = Round(CQ_CalculoETC(CQestrutRes(CQtotalRes).bias, CQestrutRes(CQtotalRes).cv), 2)
            CQestrutRes(CQtotalRes).id = Round(CQ_CalculoID(CQestrutRes(CQtotalRes).media, CQestrutRes(CQtotalRes).media_lote, CQestrutRes(CQtotalRes).desvio_padrao_lote), 2)
            CQestrutRes(CQtotalRes).eta = Round(CQ_RetornaETa(CQestrutRes(CQtotalRes).cod_ana_s), 2)
            CQestrutRes(CQtotalRes).imprecisao = Round(CQ_CalculoIMP(CQestrutRes(CQtotalRes).eta), 2)
            CQestrutRes(CQtotalRes).inexatidao = Round(CQ_CalculoINE(CQestrutRes(CQtotalRes).eta), 2)
            CQestrutRes(CQtotalRes).desvio_padrao_percent_lote = Round(CQ_CalculoPercentSD(CQestrutRes(CQtotalRes).eta), 2)
            rsAna.MoveNext
        Wend
        CQ_PreencheEstrut = True
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    CQ_PreencheEstrut = False
    BG_LogFile_Erros "ERRO ao CQ_PreencheEstrut" & Err.Number & " - " & Err.Description, "Controlo_Qualidade", "CQ_PreencheEstrut", True
    Exit Function
    Resume Next
End Function

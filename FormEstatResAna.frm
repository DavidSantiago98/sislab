VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormEstatResAna 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEstatAna"
   ClientHeight    =   5730
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   14430
   Icon            =   "FormEstatResAna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5730
   ScaleWidth      =   14430
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcAux 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2040
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   7440
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcDescricao 
      Height          =   285
      Left            =   960
      Locked          =   -1  'True
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   7440
      Width           =   735
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   7440
      Width           =   735
   End
   Begin MSFlexGridLib.MSFlexGrid FgAna 
      Height          =   5175
      Left            =   6000
      TabIndex        =   26
      Top             =   120
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   9128
      _Version        =   393216
      BackColorBkg    =   -2147483644
      BorderStyle     =   0
      Appearance      =   0
   End
   Begin VB.CheckBox CkDescrAna 
      Caption         =   "Descriminar An�lises"
      Height          =   255
      Left            =   120
      TabIndex        =   24
      Top             =   5400
      Width           =   1815
   End
   Begin VB.CheckBox CkRestringeRequis 
      Caption         =   "Requisi��es Com Apenas An�lises Seleccionadas"
      Height          =   255
      Left            =   0
      TabIndex        =   23
      Top             =   7800
      Width           =   3855
   End
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   4080
      ScaleHeight     =   315
      ScaleWidth      =   435
      TabIndex        =   6
      Top             =   9840
      Width           =   495
   End
   Begin VB.Frame Frame3 
      Height          =   5295
      Left            =   240
      TabIndex        =   0
      Top             =   0
      Width           =   5775
      Begin VB.CommandButton BtPesqRapAna 
         Height          =   450
         Left            =   4560
         Picture         =   "FormEstatResAna.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   25
         ToolTipText     =   "Pesquisa R�pida de An�lises Simples"
         Top             =   4780
         Width           =   1095
      End
      Begin VB.ComboBox CbUrgencia 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   4440
         Width           =   3615
      End
      Begin VB.CommandButton BtPesquisaMedicos 
         Height          =   450
         Left            =   5280
         Picture         =   "FormEstatResAna.frx":0776
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   3600
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaEFR 
         Height          =   450
         Left            =   5280
         Picture         =   "FormEstatResAna.frx":0B00
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   2880
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   450
         Left            =   5280
         Picture         =   "FormEstatResAna.frx":0E8A
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   720
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaLocais 
         Height          =   450
         Left            =   5280
         Picture         =   "FormEstatResAna.frx":1214
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   2160
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   450
         Left            =   5280
         Picture         =   "FormEstatResAna.frx":159E
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
         Top             =   1440
         Width           =   375
      End
      Begin VB.ListBox EcListaProven 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         TabIndex        =   5
         Top             =   720
         Width           =   3615
      End
      Begin VB.ListBox EcListaSala 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         TabIndex        =   4
         Top             =   1440
         Width           =   3615
      End
      Begin VB.ListBox EcListaLocais 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         TabIndex        =   3
         Top             =   2160
         Width           =   3615
      End
      Begin VB.ListBox EcListaEFR 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         TabIndex        =   2
         Top             =   2880
         Width           =   3615
      End
      Begin VB.ListBox EcListaMedicos 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         TabIndex        =   1
         Top             =   3600
         Width           =   3615
      End
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   255
         Left            =   1560
         TabIndex        =   21
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   199557121
         CurrentDate     =   39588
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   3840
         TabIndex        =   22
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   199557121
         CurrentDate     =   39588
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000000&
         X1              =   120
         X2              =   5640
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Label Label5 
         Caption         =   "Urg�ncia"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   4440
         Width           =   855
      End
      Begin VB.Label Label7 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label6 
         Caption         =   "a"
         Height          =   255
         Left            =   3240
         TabIndex        =   18
         Top             =   240
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "Proveni�ncias"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   11
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Salas / Postos"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   10
         Top             =   1440
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Locais"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   9
         Top             =   2160
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Entidades Finac."
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   8
         Top             =   2880
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "M�dicos"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   7
         Top             =   3600
         Width           =   1095
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3000
      Top             =   9600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
End
Attribute VB_Name = "FormEstatResAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'      .............................
'     .                             .
'    .   Paulo Ferreira 2010.03.08   .
'     .       � 2010 Glintt-HS      .
'      .............................

Option Explicit

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Const lColObrig = 0
Const lColCodigo = 1
Const lColDescricao = 2
Const lColSinalInf = 3
Const lcolResInf = 4
Const lcolSinalSup = 5
Const lcolResSup = 6

Public rs As ADODB.recordset
Private Type analises
    cod_ana_s As String
    descr_ana_s As String
    t_res As Integer
    sinal_inf As String
    descr_sinal_inf As String
    res_inf As String
    sinal_sup As String
    descr_sinal_sup As String
    res_sup As String
    cod_frase As String
    descr_frase As String
    cod_micro As String
    descr_micro As String
    flg_obrig As Boolean
End Type
Dim estrutAna() As analises
Dim totalAna As Integer

Dim colunaSel As Integer

Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        EcAux.Visible = False
        FgAna.SetFocus
    ElseIf KeyCode = vbKeyReturn Then
        If FgAna.Col = lcolResInf Then
            If estrutAna(FgAna.row).descr_sinal_inf <> "=" And IsNumeric(EcAux.Text) = False Then
                Call BG_Mensagem(mediMsgBox, "Para o sinal indicado(" & estrutAna(FgAna.row).descr_sinal_inf & "), o valor ter� que ser num�rico.", vbOKOnly + vbExclamation, App.ProductName)
                estrutAna(FgAna.row).res_inf = ""
                FgAna.TextMatrix(FgAna.row, FgAna.Col) = ""
                Exit Sub
            End If
                
            estrutAna(FgAna.row).res_inf = EcAux.Text
            FgAna.TextMatrix(FgAna.row, FgAna.Col) = estrutAna(FgAna.row).res_inf
        Else
            If estrutAna(FgAna.row).descr_sinal_sup <> "=" And IsNumeric(EcAux.Text) = False Then
                Call BG_Mensagem(mediMsgBox, "Para o sinal indicado(" & estrutAna(FgAna.row).descr_sinal_sup & "), o valor ter� que ser num�rico.", vbOKOnly + vbExclamation, App.ProductName)
                estrutAna(FgAna.row).res_sup = ""
                FgAna.TextMatrix(FgAna.row, FgAna.Col) = ""
                Exit Sub
            End If
            estrutAna(FgAna.row).res_sup = EcAux.Text
            FgAna.TextMatrix(FgAna.row, FgAna.Col) = estrutAna(FgAna.row).res_sup
        End If
        EcAux.Visible = False
        FgAna.SetFocus
    End If
End Sub

Private Sub EcAux_LostFocus()
    EcAux.Visible = False
    FgAna.SetFocus
End Sub

Private Sub Fgana_DblClick()
    If FgAna.row <= totalAna Then
        If FgAna.Col = lColObrig Then
            If estrutAna(FgAna.row).flg_obrig = True Then
                estrutAna(FgAna.row).flg_obrig = False
                estrutAna(FgAna.row).cod_frase = ""
                estrutAna(FgAna.row).cod_micro = ""
                estrutAna(FgAna.row).descr_frase = ""
                estrutAna(FgAna.row).descr_micro = ""
                estrutAna(FgAna.row).descr_sinal_inf = ""
                estrutAna(FgAna.row).descr_sinal_sup = ""
                estrutAna(FgAna.row).res_inf = ""
                estrutAna(FgAna.row).res_sup = ""
                estrutAna(FgAna.row).sinal_inf = ""
                estrutAna(FgAna.row).sinal_sup = ""
                FgAna.TextMatrix(FgAna.row, lColSinalInf) = ""
                FgAna.TextMatrix(FgAna.row, lcolSinalSup) = ""
                FgAna.TextMatrix(FgAna.row, lcolResInf) = ""
                FgAna.TextMatrix(FgAna.row, lcolResSup) = ""
            Else
                estrutAna(FgAna.row).flg_obrig = True
            End If
            FgAna.TextMatrix(FgAna.row, FgAna.Col) = estrutAna(FgAna.row).flg_obrig
        End If
        If (FgAna.Col = lColSinalInf Or FgAna.Col = lcolSinalSup) Then
            If estrutAna(FgAna.row).flg_obrig = False Then
                Call BG_Mensagem(mediMsgBox, "Apenas pode usar crit�rios se an�lise for obrigat�ria.", vbOKOnly + vbExclamation, App.ProductName)
                Exit Sub
            End If
        
            PA_PesquisaSinal EcCodigo, EcDescricao
            Select Case FgAna.Col
                Case lColSinalInf
                    estrutAna(FgAna.row).sinal_inf = EcCodigo.Text
                    estrutAna(FgAna.row).descr_sinal_inf = EcDescricao.Text
                    FgAna.TextMatrix(FgAna.row, lColSinalInf) = estrutAna(FgAna.row).descr_sinal_inf
                Case lcolSinalSup
                    estrutAna(FgAna.row).sinal_sup = EcCodigo.Text
                    estrutAna(FgAna.row).descr_sinal_sup = EcDescricao.Text
                    FgAna.TextMatrix(FgAna.row, lcolSinalSup) = estrutAna(FgAna.row).descr_sinal_sup
            End Select
        End If
        If (FgAna.Col = lcolResInf) Then
            If estrutAna(FgAna.row).sinal_inf = "" Then
                Call BG_Mensagem(mediMsgBox, "Indique primeiro o sinal.", vbOKOnly + vbExclamation, App.ProductName)
                Exit Sub
            End If
            EcAux.left = FgAna.CellLeft + FgAna.left
            EcAux.top = FgAna.CellTop + FgAna.top
            EcAux.Width = FgAna.CellWidth + 10
            EcAux = estrutAna(FgAna.row).res_inf
            EcAux.Visible = True
            EcAux.SetFocus
        ElseIf (FgAna.Col = lcolResSup) Then
            If estrutAna(FgAna.row).sinal_sup = "" Then
                Call BG_Mensagem(mediMsgBox, "Indique primeiro o sinal.", vbOKOnly + vbExclamation, App.ProductName)
                Exit Sub
            End If
            If estrutAna(FgAna.row).descr_sinal_inf = "=" Then
                Call BG_Mensagem(mediMsgBox, "O Sinal do intervalo inferior n�o pode ser =.", vbOKOnly + vbExclamation, App.ProductName)
                Exit Sub
            End If
            If estrutAna(FgAna.row).res_inf = "" Then
                Call BG_Mensagem(mediMsgBox, "Necess�rio preencher primeiro o intervalo inferior", vbOKOnly + vbExclamation, App.ProductName)
                Exit Sub
            End If
            EcAux.left = FgAna.CellLeft + FgAna.left
            EcAux.top = FgAna.CellTop + FgAna.top
            EcAux.Width = FgAna.CellWidth + 10
            EcAux = estrutAna(FgAna.row).res_sup
            EcAux.Visible = True
            EcAux.SetFocus
        End If
        
    End If
    EcCodigo.Text = ""
    EcDescricao.Text = ""
End Sub

Private Sub FgAna_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    If KeyCode = vbKeyReturn Then
        Fgana_DblClick
    ElseIf KeyCode = vbKeyDelete Then
        If FgAna.Col = lColCodigo Or FgAna.Col = lColDescricao Then
            For i = FgAna.row To totalAna - 1
                estrutAna(i).cod_ana_s = estrutAna(i + 1).cod_ana_s
                estrutAna(i).cod_frase = estrutAna(i + 1).cod_frase
                estrutAna(i).cod_micro = estrutAna(i + 1).cod_micro
                estrutAna(i).descr_frase = estrutAna(i + 1).descr_frase
                estrutAna(i).descr_frase = estrutAna(i + 1).descr_frase
                estrutAna(i).descr_micro = estrutAna(i + 1).descr_micro
                estrutAna(i).descr_sinal_inf = estrutAna(i + 1).descr_sinal_inf
                estrutAna(i).descr_sinal_sup = estrutAna(i + 1).descr_sinal_sup
                estrutAna(i).res_inf = estrutAna(i + 1).res_inf
                estrutAna(i).res_sup = estrutAna(i + 1).res_sup
                estrutAna(i).sinal_inf = estrutAna(i + 1).sinal_inf
                estrutAna(i).sinal_sup = estrutAna(i + 1).sinal_sup
                estrutAna(i).t_res = estrutAna(i + 1).t_res
            Next i
            totalAna = totalAna - 1
            ReDim Preserve estrutAna(totalAna)
            FgAna.RemoveItem FgAna.row
        End If
    End If
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = "Estat�stica de An�lises"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 14520
    Me.Height = 6090 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtInicio
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de An�lises")
    
    Set FormEstatResAna = Nothing
    
End Sub

Private Sub LimpaFGAna()
    Dim i As Integer
    FgAna.rows = 2
    For i = 0 To FgAna.Cols - 1
        FgAna.TextMatrix(1, i) = ""
    Next i
End Sub
Sub LimpaCampos()
    Me.SetFocus
    LimpaFGAna
    EcListaProven.Clear
    EcListaSala.Clear
    EcListaLocais.Clear
    EcListaEFR.Clear
    EcListaMedicos.Clear
    CkDescrAna.value = vbUnchecked
    CkRestringeRequis.value = vbUnchecked
    CkRestringeRequis.Enabled = False
    EcDtFim.value = Bg_DaData_ADO
    EcDtInicio.value = Bg_DaData_ADO
    totalAna = 0
    ReDim estrutAna(0)
End Sub

Sub DefTipoCampos()
    
    With FgAna
        .rows = 2
        .FixedRows = 1
        .Cols = 7
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .MergeCells = flexMergeFree
        .row = 0
        
        .ColWidth(lColObrig) = 500
        .Col = lColObrig
        .ColAlignment(lColObrig) = flexAlignLeftCenter
        .TextMatrix(0, lColObrig) = "Obrig."
        
        .ColWidth(lColCodigo) = 1000
        .Col = lColCodigo
        .ColAlignment(lColCodigo) = flexAlignLeftCenter
        .TextMatrix(0, lColCodigo) = "C�digo"
        
        .ColWidth(lColDescricao) = 3300
        .Col = lColDescricao
        .ColAlignment(lColDescricao) = flexAlignLeftCenter
        .TextMatrix(0, lColDescricao) = "Descri��o"
        
        .ColWidth(lColSinalInf) = 800
        .Col = lColSinalInf
        .ColAlignment(lColSinalInf) = flexAlignLeftCenter
        .TextMatrix(0, lColSinalInf) = "Sinal Inf."
        
        .ColWidth(lcolResInf) = 1000
        .Col = lcolResInf
        .ColAlignment(lcolResInf) = flexAlignLeftCenter
        .TextMatrix(0, lcolResInf) = "Res. Inf"
        
        .ColWidth(lcolSinalSup) = 800
        .Col = lcolSinalSup
        .ColAlignment(lcolSinalSup) = flexAlignLeftCenter
        .TextMatrix(0, lcolSinalSup) = "Sinal Sup."
        
        .ColWidth(lcolResSup) = 1000
        .Col = lcolResSup
        .ColAlignment(lcolResSup) = flexAlignLeftCenter
        .TextMatrix(0, lcolResSup) = "Res. Sup"
        
                
        .WordWrap = False
        .row = 0
        .Col = 0
    End With
    
    totalAna = 0
    ReDim estrutAna(0)
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia
    EcDtFim.value = Bg_DaData_ADO
    EcDtInicio.value = Bg_DaData_ADO
    'CkRestringeRequis.Value = vbUnchecked
    'CkRestringeRequis.Enabled = False
    
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub


Private Sub EclistaProven_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaProven, KeyCode, Shift
End Sub

Private Sub EclistaSala_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaSala, KeyCode, Shift
End Sub
Private Sub EclistaLocais_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaLocais, KeyCode, Shift
End Sub

Private Sub EclistaEFR_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaEFR, KeyCode, Shift
End Sub
Private Sub EcListaMedicos_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaMedicos, KeyCode, Shift
End Sub






Private Sub BtPesquisaMedicos_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_med"
    CamposEcran(1) = "cod_med"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome_med"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_medicos"
    CWhere = ""
    CampoPesquisa = "nome_med"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome_med ", _
                                                                           " M�dicos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If Trim(resultados(i) <> "") Then
                If EcListaMedicos.ListCount = 0 Then
                    EcListaMedicos.AddItem BL_SelCodigo("sl_medicos", "nome_med", "seq_med", resultados(i))
                    EcListaMedicos.ItemData(0) = resultados(i)
                Else
                    EcListaMedicos.AddItem BL_SelCodigo("sl_medicos", "nome_med", "seq_med", resultados(i))
                    EcListaMedicos.ItemData(EcListaMedicos.NewIndex) = resultados(i)
                End If
            End If
        Next i
    End If
End Sub



Private Sub BtPesquisaProveniencia_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaProven.ListCount = 0 Then
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(0) = resultados(i)
            Else
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(EcListaProven.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub BtPesquisaSala_Click()
    PA_PesquisaSalaMultiSel EcListaSala
End Sub

Private Sub BtPesquisaLocais_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "Empresa_id"
    CamposEcran(1) = "Empresa_id"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome_empr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "gr_empr_inst"
    CWhere = ""
    CampoPesquisa = "nome_empr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome_empr ", _
                                                                           " Locais")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaLocais.ListCount = 0 Then
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", resultados(i))
                EcListaLocais.ItemData(0) = resultados(i)
            Else
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", resultados(i))
                EcListaLocais.ItemData(EcListaLocais.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub


Private Sub BtPesquisaEFR_Click()
    PA_PesquisaEFRMultiSel EcListaEFR
End Sub


Sub Preenche_Estatistica()
    Dim sql As String
    Dim continua As Boolean
    Dim StrTemp  As String
    Dim i As Integer
    Dim nomeReport As String
    
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estat�stica por Laborat�rio") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtInicio.value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtInicio.SetFocus
        Exit Sub
    End If
    If EcDtFim.value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    If totalAna <= 0 Then
        Call BG_Mensagem(mediMsgBox, "Indique pelo menos uma an�lise.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    If CkDescrAna.value = vbChecked Then
        nomeReport = "EstatisticaResultadosAna_OrderAna"
    Else
        nomeReport = "EstatisticaResultadosAna"
    End If
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport(nomeReport, "Estat�stica de An�lises", crptToPrinter)
    Else
        continua = BL_IniciaReport(nomeReport, "Estat�stica de An�lises", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    
    PreencheTabelaTemporaria
    
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT sl_cr_resultados_ana.nome_computador, sl_cr_resultados_ana.num_sessao, sl_cr_resultados_ana.utente, sl_cr_resultados_ana.nome_ute, n_req,"
    Report.SQLQuery = Report.SQLQuery & " sl_cr_resultados_ana.dt_chega, sl_cr_resultados_ana.cod_proven, sl_cr_resultados_ana.descr_proven, sl_cr_resultados_ana.cod_sala,"
    Report.SQLQuery = Report.SQLQuery & " sl_cr_resultados_ana.descr_sala, sl_cr_resultados_ana.cod_gr_ana, sl_cr_resultados_ana.descr_gr_ana, sl_cr_resultados_ana.cod_gr_trab,"
    Report.SQLQuery = Report.SQLQuery & " sl_cr_resultados_ana.Descr_Gr_Trab , sl_cr_resultados_ana.cod_ana, sl_cr_resultados_ana.descr_ana, sl_cr_resultados_ana.peso_estatistico, "
    Report.SQLQuery = Report.SQLQuery & " sl_cr_resultados_ana.N_EPIS , sl_cr_resultados_ana.t_sit, sl_cr_resultados_ana.resultado "
    Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_resultados_ana WHERE sl_cr_resultados_ana.nome_computador = " & BL_TrataStringParaBD(gComputador)
    Report.SQLQuery = Report.SQLQuery & " AND sl_cr_resultados_ana.num_sessao = " & gNumeroSessao
    
    If CkDescrAna.value = vbChecked Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_cr_resultados_ana.cod_ana "
    Else
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_cr_resultados_ana.n_req, sl_cr_resultados_ana.ord_marca "
    End If
    
    Report.SelectionFormula = "{sl_cr_resultados_ana.nome_computador} = '" & gComputador & "' AND {sl_cr_resultados_ana.num_sessao}= " & gNumeroSessao
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtInicio.value)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.value)
    

       
    'PROVENIENCIAS
    StrTemp = ""
    For i = 0 To EcListaProven.ListCount - 1
        StrTemp = StrTemp & EcListaProven.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(4) = "Proven=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(4) = "Proven=" & BL_TrataStringParaBD("Todas")
    End If
    
    'SALAS
    StrTemp = ""
    For i = 0 To EcListaSala.ListCount - 1
        StrTemp = StrTemp & EcListaSala.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(5) = "Salas=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(5) = "Salas=" & BL_TrataStringParaBD("Todas")
    End If
    
    'LOCAIS
    StrTemp = ""
    For i = 0 To EcListaLocais.ListCount - 1
        StrTemp = StrTemp & EcListaLocais.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(6) = "Locais=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(6) = "Locais=" & BL_TrataStringParaBD("Todos")
    End If
    
    'ENTIDADES
    StrTemp = ""
    For i = 0 To EcListaEFR.ListCount - 1
        StrTemp = StrTemp & EcListaEFR.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(7) = "EFR=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(7) = "EFR=" & BL_TrataStringParaBD("Todas")
    End If
    
    'ANALISES
    StrTemp = ""
    For i = 1 To totalAna
        StrTemp = StrTemp & estrutAna(i).descr_ana_s & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(8) = "Analises=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(8) = "Analises=" & BL_TrataStringParaBD("Todas")
    End If
    
    Report.formulas(9) = "AgruparGrAna=" & BL_TrataStringParaBD("N")
    Report.formulas(10) = "AgruparGrTrab=" & BL_TrataStringParaBD("N")
    Report.formulas(11) = "AgruparProven=" & BL_TrataStringParaBD("N")
    Report.formulas(12) = "AgruparEFR=" & BL_TrataStringParaBD("N")
    Report.formulas(13) = "AgruparSala=" & BL_TrataStringParaBD("N")
    
    'DESCRIMINAR ANALISES
    If CkDescrAna.value = vbChecked Then
        Report.formulas(14) = "DescrAna=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(14) = "DescrAna=" & BL_TrataStringParaBD("N")
    End If
    
    Report.formulas(15) = "DescrReq=" & BL_TrataStringParaBD("N")
    Report.formulas(16) = "AgruparEFR=" & BL_TrataStringParaBD("S")
    Report.formulas(16) = "AgruparEFR=" & BL_TrataStringParaBD("N")
    Report.formulas(17) = "Urgencia=" & BL_TrataStringParaBD(CbUrgencia)
    
    'MEDICOS
    StrTemp = ""
    For i = 0 To EcListaMedicos.ListCount - 1
        StrTemp = StrTemp & EcListaMedicos.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(18) = "MEDICOS=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(18) = "MEDICOS=" & BL_TrataStringParaBD("Todos")
    End If
    
    If CkRestringeRequis.value = vbChecked Then
        Report.formulas(19) = "RestrRequis=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(19) = "RestrRequis=" & BL_TrataStringParaBD("N")
    End If
    'Report.SubreportToChange = ""
    Call BL_ExecutaReport
 
    
End Sub

Private Sub PreencheTabelaTemporaria()
    Dim sSql As String
    Dim sSqlCONDICOES As String
    Dim iAna As Integer
    Dim rsReq As New ADODB.recordset
    Dim ssql2 As String
    On Error GoTo TrataErro
    
    sSql = " DELETE FROM sl_cr_resultados_ana WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    sSql = sSql & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
        
    ssql2 = " SELECT req.n_req "
    ssql2 = ssql2 & ConstroiFROM
    ssql2 = ssql2 & ConstroiWHERE
    ssql2 = ssql2 & ConstroiGROUPBY
            
    sSql = "INSERT INTO sl_cr_resultados_ana (nome_computador, num_sessao, utente, nome_ute, n_req,dt_chega, cod_proven,"
    sSql = sSql & " descr_proven, cod_sala, descr_sala, cod_gr_ana, descr_gr_ana, "
    sSql = sSql & " cod_ana, descr_ana, peso_estatistico, cod_efr, descr_efr, n_epis, t_sit, ord_marca, quantidade, resultado )"
    sSql = sSql & "SELECT " & BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ", utente, nome_ute, n_req, dt_chega,"
    sSql = sSql & " cod_proven , descr_proven, "
    sSql = sSql & " cod_sala, descr_sala,  cod_gr_ana, descr_gr_Ana, cod_ana_s, descr_ana_s, "
    sSql = sSql & " 1, cod_efr, descr_Efr ,n_epis, cod_t_sit, ord_ana, 1,result_1 "
    sSql = sSql & " FROM sl_bi_resultados WHERE sl_bi_resultados.n_req IN (" & ssql2 & ")"
    sSql = sSql & " AND cod_ana_s IN("
    For iAna = 1 To totalAna
        sSql = sSql & BL_TrataStringParaBD(estrutAna(iAna).cod_ana_s) & ", "
    Next iAna
    sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    BG_ExecutaQuery_ADO sSql
Exit Sub
TrataErro:
    BG_Mensagem mediMsgBox, "N�o � poss�vel gerar a estat�stica de an�lises.", vbCritical, "Erro de Base Dados"
    BG_LogFile_Erros "FormEstatResAna - PreencheTabelaTemporaria: " & Err.Number & " (" & Err.Description & ")"
    BG_LogFile_Erros sSql
    Exit Sub
    Resume Next
End Sub

Private Function ConstroiFROM() As String
    Dim sSql As String
    Dim i As Integer
    Dim iAna As Integer
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
    sSql = ""
    sSql = sSql & " FROM " & tabela_aux & " ute, sl_requis req "
    
    ' ------------------------------------------------------------------------------
    ' ANALISES PREENCHIDO
    ' ------------------------------------------------------------------------------
    For iAna = 1 To totalAna
         If estrutAna(iAna).flg_obrig = False Then
            sSql = sSql & " LEFT OUTER JOIN sl_bi_resultados res" & iAna & " ON req.n_req = res" & iAna & ".n_req "
            sSql = sSql & " AND res" & iAna & ".cod_ana_s = " & BL_TrataStringParaBD(estrutAna(iAna).cod_ana_s)
            sSql = sSql & ConstroiCriterioResultado(iAna)
        End If
    Next iAna
    
    

    ' ------------------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ---------------------------------------------------------------------------------------
    sSql = sSql & " LEFT OUTER JOIN sl_proven proven ON req.cod_proven = proven.cod_proven"
    
    ' ------------------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ---------------------------------------------------------------------------------------
    sSql = sSql & " LEFT OUTER JOIN SL_EFR efr ON req.cod_efr= efr.cod_efr "

    ' ------------------------------------------------------------------------------
    ' SALA PREENCHIDO
    ' ------------------------------------------------------------------------------
    sSql = sSql & " LEFT OUTER JOIN sl_cod_salas sala ON req.cod_sala = sala.cod_sala "
        
    
    For iAna = 1 To totalAna
         If estrutAna(iAna).flg_obrig = True Then
             sSql = sSql & ", sl_bi_resultados res" & iAna
        End If
    Next iAna
    
    

    
    ' ------------------------------------------------------------------------------
    ' M�DICO
    ' ------------------------------------------------------------------------------
    If EcListaMedicos.ListCount > 0 Then
        sSql = sSql & ", sl_medicos med "
    End If
    
    ConstroiFROM = sSql
End Function


Private Function ConstroiWHERE() As String
    Dim iAna As Integer
    Dim sSql As String
    Dim sSqlAny As String
    Dim sSqlAll As String
    Dim item As Variant
    Dim i As Integer
    
    sSql = ""
    sSql = sSql & " WHERE ute.seq_utente = req.seq_utente "
    sSql = sSql & " AND (req.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & " )"
    
    ' ------------------------------------------------------------------------------
    ' SE URGENCIA PREENCHIDA
    ' ------------------------------------------------------------------------------
    If CbUrgencia.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND req.t_urg = " & BG_DaComboSel(CbUrgencia)
    End If
    
    ' ------------------------------------------------------------------------------
    ' MEDICOS PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaMedicos.ListCount > 0 Then
        sSql = sSql & " AND med.seq_med  IN ("
        For i = 0 To EcListaMedicos.ListCount - 1
            sSql = sSql & EcListaMedicos.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ------------------------------------------------------------------------------

    If EcListaProven.ListCount > 0 Then
        sSql = sSql & " AND proven.seq_proven  IN ("
        For i = 0 To EcListaProven.ListCount - 1
            sSql = sSql & EcListaProven.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If

    ' ------------------------------------------------------------------------------
    ' SALA PREENCHIDO
    ' ------------------------------------------------------------------------------

    If EcListaSala.ListCount > 0 Then
        sSql = sSql & " AND sala.seq_sala  IN ("
        For i = 0 To EcListaSala.ListCount - 1
            sSql = sSql & EcListaSala.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' LOCAL PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaLocais.ListCount > 0 Then
        sSql = sSql & " AND req.cod_local IN ("
        For i = 0 To EcListaLocais.ListCount - 1
            sSql = sSql & EcListaLocais.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' ENTIDADES PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaEFR.ListCount > 0 Then
        sSql = sSql & " AND req.cod_efr= efr.cod_efr AND efr.seq_efr  IN ("
        For i = 0 To EcListaEFR.ListCount - 1
            sSql = sSql & EcListaEFR.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ' ------------------------------------------------------------------------------
    ' ANALISES PREENCHIDO
    ' ------------------------------------------------------------------------------
    For iAna = 1 To totalAna
        If estrutAna(iAna).flg_obrig = True Then
            sSql = sSql & " AND req.n_req = res" & iAna & ".n_req "
            sSql = sSql & " AND res" & iAna & ".cod_ana_s = " & BL_TrataStringParaBD(estrutAna(iAna).cod_ana_s)
            sSql = sSql & ConstroiCriterioResultado(iAna)

        End If
    Next iAna
    ConstroiWHERE = sSql
End Function


Private Function ConstroiGROUPBY() As String
    Dim sSql As String
    If gSGBD = gOracle Then
        sSql = "GROUP BY req.n_req"
    End If
    ConstroiGROUPBY = sSql
    
End Function

Private Function ConstroiORDER() As String
    Dim sSql As String
    sSql = ""
    sSql = sSql & " ORDER BY   n_Req, cod_sala,ord_ana, cod_ana_s "
    ConstroiORDER = sSql

End Function

Private Sub BtPesqRapAna_Click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean
    Dim i As Integer
    Dim TotalElementosSel As Integer

    On Error GoTo ErrorHandler
    PesqRapida = False
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_s"
    CWhere = "  (flg_invisivel is null or flg_invisivel = 0) "
    CampoPesquisa = "descr_ana_s"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana_s ", _
                                                                           " Simples")

     FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            Call AdicionaAnalise(CStr(resultados(i)), BL_SelCodigo("sl_ana_s", "descr_ana_s", "cod_ana_s", resultados(i)))
        Next i
    End If
    Exit Sub

ErrorHandler:
    Exit Sub
    
End Sub

Private Sub AdicionaAnalise(Codigo As String, descricao As String)
    
    On Error GoTo ErrorHandler
    totalAna = totalAna + 1
    ReDim Preserve estrutAna(totalAna)
    estrutAna(totalAna).cod_ana_s = Codigo
    estrutAna(totalAna).descr_ana_s = descricao
    estrutAna(totalAna).t_res = BL_SelCodigo("SL_ANA_S", "T_RESULT", "COD_ANA_S", estrutAna(totalAna).cod_ana_s, "V")
    estrutAna(totalAna).flg_obrig = True
    
    FgAna.TextMatrix(totalAna, lColCodigo) = estrutAna(totalAna).cod_ana_s
    FgAna.TextMatrix(totalAna, lColDescricao) = estrutAna(totalAna).descr_ana_s
    FgAna.TextMatrix(totalAna, lColObrig) = estrutAna(totalAna).flg_obrig
    
    FgAna.AddItem ""
    Exit Sub

ErrorHandler:
    Exit Sub
    
End Sub

Private Function ConstroiCriterioResultado(linha As Integer) As String
    Dim sSql As String
    If estrutAna(linha).sinal_inf <> "" Then
        If estrutAna(linha).descr_sinal_inf = "=" Then
            Select Case estrutAna(linha).t_res
                Case gT_Frase
                    sSql = " AND res" & linha & ".seq_realiza IN (SELECT resf" & linha & ".seq_realiza "
                    sSql = sSql & " FROM sl_bi_resultados_frase resf" & linha
                    sSql = sSql & " WHERE resf" & linha & ".cod_frase= " & BL_TrataStringParaBD(estrutAna(linha).cod_frase) & ")"
                Case gT_Microrganismo
                    sSql = " res" & linha & ".seq_realiza IN (SELECT resm" & linha & ".seq_realiza "
                    sSql = sSql & " FROM sl_bi_resultados_micro resm" & linha
                    sSql = sSql & " WHERE resm" & linha & ".cod_micro= " & BL_TrataStringParaBD(estrutAna(linha).cod_micro) & ")"
                Case gT_Alfanumerico, gT_Numerico, gT_Auxiliar
                    sSql = " UPPER(res" & linha & ".result_1) = " & UCase(BL_TrataStringParaBD(estrutAna(linha).res_inf))
            End Select
        Else
            If estrutAna(linha).t_res = gT_Alfanumerico Or estrutAna(linha).t_res = gT_Numerico Or estrutAna(linha).t_res = gT_Auxiliar Then
                sSql = " AND (res" & linha & ".result_num1 " & estrutAna(linha).descr_sinal_inf & " " & Replace(estrutAna(linha).res_inf, ",", ".")
            
                If estrutAna(linha).sinal_sup <> "" Then
                    sSql = sSql & " AND res" & linha & ".result_num1 " & estrutAna(linha).descr_sinal_sup & " " & Replace(estrutAna(linha).res_sup, ",", ".")
                End If
                sSql = sSql & ")"
            End If
        End If
    End If
    ConstroiCriterioResultado = sSql
Exit Function
TrataErro:
    BG_LogFile_Erros "ConstroiCriterioResultado: " & Err.Number & " (" & Err.Description & ")", Me.Name, "ConstroiCriterioResultado", False
    Exit Function
    Resume Next
End Function

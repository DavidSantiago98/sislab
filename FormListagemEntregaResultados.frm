VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormListagemEntregaResultados 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormListagemEntregaResultados"
   ClientHeight    =   4125
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7875
   Icon            =   "FormListagemEntregaResultados.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4125
   ScaleWidth      =   7875
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame3 
      Height          =   1335
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   7695
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   735
         Left            =   600
         TabIndex        =   17
         Top             =   480
         Width           =   1695
         Begin VB.OptionButton optEntregue 
            Caption         =   "Por Entregar"
            Height          =   375
            Index           =   1
            Left            =   120
            TabIndex        =   19
            Top             =   360
            Width           =   1455
         End
         Begin VB.OptionButton optEntregue 
            Caption         =   "J� Entregues"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   18
            Top             =   120
            Value           =   -1  'True
            Width           =   1335
         End
      End
      Begin VB.ComboBox cbUrgencia 
         Height          =   315
         Left            =   6120
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   195
         Width           =   1215
      End
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   255
         Left            =   1440
         TabIndex        =   12
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   147259393
         CurrentDate     =   39588
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   3120
         TabIndex        =   13
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   147259393
         CurrentDate     =   39588
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   720
         TabIndex        =   16
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2850
         TabIndex        =   15
         Top             =   240
         Width           =   255
      End
      Begin VB.Label Label4 
         Caption         =   "Urg�ncia"
         Height          =   255
         Index           =   0
         Left            =   5400
         TabIndex        =   14
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame FrameFiltros 
      Height          =   2655
      Left            =   0
      TabIndex        =   0
      Top             =   1320
      Width           =   7695
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   7200
         Picture         =   "FormListagemEntregaResultados.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   360
         Width           =   375
      End
      Begin VB.ListBox EcListaProven 
         Height          =   645
         Left            =   1440
         TabIndex        =   5
         Top             =   360
         Width           =   5775
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   7200
         Picture         =   "FormListagemEntregaResultados.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1080
         Width           =   375
      End
      Begin VB.ListBox EcListaSala 
         Height          =   645
         Left            =   1440
         TabIndex        =   3
         Top             =   1080
         Width           =   5775
      End
      Begin VB.CommandButton BtPesquisaLocais 
         Height          =   315
         Left            =   7200
         Picture         =   "FormListagemEntregaResultados.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1800
         Width           =   375
      End
      Begin VB.ListBox EcListaLocais 
         Height          =   645
         Left            =   1440
         TabIndex        =   1
         Top             =   1800
         Width           =   5775
      End
      Begin VB.Label Label1 
         Caption         =   "Proveni�ncias"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Salas / Postos"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   8
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Locais"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   7
         Top             =   1800
         Width           =   1095
      End
   End
End
Attribute VB_Name = "FormListagemEntregaResultados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = "Listagem de Entrega de Resultados"
    
    Me.left = 5
    Me.top = 5
    Me.Width = 7965
    Me.Height = 4545 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtInicio
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de Entrega de Resultados")
    
    Set FormListagemEntregaResultados = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    EcListaProven.Clear
    EcListaSala.Clear
    EcListaLocais.Clear
    CbUrgencia.ListIndex = mediComboValorNull
    EcDtFim.value = Bg_DaData_ADO
    EcDtInicio.value = Bg_DaData_ADO
End Sub

Sub DefTipoCampos()
        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia
    EcDtFim.value = Bg_DaData_ADO
    EcDtInicio.value = Bg_DaData_ADO

End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub


Private Sub EclistaProven_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaProven, KeyCode, Shift
End Sub

Private Sub EclistaSala_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaSala, KeyCode, Shift
End Sub
Private Sub EclistaLocais_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaLocais, KeyCode, Shift
End Sub


Private Sub BtPesquisaProveniencia_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaProven.ListCount = 0 Then
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(0) = resultados(i)
            Else
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(EcListaProven.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub BtPesquisaSala_Click()
    PA_PesquisaSalaMultiSel EcListaSala
End Sub

Private Sub BtPesquisaLocais_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "Empresa_id"
    CamposEcran(1) = "Empresa_id"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "nome_empr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "gr_empr_inst"
    CWhere = ""
    CampoPesquisa = "nome_empr"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY nome_empr ", _
                                                                           " Locais")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaLocais.ListCount = 0 Then
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", resultados(i))
                EcListaLocais.ItemData(0) = resultados(i)
            Else
                EcListaLocais.AddItem BL_SelCodigo("gr_empr_inst", "nome_empr", "Empresa_id", resultados(i))
                EcListaLocais.ItemData(EcListaLocais.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub

Sub Preenche_Estatistica()
    Dim sql As String
    Dim continua As Boolean
    Dim StrTemp  As String
    Dim i As Integer
    Dim nomeReport As String
    
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Listagem de Entrega de Resultados") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtInicio.value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtInicio.SetFocus
        Exit Sub
    End If
    If EcDtFim.value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    'brunodsantos 30.11.2015
    Dim Report As CrystalReport
    If optEntregue(0).value = True Then
        nomeReport = "ListagemEntregaResultados"
        
        'Printer Common Dialog
        If gImprimirDestino = 1 Then
            continua = BL_IniciaReport(nomeReport, "Listagem de Entrega de Resultados", crptToPrinter)
        Else
            continua = BL_IniciaReport(nomeReport, "Listagem de Entrega de Resultados", crptToWindow)
        End If
        If continua = False Then Exit Sub
        
        PreencheTabela
        
        Set Report = forms(0).Controls("Report")
      
        Report.SQLQuery = "SELECT sl_cr_entrega_resultados.n_req, sl_cr_entrega_resultados.resp_entrega,sl_cr_entrega_resultados.dt_entrega,"
        Report.SQLQuery = Report.SQLQuery & " sl_cr_entrega_resultados.hr_entrega,sl_cr_entrega_resultados.nome_entrega "
        Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_entrega_resultados WHERE sl_cr_entrega_resultados.nome_computador = " & BL_TrataStringParaBD(gComputador)
        Report.SQLQuery = Report.SQLQuery & " AND num_sessao = " & gNumeroSessao
    
    Else
        nomeReport = "ListagemResultadosPorEntregar"
        
        'Printer Common Dialog
        If gImprimirDestino = 1 Then
            continua = BL_IniciaReport(nomeReport, "Listagem de Resultados por Entregar", crptToPrinter)
        Else
            continua = BL_IniciaReport(nomeReport, "Listagem de Resultados por Entregar", crptToWindow)
        End If
        If continua = False Then Exit Sub
        
        PreencheTabelaPorEntregar
        
        Set Report = forms(0).Controls("Report")
        
        Report.SQLQuery = "SELECT sl_cr_entrega_resultados.n_req, sl_cr_entrega_resultados.resp_entrega,sl_cr_entrega_resultados.dt_entrega,"
        Report.SQLQuery = Report.SQLQuery & " sl_cr_entrega_resultados.hr_entrega,sl_cr_entrega_resultados.nome_entrega, "
        Report.SQLQuery = Report.SQLQuery & " sl_cr_entrega_resultados.nr_utente,sl_cr_entrega_resultados.nome_utente "
        Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_entrega_resultados WHERE sl_cr_entrega_resultados.nome_computador = " & BL_TrataStringParaBD(gComputador)
        Report.SQLQuery = Report.SQLQuery & " AND num_sessao = " & gNumeroSessao
    End If
        
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtInicio.value)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.value)
    
           
    'PROVENIENCIAS
    StrTemp = ""
    For i = 0 To EcListaProven.ListCount - 1
        StrTemp = StrTemp & EcListaProven.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(2) = "Proven=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(2) = "Proven=" & BL_TrataStringParaBD("Todas")
    End If
    
    'SALAS
    StrTemp = ""
    For i = 0 To EcListaSala.ListCount - 1
        StrTemp = StrTemp & EcListaSala.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(3) = "Salas=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(3) = "Salas=" & BL_TrataStringParaBD("Todas")
    End If
    
    'LOCAIS
    StrTemp = ""
    For i = 0 To EcListaLocais.ListCount - 1
        StrTemp = StrTemp & EcListaLocais.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(4) = "Locais=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(4) = "Locais=" & BL_TrataStringParaBD("Todos")
    End If
    
    
    
    Call BL_ExecutaReport
 
    
End Sub

Private Function ConstroiFROM() As String
    Dim sSql As String
    Dim i As Integer
    sSql = "FROM sl_entrega_resultados , sl_idutilizador"
    sSql = sSql & ", sl_requis "
    
    If EcListaProven.ListCount > 0 Then
        sSql = sSql & ", sl_proven proven "
    End If
    If EcListaSala.ListCount > 0 Then
        sSql = sSql & ", sl_cod_salas sala"
    End If
    ConstroiFROM = sSql
End Function
'brunodsantos 30.11.2015 Glintt-HS-10690
Private Function ConstroiFROMPorEntregar() As String
    Dim sSql As String
    Dim i As Integer
    sSql = "FROM sl_identif, sl_requis "
    
    If EcListaProven.ListCount > 0 Then
        sSql = sSql & ", sl_proven proven "
    End If
    If EcListaSala.ListCount > 0 Then
        sSql = sSql & ", sl_cod_salas sala"
    End If
    ConstroiFROMPorEntregar = sSql
End Function


Private Function ConstroiWHERE() As String
    Dim sSql As String
    Dim i As Integer
    sSql = ""
    sSql = sSql & " WHERE sl_entrega_resultados.user_entrega = sl_idutilizador.cod_utilizador "
    sSql = sSql & " AND sl_Requis.n_req= sl_entrega_resultados.n_req "
    sSql = sSql & " AND dt_entrega BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & " "
    
    ' ------------------------------------------------------------------------------
    ' SE URGENCIA PREENCHIDA
    ' ------------------------------------------------------------------------------
    If CbUrgencia.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND sl_requis.t_urg = " & BG_DaComboSel(CbUrgencia)
    End If
    
    ' ------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaProven.ListCount > 0 Then
        sSql = sSql & "  AND sl_requis.cod_proven= proven.cod_proven  "
        sSql = sSql & " AND proven.seq_proven  IN ("
        For i = 0 To EcListaProven.ListCount - 1
            sSql = sSql & EcListaProven.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If

    ' ------------------------------------------------------------------------------
    ' SALA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaSala.ListCount > 0 Then
        sSql = sSql & "  AND sl_requis.cod_sala= sala.cod_sala "
        sSql = sSql & " AND sala.seq_sala  IN ("
        For i = 0 To EcListaSala.ListCount - 1
            sSql = sSql & EcListaSala.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    ' ------------------------------------------------------------------------------
    ' LOCAL PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaLocais.ListCount > 0 Then
        sSql = sSql & " AND sl_requis.cod_local IN ("
        For i = 0 To EcListaLocais.ListCount - 1
            sSql = sSql & EcListaLocais.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ConstroiWHERE = sSql
End Function

'brunodsantos 30.11.2015 Glintt-HS-10690
Private Function ConstroiWHEREPorEntregar() As String
    Dim sSql As String
    Dim i As Integer
    sSql = ""
    sSql = sSql & " WHERE sl_requis.seq_utente = sl_identif.seq_utente "
    sSql = sSql & " AND sl_requis.dt_conclusao BETWEEN " & BL_TrataDataParaBD(EcDtInicio.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value) & " "
    
    ' ------------------------------------------------------------------------------
    ' SE URGENCIA PREENCHIDA
    ' ------------------------------------------------------------------------------
    If CbUrgencia.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND sl_requis.t_urg = " & BG_DaComboSel(CbUrgencia)
    End If
    
    ' ------------------------------------------------------------------------------
    ' PROVENIENCIA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaProven.ListCount > 0 Then
        sSql = sSql & "  AND sl_requis.cod_proven= proven.cod_proven  "
        sSql = sSql & " AND proven.seq_proven  IN ("
        For i = 0 To EcListaProven.ListCount - 1
            sSql = sSql & EcListaProven.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If

    ' ------------------------------------------------------------------------------
    ' SALA PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaSala.ListCount > 0 Then
        sSql = sSql & "  AND sl_requis.cod_sala= sala.cod_sala "
        sSql = sSql & " AND sala.seq_sala  IN ("
        For i = 0 To EcListaSala.ListCount - 1
            sSql = sSql & EcListaSala.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    ' ------------------------------------------------------------------------------
    ' LOCAL PREENCHIDO
    ' ------------------------------------------------------------------------------
    If EcListaLocais.ListCount > 0 Then
        sSql = sSql & " AND sl_requis.cod_local IN ("
        For i = 0 To EcListaLocais.ListCount - 1
            sSql = sSql & EcListaLocais.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    ConstroiWHEREPorEntregar = sSql
End Function


Private Function ConstroiORDER() As String
    Dim sSql As String
    sSql = " ORDER BY dt_entrega, hr_entrega "
    ConstroiORDER = sSql
End Function
'brunodsantos 30.11.2015 Glintt-HS-10690
Private Function ConstroiORDERPorEntregar() As String
    Dim sSql As String
    sSql = " ORDER BY sl_requis.dt_conclusao, sl_requis.n_req "
    ConstroiORDERPorEntregar = sSql
End Function


Private Sub PreencheTabela()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsTabela As New ADODB.recordset
    sSql = "DELETE FROM sl_cr_entrega_resultados WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    
    sSql = "SELECT sl_entrega_resultados.n_req, sl_entrega_resultados.resp_entrega,sl_entrega_resultados.dt_entrega,"
    sSql = sSql & " sl_entrega_resultados.hr_entrega,sl_idutilizador.nome "
    sSql = sSql & ConstroiFROM & ConstroiWHERE & ConstroiORDER
    rsTabela.CursorLocation = adUseServer
    rsTabela.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTabela.Open sSql, gConexao
    While Not rsTabela.EOF
        sSql = "INSERT into sl_cr_entrega_resultados (nome_computador, num_sessao, n_req, dt_entrega, hr_entrega, nome_entrega, resp_entrega) VALUES("
        sSql = sSql & BL_TrataStringParaBD(gComputador) & "," & gNumeroSessao & "," & BL_HandleNull(rsTabela!n_req, "") & "," & BL_TrataDataParaBD(rsTabela!dt_entrega, "") & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsTabela!hr_entrega, "")) & "," & BL_TrataStringParaBD(BL_HandleNull(rsTabela!nome, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsTabela!resp_entrega, "")) & ")"
        BG_ExecutaQuery_ADO sSql
        rsTabela.MoveNext
    Wend
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no PreencheTabela: " & sSql & vbCrLf & Err.Description, Me.Name, "PreencheTabela", False
    Exit Sub
    Resume Next
End Sub

'brunodsantos 30.11.2015 Glintt-HS-10690

Private Sub PreencheTabelaPorEntregar()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsTabela As New ADODB.recordset
    sSql = "DELETE FROM sl_cr_entrega_resultados WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    
    sSql = "SELECT sl_requis.n_req, sl_requis.dt_conclusao, sl_identif.utente, sl_identif.n_proc_1, sl_identif.nome_ute "
    sSql = sSql & ConstroiFROMPorEntregar & ConstroiWHEREPorEntregar & ConstroiORDERPorEntregar
    rsTabela.CursorLocation = adUseServer
    rsTabela.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTabela.Open sSql, gConexao
    While Not rsTabela.EOF
        sSql = "INSERT into sl_cr_entrega_resultados (nome_computador, num_sessao, n_req, dt_entrega, nr_utente, nome_utente, processo) VALUES("
        sSql = sSql & BL_TrataStringParaBD(gComputador) & "," & gNumeroSessao & "," & BL_HandleNull(rsTabela!n_req, "") & "," & BL_TrataDataParaBD(rsTabela!dt_conclusao, "") & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsTabela!Utente, "")) & "," & BL_TrataStringParaBD(BL_HandleNull(rsTabela!nome_ute, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsTabela!n_proc_1, "")) & ")"
        BG_ExecutaQuery_ADO sSql
        rsTabela.MoveNext
    Wend
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no PreencheTabela: " & sSql & vbCrLf & Err.Description, Me.Name, "PreencheTabela", False
    Exit Sub
    Resume Next
End Sub




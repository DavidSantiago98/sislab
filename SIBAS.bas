Attribute VB_Name = "SIBAS"
Option Explicit

Public Function SIBAS_InsereResultado(n_prescricao As String, cod_ana As String, resultado As String, _
                                      dt_val As String, hr_val As String, user_val As String) As Boolean
    Dim sSql As String
    Dim rsDad As New ADODB.recordset
    Dim rsCod As New ADODB.recordset
    Dim rsLab2 As New ADODB.recordset
    Dim iReg As Integer
    Dim cod_ana_sibas As String
    Dim reagente As String
    
    On Error GoTo TrataErro
    SIBAS_InsereResultado = False
    
    '-------------------------------------------------------------------------------------------
    ' VERIFICA SE EXISTE MESMO O REGISTO DE COLHEITA
    '-------------------------------------------------------------------------------------------
    sSql = "SELECT cod_dador FROM sb_dad_registo_colheita WHERE cod_colheita = " & n_prescricao
    rsDad.CursorLocation = adUseServer
    rsDad.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsDad.Open sSql, gConexao
    If rsDad.RecordCount >= 1 Then
        '-------------------------------------------------------------------------------------------
        ' VERIFICA CODIGO SIBAS E REAGENTE
        '-------------------------------------------------------------------------------------------
        sSql = "SELECT * FROM sb_cod_analises WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana)
        rsCod.CursorLocation = adUseServer
        rsCod.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsCod.Open sSql, gConexao
        If rsCod.RecordCount >= 1 Then
            cod_ana_sibas = BL_HandleNull(rsCod!Codigo, "")
            reagente = BL_HandleNull(rsCod!reagente, "")
            
            '-------------------------------------------------------------------------------------------
            ' VERIFICA SE EXISTE SB_DAD_LABORATORIO_2
            '-------------------------------------------------------------------------------------------
            sSql = "SELECT * FROM sb_dad_laboratorio_2 WHERE cod_colheita = " & n_prescricao & " AND cod_analise =" & cod_ana_sibas
            rsLab2.CursorLocation = adUseServer
            rsLab2.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsLab2.Open sSql, gConexao
            If rsLab2.RecordCount >= 1 Then
                If BL_HandleNull(rsLab2!user_validacao, "") = "" Then
                    '-------------------------------------------------------------------------------------------
                    ' EXISTE E NAO ESTA VALIDADO - UPDATE
                    '-------------------------------------------------------------------------------------------
                    sSql = "UPDATE sb_dad_laboratorio_2 SET resultado = " & BL_TrataStringParaBD(resultado) & ","
                    sSql = sSql & " user_validacao = " & BL_TrataStringParaBD(user_val) & ", dt_validacao = " & BL_TrataDataParaBD(dt_val)
                    sSql = sSql & " hr_validacao = " & BL_TrataStringParaBD(hr_val) & " WHERE cod_colheita = " & n_prescricao
                    sSql = sSql & " AND cod_analise = " & cod_ana_sibas
                    iReg = BG_ExecutaQuery_ADO(sSql)
                    If iReg <> 1 Then
                        GoTo TrataErro
                    End If
                End If
            Else
                '-------------------------------------------------------------------------------------------
                ' NAO EXISTE - INSERT
                '-------------------------------------------------------------------------------------------
                sSql = "INSERT INTO sb_dad_laboratorio_2(cod_colheita, cod_analise, resultado, user_cri, dt_cri, hr_cri,"
                sSql = sSql & " cod_reagente, laboratorio, user_validacao, dt_validacao, cod_local) VALUES("
                sSql = sSql & n_prescricao & "," & cod_ana_sibas & ", " & BL_TrataStringParaBD(resultado) & ","
                sSql = sSql & BL_TrataStringParaBD(user_val) & ", " & BL_TrataDataParaBD(dt_val) & ","
                sSql = sSql & BL_TrataStringParaBD(hr_val) & "," & BL_HandleNull(reagente, "NULL") & ",2,"
                sSql = sSql & BL_TrataStringParaBD(user_val) & ", " & BL_TrataDataParaBD(dt_val) & ","
                sSql = sSql & gCodLocal & ")"
                iReg = BG_ExecutaQuery_ADO(sSql)
                If iReg <> 1 Then
                    GoTo TrataErro
                End If
            End If
        End If
        rsCod.Close
        Set rsCod = Nothing
    End If
    rsDad.Close
    Set rsDad = Nothing
    SIBAS_InsereResultado = True
Exit Function
TrataErro:
    SIBAS_InsereResultado = False
    BG_LogFile_Erros "SIBAS_InsereResultado: " & Err.Description & " " & sSql, "SIBAS", "SIBAS_InsereResultado"
    Exit Function
End Function

VERSION 5.00
Begin VB.Form FormAnaExt 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAnaExt"
   ClientHeight    =   5595
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8355
   Icon            =   "FormAnaExt.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5595
   ScaleWidth      =   8355
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcPesqAnaSel 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4560
      TabIndex        =   11
      Top             =   840
      Width           =   3375
   End
   Begin VB.TextBox EcPesqAna 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   240
      TabIndex        =   10
      Top             =   840
      Width           =   3375
   End
   Begin VB.CommandButton BtPesqEnt 
      Height          =   315
      Left            =   7560
      Picture         =   "FormAnaExt.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
      Top             =   120
      Width           =   375
   End
   Begin VB.TextBox EcDescrEnt 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   120
      Width           =   5895
   End
   Begin VB.TextBox EcCodEnt 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   960
      Locked          =   -1  'True
      MaxLength       =   4
      TabIndex        =   6
      Top             =   120
      Width           =   735
   End
   Begin VB.ListBox EcAnalises 
      Appearance      =   0  'Flat
      Height          =   4125
      Left            =   240
      MultiSelect     =   2  'Extended
      TabIndex        =   3
      Top             =   1200
      Width           =   3375
   End
   Begin VB.ListBox EcAnalisesSel 
      Appearance      =   0  'Flat
      Height          =   4125
      Left            =   4560
      TabIndex        =   2
      Top             =   1200
      Width           =   3375
   End
   Begin VB.CommandButton BtRetira 
      Height          =   495
      Left            =   3840
      Picture         =   "FormAnaExt.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   " Retirar do Perfil "
      Top             =   2835
      Width           =   495
   End
   Begin VB.CommandButton BtInsere 
      Height          =   495
      Left            =   3840
      Picture         =   "FormAnaExt.frx":0920
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   " Inserir no Perfil "
      Top             =   2280
      Width           =   495
   End
   Begin VB.Label Label4 
      BackColor       =   &H80000004&
      Caption         =   "Entidade"
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "An�lises Dispon�veis"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   5
      Top             =   600
      Width           =   2175
   End
   Begin VB.Label Label1 
      Caption         =   "An�lises Seleccionadas"
      Height          =   255
      Index           =   2
      Left            =   4680
      TabIndex        =   4
      Top             =   600
      Width           =   2295
   End
End
Attribute VB_Name = "FormAnaExt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 30/03/2003
' T�cnico : Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'
Private Type AnaEnt
    codEntidade As String
    seqAna As Long
    codAna As String
    descrAna As String
End Type
Dim estrutAnaEnt() As AnaEnt
Dim totalAnaEnt As Integer

Private Sub BD_Update()
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    If EcCodEnt = "" Then
        Exit Sub
    End If
    
    BG_BeginTransaction
    
    ' APAGA O QUE EXISTE
    sSql = "DELETE FROM sl_ent_ext_ana WHERE cod_ent_ext = " & BL_TrataStringParaBD(EcCodEnt)
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To totalAnaEnt
        sSql = "INSERT INTO sl_ent_ext_ana (cod_ent_ext, cod_ana) VALUES ("
        sSql = sSql & BL_TrataStringParaBD(estrutAnaEnt(i).codEntidade) & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutAnaEnt(i).codAna) & ") "
        BG_ExecutaQuery_ADO sSql
    Next
    BG_CommitTransaction
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro  ao Gravar An�lises: " & Err.Description, Me.Name, "BD_Update", True
    Exit Sub
    Resume Next
End Sub

Private Sub BtInsere_Click()
    Dim aux As String
    Dim sSql As String
    Dim rsDados As New ADODB.recordset
    Dim j As Integer
    Dim i As Integer
    Dim Existe As Boolean
    For i = 0 To EcAnalises.ListCount - 1
        Existe = False
        If EcAnalises.Selected(i) = True Then
            aux = EcAnalises.ItemData(i)
            For j = 1 To totalAnaEnt
                If estrutAnaEnt(j).seqAna = EcAnalises.ItemData(i) Then
                       Existe = True
                       Exit For
                End If
            Next
            If Existe = False Then
                sSql = "SELECT cod_ana, descr_ana FROM slv_analises "
                sSql = sSql & " WHERE seq_ana = " & EcAnalises.ItemData(i)
                rsDados.CursorLocation = adUseServer
                rsDados.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsDados.Open sSql, gConexao
                If rsDados.RecordCount = 1 Then
                
                    totalAnaEnt = totalAnaEnt + 1
                    ReDim Preserve estrutAnaEnt(totalAnaEnt)
                    estrutAnaEnt(totalAnaEnt).seqAna = EcAnalises.ItemData(i)
                    estrutAnaEnt(totalAnaEnt).codEntidade = EcCodEnt
                    estrutAnaEnt(totalAnaEnt).descrAna = BL_HandleNull(rsDados!descr_ana, "")
                    estrutAnaEnt(totalAnaEnt).codAna = BL_HandleNull(rsDados!cod_ana, "")
                    
                    EcAnalisesSel.AddItem estrutAnaEnt(totalAnaEnt).descrAna
                    EcAnalisesSel.ItemData(EcAnalisesSel.NewIndex) = CLng(aux)
                End If
                rsDados.Close
            End If
        End If
    Next
End Sub

Private Sub BtRetira_Click()
    Dim i As Integer
    Dim j As Integer
    If EcAnalisesSel.ListIndex > -1 Then
        j = EcAnalisesSel.ListIndex
        For i = EcAnalisesSel.ListIndex + 1 To totalAnaEnt - 1
            estrutAnaEnt(i).codAna = estrutAnaEnt(i + 1).codAna
            estrutAnaEnt(i).codEntidade = estrutAnaEnt(i + 1).codEntidade
            estrutAnaEnt(i).descrAna = estrutAnaEnt(i + 1).descrAna
            estrutAnaEnt(i).seqAna = estrutAnaEnt(i + 1).seqAna
        Next
        totalAnaEnt = totalAnaEnt - 1
        ReDim Preserve estrutAnaEnt(totalAnaEnt)
        EcAnalisesSel.RemoveItem EcAnalisesSel.ListIndex
        If j < EcAnalisesSel.ListCount Then
            EcAnalisesSel.ListIndex = j
            EcAnalisesSel.SetFocus
        End If
    End If
End Sub


Private Sub EcPesqAna_Change()
    If Trim(EcPesqAna) = "" Or EcPesqAna = "*" Then
        BG_PreencheComboBD_ADO "slv_analises", "seq_ana", "descr_ana", EcAnalises, mediAscComboDesignacao
    Else
        BG_PreencheComboBD_ADO "SELECT seq_ana, descr_ana FROM slv_analises WHERE upper(descr_ana) like '%" & UCase(EcPesqAna) & "%'", "seq_ana", "descr_ana", EcAnalises, mediAscComboDesignacao
    End If

End Sub

Private Sub EcPesqAnaSel_Change()
    Dim i As Integer
    EcAnalisesSel.ListIndex = mediComboValorNull
    EcAnalisesSel.TopIndex = 0
    
    For i = 0 To EcAnalisesSel.ListCount - 1
        If InStr(1, UCase(EcAnalisesSel.List(i)), UCase(EcPesqAnaSel)) > 0 Then
            EcAnalisesSel.ListIndex = i
            EcAnalisesSel.Selected(i) = True
            EcAnalisesSel.TopIndex = i
            Exit For
        End If
    Next
End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()

    Me.caption = " Codifica��o de An�lises Associadas a Entidades Externas"
    Me.left = 540
    Me.top = 450
    Me.Width = 8445
    Me.Height = 6030 ' Normal
    'Me.Height = 8825 ' Campos Extras
    
    Set CampoDeFocus = EcCodEnt
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

   
    Call FuncaoProcurar
    
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "Activo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"

'    Me.opC.Value = True
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormAnaExt = Nothing

End Sub

Sub LimpaCampos()
    
    ' Me.SetFocus
    EcAnalises.Clear
    EcAnalisesSel.Clear
    EcCodEnt = ""
    EcDescrEnt = ""
    

End Sub

Sub DefTipoCampos()
    BG_PreencheComboBD_ADO "slv_analises", "seq_ana", "descr_ana", EcAnalises, mediAscComboDesignacao
    
'nada
End Sub

Sub PreencheValoresDefeito()
    

End Sub

Sub PreencheCampos()
    
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

    Me.EcCodEnt.Locked = False

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        
        
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If

End Sub

Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    totalAnaEnt = 0
    ReDim estrutAnaEnt(totalAnaEnt)
    EcAnalisesSel.Clear
    EcAnalises.Clear
    
    If EcCodEnt <> "" Then
        sSql = "SELECT x1.cod_ent_ext,  x1.cod_ana, x2.descr_ana,x2.seq_ana "
        sSql = sSql & " FROM sl_ent_ext_ana x1, slv_analises x2"
        sSql = sSql & " WHERE cod_ent_ext = " & BL_TrataStringParaBD(EcCodEnt) & " AND x1.cod_ana = x2.cod_ana "

        
        Set rsAna = New ADODB.recordset
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            While Not rsAna.EOF
                totalAnaEnt = totalAnaEnt + 1
                ReDim Preserve estrutAnaEnt(totalAnaEnt)
                estrutAnaEnt(totalAnaEnt).seqAna = BL_HandleNull(rsAna!seq_ana, "")
                estrutAnaEnt(totalAnaEnt).codAna = BL_HandleNull(rsAna!cod_ana, "")
                estrutAnaEnt(totalAnaEnt).descrAna = BL_HandleNull(Trim(rsAna!descr_ana), "(Sem Descri��o)")
                estrutAnaEnt(totalAnaEnt).codEntidade = BL_HandleNull(rsAna!cod_ent_ext, "")
                

                EcAnalisesSel.AddItem estrutAnaEnt(totalAnaEnt).descrAna
                EcAnalisesSel.ItemData(EcAnalisesSel.NewIndex) = estrutAnaEnt(totalAnaEnt).seqAna
                rsAna.MoveNext
            Wend
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
End Sub

Sub FuncaoAnterior()
    
'nada

End Sub

Sub FuncaoSeguinte()
    
'nada
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = " Modificar"
    gMsgMsg = "Tem a certeza que deseja validar as altera��es efectuadas ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        
        BD_Update
        BL_FimProcessamento Me
    End If

End Sub



Sub BD_Insert()
    'nada
End Sub
Sub FuncaoInserir()

    Dim iRes As Integer
    Dim SQLQuery As String
    Dim rsInsAux As ADODB.recordset

    gMsgTitulo = " Inserir"
    gMsgMsg = "Tem a certeza que deseja Inserir estes dados ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me
        BD_Insert
        BL_FimProcessamento Me
    End If
    
End Sub


Sub FuncaoRemover()

    Dim sql As String

    gMsgTitulo = " Remover"
    gMsgMsg = "Tem a certeza que deseja apagar o registo ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BD_Delete
    End If
    
End Sub

Sub BD_Delete()
    
'nada

End Sub


Public Sub eccodent_Validate(Cancel As Boolean)

    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset

    EcCodEnt.Text = UCase(EcCodEnt.Text)
    If Trim(EcCodEnt) <> "" Then

        sql = "SELECT " & _
              "     descr_ent_ext " & _
              "FROM " & _
              "sl_ent_ext " & _
              "WHERE " & _
              "     cod_ent_ext=" & BL_TrataStringParaBD(EcCodEnt)

        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao

        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Entidade inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrEnt = ""
            EcCodEnt = ""
        Else
            EcDescrEnt.Text = rsCodigo!descr_ent_ext
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrEnt.Text = ""
        EcCodEnt = ""
    End If

End Sub







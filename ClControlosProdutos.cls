VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClControlosProdutos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public CtlCodProd As Object
Public CtlDescrProd As Object
Public CtlCodEspecif As Object
Public CtlDescrEspecif As Object
Public CtlDtPrev As Object
Public CtlDtChega As Object
Public CtlEstadoProd As Object
' constantes para as cores
Private Const FORECOLOR_SELEC_ENABLED = &H80000008
Private Const BACKCOLOR_SELEC_ENABLED = &H80000005
Private Const FORECOLOR_SELEC_DISABLED = &H80000008
Private Const BACKCOLOR_SELEC_DISABLED = &H8000000F
Private Const FORECOLOR_ENABLED = &H80000008
Private Const BACKCOLOR_ENABLED = &H80000009
Private Const FORECOLOR_DISABLED = &H80000008
Private Const BACKCOLOR_DISABLED = &H8000000F '&HE0E0E0

Public Sub EscondeLinha(Indice As Integer)
    On Error GoTo HandleError
    CtlCodProd(Indice).Visible = False
    CtlDescrProd(Indice).Visible = False
    CtlCodEspecif(Indice).Visible = False
    CtlDescrEspecif(Indice).Visible = False
    CtlDtPrev(Indice).Visible = False
    CtlDtChega(Indice).Visible = False
    CtlEstadoProd(Indice).Visible = False
    LimpaLinha Indice
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "CLControlosProdutos.escondeLinha"

End Sub
Public Sub LimpaLinha(Indice As Integer)
    On Error GoTo HandleError
    CtlCodProd(Indice).Text = ""
    CtlDescrProd(Indice).Caption = ""
    CtlCodEspecif(Indice).Text = ""
    CtlDescrEspecif(Indice).Caption = ""
    CtlDtPrev(Indice).Text = ""
    CtlDtChega(Indice).Text = ""
    CtlEstadoProd(Indice).Picture = Nothing
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "CLProdutos.LimpaLinha"

End Sub

Public Sub MostraLinha(Indice As Integer)
    On Error GoTo HandleError
    CtlCodProd(Indice).Visible = True
    CtlDescrProd(Indice).Visible = True
    CtlCodEspecif(Indice).Visible = True
    CtlDescrEspecif(Indice).Visible = True
    CtlDtPrev(Indice).Visible = True
    CtlDtChega(Indice).Visible = True
    CtlEstadoProd(Indice).Visible = True
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "CLProdutos.MostraLinha"

End Sub
Public Sub CarregaControlo(Indice As Integer, Deslocacao As Long)
    'On Error Resume Next
    On Error GoTo HandleError
    BG_LogFile "CarregaControlo indice:" & Indice
    Load CtlCodProd(Indice)
    CtlCodProd(Indice).Top = CtlCodProd(Indice).Top + (Indice * Deslocacao)
    Load CtlDescrProd(Indice)
    CtlDescrProd(Indice).Top = CtlDescrProd(Indice).Top + (Indice * Deslocacao)
    Load CtlCodEspecif(Indice)
    CtlCodEspecif(Indice).Top = CtlCodEspecif(Indice).Top + (Indice * Deslocacao)
    Load CtlDescrEspecif(Indice)
    CtlDescrEspecif(Indice).Top = CtlDescrEspecif(Indice).Top + (Indice * Deslocacao)
    'BL_DesactivaCampo CtlDescricaoRubrica(Indice)
    Load CtlDtPrev(Indice)
    CtlDtPrev(Indice).Top = CtlDtPrev(Indice).Top + (Indice * Deslocacao)
    'BL_DesactivaCampo CtlTipoPag(Indice)
    Load CtlDtChega(Indice)
    CtlDtChega(Indice).Top = CtlDtChega(Indice).Top + (Indice * Deslocacao)
    Load CtlEstadoProd(Indice)
    CtlEstadoProd(Indice).Top = CtlEstadoProd(Indice).Top + (Indice * Deslocacao)
    'BL_DesactivaCampo CtlTotalLinha(Indice)
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "CLProdutos.CarregaLinha"
End Sub
Public Sub DescarregaControlo(Indice As Integer)
    On Error GoTo HandleError
    Unload CtlCodProd(Indice)
    Unload CtlDescrProd(Indice)
    Unload CtlCodEspecif(Indice)
    Unload CtlDescrEspecif(Indice)
    Unload CtlDtPrev(Indice)
    Unload CtlDtChega(Indice)
    Unload CtlEstadoProd(Indice)
Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "CLProdutos.DescarregaControlo"
End Sub

Public Sub MudaLinhaCursor(CampoActivo As Control, Optional NovaLinha)
    Dim IndiceActivo As Integer
    On Error GoTo HandleError
    If CampoActivo.Enabled And CampoActivo.Visible Then
            CampoActivo.SetFocus
        End If
    IndiceActivo = CampoActivo.Index
    ActivaLinha IndiceActivo
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "clControlosProdutos.MudaLinhaCursor"

 End Sub
Public Sub ActivaLinha(Indice As Integer)
    On Error GoTo HandleError
    CtlCodProd(Indice).BackColor = BACKCOLOR_SELEC_ENABLED
    CtlCodProd(Indice).ForeColor = FORECOLOR_SELEC_ENABLED
    CtlCodProd(Indice).FontBold = True
    CtlDescrProd(Indice).BackColor = BACKCOLOR_SELEC_DISABLED
    CtlDescrProd(Indice).ForeColor = FORECOLOR_SELEC_DISABLED
    CtlDescrProd(Indice).FontBold = True
    CtlCodEspecif(Indice).BackColor = BACKCOLOR_SELEC_ENABLED
    CtlCodEspecif(Indice).ForeColor = FORECOLOR_SELEC_ENABLED
    CtlCodEspecif(Indice).FontBold = True
    CtlDescrEspecif(Indice).BackColor = BACKCOLOR_SELEC_DISABLED
    CtlDescrEspecif(Indice).ForeColor = FORECOLOR_SELEC_DISABLED
    CtlDescrEspecif(Indice).FontBold = True
    CtlDtPrev(Indice).BackColor = BACKCOLOR_SELEC_ENABLED
    CtlDtPrev(Indice).ForeColor = FORECOLOR_SELEC_ENABLED
    CtlDtPrev(Indice).FontBold = True
    CtlDtChega(Indice).BackColor = BACKCOLOR_SELEC_DISABLED
    CtlDtChega(Indice).ForeColor = FORECOLOR_SELEC_DISBLED
    CtlDtChega(Indice).FontBold = True
    CtlEstadoProd(Indice).BackColor = BACKCOLOR_SELEC_DISABLED
    CtlEstadoProd(Indice).ForeColor = FORECOLOR_SELEC_DISABLED
    CtlEstadoProd(Indice).FontBold = True
    Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "clControlosInsereDadosFact.ActivaLinha"

End Sub

Public Sub DesactivaLinha(Indice As Integer)
    On Error GoTo HandleError
    CtlCodProd(Indice).BackColor = BACKCOLOR_ENABLED
    CtlCodProd(Indice).ForeColor = FORECOLOR_ENABLED
    CtlCodProd(Indice).FontBold = False
    CtlDescrProd(Indice).BackColor = BACKCOLOR_DISABLED
    CtlDescrProd(Indice).ForeColor = FORECOLOR_DISABLED
    CtlDescrProd(Indice).FontBold = False
    CtlCodEspecif(Indice).BackColor = BACKCOLOR_ENABLED
    CtlCodEspecif(Indice).ForeColor = FORECOLOR_ENABLED
    CtlCodEspecif(Indice).FontBold = False
    CtlDescrEspecif(Indice).BackColor = BACKCOLOR_DISABLED
    CtlDescrEspecif(Indice).ForeColor = FORECOLOR_DISABLED
    CtlDescrEspecif(Indice).FontBold = False
    CtlDtPrev(Indice).BackColor = BACKCOLOR_ENABLED
    CtlDtPrev(Indice).ForeColor = FORECOLOR_ENABLED
    CtlDtPrev(Indice).FontBold = False
    CtlDtChega(Indice).BackColor = BACKCOLOR_DISABLED
    CtlDtChega(Indice).ForeColor = FORECOLOR_DISABLED
    CtlDtChega(Indice).FontBold = False
    CtlEstadoProd(Indice).BackColor = BACKCOLOR_DISABLED
    CtlEstadoProd(Indice).ForeColor = FORECOLOR_DISABLED
    CtlEstadoProd(Indice).FontBold = False
Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "CLProdutos.DesactivaLinha"
End Sub

Public Sub MudaLinhaActiva(Indice As Integer)
    On Error GoTo HandleError
    If CtlCodProd(Indice).Enabled And CtlCodProd(Indice).Visible Then
        CtlCodProd(Indice).SetFocus
    End If
Exit Sub
HandleError:
    BL_TrataErroRuntime Err.Number, Err.Description, "CLProdutos.MudaLinhaActiva"
End Sub
Sub PreencheValoresDefeito(PosicaoListaControlos As Integer)

End Sub


VERSION 5.00
Begin VB.Form FormEstatGrupo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   7530
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7605
   Icon            =   "FormEstatGrupo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7530
   ScaleWidth      =   7605
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox Flg_PesoEstat 
      Caption         =   "Peso Estat�stico"
      Height          =   255
      Left            =   480
      TabIndex        =   41
      Top             =   7200
      Width           =   2655
   End
   Begin VB.Frame Frame1 
      Caption         =   "Condi��es de Pesquisa"
      Height          =   6135
      Left            =   120
      TabIndex        =   5
      Top             =   960
      Width           =   7335
      Begin VB.CommandButton BtPesquisaPosto 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatGrupo.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   46
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   3120
         Width           =   375
      End
      Begin VB.TextBox EcDescrPosto 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   45
         TabStop         =   0   'False
         Top             =   3120
         Width           =   4095
      End
      Begin VB.TextBox EcCodPosto 
         Height          =   315
         Left            =   1560
         TabIndex        =   44
         Top             =   3120
         Width           =   735
      End
      Begin VB.ComboBox CbLocal 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   42
         Top             =   360
         Width           =   4815
      End
      Begin VB.CheckBox Flg_DescrRequis 
         Caption         =   "Descriminar Requisi��es"
         Height          =   255
         Left            =   360
         TabIndex        =   40
         Top             =   5640
         Width           =   2055
      End
      Begin VB.ComboBox CbAnalises 
         Height          =   315
         Left            =   4920
         Style           =   2  'Dropdown List
         TabIndex        =   32
         Top             =   1320
         Width           =   1455
      End
      Begin VB.ComboBox EcDescrSexo 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   31
         Top             =   840
         Width           =   1455
      End
      Begin VB.CheckBox Flg_DetAna 
         Caption         =   "Detalhe das An�lises"
         Height          =   195
         Left            =   2640
         TabIndex        =   30
         Top             =   5400
         Width           =   1815
      End
      Begin VB.CheckBox Flg_DescrAna 
         Caption         =   "Descriminar An�lises"
         Height          =   255
         Left            =   360
         TabIndex        =   29
         Top             =   5340
         Width           =   2055
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   4920
         Style           =   2  'Dropdown List
         TabIndex        =   28
         Top             =   840
         Width           =   1455
      End
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   27
         Top             =   1320
         Width           =   1455
      End
      Begin VB.TextBox EcCodEFR 
         Height          =   315
         Left            =   1560
         TabIndex        =   26
         Top             =   2700
         Width           =   735
      End
      Begin VB.TextBox EcDescrEFR 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   2700
         Width           =   4095
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatGrupo.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   24
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   2700
         Width           =   375
      End
      Begin VB.TextBox EcCodProveniencia 
         Height          =   315
         Left            =   1560
         TabIndex        =   23
         Top             =   2280
         Width           =   735
      End
      Begin VB.TextBox EcDescrProveniencia 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   2280
         Width           =   4095
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatGrupo.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   21
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   2280
         Width           =   375
      End
      Begin VB.ComboBox CbGrupo 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   1800
         Width           =   2655
      End
      Begin VB.Frame Frame2 
         Caption         =   "An�lise"
         Height          =   1695
         Left            =   360
         TabIndex        =   7
         Top             =   3480
         Width           =   6615
         Begin VB.OptionButton OptSimples 
            Caption         =   "Simples"
            Height          =   255
            Left            =   240
            TabIndex        =   19
            Top             =   360
            Width           =   855
         End
         Begin VB.OptionButton OptComplexas 
            Caption         =   "Complexas"
            Height          =   255
            Left            =   240
            TabIndex        =   18
            Top             =   840
            Width           =   1095
         End
         Begin VB.OptionButton OptPerfis 
            Caption         =   "Perfis"
            Height          =   255
            Left            =   240
            TabIndex        =   17
            Top             =   1320
            Width           =   1095
         End
         Begin VB.TextBox EcDescrAnaS 
            Enabled         =   0   'False
            Height          =   285
            Left            =   2280
            TabIndex        =   16
            Top             =   360
            Width           =   3615
         End
         Begin VB.TextBox EcCodAnaS 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1440
            TabIndex        =   15
            Top             =   360
            Width           =   855
         End
         Begin VB.TextBox EcCodAnaC 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1440
            TabIndex        =   14
            Top             =   840
            Width           =   855
         End
         Begin VB.TextBox EcCodAnaP 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1440
            TabIndex        =   13
            Top             =   1320
            Width           =   855
         End
         Begin VB.TextBox EcDescrAnaC 
            Enabled         =   0   'False
            Height          =   285
            Left            =   2280
            TabIndex        =   12
            Top             =   840
            Width           =   3615
         End
         Begin VB.TextBox EcDescrAnaP 
            Enabled         =   0   'False
            Height          =   285
            Left            =   2280
            TabIndex        =   11
            Top             =   1320
            Width           =   3615
         End
         Begin VB.CommandButton BtPesqRapS 
            Caption         =   "..."
            Enabled         =   0   'False
            Height          =   320
            Left            =   5880
            Style           =   1  'Graphical
            TabIndex        =   10
            ToolTipText     =   "Procurar An�lises Simples"
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton BtPesqRapC 
            Caption         =   "..."
            Enabled         =   0   'False
            Height          =   320
            Left            =   5880
            Style           =   1  'Graphical
            TabIndex        =   9
            ToolTipText     =   "Procurar An�lises Simples"
            Top             =   840
            Width           =   375
         End
         Begin VB.CommandButton BtPesqRapP 
            Caption         =   "..."
            Enabled         =   0   'False
            Height          =   320
            Left            =   5880
            Style           =   1  'Graphical
            TabIndex        =   8
            ToolTipText     =   "Procurar An�lises Simples"
            Top             =   1320
            Width           =   375
         End
      End
      Begin VB.CheckBox Flg_OrderProduto 
         Caption         =   "Ordenar por produto"
         Height          =   195
         Left            =   4680
         TabIndex        =   6
         Top             =   5400
         Width           =   2175
      End
      Begin VB.Label Label8 
         Caption         =   "&Sala/Posto"
         Height          =   255
         Left            =   360
         TabIndex        =   47
         Top             =   3120
         Width           =   975
      End
      Begin VB.Label Label7 
         Caption         =   "Local"
         Height          =   195
         Left            =   360
         TabIndex        =   43
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label5 
         Caption         =   "An�lises"
         Height          =   255
         Left            =   4080
         TabIndex        =   39
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Se&xo"
         Height          =   195
         Left            =   360
         TabIndex        =   38
         Top             =   840
         Width           =   480
      End
      Begin VB.Label Label6 
         Caption         =   "Prio&ridade"
         Height          =   255
         Left            =   4080
         TabIndex        =   37
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   360
         TabIndex        =   36
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label Label10 
         Caption         =   "E.&F.R."
         Height          =   255
         Left            =   360
         TabIndex        =   35
         Top             =   2700
         Width           =   495
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Left            =   360
         TabIndex        =   34
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Grupo An�lises"
         Height          =   255
         Left            =   360
         TabIndex        =   33
         Top             =   1800
         Width           =   1095
      End
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   7335
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   3120
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   1560
         TabIndex        =   1
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   720
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2850
         TabIndex        =   3
         Top             =   360
         Width           =   255
      End
   End
End
Attribute VB_Name = "FormEstatGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim NRegistos As Long
Dim NAnaSMarc As Long
Dim NAnaCMarc As Long
Dim NAnaSReal As Long
Dim NAnaCReal As Long
Dim NAnaSReq As Long
Dim NAnaCReq As Long
Dim NAnaTabela As Long
Dim TotalUtentes As Long
Dim totalRequis As Long

Public rs As ADODB.recordset

Sub Preenche_EstatisticaGrupo()
    
    Dim sql As String
    Dim continua As Boolean
            
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estat�stica por Laborat�rio") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    
    'Printer Common Dialog
        If gImprimirDestino = 1 Then
            continua = BL_IniciaReport("EstatisticaGrupo", "Estat�stica por Laborat�rio", crptToPrinter)
        Else
            continua = BL_IniciaReport("EstatisticaGrupo", "Estat�stica por Laborat�rio", crptToWindow)
        End If
        If continua = False Then Exit Sub
    
    Call Cria_TmpRec_Estatistica
    
    BG_BeginTransaction
    PreencheTabelaTemporaria
    BG_CommitTransaction
    
'    If NAnaTabela = 0 Then
'        Call BG_Mensagem(mediMsgBox, "N�o foram encontradas an�lises requisitadas para iniciar um Relat�rio", vbOKOnly + vbExclamation, App.ProductName)
'        Exit Sub
'    End If
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = " SELECT   sl_cr_estatgrupo.descr_grupo, sl_cr_estatgrupo.tipo_ana, " & _
         "sl_cr_estatgrupo.descricao, sl_cr_estatgrupo.codigo, " & _
         "sl_cr_estatgrupo.peso, sl_cr_estatgrupo.n_req, " & _
         " sl_cr_estatgrupo.descr_sgr_ana, sl_cr_estatgrupo.cod_rubrica, " & _
         " sl_cr_estatgrupo.utente , sl_requis.dt_chega, sl_identif.nome_ute " & _
    "FROM SL_CR_REL" & gNumeroSessao & "  sl_cr_estatgrupo, " & _
         " sl_requis sl_requis, " & _
         " sl_identif sl_identif " & _
   "Where sl_cr_estatgrupo.n_req = sl_requis.n_req " & _
     "AND sl_requis.seq_utente = sl_identif.seq_utente " & _
"ORDER BY sl_cr_estatgrupo.descr_sgr_ana ASC, " & _
         "sl_cr_estatgrupo.descr_grupo ASC, " & _
         "sl_cr_estatgrupo.tipo_ana ASC, " & _
         "sl_cr_estatgrupo.descricao ASC, " & _
         "sl_cr_estatgrupo.n_req Asc "

'    Report.SQLQuery = "SELECT SL_CR_ESTATGRUPO.DESCR_GRUPO, SL_CR_ESTATGRUPO.TIPO_ANA, SL_CR_ESTATGRUPO.DESCRICAO" & _
'        " FROM SL_CR_REL" & gNumeroSessao & " SL_CR_ESTATGRUPO, SL_REQUIS, SL_IDENTIF " & _
'        " WHERE SL_CR_ESTATGRUPO.N_REQ = SL_REQUIS.N_REQ AND SL_REQUIS.SEQ_UTENTE = SL_IDENTIF.SEQ_UTENTE "
'    If Flg_OrderProduto.Value = 1 Then
'        Report.SQLQuery = Report.SQLQuery & " ORDER BY DESCR_SGR_ANA, DESCR_GRUPO, TIPO_ANA, DESCRICAO "
'    Else
'        Report.SQLQuery = Report.SQLQuery & " ORDER BY DESCR_GRUPO, TIPO_ANA, DESCRICAO "
'    End If
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.text)
    If EcCodEFR = "" Then
        Report.formulas(2) = "EFR=" & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(2) = "EFR=" & BL_TrataStringParaBD("" & EcDescrEFR.text)
    End If
    If EcCodProveniencia = "" Then
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("" & EcDescrProveniencia.text)
    End If
    If CbUrgencia.ListIndex = -1 Then
        Report.formulas(4) = "Urgencia=" & BL_TrataStringParaBD("-")
    Else
        Report.formulas(4) = "Urgencia=" & BL_TrataStringParaBD("" & CbUrgencia.text)
    End If
    If CbSituacao.ListIndex = -1 Then
        Report.formulas(5) = "Situacao=" & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(5) = "Situacao=" & BL_TrataStringParaBD("" & CbSituacao.text)
    End If
    If EcDescrSexo.ListIndex = -1 Then
        Report.formulas(6) = "Sexo=" & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(6) = "Sexo=" & BL_TrataStringParaBD("" & EcDescrSexo.text)
    End If
    If CbAnalises.ListIndex = -1 Then
        Report.formulas(7) = "Analises= " & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(7) = "Analises= " & BL_TrataStringParaBD("" & CbAnalises.text)
    End If
    If Flg_DescrAna.Value = 1 Then
        Report.formulas(8) = "DescrAna=" & BL_TrataStringParaBD(" S")
    Else
        Report.formulas(8) = "DescrAna=" & BL_TrataStringParaBD(" N")
    End If
    If Flg_DetAna.Value = 1 Then
        Report.formulas(9) = "DetAna=" & BL_TrataStringParaBD(" S")
    Else
        Report.formulas(9) = "DetAna=" & BL_TrataStringParaBD(" N")
    End If
    If Flg_PesoEstat.Value = 1 Then
        Report.formulas(10) = "PesoEstat=" & BL_TrataStringParaBD(" S")
    Else
        Report.formulas(10) = "PesoEstat=" & BL_TrataStringParaBD(" N")
    End If
    If Flg_OrderProduto.Value = 1 Then
        Report.formulas(11) = "OrderProduto=" & "'1'"
    Else
        Report.formulas(11) = "OrderProduto=" & "'0'"
    End If
    If Flg_DescrRequis.Value = 1 Then
        Report.formulas(12) = "DescrRequis=" & BL_TrataStringParaBD("S")
    End If
    Report.formulas(13) = "TotalUtentes=" & BL_TrataStringParaBD(CStr(TotalUtentes))
    Report.formulas(14) = "TotalRequis=" & BL_TrataStringParaBD(CStr(totalRequis))
    If EcCodPosto <> "" Then
        Report.formulas(15) = "Posto=" & BL_TrataStringParaBD(EcDescrPosto)
    End If
    
'    Me.SetFocus
    
    Report.SubreportToChange = "SubRepEstatGrupo"
    Report.Connect = "DSN=" & gDSN
    
    Report.SubreportToChange = ""
    
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
 
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    Call BL_RemoveTabela("SL_CR_REL" & gNumeroSessao)
    
End Sub

Sub PreencheTabelaTemporaria()

    Dim sql As String
    Dim SqlC As String
    Dim flg_tabela As Boolean
    Dim SqlR As String
    Dim SqlM As String
    Dim RsUtente As New ADODB.recordset
    
    'verifica os campos preenchidos
    SqlC = " AND sl_requis.estado_req <> 'C' "
    
    'se grupo preenchido
    If (CbGrupo.ListIndex <> -1) Then
        SqlC = SqlC & " AND sl_gr_ana.descr_gr_ana = " & BL_TrataStringParaBD(CbGrupo)
    End If
    
    'se sexo preenchido
    If (EcDescrSexo.ListIndex <> -1) Then
        SqlC = SqlC & " AND sl_identif.sexo_ute = " & EcDescrSexo.ListIndex
    End If
    
    'Data preenchida
    SqlC = SqlC & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    
    'Situa��o preenchida?
    If (CbSituacao.ListIndex <> -1) Then
        SqlC = SqlC & " AND sl_requis.T_sit= " & CbSituacao.ListIndex
    End If
    
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> -1) Then
        SqlC = SqlC & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(Left(CbUrgencia.text, 1))
    End If
    
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    If EcCodProveniencia.text <> "" Then
        SqlC = SqlC & " AND sl_requis.Cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.text)
    End If
    
    'C�digo do Posto preenchido?
    If EcCodPosto.text <> "" Then
        SqlC = SqlC & " AND sl_requis.Cod_sala=" & BL_TrataStringParaBD(EcCodPosto.text)
    End If
    
    'C�digo da EFR preenchido?
    If EcCodEFR.text <> "" Then
        SqlC = SqlC & " AND sl_requis.Cod_efr=" & BL_TrataStringParaBD(EcCodEFR.text)
    End If
    
    'local preenchido?
    If CbLocal.ListIndex <> -1 Then
        SqlC = SqlC & " AND sl_requis.cod_local=" & CbLocal.ItemData(CbLocal.ListIndex)
    End If
    
    'Analises preenchidas?
    If CbAnalises.ListIndex = 0 Then
        'Com Resultado
        If OptSimples.Value = True Then
            SqlC = SqlC & " AND sl_realiza.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS.text)
        ElseIf OptComplexas.Value = True Then
            SqlC = SqlC & " AND sl_realiza.cod_ana_c = " & BL_TrataStringParaBD(EcCodAnaC.text)
        ElseIf OptPerfis.Value = True Then
            SqlC = SqlC & " AND sl_realiza.cod_perfil = " & BL_TrataStringParaBD(EcCodAnaP.text)
        End If
    ElseIf CbAnalises.ListIndex = 1 Then
        'Sem Resultado
        If OptSimples.Value = True Then
            SqlC = SqlC & " AND sl_marcacoes.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS.text)
        ElseIf OptComplexas.Value = True Then
            SqlC = SqlC & " AND sl_marcacoes.cod_ana_c = " & BL_TrataStringParaBD(EcCodAnaC.text)
        ElseIf OptPerfis.Value = True Then
            SqlC = SqlC & " AND sl_marcacoes.cod_perfil = " & BL_TrataStringParaBD(EcCodAnaP.text)
        End If
    Else
        SqlR = ""
        SqlM = ""
        'Todas
        'Com Resultado
        If OptSimples.Value = True Then
            SqlR = SqlR & " AND sl_realiza.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS.text)
        ElseIf OptComplexas.Value = True Then
            SqlR = SqlR & " AND sl_realiza.cod_ana_c = " & BL_TrataStringParaBD(EcCodAnaC.text)
        ElseIf OptPerfis.Value = True Then
            SqlR = SqlR & " AND sl_realiza.cod_perfil = " & BL_TrataStringParaBD(EcCodAnaP.text)
        End If
        'Sem Resultado
        If OptSimples.Value = True Then
            SqlM = SqlM & " AND sl_marcacoes.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS.text)
        ElseIf OptComplexas.Value = True Then
            SqlM = SqlM & " AND sl_marcacoes.cod_ana_c = " & BL_TrataStringParaBD(EcCodAnaC.text)
        ElseIf OptPerfis.Value = True Then
            SqlM = SqlM & " AND sl_marcacoes.cod_perfil = " & BL_TrataStringParaBD(EcCodAnaP.text)
        End If
        
    End If
    
    If CbAnalises.ListIndex = 0 Then
        '___________________________________________________________________________
        'insere na tabela tempor�ria todos os perfis c/ resultado que satisfazem os campos preenchidos
        sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ, DESCR_SGR_ANA, COD_RUBRICA) " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Perfis', sl_perfis.descr_perfis, sl_perfis.cod_perfis, sl_perfis.peso, sl_realiza.n_req," & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ",  sl_ana_facturacao.cod_ana_gh " & _
        " FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        " WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        " AND sl_gr_ana.cod_gr_ana = sl_perfis.gr_ana " & _
        " AND sl_sgr_ana.cod_sgr_ana(+) = sl_perfis.sgr_ana " & _
        " AND sl_realiza.n_req = sl_requis.n_req " & _
        " AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
        " AND sl_perfis.peso <> '0' " & _
        " AND sl_perfis.cod_perfis = sl_ana_facturacao.cod_ana (+) "
        sql = sql & SqlC
        
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
        
        '______________________________________________________________________________
        'insere na tabela tempor�ria todas as anas. complexas c/ resultado que satisfazem os campos preenchidos
        sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ, DESCR_SGR_ANA, COD_RUBRICA) " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso, sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ",  sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_c, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_c.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_realiza.cod_perfil = '0' and sl_ana_c.peso <> 0 " & _
        " AND sl_ana_c.cod_ana_c = sl_ana_facturacao.cod_ana (+) "
        
        
        sql = sql & SqlC
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso, sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ",  sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_c, sl_perfis, sl_sgr_ana,sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_c.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_realiza.cod_perfil <> '0' and sl_ana_c.peso <> 0 and sl_perfis.peso = 0 " & _
        " AND sl_ana_c.cod_ana_c = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC
        
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
              
        '_____________________________________________________________________________
        'insere na tabela tempor�ria todas anas. simples com resultado q satisfa�am os campos preenchidos
        sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ,DESCR_SGR_ANA, COD_RUBRICA) " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ",  sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_realiza.cod_ana_c = '0' and sl_realiza.cod_perfil = '0' and sl_ana_s.peso <> 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "

        sql = sql & SqlC
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ",  sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_ana_c, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_realiza.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_ana_c.peso = 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_realiza.cod_perfil <> '0' and sl_realiza.cod_ana_c = '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_perfis, sl_ana_c, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_realiza.cod_perfil <> '0' and sl_realiza.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 and sl_ana_c.peso = 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC
        
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro

            
    ElseIf CbAnalises.ListIndex = 1 Then
        '____________________________________________________________________________
        'insere na tabela tempor�ria todas os perfis s/ resultado que satisfazem os campos preenchidos
        sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ, DESCR_SGR_ANA, COD_RUBRICA) " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Perfis', sl_perfis.descr_perfis, sl_perfis.cod_perfis, sl_perfis.peso, sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_perfis.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_perfis.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_perfis.peso <> 0 " & _
        " AND sl_perfis.cod_perfis = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC
        
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
        
        '_____________________________________________________________________________
        'insere na tabela tempor�ria todas as anas. complexas s/ resultado que satisfazem os campos preenchidos
        sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ, DESCR_SGR_ANA, COD_RUBRICA) " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso, sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_c, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_c.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_marcacoes.cod_perfil = '0' and sl_ana_c.peso <> 0 " & _
        " AND sl_ana_c.cod_ana_c = sl_ana_facturacao.cod_ana (+) "

        sql = sql & SqlC
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso, sl_marcacoes.n_req, sl_sgr_ana.descr_sgr_ana  " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_c, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_c.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_marcacoes.cod_perfil <> '0' and sl_ana_c.peso <> 0 and sl_perfis.peso = 0 " & _
        " AND sl_ana_c.cod_ana_c = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC

        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro

        '__________________________________________________________________________
        'obtem todas anas. simples sem resultado q satisfa�am os campos preenchidos
        'Set RsAnaSMarc = New ADODB.Recordset
        sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO,CODIGO, PESO, N_REQ,DESCR_SGR_ANA, COD_RUBRICA) " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_marcacoes.cod_ana_c = '0' and sl_marcacoes.cod_perfil = '0' and sl_ana_s.peso <> 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "

        sql = sql & SqlC
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_ana_c, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_marcacoes.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_ana_c.peso = 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        
        
        sql = sql & SqlC
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_marcacoes.cod_perfil <> '0' and sl_marcacoes.cod_ana_c = '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        
        
        sql = sql & SqlC
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_perfis, sl_ana_c, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_marcacoes.cod_perfil <> '0' and sl_marcacoes.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 and sl_ana_c.peso = 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC
 
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro

          
    Else
        '_____________________________________________________________________________
        'insere na tabela tempor�ria a uni�o de perfis com resultados com perfis sem resultados q perte�am ao grupo e a requisi��o em quest�o
        sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ, DESCR_SGR_ANA, COD_RUBRICA) " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Perfis', sl_perfis.descr_perfis, sl_perfis.cod_perfis, sl_perfis.peso, sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_perfis.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_perfis.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_perfis.peso <> 0 " & _
        " AND sl_perfis.cod_perfis = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC & SqlR
        
        sql = sql & " UNION ALL SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Perfis', sl_perfis.descr_perfis, sl_perfis.cod_perfis, sl_perfis.peso,sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_perfis, sl_proven, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_perfis.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_perfis.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_requis.cod_proven = sl_proven.cod_proven " & _
        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_perfis.peso <> 0 " & _
        " AND sl_perfis.cod_perfis = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC & SqlM

        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
        
        '______________________________________________________________________________
        'insere na tabela tempor�ria a uni�o de anas. complexas com resultados com anas. complexas sem resultados q perte�am ao grupo e a requisi��o em quest�o
                
        sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO, N_REQ,DESCR_SGR_ANA, COD_RUBRICA) " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso,sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_c, sl_proven, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_c.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_requis.cod_proven = sl_proven.cod_proven(+) " & _
        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_realiza.cod_perfil = '0' and sl_ana_c.peso <> 0 " & _
        " AND sl_ana_c.cod_ana_c = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC & SqlR
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso, sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_c, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_c.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_realiza.cod_perfil <> '0' and sl_ana_c.peso <> 0 and sl_perfis.peso = 0 " & _
        " AND sl_ana_c.cod_ana_c = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC & SqlR
        
        sql = sql & " UNION ALL SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso,sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_c, sl_proven, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_c.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_requis.cod_proven = sl_proven.cod_proven (+)" & _
        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_marcacoes.cod_perfil = '0' and sl_ana_c.peso <> 0 " & _
        " AND sl_ana_c.cod_ana_c = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC & SqlM
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Complexas', sl_ana_c.descr_ana_c, sl_ana_c.cod_ana_c, sl_ana_c.peso, sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_c, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_c.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_c.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_marcacoes.cod_perfil <> '0' and sl_ana_c.peso <> 0 and sl_perfis.peso = 0 " & _
        " AND sl_ana_c.cod_ana_c = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC & SqlM
    
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro

        '______________________________________________________________________________
        'insere na tabela tempor�ria a uni�o de anas. simples sem resultado com anas. simples com resultados q perten�am ao grupo e a requisi��o em quest�o
        sql = "INSERT INTO SL_CR_REL" & gNumeroSessao & "(DESCR_GRUPO, TIPO_ANA, DESCRICAO, CODIGO, PESO,N_REQ, DESCR_SGR_ANA, COD_RUBRICA) " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_proven, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_requis.cod_proven = sl_proven.cod_proven (+)" & _
        "AND sl_realiza.cod_ana_c = '0' and sl_realiza.cod_perfil = '0' and sl_ana_s.peso <> 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "

        sql = sql & SqlC & SqlR
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_ana_c, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_realiza.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_ana_c.peso = 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC & SqlR
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_realiza.cod_perfil <> '0' and sl_realiza.cod_ana_c = '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC & SqlR
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_realiza.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_realiza, sl_ana_s, sl_perfis, sl_ana_c, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_realiza.cod_perfil <> '0' and sl_realiza.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 and sl_ana_c.peso = 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC & SqlR
        
        sql = sql & " UNION ALL SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_proven, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_requis.cod_proven = sl_proven.cod_proven(+) " & _
        "AND sl_marcacoes.cod_ana_c = '0' and sl_marcacoes.cod_perfil = '0' and sl_ana_s.peso <> 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "

        sql = sql & SqlC & SqlM
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_ana_c, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_marcacoes.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_ana_c.peso = 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        
        
        sql = sql & SqlC & SqlM
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_marcacoes.cod_perfil <> '0' and sl_marcacoes.cod_ana_c = '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        
        
        sql = sql & SqlC & SqlM
        
        sql = sql & " UNION ALL " & _
        "SELECT DISTINCT sl_gr_ana.descr_gr_ana, 'Simples', sl_ana_s.descr_ana_s, sl_ana_s.cod_ana_s, sl_ana_s.peso, sl_marcacoes.n_req, " & IIf(Flg_OrderProduto.Value = 1, "sl_sgr_ana.descr_sgr_ana ", "'' ") & ", sl_ana_facturacao.cod_ana_gh " & _
        "FROM sl_gr_ana, sl_identif, sl_requis, sl_marcacoes, sl_ana_s, sl_perfis, sl_ana_c, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_marcacoes.n_req = sl_requis.n_req " & _
        "AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis " & _
        "AND sl_marcacoes.cod_ana_c = sl_ana_c.cod_ana_c " & _
        "AND sl_marcacoes.cod_perfil <> '0' and sl_marcacoes.cod_ana_c <> '0' and sl_ana_s.peso <> 0 and sl_perfis.peso = 0 and sl_ana_c.peso = 0 " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        
        sql = sql & SqlC & SqlM
    
        BG_ExecutaQuery_ADO sql
        BG_Trata_BDErro
    End If
    
    If CbAnalises.ListIndex = 0 Then
        sql = "SELECT distinct sl_identif.seq_utente FROM sl_identif, sl_requis, sl_realiza,sl_gr_ana, sl_ana_s, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        " WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        " AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        " AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        " AND sl_realiza.n_req = sl_requis.n_req " & _
        " AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
        " AND sl_realiza.cod_perfil = sl_perfis.cod_perfis (+) " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        sql = sql & SqlC
    ElseIf CbAnalises.ListIndex = 1 Then
        sql = "SELECT distinct sl_identif.seq_utente FROM sl_identif, sl_requis, sl_marcacoes, sl_gr_ana, sl_ana_s, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        " WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        " AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        " AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        " AND sl_marcacoes.n_req = sl_requis.n_req " & _
        " AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
        " AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis (+) " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
    Else
        sql = "SELECT distinct sl_identif.seq_utente FROM sl_identif, sl_requis, sl_realiza, sl_gr_ana, sl_ana_s, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis (+) " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        sql = sql & SqlC
        sql = sql & " UNION SELECT distinct sl_identif.seq_utente FROM sl_identif, sl_requis, sl_marcacoes, sl_gr_ana, sl_ana_s, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        " WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        " AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        " AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        " AND sl_marcacoes.n_req = sl_requis.n_req " & _
        " AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
        " AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis (+) " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        sql = sql & SqlC
    End If
    
    
    RsUtente.Source = sql
    RsUtente.CursorLocation = adUseClient
    RsUtente.CursorType = adOpenStatic
    RsUtente.ActiveConnection = gConexao
    RsUtente.Open
    TotalUtentes = RsUtente.RecordCount
    RsUtente.Close
    If CbAnalises.ListIndex = 0 Then
        sql = "SELECT distinct sl_identif.seq_utente FROM sl_identif, sl_requis, sl_realiza,sl_gr_ana, sl_ana_s, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        " WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        " AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        " AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        " AND sl_realiza.n_req = sl_requis.n_req " & _
        " AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
        " AND sl_realiza.cod_perfil = sl_perfis.cod_perfis (+) " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        sql = sql & SqlC
    ElseIf CbAnalises.ListIndex = 1 Then
        sql = "SELECT distinct sl_identif.seq_utente FROM sl_identif, sl_requis, sl_marcacoes, sl_gr_ana, sl_ana_s, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        " WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        " AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        " AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        " AND sl_marcacoes.n_req = sl_requis.n_req " & _
        " AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
        " AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis (+) " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
    Else
        sql = "SELECT distinct sl_identif.seq_utente FROM sl_identif, sl_requis, sl_realiza, sl_gr_ana, sl_ana_s, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        "WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        "AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        "AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        "AND sl_realiza.n_req = sl_requis.n_req " & _
        "AND sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s " & _
        "AND sl_realiza.cod_perfil = sl_perfis.cod_perfis (+) " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
                    
        sql = sql & SqlC
        sql = sql & " UNION SELECT distinct sl_identif.seq_utente FROM sl_identif, sl_requis, sl_marcacoes, sl_gr_ana, sl_ana_s, sl_perfis, sl_sgr_ana, sl_ana_facturacao " & _
        " WHERE sl_requis.seq_utente = sl_identif.seq_utente " & _
        " AND sl_gr_ana.cod_gr_ana = sl_ana_s.gr_ana " & _
        " AND sl_sgr_ana.cod_sgr_ana(+) = sl_ana_s.sgr_ana " & _
        " AND sl_marcacoes.n_req = sl_requis.n_req " & _
        " AND sl_marcacoes.cod_ana_s = sl_ana_s.cod_ana_s " & _
        " AND sl_marcacoes.cod_perfil = sl_perfis.cod_perfis (+) " & _
        " AND sl_ana_s.cod_ana_s = sl_ana_facturacao.cod_ana (+) "
        sql = sql & SqlC
    End If
    
    RsUtente.Source = sql
    RsUtente.CursorLocation = adUseClient
    RsUtente.CursorType = adOpenStatic
    RsUtente.ActiveConnection = gConexao
    RsUtente.Open
    totalRequis = RsUtente.RecordCount
    RsUtente.Close
    Set RsUtente = Nothing
    
End Sub

Sub Cria_TmpRec_Estatistica()

    Dim TmpRec(9) As DefTable
    
    TmpRec(1).NomeCampo = "DESCR_GRUPO"
    TmpRec(1).tipo = "STRING"
    TmpRec(1).tamanho = 40
    
    TmpRec(2).NomeCampo = "TIPO_ANA"
    TmpRec(2).tipo = "STRING"
    TmpRec(2).tamanho = 10
    
    TmpRec(3).NomeCampo = "DESCRICAO"
    TmpRec(3).tipo = "STRING"
    TmpRec(3).tamanho = 120
    
    TmpRec(4).NomeCampo = "CODIGO"
    TmpRec(4).tipo = "STRING"
    TmpRec(4).tamanho = 10
    
    TmpRec(5).NomeCampo = "PESO"
    TmpRec(5).tipo = "INTEGER"
    TmpRec(5).tamanho = 3
    
    TmpRec(6).NomeCampo = "N_REQ"
    TmpRec(6).tipo = "INTEGER"
    TmpRec(6).tamanho = 9
    
    TmpRec(7).NomeCampo = "DESCR_SGR_ANA"
    TmpRec(7).tipo = "STRING"
    TmpRec(7).tamanho = 40
    
    TmpRec(8).NomeCampo = "COD_RUBRICA"
    TmpRec(8).tipo = "STRING"
    TmpRec(8).tamanho = 10
    
    TmpRec(9).NomeCampo = "UTENTE"
    TmpRec(9).tipo = "STRING"
    TmpRec(9).tamanho = 10
    
    Call BL_CriaTabela("SL_CR_REL" & gNumeroSessao, TmpRec)
    
End Sub

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Private Sub BtPesqRapC_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_c"
    CamposEcran(1) = "cod_ana_c"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_c"
    CamposEcran(2) = "descr_ana_c"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_c"
    CampoPesquisa = "descr_ana_c"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Complexas")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaC.text = Resultados(2)
            EcCodAnaC.text = Resultados(1)
'            EcCodAna.Text = Resultados(1)
'            EcDescrAna.Text = Resultados(2)
            EcCodAnaC.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises complexas", vbExclamation, "ATEN��O"
        EcCodAnaC.SetFocus
    End If

End Sub

Private Sub BtPesqRapP_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_perfis"
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_perfis"
    CampoPesquisa = "descr_perfis"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Perfis")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAnaP.text = Resultados(1)
            EcDescrAnaP.text = Resultados(2)
'            EcCodAna.Text = Resultados(1)
'            EcDescrAna.Text = Resultados(2)
            EcCodAnaP.Enabled = True
            EcCodAnaP.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem perfis", vbExclamation, "ATEN��O"
        EcCodAnaP.SetFocus
    End If

End Sub


Private Sub BtPesqRapS_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_s"
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_s"
    CampoPesquisa = "descr_ana_s"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Simples")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaS.text = Resultados(2)
            EcCodAnaS.text = Resultados(1)
'            EcCodAna.Text = Resultados(1)
'            EcDescrAna.Text = Resultados(2)
            EcCodAnaS.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises simples", vbExclamation, "ATEN��O"
        EcCodAnaS.SetFocus
    End If

End Sub


Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
End Sub

Private Sub BtPesquisaProveniencia_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.text = Resultados(1)
            EcDescrProveniencia.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
        
End Sub
Private Sub BtPesquisaPosto_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_sala"
    CamposEcran(1) = "cod_sala"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_sala"
    CamposEcran(2) = "descr_sala"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_cod_salas"
    CampoPesquisa1 = "descr_sala"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Salas/Postos")
    
    mensagem = "N�o foi encontrada nenhuma Sala/Posto."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodPosto.text = Resultados(1)
            EcDescrPosto.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
        
End Sub

Private Sub CbLocal_Click()

If CbLocal.ListIndex <> -1 Then
    BG_PreencheComboBD_ADO "select seq_gr_ana, descr_gr_ana from sl_gr_ana where cod_local=" & CbLocal.ItemData(CbLocal.ListIndex), "seq_gr_ana", "descr_gr_ana", CbGrupo
Else
    BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", CbGrupo
End If

End Sub

Private Sub CbUrgencia_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1
    
End Sub

Private Sub EcCodAnaC_Validate(Cancel As Boolean)
    If EcCodAnaC.text <> "" Then
        Dim RsDescrAnaC As ADODB.recordset
        Set RsDescrAnaC = New ADODB.recordset
        EcCodAnaC.text = UCase(EcCodAnaC.text)
        With RsDescrAnaC
            .Source = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c= " & UCase(BL_TrataStringParaBD(EcCodAnaC.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaC.RecordCount > 0 Then
            EcDescrAnaC.text = "" & RsDescrAnaC!Descr_Ana_C
            OptComplexas.Value = True
        Else
            Cancel = True
            EcDescrAnaC.text = ""
            BG_Mensagem mediMsgBox, "An�lise complexa inexistente!", vbOKOnly + vbExclamation, App.ProductName
            SendKeys ("{HOME}+{END}")
            EcCodAnaC.text = ""
        End If
        RsDescrAnaC.Close
        Set RsDescrAnaC = Nothing
    Else
        OptComplexas.Value = False
        EcCodAnaC.text = ""
        EcDescrAnaC.text = ""
    End If
End Sub

Private Sub EcCodAnaP_Validate(Cancel As Boolean)
    If EcCodAnaP.text <> "" Then
        Dim RsDescrAnaP As ADODB.recordset
        Set RsDescrAnaP = New ADODB.recordset
        EcCodAnaP.text = UCase(EcCodAnaP.text)
        With RsDescrAnaP
            .Source = "SELECT descr_perfis FROM sl_perfis WHERE cod_perfis= " & UCase(BL_TrataStringParaBD(EcCodAnaP.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaP.RecordCount > 0 Then
            EcDescrAnaP.text = "" & RsDescrAnaP!descr_perfis
            OptPerfis.Value = True
        Else
            Cancel = True
            EcDescrAnaP.text = ""
            BG_Mensagem mediMsgBox, "Perfil inexistente!", vbOKOnly + vbExclamation, App.ProductName
            SendKeys ("{HOME}+{END}")
            EcCodAnaP.text = ""
        End If
        RsDescrAnaP.Close
        Set RsDescrAnaP = Nothing
    Else
        OptPerfis.Value = False
        EcCodAnaP.text = ""
        EcDescrAnaP.text = ""
    End If
End Sub

Private Sub EcCodAnaS_Validate(Cancel As Boolean)
    If EcCodAnaS.text <> "" Then
        
        Dim RsDescrAnaS As ADODB.recordset
        Set RsDescrAnaS = New ADODB.recordset
        
        EcCodAnaS.text = UCase(EcCodAnaS.text)
        With RsDescrAnaS
            .Source = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s= " & UCase(BL_TrataStringParaBD(EcCodAnaS.text))
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaS.RecordCount > 0 Then
            EcDescrAnaS.text = "" & RsDescrAnaS!descr_ana_s
            OptSimples.Value = True
        Else
            Cancel = True
            EcDescrAnaS.text = ""
            BG_Mensagem mediMsgBox, "An�lise simples inexistente!", vbOKOnly + vbExclamation, App.ProductName
            SendKeys ("{HOME}+{END}")
            EcCodAnaS.text = ""
        End If
        RsDescrAnaS.Close
        Set RsDescrAnaS = Nothing
    Else
        OptSimples.Value = False
        EcCodAnaS.text = ""
        EcDescrAnaS.text = ""
    End If
End Sub


Private Sub EcCodProveniencia_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub
Private Sub EcCodPosto_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub
Private Sub EcCodEFR_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, "")
End Sub
      
Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProveniencia.text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_proven FROM sl_proven WHERE cod_proven= " & BL_TrataStringParaBD(EcCodProveniencia.text)
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrProveniencia.text = "" & RsDescrProv!descr_proven
        Else
            Cancel = True
            EcDescrProveniencia.text = ""
            BG_Mensagem mediMsgBox, "A Proveni�ncia indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            SendKeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrProveniencia.text = ""
    End If
    
End Sub
Private Sub EcCodposto_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodPosto.text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_sala FROM sl_cod_salas WHERE cod_sala= " & BL_TrataStringParaBD(EcCodPosto.text)
            .CursorLocation = adUseClient
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrPosto.text = "" & RsDescrProv!descr_sala
        Else
            Cancel = True
            EcDescrPosto.text = ""
            BG_Mensagem mediMsgBox, "A Sala/Posto indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            SendKeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrPosto.text = ""
    End If
    
End Sub

Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.text) = "" Then
        EcDtFim.text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.text) = "" Then
        EcDtIni.text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Estat�stica por Laborat�rio"
    
    Me.Left = 540
    Me.Top = 450
    Me.Width = 7725
    Me.Height = 7290 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcCodProveniencia
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica por Laborat�rio")
    
    Set FormEstatGrupo = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    CbAnalises.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    CbSituacao.ListIndex = mediComboValorNull
    EcDescrSexo.ListIndex = mediComboValorNull
    CbGrupo.ListIndex = mediComboValorNull
    EcCodProveniencia.text = ""
    EcDescrProveniencia.text = ""
    EcDescrPosto.text = ""
    EcCodEFR.text = ""
    EcDescrEFR.text = ""
    EcDtFim.text = ""
    EcDtIni.text = ""
    Flg_DescrAna.Value = 0
    Flg_DetAna.Value = 0
    Flg_PesoEstat.Value = 0
    CbLocal.ListIndex = mediComboValorNull
    EcDescrPosto.text = ""
    EcCodPosto.text = ""
    
End Sub

Sub DefTipoCampos()

    'Tipo VarChar
    EcCodProveniencia.Tag = "200"
    EcCodEFR.Tag = "200"
    EcCodPosto.Tag = "200"

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    EcCodProveniencia.MaxLength = 10
    EcCodEFR.MaxLength = 9
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
'    EcNumFolhaTrab.MaxLength = 10
        
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If Estado = 2 Then
        Estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", CbGrupo
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", CbLocal
    
    'Preenche Combo Urgencia
    CbUrgencia.AddItem "Normal"
    CbUrgencia.AddItem "Urgente"
    
    'Preenche Combo Analises
    CbAnalises.AddItem "Com Resultado"
    CbAnalises.AddItem cSemResultado
    CbAnalises.AddItem "Todas"
    
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_EstatisticaGrupo
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_EstatisticaGrupo

End Sub



Private Sub OptComplexas_Click()
    EcCodAnaS.text = ""
    EcDescrAnaS.text = ""
    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    
    EcCodAnaC.Enabled = True
    BtPesqRapC.Enabled = True
    
    EcCodAnaP.text = ""
    EcDescrAnaP.text = ""
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    
    EcCodAnaC.SetFocus
End Sub

Private Sub OptPerfis_Click()
    EcCodAnaS.text = ""
    EcDescrAnaS.text = ""
    EcCodAnaS.Enabled = False
    BtPesqRapS.Enabled = False
    
    EcCodAnaC.text = ""
    EcDescrAnaC.text = ""
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    
    EcCodAnaP.Enabled = True
    BtPesqRapP.Enabled = True
    
    EcCodAnaP.SetFocus
End Sub

Private Sub OptSimples_Click()
    EcCodAnaS.Enabled = True
    BtPesqRapS.Enabled = True
    
    EcCodAnaC.text = ""
    EcDescrAnaC.text = ""
    EcCodAnaC.Enabled = False
    BtPesqRapC.Enabled = False
    
    EcCodAnaP.text = ""
    EcDescrAnaP.text = ""
    EcCodAnaP.Enabled = False
    BtPesqRapP.Enabled = False
    
    EcCodAnaS.SetFocus
End Sub



VERSION 5.00
Begin VB.Form FormMudarGrAnalises 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Mudar Grupo de An�lises"
   ClientHeight    =   330
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4695
   Icon            =   "FormMudarGrAnalises.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   330
   ScaleWidth      =   4695
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CbPostoColhe 
      Height          =   315
      Left            =   0
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   0
      Width           =   4695
   End
   Begin VB.ComboBox CbComputador 
      Height          =   315
      Left            =   0
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   0
      Width           =   4695
   End
   Begin VB.ComboBox CbGrAnalises 
      Height          =   315
      Left            =   0
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   0
      Width           =   4695
   End
End
Attribute VB_Name = "FormMudarGrAnalises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ExecutaClick As Boolean


Private Sub CbGrAnalises_Click()
    Dim sql As String
    Dim RsGr As ADODB.recordset
    
    If ExecutaClick = True And CbGrAnalises.ListIndex <> mediComboValorNull Then

        gMsgTitulo = "Modificar"
        gMsgMsg = "Tem a certeza que deseja mudar o seu grupo de an�lises para o grupo " & CbGrAnalises.Text & " ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        
        If gMsgResp = vbYes Then
            Set RsGr = New ADODB.recordset
            RsGr.CursorLocation = adUseServer
            RsGr.CursorType = adOpenStatic
            RsGr.Source = "select cod_gr_ana from sl_gr_ana where seq_gr_ana = " & CbGrAnalises.ItemData(CbGrAnalises.ListIndex)
            RsGr.Open , gConexao
            If RsGr.RecordCount > 0 Then
               sql = "update sl_idutilizador set gr_ana = " & BL_TrataStringParaBD(RsGr!cod_gr_ana) & " where cod_utilizador = " & gCodUtilizador
               BG_ExecutaQuery_ADO sql
               BG_Trata_BDErro
               gCodGrAnaUtilizador = RsGr!cod_gr_ana
               MDIFormInicio.StatusBar1.Panels.item("Grupo").Text = BL_PesquisaCodDes("sl_gr_ana", "cod_gr_ana", "descr_gr_ana", gCodGrAnaUtilizador, "DES")
            Else
                BG_Mensagem mediMsgBox, "Grupo de An�lises inexistente!", vbError
            End If
            RsGr.Close
            Set RsGr = Nothing

            FormMudarGrAnalises.Visible = False
        End If
    End If
End Sub

Private Sub CbGrAnalises_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim sql As String

    If KeyCode = 46 And Shift = 0 Then
        gMsgTitulo = "Modificar"
        gMsgMsg = "Tem a certeza que deseja n�o ter grupo de an�lises associado ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        
        If gMsgResp = vbYes Then
            
            sql = "update sl_idutilizador set gr_ana = null where cod_utilizador = " & gCodUtilizador
            BG_ExecutaQuery_ADO sql
            BG_Trata_BDErro
            gCodGrAnaUtilizador = ""
            MDIFormInicio.StatusBar1.Panels.item("Grupo").Text = ""
            CbGrAnalises.ListIndex = mediComboValorNull

            FormMudarGrAnalises.Visible = False
        End If
    End If
End Sub

Private Sub Form_Load()
    Dim RsGr As ADODB.recordset
    Dim i As Integer

    FormMudarGrAnalises.left = 9000
    FormMudarGrAnalises.top = 120
    
    ExecutaClick = False
    
    Set RsGr = New ADODB.recordset
    RsGr.CursorLocation = adUseServer
    RsGr.CursorType = adOpenStatic
    RsGr.Source = "select seq_gr_ana,cod_gr_ana,descr_gr_ana from sl_gr_ana where (cod_local = " & gCodLocal & " OR cod_local is null) order by seq_gr_ana asc "
    RsGr.ActiveConnection = gConexao
    RsGr.Open
    
    CbGrAnalises.Clear

    i = 0
    Do Until RsGr.EOF
       CbGrAnalises.AddItem Trim(RsGr!descr_gr_ana)
        CbGrAnalises.ItemData(i) = Trim(RsGr!seq_gr_ana)
        RsGr.MoveNext
        i = i + 1
    Loop
    'BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", CbGrAnalises
    
    If gCodGrAnaUtilizador <> "" Then
        Set RsGr = New ADODB.recordset
        RsGr.CursorLocation = adUseServer
        RsGr.CursorType = adOpenStatic
        RsGr.Source = "select seq_gr_ana from sl_gr_ana where cod_gr_ana = " & BL_TrataStringParaBD(gCodGrAnaUtilizador)
        RsGr.Open , gConexao
        If RsGr.RecordCount > 0 Then
            For i = 0 To CbGrAnalises.ListCount - 1
                If CbGrAnalises.ItemData(i) = RsGr!seq_gr_ana Then
                    CbGrAnalises.ListIndex = i
                End If
            Next i
        End If
        RsGr.Close
        Set RsGr = Nothing
    Else
        CbGrAnalises.ListIndex = mediComboValorNull
    End If
    
    Set RsGr = New ADODB.recordset
    RsGr.CursorLocation = adUseServer
    RsGr.CursorType = adOpenStatic
    RsGr.Source = "select seq_util_posto,cod_posto,descr_sala from sl_ass_util_posto_colhe, sl_cod_salas where cod_posto = cod_sala and cod_util = " & gCodUtilizador & " order by descr_sala asc "
    RsGr.ActiveConnection = gConexao
    RsGr.Open
    
    CbPostoColhe.Clear

    i = 0
    Do Until RsGr.EOF
        CbPostoColhe.AddItem Trim(RsGr!descr_sala)
        CbPostoColhe.ItemData(i) = Trim(RsGr!seq_util_posto)
        RsGr.MoveNext
        i = i + 1
    Loop

    If gCodSalaAssocUser <> 0 Then
        Set RsGr = New ADODB.recordset
        RsGr.CursorLocation = adUseServer
        RsGr.CursorType = adOpenStatic
        RsGr.Source = "select seq_util_posto from sl_ass_util_posto_colhe where cod_posto = " & gCodSalaAssocUser & " and cod_util = " & gCodUtilizador
        RsGr.Open , gConexao
        If RsGr.RecordCount > 0 Then
            For i = 0 To CbPostoColhe.ListCount - 1
                If CbPostoColhe.ItemData(i) = RsGr!seq_util_posto Then
                    CbPostoColhe.ListIndex = i
                End If
            Next i
        End If
        RsGr.Close
        Set RsGr = Nothing
    Else
        CbPostoColhe.ListIndex = mediComboValorNull
    End If

    BG_PreencheComboBD_ADO "SL_COMPUTADOR", "COD_COMPUTADOR", "DESCR_COMPUTADOR", CbComputador
    For i = 0 To CbComputador.ListCount - 1
        CbComputador.ListIndex = i
        If CbComputador = gComputador Then
            CbComputador.ListIndex = i
            ExecutaClick = True
            Exit Sub
        End If
    Next
    CbComputador.ListIndex = -1
    ExecutaClick = True
End Sub

Private Sub CbComputador_click()
    Dim sSql As String
    
    If ExecutaClick = True And CbComputador.ListIndex <> mediComboValorNull Then

        gMsgTitulo = "Modificar"
        gMsgMsg = "Tem a certeza que deseja mudar o seu computador para  " & CbComputador.Text & " ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        
        If gMsgResp = vbYes Then
            gComputador = CbComputador
            gCodSalaAssocComp = CInt(BL_HandleNull(BL_SelCodigo("SL_COMPUTADOR", "COD_SALA", "DESCR_COMPUTADOR", gComputador), 0))
            sSql = "UPDATE sl_idutilizador SET computador = " & BL_TrataStringParaBD(gComputador) & " WHERE cod_utilizador = " & gCodUtilizador
            BG_ExecutaQuery_ADO sSql
            BG_Trata_BDErro
            FormMudarGrAnalises.Visible = False
        End If
    End If
End Sub

Private Sub CbComputador_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim sSql As String

    If KeyCode = 46 And Shift = 0 Then
        gMsgTitulo = "Modificar"
        gMsgMsg = "Tem a certeza que deseja n�o ter Computador associado ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        
        If gMsgResp = vbYes Then
            gComputador = BG_SYS_GetComputerName
            gCodSalaAssocComp = CInt(BL_HandleNull(BL_SelCodigo("SL_COMPUTADOR", "COD_SALA", "DESCR_COMPUTADOR", gComputador), 0))
            sSql = "UPDATE sl_idutilizador SET computador = NULL WHERE cod_utilizador = " & gCodUtilizador
            BG_ExecutaQuery_ADO sSql
            BG_Trata_BDErro
            FormMudarGrAnalises.Visible = False
            Unload Me
        End If
    End If
End Sub
Private Sub CbPostoColhe_Click()
    Dim sql As String
    Dim RsGr As ADODB.recordset
    
    If ExecutaClick = True And CbPostoColhe.ListIndex <> mediComboValorNull Then

        gMsgTitulo = "Modificar"
        gMsgMsg = "Tem a certeza que deseja mudar o seu posto de colheita para o posto " & CbPostoColhe.Text & " ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        
        If gMsgResp = vbYes Then
            Set RsGr = New ADODB.recordset
            RsGr.CursorLocation = adUseServer
            RsGr.CursorType = adOpenStatic
            RsGr.Source = "select cod_posto from sl_ass_util_posto_colhe where seq_util_posto = " & CbPostoColhe.ItemData(CbPostoColhe.ListIndex)
            RsGr.Open , gConexao
            If RsGr.RecordCount > 0 Then
                If RsGr!cod_posto = "-1" Then
                    sql = "update sl_idutilizador set cod_posto = NULL where cod_utilizador = " & gCodUtilizador
                Else
                    sql = "update sl_idutilizador set cod_posto = " & RsGr!cod_posto & " where cod_utilizador = " & gCodUtilizador
                End If
               BG_ExecutaQuery_ADO sql
               BG_Trata_BDErro
               gCodSalaAssocUser = RsGr!cod_posto
            Else
                BG_Mensagem mediMsgBox, "Posto de Colheita inexistente!", vbError
            End If
            RsGr.Close
            Set RsGr = Nothing

            FormMudarGrAnalises.Visible = False
        End If
    End If
End Sub


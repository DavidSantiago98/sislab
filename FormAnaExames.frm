VERSION 5.00
Begin VB.Form FormAnaExames 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAnaExames"
   ClientHeight    =   6990
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9795
   Icon            =   "FormAnaExames.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6990
   ScaleWidth      =   9795
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   930
      Left            =   120
      TabIndex        =   17
      Top             =   0
      Width           =   8900
      Begin VB.ComboBox EcDescricaoCombo 
         Height          =   315
         Left            =   3720
         Locked          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   400
         Visible         =   0   'False
         Width           =   4875
      End
      Begin VB.TextBox EcCodigo 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1560
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   400
         Width           =   1065
      End
      Begin VB.TextBox EcDescricao 
         Height          =   285
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   400
         Width           =   4935
      End
      Begin VB.Label Label1 
         Caption         =   "Descri��o"
         Height          =   255
         Left            =   2880
         TabIndex        =   22
         Top             =   400
         Width           =   915
      End
      Begin VB.Label Label3 
         Caption         =   "C�digo do Perfil"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   400
         Width           =   1335
      End
   End
   Begin VB.Frame Frame2 
      Height          =   4590
      Left            =   120
      TabIndex        =   1
      Top             =   960
      Width           =   8895
      Begin VB.ListBox EcListaPerfis 
         Height          =   3570
         Left            =   1560
         TabIndex        =   24
         Top             =   840
         Width           =   3105
      End
      Begin VB.CommandButton BtRetira 
         Height          =   495
         Left            =   4780
         Picture         =   "FormAnaExames.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   " Retirar do Perfil "
         Top             =   2220
         Width           =   495
      End
      Begin VB.CommandButton BtBaixo 
         Height          =   495
         Left            =   4780
         Picture         =   "FormAnaExames.frx":0396
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   " Alterar ordem no Perfil "
         Top             =   3105
         Width           =   495
      End
      Begin VB.CommandButton BtCima 
         Height          =   495
         Left            =   4780
         Picture         =   "FormAnaExames.frx":0720
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   " Alterar ordem no Perfil "
         Top             =   630
         Width           =   495
      End
      Begin VB.ListBox EcListaSC 
         Height          =   3960
         Left            =   5400
         TabIndex        =   10
         Top             =   480
         Width           =   3345
      End
      Begin VB.ListBox EcListaComplexa 
         Height          =   3570
         Left            =   1560
         TabIndex        =   9
         Top             =   840
         Width           =   3105
      End
      Begin VB.ListBox EcListaSimples 
         Height          =   3570
         Left            =   1560
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   840
         Width           =   3105
      End
      Begin VB.TextBox EcPesquisa 
         Height          =   285
         Left            =   1560
         TabIndex        =   7
         Top             =   480
         Width           =   3105
      End
      Begin VB.CommandButton BtInsere 
         Height          =   495
         Left            =   4780
         Picture         =   "FormAnaExames.frx":0AAA
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   " Inserir no Perfil "
         Top             =   1665
         Width           =   495
      End
      Begin VB.Frame Tipoanalise 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4275
         Left            =   120
         TabIndex        =   2
         ToolTipText     =   "Tipo de an�lise a pesquisar."
         Top             =   150
         Width           =   1370
         Begin VB.OptionButton OpP 
            Caption         =   "Perfis"
            Height          =   195
            Left            =   150
            TabIndex        =   23
            Top             =   1560
            Width           =   1125
         End
         Begin VB.OptionButton opS 
            Caption         =   "Simples"
            Height          =   195
            Left            =   150
            TabIndex        =   5
            Top             =   840
            Value           =   -1  'True
            Width           =   855
         End
         Begin VB.OptionButton opC 
            Caption         =   "Complexas"
            Height          =   195
            Left            =   150
            TabIndex        =   4
            Top             =   1200
            Width           =   1125
         End
         Begin VB.PictureBox Lupa 
            BorderStyle     =   0  'None
            Height          =   570
            Left            =   440
            Picture         =   "FormAnaExames.frx":0E34
            ScaleHeight     =   570
            ScaleWidth      =   495
            TabIndex        =   3
            TabStop         =   0   'False
            Top             =   1750
            Width           =   495
         End
      End
      Begin VB.ListBox EcListaTipoAnalises 
         Height          =   1815
         Left            =   120
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   2160
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.Label Label2 
         Caption         =   "Pesquisa R�pida"
         Height          =   255
         Left            =   1560
         TabIndex        =   16
         Top             =   240
         Width           =   2955
      End
      Begin VB.Label Label5 
         Caption         =   "An�lises do Perfil"
         Height          =   255
         Left            =   5400
         TabIndex        =   15
         Top             =   240
         Width           =   2955
      End
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   5520
      TabIndex        =   0
      Top             =   6360
      Width           =   1095
   End
End
Attribute VB_Name = "FormAnaExames"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 30/03/2003
' T�cnico : Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String
Dim CriterioTabelaD As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox As Variant
Dim CamposBDparaListBoxS As Variant
Dim CamposBDparaListBoxC As Variant
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim ListaOrigem As Control
Dim ListaDestino As Control
    
Public rs As ADODB.recordset
Public RSListaDestino As ADODB.recordset

Private Sub BtBaixo_Click()

    Dim sdummy As String
    Dim ldummy As Long
    
    If EcListaSC.ListIndex < EcListaSC.ListCount - 1 Then
        sdummy = EcListaSC.List(EcListaSC.ListIndex + 1)
        ldummy = EcListaSC.ItemData(EcListaSC.ListIndex + 1)
        EcListaSC.List(EcListaSC.ListIndex + 1) = EcListaSC.List(EcListaSC.ListIndex)
        EcListaSC.ItemData(EcListaSC.ListIndex + 1) = EcListaSC.ItemData(EcListaSC.ListIndex)
        EcListaSC.List(EcListaSC.ListIndex) = sdummy
        EcListaSC.ItemData(EcListaSC.ListIndex) = ldummy
        
        sdummy = EcListaTipoAnalises.List(EcListaSC.ListIndex + 1)
        ldummy = EcListaTipoAnalises.ItemData(EcListaSC.ListIndex + 1)
        EcListaTipoAnalises.List(EcListaSC.ListIndex + 1) = EcListaTipoAnalises.List(EcListaSC.ListIndex)
        EcListaTipoAnalises.ItemData(EcListaSC.ListIndex + 1) = EcListaTipoAnalises.ItemData(EcListaSC.ListIndex)
        EcListaTipoAnalises.List(EcListaSC.ListIndex) = sdummy
        EcListaTipoAnalises.ItemData(EcListaSC.ListIndex) = ldummy
        EcListaSC.ListIndex = EcListaSC.ListIndex + 1
    End If
    
End Sub

Private Sub BtCima_Click()

    Dim sdummy As String
    Dim ldummy As Long
    
    If EcListaSC.ListIndex > 0 Then
        sdummy = EcListaSC.List(EcListaSC.ListIndex - 1)
        ldummy = EcListaSC.ItemData(EcListaSC.ListIndex - 1)
        EcListaSC.List(EcListaSC.ListIndex - 1) = EcListaSC.List(EcListaSC.ListIndex)
        EcListaSC.ItemData(EcListaSC.ListIndex - 1) = EcListaSC.ItemData(EcListaSC.ListIndex)
        EcListaSC.List(EcListaSC.ListIndex) = sdummy
        EcListaSC.ItemData(EcListaSC.ListIndex) = ldummy
        
        sdummy = EcListaTipoAnalises.List(EcListaSC.ListIndex - 1)
        ldummy = EcListaTipoAnalises.ItemData(EcListaSC.ListIndex - 1)
        EcListaTipoAnalises.List(EcListaSC.ListIndex - 1) = EcListaTipoAnalises.List(EcListaSC.ListIndex)
        EcListaTipoAnalises.ItemData(EcListaSC.ListIndex - 1) = EcListaTipoAnalises.ItemData(EcListaSC.ListIndex)
        EcListaTipoAnalises.List(EcListaSC.ListIndex) = sdummy
        EcListaTipoAnalises.ItemData(EcListaSC.ListIndex) = ldummy
        EcListaSC.ListIndex = EcListaSC.ListIndex - 1
    End If
    
End Sub

Private Sub BtInsere_Click()
    
    Dim i As Integer
    Dim CountAntes As Integer
    Dim CountDepois As Integer
            
    If opS.value = True Then
        Set ListaOrigem = EcListaSimples
    ElseIf opC.value = True Then
        Set ListaOrigem = EcListaComplexa
    ElseIf OpP.value = True Then
        Set ListaOrigem = EcListaPerfis
    End If
    
    If Not ListaOrigem Is Nothing Then
        Set ListaDestino = EcListaSC
           
        For i = 0 To ListaOrigem.ListCount - 1
            
            If ListaOrigem.Selected(i) Then
                CountAntes = EcListaSC.ListCount
                
                Call BG_PassaElementoEntreListas(ListaOrigem, _
                                                 ListaDestino, _
                                                 i, _
                                                 False, _
                                                 False)
                
                CountDepois = EcListaSC.ListCount
                If CountAntes < CountDepois Then
                    If opS.value = True Then
                        EcListaTipoAnalises.AddItem "S", CountDepois - 1
                    ElseIf opC.value = True Then
                        EcListaTipoAnalises.AddItem "C", CountDepois - 1
                    ElseIf OpP.value = True Then
                        EcListaTipoAnalises.AddItem "P", CountDepois - 1
                    End If
                End If
                ListaOrigem.Selected(i) = False
            End If
        Next i
    
    End If
    
End Sub

Private Sub BtRetira_Click()
    
    Set ListaDestino = EcListaSC
    If ListaDestino.ListIndex <> -1 Then
        EcListaTipoAnalises.RemoveItem (ListaDestino.ListIndex)
        ListaDestino.RemoveItem (ListaDestino.ListIndex)
    End If

End Sub

Private Sub cmdOK_Click()
    
    EcCodigo.Text = UCase(Trim(EcCodigo.Text))
    If (Mid(EcCodigo.Text, 1, 1) <> "P") Then
        EcCodigo.Text = "P" & EcCodigo.Text
    End If
    
    Call FuncaoProcurar

End Sub

Private Sub EcCodigo_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    cmdOK.Default = True
    
End Sub

Private Sub EcCodigo_LostFocus()

    cmdOK.Default = False

End Sub

Private Sub EcCodigo_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodigo)
    If Cancel = True Then Exit Sub
    
    EcCodigo.Text = UCase(EcCodigo.Text)
    If Trim(EcCodigo.Text) = "P" Then
        BG_Mensagem mediMsgBox, "O [P] por si s�, n�o pode ser c�digo do perfil, pois � identificativo do mesmo!", vbExclamation, "C�digo do Perfil"
        Cancel = True
        Exit Sub
    End If
    
    If left(Trim(EcCodigo.Text), 1) <> "P" And Trim(EcCodigo.Text) <> "" Then
        EcCodigo.Text = "P" & EcCodigo.Text
    ElseIf Trim(EcCodigo.Text) <> "" Then
        EcCodigo.Text = left(Trim(EcCodigo.Text), 1) & Mid(EcCodigo.Text, 2, Len(EcCodigo.Text) - 1)
    End If
    
    BL_ColocaTextoCombo "sl_perfis", "seq_perfis", "cod_perfis", EcCodigo, EcDescricaoCombo
    
End Sub

Private Sub EcDescricaoCombo_Click()
    
    Dim EcAux As String
    
    EcAux = EcCodigo
    BL_ColocaComboTexto "sl_perfis", "seq_perfis", "cod_perfis", EcCodigo, EcDescricaoCombo
    If EcAux <> EcCodigo And EcAux <> "" Then EcListaSC.Clear
        
End Sub

Private Sub EcDescricaoCombo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then EcDescricaoCombo.ListIndex = -1

End Sub

Private Sub EcListaSC_Click()
    
    EcListaSC.ToolTipText = Trim(EcListaSC.List(EcListaSC.ListIndex))

End Sub

Private Sub EcListaComplexa_Click()
    
    EcListaComplexa.ToolTipText = Trim(EcListaComplexa.List(EcListaComplexa.ListIndex))

End Sub

Private Sub EcListaSC_DblClick()
    Dim pos As Integer
    If Mid(Trim(EcListaSC.List(EcListaSC.ListIndex)), 1, 1) = "C" Then
        FormAnaComplexas.Show
        pos = InStr(1, Trim(EcListaSC.List(EcListaSC.ListIndex)), " ", vbTextCompare)
        FormAnaComplexas.EcCodigo.Text = left(Trim(EcListaSC.List(EcListaSC.ListIndex)), pos - 1)
        FormAnaComplexas.FuncaoProcurar
        Set gFormActivo = FormAnaComplexas
    ElseIf Mid(Trim(EcListaSC.List(EcListaSC.ListIndex)), 1, 1) = "S" Then
        FormCodAna.Show
        pos = InStr(1, Trim(EcListaSC.List(EcListaSC.ListIndex)), " ", vbTextCompare)
        FormCodAna.EcCodigo.Text = left(Trim(EcListaSC.List(EcListaSC.ListIndex)), pos - 1)
        FormCodAna.FuncaoProcurar
        Set gFormActivo = FormCodAna
    End If
End Sub

Private Sub EcListaSC_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim sdummy As String
    Dim ldummy As Long
    
    If KeyCode = 38 And Shift = 2 Then 'Ctrl + Up: mover para cima
        If EcListaSC.ListIndex > 0 Then
            sdummy = EcListaSC.List(EcListaSC.ListIndex - 1)
            ldummy = EcListaSC.ItemData(EcListaSC.ListIndex - 1)
            EcListaSC.List(EcListaSC.ListIndex - 1) = EcListaSC.List(EcListaSC.ListIndex)
            EcListaSC.ItemData(EcListaSC.ListIndex - 1) = EcListaSC.ItemData(EcListaSC.ListIndex)
            EcListaSC.List(EcListaSC.ListIndex) = sdummy
            EcListaSC.ItemData(EcListaSC.ListIndex) = ldummy
            
            sdummy = EcListaTipoAnalises.List(EcListaSC.ListIndex - 1)
            ldummy = EcListaTipoAnalises.ItemData(EcListaSC.ListIndex - 1)
            EcListaTipoAnalises.List(EcListaSC.ListIndex - 1) = EcListaTipoAnalises.List(EcListaSC.ListIndex)
            EcListaTipoAnalises.ItemData(EcListaSC.ListIndex - 1) = EcListaTipoAnalises.ItemData(EcListaSC.ListIndex)
            EcListaTipoAnalises.List(EcListaSC.ListIndex) = sdummy
            EcListaTipoAnalises.ItemData(EcListaSC.ListIndex) = ldummy
        End If
    ElseIf KeyCode = 40 And Shift = 2 Then 'Ctrl + Down: mover para baixo
        If EcListaSC.ListIndex < EcListaSC.ListCount - 1 Then
            sdummy = EcListaSC.List(EcListaSC.ListIndex + 1)
            ldummy = EcListaSC.ItemData(EcListaSC.ListIndex + 1)
            EcListaSC.List(EcListaSC.ListIndex + 1) = EcListaSC.List(EcListaSC.ListIndex)
            EcListaSC.ItemData(EcListaSC.ListIndex + 1) = EcListaSC.ItemData(EcListaSC.ListIndex)
            EcListaSC.List(EcListaSC.ListIndex) = sdummy
            EcListaSC.ItemData(EcListaSC.ListIndex) = ldummy
            
            sdummy = EcListaTipoAnalises.List(EcListaSC.ListIndex + 1)
            ldummy = EcListaTipoAnalises.ItemData(EcListaSC.ListIndex + 1)
            EcListaTipoAnalises.List(EcListaSC.ListIndex + 1) = EcListaTipoAnalises.List(EcListaSC.ListIndex)
            EcListaTipoAnalises.ItemData(EcListaSC.ListIndex + 1) = EcListaTipoAnalises.ItemData(EcListaSC.ListIndex)
            EcListaTipoAnalises.List(EcListaSC.ListIndex) = sdummy
            EcListaTipoAnalises.ItemData(EcListaSC.ListIndex) = ldummy
        End If
    End If

End Sub

Private Sub EcListaSimples_Click()
    
    EcListaSimples.ToolTipText = Trim(EcListaSimples.List(EcListaSimples.ListIndex))

End Sub

Private Sub EcPesquisa_Change()

    Call RefinaPesquisa
    
End Sub

Sub RefinaPesquisa()
    
    Dim i As Integer
    Dim k As Integer
    Dim TSQLQueryAux As String
    Dim TCampoChave As String
    Dim TCampoPesquisa As String
    Dim TCamposSel As String
    Dim TNomeTabela As String
    Dim TClausulaWhereAuxiliar As String
    Dim TTabelaAux As ADODB.recordset
    Dim NomeControl As Control
    
    On Error GoTo Trata_Erro
    Set TTabelaAux = New ADODB.recordset
    
    If opS.value = True Then
        Set NomeControl = EcListaSimples
        TCampoChave = "seq_ana_s"
        TCamposSel = "cod_ana_s, descr_ana_s"
        TCampoPesquisa = "descr_ana_s"
        TNomeTabela = "sl_ana_s"
    ElseIf opC.value = True Then
        Set NomeControl = EcListaComplexa
        TCampoChave = "seq_ana_c"
        TCamposSel = "cod_ana_c, descr_ana_c"
        TCampoPesquisa = "descr_ana_c"
        TNomeTabela = "sl_ana_c"
    ElseIf OpP.value = True Then
        Set NomeControl = EcListaPerfis
        TCampoChave = "seq_perfis"
        TCamposSel = "cod_perfis, descr_perfis"
        TCampoPesquisa = "descr_perfis"
        TNomeTabela = "sl_perfis"
    End If
    
    TSQLQueryAux = "SELECT " & TCampoChave & " ," & (TCamposSel) & " FROM " & TNomeTabela
    
    If (gPesquisaDentroCampo) Then
        TSQLQueryAux = TSQLQueryAux & " WHERE UPPER(" & TCampoPesquisa & ") LIKE '%" & Trim(UCase(BG_CvWilcard(EcPesquisa))) & "%'"
    Else
        TSQLQueryAux = TSQLQueryAux & " WHERE UPPER(" & TCampoPesquisa & ") LIKE '" & Trim(UCase(BG_CvWilcard(EcPesquisa))) & "%'"
    End If
    
    If TClausulaWhereAuxiliar <> "" Then
        TSQLQueryAux = TSQLQueryAux & " AND " & TClausulaWhereAuxiliar
    End If
    TSQLQueryAux = TSQLQueryAux & " ORDER BY " & TCampoPesquisa
    
    TTabelaAux.CursorLocation = adUseServer
    TTabelaAux.CursorType = adOpenStatic
    TTabelaAux.Open TSQLQueryAux, gConexao
    NomeControl.ListIndex = mediComboValorNull
    NomeControl.Clear
    
    i = 0
    Do Until TTabelaAux.EOF
        k = InStr(1, TCamposSel, ",")
        NomeControl.AddItem Trim(TTabelaAux(Trim(Mid(TCamposSel, 1, k - 1)))) & _
                                  Space(8 - Len(Trim(TTabelaAux(Trim(Mid(TCamposSel, 1, k - 1))))) + 1) & Trim(TTabelaAux(Trim(Mid(TCamposSel, k + 1)))) & Space(80 - Len(Trim(TTabelaAux(Trim(Mid(TCamposSel, k + 1))))) + 1)
        NomeControl.ItemData(i) = CInt(TTabelaAux(TCampoChave))
        TTabelaAux.MoveNext
        i = i + 1
    Loop
    TTabelaAux.Close
    Set TTabelaAux = Nothing
    
Trata_Erro:
End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()

    Me.caption = " Codifica��o de An�lises dos Exames."
    Me.left = 540
    Me.top = 450
    Me.Width = 9210
    Me.Height = 5985 ' Normal
    'Me.Height = 8825 ' Campos Extras
    
    NomeTabela = "sl_ana_perfis"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 1
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "cod_perfis"
        
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "C�digo do Perfil"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_perfis"
    Set ChaveEc = EcCodigo
    
    CamposBDparaListBoxS = Array("descr_ana_s")
    CamposBDparaListBoxC = Array("descr_ana_c")
    CamposBDparaListBox = Array("cod_analise", "descricao")
    opC.value = False
    opS.value = False
    NumEspacos = Array(8, 40)
    
    EcPesquisa.ToolTipText = cMsgWilcards
    Call BL_FormataCodigo(EcCodigo)

End Sub

Sub EventoLoad()
    DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    opC.value = False
    opS.value = False
    OpP.value = False
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

   
    Call FuncaoProcurar
    
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "Activo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"

'    Me.opC.Value = True
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormAnaExames = Nothing

End Sub

Sub LimpaCampos()
    
    ' Me.SetFocus
        
    BG_LimpaCampo_Todos CamposEc
    EcListaSC.Clear
    EcListaTipoAnalises.Clear
    EcDescricaoCombo.ListIndex = mediComboValorNull

End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheValoresDefeito()
    
    ' BG_PreencheComboBD_ADO "sl_perfis", "seq_perfis", "descr_perfis", EcDescricaoCombo
    
    If gF_EXAMES = 1 Then
        ' Trazer codigo do form de perfis
        ' Caso a chamada a este form tenha sido efectuada de l�.
        EcCodigo = FormExames.EcCodigo
        ' BL_ColocaTextoCombo "sl_perfis", "seq_perfis", "cod_perfis", EcCodigo, EcDescricaoCombo
        Me.EcDescricao.Text = FormExames.EcDescricao
        
'        Call Me.FuncaoProcurar
    ElseIf gF_PERFISANA = 1 Then
        ' Trazer codigo do form de perfis
        ' Caso a chamada a este form tenha sido efectuada de l�.
        EcCodigo = FormPerfisAna.EcCodigo
        Me.EcDescricao.Text = FormPerfisAna.EcDescricao
        
'        Call Me.FuncaoProcurar
    End If

End Sub

Sub PreencheCampos()
    
    Dim ii As Integer
    
    ' Me.SetFocus
    
    BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
    
    If Trim(EcCodigo) <> "" Then
        BL_ColocaTextoCombo "sl_perfis", "seq_perfis", "cod_perfis", EcCodigo, EcDescricaoCombo
    End If

    Set RSListaDestino = New ADODB.recordset
    RSListaDestino.CursorType = adOpenStatic
    RSListaDestino.CursorLocation = adUseServer

    CriterioTabelaD = "( SELECT sl_ana_perfis.ordem, cod_analise, seq_ana_c as sequencia, descr_ana_c as descricao " & _
                      "FROM sl_ana_perfis, sl_ana_c " & _
                      "WHERE cod_ana_c = cod_analise AND cod_analise LIKE 'C%' AND cod_perfis = '" & Trim(EcCodigo) & "') " & _
                      " UNION " & _
                      "( SELECT sl_ana_perfis.ordem, cod_analise, seq_ana_s as sequencia, descr_ana_s as descricao " & _
                      " FROM sl_ana_perfis, sl_ana_s " & _
                      " WHERE cod_ana_s = cod_analise and cod_analise LIKE 'S%' AND cod_perfis = '" & Trim(EcCodigo) & "')" & _
                      " UNION " & _
                      "( SELECT sl_ana_perfis.ordem, cod_analise, seq_perfis as sequencia, descr_perfis as descricao " & _
                      " FROM sl_ana_perfis, sl_perfis " & _
                      " WHERE sl_perfis.cod_perfis = cod_analise and cod_analise LIKE 'P%' AND sl_ana_perfis.cod_perfis = '" & Trim(EcCodigo) & "')" & _
                      " ORDER BY 1"
    
    RSListaDestino.Open CriterioTabelaD, gConexao

    If RSListaDestino.RecordCount > 0 Then
        estado = 2
        ' Inicio do preenchimento de 'EcLista'

        Call BL_PreencheListBoxMultipla_ADO(EcListaSC, _
                                            RSListaDestino, _
                                            CamposBDparaListBox, _
                                            NumEspacos, _
                                            CamposBD, _
                                            CamposEc, _
                                            "SELECT", , _
                                            "sequencia")
        
        EcListaTipoAnalises.Clear
        RSListaDestino.MoveFirst
        For ii = 0 To RSListaDestino.RecordCount - 1
            EcListaTipoAnalises.AddItem Trim(left(RSListaDestino!cod_analise, 1)), ii
            RSListaDestino.MoveNext
        Next ii
        
    End If
    
    RSListaDestino.Close
    Set RSListaDestino = Nothing
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

    Me.EcCodigo.Locked = False

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcListaSC.Clear
        
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
        
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    If Trim(EcCodigo) = "" Then
        CriterioTabela = "SELECT cod_perfis FROM sl_ana_perfis GROUP BY cod_perfis ORDER BY cod_perfis ASC"
    Else
        CriterioTabela = "SELECT cod_perfis FROM sl_ana_perfis WHERE cod_perfis ='" & Trim(EcCodigo) & "' GROUP BY cod_perfis"
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        'nao lan�a mensagem quando entra pelo form de perfis
        If gF_EXAMES = 0 And gF_PERFISANA = 0 Then
            BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
            FuncaoLimpar
        End If
    Else
        estado = 2
        LimpaCampos

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        Me.EcCodigo.Locked = True
        
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = " Modificar"
    gMsgMsg = "Tem a certeza que deseja validar as altera��es efectuadas ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    On Error GoTo ErrorHandler
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim SQLAux As String
    Dim SQLAux2 As String
    Dim MarcaLocal As Variant
    Dim i As Integer
    Dim rsAux As ADODB.recordset
                
    SQLAux2 = "DELETE FROM " & NomeTabela & " WHERE cod_perfis='" & EcCodigo & "'"
    BG_ExecutaQuery_ADO SQLAux2
    
    Set rsAux = New ADODB.recordset
    rsAux.CursorLocation = adUseServer
    
    For i = 0 To EcListaSC.ListCount - 1
        EcListaTipoAnalises.ListIndex = i
        If EcListaTipoAnalises.Text = "C" Then
            SQLAux = "SELECT cod_ana_c FROM sl_ana_c WHERE seq_ana_c=" & Trim(EcListaSC.ItemData(i))
            rsAux.Open SQLAux, gConexao, adOpenStatic, adLockOptimistic
            
            SQLQuery = "INSERT INTO " & NomeTabela & " (Cod_perfis,Cod_analise, ordem) " & _
                " VALUES ('" & Trim(EcCodigo) & "','" & Trim(rsAux!cod_ana_c) & "'," & i & ")"
            BG_ExecutaQuery_ADO SQLQuery
            rsAux.Close
        ElseIf EcListaTipoAnalises.Text = "S" Then
            SQLAux = "SELECT cod_ana_s FROM sl_ana_s WHERE seq_ana_s=" & Trim(EcListaSC.ItemData(i))
            rsAux.Open SQLAux, gConexao, adOpenStatic, adLockOptimistic
            
            SQLQuery = "INSERT INTO " & NomeTabela & " (Cod_perfis,Cod_analise,ordem) " & _
                " VALUES ('" & Trim(EcCodigo) & "','" & Trim(rsAux!cod_ana_s) & "'," & i & ")"
            BG_ExecutaQuery_ADO SQLQuery
            rsAux.Close
        ElseIf EcListaTipoAnalises.Text = "P" Then
            SQLAux = "SELECT cod_perfis FROM sl_perfis WHERE seq_perfis=" & Trim(EcListaSC.ItemData(i))
            rsAux.Open SQLAux, gConexao, adOpenStatic, adLockOptimistic
            
            SQLQuery = "INSERT INTO " & NomeTabela & " (Cod_perfis,Cod_analise,ordem) " & _
                " VALUES ('" & Trim(EcCodigo) & "','" & Trim(rsAux!cod_perfis) & "'," & i & ")"
            BG_ExecutaQuery_ADO SQLQuery
            rsAux.Close
        End If
    Next i
    
    Set rsAux = Nothing
    
    'Permite que a FuncaoInserir utilize esta funcao
    If rs Is Nothing Then Exit Sub Else: If rs.RecordCount <= 0 Then Exit Sub
    
    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If MarcaLocal <= rs.RecordCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : BD_Update (FormAnaExames) -> " & Err.Description)
            LimpaCampos
            PreencheCampos
    End Select
End Sub

Sub FuncaoInserir()

    Dim iRes As Integer
    Dim SQLQuery As String
    Dim rsInsAux As ADODB.recordset

    gMsgTitulo = " Inserir"
    gMsgMsg = "Tem a certeza que deseja Inserir estes dados ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        Set rsInsAux = New ADODB.recordset
    
        SQLQuery = "SELECT " & ChaveBD & " FROM " & NomeTabela & " WHERE " & ChaveBD & "='" & EcCodigo & "'"
    
        rsInsAux.CursorLocation = adUseServer
        rsInsAux.CursorType = adOpenStatic
        rsInsAux.Open SQLQuery, gConexao
    
        If rsInsAux.RecordCount = (-1) Then
            BG_Mensagem mediMsgBox, "Erro a verificar c�digo!", vbExclamation, "Inserir"
        ElseIf rsInsAux.RecordCount > 0 Then
            BG_Mensagem mediMsgBox, "J� existe registo com esse c�digo !", vbExclamation, "Inserir"
        Else
            BL_InicioProcessamento Me, "A inserir registo."
            iRes = ValidaCamposEc
        
            If iRes = True Then
                BD_Update
            End If
            BL_FimProcessamento Me
        End If
        
        rsInsAux.Close
        Set rsInsAux = Nothing
    End If
    
End Sub


Private Sub opS_Click()
    
    EcListaSimples.Visible = True
    EcListaComplexa.Visible = False
    EcListaPerfis.Visible = False
    RefinaPesquisa
    
End Sub

Private Sub opC_Click()

    EcListaSimples.Visible = False
    EcListaComplexa.Visible = True
    EcListaPerfis.Visible = False
    RefinaPesquisa
    
End Sub
Private Sub opP_Click()

    EcListaSimples.Visible = False
    EcListaComplexa.Visible = False
    EcListaPerfis.Visible = True
    RefinaPesquisa
    
End Sub

Sub FuncaoRemover()

    Dim sql As String

    gMsgTitulo = " Remover"
    gMsgMsg = "Tem a certeza que deseja apagar o registo ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        sql = "DELETE FROM sl_ana_perfis WHERE cod_perfis=" & BL_TrataStringParaBD(EcCodigo.Text)
        BG_ExecutaQuery_ADO sql
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
   
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If MarcaLocal <= rs.RecordCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
        
    LimpaCampos
    PreencheCampos

End Sub




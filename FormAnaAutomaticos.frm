VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormAnaAutomaticos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAnaAutomaticos"
   ClientHeight    =   8175
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   8250
   Icon            =   "FormAnaAutomaticos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8175
   ScaleWidth      =   8250
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrameRes 
      Caption         =   "Resultado"
      Height          =   1335
      Left            =   1800
      TabIndex        =   20
      Top             =   3120
      Width           =   3495
      Begin VB.ComboBox CbPMax 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   360
         Width           =   615
      End
      Begin VB.CommandButton BtResCanc 
         Height          =   375
         Left            =   1800
         Picture         =   "FormAnaAutomaticos.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   23
         ToolTipText     =   "Cancelar"
         Top             =   840
         Width           =   495
      End
      Begin VB.CommandButton BtResOK 
         Height          =   375
         Left            =   1200
         Picture         =   "FormAnaAutomaticos.frx":0776
         Style           =   1  'Graphical
         TabIndex        =   22
         ToolTipText     =   "OK"
         Top             =   840
         Width           =   495
      End
      Begin VB.TextBox EcResultadoMax 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1680
         TabIndex        =   21
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame FrameGeral 
      BorderStyle     =   0  'None
      Height          =   7215
      Left            =   0
      TabIndex        =   8
      Top             =   480
      Width           =   7815
      Begin VB.CommandButton BtConclusoes 
         Height          =   495
         Left            =   2400
         Picture         =   "FormAnaAutomaticos.frx":0B00
         Style           =   1  'Graphical
         TabIndex        =   26
         ToolTipText     =   "Resultados se f�rmula se verificar"
         Top             =   1560
         Width           =   2535
      End
      Begin VB.CommandButton BtAnalises2 
         Height          =   495
         Left            =   1440
         Picture         =   "FormAnaAutomaticos.frx":126A
         Style           =   1  'Graphical
         TabIndex        =   19
         ToolTipText     =   "Segundo Resultado"
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox EcFormula 
         Appearance      =   0  'Flat
         Height          =   495
         Left            =   840
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   17
         Top             =   960
         Width           =   6360
      End
      Begin VB.Frame Frame8 
         Caption         =   "Operadores"
         Height          =   855
         Left            =   3720
         TabIndex        =   11
         Top             =   0
         Width           =   3495
         Begin VB.CommandButton Bt_e 
            Caption         =   "&&"
            Height          =   375
            Left            =   120
            TabIndex        =   16
            TabStop         =   0   'False
            ToolTipText     =   "e"
            Top             =   360
            Width           =   495
         End
         Begin VB.CommandButton bt_ou 
            Caption         =   "|"
            Height          =   375
            Left            =   840
            TabIndex        =   15
            TabStop         =   0   'False
            ToolTipText     =   "ou"
            Top             =   360
            Width           =   495
         End
         Begin VB.CommandButton bt_nao 
            Caption         =   "!"
            Height          =   375
            Left            =   1560
            TabIndex        =   14
            TabStop         =   0   'False
            ToolTipText     =   "Nega��o"
            Top             =   360
            Width           =   495
         End
         Begin VB.CommandButton bt_abreParenteses 
            Caption         =   "("
            Height          =   375
            Left            =   2160
            TabIndex        =   13
            TabStop         =   0   'False
            ToolTipText     =   "Parentesis Esquerdo"
            Top             =   360
            Width           =   495
         End
         Begin VB.CommandButton bt_FechaParenteses 
            Caption         =   ")"
            Height          =   375
            Left            =   2760
            TabIndex        =   12
            TabStop         =   0   'False
            ToolTipText     =   "Parentesis Direito"
            Top             =   360
            Width           =   495
         End
      End
      Begin VB.CommandButton BtAnalises 
         Height          =   495
         Left            =   720
         Picture         =   "FormAnaAutomaticos.frx":19D4
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Primeiro Resultado"
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton BtLimpar 
         Height          =   495
         Left            =   2160
         Picture         =   "FormAnaAutomaticos.frx":213E
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Limpar F�rmula"
         Top             =   240
         Width           =   615
      End
      Begin MSFlexGridLib.MSFlexGrid FgConc 
         Height          =   2655
         Left            =   120
         TabIndex        =   18
         Top             =   2280
         Width           =   7335
         _ExtentX        =   12938
         _ExtentY        =   4683
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
      End
      Begin VB.Label Label1 
         Caption         =   "F�rmula"
         Height          =   255
         Index           =   1
         Left            =   0
         TabIndex        =   25
         Top             =   960
         Width           =   735
      End
   End
   Begin VB.TextBox EcPesqRapidaAna2 
      Height          =   285
      Left            =   1680
      TabIndex        =   6
      Top             =   6720
      Width           =   615
   End
   Begin VB.TextBox EcPesqRapidaAna 
      Height          =   285
      Left            =   1680
      TabIndex        =   4
      Top             =   6360
      Width           =   615
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      Height          =   285
      Left            =   720
      TabIndex        =   2
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox EcDesignacao 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      Height          =   285
      Left            =   1920
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   120
      Width           =   5295
   End
   Begin VB.CommandButton BtPesqRapAna 
      Height          =   340
      Left            =   7200
      Picture         =   "FormAnaAutomaticos.frx":28A8
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "  Pesquisa R�pida de  An�lises Simples "
      Top             =   120
      Width           =   375
   End
   Begin VB.Label Label21 
      Caption         =   "EcPesqRapidaAna2"
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   7
      Top             =   6720
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label21 
      Caption         =   "EcPesqRapidaAna"
      Height          =   255
      Index           =   1
      Left            =   0
      TabIndex        =   5
      Top             =   6360
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "An�lise"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "FormAnaAutomaticos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type frases
    cod_frase As String
    descr_frase As String
End Type

Private Type micro
    cod_micro As String
    descr_micro As String
End Type

Private Type tipo_result
    frase() As frases
    totalFrases As Integer
    microrg() As micro
    totalMicro As Integer

    cod_sinal As Integer
    descr_sinal As Integer
    resultado As String
End Type

Private Type resultado
    conclusao As tipo_result
    formula As String
End Type
Dim resultados() As resultado
Dim totalRes As Integer

Dim ana_res As String

Dim estado As Integer
Dim flg_conclusao As Boolean

Dim rsProc As New ADODB.recordset

Private Sub BtAnalises_Click()
    If EcCodigo.Text = "" Then
        BG_Mensagem mediMsgBox, "Deve escolher uma an�lise.", vbExclamation, "ATEN��O"
        Exit Sub
    End If
    FormPesquisaRapida.InitPesquisaRapida "sl_ana_s", _
                        "descr_ana_s", "seq_ana_s", _
                       EcPesqRapidaAna

End Sub

Private Sub BtAnalises2_Click()
    If EcCodigo.Text = "" Then
        BG_Mensagem mediMsgBox, "Deve escolher uma an�lise.", vbExclamation, "ATEN��O"
        Exit Sub
    End If
    FormPesquisaRapida.InitPesquisaRapida "sl_ana_s", _
                        "descr_ana_s", "seq_ana_s", _
                       EcPesqRapidaAna2
End Sub

Private Sub BtConclusoes_Click()
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim frase() As frases
    Dim totalFrases As Integer
    
    If EcFormula.Text = "" Then
        BG_Mensagem mediMsgBox, "Deve primeiro seleccionar a f�rmula", vbExclamation, "Aten��o"
        Exit Sub
    End If
    
    If EcCodigo.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_ana_s, t_result FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(EcCodigo.Text), gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar dados da An�lise!", vbExclamation, "Pesquisa r�pida"
        Else
            If BL_HandleNull(rsCodigo!t_result, "") = gT_Frase Then
                totalFrases = 0
                ReDim frase(totalFrases)
                If PesquisaFrasesNovoReg(EcPesqRapidaAna.Text, frase, totalFrases) = True Then
                    adicionaResultadoConclusao "", frase, totalFrases
                End If
            
            ElseIf BL_HandleNull(rsCodigo!t_result, "") = gT_Alfanumerico Or BL_HandleNull(rsCodigo!t_result, "") = gT_Numerico Or BL_HandleNull(rsCodigo!t_result, "") = gT_Auxiliar Then
                flg_conclusao = True
                CbPMax.Visible = False
                FrameGeral.Enabled = False
                FrameRes.Enabled = True
                FrameRes.top = 2280
                FrameRes.left = 1440
                FrameRes.Visible = True
            End If
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
        
        EcPesqRapidaAna.Text = ""
        Sendkeys ("{END}")
    End If
End Sub

Private Sub BtLimpar_Click()
    EcFormula.Text = ""
End Sub

Private Sub BtResCanc_Click()
    FrameRes.Enabled = False
    FrameRes.Visible = False
    FrameGeral.Enabled = True
    CbPMax.ListIndex = mediComboValorNull
    EcResultadoMax.Text = ""
End Sub

Private Sub BtResOK_Click()
    Dim caracter As String
    Dim frase() As frases
    Dim totalFrases As Integer
    If (CbPMax.ListIndex = mediComboValorNull And flg_conclusao = False) Or EcResultadoMax.Text = "" Then
        BG_Mensagem mediMsgBox, "Deve seleccionar um resultado e um sinal!", vbExclamation, "Aten��o"
        Exit Sub
    End If
    If flg_conclusao = False Then
        AdicionaFormula ana_res & " " & CbPMax.Text & " " & EcResultadoMax.Text
    Else
        adicionaResultadoConclusao EcResultadoMax.Text, frase, totalFrases
    End If
    FrameRes.Enabled = False
    FrameRes.Visible = False
    FrameGeral.Enabled = True
    CbPMax.ListIndex = mediComboValorNull
    EcResultadoMax.Text = ""
        
End Sub

Private Sub CbPMax_Change()
    If EcResultadoMax.Text <> "" And flg_conclusao = False Then
        If CbPMax.ListIndex <> mediComboValorNull Then
            If IsNumeric(EcResultadoMax.Text) = False Then
                If CbPMax.ItemData(CbPMax.ListIndex) <> 5 Then
                    BG_Mensagem mediMsgBox, "Apenas o sinal de = pode ser seleccionado para valores alfanum�ricos", vbExclamation, "Aten��o"
                    CbPMax.ListIndex = mediComboValorNull
                End If
            End If
        End If
    End If
End Sub

Private Sub EcPesqRapidaAna_Change()
    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim frase() As frases
    Dim totalFrases As Integer
    
    If EcPesqRapidaAna.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_ana_s, t_result FROM sl_ana_s WHERE seq_ana_s = " & EcPesqRapidaAna, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo da An�lise!", vbExclamation, "Pesquisa r�pida"
        Else
            ana_res = "$" & BL_HandleNull(rsCodigo!cod_ana_s, "")
            If BL_HandleNull(rsCodigo!t_result, "") = gT_Frase Then
                totalFrases = 0
                ReDim frase(totalFrases)
                If PesquisaFrasesNovoReg(EcPesqRapidaAna.Text, frase, totalFrases) = True Then
                    If totalFrases > 1 Then
                        AdicionaFormula "("
                    End If
                    For i = 1 To totalFrases
                        AdicionaFormula "$" & Trim(rsCodigo!cod_ana_s) & " = �" & frase(i).cod_frase & "�"
                        If i < totalFrases Then
                            AdicionaFormula "|"
                        End If
                    Next i
                    If totalFrases > 1 Then
                        AdicionaFormula ")"
                    End If
                End If
            ElseIf BL_HandleNull(rsCodigo!t_result, "") = gT_Alfanumerico Or BL_HandleNull(rsCodigo!t_result, "") = gT_Numerico Or BL_HandleNull(rsCodigo!t_result, "") = gT_Auxiliar Then
                flg_conclusao = False
                CbPMax.Visible = True
                FrameGeral.Enabled = False
                FrameRes.Enabled = True
                FrameRes.top = 2280
                FrameRes.left = 1440
                FrameRes.Visible = True
            End If
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
        
        EcPesqRapidaAna.Text = ""
        Sendkeys ("{END}")
    End If
End Sub
Private Sub EcPesqRapidaAna2_Change()

    Dim rsCodigo As ADODB.recordset
    Dim i As Integer
    Dim frase() As frases
    Dim totalFrases As Integer
    
    If EcPesqRapidaAna2.Text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        rsCodigo.Open "SELECT cod_ana_s, t_result FROM sl_segundo_res WHERE cod_ana_s IN (select cod_ana_s FROM sl_ana_s WHERE seq_ana_s = " & EcPesqRapidaAna2.Text & ")", gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "An�lise seleccionada n�o tem segundo Resultado parametrizado!", vbExclamation, "Pesquisa r�pida"
        Else
            ana_res = "@" & BL_HandleNull(rsCodigo!cod_ana_s, "")
            If BL_HandleNull(rsCodigo!t_result, "") = gT_Frase Then
                totalFrases = 0
                ReDim frase(totalFrases)
                If PesquisaFrasesNovoReg(EcPesqRapidaAna2.Text, frase, totalFrases) = True Then
                    If totalFrases > 1 Then
                        AdicionaFormula "("
                    End If
                    For i = 1 To totalFrases
                        AdicionaFormula "@" & Trim(rsCodigo!cod_ana_s) & " = �" & frase(i).cod_frase & "�"
                        If i < totalFrases Then
                            AdicionaFormula "|"
                        End If
                    Next i
                    If totalFrases > 1 Then
                        AdicionaFormula ")"
                    End If
                End If
            ElseIf BL_HandleNull(rsCodigo!t_result, "") = gT_Alfanumerico Or BL_HandleNull(rsCodigo!t_result, "") = gT_Numerico Or BL_HandleNull(rsCodigo!t_result, "") = gT_Auxiliar Then
                flg_conclusao = False
                CbPMax.Visible = True
                FrameGeral.Enabled = False
                FrameRes.Enabled = True
                FrameRes.top = 2280
                FrameRes.left = 1440
                FrameRes.Visible = True
            End If
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
        
        EcPesqRapidaAna2.Text = ""

    End If


End Sub


Private Sub EcResultadoMax_Validate(Cancel As Boolean)
    If EcResultadoMax.Text <> "" Then
        If CbPMax.ListIndex <> mediComboValorNull Then
            If IsNumeric(EcResultadoMax.Text) = False Then
                If CbPMax.ItemData(CbPMax.ListIndex) <> 5 Then
                    BG_Mensagem mediMsgBox, "Apenas valores num�ricos s�o permitidos para o sinal " & CbPMax.Text, vbExclamation, "Aten��o"
                    EcResultadoMax.Text = ""
                End If
            End If
        End If
    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Atribui��o Autom�tica de Resultados"
    Me.left = 50
    Me.top = 50
    Me.Width = 8100
    Me.Height = 6090 ' Normal
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormScriptFactus = Nothing
End Sub



Sub DefTipoCampos()
    FrameRes.Visible = False
    BG_PreencheComboBD_ADO "sl_tbf_SINAIS", "COD_SINAL", "DESCR_SINAL", CbPMax, mediAscComboCodigo
    With FgConc
        .WordWrap = True
       .rows = 2
       .Cols = 3
       .FixedRows = 1
       .FixedCols = 0
       .AllowBigSelection = False
       .HighLight = flexHighlightAlways
       .FocusRect = flexFocusHeavy
       .MousePointer = flexDefault
       .FillStyle = flexFillSingle
       .SelectionMode = flexSelectionFree
       .AllowUserResizing = flexResizeColumns
       .GridLinesFixed = flexGridInset
       .GridColorFixed = flexTextInsetLight
       .MergeCells = flexMergeFree
       .PictureType = flexPictureColor
       .RowHeightMin = 280
       .row = 0
       
       .ColWidth(0) = 0
       .Col = 0
       .Text = "Seq"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(1) = 4000
       .Col = 1
       .Text = "F�rmula"
       .CellAlignment = flexAlignLeftCenter
       
       .ColWidth(2) = 3000
       .Col = 2
       .Text = "Resultado"
       .CellAlignment = flexAlignLeftCenter
       
       .Col = 0
    End With
    

End Sub

Sub PreencheCampos()
    Dim aux As String
    Dim frases() As String
    Dim micro() As String
    Dim iFrase As Integer
    Dim iMicro As Integer
    
    EcCodigo.Text = BL_HandleNull(rsProc!cod_ana_s, "")
    EcCodigo_Validate False
    While Not rsProc.EOF
        ReDim frases(0)
        ReDim micro(0)
        
        resultados(totalRes).formula = BL_HandleNull(rsProc!formula, "")
        resultados(totalRes).conclusao.resultado = BL_HandleNull(rsProc!resultado, "")
        
        BL_Tokenize BL_HandleNull(rsProc!frases, ""), "#", frases
        resultados(totalRes).conclusao.totalFrases = UBound(frases) + 1
        ReDim resultados(totalRes).conclusao.frase(resultados(totalRes).conclusao.totalFrases)
        For iFrase = 0 To UBound(frases)
            resultados(totalRes).conclusao.frase(iFrase + 1).cod_frase = frases(iFrase)
            resultados(totalRes).conclusao.frase(iFrase + 1).descr_frase = BL_SelCodigo("SL_DICIONARIO", "DESCR_FRASE", "COD_FRASE", resultados(totalRes).conclusao.frase(iFrase + 1).cod_frase, "V")
        Next iFrase
        
        BL_Tokenize BL_HandleNull(rsProc!microrg, ""), "#", micro
        resultados(totalRes).conclusao.totalMicro = UBound(micro) + 1
        ReDim resultados(totalRes).conclusao.microrg(resultados(totalRes).conclusao.totalMicro)
        For iMicro = 0 To UBound(micro)
            resultados(totalRes).conclusao.microrg(iMicro + 1).cod_micro = micro(iMicro)
            resultados(totalRes).conclusao.microrg(iMicro + 1).descr_micro = BL_SelCodigo("SL_MICRORG", "DESCR_MICRORG", "COD_MICRORG", resultados(totalRes).conclusao.microrg(iMicro + 1).cod_micro, "V")
        Next iMicro
        AdicionaFgCon totalRes
        totalRes = totalRes + 1
        ReDim Preserve resultados(totalRes)
        rsProc.MoveNext
    Wend
End Sub


Sub FuncaoLimpar()
    EcFormula.Text = ""
    totalRes = 1
    ReDim resultados(totalRes)
    resultados(totalRes).formula = ""
    resultados(totalRes).conclusao.resultado = ""
    ReDim resultados(totalRes).conclusao.frase(0)
    ReDim resultados(totalRes).conclusao.microrg(0)
    resultados(totalRes).conclusao.totalFrases = 0
    resultados(totalRes).conclusao.totalMicro = 0
    LimpaFgConc
End Sub

Sub FuncaoEstadoAnterior()

End Sub

Sub FuncaoProcurar()
    Dim sSql As String
    
    If EcCodigo.Text <> "" Then
        Set rsProc = New ADODB.recordset
        sSql = "SELECT * FROM sl_ana_resultados_automaticos WHERE cod_ana_S = " & BL_TrataStringParaBD(EcCodigo.Text)
        rsProc.CursorType = adOpenStatic
        rsProc.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsProc.Open sSql, gConexao
        
        If rsProc.RecordCount <= 0 Then
            BL_FimProcessamento Me
            BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
            FuncaoLimpar
        Else
            estado = 2
            FuncaoLimpar
            PreencheCampos
        End If
    Else
        BG_Mensagem mediMsgBox, "Tem que seleccionar uma an�lise!", vbExclamation, "Procurar"
    End If
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()
    gMsgTitulo = " Inserir"
    gMsgMsg = "Tem a certeza que deseja Inserir estes dados ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        GravaResultadosAutomaticos
    End If
    BL_FimProcessamento Me
    FuncaoProcurar
End Sub


Sub FuncaoModificar()
    gMsgTitulo = " Modificar"
    gMsgMsg = "Tem a certeza que deseja Modificar estes dados ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        GravaResultadosAutomaticos
    End If
End Sub

Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub

Private Sub bt_e_Click()
    AdicionaFormula "&"

End Sub

Private Sub bt_ou_Click()
    AdicionaFormula "|"
End Sub


Private Sub bt_nao_Click()
    AdicionaFormula "!"

End Sub


Private Sub bt_abreParenteses_Click()
    AdicionaFormula "("

End Sub


Private Sub bt_fechaParenteses_Click()
    AdicionaFormula ")"

End Sub

Private Sub EcCodigo_Validate(Cancel As Boolean)
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    If EcCodigo.Text = "" Then Exit Sub
    EcCodigo.Text = UCase(EcCodigo.Text)
    If Trim(EcCodigo.Text) = "S" Then
        BG_Mensagem mediMsgBox, "O [S] por si s�, n�o pode ser c�digo da an�lise, pois � identificativo do tipo de an�lise!", vbExclamation, "C�digo da Simples"
        Cancel = True
    ElseIf left(Trim(EcCodigo.Text), 1) <> "S" And Trim(EcCodigo.Text) <> "" Then
        EcCodigo.Text = "S" & EcCodigo.Text
    ElseIf Trim(EcCodigo.Text) <> "" Then
        EcCodigo.Text = left(Trim(EcCodigo.Text), 1) & Mid(EcCodigo.Text, 2, Len(EcCodigo.Text) - 1)
    End If
    sSql = "SELECT * FROM SL_ANA_S WHERE COD_ANA_S = " & BL_TrataStringParaBD(EcCodigo.Text)
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If rsAna.RecordCount = 1 Then
        EcDesignacao.Text = BL_HandleNull(rsAna!descr_ana_s, "")
        totalRes = 1
        
        ReDim Preserve resultados(totalRes)
    Else
        totalRes = 0
        ReDim resultados(totalRes)
        BG_Mensagem mediMsgBox, "O c�digo n�o existe", vbExclamation, "ATEN��O"
        EcCodigo.Text = ""
        EcDesignacao.Text = ""
    End If
    rsAna.Close
    Set rsAna = Nothing
End Sub

Private Sub BtPesqRapAna_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1) As Variant
    Dim PesqRapida As Boolean
    Dim campoPesquisaCod As String
    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3900
    Headers(2) = "Descri��o"
    
    CamposEcran(3) = "descR_produto"
    Tamanhos(3) = 2000
    Headers(3) = "Produto"
    
    CamposRetorno.InicializaResultados 1

        
    CFrom = "sl_ana_s, sl_produto"
    CWhere = "sl_ana_s.cod_produto = sl_produto.cod_produto(+) and (flg_invisivel is null or flg_invisivel <> 1)"
    CampoPesquisa = "descr_ana_s"
    campoPesquisaCod = "cod_ana_s"
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, _
                                                                   ChavesPesq, _
                                                                   CamposEcran, _
                                                                   CamposRetorno, _
                                                                   Tamanhos, _
                                                                   Headers, _
                                                                   CWhere, _
                                                                   CFrom, _
                                                                   "", _
                                                                   CampoPesquisa, _
                                                                   " ORDER BY descr_ana_s ", _
                                                                   " Pesquisa R�pida de An�lises Simples", campoPesquisaCod)
                                                                   

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodigo.Text = resultados(1)
            EcCodigo_Validate False

        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises Simples", vbExclamation, "ATEN��O"
        EcCodigo.SetFocus
    End If

End Sub



' --------------------------------------------------------------------------------------------------------------------

' PESQUISA FRASES ASSOCIADAS A AN�LISE

' --------------------------------------------------------------------------------------------------------------------
Private Function PesquisaFrasesNovoReg(cod_ana As String, frase() As frases, totalFrases As Integer) As Boolean
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    'Dim Res(1 To 2)  As Variant
    Dim res(100) As Variant
    Dim PesqRapida As Boolean
    Dim FormWidth As Long
    Dim LwWidth As Long
    Dim Repetida As Boolean
    Dim RsRelacao As ADODB.recordset
    Dim ordem As Long
    Dim TotalElementosSel As Integer
    Dim i As Integer
    Dim j As Integer
    Dim flg_existe As Boolean
    ' Verificar se existem frases pertencentes � an�lise
    Set RsRelacao = New ADODB.recordset
    With RsRelacao
        .Source = "SELECT COUNT(*) as Contador FROM sl_ana_frase WHERE cod_ana_s =" & BL_TrataStringParaBD(cod_ana)
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open , gConexao
        If BL_HandleNull(RsRelacao!contador, 0) <= 0 Then

            .Close
            .Source = "SELECT COUNT(*) as Contador FROM sl_ana_frase WHERE cod_ana_s =" & BL_TrataStringParaBD(cod_ana)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open , gConexao
        End If
    End With
    
    CWhere = ""
    If Not RsRelacao.EOF Then
        If BL_HandleNull(RsRelacao!contador, 0) <> 0 Then
            CWhere = " cod_frase IN ( SELECT cod_frase FROM sl_ana_frase WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana)
            CWhere = CWhere & ")"
            CWhere = CWhere & " AND (flg_invisivel is null or flg_invisivel  = 0 ) "
        Else
            CWhere = CWhere & " (flg_invisivel is null or flg_invisivel  = 0 ) "
        End If
    End If
    RsRelacao.Close
    Set RsRelacao = Nothing
        
    FormWidth = FormPesqRapidaAvancada.Width
    FormPesqRapidaAvancada.Width = 10000
    LwWidth = FormPesqRapidaAvancada.LwPesquisa.Width
    FormPesqRapidaAvancada.LwPesquisa.Width = 9500
    PesqRapida = False
    
    ChavesPesq(1) = "descr_frase"
    CamposEcran(1) = "descr_frase"
    Tamanhos(1) = 8000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "cod_frase"
    CamposEcran(2) = "cod_frase"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_dicionario"
    CampoPesquisa = "descr_frase"
    
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_frase ", " Dicion�rio de Frases ")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancadaMultiSel.Show vbModal
    
        CamposRetorno.RetornaResultados res, CancelouPesquisa, TotalElementosSel
        PesquisaFrasesNovoReg = False
        If Not CancelouPesquisa Then
        
            For i = 1 To TotalElementosSel
                If Trim(res(i)) <> "" Then
                    flg_existe = False
                    For j = 1 To totalFrases
                        If frase(j).cod_frase = res(i) Then
                            flg_existe = True
                            Exit For
                        End If
                    Next j
                    If flg_existe = False Then
                        totalFrases = totalFrases + 1
                        ReDim Preserve frase(totalFrases)
                        frase(totalFrases).cod_frase = res(i)
                        frase(totalFrases).descr_frase = BL_SelCodigo("SL_DICIONARIO", "DESCR_FRASE", "COD_FRASE", res(i), "V")
                    End If
                End If
                PesquisaFrasesNovoReg = True
            Next i
        Else
            PesquisaFrasesNovoReg = False
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem frases no dicion�rio!", vbExclamation, "ATEN��O"
    End If
    
End Function


Private Sub AdicionaFormula(texto As String)
    EcFormula.Text = EcFormula.Text & " " & texto
    resultados(totalRes).formula = EcFormula.Text
    
End Sub

Private Sub adicionaResultadoConclusao(res As String, frase() As frases, totalFrases As Integer)
    resultados(totalRes).conclusao.resultado = res
    resultados(totalRes).conclusao.frase = frase
    resultados(totalRes).conclusao.totalFrases = totalFrases
    AdicionaFgCon totalRes
    totalRes = totalRes + 1
    ReDim Preserve resultados(totalRes)
    EcFormula.Text = ""
End Sub

Private Sub AdicionaFgCon(linha As Integer)
    Dim conclusao As String
    Dim i As Integer
    If resultados(linha).conclusao.resultado <> "" Then
        conclusao = resultados(linha).conclusao.resultado & vbCrLf
    End If
    For i = 1 To resultados(linha).conclusao.totalFrases
        conclusao = conclusao & resultados(linha).conclusao.frase(i).descr_frase & vbCrLf
    Next i
    FgConc.TextMatrix(linha, 1) = resultados(linha).formula
    FgConc.TextMatrix(linha, 2) = conclusao
    If resultados(linha).conclusao.totalFrases > 1 Then
        FgConc.RowHeight(linha) = resultados(linha).conclusao.totalFrases * 285
    End If
    FgConc.AddItem ""

End Sub
Private Sub LimpaFgConc()
    Dim i As Integer
    FgConc.rows = 2
    FgConc.RowHeight(1) = 285
    For i = 0 To FgConc.Cols - 1
        FgConc.TextMatrix(1, i) = ""
    Next i
End Sub

Private Sub GravaResultadosAutomaticos()
    Dim sSql As String
    Dim iRes As Integer
    Dim iFrase As Integer
    Dim iMicro As Integer
    Dim frases As String
    Dim micro As String
    
    sSql = "DELETE FROM sl_ana_resultados_automaticos WHERE cod_ana_s = " & BL_TrataStringParaBD(EcCodigo.Text)
    BG_ExecutaQuery_ADO sSql
    
    For iRes = 1 To totalRes
        If resultados(iRes).formula <> "" And (resultados(iRes).conclusao.resultado <> "" Or resultados(iRes).conclusao.totalFrases > 0 Or _
                resultados(iRes).conclusao.totalMicro > 0) Then
            frases = ""
            micro = ""
            'concatena frases
            For iFrase = 1 To resultados(iRes).conclusao.totalFrases
                frases = frases & resultados(iRes).conclusao.frase(iFrase).cod_frase & "#"
            Next iFrase
            
            'concatena microrg
            For iMicro = 1 To resultados(iRes).conclusao.totalMicro
                micro = micro & resultados(iRes).conclusao.microrg(iMicro).cod_micro & "#"
            Next iMicro
            
            sSql = "INSERT INTO sl_ana_resultados_automaticos( cod_ana_s, formula, resultado,frases, microrg) VALUES("
            sSql = sSql & BL_TrataStringParaBD(EcCodigo.Text) & "," & BL_TrataStringParaBD(resultados(iRes).formula) & ","
            sSql = sSql & BL_TrataStringParaBD(resultados(iRes).conclusao.resultado) & "," & BL_TrataStringParaBD(frases) & ","
            sSql = sSql & BL_TrataStringParaBD(micro) & ")"
            BG_ExecutaQuery_ADO sSql
        End If
    Next iRes
End Sub


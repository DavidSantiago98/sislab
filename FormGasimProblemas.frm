VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormGasimProblemas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormGasimProblemas"
   ClientHeight    =   2610
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   6450
   Icon            =   "FormGasimProblemas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2610
   ScaleWidth      =   6450
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtPesquisaAparelho 
      Height          =   315
      Left            =   5780
      Picture         =   "FormGasimProblemas.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   1560
      Width           =   375
   End
   Begin VB.ListBox EcListaAparelho 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   840
      TabIndex        =   5
      Top             =   1560
      Width           =   4935
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6135
      Begin MSComCtl2.DTPicker EcDtInicio 
         Height          =   255
         Left            =   1440
         TabIndex        =   1
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   145489921
         CurrentDate     =   39588
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   3720
         TabIndex        =   2
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   145489921
         CurrentDate     =   39588
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   720
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   3210
         TabIndex        =   3
         Top             =   360
         Width           =   255
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Aparelho"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   7
      Top             =   1560
      Width           =   1095
   End
End
Attribute VB_Name = "FormGasimProblemas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Private Sub BtPesquisaAparelho_click()
    PA_PesquisaAparelhoMultiSel EcListaAparelho
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = "Gasimetrias N�o Integradas"
    
    Me.Left = 5
    Me.Top = 5
    Me.Width = 6540
    Me.Height = 3045 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtInicio
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Gasimetrias N�o Integradas")
    
    Set FormGasimProblemas = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    EcDtFim.Value = Bg_DaData_ADO
    EcDtInicio.Value = Bg_DaData_ADO
End Sub

Sub DefTipoCampos()
        
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If Estado = 2 Then
        Estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    EcDtFim.Value = Bg_DaData_ADO
    EcDtInicio.Value = Bg_DaData_ADO

End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub

Private Sub EcListaAparelho_keydown(KeyCode As Integer, Shift As Integer)
    PA_ApagaItemListaMultiSel EcListaAparelho, KeyCode, Shift
End Sub

Sub Preenche_Estatistica()
    Dim sSql As String
    Dim sql As String
    Dim continua As Boolean
    Dim StrTemp  As String
    Dim i As Integer
    Dim nomeReport As String
    
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estat�stica por Laborat�rio") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtInicio.Value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtInicio.SetFocus
        Exit Sub
    End If
    If EcDtFim.Value = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    nomeReport = "ListagemGasimetriasProblemas"
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport(nomeReport, "Gasimetrias N�o Integradas", crptToPrinter)
    Else
        continua = BL_IniciaReport(nomeReport, "Gasimetrias N�o Integradas", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    
    
    
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT x1.seq_gasim_problema, x1.id_utilizador, x1.nome_utilizador, x1.nsc, x1.data, x1.seq_apar, x2.descr_apar "
    Report.SQLQuery = Report.SQLQuery & " FROM sl_gasim_problemas x1, gc_apar x2 WHERE x1.seq_apar = x2.seq_apar"
    Report.SQLQuery = Report.SQLQuery & " AND trunc(data) BETWEEN " & BL_TrataDataParaBD(EcDtInicio.Value) & " AND " & BL_TrataDataParaBD(EcDtFim.Value)
    If EcListaAparelho.ListCount > 0 Then
        Report.SQLQuery = Report.SQLQuery & " AND x1.seq_apar IN ("
        For i = 0 To EcListaAparelho.ListCount - 1
            Report.SQLQuery = Report.SQLQuery & EcListaAparelho.ItemData(i) & ", "
        Next i
        Report.SQLQuery = Mid(Report.SQLQuery, 1, Len(Report.SQLQuery) - 2) & ") "
    End If
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtInicio.Value)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Value)
    For i = 0 To EcListaAparelho.ListCount - 1
        StrTemp = StrTemp & EcListaAparelho.List(i) & "; "
    Next i
    If StrTemp <> "" Then
        StrTemp = Mid(StrTemp, 1, Len(StrTemp) - 2)
        If Len(StrTemp) > 250 Then StrTemp = Mid(StrTemp, 1, 245) & " ..."
        Report.formulas(2) = "Aparelhos=" & BL_TrataStringParaBD("" & StrTemp)
    Else
        Report.formulas(2) = "Aparelhos=" & BL_TrataStringParaBD("Todos")
    End If
    
    
    
    'Report.SubreportToChange = ""
    Call BL_ExecutaReport
    
    
End Sub



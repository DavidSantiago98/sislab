VERSION 5.00
Begin VB.Form FormAjudaFormulaAna 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAjudaFormulaAna"
   ClientHeight    =   5925
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7995
   Icon            =   "FormAjudaFormulaAna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5925
   ScaleWidth      =   7995
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command1 
      Caption         =   "OK"
      Height          =   495
      Left            =   6720
      TabIndex        =   15
      Top             =   120
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   " Ajuda "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5055
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   7695
      Begin VB.Frame Frame2 
         Height          =   2775
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   3615
         Begin VB.Label Label19 
            Caption         =   "I - Idade"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   25
            Top             =   2280
            Width           =   3255
         End
         Begin VB.Label Label18 
            Caption         =   "A -Altura"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   24
            Top             =   2040
            Width           =   3255
         End
         Begin VB.Label Label17 
            Caption         =   "P - Peso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   23
            Top             =   1800
            Width           =   3255
         End
         Begin VB.Label Label16 
            Caption         =   "V - Volume"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   22
            Top             =   1560
            Width           =   3255
         End
         Begin VB.Label Label4 
            Caption         =   "@[c�digo da an�lise simples: segundo resultado]"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   14
            Top             =   960
            Width           =   3255
         End
         Begin VB.Label Label3 
            Caption         =   "$[c�digo da an�lise simples: primeiro resultado]"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   13
            Top             =   480
            Width           =   3255
         End
         Begin VB.Label Label5 
            Caption         =   "Vari�veis :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   12
            Top             =   240
            Width           =   1095
         End
      End
      Begin VB.Frame Frame4 
         Height          =   2775
         Left            =   3840
         TabIndex        =   5
         Top             =   240
         Width           =   3735
         Begin VB.Label Label15 
            Caption         =   "! � nega��o da condi��o seguinte"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   21
            Top             =   2400
            Width           =   3255
         End
         Begin VB.Label Label13 
            Caption         =   "| � o operador l�gico [ou]"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   20
            Top             =   2040
            Width           =   3255
         End
         Begin VB.Label Label9 
            Caption         =   "&& � o operador l�gico [e]"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   19
            Top             =   1680
            Width           =   3015
         End
         Begin VB.Label Label11 
            Caption         =   "^n  pot�ncia de n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   18
            Top             =   960
            Width           =   3015
         End
         Begin VB.Label Label6 
            Caption         =   "/ divis�o"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   2160
            TabIndex        =   17
            Top             =   720
            Width           =   1095
         End
         Begin VB.Label Label14 
            Caption         =   "( )   delimitador de prioridades"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   10
            Top             =   1320
            Width           =   3255
         End
         Begin VB.Label Label12 
            Caption         =   "*     multiplica��o"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   9
            Top             =   720
            Width           =   3255
         End
         Begin VB.Label Label10 
            Caption         =   "- subtra��o"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   2160
            TabIndex        =   8
            Top             =   480
            Width           =   1215
         End
         Begin VB.Label Label8 
            Caption         =   "+    adi��o"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   7
            Top             =   480
            Width           =   1455
         End
         Begin VB.Label Label2 
            Caption         =   "Operadores :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   6
            Top             =   240
            Width           =   1215
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   " Exemplo "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1815
         Left            =   120
         TabIndex        =   2
         Top             =   3120
         Width           =   7455
         Begin VB.TextBox EcExemplo 
            Enabled         =   0   'False
            Height          =   405
            Left            =   240
            TabIndex        =   3
            Text            =   "( $S325 * $S402 ) + (@S20^(1/2))  - $S5^2"
            Top             =   360
            Width           =   6975
         End
         Begin VB.Label Label7 
            Caption         =   $"FormAjudaFormulaAna.frx":000C
            Height          =   735
            Left            =   240
            TabIndex        =   4
            Top             =   960
            Width           =   6975
         End
      End
   End
   Begin VB.PictureBox PcFormula 
      Height          =   530
      Left            =   5640
      Picture         =   "FormAjudaFormulaAna.frx":00F8
      ScaleHeight     =   465
      ScaleWidth      =   480
      TabIndex        =   0
      Top             =   120
      Width           =   545
   End
   Begin VB.Label Label1 
      Caption         =   "F�rmula para Resultados Calculados"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   16
      Top             =   150
      Width           =   5295
   End
End
Attribute VB_Name = "FormAjudaFormulaAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

Dim Estado As Integer

Sub FuncaoInserir()
End Sub

Sub FuncaoLimpar()
End Sub


Sub FuncaoModificar()
End Sub
Sub FuncaoProcurar()
End Sub

Sub FuncaoRemover()
End Sub

Private Sub Command1_Click()
    Unload Me
End Sub


Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub


Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    BG_ParametrizaPermissoes_ADO Me.Name
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    Set gFormActivo = Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub Inicializacoes()

    Me.caption = " Ajuda"
    Me.Left = 540
    Me.Top = 450
    Me.Width = 8070
    Me.Height = 6300 ' Normal
    
End Sub

Sub EventoUnload()
    Set gFormActivo = MDIFormInicio
    Set FormAjudaFormulaAna = Nothing
End Sub


Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub


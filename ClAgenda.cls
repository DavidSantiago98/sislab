VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClAgenda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Type CodAgenda
    cod_t_agenda As Integer
    descr_t_agenda As String
    limite As Integer
    hr_inicio As String
    hr_fim As String
    duracao_colheita As Integer
    intervalo As Integer
    num_marcacoes_slot As Integer
End Type

Private Type DadosReq
    n_req As Long
    tipo_req As String
    nome_ute As String
    Utente As String
    t_utente As String
    cod_proven As String
    descr_proven As String
End Type
Private requisicao As DadosReq
Private CalAgenda As CalendarControl
Private codificacao As CodAgenda
Private eventos() As CalendarEvent


Public Function Ag_InicializaAgendaByCod(cod_t_agenda As Integer) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsA As New ADODB.recordset
    
    sSql = "SELECT * FROM sl_t_agenda where cod_t_agenda = " & cod_t_agenda
    'BRUNODSANTOS ULSNE-1902 06.08.2018
    'rsA.CursorLocation = adUseServer
    rsA.CursorLocation = adUseClient
    rsA.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsA.Open sSql, gConexao
    If rsA.RecordCount = 1 Then
        codificacao.cod_t_agenda = BL_HandleNull(rsA!cod_t_agenda, mediComboValorNull)
        codificacao.descr_t_agenda = BL_HandleNull(rsA!descr_t_agenda, "")
        codificacao.hr_inicio = BL_HandleNull(rsA!hr_inicio, "00:00")
        codificacao.hr_fim = BL_HandleNull(rsA!hr_fim, "23:00")
        codificacao.duracao_colheita = BL_HandleNull(rsA!duracao_colheita, 5)
        codificacao.intervalo = BL_HandleNull(rsA!intervalo, 15)
        codificacao.limite = BL_HandleNull(rsA!limite, mediComboValorNull)
        codificacao.num_marcacoes_slot = BL_HandleNull(rsA!num_marcacoes_slot, 1)
    End If
    rsA.Close
    Set rsA = Nothing
Exit Function
TrataErro:
    Ag_InicializaAgendaByCod = False
    BG_LogFile_Erros "Erro Ag_InicializaAgendaByCod: -> " & Err.Description & " ", "ClAgenda", "Ag_InicializaAgendaByCod", True
    Exit Function
    Resume Next
End Function

Public Function Ag_DevolveIntervalo() As Integer
    Ag_DevolveIntervalo = codificacao.intervalo
End Function
Public Function Ag_DevolveHoraInicio() As String
    Ag_DevolveHoraInicio = codificacao.hr_inicio
End Function
Public Function Ag_DevolveHoraFim() As String
    Ag_DevolveHoraFim = codificacao.hr_fim
End Function
Public Function Ag_devolveDuracao() As Integer
    Ag_devolveDuracao = codificacao.intervalo
End Function
Public Function cenas()
    Dim m_pEditingEvent As CalendarEvent

    Dim StartTime As Date, EndTime As Date
    StartTime = DateFromString(Bg_DaData_ADO, Bg_DaHora_ADO)
    EndTime = DateFromString(Bg_DaData_ADO, "23:00")
    
    
    m_pEditingEvent.StartTime = StartTime
    m_pEditingEvent.EndTime = EndTime
    
    m_pEditingEvent.Subject = "REQ:123123"
    m_pEditingEvent.Location = "Localizacao"
    m_pEditingEvent.Body = "NADA"

End Function

Public Function Ag_inicializaControlo(ca As CalendarControl, wpck As DatePicker)
'    Set CalAgenda = New CalendarControl
    Dim eViewType As Long
    Set CalAgenda = ca
    eViewType = CalAgenda.ViewType
    wpck.AttachToCalendar CalAgenda
    
    'CalAgenda.Options.WorkWeekMask = 0


    
    Ag_PreenchePrioridades
    CalAgenda.DayView.TimeScale = Ag_DevolveIntervalo
    CalAgenda.Options.WeekViewShowTimeAsClocks = False
    CalAgenda.Options.WeekViewShowEndDate = False
    
    CalAgenda.Options.MonthViewShowTimeAsClocks = True
    CalAgenda.Options.MonthViewShowEndDate = True
    CalAgenda.Options.MonthViewCompressWeekendDays = True
    
    CalAgenda.MonthView.WeeksCount = 1
    
    CalAgenda.Options.WorkDayStartTime = TimeValue(Ag_DevolveHoraInicio)
    CalAgenda.Options.WorkDayEndTime = TimeValue(Ag_DevolveHoraFim)
    
    CalAgenda.DayView.ScrollToWorkDayBegin
    CalAgenda.RedrawControl
    
End Function

Function DateFromString(DatePart As String, TimePart As String) As Date
    Dim dtDatePart As Date, dtTimePart As Date
    dtDatePart = DatePart
    dtTimePart = TimePart
    DateFromString = dtDatePart + dtTimePart
End Function

Public Function Ag_InicializaReq(n_req As Long, tipo As String) As Boolean
    Dim sSql As String
    Dim rsA As New ADODB.recordset
    On Error GoTo TrataErro
    
    requisicao.n_req = n_req
    requisicao.tipo_req = tipo
   
    
    sSql = "SELECT x1.t_utente, x1.utente, x1.nome_ute, x1.dt_nasc_ute, x2.cod_proven, x3.descr_proven "
    sSql = sSql & " FROM sl_identif x1 "
    If requisicao.tipo_req = "C" Then
        sSql = sSql & " JOIN sl_requis_consultas x2 ON x1.seq_utente = x2.seq_utente"
    Else
        sSql = sSql & " JOIN sl_requis x2 ON x1.seq_utente = x2.seq_utente"
    End If
    sSql = sSql & " LEFT OUTER JOIN sl_proven x3 on x2.cod_proven = x3.cod_proven "
    sSql = sSql & " WHERE x2.n_req = " & requisicao.n_req
    'BRUNODSANTOS ULSNE-1902 06.08.2018
    'rsA.CursorLocation = adUseServer
    rsA.CursorLocation = adUseClient
    rsA.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsA.Open sSql, gConexao
    If rsA.RecordCount = 1 Then
        requisicao.t_utente = BL_HandleNull(rsA!t_utente, "")
        requisicao.Utente = BL_HandleNull(rsA!Utente, "")
        requisicao.nome_ute = BL_HandleNull(rsA!nome_ute, "")
        requisicao.descr_proven = BL_HandleNull(rsA!descr_proven, "")
        requisicao.cod_proven = BL_HandleNull(rsA!cod_proven, "")
    End If
    rsA.Close
    Set rsA = Nothing
    
    Ag_InicializaReq = True
Exit Function
TrataErro:
    Ag_InicializaReq = False
    BG_LogFile_Erros "Erro Ag_InicializaReq: -> " & Err.Description & " " & sSql, "ClAgenda", "Ag_InicializaReq", True
    Exit Function
    Resume Next
End Function
Public Function Ag_DevolveReq() As Long
    Ag_DevolveReq = requisicao.n_req
End Function

Public Function Ag_novoEvento() As CalendarEvent
    Dim m_pEditingEvent As CalendarEvent
    Dim BeginSelection As Date, EndSelection As Date, AllDay As Boolean
    '-------------------------------------------------------------------------
    ' FALTA FAZER VERIFICACAO DE TOTAL DE VAGAS E NUM POR SLOTS
    
    If Ag_DevolveReq <= 0 Then
        BG_Mensagem mediMsgBox, "Ter� que seleccionar uma requisi��o."
        Exit Function
    End If
    CalAgenda.ActiveView.GetSelection BeginSelection, EndSelection, AllDay
    If AG_VerificaNumReqPorSlot(CStr(BeginSelection)) = False Then
        BG_Mensagem mediMsgBox, "N�o existem mais vagas na slot seleccionada."
        Exit Function
    End If
    If AG_VerificaNumVagas(CStr(BeginSelection)) = False Then
        BG_Mensagem mediMsgBox, "N�o existem mais vagas para dia seleccionado."
        Exit Function
    End If
    Set m_pEditingEvent = CalAgenda.DataProvider.CreateEvent
    
    m_pEditingEvent.StartTime = BeginSelection
    m_pEditingEvent.EndTime = BeginSelection + Ag_DevolveIntervalo / (24 * 60)
    If requisicao.tipo_req = "C" Then
        m_pEditingEvent.Subject = "Marc."
    Else
        m_pEditingEvent.Subject = "Requis."
    End If
    m_pEditingEvent.Subject = m_pEditingEvent.Subject & requisicao.n_req & "-" & requisicao.nome_ute
    m_pEditingEvent.Location = requisicao.descr_proven
    m_pEditingEvent.Body = ""
    m_pEditingEvent.CustomProperties.Property("N_REQ") = requisicao.n_req
   Set Ag_novoEvento = m_pEditingEvent
End Function

Private Sub Ag_PreenchePrioridades()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsA As New ADODB.recordset
    CalAgenda.DataProvider.LabelList.RemoveAll
    
    sSql = "SELECT * FROM sl_cod_prioridades"
    'BRUNODSANTOS ULSNE-1902 06.08.2018
    'rsA.CursorLocation = adUseServer
    rsA.CursorLocation = adUseClient
    rsA.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsA.Open sSql, gConexao
    While Not rsA.EOF
        CalAgenda.DataProvider.LabelList.AddLabel BL_HandleNull(rsA!cod_prio, mediComboValorNull), BL_HandleNull(rsA!cor, vbWhite), BL_HandleNull(rsA!descr_prio, "")
        rsA.MoveNext
    Wend
    rsA.Close
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro carregar prioridades: -> " & Err.Description & " ", "ClAgenda", "Ag_InicializaAgendaByCod", True
    Exit Sub
    Resume Next
End Sub

Public Function AG_DevolveIdentificadorEvento() As Long
    Dim prefixo As Long
    If requisicao.tipo_req = "C" Then
        prefixo = 10000000
    Else
        prefixo = 20000000
    End If
    AG_DevolveIdentificadorEvento = prefixo + requisicao.n_req
End Function

Private Function AG_VerificaEventoJaExiste(SchID As Long) As Boolean
    Dim i As Integer
End Function

Public Function AG_GravaEventoBD(m_evento As CalendarEvent) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iReg As Integer
    Dim rsA As New ADODB.recordset
    AG_GravaEventoBD = False
    BG_BeginTransaction
    sSql = "SELECT seq_agenda.nextval proximo FROM dual"
    'BRUNODSANTOS ULSNE-1902 06.08.2018
    'rsA.CursorLocation = adUseServer
    rsA.CursorLocation = adUseClient
    rsA.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsA.Open sSql, gConexao
    If rsA.RecordCount >= 1 Then
        m_evento.ScheduleID = BL_HandleNull(rsA!proximo, mediComboValorNull)
    End If
    rsA.Close
    Set rsA = Nothing
    If m_evento.ScheduleID > 0 Then
        sSql = "INSERT INTO sl_agenda (seq_agenda, flg_activo, cod_t_agenda, dT_marcacao, n_req, tipo_req, dt_ini, dt_fim, subject, observacao, location,cod_prioridade) VALUES("
        sSql = sSql & m_evento.ScheduleID & ",1, "
        sSql = sSql & BL_TrataStringParaBD(CStr(codificacao.cod_t_agenda)) & ", " & BL_TrataDataParaBD(m_evento.StartTime) & ","
        sSql = sSql & requisicao.n_req & ", " & BL_TrataStringParaBD(requisicao.tipo_req) & ","
        sSql = sSql & "to_date('" & m_evento.StartTime & "', 'dd-mm-yyyy hh24:mi:ss'), to_date('" & m_evento.EndTime & "', 'dd-mm-yyyy hh24:mi:ss'),"
        sSql = sSql & BL_TrataStringParaBD(m_evento.Subject) & ", " & BL_TrataStringParaBD(m_evento.Body) & ","
        sSql = sSql & BL_TrataStringParaBD(m_evento.Location) & ","
        sSql = sSql & m_evento.Label & ")"
        iReg = BG_ExecutaQuery_ADO(sSql)
        If iReg = 1 Then
            AG_GravaEventoBD = AG_RegistaLogAgenda(m_evento, "NOVO")
        End If
    End If
    BG_CommitTransaction
Exit Function
TrataErro:
    BG_RollbackTransaction
    AG_GravaEventoBD = False
    BG_LogFile_Erros "Erro AG_GravaEventoBD: -> " & Err.Description & " " & sSql, "ClAgenda", "AG_GravaEventoBD", True
    Exit Function
    Resume Next
End Function


Public Function AG_ExisteRequisicaoAgendada() As String
    Dim rsA As New ADODB.recordset
    Dim sSql As String
    On Error GoTo TrataErro
    
    AG_ExisteRequisicaoAgendada = ""
    sSql = "SELECT * FROM sl_agenda WHERE n_req = " & requisicao.n_req & " AND tipo_Req = " & BL_TrataStringParaBD(requisicao.tipo_req)
    sSql = sSql & " AND dt_ini IS NOT NULL AND dt_fim is not null AND seq_agenda IS NOT NULL AND nvl(flg_activo,1) = 1 "
    'BRUNODSANTOS ULSNE-1902 06.08.2018
    'rsA.CursorLocation = adUseServer
    rsA.CursorLocation = adUseClient
    rsA.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsA.Open sSql, gConexao
    If rsA.RecordCount >= 1 Then
        AG_ExisteRequisicaoAgendada = BL_HandleNull(rsA!dt_ini, "") & "-" & BL_HandleNull(rsA!dt_fim, "")
    Else
        AG_ExisteRequisicaoAgendada = ""
        
    End If
    rsA.Close
    Set rsA = Nothing
Exit Function
TrataErro:
    AG_ExisteRequisicaoAgendada = ""
    BG_LogFile_Erros "Erro AG_ExisteRequisicaoAgendada: -> " & Err.Description & " " & sSql, "ClAgenda", "AG_ExisteRequisicaoAgendada", True
    Exit Function
    Resume Next
End Function

Public Function AG_CarregaEventosAgenda() As Boolean
    Dim rsA As New ADODB.recordset
    Dim sSql As String
    Dim m_pEditingEvent As CalendarEvent
    On Error GoTo TrataErro
    
    AG_CarregaEventosAgenda = False
    sSql = "SELECT * FROM sl_agenda WHERE cod_t_agenda = " & codificacao.cod_t_agenda & " AND dt_ini IS NOT NULL "
    sSql = sSql & " AND dt_fim is not null AND seq_agenda IS NOT NULL AND nvl(flg_activo,1) = 1 "
    'BRUNODSANTOS ULSNE-1902 06.08.2018
    'rsA.CursorLocation = adUseServer
    rsA.CursorLocation = adUseClient
    '
    rsA.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsA.Open sSql, gConexao
    If rsA.RecordCount >= 1 Then
        While Not rsA.EOF
            Set m_pEditingEvent = Nothing
            Set m_pEditingEvent = CalAgenda.DataProvider.CreateEvent
            m_pEditingEvent.StartTime = BL_HandleNull(rsA!dt_ini, "")
            m_pEditingEvent.EndTime = BL_HandleNull(rsA!dt_fim, "")
            m_pEditingEvent.Subject = BL_HandleNull(rsA!Subject, "")
            m_pEditingEvent.Location = BL_HandleNull(rsA!Location, "")
            m_pEditingEvent.Body = BL_HandleNull(rsA!observacao, "")
            m_pEditingEvent.ScheduleID = BL_HandleNull(rsA!seq_agenda, mediComboValorNull)
            m_pEditingEvent.Label = BL_HandleNull(rsA!cod_prioridade, "")
            m_pEditingEvent.CustomProperties.Property("N_REQ") = BL_HandleNull(rsA!n_req, "")

            CalAgenda.DataProvider.AddEvent m_pEditingEvent
            rsA.MoveNext
        Wend
        CalAgenda.Populate
    End If
    rsA.Close
    Set rsA = Nothing
    AG_CarregaEventosAgenda = True
Exit Function
TrataErro:
    AG_CarregaEventosAgenda = False
    BG_LogFile_Erros "Erro AG_CarregaEventosAgenda: -> " & Err.Description & " " & sSql, "ClAgenda", "AG_CarregaEventosAgenda", True
    Exit Function
    Resume Next
End Function

Public Function AG_ApagaventoAgenda(m_evento As CalendarEvent) As Boolean
    Dim rsA As New ADODB.recordset
    Dim sSql As String
    Dim iReg As Integer

    On Error GoTo TrataErro
    BG_BeginTransaction
    AG_ApagaventoAgenda = False
    sSql = "UPDATE sl_agenda SET flg_activo = 0 WHERE seq_agenda = " & m_evento.ScheduleID
    sSql = sSql & ""
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg = 1 Then
        AG_ApagaventoAgenda = AG_RegistaLogAgenda(m_evento, "APAGAR")
    End If
    BG_CommitTransaction
Exit Function
TrataErro:
    BG_RollbackTransaction
    AG_ApagaventoAgenda = False
    BG_LogFile_Erros "Erro AG_ApagaventoAgenda: -> " & Err.Description & " " & sSql, "ClAgenda", "AG_ApagaventoAgenda", True
    Exit Function
    Resume Next
End Function

Public Function AG_ActualizaEventoAgenda(m_evento As CalendarEvent) As Boolean
    Dim rsA As New ADODB.recordset
    Dim sSql As String
    Dim iReg As Integer

    On Error GoTo TrataErro
    BG_BeginTransaction
    AG_ActualizaEventoAgenda = False
    sSql = "UPDATE sl_agenda SET "
    sSql = sSql & "cod_t_agenda = " & BL_TrataStringParaBD(CStr(codificacao.cod_t_agenda)) & ", dt_marcacao = " & BL_TrataDataParaBD(m_evento.StartTime) & ","
    sSql = sSql & "dt_ini  = to_date('" & m_evento.StartTime & "', 'dd-mm-yyyy hh24:mi:ss'), dt_fim = to_date('" & m_evento.EndTime & "', 'dd-mm-yyyy hh24:mi:ss'),"
    sSql = sSql & "subject = " & BL_TrataStringParaBD(m_evento.Subject) & ", observacao = " & BL_TrataStringParaBD(m_evento.Body) & ", location ="
    sSql = sSql & BL_TrataStringParaBD(m_evento.Location) & " where seq_agenda = " & m_evento.ScheduleID
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg = 1 Then
        AG_ActualizaEventoAgenda = AG_RegistaLogAgenda(m_evento, "ALTERACAO")
    End If
    BG_CommitTransaction
Exit Function
TrataErro:
    BG_RollbackTransaction
    AG_ActualizaEventoAgenda = False
    BG_LogFile_Erros "Erro AG_ActualizaEventoAgenda: -> " & Err.Description & " " & sSql, "ClAgenda", "AG_ActualizaEventoAgenda", True
    Exit Function
    Resume Next
End Function
Private Function AG_RegistaLogAgenda(m_evento As CalendarEvent, accao As String) As Boolean

    Dim rsA As New ADODB.recordset
    Dim sSql As String
    Dim iReg As Integer
    On Error GoTo TrataErro
    
    AG_RegistaLogAgenda = False
    sSql = "INSERT INTO sl_agenda_log (seq_agenda, n_req, user_cri, dt_cri, dt_ini,dt_fim, accao) VALUES("
    sSql = sSql & m_evento.ScheduleID & "," & m_evento.CustomProperties.Property("N_REQ") & ","
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ",SYSDATE, "
    sSql = sSql & "to_date('" & m_evento.StartTime & "', 'dd-mm-yyyy hh24:mi:ss'), to_date('" & m_evento.EndTime & "', 'dd-mm-yyyy hh24:mi:ss'),"
    sSql = sSql & BL_TrataStringParaBD(accao) & ")"
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg = 1 Then
        AG_RegistaLogAgenda = True
    End If
Exit Function
TrataErro:
    AG_RegistaLogAgenda = False
    BG_LogFile_Erros "Erro AG_RegistaLogAgenda: -> " & Err.Description & " " & sSql, "ClAgenda", "AG_RegistaLogAgenda", True
    Exit Function
    Resume Next
End Function


Private Function AG_VerificaNumReqPorSlot(dt_ini As String) As Boolean
    Dim rsA As New ADODB.recordset
    Dim sSql As String
    Dim iReg As Integer
    On Error GoTo TrataErro
    
    AG_VerificaNumReqPorSlot = False
    sSql = "SELECT * FROM sl_agenda where cod_t_agenda = " & codificacao.cod_t_agenda & " AND nvl(flg_Activo,1) =1 "
    sSql = sSql & " AND dt_ini = to_date('" & dt_ini & "', 'dd-mm-yyyy hh24:mi:ss')"
    'BRUNODSANTOS ULSNE-1902 06.08.2018
    'rsA.CursorLocation = adUseServer
    rsA.CursorLocation = adUseClient
    rsA.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsA.Open sSql, gConexao
    If rsA.RecordCount < codificacao.num_marcacoes_slot Then
        AG_VerificaNumReqPorSlot = True
    End If
    rsA.Close
    Set rsA = Nothing
Exit Function
TrataErro:
    AG_VerificaNumReqPorSlot = False
    BG_LogFile_Erros "Erro AG_VerificaNumReqPorSlot: -> " & Err.Description & " " & sSql, "ClAgenda", "AG_VerificaNumReqPorSlot", True
    Exit Function
    Resume Next
End Function

Private Function AG_VerificaNumVagas(dt_ini As String) As Boolean
    Dim rsA As New ADODB.recordset
    Dim sSql As String
    Dim iReg As Integer
    On Error GoTo TrataErro
    
    AG_VerificaNumVagas = False
    sSql = "SELECT * FROM sl_agenda where cod_t_agenda = " & codificacao.cod_t_agenda & " AND nvl(flg_Activo,1) =1 "
    sSql = sSql & " AND trunc(dt_ini) = " & BL_TrataDataParaBD(dt_ini)
    'BRUNODSANTOS ULSNE-1902 06.08.2018
    'rsA.CursorLocation = adUseServer
    rsA.CursorLocation = adUseClient
    rsA.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsA.Open sSql, gConexao
    If rsA.RecordCount < codificacao.limite Then
        AG_VerificaNumVagas = True
    End If
    rsA.Close
    Set rsA = Nothing
Exit Function
TrataErro:
    AG_VerificaNumVagas = False
    BG_LogFile_Erros "Erro AG_VerificaNumVagas: -> " & Err.Description & " " & sSql, "ClAgenda", "AG_VerificaNumVagas", True
    Exit Function
    Resume Next
End Function

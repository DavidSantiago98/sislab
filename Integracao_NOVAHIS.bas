Attribute VB_Name = "Integracao_NOVAHIS"
Option Explicit

' ---------------------------------------------------------------------------

' TRANSFERE OS DADOS DO NOVAHIS PARA SISLAB

' ---------------------------------------------------------------------------
Public Function IN_TransferePedidoParaSISLAB(Utente As String, num_pedido As Long) As Boolean
    On Error GoTo TrataErro
    
    ' TRANSFERE UTENTES
    If IN_TransfereUtente(Utente, num_pedido, False) = False Then
        IN_TransferePedidoParaSISLAB = False
        Exit Function
    End If
    
    
    'TRANSFERE ENVIO DE EMAIL
    If IN_TransfereEnvioEmail(num_pedido) = False Then
        IN_TransferePedidoParaSISLAB = False
        Exit Function
    End If
    
    ' TRANSFERE ANALISES
    If IN_TransfereAnalises(Utente, num_pedido) = False Then
        IN_TransferePedidoParaSISLAB = False
        Exit Function
    End If

    
Exit Function
TrataErro:
    IN_TransferePedidoParaSISLAB = False
    BG_LogFile_Erros "Integracao_NOVAHIS: IN_TransferePedidoParaSISLAB: " & Err.Description, "BL", "IN_TransferePedidoParaSISLAB", False
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------------------

' TRANSFERE OS DADOS DAS TABELAS PACIENTES_HIS PARA SL_NOVAHIS_PACIENTES_HIS

' ----------------------------------------------------------------------------------------------------------
Public Function IN_TransfereUtente(Utente As String, num_pedido As Long, flg_insereSislab As Boolean) As Boolean
    Dim sSql As String
    Dim rsPac As New ADODB.recordset
    Dim registos As Integer
    Dim iBD_Aberta
    Dim t_utente As String
    Dim seq_utente As Long
    Dim Sexo As Integer
    Dim nome As String
    
    On Error GoTo TrataErro
    
    iBD_Aberta = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
    If iBD_Aberta = 0 Then
        IN_TransfereUtente = False
        Exit Function
    End If
    
    sSql = "SELECT * FROM pacientes_his WHERE nhc = " & BL_TrataStringParaBD(Utente)
    rsPac.CursorLocation = adUseServer
    rsPac.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsPac.Open sSql, gConnHIS
    If (rsPac.RecordCount = 1) Then
        sSql = " INSERT INTO sl_novahis_pacientes_his (nombre, restantes, ultimo_ape, nhc, sexo, nac_fecha, telefono1,"
        sSql = sSql & " telefono2, morada, cod_postal, nom_postal,dt_cri, num_pedido, cod_local,email) VALUES ("
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!nombre, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!restantes, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!ultimo_ape, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!nhc, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!Sexo, "")) & ","
        sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsPac!nac_fecha, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!telefono1, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!telefono2, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!morada, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!cod_postal, "")) & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!nom_postal, "")) & ","
        sSql = sSql & "sysdate,"
        sSql = sSql & num_pedido & ","
        sSql = sSql & gCodLocal & ","
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!nom_postal, "")) & ")"
        registos = BG_ExecutaQuery_ADO(sSql)
        If registos = 1 Then
            IN_TransfereUtente = True
        Else
            IN_TransfereUtente = False
        End If
        If flg_insereSislab = True Then
        
            If gCodLocal = 1 Then
                t_utente = "CLP"
            Else
                t_utente = "HAR"
            End If
            If BL_HandleNull(rsPac!Sexo, "") = "Feminino" Then
                Sexo = gT_Feminino
            ElseIf BL_HandleNull(rsPac!Sexo, "") = "Masculino" Then
                Sexo = gT_Masculino
            Else
                Sexo = gT_Indeterminado
            End If
            nome = Trim(BL_HandleNull(rsPac!nombre, "")) & " " & Trim(BL_HandleNull(rsPac!restantes, "")) & " " & Trim(BL_HandleNull(rsPac!ultimo_ape, ""))
            seq_utente = IN_VerificaUtenteJaExiste(t_utente, Utente)
            If seq_utente = mediComboValorNull Then
                seq_utente = OBJ_Utente.UTENTE_Inserir(Utente, t_utente, Utente, "", "", Bg_DaData_ADO, nome, _
                                        BL_HandleNull(rsPac!nac_fecha, ""), CStr(Sexo), "", _
                                        BL_HandleNull(rsPac!morada, ""), BL_HandleNull(rsPac!cod_postal, ""), _
                                        BL_HandleNull(rsPac!telefono1, ""), "", "0", "", "", "", CStr(gCodUtilizador), mediComboValorNull)
            Else
                 OBJ_Utente.UTENTE_Actualizar Utente, t_utente, Utente, "", "", Bg_DaData_ADO, nome, _
                                        BL_HandleNull(rsPac!nac_fecha, ""), CStr(Sexo), "", _
                                        BL_HandleNull(rsPac!morada, ""), BL_HandleNull(rsPac!cod_postal, ""), _
                                        BL_HandleNull(rsPac!telefono1, ""), "", "0", "", "", "", CStr(gCodUtilizador), seq_utente
            End If
            IN_TransfereUtente = True
        End If
    Else
        IN_TransfereUtente = False
    End If
    rsPac.Close
    Set rsPac = Nothing
    gConnHIS.Close
    
Exit Function
TrataErro:
    IN_TransfereUtente = False
    BG_LogFile_Erros "Integracao_NOVAHIS: IN_TransfereUtente: " & Err.Description, "BL", "IN_TransfereUtente", False
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------------------

' TRANSFERE OS DADOS DAS TABELAS BUZON_LAB PARA SL_NOVAHIS_BUZON_LAB

' ----------------------------------------------------------------------------------------------------------
Private Function IN_TransfereAnalises(Utente As String, num_pedido As Long) As Boolean
    Dim sSql As String
    Dim rsPac As New ADODB.recordset
    Dim n_pedido As String
    Dim registos As Integer
    Dim iBD_Aberta
    On Error GoTo TrataErro
    
    iBD_Aberta = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
    If iBD_Aberta = 0 Then
        IN_TransfereAnalises = False
        Exit Function
    End If
    n_pedido = ""
    sSql = "SELECT * FROM buzon_lab WHERE nhc = " & BL_TrataStringParaBD(Utente) & " AND ((tratado = 0 and trunc(fecha_petic) = (trunc(sysdate)-2)) or n_pedido = " & num_pedido & ") "
    sSql = sSql & " ORDER by n_pedido ASC, n_orden ASC "
    rsPac.CursorLocation = adUseServer
    rsPac.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsPac.Open sSql, gConnHIS
    If (rsPac.RecordCount >= 1) Then
        While Not rsPac.EOF
            If BL_HandleNull(rsPac!tratado, "0") = 1 And n_pedido <> BL_HandleNull(rsPac!tratado, "0") Then
                BG_Mensagem mediMsgBox, "O pedido " & BL_HandleNull(rsPac!tratado, "0") & " j� foi tratado.", vbExclamation, "Pedido j� tratado"
                n_pedido = BL_HandleNull(rsPac!tratado, "0")
            End If
            sSql = "INSERT INTO sl_novahis_buzon_lab(buzon_lab_pk, actividad_det_pk, codigo_cliente, prest_item_pk, codigo_servicio,"
            sSql = sSql & " codigo_personal, codigo_personal2, n_pedido, n_orden,nhc,fecha_petic, prest_item_cod, tratado, fecha_exp,"
            sSql = sSql & " hora_exp, fecha_imp, hora_imp, usuario_imp, tipo_episodio_pk, entidad, n_contrib, codigo_garante_pk, "
            sSql = sSql & " cod_centro, codigo_area, cod_local,dt_cri) VALUES( "
            sSql = sSql & BL_HandleNull(rsPac!buzon_lab_pk, -1) & ", "
            sSql = sSql & BL_HandleNull(rsPac!actividad_det_pk, -1) & ", "
            sSql = sSql & BL_HandleNull(rsPac!codigo_cliente, -1) & ", "
            sSql = sSql & BL_HandleNull(rsPac!prest_item_pk, -1) & ", "
            sSql = sSql & BL_HandleNull(rsPac!codigo_servicio, -1) & ", "
            sSql = sSql & BL_HandleNull(rsPac!codigo_personal, -1) & ", "
            sSql = sSql & BL_HandleNull(rsPac!codigo_personal2, -1) & ", "
            sSql = sSql & BL_HandleNull(rsPac!n_pedido, -1) & ", "
            sSql = sSql & BL_HandleNull(rsPac!n_orden, -1) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!nhc, "")) & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsPac!fecha_petic, "")) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!prest_item_cod, "")) & ", "
            sSql = sSql & "0, "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsPac!fecha_exp, "")) & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsPac!hora_exp, "")) & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsPac!fecha_imp, "")) & ", "
            sSql = sSql & BL_TrataDataParaBD(BL_HandleNull(rsPac!hora_imp, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!usuario_imp, -1)) & ", "
            sSql = sSql & BL_HandleNull(rsPac!tipo_episodio_pk, -1) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!entidad, "")) & ", "
            sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsPac!n_contrib, "")) & ", "
            sSql = sSql & BL_HandleNull(rsPac!codigo_garante_pk, -1) & ", "
            sSql = sSql & BL_HandleNull(rsPac!cod_centro, -1) & ", "
            sSql = sSql & BL_HandleNull(rsPac!codigo_area, -1) & ", "
            sSql = sSql & gCodLocal & ", "
            sSql = sSql & "sysdate) "
            registos = BG_ExecutaQuery_ADO(sSql)
            If registos <> 1 Then
                GoTo TrataErro
            End If
            
            ' TRANSFERE DADOS EPISODIO
            If IN_TransfereEpisodio(BL_HandleNull(rsPac!actividad_det_pk, -1)) = False Then
                GoTo TrataErro
            End If
            
            sSql = "UPDATE buzon_lab SET tratado = 1, fecha_imp = trunc(sysdate),hora_imp = sysdate, usuario_imp = 'SISLAB'  "
            sSql = sSql & " WHERE buzon_lab_pk = " & rsPac!buzon_lab_pk & " AND trim(nhc)  = " & BL_TrataStringParaBD(rsPac!nhc) & " AND trim(prest_item_cod) = " & BL_TrataStringParaBD(rsPac!prest_item_cod)
            gConnHIS.Execute sSql, registos
            
            If registos <> 1 Then
                GoTo TrataErro
            End If
            rsPac.MoveNext
        Wend
    Else
        IN_TransfereAnalises = False
    End If
    rsPac.Close
    Set rsPac = Nothing
    gConnHIS.Close
    
Exit Function
TrataErro:
    IN_TransfereAnalises = False
    BG_LogFile_Erros "Integracao_NOVAHIS: IN_TransfereAnalises: " & Err.Description, "BL", "IN_TransfereAnalises", False
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------------------------------------------

' TRANSFERE ENVIO DE EMAIL PARA TABELA DO SISLAB

' ----------------------------------------------------------------------------------------------------------
Private Function IN_TransfereEnvioEmail(num_pedido As Long) As Boolean
    Dim sSql As String
    Dim rsPac As New ADODB.recordset
    Dim n_pedido As String
    Dim registos As Integer
    Dim iBD_Aberta
    On Error GoTo TrataErro
    
    iBD_Aberta = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
    If iBD_Aberta = 0 Then
        IN_TransfereEnvioEmail = False
        Exit Function
    End If
    n_pedido = ""
    sSql = "SELECT * FROM sislab_envio WHERE n_pedido_env_email = " & num_pedido
    rsPac.CursorLocation = adUseServer
    rsPac.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsPac.Open sSql, gConnHIS
    If (rsPac.RecordCount >= 1) Then
        sSql = "INSERT INTO sl_novahis_sislab_envio(n_pedido_env_email, data) VALUES( "
        sSql = sSql & BL_HandleNull(rsPac!n_pedido_env_email, -1) & ", "
        sSql = sSql & "sysdate) "
        
        registos = BG_ExecutaQuery_ADO(sSql)
        If registos <> 1 Then
            GoTo TrataErro
        End If
    End If
    IN_TransfereEnvioEmail = True
    rsPac.Close
    Set rsPac = Nothing
    gConnHIS.Close
Exit Function
TrataErro:
    IN_TransfereEnvioEmail = False
    BG_LogFile_Erros "Integracao_NOVAHIS: IN_TransfereEnvioEmail: " & Err.Description, "BL", "IN_TransfereEnvioEmail", False
    Exit Function
    Resume Next
End Function




' ----------------------------------------------------------------------------------------------------------

' TRANSFERE OS DADOS DO EPISODIO
' ----------------------------------------------------------------------------------------------------------
Private Function IN_TransfereEpisodio(actividad_det_pk As String) As Boolean
    Dim sSql As String
    Dim RsP As New ADODB.recordset
    Dim registos As Integer
    Dim iBD_Aberta
    On Error GoTo TrataErro
    
    iBD_Aberta = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
    If iBD_Aberta = 0 Then
        IN_TransfereEpisodio = False
        Exit Function
    End If
    
    sSql = "SELECT * FROM sislab_epi WHERE (actividad_det_pk) = " & actividad_det_pk
    RsP.CursorLocation = adUseServer
    RsP.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsP.Open sSql, gConnHIS
    If (RsP.RecordCount >= 1) Then
        sSql = "INSERT INTO sl_novahis_sislab_epi(cod_unidade, actividad_det_pk, epis_pk, prest_item_pk, "
        sSql = sSql & " prest_item_cod, prest_item_desc, cod_med_req, med_req,cod_serv_req, serv_req) VALUES( "
        sSql = sSql & BL_HandleNull(RsP!cod_unidade, -1) & ", "
        sSql = sSql & BL_HandleNull(RsP!actividad_det_pk, -1) & ", "
        sSql = sSql & BL_HandleNull(RsP!epis_pk, -1) & ", "
        sSql = sSql & BL_HandleNull(RsP!prest_item_pk, -1) & ", "
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(RsP!prest_item_cod, "")) & ", "
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(RsP!prest_item_desc, "")) & ", "
        sSql = sSql & BL_HandleNull(RsP!cod_med_req, -1) & ", "
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(RsP!med_req, "")) & ", "
        sSql = sSql & BL_HandleNull(RsP!cod_serv_req, -1) & ", "
        sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(RsP!serv_req, "")) & ") "
        registos = BG_ExecutaQuery_ADO(sSql)
        
        If registos <> 1 Then
            GoTo TrataErro
        End If
        IN_TransfereEpisodio = True
    Else
        IN_TransfereEpisodio = False
    End If
    RsP.Close
    Set RsP = Nothing

    
Exit Function
TrataErro:
    IN_TransfereEpisodio = False
    BG_LogFile_Erros "Integracao_NOVAHIS: IN_TransfereEpisodio: " & Err.Description, "BL", "IN_TransfereEpisodio", False
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------------------------------------------

' REGISTA ANALISES NO ECRA GEST�O DE REQUISICOES

' ----------------------------------------------------------------------------------------------------------
Public Function IN_RegistaAnalises(Utente As String) As Boolean
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim codAna As String
    Dim i As Integer
    On Error GoTo TrataErro
    sSql = "SELECT distinct x1.cod_centro,x1.codigo_garante_pk, x1.prest_item_cod,x2.cod_med_req,x2.cod_serv_req, x1.n_pedido,x1.n_orden, x3.n_pedido_env_email "
    sSql = sSql & " FROM sl_novahis_buzon_lab x1 LEFT OUTER JOIN sl_novahis_sislab_envio x3 ON x1.n_pedido = x3.n_pedido_env_email, sl_novahis_sislab_epi x2 "
    sSql = sSql & " WHERE x1.actividad_det_pk = x2.actividad_det_pk AND  nhc = " & BL_TrataStringParaBD(Utente) & " AND tratado = 0  and cod_local = " & gCodLocal
    sSql = sSql & " AND  x1.fecha_petic >= trunc(sysdate) -10 "
    sSql = sSql & " ORDER by n_pedido ASC , n_orden ASC "
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If (rsAna.RecordCount >= 1) Then
        i = 1
        FormGestaoRequisicao.EcCodSala = IN_DevolveCodigo("sl_cod_salas", "sl_ass_salas_locais", "cod_sala", BL_HandleNull(rsAna!cod_centro, ""))
        FormGestaoRequisicao.EcCodsala_Validate False
        If FormGestaoRequisicao.EcCodSala <> "" Then
            If IsNumeric(FormGestaoRequisicao.EcCodSala) = True Then
                FormGestaoRequisicao.EcCodProveniencia = FormGestaoRequisicao.EcCodSala * 100
            Else
                FormGestaoRequisicao.EcCodProveniencia = "0"
            End If
        Else
                FormGestaoRequisicao.EcCodProveniencia = "0"
        End If
        FormGestaoRequisicao.EcCodProveniencia_Validate False
        FormGestaoRequisicao.EcCodValencia = IN_DevolveCodigo("sl_valencia", "sl_ass_valencia_locais", "cod_valencia", BL_HandleNull(rsAna!cod_serv_req, ""))
        FormGestaoRequisicao.EcCodValencia_Validate False
        If FormGestaoRequisicao.EcCodValencia.Text = "10404" Then
            FormGestaoRequisicao.EcCodSala.Text = "10"
            FormGestaoRequisicao.EcCodsala_Validate False
        End If
        FormGestaoRequisicao.EcCodMedico = IN_DevolveCodigo("sl_medicos", "sl_ass_med_locais", "cod_med", BL_HandleNull(rsAna!cod_med_req, ""))
        FormGestaoRequisicao.EcCodMedico_Validate False
        FormGestaoRequisicao.EcCodEfr = IN_DevolveCodigo("sl_efr", "sl_ass_efr_locais", "cod_efr", BL_HandleNull(rsAna!codigo_garante_pk, ""))
        FormGestaoRequisicao.EcCodEFR_Validate False
        FormGestaoRequisicao.EcDataPrevista = Bg_DaData_ADO
        FormGestaoRequisicao.EcDataPrevista_Validate False
        'FormGestaoRequisicao.EcPrescricao = BL_HandleNull(rsAna!n_pedido, "")
        If BL_HandleNull(rsAna!n_pedido_env_email, "") <> "" Then
            For i = 0 To FormGestaoRequisicao.CbDestino.ListCount - 1
                If FormGestaoRequisicao.CbDestino.ItemData(i) = gCodDestinoEmailUte Then
                    FormGestaoRequisicao.CbDestino.ListIndex = i
                    Exit For
                End If
            Next
        Else
            FormGestaoRequisicao.CbDestino.ListIndex = mediComboValorNull
        End If
        DoEvents
        
        FormGestaoRequisicao.FGAna.Visible = False
        While Not rsAna.EOF
            If BL_HandleNull(rsAna!prest_item_cod, "") <> "" Then
                codAna = IN_DevolveCodigoFact(BL_HandleNull(rsAna!prest_item_cod, ""))
                If codAna <> "" Then
                    FormGestaoRequisicao.EcAuxAna = codAna
                    FormGestaoRequisicao.EcAuxAna_KeyDown vbKeyReturn, 0
                Else
                    ' MARTELANCO
                    codAna = "S9999"
                End If
            End If
            i = i + 1
            rsAna.MoveNext
        Wend
        FormGestaoRequisicao.FGAna.Visible = True
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    IN_RegistaAnalises = False
    BG_LogFile_Erros "Integracao_NOVAHIS: IN_RegistaAnalises: " & Err.Description, "BL", "IN_RegistaAnalises", False
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------------------

' DEVOLVE CODIGO BASEADO NO CODIGO_EXT DE CADA TABELA. ENTIDADE, MEDICO, SALA, PROVENIENCIA

' ----------------------------------------------------------------------------------------------------------
Public Function IN_DevolveCodigo(Tabela As String, tabela_assoc As String, Codigo As String, cod_ext As String) As String
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    If cod_ext = "" Then Exit Function
    
    sSql = "SELECT " & Codigo & " FROM " & Tabela & " WHERE cod_ext = " & BL_TrataStringParaBD(cod_ext) & " AND " & Codigo & " IN (SELECT x2." & Codigo
    sSql = sSql & " FROM " & tabela_assoc & " x2 WHERE x2.cod_local = " & gCodLocal & ")"
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If (rsAna.RecordCount = 1) Then
        IN_DevolveCodigo = BL_HandleNull(rsAna(0), "")
    Else
        IN_DevolveCodigo = ""
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    IN_DevolveCodigo = ""
    BG_LogFile_Erros "Integracao_NOVAHIS: IN_DevolveCodigo: " & Err.Description, "BL", "IN_DevolveCodigo", False
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------------------

' DEVOLVE CODIGO BASEADO NO CODIGO_EXT DE CADA TABELA. ENTIDADE, MEDICO, SALA, PROVENIENCIA

' ----------------------------------------------------------------------------------------------------------
Public Function IN_DevolveCodigoFact(cod_ext As String) As String
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
    If cod_ext = "" Then Exit Function
    
    sSql = "SELECT cod_ana FROM sl_ana_facturacao WHERE cod_ana_gh = " & BL_TrataStringParaBD(cod_ext) & " AND seq_ana  IN (SELECT x2.seq_ana"
    sSql = sSql & " FROM sl_ass_ana_fact_locais x2 WHERE x2.cod_local = " & gCodLocal & ")"
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If (rsAna.RecordCount >= 1) Then
        IN_DevolveCodigoFact = BL_HandleNull(rsAna(0), "")
    Else
        IN_DevolveCodigoFact = ""
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    IN_DevolveCodigoFact = ""
    BG_LogFile_Erros "Integracao_NOVAHIS: IN_DevolveCodigoFact: " & Err.Description, "BL", "IN_DevolveCodigoFact", False
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------------------

' DEVOLVE CODIGO BASEADO NO CODIGO_EXT DE CADA TABELA. ENTIDADE, MEDICO, SALA, PROVENIENCIA

' ----------------------------------------------------------------------------------------------------------
Public Function IN_DevolveCodigoFactExt(cod_ana As String) As String
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    Dim cod_ext As String
    On Error GoTo TrataErro
    If cod_ana = "" Then Exit Function
    
    sSql = "SELECT cod_ana_gh FROM sl_ana_facturacao WHERE cod_ana = " & BL_TrataStringParaBD(cod_ana) & " AND seq_ana  IN (SELECT x2.seq_ana"
    sSql = sSql & " FROM sl_ass_ana_fact_locais x2 WHERE x2.cod_local = " & gCodLocal & ")"
    sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
    sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    IN_DevolveCodigoFactExt = ""
    If (rsAna.RecordCount >= 1) Then
        While Not rsAna.EOF
            IN_DevolveCodigoFactExt = IN_DevolveCodigoFactExt & BL_TrataStringParaBD(BL_HandleNull(rsAna(0), "")) & ","
            rsAna.MoveNext
        Wend
    Else
        IN_DevolveCodigoFactExt = ""
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    IN_DevolveCodigoFactExt = ""
    BG_LogFile_Erros "Integracao_NOVAHIS: IN_DevolveCodigoFactExt: " & Err.Description, "BL", "IN_DevolveCodigoFactExt", False
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------------------

' ALTERA ESTADO NA SL_NOVAHIS_BUZON_LAB PARA AS ANALISES INSERIDAS NO SISLAB

' ----------------------------------------------------------------------------------------------------------

Public Function IN_AlteraEstadoSISLAB(Utente As String, n_req As String, analises As String) As Boolean
    Dim sSql As String
    Dim cod_ext As String
    Dim registos As Integer
    Dim analise() As String
    Dim i As Integer
    
    On Error GoTo TrataErro
    
    BL_Tokenize analises, ",", analise
    
    For i = 0 To UBound(analise)
        If analise(i) <> "" Then
            cod_ext = IN_DevolveCodigoFactExt(Replace(analise(i), "'", ""))
            
            IN_AlteraEstadoSISLAB = False
            If cod_ext <> "" Then
                cod_ext = "(" & Mid(cod_ext, 1, Len(cod_ext) - 1) & ")"
                sSql = "UPDATE sl_novahis_buzon_lab SET tratado = 1, fecha_imp = trunc(sysdate), hora_imp = sysdate, usuario_imp = 'SISLAB',"
                sSql = sSql & " n_req = " & n_req & ", cod_agrup = " & analise(i) & " WHERE nhc = " & BL_TrataStringParaBD(Utente)
                sSql = sSql & " AND prest_item_cod in " & cod_ext & " AND (tratado IS NULL or Tratado = 0) "
                registos = BG_ExecutaQuery_ADO(sSql)
            End If
        End If
    Next
    sSql = "UPDATE sl_novahis_buzon_lab SET tratado = -1 WHERE nhc = " & BL_TrataStringParaBD(Utente) & " AND tratado = 0 "
    registos = BG_ExecutaQuery_ADO(sSql)
    IN_AlteraEstadoSISLAB = True
Exit Function
TrataErro:
    IN_AlteraEstadoSISLAB = False
    BG_LogFile_Erros "Integracao_NOVAHIS: IN_AlteraEstadoSISLAB: " & Err.Description, "BL", "IN_AlteraEstadoSISLAB", False
    Exit Function
    Resume Next
End Function


Private Function IN_VerificaUtenteJaExiste(t_utente As String, Utente As String) As Long
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    
    On Error GoTo TrataErro
    IN_VerificaUtenteJaExiste = mediComboValorNull
    sSql = "SELECT * FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(t_utente) & " And utente = " & BL_TrataStringParaBD(Utente)
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAna.Open sSql, gConexao
    If (rsAna.RecordCount >= 1) Then
        IN_VerificaUtenteJaExiste = BL_HandleNull(rsAna!seq_utente, mediComboValorNull)
    Else
        IN_VerificaUtenteJaExiste = mediComboValorNull
    End If
    rsAna.Close
    Set rsAna = Nothing
Exit Function
TrataErro:
    IN_VerificaUtenteJaExiste = mediComboValorNull
    BG_LogFile_Erros "Integracao_NOVAHIS: IN_VerificaUtenteJaExiste: " & Err.Description, "BL", "IN_VerificaUtenteJaExiste", False
    Exit Function
    Resume Next
End Function

VERSION 5.00
Begin VB.Form FormIdentCliente 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormIdentCliente"
   ClientHeight    =   6075
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8685
   Icon            =   "FormIdentCliente.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6075
   ScaleWidth      =   8685
   ShowInTaskbar   =   0   'False
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   120
      TabIndex        =   13
      Top             =   3225
      Width           =   8445
   End
   Begin VB.TextBox EcMorada 
      Height          =   285
      Left            =   1320
      TabIndex        =   12
      Top             =   2400
      Width           =   6615
   End
   Begin VB.TextBox EcEstado 
      Height          =   285
      Left            =   1080
      TabIndex        =   20
      Top             =   5400
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox EcNSeq 
      Height          =   285
      Left            =   2400
      TabIndex        =   10
      Top             =   2040
      Width           =   585
   End
   Begin VB.TextBox EcCodPost 
      Height          =   285
      Left            =   1320
      TabIndex        =   9
      Top             =   2040
      Width           =   1035
   End
   Begin VB.TextBox EcLocalidade 
      Height          =   285
      Left            =   3120
      TabIndex        =   11
      Top             =   2040
      Width           =   4815
   End
   Begin VB.Frame Frame1 
      Caption         =   "Estado "
      Height          =   1845
      Left            =   6360
      TabIndex        =   18
      Top             =   120
      Width           =   2085
      Begin VB.CheckBox CkGrupoAna 
         Caption         =   "Display Grupo An�lises"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   1560
         Width           =   1935
      End
      Begin VB.OptionButton opt_d 
         Caption         =   "Desactivado"
         Height          =   285
         Left            =   120
         TabIndex        =   8
         Top             =   1080
         Width           =   1905
      End
      Begin VB.OptionButton opt_s 
         Caption         =   "Display em Relat�rios"
         Height          =   285
         Left            =   120
         TabIndex        =   7
         Top             =   720
         Width           =   1875
      End
      Begin VB.OptionButton opt_e 
         Caption         =   "Display em Forms"
         Height          =   285
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   1635
      End
   End
   Begin VB.TextBox EcFax 
      Height          =   285
      Left            =   1320
      TabIndex        =   5
      Top             =   1680
      Width           =   4785
   End
   Begin VB.TextBox EcTelefones 
      Height          =   285
      Left            =   1320
      TabIndex        =   4
      Top             =   1320
      Width           =   4785
   End
   Begin VB.TextBox EcContacto 
      Height          =   285
      Left            =   1320
      TabIndex        =   3
      Top             =   960
      Width           =   4785
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   1320
      TabIndex        =   1
      Top             =   240
      Width           =   1455
   End
   Begin VB.TextBox EcEmpresa 
      Height          =   285
      Left            =   1320
      TabIndex        =   2
      Top             =   600
      Width           =   4785
   End
   Begin VB.Label Label7 
      Caption         =   "EcEstado"
      Height          =   255
      Left            =   240
      TabIndex        =   24
      Top             =   5400
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label9 
      Caption         =   "Contacto"
      Height          =   255
      Left            =   4560
      TabIndex        =   23
      Top             =   2880
      Width           =   885
   End
   Begin VB.Label Label5 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   240
      TabIndex        =   22
      Top             =   2880
      Width           =   885
   End
   Begin VB.Label Label1 
      Caption         =   "Morada "
      Height          =   255
      Left            =   240
      TabIndex        =   21
      Top             =   2400
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "C�digo Postal "
      Height          =   255
      Left            =   240
      TabIndex        =   19
      Top             =   2040
      Width           =   1185
   End
   Begin VB.Label Label10 
      Caption         =   "Fax "
      Height          =   255
      Left            =   240
      TabIndex        =   17
      Top             =   1680
      Width           =   885
   End
   Begin VB.Label Label4 
      Caption         =   "Telefones "
      Height          =   255
      Left            =   240
      TabIndex        =   16
      Top             =   1320
      Width           =   885
   End
   Begin VB.Label Label2 
      Caption         =   "Contacto "
      Height          =   255
      Left            =   240
      TabIndex        =   15
      Top             =   960
      Width           =   885
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo "
      Height          =   255
      Left            =   240
      TabIndex        =   14
      Top             =   240
      Width           =   645
   End
   Begin VB.Label Label6 
      Caption         =   "Empresa "
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   885
   End
End
Attribute VB_Name = "FormIdentCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.text = UCase(EcCodigo.text)
End Sub

Private Sub EcDescricao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDescricao_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Private Sub EcCodPost_LostFocus()
    EcCodPost.text = UCase(EcCodPost.text)
End Sub


Private Sub EcEstado_Change()

If EcEstado.text = "E" Then
    opt_e.value = True
ElseIf EcEstado = "S" Then
    opt_s.value = True
Else
    opt_d.value = True
End If

End Sub

Private Sub EcLista_Click()

    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If

End Sub

Private Sub EcNSeq_Change()
    EcNSeq.text = UCase(EcNSeq.text)
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Identifica��o do Cliente"
    Me.left = 540
    Me.top = 80
    Me.Width = 8775
    Me.Height = 5760 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "gr_empr_inst"
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 11
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "empresa_id"
    CamposBD(1) = "nome_empr"
    CamposBD(2) = "t_contacto"
    CamposBD(3) = "telefones"
    CamposBD(4) = "n_fax"
    CamposBD(5) = "estado"
    CamposBD(6) = "localidade"
    CamposBD(7) = "cod_postal"
    CamposBD(8) = "n_seq_postal"
    CamposBD(9) = "morada"
    CamposBD(10) = "flg_grupoana"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcEmpresa
    Set CamposEc(2) = EcContacto
    Set CamposEc(3) = EcTelefones
    Set CamposEc(4) = EcFax
    Set CamposEc(5) = EcEstado
    Set CamposEc(6) = EcLocalidade
    Set CamposEc(7) = EcCodPost
    Set CamposEc(8) = EcNSeq
    Set CamposEc(9) = EcMorada
    Set CamposEc(10) = CkGrupoAna
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "C�digo da Empresa"
    TextoCamposObrigatorios(1) = "Nome do Cliente"
        
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "empresa_id"
    Set ChaveEc = EcCodigo
        
    CamposBDparaListBox = Array("nome_empr", "t_contacto")
    NumEspacos = Array(42, 23)

    CkGrupoAna.value = 2
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormIdentCliente = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus

    opt_e.value = False
    opt_s.value = False
    EcEstado.text = ""
    BG_LimpaCampo_Todos CamposEc
    
End Sub

Sub DefTipoCampos()

    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
End Sub


Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
    End If
End Sub


Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub
Sub FuncaoEstadoAnterior()
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY empresa_id ASC, nome_empr ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN Estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim obTabelaAux As ADODB.recordset
    Dim SQLQuery As String

    Set obTabelaAux = New ADODB.recordset
    Set rs = New ADODB.recordset
    gSQLError = 0
   
    SQLQuery = "SELECT " & ChaveBD & " FROM " & NomeTabela
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenKeyset
    rs.Open SQLQuery, gConexao
    
    If rs.RecordCount = (-1) Then
        BG_Mensagem mediMsgBox, "Erro a verificar utilizador !", vbExclamation, "Inserir"
    Else
        ' In�cio: Determinar Novo C�digo para Utilizador
        obTabelaAux.Open "SELECT MAX(empresa_id) AS maximo FROM " & NomeTabela, gConexao
        If BL_HandleNull(obTabelaAux!maximo, "") = "" Then
            EcCodigo = 1
        Else
            EcCodigo = obTabelaAux!maximo + 1
        End If
        obTabelaAux.Close
        ' Fim: Determinar Novo C�digo para Utilizador
                
        SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
        BG_ExecutaQuery_ADO SQLQuery
        
    End If
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    
    LimpaCampos
    PreencheCampos
    
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
               
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    BG_Mensagem mediMsgBox, "Opera��o efectuada com sucesso.", vbInformation, "Remover"

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub


Private Sub opt_d_Click()

If opt_d.value = True Then
    EcEstado = "N"
End If

End Sub

Private Sub opt_e_click()

If opt_e.value = True Then
    EcEstado = "E"
End If

End Sub

Private Sub opt_s_click()

If opt_s.value = True Then
    EcEstado = "S"
End If


End Sub

VERSION 5.00
Begin VB.Form FormNEtiqConsultas 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormNEtiq"
   ClientHeight    =   4035
   ClientLeft      =   2760
   ClientTop       =   3705
   ClientWidth     =   4710
   Icon            =   "FormNEtiqConsultas.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4035
   ScaleWidth      =   4710
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Caption         =   "Imprimir etiquetas para"
      Height          =   735
      Left            =   120
      TabIndex        =   9
      Top             =   2520
      Width           =   4455
      Begin VB.ComboBox CmbTipoImp 
         DragMode        =   1  'Automatic
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   240
         Width           =   4215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Impressora"
      Height          =   735
      Left            =   120
      TabIndex        =   8
      Top             =   3240
      Width           =   4455
      Begin VB.ComboBox CmbPrinters 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   240
         Width           =   4215
      End
   End
   Begin VB.Frame FrNEtiq 
      Height          =   2415
      Left            =   120
      TabIndex        =   5
      Top             =   0
      Width           =   4455
      Begin VB.CheckBox CkResumo 
         Caption         =   "Imprimir folha resumo da requisi��o"
         Height          =   195
         Left            =   960
         TabIndex        =   12
         Top             =   1560
         Width           =   2895
      End
      Begin VB.TextBox EcCopias 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2520
         TabIndex        =   11
         Tag             =   "131(3,0)"
         Text            =   "1"
         Top             =   840
         Width           =   1095
      End
      Begin VB.TextBox EcAdm 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2520
         MaxLength       =   3
         TabIndex        =   0
         Tag             =   "131(3,0)"
         Text            =   "1"
         Top             =   1200
         Width           =   855
      End
      Begin VB.CommandButton BtCancNetiq 
         Caption         =   "&Fechar"
         Height          =   375
         Left            =   2280
         TabIndex        =   2
         Top             =   1920
         Width           =   1095
      End
      Begin VB.CommandButton BtOkNetiq 
         Caption         =   "&Confimar"
         Height          =   375
         Left            =   960
         TabIndex        =   1
         Top             =   1920
         Width           =   1095
      End
      Begin VB.Label LaCopiasTubos 
         Caption         =   "(Copias de etiquetas para tubos                          )"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   840
         Width           =   3975
      End
      Begin VB.Label Label1 
         Caption         =   "N� de etiquetas administrativas "
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   1200
         Width           =   2415
      End
      Begin VB.Label LaDescr 
         Caption         =   "LaDescr"
         Height          =   495
         Left            =   240
         TabIndex        =   6
         Top             =   240
         Width           =   3975
      End
   End
End
Attribute VB_Name = "FormNEtiqConsultas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 17/04/2002
' T�cnico Paulo Costa

Private Sub BtCancNetiq_Click()
    
    EcAdm = "-1"
    Unload Me

End Sub

Private Sub BtOkNetiq_Click()
    
    Unload Me

End Sub
 
Private Sub CmbTipoImp_Click()
    
    Select Case CmbTipoImp.ListIndex
        Case 0
            'Tubos e Administrativos
            LaDescr.Visible = True
            Label1.Visible = True
            If LaDescr.caption <> "Nenhuma etiqueta para tubos foi gerada." Then
                LaCopiasTubos.Visible = True
                EcCopias.Visible = True
            End If
        Case 1
            'Administrativos
            LaDescr.Visible = False
            LaCopiasTubos.Visible = False
            EcCopias.Visible = False
            Label1.Visible = True
        Case 2
            'Tubos
            LaDescr.Visible = True
            Label1.Visible = False
            If LaDescr.caption <> "Nenhuma etiqueta para tubos foi gerada." Then
                LaCopiasTubos.Visible = True
                EcCopias.Visible = True
            End If
        Case 3
            'Separa��o de Soros
            LaDescr.Visible = True
            Label1.Visible = False
            If LaDescr.caption <> "Nenhuma etiqueta para separa��o de soros foi gerada." Then
                LaCopiasTubos.Visible = True
                EcCopias.Visible = True
            End If
    End Select
      
End Sub

Private Sub EcAdm_GotFocus()
    
    EcAdm.SelStart = 0
    EcAdm.SelLength = Len(EcAdm)

End Sub

Private Sub EcAdm_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, EcAdm

End Sub

Private Sub Form_Activate()
    
    BtOkNetiq.SetFocus

End Sub

Private Sub Form_Load()
    
    Dim Index As Long
    Dim i As Integer
    
    If BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ResumoRequisicoes") = "1" Then
        CkResumo.Visible = True
        If BL_SeleccionaGrupoAnalises = True Then
            CkResumo.Value = 0
        Else
            CkResumo.Value = 0
        End If
    Else
        CkResumo.Visible = False
        CkResumo.Value = 0
    End If
    
    Me.caption = " Etiquetas - Marca��o de Colheitas"
    If CLng(FormGesReqCons.EcEtqNTubos) > 1 Then
        LaDescr.caption = "Foram geradas " & FormGesReqCons.EcEtqNTubos & " etiquetas para tubos"
    ElseIf CLng(FormGesReqCons.EcEtqNTubos) = 1 Then
        LaDescr.caption = "Foi gerada " & FormGesReqCons.EcEtqNTubos & " etiqueta para tubos"
    Else
        LaCopiasTubos.Visible = False
        EcCopias.Visible = False
        LaDescr.caption = "Nenhuma etiqueta para tubos foi gerada"
    End If
    If CLng(FormGesReqCons.EcEtqNEsp) > 1 Then
        If CLng(FormGesReqCons.EcEtqNTubos) > 1 Then
            'LaDescr.Caption = LaDescr.Caption & ", das quais " & FormGesReqCons.EcEtqNEsp & " t�m no c�digo de barras o produto ou a ordem das an�lises."
        End If
    Else
        LaDescr.caption = LaDescr.caption & "."
    End If
    EcAdm = "1"
    
    If Printers.Count = 0 Then
        CmbPrinters.Enabled = False
    Else
        CmbPrinters.Clear
        For i = 0 To Printers.Count - 1
            CmbPrinters.AddItem Printers(i).DeviceName
            If FormGesReqCons.EcPrinterEtiq = Printers(i).DeviceName Then Index = i
        Next i
        
        CmbPrinters.ListIndex = Index
    End If

    CmbTipoImp.Clear
    CmbTipoImp.AddItem "Tubos e fins administrativos"
    CmbTipoImp.AddItem "Fins administrativos"
    CmbTipoImp.AddItem "Tubos"
'    CmbTipoImp.AddItem "Separa��o de Soros"
    If gEtiqTubosMarcacao = 1 Then
        CmbTipoImp.ListIndex = 0
    Else
        CmbTipoImp.ListIndex = 1
    End If

    ' Inicializa com o n�mero de grupos da requisi��o.
    If (gNumEtiqAdmin > 1) Then
        Dim nEtiq As Integer
        nEtiq = REQUISICAO_Conta_Grupos_Req(FormGesReqCons.EcNumReq.text)
        Select Case nEtiq
            Case 0
                EcAdm.text = 1
            Case Else
                If gLAB = "BIO" Then
                    EcAdm.text = gNumEtiqAdmin
                Else
                    EcAdm.text = nEtiq * gNumEtiqAdmin
                End If
        End Select
    Else
        ' Chave n�o definida.
        EcAdm.text = 1
    End If

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    
    If UnloadMode = 0 Then
        EcAdm = "-1"
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    If Trim(EcAdm.text) = "-1" Then
        FormGesReqCons.EcEtqTipo = "-1"
    Else
        If Trim(EcAdm) <> "" Then
            FormGesReqCons.EcEtqNAdm = EcAdm
        Else
            FormGesReqCons.EcEtqNAdm = "0"
        End If
        If Trim(EcCopias) <> "" Then
            FormGesReqCons.EcEtqNCopias = EcCopias
        Else
            FormGesReqCons.EcEtqNCopias = "0"
        End If
        FormGesReqCons.EcEtqTipo = CmbTipoImp.ListIndex
        If CmbPrinters.ListIndex <> -1 Then
            FormGesReqCons.EcPrinterEtiq = CmbPrinters.text
        End If
        FormGesReqCons.EcImprimirResumo = CStr(CkResumo.Value)
    End If
    
End Sub



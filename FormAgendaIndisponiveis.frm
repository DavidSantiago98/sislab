VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormAgendaIndisponiveis 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAgendaIndisponiveis"
   ClientHeight    =   7875
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10350
   Icon            =   "FormAgendaIndisponiveis.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7875
   ScaleWidth      =   10350
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   840
      ScaleHeight     =   315
      ScaleWidth      =   435
      TabIndex        =   11
      Top             =   7080
      Width           =   495
   End
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   120
      TabIndex        =   4
      Top             =   5040
      Width           =   10095
      Begin VB.Label LaCriacao 
         AutoSize        =   -1  'True
         Caption         =   "Cria��o"
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   120
         Width           =   540
      End
      Begin VB.Label LaAlteracao 
         AutoSize        =   -1  'True
         Caption         =   "Altera��o"
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   480
         Width           =   675
      End
      Begin VB.Label LaUtilCri 
         ForeColor       =   &H00808080&
         Height          =   195
         Left            =   4800
         TabIndex        =   8
         Top             =   120
         Width           =   4095
      End
      Begin VB.Label LaUtilAct 
         ForeColor       =   &H00808080&
         Height          =   195
         Left            =   4800
         TabIndex        =   7
         Top             =   480
         Width           =   4095
      End
      Begin VB.Label LaDataAct 
         ForeColor       =   &H00808080&
         Height          =   195
         Left            =   2040
         TabIndex        =   6
         Top             =   480
         Width           =   1530
      End
      Begin VB.Label LaDataCri 
         ForeColor       =   &H00808080&
         Height          =   195
         Left            =   2040
         TabIndex        =   5
         Top             =   120
         Width           =   1530
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   240
      Top             =   7080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAgendaIndisponiveis.frx":000C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAgendaIndisponiveis.frx":03A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormAgendaIndisponiveis.frx":07CF
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView EcLista 
      Height          =   4815
      Left            =   3000
      TabIndex        =   3
      Top             =   120
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   8493
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   0
   End
   Begin VB.Frame Frame1 
      Height          =   4935
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   2775
      Begin VB.CheckBox CkFeriado 
         Caption         =   "Feriado"
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   4560
         Width           =   2475
      End
      Begin MSComCtl2.MonthView EcCalendario 
         Height          =   2310
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   2460
         _ExtentX        =   4339
         _ExtentY        =   4075
         _Version        =   393216
         ForeColor       =   -2147483630
         BackColor       =   16777215
         BorderStyle     =   1
         Appearance      =   0
         MonthBackColor  =   16777215
         StartOfWeek     =   147521538
         TitleBackColor  =   16574424
         TrailingForeColor=   16574424
         CurrentDate     =   39477
      End
      Begin VB.TextBox EcNota 
         Appearance      =   0  'Flat
         ForeColor       =   &H00000000&
         Height          =   1815
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   2
         Top             =   2640
         Width           =   2460
      End
   End
End
Attribute VB_Name = "FormAgendaIndisponiveis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Const cDiaNormal = "N"
Const cDiaFeriado = "F"
Const cDiaSabado = "S"
Const cDiaDomingo = "D"

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Private Type Dias
    dia As Date
    user_cri As String
    dt_cri As String
    hr_cri As String
    user_act As String
    dt_act As String
    hr_act As String
    nota As String
    t_dia As String
End Type

Dim estrutDias() As Dias
Dim TotalEstrutDias As Long

Dim diaInicial As Date
Dim diaFinal As Date
Dim mesActual As Long
Dim anoActual As Long
Public rs As ADODB.recordset

Private Sub EcCalendario_GotFocus()
    MarcaBold
End Sub

Private Sub EcLista_KeyDown(KeyCode As Integer, Shift As Integer)

    Dim vItem As Variant
    Dim cItems As Collection
        
    If (KeyCode <> vbKeyDelete) Then: Exit Sub
    Set cItems = New Collection
    For Each vItem In EcLista.ListItems
        If (vItem.Checked) Then: cItems.Add vItem.Index
    Next
    For Each vItem In cItems
        EcLista.ListItems.Remove (CStr(estrutDias(vItem).dia))
        estrutDias(vItem).dia = Empty
        estrutDias(vItem).dt_act = Empty
        estrutDias(vItem).dt_cri = Empty
        estrutDias(vItem).hr_cri = Empty
        estrutDias(vItem).hr_act = Empty
        estrutDias(vItem).nota = Empty
        estrutDias(vItem).user_act = Empty
        estrutDias(vItem).user_cri = Empty
        estrutDias(vItem).t_dia = Empty
    Next
    ReordenaEstrutura
End Sub

Private Sub EcLista_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    
    On Error GoTo ErrorHandler
    
    ' Commence sorting
    With EcLista
    
        ' Display the hourglass cursor whilst sorting
        
        Dim lngCursor As Long
        lngCursor = .MousePointer
        .MousePointer = vbHourglass
        
        ' Prevent the ListView control from updating on screen -
        ' this is to hide the changes being made to the listitems
        ' and also to speed up the sort
        
        LockWindowUpdate .hwnd
        
        ' Check the data type of the column being sorted,
        ' and act accordingly
        
        Dim l As Long
        Dim strFormat As String
        Dim strData() As String
        
        Dim lngIndex As Long
        lngIndex = ColumnHeader.Index - 1
    
        Select Case UCase$(ColumnHeader.Tag)
        Case "DATA"
        
            ' Sort by date.
            
            strFormat = "YYYYMMDDHhNnSs"
        
            ' Loop through the values in this column. Re-format
            ' the dates so as they can be sorted alphabetically,
            ' having already stored their visible values in the
            ' tag, along with the tag's original value
        
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsDate(.Text) Then
                                .Text = Format(CDate(.Text), _
                                                    strFormat)
                            Else
                                .Text = ""
                            End If
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsDate(.Text) Then
                                .Text = Format(CDate(.Text), _
                                                    strFormat)
                            Else
                                .Text = ""
                            End If
                        End With
                    Next l
                End If
            End With
            
            ' Sort the list alphabetically by this column
            
            .sortOrder = (.sortOrder + 1) Mod 2
            .SortKey = ColumnHeader.Index - 1
            .Sorted = True
            
            ' Restore the previous values to the 'cells' in this
            ' column of the list from the tags, and also restore
            ' the tags to their original values
            
            With .ListItems
                If (lngIndex > 0) Then
                    For l = 1 To .Count
                        With .item(l).ListSubItems(lngIndex)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                Else
                    For l = 1 To .Count
                        With .item(l)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next l
                End If
            End With
            
            ReordenaEstrutura
             
        Case Else
            
        End Select

        ' Unlock the list window so that the OCX can update it
        LockWindowUpdate 0&
        ' Restore the previous cursor
        .MousePointer = lngCursor
        
            
    End With
    Exit Sub
    
ErrorHandler:
    
    LockWindowUpdate 0&
    EcLista.MousePointer = lngCursor
    
End Sub

Private Sub EcCalendario_DateDblClick(ByVal DateDblClicked As Date)
    Dim i As Integer
        For i = 1 To TotalEstrutDias
            If estrutDias(i).dia = DateDblClicked Then
                Exit Sub
            End If
        Next
        If (VerificaMarcacoes(CStr(DateDblClicked))) Then
            If (BG_Mensagem(mediMsgBox, "Existem marca��es para esta data! Deseja imprimir as marca��es?", vbExclamation + vbYesNo, " Aten��o") = vbYes) Then
                ImprimeMarcacoes CStr(DateDblClicked)
            End If
            Exit Sub
        End If
        
        TotalEstrutDias = TotalEstrutDias + 1
        ReDim Preserve estrutDias(TotalEstrutDias)
        estrutDias(TotalEstrutDias).dia = DateDblClicked
        estrutDias(TotalEstrutDias).user_cri = gCodUtilizador
        estrutDias(TotalEstrutDias).dt_cri = Bg_DaData_ADO
        estrutDias(TotalEstrutDias).hr_cri = Bg_DaHora_ADO
        estrutDias(TotalEstrutDias).nota = EcNota.Text
        estrutDias(TotalEstrutDias).t_dia = IIf(CkFeriado.value = vbChecked, cDiaFeriado, cDiaNormal)
        
        With EcLista.ListItems.Add(, CStr(DateDblClicked), DateDblClicked, , IIf(estrutDias(TotalEstrutDias).t_dia = cDiaFeriado, 2, 3))
            .ListSubItems.Add , , EcNota.Text
            .Selected = True
            .EnsureVisible
            EcLista_ItemClick EcLista.SelectedItem
        End With
        EcCalendario.SetFocus
End Sub

Private Sub EcCalendario_SelChange(ByVal StartDate As Date, ByVal EndDate As Date, Cancel As Boolean)
    
    If (mesActual <> EcCalendario.Month And Not Cancel) Then
        LimpaCampos
        diaInicial = CDate("01-" & EcCalendario.Month & "-" & EcCalendario.Year)
        If EcCalendario.Month = mvwJanuary Or EcCalendario.Month = mvwMarch Or EcCalendario.Month = mvwMay Or EcCalendario.Month = mvwJuly Or EcCalendario.Month = mvwAugust Or EcCalendario.Month = mvwOctober Or EcCalendario.Month = mvwDecember Then
            diaFinal = CDate("31-" & EcCalendario.Month & "-" & EcCalendario.Year)
        ElseIf EcCalendario.Month = mvwFebruary Then
            If Month(DateSerial(EcCalendario.Year, 2, 29)) = 2 Then
                diaFinal = CDate("29-02-" & EcCalendario.Year)
            Else
                diaFinal = CDate("28-02-" & EcCalendario.Year)
            End If
        Else
            diaFinal = CDate("30-" & EcCalendario.Month & "-" & EcCalendario.Year)
        End If
        mesActual = EcCalendario.Month
        anoActual = EcCalendario.Year
        FuncaoProcurar
    End If
    
End Sub

Private Sub EcLista_ItemClick(ByVal item As MSComctlLib.ListItem)
    
    If (item Is Nothing Or gFormActivo.Name <> Me.Name) Then: Exit Sub
    LaUtilCri.caption = BL_SelNomeUtil(estrutDias(item.Index).user_cri)
    LaDataCri.caption = estrutDias(item.Index).dt_cri & " " & estrutDias(item.Index).hr_cri
    LaUtilAct.caption = BL_SelNomeUtil(estrutDias(item.Index).user_act)
    LaDataAct.caption = estrutDias(item.Index).dt_act & " " & estrutDias(item.Index).hr_act
    EcNota.Text = estrutDias(item.Index).nota
    EcCalendario.value = estrutDias(item.Index).dia
    EcLista.SetFocus
    item.Selected = True
    item.EnsureVisible
   
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    IniciaLista
    
    Set CampoActivo = Me.ActiveControl
    
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    GeraFeriados
    
    estado = 1
    BG_StackJanelas_Push Me
    
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Agenda - Dias Indispon�veis"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 10440
    Me.Height = 6375 ' Normal
    Set CampoDeFocus = EcCalendario
    EcCalendario.MultiSelect = True
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
        
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Set FormAgendaIndisponiveis = Nothing
    
End Sub

Sub LimpaCampos()
    
    TotalEstrutDias = 0
    ReDim estrutDias(0)
    EcNota.Text = Empty
    EcLista.ListItems.Clear
    LaUtilCri.caption = Empty
    LaDataCri.caption = Empty
    LaUtilAct.caption = Empty
    LaDataAct.caption = Empty
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        'CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
'        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    EcCalendario.value = Bg_DaData_ADO
    EcCalendario_SelChange Bg_DaData_ADO, Bg_DaData_ADO, True
End Sub

Sub FuncaoProcurar()
    Dim sSql As String
    Dim rs As New ADODB.recordset
    Dim lngCursor As Long
        
    lngCursor = EcLista.MousePointer
    EcLista.MousePointer = vbHourglass
    LockWindowUpdate EcLista.hwnd
        
    sSql = "SELECT * FROM sl_agenda_indisponiveis WHERE dia_indisp between " & BL_TrataDataParaBD(CStr(diaInicial)) & _
           " AND " & BL_TrataDataParaBD(CStr(diaFinal)) & " order by dia_indisp"
    
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        'BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        While Not rs.EOF
            TotalEstrutDias = TotalEstrutDias + 1
            ReDim Preserve estrutDias(TotalEstrutDias)
            estrutDias(TotalEstrutDias).dia = rs!dia_indisp
            estrutDias(TotalEstrutDias).dt_cri = rs!dt_cri
            estrutDias(TotalEstrutDias).user_cri = rs!user_cri
            estrutDias(TotalEstrutDias).hr_cri = rs!hr_cri
            estrutDias(TotalEstrutDias).dt_act = BL_HandleNull(rs!dt_act, "")
            estrutDias(TotalEstrutDias).user_act = BL_HandleNull(rs!user_act, "")
            estrutDias(TotalEstrutDias).hr_act = BL_HandleNull(rs!hr_act, "")
            estrutDias(TotalEstrutDias).nota = BL_HandleNull(rs!nota, Empty)
            estrutDias(TotalEstrutDias).t_dia = BL_HandleNull(rs!t_dia, cDiaNormal)
            EcLista.ListItems.Add(, CStr(estrutDias(TotalEstrutDias).dia), estrutDias(TotalEstrutDias).dia, , IIf(estrutDias(TotalEstrutDias).t_dia = cDiaFeriado, 2, 3)).ListSubItems.Add , , estrutDias(TotalEstrutDias).nota
            rs.MoveNext
        Wend
        
        BL_FimProcessamento Me
    End If
    rs.Close
    Set rs = Nothing
    If (EcLista.ListItems.Count) Then: EcLista_ItemClick EcLista.ListItems(1)
    
    LockWindowUpdate 0&
    EcLista.MousePointer = lngCursor
        
End Sub


Public Sub FuncaoInserir()
    gMsgMsg = "Tem a certza que deseja inserir as datas seleccionadas?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        
    If gMsgResp = vbYes Then
        BD_Insert
    End If
End Sub

Private Sub BD_Insert(Optional bModify As Boolean)
    Dim sSql As String
    Dim i As Integer
    
    For i = 1 To TotalEstrutDias
        If (estrutDias(i).dia <> Empty) Then
            If (Not bModify) Then
                estrutDias(i).dt_cri = Bg_DaData_ADO
                estrutDias(i).user_cri = gCodUtilizador
                estrutDias(i).hr_cri = Bg_DaHora_ADO
            Else
                estrutDias(i).dt_act = Bg_DaData_ADO
                estrutDias(i).user_act = gCodUtilizador
                estrutDias(i).hr_act = Bg_DaHora_ADO
            End If
            sSql = "INSERT INTO sl_agenda_indisponiveis (dia_indisp,user_cri,dt_cri,hr_cri,user_act,dt_act,hr_act,nota,t_dia) VALUES( "
            sSql = sSql & BL_TrataDataParaBD(CStr(estrutDias(i).dia)) & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutDias(i).user_cri) & ", "
            sSql = sSql & BL_TrataDataParaBD(estrutDias(i).dt_cri) & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutDias(i).hr_cri) & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutDias(i).user_act) & ", "
            sSql = sSql & BL_TrataDataParaBD(estrutDias(i).dt_act) & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutDias(i).hr_act) & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutDias(i).nota) & ", "
            sSql = sSql & BL_TrataStringParaBD(estrutDias(i).t_dia) & ") "
            BG_ExecutaQuery_ADO sSql
            BG_Trata_BDErro
        End If
    Next
End Sub

Public Sub FuncaoModificar()
    Dim sSql As String
    gMsgMsg = "Tem a certeza que deseja inserir as datas seleccionadas?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        
    If gMsgResp = vbYes Then
        sSql = "DELETE FROM sl_agenda_indisponiveis WHERE dia_indisp between " & BL_TrataDataParaBD(CStr(diaInicial)) & " AND " & BL_TrataDataParaBD(CStr(diaFinal))
        BG_ExecutaQuery_ADO sSql
        BG_Trata_BDErro
        
        BD_Insert True
    End If
End Sub

Private Sub IniciaLista()
   
    With EcLista
        .ColumnHeaders.Add(, , "Data", 1379, lvwColumnLeft).Tag = "DATA"
        .ColumnHeaders.Add(, , "Nota", 5550, lvwColumnLeft).Tag = "NOTA"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .SmallIcons = ImageList1
        .AllowColumnReorder = False
        .FullRowSelect = True
        .Checkboxes = True
        .MultiSelect = False
    End With
    SetListViewColor EcLista, Picture1, RGB(216, 231, 252), vbWhite
 
End Sub

Public Sub SetListViewColor(�lvCtrlListView� As ListView, �pCtrlPictureBox� As PictureBox, �lColorOne� As Long, Optional �lColorTwo� As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo ErrorHandler
    lColor1 = �lColorOne�
    lColor2 = �lColorTwo�
    �lvCtrlListView�.Picture = LoadPicture("")
    �lvCtrlListView�.Refresh
    �pCtrlPictureBox�.Cls
    �pCtrlPictureBox�.AutoRedraw = True
    �pCtrlPictureBox�.BorderStyle = vbBSNone
    �pCtrlPictureBox�.ScaleMode = vbTwips
    �pCtrlPictureBox�.Visible = False
    �lvCtrlListView�.PictureAlignment = lvwTile
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    �pCtrlPictureBox�.top = �lvCtrlListView�.top
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    With �pCtrlPictureBox�.Font
        .Size = �lvCtrlListView�.Font.Size + 2
        .Bold = �lvCtrlListView�.Font.Bold
        .Charset = �lvCtrlListView�.Font.Charset
        .Italic = �lvCtrlListView�.Font.Italic
        .Name = �lvCtrlListView�.Font.Name
        .Strikethrough = �lvCtrlListView�.Font.Strikethrough
        .Underline = �lvCtrlListView�.Font.Underline
        .Weight = �lvCtrlListView�.Font.Weight
    End With
    �pCtrlPictureBox�.Refresh
    lLineHeight = �pCtrlPictureBox�.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = �lvCtrlListView�.Width
    �pCtrlPictureBox�.Height = lBarHeight * 2
    �pCtrlPictureBox�.Width = lBarWidth
    �pCtrlPictureBox�.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    �pCtrlPictureBox�.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    �pCtrlPictureBox�.AutoSize = True
    �lvCtrlListView�.Picture = �pCtrlPictureBox�.Image
    �lvCtrlListView�.Refresh
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub ReordenaEstrutura()
    
    Dim vItem As Variant
    Dim i As Integer
    Dim dia As Dias
    Dim estrutDiasCopy() As Dias
    
    ReDim estrutDiasCopy(TotalEstrutDias)
    estrutDiasCopy = estrutDias
    ReDim estrutDias(0)
    ReDim Preserve estrutDias(EcLista.ListItems.Count)
    For Each vItem In EcLista.ListItems
        For i = 1 To TotalEstrutDias
            If (vItem.Text = estrutDiasCopy(i).dia) Then
                estrutDias(vItem.Index) = estrutDiasCopy(i)
                Exit For
            End If
        Next
    Next
    TotalEstrutDias = EcLista.ListItems.Count
End Sub

Private Sub MarcaBold()

    Dim i As Integer
 
    If (gFormActivo.Name <> Me.Name) Then: Exit Sub
    For i = 1 To TotalEstrutDias
        If (Year(estrutDias(i).dia) = Year(EcCalendario.SelEnd) And _
            Month(estrutDias(i).dia) = Month(EcCalendario.SelEnd)) Then
            If (estrutDias(i).t_dia = cDiaFeriado) Then: EcCalendario.DayBold(estrutDias(i).dia) = True
        End If
    Next

End Sub

Private Function VerificaMarcacoes(data As String) As Boolean
    
    Dim sql As String
    Dim rsReq As ADODB.recordset

    sql = "select * from sl_requis_consultas where dt_previ = " & BL_TrataDataParaBD(data)
    Set rsReq = New ADODB.recordset
    rsReq.CursorType = adOpenStatic
    rsReq.CursorLocation = adUseServer
    rsReq.Open sql, gConexao
    VerificaMarcacoes = CBool(rsReq.RecordCount > 0)
    If (rsReq.state = adStateOpen) Then: rsReq.Close
    Exit Function
    
End Function

Private Sub ImprimeMarcacoes(data As String)
    
    Dim sql As String
    Dim Report As CrystalReport
    Dim rsReq As ADODB.recordset
        
    sql = "SELECT distinct * FROM sl_requis_consultas,sl_identif Where sl_requis_consultas.dt_previ = " & BL_TrataDataParaBD(data) & _
          " AND sl_requis_consultas.seq_utente = sl_identif.seq_utente"
    Set rsReq = New ADODB.recordset
    rsReq.CursorType = adOpenStatic
    rsReq.CursorLocation = adUseServer
    rsReq.Open sql, gConexao
    While (Not rsReq.EOF)
        BG_ExecutaQuery_ADO "insert into sl_cr_agenda_req_marcadas (n_req,utente,nome,data,telefone) values (" & _
                            BL_TrataStringParaBD(BL_HandleNull(rsReq!n_req, "")) & "," & _
                            BL_TrataStringParaBD(BL_HandleNull(rsReq!Utente, "")) & "," & _
                            BL_TrataStringParaBD(BL_HandleNull(rsReq!nome_ute, "")) & "," & _
                            BL_TrataStringParaBD(BL_HandleNull(rsReq!dt_nasc_ute, "")) & "," & _
                            BL_TrataStringParaBD(BL_HandleNull(rsReq!telemovel, BL_HandleNull(rsReq!telef_ute, ""))) & ")"
        BG_Trata_BDErro
        rsReq.MoveNext
    Wend
    If (rsReq.state = adStateOpen) Then: rsReq.Close
    If (Not BL_IniciaReport("ReqMarcadas", " Requisi��es Marcadas ", crptToWindow, True, True)) Then: Exit Sub
    Set Report = forms(0).Controls("Report")
    Report.formulas(0) = "USER=" & BL_TrataStringParaBD("" & BL_SelNomeUtil(CStr(gCodUtilizador)))
    Report.formulas(1) = "DATA=" & BL_TrataDataParaBD("" & data)
    Report.Connect = "DSN=" & gDSN
    Call BL_ExecutaReport
    BG_ExecutaQuery_ADO "delete from sl_cr_agenda_req_marcadas"
    BG_Trata_BDErro
    Exit Sub

End Sub

Private Sub GeraFeriados()

    Dim i As Integer
    Dim sql As String
    Dim ano As String
    Dim dia(10) As String
    Dim Feriado(10) As String
    Dim RsFeriado As ADODB.recordset
    
    On Error GoTo TrataErro
    ano = Year(Bg_DaData_ADO)
    dia(0) = "1-JAN-" & ano
    dia(1) = "25-ABR-" & ano
    dia(2) = "1-MAI-" & ano
    dia(3) = "22-MAI-" & ano
    dia(4) = "10-JUN-" & ano
    dia(5) = "15-AGO-" & ano
    dia(6) = "5-OUT-" & ano
    dia(7) = "1-NOV-" & ano
    dia(8) = "1-DEZ-" & ano
    dia(9) = "8-DEZ-" & ano
    dia(10) = "25-DEZ-" & ano
    
    Feriado(0) = "Feriado Nacional (Dia de Ano Novo)"
    Feriado(1) = "Feriado Nacional (Dia da Liberdade)"
    Feriado(2) = "Feriado Nacional (Dia do Trabalhador)"
    Feriado(3) = "Feriado Nacional (Dia do Corpo de Cristo)"
    Feriado(4) = "Feriado Nacional (Dia de Cam�es)"
    Feriado(5) = "Feriado Nacional (Dia da Assun��o)"
    Feriado(6) = "Feriado Nacional (Dia da R�publica)"
    Feriado(7) = "Feriado Nacional (Dia de Todos os Santos)"
    Feriado(8) = "Feriado Nacional (Dia da Independ�ncia)"
    Feriado(9) = "Feriado Nacional (Dia da Imaculada Concei��o)"
    Feriado(10) = "Feriado Nacional (Dia de Natal)"
        
    Set RsFeriado = New ADODB.recordset
    For i = 0 To UBound(dia)
        RsFeriado.CursorType = adOpenStatic
        RsFeriado.CursorLocation = adUseServer
        sql = "SELECT dia_indisp FROM sl_agenda_indisponiveis WHERE dia_indisp = " & BL_TrataDataParaBD(dia(i))
        RsFeriado.Open sql, gConexao
        If (RsFeriado.RecordCount = 0) Then
            sql = "INSERT INTO sl_agenda_indisponiveis (dia_indisp, user_cri, dt_cri, hr_cri,nota, user_act, dt_act, hr_act, t_dia) VALUES (" & _
                  BL_TrataDataParaBD(dia(i)) & "," & _
                  BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & _
                  BL_TrataDataParaBD(Bg_DaData_ADO) & "," & _
                  BL_TrataStringParaBD(Bg_DaHora_ADO) & "," & _
                  BL_TrataStringParaBD(Feriado(i)) & ",NULL,NULL,NULL,'F')"
            BG_ExecutaQuery_ADO sql
        End If
        If (RsFeriado.state = adStateOpen) Then: RsFeriado.Close
    Next
    Exit Sub
    
TrataErro:

End Sub

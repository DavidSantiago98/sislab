VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormCQlotes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCQlotes"
   ClientHeight    =   8175
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   11910
   Icon            =   "FormCQlotes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8175
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CbNivel 
      Height          =   315
      Left            =   9360
      TabIndex        =   33
      Text            =   "Combo1"
      Top             =   720
      Width           =   1095
   End
   Begin VB.TextBox ecSeqcontrolo 
      Height          =   285
      Left            =   7800
      TabIndex        =   31
      Top             =   7560
      Width           =   855
   End
   Begin VB.TextBox EcActivo 
      Height          =   285
      Left            =   6480
      TabIndex        =   30
      Top             =   7560
      Width           =   855
   End
   Begin VB.TextBox EcControlo 
      Height          =   285
      Left            =   5520
      TabIndex        =   29
      Top             =   7560
      Width           =   735
   End
   Begin VB.CommandButton BtAnalises 
      Caption         =   "An�lises"
      Height          =   735
      Left            =   10200
      Picture         =   "FormCQlotes.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   28
      ToolTipText     =   "An�lises Associadas"
      Top             =   6360
      Width           =   1455
   End
   Begin VB.PictureBox PictureListColor 
      Height          =   495
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   555
      TabIndex        =   27
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox EcUserAct 
      Height          =   285
      Left            =   3960
      TabIndex        =   26
      Top             =   7560
      Width           =   1335
   End
   Begin VB.TextBox EcUserCri 
      Height          =   285
      Left            =   2160
      TabIndex        =   25
      Top             =   7560
      Width           =   1335
   End
   Begin VB.TextBox EcSeqLote 
      Height          =   285
      Left            =   1080
      TabIndex        =   24
      Top             =   7440
      Width           =   975
   End
   Begin VB.Frame Frame2 
      Height          =   945
      Left            =   0
      TabIndex        =   15
      Top             =   5280
      Width           =   11685
      Begin VB.TextBox EcUserCriNome 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   21
         Top             =   195
         Width           =   2055
      End
      Begin VB.TextBox EcDtCri 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   3840
         Locked          =   -1  'True
         TabIndex        =   20
         Top             =   195
         Width           =   1215
      End
      Begin VB.TextBox EcHrCri 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   5520
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   195
         Width           =   735
      End
      Begin VB.TextBox EcUserActNome 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   600
         Width           =   2055
      End
      Begin VB.TextBox EcDtAct 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   3840
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox EcHrAct 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   5520
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   195
         Width           =   675
      End
      Begin VB.Label Label6 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   600
         Width           =   705
      End
   End
   Begin MSComctlLib.ListView LvLotes 
      Height          =   3495
      Left            =   120
      TabIndex        =   14
      Top             =   1680
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   6165
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.TextBox EcDescrLote 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2640
      TabIndex        =   2
      Top             =   120
      Width           =   3495
   End
   Begin VB.TextBox EcCodLote 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   960
      TabIndex        =   1
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox EcDtValidade 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7440
      TabIndex        =   6
      Top             =   720
      Width           =   1095
   End
   Begin VB.TextBox EcDtFim 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9360
      TabIndex        =   4
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox EcDtIni 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7440
      TabIndex        =   3
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox EcCodControlo 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   960
      TabIndex        =   5
      Top             =   720
      Width           =   735
   End
   Begin VB.TextBox EcDescrControlo 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   720
      Width           =   4455
   End
   Begin VB.CommandButton BtPesquisaControlo 
      Height          =   315
      Left            =   6120
      Picture         =   "FormCQlotes.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
      Top             =   720
      Width           =   375
   End
   Begin VB.Label Label1 
      Caption         =   "N�vel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   8640
      TabIndex        =   34
      Top             =   720
      Width           =   795
   End
   Begin VB.Label LbActivo 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   960
      TabIndex        =   32
      Top             =   1200
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Descri��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   1800
      TabIndex        =   13
      Top             =   120
      Width           =   795
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Width           =   795
   End
   Begin VB.Label Label1 
      Caption         =   "Validade"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   6600
      TabIndex        =   11
      Top             =   720
      Width           =   795
   End
   Begin VB.Label Label1 
      Caption         =   "Data Fim"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   8640
      TabIndex        =   10
      Top             =   120
      Width           =   795
   End
   Begin VB.Label Label1 
      Caption         =   "Data Inicio"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   6600
      TabIndex        =   9
      Top             =   120
      Width           =   795
   End
   Begin VB.Label Label1 
      Caption         =   "Controlo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   8
      Top             =   720
      Width           =   795
   End
End
Attribute VB_Name = "FormCQlotes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rsLote As ADODB.recordset
' Constant to define listview text color.
Const cLightGray = &H808080


Const cLightYellow = &H80000018
Const cWhite = &HFFFFFF
Const cLightBlue = &HFCE7D8

Private Type lote
    seq_lote As Long
    cod_lote As String
    descr_lote As String
    cod_controlo As String
    descr_controlo As String
    dt_ini As String
    dt_fim As String
    dt_validade As String
    flg_activo As Integer
    user_cri As String
    dt_cri As String
    hr_cri As String
    user_act As String
    dt_act As String
    hr_act As String
    cod_nivel As Integer
    descr_nivel As String
End Type
Dim estrutLote() As lote
Dim totalLote As Integer


Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub
Sub EventoUnload()
    BG_StackJanelas_Pop
    BL_ToolbarEstadoN 0
    
    If Not rsLote Is Nothing Then
        rsLote.Close
        Set rsLote = Nothing
    End If
    If EcControlo <> "" Then
        Set gFormActivo = FormCQcontrolos
    Else
        Set gFormActivo = MDIFormInicio
    End If
    Set FormCQlotes = Nothing
End Sub

Sub Inicializacoes()

    Me.caption = "Lotes"
    Me.left = 5
    Me.top = 5
    Me.Width = 12000
    Me.Height = 7485 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "SL_CQ_LOTE"
    Set CampoDeFocus = EcCodLote
    
    NumCampos = 15
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_lote"
    CamposBD(1) = "cod_lote"
    CamposBD(2) = "descr_lote"
    CamposBD(3) = "cod_controlo"
    CamposBD(4) = "dt_ini"
    CamposBD(5) = "dt_fim"
    CamposBD(6) = "dt_validade"
    CamposBD(7) = "flg_activo"
    CamposBD(8) = "user_cri"
    CamposBD(9) = "dt_cri"
    CamposBD(10) = "hr_cri"
    CamposBD(11) = "user_act"
    CamposBD(12) = "dt_act"
    CamposBD(13) = "hr_act"
    CamposBD(14) = "cod_nivel"
    
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcSeqLote
    Set CamposEc(1) = EcCodLote
    Set CamposEc(2) = EcDescrLote
    Set CamposEc(3) = EcCodControlo
    Set CamposEc(4) = EcDtIni
    Set CamposEc(5) = EcDtFim
    Set CamposEc(6) = EcDtValidade
    Set CamposEc(7) = EcActivo
    Set CamposEc(8) = EcUserCri
    Set CamposEc(9) = EcDtCri
    Set CamposEc(10) = EcHrCri
    Set CamposEc(11) = EcUserAct
    Set CamposEc(12) = EcDtAct
    Set CamposEc(13) = EcHrAct
    Set CamposEc(14) = CbNivel
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "C�digo do Lote"
    TextoCamposObrigatorios(2) = "Descr��o do Lote"
    TextoCamposObrigatorios(3) = "C�digo do Controlo"
    TextoCamposObrigatorios(4) = "Data de Inicio"
    TextoCamposObrigatorios(6) = "Data de Validade"
    TextoCamposObrigatorios(7) = "Activo?"
    TextoCamposObrigatorios(14) = "N�vel"
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_lote"
    Set ChaveEc = EcSeqLote
        
End Sub
Sub DefTipoCampos()
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDtCri, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDtAct, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_cri", EcHrCri, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_act", EcHrAct, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_validade", EcDtValidade, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_ini", EcDtIni, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_fim", EcDtFim, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    InicializaTreeView
    
End Sub
Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_tbf_nivel", "cod_nivel", "descr_nivel", CbNivel
End Sub


Private Sub BtAnalises_Click()
    If EcCodLote <> "" And EcSeqLote <> "" Then
        FormCQAnalises.Show
        FormCQAnalises.EcCodLote = EcCodLote
        FormCQAnalises.EcCodana_Validate False
        'FormCQlotes.FuncaoProcurar
        FormCQAnalises.EcLote = "1"
        Set gFormActivo = FormCQAnalises
    End If

End Sub

Private Sub BtPesquisaControlo_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "SL_CQ_CONTROLO", _
                        "COD_CONTROLO", "SEQ_controlo", _
                        ecSeqcontrolo, EcCodControlo
    
    EcCodControlo_Validate False
End Sub

Private Sub eccodlote_Validate(Cancel As Boolean)
    EcCodLote = UCase(EcCodLote)
End Sub



Private Sub Form_Activate()
    EventoActivate
End Sub

Private Sub Form_Load()
    EventoLoad
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub
Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rsLote Is Nothing Then
            rsLote.Close
            Set rsLote = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Sub LimpaCampos()
    Me.SetFocus
    LbActivo.caption = ""
    EcCodLote.Locked = False
    BG_LimpaCampo_Todos CamposEc
    EcUserActNome = ""
    EcUserCriNome = ""
    LvLotes.ListItems.Clear
    totalLote = 0
    ReDim estrutLote(0)
    EcDescrControlo = ""
End Sub
Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUserCri = gCodUtilizador
        EcDtCri = Bg_DaData_ADO
        EcHrCri = Bg_DaHora_ADO
        If EcDtFim <> "" Then
            EcActivo = "0"
        Else
            EcActivo = "1"
        End If
        
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcUserAct = gCodUtilizador
        EcDtAct = Bg_DaData_ADO
        EcHrAct = Bg_DaHora_ADO
        If EcDtFim <> "" Then
            EcActivo = "0"
        Else
            EcActivo = "1"
        End If
        
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(ChaveEc)
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    rsLote.Requery
    PreencheCampos
End Sub


Sub BD_Insert()
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcSeqLote = BG_DaMAX(NomeTabela, "seq_lote") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoRemover()
    
End Sub

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rsLote = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY dt_ini DESC, dt_fim DESC "
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rsLote.CursorType = adOpenStatic
    rsLote.CursorLocation = adUseServer
    
    rsLote.Open CriterioTabela, gConexao
    
    If rsLote.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rsLote.RecordCount = 0 Then
        FuncaoLimpar
    Else
        PreencheLV
        EcCodControlo_Validate False
        lvlotes_Click
        EcCodLote.Locked = True
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rsLote.MovePrevious
    
    If rsLote.BOF Then
        rsLote.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
End Sub
Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rsLote.MoveNext
    
    If rsLote.EOF Then
        rsLote.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        BL_FimProcessamento Me
        PreencheCampos
    End If
End Sub


Private Sub InicializaTreeView()
    With LvLotes
        .ColumnHeaders.Add(, , "C�digo", 1000, lvwColumnLeft).Key = "CODIGO"
        .ColumnHeaders.Add(, , "Descri��o", 3000, lvwColumnCenter).Key = "DESCRICAO"
        .ColumnHeaders.Add(, , "N�vel", 800, lvwColumnCenter).Key = "NIVEL"
        .ColumnHeaders.Add(, , "Controlo", 2200, lvwColumnCenter).Key = "CONTROLO"
        .ColumnHeaders.Add(, , "Dt.Inicio", 1500, lvwColumnCenter).Key = "DT_INI"
        .ColumnHeaders.Add(, , "Dt.Fim", 1500, lvwColumnCenter).Key = "DT_FIM"
        .ColumnHeaders.Add(, , "Dt.Validade", 1500, lvwColumnCenter).Key = "DT_VALIDADE"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .Checkboxes = True
        .FullRowSelect = True
        .ForeColor = cLightGray
        .MultiSelect = False
    End With
    ColocaCores LvLotes, PictureListColor, cLightYellow, cWhite
    Exit Sub

End Sub

Public Sub ColocaCores(lv As ListView, pb As PictureBox, cor1 As Long, Optional cor2 As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo TrataErro
    
    lColor1 = cor1
    lColor2 = cor2
    lv.Picture = LoadPicture("")
    lv.Refresh
    pb.Cls
    pb.AutoRedraw = True
    pb.BorderStyle = vbBSNone
    pb.ScaleMode = vbTwips
    pb.Visible = False
    lv.PictureAlignment = lvwTile
    pb.Font = lv.Font
    pb.top = lv.top
    pb.Font = lv.Font
    With pb.Font
        .Size = lv.Font.Size + 2
        .Bold = lv.Font.Bold
        .Charset = lv.Font.Charset
        .Italic = lv.Font.Italic
        .Name = lv.Font.Name
        .Strikethrough = lv.Font.Strikethrough
        .Underline = lv.Font.Underline
        .Weight = lv.Font.Weight
    End With
    pb.Refresh
    lLineHeight = pb.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = lv.Width
    pb.Height = lBarHeight * 2
    pb.Width = lBarWidth
    pb.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    pb.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    pb.AutoSize = True
    lv.Picture = pb.Image
    lv.Refresh
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "Erro: " & Err.Description, Me.Name, "ColocaCores", False
    Exit Sub
End Sub



Private Sub PreencheLV()
    Dim i As Integer
    LvLotes.ListItems.Clear
    totalLote = 0
    ReDim estrutLote(0)
    While Not rsLote.EOF
        totalLote = totalLote + 1
        ReDim Preserve estrutLote(totalLote)
        
        estrutLote(totalLote).seq_lote = BL_HandleNull(rsLote!seq_lote, -1)
        estrutLote(totalLote).cod_lote = BL_HandleNull(rsLote!cod_lote, "")
        estrutLote(totalLote).descr_lote = BL_HandleNull(rsLote!descr_lote, "")
        
        estrutLote(totalLote).cod_controlo = BL_HandleNull(rsLote!cod_controlo, "")
        estrutLote(totalLote).descr_controlo = BL_SelCodigo("SL_CQ_CONTROLO", "DESCR_CONTROLO", "COD_CONTROLO", BL_HandleNull(rsLote!cod_controlo, ""), "V")
        estrutLote(totalLote).dt_ini = BL_HandleNull(rsLote!dt_ini, "")
        estrutLote(totalLote).dt_fim = BL_HandleNull(rsLote!dt_fim, "")
        estrutLote(totalLote).dt_validade = BL_HandleNull(rsLote!dt_validade, "")
        estrutLote(totalLote).flg_activo = BL_HandleNull(rsLote!flg_activo, 0)
        
        estrutLote(totalLote).user_cri = BL_HandleNull(rsLote!user_cri, "")
        estrutLote(totalLote).user_act = BL_HandleNull(rsLote!user_act, "")
        estrutLote(totalLote).dt_cri = BL_HandleNull(rsLote!dt_cri, "")
        estrutLote(totalLote).dt_act = BL_HandleNull(rsLote!dt_act, "")
        estrutLote(totalLote).hr_cri = BL_HandleNull(rsLote!hr_cri, "")
        estrutLote(totalLote).hr_act = BL_HandleNull(rsLote!hr_act, "")
        estrutLote(totalLote).cod_nivel = BL_HandleNull(rsLote!cod_nivel, -1)
        estrutLote(totalLote).descr_nivel = BL_SelCodigo("SL_TBF_NIVEL", "DESCR_NIVEL", "COD_NIVEL", BL_HandleNull(rsLote!cod_nivel, ""), "V")
        
        With LvLotes.ListItems.Add(totalLote, "KEY_" & rsLote!seq_lote, rsLote!cod_lote)
            .ListSubItems.Add , , estrutLote(totalLote).descr_lote
            .ListSubItems.Add , , estrutLote(totalLote).descr_nivel
            .ListSubItems.Add , , estrutLote(totalLote).descr_controlo
            .ListSubItems.Add , , estrutLote(totalLote).dt_ini
            .ListSubItems.Add , , estrutLote(totalLote).dt_fim
            .ListSubItems.Add , , estrutLote(totalLote).dt_validade
            If estrutLote(totalLote).flg_activo = 1 Then
                .Checked = True
            Else
                .Checked = False
            End If
        End With
        rsLote.MoveNext
    Wend
    rsLote.MoveFirst
End Sub



Private Sub lvlotes_Click()
    Dim i As Long
    If LvLotes.ListItems.Count = 0 Then Exit Sub
    EcSeqLote = estrutLote(LvLotes.SelectedItem.Index).seq_lote
    EcCodLote = estrutLote(LvLotes.SelectedItem.Index).cod_lote
    EcDescrLote = estrutLote(LvLotes.SelectedItem.Index).descr_lote
    EcCodControlo = estrutLote(LvLotes.SelectedItem.Index).cod_controlo
    EcDescrControlo = estrutLote(LvLotes.SelectedItem.Index).descr_controlo
    EcDtIni = estrutLote(LvLotes.SelectedItem.Index).dt_ini
    EcDtFim = estrutLote(LvLotes.SelectedItem.Index).dt_fim
    EcDtValidade = estrutLote(LvLotes.SelectedItem.Index).dt_validade
    EcActivo = estrutLote(LvLotes.SelectedItem.Index).flg_activo
    EcUserCri = estrutLote(LvLotes.SelectedItem.Index).user_cri
    EcUserAct = estrutLote(LvLotes.SelectedItem.Index).user_act
    EcDtCri = estrutLote(LvLotes.SelectedItem.Index).dt_cri
    EcDtAct = estrutLote(LvLotes.SelectedItem.Index).dt_act
    EcHrCri = estrutLote(LvLotes.SelectedItem.Index).hr_cri
    EcHrAct = estrutLote(LvLotes.SelectedItem.Index).hr_act
    EcUserCriNome = BL_SelNomeUtil(EcUserCri.Text)
    EcUserActNome = BL_SelNomeUtil(EcUserAct.Text)
    CbNivel.Text = estrutLote(LvLotes.SelectedItem.Index).descr_nivel
    
    If estrutLote(LvLotes.SelectedItem.Index).flg_activo = mediSim Then
        LbActivo.ForeColor = vbGreen
        LbActivo.caption = "Lote Activo"
    Else
        LbActivo.ForeColor = vbRed
        LbActivo.caption = "Lote N�o Activo"
    End If
End Sub

Sub Funcao_DataActual()
    
    If Me.ActiveControl.Name = EcDtIni.Name Then
        BL_PreencheData EcDtIni, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = EcDtFim.Name Then
        BL_PreencheData EcDtFim, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = EcDtValidade.Name Then
        BL_PreencheData EcDtValidade, Me.ActiveControl
    End If
End Sub

Public Sub EcCodControlo_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
        
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodControlo.Text <> "" Then
        EcCodControlo = UCase(EcCodControlo)
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_controlo FROM sl_cq_controlo WHERE cod_controlo='" & Trim(EcCodControlo.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodControlo.Text = ""
            EcDescrControlo.Text = ""
        Else
            EcDescrControlo.Text = Tabela!descr_controlo
        End If
       
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrControlo.Text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "eccodcontrolo_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "eccodcontrolo_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub LvLotes_DblClick()
    BtAnalises_Click
End Sub

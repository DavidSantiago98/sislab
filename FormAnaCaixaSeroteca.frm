VERSION 5.00
Begin VB.Form FormAnaCaixaSeroteca 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormAnaCaixaSeroteca"
   ClientHeight    =   5760
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   9885
   Icon            =   "FormAnaCaixaSeroteca.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5760
   ScaleWidth      =   9885
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   4455
      Left            =   240
      TabIndex        =   2
      Top             =   1200
      Width           =   8895
      Begin VB.CommandButton BtInsere 
         Height          =   495
         Left            =   4305
         Picture         =   "FormAnaCaixaSeroteca.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   " Inserir no Perfil "
         Top             =   1665
         Width           =   495
      End
      Begin VB.TextBox EcPesquisa 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   3705
      End
      Begin VB.ListBox EcListaAnalises 
         Appearance      =   0  'Flat
         Height          =   3540
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   840
         Width           =   3705
      End
      Begin VB.ListBox EcListaSC 
         Appearance      =   0  'Flat
         Height          =   3930
         Left            =   5400
         TabIndex        =   4
         Top             =   480
         Width           =   3345
      End
      Begin VB.CommandButton BtRetira 
         Height          =   495
         Left            =   4305
         Picture         =   "FormAnaCaixaSeroteca.frx":0396
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   " Retirar do Perfil "
         Top             =   2220
         Width           =   495
      End
      Begin VB.Label Label5 
         Caption         =   "An�lises"
         Height          =   255
         Left            =   5400
         TabIndex        =   9
         Top             =   240
         Width           =   2955
      End
      Begin VB.Label Label2 
         Caption         =   "Pesquisa R�pida"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   2955
      End
   End
   Begin VB.CommandButton BtCopiaDadosAna 
      Height          =   360
      Left            =   9360
      Picture         =   "FormAnaCaixaSeroteca.frx":0720
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Copia An�lises de outro Grupo de Trabalho"
      Top             =   1200
      Width           =   375
   End
   Begin VB.Frame Frame1 
      Caption         =   "Suporte/Seroteca"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1125
      Left            =   240
      TabIndex        =   0
      Top             =   0
      Width           =   9585
      Begin VB.CommandButton BtPesquisaSeroteca 
         Height          =   315
         Left            =   4800
         Picture         =   "FormAnaCaixaSeroteca.frx":0AAA
         Style           =   1  'Graphical
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Suportes"
         Top             =   360
         Width           =   375
      End
      Begin VB.TextBox EcDescrSeroteca 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1800
         TabIndex        =   15
         Top             =   360
         Width           =   3015
      End
      Begin VB.TextBox EcCodSeroteca 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   14
         Top             =   360
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaCaixa 
         Height          =   315
         Left            =   4800
         Picture         =   "FormAnaCaixaSeroteca.frx":0E34
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Suportes"
         Top             =   720
         Width           =   375
      End
      Begin VB.TextBox EcDescrCaixa 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1800
         TabIndex        =   11
         Top             =   720
         Width           =   3015
      End
      Begin VB.TextBox EcCodCaixa 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   10
         Top             =   720
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Seroteca"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Suporte"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   13
         Top             =   720
         Width           =   735
      End
   End
End
Attribute VB_Name = "FormAnaCaixaSeroteca"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 30/03/2003
' T�cnico : Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String
Dim CriterioTabelaD As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox As Variant
Dim CamposBDparaListBoxS As Variant
Dim CamposBDparaListBoxC As Variant
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim ListaOrigem As Control
Dim ListaDestino As Control
    
Public rs As ADODB.recordset
Public RSListaDestino As ADODB.recordset


Private Sub BtInsere_Click()
    
    Dim i As Integer
    Dim CountAntes As Integer
    Dim CountDepois As Integer
    Dim flg_existe As Boolean
    Dim j As Integer
    Set ListaOrigem = EcListaAnalises
    
    If Not ListaOrigem Is Nothing Then
        Set ListaDestino = EcListaSC
           
        For i = 0 To ListaOrigem.ListCount - 1
            
            If ListaOrigem.Selected(i) Then
                flg_existe = False
                For j = 0 To ListaDestino.ListCount - 1
                    If ListaDestino.ItemData(j) = ListaOrigem.ItemData(i) Then
                        flg_existe = True
                        Exit For
                    End If
                Next
                If flg_existe = False Then
                    CountAntes = EcListaSC.ListCount
                    
                    Call BG_PassaElementoEntreListas(ListaOrigem, _
                                                     ListaDestino, _
                                                     i, _
                                                     False, _
                                                     False)
                    
                    CountDepois = EcListaSC.ListCount
                    ListaOrigem.Selected(i) = False
                End If
            End If
        Next i
    
    End If
    
End Sub

Private Sub BtRetira_Click()
    
    Set ListaDestino = EcListaSC
    If ListaDestino.ListIndex <> -1 Then
        ListaDestino.RemoveItem (ListaDestino.ListIndex)
    End If

End Sub

Private Sub EcListaSC_Click()
    
    EcListaSC.ToolTipText = Trim(EcListaSC.List(EcListaSC.ListIndex))

End Sub




Private Sub EcPesquisa_Change()

    Call RefinaPesquisa
    
End Sub

Sub RefinaPesquisa()
    
    Dim i As Integer
    Dim k As Integer
    Dim TSQLQueryAux As String
    Dim TCampoChave As String
    Dim TCampoPesquisa As String
    Dim TCamposSel As String
    Dim TNomeTabela As String
    Dim TClausulaWhereAuxiliar As String
    Dim TTabelaAux As ADODB.recordset
    Dim NomeControl As Control
    
    On Error GoTo Trata_Erro
    Set TTabelaAux = New ADODB.recordset
    
    Set NomeControl = EcListaAnalises
    TCampoChave = "seq_ana"
    TCamposSel = "cod_ana, descr_ana"
    TCampoPesquisa = "descr_ana"
    TNomeTabela = "slv_analises"
    
    TSQLQueryAux = "SELECT " & TCampoChave & " ," & (TCamposSel) & " FROM " & TNomeTabela
    
    If (gPesquisaDentroCampo) Then
        TSQLQueryAux = TSQLQueryAux & " WHERE UPPER(" & TCampoPesquisa & ") LIKE '%" & Trim(UCase(BG_CvWilcard(EcPesquisa))) & "%'"
    Else
        TSQLQueryAux = TSQLQueryAux & " WHERE UPPER(" & TCampoPesquisa & ") LIKE '" & Trim(UCase(BG_CvWilcard(EcPesquisa))) & "%'"
    End If
    
    If TClausulaWhereAuxiliar <> "" Then
        TSQLQueryAux = TSQLQueryAux & " AND " & TClausulaWhereAuxiliar
    End If
    TSQLQueryAux = TSQLQueryAux & " ORDER BY " & TCampoPesquisa
    
    TTabelaAux.CursorLocation = adUseServer
    TTabelaAux.CursorType = adOpenStatic
    TTabelaAux.Open TSQLQueryAux, gConexao
    NomeControl.ListIndex = mediComboValorNull
    NomeControl.Clear
    
    i = 0
    Do Until TTabelaAux.EOF
        k = InStr(1, TCamposSel, ",")
        NomeControl.AddItem Trim(TTabelaAux(Trim(Mid(TCamposSel, 1, k - 1)))) & _
                                  Space(8 - Len(Trim(TTabelaAux(Trim(Mid(TCamposSel, 1, k - 1))))) + 1) & Trim(TTabelaAux(Trim(Mid(TCamposSel, k + 1)))) & Space(80 - Len(Trim(TTabelaAux(Trim(Mid(TCamposSel, k + 1))))) + 1)
        NomeControl.ItemData(i) = CInt(TTabelaAux(TCampoChave))
        TTabelaAux.MoveNext
        i = i + 1
    Loop
    TTabelaAux.Close
    Set TTabelaAux = Nothing
    
Trata_Erro:
End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()

    Me.caption = " Associa��o de An�lises a um Suporte/Seroteca"
    Me.left = 5
    Me.top = 5
    Me.Width = 9975
    Me.Height = 6180 ' Normal
    'Me.Height = 8825 ' Campos Extras
    
    NomeTabela = "sl_ser_caixa_ana"
    Set CampoDeFocus = EcCodCaixa
    
    NumCampos = 2
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "cod_seroteca"
    CamposBD(1) = "cod_caixa"
        
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodSeroteca
    Set CamposEc(1) = EcCodCaixa
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "C�digo da Seroteca"
    TextoCamposObrigatorios(1) = "C�digo do Suporte"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_caixa"
    Set ChaveEc = EcCodCaixa
    
    CamposBDparaListBox = Array("cod_ana", "descr_ana")
    NumEspacos = Array(8, 40)
    
    EcPesquisa.ToolTipText = cMsgWilcards
    'Call BL_FormataCodigo(EcCodigo)

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
        
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

   
    Call FuncaoProcurar
    
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "Activo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"

'    Me.opC.Value = True
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    
    If Not rs Is Nothing Then
        If rs.state = adStateOpen Then
            rs.Close
        End If
        Set rs = Nothing
    End If

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormAnaCaixaSeroteca = Nothing

End Sub

Sub LimpaCampos()
    
    ' Me.SetFocus
        
    BG_LimpaCampo_Todos CamposEc
    EcListaSC.Clear
    EcDescrCaixa = ""
    EcDescrSeroteca = ""
    EcPesquisa = ""
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheValoresDefeito()
    
    EcCodCaixa = FormSerCodCaixas.EcCodigo
    EcCodcaixa_Validate False
    If FormSerCodCaixas.CbSeroteca.ListIndex > mediComboValorNull Then
        EcCodSeroteca = FormSerCodCaixas.CbSeroteca.ItemData(FormSerCodCaixas.CbSeroteca.ListIndex)
    End If
    EcCodSeroteca_Validate False
End Sub

Sub PreencheCampos()
    
    Dim ii As Integer
    
    ' Me.SetFocus
    
    BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
    EcCodcaixa_Validate False
    EcCodSeroteca_Validate False
    Set RSListaDestino = New ADODB.recordset
    RSListaDestino.CursorType = adOpenStatic
    RSListaDestino.CursorLocation = adUseServer

    If EcCodSeroteca = "" Or EcCodCaixa = "" Then
        Exit Sub
    End If
    CriterioTabelaD = "SELECT sl_ser_caixa_ana.cod_ana, seq_ana, descr_ana " & _
                      "FROM sl_ser_caixa_ana, slv_analises " & _
                      "WHERE sl_ser_caixa_ana.cod_Ana = slv_analises.cod_ana AND cod_seroteca =" & Trim(EcCodSeroteca) & " AND cod_caixa = " & EcCodCaixa
    
    RSListaDestino.Open CriterioTabelaD, gConexao

    If RSListaDestino.RecordCount > 0 Then
        estado = 2
        ' Inicio do preenchimento de 'EcLista'

        Call BL_PreencheListBoxMultipla_ADO(EcListaSC, _
                                            RSListaDestino, _
                                            CamposBDparaListBox, _
                                            NumEspacos, _
                                            CamposBD, _
                                            CamposEc, _
                                            "SELECT", , _
                                            "seq_ana")
        
    End If
    
    RSListaDestino.Close
    Set RSListaDestino = Nothing
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

    EcCodCaixa.Locked = False
    EcCodSeroteca.Locked = False
End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcListaSC.Clear
        
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
        
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    If EcCodSeroteca = "" Or EcCodCaixa = "" Then
        Exit Sub
    End If
    CriterioTabela = "SELECT cod_seroteca, cod_caixa FROM sl_ser_caixa_ana WHERE cod_seroteca =" & Trim(EcCodSeroteca) & " AND cod_caixa = " & EcCodCaixa & " GROUP BY cod_seroteca, cod_caixa"
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        'nao lan�a mensagem quando entra pelo form de perfis
        'BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        'FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        EcCodCaixa.Locked = True
        EcCodSeroteca.Locked = True
        
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = " Modificar"
    gMsgMsg = "Tem a certeza que deseja validar as altera��es efectuadas ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    On Error GoTo ErrorHandler
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim SQLAux As String
    Dim SQLAux2 As String
    Dim MarcaLocal As Variant
    Dim i As Integer
    Dim rsAux As ADODB.recordset
                
    SQLAux2 = "DELETE FROM " & NomeTabela & " WHERE cod_seroteca = " & EcCodSeroteca & " AND cod_caixa = " & EcCodCaixa
    BG_ExecutaQuery_ADO SQLAux2
    
    Set rsAux = New ADODB.recordset
    rsAux.CursorLocation = adUseServer
    
    For i = 0 To EcListaSC.ListCount - 1
        SQLAux = "SELECT cod_ana FROM slv_analises WHERE seq_ana=" & Trim(EcListaSC.ItemData(i))
        rsAux.Open SQLAux, gConexao, adOpenStatic, adLockOptimistic
        
        SQLQuery = "INSERT INTO " & NomeTabela & " (cod_seroteca,cod_caixa, cod_ana) " & _
            " VALUES (" & Trim(EcCodSeroteca) & "," & Trim(EcCodCaixa) & "," & BL_TrataStringParaBD(Trim(rsAux!cod_ana)) & ")"
        BG_ExecutaQuery_ADO SQLQuery
        rsAux.Close
    Next i
    
    Set rsAux = Nothing
    
    'Permite que a FuncaoInserir utilize esta funcao
    If rs Is Nothing Then Exit Sub Else: If rs.RecordCount <= 0 Then Exit Sub
    
    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If MarcaLocal <= rs.RecordCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : BD_Update (FormAnaExames) -> " & Err.Description)
            LimpaCampos
            PreencheCampos
    End Select
End Sub

Sub FuncaoInserir()

    Dim iRes As Integer
    Dim SQLQuery As String
    Dim rsInsAux As ADODB.recordset

    gMsgTitulo = " Inserir"
    gMsgMsg = "Tem a certeza que deseja Inserir estes dados ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        Set rsInsAux = New ADODB.recordset
    
        SQLQuery = "SELECT " & ChaveBD & " FROM " & NomeTabela & " WHERE " & "cod_seroteca = " & EcCodSeroteca & " AND cod_caixa = " & EcCodCaixa

    
        rsInsAux.CursorLocation = adUseServer
        rsInsAux.CursorType = adOpenStatic
        rsInsAux.Open SQLQuery, gConexao
    
        If rsInsAux.RecordCount = (-1) Then
            BG_Mensagem mediMsgBox, "Erro a verificar c�digo!", vbExclamation, "Inserir"
        ElseIf rsInsAux.RecordCount > 0 Then
            BG_Mensagem mediMsgBox, "J� existe registo com esse c�digo !", vbExclamation, "Inserir"
        Else
            BL_InicioProcessamento Me, "A inserir registo."
            iRes = ValidaCamposEc
        
            If iRes = True Then
                BD_Update
            End If
            BL_FimProcessamento Me
        End If
        
        rsInsAux.Close
        Set rsInsAux = Nothing
    End If
    
End Sub




Sub FuncaoRemover()

    Dim sql As String

    gMsgTitulo = " Remover"
    gMsgMsg = "Tem a certeza que deseja apagar o registo ?   "
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
           
    condicao = "cod_seroteca = " & EcCodSeroteca & " AND cod_caixa = " & EcCodCaixa
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
   
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    If MarcaLocal <= rs.RecordCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
        
    LimpaCampos
    PreencheCampos

End Sub

Private Sub BtPesquisaCaixa_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_caixa"
    CamposEcran(1) = "cod_caixa"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_caixa"
    CamposEcran(2) = "descr_caixa"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_ser_caixas"
    CampoPesquisa1 = "descr_caixa"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Suportes Seroteca")
    
    mensagem = "N�o foi encontrada nenhuma Suporte."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodCaixa.Text = resultados(1)
            EcDescrCaixa.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub
Private Sub BtPesquisaSeroteca_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_seroteca"
    CamposEcran(1) = "cod_seroteca"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_seroteca"
    CamposEcran(2) = "descr_seroteca"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_serotecas"
    CampoPesquisa1 = "descr_seroteca"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar  Seroteca")
    
    mensagem = "N�o foi encontrada nenhuma Suporte."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodSeroteca.Text = resultados(1)
            EcDescrSeroteca.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub EcCodcaixa_Validate(Cancel As Boolean)
    Dim RsDescrCaixa As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodCaixa)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodCaixa.Text) <> "" Then
        Set RsDescrCaixa = New ADODB.recordset
        
        With RsDescrCaixa
            .Source = "SELECT cod_caixa, descr_caixa FROM sl_ser_caixas WHERE cod_caixa= " & EcCodCaixa.Text
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrCaixa.RecordCount > 0 Then
            EcCodCaixa.Text = "" & RsDescrCaixa!cod_caixa
            EcDescrCaixa.Text = "" & RsDescrCaixa!descr_caixa
        Else
            Cancel = True
            EcDescrCaixa.Text = ""
            BG_Mensagem mediMsgBox, "A caixa indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrCaixa.Close
        Set RsDescrCaixa = Nothing
    Else
        EcDescrCaixa.Text = ""
    End If

End Sub

Private Sub EcCodSeroteca_Validate(Cancel As Boolean)
    Dim RsDescrCaixa As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodSeroteca)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodSeroteca.Text) <> "" Then
        Set RsDescrCaixa = New ADODB.recordset
        
        With RsDescrCaixa
            .Source = "SELECT cod_Seroteca, descr_seroteca FROM sl_serotecas WHERE cod_seroteca= " & EcCodSeroteca.Text
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrCaixa.RecordCount > 0 Then
            EcCodSeroteca.Text = "" & RsDescrCaixa!cod_seroteca
            EcDescrSeroteca.Text = "" & RsDescrCaixa!descr_seroteca
        Else
            Cancel = True
            EcDescrSeroteca.Text = ""
            BG_Mensagem mediMsgBox, "A seroteca indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrCaixa.Close
        Set RsDescrCaixa = Nothing
    Else
        EcDescrSeroteca.Text = ""
    End If

End Sub




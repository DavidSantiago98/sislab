VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormStkLotes 
   BackColor       =   &H00FFE0C7&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormStkLotes"
   ClientHeight    =   11100
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   14130
   Icon            =   "FormStkLotes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   11100
   ScaleWidth      =   14130
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox PictureListColor 
      Height          =   255
      Left            =   240
      ScaleHeight     =   195
      ScaleWidth      =   555
      TabIndex        =   63
      Top             =   9960
      Width           =   615
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4920
      TabIndex        =   55
      Top             =   9840
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2520
      TabIndex        =   54
      Top             =   9840
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4920
      TabIndex        =   53
      Top             =   10200
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2520
      TabIndex        =   52
      Top             =   10200
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcHrCri 
      Enabled         =   0   'False
      Height          =   285
      Left            =   7200
      TabIndex        =   51
      Top             =   9840
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.TextBox EcHrAct 
      Enabled         =   0   'False
      Height          =   285
      Left            =   7200
      TabIndex        =   50
      Top             =   10200
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFE0C7&
      Height          =   795
      Left            =   240
      TabIndex        =   43
      Top             =   7200
      Width           =   13605
      Begin VB.Label LaDataAlteracao 
         BackColor       =   &H00FFE0C7&
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   49
         Top             =   480
         Width           =   2295
      End
      Begin VB.Label LaDataCriacao 
         BackColor       =   &H00FFE0C7&
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   48
         Top             =   195
         Width           =   2055
      End
      Begin VB.Label LaUtilAlteracao 
         BackColor       =   &H00FFE0C7&
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   47
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         BackColor       =   &H00FFE0C7&
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   46
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   45
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   44
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.Frame FrameGeral 
      BackColor       =   &H00FFE0C7&
      Height          =   2655
      Left            =   240
      TabIndex        =   0
      Top             =   0
      Width           =   13815
      Begin VB.CommandButton BtAnalises 
         BackColor       =   &H80000002&
         Height          =   315
         Left            =   13320
         Picture         =   "FormStkLotes.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   66
         ToolTipText     =   "An�lises Associadas"
         Top             =   1320
         Width           =   375
      End
      Begin VB.CommandButton BtFecharLote 
         BackColor       =   &H00C0C0FF&
         Height          =   375
         Left            =   13320
         Picture         =   "FormStkLotes.frx":0776
         Style           =   1  'Graphical
         TabIndex        =   65
         ToolTipText     =   "Fechar Lote"
         Top             =   2160
         Width           =   375
      End
      Begin VB.CommandButton BtAbrirLote 
         BackColor       =   &H00C0FFC0&
         Height          =   375
         Left            =   13320
         Picture         =   "FormStkLotes.frx":0B00
         Style           =   1  'Graphical
         TabIndex        =   64
         ToolTipText     =   "Abrir Lote"
         Top             =   1680
         Width           =   375
      End
      Begin VB.TextBox EcStkTotal 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   8160
         Locked          =   -1  'True
         MaxLength       =   9
         TabIndex        =   18
         Top             =   2280
         Width           =   975
      End
      Begin VB.TextBox EcStkMin 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10320
         MaxLength       =   9
         TabIndex        =   16
         Top             =   2280
         Width           =   975
      End
      Begin VB.TextBox EcStkAct 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   12360
         Locked          =   -1  'True
         MaxLength       =   9
         TabIndex        =   17
         Top             =   2280
         Width           =   855
      End
      Begin VB.TextBox EcUnidade 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8160
         TabIndex        =   13
         Top             =   1920
         Width           =   975
      End
      Begin VB.TextBox EcQtdUnid 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10320
         TabIndex        =   14
         Top             =   1920
         Width           =   975
      End
      Begin VB.TextBox EcDecremento 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   12360
         TabIndex        =   15
         Top             =   1920
         Width           =   855
      End
      Begin VB.TextBox EcNumUnidades 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   12360
         TabIndex        =   12
         Top             =   1560
         Width           =   855
      End
      Begin VB.TextBox EcDtIni 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   8160
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   1560
         Width           =   975
      End
      Begin VB.TextBox EcDtFim 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   10320
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   1560
         Width           =   975
      End
      Begin VB.TextBox EcDtValidade 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   12360
         TabIndex        =   9
         Top             =   1200
         Width           =   855
      End
      Begin VB.TextBox EcPreco 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8160
         TabIndex        =   7
         Top             =   1200
         Width           =   975
      End
      Begin VB.TextBox EcPrazoEntrega 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10320
         TabIndex        =   8
         Top             =   1200
         Width           =   975
      End
      Begin VB.TextBox EcConservacao 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8160
         TabIndex        =   6
         Top             =   720
         Width           =   5055
      End
      Begin VB.TextBox EcObs 
         Appearance      =   0  'Flat
         Height          =   1005
         Left            =   1080
         TabIndex        =   5
         Top             =   1560
         Width           =   5895
      End
      Begin VB.TextBox EcCodFornecedor 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   315
         Left            =   1110
         MaxLength       =   4
         TabIndex        =   4
         Top             =   1200
         Width           =   735
      End
      Begin VB.TextBox EcDescrFornecedor 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1800
         Locked          =   -1  'True
         TabIndex        =   27
         Top             =   1200
         Width           =   4815
      End
      Begin VB.CommandButton BtPesqFornecedor 
         Height          =   315
         Left            =   6600
         Picture         =   "FormStkLotes.frx":0E8A
         Style           =   1  'Graphical
         TabIndex        =   26
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
         Top             =   1200
         Width           =   375
      End
      Begin VB.TextBox EcCodArtigo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   315
         Left            =   1110
         MaxLength       =   4
         TabIndex        =   3
         Top             =   720
         Width           =   735
      End
      Begin VB.TextBox EcDescrArtigo 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1800
         Locked          =   -1  'True
         TabIndex        =   24
         Top             =   720
         Width           =   4815
      End
      Begin VB.CommandButton BtPesqArtigo 
         Height          =   315
         Left            =   6600
         Picture         =   "FormStkLotes.frx":1414
         Style           =   1  'Graphical
         TabIndex        =   23
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
         Top             =   720
         Width           =   375
      End
      Begin VB.TextBox EcCodigo 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox EcReferencia 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   2760
         TabIndex        =   1
         Top             =   240
         Width           =   1935
      End
      Begin VB.TextBox EcDescricao 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   5760
         TabIndex        =   2
         Top             =   240
         Width           =   7455
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Stock Total"
         Height          =   255
         Index           =   11
         Left            =   7200
         TabIndex        =   42
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Stock Min�mo"
         Height          =   255
         Index           =   12
         Left            =   9240
         TabIndex        =   41
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Stock Actual"
         Height          =   255
         Index           =   13
         Left            =   11400
         TabIndex        =   40
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Unidade"
         Height          =   255
         Index           =   8
         Left            =   7200
         TabIndex        =   39
         Top             =   1920
         Width           =   735
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Qtd por Unid."
         Height          =   255
         Index           =   10
         Left            =   9240
         TabIndex        =   38
         Top             =   1920
         Width           =   975
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Decremento"
         Height          =   255
         Index           =   16
         Left            =   11400
         TabIndex        =   37
         Top             =   1920
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "N� Unidades"
         Height          =   255
         Index           =   9
         Left            =   11400
         TabIndex        =   36
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Data Inicial"
         Height          =   255
         Index           =   6
         Left            =   7200
         TabIndex        =   35
         Top             =   1560
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Data Final"
         Height          =   255
         Index           =   7
         Left            =   9240
         TabIndex        =   34
         Top             =   1560
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Data Valid."
         Height          =   255
         Index           =   15
         Left            =   11400
         TabIndex        =   33
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Pre�o Unit."
         Height          =   255
         Index           =   3
         Left            =   7200
         TabIndex        =   32
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Prazo Entrega"
         Height          =   255
         Index           =   4
         Left            =   9240
         TabIndex        =   31
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Conserva��o"
         Height          =   255
         Index           =   5
         Left            =   7200
         TabIndex        =   30
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Observa��o"
         Height          =   255
         Index           =   14
         Left            =   120
         TabIndex        =   29
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Fornecedor"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   28
         Top             =   1200
         Width           =   855
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Artigo"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   25
         Top             =   720
         Width           =   615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Codigo"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   22
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Ref. Lote"
         Height          =   255
         Index           =   1
         Left            =   1920
         TabIndex        =   21
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFE0C7&
         Caption         =   "Descri��o"
         Height          =   255
         Index           =   2
         Left            =   4920
         TabIndex        =   20
         Top             =   240
         Width           =   855
      End
   End
   Begin MSComctlLib.ListView LvLotes 
      Height          =   4335
      Left            =   240
      TabIndex        =   62
      Top             =   2760
      Width           =   13815
      _ExtentX        =   24368
      _ExtentY        =   7646
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   0
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   3480
      TabIndex        =   61
      Top             =   10200
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   3600
      TabIndex        =   60
      Top             =   9840
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   840
      TabIndex        =   59
      Top             =   10200
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Index           =   2
      Left            =   840
      TabIndex        =   58
      Top             =   9840
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "EcHrCri"
      Height          =   255
      Index           =   18
      Left            =   5880
      TabIndex        =   57
      Top             =   9840
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "EcHrAct"
      Height          =   255
      Index           =   17
      Left            =   5880
      TabIndex        =   56
      Top             =   10200
      Visible         =   0   'False
      Width           =   1215
   End
End
Attribute VB_Name = "FormStkLotes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Const cLightGray = &H808080
Const cLightYellow = &H80000018
Const cWhite = &HFFFFFF
Const cLightBlue = &HFCE7D8

Private Type lote
    Codigo As Long
    referencia As String
    descricao As String
    cod_artigo As Long
    descr_artigo As String
    cod_fornecedor As Long
    descr_fornecedor As String
    observacao As String
    conservacao As String
    preco As String
    prazo_entrega As String
    data_validade As String
    data_inicio As String
    data_fim As String
    num_unidades As String
    unidade As String
    qtd_unidade As String
    decremento As String
    stock_actual As Double
    stock_minimo As Double
    stock_total As Double
    user_cri As String
    user_act As String
    dt_cri As String
    dt_act As String
    hr_cri As String
    hr_act As String
End Type
Dim estrutLote() As lote
Dim totalLote As Integer


Private Sub BtAbrirLote_Click()
    If EcDtIni = "" And EcCodigo <> "" Then
        EcDtIni = Bg_DaData_ADO
        EcDtFim = ""
'        rs.Requery
        STK_InsereMovimento EcCodigo, EcCodArtigo, gCodMovAberLote, 0, "Abertura de Lote"
    End If
End Sub

Private Sub BtAnalises_Click()
    If EcCodigo <> "" Then
        FormStkLotesAna.Show
        FormStkLotesAna.EcCodLote = EcCodigo
        FormStkLotesAna.eccodlote_Validate False
        FormStkLotesAna.FuncaoProcurar
    End If
End Sub

Private Sub BtFecharLote_Click()
    If EcDtIni <> "" And EcDtFim = "" And EcCodigo <> "" Then
        EcDtFim = Bg_DaData_ADO
        rs.Requery
        STK_InsereMovimento EcCodigo, EcCodArtigo, gCodMovFechoLote, 0, "Abertura de Lote"
    End If

End Sub

Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)
End Sub

Private Sub EcDescricao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcDescricao_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub


Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Lotes"
    Me.left = 5
    Me.top = 5
    Me.Width = 14130
    Me.Height = 8520 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = gBD_PREFIXO_TAB & "stk_lotes"
    Set CampoDeFocus = EcDescricao
    
    NumCampos = 25
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "codigo"
    CamposBD(1) = "descricao"
    CamposBD(2) = "user_cri"
    CamposBD(3) = "dt_cri"
    CamposBD(4) = "user_act"
    CamposBD(5) = "dt_act"
    CamposBD(6) = "hr_cri"
    CamposBD(7) = "hr_act"
    CamposBD(8) = "conservacao"
    CamposBD(9) = "referencia"
    
    CamposBD(10) = "cod_artigo"
    CamposBD(11) = "cod_fornecedor"
    CamposBD(12) = "observacao"
    CamposBD(13) = "preco_unit"
    CamposBD(14) = "prazo_entrega"
    CamposBD(15) = "data_validade"
    CamposBD(16) = "data_inicio"
    CamposBD(17) = "data_fim"
    CamposBD(18) = "num_unidades"
    CamposBD(19) = "unidade"
    CamposBD(20) = "qtd_unidade"
    CamposBD(21) = "decremento"
    CamposBD(22) = "stock_actual"
    CamposBD(23) = "stock_minimo"
    CamposBD(24) = "stock_total"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcDescricao
    Set CamposEc(2) = EcUtilizadorCriacao
    Set CamposEc(3) = EcDataCriacao
    Set CamposEc(4) = EcUtilizadorAlteracao
    Set CamposEc(5) = EcDataAlteracao
    Set CamposEc(6) = EcHrCri
    Set CamposEc(7) = EcHrAct
    Set CamposEc(8) = EcConservacao
    Set CamposEc(9) = EcReferencia
    
    Set CamposEc(10) = EcCodArtigo
    Set CamposEc(11) = EcCodFornecedor
    Set CamposEc(12) = EcObs
    Set CamposEc(13) = EcPreco
    Set CamposEc(14) = EcPrazoEntrega
    Set CamposEc(15) = EcDtValidade
    Set CamposEc(16) = EcDtIni
    Set CamposEc(17) = EcDtFim
    Set CamposEc(18) = EcNumUnidades
    Set CamposEc(19) = EcUnidade
    Set CamposEc(20) = EcQtdUnid
    Set CamposEc(21) = EcDecremento
    Set CamposEc(22) = EcStkAct
    Set CamposEc(23) = EcStkMin
    Set CamposEc(24) = EcStkTotal
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    'TextoCamposObrigatorios(0) = "C�digo"
    TextoCamposObrigatorios(1) = "Descri��o"
    TextoCamposObrigatorios(9) = "Refer�ncia"
    TextoCamposObrigatorios(10) = "Artigo"
    TextoCamposObrigatorios(11) = "Tipo Artigo"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "codigo"
    Set ChaveEc = EcCodigo
    
    CamposBDparaListBox = Array("codigo", "descricao", "referencia", "cod_artigo", "preco_unit", "data_inicio", "data_fim", "stock_actual")
    NumEspacos = Array(10, 2, 40, 20, 20, 20, 20, 20)
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormStkLotes = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    EcDescrArtigo = ""
    EcDescrFornecedor = ""
End Sub

Sub DefTipoCampos()
    BG_DefTipoCampoEc_ADO NomeTabela, "data_validade", EcDtValidade, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "data_inicio", EcDtIni, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "data_fim", EcDtFim, mediTipoData
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_act", EcDataAlteracao, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_cri", EcDataAlteracao, mediTipoHora
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    InicializaTreeView
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
'        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
'        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
'        LaDataCriacao = EcDataCriacao & " " & EcHrCri
'        LaDataAlteracao = EcDataAlteracao & " " & EcHrAct
'        ecCodArtigo_Validate False
'        ecCodFornecedor_Validate False
        PreencheLV
        LvLotes.ListItems(1).Selected = True
        lvlotes_Click
        LvLotes.SetFocus
    End If
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        LvLotes.ListItems.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY codigo ASC, descricao ASC"
    Else
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        Call BG_Mensagem(mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, " Procurar")
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    
    If LvLotes.SelectedItem.Index = 1 Then
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LvLotes.ListItems(LvLotes.SelectedItem - 1).Selected = True
        lvlotes_Click
        BL_FimProcessamento Me
        'EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    
    If LvLotes.SelectedItem = totalLote Then
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LvLotes.ListItems(LvLotes.SelectedItem + 1).Selected = True
        lvlotes_Click
        BL_FimProcessamento Me
        'EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        EcHrCri = Bg_DaHora_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    Dim SQLQuery As String
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcCodigo = BG_DaMAX(NomeTabela, "codigo") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        EcHrAct = Bg_DaHora_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
        
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(ChaveEc)
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    PreencheLV
    'BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
    
    'If MarcaLocal <= rs.RecordCount Then rs.Move LvLotes.ListItems.Item.lis, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer apagar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant

    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE codigo=" & EcCodigo
    BG_ExecutaQuery_ADO SQLQuery

    'temos de colocar o cursor Statc para utilizar a propriedade Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    'BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
    
'    If MarcaLocal <= EcLista.ListCount Then
'        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
'        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
'    Else
'        rs.MoveLast
'    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos

End Sub




Private Sub ecCodArtigo_Validate(Cancel As Boolean)

    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset

    EcCodArtigo.Text = UCase(EcCodArtigo.Text)
    If Trim(EcCodArtigo) <> "" Then

        sql = "SELECT " & _
              "     descricao " & _
              "FROM " & _
              gBD_PREFIXO_TAB & "stk_artigo " & _
              "WHERE " & _
              "     codigo=" & (EcCodArtigo)

        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao

        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Artigo inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrArtigo = ""
            EcCodArtigo = ""
        Else
            EcDescrArtigo.Text = rsCodigo!descricao
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrArtigo.Text = ""
        EcCodArtigo = ""
    End If

End Sub


Private Sub BtPesqArtigo_Click()



    Dim ChavesPesq(1 To 2) As String

    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String

    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String

    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String

    'Classe
    Dim CamposRetorno As New ClassPesqResultados

    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long

    'Cabe�alhos
    Dim Headers(1 To 2) As String

    'Mensagem do resultado da pesquisa
    Dim mensagem As String

    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean

    Dim resultados(1 To 2) As Variant

    'Defini��o dos campos a retornar
    ChavesPesq(1) = "codigo"
    CamposEcran(1) = "codigo"
    Tamanhos(1) = 2000

    ChavesPesq(2) = "descricao"
    CamposEcran(2) = "descricao"
    Tamanhos(2) = 3000

    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"

    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_stk_Artigo"
    CampoPesquisa1 = "descricao"
    ClausulaWhere = "  "

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Artigos")

    mensagem = "N�o foi encontrada nenhum Artigo."

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodArtigo.Text = resultados(1)
            EcDescrArtigo.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
End Sub


Sub Funcao_DataActual()
    On Error GoTo TrataErro

    If Me.ActiveControl.Name = "EcDtIni" Then
        BL_PreencheData EcDtIni, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtFim" Then
        BL_PreencheData EcDtFim, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtValidade" Then
        BL_PreencheData EcDtValidade, Me.ActiveControl
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_DataActual: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_DataActual"
    Exit Sub
    Resume Next
    
End Sub

Private Sub ecCodFornecedor_Validate(Cancel As Boolean)

    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As ADODB.recordset

    EcCodFornecedor.Text = UCase(EcCodFornecedor.Text)
    If Trim(EcCodFornecedor) <> "" Then

        sql = "SELECT " & _
              "     nome " & _
              "FROM " & _
              gBD_PREFIXO_TAB & "stk_fornecedores " & _
              "WHERE " & _
              "     codigo=" & (EcCodFornecedor)

        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao

        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Fornecedor inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrFornecedor = ""
            EcCodFornecedor = ""
        Else
            EcDescrFornecedor.Text = rsCodigo!nome
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrFornecedor.Text = ""
        EcCodFornecedor = ""
    End If

End Sub


Private Sub BtPesqFornecedor_Click()



    Dim ChavesPesq(1 To 2) As String

    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String

    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String

    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String

    'Classe
    Dim CamposRetorno As New ClassPesqResultados

    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long

    'Cabe�alhos
    Dim Headers(1 To 2) As String

    'Mensagem do resultado da pesquisa
    Dim mensagem As String

    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean

    Dim resultados(1 To 2) As Variant

    'Defini��o dos campos a retornar
    ChavesPesq(1) = "codigo"
    CamposEcran(1) = "codigo"
    Tamanhos(1) = 2000

    ChavesPesq(2) = "nome"
    CamposEcran(2) = "nome"
    Tamanhos(2) = 3000

    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"

    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_stk_fornecedores"
    CampoPesquisa1 = "descricao"
    ClausulaWhere = "  "

    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Fornecedor")

    mensagem = "N�o foi encontrada nenhum Fornecedor."

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodFornecedor.Text = resultados(1)
            EcDescrFornecedor.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
End Sub



Private Sub InicializaTreeView()
    With LvLotes
        .ColumnHeaders.Add(, , "C�digo", 1000, lvwColumnLeft).Key = "CODIGO"
        .ColumnHeaders.Add(, , "Descri��o", 4700, lvwColumnCenter).Key = "DESCRICAO"
        .ColumnHeaders.Add(, , "Referencia", 1000, lvwColumnCenter).Key = "REFERENCIA"
        .ColumnHeaders.Add(, , "Artigo", 1000, lvwColumnCenter).Key = "COD_ARTIGO"
        .ColumnHeaders.Add(, , "Preco", 1000, lvwColumnCenter).Key = "PRECO_UNIT"
        .ColumnHeaders.Add(, , "Dt.Inicio", 1000, lvwColumnCenter).Key = "DATA_INI"
        .ColumnHeaders.Add(, , "Dt.Fim", 1000, lvwColumnCenter).Key = "DATA_FIM"
        .ColumnHeaders.Add(, , "Stock", 1000, lvwColumnCenter).Key = "STOCK_ACTUAL"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .FullRowSelect = True
        .ForeColor = cLightGray
        .MultiSelect = False
    End With
    ColocaCores LvLotes, PictureListColor, cLightBlue, cWhite
    Exit Sub

End Sub

Public Sub ColocaCores(lv As ListView, pb As PictureBox, cor1 As Long, Optional cor2 As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo TrataErro
    
    lColor1 = cor1
    lColor2 = cor2
    lv.Picture = LoadPicture("")
    lv.Refresh
    pb.Cls
    pb.AutoRedraw = True
    pb.BorderStyle = vbBSNone
    pb.ScaleMode = vbTwips
    pb.Visible = False
    lv.PictureAlignment = lvwTile
    pb.Font = lv.Font
    pb.top = lv.top
    pb.Font = lv.Font
    With pb.Font
        .Size = lv.Font.Size + 1
        .Bold = lv.Font.Bold
        .Charset = lv.Font.Charset
        .Italic = lv.Font.Italic
        .Name = lv.Font.Name
        .Strikethrough = lv.Font.Strikethrough
        .Underline = lv.Font.Underline
        .Weight = lv.Font.Weight
    End With
    pb.Refresh
    lLineHeight = pb.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = lv.Width
    pb.Height = lBarHeight * 2
    pb.Width = lBarWidth
    pb.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    pb.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    pb.AutoSize = True
    lv.Picture = pb.Image
    lv.Refresh
    Exit Sub
    
TrataErro:
    BG_LogFile_Erros "Erro: " & Err.Description, Me.Name, "ColocaCores", False
    Exit Sub
End Sub

Private Sub PreencheLV()
    Dim i As Integer
    
    LvLotes.ListItems.Clear
    totalLote = 0
    ReDim estrutLote(0)
    MarcaInicial = rs.Bookmark
    While Not rs.EOF
        totalLote = totalLote + 1
        ReDim Preserve estrutLote(totalLote)
        
        estrutLote(totalLote).cod_artigo = BL_HandleNull(rs!cod_artigo, -1)
        estrutLote(totalLote).cod_fornecedor = BL_HandleNull(rs!cod_fornecedor, -1)
        estrutLote(totalLote).Codigo = BL_HandleNull(rs!Codigo, -1)
        estrutLote(totalLote).conservacao = BL_HandleNull(rs!conservacao, "")
        estrutLote(totalLote).data_fim = BL_HandleNull(rs!data_fim, "")
        estrutLote(totalLote).data_inicio = BL_HandleNull(rs!data_inicio, "")
        estrutLote(totalLote).data_validade = BL_HandleNull(rs!data_validade, "")
        estrutLote(totalLote).decremento = BL_HandleNull(rs!decremento, "")
        estrutLote(totalLote).descr_artigo = ""
        estrutLote(totalLote).descr_fornecedor = ""
        estrutLote(totalLote).descricao = BL_HandleNull(rs!descricao, "")
        estrutLote(totalLote).unidade = BL_HandleNull(rs!unidade, "")
        estrutLote(totalLote).num_unidades = BL_HandleNull(rs!num_unidades, "")
        estrutLote(totalLote).observacao = BL_HandleNull(rs!observacao, "")
        estrutLote(totalLote).prazo_entrega = BL_HandleNull(rs!prazo_entrega, "")
        estrutLote(totalLote).preco = BL_HandleNull(rs!preco_unit, "")
        estrutLote(totalLote).qtd_unidade = BL_HandleNull(rs!qtd_unidade, "")
        estrutLote(totalLote).referencia = BL_HandleNull(rs!referencia, "")
        estrutLote(totalLote).stock_actual = BL_HandleNull(rs!stock_actual, 0)
        estrutLote(totalLote).stock_minimo = BL_HandleNull(rs!stock_minimo, 0)
        estrutLote(totalLote).stock_total = BL_HandleNull(rs!stock_total, 0)
        estrutLote(totalLote).user_cri = BL_HandleNull(rs!user_cri, "")
        estrutLote(totalLote).user_act = BL_HandleNull(rs!user_act, "")
        estrutLote(totalLote).dt_cri = BL_HandleNull(rs!dt_cri, "")
        estrutLote(totalLote).dt_act = BL_HandleNull(rs!dt_act, "")
        estrutLote(totalLote).hr_cri = BL_HandleNull(rs!hr_cri, "")
        estrutLote(totalLote).hr_act = BL_HandleNull(rs!hr_act, "")
        With LvLotes.ListItems.Add(totalLote, "KEY_" & rs!Codigo, rs!cod_artigo)
            .ListSubItems.Add , , estrutLote(totalLote).descricao
            .ListSubItems.Add , , estrutLote(totalLote).referencia
            .ListSubItems.Add , , estrutLote(totalLote).cod_artigo
            .ListSubItems.Add , , estrutLote(totalLote).preco
            .ListSubItems.Add , , estrutLote(totalLote).data_inicio
            .ListSubItems.Add , , estrutLote(totalLote).data_fim
            .ListSubItems.Add , , estrutLote(totalLote).stock_actual
        End With
        rs.MoveNext
    Wend
    rs.MoveFirst
End Sub

Private Sub lvlotes_Click()
    If LvLotes.ListItems.Count = 0 Then Exit Sub
    rs.Move LvLotes.SelectedItem.Index - 1, 1
    If rs.EOF Then
        rs.MovePrevious
    End If
    If rs.BOF Then
        rs.MoveNext
    End If
    EcCodigo = estrutLote(LvLotes.SelectedItem.Index).Codigo
    EcReferencia = estrutLote(LvLotes.SelectedItem.Index).referencia
    EcDescricao = estrutLote(LvLotes.SelectedItem.Index).descricao
    EcCodArtigo = estrutLote(LvLotes.SelectedItem.Index).cod_artigo
    EcCodFornecedor = estrutLote(LvLotes.SelectedItem.Index).cod_fornecedor
    EcConservacao = estrutLote(LvLotes.SelectedItem.Index).conservacao
    EcPreco = estrutLote(LvLotes.SelectedItem.Index).preco
    EcPrazoEntrega = estrutLote(LvLotes.SelectedItem.Index).prazo_entrega
    EcDtValidade = estrutLote(LvLotes.SelectedItem.Index).data_validade
    EcDtIni = estrutLote(LvLotes.SelectedItem.Index).data_inicio
    EcDtFim = estrutLote(LvLotes.SelectedItem.Index).data_fim
    EcNumUnidades = estrutLote(LvLotes.SelectedItem.Index).num_unidades
    EcUnidade = estrutLote(LvLotes.SelectedItem.Index).unidade
    EcQtdUnid = estrutLote(LvLotes.SelectedItem.Index).qtd_unidade
    EcDecremento = estrutLote(LvLotes.SelectedItem.Index).decremento
    EcStkTotal = estrutLote(LvLotes.SelectedItem.Index).stock_total
    EcStkMin = estrutLote(LvLotes.SelectedItem.Index).stock_minimo
    EcStkAct = estrutLote(LvLotes.SelectedItem.Index).stock_actual
    EcUtilizadorCriacao = estrutLote(LvLotes.SelectedItem.Index).user_cri
    EcUtilizadorAlteracao = estrutLote(LvLotes.SelectedItem.Index).user_act
    EcDataCriacao = estrutLote(LvLotes.SelectedItem.Index).dt_cri
    EcDataAlteracao = estrutLote(LvLotes.SelectedItem.Index).dt_act
    EcHrCri = estrutLote(LvLotes.SelectedItem.Index).hr_cri
    EcHrAct = estrutLote(LvLotes.SelectedItem.Index).hr_act
    EcObs = estrutLote(LvLotes.SelectedItem.Index).observacao
    ecCodArtigo_Validate False
    ecCodFornecedor_Validate False
    BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
    LaDataCriacao = EcDataCriacao & " " & EcHrCri
    LaDataAlteracao = EcDataAlteracao & " " & EcHrAct
    
End Sub



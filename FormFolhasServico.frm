VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormFolhasServico 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Passagem a Hist�rico"
   ClientHeight    =   4050
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6855
   Icon            =   "FormFolhasServico.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4050
   ScaleWidth      =   6855
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   3855
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6615
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   6120
         Picture         =   "FormFolhasServico.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1800
         Width           =   375
      End
      Begin VB.ListBox EcListaProven 
         Height          =   645
         Left            =   1440
         TabIndex        =   11
         Top             =   1800
         Width           =   4695
      End
      Begin VB.CheckBox CkOrderProduto 
         Caption         =   "Ordenado por Produto"
         Height          =   255
         Left            =   3480
         TabIndex        =   10
         Top             =   3240
         Width           =   2055
      End
      Begin VB.CheckBox CbSoImpressas 
         Caption         =   "S� Impressas"
         Height          =   195
         Left            =   1440
         TabIndex        =   9
         Top             =   3240
         Width           =   3855
      End
      Begin VB.ComboBox EcGrupoDesc 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1440
         TabIndex        =   7
         Top             =   1200
         Width           =   4695
      End
      Begin MSComCtl2.DTPicker DTPicker_Inicio 
         Height          =   300
         Left            =   1440
         TabIndex        =   6
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   147456001
         CurrentDate     =   37447
      End
      Begin MSComCtl2.DTPicker DTPicker_Fim 
         Height          =   300
         Left            =   4800
         TabIndex        =   5
         Top             =   600
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   529
         _Version        =   393216
         Format          =   147456001
         CurrentDate     =   37447
      End
      Begin VB.TextBox EcDataFim 
         Height          =   285
         Left            =   5160
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcDataInicio 
         Height          =   285
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   600
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Proveni�ncias"
         Height          =   255
         Index           =   2
         Left            =   360
         TabIndex        =   13
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Grupo"
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   8
         Top             =   1230
         Width           =   2175
      End
      Begin VB.Label LaDe 
         Caption         =   "De"
         Height          =   255
         Left            =   360
         TabIndex        =   4
         Top             =   630
         Width           =   255
      End
      Begin VB.Label Laa 
         Caption         =   "�"
         Height          =   255
         Left            =   3600
         TabIndex        =   3
         Top             =   630
         Width           =   255
      End
   End
End
Attribute VB_Name = "FormFolhasServico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 15/04/2003
' T�cnico Paulo Costa

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Private Sub DefTipoCampos()

    'Tipo Data
    EcDataInicio.Tag = adDBTimeStamp
    EcDataFim.Tag = adDBTimeStamp
    
    EcDataInicio.MaxLength = 10
    EcDataFim.MaxLength = 10
    
End Sub

Private Sub DTPicker_Fim_Change()

    EcDataFim.Text = BG_CvData(str(DTPicker_Fim.value))

    If (Me.DTPicker_Inicio.value > Me.DTPicker_Fim.value) Then
        Me.DTPicker_Inicio.value = Me.DTPicker_Fim.value
    End If

End Sub

Private Sub DTPicker_Inicio_Change()

    EcDataInicio.Text = BG_CvData(str(DTPicker_Inicio.value))
    
    If (Me.DTPicker_Fim.value < Me.DTPicker_Inicio.value) Then
        Me.DTPicker_Fim.value = Me.DTPicker_Inicio.value
    End If

End Sub

Private Sub EcDataFim_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDataFim_Validate(Cancel As Boolean)
    
    If BG_ValidaTipoCampo_ADO(Me, CampoActivo) = False Then
        Cancel = True
    End If
        
End Sub

Private Sub EcDataInicio_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDataInicio_Validate(Cancel As Boolean)
    
    If BG_ValidaTipoCampo_ADO(Me, CampoActivo) = False Then
        Cancel = True
    End If
    
End Sub

Private Sub EcReq_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcReq_KeyPress(KeyAscii As Integer)
    
    If Not IsNumeric(Chr(KeyAscii)) And KeyAscii <> 8 Then KeyAscii = 0
    
End Sub

Private Sub Form_Activate()
    
    Call EventoActivate
    
End Sub

Private Sub Form_Load()
    
    Call EventoLoad
    
End Sub

Public Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
        
    BL_InicioProcessamento Me, "Inicializar �cran..."
    
    Call Inicializacoes
    Call DefTipoCampos
    
    BG_PreencheComboBD_ADO "sl_gr_ana", "seq_gr_ana", "descr_gr_ana", EcGrupoDesc, mediAscComboDesignacao
    
    If (gLAB = cHSMARTA) Then
        On Error Resume Next
        ' GRUPO POR DEFEITO.
        EcGrupoDesc.ListIndex = 4
    End If
    
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    
    BL_FimProcessamento Me
        
End Sub

Public Sub LimpaCampos()

    Me.SetFocus
    
    EcGrupoDesc.ListIndex = mediComboValorNull
    EcDataFim.Text = ""
    EcDataInicio.Text = ""
    EcListaProven.Clear
End Sub

Public Sub FuncaoImprimir()
    
    Dim rv As Integer
    
    Me.MousePointer = vbHourglass
    MDIFormInicio.MousePointer = vbHourglass
    If EcGrupoDesc.ListIndex <> -1 Then
        rv = Mostra_Folha_de_Servi�o()
    End If
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Public Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Public Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If
    
End Sub

Public Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDATAFIM"
            EcDataFim = Bg_DaData_ADO
        Case "ECDATAINICIO"
            EcDataInicio = Bg_DaData_ADO
    End Select
    
End Function

Public Sub EventoUnload()
       
    MDIFormInicio.Tag = ""
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormHistorico = Nothing
    
End Sub

Public Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub

Private Sub Inicializacoes()
        
    Me.caption = " Registos de Servi�o"
    
    Me.left = 600
    Me.top = 400
    Me.Width = 6945
    Me.Height = 4470
   
    Set CampoDeFocus = EcDataInicio
    Set CampoActivo = Me.ActiveControl
    
    EcDataFim.Text = BG_CvData(str(Date))
    DTPicker_Fim.value = Date
    
    EcDataInicio.Text = BG_CvData(str(Date))
    DTPicker_Inicio.value = Date
    
End Sub

Private Function Mostra_Folha_de_Servi�o() As Integer
    On Error GoTo ErrorHandler
    Dim sql As String
    Dim rs As ADODB.recordset
    Dim aux_grupo_ana As String
    Dim aux_grupos As String
    Dim total_reqs As Long
    Dim continua As Boolean
    Dim i As Integer
    sql = "DELETE FROM sl_cr_registo_servico WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sql
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("RegistoServico", "Registo Servi�o", crptToPrinter)
    Else
        continua = BL_IniciaReport("RegistoServico", "Registo Servi�o", crptToWindow)
    End If
    If continua = False Then Exit Function
    
        
    ' -------------------------------------------------------------------
    ' Determina o c�digo do grupo.
    ' -------------------------------------------------------------------
    
    sql = "SELECT " & vbCrLf & _
          "     cod_gr_ana " & vbCrLf & _
          "FROM " & vbCrLf & _
          "     sl_gr_ana " & vbCrLf & _
          "WHERE " & vbCrLf & _
          "     seq_gr_ana = " & EcGrupoDesc.ItemData(EcGrupoDesc.ListIndex)
    
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    rs.Open sql, gConexao
    
    If Not (rs.EOF) Then
        aux_grupo_ana = BL_HandleNull(rs(0), "")
        If (aux_grupo_ana = "") Then
            ' Grupo n�o encontrado.
            Set rs = Nothing
            Call BG_LogFile_Erros("Grupo n�o encontrado : Mostra_Folha_de_Servi�o (Folhas_de_Servico) -> " & Err.Description)
            Mostra_Folha_de_Servi�o = -2
            Exit Function
        End If
    Else
        ' Grupo n�o encontrado.
        Set rs = Nothing
        Call BG_LogFile_Erros("Grupo n�o encontrado : Mostra_Folha_de_Servi�o (Folhas_de_Servico) -> " & Err.Description)
        Mostra_Folha_de_Servi�o = -2
        Exit Function
    End If
    
    rs.Close
    Set rs = Nothing

    ' -------------------------------------------------------------------
    
    Select Case gSGBD
        Case gOracle
            sql = "SELECT re.n_req, ut.nome_ute, pr.descr_proven, re.dt_chega, re.dt_imp, re.gr_ana, RP.cod_prod, PR.descr_produto, ut.t_utente, ut.utente"
            sql = sql & " FROM sl_requis RE, sl_proven PR, sl_identif UT, sl_req_prod RP, sl_produto PR "
            sql = sql & " WHERE re.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDataInicio) & " AND " & BL_TrataDataParaBD(EcDataFim)
            sql = sql & " AND re.cod_proven = pr.cod_proven(+) AND re.seq_utente = ut.seq_utente AND re.estado_req <> 'C'"
            sql = sql & " AND re.n_req = rp.n_req AND rp.cod_prod = pr.cod_produto"
            If CbSoImpressas.value = vbChecked Then
                sql = sql & " AND re.dt_imp IS NOT NULL "
            End If
        Case gSqlServer
            sql = "SELECT re.n_req, ut.nome_ute, pr.descr_proven, re.dt_chega, re.dt_imp, re.gr_ana, RP.cod_prod, PR.descr_produto, ut.t_utente, ut.utente"
            sql = sql & " FROM sl_requis RE LEFT OUTER JOIN sl_proven PR ON re.cod_proven = pr.cod_proven, sl_identif UT, sl_req_prod RP, sl_produto PR  "
            sql = sql & " WHERE re.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDataInicio) & " AND " & BL_TrataDataParaBD(EcDataFim)
            sql = sql & " AND re.seq_utente = ut.seq_utente AND re.estado_req <> 'C'"
            sql = sql & " AND re.n_req = rp.n_req AND rp.cod_prod = pr.cod_produto"
            If CbSoImpressas.value = vbChecked Then
                sql = sql & " AND re.dt_imp IS NOT NULL "
            End If
            
    End Select
    If EcListaProven.ListCount > 0 Then
        sql = sql & " AND pr.seq_proven  IN ("
        For i = 0 To EcListaProven.ListCount - 1
            sql = sql & EcListaProven.ItemData(i) & ", "
        Next i
        sql = Mid(sql, 1, Len(sql) - 2) & ") "
    End If
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    rs.Open sql, gConexao
    
    
    total_reqs = 0
    
    While Not (rs.EOF)
        aux_grupos = ";" & Trim(BL_HandleNull(rs!gr_ana, "")) & ";"
        
        If (InStr(1, aux_grupos, ";" & aux_grupo_ana & ";", vbTextCompare) <> 0) Then
        
            sql = "INSERT INTO sl_cr_registo_servico (nome_computador, num_sessao, n_req,nome,descr_proven,dt_chega,"
            sql = sql & " dt_imp,cod_produto, descr_produto, t_utente, utente) VALUES("
            sql = sql & BL_TrataStringParaBD(gComputador) & ", "
            sql = sql & gNumeroSessao & ", "
            sql = sql & BL_HandleNull(rs!n_req, -1) & ", "
            sql = sql & BL_TrataStringParaBD(BL_HandleNull(rs!nome_ute, "")) & ", "
            sql = sql & BL_TrataStringParaBD(BL_HandleNull(rs!descr_proven, "")) & ", "
            sql = sql & BL_TrataDataParaBD(BL_HandleNull(rs!dt_chega, "")) & ", "
            sql = sql & BL_TrataStringParaBD(BL_HandleNull(rs!dt_imp, "N�O IMPR.")) & ", "
            sql = sql & BL_TrataStringParaBD(BL_HandleNull(rs!cod_prod, "")) & ", "
            sql = sql & BL_TrataStringParaBD(BL_HandleNull(rs!descr_produto, "")) & ", "
            sql = sql & BL_TrataStringParaBD(BL_HandleNull(rs!t_utente, "")) & ", "
            sql = sql & BL_TrataStringParaBD(BL_HandleNull(rs!Utente, "")) & ") "
            total_reqs = total_reqs + 1
            BG_ExecutaQuery_ADO sql
        End If
        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
    If total_reqs > 0 Then
    
        Dim Report As CrystalReport
        Set Report = forms(0).Controls("Report")
        
        Report.SQLQuery = "select nome_computador, num_sessao, n_req, nome, descr_proven, dt_chega, dt_imp, "
        Report.SQLQuery = Report.SQLQuery & " cod_produto, descr_produto "
        Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_registo_servico WHERE sl_cr_registo_servico.nome_computador = " & BL_TrataStringParaBD(gComputador)
        Report.SQLQuery = Report.SQLQuery & " AND sl_cr_registo_servico.num_sessao = " & gNumeroSessao
        
        Report.SQLQuery = Report.SQLQuery & " ORDER BY  sl_cr_registo_servico.descr_produto, n_req "
        'F�rmulas do Report
        Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDataInicio)
        Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDataFim)
        Report.formulas(2) = "Grupo=" & BL_TrataStringParaBD("" & EcGrupoDesc)
        If CkOrderProduto.value = vbChecked Then
            Report.formulas(3) = "OrderProduto=" & BL_TrataStringParaBD("S")
        Else
            Report.formulas(3) = "OrderProduto=" & BL_TrataStringParaBD("N")
        End If
        
        Call BL_ExecutaReport
    End If
    
    Mostra_Folha_de_Servi�o = 1

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Set rs = Nothing
            Call BG_LogFile_Erros("Erro Inesperado : Mostra_Folha_de_Servi�o (Folhas_de_Servico) -> " & Err.Description)
            Mostra_Folha_de_Servi�o = -1
            Exit Function
    End Select
    Exit Function
    Resume Next
End Function

Private Sub Imprime_Linha(f As Integer, _
                          aux_nreq As String, _
                          aux_nome As String, _
                          aux_proven As String, _
                          aux_dt_chega As String, _
                          aux_dt_imp As String, _
                          aux_produtos As String)
         
    On Error GoTo ErrorHandler
    
    Dim rv As Integer
    Dim l_cod As String
    Dim l_descr As String
        
    rv = REQUISICAO_Get_Produtos(aux_nreq, l_cod, l_descr)
        
'   Print #f, "   Requisi��o        : " & aux_nreq & vbCrLf & _
              "   Utente            : " & aux_nome & vbCrLf & _
              "   Proveni�ncia      : " & aux_proven & vbCrLf & _
              "   Produtos          : " & l_descr & vbCrLf & _
              "   Data de Chegada   : " & Format(aux_dt_chega, gFormatoData) & vbCrLf & _
              "   Data de Impress�o : " & Format(aux_dt_imp, gFormatoData) & vbCrLf
 
'    Print #f, Right("" & aux_nreq, 8) & " | " & _
              Left(aux_nome & "                                        ", 40) & " | " & _
              Left(aux_proven & "               ", 15) & " | " & _
              Left(Format(aux_dt_chega, gFormatoData) & "          ", 10) & " | " & _
              Left(Format(aux_dt_imp, gFormatoData) & "          ", 10) & " | " & _
              l_descr

    Print #f, Right("" & aux_nreq, 5) & " | " & _
              left(aux_nome & "                                        ", 30) & " | " & _
              left(aux_proven & "               ", 17) & " | " & _
              left(Format(aux_dt_imp, gFormatoData) & "          ", 10) & " | " & _
              l_cod
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Imprime_Linha (?) -> " & Err.Description)
            Exit Sub
    End Select
End Sub


Private Function REQUISICAO_Get_Produtos(num_req As String, _
                                         lista_codigos As String, _
                                         lista_descr As String) As Integer

    ' Devolve a lista de produtos.

    On Error GoTo ErrorHandler
    
    Dim str_sql As String
    Dim rs As ADODB.recordset
    Dim i As Integer
    
    lista_codigos = ""
    lista_descr = ""
    
    num_req = Trim(num_req)
    
    If (Len(num_req) = 0) Then
        REQUISICAO_Get_Produtos = -2
        Exit Function
    End If
    
    Select Case gSGBD
        
        Case gOracle
    
            str_sql = "SELECT " & _
                      "     PR.cod_produto ," & _
                      "     PR.descr_produto " & _
                      "FROM " & _
                      "     sl_req_prod RP, " & _
                      "     sl_produto PR " & _
                      "WHERE " & _
                      "     RP.cod_prod = PR.cod_produto (+) AND " & _
                      "     RP.n_req    = " & num_req & " " & _
                      "ORDER BY " & _
                      "     PR.seq_produto"
        
        Case gSqlServer
            
            str_sql = "SELECT " & _
                      "     PR.cod_produto ," & _
                      "     PR.descr_produto " & _
                      "FROM " & _
                      "     sl_req_prod RP, " & _
                      "     sl_produto PR " & _
                      "WHERE " & _
                      "     RP.cod_prod *= PR.cod_produto AND " & _
                      "     RP.n_req     = " & num_req & " " & _
                      "ORDER BY " & _
                      "     PR.seq_produto"
    
    End Select
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open str_sql, gConexao, adOpenForwardOnly, adLockReadOnly
    
    lista_codigos = ""
    lista_descr = ""
    i = 0
    
    While Not (rs.EOF)
        
        i = i + 1
        
        lista_codigos = lista_codigos & BL_HandleNull(rs(0), "") & ", "
        lista_descr = lista_descr & BL_HandleNull(rs(1), "") & ", "
            
        rs.MoveNext
        
    Wend

    lista_codigos = Trim(lista_codigos)
    lista_descr = Trim(lista_descr)
    
    If (i = 0) Then
        lista_codigos = ""
        lista_descr = ""
    Else
        lista_codigos = left(lista_codigos, Len(lista_codigos) - 1) & "."
        lista_descr = left(lista_descr, Len(lista_descr) - 1) & "."
    End If

    rs.Close
    Set rs = Nothing
            
    REQUISICAO_Get_Produtos = 1

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : REQUISICAO_Get_Produtos (?) -> " & Err.Description)
            lista_codigos = ""
            lista_descr = ""
            Set rs = Nothing
            REQUISICAO_Get_Produtos = -1
            Exit Function
    End Select
End Function




Private Sub BtPesquisaProveniencia_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If EcListaProven.ListCount = 0 Then
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(0) = resultados(i)
            Else
                EcListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                EcListaProven.ItemData(EcListaProven.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub



Private Sub EclistaProven_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And EcListaProven.ListCount > 0 Then     'Delete
        If EcListaProven.ListIndex > mediComboValorNull Then
            EcListaProven.RemoveItem (EcListaProven.ListIndex)
        End If
    End If
End Sub


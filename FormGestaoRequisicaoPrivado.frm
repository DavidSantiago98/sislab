VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormGestaoRequisicaoPrivado 
   BackColor       =   &H80000004&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormGestaoRequisicaoPrivado"
   ClientHeight    =   8910
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13185
   Icon            =   "FormGestaoRequisicaoPrivado.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8910
   ScaleWidth      =   13185
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.TextBox EcNumColheita 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000040C0&
      Height          =   315
      Left            =   7320
      Locked          =   -1  'True
      TabIndex        =   297
      Top             =   120
      Width           =   1215
   End
   Begin VB.TextBox EcAuxRec 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   12000
      TabIndex        =   219
      Top             =   9960
      Width           =   375
   End
   Begin VB.TextBox EcAuxAna 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   3960
      MaxLength       =   20
      TabIndex        =   93
      Top             =   10275
      Visible         =   0   'False
      Width           =   1215
   End
   Begin TabDlg.SSTab SSTGestReq 
      Height          =   5415
      Left            =   120
      TabIndex        =   59
      Top             =   1440
      Width           =   13095
      _ExtentX        =   23098
      _ExtentY        =   9551
      _Version        =   393216
      Style           =   1
      Tabs            =   7
      Tab             =   1
      TabsPerRow      =   7
      TabHeight       =   520
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&Dados da Requisi��o"
      TabPicture(0)   =   "FormGestaoRequisicaoPrivado.frx":000C
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Label1(8)"
      Tab(0).Control(1)=   "Label1(5)"
      Tab(0).Control(2)=   "Label1(1)"
      Tab(0).Control(3)=   "Label1(4)"
      Tab(0).Control(4)=   "Label10(0)"
      Tab(0).Control(5)=   "Label1(10)"
      Tab(0).Control(6)=   "Label1(3)"
      Tab(0).Control(7)=   "Label41(25)"
      Tab(0).Control(8)=   "Label1(9)"
      Tab(0).Control(9)=   "Label1(12)"
      Tab(0).Control(10)=   "Label1(11)"
      Tab(0).Control(11)=   "Label9(0)"
      Tab(0).Control(12)=   "Label1(2)"
      Tab(0).Control(13)=   "Label1(26)"
      Tab(0).Control(14)=   "Label1(29)"
      Tab(0).Control(15)=   "Label1(7)"
      Tab(0).Control(16)=   "Labe41(5)"
      Tab(0).Control(17)=   "Label1(13)"
      Tab(0).Control(18)=   "Label1(36)"
      Tab(0).Control(19)=   "Labe41(2)"
      Tab(0).Control(20)=   "Label10(1)"
      Tab(0).Control(21)=   "EcDescrPostal"
      Tab(0).Control(22)=   "EcDescrPais"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "EcDescrEFR"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "EcDescrProveniencia"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "EcDescrMedico"
      Tab(0).Control(26)=   "EcNomeMedico"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "EcCodMedico"
      Tab(0).Control(28)=   "BtPesquisaMedico"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "CbUrgencia"
      Tab(0).Control(30)=   "EcDataPrevista"
      Tab(0).Control(31)=   "EcDataChegada"
      Tab(0).Control(32)=   "BtPesquisaEntFin"
      Tab(0).Control(32).Enabled=   0   'False
      Tab(0).Control(33)=   "EcCodEFR"
      Tab(0).Control(34)=   "CbTipoIsencao"
      Tab(0).Control(35)=   "EcNumBenef"
      Tab(0).Control(36)=   "CbPrioColheita"
      Tab(0).Control(37)=   "Frame4"
      Tab(0).Control(38)=   "CbDestino"
      Tab(0).Control(39)=   "EcRuaPostal"
      Tab(0).Control(40)=   "EcCodPostal"
      Tab(0).Control(41)=   "BtPesqCodPostal"
      Tab(0).Control(41).Enabled=   0   'False
      Tab(0).Control(42)=   "EcMorada"
      Tab(0).Control(43)=   "Frame3"
      Tab(0).Control(44)=   "Frame5"
      Tab(0).Control(45)=   "Frame7"
      Tab(0).Control(46)=   "BtPesquisaProveniencia"
      Tab(0).Control(46).Enabled=   0   'False
      Tab(0).Control(47)=   "EcCodProveniencia"
      Tab(0).Control(48)=   "Frame8(0)"
      Tab(0).Control(49)=   "EcCodSala"
      Tab(0).Control(50)=   "EcDescrSala"
      Tab(0).Control(50).Enabled=   0   'False
      Tab(0).Control(51)=   "BtPesquisaSala"
      Tab(0).Control(51).Enabled=   0   'False
      Tab(0).Control(52)=   "CbConvencao"
      Tab(0).Control(53)=   "Frame9"
      Tab(0).Control(54)=   "Frame10"
      Tab(0).Control(55)=   "EcDtPretend"
      Tab(0).Control(56)=   "EcDtEntrega"
      Tab(0).Control(57)=   "EcObsProveniencia"
      Tab(0).Control(58)=   "CbHemodialise"
      Tab(0).Control(59)=   "FrameDig"
      Tab(0).Control(60)=   "BtPesqPais"
      Tab(0).Control(60).Enabled=   0   'False
      Tab(0).Control(61)=   "EcCodPais"
      Tab(0).Control(62)=   "EcDoenteProf"
      Tab(0).Control(63)=   "FrameDomicilio"
      Tab(0).Control(64)=   "EcCodLocalEntrega"
      Tab(0).Control(65)=   "EcDescrLocalEntrega"
      Tab(0).Control(65).Enabled=   0   'False
      Tab(0).Control(66)=   "BtPesquisaLocalEntrega"
      Tab(0).Control(66).Enabled=   0   'False
      Tab(0).ControlCount=   67
      TabCaption(1)   =   "&An�lises"
      TabPicture(1)   =   "FormGestaoRequisicaoPrivado.frx":0028
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "LbTotalAna"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Label7"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "ListaDiagnSec"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "FrAnalises"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "FrameMarcaArs"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "TAna"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "BtAjuda"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).ControlCount=   7
      TabCaption(2)   =   "Pro&dutos"
      TabPicture(2)   =   "FormGestaoRequisicaoPrivado.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "FrameObsEspecif"
      Tab(2).Control(1)=   "FrBtProd"
      Tab(2).Control(2)=   "FrProdutos"
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "Tu&bos"
      TabPicture(3)   =   "FormGestaoRequisicaoPrivado.frx":0060
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "FrBtTubos"
      Tab(3).Control(1)=   "FGTubos"
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Factura Recib&o"
      TabPicture(4)   =   "FormGestaoRequisicaoPrivado.frx":007C
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "FGRecibos"
      Tab(4).Control(1)=   "BtImprimirRecibo"
      Tab(4).Control(2)=   "BtAnularRecibo"
      Tab(4).Control(3)=   "BtGerarRecibo"
      Tab(4).Control(4)=   "BtCobranca"
      Tab(4).Control(5)=   "BtPagamento"
      Tab(4).Control(6)=   "BtAdiantamentos"
      Tab(4).Control(7)=   "BtContaCorrente"
      Tab(4).Control(8)=   "FrameAnaRecibos"
      Tab(4).Control(9)=   "FrameAdiantamentos"
      Tab(4).ControlCount=   10
      TabCaption(5)   =   "An�lises &EFR"
      TabPicture(5)   =   "FormGestaoRequisicaoPrivado.frx":0098
      Tab(5).ControlEnabled=   0   'False
      Tab(5).ControlCount=   0
      TabCaption(6)   =   "Informa��es / Quest�es"
      TabPicture(6)   =   "FormGestaoRequisicaoPrivado.frx":00B4
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "FgQuestoes"
      Tab(6).Control(1)=   "FgInformacao"
      Tab(6).Control(2)=   "EcAuxQuestao"
      Tab(6).ControlCount=   3
      Begin VB.CommandButton BtAjuda 
         Height          =   375
         Left            =   12360
         Picture         =   "FormGestaoRequisicaoPrivado.frx":00D0
         Style           =   1  'Graphical
         TabIndex        =   294
         ToolTipText     =   " Ajuda na constru��o da f�rmula "
         Top             =   4920
         Width           =   400
      End
      Begin MSComctlLib.TreeView TAna 
         Height          =   4425
         Left            =   9120
         TabIndex        =   292
         ToolTipText     =   "Duplo click para saltar para a an�lise."
         Top             =   360
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   7805
         _Version        =   393217
         Indentation     =   9999800
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         BorderStyle     =   1
         Appearance      =   0
      End
      Begin VB.Frame FrameAdiantamentos 
         Caption         =   "Adiantamentos"
         Height          =   4455
         Left            =   -61920
         TabIndex        =   243
         Top             =   360
         Width           =   12735
         Begin VB.CommandButton BtSairAdiantamento 
            Height          =   495
            Left            =   10320
            Picture         =   "FormGestaoRequisicaoPrivado.frx":065A
            Style           =   1  'Graphical
            TabIndex        =   249
            Top             =   3840
            Width           =   615
         End
         Begin VB.CommandButton BtImprimirAdiantamento 
            Height          =   495
            Left            =   12000
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1324
            Style           =   1  'Graphical
            TabIndex        =   246
            ToolTipText     =   "ReImprimir Factura Recibo"
            Top             =   3840
            Width           =   615
         End
         Begin VB.CommandButton BtAnularAdiamento 
            BackColor       =   &H80000004&
            Height          =   495
            Left            =   11160
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1FEE
            Style           =   1  'Graphical
            TabIndex        =   245
            ToolTipText     =   "Devolver Adiantamento"
            Top             =   3840
            Width           =   615
         End
         Begin MSFlexGridLib.MSFlexGrid FgAdiantamentos 
            Height          =   2655
            Left            =   120
            TabIndex        =   244
            Top             =   360
            Width           =   8775
            _ExtentX        =   15478
            _ExtentY        =   4683
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
            Appearance      =   0
         End
      End
      Begin VB.Frame FrameAnaRecibos 
         Caption         =   "An�lises"
         Height          =   4455
         Left            =   -74880
         TabIndex        =   209
         Top             =   480
         Width           =   12975
         Begin MSFlexGridLib.MSFlexGrid FgMoviFact 
            Height          =   3615
            Left            =   120
            TabIndex        =   299
            Top             =   240
            Width           =   12735
            _ExtentX        =   22463
            _ExtentY        =   6376
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
         End
         Begin VB.CommandButton BtAnaAcresc 
            Height          =   375
            Left            =   5880
            Picture         =   "FormGestaoRequisicaoPrivado.frx":2CB8
            Style           =   1  'Graphical
            TabIndex        =   253
            Top             =   720
            Width           =   375
         End
         Begin VB.TextBox EcQtdAnaAcresc 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2160
            TabIndex        =   214
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox EcTaxaAnaAcresc 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   3000
            Locked          =   -1  'True
            TabIndex        =   252
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox EcDescrAnaAcresc 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1200
            Locked          =   -1  'True
            TabIndex        =   251
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox EcCodAnaAcresc 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   240
            TabIndex        =   213
            Top             =   720
            Width           =   735
         End
         Begin VB.CommandButton BtFgAnaRecRecalcula 
            Height          =   495
            Left            =   10560
            Picture         =   "FormGestaoRequisicaoPrivado.frx":3042
            Style           =   1  'Graphical
            TabIndex        =   212
            ToolTipText     =   "Recalcula Pre�o Factura Recibo"
            Top             =   3840
            Width           =   615
         End
         Begin VB.CommandButton BtFgAnaRecSair 
            Height          =   495
            Left            =   11280
            Picture         =   "FormGestaoRequisicaoPrivado.frx":3D0C
            Style           =   1  'Graphical
            TabIndex        =   211
            Top             =   3840
            Width           =   615
         End
         Begin MSFlexGridLib.MSFlexGrid FgAnaRec 
            Height          =   2655
            Left            =   240
            TabIndex        =   210
            Top             =   1080
            Width           =   6615
            _ExtentX        =   11668
            _ExtentY        =   4683
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
         End
      End
      Begin VB.CommandButton BtContaCorrente 
         Height          =   495
         Left            =   -63120
         Picture         =   "FormGestaoRequisicaoPrivado.frx":49D6
         Style           =   1  'Graphical
         TabIndex        =   291
         ToolTipText     =   "Conta Corrente Utente"
         Top             =   4320
         Width           =   615
      End
      Begin VB.Frame FrameMarcaArs 
         BorderStyle     =   0  'None
         Height          =   495
         Left            =   0
         TabIndex        =   287
         Top             =   4800
         Width           =   6375
         Begin VB.TextBox EcAnaEfr 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   120
            TabIndex        =   289
            Top             =   120
            Width           =   3135
         End
         Begin VB.TextBox EcCredAct 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   205
            Left            =   4320
            Locked          =   -1  'True
            TabIndex        =   288
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label Label1 
            Caption         =   "Credencial"
            Height          =   255
            Index           =   32
            Left            =   3480
            TabIndex        =   290
            Top             =   120
            Width           =   975
         End
      End
      Begin VB.CommandButton BtPesquisaLocalEntrega 
         Height          =   315
         Left            =   -69600
         Picture         =   "FormGestaoRequisicaoPrivado.frx":5C38
         Style           =   1  'Graphical
         TabIndex        =   20
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   4920
         Width           =   375
      End
      Begin VB.TextBox EcDescrLocalEntrega 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   285
         TabStop         =   0   'False
         Top             =   4920
         Width           =   3375
      End
      Begin VB.TextBox EcCodLocalEntrega 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   19
         Top             =   4920
         Width           =   735
      End
      Begin VB.Frame FrameDomicilio 
         Caption         =   "Domicilio"
         Height          =   1815
         Left            =   -68280
         TabIndex        =   167
         Top             =   1560
         Width           =   6135
         Begin VB.CheckBox CkCobrarDomicilio 
            Caption         =   "Emitir Recibo do Domicilio"
            Height          =   255
            Left            =   240
            TabIndex        =   208
            Top             =   1320
            Width           =   2295
         End
         Begin VB.ComboBox CbCodUrbano 
            Height          =   315
            Left            =   1440
            Style           =   2  'Dropdown List
            TabIndex        =   169
            Top             =   480
            Width           =   1455
         End
         Begin VB.TextBox EcKm 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1425
            TabIndex        =   168
            Top             =   960
            Width           =   855
         End
         Begin VB.Label Label3 
            Caption         =   "C�digo Urbano"
            Height          =   255
            Left            =   240
            TabIndex        =   171
            Top             =   480
            Width           =   1215
         End
         Begin VB.Label Label4 
            Caption         =   "Km"
            Height          =   255
            Left            =   240
            TabIndex        =   170
            Top             =   960
            Width           =   495
         End
      End
      Begin VB.TextBox EcDoenteProf 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -67080
         TabIndex        =   269
         Top             =   4440
         Width           =   3075
      End
      Begin VB.TextBox EcCodPais 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -67080
         TabIndex        =   266
         Top             =   3000
         Width           =   615
      End
      Begin VB.CommandButton BtPesqPais 
         Height          =   315
         Left            =   -62520
         Picture         =   "FormGestaoRequisicaoPrivado.frx":5FC2
         Style           =   1  'Graphical
         TabIndex        =   264
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Pa�ses"
         Top             =   3000
         Width           =   375
      End
      Begin VB.TextBox EcAuxQuestao 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -74160
         TabIndex        =   262
         Top             =   1560
         Visible         =   0   'False
         Width           =   1215
      End
      Begin MSFlexGridLib.MSFlexGrid FgInformacao 
         Height          =   2175
         Left            =   -74880
         TabIndex        =   259
         Top             =   480
         Width           =   10935
         _ExtentX        =   19288
         _ExtentY        =   3836
         _Version        =   393216
         BackColorBkg    =   -2147483644
         BorderStyle     =   0
      End
      Begin VB.Frame FrameDig 
         Height          =   600
         Left            =   -63480
         TabIndex        =   254
         ToolTipText     =   "Digitalizar Documento"
         Top             =   360
         Width           =   1335
         Begin VB.CommandButton BtDigitalizacao 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoPrivado.frx":634C
            Style           =   1  'Graphical
            TabIndex        =   255
            ToolTipText     =   "Digitalizar Documento"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Digitalizar"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   31
            Left            =   120
            TabIndex        =   256
            ToolTipText     =   "Digitalizar Documento"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.CommandButton BtAdiantamentos 
         BackColor       =   &H80000004&
         Height          =   495
         Left            =   -69000
         Picture         =   "FormGestaoRequisicaoPrivado.frx":6AB6
         Style           =   1  'Graphical
         TabIndex        =   250
         ToolTipText     =   "Adiantamentos Emitidos"
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton BtPagamento 
         Height          =   495
         Left            =   -67200
         Picture         =   "FormGestaoRequisicaoPrivado.frx":7780
         Style           =   1  'Graphical
         TabIndex        =   142
         ToolTipText     =   "Colocar em Pagamento"
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton BtCobranca 
         Height          =   495
         Left            =   -67920
         Picture         =   "FormGestaoRequisicaoPrivado.frx":844A
         Style           =   1  'Graphical
         TabIndex        =   141
         ToolTipText     =   "Colocar em Cobran�a"
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton BtGerarRecibo 
         Height          =   495
         Left            =   -65640
         Picture         =   "FormGestaoRequisicaoPrivado.frx":9114
         Style           =   1  'Graphical
         TabIndex        =   140
         ToolTipText     =   "Emitir Factura Recibo"
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton BtAnularRecibo 
         Height          =   495
         Left            =   -64920
         Picture         =   "FormGestaoRequisicaoPrivado.frx":9DDE
         Style           =   1  'Graphical
         TabIndex        =   139
         ToolTipText     =   "Creditar Factura Recibo"
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton BtImprimirRecibo 
         Height          =   495
         Left            =   -64200
         Picture         =   "FormGestaoRequisicaoPrivado.frx":AAA8
         Style           =   1  'Graphical
         TabIndex        =   138
         ToolTipText     =   "ReImprimir Factura Recibo"
         Top             =   4320
         Width           =   615
      End
      Begin VB.ComboBox CbHemodialise 
         Height          =   315
         Left            =   -70560
         Style           =   2  'Dropdown List
         TabIndex        =   241
         Top             =   2520
         Width           =   1335
      End
      Begin VB.TextBox EcObsProveniencia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -67080
         TabIndex        =   239
         Top             =   3960
         Width           =   3075
      End
      Begin VB.TextBox EcDtEntrega 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -70560
         Locked          =   -1  'True
         TabIndex        =   230
         Top             =   1020
         Width           =   1335
      End
      Begin VB.TextBox EcDtPretend 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -73680
         TabIndex        =   226
         Top             =   1020
         Width           =   1335
      End
      Begin VB.Frame Frame10 
         Height          =   600
         Left            =   -65040
         TabIndex        =   223
         ToolTipText     =   "Folha Resumo"
         Top             =   960
         Width           =   1335
         Begin VB.CommandButton BtResumo 
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoPrivado.frx":B772
            Style           =   1  'Graphical
            TabIndex        =   224
            ToolTipText     =   "Folha Resumo"
            Top             =   120
            Width           =   495
         End
         Begin VB.Label Label6 
            Caption         =   "Resumo"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   225
            ToolTipText     =   "Folha Resumo"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame9 
         Height          =   600
         Left            =   -65040
         TabIndex        =   205
         ToolTipText     =   "Identifica��o de Utente"
         Top             =   360
         Width           =   1335
         Begin VB.CommandButton BtIdentif 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoPrivado.frx":BEDC
            Style           =   1  'Graphical
            TabIndex        =   206
            ToolTipText     =   "Identifica��o de Utente"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Identif."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   120
            TabIndex        =   207
            ToolTipText     =   "Identifica��o de Utente"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame FrameObsEspecif 
         Caption         =   "Observa��o do Produto/Especifica��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2295
         Left            =   -69000
         TabIndex        =   73
         Top             =   480
         Width           =   4575
         Begin VB.TextBox EcObsEspecif 
            Height          =   1335
            Left            =   240
            TabIndex        =   75
            Top             =   360
            Width           =   4095
         End
         Begin VB.CommandButton BtOkObsEspecif 
            Caption         =   "OK"
            Height          =   375
            Left            =   1920
            TabIndex        =   74
            Top             =   1800
            Width           =   735
         End
      End
      Begin VB.ComboBox CbConvencao 
         BackColor       =   &H00C0FFFF&
         Height          =   315
         Left            =   -70560
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   2050
         Width           =   1335
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   -69600
         Picture         =   "FormGestaoRequisicaoPrivado.frx":C646
         Style           =   1  'Graphical
         TabIndex        =   176
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   3000
         Width           =   375
      End
      Begin VB.TextBox EcDescrSala 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   175
         TabStop         =   0   'False
         Top             =   3000
         Width           =   3375
      End
      Begin VB.TextBox EcCodSala 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   13
         Top             =   3000
         Width           =   735
      End
      Begin VB.Frame Frame8 
         Height          =   600
         Index           =   0
         Left            =   -66600
         TabIndex        =   164
         ToolTipText     =   "Domicilio"
         Top             =   960
         Width           =   1335
         Begin VB.CommandButton BtDomicilio 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoPrivado.frx":C9D0
            Style           =   1  'Graphical
            TabIndex        =   165
            ToolTipText     =   "Domicilio"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Domicilio"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   17
            Left            =   120
            TabIndex        =   166
            ToolTipText     =   "Domicilio"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.TextBox EcCodProveniencia 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   14
         Top             =   3480
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   -69600
         Picture         =   "FormGestaoRequisicaoPrivado.frx":D13A
         Style           =   1  'Graphical
         TabIndex        =   161
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   3480
         Width           =   375
      End
      Begin VB.Frame Frame7 
         Height          =   600
         Left            =   -66600
         TabIndex        =   158
         ToolTipText     =   "Consulta de Requisi��es"
         Top             =   360
         Width           =   1335
         Begin VB.CommandButton BtConsReq 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoPrivado.frx":D4C4
            Style           =   1  'Graphical
            TabIndex        =   159
            ToolTipText     =   "Consulta de Requisi��es"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Consulta"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   120
            TabIndex        =   160
            ToolTipText     =   "Consulta de Requisi��es"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame5 
         Height          =   600
         Left            =   -68280
         TabIndex        =   155
         ToolTipText     =   "Impress�o de Etiquetas"
         Top             =   360
         Width           =   1335
         Begin VB.CommandButton BtFichaCruzada 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoPrivado.frx":DC2E
            Style           =   1  'Graphical
            TabIndex        =   156
            ToolTipText     =   "Impress�o de Etiquetas"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label LaFichaCruz 
            Caption         =   "Etiquetas"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   157
            ToolTipText     =   "Impress�o de Etiquetas"
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame3 
         Height          =   600
         Left            =   -68280
         TabIndex        =   148
         ToolTipText     =   "Notas "
         Top             =   960
         Width           =   1335
         Begin VB.CommandButton BtNotasVRM 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoPrivado.frx":E398
            Style           =   1  'Graphical
            TabIndex        =   187
            ToolTipText     =   "Notas"
            Top             =   120
            Width           =   495
         End
         Begin VB.CommandButton BtNotas 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoPrivado.frx":EB02
            Style           =   1  'Graphical
            TabIndex        =   149
            ToolTipText     =   "Notas"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Notas"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   120
            TabIndex        =   150
            ToolTipText     =   "Notas"
            Top             =   240
            Width           =   975
         End
      End
      Begin MSFlexGridLib.MSFlexGrid FGRecibos 
         Height          =   3135
         Left            =   -74880
         TabIndex        =   137
         Top             =   600
         Width           =   12495
         _ExtentX        =   22040
         _ExtentY        =   5530
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
         Appearance      =   0
      End
      Begin VB.TextBox EcMorada 
         Appearance      =   0  'Flat
         Height          =   735
         Left            =   -67080
         MultiLine       =   -1  'True
         TabIndex        =   21
         Top             =   1740
         Width           =   4935
      End
      Begin VB.CommandButton BtPesqCodPostal 
         Height          =   315
         Left            =   -62520
         Picture         =   "FormGestaoRequisicaoPrivado.frx":F26C
         Style           =   1  'Graphical
         TabIndex        =   24
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de C�digos Postais"
         Top             =   2580
         Width           =   375
      End
      Begin VB.TextBox EcCodPostal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -67080
         MaxLength       =   4
         TabIndex        =   22
         Top             =   2580
         Width           =   615
      End
      Begin VB.TextBox EcRuaPostal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -66480
         MaxLength       =   3
         TabIndex        =   23
         Top             =   2580
         Width           =   490
      End
      Begin VB.ComboBox CbDestino 
         Height          =   315
         Left            =   -73680
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   2520
         Width           =   1335
      End
      Begin VB.Frame Frame4 
         Height          =   600
         Left            =   -63480
         TabIndex        =   121
         ToolTipText     =   "Informa��o Cl�nica do Utente"
         Top             =   960
         Width           =   1335
         Begin VB.CommandButton BtInfClinica 
            Enabled         =   0   'False
            Height          =   480
            Left            =   840
            Picture         =   "FormGestaoRequisicaoPrivado.frx":F5F6
            Style           =   1  'Graphical
            TabIndex        =   122
            ToolTipText     =   "Informa��o Cl�nica do Utente"
            Top             =   110
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Inf. C&l�nica"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   120
            TabIndex        =   123
            ToolTipText     =   "Informa��o Cl�nica do Utente"
            Top             =   240
            Width           =   1095
         End
      End
      Begin VB.ComboBox CbPrioColheita 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -73680
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   1560
         Width           =   1335
      End
      Begin VB.Frame FrBtProd 
         BorderStyle     =   0  'None
         Height          =   495
         Left            =   -74760
         TabIndex        =   78
         Top             =   4200
         Width           =   12495
         Begin VB.CommandButton BtPesquisaEspecif 
            Height          =   375
            Left            =   360
            Picture         =   "FormGestaoRequisicaoPrivado.frx":FD60
            Style           =   1  'Graphical
            TabIndex        =   81
            ToolTipText     =   "Pesquisa R�pida de Especifica��es de Produtos"
            Top             =   120
            Width           =   375
         End
         Begin VB.CommandButton BtPesquisaProduto 
            Height          =   375
            Left            =   0
            Picture         =   "FormGestaoRequisicaoPrivado.frx":102EA
            Style           =   1  'Graphical
            TabIndex        =   80
            ToolTipText     =   "Pesquisa R�pida de Produtos"
            Top             =   120
            Width           =   375
         End
         Begin VB.CommandButton BtObsEspecif 
            Enabled         =   0   'False
            Height          =   375
            Left            =   840
            Picture         =   "FormGestaoRequisicaoPrivado.frx":10874
            Style           =   1  'Graphical
            TabIndex        =   79
            ToolTipText     =   "Obs. Especifica��o"
            Top             =   120
            Width           =   375
         End
         Begin VB.Label LaProdutos 
            Height          =   345
            Left            =   1320
            TabIndex        =   82
            Top             =   120
            Width           =   9135
         End
      End
      Begin VB.Frame FrProdutos 
         BorderStyle     =   0  'None
         Height          =   3615
         Left            =   -74880
         TabIndex        =   76
         Top             =   480
         Width           =   12615
         Begin MSFlexGridLib.MSFlexGrid FgProd 
            Height          =   2235
            Left            =   120
            TabIndex        =   77
            Top             =   120
            Width           =   10500
            _ExtentX        =   18521
            _ExtentY        =   3942
            _Version        =   393216
            BackColorBkg    =   -2147483633
            BorderStyle     =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame FrAnalises 
         BorderStyle     =   0  'None
         Height          =   4455
         Left            =   120
         TabIndex        =   70
         Top             =   360
         Width           =   12975
         Begin VB.CommandButton BtHistCred 
            Height          =   375
            Left            =   12600
            Picture         =   "FormGestaoRequisicaoPrivado.frx":10BFE
            Style           =   1  'Graphical
            TabIndex        =   302
            ToolTipText     =   "Informa��o da Credencial Cativada"
            Top             =   1080
            Width           =   375
         End
         Begin VB.CommandButton BTAnalisesMarcadas 
            Height          =   375
            Left            =   12600
            Picture         =   "FormGestaoRequisicaoPrivado.frx":10FE1
            Style           =   1  'Graphical
            TabIndex        =   293
            ToolTipText     =   "An�lises Marcadas"
            Top             =   600
            Width           =   375
         End
         Begin VB.CommandButton BtPesquisaAna 
            Height          =   375
            Left            =   12600
            Picture         =   "FormGestaoRequisicaoPrivado.frx":17833
            Style           =   1  'Graphical
            TabIndex        =   71
            Top             =   90
            Width           =   375
         End
         Begin VB.Frame FrameObsAna 
            BorderStyle     =   0  'None
            Caption         =   "Observa��o da An�lise"
            Height          =   4455
            Left            =   10080
            TabIndex        =   147
            Top             =   0
            Width           =   2655
            Begin RichTextLib.RichTextBox EcObsAna 
               Height          =   4095
               Left            =   0
               TabIndex        =   177
               Top             =   120
               Width           =   2415
               _ExtentX        =   4260
               _ExtentY        =   7223
               _Version        =   393217
               Appearance      =   0
               TextRTF         =   $"FormGestaoRequisicaoPrivado.frx":17DBD
            End
         End
         Begin VB.Frame FrameAjuda 
            Caption         =   "Ajuda"
            Height          =   4455
            Left            =   10080
            TabIndex        =   178
            Top             =   0
            Width           =   2655
            Begin VB.Label Label1 
               Caption         =   "F11 -Incrementa Credencial"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   27
               Left            =   120
               TabIndex        =   218
               Top             =   2640
               Width           =   2775
            End
            Begin VB.Label Label1 
               Caption         =   "F9 - Colocar An�lise - N�O Isenta"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   25
               Left            =   120
               TabIndex        =   186
               Top             =   2000
               Width           =   2775
            End
            Begin VB.Label Label1 
               BackColor       =   &H00FFC0FF&
               Caption         =   "F10 - Colocar An�lise - Borla"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   24
               Left            =   120
               TabIndex        =   185
               Top             =   2300
               Width           =   2775
            End
            Begin VB.Label Label1 
               Caption         =   "F3 - Alterar M�dico"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   23
               Left            =   120
               TabIndex        =   184
               Top             =   500
               Width           =   2775
            End
            Begin VB.Label Label1 
               Caption         =   "F4 - Alterar Entidade Respons�vel"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   22
               Left            =   120
               TabIndex        =   183
               Top             =   800
               Width           =   2775
            End
            Begin VB.Label Label1 
               Caption         =   "F5 - Alterar Pre�o"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   21
               Left            =   120
               TabIndex        =   182
               Top             =   1100
               Width           =   2775
            End
            Begin VB.Label Label1 
               Caption         =   "F7 - Observa��es da An�lise"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   20
               Left            =   120
               TabIndex        =   181
               Top             =   1400
               Width           =   2775
            End
            Begin VB.Label Label1 
               BackColor       =   &H00FFFFC0&
               Caption         =   "F8 - Colocar An�lise - Isenta"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   19
               Left            =   120
               TabIndex        =   180
               Top             =   1700
               Width           =   2775
            End
            Begin VB.Label Label1 
               Caption         =   "F2 - Alterar Credencial"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   18
               Left            =   120
               TabIndex        =   179
               Top             =   240
               Width           =   2775
            End
         End
         Begin MSFlexGridLib.MSFlexGrid FGAna 
            Height          =   3690
            Left            =   120
            TabIndex        =   72
            TabStop         =   0   'False
            ToolTipText     =   "F7 - Para introduzir observa��o"
            Top             =   120
            Width           =   12345
            _ExtentX        =   21775
            _ExtentY        =   6509
            _Version        =   393216
            BackColorBkg    =   -2147483633
            SelectionMode   =   1
            AllowUserResizing=   3
            BorderStyle     =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame FrBtTubos 
         BorderStyle     =   0  'None
         Height          =   735
         Left            =   -74880
         TabIndex        =   68
         Top             =   4140
         Width           =   12615
         Begin VB.Label LaTubos 
            Height          =   465
            Left            =   1320
            TabIndex        =   69
            Top             =   120
            Width           =   9135
         End
      End
      Begin VB.TextBox EcNumBenef 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -67080
         TabIndex        =   25
         Top             =   3495
         Width           =   3060
      End
      Begin VB.ComboBox CbTipoIsencao 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -73680
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   2050
         Width           =   1335
      End
      Begin VB.TextBox EcCodEFR 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   16
         Top             =   4440
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   -69600
         Picture         =   "FormGestaoRequisicaoPrivado.frx":17E3F
         Style           =   1  'Graphical
         TabIndex        =   18
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   4440
         Width           =   375
      End
      Begin VB.TextBox EcDataChegada 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -70560
         TabIndex        =   7
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox EcDataPrevista 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   6
         Top             =   540
         Width           =   1335
      End
      Begin VB.ComboBox CbUrgencia 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -70560
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1560
         Width           =   1335
      End
      Begin VB.ListBox ListaDiagnSec 
         Height          =   2790
         Left            =   5760
         TabIndex        =   60
         Top             =   480
         Visible         =   0   'False
         Width           =   2655
      End
      Begin VB.CommandButton BtPesquisaMedico 
         Height          =   315
         Left            =   -69600
         Picture         =   "FormGestaoRequisicaoPrivado.frx":181C9
         Style           =   1  'Graphical
         TabIndex        =   191
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de M�dicos "
         Top             =   3960
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox EcCodMedico 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73680
         TabIndex        =   15
         Top             =   3960
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.TextBox EcNomeMedico 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   192
         TabStop         =   0   'False
         Top             =   3960
         Visible         =   0   'False
         Width           =   3375
      End
      Begin VB.TextBox EcDescrMedico 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   -73680
         TabIndex        =   17
         Top             =   3960
         Width           =   4455
      End
      Begin MSFlexGridLib.MSFlexGrid FGTubos 
         Height          =   3315
         Left            =   -74760
         TabIndex        =   217
         Top             =   600
         Width           =   12420
         _ExtentX        =   21908
         _ExtentY        =   5847
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid FgQuestoes 
         Height          =   2175
         Left            =   -74880
         TabIndex        =   260
         Top             =   2640
         Width           =   10935
         _ExtentX        =   19288
         _ExtentY        =   3836
         _Version        =   393216
         BackColorBkg    =   -2147483644
         BorderStyle     =   0
      End
      Begin VB.TextBox EcDescrProveniencia 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   162
         TabStop         =   0   'False
         Top             =   3480
         Width           =   3375
      End
      Begin VB.TextBox EcDescrEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72960
         Locked          =   -1  'True
         TabIndex        =   61
         TabStop         =   0   'False
         Top             =   4440
         Width           =   3375
      End
      Begin VB.TextBox EcDescrPais 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -66480
         Locked          =   -1  'True
         TabIndex        =   265
         TabStop         =   0   'False
         Top             =   3000
         Width           =   3975
      End
      Begin VB.TextBox EcDescrPostal 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   -66000
         TabIndex        =   125
         Top             =   2580
         Width           =   3495
      End
      Begin VB.Label Label7 
         Caption         =   "Total An�lises:"
         Height          =   255
         Left            =   10560
         TabIndex        =   296
         Top             =   5040
         Width           =   1095
      End
      Begin VB.Label LbTotalAna 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   11640
         TabIndex        =   295
         Top             =   5040
         Width           =   375
      End
      Begin VB.Label Label10 
         Caption         =   "Local Entrega"
         Height          =   255
         Index           =   1
         Left            =   -74880
         TabIndex        =   286
         Top             =   4920
         Width           =   1335
      End
      Begin VB.Label Labe41 
         Caption         =   "N� Doente Prof."
         Height          =   255
         Index           =   2
         Left            =   -68280
         TabIndex        =   270
         Top             =   4440
         Width           =   1500
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Pa�s"
         Height          =   195
         Index           =   36
         Left            =   -68280
         TabIndex        =   263
         Top             =   3000
         Width           =   330
      End
      Begin VB.Label Label1 
         Caption         =   "Hemodi�lise"
         Height          =   255
         Index           =   13
         Left            =   -71520
         TabIndex        =   242
         Top             =   2520
         Width           =   855
      End
      Begin VB.Label Labe41 
         Caption         =   "Obs. Proven."
         Height          =   255
         Index           =   5
         Left            =   -68280
         TabIndex        =   240
         Top             =   3960
         Width           =   1500
      End
      Begin VB.Label Label1 
         Caption         =   "Dt Entrega"
         Height          =   255
         Index           =   7
         Left            =   -71520
         TabIndex        =   231
         Top             =   1040
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Dt Pretendida"
         Height          =   255
         Index           =   29
         Left            =   -74880
         TabIndex        =   227
         Top             =   1040
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Conven��o"
         Height          =   255
         Index           =   26
         Left            =   -71520
         TabIndex        =   188
         Top             =   2050
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Sala / Posto"
         Height          =   255
         Index           =   2
         Left            =   -74880
         TabIndex        =   174
         Top             =   3000
         Width           =   1155
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Index           =   0
         Left            =   -74880
         TabIndex        =   163
         Top             =   3480
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Morada"
         Height          =   255
         Index           =   11
         Left            =   -68280
         TabIndex        =   127
         Top             =   1740
         Width           =   855
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Cod.Pos&tal"
         Height          =   195
         Index           =   12
         Left            =   -68280
         TabIndex        =   126
         Top             =   2640
         Width           =   765
      End
      Begin VB.Label Label1 
         Caption         =   "Destino"
         Height          =   255
         Index           =   9
         Left            =   -74880
         TabIndex        =   124
         Top             =   2520
         Width           =   855
      End
      Begin VB.Label Label41 
         Caption         =   "Isento"
         Height          =   255
         Index           =   25
         Left            =   -74880
         TabIndex        =   119
         Top             =   2050
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Prioridade"
         Height          =   375
         Index           =   3
         Left            =   -74880
         TabIndex        =   118
         Top             =   1560
         Width           =   1395
      End
      Begin VB.Label Label1 
         Caption         =   "N�. Benef."
         Height          =   255
         Index           =   10
         Left            =   -68280
         TabIndex        =   67
         Top             =   3495
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "E.&F.R."
         Height          =   255
         Index           =   0
         Left            =   -74880
         TabIndex        =   66
         Top             =   4440
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Dt &chegada"
         Height          =   255
         Index           =   4
         Left            =   -71520
         TabIndex        =   65
         Top             =   540
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Dt previs&ta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   -74880
         TabIndex        =   64
         Top             =   540
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Urg�ncia"
         Height          =   255
         Index           =   5
         Left            =   -71520
         TabIndex        =   63
         Top             =   1560
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "&M�dico"
         Height          =   255
         Index           =   8
         Left            =   -74880
         TabIndex        =   62
         Top             =   3960
         Width           =   735
      End
   End
   Begin VB.TextBox EcFlgDiagSec 
      Height          =   285
      Left            =   3480
      TabIndex        =   283
      Top             =   11160
      Width           =   375
   End
   Begin VB.CommandButton BtHistAnt 
      Height          =   255
      Left            =   12600
      Picture         =   "FormGestaoRequisicaoPrivado.frx":18553
      Style           =   1  'Graphical
      TabIndex        =   272
      Top             =   120
      Width           =   255
   End
   Begin VB.CommandButton BtHistSeg 
      Height          =   255
      Left            =   12840
      Picture         =   "FormGestaoRequisicaoPrivado.frx":188DD
      Style           =   1  'Graphical
      TabIndex        =   271
      Top             =   120
      Width           =   255
   End
   Begin RichTextLib.RichTextBox EcInfo 
      Height          =   375
      Left            =   4080
      TabIndex        =   261
      Top             =   10320
      Width           =   495
      _ExtentX        =   873
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"FormGestaoRequisicaoPrivado.frx":18C67
   End
   Begin VB.TextBox EcDescrEstado 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   2400
      Locked          =   -1  'True
      TabIndex        =   258
      Top             =   120
      Width           =   3255
   End
   Begin VB.PictureBox PicCredencial 
      Height          =   735
      Left            =   960
      ScaleHeight     =   675
      ScaleWidth      =   795
      TabIndex        =   257
      Top             =   10320
      Width           =   855
   End
   Begin VB.TextBox EcHemodialise 
      Height          =   285
      Left            =   3360
      TabIndex        =   247
      Top             =   11760
      Width           =   975
   End
   Begin VB.TextBox EcAbrevUte 
      Height          =   285
      Left            =   7680
      TabIndex        =   238
      Top             =   11880
      Width           =   1335
   End
   Begin VB.CommandButton BtConfirm 
      Height          =   375
      Left            =   5640
      Picture         =   "FormGestaoRequisicaoPrivado.frx":18CF2
      Style           =   1  'Graphical
      TabIndex        =   232
      ToolTipText     =   "Confirmar Requisi��o"
      Top             =   120
      Width           =   375
   End
   Begin VB.TextBox EcSMSEnviada 
      Height          =   285
      Left            =   1920
      TabIndex        =   228
      Top             =   11880
      Width           =   375
   End
   Begin VB.TextBox EcFlgFacturado 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   215
      Top             =   12480
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton BtCancelarReq 
      Height          =   315
      Left            =   5520
      Picture         =   "FormGestaoRequisicaoPrivado.frx":1945C
      Style           =   1  'Graphical
      TabIndex        =   130
      TabStop         =   0   'False
      ToolTipText     =   "Raz�o cancelamento"
      Top             =   120
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox EcEtqNAdm 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   198
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNTubos 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   197
      Top             =   11640
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPrinterEtiq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   196
      Top             =   12240
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqTipo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   195
      Top             =   12120
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNEsp 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   194
      Top             =   11400
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEtqNCopias 
      Enabled         =   0   'False
      Height          =   285
      Left            =   11280
      TabIndex        =   193
      Top             =   11160
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcFimSemana 
      Height          =   285
      Left            =   7680
      TabIndex        =   189
      Top             =   11040
      Width           =   375
   End
   Begin VB.ComboBox CbSala 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6960
      Style           =   2  'Dropdown List
      TabIndex        =   172
      Top             =   13440
      Width           =   1335
   End
   Begin VB.TextBox EcPrinterRecibo 
      Height          =   285
      Left            =   3960
      TabIndex        =   153
      Top             =   13320
      Width           =   1215
   End
   Begin VB.TextBox EcObsReq 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1560
      MultiLine       =   -1  'True
      TabIndex        =   151
      Top             =   13320
      Width           =   1215
   End
   Begin VB.TextBox EcPesqRapMedico 
      Height          =   285
      Left            =   1920
      TabIndex        =   145
      Top             =   12120
      Width           =   375
   End
   Begin VB.TextBox EcNumReqAssoc 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H002C7124&
      Height          =   315
      Left            =   10920
      TabIndex        =   143
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox EcCodPostalAux 
      Height          =   285
      Left            =   5760
      TabIndex        =   136
      Top             =   11760
      Width           =   375
   End
   Begin VB.TextBox EcDescrEfr2 
      Height          =   285
      Left            =   5760
      TabIndex        =   134
      Top             =   11400
      Width           =   375
   End
   Begin VB.TextBox EcCodEfr2 
      Height          =   285
      Left            =   5760
      TabIndex        =   131
      Top             =   11040
      Width           =   375
   End
   Begin VB.TextBox EcNumReq 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H002C7124&
      Height          =   315
      Left            =   1320
      TabIndex        =   0
      Top             =   120
      Width           =   1095
   End
   Begin VB.ComboBox CbEstadoReq 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      Height          =   315
      Left            =   2400
      Style           =   1  'Simple Combo
      TabIndex        =   128
      TabStop         =   0   'False
      Top             =   11520
      Width           =   3135
   End
   Begin VB.TextBox EcUrgencia 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   103
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPagUte 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   102
      Top             =   11640
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcEstadoReq 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   101
      Top             =   11160
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcPesqRapProven 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3960
      TabIndex        =   100
      Top             =   11040
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ComboBox CbEstadoReqAux 
      Height          =   315
      Left            =   7080
      Style           =   2  'Dropdown List
      TabIndex        =   99
      TabStop         =   0   'False
      Top             =   11760
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Timer TimerRS 
      Interval        =   60000
      Left            =   0
      Top             =   10200
   End
   Begin VB.TextBox EcSeqUtente 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   98
      Top             =   11400
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.TextBox EcPesqRapProduto 
      Enabled         =   0   'False
      Height          =   285
      Left            =   8280
      TabIndex        =   97
      Top             =   11400
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox CodAnaTemp 
      Height          =   285
      Left            =   3960
      TabIndex        =   96
      Top             =   11520
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcCodIsencao 
      Height          =   285
      Left            =   3960
      TabIndex        =   95
      Top             =   11280
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcAuxProd 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   94
      Top             =   12480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcTEFR 
      BackColor       =   &H00EDFAED&
      Enabled         =   0   'False
      Height          =   285
      Left            =   6240
      TabIndex        =   92
      Top             =   12480
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox EcImprimirResumo 
      Height          =   285
      Left            =   8520
      TabIndex        =   91
      Top             =   12120
      Width           =   975
   End
   Begin VB.TextBox EcPrinterResumo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4560
      TabIndex        =   90
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   8520
      TabIndex        =   89
      Top             =   12480
      Width           =   1095
   End
   Begin VB.TextBox EcGrupoAna 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3960
      TabIndex        =   88
      Top             =   11880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcHoraChegada 
      Height          =   285
      Left            =   3960
      Locked          =   -1  'True
      TabIndex        =   87
      Top             =   12840
      Width           =   975
   End
   Begin VB.TextBox EcAuxTubo 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   86
      Top             =   12840
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcPesqRapTubo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   9000
      TabIndex        =   85
      Top             =   11400
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcinfComplementar 
      Height          =   285
      Left            =   6240
      TabIndex        =   84
      Top             =   12840
      Width           =   735
   End
   Begin VB.TextBox EcPrColheita 
      Height          =   285
      Left            =   7920
      TabIndex        =   83
      Top             =   12600
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   975
      Left            =   120
      TabIndex        =   55
      Top             =   360
      Width           =   12975
      Begin VB.CommandButton btCativarReq 
         Height          =   375
         Left            =   7320
         MaskColor       =   &H00FFFFFF&
         Picture         =   "FormGestaoRequisicaoPrivado.frx":19AE6
         Style           =   1  'Graphical
         TabIndex        =   301
         Top             =   360
         UseMaskColor    =   -1  'True
         Width           =   615
      End
      Begin VB.TextBox EcSexo 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10800
         TabIndex        =   222
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   1935
      End
      Begin VB.TextBox EcIdade 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   11880
         TabIndex        =   5
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   600
         Width           =   4335
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   2
         Top             =   240
         Width           =   1455
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10800
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Data de Nascimento"
         Top             =   240
         Width           =   1095
      End
      Begin VB.ComboBox CbTipoUtente 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label lbCativar 
         Caption         =   "Cativar Requisi��es"
         Height          =   255
         Left            =   5760
         TabIndex        =   300
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Sexo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   28
         Left            =   9480
         TabIndex        =   221
         ToolTipText     =   "Data de Nascimento"
         Top             =   600
         Width           =   375
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   58
         Top             =   600
         Width           =   405
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   57
         Top             =   240
         Width           =   465
      End
      Begin VB.Label LbData 
         AutoSize        =   -1  'True
         Caption         =   "Data Nasc."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   9480
         TabIndex        =   56
         ToolTipText     =   "Data de Nascimento"
         Top             =   240
         Width           =   795
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Registo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2055
      Left            =   120
      TabIndex        =   26
      Top             =   6840
      Width           =   12975
      Begin VB.TextBox EcUtilSMS 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7920
         Locked          =   -1  'True
         TabIndex        =   280
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox EcHrSMS 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11040
         Locked          =   -1  'True
         TabIndex        =   279
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcDtSMS 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10080
         Locked          =   -1  'True
         TabIndex        =   278
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcDtEmail 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   275
         Top             =   1680
         Width           =   975
      End
      Begin VB.TextBox EcHrEmail 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4320
         Locked          =   -1  'True
         TabIndex        =   274
         Top             =   1680
         Width           =   735
      End
      Begin VB.TextBox EcUtilEmail 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   273
         Top             =   1680
         Width           =   615
      End
      Begin VB.CheckBox CkAvisarSMS 
         Caption         =   "Enviar SMS quando resultados completos"
         Height          =   255
         Left            =   9360
         TabIndex        =   268
         Top             =   1680
         Width           =   3375
      End
      Begin VB.CheckBox CkReqHipocoagulados 
         Caption         =   "Requisi��o de hipocoagulados"
         Height          =   255
         Left            =   6720
         TabIndex        =   267
         Top             =   1680
         Width           =   2535
      End
      Begin VB.TextBox EcUtilConf 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   235
         Top             =   1320
         Width           =   615
      End
      Begin VB.TextBox EcHrConf 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4320
         Locked          =   -1  'True
         TabIndex        =   234
         Top             =   1320
         Width           =   735
      End
      Begin VB.TextBox EcDtConf 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   233
         Top             =   1320
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   42
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox EcDataCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   41
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcHoraCriacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4320
         Locked          =   -1  'True
         TabIndex        =   40
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox EcDataImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10080
         Locked          =   -1  'True
         TabIndex        =   39
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7920
         Locked          =   -1  'True
         TabIndex        =   38
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox EcHoraImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11040
         Locked          =   -1  'True
         TabIndex        =   37
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcDataImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10080
         Locked          =   -1  'True
         TabIndex        =   36
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorImpressao2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7920
         Locked          =   -1  'True
         TabIndex        =   35
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox EcHoraImpressao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11040
         Locked          =   -1  'True
         TabIndex        =   34
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcUtilizadorAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   33
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox EcHoraAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4320
         Locked          =   -1  'True
         TabIndex        =   32
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox EcDataAlteracao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   31
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox EcDataFecho 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcHoraFecho 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4320
         Locked          =   -1  'True
         TabIndex        =   29
         Top             =   960
         Width           =   735
      End
      Begin VB.TextBox EcUtilizadorFecho 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   28
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox EcLocal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7920
         Locked          =   -1  'True
         TabIndex        =   27
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label EcUtilNomeSMS 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8520
         TabIndex        =   282
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "SMS"
         Height          =   255
         Index           =   40
         Left            =   6720
         TabIndex        =   281
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Email"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   38
         Left            =   120
         TabIndex        =   277
         Top             =   1680
         Width           =   975
      End
      Begin VB.Label EcUtilNomeEmail 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   276
         Top             =   1680
         Width           =   1695
      End
      Begin VB.Label EcUtilNomeConf 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   237
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Confirma��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   30
         Left            =   120
         TabIndex        =   236
         Top             =   1320
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Cria��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   54
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label16 
         Caption         =   "Altera��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   53
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Impress�o"
         Height          =   255
         Index           =   42
         Left            =   6720
         TabIndex        =   52
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "�ltima 2� Via"
         Height          =   255
         Index           =   41
         Left            =   6720
         TabIndex        =   51
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label LbNomeUtilAlteracao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   50
         Top             =   600
         Width           =   1695
      End
      Begin VB.Label LbNomeUtilCriacao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   49
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label LbNomeUtilImpressao 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8520
         TabIndex        =   48
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label LbNomeUtilImpressao2 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8520
         TabIndex        =   47
         Top             =   600
         Width           =   1575
      End
      Begin VB.Label LbNomeUtilFecho 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1680
         TabIndex        =   46
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label2 
         Caption         =   "Fecho"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   45
         Top             =   960
         Width           =   855
      End
      Begin VB.Label LbNomeLocal 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8520
         TabIndex        =   44
         Top             =   1320
         Width           =   3495
      End
      Begin VB.Label Label1 
         Caption         =   "Local Requis."
         Height          =   255
         Index           =   39
         Left            =   6720
         TabIndex        =   43
         Top             =   1320
         Width           =   1095
      End
   End
   Begin MSComctlLib.ImageList ListaImagensAna 
      Left            =   8280
      Top             =   13080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1A212
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1A36C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1A4C6
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1A620
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1A77A
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1A8D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1AA2E
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1AB88
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1ACE2
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1AE3C
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1AF96
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1B0F0
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton BtGetSONHO 
      Enabled         =   0   'False
      Height          =   315
      Left            =   5040
      MaskColor       =   &H00C0C0FF&
      Picture         =   "FormGestaoRequisicaoPrivado.frx":1B24A
      Style           =   1  'Graphical
      TabIndex        =   129
      ToolTipText     =   "Importar do SONHO"
      Top             =   120
      Visible         =   0   'False
      Width           =   495
   End
   Begin MSComctlLib.ImageList ImageListSearch 
      Left            =   480
      Top             =   10920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   24
      ImageHeight     =   24
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1B55C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1BCD6
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1C450
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormGestaoRequisicaoPrivado.frx":1CBCA
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label LbNumColheita 
      AutoSize        =   -1  'True
      Caption         =   "N�Colheita"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   6360
      TabIndex        =   298
      Top             =   120
      Width           =   750
   End
   Begin VB.Label Label41 
      Caption         =   "EcFlgDiagSec"
      Enabled         =   0   'False
      Height          =   255
      Index           =   29
      Left            =   2160
      TabIndex        =   284
      Top             =   11160
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcHemodialise"
      Enabled         =   0   'False
      Height          =   255
      Index           =   28
      Left            =   2400
      TabIndex        =   248
      Top             =   11760
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcSMSEnviada"
      Enabled         =   0   'False
      Height          =   255
      Index           =   27
      Left            =   600
      TabIndex        =   229
      Top             =   11880
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxRec"
      Enabled         =   0   'False
      Height          =   255
      Index           =   26
      Left            =   10320
      TabIndex        =   220
      Top             =   13080
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcFlgFacturado"
      Enabled         =   0   'False
      Height          =   255
      Index           =   23
      Left            =   10200
      TabIndex        =   216
      Top             =   12480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqNAdm"
      Enabled         =   0   'False
      Height          =   255
      Index           =   18
      Left            =   10200
      TabIndex        =   204
      Top             =   11880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqNTubos"
      Enabled         =   0   'False
      Height          =   255
      Index           =   16
      Left            =   10200
      TabIndex        =   203
      Top             =   11640
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcPrinterEtiq"
      Enabled         =   0   'False
      Height          =   255
      Index           =   12
      Left            =   10200
      TabIndex        =   202
      Top             =   12240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcEtqTipo"
      Enabled         =   0   'False
      Height          =   255
      Index           =   15
      Left            =   10200
      TabIndex        =   201
      Top             =   12120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Labe41 
      Caption         =   "EcEtqNEsp"
      Enabled         =   0   'False
      Height          =   255
      Index           =   1
      Left            =   10200
      TabIndex        =   200
      Top             =   11400
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Labe41 
      Caption         =   "EcEtqNCopias"
      Enabled         =   0   'False
      Height          =   255
      Index           =   0
      Left            =   10200
      TabIndex        =   199
      Top             =   11160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcFimSemana"
      Enabled         =   0   'False
      Height          =   255
      Index           =   11
      Left            =   6240
      TabIndex        =   190
      Top             =   11040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label LbNumero 
      AutoSize        =   -1  'True
      Caption         =   "&Requisi��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Index           =   0
      Left            =   240
      TabIndex        =   173
      Top             =   120
      Width           =   900
   End
   Begin VB.Label Label15 
      Caption         =   "EcPrinterRecibo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   2880
      TabIndex        =   154
      Top             =   13320
      Width           =   975
   End
   Begin VB.Label Label15 
      Caption         =   "Obs. &Final"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   360
      TabIndex        =   152
      Top             =   13320
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapMedico"
      Enabled         =   0   'False
      Height          =   255
      Index           =   7
      Left            =   840
      TabIndex        =   146
      Top             =   12120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label LbNumero 
      AutoSize        =   -1  'True
      Caption         =   "&N�mero do Posto"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Index           =   1
      Left            =   9600
      TabIndex        =   144
      Top             =   120
      Width           =   1230
   End
   Begin VB.Label Label41 
      Caption         =   "EcCodPostalAux"
      Enabled         =   0   'False
      Height          =   255
      Index           =   4
      Left            =   4440
      TabIndex        =   135
      Top             =   11640
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcDescrEfr2"
      Enabled         =   0   'False
      Height          =   255
      Index           =   3
      Left            =   4440
      TabIndex        =   133
      Top             =   11400
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcCodEfr2"
      Enabled         =   0   'False
      Height          =   255
      Index           =   2
      Left            =   4440
      TabIndex        =   132
      Top             =   11040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxProd"
      Enabled         =   0   'False
      Height          =   255
      Index           =   5
      Left            =   480
      TabIndex        =   120
      Top             =   12480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcUrgencia"
      Enabled         =   0   'False
      Height          =   255
      Index           =   10
      Left            =   840
      TabIndex        =   117
      Top             =   11880
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcPagUte"
      Enabled         =   0   'False
      Height          =   255
      Index           =   9
      Left            =   840
      TabIndex        =   116
      Top             =   11640
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcEstadoReq"
      Enabled         =   0   'False
      Height          =   255
      Index           =   1
      Left            =   840
      TabIndex        =   115
      Top             =   11160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapProven"
      Enabled         =   0   'False
      Height          =   255
      Index           =   6
      Left            =   2520
      TabIndex        =   114
      Top             =   11040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcSeqUtente"
      Enabled         =   0   'False
      Height          =   255
      Index           =   8
      Left            =   840
      TabIndex        =   113
      Top             =   11400
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcPesqRapProduto"
      Enabled         =   0   'False
      Height          =   255
      Index           =   14
      Left            =   6840
      TabIndex        =   112
      Top             =   11400
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "CodAnaTemp"
      Height          =   255
      Index           =   0
      Left            =   2520
      TabIndex        =   111
      Top             =   11520
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label41 
      Caption         =   "EcCodIsencao"
      Height          =   255
      Index           =   19
      Left            =   2520
      TabIndex        =   110
      Top             =   11280
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxAna"
      Enabled         =   0   'False
      Height          =   255
      Index           =   17
      Left            =   2880
      TabIndex        =   109
      Top             =   12480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcTEFR"
      Enabled         =   0   'False
      Height          =   255
      Index           =   20
      Left            =   5400
      TabIndex        =   108
      Top             =   12480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcGrupoAna"
      Enabled         =   0   'False
      Height          =   255
      Index           =   13
      Left            =   2520
      TabIndex        =   107
      Top             =   11880
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label41 
      Caption         =   "EcAuxTubo"
      Enabled         =   0   'False
      Height          =   255
      Index           =   22
      Left            =   480
      TabIndex        =   106
      Top             =   12840
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label41 
      Caption         =   "EcinfComplementar"
      Height          =   255
      Index           =   21
      Left            =   5400
      TabIndex        =   105
      Top             =   12840
      Width           =   855
   End
   Begin VB.Label Label41 
      Caption         =   "EcPrColheita"
      Enabled         =   0   'False
      Height          =   255
      Index           =   24
      Left            =   7200
      TabIndex        =   104
      Top             =   12600
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "FormGestaoRequisicaoPrivado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 27/03/2003
' T�cnico : Paulo Costa

' Vari�veis  para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim CoresCamposEc() As Long
Dim CamposEcBckp() As String
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Public CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim Formato1 As String
Dim Formato2 As String


Public rs As adodb.recordset
Public MarcaLocal As Variant

'Controlo dos produtos
Public RegistosP As New Collection
Dim LastColP As Integer
Dim LastRowP As Integer
Dim ExecutaCodigoP As Boolean

'Controlo dos tubos
Dim LastColT As Integer
Dim LastRowT As Integer
Dim ExecutaCodigoT As Boolean

'Flag que indica se a entidade em causa � ARS
Dim FlgARS As Boolean

'Flag que indica se a entidade em causa � ADSE
Dim FlgADSE As Boolean

'Controlo de analises
Public RegistosA As New Collection
Dim LastColA As Integer
Dim LastRowA As Long
Dim ExecutaCodigoA As Boolean

'Controlo de Recibos
Public RegistosR As New Collection
Dim LastColR As Integer
Dim LastRowR As Integer
Dim ExecutaCodigoR As Boolean

'Controlo de Recibos Manuais
Public RegistosRM As New Collection

'Controlo de Adiantamentos
Public RegistosAD As New Collection

'acumula o numero do P1 para quando este for superior a 7 incrementar 1 ao P1
Dim ContP1 As Integer
Dim Flg_IncrementaP1 As Boolean

'Comando utilizados obter a ordem correcta das an�lises
Dim CmdOrdAna As New adodb.Command

'Comando utilizados para seleccionar as an�lises da requisi��o mais os seu dados
Dim CmdAnaReq As String
Dim CmdAnaReq_H As String


'Flag usada para controlar a coloca��o da data actual no lostfocus da EcDataPrevista
'quando se faz o limpacampos
Dim Flg_LimpaCampos As Boolean

'flag que indica se se deve preencher a data de chegada de todos os produtos na altura do Insert ou UpDate
Dim Flg_PreencherDtChega As Boolean

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind, Max  As Integer

'valor do indice nas combos de Pagamento (TM,Ent,Ute) quando se executa a fun��o procurar
Dim CbTMIndex As Integer
Dim CbEntIndex As Integer
Dim CbUteIndex As Integer

Dim gRec As Long

'Flag que evita os beep dos enters nas textbox
Dim Enter As Boolean

'Constante com cor azul
Const azul = &H8000000D

Dim Flg_PergProd As Boolean
Dim Flg_DtChProd As Boolean
Dim Resp_PergProd As Boolean
Dim Resp_PergDtChProd As Boolean

Dim Flg_PergTubo As Boolean
Dim Flg_DtChTubo As Boolean
Dim Resp_PergTubo As Boolean
Dim Resp_PergDtChTubo As Boolean


'contem a data de chegada da verificacao dos produtos para utilizacao na marcacao de analises
Dim gColocaDataChegada As String

'Flag que indica a entrada no form de pesquisa de utentes
Dim Flg_PesqUtente As Boolean


'Variaveis utilizadas na marca��o de an�lises GhostMember na pr�pria requisi�ao
Dim GhostPerfil As String
Dim GhostComplexa As String

Dim Flg_DadosAnteriores As Boolean

' ---------------------------------------------------

' 02


Dim Flg_Gravou As Boolean


'Estrutura que guarda as an�lises eliminadas
Private Type Tipo_AnalisesEliminadas
    codAna As String
    NReq As String
    credencial As String
End Type
Dim Eliminadas() As Tipo_AnalisesEliminadas
Dim TotalEliminadas As Integer

Private Type Tipo_tuboEliminado
    seq_req_tubo As Long
    n_req As String
End Type
Dim tubosEliminados() As Tipo_tuboEliminado
Dim TotalTubosEliminados As Integer

'Estrutura que guarda as an�lises acrescentadas
Private Type Tipo_AnalisesAcrescentadas
    cod_agrup As String
    NReq As String
End Type
Dim Acrescentadas() As Tipo_AnalisesAcrescentadas
Dim TotalAcrescentadas As Integer

Public Req_FDS As Boolean

Const vermelho = &HC0C0FF
Const Verde = &HC0FFC0
Const Amarelo = &HC0FFFF

Const lColFgAnaCodigo = 0
Const lColFgAnaDescricao = 1
Const lColFgAnaP1 = 2
Const lColFgAnaMedico = 3
Const lColFgAnaCodEFR = 4
Const lColFgAnaDescrEFR = 5
Const lColFgAnaNrBenef = 6
Const lColFgAnaPreco = 7
Const lColFgAnaDestino = 8
'NELSONPSILVA GLINTT-HS-21312
Const lColFgAnaNAmostras = 9
'
'BRUNODSANTOS GLINTT-HS-15750
Const lColFgAnaConsenteEnvioPDS = 10
'
Const lColFgAnaRecCodAna = 0
Const lColFgAnaRecDescrAna = 1
Const lColFgAnaRecQtd = 2
Const lColFgAnaRecTaxa = 3

Const lColFgMoviFactCodRubr = 0
Const lColFgMoviFactDescrRubr = 1
Const lColFgMoviFactCodRubrEfr = 2
Const lColFgMoviFactQtd = 3
Const lColFgMoviFactValPrUDoente = 4
Const lColFgMoviFactValPagDoe = 5
Const lColFgMoviFactEstadoDoe = 6
Const lColFgMoviFactSerieFacDoe = 7
Const lColFgMoviFactNFacDoe = 8
Const lColFgMoviFactValTaxa = 9
Const lColFgMoviFactEstadoTx = 10
Const lColFgMoviFactSerieFacTx = 11
Const lColFgMoviFactNFacTx = 12
Const lColFgMoviFactValPrUnit = 13
Const lColFgMoviFactValTotal = 14
Const lColFgMoviFactEstado = 15
Const lColFgMoviFactSerieFac = 16
Const lColFgMoviFactNFac = 17

Const lCodUrbano = 2
Const lCodNaoUrbano = 3
Const lCodUrbanoPortoLisboa = 1
Const lCodUrbanoBiDiario = 4

Dim linhaAnaliseOBS As Long
Dim CodEfrAUX As String
'edgar.parada Glintt-HS-19577 19.08.2019
Dim efrDescrAUX As String
'
' ESTRUTURA COM ANALISES PERTENCENTES A UM RECIBO
Private Type AnaRecibo
    codAna As String
    descrAna As String
    taxa As String
    qtd As Integer
    tipo As String  ' MARCADA - MANUAL
    indice As Long  ' INDICE NA ESTRUTURA
    Flg_adicionada As Integer
End Type
Dim EstrutAnaRecibo() As AnaRecibo
Dim TotalAnaRecibo As Integer


Const rosa = &HFFC0FF
Const azul2 = &HFFFFC0
Const CorBorla = rosa
Const CorIsento = azul2

Dim TuboColunaActual As Long
Dim TuboLinhaActual As Long

Dim flgEmissaoRecibos As Boolean

' -------------------------------------------------------------------
' COLUNAS NA GRELHA RECIBOS
' -------------------------------------------------------------------
Const lColRecibosTipo = 0
Const lColRecibosNumDoc = 1
Const lColRecibosEntidade = 2
Const lColRecibosValorPagar = 3
Const lColRecibosCaucao = 8
Const lColRecibosModoPag = 9
Const lColRecibosDtEmissao = 5
Const lColRecibosDtPagamento = 6
Const lColRecibosEstado = 7
Const lColRecibosPercentagem = 4
Const lColRecibosBorla = 10
Const lColRecibosNaoConfmidade = 11

Const lColAdiantamentosNumDoc = 0
Const lColAdiantamentosValor = 1
Const lColAdiantamentosEstado = 2
Const lColAdiantamentosDtEmissao = 3
Const lColAdiantamentosDtAnulacao = 4
Dim flg_Aberto As Boolean
Dim flg_preencheCampos As Boolean
Dim flg_ReqBloqueada As Boolean
Dim flg_codBarras As Boolean
Dim Acumula As String
Dim cabecalho As String

Private Type entidades
    cod_efr As String
    descr_efr As String
    flg_perc_facturar As String
    flg_controla_isencao As String
    flg_obriga_recibo As String
    deducao_ute As String
    flg_acto_med As String
    cod_empresa As String
    flg_compart_dom As String
    flg_dom_defeito As String
    flg_nao_urbano As String
    flg_65anos As String
    flg_separa_dom As String
    flg_insere_cod_rubr_efr As String
    flgActivo As Boolean
    flg_novaArs As Integer
End Type
Dim EstrutEFR() As entidades
Dim totalEFR As Integer

Const laranja = &HC0E0FF

Private Type informacao
    cod_ana As String
    cod_informacao As String
    descr_informacao As String
    informacao As String
End Type
Dim EstrutInfo() As informacao
Dim totalInfo As Integer

Private Type Questoes
    Resposta As String
    cod_questao As String
    descr_questao As String
End Type
Dim EstrutQuestao() As Questoes
Dim totalQuestao As Integer

Dim flg_novaArs As Integer

Dim dataAct As String
Const vermelhoClaro = &HBDC8F4

Dim IndiceHistorico As Integer
Dim numHistoricos As Integer
Dim cod_proven_efr As String

Dim lObrigaTerap As Boolean
Dim lObrigaInfoCli As Boolean
'BRUNODSANTOS 09.06.2017 LRV-469
Dim codSalaAux As Integer
'
'RGONCALVES 17.02.2015 CHSJ-1785
Dim A_PREENCHER_CAMPOS As Boolean


Private docFinanceiros() As Recibo
Private totalDodFinanceiros As Integer
Const EstadoCheckboxEnvioPDS_Checked = "�"
Const EstadoCheckboxEnvioPDS_Unchecked = "q"
'
'NELSONPSILVA Glintt-HS-18011 04.04.2018 - Acesso contextualizado quando chamado pelo form anterior (Log RGPD)
Public ExternalAccess As Boolean
'
'Dim AuxRecVal As String
'edgar.parada Glintt-HS-19165 * cor para os campos obrigat�rios
Const corObrigatorio = &HC0FFFF
Private anaExistente As Boolean

Private Type ESPCredMCDT
    codAna As String
    CredencialARS As String
    mcdtID As String
End Type

Private CredenciaisARS() As ESPCredMCDT
Private totalCredArs  As Integer
Private estrutOperation() As ESPOperation
Private escolheEnt As Boolean
Private notAtualizouEFR As Boolean
'
 Dim colRec As Integer
'edgar.parada LJMANSO-351 10.09.2018
Dim indexCred As Integer
Dim indexAna  As Integer
'

Sub Elimina_Mareq(codAgrup As String)

    Dim i As Integer

    On Error GoTo Trata_Erro

    TAna.Nodes.Remove Trim(UCase(codAgrup))

    i = 1
    While i <= UBound(MaReq)
        If Trim(UCase(codAgrup)) = Trim(UCase(MaReq(i).cod_agrup)) Then
            Actualiza_Estrutura_MAReq i
        Else
            i = i + 1
        End If
    Wend
        
    If UBound(MaReq) = 0 Then ReDim MaReq(1)

Exit Sub
Trata_Erro:
    If Err.Number <> 35601 Then
        BG_LogFile_Erros Me.Name & "Elimina_Mareq: " & Err.Description
    End If
    
    Resume Next
End Sub

Sub Elimina_RegistoA(codAgrup As String)
    On Error GoTo TrataErro
    Dim i As Long

    For i = 1 To RegistosA.Count - 1
        If Trim(codAgrup) = Trim(RegistosA(i).codAna) Then
            RegistosA.Remove i
            FGAna.RemoveItem i
        End If
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "Elimina_RegistoA: " & Err.Number & " - " & Err.Description, Me.Name, "Elimina_RegistoA"
    Exit Sub
    Resume Next
End Sub

Function Insere_Nova_Analise(indice As Long, codAgrup As String, AnaMarcada As String, codBarras As Boolean, multiAna As String) As Boolean
    On Error GoTo TrataErro

    Dim descricao As String
    Dim Marcar() As String
    Dim reqComAnalise As String
    Dim DataReq As String
    Dim CodFacturavel As String
    

    
    Insere_Nova_Analise = False
    If BG_DaComboSel(CbTipoIsencao) = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "Deve indicar primeiro o tipo de isen��o!", vbInformation, "Tipo de Isen��o"
        SSTGestReq.Tab = 0
        CbTipoIsencao.SetFocus
        Exit Function
    End If
    If BG_DaComboSel(CbDestino) = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "Deve indicar primeiro o destino dos resultados!", vbInformation, "Destino"
        SSTGestReq.Tab = 0
        CbDestino.SetFocus
        Exit Function
    End If
    

    descricao = SELECT_Descr_Ana(codAgrup)
    If codAgrup <> AnaMarcada Then
        If Mid(codAgrup, 2) <> UCase(AnaMarcada) Then
            CodFacturavel = AnaMarcada
        Else
            CodFacturavel = codAgrup
        End If
        
    Else
        CodFacturavel = codAgrup
    End If
    If descricao = "" Then
        BG_Mensagem mediMsgBox, "An�lise inexistente!", vbOKOnly + vbInformation, "An�lises"
        EcAuxAna.text = ""
    Else
        If VerificaRegraSexoMarcar(codAgrup, EcSeqUtente) = False Then
            BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
            EcAuxAna.text = ""
            Insere_Nova_Analise = False
            Exit Function
        End If
        gColocaDataChegada = ""
        If Verifica_Ana_Prod(codAgrup, Marcar) Then
            If Trim(RegistosA(indice).codAna) <> "" Then
                Elimina_Mareq RegistosA(indice).codAna
                RegistosA(indice).codAna = ""
            End If
        
            ' --------------------------------------------------------------------------------
            
            If (gProcAnPendentesAoRegistar = mediSim) Then
            
                ' Indica a requisi��o mais recente com esta an�lise pendente, para este utente.
                            
                reqComAnalise = ""
                reqComAnalise = REQUISICAO_Get_Com_Analise_Pendente(UCase(EcAuxAna.text), _
                                                                    EcSeqUtente.text, _
                                                                    DataReq)
        
                If (Len(reqComAnalise)) Then
                    MsgBox "A an�lise " & UCase(EcAuxAna.text) & " est� pendente na requisi��o " & reqComAnalise & "        " & vbCrLf & _
                           "datada de " & DataReq & ".", vbInformation, " Pesquisa de An�lises Pendentes "
                End If
            
            End If
            ' --------------------------------------------------------------------------------
            Select Case Mid(Trim(UCase(codAgrup)), 1, 1)
                Case "P"
                
                    SELECT_Dados_Perfil Marcar, indice, Trim(UCase(codAgrup)), CodFacturavel, multiAna
                Case "C"
                    SELECT_Dados_Complexa Marcar, indice, Trim(UCase(codAgrup)), multiAna, , , , CodFacturavel
                Case "S"
                    If SELECT_Dados_Simples(Marcar, indice, Trim(UCase(codAgrup)), multiAna, , , , , , CodFacturavel) = False Then
                        BG_Mensagem mediMsgBox, "An�lise inexistente!", vbOKOnly + vbInformation, "An�lises"
                        EcAuxAna.text = ""
                        Exit Function
                    End If
                Case Else
            End Select
            
            
            Insere_Nova_Analise = True
        End If
        
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Insere_Nova_Analise: " & Err.Number & " - " & Err.Description, Me.Name, "Insere_Nova_Analise"
    Insere_Nova_Analise = False
    Exit Function
    Resume Next
End Function

Sub InsereAutoProd(CodProd As String, DtChega As String)
    On Error GoTo TrataErro

    Dim LastLastRP As Integer
    Dim i As Integer
    
    EcAuxProd.text = CodProd
    
    LastLastRP = LastRowP
    LastRowP = FgProd.rows - 1
    
    'Verificar se o produto j� est� na lista
    For i = 1 To LastRowP
        If Trim(FgProd.TextMatrix(i, 0)) = Trim(CodProd) Then
            Exit Sub
        End If
    Next
    
    If SELECT_Descr_Prod = True Then
        ExecutaCodigoP = False
        
        If Trim(DtChega) <> "" Then
            FgProd.TextMatrix(LastRowP, 5) = Trim(DtChega)
            RegistosP(LastRowP).DtPrev = Trim(DtChega)
            FgProd.TextMatrix(LastRowP, 6) = Trim(DtChega)
            RegistosP(LastRowP).DtChega = Trim(DtChega)
            gColocaDataChegada = Trim(DtChega)
            If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto Then
                EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
            End If
        Else
            FgProd.TextMatrix(LastRowP, 5) = EcDataPrevista
            RegistosP(LastRowP).DtPrev = EcDataPrevista
        End If

        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
        FgProd.TextMatrix(LastRowP, 7) = RegistosP(LastRowP).EstadoProd
                        
        FgProd.TextMatrix(LastRowP, 0) = UCase(CodProd)
        RegistosP(LastRowP).CodProd = UCase(CodProd)
        
        'Cria linha vazia
        FgProd.AddItem ""
        CriaNovaClasse RegistosP, FgProd.rows - 1, "PROD"
        
        ExecutaCodigoP = True
        
        FgProd.row = FgProd.rows - 1
        FgProd.Col = 0
        LastColP = FgProd.Col
        LastRowP = FgProd.row
    Else
        LastRowP = LastLastRP
    End If
    
    EcAuxProd.text = ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "InsereAutoProd: " & Err.Number & " - " & Err.Description, Me.Name, "InsereAutoProd"
    Exit Sub
    Resume Next
End Sub



Sub InsereGhostMember(codAnaS As String)
    On Error GoTo TrataErro

    Dim RsCompl As adodb.recordset
    Dim sql As String
    Dim descricao As String
    Dim Marcar(1) As String
    Dim indice  As Long
    Dim i As Long
    Dim OrdAna As Long
    Dim CodPerfil As String
    Dim DescrPerfil As String
    Dim OrdPerf As Integer
    Dim CodAnaC As String
    Dim DescrAnaC As String

codAnaS = Trim(codAnaS)

'For�ar a marca��o
Marcar(1) = codAnaS & ";"

indice = -1
For i = 1 To UBound(MaReq)
    If Trim(GhostPerfil) = Trim(MaReq(i).Cod_Perfil) And _
       Trim(GhostComplexa) = Trim(MaReq(i).cod_ana_c) Then
       indice = i
       Exit For
    End If
Next i

If indice = -1 Then
    BG_Mensagem mediMsgBox, "Erro a inserir an�lise!", vbCritical + vbOKOnly, App.ProductName
    Exit Sub
End If

gColocaDataChegada = MaReq(indice).dt_chega

'Dados da Complexa
sql = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c = " & BL_TrataStringParaBD(GhostComplexa)
Set RsCompl = New adodb.recordset
With RsCompl
    .Source = sql
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    .Open , gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
End With

If RsCompl.RecordCount > 0 Then
    descricao = BL_HandleNull(RsCompl!Descr_Ana_C, "?")
Else
    BG_LogFile_Erros Me.Name & " - SELECT_Dados_Complexa: Erro a seleccionar dados da complexa!"
End If
RsCompl.Close
Set RsCompl = Nothing

OrdAna = MaReq(indice).Ord_Ana
CodPerfil = MaReq(indice).Cod_Perfil
DescrPerfil = MaReq(indice).Descr_Perfil
OrdPerf = MaReq(indice).Ord_Ana_Perf
CodAnaC = MaReq(indice).cod_ana_c
DescrAnaC = MaReq(indice).Descr_Ana_C

'Dados do Membro da complexa (CodAnaS)
sql = "SELECT ordem FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(GhostComplexa) & _
        " AND cod_membro = " & BL_TrataStringParaBD(codAnaS)
Set RsCompl = New adodb.recordset
With RsCompl
    .Source = sql
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    .Open , gConexao
End With

If Not RsCompl.EOF Then
    If GhostPerfil <> "0" Then
        SELECT_Dados_Simples Marcar, OrdAna, codAnaS, "", CodPerfil, DescrPerfil, OrdPerf, CodAnaC, DescrAnaC, BL_HandleNull(RsCompl!ordem, " ")
    Else
        SELECT_Dados_Simples Marcar, OrdAna, codAnaS, "", , , , CodAnaC, DescrAnaC, BL_HandleNull(RsCompl!ordem, " ")
    End If
End If

RsCompl.Close
Set RsCompl = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "InsereGhostMember: " & Err.Number & " - " & Err.Description, Me.Name, "InsereGhostMember"
    Exit Sub
    Resume Next
End Sub

Function Pesquisa_RegistosA(codAgrup As String) As Long
    On Error GoTo TrataErro

    Dim i As Long
    Dim lRet As Long
    
    lRet = -1
    
    For i = 1 To RegistosA.Count - 1
        If Trim(codAgrup) = Trim(RegistosA(i).codAna) Then
            lRet = i
            Exit For
        End If
    Next i
    
    Pesquisa_RegistosA = lRet
Exit Function
TrataErro:
    BG_LogFile_Erros "Pesquisa_RegistosA: " & Err.Number & " - " & Err.Description, Me.Name, "Pesquisa_RegistosA"
    Pesquisa_RegistosA = -1
    Exit Function
    Resume Next
End Function

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    Inicializacoes
    flgEmissaoRecibos = False

    Max = -1
    ReDim FOPropertiesTemp(0)
    
    DefTipoCampos
    
    EcUtente.text = ""
    DoEvents
    
    PreencheValoresDefeito
        
    gF_REQUIS_PRIVADO = 1
    Flg_PesqUtente = False
    
    Estado = 1
    BG_StackJanelas_Push Me
        
    SSTGestReq.Tab = 0
    DoEvents
    Flg_DadosAnteriores = False

End Sub

Sub Inicializacoes()
    Dim i As Integer
    On Error GoTo TrataErro
    'edgar.parada Liga��o Secund�ria
    BL_Abre_Conexao_Secundaria gOracle
    '
    
    If Req_FDS = True Then
        cabecalho = " Gest�o de Requisi��es - FIM DE SEMANA"
    Else
        cabecalho = " Gest�o de Requisi��es - SEMANA"
    End If
    Me.caption = cabecalho
    Me.left = 5
    Me.top = 20
    Me.Width = 13275
    Me.Height = 9390
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_requis"
    Set CampoDeFocus = EcNumReq
    NumCampos = 63
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim CoresCamposEc(0 To NumCampos - 1)
    ReDim CamposEcBckp(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "n_req"
    CamposBD(1) = "seq_utente"
    CamposBD(2) = "dt_previ"
    CamposBD(3) = "dt_chega"
    CamposBD(4) = "dt_imp"
    CamposBD(5) = "t_urg"
    CamposBD(6) = "cod_efr"
    CamposBD(7) = "n_benef"
    If gListaMedicos = 1 Then
        CamposBD(8) = "cod_med"
    Else
        CamposBD(8) = "descr_medico"
    End If
    CamposBD(9) = "obs_req"
    CamposBD(10) = "hr_imp"
    CamposBD(11) = "user_imp"
    CamposBD(12) = "dt_imp2"
    CamposBD(13) = "hr_imp2"
    CamposBD(14) = "user_imp2"
    CamposBD(15) = "estado_req"
    CamposBD(16) = "user_cri"
    CamposBD(17) = "hr_cri"
    CamposBD(18) = "dt_cri"
    CamposBD(19) = "user_act"
    CamposBD(20) = "hr_act"
    CamposBD(21) = "dt_act"
    CamposBD(22) = "cod_isencao"
    CamposBD(23) = "gr_ana"
    CamposBD(24) = "hr_chega"
    CamposBD(25) = "cod_local"
    CamposBD(26) = "user_fecho"
    CamposBD(27) = "dt_fecho"
    CamposBD(28) = "hr_fecho"
    CamposBD(29) = "inf_complementar"
    CamposBD(30) = "cod_t_colheita"
    CamposBD(31) = "cod_sala"
    CamposBD(32) = "cod_destino"
    CamposBD(33) = "hemodialise"
    CamposBD(34) = "morada"
    CamposBD(35) = "cod_postal"
    CamposBD(36) = "n_req_assoc"
    CamposBD(37) = "cod_proven"
    CamposBD(38) = "cod_urbano"
    CamposBD(39) = "km"
    CamposBD(40) = "convencao"
    CamposBD(41) = "fim_semana"
    CamposBD(42) = "FLG_COBRA_DOMICILIO"
    CamposBD(43) = "flg_facturado"
    CamposBD(44) = "dt_pretend"
    CamposBD(45) = "flg_aviso_sms"
    CamposBD(46) = "flg_sms_enviada"
    CamposBD(47) = "dt_conclusao"
    CamposBD(48) = "user_confirm"
    CamposBD(49) = "dt_confirm"
    CamposBD(50) = "hr_confirm"
    CamposBD(51) = "obs_proven"
    CamposBD(52) = "flg_hipo"
    CamposBD(53) = "num_doe_prof"
    CamposBD(54) = "cod_pais"
    CamposBD(55) = "user_sms"
    CamposBD(56) = "dt_sms"
    CamposBD(57) = "hr_sms"
    CamposBD(58) = "user_email"
    CamposBD(59) = "dt_email"
    CamposBD(60) = "hr_email"
    CamposBD(61) = "flg_diag_sec"
    CamposBD(62) = "cod_local_entrega"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcSeqUtente
    Set CamposEc(2) = EcDataPrevista
    Set CamposEc(3) = EcDataChegada
    Set CamposEc(4) = EcDataImpressao
    Set CamposEc(5) = CbUrgencia
    Set CamposEc(6) = EcCodEFR
    Set CamposEc(7) = EcNumBenef
    If gListaMedicos = 1 Then
        Set CamposEc(8) = EcCodMedico
    Else
        Set CamposEc(8) = EcDescrMedico
    End If
    Set CamposEc(9) = EcObsReq
    Set CamposEc(10) = EcHoraImpressao
    Set CamposEc(11) = EcUtilizadorImpressao
    Set CamposEc(12) = EcDataImpressao2
    Set CamposEc(13) = EcHoraImpressao2
    Set CamposEc(14) = EcUtilizadorImpressao2
    Set CamposEc(15) = EcEstadoReq
    Set CamposEc(16) = EcUtilizadorCriacao
    Set CamposEc(17) = EcHoraCriacao
    Set CamposEc(18) = EcDataCriacao
    Set CamposEc(19) = EcUtilizadorAlteracao
    Set CamposEc(20) = EcHoraAlteracao
    Set CamposEc(21) = EcDataAlteracao
    Set CamposEc(22) = EcCodIsencao
    Set CamposEc(23) = EcGrupoAna
    Set CamposEc(24) = EcHoraChegada
    Set CamposEc(25) = EcLocal
    Set CamposEc(26) = EcUtilizadorFecho
    Set CamposEc(27) = EcDataFecho
    Set CamposEc(28) = EcHoraFecho
    Set CamposEc(29) = EcinfComplementar
    Set CamposEc(30) = EcPrColheita
    Set CamposEc(31) = EcCodSala
    Set CamposEc(32) = CbDestino
    Set CamposEc(33) = EcHemodialise
    Set CamposEc(34) = EcMorada
    Set CamposEc(35) = EcCodPostalAux
    Set CamposEc(36) = EcNumReqAssoc
    Set CamposEc(37) = EcCodProveniencia
    Set CamposEc(38) = CbCodUrbano
    Set CamposEc(39) = EcKm
    Set CamposEc(40) = CbConvencao
    Set CamposEc(41) = EcFimSemana
    Set CamposEc(42) = CkCobrarDomicilio
    Set CamposEc(43) = EcFlgFacturado
    Set CamposEc(44) = EcDtPretend
    Set CamposEc(45) = CkAvisarSMS
    Set CamposEc(46) = EcSMSEnviada
    Set CamposEc(47) = EcDtEntrega
    Set CamposEc(48) = EcUtilConf
    Set CamposEc(49) = EcDtConf
    Set CamposEc(50) = EcHrConf
    Set CamposEc(51) = EcObsProveniencia
    Set CamposEc(52) = CkReqHipocoagulados
    Set CamposEc(53) = EcDoenteProf
    Set CamposEc(54) = EcCodPais
    Set CamposEc(55) = EcUtilSMS
    Set CamposEc(56) = EcDtSMS
    Set CamposEc(57) = EcHrSMS
    Set CamposEc(58) = EcUtilEmail
    Set CamposEc(59) = EcDtEmail
    Set CamposEc(60) = EcHrEmail
    Set CamposEc(61) = EcFlgDiagSec
    Set CamposEc(62) = EcCodLocalEntrega
    
    For i = 0 To NumCampos - 1
        CoresCamposEc(i) = CamposEc(i).BackColor
    Next
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Utente"
    TextoCamposObrigatorios(2) = "Data activa��o da requisi��o"
    TextoCamposObrigatorios(5) = "Tipo de urg�ncia"
    TextoCamposObrigatorios(40) = "Tipo de Conven��o"
    TextoCamposObrigatorios(41) = "Fim de Semana"
    If gLAB = "CL" Then
        TextoCamposObrigatorios(32) = "Destino dos Resultados"
    End If
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "n_req"
    Set ChaveEc = EcNumReq
                
    'inicializa��es das estruturas de mem�ria (an�lises e recibos)
    Inicializa_Estrutura_Analises
    
    'O TAB de an�lises est� desactivado at� se introduzir uma entidade financeira
    ' e o de produtos at� introduzir uma data prevista
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabEnabled(5) = False
    SSTGestReq.TabEnabled(6) = False

    FlgARS = False
    FlgADSE = False
    gFlg_JaExisteReq = False
    Flg_LimpaCampos = False
    
    BtPesquisaEspecif.Enabled = False
    BtPesquisaProduto.Enabled = False
    
    'colocar o bot�o de cancelar requisi��es invisivel
    BtCancelarReq.Visible = False
    
    
    'Inicializa comando para ir buscar as an�lises da requisi��o e os seus dados
    
    CmdAnaReq = "( Select  -1 seq_realiza, m.n_folha_trab, m.n_req, m.dt_chega, m.cod_agrup, m.ord_ana_perf, m.ord_ana_compl, m.ord_ana, m.cod_perfil, descr_perfis, m.cod_ana_c, descr_ana_c, m.cod_ana_s, descr_ana_s, m.flg_apar_trans, '-1' as flg_estado, m.flg_facturado,m.ord_marca, m.transmit_psm, m.n_envio, re.n_prescricao, re.cod_destino from sl_marcacoes m LEFT OUTER JOIN sl_perfis p ON p.cod_perfis = m.cod_perfil LEFT OUTER JOIN sl_ana_c c on c.cod_ana_c = m.cod_ana_c LEFT OUTER JOIN sl_ana_s s ON s.cod_ana_s = m.cod_ana_s LEFT OUTER JOIN sl_req_episodio re on m.cod_agrup = re.cod_agrup and m.n_req =re.n_req where m.n_req =  ?  )" & _
                            " Union " & _
                            "( select  r.seq_realiza, r.n_folha_trab, r.n_req, r.dt_chega, r.cod_agrup, r.ord_ana_perf, r.ord_ana_compl, r.ord_ana, r.cod_perfil, descr_perfis, r.cod_ana_c, descr_ana_c, r.cod_ana_s, descr_ana_s, '-1' as flg_apar_trans, r.flg_estado, r.flg_facturado,r.ord_marca,1 transmit_psm, 1 n_envio, re.n_prescricao, re.cod_destino from  sl_realiza r LEFT OUTER JOIN sl_perfis p ON p.cod_perfis = r.cod_perfil LEFT OUTER JOIN sl_ana_c c ON c.cod_ana_c = r.cod_ana_c LEFT OUTER JOIN sl_ana_s s ON s.cod_ana_s = r.cod_ana_s  LEFT OUTER JOIN sl_req_episodio re on r.cod_agrup = re.cod_agrup  and r.n_req =re.n_req where r.n_req =  ? )" & _
                            "order by ord_marca, ord_ana, cod_agrup, ord_ana_perf, ord_ana_compl"
    
    ' HISTORICO
    CmdAnaReq_H = " select  h.seq_realiza,h.n_folha_trab, h.n_req, h.dt_chega, h.cod_agrup, h.ord_ana_perf, h.ord_ana_compl, h.ord_ana, h.cod_perfil, descr_perfis, h.cod_ana_c, descr_ana_c, h.cod_ana_s, descr_ana_s, '-1' as flg_apar_trans, h.flg_estado, h.flg_facturado,h.ord_marca, 1 transmit_psm, 1 n_envio from  sl_realiza_h h LEFT OUTER JOIN sl_perfis p ON p.cod_perfis = h.cod_perfil LEFT OUTER JOIN sl_ana_c c ON c.cod_ana_c = h.cod_ana_c LEFT OUTER JOIN sl_ana_s s ON s.cod_ana_s = h.cod_ana_s  where h.n_req =  ? " & _
                  "order by ord_marca, ord_ana, cod_agrup, ord_ana_perf, ord_ana_compl"
            
        
    'Inicializa comando para ir buscar a ordem e o grupo das an�lises
    Set CmdOrdAna.ActiveConnection = gConexao
    CmdOrdAna.CommandType = adCmdText
    CmdOrdAna.CommandText = " SELECT ordem, gr_ana FROM sl_ana_s WHERE cod_ana_s = ? " & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_ana_c WHERE cod_ana_c = ? " & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_perfis WHERE cod_perfis = ? "
    CmdOrdAna.Prepared = True

        
    'Inicializa��o das variaveis que guardam os indices nas combos de pagamentos
    CbTMIndex = -1
    CbUteIndex = -1
    CbEntIndex = -1
    
    EcPrinterRecibo = BL_SelImpressora("ReciboReq.rpt")
    EcPrinterEtiq = BL_SelImpressora("Etiqueta.rpt")
    
    FrameObsEspecif.Visible = False
    FrameObsEspecif.left = 6120
    FrameObsEspecif.top = 360
    
    Flg_Gravou = False
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    If EcFimSemana = "" Then
        If Req_FDS = True Then
            EcFimSemana = "1"
        Else
            EcFimSemana = "0"
        End If
    End If
    flg_preencheCampos = False
    
    'RGONCALVES 16.06.2013 ICIL-470
    If gEnvioSMSActivo <> mediSim And gEnvioResultadosSMSActivo <> mediSim Then
        CkAvisarSMS.Visible = False
    End If
    '
    'BRUNODSANTOS 08.03.2016 - Glintt-HS-11792
    FrameAdiantamentos.left = 120
    FrameAdiantamentos.top = 480
    FrameAnaRecibos.left = 120
    FrameAnaRecibos.top = 480
    '
    'LJMANSO-302
    'AuxRecVal = ""
    colRec = 0
        'edgar.parada Glintt-HS-19165
    btCativarReq.Visible = False
    lbCativar.Visible = False
    BtHistCred.Visible = False
    anaExistente = True
    If gAtivaESP = mediSim Then
        BtHistCred.Visible = True
        If FormGestaoRequisicaoPrivado.ExternalAccess = True Then
            lbCativar.Visible = True
            btCativarReq.Visible = True
        End If
    End If
    totalCredArs = 1
    ReDim estrutOperation(0)
    ReDim CredenciaisARS(0)
    escolheEnt = True
    notAtualizouEFR = True
    '
    '
Exit Sub
TrataErro:
    BG_LogFile_Erros "Inicializacoes: " & Err.Number & " - " & Err.Description, Me.Name, "Inicializacoes"
    Exit Sub
    Resume Next
End Sub

Function FuncaoProcurar_PreencheProd(NumReq As Long)
    On Error GoTo TrataErro
    
    ' Selecciona todos os produtos da requisi��o e preenche a lista

    Dim i As Long
    Dim rsDescr As adodb.recordset
    Dim RsProd As adodb.recordset
    Dim CmdDescrP As New adodb.Command
    Dim PmtCodProd As adodb.Parameter
    Dim CmdDescrE As New adodb.Command
    Dim PmtCodEsp As adodb.Parameter
      
    Set RsProd = New adodb.recordset
    With RsProd
        .Source = "SELECT cod_prod, cod_especif,dt_previ, dt_chega, volume, obs_especif FROM sl_req_prod " & _
                   "WHERE n_req =" & NumReq
                   
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockReadOnly
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With

    'Inicializa o comando ADO para selec��o da descri��o do produto
    Set CmdDescrP.ActiveConnection = gConexao
    CmdDescrP.CommandType = adCmdText
    CmdDescrP.CommandText = "SELECT descr_produto,especif_obrig FROM sl_produto WHERE " & _
                            "cod_produto =?"
    CmdDescrP.Prepared = True
    Set PmtCodProd = CmdDescrE.CreateParameter("COD_PRODUTO", adVarChar, adParamInput, 5)
    CmdDescrP.Parameters.Append PmtCodProd
    
    'Inicializa o comando ADO para selec��o da descri��o da especifica��o
     Set CmdDescrE.ActiveConnection = gConexao
     CmdDescrE.CommandType = adCmdText
     CmdDescrE.CommandText = "SELECT descr_especif FROM sl_especif WHERE " & _
                              "cod_especif =?"
     CmdDescrE.Prepared = True
     Set PmtCodEsp = CmdDescrE.CreateParameter("COD_ESPECIF", adVarChar, adParamInput, 5)
     CmdDescrE.Parameters.Append PmtCodEsp
     
     i = 1
     LimpaFgProd
     While Not RsProd.EOF
        RegistosP(i).CodProd = RsProd!cod_prod
        RegistosP(i).Volume = BL_HandleNull(RsProd!Volume, "")
        CmdDescrP.Parameters(0).value = Trim(UCase(RsProd!cod_prod))
        Set rsDescr = CmdDescrP.Execute
        RegistosP(i).DescrProd = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(i).EspecifObrig = rsDescr!especif_obrig
        RegistosP(i).ObsEspecif = BL_HandleNull(RsProd!obs_especif, "")
        rsDescr.Close

        RegistosP(i).CodEspecif = BL_HandleNull(RsProd!cod_Especif, "")
        'descri��o da especifica��o
        If Trim(RegistosP(i).CodEspecif) <> "" Then
            CmdDescrE.Parameters(0) = UCase(RsProd!cod_Especif)
            Set rsDescr = CmdDescrE.Execute
            RegistosP(i).DescrEspecif = BL_HandleNull(rsDescr!descr_especif, "")
            rsDescr.Close
        End If
        RegistosP(i).DtPrev = BL_HandleNull(RsProd!dt_previ, "")
        RegistosP(i).DtChega = BL_HandleNull(RsProd!dt_chega, "")
        
        RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
        
        FgProd.TextMatrix(i, 0) = RegistosP(i).CodProd
        FgProd.TextMatrix(i, 1) = RegistosP(i).DescrProd
        FgProd.TextMatrix(i, 2) = RegistosP(i).CodEspecif
        FgProd.TextMatrix(i, 3) = RegistosP(i).DescrEspecif
        FgProd.TextMatrix(i, 4) = RegistosP(i).Volume
        FgProd.TextMatrix(i, 5) = RegistosP(i).DtPrev
        FgProd.TextMatrix(i, 6) = RegistosP(i).DtChega
        FgProd.TextMatrix(i, 7) = RegistosP(i).EstadoProd
        
        'Se h� produtos que j� chegaram ent�o estado n�o deve estar "� espera de prod."
        If (Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Or Trim(EcEstadoReq) = gEstadoReqEsperaProduto) And Trim(FgProd.TextMatrix(i, 5)) <> "" Then
            EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        End If
        
        RsProd.MoveNext
        i = i + 1
        CriaNovaClasse RegistosP, i, "PROD"
        FgProd.AddItem "", i
        FgProd.row = i
     Wend
            
     Set CmdDescrE = Nothing
     Set CmdDescrP = Nothing
     Set rsDescr = Nothing
     Set RsProd = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar_PreencheProd: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar_PreencheProd"
    Exit Function
    Resume Next
End Function

Sub FuncaoProcurar_PreencheAna(NumReq As Long)
    On Error GoTo TrataErro

    Dim i As Long
    Dim k As Long
    Dim MudaIcon As Integer
    Dim LastIntermedio As String
    Dim LastPai As String
    Dim Pai As String
    Dim Ja_Existe As Boolean
    Dim rsAna As adodb.recordset
    Dim EstadoFinal As Integer
    Dim EArs As Boolean
    Dim ImgPai As Integer
    Dim ImgPaiSel As Integer
    Dim ObsAnaReq As String
    Dim seqObsAnaReq As Long
    Dim sql As String
    Dim flg_estado_aux As String
    
    ' PARA REQUISICOES QUE ESTAO NO HISTORICO APENAS VAI TABELA SL_REALIZA_H
    If EcEstadoReq = gEstadoReqHistorico Then
        sql = Replace(CmdAnaReq_H, "?", NumReq)
    Else
        sql = Replace(CmdAnaReq, "?", NumReq)
    End If
    
    Set rsAna = New adodb.recordset
    With rsAna
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .LockType = adLockPessimistic
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open
    End With
i = 1
LimpaFGAna
ExecutaCodigoA = False
ContP1 = 0
FGAna.Col = 0
LastIntermedio = ""
LastPai = ""
Pai = ""
MudaIcon = 0
ImgPai = 5
ImgPaiSel = 6

EArs = EFR_Verifica_ARS(EcCodEFR)

While Not rsAna.EOF
    
    flg_estado_aux = BL_HandleNull(rsAna!flg_estado, "-1")
    If BL_HandleNull(rsAna!flg_estado, "-1") <> "-1" Then
        If VerificaAnaliseJaExisteRealiza(NumReq, BL_HandleNull(rsAna!Cod_Perfil, ""), BL_HandleNull(rsAna!cod_ana_c, ""), BL_HandleNull(rsAna!cod_ana_s, ""), False) = 0 Then
            flg_estado_aux = -1
        Else
            flg_estado_aux = BL_HandleNull(rsAna!flg_estado, "-1")
        End If
    Else
        flg_estado_aux = BL_HandleNull(rsAna!flg_estado, "-1")
    End If
    
        'Debug.Print RsAna!Cod_Ana_C & "-" & RsAna!cod_ana_s
        BL_Preenche_MAReq FGTubos, BL_HandleNull(rsAna!Cod_Perfil, ""), _
                        BL_HandleNull(rsAna!descr_perfis, ""), _
                        BL_HandleNull(rsAna!cod_ana_c, ""), _
                        BL_HandleNull(rsAna!Descr_Ana_C, ""), _
                        BL_HandleNull(rsAna!cod_ana_s, ""), _
                        BL_HandleNull(rsAna!descr_ana_s, ""), _
                        BL_HandleNull(rsAna!Ord_Ana, 0), _
                        BL_HandleNull(rsAna!Ord_Ana_Compl, 0), _
                        BL_HandleNull(rsAna!Ord_Ana_Perf, 0), _
                        BL_HandleNull(rsAna!cod_agrup, ""), _
                        BL_HandleNull(rsAna!N_Folha_Trab, 0), _
                        BL_HandleNull(rsAna!dt_chega, ""), _
                        BL_HandleNull(rsAna!flg_apar_trans, "0"), _
                        flg_estado_aux, _
                        BL_HandleNull(rsAna!Flg_Facturado, 0), _
                         BL_HandleNull(rsAna!Ord_Marca, 0), _
                         BL_HandleNull(rsAna!transmit_psm, 0), _
                         BL_HandleNull(rsAna!n_envio, 0), _
                         BL_HandleNull(EcDataPrevista, "")

        Ja_Existe = False
        For k = 1 To RegistosA.Count
            If Trim(RegistosA(k).codAna) = Trim(MaReq(i).cod_agrup) Then
                Ja_Existe = True
                Exit For
            End If
        Next k


        If Flg_DadosAnteriores = True Then
            'For�a o estado da an�lise para sem resultado quando se copia uma requisi��o
            MaReq(i).flg_estado = "-1"
        End If
        
        If MaReq(i).flg_estado = "-1" Then
            MudaIcon = 0
        Else
            MudaIcon = 6
        End If

        If Not Ja_Existe Then
            AdicionaDadosMoviFact MaReq(i).cod_agrup, mediComboValorNull, mediComboValorNull, mediComboValorNull, mediComboValorNull
            LbTotalAna = LbTotalAna + 1
            RegistosA(FGAna.rows - 1).codAna = MaReq(i).cod_agrup
            If Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).Cod_Perfil) Then
                RegistosA(FGAna.rows - 1).descrAna = MaReq(i).Descr_Perfil
                ImgPai = 1 + MudaIcon
                ImgPaiSel = 2 + MudaIcon
            ElseIf Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).cod_ana_c) Then
                RegistosA(FGAna.rows - 1).descrAna = MaReq(i).Descr_Ana_C
                ImgPai = 3 + MudaIcon
                ImgPaiSel = 4 + MudaIcon
            ElseIf Trim(MaReq(i).cod_agrup) = Trim(MaReq(i).cod_ana_s) Then
                RegistosA(FGAna.rows - 1).descrAna = MaReq(i).descr_ana_s
                ImgPai = 5 + MudaIcon
                ImgPaiSel = 6 + MudaIcon
            Else
                RegistosA(FGAna.rows - 1).descrAna = ""
                ImgPai = 5 + MudaIcon
                ImgPaiSel = 6 + MudaIcon
            End If
            
            ObsAnaReq = ""
            seqObsAnaReq = 0
            BL_DevolveObsAnaReq NumReq, MaReq(i).cod_agrup, ObsAnaReq, seqObsAnaReq
            RegistosA(FGAna.rows - 1).ObsAnaReq = ObsAnaReq
            RegistosA(FGAna.rows - 1).seqObsAnaReq = seqObsAnaReq
            RegistosA(FGAna.rows - 1).n_prescricao = BL_HandleNull(rsAna!n_prescricao, "1")
            RegistosA(FGAna.rows - 1).cod_destino = BL_HandleNull(rsAna!cod_destino, "")
            If RegistosA(FGAna.rows - 1).cod_destino = "" And CbDestino.ListIndex > mediComboValorNull Then
                RegistosA(FGAna.rows - 1).cod_destino = CbDestino.ItemData(CbDestino.ListIndex)
            End If
            If RegistosA(FGAna.rows - 1).cod_destino <> "" Then
                RegistosA(FGAna.rows - 1).descr_destino = BL_SelCodigo("SL_TBF_T_DESTINO", "DESCR_T_DEST", "COD_T_DEST", RegistosA(FGAna.rows - 1).cod_destino)
            End If
            ' ------------------------------------------------------------------------------------------
            ' PREENCHE DADOS DOS RECIBOS ASSOCIADO A ANALISE EM CAUSA
            ' ------------------------------------------------------------------------------------------
            FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaCodigo) = RegistosA(FGAna.rows - 1).codAna
            FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaDescricao) = RegistosA(FGAna.rows - 1).descrAna
            FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaDestino) = RegistosA(FGAna.rows - 1).n_prescricao & "-" & RegistosA(FGAna.rows - 1).descr_destino
            RegistosA(FGAna.rows - 1).DtChega = BL_HandleNull(rsAna!dt_chega, "")
            
            If MaReq(i).flg_estado <> "-1" And Trim(FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaCodigo)) <> "" Then
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbRed
            Else
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbBlack
            End If
            
            'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
            If gUsaEnvioPDS = mediSim Then
                Call ChangeCheckboxState(FGAna.rows - 1, lColFgAnaConsenteEnvioPDS, DevolveEstadoEnvioPDS(rsAna!cod_agrup, EcNumReq.text))
                'FGAna.TextMatrix(FGAna.rows - 1, lColFgAnaConsenteEnvioPDS) = DevolveEstadoEnvioPDS(rsAna!cod_agrup, EcNumReq.Text)
            End If
            '
            
            CriaNovaClasse RegistosA, 0, "ANA"
            FGAna.AddItem ""
            FGAna.row = FGAna.rows - 1
        
        End If
        
        Pai = Trim(MaReq(i).cod_agrup)
        
        If Pai <> LastPai Then
            'Pai = codigo de agupamento (seja Perfil, Complexa ou Simples)
            BL_AcrescentaNodoPai TAna, IIf((RegistosA(FGAna.rows - 2).descrAna = ""), MaReq(i).cod_agrup, RegistosA(FGAna.rows - 2).descrAna), MaReq(i).cod_agrup, ImgPai, ImgPaiSel
            LastPai = Pai
        End If
        
        
        If Trim(MaReq(i).cod_ana_c) <> "0" And Trim(MaReq(i).cod_ana_c) <> "" And Trim(Pai) <> Trim(MaReq(i).cod_ana_c) Then
            'Se existir complexa e for diferente do Pai insere-a como intermedia entre o Pai e a simples
            If LastIntermedio = MaReq(i).cod_ana_c Then
                'j� foi inserida uma vez -> insere-se directamente a simples
                If MaReq(i).cod_ana_s = "S99999" Then
                    BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", Pai & MaReq(i).cod_ana_c, MaReq(i).cod_ana_c & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                Else
                    BL_AcrescentaNodoFilho TAna, MaReq(i).descr_ana_s, Pai & MaReq(i).cod_ana_c, MaReq(i).cod_ana_c & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                End If
            Else
                '� a primeira vez -> insere a complexa e a simples por debaixo da mesma
                LastIntermedio = MaReq(i).cod_ana_c
                BL_AcrescentaNodoFilho TAna, MaReq(i).Descr_Ana_C, Pai, Pai & MaReq(i).cod_ana_c, 3 + MudaIcon, 4 + MudaIcon
                
                'inserir a simples
                If MaReq(i).cod_ana_s = "S99999" Then
                    BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", Pai & MaReq(i).cod_ana_c, MaReq(i).cod_ana_c & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                Else
                    BL_AcrescentaNodoFilho TAna, MaReq(i).descr_ana_s, Pai & MaReq(i).cod_ana_c, MaReq(i).cod_ana_c & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                End If
            End If
        Else
            'N�o existe complexa ou � o pai
            If Trim(Pai) <> Trim(MaReq(i).cod_ana_s) Then
                'Se o pai nao for a propria simples insere-a de baixo do pai
                If MaReq(i).cod_ana_s = "S99999" Then
                    BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", Pai, Pai & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                Else
                    BL_AcrescentaNodoFilho TAna, MaReq(i).descr_ana_s, Pai, Pai & MaReq(i).cod_ana_s, 5 + MudaIcon, 6 + MudaIcon
                End If
                
            End If
        End If
        
        If MudaIcon = 6 And TAna.Nodes.Item(MaReq(i).cod_agrup).Image < 6 Then
            TAna.Nodes.Item(MaReq(i).cod_agrup).Image = TAna.Nodes.Item(MaReq(i).cod_agrup).Image + MudaIcon
            TAna.Nodes.Item(MaReq(i).cod_agrup).SelectedImage = TAna.Nodes.Item(MaReq(i).cod_agrup).SelectedImage + MudaIcon
        End If
        
        i = i + 1
        rsAna.MoveNext
        
        'Se encontrou analises o estado n�o deve estar "Sem Analises"
        If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Then
            EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        End If
Wend

For i = 1 To FGAna.rows - 2
    EstadoFinal = -1
    For k = 1 To UBound(MaReq)
        If Trim(RegistosA(i).codAna) = Trim(MaReq(k).cod_agrup) Then
            If MaReq(k).flg_estado <> "-1" Then
                EstadoFinal = 0
                Exit For
            End If
        End If
    Next k
    RegistosA(i).Estado = EstadoFinal
Next i
RegistosA(FGAna.rows - 1).Estado = "-1"

rsAna.Close
Set rsAna = Nothing

ExecutaCodigoA = True

FGAna.Col = 0
FGAna.row = 1
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar_PreencheAna: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar_PreencheAna"
    Exit Sub
    Resume Next
End Sub

Sub DefTipoCampos()
    On Error GoTo TrataErro
    dataAct = Bg_DaData_ADO
    If gLAB = "CL" Then
        CbDestino.BackColor = &HC0FFFF
    Else
        CbDestino.BackColor = vbWhite
    End If
    If gGestaoColheita <> mediSim Then
        LbNumColheita.Visible = False
        EcNumColheita.Visible = False
    Else
        LbNumColheita.Visible = True
        EcNumColheita.Visible = True
    End If
    ReDim fa_movi_resp(0)
    fa_movi_resp_tot = 0
    ReDim AnalisesFact(0)
    TotalAnalisesFact = 0
    ReDim docFinanceiros(0)
    totalDodFinanceiros = 0
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_previ", EcDataPrevista, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_chega", EcDataChegada, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_imp", EcDataImpressao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_imp", EcHoraImpressao, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_imp2", EcDataImpressao2, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "hr_imp2", EcHoraImpressao2, mediTipoHora
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_fecho", EcDataFecho, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_pretend", EcDtPretend, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_conclusao", EcDtEntrega, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_sms", EcDtSMS, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_email", EcDtEmail, mediTipoData
    
    BG_DefTipoCampoEc_ADO NomeTabela, "user_imp", EcUtilizadorImpressao, adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "user_imp2", EcUtilizadorImpressao2, adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "user_cri", EcUtilizadorCriacao, adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "user_act", EcUtilizadorAlteracao, adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_confirm", EcDtConf, mediTipoData
    
    
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    ExecutaCodigoP = False
    
    With FGAna
        .rows = 2
        .FixedRows = 1
        .Cols = 10
        'BRUNODSANTOS GLINTT-HS-15750 -> NELSONPSILVA GLINTT-HS-21312
        If gUsaEnvioPDS = mediSim Then
             .Cols = 11
        End If
        '
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeRestrictColumns
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        
        .ColWidth(lColFgAnaCodigo) = 1000
        .Col = lColFgAnaCodigo
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaCodigo) = "C�digo"
        
        .ColWidth(lColFgAnaDescricao) = 2500
        .Col = lColFgAnaDescricao
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaDescricao) = "Descri��o"
        
        .ColWidth(lColFgAnaP1) = 1900
        .Col = lColFgAnaP1
        .TextMatrix(0, lColFgAnaP1) = "Credencial"
        
        .ColWidth(lColFgAnaMedico) = 800
        .Col = lColFgAnaMedico
        .TextMatrix(0, lColFgAnaMedico) = "M�dico"
        
        .ColWidth(lColFgAnaCodEFR) = 0
        .Col = lColFgAnaCodEFR
        .TextMatrix(0, lColFgAnaCodEFR) = ""
        
        .ColWidth(lColFgAnaDescrEFR) = 1500
        .Col = lColFgAnaDescrEFR
        .TextMatrix(0, lColFgAnaDescrEFR) = "Entidade"
        
        .ColWidth(lColFgAnaNrBenef) = 1400
        .Col = lColFgAnaNrBenef
        .TextMatrix(0, lColFgAnaNrBenef) = "Nr. Benef."
        
        .ColWidth(lColFgAnaPreco) = 700
        .Col = lColFgAnaPreco
        .TextMatrix(0, lColFgAnaPreco) = "Taxa �"
        
        .ColWidth(lColFgAnaDestino) = 1400
        .Col = lColFgAnaDestino
        .ColAlignment(lColFgAnaDestino) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaDestino) = "Destino"
        
         'NELSONPSILVA GLINTT-HS-21312 24.09.2019
        .ColWidth(lColFgAnaNAmostras) = 435
        .Col = lColFgAnaNAmostras
        .ColAlignment(lColFgAnaNAmostras) = flexAlignLeftCenter
        .TextMatrix(0, lColFgAnaNAmostras) = "Qtd."
        
        'BRUNODSANTOS GLINTT-HS-15750 03.12.2018 ->
        If gUsaEnvioPDS = mediSim Then
            .ColWidth(lColFgAnaConsenteEnvioPDS) = 900
            .Col = lColFgAnaConsenteEnvioPDS
            .ColAlignment(lColFgAnaConsenteEnvioPDS) = flexAlignLeftCenter
            .TextMatrix(0, lColFgAnaConsenteEnvioPDS) = "Envio PDS"
        End If
        
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    FGAna.MergeCol(lColFgAnaP1) = True
    FGAna.MergeCol(lColFgAnaMedico) = True
    FGAna.MergeCol(lColFgAnaDescrEFR) = True
    FGAna.MergeCol(lColFgAnaNrBenef) = True
    LastColA = 0
    LastRowA = 1
    CriaNovaClasse RegistosA, 1, "ANA", True
    
    With FgProd
        .rows = 2
        .FixedRows = 1
        .Cols = 8
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 900
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, 0) = "Produto"
        .ColWidth(1) = 2700
        .Col = 1
        .TextMatrix(0, 1) = "Descri��o"
        .ColWidth(2) = 850
        .Col = 2
        .TextMatrix(0, 2) = "Especifica."
        .ColWidth(3) = 2700
        .Col = 3
        .TextMatrix(0, 3) = "Descri��o"
        .Col = 4
        .ColWidth(4) = 750
        .TextMatrix(0, 4) = "Volume"
        .ColWidth(5) = 900
        .Col = 5
        .TextMatrix(0, 5) = "Previsto"
        .ColWidth(6) = 900
        .Col = 6
        .TextMatrix(0, 6) = "Chegada"
        .ColWidth(7) = 600
        .Col = 7
        .TextMatrix(0, 7) = "Estado"
        .WordWrap = False
        .row = 1
        .Col = 0
    End With

    LastColP = 0
    LastRowP = 1
    CriaNovaClasse RegistosP, 1, "PROD", True
    
    TB_DefTipoFGTubos FGTubos
    LastColT = 0
    LastRowT = 1

    With FGRecibos
        .rows = 2
        .FixedRows = 1
        .Cols = 12
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        If gPassaRecFactus <> mediSim Then
            .ColWidth(lColRecibosTipo) = 0
            .Col = lColRecibosTipo
            .TextMatrix(0, lColRecibosTipo) = ""
            .ColAlignment(lColRecibosTipo) = flexAlignLeftCenter
        Else
            .ColWidth(lColRecibosTipo) = 700
            .Col = lColRecibosTipo
            .TextMatrix(0, lColRecibosTipo) = "Tipo"
            .ColAlignment(lColRecibosTipo) = flexAlignLeftCenter
        End If
        
        .Col = lColRecibosNumDoc
        .TextMatrix(0, lColRecibosNumDoc) = "N�mero"
        .ColAlignment(lColRecibosNumDoc) = flexAlignLeftCenter
        
        .ColWidth(lColRecibosNumDoc) = 1500
        .Col = lColRecibosNumDoc
        .TextMatrix(0, lColRecibosNumDoc) = "N�mero"
        .ColAlignment(lColRecibosNumDoc) = flexAlignLeftCenter
        
        .ColWidth(lColRecibosEntidade) = 2500
        .Col = lColRecibosEntidade
        .TextMatrix(0, lColRecibosEntidade) = "Entidade"
        
        .ColWidth(lColRecibosValorPagar) = 700
        .Col = lColRecibosValorPagar
        .TextMatrix(0, lColRecibosValorPagar) = "Valor �"
        
        .ColWidth(lColRecibosCaucao) = 0
        .Col = lColRecibosCaucao
        .TextMatrix(0, lColRecibosCaucao) = "Cau��o"
                
        .ColWidth(lColRecibosDtEmissao) = 1300
        .Col = lColRecibosDtEmissao
        .TextMatrix(0, lColRecibosDtEmissao) = "Data Emiss�o"
        
        .ColWidth(lColRecibosDtPagamento) = 1300
        .Col = lColRecibosDtPagamento
        .TextMatrix(0, lColRecibosDtPagamento) = "Data Pagamento"
        
        .ColWidth(lColRecibosEstado) = 1300
        .Col = lColRecibosEstado
        .TextMatrix(0, lColRecibosEstado) = "Estado"
        
        .ColWidth(lColRecibosPercentagem) = 900
        .Col = lColRecibosPercentagem
        .TextMatrix(0, lColRecibosPercentagem) = "Percent %"
        
        .ColWidth(lColRecibosModoPag) = 1200
        .Col = lColRecibosModoPag
        .TextMatrix(0, lColRecibosModoPag) = "Modo Pag."
        
        .ColWidth(lColRecibosBorla) = 1000
        .Col = lColRecibosBorla
        .TextMatrix(0, lColRecibosBorla) = "Borla"
        
        .ColWidth(lColRecibosNaoConfmidade) = 0
        .Col = lColRecibosNaoConfmidade
        .TextMatrix(0, lColRecibosNaoConfmidade) = "Conform."
        
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    
    With FgAdiantamentos
        .rows = 2
        .FixedRows = 1
        .Cols = 5
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        
        .ColWidth(0) = 1200
        
        .Col = lColAdiantamentosNumDoc
        .TextMatrix(0, lColAdiantamentosNumDoc) = "N�mero"
        .ColAlignment(lColAdiantamentosNumDoc) = flexAlignLeftCenter
        
        
        .ColWidth(lColAdiantamentosValor) = 1000
        .Col = lColAdiantamentosValor
        .TextMatrix(0, lColAdiantamentosValor) = "Valor �"
        
                
        .ColWidth(lColAdiantamentosDtEmissao) = 1500
        .Col = lColAdiantamentosDtEmissao
        .TextMatrix(0, lColAdiantamentosDtEmissao) = "Data Emiss�o"
        
        .ColWidth(lColAdiantamentosDtAnulacao) = 1500
        .Col = lColAdiantamentosDtAnulacao
        .TextMatrix(0, lColAdiantamentosDtAnulacao) = "Data Anula��o"
        
        
        .ColWidth(lColAdiantamentosEstado) = 2000
        .Col = lColAdiantamentosEstado
        .TextMatrix(0, lColAdiantamentosEstado) = "Estado"
        
        
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    
    With FgAnaRec
        .rows = 2
        .FixedRows = 1
        .Cols = 4
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        
        .ColWidth(lColFgAnaRecCodAna) = 1000
        .Col = lColFgAnaRecCodAna
        .TextMatrix(0, lColFgAnaRecCodAna) = "C�d Ana"
        .ColAlignment(lColFgAnaRecCodAna) = flexAlignLeftCenter
        
        .ColWidth(lColFgAnaRecDescrAna) = 3000
        .Col = lColFgAnaRecDescrAna
        .TextMatrix(0, lColFgAnaRecDescrAna) = "An�lise"
        
        .ColWidth(lColFgAnaRecQtd) = 600
        .Col = lColFgAnaRecQtd
        .TextMatrix(0, lColFgAnaRecQtd) = "Qtd"
        
        .ColWidth(lColFgAnaRecTaxa) = 1000
        .Col = lColFgAnaRecTaxa
        .TextMatrix(0, lColFgAnaRecTaxa) = "Valor �"
        
        .row = 1
        .Col = 0
    End With
    
    With FgInformacao
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        
        .ColWidth(0) = 2000
        .Col = 0
        .TextMatrix(0, 0) = "Nome"
        .ColAlignment(0) = flexAlignLeftCenter
        
        .ColWidth(1) = 8000
        .Col = 1
        .TextMatrix(0, 1) = "Informacao"
        .ColAlignment(1) = flexAlignLeftCenter
         
        .row = 1
        .Col = 0
    End With
    
    With FgQuestoes
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        
        .ColWidth(0) = 7000
        .Col = 0
        .TextMatrix(0, 0) = "Quest�o"
        .ColAlignment(0) = flexAlignLeftCenter
        
        .ColWidth(1) = 3000
        .Col = 1
        .TextMatrix(0, 1) = "Resposta"
        .ColAlignment(1) = flexAlignLeftCenter
         
        .row = 1
        .Col = 0
    End With
    EcAuxRec.MaxLength = 10
    
    Set TAna.ImageList = ListaImagensAna
    TAna.Indentation = 0
    
    Flg_PergProd = False
    Resp_PergProd = False
    Flg_DtChProd = False
    Resp_PergDtChProd = False
    
    Flg_PergTubo = False
    Resp_PergTubo = False
    Flg_DtChTubo = False
    Resp_PergDtChTubo = False
    
    
    ExecutaCodigoA = False
    ExecutaCodigoP = False
    

    SSTGestReq.Enabled = True
    BtAnularRecibo.Enabled = False
    BtCobranca.Enabled = False
    BtPagamento.Enabled = False
    BtGerarRecibo.Enabled = False
    BtResumo.Enabled = False
    
    BtImprimirRecibo.Enabled = False
    FrameObsAna.Visible = False
    FrameAjuda.Visible = False
    FrameDomicilio.Visible = False
    CkCobrarDomicilio.value = vbGrayed
    FrameAnaRecibos.Visible = False
    CkAvisarSMS.value = vbGrayed
    GereFrameAdiantamentos False
    CbHemodialise.Enabled = False
    SSTGestReq.TabVisible(5) = True
    PreencheEntidadesDefeito
    CkReqHipocoagulados.value = vbGrayed
    SSTGestReq.TabVisible(5) = False
    BtHistAnt.Enabled = False
    BtHistSeg.Enabled = False
    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    EcUtilNomeEmail = ""
    EcUtilNomeSMS = ""
    PM_LimpaEstrutura
    If gUsaMarcacaoCodAnaEFR = mediSim Then
        FrameMarcaArs.Visible = True
    Else
        FrameMarcaArs.Visible = False
    End If
    ReDim reqColh(0)
    TotalReqColh = 0
 
Exit Sub
TrataErro:
    BG_LogFile_Erros "DefTipoCampos: " & Err.Number & " - " & Err.Description, Me.Name, "DefTipoCampos"
    Exit Sub
    Resume Next
End Sub


Sub PreencheValoresDefeito()
    
    On Error GoTo TrataErro
    
    EcNumColheita.text = ""
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    
    
    BG_PreencheComboBD_ADO "sl_tbf_t_urg", "cod_t_urg", "descr_t_urg", CbUrgencia, mediAscComboCodigo
    
    BG_PreencheComboBD_ADO "sl_isencao", "seq_isencao", "descr_isencao", CbTipoIsencao
    
    ' PFerreira 08.03.2007
    BG_PreencheComboBD_ADO "sl_cod_prioridades", "cod_prio", "nome_prio", CbPrioColheita, mediAscComboCodigo
    
    ' PFerreira 16.03.2007
    BG_PreencheComboBD_ADO "sl_cod_salas", "seq_sala", "descr_sala", CbSala, mediAscComboCodigo
    
    ' PFerreira 04.04.2007
    BG_PreencheComboBD_ADO "sl_tbf_t_destino", "cod_t_dest", "descr_t_dest", CbDestino, mediAscComboCodigo
    
    
    BG_PreencheComboBD_ADO "sl_cod_hemodialise", "seq_hemodialise", "descr_hemodialise", CbHemodialise, mediAscComboDesignacao

    BG_PreencheComboBD_ADO "sl_tbf_t_urbano", "cod_t_urbano", "descr_t_urbano", CbCodUrbano
    
    BG_PreencheComboBD_ADO "sl_tbf_convencao", "cod_convencao", "descr_convencao", CbConvencao, mediCodConvencao
    
    
    
    TotalEliminadas = 0
    ReDim Eliminadas(0)
    
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
    
    If gF_ENTREGA_RESULTADOS <> mediSim Then
        PreencheSalaDefeito
    End If
    Flg_IncrementaP1 = False
    
    
    PreencheEntidadesDefeito
    TAna.Visible = False
    lObrigaTerap = False
    lObrigaInfoCli = False
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheValoresDefeito: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheValoresDefeito"
    Exit Sub
    Resume Next
End Sub

Sub EventoActivate()
    On Error GoTo TrataErro
    
    Dim i As Integer
    
    If FormGestaoRequisicaoPrivado.Enabled = False Then
        FormGestaoRequisicaoPrivado.Enabled = True
    End If
    If flgEmissaoRecibos = True And FGRecibos.rows > 2 Then
        If gPassaRecFactus <> mediSim Then
            PreencheFgRecibos
            PreencheFgAdiantamentos
            flgEmissaoRecibos = False
        ElseIf gPassaRecFactus = mediSim Then
            AtualizaFgRecibosNew
            flgEmissaoRecibos = False
        End If
    End If
    Set gFormActivo = Me
    
    If Max <> -1 Then
        ReDim gFieldObjectProperties(0)
        ' Carregar as propriedades ja defenidas dos campos do form
        For Ind = 0 To Max
            ReDim Preserve gFieldObjectProperties(Ind)
            gFieldObjectProperties(Ind).EscalaNumerica = FOPropertiesTemp(Ind).EscalaNumerica
            gFieldObjectProperties(Ind).TamanhoConteudo = FOPropertiesTemp(Ind).TamanhoConteudo
            gFieldObjectProperties(Ind).nome = FOPropertiesTemp(Ind).nome
            gFieldObjectProperties(Ind).Precisao = FOPropertiesTemp(Ind).Precisao
            gFieldObjectProperties(Ind).TamanhoCampo = FOPropertiesTemp(Ind).TamanhoCampo
            gFieldObjectProperties(Ind).tipo = FOPropertiesTemp(Ind).tipo
        Next Ind
        
        Max = -1
        ReDim FOPropertiesTemp(0)
    End If
    
    BG_StackJanelas_Actualiza Me
    
    BL_ToolbarEstadoN Estado
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BG_ParametrizaPermissoes_ADO Me.Name
    
    If Estado = 2 Then
       BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        Err.Clear
        
        If EcEstadoReq = gEstadoReqCancelada Then
            
            Select Case Err.Number
                Case 0
                    ' OK
                Case 3021
                    ' BOF ou EOF � verdadeiro ou o registo actual foi eliminado; a opera��o pedida necessita de um registo actual.
                    rs.Requery
                Case Else
                
            End Select
            
            BtCancelarReq.Visible = True
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        Else
            BtCancelarReq.Visible = False
            BL_Toolbar_BotaoEstado "Remover", "Activo"
            BL_Toolbar_BotaoEstado "Modificar", "Activo"
        End If
    End If
       
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    Dim RsPesqUte As adodb.recordset
    Dim sql As String
    If gDUtente.seq_utente <> "" And (Flg_PesqUtente = True Or gF_IDENTIF = 1) Then
        If gDUtente.seq_utente = "-1" And gF_IDENTIF = 0 Then
'            ' UTENTE HIS
            Set RsPesqUte = New adodb.recordset
            With RsPesqUte
                .Source = "SELECT * from sl_identif where dt_nasc_ute = " & BL_TrataDataParaBD(gDUtente.dt_nasc_ute) & _
                        " and nome_ute = " & BL_TrataStringParaBD(gDUtente.nome_ute) & _
                        " and sexo_ute = " & BL_TrataStringParaBD(gDUtente.sexo_ute) & _
                        " and t_utente = " & BL_TrataStringParaBD(gDUtente.t_utente) & _
                        " and utente = " & BL_TrataStringParaBD(gDUtente.Utente)
                .CursorType = adOpenStatic
                .CursorLocation = adUseServer
                If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                .Open , gConexao
            End With
            If RsPesqUte.RecordCount > 0 Then
                EcSeqUtente = RsPesqUte!seq_utente
                PreencheDadosUtente
            Else
                BD_Insert_Utente
            End If
        ElseIf gDUtente.seq_utente = "-1" And gF_IDENTIF = 1 Then
            FormIdentificaUtente.Flg_PesqUtente = True
            Unload Me
            Exit Sub
        Else
            'j� tem utente
            EcSeqUtente = gDUtente.seq_utente
            PreencheDadosUtente
            Flg_PesqUtente = False
        End If
        Flg_PesqUtente = False

    End If
    '-------------------------------------------------------------------
    
    
    ' PFerreira 10.04.2007
    ' Caso o ecra de identificacao de utentes for o ecra anterior preenche a prioridade de colheita
    If (Flg_PesqUtente = True Or gF_IDENTIF = 1) Then
        If (Trim(EcDataNasc) <> "" And IsChild) Then
            CbPrioColheita.ListIndex = 3
        Else
            CbPrioColheita.ListIndex = 1
        End If
    End If
    If gF_IDENTIF = 1 Then
                 'edgar.parada Glintt-HS-19165 * nao faz focus se for importado
        If RequisicaoESP(EcNumReq.text) = False Then
            EcDataPrevista_GotFocus
            EcDataPrevista_Validate True
        End If
        '
    End If
    
    If EcNumReq <> "" Then
        PreencheNotas
    End If
    
    If gListaMedicos = 1 Then
        EcDescrMedico.Visible = False
        EcCodMedico.Visible = True
        EcNomeMedico.Visible = True
        BtPesquisaMedico.Visible = True
    End If
    
    'soliveira lacto
    If gFichaCruz = 1 Then
        LaFichaCruz.caption = "Ficha Crz"
    Else
        LaFichaCruz.caption = "Etiquetas"
    End If

'Coloca codigo da sala por defeito

    If gCodSalaDefeito <> "-1" Then

        If EcCodSala.text = "" Then

            EcCodSala.text = gCodSalaDefeito
            'BRUNODSANTOS 09.06.2017 LRV-469
            codSalaAux = gCodSalaDefeito
            '
        End If

    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheValoresDefeito: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheValoresDefeito"
    Exit Sub
    Resume Next
End Sub

Sub PreencheDadosUtente()
    On Error GoTo TrataErro
    
    Dim Campos(6) As String
    Dim retorno() As String
    Dim iret As Integer
    
    Campos(0) = "utente"
    Campos(1) = "t_utente"
    Campos(2) = "nome_ute"
    Campos(3) = "dt_nasc_ute"
    Campos(4) = "sexo_ute"
    Campos(5) = "abrev_ute"

    iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)
    
    If iret = -1 Then
        BG_Mensagem mediMsgBox, "Erro a seleccionar utente!", vbCritical + vbOKOnly, App.ProductName
        CbTipoUtente.ListIndex = mediComboValorNull
        EcUtente.text = ""
        Exit Sub
    End If
    
    EcUtente.text = retorno(0)
    CbTipoUtente.text = retorno(1)
    EcNome.text = retorno(2)
    EcDataNasc.text = retorno(3)
    If retorno(4) = gT_Masculino Then
        EcSexo = "Masculino"
    Else
        EcSexo = "Feminino"
    End If
    EcAbrevUte.text = retorno(5)
    If EcDataNasc <> "" Then
        EcIdade = BG_CalculaIdade(CDate(EcDataNasc.text))
    End If
    
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim And Me.ExternalAccess = True Then
    Dim contextJson As String
    Dim responseJson As String
    Dim requestJson As String
   
        contextJson = "{" & Chr(34) & "EcTipoUtente" & Chr(34) & ":" & Chr(34) & retorno(1) & Chr(34) & "," & Chr(34) & "EcUtente" & Chr(34) & ":" & Chr(34) & retorno(0) & Chr(34) & "}"
        
        responseJson = "{" & Chr(34) & "EcTipoUtente" & Chr(34) & ":" & Chr(34) & retorno(1) & ", " & Chr(34) & "EcUtente" & Chr(34) & ":" & Chr(34) & retorno(0) & Chr(34) & _
                ", " & Chr(34) & "EcNome" & Chr(34) & ":" & Chr(34) & retorno(2) & Chr(34) & ", " & Chr(34) & "EcDataNasc" & Chr(34) & ":" & Chr(34) & retorno(3) & Chr(34) & _
                ", " & Chr(34) & "EcSexo" & Chr(34) & ":" & Chr(34) & EcSexo & Chr(34) & ", " & Chr(34) & "EcAbrevUte" & Chr(34) & ":" & Chr(34) & retorno(5) & Chr(34) & _
                ", " & Chr(34) & "EcIdade" & Chr(34) & ":" & Chr(34) & EcIdade & Chr(34) & "}"
        
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, contextJson, contextJson, IIf(responseJson = vbNullString, "{}", responseJson)

    End If
    
    BtInfClinica.Enabled = True
    BtDomicilio.Enabled = True
    BtConsReq.Enabled = True
    BtIdentif.Enabled = True
    
 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDadosUtente: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDadosUtente"
    Exit Sub
    Resume Next
End Sub

Sub PreencheEFRUtente()
    
    On Error GoTo ErrorHandler
    
    Dim Campos(1) As String
    Dim retorno() As String
    Dim iret As Integer
    Dim SQLEFR As String
    Dim tabelaEFR As adodb.recordset
    Dim SelEntUte As Boolean
    
'    If (Trim(EcCodEFR.Text) = "") Then
    
        SelEntUte = False
        If Trim(EcSeqUtente) <> "" Then
            If rs Is Nothing Then
                SelEntUte = True
            ElseIf rs.state = adStateClosed Then
                SelEntUte = True
            End If
        End If
        
        If SelEntUte = True Then
            Campos(0) = "cod_efr_ute"
            Campos(1) = "n_benef_ute"
            iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)
            
            If iret = -1 Then
                BG_Mensagem mediMsgBox, "Erro a seleccionar E.F.R. do utente!", vbCritical + vbOKOnly, App.ProductName
                Exit Sub
            End If
            
            EcCodEFR.text = retorno(0)
            'NELSONPSILVA GLINTT-HS-21312
            CodEfrAUX = EcCodEFR
            '
            ' PARA ENTIDADE GNR FORMATA O NUMERO DO CARTAO
            If gCodEfrGNR = EcCodEFR And gTipoInstituicao = "PRIVADA" Then
                If Mid(retorno(1), 1, 1) = "G" Then
                    EcNumBenef = Mid(retorno(1), 3, 10)
                Else
                    EcNumBenef = Mid(retorno(1), 1, 10)
                End If
            ' PARA ENTIDADE PT FORMATA O NUMERO DO CARTAO
            ElseIf gCodEfrPT = EcCodEFR And gTipoInstituicao = "PRIVADA" Then
                If Len(Trim(retorno(1))) = 14 Then
                    EcNumBenef.text = Mid(retorno(1), 3, 8) & "/" & Mid(retorno(1), 11, 2)
                    While Mid(EcNumBenef.text, 1, 1) = "0"
                        EcNumBenef.text = Mid(EcNumBenef.text, 2)
                    Wend
                Else
                    EcNumBenef.text = retorno(1)
                End If
            Else
                EcNumBenef.text = retorno(1)
            End If
            
            Set tabelaEFR = New adodb.recordset
            tabelaEFR.CursorType = adOpenStatic
            tabelaEFR.CursorLocation = adUseServer
            
            SQLEFR = "SELECT " & _
                     "      descr_efr " & _
                     "FROM " & _
                     "      sl_efr " & _
                     "WHERE " & _
                     "      cod_efr = " & Trim(EcCodEFR.text)
            
            If gModoDebug = mediSim Then BG_LogFile_Erros SQLEFR
            tabelaEFR.Open SQLEFR, gConexao
            If tabelaEFR.RecordCount > 0 Then
                EcDescrEFR.text = tabelaEFR!descr_efr
                'NELSONPSILVA GLINTT-HS-21312
                efrDescrAUX = EcDescrEFR
                '
            End If
        
            tabelaEFR.Close
            Set tabelaEFR = Nothing
        End If
        
'        ValidarARS EcCodEFR
    
'    End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros "Erro Inesperado : PreencheEFRUtente (FormGestaoRequisicaoPrivado) -> " & Err.Description
            Exit Sub
    End Select
End Sub
Sub EventoUnload()
    On Error GoTo TrataErro
    
    BG_StackJanelas_Pop
    BL_ToolbarEstadoN 0
    
    
    ' Descarregar todos os objectos
    Set RegistosP = Nothing
    
    If Not rs Is Nothing Then
        If rs.state <> adStateClosed Then
            rs.Close
        End If
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Isencao")
    Call BL_FechaPreview("Recibo Requisi��o")
    
    Set FormGestaoRequisicaoPrivado = Nothing
    
    Call BG_RollbackTransaction
    Call Apaga_DS_TM
        
'    If gF_IDENTIF = 1 Then
'        FormIdentificaUtente.Enabled = True
'        Set gFormActivo = FormIdentificaUtente
'    Else
'        Set gFormActivo = MDIFormInicio
'    End If
    
    gF_REQUIS_PRIVADO = 0
    'Hoje
    'gEpisodioActivo = ""
    'gSituacaoActiva = mediComboValorNull

    If (gFormGesReqCons_Aberto = True) Then
        Flg_PesqUtente = False
        ' Quando este form foi invocado atrav�s de FormGesReqCons.
        gFormGesReqCons_Aberto = False
        Set gFormActivo = FormGesReqCons
        FormGesReqCons.Enabled = True
    ElseIf gF_IDENTIF = 1 Then
        FormIdentificaUtente.Enabled = True
        Set gFormActivo = FormIdentificaUtente
    ElseIf gF_FILA_ESPERA = mediSim Then
        FormFilaEspera.Enabled = True
        Set gFormActivo = FormFilaEspera
    Else
        Set gFormActivo = MDIFormInicio
    End If
        
    'edgar.parada Liga��o Secund�ria
    If gConexaoSecundaria.state <> adStateClosed Then
       BL_Fecha_Conexao_Secundaria
    End If
    '
 Exit Sub
TrataErro:
    BG_LogFile_Erros "EventoUnload: " & Err.Number & " - " & Err.Description, Me.Name, "EventoUnload"
    Exit Sub
    Resume Next
End Sub

Sub SELECT_Dados_Perfil(Marcar() As String, indice As Long, CodPerfil As String, CodFacturavel As String, multiAna As String)
    On Error GoTo TrataErro

    Dim RsPerf As adodb.recordset
    Dim sql As String
    Dim Activo As String
    Dim descricao As String
    Dim JaExiste As Boolean
    Dim flgIncrmenta As Boolean
    Dim qtd As Integer
    Dim qtdMapeada As String
    Dim flg_FdsValFixo   As Integer
    CodPerfil = UCase(Trim(CodPerfil))
    
    'Activo ?
    sql = "SELECT descr_perfis, flg_activo FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(CodPerfil)
    Set RsPerf = New adodb.recordset
    With RsPerf
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open , gConexao
    End With
    
    Activo = "-1"
    If RsPerf.RecordCount > 0 Then
        Activo = BL_HandleNull(RsPerf!flg_activo, "0")
        descricao = BL_HandleNull(RsPerf!descr_perfis, " ")
    Else
        BG_LogFile_Erros Me.Name & " - SELECT_Dados_Perfil: Erro a seleccionar dados do perfil!"
    End If
    RsPerf.Close
    Set RsPerf = Nothing
    
    'Membros do perfil
    sql = "SELECT cod_analise, ordem FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(CodPerfil)
    
    Set RsPerf = New adodb.recordset
    With RsPerf
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open , gConexao
    End With
    
    If RsPerf.RecordCount <> 0 Then
        If Activo = "1" Then
            LbTotalAna = LbTotalAna + 1
            BL_AcrescentaNodoPai TAna, descricao, CodPerfil, 1, 2
            'soliveira 03.12.2007 Acrescentar analise "C99999" para descri��o do perfil no ecra de resultados
            BL_Preenche_MAReq FGTubos, CodPerfil, descricao, "C99999", " ", gGHOSTMEMBER_S, " ", indice, -1, -1, CodPerfil, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
            AdicionaInformacaoAna CodPerfil
            AdicionaQuestaoAna CodPerfil
        End If
    Else
        BG_Mensagem mediMsgBox, "Perfil '" & descricao & "' sem membros!", vbInformation + vbOKOnly, App.ProductName
    End If
    
    'SE Perfil MARCADA PARA ENVIAR QUANTIDADE PARA FACTUS
    qtdMapeada = RetornaQtd(CodPerfil, EcCodEFR)
    If qtdMapeada <> "" Then
        qtd = qtdMapeada
        flgIncrmenta = False
    Else
        If IF_ContaMembros(CodPerfil, EcCodEFR) > 0 Then
            qtd = 0
            flgIncrmenta = True
        Else
            qtd = 1
            flgIncrmenta = False
        End If
    End If
    
    FGAna.CellBackColor = vbWhite
    FGAna.CellForeColor = vbBlack
    While Not RsPerf.EOF
        JaExiste = Verifica_Ana_ja_Existe(indice, BL_HandleNull(RsPerf!cod_analise, " "))
    
        If Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "S" Then
            If Activo = "1" Then
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), multiAna, CodPerfil, descricao, BL_HandleNull(RsPerf!ordem, 0), , , , CodFacturavel
            ElseIf Activo = "0" And JaExiste = False Then
                If VerificaRegraSexoMarcar(BL_HandleNull(RsPerf!cod_analise, " "), EcSeqUtente) = False Then
                    BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
                    JaExiste = True
                Else
                    PM_AdicionaItemEstrutura mediComboValorNull, mediComboValorNull, CodPerfil, BL_HandleNull(RsPerf!cod_analise, " "), "", "", ""
                    If SELECT_Dados_Simples(Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), multiAna, , , , , , , CodFacturavel) = False Then
                          indice = indice - 1
                    End If
                End If
            End If
        ElseIf Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "C" Then
            If Activo = "1" Then
                SELECT_Dados_Complexa Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), multiAna, CodPerfil, descricao, BL_HandleNull(RsPerf!ordem, 0), CodFacturavel
            ElseIf Activo = "0" And JaExiste = False Then
                If VerificaRegraSexoMarcar(BL_HandleNull(RsPerf!cod_analise, " "), EcSeqUtente) = False Then
                    BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
                    JaExiste = True
                Else
                    PM_AdicionaItemEstrutura mediComboValorNull, mediComboValorNull, CodPerfil, BL_HandleNull(RsPerf!cod_analise, " "), "", "", ""
                    SELECT_Dados_Complexa Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), multiAna, , , , CodFacturavel
                End If
            End If
        ElseIf Mid(BL_HandleNull(RsPerf!cod_analise, " "), 1, 1) = "P" Then
            If Activo = "1" Then
                SELECT_Dados_Perfil Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), CodFacturavel, multiAna
            ElseIf Activo = "0" And JaExiste = False Then
                If VerificaRegraSexoMarcar(BL_HandleNull(RsPerf!cod_analise, " "), EcSeqUtente) = False Then
                    BG_Mensagem mediMsgBox, "Utente n�o pode efectuar an�lise em causa!", vbOKOnly + vbInformation, "An�lises"
                    JaExiste = True
                Else
                    PM_AdicionaItemEstrutura mediComboValorNull, mediComboValorNull, CodPerfil, BL_HandleNull(RsPerf!cod_analise, " "), "", "", ""
                    SELECT_Dados_Perfil Marcar, indice, BL_HandleNull(RsPerf!cod_analise, " "), CodFacturavel, multiAna
                End If
            End If
        End If
        RsPerf.MoveNext
        
        If Not RsPerf.EOF And Activo = "0" And JaExiste = False Then
            ExecutaCodigoA = False
            indice = indice + 1
            FGAna.AddItem "", indice
            Insere_aMeio_RegistosA FGAna.row + 1
            FGAna.row = FGAna.row + 1
            FGAna.Col = 0
            ExecutaCodigoA = True
        End If
        If flgIncrmenta = True Then
            qtd = qtd + 1
        End If
    Wend
    
    If flgIncrmenta = True Then
        FlgIncrementa indice, qtd
    End If
    RsPerf.Close
    Set RsPerf = Nothing
 Exit Sub
TrataErro:
    BG_LogFile_Erros "SELECT_Dados_Perfil: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Dados_Perfil"
    Exit Sub
    Resume Next
End Sub

Sub SELECT_Dados_Complexa(Marcar() As String, indice As Long, CodComplexa As String, multiAna As String, Optional Perfil As Variant, _
                          Optional DescrPerfil As Variant, Optional OrdemP As Variant, Optional CodFacturavel As String)
    On Error GoTo TrataErro

    Dim RsCompl As adodb.recordset
    Dim sql As String
    Dim Activo As String
    Dim descricao As String
    Dim qtd As Integer
    Dim flgIncrmenta As Boolean
    Dim qtdMapeada As String
    Dim flg_FdsValFixo   As Integer
    'Activo ?
    sql = "SELECT descr_ana_c, flg_marc_memb " & _
          "FROM sl_ana_c " & _
          "WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa)
    
    Set RsCompl = New adodb.recordset

    With RsCompl
        .Source = sql
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open , gConexao
    End With

    Activo = "-1"
    If RsCompl.RecordCount > 0 Then
        Activo = BL_HandleNull(RsCompl!flg_marc_memb, "0")
        descricao = BL_HandleNull(RsCompl!Descr_Ana_C, "?")
    Else
        BG_LogFile_Erros Me.Name & " - SELECT_Dados_Complexa: Erro a seleccionar dados da complexa!"
    End If
    RsCompl.Close
    Set RsCompl = Nothing
    
    If IsMissing(Perfil) Then
        AdicionaInformacaoAna CodComplexa
        AdicionaQuestaoAna CodComplexa
    ElseIf Perfil = "" Or Perfil = "0" Then
        AdicionaInformacaoAna CodComplexa
        AdicionaQuestaoAna CodComplexa
    End If
    
    If Activo = "1" Then
        'soliveira teste
        If Not IsMissing(Perfil) Then
            BL_AcrescentaNodoFilho TAna, descricao, Perfil, Perfil & CodComplexa, 3, 4
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, multiAna, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, -1, CodFacturavel
        Else
            LbTotalAna = LbTotalAna + 1
            BL_AcrescentaNodoPai TAna, descricao, CodComplexa, 3, 4
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, multiAna, , , , CodComplexa, descricao, -1, CodFacturavel
        End If
        
        'Membros da complexa
        sql = "SELECT cod_membro, ordem FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa) & _
            " AND t_membro = 'A' ORDER BY ordem "
        Set RsCompl = New adodb.recordset
        With RsCompl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
        
        If RsCompl.RecordCount = 0 Then
            BG_Mensagem mediMsgBox, "Complexa '" & descricao & "' sem membros!", vbInformation + vbOKOnly, App.ProductName
        Else
            If Not IsMissing(Perfil) Then
                BL_AcrescentaNodoFilho TAna, descricao, Perfil, Perfil & CodComplexa, 3, 4
            Else
                BL_AcrescentaNodoPai TAna, descricao, CodComplexa, 3, 4
            End If
        End If
        
        'SE COMPLEXA MARCADA PARA ENVIAR QUANTIDADE PARA FACTUS
        qtdMapeada = RetornaQtd(CodComplexa, EcCodEFR)
        If qtdMapeada <> "" Then
            qtd = qtdMapeada
            flgIncrmenta = False
        Else
            If IF_ContaMembros(CodComplexa, EcCodEFR) > 0 Then
                qtd = 0
                flgIncrmenta = True
            Else
                qtd = 1
                flgIncrmenta = False
            End If
        End If
        
        
        While Not RsCompl.EOF
            If Not IsMissing(Perfil) Then
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), multiAna, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " "), CodFacturavel
            Else
                SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), multiAna, , , , CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " "), CodFacturavel
            End If
            RsCompl.MoveNext
            
            If flgIncrmenta = True Then
                qtd = qtd + 1
            End If
        Wend
        If flgIncrmenta = True Then
            FlgIncrementa indice, qtd
        End If
    Else
        If Not IsMissing(Perfil) Then
            BL_AcrescentaNodoFilho TAna, descricao, Perfil, Perfil & CodComplexa, 3, 4
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, multiAna, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, -1, CodFacturavel
        Else
            LbTotalAna = LbTotalAna + 1
            BL_AcrescentaNodoPai TAna, descricao, CodComplexa, 3, 4
            SELECT_Dados_Simples Marcar, indice, gGHOSTMEMBER_S, multiAna, , , , CodComplexa, descricao, -1, CodFacturavel
        End If
    
        'Membros da complexa
        sql = "SELECT cod_membro, sl_membro.ordem FROM sl_membro,sl_ana_s " & _
            " WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa) & _
            " AND cod_ana_s = cod_membro " & _
            " AND flg_marc_auto = '1' " & _
            " AND t_membro = 'A' ORDER BY sl_membro.ordem "
        Set RsCompl = New adodb.recordset
        With RsCompl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
        
        If Not RsCompl.EOF Then
            While Not RsCompl.EOF
                
                If Not IsMissing(Perfil) Then
                    SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), multiAna, Perfil, DescrPerfil, OrdemP, CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " ")
                Else
                    SELECT_Dados_Simples Marcar, indice, BL_HandleNull(RsCompl!cod_membro, " "), multiAna, , , , CodComplexa, descricao, BL_HandleNull(RsCompl!ordem, " ")
                End If
                RsCompl.MoveNext

            Wend
            If flgIncrmenta = True Then
                FlgIncrementa indice, qtd
            End If
        End If
        RsCompl.Close
        
        sql = "SELECT cod_membro, sl_membro.ordem FROM sl_membro,sl_ana_s " & _
            " WHERE cod_ana_c = " & BL_TrataStringParaBD(CodComplexa) & _
            " AND cod_ana_s = cod_membro " & _
            " AND flg_opcional = '1' " & _
            " AND t_membro = 'A' ORDER BY sl_membro.ordem "
        Set RsCompl = New adodb.recordset
        With RsCompl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
        
        If RsCompl.RecordCount > 0 And (IsMissing(Perfil) Or IsEmpty(Perfil)) Then
            GhostPerfil = "0"
            GhostComplexa = CodComplexa
            Lista_GhostMembers GhostComplexa, descricao
        End If
        
    End If
    RsCompl.Close
    Set RsCompl = Nothing
 Exit Sub
TrataErro:
    BG_LogFile_Erros "SELECT_Dados_Complexa: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Dados_Complexa"
    Exit Sub
    Resume Next
End Sub

Function SELECT_Dados_Simples(Marcar() As String, indice As Long, CodSimples As String, multiAna As String, Optional Perfil As Variant, Optional DescrPerfil As Variant, Optional OrdemP As Variant, Optional complexa As Variant, Optional DescrComplexa As Variant, Optional OrdemC As Variant, Optional CodFacturavel As String) As Boolean
    On Error GoTo TrataErro
    Dim PerfilFact As Integer
    Dim RsSimpl As adodb.recordset
    Dim sql As String
    Dim descricao As String
    Dim codAgrup As String
    Dim descrAgrup As String
    Dim i As Integer
    Dim NaoMarcar As Boolean
    Dim taxa As String
    Dim codAnaEfr As String
    Dim mens As String
    Dim p1 As Long
    Dim corP1 As String
    Dim AnaRegra As String
    Dim anaFacturar As String
    Dim flg_FdsValFixo As Integer
    Dim iRec As Long
    Dim NrReqARS As String
    Dim NumBenef As String
    Dim codUrbano As Integer
        Dim codIsencao As Integer
    'edgar.parada LJMANSO-351 10.09.2018
    Dim convencao As Integer
    '
    SELECT_Dados_Simples = True
 
    If CodSimples = gGHOSTMEMBER_S Then
        If Not IsMissing(complexa) Then
            If Not IsMissing(Perfil) Then
                codAgrup = Perfil
                descrAgrup = DescrPerfil
                BL_Preenche_MAReq FGTubos, Perfil, DescrPerfil, complexa, DescrComplexa, CodSimples, " ", indice, OrdemC, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
                BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", Perfil & complexa, complexa & gGHOSTMEMBER_S, 5, 6
                AdicionaDadosMoviFact codAgrup, mediComboValorNull, mediComboValorNull, mediComboValorNull, mediComboValorNull
            Else
                codAgrup = complexa
                descrAgrup = DescrComplexa
            
                BL_Preenche_MAReq FGTubos, "0", " ", complexa, DescrComplexa, CodSimples, " ", indice, OrdemC, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
                BL_AcrescentaNodoFilho TAna, "Duplo click para escolher an�lises.", complexa, complexa & gGHOSTMEMBER_S, 5, 6
                AdicionaDadosMoviFact codAgrup, mediComboValorNull, mediComboValorNull, mediComboValorNull, mediComboValorNull
            End If
        End If
    Else
    
        sql = "SELECT descr_ana_s, flg_invisivel FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(CodSimples)
        Set RsSimpl = New adodb.recordset
        With RsSimpl
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
        
        If RsSimpl.RecordCount > 0 Then
            descricao = BL_HandleNull(RsSimpl!descr_ana_s, " ")
            'N�o marcar a an�lise se flg_invisivel = 1
            If RsSimpl!flg_invisivel = 1 Then
                RsSimpl.Close
                Set RsSimpl = Nothing
                SELECT_Dados_Simples = False
                Exit Function
            End If
        End If
        RsSimpl.Close
        Set RsSimpl = Nothing
        
        If Not IsMissing(complexa) Then
             If Not IsMissing(Perfil) Then
                codAgrup = Perfil
                descrAgrup = DescrPerfil
                BL_Preenche_MAReq FGTubos, Perfil, DescrPerfil, complexa, DescrComplexa, CodSimples, descricao, indice, OrdemC, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
                BL_AcrescentaNodoFilho TAna, descricao, Perfil & complexa, complexa & CodSimples, 5, 6
                AdicionaDadosMoviFact codAgrup, mediComboValorNull, mediComboValorNull, mediComboValorNull, mediComboValorNull
             Else
                codAgrup = complexa
                descrAgrup = DescrComplexa
             
                BL_Preenche_MAReq FGTubos, "0", " ", complexa, DescrComplexa, CodSimples, descricao, indice, OrdemC, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
                BL_AcrescentaNodoFilho TAna, descricao, complexa, complexa & CodSimples, 5, 6
                AdicionaDadosMoviFact codAgrup, mediComboValorNull, mediComboValorNull, mediComboValorNull, mediComboValorNull
             End If
        Else
             If Not IsMissing(Perfil) Then
                codAgrup = Perfil
                descrAgrup = DescrPerfil
                
                BL_AcrescentaNodoFilho TAna, descricao, Perfil, Perfil & CodSimples, 5, 6
                BL_Preenche_MAReq FGTubos, Perfil, DescrPerfil, "0", " ", CodSimples, descricao, indice, 0, OrdemP, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
                AdicionaDadosMoviFact codAgrup, mediComboValorNull, mediComboValorNull, mediComboValorNull, mediComboValorNull
             Else
                LbTotalAna = LbTotalAna + 1
                codAgrup = CodSimples
                descrAgrup = descricao
             
                BL_AcrescentaNodoPai TAna, descricao, CodSimples, 5, 6
                BL_Preenche_MAReq FGTubos, "0", " ", "0", " ", CodSimples, descricao, indice, 0, 0, codAgrup, 0, gColocaDataChegada, "0", "-1", 0, CLng(indice), 0, 0, EcDataPrevista
                AdicionaDadosMoviFact codAgrup, mediComboValorNull, mediComboValorNull, mediComboValorNull, mediComboValorNull
             End If
        End If
        
    End If

    If Verifica_Ana_ja_Existe(0, codAgrup) = False Then
        If Trim(RegistosA(indice).codAna) = "" Then
            If codAgrup = CodSimples Then
                AdicionaInformacaoAna CodSimples
                AdicionaQuestaoAna CodSimples
                AdicionaDadosMoviFact CodSimples, mediComboValorNull, mediComboValorNull, mediComboValorNull, mediComboValorNull
            End If
            
            If BL_VerificaInfoClinObrig(codAgrup, "T") = True Then
                lObrigaTerap = True
            End If
            If BL_VerificaInfoClinObrig(codAgrup, "I") = True Then
                lObrigaInfoCli = True
            End If
            
            TotalAcrescentadas = TotalAcrescentadas + 1
            ReDim Preserve Acrescentadas(TotalAcrescentadas)
            Acrescentadas(TotalAcrescentadas).cod_agrup = codAgrup
            
            RegistosA(indice).codAna = codAgrup
            RegistosA(indice).descrAna = descrAgrup
            RegistosA(indice).Estado = "-1"
            RegistosA(indice).ObsAnaReq = ""
            RegistosA(indice).seqObsAnaReq = 0
            RegistosA(indice).multiAna = multiAna
            
            '-------------------------------------------------------------------------------------------
            ' DESTINO DAS ANALISES
            '-------------------------------------------------------------------------------------------
            If indice = 1 Then
                RegistosA(indice).n_prescricao = "1"
                If CbDestino.ListIndex > mediComboValorNull Then
                    RegistosA(indice).cod_destino = CbDestino.ItemData(CbDestino.ListIndex)
                End If
                RegistosA(indice).descr_destino = BL_SelCodigo("SL_TBF_T_DESTINO", "DESCR_T_DEST", "COD_T_DEST", RegistosA(indice).cod_destino)
            Else
                RegistosA(indice).n_prescricao = RegistosA(indice - 1).n_prescricao
                RegistosA(indice).cod_destino = RegistosA(indice - 1).cod_destino
                RegistosA(indice).descr_destino = RegistosA(indice - 1).descr_destino
            End If
                
            ' ------------------------------------------------------------------------------------------
            ' PARA ANALISES QUE NAOOOOO SAO MARCADAS DENTRO DE NENHUM PERFIL DE MARCACAO
            ' ------------------------------------------------------------------------------------------
            If RegistosA(indice).codAna <> "" And flg_codBarras = False Then
                If gPassaRecFactus <> mediSim Then
                    If RegistosA(indice).codAna <> BL_HandleNull(UCase(CodFacturavel), "0") And Mid(UCase(CodFacturavel), 1, 1) = "P" Then
                        PerfilFact = BL_HandleNull(BL_SelCodigo("SL_PERFIS", "flg_nao_facturar", "cod_perfis", UCase(CodFacturavel), "V"), 0)
                        If PerfilFact = 0 Then
                            ' PERFIL DE MARCACAO. COMO TAL VERIFICAR SE JA FOI MARCADO
                            If VerificaPerfilMarcacaoJaMarcado(BL_HandleNull(UCase(CodFacturavel), "0")) = False Then
                                AdicionaReciboManual 0, EcCodEFR, "0", "0", BL_HandleNull(UCase(CodFacturavel), "0"), 1, "", BL_HandleNull(UCase(CodFacturavel), "0"), False
                            End If
                        Else
                            AdicionaReciboManual 0, EcCodEFR, "0", "0", BL_HandleNull(UCase(RegistosA(indice).codAna), "0"), 1, "", RegistosA(indice).codAna, False
                        End If
                    Else
                        AdicionaReciboManual 0, EcCodEFR, "0", "0", BL_HandleNull(UCase(CodFacturavel), "0"), 1, "", RegistosA(indice).codAna, False
                    End If
                Else
                    If CbCodUrbano.ListIndex > mediComboValorNull Then
                        codUrbano = CbCodUrbano.ItemData(CbCodUrbano.ListIndex)
                    Else
                        codUrbano = mediComboValorNull
                    End If
                    If indice = 1 Then
                        NrReqARS = ""
                        NumBenef = EcNumBenef.text
                    ElseIf indice > 1 Then
                        'edgar.parada Glintt-HS-19165 * no caso de credencial adicionada manualmente
                        If CredencialESP(FGAna.TextMatrix(indice - 1, lColFgAnaP1)) Then
                             NrReqARS = ""
                             'efrESP = True
                        Else
                           ' efrESP = True
                            NrReqARS = FGAna.TextMatrix(indice - 1, lColFgAnaP1)
                        End If
                        '
                        NumBenef = FGAna.TextMatrix(indice - 1, lColFgAnaNrBenef)
                    End If
                    'edgar.parada LJMANSO-351 10.09.2018
                    convencao = obtemConvencao(indexCred, BL_HandleNull(UCase(RegistosA(indice).codAna), ""), NrReqARS)
                    '
                    If EcCodEFR = gEfrParticular Then
                        codIsencao = gTipoIsencaoNaoIsento
                    Else
                        codIsencao = BG_DaComboSel(CbTipoIsencao)
                    End If
                    If RegistosA(indice).codAna <> BL_HandleNull(UCase(CodFacturavel), "0") And Mid(UCase(CodFacturavel), 1, 1) = "P" Then
                        PerfilFact = BL_HandleNull(BL_SelCodigo("SL_PERFIS", "flg_nao_facturar", "cod_perfis", UCase(CodFacturavel), "V"), 0)
                        If PerfilFact = 0 Then
                        
                            ' PERFIL DE MARCACAO. COMO TAL VERIFICAR SE JA FOI MARCADO
                                                        'edgar.parada LJMANSO-351 10.09.2018Troca de CbConvencao.ItemData(CbConvencao.ListIndex) por convencao
                            If FACTUS_VerificaAnaJaMarcada(CodFacturavel) = False Then
                               FACTUS_AdicionaAnalise EcCodEFR.text, BL_HandleNull(UCase(CodFacturavel), ""), _
                                                        BL_HandleNull(EcDataPrevista.text, Bg_DaData_ADO), 0, mediComboValorNull, codIsencao, _
                                                        CLng(indice), NumBenef, CbTipoUtente, EcUtente, EcNumReq.text, NrReqARS, convencao, _
                                                        EcCodMedico.text, codUrbano, BL_HandleNull(EcKm.text, mediComboValorNull), _
                                                         EcCodPostalAux.text, EcDescrPostal.text, EcCodProveniencia.text, mediComboValorNull
                            End If
                        Else
                                                           'edgar.parada LJMANSO-351 10.09.2018Troca de CbConvencao.ItemData(CbConvencao.ListIndex) por convencao
                               FACTUS_AdicionaAnalise EcCodEFR.text, BL_HandleNull(UCase(RegistosA(indice).codAna), ""), _
                                                        BL_HandleNull(EcDataPrevista.text, Bg_DaData_ADO), 0, mediComboValorNull, codIsencao, _
                                                        CLng(indice), NumBenef, CbTipoUtente, EcUtente, EcNumReq.text, NrReqARS, convencao, _
                                                        EcCodMedico.text, codUrbano, BL_HandleNull(EcKm.text, mediComboValorNull), _
                                                        EcCodPostalAux.text, EcDescrPostal.text, EcCodProveniencia.text, mediComboValorNull
                        End If
                    Else
                                                           'edgar.parada LJMANSO-351 10.09.2018Troca de CbConvencao.ItemData(CbConvencao.ListIndex) por convencao
                               FACTUS_AdicionaAnalise EcCodEFR.text, BL_HandleNull(UCase(RegistosA(indice).codAna), ""), _
                                                        BL_HandleNull(EcDataPrevista.text, Bg_DaData_ADO), 0, mediComboValorNull, codIsencao, _
                                                        CLng(indice), NumBenef, CbTipoUtente, EcUtente, EcNumReq.text, NrReqARS, convencao, _
                                                        EcCodMedico.text, codUrbano, BL_HandleNull(EcKm.text, mediComboValorNull), _
                                                         EcCodPostalAux.text, EcDescrPostal.text, EcCodProveniencia.text, mediComboValorNull
                    
                    End If
                    PreencheDadosNovoRecibo indice
                    AtualizaFgRecibosNew
                End If
            End If
        
            If indice <> 1 Then
                If Trim(RegistosA(indice - 1).NReqARS) <> "" Then
                    RegistosA(indice).NReqARS = RegistosA(indice - 1).NReqARS
                Else
                    If Trim(EcNumReq) <> "" Then
                        RegistosA(indice).NReqARS = EcNumReq
                    Else
                        RegistosA(indice).NReqARS = "A gerar"
                    End If
                End If
            Else
                If Trim(EcNumReq) <> "" Then
                    RegistosA(indice).NReqARS = EcNumReq
                Else
                    RegistosA(indice).NReqARS = "A gerar"
                End If
            End If
            iRec = DevIndice(codAgrup)
            FGAna.TextMatrix(indice, lColFgAnaCodigo) = codAgrup
            FGAna.TextMatrix(indice, lColFgAnaDescricao) = descrAgrup
            If iRec > -1 Then
                FGAna.TextMatrix(indice, lColFgAnaP1) = RegistosRM(iRec).ReciboP1
                FGAna.TextMatrix(indice, lColFgAnaMedico) = RegistosRM(iRec).ReciboCodMedico
                FGAna.TextMatrix(indice, lColFgAnaCodEFR) = RegistosRM(iRec).ReciboEntidade
                FGAna.TextMatrix(indice, lColFgAnaDescrEFR) = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(iRec).ReciboEntidade)
                FGAna.TextMatrix(indice, lColFgAnaNrBenef) = RegistosRM(iRec).ReciboNrBenef
                FGAna.TextMatrix(indice, lColFgAnaPreco) = RegistosRM(iRec).ReciboTaxa
                'NELSONPSILVA GLINTT-HS-21312
                FGAna.TextMatrix(indice, lColFgAnaNAmostras) = RegistosRM(iRec).ReciboQuantidade
                '
            End If
            FGAna.row = indice
            FGAna.Col = 1
            If multiAna <> "" Then
                FGAna.CellBackColor = laranja
            Else
                FGAna.CellBackColor = vbWhite
            End If
            FGAna.TextMatrix(indice, lColFgAnaDestino) = RegistosA(indice).n_prescricao & "-" & RegistosA(indice).descr_destino
            'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
            If gUsaEnvioPDS = mediSim Then
                Call ChangeCheckboxState(indice, lColFgAnaConsenteEnvioPDS, EstadoCheckboxEnvioPDS_Unchecked)
            End If
            '
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "SELECT_Dados_Simples: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Dados_Simples"
    SELECT_Dados_Simples = False
    Exit Function
    Resume Next
End Function

Function SELECT_Descr_Ana(codAna As String) As String
    On Error GoTo TrataErro

    Dim rsDescr As adodb.recordset
    Set rsDescr = New adodb.recordset
    Dim Flg_SemTipo As Boolean
    
    codAna = UCase(codAna)
    If InStr(1, "SCP", Mid(codAna, 1, 1)) = 0 Then
        Flg_SemTipo = True
    End If
    
    With rsDescr
        If Flg_SemTipo = True Then
            .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                            "cod_ana_s ='S" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_s in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                        " UNION " & _
                            "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                            "cod_ana_c='C" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_c in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                        " UNION " & _
                            "(SELECT descr_perfis as descricao, 'P' Tipo, null inibe_marcacao FROM sl_perfis WHERE " & _
                            "cod_perfis='P" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND (cod_perfis in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ") OR flg_activo = '0'))"
                            
        Else
            .Source = "(SELECT descr_ana_s as descricao, 'S' Tipo, inibe_marcacao FROM sl_ana_s WHERE " & _
                            "cod_ana_s ='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_s in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & "))" & _
                        " UNION " & _
                            "(SELECT descr_ana_c as descricao, 'C' Tipo, inibe_marcacao FROM sl_ana_c WHERE " & _
                            "cod_ana_c='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND cod_ana_c in (Select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ")) " & _
                        " UNION " & _
                            "(SELECT descr_perfis as descricao, 'P' Tipo,null inibe_marcacao FROM sl_perfis WHERE " & _
                            "cod_perfis='" & UCase(codAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) AND (cod_perfis in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ") OR flg_activo = '0'))"

                        
        End If
        .ActiveConnection = gConexao
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With
    
    If rsDescr.RecordCount <= 0 Then
        SELECT_Descr_Ana = ""
    Else
        If BL_HandleNull(rsDescr!inibe_marcacao, "0") = "1" Then
            SELECT_Descr_Ana = ""
        Else
            SELECT_Descr_Ana = BL_HandleNull(rsDescr!descricao, "")
            If Flg_SemTipo = True Then
                codAna = rsDescr!tipo & codAna & ""
            End If
        End If
    End If
    
    rsDescr.Close
    Set rsDescr = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "SELECT_Descr_Ana: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Descr_Ana"
    SELECT_Descr_Ana = ""
    Exit Function
    Resume Next
End Function

Function Verifica_Ana_Prod(codAgrup As String, ByRef Marcar() As String) As Boolean
    On Error GoTo TrataErro

    Dim RsAnaProd As adodb.recordset
    Dim sql As String
    Dim i As Integer
    Dim k As Integer
    Dim CodProduto() As String
    Dim Resp As Integer
    Dim ListaProd() As String
    Dim JaTemProduto() As String
    Dim TotJaTemProduto As Integer
    Dim SQLAux As String
    Dim RsPerf As New adodb.recordset
    
    gColocaDataChegada = ""
    codAgrup = UCase(Trim(codAgrup))

    'PERFIL Activo ?
    If Mid(codAgrup, 1, 1) = "P" Then
        sql = "SELECT  flg_activo FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup)
        Set RsPerf = New adodb.recordset
        With RsPerf
            .Source = sql
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open , gConexao
        End With
        
        If RsPerf.RecordCount > 0 Then
            If BL_HandleNull(RsPerf!flg_activo, 0) = 0 Then
                RsPerf.Close
                sql = "SELECT * FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup)
                With RsPerf
                    .Source = sql
                    .CursorLocation = adUseServer
                    .CursorType = adOpenStatic
                    If gModoDebug = mediSim Then BG_LogFile_Erros sql
                    .Open , gConexao
                End With
                If RsPerf.RecordCount > 0 Then
                    While Not RsPerf.EOF
                        Verifica_Ana_Prod BL_HandleNull(RsPerf!cod_analise, ""), Marcar
                        RsPerf.MoveNext
                    Wend
                End If
            End If
        End If
        RsPerf.Close
        Set RsPerf = Nothing
    
    End If

'----------- PROCURAR NOS PRODUTOS JA MARCADOS, OS PRODUTOS DA ANALISE

sql = "SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto " & _
    " FROM sl_ana_s a LEFT OUTER JOIN  sl_produto p  ON a.cod_produto = p.cod_produto  " & _
    " Where " & _
    " Cod_Ana_S = " & BL_TrataStringParaBD(codAgrup) & _
    " Union " & _
    " SELECT ac.cod_ana_c cod_ana_s, ac.descr_ana_c descr_ana_s, ac.cod_produto, p.descr_produto " & _
    " FROM sl_ana_c ac LEFT OUTER JOIN  sl_produto p  ON ac.cod_produto = p.cod_produto  " & _
    " Where  " & _
    " ac.Cod_Ana_c in (select cod_analise from sl_ana_perfis where cod_perfis = " & BL_TrataStringParaBD(codAgrup) & ")" & _
    " Union " & _
    " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto " & _
    " FROM sl_ana_s a LEFT OUTER JOIN  sl_produto p  ON a.cod_produto = p.cod_produto   " & _
    " WHERE  " & _
    " cod_ana_s IN ( SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(codAgrup) & _
    " AND t_membro = 'A' )" & _
    " Union " & _
    " SELECT a.cod_ana_s, a.descr_ana_s, a.cod_produto, p.descr_produto " & _
    " FROM sl_ana_s a LEFT OUTER JOIN  sl_produto p  ON a.cod_produto = p.cod_produto  " & _
    " WHERE  " & _
    " cod_ana_s IN " & _
    "(( SELECT cod_analise as cod_ana_s FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & _
    " AND cod_analise LIKE 'S%' ) Union " & _
    " ( SELECT cod_membro as cod_ana_s FROM sl_membro WHERE cod_ana_c IN ( SELECT cod_analise FROM " & _
    " sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(codAgrup) & " AND cod_analise LIKE 'C%' ) AND t_membro = 'A' )) "

SQLAux = " select cod_perfis cod_ana_s, descr_perfis descr_ana_s, sl_perfis.cod_produto,descr_produto from sl_perfis, sl_produto where " & _
          " sl_perfis.cod_produto = sl_produto.cod_produto and cod_perfis = " & BL_TrataStringParaBD(codAgrup)
    
Set RsAnaProd = New adodb.recordset
With RsAnaProd
    .CursorLocation = adUseServer
    .CursorType = adOpenStatic
    .Source = SQLAux
    .ActiveConnection = gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros SQLAux
    .Open
End With


Verifica_Ana_Prod = False

ReDim JaTemProduto(0)
TotJaTemProduto = 0
ReDim ListaProd(0)
ReDim Marcar(0)
k = 0
If RsAnaProd.RecordCount <= 0 Then
    SQLAux = " select cod_ana_c cod_ana_s, descr_ana_c descr_ana_s, sl_ana_c.cod_produto,descr_produto from sl_ana_c, sl_produto where " & _
              " sl_ana_c.cod_produto = sl_produto.cod_produto and cod_ana_c = " & BL_TrataStringParaBD(codAgrup)
    Set RsAnaProd = New adodb.recordset
    With RsAnaProd
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        .Source = SQLAux
        .ActiveConnection = gConexao
        If gModoDebug = mediSim Then BG_LogFile_Erros SQLAux
        .Open
    End With
    If RsAnaProd.RecordCount <= 0 Then
        Set RsAnaProd = New adodb.recordset
        With RsAnaProd
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .Source = sql
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            .Open
        End With
    End If
End If

While Not RsAnaProd.EOF
    If Trim(BL_HandleNull(RsAnaProd!cod_produto, "")) <> "" Then
        For i = 1 To RegistosP.Count - 1
            If Trim(RegistosP(i).CodProd) = Trim(BL_HandleNull(RsAnaProd!cod_produto, "")) Then
                Verifica_Ana_Prod = True
                gColocaDataChegada = RegistosP(i).DtChega
                Exit For
            Else
                Verifica_Ana_Prod = False
            End If
        Next i
        
        If Verifica_Ana_Prod = False Then
            k = k + 1
            ReDim Preserve ListaProd(k)
            ListaProd(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";" & BL_HandleNull(RsAnaProd!cod_produto, "") & ";" & BL_HandleNull(RsAnaProd!descr_produto, "") & Space(35 - Len(Trim(BL_HandleNull(RsAnaProd!descr_produto, "")))) & " -> " & Trim(BL_HandleNull(RsAnaProd!descr_ana_s, ""))
        Else
            TotJaTemProduto = TotJaTemProduto + 1
            ReDim Preserve JaTemProduto(TotJaTemProduto)
            JaTemProduto(TotJaTemProduto) = BL_HandleNull(RsAnaProd!cod_ana_s, "")
        End If
    End If
    
    RsAnaProd.MoveNext
Wend
    
'----------- ALGUM NAO ESTA NA LISTA

If k <> 0 Then
    
    Verifica_Ana_Prod = False
    
    'Pergunta 1: Quer marcar o produto da analise ?
        
    If Flg_PergProd = True Then
        'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
        'que se deve adoptar a ultima resposta � pergunta
            
        If Resp_PergProd = True Then
            'respondeu na ultima vez que sim � pergunta 1
            Verifica_Ana_Prod = True
                
            'Pergunta 2: Quer inserir a data de hoje na chegada do produto ?
            
            If Flg_DtChProd = True Then
                'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
                'que se deve adoptar a ultima resposta � pergunta
                If Flg_PreencherDtChega = True Then
                    'respondeu na ultima vez que sim � pergunta 2
                    For i = 1 To UBound(ListaProd)
                        CodProduto = Split(ListaProd(i), ";")
                        InsereAutoProd CodProduto(1), dataAct
                    Next i
                Else
                    'respondeu na ultima vez que n�o � pergunta 2
                    For i = 1 To UBound(ListaProd)
                        CodProduto = Split(ListaProd(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                End If
            Else
                'O utilizador ainda n�o escolheu que a pergunta n�o
                'deve ser efectuada de novo, logo questiona-se a pergunta 2
                k = 0
                ReDim Marcar(0)
                For i = 1 To UBound(ListaProd)
                    CodProduto = Split(ListaProd(i), ";")
                    k = k + 1
                    ReDim Preserve Marcar(k)
                    Marcar(k) = CodProduto(0) & ";" & CodProduto(1)
                Next i
                
                FormGestaoRequisicaoPrivado.Enabled = False

                If FormPergProd.PerguntaProduto(ConfirmarDtChega, Flg_DtChProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
                    'respondeu sim � pergunta 2
                    Flg_PreencherDtChega = True
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), dataAct
                    Next i
                Else
                    'respondeu n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                    Flg_PreencherDtChega = False
                End If
                
            End If
        Else
            'respondeu na ultima vez que n�o � pergunta 1
            Verifica_Ana_Prod = True
        End If
    Else
        'O utilizador ainda n�o escolheu que a pergunta n�o
        'deve ser efectuada de novo, logo questiona-se a pergunta 1
        
        FormGestaoRequisicaoPrivado.Enabled = False
            
        If FormPergProd.PerguntaProduto(ConfirmarDefeito, Flg_PergProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
            'respondeu sim � pergunta 1
            Resp_PergProd = True
            Verifica_Ana_Prod = True
            
            'Pergunta 2: Quer inserir a data de hoje na chegada do produto ?
            
            If Flg_DtChProd = True Then
                'O utilizador j� indicou que n�o quer que a pergunta seja mais efectuada e
                'que se deve adoptar a ultima resposta � pergunta
                If Flg_PreencherDtChega = True Then
                    'respondeu na ultima vez que sim � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), dataAct
                    Next i
                Else
                    'respondeu na ultima vez que n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                End If
            Else
                If gRegChegaTubos <> 1 Then
                    If gIgnoraPerguntaProdutoTubo = 1 Then
                        For i = 1 To UBound(Marcar)
                            CodProduto = Split(Marcar(i), ";")
                            InsereAutoProd CodProduto(1), dataAct
                        Next i
                        Flg_PreencherDtChega = True
                    Else
                        'O utilizador ainda n�o escolheu que a pergunta n�o
                        'deve ser efectuada de novo, logo questiona-se a pergunta 2
                        If FormPergProd.PerguntaProduto(ConfirmarDtChega, Flg_DtChProd, SELECT_Descr_Ana(codAgrup), ListaProd, Marcar) = 0 Then
                            'respondeu sim � pergunta 2
                            Flg_PreencherDtChega = True
                            For i = 1 To UBound(Marcar)
                                CodProduto = Split(Marcar(i), ";")
                                InsereAutoProd CodProduto(1), dataAct
                            Next i
                        Else
                            'respondeu n�o � pergunta 2
                            For i = 1 To UBound(Marcar)
                                CodProduto = Split(Marcar(i), ";")
                                InsereAutoProd CodProduto(1), ""
                            Next i
                            Flg_PreencherDtChega = False
                        End If
                    End If
                Else
                    'respondeu n�o � pergunta 2
                    For i = 1 To UBound(Marcar)
                        CodProduto = Split(Marcar(i), ";")
                        InsereAutoProd CodProduto(1), ""
                    Next i
                    Flg_PreencherDtChega = False
                End If
            End If
        Else
            'respondeu n�o � pergunta 1
            Resp_PergProd = False
        End If
        
    End If

    ' retirar do array de an�lises a marcar o codigo do produto (fica "S1;S2;S3;S4;")
    For i = 1 To UBound(Marcar)
        CodProduto = Split(Marcar(i))
        Marcar(i) = CodProduto(0) & ";"
    Next i
     
    '----------- PERMITIR MARCAR AS ANALISES QUE NAO TEM PRODUTO ASSOCIADO
    
    If RsAnaProd.RecordCount <> 0 Then RsAnaProd.MoveFirst
    k = UBound(Marcar)
    While Not RsAnaProd.EOF
        If BL_HandleNull(RsAnaProd!cod_produto, "") = "" Then
            k = k + 1
            ReDim Preserve Marcar(k)
            Marcar(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";"
            If gColocaDataChegada = "" Then gColocaDataChegada = dataAct
            Verifica_Ana_Prod = True
        End If
        RsAnaProd.MoveNext
    Wend
    
    '----------- MARCAR AS ANALISES QUE J� TINHAM O PRODUTO REGISTADO
    
    k = UBound(Marcar)
    If TotJaTemProduto <> 0 Then
        For i = 1 To TotJaTemProduto
            k = k + 1
            ReDim Preserve Marcar(k)
            Marcar(k) = JaTemProduto(i) & ";"
        Next i
    End If
    
Else
    Verifica_Ana_Prod = True
    
    'Marca todas
    If RsAnaProd.RecordCount <> 0 Then RsAnaProd.MoveFirst
    k = 0
    ReDim Marcar(0)
    If Not RsAnaProd.EOF Then
        If gColocaDataChegada = "" And EcDataChegada.text <> "" Then    'sdo em vez de: If gColocaDataChegada = "" Then gColocaDataChegada = dataAct
            gColocaDataChegada = EcDataChegada.text                     'sdo
        ElseIf EcDataChegada.text = "" Then                             'sdo
            gColocaDataChegada = ""                                     'sdo
        End If                                                          'sdo
    End If
    While Not RsAnaProd.EOF
        k = k + 1
        ReDim Preserve Marcar(k)
        Marcar(k) = BL_HandleNull(RsAnaProd!cod_ana_s, "") & ";"
        RsAnaProd.MoveNext
    Wend

End If

RsAnaProd.Close
Set RsAnaProd = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Ana_Prod: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Ana_Prod"
    Verifica_Ana_Prod = False
    Exit Function
    Resume Next
End Function

Private Sub BtAdiantamentos_Click()
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    GereFrameAdiantamentos True
End Sub

Private Sub BtAjuda_Click()
    On Error GoTo TrataErro
    If FrameAjuda.Visible = True Then
        FrameAjuda.Visible = False
    Else
        FrameAjuda.Visible = True
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtAjuda_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtAjuda_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BTAnalisesMarcadas_Click()
    If TAna.Visible = True Then
        TAna.Visible = False
    Else
        TAna.Visible = True
    End If
End Sub

Private Sub BtAnularAdiamento_Click()
    If FgAdiantamentos.row < FgAdiantamentos.rows - 1 And FgAdiantamentos.row > 0 Then
        If RegistosAD(FgAdiantamentos.row).NumAdiantamento > 0 Then
            If RegistosAD(FgAdiantamentos.row).UserAnul = "" And RegistosAD(FgAdiantamentos.row).DtAnul = "" Then
                gMsgTitulo = "Adiantamento"
                gMsgMsg = "Tem a certeza que quer devolver o adiantamento: " & RegistosAD(FgAdiantamentos.row).NumAdiantamento & "?"
                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                If gMsgResp = vbYes Then
                    If RegistosAD(FgAdiantamentos.row).UserAnul = "" And RegistosAD(FgAdiantamentos.row).DtAnul = "" Then
                        RECIBO_AnulaAdiantamento CStr(EcNumReq), FgAdiantamentos.row, RegistosAD
                        PreencheFgAdiantamentos
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub BtAnularRecibo_Click()
    On Error GoTo TrataErro
    If gPassaRecFactus <> mediSim Then
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    If RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca Or RegistosR(FGRecibos.row).Estado = gEstadoReciboPago Then
        FormGestaoRequisicaoPrivado.Enabled = False
        FormCancelarRecibos.Show
    ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboAnulado Then
        FormGestaoRequisicaoPrivado.Enabled = False
        FormCancelarRecibos.Show
    End If
    Else
        CreditaFaturaRecibo
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtAnularRecibo_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtAnularRecibo_Click"
    Exit Sub
    Resume Next
End Sub



Private Sub BtCancelarReq_Click()
    On Error GoTo TrataErro

    Max = UBound(gFieldObjectProperties)
    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind
    
    MarcaLocal = rs.Bookmark
    FormGestaoRequisicaoPrivado.Enabled = False
    FormCancelarRequisicoes.Show
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtCancelarReq_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtCancelarReq_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub btCativarReq_Click()
        FormGestaoRequisicaoPrivado.Enabled = False
        FormCativarESP.EcEFR = EcCodEFR.text
        FormCativarESP.EcMedico = EcCodMedico.text
        FormCativarESP.EcPrestadora = EcCodProveniencia.text
        FormCativarESP.EcUtente = EcUtente.text
        FormCativarESP.EcTipoU = CbTipoUtente.text
        FormCativarESP.EcNome = EcNome.text
        FormCativarESP.Show
        'edgar.parada Glintt-HS-19165
        FormCativarESP.passaEstrur estrutOperation
        '
        'Set gFormActivo = FormCativarESP
End Sub

Private Sub BtCobranca_Click()
    On Error GoTo TrataErro

    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    
    ' ------------------------------------------------------------------------------------------
    ' COLOCA RECIBO PARA COBRAN�A
    ' ------------------------------------------------------------------------------------------
    If gPassaRecFactus <> mediSim Then
        If RegistosR(FGRecibos.row).Estado = gEstadoReciboPago Then
            RegistosR(FGRecibos.row).DtPagamento = ""
            RegistosR(FGRecibos.row).HrPagamento = ""
            RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca
        
            FGRecibos.TextMatrix(FGRecibos.row, lColRecibosDtPagamento) = ""
            FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "Em Cobran�a"
            BL_MudaCorFg FGRecibos, FGRecibos.row, Amarelo
            ColocaCobrancaPago FGRecibos.row, RegistosR(FGRecibos.row).SerieDoc, CStr(RegistosR(FGRecibos.row).NumDoc), 1
            BtPagamento.Enabled = True
            BtCobranca.Enabled = False
        End If
        PreencheFgRecibos
    Else
        If RecibosNew(FGRecibos.row).flg_estado = gEstadoFactFaturado And RecibosNew(FGRecibos.row).dt_pagamento <> "" Then
            docFinanceiros(FGRecibos.row).dt_pagamento = ""
            FACTUS_ReciboCobranca FGRecibos.row, gEstadoReciboCobranca
        End If
        AtualizaFgRecibosNew
        FGRecibos_Click
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtCobranca_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtCobranca_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtConfirm_Click()
    If EcNumReq <> "" And EcUtilConf = "" And EcDtConf = "" And EcHrConf = "" Then
        EcUtilConf = gCodUtilizador
        EcUtilNomeConf.caption = BL_SelNomeUtil(EcUtilConf.text)
        EcDtConf = dataAct
        EcHrConf = Bg_DaHora_ADO
        BD_Update
    End If
End Sub

Private Sub BtConsReq_Click()
    On Error GoTo TrataErro
    If EcSeqUtente.text <> "" Then
        FormGestaoRequisicaoPrivado.Enabled = False
        'gRequisicaoActiva = EcNumReq
        BL_LimpaDadosUtente
        gDUtente.Utente = EcUtente.text
        gDUtente.t_utente = BG_DaComboSel(CbTipoUtente)
'        gDUtente.nr_benef = EcNumBenef.Text
        gDUtente.nome_ute = EcNome.text
        gDUtente.dt_nasc_ute = EcDataNasc.text
        'BRUNODSANTOS AC-307 10.01.207
        'FormReqUte.Show
        FormConsNovo.Show
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtConsReq_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtConsReq_Click"
    Exit Sub
    Resume Next
End Sub



Private Sub BtContaCorrente_Click()
    If EcSeqUtente.text <> "" Then
        FormFactContaCorrente.Show
        FormFactContaCorrente.EcSeqUtente.text = EcSeqUtente.text
        FormFactContaCorrente.EcTutente.text = CbTipoUtente.text
        FormFactContaCorrente.EcUtente.text = EcUtente.text
        FormFactContaCorrente.FuncaoProcurar
    End If
    
End Sub

Private Sub BtDigitalizacao_Click()
    If EcSeqUtente <> "" And EcNumReq <> "" And EcUtilizadorCriacao <> "" Then
        FormDigitalizacao.Show 'vbModal
        FormDigitalizacao.EcSeqUtente = EcSeqUtente
        FormDigitalizacao.EcNome = EcNome
        FormDigitalizacao.EcNumReq = EcNumReq
        FormDigitalizacao.FuncaoProcurar
    End If
        

End Sub

Private Sub BtDomicilio_Click()
    On Error GoTo TrataErro
    If FrameDomicilio.Visible = True Then
        FrameDomicilio.Visible = False
    Else
        FrameDomicilio.Visible = True
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtDomicilio_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtDomicilio_Click"
    Exit Sub
    Resume Next
End Sub


Private Sub BtFgAnaRecRecalcula_Click()
    On Error GoTo TrataErro
    Dim i As Long
    Dim taxa As Double
    If RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido Or RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo Or _
        RegistosR(FGRecibos.row).Estado = gEstadoReciboPerdido Then
        taxa = 0
        For i = 1 To TotalAnaRecibo
            taxa = taxa + EstrutAnaRecibo(i).taxa
        Next
        RegistosR(FGRecibos.row).ValorPagar = taxa
        RegistosR(FGRecibos.row).ValorOriginal = taxa
        RegistosR(FGRecibos.row).NumAnalises = TotalAnaRecibo
        FGRecibos.TextMatrix(FGRecibos.row, lColRecibosValorPagar) = taxa
        
        If taxa = 0 And TotalAnaRecibo = 0 Then
            FGRecibos.RemoveItem FGRecibos.row
            LimpaColeccao RegistosR, FGRecibos.row
            
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtFgAnaRecRecalcula_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtFgAnaRecRecalcula_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtFgAnaRecSair_Click()
    On Error GoTo TrataErro
    FrameAnaRecibos.Visible = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtFgAnaRecSair_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtFgAnaRecSair_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtFichaCruzada_Click()
    On Error GoTo TrataErro
     'BRUNODSANTOS 09.06.2017 - LRV-469
    If MDIFormInicio.IDM_MODIFICAR.Enabled = True Then
        BD_Update
        'FuncaoProcurar
    End If
    '
    ImprimeEtiqNovo
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtFichaCruzada_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtFichaCruzada_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtHistAnt_Click()
    ConsultaHistoricoAlteracoes -1
End Sub

Private Sub BtHistCred_Click()
            'Glintt-HS-21231 Ecr� de hist�rico de credencial
        If FGAna.TextMatrix(LastRowA, lColFgAnaP1) <> "" And ExecutaCodigoA = True And LastColA = lColFgAnaP1 Then
                FormGestaoRequisicaoPrivado.Enabled = False
                FormHistCredencial.FuncaoProcurar FGAna.TextMatrix(LastRowA, LastColA)
        ElseIf (FGAna.TextMatrix(LastRowA, lColFgAnaP1) <> "") And ExecutaCodigoA = False Then
                FormGestaoRequisicaoPrivado.Enabled = False
                FormHistCredencial.FuncaoProcurar FGAna.TextMatrix(LastRowA, lColFgAnaP1)
        Else
            BG_Mensagem mediMsgBox, "Tem que selecionar uma credencial cativada!", vbExclamation, "Procurar"
        End If
        '
End Sub

Private Sub BtHistSeg_Click()
    ConsultaHistoricoAlteracoes 1
End Sub

Private Sub BtIdentif_Click()
    On Error GoTo TrataErro
    Dim i As Integer
    For i = 0 To 20
        If StackJanelas(i).Name = "FormIdentificaUtente" Then
            Exit Sub
        End If
    Next
    If EcNumReq <> "" Then
        FormIdentificaUtente.Show
        Set gFormActivo = FormIdentificaUtente
        FormIdentificaUtente.EcCodSequencial = EcSeqUtente
        FormIdentificaUtente.FuncaoProcurar
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtIdentif_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtIdentif_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtImprimirAdiantamento_Click()
    If FgAdiantamentos.row < FgAdiantamentos.rows - 1 And FgAdiantamentos.row > 0 Then
        If RegistosAD(FgAdiantamentos.row).NumAdiantamento > 0 Then
            RECIBO_ImprimeAdiantamento EcNumReq, RegistosAD(FgAdiantamentos.row).NumAdiantamento, EcPrinterRecibo
        End If
    End If
End Sub

Public Sub BtImprimirRecibo_Click()
    On Error GoTo TrataErro
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    If gUsaCopiaRecibo <> mediSim Then
        ImprimeRecibo ""
    Else
        MDIFormInicio.PopupMenu MDIFormInicio.HIDE_IMPR_REC
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtImprimirRecibo_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtImprimirRecibo_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtInfClinica_Click()
    On Error GoTo TrataErro
    
    Dim countEstrut As Integer
    
    countEstrut = 1
    
    'edgar.parada Glintt-HS-19165 * passar parametro para FormInformacaoClinica
    If UBound(estrutOperation) > 0 Then
      If RequisicaoESP(EcNumReq.text) = False And countEstrut = 0 Then
       'numCredencial
        gPassaParams.Param(0) = False 'n�o importado
      Else
        BG_LimpaPassaParams
        gPassaParams.id = "FormGestaoRequisicaoPrivado"
        gPassaParams.Param(0) = True 'importado
      End If
    End If
    '
    'for�ar a fazer o procurar da inf. clinica para a requisi��o em causa
    'caso j� exista uma
    
    BL_PreencheDadosUtente EcSeqUtente
    FormGestaoRequisicaoPrivado.Enabled = False
    FormInformacaoClinica.Show
    'If EcNumReq.Text <> "" Or EcSeqUtente.Text <> "" Then
        FormInformacaoClinica.FuncaoProcurar
    'End If
    'BRUNODSANTOS 21.10.2016 - LACTO-336
    If FormInformacaoClinica.flg_passou_no_ecra_ID = True Then
        If EcinfComplementar.text <> "" Then
            EcinfComplementar.text = FormIdentificaUtente.InfoComplementarID & " " & EcinfComplementar.text
        Else
            EcinfComplementar.text = FormIdentificaUtente.InfoComplementarID
        End If
              
        FormIdentificaUtente.InfoComplementarID = ""
        FormInformacaoClinica.flg_passou_no_ecra_ID = False
    End If
    FormInformacaoClinica.EcInfCompl = EcinfComplementar.text

Exit Sub
TrataErro:
        'edgar.parada Glintt-HS-19165 * tratar exception
    If Err.Number = 9 Then
      countEstrut = 0
      Resume Next
    End If
    '
    BG_LogFile_Erros "BtInfClinica_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtInfClinica_Click"
    Exit Sub
    Resume Next
End Sub



Private Sub BtNotas_Click()
    On Error GoTo TrataErro
    FormGestaoRequisicaoPrivado.Enabled = False
    FormNotasReq.Show
    FormNotasReq.EcNumReq = EcNumReq
    FormNotasReq.EcSeqUtente = EcSeqUtente
    FormNotasReq.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotas_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotas_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtNotasVRM_Click()
    On Error GoTo TrataErro
    FormGestaoRequisicaoPrivado.Enabled = False
    FormNotasReq.Show
    FormNotasReq.EcNumReq = EcNumReq
    FormNotasReq.EcSeqUtente = EcSeqUtente
    FormNotasReq.FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtNotasVRM_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtNotasVRM_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtObsEspecif_Click()
    On Error GoTo TrataErro
    EcObsEspecif = RegistosP(LastRowP).ObsEspecif
    FrameObsEspecif.Visible = True
    EcObsEspecif.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtObsEspecif_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtObsEspecif_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtOkObsEspecif_Click()
    On Error GoTo TrataErro
    RegistosP(LastRowP).ObsEspecif = Trim(EcObsEspecif)
    FgProd.SetFocus
    EcObsEspecif = ""
    FrameObsEspecif.Visible = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtOkObsEspecif_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtOkObsEspecif_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPagamento_Click()
    On Error GoTo TrataErro
    Dim i As Integer
    
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    
    If gPassaRecFactus <> mediSim Then
        For i = 1 To RegistosAD.Count
            If RegistosAD.Item(i).Estado = gEstadoReciboPago Then
                MsgBox "Existem adiantamento(s) emitido(s). N�o � possivel colocar fatura recibo como paga.", vbExclamation, " Adiantamento(s)"
                Exit Sub
            End If
        Next i
        
        ' ------------------------------------------------------------------------------------------
        ' COLOCA RECIBO COMO PAGO
        ' ------------------------------------------------------------------------------------------
        If RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca Then
            'RegistosR(FGRecibos.Row).us = dataAct
            RegistosR(FGRecibos.row).UserEmi = gCodUtilizador
            RegistosR(FGRecibos.row).DtPagamento = dataAct
            RegistosR(FGRecibos.row).HrPagamento = Bg_DaHora_ADO
            RegistosR(FGRecibos.row).Estado = gEstadoReciboPago
        
            FGRecibos.TextMatrix(FGRecibos.row, lColRecibosDtPagamento) = RegistosR(FGRecibos.row).DtPagamento & " " & RegistosR(FGRecibos.row).HrPagamento
            FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "Pago"
            BL_MudaCorFg FGRecibos, FGRecibos.row, Verde
            
            ColocaCobrancaPago FGRecibos.row, RegistosR(FGRecibos.row).SerieDoc, CStr(RegistosR(FGRecibos.row).NumDoc), 0
            BtPagamento.Enabled = False
            BtCobranca.Enabled = True
        End If
    Else
        If RecibosNew(FGRecibos.row).flg_estado = gEstadoFactFaturado And RecibosNew(FGRecibos.row).dt_pagamento = "" Then
            docFinanceiros(FGRecibos.row).dt_pagamento = Bg_DaData_ADO
            FACTUS_ReciboCobranca FGRecibos.row, gEstadoReciboPago
        End If
        AtualizaFgRecibosNew
        FGRecibos_Click
    End If


Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPagamento_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPagamento_Click"
    Exit Sub
    Resume Next
End Sub
Private Sub BtPesquisaAna_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error GoTo TrataErro
    FGAna.Col = 0
    FGAna.row = FGAna.rows - 1
    ExecutaCodigoA = True
    If Button = 1 Then
        If RegistosA(FGAna.row).Estado <> "-1" And RegistosA(FGAna.row).codAna <> "" And FGAna.Col = 0 Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser alterada: j� cont�m resultados!"
        Else
            MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANAMARC
        End If
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaAna_MouseDown: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaAna_MouseDown"
    Exit Sub
    Resume Next
End Sub


Private Sub BtPesquisaEFR2_Click()
    BtPesquisaEntFin_Click
End Sub

Private Sub BtPesquisaEntFin_Click()
   PA_PesquisaEFR EcCodEFR, EcDescrEFR, CStr(gCodLocal)
    EcCodEFR_LostFocus
End Sub

Private Sub BtPesquisaEspecif_Click()
    On Error GoTo TrataErro
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "sl_especif.descr_especif"
    CamposEcran(1) = "sl_especif.descr_especif"
    Tamanhos(1) = 3000
    Headers(1) = "Descri��o"
    
    ChavesPesq(2) = "sl_prod_esp.cod_especif"
    CamposEcran(2) = "sl_prod_esp.cod_especif"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = " sl_prod_esp,sl_especif "
    CWhere = " sl_prod_esp.cod_produto='" & UCase(FgProd.TextMatrix(FgProd.row, 0)) & "' AND sl_prod_esp.cod_especif=sl_especif.cod_especif"
    CampoPesquisa = ""
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "ORDER BY DESCR_especif ", " Especifica��es")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            FgProd.TextMatrix(FgProd.row, 3) = Resultados(1)
            FgProd.TextMatrix(FgProd.row, 2) = Resultados(2)
            FgProd.row = 4
        End If
    Else
        Set CamposRetorno = Nothing
        Set CamposRetorno = New ClassPesqResultados
    
        PesqRapida = False

        ChavesPesq(1) = "descr_especif"
        CamposEcran(1) = "descr_especif"
        Tamanhos(1) = 3000
        Headers(1) = "Descri��o"
        
        ChavesPesq(2) = "cod_especif"
        CamposEcran(2) = "cod_especif"
        Tamanhos(2) = 1000
        Headers(2) = "C�digo"
        
        CamposRetorno.InicializaResultados 2
        
        CFrom = " sl_especif "
        CWhere = ""
        CampoPesquisa = "descr_especif"
        
        PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Especifica��es")
        
        If PesqRapida = True Then
            FormPesqRapidaAvancada.Show vbModal
            CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
            If Not CancelouPesquisa Then
                FgProd.TextMatrix(FgProd.row, 3) = Resultados(1)
                FgProd.TextMatrix(FgProd.row, 2) = Resultados(2)
                RegistosP(LastRowP).CodEspecif = UCase(Resultados(2))
                FgProd.Col = 4
            End If
        Else
            BG_Mensagem mediMsgBox, "N�o existem especifica��es codificadas!", vbExclamation, "Especifica��es"
            FgProd.Col = 2
        End If
    End If
    FgProd.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaEspecif_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaEspecif_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaLocalEntrega_Click()
    PA_PesquisaLocalEntrega EcCodLocalEntrega, EcDescrLocalEntrega

End Sub

Private Sub BtPesquisaMedico_Click()
    On Error GoTo TrataErro


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_med"
    CamposEcran(1) = "cod_med"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "nome_med"
    CamposEcran(2) = "nome_med"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_medicos"
    CampoPesquisa1 = "nome_med"
    ClausulaWhere = " (flg_invisivel is null or flg_invisivel = 0) "
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar M�dicos")
    
    mensagem = "N�o foi encontrado nenhum M�dico."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMedico.text = Resultados(1)
            EcNomeMedico.text = Resultados(2)
            
            'LJMANSO-310
            Call AtualizaEstrutFact
            '
            
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaMedico_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaMedico_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaProduto_click()
    On Error GoTo TrataErro
    
    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
                        "descr_produto", "seq_produto", _
                         EcPesqRapProduto
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaProduto_click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaProduto_click"
    Exit Sub
    Resume Next
End Sub


Sub Funcao_CopiaUte()
    On Error GoTo TrataErro
    
    If Trim(gDUtente.seq_utente) <> "" Then
        EcSeqUtente = CLng(gDUtente.seq_utente)
        PreencheDadosUtente
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe utente activo!", , "Copiar utente"
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_CopiaUte: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_CopiaUte"
    Exit Sub
    Resume Next
End Sub

Sub Actualiza_Estrutura_MAReq(inicio As Integer)
    On Error GoTo TrataErro

    Dim j As Long

    If inicio <> UBound(MaReq) Then
        For j = inicio To UBound(MaReq) - 1
            MaReq(j).Cod_Perfil = MaReq(j + 1).Cod_Perfil
            MaReq(j).Descr_Perfil = MaReq(j + 1).Descr_Perfil
            MaReq(j).cod_ana_c = MaReq(j + 1).cod_ana_c
            MaReq(j).Descr_Ana_C = MaReq(j + 1).Descr_Ana_C
            MaReq(j).cod_ana_s = MaReq(j + 1).cod_ana_s
            MaReq(j).descr_ana_s = MaReq(j + 1).descr_ana_s
            MaReq(j).Ord_Ana = MaReq(j + 1).Ord_Ana
            MaReq(j).Ord_Ana_Compl = MaReq(j + 1).Ord_Ana_Compl
            MaReq(j).Ord_Ana_Perf = MaReq(j + 1).Ord_Ana_Perf
            MaReq(j).cod_agrup = MaReq(j + 1).cod_agrup
            MaReq(j).N_Folha_Trab = MaReq(j + 1).N_Folha_Trab
            MaReq(j).dt_chega = MaReq(j + 1).dt_chega
            MaReq(j).Flg_Apar_Transf = MaReq(j + 1).Flg_Apar_Transf
            MaReq(j).flg_estado = MaReq(j + 1).flg_estado
            MaReq(j).Flg_Facturado = MaReq(j + 1).Flg_Facturado
            MaReq(j).seq_req_tubo = MaReq(j + 1).seq_req_tubo
            MaReq(j).indice_tubo = MaReq(j + 1).indice_tubo
        Next
    End If
    
    ReDim Preserve MaReq(UBound(MaReq) - 1)

Exit Sub
TrataErro:
    BG_LogFile_Erros "Actualiza_Estrutura_MAReq: " & Err.Number & " - " & Err.Description, Me.Name, "Actualiza_Estrutura_MAReq"
    Exit Sub
    Resume Next
End Sub

Sub Insere_aMeio_RegistosA(inicio As Integer)
    On Error GoTo TrataErro

    Dim j As Long

    CriaNovaClasse RegistosA, -1, "ANA"
    
    If inicio <> RegistosA.Count Then
        For j = RegistosA.Count To inicio + 1 Step -1
            RegistosA(j).codAna = RegistosA(j - 1).codAna
            RegistosA(j).descrAna = RegistosA(j - 1).descrAna
            RegistosA(j).NReqARS = RegistosA(j - 1).NReqARS
            RegistosA(j).p1 = RegistosA(j - 1).p1
            RegistosA(j).Estado = RegistosA(j - 1).Estado
            RegistosA(j).DtChega = RegistosA(j - 1).DtChega
            RegistosA(j).ObsAnaReq = RegistosA(j - 1).ObsAnaReq
            RegistosA(j).seqObsAnaReq = RegistosA(j - 1).seqObsAnaReq
        
        Next
    End If

    RegistosA(inicio).codAna = ""
    RegistosA(inicio).descrAna = ""
    RegistosA(inicio).NReqARS = ""
    RegistosA(inicio).p1 = ""
    RegistosA(inicio).Estado = ""
    RegistosA(inicio).DtChega = ""
    RegistosA(inicio).ObsAnaReq = ""
    RegistosA(inicio).seqObsAnaReq = ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "Insere_aMeio_RegistosA: " & Err.Number & " - " & Err.Description, Me.Name, "Insere_aMeio_RegistosA"
    Exit Sub
    Resume Next
End Sub

Function Calcula_Valor(valor As Double, TaxaConv) As Double
    On Error GoTo TrataErro
    
    Calcula_Valor = Round(valor / TaxaConv)
Exit Function
TrataErro:
    BG_LogFile_Erros "Calcula_Valor: " & Err.Number & " - " & Err.Description, Me.Name, "Insere_aMeCalcula_Valorio_RegistosA"
    Calcula_Valor = 0
    Exit Function
    Resume Next
End Function

Private Sub BtRegistaMedico_Click()
    On Error GoTo TrataErro
    If gLAB = "BIO" Then
        FormGestaoRequisicaoPrivado.Enabled = False
        FormMedicosReq.Show
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtRegistaMedico_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtRegistaMedico_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtSairAdiantamento_Click()
    GereFrameAdiantamentos False
End Sub

Private Sub CbDestino_Click()
 On Error GoTo TrataErro
 
 Dim indice As Long
     
    For indice = 1 To RegistosA.Count - 1
        RegistosA(indice).n_prescricao = "1"
        RegistosA(indice).cod_destino = CbDestino.ItemData(CbDestino.ListIndex)
        RegistosA(indice).descr_destino = BL_SelCodigo("SL_TBF_T_DESTINO", "DESCR_T_DEST", "COD_T_DEST", RegistosA(indice).cod_destino)
        FGAna.TextMatrix(indice, lColFgAnaDestino) = RegistosA(indice).n_prescricao & "-" & RegistosA(indice).descr_destino
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbDestino_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "CbDestino_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub CbHemodialise_Click()
    If CbHemodialise.ListIndex <> mediComboValorNull Then
        If EcCodIsencao <> gTipoIsencaoIsento Then
            BG_Mensagem mediMsgBox, "Para indicar uma doen�a o Utente ter� que ser isento.", vbInformation
            CbHemodialise.ListIndex = mediComboValorNull
        Else
            BL_ColocaComboTexto "sl_cod_hemodialise", "seq_hemodialise", "cod_hemodialise", EcHemodialise, CbHemodialise
        End If
    Else
        EcHemodialise = ""
    End If
End Sub

Private Sub CbTipoIsencao_Click()
    On Error GoTo TrataErro
    Dim i As Long
    Dim iResp As Integer
    Dim iAnaFact As Integer
    Dim iAna As Integer
    
    If CbTipoIsencao.ListIndex > mediComboValorNull Then
        If EcCodEFR <> "" And CbTipoIsencao.ItemData(CbTipoIsencao.ListIndex) = gTipoIsencaoIsento Then
            CbHemodialise.ListIndex = mediComboValorNull
            CbHemodialise_Click
            CbHemodialise.Enabled = False
            If VerificaEfrHemodialise(EcCodEFR) = True Then
                CbHemodialise.Enabled = True
            End If
        Else
            CbHemodialise.ListIndex = mediComboValorNull
            CbHemodialise_Click
            CbHemodialise.Enabled = False
        End If
    
        If CStr(CbTipoIsencao.ItemData(CbTipoIsencao.ListIndex)) <> EcCodIsencao And RegistosA.Count > 1 Then
            gMsgTitulo = "Isen��o"
            gMsgMsg = "Deseja alterar todas as an�lises para " & CbTipoIsencao & "?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                If gPassaRecFactus <> mediSim Then
                    For i = 1 To RegistosRM.Count - 1
                        If (RegistosRM(i).ReciboNumDoc = "0" Or RegistosRM(i).ReciboNumDoc = "0") And (RegistosRM(i).ReciboEntidade <> gEfrParticular Or CbTipoIsencao.ItemData(CbTipoIsencao.ListIndex) = gTipoIsencaoBorla) Then
                            AlteraIsencao CbTipoIsencao.ItemData(CbTipoIsencao.ListIndex), i, -1
                        Else
                            AlteraIsencao gTipoIsencaoNaoIsento, i, -1
                        End If
                    Next
                    ActualizaLinhasRecibos -1
                    If EcNumReq <> "" And EcUtilizadorCriacao <> "" Then
                        FuncaoModificar (True)
                    End If
                Else
                For i = 1 To FGAna.rows - 2
                    AlteraIsencao2 CInt(i), CbTipoIsencao.ItemData(CbTipoIsencao.ListIndex)
                Next i
                End If
            End If
        End If
    End If
    BL_ColocaComboTexto "sl_isencao", "seq_isencao", "cod_isencao", EcCodIsencao, CbTipoIsencao
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbTipoIsencao_Click: " & Err.Number & " - " & Err.Description, Me.Name, "CbTipoIsencao_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub AlteraIsencao2(linha As Integer, isencao As Integer)
    Dim cod_dom As String
    Dim i As Integer
    Dim iResp As Integer
    Dim iAna As Integer
    Dim iAnaFact As Integer
    Dim hemodialise As Integer
    Dim convencao As Integer
    Dim iAna2 As Integer
    
    If CbHemodialise.ListIndex > mediComboValorNull Then
        hemodialise = CbHemodialise.ItemData(CbHemodialise.ListIndex)
    Else
        hemodialise = mediComboValorNull
    End If
    
    If CbConvencao.ListIndex > mediComboValorNull Then
        convencao = CbConvencao.ItemData(CbConvencao.ListIndex)
    Else
        convencao = mediComboValorNull
    End If
    
    If CbCodUrbano.ListIndex > mediComboValorNull Then
        cod_dom = CbCodUrbano.ItemData(CbCodUrbano.ListIndex)
    Else
        cod_dom = mediComboValorNull
    End If
    
    For iAna2 = 1 To TotalAnalisesFact
        If AnalisesFact(iAna2).cod_ana = RegistosA(linha).codAna Then
            If AnalisesFact(iAna2).iResp <= 0 Or AnalisesFact(iAna2).iAnaFact <= 0 Then
                FACTUS_AdicionaAnalise EcCodEFR.text, BL_HandleNull(UCase(RegistosA(linha).codAna), ""), _
                                         BL_HandleNull(EcDataPrevista.text, Bg_DaData_ADO), 0, mediComboValorNull, BG_DaComboSel(CbTipoIsencao), _
                                         CLng(iAna2), EcNumBenef.text, CbTipoUtente, EcUtente, EcNumReq.text, "", convencao, _
                                         EcCodMedico.text, cod_dom, BL_HandleNull(EcKm.text, mediComboValorNull), _
                                          EcCodPostalAux.text, EcDescrPostal.text, EcCodProveniencia.text, mediComboValorNull
                PreencheDadosNovoRecibo CLng(iAna2)
                AtualizaFgRecibosNew
                
            Else
                For iResp = 1 To fa_movi_resp_tot
                    For iAnaFact = 1 To fa_movi_resp(iResp).totalAna
                        If fa_movi_resp(iResp).analises(iAnaFact).iAnalise = iAna2 Then
                            If (fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe <> 3 And fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx <> 3) Then
                                If FACTUS_AlteraIsencao(isencao, iResp, iAnaFact, AnalisesFact(iAna2).cod_ana, _
                                                        1, hemodialise, mediComboValorNull) = True Then
                                    AtualizaDadosMoviFact iAna2
                                    AtualisaDadosMoviFact CLng(iAna2), AnalisesFact(iAna2).cod_ana
                                     'NELSONPSILVA UALIA-856 31.01.2019
                                    If AnalisesFact(iAna2).cod_rubr = mediComboValorNull Then
                                        If fa_movi_resp(iResp).analises(iAna2).val_Taxa > 0 Then
                                            FGAna.TextMatrix(iAna2, lColFgAnaPreco) = Round(fa_movi_resp(iResp).analises(iAna2).val_Taxa * fa_movi_resp(iResp).analises(iAna2).qtd, 2)
                                        Else
                                            FGAna.TextMatrix(iAna2, lColFgAnaPreco) = fa_movi_resp(iResp).analises(iAna2).val_pag_doe
                                        End If
                                    End If
                                    '
                                End If
                            Else
                                BG_Mensagem mediMsgBox, "Isen��o n�o pode ser alterada.", vbInformation, ""
                                
                            End If
                        End If
                    Next iAnaFact
                Next iResp
            End If
        End If
    Next iAna2
    AtualizaFgRecibosNew
    
End Sub

Private Sub CbTipoIsencao_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    BG_LimpaOpcao CbTipoIsencao, KeyCode

Exit Sub
TrataErro:
    BG_LogFile_Erros "CbTipoIsencao_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbTipoIsencao_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub CbTipoUtente_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    BG_LimpaOpcao CbTipoUtente, KeyCode

Exit Sub
TrataErro:
    BG_LogFile_Erros "CbTipoUtente_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbTipoUtente_KeyDown"
    Exit Sub
    Resume Next
End Sub

Public Sub CbTipoUtente_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    
    Call EcUtente_Validate(Cancel)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbTipoUtente_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "CbTipoUtente_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub CbUrgencia_Click()
'    EcUrgencia.Text = Mid(CbUrgencia.Text, 1, 1)
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    BG_LimpaOpcao CbUrgencia, KeyCode

Exit Sub
TrataErro:
    BG_LogFile_Erros "CbUrgencia_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbUrgencia_KeyDown"
    Exit Sub
    Resume Next
End Sub



Private Sub CkCobrarDomicilio_Click()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim cod_efr As String
    Dim codAna As String
    Dim hemodialise As Long
    Dim convencao As Long
    Dim cod_dom As Long
    
    If flg_preencheCampos = True Then
        Exit Sub
    End If
    
    If CkCobrarDomicilio.value = vbChecked Then

        If EcCodEFR <> "" Then
            If IF_EFRComparticipaDomicilio(EcCodEFR) = True Then
                cod_efr = EcCodEFR
            Else
                cod_efr = gEfrParticular
            End If
        End If
        
        If gPassaRecFactus <> mediSim Then
            If BG_DaComboSel(CbCodUrbano) = lCodUrbano And gCodAnaDomUrbano <> "" And gCodAnaDomUrbano <> "-1" Then
                AdicionaReciboManual 1, cod_efr, 0, "0", gCodAnaDomUrbano, 1, "", gCodAnaDomUrbano, False
            ElseIf BG_DaComboSel(CbCodUrbano) = lCodNaoUrbano And gCodAnaDomNaoUrbano <> "" And gCodAnaDomNaoUrbano <> "-1" Then
                AdicionaReciboManual 1, cod_efr, 0, "0", gCodAnaDomNaoUrbano, CInt(BL_HandleNull(EcKm, 0)), "", gCodAnaDomNaoUrbano, False
            End If
        Else
            If BG_DaComboSel(CbCodUrbano) = lCodUrbano And gCodAnaDomUrbano <> "" And gCodAnaDomUrbano <> "-1" Then
                codAna = gCodAnaDomUrbano
            ElseIf BG_DaComboSel(CbCodUrbano) = lCodNaoUrbano And gCodAnaDomNaoUrbano <> "" And gCodAnaDomNaoUrbano <> "-1" Then
                codAna = gCodAnaDomNaoUrbano
            End If
            If codAna <> "" Then
                If CbHemodialise.ListIndex > mediComboValorNull Then
                    hemodialise = CbHemodialise.ItemData(CbHemodialise.ListIndex)
                End If
                If CbConvencao.ListIndex > mediComboValorNull Then
                    convencao = CbConvencao.ItemData(CbConvencao.ListIndex)
                Else
                    convencao = mediComboValorNull
                End If
                If CbCodUrbano.ListIndex > mediComboValorNull Then
                    cod_dom = CbCodUrbano.ItemData(CbCodUrbano.ListIndex)
                Else
                    cod_dom = mediComboValorNull
                End If
                FACTUS_AdicionaAnalise cod_efr, codAna, BL_HandleNull(EcDataPrevista.text, Bg_DaData_ADO), 0, mediComboValorNull, _
                                         BG_DaComboSel(CbTipoIsencao), mediComboValorNull, EcNumBenef.text, CbTipoUtente, EcUtente, _
                                        EcNumReq.text, "", convencao, EcCodMedico.text, cod_dom, BL_HandleNull(EcKm.text, mediComboValorNull), _
                                          EcCodPostalAux.text, EcDescrPostal.text, EcCodProveniencia.text, mediComboValorNull
                AtualizaFgRecibosNew
            End If
        
        End If
    ElseIf CkCobrarDomicilio.value = vbUnchecked Then
        If BG_DaComboSel(CbCodUrbano) = lCodUrbano And gCodAnaDomUrbano <> "" And gCodAnaDomUrbano <> "-1" Then
                        If gPassaRecFactus <> mediSim Then
            For i = 1 To RegistosRM.Count
                If RegistosRM(i).ReciboCodFacturavel = gCodAnaDomUrbano Then
                    EliminaReciboManual CLng(i), RegistosRM(i).ReciboCodFacturavel, True
                    LimpaColeccao RegistosRM, CLng(i)

                    Exit Sub
                End If
            Next
           Else
                'nada
           End If
        ElseIf BG_DaComboSel(CbCodUrbano) = lCodNaoUrbano And gCodAnaDomNaoUrbano <> "" And gCodAnaDomNaoUrbano <> "-1" Then
            For i = 1 To RegistosRM.Count
                If RegistosRM(i).ReciboCodFacturavel = gCodAnaDomNaoUrbano Then
                    EliminaReciboManual CLng(i), RegistosRM(i).ReciboCodFacturavel, True
                    LimpaColeccao RegistosRM, CLng(i)
                    Exit Sub
                End If
            Next
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "CkCobrarDomicilio_Click: " & Err.Number & " - " & Err.Description, Me.Name, "CkCobrarDomicilio_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub cmdOK_Click()
    On Error GoTo TrataErro
    
    FuncaoProcurar
Exit Sub
TrataErro:
    BG_LogFile_Erros "cmdOK_Click: " & Err.Number & " - " & Err.Description, Me.Name, "cmdOK_Click"
    Exit Sub
    Resume Next
End Sub





Private Sub BtResumo_Click()
    ImprimeResumo
End Sub

Private Sub btAnaAcresc_Click()
    ' ---------------------------------------------------------------------------
    ' VERIFICA SE ANALISE ESTA MAPEADA
    ' ---------------------------------------------------------------------------
    AdicionaReciboManual 1, RegistosR(FGRecibos.row).codEntidade, RegistosR(FGRecibos.row).NumDoc, RegistosR(FGRecibos.row).SerieDoc, EcCodAnaAcresc, BL_HandleNull(EcQtdAnaAcresc, 1), "", EcCodAnaAcresc, False
    FGRecibos_DblClick
    EcCodAnaAcresc = ""
    EcDescrAnaAcresc = ""
    EcQtdAnaAcresc = ""
    EcTaxaAnaAcresc = ""

End Sub



Private Sub EcAuxAna_GotFocus()
    On Error GoTo TrataErro
    
    Dim i As Integer
    
    On Local Error Resume Next
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i
    
    If EcAuxAna.SelLength = 0 Then
        Sendkeys ("{END}")
    End If
   
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxAna_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxAna_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxAna_LostFocus()
    On Error GoTo TrataErro

    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i
    
    EcAuxAna.Visible = False
    If FGAna.Enabled = True Then FGAna.SetFocus
    Me.Enabled = True
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxAna_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxAna_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxAna_KeyPress(KeyAscii As Integer)
    On Error GoTo TrataErro
    
    If Enter = True Then KeyAscii = 0

Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxAna_KeyPress: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxAna_KeyPress"
    Exit Sub
    Resume Next
End Sub


Public Sub EcAuxAna_KeyDown(KeyCode As Integer, Shift As Integer)
     On Error GoTo TrataErro
    Dim a1 As String
    Dim CodigoMarcado As String
    Dim i As Long
    Dim aux As String
    Dim an_aux As String
    Dim rv As Integer
    Dim iRec As Long
    Dim iAna As Integer
    
    ' UTILIZADOR MARCA UMA MAS PODE MAPEAR PARA OUTRA. ESTA VARIAVEL GUARDA O QUE ELE REALMENTE DIGITA
    Dim AnaFacturavel As String
    a1 = EcAuxAna
    ' For�a o formato "0".
    'DoEvents
    Enter = False
    'edgar.parada  LJMANSO-351 10.09.2018
    Dim convencao As Integer
    Dim iResp As Integer
    '
    If (KeyCode = 27 And Shift = 0) Or (KeyCode = 13 And Shift = 0 And Trim(EcAuxAna.text) = "") Then
        Enter = True
        EcAuxAna.Visible = False
        FGAna.SetFocus
    ElseIf (KeyCode = 13 And Shift = 0) Or (KeyCode = 9 And Shift = 0) Then
   
        
        Enter = True
        DoEvents
        CodigoMarcado = EcAuxAna
        
        Select Case LastColA
            Case lColFgAnaCodigo ' Codigo da An�lise
                'edgar.parada Glintt-HS-19165 * se for importado
                If UBound(estrutOperation) >= 0 Then
                    'btCativarReq.Enabled = False
                    AdicionaAnalise CodigoMarcado, False, True, ""
                Else
                    AdicionaAnalise CodigoMarcado, False, False, ""
                End If
                '
            Case lColFgAnaP1 ' P1
                ' If gPassaRecFactus <> mediSim Then
                    ' iRec = DevIndice(RegistosA(LastRowA).codAna)
                    ' If FGAna.TextMatrix(LastRowA, lColFgAnaCodigo) <> "" Then
                        ' If iRec > -1 Then
                            ' For i = 1 To RegistosRM.Count - 1
                                ' If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                    ' RegistosRM(i).ReciboP1 = CodigoMarcado
                                    ' FGAna.TextMatrix(LastRowA, lColFgAnaP1) = CodigoMarcado
                                    ' RegistosRM(i).ReciboOrdemMarcacao = RetornaOrdemP1(i)
                                    ' 'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
                                    ' If gUsaEnvioPDS = mediSim Then
                                        ' AtualizaColEnvioPDS
                                    ' End If
                                    ' '
                                ' End If
                            ' Next
                        ' End If
                    ' End If
                ' ElseIf gPassaRecFactus = mediSim Then
                    ' If fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).flg_estado_doe <> 3 And _
                    ' fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).flg_estado_tx <> 3 Then
                        ' fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).nr_req_ars = CodigoMarcado
                        ' FGAna.TextMatrix(LastRowA, lColFgAnaP1) = CodigoMarcado
                        ' 'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
                        ' If gUsaEnvioPDS = mediSim Then
                            ' AtualizaColEnvioPDS
                        ' End If
                        ' '
                    ' End If
                    'edgar.parada  LJMANSO-351 10.09.2018
                    iResp = FACTUS_DevolveLinhaResp(EcNumReq.text, FGAna.TextMatrix(LastRowA, lColFgAnaCodEFR))
                    convencao = obtemConvencao(indexCred, FGAna.TextMatrix(LastRowA, lColFgAnaCodigo), EcAuxAna.text)
                    Call FACTUS_AtualizaTipoReqFromConvencao(convencao, iResp, CInt(LastRowA))
                    '
                'If CredencialESP(Trim(EcAuxAna.text)) = False Then
                    'Glintt-HS-19835 09.04.2019
                    'Call TrataCredencialESP(CodigoMarcado, RegistosA(LastRowA).codAna, False, "", "", "", "")
                    Call TrataCredencialESP(CodigoMarcado, RegistosA(LastRowA).codAna, False, "", FGAna.TextMatrix(LastRowA, lColFgAnaMedico), FGAna.TextMatrix(LastRowA, lColFgAnaCodEFR), "", FGAna.TextMatrix(LastRowA, lColFgAnaNAmostras))
                'Else
                '    EcAuxAna.text = ""
                '    EcAuxAna.Visible = False
                '    BG_Mensagem mediMsgBox, "Credencial j� marcada pelo processo de cativa��o!", vbInformation, App.ProductName
                'End If
            Case lColFgAnaNrBenef ' nr benef
                If gPassaRecFactus <> mediSim Then
                    iRec = DevIndice(RegistosA(LastRowA).codAna)
                    CodigoMarcado = UCase(CodigoMarcado)
                    'If FGAna.TextMatrix(LastRowA, lColFgAnaNrBenef) <> "" Then
                        If iRec > -1 Then
                            For i = 1 To RegistosRM.Count - 1
                                If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                    RegistosRM(i).ReciboNrBenef = CodigoMarcado
                                    FGAna.TextMatrix(LastRowA, lColFgAnaNrBenef) = CodigoMarcado
                                End If
                            Next
                        End If
                    'End If
                ElseIf gPassaRecFactus = mediSim Then
                    If fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).flg_estado_doe <> 3 And _
                    fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).flg_estado_tx <> 3 Then
                        fa_movi_resp(AnalisesFact(LastRowA).iResp).n_benef_doe = CodigoMarcado
                        FGAna.TextMatrix(LastRowA, lColFgAnaNrBenef) = CodigoMarcado
                    End If
                End If
            Case lColFgAnaMedico ' MEDICO
                If gPassaRecFactus <> mediSim Then
                    iRec = DevIndice(RegistosA(LastRowA).codAna)
                    If FGAna.TextMatrix(LastRowA, lColFgAnaCodigo) <> "" Then
                        If iRec > -1 Then
                            For i = 1 To RegistosRM.Count - 1
                                If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                    RegistosRM(i).ReciboCodMedico = CodigoMarcado
                                    FGAna.TextMatrix(LastRowA, lColFgAnaMedico) = CodigoMarcado
                                End If
                            Next
                        End If
                    End If
                ElseIf gPassaRecFactus = mediSim Then
                    If fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).flg_estado_doe <> 3 And _
                    fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).flg_estado_tx <> 3 Then
                        fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).cod_medico = CodigoMarcado
                        FGAna.TextMatrix(LastRowA, lColFgAnaMedico) = CodigoMarcado
                    End If
                End If
            Case lColFgAnaPreco ' TAXA
                If gPassaRecFactus <> mediSim Then
                    iRec = DevIndice(RegistosA(LastRowA).codAna)
                    aux = Replace(Trim(CodigoMarcado), ",", ".")
                    If (IsNumeric(aux)) Then
                        If (CLng(aux) = 0) Then
                            CodigoMarcado = "0"
                        End If
                    End If
                    CodigoMarcado = Replace(CodigoMarcado, ".", ",")
                    If FGAna.TextMatrix(LastRowA, lColFgAnaCodigo) <> "" Then
                        If iRec > -1 Then
                            For i = 1 To RegistosRM.Count - 1
                                If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                    If EliminaReciboManual(i, RegistosRM(i).ReciboCodFacturavel, False) = True Then
                                        If (RegistosRM(i).ReciboIsencao = gTipoIsencaoIsento Or RegistosRM(i).ReciboIsencao = gTipoIsencaoBorla) And CodigoMarcado > 0 Then
                                            RegistosRM(i).ReciboIsencao = gTipoIsencaoNaoIsento
                                            BG_Mensagem mediMsgBox, "An�lise passou a N�O ISENTA", vbInformation, ""
                                        End If
                                        RegistosRM(i).ReciboTaxa = CodigoMarcado
                                        AdicionaAnaliseAoReciboManual i, False
                                        FGAna.TextMatrix(LastRowA, lColFgAnaPreco) = EcAuxAna
                                    End If
                                End If
                            Next
                        End If
                    End If
                End If
             'NELSONPSILVA GLINTT-HS-21312
              Case lColFgAnaNAmostras 'QTD
                If gPassaRecFactus <> mediSim Then
                    iRec = DevIndice(RegistosA(LastRowA).codAna)
                    If Not (IsNumeric(EcAuxAna)) Then
                       Exit Sub
                    End If
                    If iRec > -1 Then
                        For i = 1 To RegistosRM.Count - 1
                            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                                If EliminaReciboManual(i, RegistosRM(i).ReciboCodFacturavel, False) = True Then
                                    RegistosRM(i).ReciboQuantidade = CodigoMarcado
                                    'RegistosRM(i).ReciboQuantidadeEFR = CodigoMarcado
                                    AdicionaAnaliseAoReciboManual i, False
                                    FGAna.TextMatrix(LastRowA, lColFgAnaNAmostras) = EcAuxAna
                                End If
                            End If
                        Next
                    End If
                  End If
                If gPassaRecFactus = mediSim Then
                        If fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).flg_estado_doe <> 3 And _
                        fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).flg_estado_tx <> 3 Then
                            fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).qtd = CodigoMarcado
                            FGAna.TextMatrix(LastRowA, lColFgAnaNAmostras) = CodigoMarcado
                            'iRec = AnalisesFact(LastRowA).iAnaFact
                            AtualizaQtdRecibos
                        End If
                End If
                '
        End Select
        CodigoMarcado = ""
        EcAuxAna = ""
        EcAuxAna.Visible = False
        If FGAna.row <= FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
            FGAna.row = FGAna.row + 1
            'FgAna.Col = 1
        Else
            Fgana_RowColChange
        End If
        If FGAna.Enabled = True Then
            FGAna.SetFocus
        End If
        
    End If
Exit Sub
TrataErro:
        'edgar.parada Glintt-HS-19165 * tratar exception
    If Err.Number = 9 Then
      Resume Next
    End If
    '
    BG_LogFile_Erros "EcAuxAna_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxAna_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxProd_GotFocus()
     On Error GoTo TrataErro
   
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i
    
    If LastColP = 5 Then
        EcAuxProd.Tag = adDate
    Else
        EcAuxProd.Tag = ""
    End If
    
    If EcAuxProd.SelLength = 0 Then
        Sendkeys ("{END}")
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxProd_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxProd_GotFocus"
    Exit Sub
    Resume Next
End Sub





Private Sub EcAuxQuestao_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        EstrutQuestao(FgQuestoes.row).Resposta = EcAuxQuestao
        FgQuestoes.TextMatrix(FgQuestoes.row, 1) = EstrutQuestao(FgQuestoes.row).Resposta
        FgQuestoes.SetFocus
    End If
End Sub

Private Sub EcAuxQuestao_LostFocus()
    EcAuxQuestao.Visible = False
    EcAuxQuestao = ""
End Sub

Private Sub EcAuxRec_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim iResp As Integer
    Dim iAnaFact As Integer
    On Error GoTo TrataErro
    If KeyCode = vbKeyReturn Then
        If FGRecibos.Col = lColRecibosPercentagem Then
            If gPassaRecFactus <> mediSim Then
                RegistosR(FGRecibos.row).Desconto = BL_HandleNull(CDbl(100 - CDbl(EcAuxRec)), 0)
                RegistosR(FGRecibos.row).ValorPagar = Round((RegistosR(FGRecibos.row).ValorOriginal * CDbl(EcAuxRec)) / 100, 2)
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosPercentagem) = EcAuxRec
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosValorPagar) = RegistosR(FGRecibos.row).ValorPagar
                EcAuxRec.Visible = False
                EcAuxRec = ""
                If RegistosR(FGRecibos.row).ValorPagar > 0 Then
                
                    RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido
                    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "N�o Emitido"
                ElseIf RegistosR(FGRecibos.row).ValorPagar = 0 Then
                    RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo
                    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "Recibo Nulo"
                End If
                If RegistosR(FGRecibos.row).ValorPagar > 0 Then
                    RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido
                Else
                    RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo
                End If
            Else
                For iResp = 1 To fa_movi_resp_tot
                    If RecibosNew(FGRecibos.row).cod_efr = fa_movi_resp(iResp).cod_efr Then
                        For iAnaFact = 1 To fa_movi_resp(iResp).totalAna
                            If (fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe = 1 Or fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe = mediComboValorNull) Or _
                               (fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx = 1 Or fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx = mediComboValorNull) Then
                                 If RecibosNew(FGRecibos.row).n_Fac_doe = fa_movi_resp(iResp).analises(iAnaFact).n_Fac_doe And RecibosNew(FGRecibos.row).serie_fac_doe = fa_movi_resp(iResp).analises(iAnaFact).serie_fac_doe Then
                                    FACTUS_AlteraPercentagemUtente iResp, EcAuxRec.text
                                    Exit For
                                 End If
                            End If
                        Next iAnaFact
                    End If
                Next iResp
            End If
            AtualizaFgRecibosNew
            EcAuxRec.Visible = False
            FGRecibos.SetFocus
        ElseIf FGRecibos.Col = lColRecibosValorPagar Then
            If IsNumeric(EcAuxRec) Then
                If gPassaRecFactus <> mediSim Then
                    RegistosR(FGRecibos.row).Desconto = CDbl(100 - BL_HandleNull(Replace(EcAuxRec, ".", ",") * 100 / (RegistosR(FGRecibos.row).ValorOriginal), 3))
                    RegistosR(FGRecibos.row).ValorPagar = Replace(EcAuxRec, ".", ",")
                    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosPercentagem) = Round(CDbl(100 - BL_HandleNull(CDbl(RegistosR(FGRecibos.row).Desconto), 0)), 3)
                    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosValorPagar) = RegistosR(FGRecibos.row).ValorPagar
                    EcAuxRec.Visible = False
                    EcAuxRec = ""
                    If RegistosR(FGRecibos.row).ValorPagar > 0 Then
                    
                        RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido
                        FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "N�o Emitido"
                    ElseIf RegistosR(FGRecibos.row).ValorPagar = 0 Then
                        RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo
                        FGRecibos.TextMatrix(FGRecibos.row, lColRecibosEstado) = "Recibo Nulo"
                    End If
                    If RegistosR(FGRecibos.row).ValorPagar > 0 Then
                        RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido
                    Else
                        RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo
                    End If
                Else
                    Dim Desc As Double
                    Desc = EcAuxRec * (100 - FGRecibos.TextMatrix(FGRecibos.row, lColRecibosPercentagem)) / FGRecibos.TextMatrix(FGRecibos.row, lColRecibosValorPagar)
                    'BRUNODSANTOS LJMANSO-302
                     'UALIA-874
                    'AuxRecVal = EcAuxRec.Text
                    For iResp = 1 To fa_movi_resp_tot
                        If RecibosNew(FGRecibos.row).cod_efr = fa_movi_resp(iResp).cod_efr Then
                            Exit For
                        End If
                    Next
                    fa_movi_resp(iResp).valor_marcado_utilizador = EcAuxRec.text
                    fa_movi_resp(iResp).flg_preco_alterado = True
                    colRec = FGRecibos.Col
                    '
                    EcAuxRec.text = Round(Desc, 2)
                    FGRecibos.Col = lColRecibosPercentagem
                    EcAuxRec_KeyDown vbKeyReturn, mediNao
                End If
            End If
        Else
            EcAuxRec.Visible = False
            EcAuxRec = ""
        End If
    End If
    FGRecibos_Click
Exit Sub
TrataErro:
    RegistosR(FGRecibos.row).Desconto = 0
    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosPercentagem) = "100"
    Exit Sub
    Resume Next
End Sub

'LJMANSO-302
Private Function PermiteAlterarPrecoEntidade() As Boolean
Dim rs As New adodb.recordset
Dim sql As String
PermiteAlterarPrecoEntidade = False

    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenStatic
    
    'LJMANSO -321 UALIA-859
    sql = "SELECT * FROM sl_efr_paramet WHERE cod_efr = " & BL_TrataStringParaBD(CStr(fa_movi_resp(FACTUS_DevolveLinhaResp(EcNumReq.text, CStr(RecibosNew(FGRecibos.row).cod_efr))).cod_efr)) & " AND perc_facturar = 1"
    
    rs.Open sql, gConexao
    
    If rs.RecordCount > 0 Then
        PermiteAlterarPrecoEntidade = True
    End If
    
    rs.Close
    Set rs = Nothing
    
End Function

Private Sub EcAuxRec_LostFocus()
    On Error GoTo TrataErro
    EcAuxRec.Visible = False
    EcAuxRec = ""

Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxRec_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxRec_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxTubo_GotFocus()
    On Error GoTo TrataErro
    
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i
    
    If LastColT = 2 Then
        EcAuxTubo.Tag = adDate
    Else
        EcAuxTubo.Tag = ""
    End If
    
    If EcAuxTubo.SelLength = 0 Then
        Sendkeys ("{END}")
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxTubo_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxTubo_GotFocus"
    Exit Sub
    Resume Next
End Sub

Public Sub EcAuxProd_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
   
    Enter = False
    
    If (KeyCode = 27 And Shift = 0) Or (KeyCode = 13 And Shift = 0 And Trim(EcAuxProd.text) = "") Then
        Enter = True
        EcAuxProd.Visible = False
        If FgProd.Enabled = True Then FgProd.SetFocus
    ElseIf (KeyCode = 13 And Shift = 0) Or (KeyCode = 9 And Shift = 0) Then
        Enter = True
        Select Case LastColP
            Case 0 ' Codigo do produto
                If Verifica_Prod_ja_Existe(LastRowP, EcAuxProd.text) = False Then
                    If SELECT_Descr_Prod = True Then
                        If Trim(EcDataPrevista.text) <> "" Then
                            FgProd.TextMatrix(LastRowP, 5) = Trim(EcDataPrevista.text)
                            RegistosP(LastRowP).DtPrev = Trim(EcDataPrevista.text)
                        End If
                        If Trim(EcDataChegada.text) <> "" Then
                            FgProd.TextMatrix(LastRowP, 6) = Trim(EcDataChegada.text)
                            RegistosP(LastRowP).DtChega = Trim(EcDataChegada.text)
                        End If

                        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
                        FgProd.TextMatrix(LastRowP, 7) = RegistosP(LastRowP).EstadoProd
                        
                        FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                        RegistosP(LastRowP).CodProd = UCase(EcAuxProd.text)
                        EcAuxProd.Visible = False
                        If FgProd.row = FgProd.rows - 1 Then
                            'Cria linha vazia
                            FgProd.AddItem ""
                            CriaNovaClasse RegistosP, FgProd.row + 1, "PROD"
                            FgProd.row = FgProd.row + 1
                            FgProd.Col = 0
                        Else
                            FgProd.Col = 2
                        End If
                    End If
                Else
                    Beep
                    BG_Mensagem mediMsgStatus, "Produto j� indicado!"
                End If
            Case 2 ' Codigo da especificacao
                If SELECT_Especif = True Then
                    FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                    RegistosP(LastRowP).CodEspecif = UCase(EcAuxProd.text)
                    EcAuxProd.Visible = False
                    FgProd.Col = 5
                End If
            Case 4 ' volume
                If Trim(EcAuxProd.text) <> "" Then
                    FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                    RegistosP(LastRowP).Volume = UCase(EcAuxProd.text)
                    EcAuxProd.Visible = False
                    FgProd.Col = 0
                End If
            Case 5 ' Data Prevista
                If Trim(EcAuxProd.text) <> "" Then
                    If BG_ValidaTipoCampo_ADO(Me, EcAuxProd) = True Then
                        FgProd.TextMatrix(LastRowP, LastColP) = UCase(EcAuxProd.text)
                        RegistosP(LastRowP).DtPrev = UCase(EcAuxProd.text)
                        EcAuxProd.Visible = False
                        RegistosP(LastRowP).EstadoProd = Coloca_Estado(RegistosP(LastRowP).DtPrev, RegistosP(LastRowP).DtChega)
                        FgProd.TextMatrix(LastRowP, 7) = RegistosP(LastRowP).EstadoProd
                        FgProd.Col = 0
                    Else
                        Beep
                        BG_Mensagem mediMsgStatus, "Data inv�lida!"
                    End If
                    
                End If
        End Select
        If FgProd.Enabled = True Then FgProd.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxProd_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxProd_KeyDown"
    Exit Sub
    Resume Next
End Sub


Private Sub EcAuxProd_KeyPress(KeyAscii As Integer)
    On Error GoTo TrataErro
    
    If Enter = True Then KeyAscii = 0
    Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxProd_KeyPress: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxProd_KeyPress"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxTubo_KeyPress(KeyAscii As Integer)
    On Error GoTo TrataErro
    
    If Enter = True Then KeyAscii = 0
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxTubo_KeyPress: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxTubo_KeyPress"
    Exit Sub
    Resume Next
End Sub

Private Sub EcAuxProd_LostFocus()
    On Error GoTo TrataErro
   
    Dim i As Integer
    
    On Local Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = True
    Next i
    
    
    EcAuxProd.Visible = False
    FgProd.SetFocus
    Exit Sub
TrataErro:
    BG_LogFile_Erros "EcAuxProd_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcAuxProd_LostFocus"
    Exit Sub
    Resume Next
End Sub


Private Sub EcCodAnaAcresc_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim ssql As String
    Dim rsAnalises As New adodb.recordset
    Dim taxa As String
    Dim mens As String
    Dim i As Long
    On Error GoTo TrataErro
    
    If KeyCode = 13 Then
        If UCase(Mid(EcCodAnaAcresc, 1, 1)) <> "S" And UCase(Mid(EcCodAnaAcresc, 1, 1)) <> "C" And UCase(Mid(EcCodAnaAcresc, 1, 1)) <> "P" Then
            If gSGBD = gSqlServer Then
                ssql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE substring(cod_ana,2,10) = " & BL_TrataStringParaBD(UCase(EcCodAnaAcresc.text))
            ElseIf gSGBD = gOracle Then
                ssql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE upper(substr(cod_ana,2,10))= " & BL_TrataStringParaBD(UCase(EcCodAnaAcresc.text))
            End If
            rsAnalises.CursorLocation = adUseServer
            rsAnalises.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros ssql
            rsAnalises.Open ssql, gConexao
            
            If rsAnalises.RecordCount > 0 Then
                EcCodAnaAcresc = rsAnalises!cod_ana
                EcDescrAnaAcresc = rsAnalises!descr_ana
                EcQtdAnaAcresc = "1"
                rsAnalises.Close
            Else
                EcCodAnaAcresc = ""
                EcDescrAnaAcresc = ""
                EcQtdAnaAcresc = ""
                rsAnalises.Close
                Exit Sub
            End If
        Else
            If gSGBD = gSqlServer Then
                ssql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE upper(cod_ana) = " & BL_TrataStringParaBD(UCase(EcCodAnaAcresc.text))
            ElseIf gSGBD = gOracle Then
                ssql = "SELECT cod_ana, descr_ana FROM slv_analises_factus WHERE upper(cod_ana)= " & BL_TrataStringParaBD(UCase(EcCodAnaAcresc.text))
            End If
            rsAnalises.CursorLocation = adUseServer
            rsAnalises.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros ssql
            rsAnalises.Open ssql, gConexao
            
            If rsAnalises.RecordCount > 0 Then
                EcCodAnaAcresc = rsAnalises!cod_ana
                EcDescrAnaAcresc = rsAnalises!descr_ana
                EcQtdAnaAcresc = "1"
                EcTaxaAnaAcresc = RetornaPrecoAnaRec(EcCodAnaAcresc, RegistosR(FGRecibos.row).codEntidade, CInt(EcQtdAnaAcresc))
                rsAnalises.Close
            Else
                EcCodAnaAcresc = ""
                EcDescrAnaAcresc = ""
                EcQtdAnaAcresc = ""
                rsAnalises.Close
            End If
        End If
        EcQtdAnaAcresc.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodAnaAcresc_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodAnaAcresc_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub EcCodAnaAcresc_LostFocus()
    EcCodAnaAcresc_KeyDown 13, 0
End Sub

Private Sub EcCodEFR_LostFocus()
    Dim iRec As Long
    On Error GoTo TrataErro
    Dim i As Integer
    Dim j As Integer
    Dim hemodialise As Integer
    Dim convencao As Integer
    Dim cod_dom As Integer
    Dim cod_medico As String
    If EcCodEFR <> "" And CbTipoIsencao.ListIndex > mediComboValorNull Then
        If CbTipoIsencao.ItemData(CbTipoIsencao.ListIndex) = gTipoIsencaoIsento Then
            CbHemodialise.ListIndex = mediComboValorNull
            CbHemodialise_Click
            CbHemodialise.Enabled = False
            If VerificaEfrHemodialise(EcCodEFR) = True Then
                CbHemodialise.Enabled = True
            End If
        Else
            CbHemodialise.ListIndex = mediComboValorNull
            CbHemodialise_Click
            CbHemodialise.Enabled = False
        End If
    Else
        CbHemodialise.ListIndex = mediComboValorNull
        CbHemodialise_Click
        CbHemodialise.Enabled = False
    End If
    
    If EcCodEFR <> "" And EcCodEFR <> CodEfrAUX And RegistosA.Count > 1 Then
        'CodEfrAUX = EcCodEFR
        gMsgTitulo = "Entidade"
        gMsgMsg = "Deseja alterar todas as an�lises para " & EcDescrEFR & "?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            If gPassaRecFactus <> mediSim Then
                For i = 1 To RegistosRM.Count - 1
                    If RegistosRM(i).ReciboNumDoc = "0" Or RegistosRM(i).ReciboNumDoc = "" Then
                        AlteraEntidadeReciboManual CLng(i), EcCodEFR, EcDescrEFR, "", -1
                        VerificaEFRCodigoFilho RegistosRM(i).ReciboCodFacturavel, EcCodEFR, EcDescrEFR, CLng(i)
                    Else
                        'BG_Mensagem mediMsgBox, "N�o � possivel alterar a entidade das an�lises associadas ao recibo emitido.", vbInformation, "Altera��o entidade"
                    End If
                Next
                For i = 1 To RegistosA.Count - 1
                    iRec = DevIndice(RegistosA(i).codAna)
                    If iRec = -1 Then
                        AdicionaReciboManual 0, EcCodEFR, "0", "0", RegistosA(i).codAna, 1, "", RegistosA(i).codAna, False
                    End If
                Next
                If EcNumReq <> "" And EcUtilizadorCriacao <> "" Then
                    FuncaoModificar (True)
                End If
                ActualizaLinhasRecibos -1
            Else

                If CbHemodialise.ListIndex > mediComboValorNull Then
                    hemodialise = CbHemodialise.ItemData(CbHemodialise.ListIndex)
                End If
                If CbConvencao.ListIndex > mediComboValorNull Then
                    convencao = CbConvencao.ItemData(CbConvencao.ListIndex)
                Else
                    convencao = mediComboValorNull
                End If
                If CbCodUrbano.ListIndex > mediComboValorNull Then
                    cod_dom = CbCodUrbano.ItemData(CbCodUrbano.ListIndex)
                Else
                    cod_dom = mediComboValorNull
                End If
                For i = 1 To FGAna.rows - 2
                    If AnalisesFact(i).iResp <= 0 Or AnalisesFact(i).iAnaFact <= 0 Or fa_movi_resp_tot <= 0 Then
                        If FACTUS_AdicionaAnalise(EcCodEFR.text, BL_HandleNull(UCase(RegistosA(i).codAna), ""), _
                                                 BL_HandleNull(EcDataPrevista.text, Bg_DaData_ADO), 0, mediComboValorNull, BG_DaComboSel(CbTipoIsencao), _
                                                 CLng(i), EcNumBenef.text, CbTipoUtente, EcUtente, EcNumReq.text, "", convencao, _
                                                 EcCodMedico.text, cod_dom, BL_HandleNull(EcKm.text, mediComboValorNull), _
                                                  EcCodPostalAux.text, EcDescrPostal.text, EcCodProveniencia.text, mediComboValorNull) = True Then
                        PreencheDadosNovoRecibo CLng(i)
                        AtualizaFgRecibosNew
                        
                        Else
                            'edgar.parada Glintt-HS-19577 19.08.2019
                             EcCodEFR = CodEfrAUX
                             EcDescrEFR = efrDescrAUX
                             '
                        End If
                    Else
                        'Glintt-HS-19577 09.04.2019 fa_movi_resp(AnalisesFact(i).iResp).analises(AnalisesFact(i).iAnaFact).flg_estado <> 3
                        If fa_movi_resp(AnalisesFact(i).iResp).analises(AnalisesFact(i).iAnaFact).flg_estado_doe <> 3 And fa_movi_resp(AnalisesFact(i).iResp).analises(AnalisesFact(i).iAnaFact).flg_estado_tx <> 3 And _
                            fa_movi_resp(AnalisesFact(i).iResp).analises(AnalisesFact(i).iAnaFact).flg_estado <> 3 Then
                            If AnalisesFact(i).iAnaFact > 0 Then
                                cod_medico = fa_movi_resp(AnalisesFact(i).iResp).analises(AnalisesFact(i).iAnaFact).cod_medico
                            Else
                                cod_medico = EcCodMedico.text
                            End If
                            If FACTUS_AlteraEntidade(EcCodEFR.text, AnalisesFact(i).iResp, AnalisesFact(i).iAnaFact, _
                                                  RegistosA(i).codAna, hemodialise, EcDataChegada.text, BG_DaComboSel(CbTipoIsencao), i, _
                                                   convencao, cod_medico, cod_dom, BL_HandleNull(EcKm.text, mediComboValorNull), _
                                                    EcCodPostalAux.text, EcDescrPostal.text, EcNumBenef.text, CbTipoUtente, _
                                                    EcUtente.text, EcNumReq.text, EcCodProveniencia.text, mediComboValorNull) = True Then
                                For j = 1 To FGAna.rows - 2
                                    AtualisaDadosMoviFact j
                                Next j
                                AtualisaDadosMoviFact CLng(i), RegistosA(i).codAna
                            Else
                               If EcCodEFR = fa_movi_resp(AnalisesFact(i).iResp).cod_efr Then
                                Exit For
                               End If
                               'edgar.parada Glintt-HS-19577 19.08.2019
                               EcCodEFR = CodEfrAUX
                               EcDescrEFR = efrDescrAUX
                               '
                            End If
                        Else
                        
                            BG_Mensagem mediMsgBox, "An�lise j� faturada. Entidade n�o pode ser alterada!", vbCritical + vbOKOnly, App.ProductName
                            'edgar.parada Glintt-HS-19577 19.08.2019
                            EcCodEFR = CodEfrAUX
                            EcDescrEFR = efrDescrAUX
                            '
                        End If
                    End If
                Next i
                AtualizaFgRecibosNew
                CodEfrAUX = EcCodEFR
                'edgar.parada Glintt-HS-19577 19.08.2019
                efrDescrAUX = EcDescrEFR
                '
            End If
        'Else
          'edgar.parada Glintt-HS-19577 19.08.2019
          'EcCodEFR = CodEfrAUX
          'EcDescrEFR = efrDescrAUX
           '
      End If
    Else
        'edgar.parada Glintt-HS-19577 19.08.2019
        CodEfrAUX = EcCodEFR
        efrDescrAUX = EcDescrEFR
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodEFR_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodEFR_LostFocus"
    Exit Sub
    Resume Next
End Sub

Public Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, CStr(gCodLocal))
    EcCodEFR_LostFocus
End Sub


Private Sub EcCodMedico_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    
    Dim Tabela As adodb.recordset
    Dim sql As String
    'RGONCALVES 15.07.2016
    EcCodMedico.text = UCase(EcCodMedico.text)
    '
    If EcCodMedico.text <> "" Then
        Set Tabela = New adodb.recordset
    
        sql = "SELECT nome_med FROM sl_medicos WHERE cod_med='" & Trim(EcCodMedico.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Tabela.Close
            sql = "SELECT cod_med, nome_med FROM sl_medicos WHERE cod_ordem ='" & Replace(UCase(Trim(EcCodMedico.text)), "M", "") & "'"
            Tabela.CursorType = adOpenStatic
            Tabela.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            Tabela.Open sql, gConexao
            If Tabela.RecordCount = 0 Then
            
                Cancel = True
                gMsgTitulo = "Pesquisa M�dico"
                gMsgMsg = " N�o existe m�dico com esse c�digo! " & vbCrLf & " Deseja criar? "
                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                If gMsgResp = vbYes Then
                    FormMedicos.Show
                Else
                    EcCodMedico.text = ""
                    EcNomeMedico.text = ""
                End If
            Else
                EcNomeMedico.text = Tabela!nome_med
                EcCodMedico.text = Tabela!cod_med
            End If
        Else
            EcNomeMedico.text = Tabela!nome_med
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcNomeMedico.text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodMedico_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodMedico_Validate"
    Exit Sub
    Resume Next
End Sub



Private Sub EcCodPostal_validate(Cancel As Boolean)
    If EcCodPostal = "" Then
        EcDescrPostal = ""
        EcRuaPostal = ""
        EcCodPostal = ""
        EcCodPostalAux = ""
    End If
End Sub

Private Sub EcDataAlteracao_Validate(Cancel As Boolean)
     On Error GoTo TrataErro
   
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataAlteracao)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataAlteracao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataAlteracao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataChegada_GotFocus()
    On Error GoTo TrataErro

    If Trim(EcDataChegada.text) = "" Then
        EcDataChegada.text = dataAct
    End If
    EcDataChegada.SelStart = 0
    EcDataChegada.SelLength = Len(EcDataChegada)
 Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataChegada_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataChegada_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataChegada_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    
    If EcDataChegada.text <> "" Then
        Cancel = Not ValidaDataChegada
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataChegada_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataChegada_Validate"
    Exit Sub
    Resume Next
End Sub


Private Sub EcDtPretend_LostFocus()
  'FrameDtPretend.Visible = False
End Sub

Private Sub EcDtPretend_Validate(Cancel As Boolean)
     On Error GoTo TrataErro
   
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtPretend)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDtPretend_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDtPretend_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataCriacao_Validate(Cancel As Boolean)
     On Error GoTo TrataErro
   
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataCriacao)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataCriacao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataCriacao_Validate"
    Exit Sub
    Resume Next
End Sub


Private Sub EcDataImpressao_Validate(Cancel As Boolean)
     On Error GoTo TrataErro
   
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataImpressao)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataImpressao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataImpressao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcDataImpressao2_Validate(Cancel As Boolean)
     On Error GoTo TrataErro
   
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataImpressao2)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataImpressao2_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataImpressao2_Validate"
    Exit Sub
    Resume Next
End Sub

Public Sub EcDataPrevista_GotFocus()
    On Error GoTo TrataErro
    Dim Campos(5) As String
    Dim retorno() As String
    Dim iret As Integer
    Dim i As Integer
        'edgar.parada Glintt-HS-19165 * flg controla estrutura
    Dim flgControlaEstrut As Boolean
    '
    'Para evitar que a nova entidade seja reposta pela codificada
    ' no utente
    If gPreencheEFRUtente = 1 Then
        If (Trim(EcCodEFR.text) = "") Then
            Call PreencheEFRUtente
        End If
    End If
    If EcMorada = "" And EcUtente <> "" Then
        Campos(0) = "descr_mor_ute"
        Campos(1) = "cod_postal_ute"
        Campos(2) = "n_benef_ute"
        Campos(3) = "cod_isencao_ute"
        Campos(4) = "cod_pais"
        Campos(5) = "num_doe_prof"
        iret = BL_DaDadosUtente(Campos, retorno, EcSeqUtente)
        
        If iret = -1 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar utente!", vbCritical + vbOKOnly, App.ProductName
            CbTipoUtente.ListIndex = mediComboValorNull
            EcUtente.text = ""
            Exit Sub
        End If
        
        EcMorada = retorno(0)
                'edgar.parada Glintt-HS-19165 * s� faz o if se n�o for requisi��o importada
        If UBound(estrutOperation) = 0 Then
            EcCodPostal = Mid(retorno(1), 1, 4)
            EcRuaPostal = Mid(retorno(1), 6)
            EcCodPostalAux = Mid(retorno(1), 1, 8)
            EcDescrPostal = BL_SeleccionaDescrPostal(EcCodPostalAux)
            EcCodPais = retorno(4)
            EcCodpais_Validate False
            EcDoenteProf = retorno(5)
        End If
        '
        'PreencheCodigoPostal
        
        If retorno(3) <> "" Then
            For i = 0 To CbTipoIsencao.ListCount - 1
                If CbTipoIsencao.ItemData(i) = retorno(3) Then
                    CbTipoIsencao.ListIndex = i
                    Exit For
                End If
            Next
        End If
                'edgar.parada Glintt-HS-19165 * s� faz o if se n�o for requisi��o importada
        flgControlaEstrut = True
        If UBound(estrutOperation) = 0 Then
             If CbUrgencia.ListCount > 0 Then
               CbUrgencia.ListIndex = 0
             End If
        End If
        'BRUNODSANTOS 08.09.2016 - SCMVC-931 -> And CbDestino.ListIndex < 0
        If CbDestino.ListCount > 0 And gLAB <> "CL" And CbDestino.ListIndex < 0 Then
            CbDestino.ListIndex = 0
        End If
                'edgar.parada LOTE 97 - Conven��o
        If UBound(estrutOperation) = 0 Then
           If CbConvencao.ListCount > 0 Then
             For i = 0 To CbConvencao.ListCount - 1
                If CbConvencao.ItemData(i) = gconvencaoNormal Then
                    CbConvencao.ListIndex = i
                    Exit For
                End If
            Next
                 End If
        End If
        'EcNumBenef = Retorno(2)
    End If
    
    Call ValidaDataPrevista
    
    
    EcDataPrevista.SelStart = 0
    EcDataPrevista.SelLength = Len(EcDataPrevista)
    EcDataChegada = EcDataPrevista
Exit Sub
TrataErro:
        'edgar.parada Glintt-HS-19165 * tratar exception
    If Err.Number = 9 And flgControlaEstrut = True Then
      flgControlaEstrut = False
      Resume Next
    End If
    '
    BG_LogFile_Erros "EcDataPrevista_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataPrevista_GotFocus"
    Exit Sub
    Resume Next
End Sub

Sub Preenche_Data_Prev_Prod()
    On Error GoTo TrataErro
    
    Dim i As Integer
    
    ExecutaCodigoP = False
    For i = 1 To RegistosP.Count - 1
        If RegistosP(i).CodProd <> "" Then
            If Trim(RegistosP(i).DtPrev) = "" Then
                RegistosP(i).DtPrev = EcDataPrevista.text
                FgProd.TextMatrix(i, 4) = EcDataPrevista.text
                RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
                FgProd.TextMatrix(i, 6) = RegistosP(i).EstadoProd
            End If
        If RegistosP(i).EstadoProd = "" Then
            RegistosP(i).EstadoProd = Coloca_Estado(RegistosP(i).DtPrev, RegistosP(i).DtChega)
            FgProd.TextMatrix(i, 6) = RegistosP(i).EstadoProd
        End If
        End If
    Next i
    
    ExecutaCodigoP = True
Exit Sub
TrataErro:
    BG_LogFile_Erros "Preenche_Data_Prev_Prod: " & Err.Number & " - " & Err.Description, Me.Name, "Preenche_Data_Prev_Prod"
    Exit Sub
    Resume Next
End Sub

Function ValidaDataChegada() As Boolean
    On Error GoTo TrataErro

    ValidaDataChegada = False
    If EcDataChegada.text <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcDataChegada) Then
'            If DateValue(EcDataChegada.Text) > DateValue(dataAct) Then
'                BG_Mensagem mediMsgBox, "A data de Chegada tem que ser inferior ou igual � data actual ", vbInformation, "Aten��o"
'                SendKeys ("{HOME}+{END}")
'                If EcDataChegada.Enabled = True Then
'                    EcDataChegada.SetFocus
'                End If
'            Else
'                ValidaDataChegada = True
'            End If
            ValidaDataChegada = True
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaDataChegada: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaDataChegada"
    ValidaDataChegada = False
    Exit Function
    Resume Next
End Function

Function ValidaDataPrevista() As Boolean
    On Error GoTo TrataErro

    ValidaDataPrevista = False
    If EcDataPrevista.text <> "" Then
        If BG_ValidaTipoCampo_ADO(Me, EcDataPrevista) And EcDataPrevista.Enabled = True Then
'            If DateValue(EcDataPrevista.Text) < DateValue(dataAct) Then
'                BG_Mensagem mediMsgBox, "A data prevista tem que ser superior ou igual � data actual ", vbInformation, "Aten��o"
'                If EcDataPrevista.Enabled = True Then
'                    EcDataPrevista.Text = ""
'                    EcDataPrevista.SetFocus
'                End If
'            Else
'                ValidaDataPrevista = True
'            End If
            ValidaDataPrevista = True
        End If
    ElseIf EcDataPrevista.text = "" Then
        If Flg_LimpaCampos = False Then
            If EcFimSemana <> "1" Then
                EcDataPrevista.text = Format(dataAct, gFormatoData)
                ValidaDataPrevista = True
            End If
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaDataPrevista: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaDataPrevista"
    ValidaDataPrevista = False
    Exit Function
    Resume Next
End Function

Public Sub EcDataPrevista_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    
    
    If Trim(EcDataPrevista.text) <> "" Then
        Cancel = Not ValidaDataPrevista
    End If
    If Cancel = True Then Exit Sub
    
    If EcDataPrevista.Enabled = True And Trim(EcSeqUtente.text) <> "" Then
        Preenche_Data_Prev_Prod
        SSTGestReq.TabEnabled(1) = True
        SSTGestReq.TabEnabled(2) = True
        SSTGestReq.TabEnabled(3) = True
        SSTGestReq.TabEnabled(4) = True
        SSTGestReq.TabEnabled(5) = True
        SSTGestReq.TabEnabled(6) = True
        FrProdutos.Enabled = True
        FrBtProd.Enabled = True
        FgProd.row = LastRowP
        FgProd.Col = LastColP
        ExecutaCodigoP = True
        FrBtTubos.Enabled = True
        FGTubos.row = LastRowT
        FGTubos.Col = LastColT
        ExecutaCodigoT = True
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcDataPrevista_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcDataPrevista_Validate"
    Exit Sub
    Resume Next
End Sub



Private Sub EcKm_Validate(Cancel As Boolean)
    If CbCodUrbano.ListIndex > mediComboValorNull And gPassaRecFactus = mediSim Then
        FACTUS_AtualizaDom CbCodUrbano.ItemData(CbCodUrbano.ListIndex), BL_HandleNull(EcKm.text, 0)
    Else
        FACTUS_AtualizaDom mediComboValorNull, mediComboValorNull
    End If
End Sub

Private Sub EcNome_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    Dim ssql As String
    Dim RsNome As New adodb.recordset
    Dim SeqUtente As String
    
    'soliveira LJM 06.11.2008
    If EcNumReq.text = "" And EcNome.text <> "" Then
        ssql = BL_Upper_Campo("SELECT seq_utente FROM sl_identif WHERE nome_ute = " & BL_TrataStringParaBD(EcNome), "nome_ute", gPesquisaDentroCampo)
        Set RsNome = BG_ExecutaSELECT(ssql)
        SeqUtente = ""
        If RsNome.RecordCount >= 1 Then
            SeqUtente = "("
            While Not RsNome.EOF
                SeqUtente = SeqUtente & RsNome!seq_utente & ", "
                RsNome.MoveNext
            Wend
            SeqUtente = Mid(SeqUtente, 1, Len(SeqUtente) - 2) & ")"
            
            EcSeqUtente.Tag = adVarChar
            EcSeqUtente.MaxLength = 100
            EcSeqUtente = SeqUtente
        'soliveira ualia
        Else
            EcSeqUtente = -1
        End If
        BG_DestroiRecordSet RsNome
    'soliveira ualia
    ElseIf EcNumReq.text = "" And EcNome.text = "" Then
        EcSeqUtente = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNome_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcNome_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumBenef_GotFocus()
    On Error GoTo TrataErro
    
    Dim Tabela As adodb.recordset
    Dim sql As String
    
    If EcCodEFR.text <> "" And Trim(EcNumBenef.text) = "" Then
        Set Tabela = New adodb.recordset
        Tabela.CursorLocation = adUseServer
        Tabela.CursorType = adOpenStatic
        sql = "SELECT sigla,formato1,formato2 FROM sl_efr WHERE cod_efr=" & EcCodEFR.text
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            Formato1 = Trim(BL_HandleNull(Tabela!Formato1, ""))
            Formato2 = Trim(BL_HandleNull(Tabela!Formato2, ""))
            EcNumBenef.text = Trim(BL_HandleNull(Tabela!sigla, ""))
            Sendkeys ("{END}")
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumBenef_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumBenef_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumBenef_LostFocus()
    On Error GoTo TrataErro
    EcNumBenef = UCase(EcNumBenef)
    If EcNumBenef = "" Then Exit Sub
    ' PARA ENTIDADE GNR FORMATA O NUMERO DO CARTAO
    
    If gCodEfrGNR = EcCodEFR And gTipoInstituicao = "PRIVADA" Then
        If Mid(EcNumBenef, 1, 1) = "G" Then
            EcNumBenef = Mid(EcNumBenef, 3, 10)
        Else
            EcNumBenef = Mid(EcNumBenef, 1, 10)
        End If
    ' PARA ENTIDADE PT FORMATA O NUMERO DO CARTAO
    ElseIf gCodEfrPT = EcCodEFR And gTipoInstituicao = "PRIVADA" Then
        If Len(Trim(EcNumBenef)) = 14 Then
            EcNumBenef.text = Mid(EcNumBenef, 3, 8) & "/" & Mid(EcNumBenef, 11, 2)
            While Mid(EcNumBenef.text, 1, 1) = "0"
                EcNumBenef.text = Mid(EcNumBenef.text, 2)
            Wend
        Else
            EcNumBenef.text = EcNumBenef
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumBenef_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumBenef_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumBenef_Validate(Cancel As Boolean)
     On Error GoTo TrataErro
   
    Dim Formato As Boolean
    
    Formato = Verifica_Formato(UCase(EcNumBenef.text), Formato1, Formato2)
    If Formato = False And EcNumBenef.text <> "" Then
        Cancel = True
        Sendkeys ("{END}")
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumBenef_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumBenef_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumReq_GotFocus()
    On Error GoTo TrataErro
    Set CampoActivo = Me.ActiveControl
    'RGONCALVES 16.07.2016
    'If gLAB <> "CL" Or gLAB <> "LRV" Then
    If gLAB <> "CL" And gLAB <> "LRV" Then
    '
        cmdOK.Default = True
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumReq_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumReq_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumReq_LostFocus()
    On Error GoTo TrataErro
    
    cmdOK.Default = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumReq_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumReq_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcNumReq_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    If gNReqPreImpressa = mediSim And gPreencheSalaDefeitoPreImpressa = mediSim Then
        PreencheSalaAuto EcNumReq
    End If
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcNumReq)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNumReq_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcNumReq_Validate"
    Exit Sub
    Resume Next
End Sub



Private Sub EcObsAna_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    If EcObsAna.text = "" Then
        Acumula = ""
        
    End If
    If KeyCode = 27 Or KeyCode = 13 Then
        EcObsAna_LostFocus
    ElseIf (KeyCode) = 220 Then
        Acumula = Acumula & "\"
    ElseIf KeyCode = 32 Then
        If Len(Acumula) > 2 Then
            If Mid(Acumula, 1, 2) = "\\" Then
                EcObsAna = (EcObsAna)
                EcObsAna.text = Replace(EcObsAna.text, (Acumula), BL_SelCodigo("SL_DICIONARIO", "DESCR_FRASE", "COD_FRASE", Mid(Trim(UCase(Acumula)), 3), "V"))
                EcObsAna.SetFocus
                Sendkeys ("{END}")
                Acumula = ""
                Exit Sub
            Else
                'EcObsAna = Replace(EcObsAna, acumula, "")
                Acumula = ""
                'EcObsAna.SetFocus
                'SendKeys ("{END}")
            End If
        Else
            If Acumula <> "" Then
                'EcObsAna = Replace(EcObsAna, acumula, "")
                Acumula = ""
                'EcObsAna.SetFocus
                'SendKeys ("{END}")
            End If
        End If
        Acumula = ""
    ElseIf KeyCode = vbKeyBack And Acumula <> "" Then
        Acumula = Mid(Acumula, 1, Len(Acumula) - 1)
    ElseIf ((KeyCode >= 65 And KeyCode <= 226) Or (KeyCode >= vbKey0 And KeyCode <= vbKey9)) And Shift = 0 Then
        Acumula = Acumula & LCase(Chr(KeyCode))
    ElseIf (KeyCode >= 65 And KeyCode <= 226) And Shift = 1 Then
        Acumula = Acumula & UCase(Chr(KeyCode))
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcObsAna_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "EcObsAna_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub EcObsAna_LostFocus()
    On Error GoTo TrataErro
    Acumula = ""
    If linhaAnaliseOBS = -1 Then
        linhaAnaliseOBS = LastRowA
    End If
    RegistosA(linhaAnaliseOBS).ObsAnaReq = Trim(EcObsAna.text)
        
'    If EcNumReq <> "" Then
'        Grava_ObsAnaReq (linhaAnaliseOBS)
'    End If
    If RegistosA(linhaAnaliseOBS).codAna <> "" And RegistosA(linhaAnaliseOBS).ObsAnaReq <> "" Then
        BL_GravaObsAnaReq BL_HandleNull((EcNumReq), 0), RegistosA(linhaAnaliseOBS).codAna, Trim(EcObsAna.text), _
                          CLng(RegistosA(linhaAnaliseOBS).seqObsAnaReq), 0, BL_HandleNull((EcSeqUtente), 0), mediComboValorNull
        PreencheNotas
    End If
    
    EcObsAna.text = ""
    FrameObsAna.Visible = False
    FGAna.SetFocus
    linhaAnaliseOBS = -1
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcObsAna_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcObsAna_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcPesqRapMedico_Change()
     On Error GoTo TrataErro
   Dim rsCodigo As adodb.recordset
    Dim sql As String
    
    If EcPesqRapMedico.text <> "" Then
        Set rsCodigo = New adodb.recordset

        sql = "SELECT cod_med,nome_med FROM sl_medicos WHERE seq_med = '" & EcPesqRapMedico & "'"
        
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do m�dico!", vbExclamation, "Pesquisa r�pida"
            EcPesqRapMedico.text = ""
        Else
            EcCodMedico.text = Trim(rsCodigo!cod_med)
            EcNomeMedico.text = Trim(rsCodigo!nome_med)
        End If
        
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "EcPesqRapMedico_Change: " & Err.Number & " - " & Err.Description, Me.Name, "EcPesqRapMedico_Change"
    Exit Sub
    Resume Next
End Sub

Private Sub EcPesqRapProduto_Change()
    On Error GoTo TrataErro
    
    Dim rsCodigo As adodb.recordset
    Dim indice As Integer
    
    If EcPesqRapProduto.text <> "" Then
        Set rsCodigo = New adodb.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        rsCodigo.Open "SELECT cod_produto FROM sl_produto WHERE seq_produto = " & EcPesqRapProduto, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do produto!", vbExclamation, "Pesquisa r�pida"
        Else
            EcAuxProd.text = BL_HandleNull(rsCodigo!cod_produto, "")
            EcAuxProd.left = FgProd.CellLeft + 270
            EcAuxProd.top = FgProd.CellTop + 2060
            EcAuxProd.Width = FgProd.CellWidth + 20
            EcAuxProd.Visible = True
            EcAuxProd.Enabled = True
            EcAuxProd.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcPesqRapProduto_Change: " & Err.Number & " - " & Err.Description, Me.Name, "EcPesqRapProduto_Change"
    Exit Sub
    Resume Next
End Sub



Private Sub EcPesqRapTubo_Change()
    On Error GoTo TrataErro
    
    Dim rsCodigo As adodb.recordset
    Dim indice As Integer
    
    If EcPesqRapTubo.text <> "" Then
        Set rsCodigo = New adodb.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT cod_tubo FROM sl_tubo WHERE seq_tubo = " & EcPesqRapTubo, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do tubo!", vbExclamation, "Pesquisa r�pida"
        Else
            EcAuxTubo.text = BL_HandleNull(rsCodigo!cod_tubo, "")
            EcAuxTubo.left = FGTubos.CellLeft + 270
            EcAuxTubo.top = FGTubos.CellTop + 2060
            EcAuxTubo.Width = FGTubos.CellWidth + 20
            EcAuxTubo.Visible = True
            EcAuxTubo.Enabled = True
            EcAuxTubo.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcPesqRapTubo_Change: " & Err.Number & " - " & Err.Description, Me.Name, "EcPesqRapTubo_Change"
    Exit Sub
    Resume Next
End Sub



Private Sub EcQtdAnaAcresc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        If IsNumeric(EcQtdAnaAcresc) = False Then
            EcQtdAnaAcresc = "1"
        End If
        EcTaxaAnaAcresc = RetornaPrecoAnaRec(EcCodAnaAcresc, RegistosR(FGRecibos.row).codEntidade, CInt(EcQtdAnaAcresc))
        If BtAnaAcresc.Enabled = True Then
            btAnaAcresc_Click
        End If
    End If
End Sub

Private Sub EcQtdAnaAcresc_LostFocus()
    EcQtdAnaAcresc_KeyDown 13, 0
End Sub

Public Sub EcUtente_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    
    Dim sql As String
    Dim Campos(1) As String
    Dim retorno() As String
    Dim iret As Integer
    
    'Limpa a estrutura de dados do utente
    If CbTipoUtente.ListIndex <> -1 And EcUtente.text <> "" Then
        'Procurar o sequencial e chamar a fun��o que preenche a identifica��o do utente
        Campos(0) = "seq_utente"
        
        iret = BL_DaDadosUtente(Campos, retorno, , CbTipoUtente.text, EcUtente.text)
        
        If iret = -1 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Utente inexistente!", vbInformation + vbOKOnly, "Pesquisa ao utente!"
            EcNome.text = ""
            EcDataNasc.text = ""
            EcSexo = ""
            EcIdade.text = ""
        Else
            BtInfClinica.Enabled = True
            BtDomicilio.Enabled = True

            BtConsReq.Enabled = True
            
            EcSeqUtente.text = retorno(0)
            BL_PreencheDadosUtente EcSeqUtente
            
            PreencheDadosUtente
            
        End If
    Else
        EcNome.text = ""
        EcDataNasc.text = ""
        EcSexo = ""
        EcIdade.text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtente_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtente_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorAlteracao_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcUtilizadorAlteracao)
    If Cancel = False Then
        LbNomeUtilAlteracao = BL_SelNomeUtil(EcUtilizadorAlteracao.text)
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorAlteracao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorAlteracao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorCriacao_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcUtilizadorCriacao)
    If Cancel = False Then
        LbNomeUtilCriacao = BL_SelNomeUtil(EcUtilizadorCriacao.text)
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorCriacao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorCriacao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorImpressao_GotFocus()
    On Error GoTo TrataErro
    
    Set CampoActivo = Me.ActiveControl
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao_GotFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorImpressao_LostFocus()
    On Error GoTo TrataErro
    
    LbNomeUtilImpressao = BL_SelNomeUtil(EcUtilizadorImpressao.text)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorImpressao_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub EcUtilizadorImpressao2_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    
    LbNomeUtilImpressao2 = BL_SelNomeUtil(EcUtilizadorImpressao2.text)
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao2_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao2_Validate"
    Exit Sub
    Resume Next
End Sub

'BRUNODSANTOS GLINTT-HS-15750
Private Sub FgAna_Click()
    'edgar.parada Glintt-HS-19165 *
    Dim credImportada As String
    
    On Error GoTo TrataErro
    
    If gUsaEnvioPDS <> mediSim Then Exit Sub
    'edgar.parada Glintt-HS-19165 * Se credencial � ESP bloquear celulas da grid
    credImportada = FGAna.TextMatrix(FGAna.row, lColFgAnaP1)
    
    If CredencialESP(credImportada) Then
       LastRowA = FGAna.row
       Exit Sub
    End If
    '
    If (FGAna.rows <= 2) Or (FGAna.TextMatrix(FGAna.RowSel, lColFgAnaCodigo) = "") Or (FGAna.TextMatrix(FGAna.RowSel, lColFgAnaConsenteEnvioPDS) = "") Then
        Exit Sub
    End If

    With FGAna
       If .MouseCol = lColFgAnaConsenteEnvioPDS Then
            If .TextMatrix(.row, lColFgAnaConsenteEnvioPDS) = EstadoCheckboxEnvioPDS_Unchecked Then
                Call ChangeCheckboxState(.RowSel, lColFgAnaConsenteEnvioPDS, EstadoCheckboxEnvioPDS_Checked)
                Call CheckAnalisesMesmaCredencial(EstadoCheckboxEnvioPDS_Checked)
            ElseIf .TextMatrix(.row, lColFgAnaConsenteEnvioPDS) = EstadoCheckboxEnvioPDS_Checked Then
                Call ChangeCheckboxState(.RowSel, lColFgAnaConsenteEnvioPDS, EstadoCheckboxEnvioPDS_Unchecked)
                Call CheckAnalisesMesmaCredencial(EstadoCheckboxEnvioPDS_Unchecked)
            End If
        End If
    End With
    Exit Sub
TrataErro:
    BG_LogFile_Erros "EcUtilizadorImpressao2_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcUtilizadorImpressao2_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub Fgana_DblClick()
    On Error GoTo TrataErro
    FgAna_KeyDown 13, 0
Exit Sub
TrataErro:
    BG_LogFile_Erros "Fgana_DblClick: " & Err.Number & " - " & Err.Description, Me.Name, "Fgana_DblClick"
    Exit Sub
    Resume Next
End Sub

'Private Sub FGAna_Click()
'
'    MsgBox Me.FGAna_Recibo.ColSel, vbInformation, "ZZ"
'
'End Sub

Private Sub FGAna_GotFocus()
    On Error GoTo TrataErro
    
    If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo)) <> "" Then
        FGAna.CellBackColor = vbRed
        FGAna.CellForeColor = vbWhite
    Else
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbBlack
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGAna_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FGAna_GotFocus"
    Exit Sub
    Resume Next
End Sub

Public Sub FgAna_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    Dim ana As String
    Dim taxa As String
    Dim mens As String
    Dim codAnaEfr As String
    Dim flgPermite As Boolean
    Dim i As Long
    Dim k As Long
    Dim iRec As Long
    Dim hemodialise As Integer
    Dim convencao As Integer
    Dim iResp As Integer
    Dim iAna As Integer
    Dim cod_medico As String
    Dim cod_dom As Integer
    'edgar.parada Glintt-HS-19165
    Dim credImportada As String
    Dim colunaSel As Integer
    '
    flgPermite = False
    'edgar.parada Glintt-HS-19165 * Se credencial � ESP bloqueia grelha excepto a coluna EFR
    credImportada = FGAna.TextMatrix(FGAna.row, lColFgAnaP1)
    
    colunaSel = FGAna.ColSel
    'NELSONPSILVA 30.04.2019 Glintt-HS-21232 Adicionado -> KeyCode <> 46
    If CredencialESP(credImportada) And colunaSel <> lColFgAnaDescrEFR And KeyCode <> 46 Then
       Exit Sub
    End If
    '
    If LastRowA > FGAna.rows - 1 Then
        LastRowA = FGAna.rows - 1
    End If
    If gPassaRecFactus <> mediSim Then
        If FGAna.row < RegistosA.Count Then
            iRec = DevIndice(RegistosA(FGAna.row).codAna)
        Else
            iRec = -1
        End If
    Else
        iRec = 1
    End If
    Select Case KeyCode
        Case 96
            KeyCode = 48
        Case 97
            KeyCode = 49
        Case 98
            KeyCode = 50
        Case 99
            KeyCode = 51
        Case 100
            KeyCode = 52
        Case 101
            KeyCode = 53
        Case 102
            KeyCode = 54
        Case 103
            KeyCode = 55
        Case 104
            KeyCode = 56
        Case 105
            KeyCode = 57
    End Select
    
    ' --------------------------------------------------------------------------------------------
    ' TECLAS DE ATALHO PARA MUDAR DADOS DAS ANALISES
    ' --------------------------------------------------------------------------------------------
    If KeyCode = 113 Then                   'F2 - Muda credencial
        FGAna.Col = lColFgAnaP1
        KeyCode = 13
        flgPermite = True
    ElseIf KeyCode = 114 Then               'F3 - Muda M�dico
        FGAna.Col = lColFgAnaMedico
        KeyCode = 13
        flgPermite = True
    ElseIf KeyCode = 115 Then               'F4 - Muda entidade
        FGAna.Col = lColFgAnaDescrEFR
        KeyCode = 13
        flgPermite = True
    ElseIf KeyCode = 116 And gPermAltPreco = 1 Then               'F5 - Muda taxa
        FGAna.Col = lColFgAnaPreco
        KeyCode = 13
        flgPermite = True
    ElseIf KeyCode = 119 And iRec > mediComboValorNull And gPassaRecFactus <> mediSim Then                'F8 - ISENTO
            If RegistosRM(iRec).ReciboEntidade = gEfrParticular Then
                AlteraIsencao gTipoIsencaoNaoIsento, iRec, FGAna.row
            Else
                AlteraIsencao (gTipoIsencaoIsento), iRec, FGAna.row
            End If
            ActualizaLinhasRecibos FGAna.row
        
        Exit Sub
        flgPermite = True
    ElseIf KeyCode = 119 And gPassaRecFactus = mediSim Then
        AlteraIsencao2 FGAna.row, gTipoIsencaoIsento
    ElseIf KeyCode = 120 And iRec > mediComboValorNull And gPassaRecFactus <> mediSim Then               'F9 - NAO ISENTO
        AlteraIsencao (gTipoIsencaoNaoIsento), iRec, FGAna.row
        ActualizaLinhasRecibos FGAna.row
        flgPermite = True
       Exit Sub
    ElseIf KeyCode = 120 And gPassaRecFactus = mediSim Then
        AlteraIsencao2 FGAna.row, gTipoIsencaoNaoIsento
    ElseIf KeyCode = 121 And iRec > mediComboValorNull And gPassaRecFactus <> mediSim Then                'F10 - BORLA
        AlteraIsencao (gTipoIsencaoBorla), iRec, FGAna.row
        ActualizaLinhasRecibos FGAna.row
        
        flgPermite = True
        Exit Sub
    
    'RGONCALVES 23.07.2013 LJMANSO-108
    'ElseIf KeyCode = 122 And iRec > mediComboValorNull Then
    ElseIf KeyCode = 122 Then               'F11 - Incrementa n� P1
    '
    Flg_IncrementaP1 = True
        Exit Sub
        flgPermite = True
    End If
    
    
    
    If (FGAna.Col = lColFgAnaCodigo Or (FGAna.Col = lColFgAnaP1 And KeyCode <> 13) Or FGAna.Col = lColFgAnaMedico Or (FGAna.Col = lColFgAnaPreco And gPermAltPreco = 1)) And (KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)) Then ' Enter = Editar
        If FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo) <> "" And FGAna.Col = lColFgAnaCodigo Then Exit Sub
        ' --------------------------------------------------------------------------------------------
        ' SE RECIBO EMITIDO NAO DEIXA ALTERAR VALOR
        ' --------------------------------------------------------------------------------------------
        If gPassaRecFactus <> mediSim Then
            If FGAna.row < RegistosA.Count And iRec > 0 Then
                If RegistosRM(iRec).ReciboNumDoc <> "0" And RegistosRM(iRec).ReciboNumDoc <> "" And FGAna.Col = lColFgAnaPreco Then
                    BG_Mensagem mediMsgBox, "J� foi emitido recibo.N�o � possivel alterar TAXA", vbOKOnly + vbInformation, "Taxas"
                    Exit Sub
                End If
            End If
        Else
            If FGAna.Col = lColFgAnaCodEFR Or FGAna.Col = lColFgAnaMedico Or FGAna.Col = lColFgAnaNrBenef Or FGAna.Col = lColFgAnaP1 Or FGAna.Col = lColFgAnaPreco Then
                If fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).flg_estado_doe = 3 Or fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).flg_estado_tx = 3 Then
                    BG_Mensagem mediMsgBox, "An�lise n�o pode ser alterada", vbOKOnly + vbInformation, "FACTUS"
                    Exit Sub
                End If
            End If
        End If
        
        If RegistosA(FGAna.row).Estado <> "-1" And RegistosA(FGAna.row).codAna <> "" And FGAna.Col = lColFgAnaCodigo Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser alterada: j� cont�m resultados!"
        Else
            If LastColA <> lColFgAnaCodigo And Trim(FGAna.TextMatrix(LastRowA, lColFgAnaCodigo)) = "" Then
                Beep
                BG_Mensagem mediMsgStatus, "Tem que indicar primeiro uma an�lise!"
            Else
                If (FGAna.Col = lColFgAnaPreco Or FGAna.Col = lColFgAnaMedico) And iRec = mediComboValorNull Then
                    Exit Sub
                End If
                If EcDataFecho.text <> "" Then
                    Beep
                    BG_Mensagem mediMsgStatus, "N�o pode alterar uma requisi��o fechada!"
                    Exit Sub
                End If
                If LastRowA > 0 And LastColA = lColFgAnaP1 And (KeyCode = 86 Or KeyCode = 66) And iRec > mediComboValorNull Then
                    AlteraCorP1
                    Exit Sub
                End If
                ExecutaCodigoA = False
                EcAuxAna.text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf(((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)), Chr(KeyCode), "")
                ExecutaCodigoA = True
                If KeyCode = 8 Then
                    If Len(EcAuxAna.text) > 1 Then EcAuxAna.text = Mid(EcAuxAna.text, 1, Len(EcAuxAna.text) - 1)
                End If
                If KeyCode = 13 Then
                    EcAuxAna.SelStart = 0
                    EcAuxAna.SelLength = Len(EcAuxAna)
                Else
                    EcAuxAna.SelStart = Len(EcAuxAna)
                    EcAuxAna.SelLength = 0
                End If

                EcAuxAna.left = FGAna.CellLeft + 360
                EcAuxAna.top = SSTGestReq.top + FGAna.CellTop + 460
                EcAuxAna.Width = FGAna.CellWidth + 20
                EcAuxAna.Visible = True
                EcAuxAna.Enabled = True
                EcAuxAna.SetFocus
                
            End If
        End If
       'NELSONPSILVA GLINTT-HS-21312
    ElseIf (FGAna.Col = lColFgAnaP1 And KeyCode = 13 And iRec > mediComboValorNull) Or (FGAna.Col = lColFgAnaNAmostras And KeyCode = 13 And iRec > mediComboValorNull) Then
        ' --------------------------------------------------------------------------------------------
        ' DUPLO CLICK ALTERA COR DO P1
        ' --------------------------------------------------------------------------------------------
        If gPassaRecFactus <> mediSim Then
            If iRec <> -1 Then
                If RegistosRM(iRec).ReciboNumDoc <> "0" Then
                    BG_Mensagem mediMsgBox, "J� foi emitido recibo.N�o � possivel alterar tipo P1", vbOKOnly + vbInformation, "Taxas"
                    Exit Sub
                End If
                
                EcAuxAna.text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf(((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)), Chr(KeyCode), "")
                If KeyCode = 13 Then
                    EcAuxAna.SelStart = 0
                    EcAuxAna.SelLength = Len(EcAuxAna)
                Else
                    EcAuxAna.SelLength = 0
                End If
        
                EcAuxAna.left = FGAna.CellLeft + 350
                EcAuxAna.top = SSTGestReq.top + FGAna.CellTop + 460
                EcAuxAna.Width = FGAna.CellWidth + 20
                EcAuxAna.Visible = True
                EcAuxAna.Enabled = True
                EcAuxAna.SetFocus
            End If
        ElseIf gPassaRecFactus = mediSim Then
                If fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).flg_estado_doe = 3 Or _
                   fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).flg_estado_tx = 3 Then
                    BG_Mensagem mediMsgBox, "N�o � possivel alterar credencial ", vbOKOnly + vbInformation, "Taxas"
                    Exit Sub
                End If
                
                EcAuxAna.text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf(((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)), Chr(KeyCode), "")
                If KeyCode = 13 Then
                    EcAuxAna.SelStart = 0
                    EcAuxAna.SelLength = Len(EcAuxAna)
                Else
                    EcAuxAna.SelLength = 0
                End If
        
                EcAuxAna.left = FGAna.CellLeft + 350
                EcAuxAna.top = SSTGestReq.top + FGAna.CellTop + 460
                EcAuxAna.Width = FGAna.CellWidth + 20
                EcAuxAna.Visible = True
                EcAuxAna.Enabled = True
                EcAuxAna.SetFocus
            End If
        
        'AlteraCorP1
    ElseIf FGAna.Col = lColFgAnaNrBenef And KeyCode = 13 And iRec > mediComboValorNull Then
        ' --------------------------------------------------------------------------------------------
        ' DUPLO CLICK EDITA NR BENEF
        ' --------------------------------------------------------------------------------------------
        
        If gPassaRecFactus <> mediSim Then
            If RegistosRM(iRec).ReciboNumDoc <> "0" And FGAna.Col = 5 Then
                BG_Mensagem mediMsgBox, "J� foi emitido recibo.N�o � possivel alterar N� Benefici�rio", vbOKOnly + vbInformation, "Taxas"
                Exit Sub
            End If
            EcAuxAna.text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf(((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)), Chr(KeyCode), "")
            If KeyCode = 13 Then
                EcAuxAna.SelStart = 0
                EcAuxAna.SelLength = Len(EcAuxAna)
            Else
                EcAuxAna.SelLength = 0
            End If
    
            EcAuxAna.left = FGAna.CellLeft + 350
            EcAuxAna.top = SSTGestReq.top + FGAna.CellTop + 460
            EcAuxAna.Width = FGAna.CellWidth + 20
            EcAuxAna.Visible = True
            EcAuxAna.Enabled = True
            EcAuxAna.SetFocus
        ElseIf gPassaRecFactus = mediSim Then
                If fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).flg_estado_doe = 3 Or _
                    fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).flg_estado_tx = 3 Then
                    BG_Mensagem mediMsgBox, "N�o � possivel alterar Benifici�rio ", vbOKOnly + vbInformation, "Taxas"
                    Exit Sub
                End If
            EcAuxAna.text = FGAna.TextMatrix(FGAna.row, FGAna.Col) & IIf(((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Or (KeyCode >= 96 And KeyCode <= 105)), Chr(KeyCode), "")
            If KeyCode = 13 Then
                EcAuxAna.SelStart = 0
                EcAuxAna.SelLength = Len(EcAuxAna)
            Else
                EcAuxAna.SelLength = 0
            End If
    
            EcAuxAna.left = FGAna.CellLeft + 350
            EcAuxAna.top = SSTGestReq.top + FGAna.CellTop + 460
            EcAuxAna.Width = FGAna.CellWidth + 20
            EcAuxAna.Visible = True
            EcAuxAna.Enabled = True
            EcAuxAna.SetFocus
        End If
    ElseIf (FGAna.Col = lColFgAnaDescrEFR) And (KeyCode = 13 And Shift = 0) And FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo) <> "" And (iRec > mediComboValorNull Or gPassaRecFactus = mediSim) Then ' Enter = Editar
        ' --------------------------------------------------------------------------------------------
        ' SE DA ENTER NA COLUNA DA ENTIDADE
        ' --------------------------------------------------------------------------------------------
            If iRec > 0 And gPassaRecFactus <> mediSim Then
                If BL_HandleNull(RegistosRM(iRec).ReciboNumDoc, "0") <> "0" Then
                    ' JA NAO E MARTELANCO--SOLIVEIRA LHL MARTELAN�O S� NAO PODE MUDAR A ENTIDADE SE O RECIBO ESTIVER NO ESTADO PAGO
                    Exit Sub
                End If
            ElseIf gPassaRecFactus = mediSim Then
                'Glintt-HS-19577 09.04.2019 Adicionado "<=" e fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).flg_estado = 3
                If AnalisesFact(FGAna.row).iResp > 0 And AnalisesFact(FGAna.row).iResp <= fa_movi_resp_tot Then
                    If AnalisesFact(FGAna.row).iAnaFact > 0 And AnalisesFact(FGAna.row).iAnaFact <= fa_movi_resp(AnalisesFact(FGAna.row).iResp).totalAna Then
                        If fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).flg_estado_doe = 3 Or _
                        fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).flg_estado_tx = 3 Or _
                        fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).flg_estado = 3 Then
                            BG_Mensagem mediMsgBox, "Entidade n�o pode ser alterada", vbOKOnly + vbInformation, "FACTUS"
                            Exit Sub
                        End If
                    End If
                End If
            End If
                        
            'edgar.parada Glintt-HS-19165
            If escolheEnt Then
              PesquisaEntFin
            End If
            '
            If EcCodEfr2 <> "" Then
                If gPassaRecFactus <> mediSim Then
                    AlteraEntidadeReciboManual iRec, EcCodEfr2, EcDescrEfr2, "", FGAna.row
                    iRec = DevIndice(RegistosA(FGAna.row).codAna)
                    VerificaEFRCodigoFilho RegistosRM(iRec).ReciboCodFacturavel, EcCodEfr2, EcDescrEfr2, FGAna.row
                Else
                    If CbHemodialise.ListIndex > mediComboValorNull Then
                        hemodialise = CbHemodialise.ItemData(CbHemodialise.ListIndex)
                    End If
                    'edgar.parada LJAMANSO-351 22.01.2019
                    convencao = obtemConvencao(indexCred, FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo), FGAna.TextMatrix(FGAna.row, lColFgAnaP1))
                    If CbCodUrbano.ListIndex > mediComboValorNull Then
                        cod_dom = CbCodUrbano.ItemData(CbCodUrbano.ListIndex)
                    Else
                        cod_dom = mediComboValorNull
                    End If
                    If AnalisesFact(i).iAnaFact > 0 Then
                        cod_medico = fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).cod_medico
                    Else
                        cod_medico = EcCodMedico.text
                    End If
                    If FACTUS_AlteraEntidade(EcCodEfr2, AnalisesFact(FGAna.row).iResp, AnalisesFact(FGAna.row).iAnaFact, _
                                          AnalisesFact(FGAna.row).cod_ana, hemodialise, EcDataChegada.text, BG_DaComboSel(CbTipoIsencao), FGAna.row, _
                                            convencao, cod_medico, cod_dom, BL_HandleNull(EcKm.text, mediComboValorNull), _
                                            EcCodPostalAux.text, EcDescrPostal.text, EcNumBenef.text, CbTipoUtente, EcUtente.text, _
                                            EcNumReq.text, EcCodProveniencia.text, mediComboValorNull) = True Then
                        For i = FGAna.row To FGAna.rows - 2
                            PreencheDadosNovoRecibo i
                                                        'edgar.parada Glintt-HS-19165
                            AtualisaDadosMoviFact CInt(i)
                                                        '
                        Next i
                        AtualizaFgRecibosNew
                        FGAna.Refresh
                        notAtualizouEFR = False

                    End If
                End If
                
            End If
    ElseIf FGAna.Col = lColFgAnaDestino And KeyCode = 13 And iRec > mediComboValorNull Then
        alteraDestinoAna FGAna.row
    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga inha
        If RegistosA(FGAna.row).Estado <> "-1" And RegistosA(FGAna.row).codAna <> "" Then
            Beep
            BG_Mensagem mediMsgStatus, "A an�lise n�o pode ser eliminada: j� cont�m resultados!"
        ElseIf RegistosA(FGAna.row).codAna <> "" Then
            Dim TempPerfilMarcacao As String
            Dim flgC As Boolean
            iRec = DevIndice(RegistosA(FGAna.row).codAna)
            If gPassaRecFactus = mediSim Then
                If FGAna.row < FGAna.rows - 1 Then
                    flgC = True
                Else
                    flgC = False
                End If
                'NELSONPSILVA 30.04.2019 Glintt-HS-21232
                If gAtivaESP = mediSim And (AnalisesFact(FGAna.row).iResp > 0 And AnalisesFact(FGAna.row).iAnaFact > 0) And fa_movi_resp_tot > 0 Then
                    If Len(fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).nr_req_ars) = 19 Then
                        If CredencialEfetivada(fa_movi_resp(AnalisesFact(FGAna.row).iResp).analises(AnalisesFact(FGAna.row).iAnaFact).nr_req_ars) = True Then
                            BG_Mensagem mediMsgBox, "A an�lise n�o pode ser apagada: j� foi efetivada!", vbExclamation
                            Exit Sub
                        End If
                    End If
                End If
                '
                If FACTUS_EliminaAna(AnalisesFact(FGAna.row).iResp, AnalisesFact(FGAna.row).iAnaFact, flgC) = False Then
                    BG_Mensagem mediMsgBox, "Erro ao apagar atividade da an�lise!", vbExclamation
                    Exit Sub
                Else
                    For i = FGAna.row To TotalAnalisesFact - 1
                        AnalisesFact(i).cod_ana = AnalisesFact(i + 1).cod_ana
                        AnalisesFact(i).iAna = AnalisesFact(i + 1).iAna
                        AnalisesFact(i).cod_rubr = AnalisesFact(i + 1).cod_rubr
                        AnalisesFact(i).n_seq_prog = AnalisesFact(i + 1).n_seq_prog
                        AnalisesFact(i).iResp = AnalisesFact(i + 1).iResp
                        AnalisesFact(i).iAnaFact = AnalisesFact(i + 1).iAnaFact
                        AnalisesFact(i).seq_ana_fact = AnalisesFact(i + 1).seq_ana_fact
                        AnalisesFact(i).cod_medico = AnalisesFact(i + 1).cod_medico
                        AtualisaDadosMoviFact CInt(i)
                    Next i
                    AtualizaFgRecibosNew
                    If TotalAnalisesFact > 0 Then
                        If TotalAnalisesFact <= UBound(AnalisesFact) Then
                            AnalisesFact(TotalAnalisesFact).cod_ana = ""
                            AnalisesFact(TotalAnalisesFact).cod_medico = ""
                            AnalisesFact(TotalAnalisesFact).cod_rubr = mediComboValorNull
                            AnalisesFact(TotalAnalisesFact).iAna = mediComboValorNull
                            AnalisesFact(TotalAnalisesFact).iAnaFact = mediComboValorNull
                            AnalisesFact(TotalAnalisesFact).iResp = mediComboValorNull
                            AnalisesFact(TotalAnalisesFact).n_seq_prog = mediComboValorNull
                            AnalisesFact(TotalAnalisesFact).seq_ana_fact = mediComboValorNull
                        End If
                        TotalAnalisesFact = TotalAnalisesFact - 1
                        ReDim Preserve AnalisesFact(TotalAnalisesFact)
                    End If
                    'FGONCALVES_UALIA
                    ActualizaOrdensFact
                End If
                TotalEliminadas = TotalEliminadas + 1
                ReDim Preserve Eliminadas(TotalEliminadas)
                Eliminadas(TotalEliminadas).codAna = RegistosA(FGAna.row).codAna
                Eliminadas(TotalEliminadas).NReq = EcNumReq.text
                'NELSONPSILVA 30.04.2019 Glintt-HS-21232
                Eliminadas(TotalEliminadas).credencial = FGAna.TextMatrix(FGAna.row, lColFgAnaP1)
                '
                Elimina_Mareq RegistosA(FGAna.row).codAna
                LbTotalAna = LbTotalAna - 1
                If LbTotalAna < 0 Then
                    LbTotalAna = 0
                End If
                ana = RegistosA(FGAna.row).codAna
                RegistosA.Remove FGAna.row
                FGAna.RemoveItem FGAna.row
                EliminaTuboEstrut
                EliminaProduto ana
                'NELSONPSILVA 30.04.2019 Glintt-HS-21232
                If gAtivaESP = mediSim And UBound(CredenciaisARS) > 0 Then EliminaAnaEspEstrut ana
                FGAna.Redraw = True
                '
            Else
                If iRec > 0 Or gPassaRecFactus = mediSim Then
                    TempPerfilMarcacao = RegistosRM(iRec).ReciboCodFacturavel
                
                    If TempPerfilMarcacao = "" Then
                        If EliminaReciboManual(iRec, ana, True) = True Then
                            LimpaColeccao RegistosRM, iRec
                        End If
                        iRec = -1
                            
                        EliminaCodigoFilho RegistosA(FGAna.row).codAna
                        TotalEliminadas = TotalEliminadas + 1
                        ReDim Preserve Eliminadas(TotalEliminadas)
                        Eliminadas(TotalEliminadas).codAna = RegistosA(FGAna.row).codAna
                        Eliminadas(TotalEliminadas).NReq = EcNumReq.text
                        'NELSONPSILVA 30.04.2019 Glintt-HS-21232
                        Eliminadas(TotalEliminadas).credencial = FGAna.TextMatrix(FGAna.row, lColFgAnaP1)
                        '
                        Elimina_Mareq RegistosA(FGAna.row).codAna
                        LbTotalAna = LbTotalAna - 1
                        If LbTotalAna < 0 Then
                            LbTotalAna = 0
                        End If
                        ana = RegistosA(FGAna.row).codAna
                        RegistosA.Remove FGAna.row
                        FGAna.RemoveItem FGAna.row
                        EliminaTuboEstrut
                        EliminaProduto ana
                        'NELSONPSILVA 30.04.2019 Glintt-HS-21232
                        If gAtivaESP = mediSim And UBound(CredenciaisARS) > 0 Then EliminaAnaEspEstrut ana
                        FGAna.Redraw = True
                        '
                    Else
                        If gPassaRecFactus <> mediSim Then
                            For i = RegistosA.Count - 1 To 1 Step -1
                                iRec = DevIndice(RegistosA(i).codAna)
                                If iRec > -1 Then
                                    If RegistosRM(iRec).ReciboCodFacturavel = TempPerfilMarcacao Then
                                        If EliminaReciboManual(iRec, ana, True) = True Then
                                            EliminaCodigoFilho RegistosRM(iRec).ReciboCodFacturavel
                                            LimpaColeccao RegistosRM, iRec
                                        Else
                                            RegistosRM(iRec).ReciboFlgMarcacaoRetirada = "1"
                                        End If
                                        TotalEliminadas = TotalEliminadas + 1
                                        ReDim Preserve Eliminadas(TotalEliminadas)
                                        Eliminadas(TotalEliminadas).codAna = RegistosA(i).codAna
                                        Eliminadas(TotalEliminadas).NReq = EcNumReq.text
                                        'NELSONPSILVA 30.04.2019 Glintt-HS-21232
                                        Eliminadas(TotalEliminadas).credencial = FGAna.TextMatrix(FGAna.row, lColFgAnaP1)
                                        '
                                        Elimina_Mareq RegistosA(i).codAna
                                        LbTotalAna = LbTotalAna - 1
                                        If LbTotalAna < 0 Then
                                            LbTotalAna = 0
                                        End If
                                        ana = RegistosA(i).codAna
                                        RegistosA.Remove i
                                        FGAna.RemoveItem i
                                        EliminaTuboEstrut
                                        EliminaProduto ana
                                        'Actualiza ord_marca da estrutura MaReq
                                        k = 1
                                        While k <= UBound(MaReq)
                                            If MaReq(k).Ord_Marca >= FGAna.row Then
                                                MaReq(k).Ord_Marca = MaReq(k).Ord_Marca - 1
                                            End If
                                            k = k + 1
                                        Wend
                                    End If
                                End If
                                iRec = -1
                            Next
                        End If
                    End If
                Else
                    EliminaCodigoFilho RegistosA(FGAna.row).codAna
                    TotalEliminadas = TotalEliminadas + 1
                    ReDim Preserve Eliminadas(TotalEliminadas)
                    Eliminadas(TotalEliminadas).codAna = RegistosA(FGAna.row).codAna
                    Eliminadas(TotalEliminadas).NReq = EcNumReq.text
                    'NELSONPSILVA 30.04.2019 Glintt-HS-21232
                    Eliminadas(TotalEliminadas).credencial = FGAna.TextMatrix(FGAna.row, lColFgAnaP1)
                    '
                    Elimina_Mareq RegistosA(FGAna.row).codAna
                    LbTotalAna = LbTotalAna - 1
                    If LbTotalAna < 0 Then
                        LbTotalAna = 0
                    End If
                    ana = RegistosA(FGAna.row).codAna
                    RegistosA.Remove FGAna.row
                    FGAna.RemoveItem FGAna.row
                    EliminaTuboEstrut
                    EliminaProduto ana
                    'NELSONPSILVA 30.04.2019 Glintt-HS-21232
                    If gAtivaESP = mediSim And UBound(CredenciaisARS) > 0 Then EliminaAnaEspEstrut ana
                    FGAna.Redraw = True
                    '
                End If
            End If
        Else
            If FGAna.TextMatrix(FGAna.row, 0) <> "" Then
            'Situa��es que se adicionou linha mas que n�o se acrescentou nada
            'Selecciou op��o delete ou elimina linha
            FGAna.RemoveItem FGAna.row
            'Actualiza ord_marca da estrutura MaReq
            k = 1
            While k <= UBound(MaReq)
                If MaReq(k).Ord_Marca >= FGAna.row Then
                    MaReq(k).Ord_Marca = MaReq(k).Ord_Marca - 1
                End If
                k = k + 1
            Wend
            End If
        End If
        
        FGAna.SetFocus
    
    ElseIf KeyCode = 118 Then ' F7 - INSERIR OBSERVA��O DA AN�LISE
        If EcNumReq = "" Then
            BG_Mensagem mediMsgBox, "Tem que criar a requisi��o primeiro!", vbExclamation
            Exit Sub
        End If
        If FGAna.TextMatrix(LastRowA, lColFgAnaCodigo) <> "" And LastRowA <> 0 Then
            linhaAnaliseOBS = LastRowA
            FrameObsAna.caption = "Observa��o da An�lise: " & FGAna.TextMatrix(LastRowA, lColFgAnaDescricao)
            FrameObsAna.Visible = True
            FrameObsAna.left = 8280
            FrameObsAna.top = 0
            EcObsAna.text = RegistosA(LastRowA).ObsAnaReq
            EcObsAna.SetFocus
        End If
    End If
    'edgar.parada  Glintt-HS-19165
    If CredencialESP(credImportada) And FGAna.Col = lColFgAnaDescrEFR And notAtualizouEFR = False Then
     atualizaEFRESP credImportada, EcCodEfr2
    End If
    '
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgAna_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "FgAna_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub ActualizaOrdensFact()
    Dim i As Integer
    Dim iResp As Integer
    Dim iAnaFact As Integer
    For i = 1 To TotalAnalisesFact
        For iResp = 1 To fa_movi_resp_tot
            For iAnaFact = 1 To fa_movi_resp(iResp).totalAna
                If AnalisesFact(i).iResp = iResp And AnalisesFact(i).iAnaFact = iAnaFact And AnalisesFact(i).n_seq_prog = fa_movi_resp(iResp).analises(iAnaFact).n_seq_prog Then
                    fa_movi_resp(iResp).analises(iAnaFact).iAnalise = i
                    Exit For
                End If
            Next iAnaFact
        Next iResp
    Next i
End Sub
Private Sub alteraDestinoAna(ByVal indice As Integer)
    On Error GoTo TrataErro
    Dim i As Integer
    Dim j As Integer
    Dim prescricoes() As String
    Dim totalPrescr As Integer
    Dim flgExiste As Boolean
    Dim destino As String
    gMsgTitulo = "Destino"
    gMsgMsg = "Pretende imprimir an�lise num novo relat�rio?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    Me.SetFocus

    If gMsgResp = vbYes Then
        totalPrescr = 0
        ReDim prescricoes(0)
        For i = 1 To RegistosA.Count
            If RegistosA(i).n_prescricao <> "" Then
                flgExiste = False
                For j = 1 To totalPrescr
                    If RegistosA(i).n_prescricao = prescricoes(j) Then
                       flgExiste = True
                    End If
                Next j
                If flgExiste = False Then
                    totalPrescr = totalPrescr + 1
                    ReDim Preserve prescricoes(totalPrescr)
                    prescricoes(totalPrescr) = RegistosA(i).n_prescricao
                End If
            End If
        Next i
        If totalPrescr >= 1 Then
            EcAuxAna.text = ""
            FormPesquisaRapida.InitPesquisaRapida "sl_tbf_t_destino", _
                                "descr_t_dest", "cod_t_dest", _
                                  EcAuxAna
            RegistosA(indice).n_prescricao = totalPrescr + 1
            RegistosA(indice).cod_destino = EcAuxAna.text
            RegistosA(indice).descr_destino = BL_SelCodigo("SL_TBF_T_DESTINO", "DESCR_T_DEST", "COD_T_DEST", RegistosA(indice).cod_destino)
            FGAna.TextMatrix(indice, lColFgAnaDestino) = RegistosA(indice).n_prescricao & "-" & RegistosA(indice).descr_destino
            EcAuxAna.text = ""
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "alteraDestinoAna: " & Err.Number & " - " & Err.Description, Me.Name, "alteraDestinoAna"
    Exit Sub
    Resume Next
End Sub
Private Sub FGAna_LostFocus()
    On Error GoTo TrataErro

    If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, lColFgAnaCodigo)) <> "" Then
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbRed
    Else
        FGAna.CellBackColor = vbWhite
        FGAna.CellForeColor = vbBlack
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGAna_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FGAna_LostFocus"
    Exit Sub
    Resume Next
    
End Sub

Private Sub FGAna_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error GoTo TrataErro
    If Button = 2 And FGAna.Col = 0 And FGAna.row > 0 Then
        MDIFormInicio.PopupMenu MDIFormInicio.HIDE_ANA
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGAna_MouseDown: " & Err.Number & " - " & Err.Description, Me.Name, "FGAna_MouseDown"
    Exit Sub
    Resume Next
End Sub

Private Sub Fgana_RowColChange()
    On Error GoTo TrataErro
    
    Dim tmpCol As Integer
    Dim tmpRow As Integer
        
    If ExecutaCodigoA = True Then
        ExecutaCodigoA = False
        
        tmpCol = FGAna.Col
        tmpRow = FGAna.row
        If FGAna.Col <> LastColA Then FGAna.Col = LastColA
        If FGAna.row <> LastRowA Then FGAna.row = LastRowA
        
        If FGAna.row > 0 And FGAna.row <= RegistosA.Count Then
            If RegistosA(LastRowA).Estado <> "-1" And LastColA = 0 And Trim(FGAna.TextMatrix(LastRowA, 0)) <> "" Then
                FGAna.CellBackColor = vbWhite
                FGAna.CellForeColor = vbRed
            Else
                'FGAna.CellBackColor = vbWhite
                'FGAna.CellForeColor = vbBlack
            End If
        End If
        
        If FGAna.Col <> tmpCol Then FGAna.Col = tmpCol
        If FGAna.row <> tmpRow Then FGAna.row = tmpRow
        
        ' Controlar as colunas
        If FGAna.Cols = 4 Then
            If FGAna.Col = 3 Then
'                FGAna.Col = 0          'sliveira 10-12-2003
'                If FGAna.Row < FGAna.Rows - 1 Then FGAna.Row = FGAna.Row + 1
            ElseIf FGAna.Col = 1 Then
                If LastColA = 2 Then
                    FGAna.Col = 0
                Else
                    FGAna.Col = 2
                End If
            End If
        Else
            If FGAna.Col = 1 Then
             '   FgAna.Col = 0
             '   If FgAna.row < FgAna.rows - 1 Then FgAna.row = FgAna.row + 1
            End If
        End If
        
        LastColA = FGAna.Col
        LastRowA = FGAna.row
        
        If FGAna.row > 0 And FGAna.row <= RegistosA.Count Then
            If RegistosA(FGAna.row).Estado <> "-1" And FGAna.Col = 0 And Trim(FGAna.TextMatrix(FGAna.row, 0)) <> "" Then
                FGAna.CellBackColor = vbRed
                FGAna.CellForeColor = vbWhite
            Else
                'FGAna.CellBackColor = azul
                'FGAna.CellForeColor = vbWhite
            End If
        End If
        
        ExecutaCodigoA = True
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "Fgana_RowColChange: " & Err.Number & " - " & Err.Description, Me.Name, "Fgana_RowColChange"
    Exit Sub
    Resume Next
End Sub


Private Sub FgAnaRec_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    On Error GoTo TrataErro
    If RegistosR(FGRecibos.row).Estado = gEstadoReciboAnulado Or RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca Or _
       RegistosR(FGRecibos.row).Estado = gEstadoReciboPago Or EstrutAnaRecibo(FgAnaRec.row).Flg_adicionada = 1 Then
        Exit Sub
    End If
    If KeyCode = 46 Then
        If (RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido Or RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo Or _
           RegistosR(FGRecibos.row).Estado = gEstadoReciboPerdido) And UBound(EstrutAnaRecibo) > 0 Then
            If EstrutAnaRecibo(FgAnaRec.row).tipo = "MANUAL" Then
                EliminaReciboManual EstrutAnaRecibo(FgAnaRec.row).indice, EstrutAnaRecibo(FgAnaRec.row).codAna, True
                LimpaColeccao RegistosRM, EstrutAnaRecibo(FgAnaRec.row).indice
                FgAnaRec.RemoveItem FgAnaRec.row
                FGRecibos_DblClick
            Else
                For i = 1 To RegistosA.Count - 1
                    If RegistosA(i).codAna = EstrutAnaRecibo(FgAnaRec.row).codAna Then
                        Exit Sub
                    End If
                Next
                EliminaReciboManual EstrutAnaRecibo(FgAnaRec.row).indice, EstrutAnaRecibo(FgAnaRec.row).codAna, True
                FgAnaRec.RemoveItem FgAnaRec.row
                LimpaColeccao RegistosRM, EstrutAnaRecibo(FgAnaRec.row).indice
                FGRecibos_DblClick
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgAnaRec_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "FgAnaRec_KeyDown"
    Exit Sub
    Resume Next
End Sub

Private Sub FgProd_GotFocus()
    On Error GoTo TrataErro
    Dim i As Integer
    FgProd.CellBackColor = azul
    FgProd.CellForeColor = vbWhite
    
    If FgProd.Col = 0 Then
        BtPesquisaProduto.Enabled = True
        BtPesquisaEspecif.Enabled = False
    ElseIf FgProd.Col = 2 Then
        BtPesquisaProduto.Enabled = False
        BtPesquisaEspecif.Enabled = True
        BtObsEspecif.Enabled = True
    Else
        BtPesquisaProduto.Enabled = False
        BtPesquisaEspecif.Enabled = False
    End If
    
    If gLAB = "CHVNG" Then
        BtPesquisaProduto.Enabled = False
        For i = 1 To FGAna.rows - 1
            If FGAna.TextMatrix(i, lColFgAnaCodigo) = "PCULMCB" Then
                BtPesquisaProduto.Enabled = True
            End If
        Next
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "FgProd_GotFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FgProd_GotFocus"
    Exit Sub
    Resume Next
End Sub


Private Sub FgQuestoes_DblClick()
    If FgQuestoes.Col = 1 Then
        If FgQuestoes.row > 0 And FgQuestoes.row <= totalQuestao Then
            EcAuxQuestao = EstrutQuestao(FgQuestoes.row).Resposta
            EcAuxQuestao.left = FgQuestoes.CellLeft + FgQuestoes.left
            EcAuxQuestao.top = FgQuestoes.CellTop + FgQuestoes.top
            EcAuxQuestao.Width = FgQuestoes.CellWidth + 20
            EcAuxQuestao.Visible = True
            EcAuxQuestao.SetFocus
        End If
    End If
End Sub

Private Sub FGRecibos_Click()
    On Error GoTo TrataErro
    If flg_ReqBloqueada = True Then
        BtAnularRecibo.Enabled = False
        BtImprimirRecibo.Enabled = False
        BtGerarRecibo.Enabled = False
        BtCobranca.Enabled = False
        BtPagamento.Enabled = False
        
        Exit Sub
    End If
    If gPassaRecFactus <> mediSim Then
        If FGRecibos.row > 0 And FGRecibos.TextMatrix(FGRecibos.row, 1) <> "" Then
            If RegistosR(FGRecibos.row).Estado = gEstadoReciboAnulado Then
                BtAnularRecibo.Enabled = True
                BtImprimirRecibo.Enabled = False
                BtGerarRecibo.Enabled = False
                BtCobranca.Enabled = False
                BtPagamento.Enabled = False
            ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboPerdido Then
                BtAnularRecibo.Enabled = False
                BtImprimirRecibo.Enabled = False
                BtGerarRecibo.Enabled = True
                BtCobranca.Enabled = False
                BtPagamento.Enabled = False
            ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboPago Then
                BtAnularRecibo.Enabled = True
                BtImprimirRecibo.Enabled = True
                BtGerarRecibo.Enabled = False
                BtCobranca.Enabled = True
                BtPagamento.Enabled = False
            ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca Then
                BtAnularRecibo.Enabled = True
                BtImprimirRecibo.Enabled = True
                BtGerarRecibo.Enabled = False
                BtCobranca.Enabled = False
                BtPagamento.Enabled = True
            ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido Then
                BtAnularRecibo.Enabled = False
                BtImprimirRecibo.Enabled = False
                BtGerarRecibo.Enabled = True
                BtCobranca.Enabled = False
                BtPagamento.Enabled = False
            ElseIf RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo Then
                BtAnularRecibo.Enabled = False
                BtImprimirRecibo.Enabled = False
                BtGerarRecibo.Enabled = False
                BtCobranca.Enabled = False
                BtPagamento.Enabled = False
            End If
        Else
            BtCobranca.Enabled = False
            BtPagamento.Enabled = False
            BtAnularRecibo.Enabled = False
            BtImprimirRecibo.Enabled = False
            BtGerarRecibo.Enabled = False
        End If
    Else
        If FGRecibos.row > 0 And FGRecibos.row <= totalRecibosNew Then
            If RecibosNew(FGRecibos.row).flg_estado = gEstadoFactCancelado Then
                BtAnularRecibo.Enabled = False
                BtImprimirRecibo.Enabled = False
                BtGerarRecibo.Enabled = False
                BtCobranca.Enabled = False
                BtPagamento.Enabled = False
            ElseIf RecibosNew(FGRecibos.row).flg_estado = gEstadoFactFaturado And RecibosNew(FGRecibos.row).dt_pagamento <> "" Then
                BtAnularRecibo.Enabled = True
                BtImprimirRecibo.Enabled = True
                BtGerarRecibo.Enabled = False
                BtCobranca.Enabled = True
                BtPagamento.Enabled = False
            ElseIf RecibosNew(FGRecibos.row).flg_estado = gEstadoFactFaturado And RecibosNew(FGRecibos.row).dt_pagamento = "" Then
                BtAnularRecibo.Enabled = True
                BtImprimirRecibo.Enabled = True
                BtGerarRecibo.Enabled = False
                BtCobranca.Enabled = False
                BtPagamento.Enabled = True
            ElseIf RecibosNew(FGRecibos.row).flg_estado = gEstadoFactNaoFaturado Then
                BtAnularRecibo.Enabled = False
                BtImprimirRecibo.Enabled = False
                BtGerarRecibo.Enabled = True
                'GX-58037 MANSO 04.09.2020
                BtGerarRecibo.Visible = True
                BtCobranca.Enabled = False
                BtPagamento.Enabled = False
            Else
                BtAnularRecibo.Enabled = False
                BtImprimirRecibo.Enabled = False
                BtGerarRecibo.Enabled = False
                BtCobranca.Enabled = False
                BtPagamento.Enabled = False
            End If
        Else
            BtCobranca.Enabled = False
            BtPagamento.Enabled = False
            BtAnularRecibo.Enabled = False
            BtImprimirRecibo.Enabled = False
            BtGerarRecibo.Enabled = False
        End If
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGRecibos_Click: " & Err.Number & " - " & Err.Description, Me.Name, "FGRecibos_Click"
    Exit Sub
    Resume Next
    
End Sub

Private Sub FGRecibos_DblClick()
    On Error GoTo TrataErro
    Dim i As Long
    InicializaFrameAnaRecibos
    If FGRecibos.row > 0 And (FGRecibos.TextMatrix(FGRecibos.row, lColRecibosNumDoc) = "" Or FGRecibos.Col = lColRecibosBorla Or FGRecibos.Col = lColRecibosEntidade) Then
        If FGRecibos.Col = lColRecibosPercentagem And gPermAltPreco = mediSim Then
            If gPassaRecFactus <> mediSim Then
                If RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido Or (RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo And RegistosR(FGRecibos.row).Desconto <> 0) Then
                    EcAuxRec.text = FGRecibos.TextMatrix(FGRecibos.row, FGRecibos.Col)
                    EcAuxRec.left = FGRecibos.CellLeft + 270
                    EcAuxRec.top = SSTGestReq.top + FGRecibos.CellTop + 600
                    EcAuxRec.Width = FGRecibos.CellWidth + 20
                    EcAuxRec.Visible = True
                    EcAuxRec.Enabled = True
                    EcAuxRec.SetFocus
                End If
            Else
                'UALIA-920 Removido (RecibosNew(FGRecibos.row).valor > 0)
                If RecibosNew(FGRecibos.row).n_Fac_doe <= 0 Then
                 'LJMANSO-302
                    If PermiteAlterarPrecoEntidade Then
                        EcAuxRec.text = FGRecibos.TextMatrix(FGRecibos.row, FGRecibos.Col)
                        EcAuxRec.left = FGRecibos.CellLeft + 270
                        EcAuxRec.top = SSTGestReq.top + FGRecibos.CellTop + 600
                        EcAuxRec.Width = FGRecibos.CellWidth + 20
                        EcAuxRec.Visible = True
                        EcAuxRec.Enabled = True
                        EcAuxRec.SetFocus
                  End If
                End If
            End If
        ElseIf FGRecibos.Col = lColRecibosValorPagar And gPermAltPreco = mediSim Then
            If gPassaRecFactus <> mediSim Then
                If RegistosR(FGRecibos.row).Estado = gEstadoReciboNaoEmitido Or (RegistosR(FGRecibos.row).Estado = gEstadoReciboNulo And RegistosR(FGRecibos.row).Desconto <> 0) Then
                    EcAuxRec.text = FGRecibos.TextMatrix(FGRecibos.row, FGRecibos.Col)
                    EcAuxRec.left = FGRecibos.CellLeft + 270
                    EcAuxRec.top = SSTGestReq.top + FGRecibos.CellTop + 600
                    EcAuxRec.Width = FGRecibos.CellWidth + 20
                    EcAuxRec.Visible = True
                    EcAuxRec.Enabled = True
                    EcAuxRec.SetFocus
                End If
            Else
                'UALIA-920 Removido (RecibosNew(FGRecibos.row).valor > 0)
                If RecibosNew(FGRecibos.row).n_Fac_doe <= 0 Then
                    'LJMANSO-302
                    If PermiteAlterarPrecoEntidade Then
                        EcAuxRec.text = FGRecibos.TextMatrix(FGRecibos.row, FGRecibos.Col)
                        EcAuxRec.left = FGRecibos.CellLeft + 270
                        EcAuxRec.top = SSTGestReq.top + FGRecibos.CellTop + 600
                        EcAuxRec.Width = FGRecibos.CellWidth + 20
                        EcAuxRec.Visible = True
                        EcAuxRec.Enabled = True
                        EcAuxRec.SetFocus
                    End If
                    '
                End If
            End If
        ElseIf FGRecibos.Col = lColRecibosBorla And gPermAltPreco = mediSim Then
            If gPassaRecFactus <> mediSim Then
                If RegistosR(FGRecibos.row).FlgBorla = mediSim Then
                    RegistosR(FGRecibos.row).FlgBorla = mediNao
                    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosBorla) = "N�o"
                Else
                    RegistosR(FGRecibos.row).FlgBorla = mediSim
                    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosBorla) = "Sim"
                End If
            Else
                If RecibosNew(FGRecibos.row).flg_borla = "S" Then
                    RecibosNew(FGRecibos.row).flg_borla = "N"
                    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosBorla) = "N"
                Else
                    RecibosNew(FGRecibos.row).flg_borla = "S"
                    FGRecibos.TextMatrix(FGRecibos.row, lColRecibosBorla) = "S"
                    
                End If
                FACTUS_ReciboBorla FGRecibos.row, RecibosNew(FGRecibos.row).flg_borla
            End If
            
        ElseIf FGRecibos.Col = lColRecibosNaoConfmidade And gPermAltPreco = mediSim Then
            If RegistosR(FGRecibos.row).FlgNaoConforme = mediSim Then
                RegistosR(FGRecibos.row).FlgNaoConforme = mediNao
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosNaoConfmidade) = "Sim"
            Else
                RegistosR(FGRecibos.row).FlgNaoConforme = mediSim
                FGRecibos.TextMatrix(FGRecibos.row, lColRecibosNaoConfmidade) = "N�o"
            End If
        Else
            If gPassaRecFactus <> mediSim Then
                TotalAnaRecibo = 0
                ReDim EstrutAnaRecibo(0)
                
                For i = 1 To RegistosRM.Count
                    If RegistosRM(i).ReciboEntidade = RegistosR(FGRecibos.row).codEntidade Then
                        If CLng(RegistosRM(i).ReciboNumDoc) = CLng(BL_HandleNull(RegistosR(FGRecibos.row).NumDoc, "0")) Then
                            TotalAnaRecibo = TotalAnaRecibo + 1
                            ReDim Preserve EstrutAnaRecibo(TotalAnaRecibo)
                            
                            EstrutAnaRecibo(TotalAnaRecibo).codAna = RegistosRM(i).ReciboCodFacturavel
                            EstrutAnaRecibo(TotalAnaRecibo).descrAna = BL_SelCodigo("SLV_ANALISES_FACTUS", "DESCR_ANA", "COD_ANA", RegistosRM(i).ReciboCodFacturavel)
                            EstrutAnaRecibo(TotalAnaRecibo).indice = i
                            EstrutAnaRecibo(TotalAnaRecibo).Flg_adicionada = RegistosRM(i).ReciboFlgAdicionado
                            If RegistosRM(i).ReciboFlgAdicionado = "1" Then
                                EstrutAnaRecibo(TotalAnaRecibo).taxa = 0
                            Else
                                EstrutAnaRecibo(TotalAnaRecibo).taxa = RegistosRM(i).ReciboTaxa
                            End If
                            EstrutAnaRecibo(TotalAnaRecibo).qtd = RegistosRM(i).ReciboQuantidade
                            If RegistosRM(i).ReciboFlgReciboManual = 1 Then
                                EstrutAnaRecibo(TotalAnaRecibo).tipo = "MANUAL"
                            Else
                                EstrutAnaRecibo(TotalAnaRecibo).tipo = "AUTOM"
                            End If
                            
                        End If
                    End If
                Next
                LimpaFgRecAna
                For i = 1 To TotalAnaRecibo
                    FgAnaRec.TextMatrix(i, lColFgAnaRecCodAna) = EstrutAnaRecibo(i).codAna
                    FgAnaRec.TextMatrix(i, lColFgAnaRecDescrAna) = EstrutAnaRecibo(i).descrAna
                    FgAnaRec.TextMatrix(i, lColFgAnaRecQtd) = EstrutAnaRecibo(i).qtd
                    FgAnaRec.TextMatrix(i, lColFgAnaRecTaxa) = EstrutAnaRecibo(i).taxa
                    FgAnaRec.AddItem ""
                Next
            Else
                PreencheFgMoviFact
            End If
            FrameAnaRecibos.Visible = True
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGRecibos_DblClick: " & Err.Number & " - " & Err.Description, Me.Name, "FGRecibos_DblClick"
    Exit Sub
    Resume Next
End Sub


Public Sub FgProd_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    If KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Then ' Enter = Editar
        If LastColP <> 0 And Trim(FgProd.TextMatrix(LastRowP, 0)) = "" Then
            Beep
            BG_Mensagem mediMsgStatus, "Tem que indicar primeiro um produto!"
        Else
            If LastColP = 6 And EcDataPrevista.Enabled = False Then
                Beep
                BG_Mensagem mediMsgStatus, "J� n�o pode alterar a data prevista a este produto!"
            Else
                If LastColP = 0 And gLAB = "CHVNG" Then
                Else
                EcAuxProd.text = FgProd.TextMatrix(FgProd.row, FgProd.Col) & IIf((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0), Chr(KeyCode), "")
                If KeyCode = 8 Then
                    EcAuxProd.text = Mid(EcAuxProd.text, 1, Len(EcAuxProd.text) - 1)
                End If
                If KeyCode = 13 Then
                    EcAuxProd.SelStart = 0
                    EcAuxProd.SelLength = Len(EcAuxProd)
                Else
                    EcAuxProd.SelLength = 0
                End If
                EcAuxProd.left = FgProd.CellLeft + 270
                EcAuxProd.top = FgProd.CellTop + 2060
                EcAuxProd.Width = FgProd.CellWidth + 20
                EcAuxProd.Visible = True
                EcAuxProd.Enabled = True
                EcAuxProd.SetFocus
                End If
            End If
        End If
    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga celula
        Select Case FgProd.Col
            Case 0
                If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Procura_Prod_ana = False Then
                    If FgProd.row < FgProd.rows - 1 Then
                        RegistosP.Remove FgProd.row
                        FgProd.RemoveItem FgProd.row
                    Else
                        FgProd.TextMatrix(FgProd.row, 0) = ""
                        FgProd.TextMatrix(FgProd.row, 1) = ""
                        FgProd.TextMatrix(FgProd.row, 2) = ""
                        FgProd.TextMatrix(FgProd.row, 3) = ""
                        FgProd.TextMatrix(FgProd.row, 4) = ""
                        FgProd.TextMatrix(FgProd.row, 5) = ""
                        FgProd.TextMatrix(FgProd.row, 6) = ""
                        FgProd.TextMatrix(FgProd.row, 7) = ""
                        RegistosP(FgProd.row).CodProd = ""
                        RegistosP(FgProd.row).DescrProd = ""
                        RegistosP(FgProd.row).EspecifObrig = ""
                        RegistosP(FgProd.row).CodEspecif = ""
                        RegistosP(FgProd.row).DescrEspecif = ""
                        RegistosP(FgProd.row).DtPrev = ""
                        RegistosP(FgProd.row).DtChega = ""
                        RegistosP(FgProd.row).EstadoProd = ""
                        RegistosP(FgProd.row).Volume = ""
                    End If
                End If
            Case 2
                FgProd.TextMatrix(FgProd.row, 2) = ""
                FgProd.TextMatrix(FgProd.row, 3) = ""
                RegistosP(FgProd.row).CodEspecif = ""
                RegistosP(FgProd.row).DescrEspecif = ""
            Case 4
                FgProd.TextMatrix(FgProd.row, 4) = ""
                RegistosP(FgProd.row).Volume = ""
            Case 5
                FgProd.TextMatrix(FgProd.row, 5) = ""
                RegistosP(FgProd.row).DtPrev = ""
            Case 6
                FgProd.TextMatrix(FgProd.row, 6) = ""
                RegistosP(FgProd.row).DtChega = ""
        End Select
        FgProd.SetFocus
    ElseIf KeyCode = 46 And Shift = 1 Then ' Shift + Delete = Apaga linha
        If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Procura_Prod_ana = False Then
            If BG_Mensagem(mediMsgBox, "Confirma a elimina��o do " & FgProd.TextMatrix(FgProd.row, 1) & " " & FgProd.TextMatrix(FgProd.row, 3) & " ?", vbYesNo + vbQuestion, "Eliminar produto.") = vbYes Then
                If FgProd.row < FgProd.rows - 1 Then
                    RegistosP.Remove FgProd.row
                    FgProd.RemoveItem FgProd.row
                Else
                    FgProd.TextMatrix(FgProd.row, 0) = ""
                    FgProd.TextMatrix(FgProd.row, 1) = ""
                    FgProd.TextMatrix(FgProd.row, 2) = ""
                    FgProd.TextMatrix(FgProd.row, 3) = ""
                    FgProd.TextMatrix(FgProd.row, 4) = ""
                    FgProd.TextMatrix(FgProd.row, 5) = ""
                    FgProd.TextMatrix(FgProd.row, 6) = ""
                    FgProd.TextMatrix(FgProd.row, 7) = ""
                    RegistosP(FgProd.row).CodProd = ""
                    RegistosP(FgProd.row).DescrProd = ""
                    RegistosP(FgProd.row).EspecifObrig = ""
                    RegistosP(FgProd.row).CodEspecif = ""
                    RegistosP(FgProd.row).DescrEspecif = ""
                    RegistosP(FgProd.row).DtPrev = ""
                    RegistosP(FgProd.row).DtChega = ""
                    RegistosP(FgProd.row).EstadoProd = ""
                    RegistosP(FgProd.row).Volume = ""
                    
                End If
            End If
        End If
        FgProd.SetFocus
    Else
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgProd_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "FgProd_KeyDown"
    Exit Sub
    Resume Next

End Sub

Public Sub FGTubos_KeyDown(KeyCode As Integer, Shift As Integer)
     On Error GoTo TrataErro
'
'    If KeyCode = 13 Or KeyCode = 8 Or (KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0) Then ' Enter = Editar
'        If LastColT <> 0 And Trim(FGTubos.TextMatrix(LastRowT, 0)) = "" Then
'            Beep
'            BG_Mensagem mediMsgStatus, "Tem que indicar primeiro um tubo!"
'        Else
'            If LastColT = 3 And EcDataPrevista.Enabled = False Then
'                Beep
'                BG_Mensagem mediMsgStatus, "J� n�o pode alterar a data prevista a este tubo!"
'            Else
'                EcAuxTubo.text = FGTubos.TextMatrix(FGTubos.row, FGTubos.Col) & IIf((KeyCode >= 32 And KeyCode <= 90 And KeyCode <> 46 And Shift = 0), Chr(KeyCode), "")
'                If KeyCode = 8 Then
'                    EcAuxTubo.text = Mid(EcAuxTubo.text, 1, Len(EcAuxTubo.text) - 1)
'                End If
'                If KeyCode = 13 Then
'                    EcAuxTubo.SelStart = 0
'                    EcAuxTubo.SelLength = Len(EcAuxTubo)
'                Else
'                    EcAuxTubo.SelLength = 0
'                End If
'                EcAuxTubo.left = FGTubos.CellLeft + 270
'                EcAuxTubo.top = FGTubos.CellTop + 2060
'                EcAuxTubo.Width = FGTubos.CellWidth + 20
'                EcAuxTubo.Visible = True
'                EcAuxTubo.Enabled = True
'                EcAuxTubo.SetFocus
'            End If
'        End If
'    ElseIf KeyCode = 46 And Shift = 0 Then ' Delete = Apaga celula
'        Select Case FGTubos.Col
'            Case 0
'                If Trim(FGTubos.TextMatrix(FGTubos.row, 0)) <> "" And Procura_Tubo_ana = False Then
'                    AcrescentaTubosEliminados EcNumReq, RegistosT(FGTubos.row).seqTubo
'                    If FGTubos.row < FGTubos.rows - 1 Then
'                        RegistosT.Remove FGTubos.row
'                        FGTubos.RemoveItem FGTubos.row
'                    Else
'                        FGTubos.TextMatrix(FGTubos.row, 0) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 1) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 2) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 3) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 4) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 5) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 6) = ""
'                        FGTubos.TextMatrix(FGTubos.row, 7) = ""
'                        RegistosT(FGTubos.row).seqTubo = mediComboValorNull
'                        RegistosT(FGTubos.row).CodTubo = ""
'                        RegistosT(FGTubos.row).descrTubo = ""
'                        RegistosT(FGTubos.row).garrafa = ""
'                        RegistosT(FGTubos.row).DtPrev = ""
'                        RegistosT(FGTubos.row).DtChega = ""
'                        RegistosT(FGTubos.row).HrChega = ""
'                        RegistosT(FGTubos.row).UserChega = ""
'                        RegistosT(FGTubos.row).EstadoTubo = ""
'                    End If
'                End If
'            Case 2  'Garrafa
'                FGTubos.TextMatrix(FGTubos.row, 2) = ""
'                RegistosT(FGTubos.row).garrafa = ""
'            Case 3  'Data Prevista
'                FGTubos.TextMatrix(FGTubos.row, 3) = ""
'                RegistosT(FGTubos.row).DtPrev = ""
'            Case 4  'Data Chegada
'                FGTubos.TextMatrix(FGTubos.row, 4) = ""
'                RegistosT(FGTubos.row).DtChega = ""
'                FGTubos.TextMatrix(FGTubos.row, 5) = ""
'                RegistosT(FGTubos.row).HrChega = ""
'            Case 5  'Hora Chegada
'                FGTubos.TextMatrix(FGTubos.row, 5) = ""
'                RegistosT(FGTubos.row).HrChega = ""
'        End Select
'        FGTubos.SetFocus
'    ElseIf KeyCode = 46 And Shift = 1 Then ' Shift + Delete = Apaga linha
'        If Trim(FGTubos.TextMatrix(FGTubos.row, 0)) <> "" And Procura_Tubo_ana = False Then
'            If BG_Mensagem(mediMsgBox, "Confirma a elimina��o do " & FGTubos.TextMatrix(FgProd.row, 1) & " ?", vbYesNo + vbQuestion, "Eliminar tubo.") = vbYes Then
'                AcrescentaTubosEliminados EcNumReq, RegistosT(FGTubos.row).seqTubo
'                If FGTubos.row < FGTubos.rows - 1 Then
'                    RegistosT.Remove FGTubos.row
'                    FGTubos.RemoveItem FGTubos.row
'                Else
'                    FGTubos.TextMatrix(FGTubos.row, 0) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 1) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 2) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 3) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 4) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 5) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 6) = ""
'                    FGTubos.TextMatrix(FGTubos.row, 7) = ""
'                    RegistosT(FGTubos.row).seqTubo = mediComboValorNull
'                    RegistosT(FGTubos.row).CodTubo = ""
'                    RegistosT(FGTubos.row).descrTubo = ""
'                    RegistosT(FGTubos.row).garrafa = ""
'                    RegistosT(FGTubos.row).DtPrev = ""
'                    RegistosT(FGTubos.row).DtChega = ""
'                    RegistosT(FGTubos.row).HrChega = ""
'                    RegistosT(FGTubos.row).UserChega = ""
'                    RegistosT(FGTubos.row).EstadoTubo = ""
'                End If
'            End If
'        End If
'        FGTubos.SetFocus
'    Else
'    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGTubos_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "FGTubos_KeyDown"
    Exit Sub
    Resume Next
End Sub


Function SELECT_Descr_Prod() As Boolean
     On Error GoTo TrataErro
   
    'Fun��o que procura a descri��o do produto
    'A fun��o devolve TRUE se encontrou a descri��o caso contr�rio devolve FALSE
    
    Dim rsDescr As adodb.recordset
    Set rsDescr = New adodb.recordset
    
    SELECT_Descr_Prod = False
    With rsDescr
        .Source = "SELECT descr_produto,especif_obrig FROM sl_produto WHERE " & _
                  "cod_produto = " & BL_TrataStringParaBD(UCase(EcAuxProd.text))
        .ActiveConnection = gConexao
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With
    
    If rsDescr.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "Produto inexistente!", vbOKOnly + vbInformation, "Produtos"
        EcAuxProd.text = ""
    Else
        FgProd.TextMatrix(LastRowP, 1) = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(LastRowP).DescrProd = BL_HandleNull(rsDescr!descr_produto, "")
        RegistosP(LastRowP).EspecifObrig = rsDescr!especif_obrig
        SELECT_Descr_Prod = True
    End If
    
    rsDescr.Close
    Set rsDescr = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "SELECT_Descr_Prod: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Descr_Prod"
    SELECT_Descr_Prod = False
    Exit Function
    Resume Next
End Function

Function SELECT_Especif() As Boolean
     On Error GoTo TrataErro
   
    Dim RsDescrEspecif As adodb.recordset
    Dim encontrou As Boolean
    
    SELECT_Especif = False
    encontrou = False
    If Trim(EcAuxProd.text) <> "" Then
        Set RsDescrEspecif = New adodb.recordset
        With RsDescrEspecif
            .Source = "SELECT descr_especif FROM sl_especif WHERE " & _
                    "cod_especif=" & UCase(BL_TrataStringParaBD(EcAuxProd.text))
            .ActiveConnection = gConexao
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
        If RsDescrEspecif.RecordCount > 0 Then
            FgProd.TextMatrix(LastRowP, 3) = BL_HandleNull(RsDescrEspecif!descr_especif, "")
            RegistosP(LastRowP).DescrEspecif = BL_HandleNull(RsDescrEspecif!descr_especif, "")
            SELECT_Especif = True
        Else
            BG_Mensagem mediMsgBox, "Especifica��o inexistente!", vbOKOnly + vbInformation, "Especifica��es"
        End If
        RsDescrEspecif.Close
        Set RsDescrEspecif = Nothing
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "SELECT_Especif: " & Err.Number & " - " & Err.Description, Me.Name, "SELECT_Especif"
    SELECT_Especif = False
    Exit Function
    Resume Next

End Function

Private Sub FgProd_LostFocus()
    On Error GoTo TrataErro

    FgProd.CellBackColor = vbWhite
    FgProd.CellForeColor = vbBlack
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgProd_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FgProd_LostFocus"
    Exit Sub
    Resume Next
    
End Sub

Private Sub FGTubos_LostFocus()

    FGTubos.CellBackColor = vbWhite
    FGTubos.CellForeColor = vbBlack
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGTubos_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "FGTubos_LostFocus"
    Exit Sub
    Resume Next
    
End Sub

Private Sub FgProd_RowColChange()
     On Error GoTo TrataErro
   
    Dim tmpCol As Integer
    Dim tmpRow As Integer
    Dim RsEspecif As adodb.recordset
    
    If ExecutaCodigoP = True Then
        ExecutaCodigoP = False
        
        BtObsEspecif.Enabled = False
        
        If Trim(FgProd.TextMatrix(FgProd.row, 0)) <> "" And Trim(FgProd.TextMatrix(FgProd.row, 2)) <> "" Then
            BtObsEspecif.Enabled = True
        End If
        
        If Trim(FgProd.TextMatrix(LastRowP, 0)) <> "" And Trim(FgProd.TextMatrix(LastRowP, 2)) = "" Then
    
            'Escolher a especifica��o de defeito do produto
            
            Set RsEspecif = New adodb.recordset
                
            With RsEspecif
                .Source = "select sl_produto.cod_especif, sl_especif.descr_especif from sl_produto, sl_especif where sl_produto.cod_especif = sl_especif.cod_especif and cod_produto = " & BL_TrataStringParaBD(Trim(FgProd.TextMatrix(LastRowP, 0)))
                .CursorLocation = adUseServer
                .CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros .Source
                .Open , gConexao
            End With
            
            If Not RsEspecif.EOF Then
                FgProd.TextMatrix(LastRowP, 2) = Trim(BL_HandleNull(RsEspecif!cod_Especif, ""))
                FgProd.TextMatrix(LastRowP, 3) = Trim(BL_HandleNull(RsEspecif!descr_especif, ""))
            End If
            
            RsEspecif.Close
        End If
            
        Set RsEspecif = Nothing
        
        tmpCol = FgProd.Col
        tmpRow = FgProd.row
        If FgProd.Col <> LastColP Then FgProd.Col = LastColP
        If FgProd.row <> LastRowP Then FgProd.row = LastRowP
        FgProd.CellBackColor = vbWhite
        FgProd.CellForeColor = vbBlack
        If FgProd.Col <> tmpCol Then FgProd.Col = tmpCol
        If FgProd.row <> tmpRow Then FgProd.row = tmpRow
        
        If FgProd.row <> LastRowP Then
            Preenche_LaProdutos FgProd.row
        End If
        
        ' Controlar as colunas
        If FgProd.Col = 1 Then
            If LastColP >= 2 Then
                FgProd.Col = 0
            Else
                FgProd.Col = 2
            End If
        ElseIf FgProd.Col = 3 Then
            If LastColP >= 5 Then
                FgProd.Col = 2
            Else
                FgProd.Col = 5
            End If
        ElseIf LastColP = 5 And LastRowP = FgProd.row And FgProd.Col <> 6 Then
            FgProd.Col = 0
            If FgProd.row <> FgProd.rows - 1 Then FgProd.row = FgProd.row + 1
        ElseIf FgProd.Col > 4 Then
            FgProd.Col = 0
            If FgProd.row <> FgProd.rows - 1 Then FgProd.row = FgProd.row + 1
        End If
        
        LastColP = FgProd.Col
        LastRowP = FgProd.row
        
        FgProd.CellBackColor = azul
        FgProd.CellForeColor = vbWhite
        
        If LastColP = 0 Then
            BtPesquisaProduto.Enabled = True
            BtPesquisaEspecif.Enabled = False
        ElseIf LastColP = 2 Then
            BtPesquisaProduto.Enabled = False
            BtPesquisaEspecif.Enabled = True
            BtObsEspecif.Enabled = True
        Else
            BtPesquisaProduto.Enabled = False
            BtPesquisaEspecif.Enabled = False
        End If
        If gLAB = "CHVNG" Then
            BtPesquisaProduto.Enabled = False
        End If
        ExecutaCodigoP = True
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgProd_RowColChange: " & Err.Number & " - " & Err.Description, Me.Name, "FgProd_RowColChange"
    Exit Sub
    Resume Next
End Sub

Private Sub FGTubos_RowColChange()
     On Error GoTo TrataErro
   
    Dim tmpCol As Integer
    Dim tmpRow As Integer
    
    If ExecutaCodigoT = True Then
        ExecutaCodigoT = False
        
        tmpCol = FGTubos.Col
        tmpRow = FGTubos.row
        If FGTubos.Col <> LastColT Then FGTubos.Col = LastColT
        If FGTubos.row <> LastRowT Then FGTubos.row = LastRowT
        FGTubos.CellBackColor = vbWhite
        FGTubos.CellForeColor = vbBlack
        If FGTubos.Col <> tmpCol Then FGTubos.Col = tmpCol
        If FGTubos.row <> tmpRow Then FGTubos.row = tmpRow
        
        If FGTubos.row <> LastRowT And FGTubos.row <> 0 Then
            Preenche_LaTubos FGTubos.row
        End If
        
        ' Controlar as colunas
        'verificar este controlo de colunas de na FGtubos '04-07-2006
        If FGTubos.Col = 1 Then
            If LastColT >= 2 Then
                FGTubos.Col = 0
            Else
                FGTubos.Col = 2
            End If
        ElseIf FGTubos.Col = 3 Then
            If LastColT >= 4 Then
                FGTubos.Col = 2
            Else
                FGTubos.Col = 4
            End If
        ElseIf LastColT = 4 And LastRowT = FGTubos.row And FGTubos.Col <> 5 Then
            FGTubos.Col = 0
            If FGTubos.row <> FGTubos.rows - 1 Then FGTubos.row = FGTubos.row + 1
        ElseIf FGTubos.Col > 4 Then
            FGTubos.Col = 0
            If FGTubos.row <> FGTubos.rows - 1 Then FGTubos.row = FGTubos.row + 1
        End If
        
        LastColT = FGTubos.Col
        LastRowT = FGTubos.row
        
        FGTubos.CellBackColor = azul
        FGTubos.CellForeColor = vbWhite
        
        ExecutaCodigoT = True
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FGTubos_RowColChange: " & Err.Number & " - " & Err.Description, Me.Name, "FGTubos_RowColChange"
    Exit Sub
    Resume Next
End Sub

Private Sub Form_Activate()

    EventoActivate

End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Me, "")

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Flg_LimpaCampos = False

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error GoTo TrataErro

    If UnloadMode <> 1 Then
        If Flg_Gravou = False Then
            If BG_Mensagem(mediMsgBox, "Deseja realmente sair sem gravar?", vbQuestion + vbYesNo, "Sislab") = vbNo Then
                'respondeu n�o
                Cancel = True
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Form_QueryUnload: " & Err.Number & " - " & Err.Description, Me.Name, "Form_QueryUnload"
    Exit Sub
    Resume Next
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub FuncaoInserir(Optional not_msg As Boolean)
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsNumBenef As New adodb.recordset
    Dim iRes As Integer
    Dim i As Integer
    Dim TotalReg As Long
    Dim rv As Integer
    Dim mostraMgmPagar As Boolean
    mostraMgmPagar = False
    Dim RsVerifNReq As New adodb.recordset
    
    If BL_VerificaCampoRequis(Me, EcNumReq, EcCodSala) = False Then
        Exit Sub
    End If
    
    If ValidaDadosFactARS = False Then
        Exit Sub
    End If
    
    If lObrigaInfoCli = True Then
        If BL_ValidaDadosInfoCli(EcSeqUtente.text, EcNumReq.text, "I") = False Then
            MsgBox "An�lises marcadas exigem informa��o cl�nica.", vbExclamation, " Requisi��o"
            Exit Sub
        End If
    End If
    If lObrigaTerap = True Then
        If BL_ValidaDadosInfoCli(EcSeqUtente.text, EcNumReq.text, "T") = False Then
            MsgBox "An�lises marcadas exigem terap�utica/medica��o.", vbExclamation, " Requisi��o"
            Exit Sub
        End If
    End If
    
    If gPreencheDatasReq = mediSim Then
        If EcDataPrevista.text = "" Then
            EcDataPrevista.text = dataAct
        End If
        If EcDataChegada.text = "" Then
            EcDataChegada.text = dataAct
        End If
    End If
    
    
    If EcNumBenef = "" And VerificaObrigaNrBenef(EcCodEFR) = True Then
        BG_Mensagem mediMsgBox, "N�mero de benefici�rio obrigat�rio! ", vbInformation, "Aten��o"
        EcNumBenef.text = ""
        EcNumBenef.SetFocus
        Exit Sub
    End If
    
    
    If EcCodEFR = "" Then
            BG_Mensagem mediMsgBox, "Tem de indicar uma entidade fin.", vbInformation, "Aten��o"
            Sendkeys ("{HOME}+{END}")
            EcCodEFR = ""
            EcCodEFR.SetFocus
            Exit Sub
    End If
    If EcCodProveniencia <> "" Then
        If BL_HandleNull(BL_SelCodigo("SL_PROVEN", "flg_invisivel", "cod_proven", EcCodProveniencia, "V"), 0) = 1 Then
            BG_Mensagem mediMsgBox, "Proveni�ncia desactivada. ", vbInformation, "Aten��o"
            EcCodProveniencia.text = ""
            EcDescrProveniencia = ""
            EcCodProveniencia.SetFocus
            Exit Sub
        End If
    End If
    If EcCodSala <> "" Then
        If BL_VerificaSalaValida(EcCodSala, CStr(gCodLocal)) = False Then
            EcCodSala.text = ""
            EcDescrSala = ""
            EcCodSala.SetFocus
            Exit Sub
        End If
    End If
    
    If EcCodEFR <> "" Then
        If BL_VerificaEfrValida(EcCodEFR, CStr(gCodLocal)) = False Then
            EcCodEFR.text = ""
            EcDescrEFR = ""
            EcCodEFR.SetFocus
            Exit Sub
        End If
    Else
        Exit Sub
    End If
    

    If (totalInfo > 0 Or totalQuestao > 0) And gLAB = "CL" Then
        SSTGestReq.Tab = 6
        BG_Mensagem mediMsgBox, "Existem Informa��es / Quest�es associadas a An�lises", vbInformation, "Aten��o"
    End If
    
    If not_msg = True Then
        gMsgResp = vbYes
    Else
        gMsgTitulo = "Inserir"
        gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    End If
    
    Me.SetFocus

    If gMsgResp = vbYes Then
        
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = dataAct
        EcHoraCriacao = Bg_DaHora_ADO
        
        'se a data prevista estiver vazia
        If EcDataPrevista.text = "" Then
            EcDataPrevista.text = Format(dataAct, gFormatoData)
        End If
        
        ValidaDataPrevista
'        If DateValue(EcDataPrevista.Text) < DateValue(dataAct) Then Exit Sub
        
        ' Obriga a especifica��o de produtos.
        If (gObrigaProduto) Then
            If Not (ValidaProdutos) Then
                MsgBox "N�o est� especificado nenhum produto.    " & vbCrLf & _
                    "A inser��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
                Exit Sub
            End If
        End If
        
        If Len(EcGrupoAna.text) = 1 Then
            If ProdutoUnico(EcGrupoAna.text) And RegistosP.Count > 1 Then
                MsgBox "Esta requisi��o s� pode ter um produto associado. " & vbCrLf & _
                    "A inser��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
            End If
        End If
        
        If CbHemodialise.ListIndex <> mediComboValorNull Then
            VerificaDoenteHemodialise
        End If
        
        If CbTipoIsencao.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Tipo de isen��o obrigat�rio!", vbOKOnly + vbInformation, "Insen��es"
            CbTipoIsencao.SetFocus
            Exit Sub
        End If
        
        If Not ValidaEspecifObrig Then Exit Sub
        
        'NELSONPSILVA LJMANSO-336 06.12.2018
        If ValidaCodPostalFact = False Then
            If CbCodUrbano.ListIndex <> -1 Then
                BG_Mensagem mediMsgBox, "Insira C�digo Postal com 7 Digitos!", vbInformation, "C�digo Postal"
                Exit Sub
            Else
                BG_Mensagem mediMsgBox, "Insira C�digo Postal com 7 Digitos!", vbInformation, "C�digo Postal"
                EcCodPostal.text = ""
                EcRuaPostal.text = ""
                EcDescrPostal.text = ""
            End If
        End If
        
        If ValidaNumBenef = True Then
            iRes = ValidaCamposEc
            If iRes = True Then
            
                Call BD_Insert
                If Trim(EcNumReq.text) <> "" Then
                    'Se n�o houve erro
                    If (BL_HandleNull(BL_SelCodigo("SL_COD_SALAS", "FLG_COLHEITA", "COD_SALA", EcCodSala), "") = "0" And gNReqPreImpressa <> 1) Or gImprAutoEtiq = mediSim Then
                        If gUsaNovaImpEtiquetas = mediSim Then
                            If gMostraMgmPagaRecibo = mediNao Then
                                ImprimeEtiqNovo
                            End If
                        Else
                        
                            ImprimeEtiq
                        End If
                        DoEvents
                    End If
                    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
                    BL_Toolbar_BotaoEstado "Procurar", "Activo"
                    BL_Toolbar_BotaoEstado "Limpar", "Activo"
                    'Mart. ergonomico
                    Call FuncaoProcurar
                    
                    'edgar.parada Glintt-HS-19165
                    btCativarReq.Enabled = False
                    '
                                                                                                                                         
                    'FGONCALVES_UALIA
                    If gTipoInstituicao = "PRIVADA" And RegistosR.Count > 1 And gPassaRecFactus <> mediSim Then
                        If gMostraMgmPagaRecibo <> mediNao Then
                            For i = 1 To RegistosR.Count - 1
                                If RegistosR(i).Estado = gEstadoReciboNaoEmitido Then
                                    If RegistosR(i).ValorPagar > 0 Then
                                        mostraMgmPagar = True
                                        Exit For
                                    End If
                                End If
                            Next
                            If mostraMgmPagar = True Then
                                If EcDataChegada.text = "" Then
                                    BG_Mensagem mediMsgBox, "Requisi��o sem data de chegada. ", vbInformation, "Aten��o"
                                    Sendkeys ("{HOME}+{END}")
                                    EcDataChegada.text = ""
                                    EcDataChegada.SetFocus
                                Else
                                
                                    gMsgTitulo = "Recibos"
                                    gMsgMsg = "Utente vai pagar?"
                                    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                                    If gMsgResp = vbYes Then
                                        If gPassaRecFactus <> mediSim Then
                                            For i = 1 To RegistosR.Count - 1
                                                VerificaUtentePagaActoMedico RegistosR(i).codEntidade, RegistosR(i).SerieDoc, RegistosR(i).NumDoc
                                            Next
                                            UPDATE_Recibos_Req
                                        Else
                                            FACTUS_VerificaUtentePagaActoMedico
                                            AtualizaFgRecibosNew
                                        End If
                                        flgEmissaoRecibos = True
                                        FormEmissaoRecibos.Form = Me.Name
                                        FormEmissaoRecibos.Show 'vbModal
                                    End If
                                End If
                            End If
                        End If
                    ElseIf gTipoInstituicao = "PRIVADA" And gPassaRecFactus = mediSim Then
                        If gMostraMgmPagaRecibo <> mediNao Then
                            'NELSONPSILVA UALIA-869
                            For i = 1 To UBound(RecibosNew)
                                If RecibosNew(i).valor > 0 Then
                                    mostraMgmPagar = True
                                    Exit For
                                End If
                            Next i
                            If mostraMgmPagar = True Then
                                If EcDataChegada.text = "" Then
                                    BG_Mensagem mediMsgBox, "Requisi��o sem data de chegada. ", vbInformation, "Aten��o"
                                    Sendkeys ("{HOME}+{END}")
                                    EcDataChegada.text = ""
                                    EcDataChegada.SetFocus
                                Else
                                
                                    gMsgTitulo = "Recibos"
                                    gMsgMsg = "Utente vai pagar?"
                                    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                                    If gMsgResp = vbYes Then
                                        FACTUS_VerificaUtentePagaActoMedico
                                        AtualizaFgRecibosNew
                                        flgEmissaoRecibos = True
                                        FormEmissaoRecibos.Form = Me.Name
                                        FormEmissaoRecibos.Show 'vbModal
                                    End If
                                End If
                            End If
                        End If
                    
                    End If
                End If
            End If
        End If
    End If

    If (gFormGesReqCons_Aberto = True) Then
        ' Quando este form foi invocado atrav�s de FormGesReqCons,
        ' apaga a marca��o correspondente.
        
        rv = MARCACAO_Apaga(FormGesReqCons.EcNumReq.text)
        FormGesReqCons.LimpaCampos
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoInserir: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoInserir"
    Exit Sub
    Resume Next
End Sub

Function BD_Insert() As Boolean
    
    Dim SQLQuery As String
    Dim i As Integer
    Dim k As Integer
    Dim req As Long
    Dim ProdJaChegou As Boolean
    Dim rv As Integer
    Dim prioridade_colheita As String
    'Dim convencao As Integer
    'edgar.parada Glintt-HS-19165
    Dim countCredESP As Integer
    '
    On Error GoTo Trata_Erro
    
        'edgar.parada Abre Liga��o Secund�ria
    If gConexaoSecundaria.state = adStateClosed Then
       BL_Abre_Conexao_Secundaria gOracle
    End If
    '
    gSQLError = 0
    gSQLISAM = 0
    BD_Insert = False
    'edgar.parada Glintt-HS-19165
    countCredESP = 1
    'If UBound(estrutOperation) >= 0 Then
        If gPassaRecFactus = mediSim Then
            'LJMANSO-301
             AtualizaCodPostalFact
             'edgar.parada LJMANSO-351
             If FACTUS_ValidaRegras(RequisicaoESP(EcNumReq.text), countCredESP) = False Then
               BD_Insert = False
               Exit Function
             End If
        End If
    'End If
    '
    'soliveira arunce
    'Utiliza etiquetas pr�-impressas com indica��o do n�mero de requisi��o final
    If gNReqPreImpressa = 1 Then
        req = EcNumReq.text
    Else
        'Determinar o novo n�mero da requisi��o (TENTA 10 VEZES - LOCK!)
        i = 0
        req = -1
        While req = -1 And i <= 10
            req = BL_GeraNumero("N_REQUIS")
            i = i + 1
        Wend
        
        If req = -1 Then
            BG_Mensagem mediMsgBox, "Erro a gerar n�mero de requisi��o !", vbCritical + vbOKOnly, "ERRO"
            Exit Function
        End If
        
        EcNumReq.text = ""
        EcNumReq.text = req
    End If
    
    
    
    If EcNumReqAssoc = "" Then
        EcNumReqAssoc = EcNumReq
    End If
    gRequisicaoActiva = CLng(EcNumReq.text)
    EcLocal = gCodLocal
    
    BG_BeginTransaction
    
    If EcNumColheita.text = "" Then
        EcNumColheita.text = COLH_InsereNovaColheita(CLng(EcSeqUtente.text))
    End If
    If EcNumColheita.text = "" Then
        BG_Mensagem mediMsgBox, "Erro ao gravar colheita!", vbCritical + vbOKOnly, "ERRO"
        Exit Function
    Else
        If COLH_AssociaReqColheita(EcNumReq.text, EcNumColheita.text, CLng(EcSeqUtente.text)) = "" Then
            BG_Mensagem mediMsgBox, "Erro ao associar requisi��o a colheita!", vbCritical + vbOKOnly, "ERRO"
            Exit Function
        End If
    End If
    ' Produtos da requisi��o
    Grava_Prod_Req
    If gSQLError <> 0 Then
        'Erro a gravar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os produtos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' Tubos da requisi��o
    'RGONCALVES 15.12.2014 - adicionado parametro dt_chega
    TB_GravaTubosReq EcNumReq.text, EcDataChegada.text
    If gSQLError <> 0 Then
        'Erro a gravar os tubos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar os tubos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'actualizar as obs das analises
    Grava_ObsAnaReq
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar as observa��es das an�lises !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    
    If gPassaRecFactus <> mediSim Then
        ' RECIBOS DA REQUISICAO
        If RECIBO_GravaRecibo(EcNumReq, RegistosA, RegistosR, RegistosRM, False) = False Then
            'Erro a gravar os tubos da requisi��o
            BG_Mensagem mediMsgBox, "Erro a gravar os recibos da requisi��o !", vbError, "ERRO"
            GoTo Trata_Erro
        End If
    
        ' ADIANTAMENTOS DA REQUISICAO
        If RECIBO_GravaAdiantamento(EcNumReq, RegistosAD) = False Then
            'Erro a gravar os tubos da requisi��o
            BG_Mensagem mediMsgBox, "Erro a gravar os adiantamentos da requisi��o !", vbError, "ERRO"
            GoTo Trata_Erro
        End If
    Else
        AtualizaCodPostalFact
        If CbCodUrbano.ListIndex > mediComboValorNull And gPassaRecFactus = mediSim Then
            FACTUS_AtualizaDom CbCodUrbano.ItemData(CbCodUrbano.ListIndex), BL_HandleNull(EcKm.text, 0)
        Else
            FACTUS_AtualizaDom 0, 0
        End If
        If FACTUS_GravaDados(EcNumReq.text, mediComboValorNull) = True Then
            GravaDadosAnaFact
        Else
            BG_Mensagem mediMsgBox, "Erro ao gravar atividade para fatura��o.", vbError, "ERRO"
            GoTo Trata_Erro
        End If
    End If

    'actualizar as obs das analises
    Grava_ObsAnaReq
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar as observa��es das an�lises !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' An�lises da requisi��o
    Call Grava_Ana_Req
    If gSQLError <> 0 Then
        'Erro a gravar as an�lises da requisi��o
        BG_Mensagem mediMsgBox, "Erro a gravar as an�lises da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If

    'GRAVA PERFIS MARCADOS
    If PM_GravaPerfisMarcados(EcNumReq.text) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar perfis marcados", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Actualizar estado da requisi��o em conformidade com an�lises marcadas, produtos marcados e sua data de chegada
    If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Or EcEstadoReq = gEstadoReqEsperaProduto Or EcEstadoReq = gEstadoReqEsperaResultados Or EcEstadoReq = gEstadoReqSemAnalises Then
        ProdJaChegou = False
        If FGAna.rows >= 1 And Trim(FGAna.TextMatrix(1, lColFgAnaCodigo)) <> "" Then
            If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
                For k = 1 To RegistosP.Count - 1
                    If Trim(RegistosP(1).DtChega) <> "" Then
                        ProdJaChegou = True
                    End If
                Next k
                If ProdJaChegou = True Then
                    EcEstadoReq = gEstadoReqEsperaResultados
                Else
                    EcEstadoReq = gEstadoReqEsperaProduto
                End If
            Else
                EcEstadoReq = gEstadoReqEsperaResultados
                EcDataChegada.text = dataAct
            End If
        Else
           EcEstadoReq = " "
        End If
    End If
    EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
    
    
    EcGrupoAna.text = ""
    
                
    ' Diagn�sticos secund�rios e terapeuticas/medica��o
    Grava_DS_TM
    If gSQLError <> 0 Then
        'Erro a actualizar diagn�sticos secund�rios e terapeuticas/medica��o
        BG_Mensagem mediMsgBox, "Erro a actualizar diagn�sticos secund�rios e terap�uticas/medica��o!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' Grava informacoes das analises
    If GravaInfoAna(EcNumReq) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar informa��es das an�lises!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Grava questoes das analises
    If GravaQuestoesAna(EcNumReq) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar Quest�es das an�lises!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    If EcDataChegada.text <> "" Then
        If EcHoraChegada.text = "" Then
            EcHoraChegada.text = Bg_DaHora_ADO
        End If
    Else
        EcHoraChegada.text = ""
    End If

    EcDtEntrega = BL_VerificaConclusaoRequisicao(EcNumReq, EcDataPrevista.text)
    
    ' Gravar os restantes dados da requisi��o
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
    'fila de espera
    If CbPrioColheita.ListIndex > mediComboValorNull Then
        prioridade_colheita = CbPrioColheita.ItemData(CbPrioColheita.ListIndex)
    Else
        prioridade_colheita = ""
    End If
    FE_InsereFilaEspera EcNumReq.text, EcSeqUtente.text, EcCodSala.text, prioridade_colheita, EcCodProveniencia.text
    
    If gAtivaESP = mediSim Then
        Call Grava_Ana_ARS
        'EP COLOCAR A CHAMADA DE INSERT SL_REQ_ARS 12-03-2021
        If gPassaRecFactus <> mediSim Then
         IF_GravaSdReqArs EcNumReq
        End If
    End If
    If gSQLError <> 0 Then
        GoTo Trata_Erro
    Else
        BG_CommitTransaction
        gRequisicaoActiva = CLng(EcNumReq.text)
        Flg_Gravou = True
        
    End If
    
    BD_Insert = True
Exit Function
Trata_Erro:
    'edgar.parada Glintt-HS-19165 * tratar exception
    'If Err.Number = 9 Then
    '  countCredESP = -1
    '  Resume Next
    'End If
    '
    BD_Insert = False
    BG_RollbackTransaction
    BG_Mensagem mediMsgBox, "Erro a inserir requisi��o!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormGestaoRequisicaoPrivado: BD_Insert -> " & Err.Description
    gRequisicaoActiva = 0
    FuncaoLimpar
    Exit Function
    Resume Next
End Function

'NELSONPSILVA LJMANSO-336 06.12.2018
Function ValidaCodPostalFact() As Boolean
    ValidaCodPostalFact = True
    If EcCodPostal.text <> "" Or EcRuaPostal.text <> "" Or EcDescrPostal.text <> "" Then
        If (IsNumeric(EcCodPostal) And Len(EcCodPostal) = 4) And (IsNumeric(EcRuaPostal) And Len(EcRuaPostal) = 3) Then
            EcCodPostalAux = (EcCodPostal.text & "-" & EcRuaPostal.text)
            EcDescrPostal = BL_SeleccionaDescrPostal(EcCodPostalAux)
            
            If EcDescrPostal.text = "" Then
                ValidaCodPostalFact = False
            End If
        Else
            ValidaCodPostalFact = False
        End If
    End If
End Function

Private Sub AtualizaCodPostalFact()
    Dim iResp As Integer
    Dim iAna As Integer
    For iResp = 1 To fa_movi_resp_tot
        For iAna = 1 To fa_movi_resp(iResp).totalAna
            fa_movi_resp(iResp).analises(iAna).cod_post_dom = EcCodPostal.text
            fa_movi_resp(iResp).analises(iAna).localidade_dom = EcDescrPostal.text
            If EcRuaPostal.text <> "" Then
                fa_movi_resp(iResp).analises(iAna).cod_post_dom = fa_movi_resp(iResp).analises(iAna).cod_post_dom & "-" & EcRuaPostal.text
            End If
        Next iAna
    Next iResp
End Sub
Function Grava_Ana_Req() As Boolean
    On Error GoTo TrataErro
    Grava_Ana_Req = False
    Dim i As Integer
    Dim k As Integer
    Dim ssql As String
    
    Dim maximo As Long
    Dim sql As String
    Dim RsOrd As New adodb.recordset
    Dim analises As String
    Dim flg_realiza As Boolean
    Dim seqReqTubo As String
    Set RsOrd = New adodb.recordset


    If UBound(MaReq) = 0 Then
        BG_Mensagem mediMsgBox, "N�o foram registadas an�lises!", vbInformation, "Grava��o de an�lises"
        SSTGestReq.Tab = 2
    End If
    
    flg_realiza = False

    'Marca��o de an�lises (sl_marcacoes)
    For i = 1 To UBound(MaReq)

        MaReq(i).dt_chega = EcDataChegada.text  'Bruno & sdo

        'S� s�o gravadas as an�lise "novas" ou que estavam na tabela de marca��es, as an�lises da tabela de realiza��es n�o s�o marcadas
            '-1 -> an�lise nova ou an�lise da tabela sl_marca�oes
        ssql = " SELECT ordem, gr_ana FROM sl_ana_s WHERE cod_ana_s =  " & BL_TrataStringParaBD(MaReq(i).cod_agrup) & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_ana_c WHERE cod_ana_c =  " & BL_TrataStringParaBD(MaReq(i).cod_agrup) & _
                            " Union " & _
                            " SELECT ordem, gr_ana FROM sl_perfis WHERE cod_perfis =  " & BL_TrataStringParaBD(MaReq(i).cod_agrup)
        
        RsOrd.CursorType = adOpenStatic
        RsOrd.CursorLocation = adUseServer
        RsOrd.LockType = adLockReadOnly
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        RsOrd.Open ssql, gConexao
        
        'Set RsOrd = CmdOrdAna.Execute

        If Not RsOrd.EOF Then
            If BL_HandleNull(RsOrd!ordem, 0) <> 0 Then
                MaReq(i).Ord_Ana = BL_HandleNull(RsOrd!ordem, 0)
            Else
                maximo = 0
                For k = 1 To UBound(MaReq)
                    If MaReq(k).Ord_Ana > maximo Then maximo = MaReq(k).Ord_Ana
                Next k
                maximo = maximo + 1
                'Colocar a an�lise para o fim
                MaReq(i).Ord_Ana = maximo
            End If
            If RsOrd!gr_ana <> "" Then
                If EcGrupoAna.text = "" Then
                    EcGrupoAna.text = RsOrd!gr_ana
                Else
                    If InStr(1, EcGrupoAna.text, RsOrd!gr_ana) <> 0 Then
                    Else
                        EcGrupoAna.text = EcGrupoAna.text & ";" & RsOrd!gr_ana
                    End If
                End If
            End If
        Else
            'Colocar a an�lise para o fim
            maximo = 0
            For k = 1 To UBound(MaReq)
                If MaReq(k).Ord_Ana > maximo Then maximo = MaReq(k).Ord_Ana
            Next k
            maximo = maximo + 1
            'Colocar a an�lise para o fim
            MaReq(i).Ord_Ana = maximo
        End If
        RsOrd.Close

        'MaReq(i).Ord_Ana = i

        If MaReq(i).flg_estado = "-1" Then
            If VerificaAnaliseJaExisteRealiza(CLng(Trim(EcNumReq.text)), UCase((MaReq(i).Cod_Perfil)), UCase((MaReq(i).cod_ana_c)), UCase((MaReq(i).cod_ana_s)), True) > 0 Then
                flg_realiza = True
            Else
                Dim RsTrans As adodb.recordset
                'verifica se a flag "flg_transmanual" na an�lise est� a 1,
                'se sim coloca "flg_apar_trans" a 2 em sl_marcacoes
                Set RsTrans = New adodb.recordset
                RsTrans.CursorLocation = adUseServer
                RsTrans.CursorType = adOpenStatic
                sql = "SELECT flg_transmanual FROM sl_ana_s WHERE cod_ana_s=" & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
                If gModoDebug = mediSim Then BG_LogFile_Erros sql
                RsTrans.Open sql, gConexao
    
                If (Not (RsTrans.EOF)) Then
                    If (BL_HandleNull(RsTrans!flg_transManual, 0) = 1 And MaReq(i).Flg_Apar_Transf = 0) Then
                        MaReq(i).Flg_Apar_Transf = 2
                    End If
                End If
                If BL_HandleNull(MaReq(i).seq_req_tubo, mediComboValorNull) > mediComboValorNull Then
                    seqReqTubo = MaReq(i).seq_req_tubo
                Else
                    seqReqTubo = " NULL "
                End If
                sql = "INSERT INTO sl_marcacoes(n_req,cod_perfil,cod_ana_c,cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup,n_folha_trab,dt_chega,flg_apar_trans,ord_marca, flg_facturado, transmit_psm, n_envio, seq_req_tubo"
                sql = sql & ") VALUES (" & CLng(Trim(EcNumReq.text)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s)) & "," & MaReq(i).Ord_Ana
                sql = sql & "," & MaReq(i).Ord_Ana_Compl & "," & MaReq(i).Ord_Ana_Perf & "," & UCase(BL_TrataStringParaBD(MaReq(i).cod_agrup)) & "," & MaReq(i).N_Folha_Trab & "," & IIf((MaReq(i).dt_chega = ""), "NULL", BL_TrataDataParaBD(MaReq(i).dt_chega)) & ","
                sql = sql & UCase(BL_TrataStringParaBD(MaReq(i).Flg_Apar_Transf)) & "," & MaReq(i).Ord_Marca & ", " & MaReq(i).Flg_Facturado & ", " & MaReq(i).transmit_psm & ", " & MaReq(i).n_envio & ", " & seqReqTubo & ")"
                BG_ExecutaQuery_ADO sql
                
    
                If gSQLError <> 0 Then
                    Exit For
                End If
    
                RsTrans.Close
                Set RsTrans = Nothing
            End If

        'soliveira 08-09-2003
        'para alterar a flag flg_facturado quando as analises j� se encontram na tabela sl_realiza
        'ElseIf MaReq(i).Flg_Facturado = 1 Then 'alterei por causa do tratamento de rejeitados
        Else
            If EcEstadoReq = gEstadoReqHistorico Then
                sql = "UPDATE sl_realiza_h set flg_facturado = " & MaReq(i).Flg_Facturado & _
                        " WHERE n_req = " & BL_TrataStringParaBD(CStr(Trim(EcNumReq.text))) & _
                        " AND cod_perfil = " & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & _
                        " AND cod_ana_c = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & _
                        " AND cod_ana_s = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
            Else
                sql = "UPDATE sl_realiza set flg_facturado = " & MaReq(i).Flg_Facturado & _
                        " WHERE n_req = " & BL_TrataStringParaBD(CStr(Trim(EcNumReq.text))) & _
                        " AND cod_perfil = " & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & _
                        " AND cod_ana_c = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & _
                        " AND cod_ana_s = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
            End If
            BG_ExecutaQuery_ADO sql
        End If

        '----------------
        If InStr(1, analises, MaReq(i).cod_agrup) = 0 Then
            analises = analises & IIf(BL_HandleNull(MaReq(i).cod_agrup, "") <> "", BL_TrataStringParaBD(MaReq(i).cod_agrup) & ",", "")
        End If
        
    Next i

    Set RsOrd = Nothing
    
    'NELSONPSILVA 30.04.2019 Glintt-HS-21232
    If gAtivaESP = mediSim Then
        Call Atualiza_Sl_Credenciais
        If gSQLError <> 0 Then
            'Erro a actualizar as credenciais
            BG_Mensagem mediMsgBox, "Erro a actualizar as credenciais!", vbError, "ERRO"
            GoTo TrataErro
        End If
    End If
    '
    Call RegistaTubosEliminados
    Call RegistaEliminadas
    Call RegistaAcrescentadas
    'BRUNODSANTOS GLINTT-HS-15750 04.01.2018
    If gUsaEnvioPDS = mediSim Then
        Call VerificaAnalisesEviarPDS(EcNumReq.text)
    End If
    '
    If BL_RegistaReqEpisodio(EcNumReq.text) = False Then
        GoTo TrataErro
    End If
    Grava_Ana_Req = True
Exit Function
TrataErro:
    Grava_Ana_Req = False
    BG_LogFile_Erros "Grava_Ana_Req: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_Ana_Req"
    Exit Function
    Resume Next
End Function

Sub Grava_Prod_Req()
    On Error GoTo TrataErro
    
    Dim sql As String
    Dim i As Integer
    Dim MenorData As String
    Dim DataPrevistaAux As String
    Dim TotalRegistos As Long
    Dim Perguntou As Boolean
     
    Perguntou = False
    If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
        'determinar a menor data da lista de produtos
        MenorData = RegistosP(1).DtPrev
        For i = 1 To RegistosP.Count - 1
            If (RegistosP(i).DtChega) <> "" Then
                If DateValue(RegistosP(i).DtPrev) < DateValue(MenorData) And DateValue(RegistosP(i).DtChega) > DateValue(EcDataPrevista) Then MenorData = RegistosP(i).DtPrev
            End If
        Next i
        If DateValue(MenorData) > DateValue(EcDataPrevista.text) Then
            gMsgTitulo = "ATEN��O"
            gMsgMsg = "Existe um produto com data inferior � data prevista, deseja alterar a data prevista para " & MenorData & "?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                DataPrevistaAux = EcDataPrevista.text
                EcDataPrevista = DateValue(MenorData)
                sql = "UPDATE sl_requis SET dt_previ=" & BL_TrataDataParaBD(EcDataPrevista.text) & " WHERE dt_previ=" & BL_TrataDataParaBD(DataPrevistaAux) & " AND n_req=" & EcNumReq.text
                BG_ExecutaQuery_ADO sql, gConexao
                If gSQLError <> 0 Then Exit Sub
            End If
        End If
        
        TotalRegistos = RegistosP.Count - 1
        For i = 1 To TotalRegistos
            If Trim(RegistosP(i).CodProd) <> "" Then
                If RegistosP(i).DtChega = "" And EcDataChegada.text <> "" Then
                    'se a data de chegada estiver vazia vai preenche-la
                    If Flg_PreencherDtChega = True Then
                        RegistosP(i).DtChega = DateValue(EcDataChegada.text)
                        FgProd.TextMatrix(i, 5) = RegistosP(i).DtChega
                    ElseIf Perguntou = False Then
'                        If gLAB <> "BIO" Then
'                        If BG_Mensagem(mediMsgBox, "Deseja preencher a data de chegada dos produtos com a data '" & EcDataChegada.Text & "' ?", vbQuestion + vbYesNo, "Data Chegada") = vbYes Then
'                            RegistosP(i).DtChega = DateValue(EcDataChegada.Text)
'                            FgProd.TextMatrix(i, 5) = RegistosP(i).DtChega
'                            Flg_PreencherDtChega = True
'                        End If
                        Perguntou = True
'                        End If
                    End If
                End If
                sql = "INSERT INTO sl_req_prod(n_req,cod_prod,cod_especif,dt_previ,dt_chega, volume, obs_especif) VALUES(" & EcNumReq.text & _
                    "," & UCase(BL_TrataStringParaBD(RegistosP(i).CodProd)) & "," & UCase(BL_TrataStringParaBD(RegistosP(i).CodEspecif)) & "," & BL_TrataDataParaBD(RegistosP(i).DtPrev) & "," & BL_TrataDataParaBD(RegistosP(i).DtChega) & ", " & BL_TrataStringParaBD(RegistosP(i).Volume) & ", " & BL_TrataStringParaBD(RegistosP(i).ObsEspecif) & ")"
                BG_ExecutaQuery_ADO sql
                
                If gSQLError <> 0 Then
                    i = TotalRegistos + 1
                End If
            End If
        Next i
    
        If gSQLError <> 0 Then Exit Sub
        
        'Actualizar o estado da requisi��o se foi introduzida data de chegada (dt_chega)
        If Flg_PreencherDtChega Then
            sql = "UPDATE sl_requis SET estado_req='A' WHERE n_req=" & EcNumReq.text
            BG_ExecutaQuery_ADO sql
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_Prod_Req: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_Prod_Req"
    Exit Sub
    Resume Next
End Sub


Sub Grava_DS_TM()
    On Error GoTo TrataErro
    
    If gLAB = "CITO" Then
        Exit Sub
    Else
        Dim sql As String
    
        'Faz o UPDATE na tabela diagn�sticos secund�rios criados temporariamente com o NumeroSessao
        sql = "UPDATE sl_diag_sec SET n_req=" & CLng(EcNumReq.text) & " WHERE n_req= -" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        If gSQLError = 0 Then
            'Faz o UPDATE na tabela terap�uticas e medica��o criados temporariamente com o NumeroSessao
            sql = "UPDATE sl_tm_ute SET n_req=" & CLng(EcNumReq.text) & " WHERE n_req= -" & gNumeroSessao
            BG_ExecutaQuery_ADO sql
        End If
        
        If gSQLError = 0 Then
            'Faz o UPDATE na tabela de motivos da requisi��o criados temporariamente com o NumeroSessao
            sql = "UPDATE sl_motivo_req SET n_req= " & CLng(EcNumReq.text) & " WHERE n_req= -" & gNumeroSessao
            BG_ExecutaQuery_ADO sql
        End If
        
        If gSQLError = 0 Then
            'Faz o UPDATE na tabela de m�dicos da requisi��o criados temporariamente com o NumeroSessao
            sql = "UPDATE sl_medicos_req SET n_req= " & CLng(EcNumReq.text) & " WHERE n_req= -" & gNumeroSessao
            BG_ExecutaQuery_ADO sql
        End If
        
        Select Case gLAB
            Case "HSMARIA", "BIO", "GM", "CHVNG"
                If gSQLError = 0 Then
                    'Faz o UPDATE � informa��o cl�nica da requisi��o criada temporariamete com o NumeroSessao
                    sql = "UPDATE sl_inf_clinica SET n_req = " & CLng(EcNumReq.text) & " WHERE n_req = -" & gNumeroSessao
                    BG_ExecutaQuery_ADO sql
                End If
        End Select
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_DS_TM: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_DS_TM"
    Exit Sub
    Resume Next
End Sub

Sub Apaga_DS_TM()
    On Error GoTo TrataErro
    
    If gLAB = "CITO" Or gLAB = "GM" Then
        Exit Sub
    Else
        Dim sql As String
    
        'Delete dos diagnosticos secund�rios criados temporariamente com o NumeroSessao
        sql = "DELETE FROM sl_diag_sec " & _
              "WHERE n_req=-" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        'BG_LogFile_Erros "FormGestaoRequisicaoPrivado: apagar diag. secund�rios -> (" & Sql & " ) ErrBD " & gSQLError
        
        'Delete das terap�uticas e medica��es criados temporariamente com o NumeroSessao
        sql = "DELETE FROM sl_tm_ute " & _
              "WHERE n_req=-" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        'Delete dos motivos da requisi��o criados temporariamente com o NumeroSessao
        sql = "DELETE FROM sl_motivo_req " & _
              "WHERE n_req=-" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        'Delete dos medicos da requisi��o criados temporariamente com o NumeroSessao
        sql = "DELETE FROM sl_medicos_req " & _
              "WHERE n_req=-" & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        
        Select Case gLAB
            Case "HSMARIA", "BIO", "GM", "CHVNG"
            'Delete da informa��o clinica da requisi��o criados temporariamente com o NumeroSessao
            sql = "DELETE FROM sl_inf_clinica " & _
                  "WHERE n_req=-" & gNumeroSessao
            BG_ExecutaQuery_ADO sql
        End Select
        
        ' BG_LogFile_Erros "FormGestaoRequisicaoPrivado: apagar terap./medica��o -> (" & Sql & " ) ErrBD " & gSQLError
    End If
        
Exit Sub
TrataErro:
    BG_LogFile_Erros "Apaga_DS_TM: " & Err.Number & " - " & Err.Description, Me.Name, "Apaga_DS_TM"
    Exit Sub
    Resume Next
End Sub

Public Sub FuncaoModificar(Optional not_msg As Boolean)
     On Error GoTo TrataErro
   
    Dim iRes As Integer
    Dim i As Integer
    Dim TotalReg As Long
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    
    If ValidaDadosFactARS = False Then
        Exit Sub
    End If
    
    If lObrigaInfoCli = True Then
        If BL_ValidaDadosInfoCli(EcSeqUtente.text, EcNumReq.text, "I") = False Then
            MsgBox "An�lises marcadas exigem informa��o cl�nica.", vbExclamation, " Requisi��o"
            Exit Sub
        End If
    End If
    If lObrigaTerap = True Then
        If BL_ValidaDadosInfoCli(EcSeqUtente.text, EcNumReq.text, "T") = False Then
            MsgBox "An�lises marcadas exigem terap�utica/medica��o.", vbExclamation, " Requisi��o"
            Exit Sub
        End If
    End If
    
    ' Obriga a especifica��o de produtos.
    If (gObrigaProduto) Then
        If Not (ValidaProdutos) Then
            MsgBox "N�o est� especificado nenhum produto.    " & vbCrLf & _
                "A modifica��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
            Exit Sub
        End If
    End If
    If Len(EcGrupoAna.text) = 1 Then
        If ProdutoUnico(EcGrupoAna.text) And RegistosP.Count - 1 > 1 Then
            MsgBox "Esta requisi��o s� pode ter um produto associado. " & vbCrLf & _
                "A inser��o n�o foi efectuada. ", vbExclamation, " Controlo dos Produtos "
            Exit Sub
        End If
    End If
    
    If EcCodEFR <> "" Then
        If BL_VerificaEfrValida(EcCodEFR, CStr(gCodLocal)) = False Then
            EcCodEFR.text = ""
            EcDescrEFR = ""
            EcCodEFR.SetFocus
            Exit Sub
        End If
    Else
        BG_Mensagem mediMsgBox, "Tem de indicar uma entidade fin.", vbInformation, "Aten��o"
        Sendkeys ("{HOME}+{END}")
        EcCodEFR = ""
        EcCodEFR.SetFocus
        Exit Sub
    End If
    
    ' --------------------------------------------------------------------------------------------------------------
    ' SE ENTROU NO ECRA PARA FIM DE SEMANA - REQUISICOES TEM OBRIGATORIAMENTE DE TER DATA DE FIM DE SEMANA
    ' --------------------------------------------------------------------------------------------------------------
    If Req_FDS = True Then
        If BG_Mensagem(mediMsgBox, "A data de chegada tem de ser um Domingo. A data indicada � um feriado ou s�bado? ", vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo) = vbYes Then
            'nada
        Else
            Sendkeys ("{HOME}+{END}")
            EcDataChegada.text = ""
            EcDataChegada.SetFocus
            Exit Sub
        End If

    End If
    
    If gPreencheDatasReq = mediSim Then
        If EcDataPrevista.text = "" Then
            EcDataPrevista.text = dataAct
        End If
        If EcDataChegada.text = "" Then
            EcDataChegada.text = dataAct
        End If
    End If
    
    If EcCodProveniencia <> "" Then
        If BL_HandleNull(BL_SelCodigo("SL_PROVEN", "flg_invisivel", "cod_proven", EcCodProveniencia, "V"), 0) = 1 Then
            BG_Mensagem mediMsgBox, "Proveni�ncia desactivada. ", vbInformation, "Aten��o"
            EcCodProveniencia.text = ""
            EcDescrProveniencia = ""
            EcCodProveniencia.SetFocus
            Exit Sub
        End If
    End If
    
    If not_msg = True Then
        gMsgResp = vbYes
    Else
        gMsgTitulo = "Modificar"
        gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    End If
    
    If gMsgResp = vbYes Then
    
        'Impedir que se alterem valores de cria��o / impress�o de registos
        EcDataCriacao = BL_HandleNull(rs!dt_cri, "")
        EcUtilizadorCriacao = BL_HandleNull(rs!user_cri, "")
        EcHoraCriacao = BL_HandleNull(rs!hr_cri, "")
        
        EcDataImpressao = BL_HandleNull(rs!dt_imp, "")
        EcUtilizadorImpressao = BL_HandleNull(rs!user_imp, "")
        EcHoraImpressao = BL_HandleNull(rs!hr_imp, "")
        
        EcDataImpressao2 = BL_HandleNull(rs!dt_imp2, "")
        EcUtilizadorImpressao2 = BL_HandleNull(rs!user_imp2, "")
        EcHoraImpressao2 = BL_HandleNull(rs!hr_imp2, "")
        
        EcDataAlteracao = dataAct
        EcUtilizadorAlteracao = gCodUtilizador
        EcHoraAlteracao = Bg_DaHora_ADO
        
        'se a data prevista estiver vazia
        If EcDataPrevista.text = "" Then
            EcDataPrevista.text = Format(dataAct, gFormatoData)
        End If
        
        
        
        If UBound(MaReq) > 0 Then
            If Trim(MaReq(1).cod_ana_s) <> "" Then
                If RegistosP.Count > 0 And Trim(RegistosP(1).CodProd) <> "" Then
                    EcEstadoReq = EcEstadoReq
                ElseIf EcEstadoReq = gEstadoReqEsperaProduto Then
                    EcEstadoReq = gEstadoReqEsperaResultados
                End If
            Else
                EcEstadoReq = gEstadoReqSemAnalises
            End If
        Else
            EcEstadoReq = gEstadoReqSemAnalises
        End If
        EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        
        'Se o utente � isento, � obrigat�rio indicar o motivo
        If CbTipoIsencao.ListIndex = -1 Then
            BG_Mensagem mediMsgBox, "Tipo de isen��o obrigat�rio!", vbOKOnly + vbInformation, "Insen��es"
            CbTipoIsencao.SetFocus
            Exit Sub
        End If
        
        
        If Not ValidaEspecifObrig Then Exit Sub
                
        EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
        
        If CbHemodialise.ListIndex <> mediComboValorNull Then
            VerificaDoenteHemodialise
        End If
        
        'NELSONPSILVA LJMANSO-336 06.12.2018
        If ValidaCodPostalFact = False Then
            If CbCodUrbano.ListIndex <> -1 Then
                BG_Mensagem mediMsgBox, "Insira C�digo Postal com 7 Digitos!", vbInformation, "C�digo Postal"
                Exit Sub
            Else
                BG_Mensagem mediMsgBox, "Insira C�digo Postal com 7 Digitos!", vbInformation, "C�digo Postal"
                EcCodPostal.text = ""
                EcRuaPostal.text = ""
                EcDescrPostal.text = ""
            End If
        End If
        
        If ValidaNumBenef = True Then
            iRes = ValidaCamposEc
            If iRes = True Then
            
                Call BD_Update
                If Trim(EcNumReq.text) <> "" Then
                    'Se n�o houve erro
                    If gRequisicaoActiva = 0 Then
                    Else
                        gRequisicaoActiva = EcNumReq.text
                    End If
                    
                    RegistaAcrescentadas
                    'BRUNODSANTOS GLINTT-HS-15750 04.01.2018
                    If gUsaEnvioPDS = mediSim Then
                        Call VerificaAnalisesEviarPDS(EcNumReq.text)
                    End If
                    '
                    RegistaAlteracoesRequis EcNumReq
                End If
    End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoModificar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoModificar"
    Exit Sub
    Resume Next
End Sub

Function ValidaNumBenef() As Boolean
    On Error GoTo TrataErro

    Dim Tabela As adodb.recordset
    Dim sql As String
    Dim Formato As Boolean
    
    If EcCodEFR.text <> "" Then
        Set Tabela = New adodb.recordset
        Tabela.CursorLocation = adUseServer
        Tabela.CursorType = adOpenStatic
        sql = "SELECT sigla,formato1,formato2 FROM sl_efr WHERE cod_efr=" & EcCodEFR.text
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
        If Tabela.RecordCount > 0 Then
            Formato1 = BL_HandleNull(Tabela!Formato1, "")
            Formato2 = BL_HandleNull(Tabela!Formato2, "")
            'SendKeys ("{END}")
        End If
        Formato = Verifica_Formato(UCase(EcNumBenef.text), Formato1, Formato2)
        If Formato = False And EcNumBenef.text <> "" Then
            'SendKeys ("{END}")
            EcNumBenef.SetFocus
            ValidaNumBenef = False
        Else
            ValidaNumBenef = True
        End If
    Else
        ValidaNumBenef = True
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaNumBenef: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaNumBenef"
    ValidaNumBenef = False
    Exit Function
    Resume Next
End Function

Function Verifica_Formato(NumBenef As String, Formato1 As String, Formato2 As String) As Boolean
    On Error GoTo TrataErro
    
    If Trim(Formato1) = "" And Trim(Formato2) = "" Then
        Verifica_Formato = True
        Exit Function
    End If
    
    If Trim(Formato1) = "" Then
        Verifica_Formato = False
    End If
    If Len(NumBenef) <> Len(Formato1) Then
        Verifica_Formato = False
    Else
        Verifica_Formato = Percorre_Formato(NumBenef, Formato1)
    End If
    
    'caso o formato 1 n�o esteja correcto vai verificar o formato 2
    If Verifica_Formato = False Then
        If Trim(Formato2) = "" Then
            Verifica_Formato = False
        End If
        If Len(NumBenef) <> Len(Formato2) Then
            Verifica_Formato = False
        Else
            Verifica_Formato = Percorre_Formato(NumBenef, Formato2)
        End If
        If Verifica_Formato = False And EcNumBenef.text <> "" Then
            gMsgTitulo = "Aten��o"
            gMsgMsg = "N�mero Benefici�rio incorrecto." & Chr(13) & "Deseja continuar ?" & Chr(13) & Chr(13) & "Exemplo de formato(s) correcto(s): " & Chr(13) & "       " & Formato1 & IIf(Trim(Formato1) <> "" And Trim(Formato2) <> "", ", ", "") & Formato2
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbInformation, gMsgTitulo)
            If gMsgResp = vbYes Then
                Verifica_Formato = True
            Else
                Verifica_Formato = False
            End If
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Formato: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Formato"
    Verifica_Formato = False
    Exit Function
    Resume Next
End Function

Function Percorre_Formato(NumBenef As String, Formato As String) As Boolean
     On Error GoTo TrataErro
   
    Dim pos As Integer
    Dim comp As Integer
    Dim CharActualF As String
    Dim CharActualN As String
    
    Percorre_Formato = True
    comp = Len(Formato)
    pos = 1
    Do While pos <= comp
        CharActualF = Mid(Formato, pos, 1)
        CharActualN = Mid(NumBenef, pos, 1)
        Select Case CharActualF
            Case "A"
                If CharActualN < "A" Or CharActualN > "Z" Then
                    Percorre_Formato = False
                End If
            Case "9"
                If CharActualN < "0" Or CharActualN > "9" Then
                    Percorre_Formato = False
                End If
            Case " "
                If CharActualN <> " " Then
                    Percorre_Formato = False
                End If
        End Select
        pos = pos + 1
    Loop

Exit Function
TrataErro:
    BG_LogFile_Erros "Percorre_Formato: " & Err.Number & " - " & Err.Description, Me.Name, "Percorre_Formato"
    Percorre_Formato = False
    Exit Function
    Resume Next
End Function

Sub BD_Update()
   
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim k As Integer
    Dim ProdJaChegou As Boolean
    'Dim convencao As Integer
    'edgar.parada Glintt-HS-19165
    Dim countCredESP As Integer
    '
    On Error GoTo Trata_Erro
    
    gSQLError = 0
    gSQLISAM = 0

    'edgar.parada Glintt-HS-19165
    countCredESP = 0
    'If UBound(estrutOperation) >= 0 Then
    If gPassaRecFactus = mediSim Then
       'LJMANSO-301
        AtualizaCodPostalFact
           'edgar.parada LJMANSO-351
       If FACTUS_ValidaRegras(RequisicaoESP(EcNumReq.text), countCredESP) = False Then
          Exit Sub
       End If
    End If
    'End If
    '
    BG_BeginTransaction
        
    
    'actualizar os produtos da requisi��o
    UPDATE_Prod_Req
    If gSQLError <> 0 Then
        'Erro a actualizar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar os produtos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'actualizar os tubos da requisi��o
    UPDATE_Tubos_Req
    If gSQLError <> 0 Then
        'Erro a actualizar os produtos da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar os tubos da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
            
    
    'actualizar as obs das analises
    Grava_ObsAnaReq
    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a actualizar as observa��es das an�lises !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'actualizar os recibos da requisi��o
    
    
    
    If gPassaRecFactus <> mediSim Then
        If UPDATE_Recibos_Req = False Then
            'Erro a actualizar os produtos da requisi��o
            BG_Mensagem mediMsgBox, "Erro a actualizar os recibos da requisi��o !", vbError, "ERRO"
            GoTo Trata_Erro
        End If
        
        ' ADIANTAMENTOS DA REQUISICAO
        If RECIBO_GravaAdiantamento(EcNumReq, RegistosAD) = False Then
            'Erro a gravar os tubos da requisi��o
            BG_Mensagem mediMsgBox, "Erro a gravar os adiantamentos da requisi��o !", vbError, "ERRO"
            GoTo Trata_Erro
        End If
    Else
        AtualizaCodPostalFact
        If CbCodUrbano.ListIndex > mediComboValorNull And gPassaRecFactus = mediSim Then
            FACTUS_AtualizaDom CbCodUrbano.ItemData(CbCodUrbano.ListIndex), BL_HandleNull(EcKm.text, 0)
        Else
            FACTUS_AtualizaDom 0, 0
        End If
        FACTUS_GravaDados EcNumReq.text, mediComboValorNull
        GravaDadosAnaFact
    End If
             
    'actualizar as an�lises da requisi��o
    If UPDATE_Ana_Req = False Then
        'Erro a actualizar as an�lises da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar as an�lises da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
                                        
    'actualizar as terap�uticas e medica��es da requisi��o
    Grava_DS_TM
    If gSQLError <> 0 Then
        'Erro a actualizar as terap�uticas e medica��es da requisi��o
        BG_Mensagem mediMsgBox, "Erro a actualizar as terap�uticas e medica��es da requisi��o !", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    ' Grava informacoes das analises
    If GravaInfoAna(EcNumReq) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar informa��es das an�lises!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'Grava questoes das analises
    If GravaQuestoesAna(EcNumReq) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar Quest�es das an�lises!", vbError, "ERRO"
        GoTo Trata_Erro
    End If
    
    'GRAVA PERFIS MARCADOS
    If PM_GravaPerfisMarcados(EcNumReq.text) = False Then
        BG_Mensagem mediMsgBox, "Erro ao gravar perfis marcados", vbError, "ERRO"
        GoTo Trata_Erro
    End If
        
    If EcDataChegada.text <> "" Then
        If EcHoraChegada.text = "" Then
            EcHoraChegada.text = Bg_DaHora_ADO
        End If
    Else
        EcHoraChegada.text = ""
    End If
   
    EcDtEntrega = BL_VerificaConclusaoRequisicao(EcNumReq, EcDataPrevista.text)
    EcEstadoReq.text = BL_MudaEstadoReq(CLng(EcNumReq))

    'gravar restantes dados da requisicao
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    ' PFerreira 16.03.2007 -- Insere utente na fila de espera
    'If (IsToInsert) Then: InsertFilaEspera
    
   
    If gSQLError <> 0 Then
        GoTo Trata_Erro
    Else
        Flg_Gravou = True
        gRequisicaoActiva = CLng(EcNumReq)
        BG_CommitTransaction
        
        EcEstadoReq.text = BL_MudaEstadoReq(CLng(EcNumReq))
        
        If (EcEstadoReq.text = gEstadoReqSemAnalises) Then
            Me.EcDataPrevista.Enabled = True
            Me.EcDataChegada.Enabled = True
        Else
            Me.EcDataPrevista.Enabled = False
            Me.EcDataChegada.Enabled = False
        End If
        
        EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)

    End If
    
    MarcaLocal = rs.Bookmark
    rs.Requery
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    rs.Bookmark = MarcaLocal
    
    'CbSituacao.SetFocus
    EcNumReq.SetFocus
    Exit Sub
    
Trata_Erro:
    'edgar.parada Glintt-HS-19165 * tratar exception
    'If Err.Number = 9 Then
    '  countCredESP = -1
    '  Resume Next
    'End If
    '
    BG_Mensagem mediMsgBox, "Erro a modificar requisi��o!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormGestaoRequisicaoPrivado: BD_Update -> " & Err.Description
    BG_RollbackTransaction
    gRequisicaoActiva = 0
    FuncaoLimpar
    Resume Next

End Sub



Sub UPDATE_Prod_Req()
    On Error GoTo TrataErro
   Dim sql As String
    
   gSQLError = 0
   sql = "DELETE FROM sl_req_prod WHERE n_req=" & Trim(EcNumReq.text)
   BG_ExecutaQuery_ADO sql
   
   If gSQLError <> 0 Then
        Exit Sub
   End If
   Grava_Prod_Req
Exit Sub
TrataErro:
    BG_LogFile_Erros "UPDATE_Prod_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Prod_Req"
    Exit Sub
    Resume Next
End Sub
Sub UPDATE_Tubos_Req()
    On Error GoTo TrataErro
   'RGONCALVES 15.12.2014 - adicionado parametro dt_chega
   TB_GravaTubosReq EcNumReq.text, EcDataChegada.text
Exit Sub
TrataErro:
    BG_LogFile_Erros "UPDATE_Tubos_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Tubos_Req"
    Exit Sub
    Resume Next
End Sub

Function UPDATE_Recibos_Req() As Boolean
     On Error GoTo TrataErro
     UPDATE_Recibos_Req = RECIBO_GravaRecibo(EcNumReq, RegistosA, RegistosR, RegistosRM, False)
Exit Function
TrataErro:
    UPDATE_Recibos_Req = False
    BG_LogFile_Erros "UPDATE_Recibos_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Recibos_Req"
    Exit Function
    Resume Next
End Function

Private Function UPDATE_Ana_Req() As Boolean
    On Error GoTo TrataErro
    
    Dim sql As String
    UPDATE_Ana_Req = False
    ActualizaFlgAparTrans
    gSQLError = 0
    'Apagar todas as requisi��es que n�o t�m dt_chega
    sql = "DELETE FROM sl_marcacoes WHERE n_req=" & Trim(EcNumReq.text)
    BG_ExecutaQuery_ADO sql
    
    If gSQLError <> 0 Then
        Exit Function
    End If

    EcGrupoAna.text = ""
    
    If Grava_Ana_Req = False Then
        Exit Function
    End If
    
    UPDATE_Ana_Req = True
Exit Function
TrataErro:
    UPDATE_Ana_Req = False
    BG_LogFile_Erros "UPDATE_Ana_Req: " & Err.Number & " - " & Err.Description, Me.Name, "UPDATE_Ana_Req"
    Exit Function
    Resume Next
End Function

Sub FuncaoRemover()
    On Error GoTo TrataErro
    Dim i As Integer
    
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    For i = 1 To RegistosR.Count
        If RegistosR(i).NumDoc <> "0" And RegistosR(i).NumDoc <> "" Then
            If RegistosR(i).Estado = gEstadoReciboPago Then
                Call BG_Mensagem(mediMsgBox, "N�o pode cancelar requisi��es com recibos emitidos!", vbExclamation + vbOKOnly, App.ProductName)
                Exit Sub
            End If
        End If
    Next
    If Trim(EcEstadoReq) <> Trim(gEstadoReqSemAnalises) And EcEstadoReq <> gEstadoReqEsperaProduto And EcEstadoReq <> gEstadoReqEsperaResultados Then
        Call BG_Mensagem(mediMsgBox, "N�o pode cancelar requisi��es com resultados!", vbExclamation + vbOKOnly, App.ProductName)
        Exit Sub
    End If
    
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Max = UBound(gFieldObjectProperties)

    ' Guardar as propriedades dos campos do form
    For Ind = 0 To Max
        ReDim Preserve FOPropertiesTemp(Ind)
        FOPropertiesTemp(Ind).EscalaNumerica = gFieldObjectProperties(Ind).EscalaNumerica
        FOPropertiesTemp(Ind).TamanhoConteudo = gFieldObjectProperties(Ind).TamanhoConteudo
        FOPropertiesTemp(Ind).nome = gFieldObjectProperties(Ind).nome
        FOPropertiesTemp(Ind).Precisao = gFieldObjectProperties(Ind).Precisao
        FOPropertiesTemp(Ind).TamanhoCampo = gFieldObjectProperties(Ind).TamanhoCampo
        FOPropertiesTemp(Ind).tipo = gFieldObjectProperties(Ind).tipo
    Next Ind

    MarcaLocal = rs.Bookmark
    
    FormGestaoRequisicaoPrivado.Enabled = False
    
    FormCancelarRequisicoes.Show
    
    SSTGestReq.TabEnabled(1) = False
    SSTGestReq.TabEnabled(2) = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabEnabled(5) = False
    SSTGestReq.TabEnabled(6) = False
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoRemover: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoRemover"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoProcurar()
          
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    On Error GoTo Trata_Erro
    
    
    If Req_FDS = True Then
        EcFimSemana = "1"
    Else
        EcFimSemana = "0"
    End If
    
    Set rs = New adodb.recordset
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    CriterioTabela = Replace(CriterioTabela, "seq_utente = '(", "seq_utente IN (")
    CriterioTabela = Replace(CriterioTabela, "'(", "(")
    CriterioTabela = Replace(CriterioTabela, ")'", ")")
    If SelTotal = True Then
        If BG_Mensagem(mediMsgBox, "N�o colocou crit�rios de pesquisa, esta opera��o pode demorar algum tempo." & vbNewLine & "Confirma a selec��o de todos as requisi��es ?", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
            Set rs = Nothing
            Exit Sub
        End If
    Else
    End If
    
    If Req_FDS = True Then
        EcFimSemana = "1"
    Else
        EcFimSemana = "0"
    End If
                   

    CriterioTabela = CriterioTabela & "  ORDER BY dt_chega DESC, n_req DESC"
              
    BL_InicioProcessamento Me, "A pesquisar registos."
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    rs.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioTabela
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbInformation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        BL_ToolbarEstadoN Estado
        
        'inicializa��es da lista de marca��es de an�lises
        Inicializa_Estrutura_Analises
        
        'limpar grids
        LimpaFGAna
        
        LimpaFgProd
        LimpaFgTubos
        LimpaFgRecibos
        LimpaFgAdiantamentos
        
        IndiceHistorico = 0
        numHistoricos = 0
        ConsultaHistoricoAlteracoes 0
        Call PreencheCampos(True)
        
        
        Set CampoDeFocus = EcDataPrevista
        

        BL_FimProcessamento Me
        gRequisicaoActiva = CLng(EcNumReq.text)

        If rs!estado_req = gEstadoReqSemAnalises Or EcDataChegada.text = "" Then
            Me.EcDataPrevista.Enabled = True
            Me.EcDataChegada.Enabled = True
        Else
            Me.EcDataChegada.Enabled = False
            Me.EcDataPrevista.Enabled = False
        End If

    End If
    
    FGAna.Refresh
    'edgar.parada Glintt-HS-19165 * bloquear campos se for credencial importada
    If RequisicaoESP(EcNumReq.text) Then
       BloqueiaCampos
       btCativarReq.Enabled = False
       If BG_DaComboSel(CbTipoIsencao) = gTipoIsencaoIsento Then
           BtGerarRecibo.Visible = False
       Else
           BtGerarRecibo.Visible = True
           BtGerarRecibo.Enabled = False
       End If
    End If
    '
    'NELSONPSILVA Glintt-HS-21031 * bloquear campos se for credencial importada
    If RequisicaoESP(EcNumReq.text) Then
        If (rs!estado_req = gEstadoReqEsperaResultados Or rs!estado_req = gEstadoReqResultadosParciais Or rs!estado_req = gEstadoReqImpressaoParcial _
         Or rs!estado_req = gEstadoReqValidacaoMedicaParcial Or rs!estado_req = gEstadoReqTodasImpressas Or rs!estado_req = gEstadoReqValicacaoMedicaCompleta Or rs!estado_req = gEstadoReqEsperaValidacao) Then
            CbCodUrbano.Enabled = False
            EcKm.Enabled = False
            EcCodPostal.Enabled = False
            EcRuaPostal.Enabled = False
            EcDescrPostal.Enabled = False
            BtPesqCodPostal.Enabled = False
        End If
    End If
    
    Exit Sub
    
Trata_Erro:
    
    BG_LogFile_Erros Me.Name & ": FuncaoProcurar (" & Err.Number & "-" & Err.Description & ")"
    Err.Clear
    BL_FimProcessamento Me
    BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbInformation, "Procurar"
    FuncaoLimpar
    Resume Next

End Sub

Sub Funcao_CopiaReq()
    On Error GoTo TrataErro
    
    If gRequisicaoActiva <> 0 Then
        EcNumReq = gRequisicaoActiva
        FuncaoProcurar
    Else
        Beep
        BG_Mensagem mediMsgStatus, "N�o existe requisi��o activa!", , "Copiar requisi��o"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_CopiaReq: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_CopiaReq"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoLimpar()
     On Error GoTo TrataErro
   
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        gMsgTitulo = "Limpar"
        gMsgMsg = "Tem a certeza que quer LIMPAR o ecr�?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            LimpaCampos
            EcUtente.text = ""
            DoEvents
            
            CampoDeFocus.SetFocus
        End If
    End If
    
    'BRUNODSANTOS 09.06.2017 LRV-469
    If gCodSalaDefeito <> "-1" Then
        EcCodSala.text = codSalaAux
    End If
    btCativarReq.Enabled = True
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoLimpar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoLimpar"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoEstadoAnterior()
    On Error GoTo TrataErro
    
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoEstadoAnterior: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoEstadoAnterior"
    Exit Sub
    Resume Next
End Sub

Function ValidaCamposEc() As Integer
    On Error GoTo TrataErro
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaCamposEc: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaCamposEc"
    ValidaCamposEc = -1
    Exit Function
    Resume Next
End Function

Sub LimpaCampos()
    On Error GoTo TrataErro
    
    Me.SetFocus

    
    BG_LimpaCampo_Todos CamposEc
    EcDescrEstado.BackColor = 14737632
    IndiceHistorico = 0
    numHistoricos = 0
    BtHistAnt.Enabled = False
    BtHistSeg.Enabled = False
    ReDim fa_movi_resp(0)
    fa_movi_resp_tot = 0
    ReDim AnalisesFact(0)
    TotalAnalisesFact = 0
    ReDim docFinanceiros(0)
    totalDodFinanceiros = 0

    'colocar o bot�o de cancelar requisi��es invisivel
    BtCancelarReq.Visible = False
    flg_novaArs = -1
    EcNumColheita.text = ""
    LbNomeUtilAlteracao.caption = ""
    LbNomeUtilAlteracao.ToolTipText = ""
    LbNomeUtilCriacao.caption = ""
    LbNomeUtilCriacao.ToolTipText = ""
    LbNomeUtilImpressao.caption = ""
    LbNomeUtilImpressao.ToolTipText = ""
    LbNomeUtilImpressao2.caption = ""
    LbNomeUtilImpressao2.ToolTipText = ""
    LbNomeUtilFecho.caption = ""
    LbNomeUtilFecho.ToolTipText = ""
    LbNomeLocal.caption = ""
    LbNomeLocal.ToolTipText = ""
    EcUtilNomeConf = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    EcDataNasc.text = ""
    EcSexo = ""
    EcIdade.text = ""
    EcNome.text = ""
    EcUtente.text = ""
    EcDescrEFR.text = ""
    EcDescrMedico = ""
    EcNomeMedico = ""
    EcPesqRapProven.text = ""
    EcPesqRapProduto.text = ""
    EcPesqRapTubo.text = ""
    EcDataNasc.text = ""
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    FrBtTubos.Enabled = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabEnabled(5) = False
    SSTGestReq.TabEnabled(6) = False
    EcCodEFR.locked = False
    EcNumBenef.locked = False
    BtPesquisaEntFin.Enabled = True
    EcCodEFR.ToolTipText = ""
    EcNumBenef.ToolTipText = ""
    BtPesquisaEntFin.ToolTipText = ""
    EcCodEFR.ForeColor = vbBlack
    EcNumBenef.ForeColor = vbBlack
    CbTipoIsencao.Enabled = True
    CbPrioColheita.ListIndex = mediComboValorNull
    CbSala.ListIndex = mediComboValorNull
    CbHemodialise.ListIndex = mediComboValorNull
    CbHemodialise.Enabled = False
    CbDestino.ListIndex = mediComboValorNull
    CbDestino.ListIndex = mediComboValorNull
    EcCodEfr2 = ""
    EcDescrEfr2 = ""
    EcMorada.text = ""
    EcCodPostal.text = ""
    EcRuaPostal.text = ""
    EcDescrPostal.text = ""
    EcObsAna.text = ""
    FrameObsAna.Visible = False
    FrameAjuda.Visible = False
    EcDescrProveniencia = ""
    EcDescrSala.text = ""
    EcDescrLocalEntrega.text = ""
    EcDescrPais = ""
    'Fun��o que limpa os produtos da requisi��o
    LaProdutos.caption = ""
    EcDescrEstado = ""
    'Fun��o que limpa os tubos da requisi��o
    LaTubos.caption = ""
    
    CodEfrAUX = ""
    'edgar.parada Glintt-HS-19577 19.08.2019
    efrDescrAUX = ""
    '
    'inicializa��es da lista de marca��es de an�lises
    Inicializa_Estrutura_Analises
     
    'Limpar treeview
    TAna.Nodes.Clear
         
    'colocar cabe�alho e data prevista enabled a false
'    EcNumReq.Enabled = True
    EcNumReq.locked = False
    EcNumReqAssoc.locked = False
    
    CbTipoUtente.Enabled = True
    EcUtente.Enabled = True
    EcDataPrevista.Enabled = True
    EcDataChegada.Enabled = True
    
    BtInfClinica.Enabled = False
    BtDomicilio.Enabled = False
    BtDigitalizacao.Enabled = False
    BtConsReq.Enabled = False
    Set CampoDeFocus = EcNumReq
    
    gFlg_JaExisteReq = False
    Flg_LimpaCampos = True

    BtPesquisaEspecif.Enabled = False
    BtPesquisaProduto.Enabled = False
    BtNotas.Enabled = False
    BtNotasVRM.Enabled = False
    BtFichaCruzada.Enabled = False
    
    BtResumo.Enabled = False
    LimpaFgProd
    LimpaFgTubos
    LimpaFGAna
    
    LimpaFgRecibos
    LimpaFgAdiantamentos
    LimpaFgRecAna
    LimpaFgInfo
    FrameAnaRecibos.Visible = False
    SSTGestReq.Tab = 0
    DoEvents

    
    CbTipoIsencao.ListIndex = mediComboValorNull
    
    
    Call Apaga_DS_TM
    
    
    
    Flg_DadosAnteriores = False
    
    SSTGestReq.Enabled = True
    
    TotalEliminadas = 0
    ReDim Eliminadas(0)
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
    
    FrameDomicilio.Visible = False
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    PreencheSalaDefeito
    CkCobrarDomicilio.value = vbGrayed
    BtResumo.Enabled = False
    
    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)
    CkAvisarSMS.value = vbGrayed
    flgEmissaoRecibos = False
    GereFrameAdiantamentos False
        
    flg_ReqBloqueada = False
    
    
    ' pferreira 2011.05.31
    CkReqHipocoagulados.value = vbGrayed
    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    EcUtilNomeEmail = ""
    EcUtilNomeSMS = ""
    PM_LimpaEstrutura
    TAna.Visible = False
    lObrigaTerap = False
    lObrigaInfoCli = False
    
    'LJMANSO-302
    'AuxRecVal = ""
    colRec = ""
    '
    'edgar.parada Glintt-HS-19165
    ReDim estrutOperation(0)
    '
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaCampos: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaCampos"
    Exit Sub
    Resume Next
End Sub

Sub LimpaCampos2()
    On Error GoTo TrataErro
    
    Me.SetFocus

    
    BG_LimpaCampo_Todos CamposEc
    EcDescrEstado.BackColor = 14737632
    
    ReDim fa_movi_resp(0)
    fa_movi_resp_tot = 0
    ReDim AnalisesFact(0)
    TotalAnalisesFact = 0
    ReDim docFinanceiros(0)
    totalDodFinanceiros = 0
    
    IndiceHistorico = 0
    numHistoricos = 0
    BtHistAnt.Enabled = False
    BtHistSeg.Enabled = False
    'colocar o bot�o de cancelar requisi��es invisivel
    EcNumColheita.text = ""
    BtCancelarReq.Visible = False
    flg_novaArs = -1
    LbNomeUtilAlteracao.caption = ""
    LbNomeUtilAlteracao.ToolTipText = ""
    LbNomeUtilCriacao.caption = ""
    LbNomeUtilCriacao.ToolTipText = ""
    LbNomeUtilImpressao.caption = ""
    LbNomeUtilImpressao.ToolTipText = ""
    LbNomeUtilImpressao2.caption = ""
    LbNomeUtilImpressao2.ToolTipText = ""
    LbNomeUtilFecho.caption = ""
    LbNomeUtilFecho.ToolTipText = ""
    LbNomeLocal.caption = ""
    LbNomeLocal.ToolTipText = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    EcDataNasc.text = ""
    EcSexo = ""
    EcIdade.text = ""
    EcNome.text = ""
    EcUtente.text = ""
    EcDescrEFR.text = ""
    EcDescrMedico = ""
    EcPesqRapProven.text = ""
    EcPesqRapProduto.text = ""
    EcDataNasc.text = ""
    SSTGestReq.TabEnabled(1) = False
    FrProdutos.Enabled = False
    FrBtProd.Enabled = False
    SSTGestReq.TabEnabled(2) = False
    FrBtTubos.Enabled = False
    SSTGestReq.TabEnabled(3) = False
    SSTGestReq.TabEnabled(4) = False
    SSTGestReq.TabEnabled(5) = False
    SSTGestReq.TabEnabled(6) = False
    EcCodEFR.locked = False
    EcNumBenef.locked = False
    BtPesquisaEntFin.Enabled = True
    EcCodEFR.ToolTipText = ""
    EcNumBenef.ToolTipText = ""
    BtPesquisaEntFin.ToolTipText = ""
    EcCodEFR.ForeColor = vbBlack
    EcNumBenef.ForeColor = vbBlack
    CbTipoIsencao.Enabled = True
    CbPrioColheita.ListIndex = mediComboValorNull
    CbSala.ListIndex = mediComboValorNull
    EcCodEfr2 = ""
    EcDescrEfr2 = ""
    EcMorada.text = ""
    EcCodPostal.text = ""
    EcRuaPostal.text = ""
    EcDescrPostal.text = ""
    CbDestino.ListIndex = mediComboValorNull
    EcObsAna.text = ""
    FrameObsAna.Visible = False
    FrameAjuda.Visible = False
    EcDescrProveniencia = ""
    EcDescrSala.text = ""
    EcDescrLocalEntrega.text = ""
    EcUtilNomeConf = ""
    EcDescrEstado = ""
    EcDescrPais = ""

    
    'Fun��o que limpa os produtos da requisi��o
    LaProdutos.caption = ""
    
    'inicializa��es da lista de marca��es de an�lises
    Inicializa_Estrutura_Analises
     
    'Limpar treeview
    TAna.Nodes.Clear
         
    'colocar cabe�alho e data prevista enabled a false
'    EcNumReq.Enabled = True
    EcNumReq.locked = False
    EcNumReqAssoc.locked = False
    
    CbTipoUtente.Enabled = True
    EcUtente.Enabled = True
    EcDataPrevista.Enabled = True
    EcDataChegada.Enabled = True
    
    BtInfClinica.Enabled = False
    BtDomicilio.Enabled = False
    BtDigitalizacao.Enabled = False
    
    BtConsReq.Enabled = False
    Set CampoDeFocus = EcNumReq
    
    gFlg_JaExisteReq = False
    Flg_LimpaCampos = True

    BtPesquisaEspecif.Enabled = False
    BtPesquisaProduto.Enabled = False
    
    LimpaFgProd
    LimpaFgTubos
    LimpaFGAna
    LimpaFgRecibos
    LimpaFgAdiantamentos
    LimpaFgInfo
    SSTGestReq.Tab = 0
    DoEvents
    
    
    Call Apaga_DS_TM
    
    
    Flg_DadosAnteriores = False
    
    TotalEliminadas = 0
    ReDim Eliminadas(0)
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
    
    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)
    
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    
    PreencheSalaDefeito
    CkAvisarSMS.value = vbGrayed
    flgEmissaoRecibos = False
    GereFrameAdiantamentos False

    CbHemodialise.Enabled = False
    flg_ReqBloqueada = False
    gTotalTubos = 0
    ReDim gEstruturaTubos(0)
    EcUtilNomeEmail = ""
    EcUtilNomeSMS = ""
    PM_LimpaEstrutura
    lObrigaTerap = False
    lObrigaInfoCli = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaCampos2: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaCampos2"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoAnterior()
    On Error GoTo TrataErro
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BG_MensagemAnterior
    Else
        
        Call LimpaCampos
        gFlg_JaExisteReq = True
        EcDataPrevista.Enabled = False
        Call PreencheCampos(True)
        
        'Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados
        If rs!estado_req = gEstadoReqCancelada Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabEnabled(5) = False
            SSTGestReq.TabEnabled(6) = False
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(3) = True
            SSTGestReq.TabEnabled(4) = True
            SSTGestReq.TabEnabled(5) = True
            SSTGestReq.TabEnabled(6) = True
            BtCancelarReq.Visible = False
        
        End If
    End If

'edgar.parada Glintt-HS-19165 * bloquear campos se for credencial importada
 If RequisicaoESP(EcNumReq.text) Then
       BloqueiaCampos
           If BG_DaComboSel(CbTipoIsencao) = gTipoIsencaoIsento Then
           BtGerarRecibo.Visible = False
       Else
           BtGerarRecibo.Visible = True
           BtGerarRecibo.Enabled = False
       End If
 End If
 '
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoAnterior: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoAnterior"
    Exit Sub
    Resume Next
End Sub

Sub FuncaoSeguinte()

    On Error GoTo TrataErro
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BG_MensagemSeguinte
    Else
        
        Call LimpaCampos
        gFlg_JaExisteReq = True
        EcDataPrevista.Enabled = False
        Call PreencheCampos(True)
        
        ' Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados.
        If rs!estado_req = gEstadoReqCancelada Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabEnabled(5) = False
            SSTGestReq.TabEnabled(6) = False
        Else
            SSTGestReq.TabEnabled(1) = True
            SSTGestReq.TabEnabled(2) = True
            SSTGestReq.TabEnabled(3) = True
            SSTGestReq.TabEnabled(4) = True
            SSTGestReq.TabEnabled(5) = True
            SSTGestReq.TabEnabled(6) = True
            BtCancelarReq.Visible = False
            
        End If
    End If
 'edgar.parada Glintt-HS-19165 * bloquear campos se for credencial importada
 If RequisicaoESP(EcNumReq.text) Then
       BloqueiaCampos
       If BG_DaComboSel(CbTipoIsencao) = gTipoIsencaoIsento Then
           BtGerarRecibo.Visible = False
       Else
           BtGerarRecibo.Visible = True
           BtGerarRecibo.Enabled = False
       End If
 End If
 '
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoSeguinte: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoSeguinte"
    Exit Sub
    Resume Next
End Sub

Sub PreencheCampos(flg_preencheCampos As Boolean)
    Dim aux As String
    Dim ssql As String
    Dim rsAnaFact As New adodb.recordset
    
    On Error GoTo TrataErro
    Dim i As Integer
    Me.SetFocus
    
    'RGONCALVES 17.02.2015 CHSJ-1785
    A_PREENCHER_CAMPOS = True
    '
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BtCancelarReq.Visible = True
        BtNotas.Enabled = True
        BtNotasVRM.Enabled = True
        BtFichaCruzada.Enabled = True
        BtResumo.Enabled = True
        BtDigitalizacao.Enabled = True
        
        If flg_preencheCampos = True Then
            BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
            BL_PreencheVectorBackup NumCampos, CamposEc, CamposEcBckp
            aux = EcHemodialise
            BL_ColocaTextoCombo "sl_isencao", "seq_isencao", "cod_isencao", EcCodIsencao, CbTipoIsencao
            EcHemodialise = aux
            BL_ColocaTextoCombo "sl_cod_hemodialise", "seq_hemodialise", "cod_hemodialise", EcHemodialise, CbHemodialise
            For i = 0 To NumCampos - 1
                CamposEc(i).BackColor = CoresCamposEc(i)
            Next
        End If
        
        LbNomeUtilCriacao.caption = BL_SelNomeUtil(EcUtilizadorCriacao.text)
        LbNomeUtilCriacao.ToolTipText = LbNomeUtilCriacao.caption
        LbNomeUtilAlteracao.caption = BL_SelNomeUtil(EcUtilizadorAlteracao.text)
        LbNomeUtilAlteracao.ToolTipText = LbNomeUtilAlteracao.caption
        LbNomeUtilImpressao.caption = BL_SelNomeUtil(EcUtilizadorImpressao.text)
        LbNomeUtilImpressao.ToolTipText = LbNomeUtilImpressao.caption
        LbNomeUtilImpressao2.caption = BL_SelNomeUtil(EcUtilizadorImpressao2.text)
        LbNomeUtilImpressao2.ToolTipText = LbNomeUtilImpressao2.caption
        LbNomeUtilFecho.caption = BL_SelNomeUtil(EcUtilizadorFecho.text)
        LbNomeUtilFecho.ToolTipText = LbNomeUtilFecho.caption
        LbNomeLocal.caption = BL_SelCodigo("gr_empr_inst", "nome_empr", "empresa_id", EcLocal, "V")
        LbNomeLocal.ToolTipText = LbNomeLocal.caption
        EcUtilNomeConf.caption = BL_SelNomeUtil(EcUtilConf.text)
        EcUtilNomeConf.ToolTipText = EcUtilNomeConf.caption
        EcUtilNomeSMS.caption = BL_SelNomeUtil(EcUtilSMS.text)
        EcUtilNomeEmail.caption = BL_SelNomeUtil(EcUtilEmail.text)

        BL_PreencheDadosUtente EcSeqUtente.text
        
        EcNumColheita.text = COLH_DevolveNumColheita(EcNumReq.text)
        
        PreencheDescEntFinanceira
        PreencheEstadoRequisicao
        PreencheDescMedico
        EcCodlocalentrega_Validate False
        PM_CarregaPerfisMarcados EcNumReq.text
        EcCodpais_Validate False
        EcNumReq.locked = True
        EcNumReqAssoc.locked = True
        PreencheDadosUtente
        If EcDataNasc <> "" Then
            EcIdade = BG_CalculaIdade(CDate(EcDataNasc.text))
        End If
                
        'colocar o bot�o de cancelar requisi��es activo
        gRequisicaoActiva = CLng(EcNumReq.text)
        gFlg_JaExisteReq = True
        
        
        CbTipoUtente.Enabled = False
        EcUtente.Enabled = False
        FrProdutos.Enabled = True
        FrBtProd.Enabled = True
        FrBtTubos.Enabled = True
        If Trim(EcDataChegada.text) <> "" Then
            EcDataChegada.Enabled = False
            EcDataPrevista.Enabled = False
        Else
            EcDataChegada.Enabled = True
            EcDataPrevista.Enabled = True
        End If
        i = InStr(1, EcCodPostalAux, "-")
        If i > 0 Then
            EcCodPostal = Mid(EcCodPostalAux, 1, i - 1)
            EcRuaPostal = Mid(EcCodPostalAux, i + 1, Len(EcCodPostalAux))
        ElseIf i = 0 And EcCodPostalAux.text <> "" Then
            EcCodPostal = Mid(EcCodPostalAux, 1, 4)
            EcRuaPostal = Mid(EcCodPostalAux, 5, 7)
        End If
        EcDescrPostal = BL_SeleccionaDescrPostal(EcCodPostalAux)
        'PreencheCodigoPostal
        FuncaoProcurar_PreencheProd CLng(Trim(EcNumReq.text))
        TB_ProcuraTubos FGTubos, CLng(Trim(EcNumReq.text)), True
        FuncaoProcurar_PreencheAna CLng(Trim(EcNumReq.text))
        
        If gPassaRecFactus <> mediSim Then
            RECIBO_PreencheCabecalho EcNumReq, RegistosR
            RECIBO_PreencheDetManuais Trim(EcNumReq.text), RegistosRM, CbTipoUtente, EcUtente
            RECIBO_PreencheAdiantamento Trim(EcNumReq.text), RegistosAD
            ActualizaLinhasRecibos -1
            PreencheFgRecibos
            PreencheFgAdiantamentos
        Else
            PreencheDadosDocumentos
            FACTUS_PreencheRequis EcNumReq.text, ""
            PreencheDadosMoviFact
            
        End If
        PreencheInformacao EcNumReq
        PreencheQuestao EcNumReq

        
        PreencheNotas
        'CbSituacao.SetFocus
        EcNumReq.SetFocus
        
        'Se a requisi��o est� cancelada os TAb�s de produtos e an�lises est�o desactivados
        If rs!estado_req = gEstadoReqCancelada Then
            SSTGestReq.TabEnabled(1) = False
            SSTGestReq.TabEnabled(2) = False
            SSTGestReq.TabEnabled(3) = False
            SSTGestReq.TabEnabled(4) = False
            SSTGestReq.TabEnabled(5) = False
            SSTGestReq.TabEnabled(6) = False
            BtCancelarReq.Visible = True
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        Else
            If flg_preencheCampos = True Then
                SSTGestReq.TabEnabled(1) = True
                SSTGestReq.TabEnabled(2) = True
                SSTGestReq.TabEnabled(3) = True
                SSTGestReq.TabEnabled(4) = True
                SSTGestReq.TabEnabled(5) = True
                SSTGestReq.TabEnabled(6) = True
    
                
                BtCancelarReq.Visible = False
                If flg_ReqBloqueada = False Then
                    BL_Toolbar_BotaoEstado "Remover", "Activo"
                    BL_Toolbar_BotaoEstado "Modificar", "Activo"
                Else
                    BL_Toolbar_BotaoEstado "Remover", "InActivo"
                    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
                End If
            End If
            
            
        End If
        
        ' PFerreira 15.03.2007
        If CbPrioColheita.ListCount > 0 Then
            If (Trim(EcPrColheita <> "")) Then
                BL_ColocaTextoCombo "sl_cod_prioridades", "seq_prio", "cod_prio", EcPrColheita, CbPrioColheita
            ElseIf (Trim(EcDataNasc) <> "" And IsChild) Then
                CbPrioColheita.ListIndex = 3
            Else
                CbPrioColheita.ListIndex = 1
            End If
        End If
        EcCodsala_Validate True
       'edgar.parada LJMANSO-353 09.01.2019
        CodEfrAUX = EcCodEFR
        'edgar.parada Glintt-HS-19577 19.08.2019
        efrDescrAUX = EcDescrEFR
        '
        EcCodProveniencia_Validate True
        If BL_HandleNull(rs!flg_cobra_domicilio, 0) = 1 Then
            CkCobrarDomicilio.value = vbChecked
            CkCobrarDomicilio.Enabled = True
        End If
    End If
    flg_preencheCampos = False
    
    'RGONCALVES 17.02.2015 CHSJ-1785
    A_PREENCHER_CAMPOS = False
    '
Exit Sub
TrataErro:
    flg_preencheCampos = False
    'RGONCALVES 17.02.2015 CHSJ-1785
    A_PREENCHER_CAMPOS = False
    '
    BG_LogFile_Erros "PreencheCampos: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheCampos"
    Exit Sub
    Resume Next
End Sub

Sub PreencheEstadoRequisicao()
    On Error GoTo TrataErro
    If EcEstadoReq = gEstadoReqBloqueada Then
       EcDescrEstado.BackColor = vermelhoClaro
    Else
       EcDescrEstado.BackColor = 14737632
    End If
   EcDescrEstado = BL_DevolveEstadoReq(Trim(BL_HandleNull(rs!estado_req, "")))
   
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheEstadoRequisicao: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheEstadoRequisicao"
    Exit Sub
    Resume Next
    
End Sub


Sub PreencheDescEntFinanceira()
    On Error GoTo TrataErro
    
    Dim Tabela As adodb.recordset
    Dim sql As String
    If EcCodEFR.text <> "" Then
        If VerificaEfrHemodialise(EcCodEFR) = True Then
            CbHemodialise.Enabled = True
        End If
        Set Tabela = New adodb.recordset
        sql = "SELECT descr_efr, flg_nova_ars FROM sl_efr WHERE cod_efr=" & Trim(EcCodEFR.text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Entidade inexistente!", vbOKOnly + vbInformation, "Entidades"
            EcCodEFR.text = ""
            EcDescrEFR.text = ""
            flg_novaArs = -1
            If (gLAB = cHSMARTA) Then
                Me.EcCodEFR = "0"
                Call EcCodEFR_Validate(True)
            End If
            
            EcCodEFR.SetFocus
        Else
            flg_novaArs = BL_HandleNull(Tabela!flg_nova_ars, 0)
            EcDescrEFR.text = Tabela!descr_efr
        End If
        Tabela.Close
        Set Tabela = Nothing

    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDescEntFinanceira: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDescEntFinanceira"
    Exit Sub
    Resume Next
    
End Sub


Private Sub SSTGestReq_Click(PreviousTab As Integer)
    On Error GoTo TrataErro
    Dim i
    Dim j As Integer
    DoEvents
    
    If SSTGestReq.Tab = 1 Then
        If EcDataFecho.text <> "" Then
            BtPesquisaAna.Enabled = False
        Else
            If UBound(estrutOperation) = 0 And RequisicaoESP(EcNumReq.text) = False Then
                BtPesquisaAna.Enabled = True
            End If
            FGAna.SetFocus
            FGAna.row = FGAna.rows - 1
            FGAna.Col = 0
            
        End If
        
    ElseIf SSTGestReq.Tab = 0 Then
        EcAuxAna.Visible = False
        EcAuxProd.Visible = False
        If EcDataPrevista.Enabled = True And EcDataPrevista.Visible = True Then
            If gMultiReport <> 1 Then
                EcDataPrevista.SetFocus
            End If
        ElseIf EcDataChegada.Enabled = True And EcDataChegada.Visible = True Then
            'CbSituacao.SetFocus             'sdo
        End If
    
    ElseIf SSTGestReq.Tab = 2 Then
        If FgProd.Enabled = True Then FgProd.SetFocus
        
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "SSTGestReq_Click: " & Err.Number & " - " & Err.Description, Me.Name, "SSTGestReq_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub TAna_DblClick()
    On Error GoTo TrataErro
    Dim i As Long
    Dim s As String
    
    If TAna.Nodes.Count <> 0 Then
        If InStr(1, TAna.Nodes(TAna.SelectedItem.Index).key, gGHOSTMEMBER_S) <> 0 Then
            'Inserir Ghostmembers
            
            'Retira nome da complexa
            s = Mid(TAna.Nodes(TAna.SelectedItem.Index).key, 1, InStr(1, TAna.Nodes(TAna.SelectedItem.Index).key, gGHOSTMEMBER_S) - 1)
            GhostComplexa = s
            
            If Mid(TAna.SelectedItem.parent.key, 1, 1) = "P" Then
                'A complexa est� dentro de um perfil
                'GhostPerfil = Mid(TAna.SelectedItem.Parent.Key, 1, InStr(1, TAna.SelectedItem.Parent.Key, "C") - 1) 'soliveira 10.05.2006 ex: perfil = PURIMCB, ficava PURIM
                GhostPerfil = Mid(TAna.SelectedItem.parent.key, 1, InStr(1, TAna.SelectedItem.parent.key, GhostComplexa) - 1)
            Else
                GhostPerfil = "0"
                'A complexa � o pai
            End If
            
            Call BL_SelGhostMember(s)
            BL_MudaPosicaoRato 100, 300
            MDIFormInicio.PopupMenu MDIFormInicio.HIDE_POPUP
        Else
            i = Pesquisa_RegistosA(TAna.Nodes(TAna.SelectedItem.Index).key)
        
            If i <> -1 Then
                FGAna.topRow = i
                FGAna.row = i
                FGAna.SetFocus
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "TAna_DblClick: " & Err.Number & " - " & Err.Description, Me.Name, "TAna_DblClick"
    Exit Sub
    Resume Next

End Sub

Sub Funcao_DataActual()
    On Error GoTo TrataErro

    If Me.ActiveControl.Name = "EcDataCriacao" Then
        BL_PreencheData EcDataCriacao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataAlteracao" Then
        BL_PreencheData EcDataAlteracao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataImpressao" Then
        BL_PreencheData EcDataImpressao, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataImpressao2" Then
        BL_PreencheData EcDataImpressao2, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataPrevista" Then
        BL_PreencheData EcDataPrevista, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDataChegada" Then
        BL_PreencheData EcDataChegada, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcDtPretend" Then
        BL_PreencheData EcDtPretend, Me.ActiveControl
    ElseIf Me.ActiveControl.Name = "EcAuxProd" Then
        If EcAuxProd.Tag = adDate Then
            BL_PreencheData EcAuxProd, Me.ActiveControl
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Funcao_DataActual: " & Err.Number & " - " & Err.Description, Me.Name, "Funcao_DataActual"
    Exit Sub
    Resume Next
    
End Sub

Function Coloca_Estado(DtPrev, DtChega, Optional DtElim = "") As String
    On Error GoTo TrataErro
    If DateValue(DtPrev) < DateValue(dataAct) And DtChega = "" Then
        Coloca_Estado = "Falta"
    ElseIf DateValue(DtPrev) >= DateValue(dataAct) And DtChega = "" Then
        Coloca_Estado = "Espera"
    ElseIf DtChega <> "" Then
        Coloca_Estado = "Entrou"
    Else
        Coloca_Estado = ""
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Coloca_Estado: " & Err.Number & " - " & Err.Description, Me.Name, "Coloca_Estado"
    Coloca_Estado = ""
    Exit Function
    Resume Next

End Function

Sub Preenche_LaProdutos(indice As Integer)
    On Error GoTo TrataErro
    
    Dim TamDescrProd As Integer
    Dim TamDescrEspecif As Integer
    Dim i As Integer
    Dim Str1 As String
    Dim Str2 As String
    Dim Str3 As String
    
    LaProdutos.caption = ""
    If RegistosP(indice).CodProd <> "" Then
        Str1 = "Produto vai chegar a: "
        Str2 = "Produto devia ter chegado a: "
        Str3 = "Produto chegou a: "
        If RegistosP(indice).EstadoProd = "Entrou" Then
            LaProdutos.caption = LaProdutos.caption & Str3 & RegistosP(indice).DtChega
        ElseIf RegistosP(indice).EstadoProd = "Falta" Then
            LaProdutos.caption = LaProdutos.caption & Str2 & RegistosP(indice).DtPrev
        ElseIf EcDataPrevista.text <> "" Then
            LaProdutos.caption = LaProdutos.caption & Str1 & RegistosP(indice).DtPrev
        End If
        TamDescrProd = Len(Trim(UCase(RegistosP(indice).DescrProd)))
        TamDescrEspecif = Len(Trim(UCase(RegistosP(indice).DescrEspecif)))
        For i = 1 To 80 - (TamDescrProd + TamDescrEspecif)
            LaProdutos.caption = LaProdutos.caption & Chr(32)
        Next i
        LaProdutos.caption = LaProdutos.caption & Trim(UCase(RegistosP(indice).DescrProd)) & " " & Trim(UCase(RegistosP(indice).DescrEspecif))
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Preenche_LaProdutos: " & Err.Number & " - " & Err.Description, Me.Name, "Preenche_LaProdutos"
    Exit Sub
    Resume Next

End Sub

Sub Preenche_LaTubos(indice As Integer)
    On Error GoTo TrataErro
    
    Dim TamDescrTubo As Integer
    Dim i As Integer
    Dim Str1 As String
    Dim Str2 As String
    Dim Str3 As String
    
    LaTubos.caption = ""
    If gEstruturaTubos(indice).CodTubo <> "" Then
        Str1 = "Tubo vai chegar a: "
        Str2 = "Tubo devia ter chegado a: "
        Str3 = "Tubo chegou a: "
        If gEstruturaTubos(indice).estado_tubo = "Entrou" Then
            LaTubos.caption = LaTubos.caption & Str3 & gEstruturaTubos(indice).dt_chega
        ElseIf gEstruturaTubos(indice).estado_tubo = "Falta" Then
            LaTubos.caption = LaTubos.caption & Str2 & gEstruturaTubos(indice).dt_previ
        ElseIf EcDataPrevista.text <> "" Then
            LaTubos.caption = LaProdutos.caption & Str1 & gEstruturaTubos(indice).dt_previ
        End If
        TamDescrTubo = Len(Trim(UCase(gEstruturaTubos(indice).Designacao_tubo)))
        
        For i = 1 To 80 - (TamDescrTubo)
            LaTubos.caption = LaTubos.caption & Chr(32)
        Next i
        LaTubos.caption = LaTubos.caption & Trim(UCase(gEstruturaTubos(indice).Designacao_tubo))
        LaTubos.caption = LaTubos.caption & vbCrLf & "Norma:  " & gEstruturaTubos(indice).normaTubo
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Preenche_LaTubos: " & Err.Number & " - " & Err.Description, Me.Name, "Preenche_LaTubos"
    Exit Sub
    Resume Next

End Sub

Sub Inicializa_Estrutura_Analises()
    On Error GoTo TrataErro
    
    'inicializa��es da lista de marca��es de an�lises
    ReDim MaReq(0 To 0)
    MaReq(0).Cod_Perfil = ""
    MaReq(0).Descr_Perfil = ""
    MaReq(0).cod_ana_c = ""
    MaReq(0).Descr_Ana_C = ""
    MaReq(0).cod_ana_s = ""
    MaReq(0).descr_ana_s = ""
    MaReq(0).Ord_Ana = 0
    MaReq(0).Ord_Ana_Compl = 0
    MaReq(0).Ord_Ana_Perf = 0
    MaReq(0).cod_agrup = ""
    MaReq(0).N_Folha_Trab = 0
    MaReq(0).dt_chega = ""
    MaReq(0).Flg_Apar_Transf = ""
    MaReq(0).flg_estado = ""
    MaReq(0).Flg_Facturado = 0
    LbTotalAna = "0"
Exit Sub
TrataErro:
    BG_LogFile_Erros "Inicializa_Estrutura_Analises: " & Err.Number & " - " & Err.Description, Me.Name, "Inicializa_Estrutura_Analises"
    Exit Sub
    Resume Next

End Sub

Sub ImprimeResumo()
    On Error GoTo TrataErro
    If gImprFolhaResumoParaRecibos <> mediSim Then
         'BRUNODSANTOS - LACTO-357 -> EcNomeMedico.Text, EcDescrMedico.Text, EcIdade.Text, EcDescrEFR.Text
        FR_ImprimeResumoCrystal_HOSP EcNumReq.text
    Else
        FR_ImprimeResumoParaImpressoraRecibos EcNumReq
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumo: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumo"
    Exit Sub
    Resume Next
End Sub

Private Sub CriaNovaClasse(Coleccao As Collection, Index As Long, tipo As String, Optional EmCascata As Variant)
    On Error GoTo TrataErro

    ' Tipo = ANA ou PROD
    ' EmCascata = True : Cria todos elementos at� o Index
    ' EmCascata = False: Cria apenas o elemento Index

    Dim Cascata As Boolean
    Dim i As Long
    Dim AuxRegEnt As New ClRegRecibos
    Dim AuxRegProd As New ClRegProdutos
    Dim AuxRegAna As New ClRegAnalises
    Dim AuxRegTubo As New ClRegTubos
    Dim AuxRegRecManuais As New ClRegRecibosAna
    Dim AuxAdiantamento As New ClAdiantamentos
    
    If IsMissing(EmCascata) Then
        Cascata = False
    Else
        Cascata = EmCascata
    End If
    
    If tipo = "PROD" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegProd
            Next i
        Else
            Coleccao.Add AuxRegProd
        End If
    ElseIf tipo = "TUBO" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegTubo
            Next i
        Else
            Coleccao.Add AuxRegTubo
        End If
    ElseIf tipo = "REC" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegEnt
            Next i
        Else
            Coleccao.Add AuxRegEnt
        End If
    ElseIf tipo = "ADI" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxAdiantamento
            Next i
        Else
            Coleccao.Add AuxAdiantamento
        End If
    ElseIf tipo = "REC_MAN" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegRecManuais
            Next i
        Else
            Coleccao.Add AuxRegRecManuais
        End If
    Else
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegAna
            Next i
        Else
            Coleccao.Add AuxRegAna
        End If
    End If
    
    Set AuxRegAna = Nothing
    Set AuxAdiantamento = Nothing
    Set AuxRegProd = Nothing
    Set AuxRegTubo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "CriaNovaClasse: " & Err.Number & " - " & Err.Description, Me.Name, "CriaNovaClasse"
    Exit Sub
    Resume Next
    
End Sub

Private Sub LimpaColeccao(Coleccao As Collection, Index As Long)
    On Error GoTo TrataErro

    ' Index = -1 : Apaga todos elementos
    
    Dim i As Long
    Dim Tot As Long

    Tot = Coleccao.Count
    If Index = -1 Then
        i = Tot
        While i > 0
            Coleccao.Remove i
            i = i - 1
        Wend
    Else
        If Index <= Coleccao.Count Then
            Coleccao.Remove Index
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaColeccao: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaColeccao"
    Exit Sub
    Resume Next

End Sub

Function Verifica_Prod_ja_Existe(indice As Integer, Produto As String) As Boolean
    On Error GoTo TrataErro
    
    Dim i As Integer
    Dim k As Integer
    Dim TotalRegistos As Integer
    
    Verifica_Prod_ja_Existe = False
    TotalRegistos = RegistosP.Count - 1
    If indice >= 0 Then
        i = 1
        While i <= TotalRegistos And Verifica_Prod_ja_Existe = False
            If UCase(Trim(Produto)) = UCase(Trim(RegistosP(i).CodProd)) And indice <> i Then
                Verifica_Prod_ja_Existe = True
            End If
            i = i + 1
        Wend
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Prod_ja_Existe: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Prod_ja_Existe"
    Verifica_Prod_ja_Existe = True
    Exit Function
    Resume Next

End Function

Function Verifica_Ana_ja_Existe(indice As Long, codAgrup As String) As Boolean
        
    Dim i As Integer
    Dim TotalRegistos As Integer
    Dim CodAgrupAux As String
    Dim codAna As String
    
    codAna = codAgrup
    SELECT_Descr_Ana codAna
    

        
    Verifica_Ana_ja_Existe = False
    TotalRegistos = RegistosA.Count - 1
   
   If InStr(1, "SCP", UCase(Mid(codAgrup, 1, 1))) > 0 Then
        If indice >= 0 Then
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAgrup)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) And indice <> i Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        Else
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAgrup)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        End If
    Else
        If indice >= 0 Then
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAna)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) And indice <> i Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        Else
            i = 1
            While i <= TotalRegistos And Verifica_Ana_ja_Existe = False
                If UCase(Trim(codAna)) = Mid(UCase(Trim(RegistosA(i).codAna)), 1) Then
                    Verifica_Ana_ja_Existe = True
                End If
                i = i + 1
            Wend
        End If
    End If

    codAgrup = CodAgrupAux & codAgrup

Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Ana_ja_Existe: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Ana_ja_Existe"
    Verifica_Ana_ja_Existe = True
    Exit Function
    Resume Next

End Function

Sub LimpaFgProd()
     On Error GoTo TrataErro
   
    Dim j As Long
    
    ExecutaCodigoP = False
    
    j = FgProd.rows - 1
    While j > 0
        If j > 1 Then
            FgProd.RemoveItem j
        Else
            FgProd.TextMatrix(j, 0) = ""
            FgProd.TextMatrix(j, 1) = ""
            FgProd.TextMatrix(j, 2) = ""
            FgProd.TextMatrix(j, 3) = ""
            FgProd.TextMatrix(j, 4) = ""
            FgProd.TextMatrix(j, 5) = ""
            FgProd.TextMatrix(j, 6) = ""
            FgProd.TextMatrix(j, 7) = ""
        End If
        
        j = j - 1
    Wend
    
    ExecutaCodigoP = True
    LastColP = 0
    LastRowP = 1
        
    CriaNovaClasse RegistosP, 1, "PROD", True
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgProd: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgProd"
    Exit Sub
    Resume Next

End Sub

Sub LimpaFgTubos()
     On Error GoTo TrataErro
   
    Dim j As Long
    
    ExecutaCodigoT = False
    
    j = FGTubos.rows - 1
    While j > 0
        If j > 1 Then
            FGTubos.RemoveItem j
        Else
            FGTubos.TextMatrix(j, 0) = ""
            FGTubos.TextMatrix(j, 1) = ""
            FGTubos.TextMatrix(j, 2) = ""
            FGTubos.TextMatrix(j, 3) = ""
            FGTubos.TextMatrix(j, 4) = ""
            FGTubos.TextMatrix(j, 5) = ""
            FGTubos.TextMatrix(j, 6) = ""
            FGTubos.TextMatrix(j, 7) = ""
        End If
        
        j = j - 1
    Wend
    
    ExecutaCodigoT = True
    LastColT = 0
    LastRowT = 1
        
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgTubos: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgTubos"
    Exit Sub
    Resume Next

End Sub

Sub LimpaFGAna()
     On Error GoTo TrataErro
   Dim i As Integer
    Dim j As Long
    
    ExecutaCodigoA = False
    
    j = FGAna.rows - 1
    While j > 0
        If j > 1 Then
            FGAna.RemoveItem j
        Else
            For i = 0 To FGAna.Cols - 1
                FGAna.TextMatrix(j, i) = ""
            Next
            BL_MudaCorFg FGAna, j, vbWhite
        End If
        
        j = j - 1
    Wend
    
    ExecutaCodigoA = True
    LastColA = 0
    LastRowA = 1
        
    CriaNovaClasse RegistosA, 1, "ANA", True
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFGAna: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFGAna"
    Exit Sub
    Resume Next

End Sub
Sub LimpaFgRecAna()
    On Error GoTo TrataErro
    
    Dim j As Long
    
    
    j = FgAnaRec.rows - 1
    While j > 0
        If j > 1 Then
            FgAnaRec.RemoveItem j
        Else
            FgAnaRec.TextMatrix(j, 0) = ""
            FgAnaRec.TextMatrix(j, 1) = ""
            FgAnaRec.TextMatrix(j, 2) = ""
        End If
        
        j = j - 1
    Wend
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgRecAna: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgRecAna"
    Exit Sub
    Resume Next
End Sub

Private Sub LimpaFgInfo()
    Dim i As Integer
    FgInformacao.rows = 2
    For i = 0 To FgInformacao.Cols - 1
        FgInformacao.TextMatrix(1, i) = ""
    Next
    
    FgQuestoes.rows = 2
    For i = 0 To FgQuestoes.Cols - 1
        FgQuestoes.TextMatrix(1, i) = ""
    Next
End Sub

Function Procura_Prod_ana() As Boolean

    On Error GoTo TrataErro
    'Verifica se existem an�lises com o produto que se pretende eliminar
    'No caso de existirem n�o deixa apagar o produto sem que se tenha apagado a an�lise

    Dim RsProd As adodb.recordset
    Dim sql As String
    Dim lista As String
    Dim ListaP As String
    Dim i As Integer
    Dim CmdPesqGhost As adodb.Command
    Dim RsPesqGhost As adodb.recordset
    
    Procura_Prod_ana = False
    
    Set CmdPesqGhost = New adodb.Command
    Set CmdPesqGhost.ActiveConnection = gConexao
    CmdPesqGhost.CommandType = adCmdText
    CmdPesqGhost.CommandText = "SELECT COD_MEMBRO FROM SL_MEMBRO WHERE COD_ANA_C = ? AND T_MEMBRO = 'A'"
    CmdPesqGhost.Prepared = True
    CmdPesqGhost.Parameters.Append CmdPesqGhost.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
    
    If UBound(MaReq) >= 1 Then
        If MaReq(1).cod_ana_s <> "" Then
            lista = ""
            ListaP = ""
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                    'Pesquisa os GhostMembers
                    CmdPesqGhost.Parameters(0) = MaReq(i).cod_ana_c
                    Set RsPesqGhost = CmdPesqGhost.Execute
                    While Not RsPesqGhost.EOF
                        lista = lista & BL_TrataStringParaBD(BL_HandleNull(RsPesqGhost!cod_membro, "-1")) & ","
                        RsPesqGhost.MoveNext
                    Wend
                    RsPesqGhost.Close
                    Set RsPesqGhost = Nothing
                Else
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_s) & ","
                End If
            Next i
            
            lista = Mid(lista, 1, Len(lista) - 1)
            
            sql = "SELECT descr_ana_s from sl_ana_s WHERE cod_produto = " & BL_TrataStringParaBD(FgProd.TextMatrix(FgProd.row, 0)) & _
                    " AND cod_ana_s IN ( " & lista & ")"
            
            Set RsProd = New adodb.recordset
            RsProd.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            RsProd.Open sql, gConexao, adOpenStatic, adLockReadOnly
                   
            lista = ""
            While Not RsProd.EOF
                lista = lista & BL_HandleNull(RsProd!descr_ana_s, "") & ","
                RsProd.MoveNext
            Wend
            
            For i = 1 To UBound(MaReq)
                If MaReq(i).Cod_Perfil <> "" Then
                    If InStr(1, ListaP, MaReq(i).Cod_Perfil) = 0 Then
                        ListaP = ListaP & BL_TrataStringParaBD(MaReq(i).Cod_Perfil)
                    End If
                End If
            Next i
            
            sql = "SELECT descr_perfis from sl_perfis WHERE cod_produto = " & BL_TrataStringParaBD(FgProd.TextMatrix(FgProd.row, 0)) & _
                    " AND cod_perfis IN ( " & ListaP & ")"
            Set RsProd = New adodb.recordset
            RsProd.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            RsProd.Open sql, gConexao, adOpenStatic, adLockReadOnly
            
            ListaP = ""
            While Not RsProd.EOF
                lista = lista & BL_HandleNull(RsProd!descr_perfis, "") & ","
                RsProd.MoveNext
            Wend
            
            If Trim(lista) <> "" Then
                lista = Mid(lista, 1, Len(lista) - 1)
                Procura_Prod_ana = True
                
                If RsProd.RecordCount > 1 Then
                    BG_Mensagem mediMsgBox, "As an�lises " & lista & " necessitam do produto " & FgProd.TextMatrix(FgProd.row, 1) & " para serem realizadas ! " & Chr(13) & "Remova primeiro as an�lises, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Produtos"
                Else
                    BG_Mensagem mediMsgBox, "A an�lise " & lista & " necessita do produto " & FgProd.TextMatrix(FgProd.row, 1) & " para ser realizada ! " & Chr(13) & "Remova primeiro a an�lise, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Produtos"
                End If
            End If
            
            RsProd.Close
            Set RsProd = Nothing
        End If
    End If
    
    Set CmdPesqGhost = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "Procura_Prod_ana: " & Err.Number & " - " & Err.Description, Me.Name, "Procura_Prod_ana"
    Procura_Prod_ana = False
    Exit Function
    Resume Next
    
End Function

Function Procura_Tubo_ana() As Boolean

    'Verifica se existem an�lises com o tubo que se pretende eliminar
    'No caso de existirem n�o deixa apagar o tubo sem que se tenha apagado a an�lise

    Dim rsTubo As adodb.recordset
    Dim sql As String
    Dim lista As String
    Dim i As Integer
    Dim CmdPesqGhost As adodb.Command
    Dim RsPesqGhost As adodb.recordset
    
    Procura_Tubo_ana = False
    
    Set CmdPesqGhost = New adodb.Command
    Set CmdPesqGhost.ActiveConnection = gConexao
    CmdPesqGhost.CommandType = adCmdText
    CmdPesqGhost.CommandText = "SELECT COD_MEMBRO FROM SL_MEMBRO WHERE COD_ANA_C = ? AND T_MEMBRO = 'A'"
    CmdPesqGhost.Prepared = True
    CmdPesqGhost.Parameters.Append CmdPesqGhost.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
    
    If UBound(MaReq) >= 1 Then
        If MaReq(1).cod_ana_s <> "" Then
            lista = ""
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_ana_c = "C99999" Then
                    lista = lista & BL_TrataStringParaBD(MaReq(i).Cod_Perfil) & ","
                ElseIf MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                    'Pesquisa os GhostMembers
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_c) & ","
                Else
                    lista = lista & BL_TrataStringParaBD(MaReq(i).cod_ana_s) & ","
                End If
            Next i
            lista = Mid(lista, 1, Len(lista) - 1)
            
            sql = "SELECT descr_ana descr_ana_s from slv_analises WHERE cod_tubo = " & BL_TrataStringParaBD(FGTubos.TextMatrix(FGTubos.row, 0)) & _
                    " AND cod_ana IN ( " & lista & ")"
            
            Set rsTubo = New adodb.recordset
            rsTubo.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sql
            rsTubo.Open sql, gConexao, adOpenStatic, adLockReadOnly
                   
            lista = ""
            While Not rsTubo.EOF
                lista = lista & BL_HandleNull(rsTubo!descr_ana_s, "") & ","
                rsTubo.MoveNext
            Wend
            
            If Trim(lista) <> "" Then
                lista = Mid(lista, 1, Len(lista) - 1)
                Procura_Tubo_ana = True
                
                If rsTubo.RecordCount > 1 Then
                    BG_Mensagem mediMsgBox, "As an�lises " & lista & " necessitam do tubo " & FgProd.TextMatrix(FgProd.row, 1) & " para serem realizadas ! " & Chr(13) & "Remova primeiro as an�lises, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Tubos"
                Else
                    BG_Mensagem mediMsgBox, "A an�lise " & lista & " necessita do tubo " & FgProd.TextMatrix(FgProd.row, 1) & " para ser realizada ! " & Chr(13) & "Remova primeiro a an�lise, e ent�o de seguida o produto.", vbOKOnly + vbExclamation, "Remover Tubos"
                End If
            End If
            
            rsTubo.Close
            Set rsTubo = Nothing
        End If
    End If
    
    Set CmdPesqGhost = Nothing
    
End Function

Function ValidaEspecifObrig() As Boolean
    On Error GoTo TrataErro

    Dim i As Integer
    Dim n As Integer
    
    n = RegistosP.Count
    If n > 0 Then
        n = n - 1
        For i = 1 To n
            If RegistosP(i).CodProd <> "" Then
                If RegistosP(i).EspecifObrig = "S" Then
                    If RegistosP(i).CodEspecif = "" Then
                        BG_Mensagem mediMsgBox, "O produto " & RegistosP(i).DescrProd & " exige especifica��o !", vbInformation, ""
                        SSTGestReq.Tab = 1
                        Exit Function
                    End If
                End If
            End If
        Next
    End If
    ValidaEspecifObrig = True
Exit Function
TrataErro:
    BG_LogFile_Erros "ValidaEspecifObrig: " & Err.Number & " - " & Err.Description, Me.Name, "ValidaEspecifObrig"
    ValidaEspecifObrig = False
    Exit Function
    Resume Next
      
End Function

Private Sub TAna_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    Dim ana As String
    Dim Flg_EncontrouMAReq As Boolean
    Dim Flg_EncontrouRegistosA As Boolean
    Dim i As Integer

    If KeyCode = 46 And InStr(1, TAna.Nodes(TAna.SelectedItem.Index).key, "S") <> 0 And (InStr(1, TAna.Nodes(TAna.SelectedItem.Index).key, "C") <> 0 Or InStr(1, TAna.Nodes(TAna.SelectedItem.Index).key, "P") <> 0) Then   'Delete de um nodo filho (analise simples) de uma complexa ou perfil
        'Apagar analise das estruturas
        Flg_EncontrouMAReq = False
        i = 1
        While Not Flg_EncontrouMAReq
            If MaReq(i).descr_ana_s = TAna.Nodes.Item(TAna.SelectedItem.Index).text Then
                ana = MaReq(i).cod_ana_s
                Flg_EncontrouMAReq = True
                Actualiza_Estrutura_MAReq i
                TotalEliminadas = TotalEliminadas + 1
                ReDim Preserve Eliminadas(TotalEliminadas)
                Eliminadas(TotalEliminadas).codAna = ana
                Eliminadas(TotalEliminadas).NReq = EcNumReq.text
                EliminaTuboEstrut
                EliminaProduto ana
            End If
            i = i + 1
        Wend
        If Flg_EncontrouMAReq Then
            TAna.Nodes.Remove (TAna.SelectedItem.Index)
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "TAna_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "TAna_KeyDown"
    Exit Sub
    Resume Next
      
End Sub


Function ValidaProdutos() As Boolean
    

    On Error GoTo ErrorHandler
    
    Dim ret As Boolean
    ret = True
    
    If (RegistosP.Count > 1) Then
        ret = True
    Else
        If (RegistosA.Count > 1) Then
            ret = False
        Else
            ret = True
        End If
    End If
    
    ValidaProdutos = ret
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : EFR_Descricao (ValidaProdutos) -> " & Err.Description)
            ValidaProdutos = True
            Exit Function
    End Select
End Function


Sub BD_Insert_Utente()
    
    
    Dim SQLQuery As String
    Dim RsNUtente As adodb.recordset
    Dim Query As String
    Dim i As Integer
    Dim seq As Long
    
    'se a data de inscri��o estiver vazia
    If gDUtente.dt_inscr = "" Then
        gDUtente.dt_inscr = Format(dataAct, gFormatoData)
    End If
   
    If gDUtente.dt_nasc_ute = "" Then
        gDUtente.dt_nasc_ute = "01-01-1900"
    End If
   
   ' caso o tipo de utente nao esteja preenchido
    If gDUtente.t_utente = "" Then
        gDUtente.t_utente = "LAB"           'obrigar a que seja por defeito criado um utente LAB
    End If

    
    On Error GoTo Trata_Erro
        
    If Trim(gDUtente.Utente) = "" Then
        If BG_Mensagem(mediMsgBox, "Quer gerar um n�mero sequencial para o utente do tipo " & gDUtente.t_utente & "?", vbYesNo + vbDefaultButton2 + vbQuestion, "Gerar Sequencial") = vbNo Then
            Exit Sub
        End If
        
        'Determinar o novo numero para o Tipo Utente (TENTA 10 VEZES - LOCK!)
        i = 0
        seq = -1
        While seq = -1 And i <= 10
            seq = BL_GeraUtente(gDUtente.t_utente)
            i = i + 1
        Wend
        
        If seq = -1 Then
            BG_Mensagem mediMsgBox, "Erro a gerar n�mero do utente!", vbCritical + vbOKOnly, "ERRO"
            Exit Sub
        End If
        
        EcUtente = seq
    Else
        Set RsNUtente = New adodb.recordset
        RsNUtente.CursorLocation = adUseServer
        RsNUtente.Open "SELECT nome_ute, dt_nasc_ute FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(gDUtente.t_utente) & " AND utente = " & BL_TrataStringParaBD(gDUtente.Utente), gConexao, adOpenStatic, adLockReadOnly
        
        If RsNUtente.RecordCount > 0 Then
            BG_Mensagem mediMsgBox, "J� existe um utente do tipo " & gDUtente.t_utente & " com o n�mero " & gDUtente.Utente & "!" & Chr(13) & "Nome : " & BL_HandleNull(RsNUtente!nome_ute) & Chr(13) & "Dt. Nasc : " & BL_HandleNull(RsNUtente!dt_nasc_ute), vbOKOnly + vbExclamation, "Inserir Utente"
            RsNUtente.Close
            Set RsNUtente = Nothing
            Exit Sub
        Else
            RsNUtente.Close
            Set RsNUtente = Nothing
        End If
    End If
        
    'Determinar o novo numero sequencial (TENTA 10 VEZES - LOCK!)
    i = 0
    seq = -1
    While seq = -1 And i <= 10
        seq = BL_GeraNumero("SEQ_UTENTE")
        i = i + 1
    Wend
    
    If seq = -1 Then
        BG_Mensagem mediMsgBox, "Erro a gerar n�mero de sequencial interno do utente!", vbCritical + vbOKOnly, "ERRO"
        Exit Sub
    End If
    
    EcSeqUtente = seq
    
    gSQLError = 0
    gSQLISAM = 0
    
    
    SQLQuery = "INSERT INTO sl_identif (seq_utente,t_utente,utente,n_proc_1,n_proc_2," & _
            " dt_inscr,n_cartao_ute,nome_ute,dt_nasc_ute,sexo_ute,est_civ_ute, " & _
            " descr_mor_ute,cod_postal_ute,telef_ute, cod_efr_ute,user_cri,dt_cri )" & _
            " values (" & EcSeqUtente & "," & BL_TrataStringParaBD(gDUtente.t_utente) & "," & BL_TrataStringParaBD(gDUtente.Utente) & "," & BL_TrataStringParaBD(gDUtente.n_proc_1) & "," & BL_TrataStringParaBD(gDUtente.n_proc_2) & _
            "," & BL_TrataDataParaBD(gDUtente.dt_inscr) & "," & BL_TrataStringParaBD(gDUtente.n_cartao_ute) & "," & BL_TrataStringParaBD(gDUtente.nome_ute) & "," & BL_TrataStringParaBD(gDUtente.dt_nasc_ute) & "," & BL_TrataStringParaBD(gDUtente.sexo_ute) & "," & BL_TrataStringParaBD(gDUtente.est_civil) & _
            "," & BL_TrataStringParaBD(gDUtente.morada) & "," & BL_TrataStringParaBD(gDUtente.cod_postal) & "," & BL_TrataStringParaBD(gDUtente.telefone) & "," & BL_TrataStringParaBD(gDUtente.cod_efr) & ",'" & gCodUtilizador & "'," & BL_TrataDataParaBD(dataAct) & ")"
            
    BG_ExecutaQuery_ADO SQLQuery
    BG_Trata_BDErro
    
    If gSQLError <> 0 Then
        'Erro a inserir utente
        
        GoTo Trata_Erro
    End If
    

    gDUtente.seq_utente = BL_String2Double(EcSeqUtente)
    PreencheDadosUtente
    
    BG_CommitTransaction
    
    Exit Sub
    
Trata_Erro:
    BG_LogFile_Erros "FormIdentificaUtente: BD_Insert_Utente -> " & Err.Description
    BG_RollbackTransaction
    
    LimpaCampos

End Sub



Function Verifica_Ana_Pertence_Local(codAgrup As String) As Boolean
    On Error GoTo TrataErro
    Dim RsLocal As adodb.recordset
    
    Set RsLocal = New adodb.recordset
    RsLocal.CursorLocation = adUseServer
    RsLocal.CursorType = adOpenStatic
    RsLocal.Source = "select * from slv_analises where cod_ana = " & BL_TrataStringParaBD(codAgrup) & " and cod_local = " & gCodLocal
    RsLocal.ActiveConnection = gConexao
    If gModoDebug = mediSim Then BG_LogFile_Erros RsLocal.Source
    RsLocal.Open
    If RsLocal.RecordCount <= 0 Then
        Verifica_Ana_Pertence_Local = False
    Else
        Verifica_Ana_Pertence_Local = True
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "Verifica_Ana_Pertence_Local: " & Err.Number & " - " & Err.Description, Me.Name, "Verifica_Ana_Pertence_Local"
    Verifica_Ana_Pertence_Local = False
    Exit Function
    Resume Next

End Function


Public Sub EliminaTuboEstrut()
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim flg_temAna As Boolean
    On Error GoTo TrataErro
    
    For i = 1 To gTotalTubos
        If i <= gTotalTubos Then
            flg_temAna = False
            For j = 1 To UBound(MaReq)
                If MaReq(j).indice_tubo = i Then
                    flg_temAna = True
                    Exit For
                End If
            Next j
            'BEDSIDE -> gEstruturaTubos(i).estado_tubo <> gEstadosTubo.cancelado
            If flg_temAna = False And gEstruturaTubos(i).estado_tubo <> gEstadosTubo.cancelado Then
                AcrescentaTubosEliminados EcNumReq.text, gEstruturaTubos(i).seq_req_tubo
                For k = i To gTotalTubos - 1
                    TB_MoveEstrutTubos k + 1, k
                Next
                gTotalTubos = gTotalTubos - 1
                ReDim Preserve gEstruturaTubos(gTotalTubos)
                FGTubos.RemoveItem i
                i = i - 1
            End If
        End If
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no FormGestaoRequisicaoPrivadoPrivado -> Elimina��o de Tubos: " & Err.Description
    Exit Sub
    Resume Next
End Sub


Private Sub EliminaProduto(cod_ana As String)
    Dim i  As Integer
    Dim ssql As String
    Dim sSqlP As String
    Dim sSqlC As String
    Dim rsAna As New adodb.recordset
    Dim RsProd As New adodb.recordset
    Dim CodProduto As String
    Dim flg_PodeEliminar As Boolean
    Dim flg_existeProd As Boolean
    
    On Error GoTo TrataErro
    
    
    ' SE FOR COMPLEXA CHAMA A MESMA FUNCAO PARA CADA SIMPLES
    If Mid(cod_ana, 1, 1) = "C" Then
        sSqlC = "SELECT cod_produto FROM sl_ana_C WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
        ssql = "SELECT cod_membro FROM sl_membro WHERE cod_ana_c = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSqlC
        rsAna.Open sSqlC, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
            GoTo continuap
        Else
            Set rsAna = New adodb.recordset
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros ssql
            rsAna.Open ssql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    EliminaProduto rsAna!cod_membro
                    rsAna.MoveNext
                Wend
            End If
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
    
    
    ' SE FOR PERFIL CHAMA A MESMA FUNCAO PARA CADA MEMBRO
    If Mid(cod_ana, 1, 1) = "P" Then
        ssql = "SELECT cod_analise FROM sl_ana_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        sSqlP = "SELECT cod_produto FROM sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSqlP
        rsAna.Open sSqlP, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
            GoTo continuap
        Else
            Set rsAna = New adodb.recordset
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros ssql
            rsAna.Open ssql, gConexao
            If rsAna.RecordCount > 0 Then
                While Not rsAna.EOF
                    EliminaProduto rsAna!cod_analise
                    rsAna.MoveNext
                Wend
            End If
        End If
    End If
    
    ' ENCONTRA O CODIGO DO PRODUTO DA ANALISE EM CAUSA
    If Mid(cod_ana, 1, 1) = "S" Then
        CodProduto = ""
        ssql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(cod_ana)
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsAna.Open ssql, gConexao
        If rsAna.RecordCount > 0 Then
            CodProduto = BL_HandleNull(rsAna!cod_produto, "")
continuap:
            ' VERIFICA SE EXISTE PRODUTO NA FLEX GRID
            flg_existeProd = False
            For i = 1 To RegistosP.Count - 1
                If RegistosP(i).CodProd = CodProduto Then
                    flg_existeProd = True
                    Exit For
                End If
            Next
            If flg_existeProd = False Then
                rsAna.Close
                Set rsAna = Nothing
                Exit Sub
            End If
            ' ---------------------------------------
        End If
        rsAna.Close
        Set rsAna = Nothing
        
        ' SE NAO TEM PRODUTO CODIFICADO...SAI
        If CodProduto = "" Then
            Exit Sub
        End If
        
        flg_PodeEliminar = VerificaPodeEliminarProduto(CodProduto)
        
        
        ' NAO ENCONTROU MAIS NENHUMA ANALISE COM ESSE TUBO LOGO PODE ELIMINAR
        If flg_PodeEliminar = True Then
            
            ' VERIFICA SE JA FOI DADO ENTRADA DO PRODUTO
            If EcNumReq <> "" Then
                ssql = "SELECT * FROM sl_req_prod WHERE n_req =" & EcNumReq
                ssql = ssql & " AND cod_prod = " & BL_TrataStringParaBD(CodProduto)
                RsProd.CursorLocation = adUseServer
                RsProd.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros ssql
                RsProd.Open ssql, gConexao
                If RsProd.RecordCount > 0 Then
                    If BL_HandleNull(RsProd!dt_chega, "") <> "" Then
                        ' SE JA DEU ENTRADA NAO ELIMINA
                        RsProd.Close
                        Set RsProd = Nothing
                        Exit Sub
                    End If
                End If
                RsProd.Close
                Set RsProd = Nothing
                ' -----------------------------------------
            End If
            
            ' ELIMINA DA FLEX GRID E DA ESTRUTURA
            For i = 1 To RegistosP.Count - 1
                If RegistosP(i).CodProd = CodProduto Then
                    FgProd.row = 1
                    If i < FgProd.rows - 1 Then
                        RegistosP.Remove i
                        FgProd.RemoveItem i
                    Else
                        FgProd.TextMatrix(i, 0) = ""
                        FgProd.TextMatrix(i, 1) = ""
                        FgProd.TextMatrix(i, 2) = ""
                        FgProd.TextMatrix(i, 3) = ""
                        FgProd.TextMatrix(i, 4) = ""
                        FgProd.TextMatrix(i, 5) = ""
                        FgProd.TextMatrix(i, 6) = ""
                        FgProd.TextMatrix(i, 7) = ""
                        RegistosP(i).CodProd = ""
                        RegistosP(i).DescrProd = ""
                        RegistosP(i).EspecifObrig = ""
                        RegistosP(i).CodEspecif = ""
                        RegistosP(i).DescrEspecif = ""
                        RegistosP(i).DtPrev = ""
                        RegistosP(i).DtChega = ""
                        RegistosP(i).EstadoProd = ""
                        RegistosP(i).Volume = ""
                    End If
                    Exit For
                End If
            Next
        End If
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro no FormGestaoRequisicaoPrivado -> Elimina��o de Produtos: " & Err.Description
    Exit Sub
    Resume Next
End Sub


Private Function VerificaPodeEliminarProduto(CodProduto As String) As Boolean
    Dim i As Integer
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    
    VerificaPodeEliminarProduto = True
    
    For i = 1 To RegistosA.Count - 1
        
        If Mid(RegistosA(i).codAna, 1, 1) = "C" Then
            flg_Elimina = VerificaPodeEliminarProduto_C(CodProduto, RegistosA(i).codAna)
        ElseIf Mid(RegistosA(i).codAna, 1, 1) = "P" Then
            flg_Elimina = VerificaPodeEliminarProduto_P(CodProduto, RegistosA(i).codAna)
        Else
            flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, RegistosA(i).codAna)
        End If
        
        If flg_Elimina = False Then
            VerificaPodeEliminarProduto = flg_Elimina
            Exit Function
        End If
    Next
    VerificaPodeEliminarProduto = True
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto"
    VerificaPodeEliminarProduto = False
    Exit Function
    Resume Next
End Function

Private Function VerificaPodeEliminarProduto_C(CodProduto As String, analise As String) As Boolean
    Dim i As Integer
    Dim ssql As String
    Dim rsAna As New adodb.recordset
    Dim flg_Elimina As Boolean
     On Error GoTo TrataErro
   
    flg_Elimina = False
    
    For i = 1 To UBound(MaReq)
        If MaReq(i).cod_ana_c = analise And MaReq(i).cod_ana_s <> gGHOSTMEMBER_S Then
            flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, MaReq(i).cod_ana_s)
            If flg_Elimina = False Then
                VerificaPodeEliminarProduto_C = flg_Elimina
                Exit Function
            End If
        End If
    Next
    VerificaPodeEliminarProduto_C = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_C: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_C"
    VerificaPodeEliminarProduto_C = False
    Exit Function
    Resume Next
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarProduto_P(CodProduto As String, analise As String) As Boolean
    Dim i As Integer
    Dim ssql As String
    Dim rsAna As New adodb.recordset
    Dim flg_Elimina As Boolean
    On Error GoTo TrataErro
    
    flg_Elimina = False
    
    ssql = "SELECT cod_produto from sl_perfis WHERE cod_perfis = " & BL_TrataStringParaBD(analise)
    Set rsAna = New adodb.recordset
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAna.Open ssql, gConexao
    If Not rsAna.EOF Then
        If BL_HandleNull(rsAna!cod_produto, "") <> "" Then
            If rsAna!cod_produto <> CodProduto Then
                flg_Elimina = True
            End If
        Else
            For i = 1 To UBound(MaReq)
                If MaReq(i).Cod_Perfil = analise Then
                    If MaReq(i).cod_ana_c <> "0" And MaReq(i).cod_ana_c <> gGHOSTMEMBER_C And MaReq(i).cod_ana_s = gGHOSTMEMBER_S Then
                        flg_Elimina = VerificaPodeEliminarProduto_C(CodProduto, MaReq(i).cod_ana_c)
                    ElseIf MaReq(i).cod_ana_s <> gGHOSTMEMBER_S Then
                        flg_Elimina = VerificaPodeEliminarProduto_S(CodProduto, MaReq(i).cod_ana_s)
                    End If
                    
                    If flg_Elimina = False Then
                        VerificaPodeEliminarProduto_P = flg_Elimina
                    End If
                End If
            Next
        End If
        
    End If
    
        
    rsAna.Close
    Set rsAna = Nothing
    VerificaPodeEliminarProduto_P = flg_Elimina
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_P: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_P"
    VerificaPodeEliminarProduto_P = False
    Exit Function
    Resume Next
End Function


' FGONCALVES
' 30 JUN 2006 - CHVNG

Private Function VerificaPodeEliminarProduto_S(CodProduto As String, analise As String) As Boolean
    Dim ssql As String
    Dim rsAna As New adodb.recordset
    On Error GoTo TrataErro
    
    
    ssql = "SELECT * FROM sl_ana_s WHERE cod_ana_s = " & BL_TrataStringParaBD(analise)
    ssql = ssql & " AND cod_produto = " & BL_TrataStringParaBD(CodProduto)
    
    rsAna.CursorLocation = adUseServer
    rsAna.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAna.Open ssql, gConexao
    If rsAna.RecordCount > 0 Then
        VerificaPodeEliminarProduto_S = False
    Else
        VerificaPodeEliminarProduto_S = True
    End If
    
    rsAna.Close
    Set rsAna = Nothing

Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeEliminarProduto_S: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeEliminarProduto_S"
    VerificaPodeEliminarProduto_S = False
    Exit Function
    Resume Next
End Function


Sub Lista_GhostMembers(CodComplexa As String, complexa As String)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean
    Dim iRec As Long
    On Error GoTo TrataErro

    Dim i As Long
    Dim TotalElementosSel As Integer
    Dim indice As Long
    
    PesqRapida = False
    iRec = DevIndice(RegistosA(FGAna.row).codAna)
    ChavesPesq(1) = "cod_membro"
    CamposEcran(1) = "cod_membro"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "sl_membro, sl_ana_s "
    CampoPesquisa = "descr_ana_s"
    CWhere = " sl_membro.cod_membro = sl_ana_s.cod_ana_s and cod_ana_c = " & BL_TrataStringParaBD(CodComplexa)
    
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                        ChavesPesq, _
                                                                        CamposEcran, _
                                                                        CamposRetorno, _
                                                                        Tamanhos, _
                                                                        Headers, _
                                                                        CWhere, _
                                                                        CFrom, _
                                                                        "", _
                                                                        CampoPesquisa, _
                                                                        " ORDER BY sl_membro.ordem ", _
                                                                        " Membros da an�lise " & complexa)
    
    If PesqRapida = True Then

        FormPesqRapidaAvancadaMultiSel.Show vbModal
        
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
        If Not CancelouPesquisa Then
            For i = 1 To TotalElementosSel
                InsereGhostMember CStr(Resultados(i))
'                FormGestaoRequisicao.EcAuxAna.Enabled = True
'                FormGestaoRequisicao.EcAuxAna = Resultados(i)
'                FormGestaoRequisicao.EcAuxAna_KeyDown 13, 0
            Next i
            'SE COMPLEXA MARCADA PARA ENVIAR QUANTIDADE PARA FACTUS
            If IF_ContaMembros(CodComplexa, RegistosRM(iRec).ReciboEntidade) > 0 Then
                i = FGAna.row
                EliminaReciboManual iRec, RegistosA(i).codAna, False
                RegistosRM(iRec).ReciboQuantidade = TotalElementosSel
                RegistosRM(iRec).ReciboQuantidadeEFR = RegistosRM(iRec).ReciboQuantidade
                RegistosRM(iRec).ReciboTaxa = Replace(RegistosRM(iRec).ReciboTaxaUnitario, ".", ",") * TotalElementosSel
                FGAna.TextMatrix(i, lColFgAnaPreco) = RegistosRM(iRec).ReciboTaxa
                AdicionaAnaliseAoReciboManual iRec, False
            End If
            FormGestaoRequisicaoPrivado.FGAna.SetFocus
        Else
        i = FGAna.row
        Dim ana As String
        ana = RegistosA(i).codAna
        If EliminaReciboManual(iRec, ana, True) = True Then
            LimpaColeccao RegistosRM, iRec
            FGAna.row = i
            Elimina_Mareq RegistosA(i).codAna
            ana = RegistosA(i).codAna
            RegistosA.Remove i
            If RegistosA.Count = 0 Then
                CriaNovaClasse RegistosA, 1, "ANA"
            End If
            If FGAna.rows = 2 Then
                FGAna.AddItem ""
            End If
            FGAna.RemoveItem i
            EliminaTuboEstrut
            EliminaProduto ana
        End If
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem membros", vbExclamation, "ATEN��O"
        If gFormActivo.Name = "FormGestaoRequisicaoPrivado" Then
            FormGestaoRequisicaoPrivado.FGAna.SetFocus
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Lista_GhostMembers: " & Err.Number & " - " & Err.Description, Me.Name, "Lista_GhostMembers"
    Exit Sub
    Resume Next
End Sub


Private Function DevolveDataValidade(n_req As String) As String
    Dim sql As String
    Dim rs As New adodb.recordset
    On Error GoTo TrataErro
    
    
    '----------------------------------
    ' DATA VALIDADE ACTUAL
    sql = "SELECT dt_conclusao FROM sl_requis WHERE n_req = " & n_req
    Set rs = New adodb.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    rs.Open sql, gConexao
    If rs.RecordCount > 0 Then
        DevolveDataValidade = BL_HandleNull(rs!dt_conclusao, CDate(1))
    End If
    rs.Close
    Set rs = Nothing
    '----------------------------------
Exit Function
TrataErro:
    BG_LogFile_Erros "DevolveDataValidade: " & Err.Number & " - " & Err.Description, Me.Name, "DevolveDataValidade"
    DevolveDataValidade = ""
    Exit Function
    Resume Next
End Function


' pferreira 2010.03.08
' TICKET GLINTTHS-263.
Public Sub RegistaEliminadas(Optional elimina_todas As Boolean)
    
    Dim i As Integer
    Dim total As Integer
    
    Dim ssql As String
    If (elimina_todas) Then
        total = FGAna.rows - 2
    Else
        total = TotalEliminadas
    End If
    If EcNumReq <> "" Then
        For i = 1 To total
            If (Not elimina_todas) Then
                BL_RegistaAnaEliminadas EcNumReq, Eliminadas(i).codAna, "", "", ""
            Else
                BL_RegistaAnaEliminadas EcNumReq, FGAna.TextMatrix(i, 0), "", "", ""
            
            End If
        Next i
    End If
    TotalEliminadas = 0
    ReDim Eliminadas(0)
    
End Sub

Sub RegistaAcrescentadas()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim t_urg As Integer
    Dim t_sit As Integer
    If CbUrgencia.ListIndex = mediComboValorNull Then
        t_urg = mediComboValorNull
    Else
        t_urg = CbUrgencia.ItemData(CbUrgencia.ListIndex)
    End If
    t_sit = mediComboValorNull
    If EcNumReq <> "" Then
        For i = 1 To TotalAcrescentadas
            BL_RegistaAnaAcrescentadas EcSeqUtente.text, EcNumReq, "", "", "", Acrescentadas(i).cod_agrup, dataAct, Bg_DaHora_ADO, t_urg, t_sit
        Next i
    End If
    TotalAcrescentadas = 0
    ReDim Acrescentadas(0)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "RegistaAcrescentadas: " & Err.Number & " - " & Err.Description, Me.Name, "RegistaAcrescentadas"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' IMPRIME FOLHA DE RESUMO ATRAV�S DO CRYSTAL REPORTS

' -----------------------------------------------------------------
Private Sub ImprimeResumoCrystal()
    Dim continua As Boolean
    Dim ObsUtente As String
    Dim codProfis As String
    Dim descrProfis As String
    Dim ssql As String
    On Error GoTo TrataErro
    
    continua = BL_IniciaReport("FolhaResumo", "Folha de Resumo", crptToPrinter)
    
    If continua = False Then Exit Sub
    BL_MudaCursorRato mediMP_Espera, Me
    
    ssql = "DELETE FROM sl_cr_folha_Resumo WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO ssql
    
    ImprimeResumoProdutos
    ImprimeResumoAnalises
    ImprimeResumoTubos
    
    'soliveira terrugem
    Dim rsOBS As adodb.recordset
    Set rsOBS = New adodb.recordset
    rsOBS.CursorLocation = adUseServer
    rsOBS.CursorType = adOpenStatic
    rsOBS.Source = "select obs,cod_profis_ute from sl_identif where seq_utente = " & EcSeqUtente
    If gModoDebug = mediSim Then BG_LogFile_Erros rsOBS.Source
    rsOBS.Open , gConexao
    If rsOBS.RecordCount > 0 Then
        ObsUtente = BL_HandleNull(rsOBS!obs, "")
        codProfis = BL_HandleNull(rsOBS!cod_profis_ute, "")
        If codProfis <> "" Then
            descrProfis = BL_SelCodigo("sl_profis", "DESCR_PROFIS", "COD_PROFIS", codProfis, "V")
        End If
    End If
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = ""
    
    'F�rmulas do Report
    Report.formulas(0) = "NumReq=" & BL_TrataStringParaBD("" & EcNumReq)
    Report.formulas(1) = "NomeUtente=" & BL_TrataStringParaBD("" & EcNome)
    Report.formulas(2) = "DtNascUtente=" & BL_TrataStringParaBD("" & EcDataNasc)
    Report.formulas(3) = "DtChegada=" & BL_TrataStringParaBD("" & EcDataChegada)
    Report.formulas(4) = "Processo=" & BL_TrataStringParaBD("" & EcUtente)
    Report.formulas(5) = "DtValidade=" & BL_TrataStringParaBD("" & DevolveDataValidade(CStr(gRequisicaoActiva)))
    Report.formulas(6) = "NSC=" & BL_TrataStringParaBD("" & EcUtente)
    Report.formulas(7) = "Servico=''"
    Report.formulas(8) = "SeqUtente=" & BL_TrataStringParaBD("" & EcSeqUtente)
    If gListaMedicos = mediSim Then
        If EcNomeMedico = "" And EcCodMedico <> "" Then
            EcCodMedico_Validate False
        End If
        Report.formulas(9) = "Medico=" & BL_TrataStringParaBD("" & Trim(EcNomeMedico))
    Else
        Report.formulas(9) = "Medico=" & BL_TrataStringParaBD("" & Trim(EcDescrMedico))
    End If
    Report.formulas(10) = "Idade=" & BL_TrataStringParaBD("" & EcIdade)
    Report.formulas(11) = "EFR=" & BL_TrataStringParaBD("" & EcDescrEFR)
    Report.formulas(12) = "Sala=" & BL_TrataStringParaBD("" & EcDescrSala)
    Report.formulas(13) = "Prioridade=" & BL_TrataStringParaBD("" & CbUrgencia)
    'soliveira terrugem
    Report.formulas(14) = "ObsUtente=" & BL_TrataStringParaBD("" & ObsUtente)
    Report.formulas(15) = "ReqAssoc=" & BL_TrataStringParaBD("" & EcNumReqAssoc)
    Report.formulas(16) = "InfComplementar=" & BL_TrataStringParaBD("" & Replace(EcinfComplementar, vbCrLf, ""))
    Report.formulas(17) = "descrProfis=" & BL_TrataStringParaBD("" & descrProfis)
    Report.SubreportToChange = "analises"
    Report.SelectionFormula = "{sl_cr_folha_resumo.nome_computador} = '" & BG_SYS_GetComputerName & "' AND trim({sl_cr_folha_resumo.cod_ana})<>'' AND {sl_cr_folha_resumo.n_req} = " & gRequisicaoActiva
    Report.Connect = "DSN=" & gDSN
    Report.SubreportToChange = "SubRepTubos"
    Report.SelectionFormula = "{sl_cr_folha_resumo.nome_computador} = '" & BG_SYS_GetComputerName & "' AND trim({sl_cr_folha_resumo.tubo}) <> '' AND {sl_cr_folha_resumo.n_req} = " & gRequisicaoActiva
    Report.Connect = "DSN=" & gDSN
    Report.SubreportToChange = ""
    


    Call BL_ExecutaReport
    BL_MudaCursorRato mediMP_Activo, Me
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoCrystal: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoCrystal"
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARIA AS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub ImprimeResumoAnalises()
    Dim i As Integer
    Dim ssql As String
    On Error GoTo TrataErro
    If FGAna.rows > 2 Then
        BG_BeginTransaction
        gSQLError = 0
        For i = 1 To FGAna.rows - 2
            ssql = "INSERT INTO sl_cr_folha_resumo (nome_computador, n_req,cod_ana,descr_ana, ordem) VALUES("
            ssql = ssql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
            ssql = ssql & gRequisicaoActiva & ", "
            ssql = ssql & BL_TrataStringParaBD(FGAna.TextMatrix(i, lColFgAnaCodigo)) & ", "
            ssql = ssql & BL_TrataStringParaBD(FGAna.TextMatrix(i, lColFgAnaDescricao)) & "," & i & ")"
            
            BG_ExecutaQuery_ADO ssql
        Next
        If gSQLError = 0 Then
            BG_CommitTransaction
        Else
            BG_RollbackTransaction
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoAnalises: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoAnalises"
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARAIA OS PRODUTOS DAS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub ImprimeResumoProdutos()
    Dim i As Integer
    Dim ssql As String
    On Error GoTo TrataErro
        
    If FgProd.rows > 2 Then
        BG_BeginTransaction
        gSQLError = 0
        For i = 1 To FgProd.rows - 2
            ssql = "INSERT INTO sl_cr_folha_resumo (nome_computador, n_req,produto) VALUES("
            ssql = ssql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
            ssql = ssql & gRequisicaoActiva & ", "
            ssql = ssql & BL_TrataStringParaBD(FgProd.TextMatrix(i, 1)) & ")"
            
            BG_ExecutaQuery_ADO ssql
        Next
        If gSQLError = 0 Then
            BG_CommitTransaction
        Else
            BG_RollbackTransaction
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoProdutos: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoProdutos"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARAIA OS TUBOS DAS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub ImprimeResumoTubos()
    Dim i As Integer
    Dim ssql As String
    On Error GoTo TrataErro
        
    If FGTubos.rows > 2 Then
        BG_BeginTransaction
        gSQLError = 0
        For i = 1 To FGTubos.rows - 2
            ssql = "INSERT INTO sl_cr_folha_resumo (nome_computador, n_req,tubo, norma_colheita) VALUES("
            ssql = ssql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
            ssql = ssql & gRequisicaoActiva & ", "
            ssql = ssql & BL_TrataStringParaBD(gEstruturaTubos(i).Designacao_tubo) & ","
            ssql = ssql & BL_TrataStringParaBD(gEstruturaTubos(i).normaTubo) & ")"
            
            BG_ExecutaQuery_ADO ssql
        Next
        If gSQLError = 0 Then
            BG_CommitTransaction
        Else
            BG_RollbackTransaction
        End If
    End If
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "ImprimeResumoTubos: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeResumoTubos"
    Exit Sub
    Resume Next
End Sub
Function ProdutoUnico(GrupoAna As String) As Boolean
    Dim RsProdU As adodb.recordset
    Dim sql As String
    Dim ret As Boolean
    On Error GoTo TrataErro
    
    
    ret = True
    
    sql = "select flg_produnico from sl_gr_ana where cod_gr_ana = " & BL_TrataStringParaBD(GrupoAna)
    
    Set RsProdU = New adodb.recordset
    RsProdU.CursorLocation = adUseServer
    RsProdU.CursorType = adOpenStatic
    RsProdU.Source = "select flg_produnico from sl_gr_ana where cod_gr_ana = " & BL_TrataStringParaBD(GrupoAna)
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsProdU.Open sql, gConexao
    If Not RsProdU.EOF Then
        If BL_HandleNull(RsProdU!flg_produnico, "") = "1" Then
            ret = True
        Else
            ret = False
        End If
    End If
    
    ProdutoUnico = ret
    
    RsProdU.Close
    Set RsProdU = Nothing
    
Exit Function
TrataErro:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : FormGestaoRequisicaoPrivado (ProdutoUnico) -> " & Err.Description)
            ProdutoUnico = False
            Exit Function
    End Select
End Function


''''''''''''''''''''''''''''''''''''''''''''
' Verifica se requisicao vai para a fila '''
''''''''''''''''''''''''''''''''''''''''''''
Private Function IsToInsert() As Boolean
    On Error GoTo TrataErro
    
    Dim rsSala As New adodb.recordset
    Dim ssql As String
    If (EcCodSala = "") Then
        IsToInsert = True
    Else
        ssql = "select flg_colheita from sl_cod_salas where cod_sala = " & EcCodSala
        rsSala.CursorLocation = adUseServer
        rsSala.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsSala.Open ssql, gConexao
        If BL_HandleNull(rsSala!flg_colheita, 0) = mediSim Then
            IsToInsert = True
        Else
            IsToInsert = False
        End If
        
        If (rsSala.state = adStateOpen) Then
            rsSala.Close
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "IsToInsert: " & Err.Number & " - " & Err.Description, Me.Name, "IsToInsert"
    IsToInsert = False
    Exit Function
    Resume Next
End Function

''''''''''''''''''''''''''''''''''''''''''''
' Insere requisicao em fila de espera ''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub InsertFilaEspera()
    
    Dim sql As String
    Dim morada As String
    Dim Sexo As String
    Dim efr As String
    Dim RsStack As adodb.recordset
    Set RsStack = New adodb.recordset
        
    On Error GoTo Trata_Erro
    If EcCodSala <> "" Then
        If BL_HandleNull(BL_SelCodigo("SL_COD_SALAS", "FLG_COLHEITA", "COD_SALA", EcCodSala), "0") = "0" Then
            Exit Sub
        End If
    End If
    ' Procura dados sobre o utente
    If gSGBD = gPostGres Then
        sql = "SELECT sl_identif.descr_mor_ute, sl_identif.sexo_ute, sl_identif.cod_efr_ute, descr_efr FROM sl_identif, sl_efr " & _
                 "WHERE seq_utente = " & EcSeqUtente & " AND cod_efr = to_number(cod_efr_ute,'999999')"
    Else
        sql = "SELECT sl_identif.descr_mor_ute, sl_identif.sexo_ute, sl_identif.cod_efr_ute, descr_efr FROM sl_identif, sl_efr " & _
                 "WHERE seq_utente = " & EcSeqUtente & " AND cod_efr = cod_efr_ute"
    End If
    
    If gModoDebug = mediSim Then BG_LogFile_Erros CStr(sql)
    RsStack.Open sql, gConexao, adOpenStatic, adLockOptimistic
    
    If (RsStack.RecordCount > 0) Then
        morada = BL_TrataStringParaBD(BL_HandleNull(RsStack!descr_mor_ute, ""))
        Sexo = BL_SelCodigo("SL_TBF_SEXO", "DESCR_SEXO", "COD_SEXO", BL_HandleNull(RsStack!sexo_ute, 0))
        efr = BL_TrataStringParaBD(BL_HandleNull(RsStack!descr_efr, ""))
    Else
        morada = "''"
        Sexo = "''"
        efr = "''"
    End If
    
    ' Apaga a requisicao existentes com a requisicao corrente
    BG_ExecutaQuery_ADO "DELETE FROM sl_fila_espera WHERE n_req = " & EcNumReq
    BG_Trata_BDErro
    
    ' Insere nova requisicao em fila de espera
    If (RsStack.state = adStateOpen) Then: RsStack.Close
    sql = "INSERT INTO sl_fila_espera (n_req,nome_utente,seq_utente," & _
        "cod_local,cod_sala,prioridade,dt_cri,hr_cri,user_cri,dt_nasc,idade,sexo,entidade,morada," & _
        "destino,medico,flg_proc, user_registo) VALUES (" & EcNumReq & "," & BL_TrataStringParaBD(EcNome) & "," & _
        EcSeqUtente & "," & gCodLocal & "," & BL_HandleNull(EcCodSala, "0") & "," & BL_HandleNull(EcPrColheita, 2) & _
        "," & BL_TrataDataParaBD(dataAct) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & _
        "," & EcUtilizadorCriacao & "," & BL_TrataDataParaBD(EcDataNasc.text) & "," & _
        BL_TrataStringParaBD(BG_CalculaIdade(CDate(EcDataNasc.text))) & "," & _
        BL_TrataStringParaBD(Sexo) & "," & BL_TrataStringParaBD(efr) & "," & BL_TrataStringParaBD(morada) & "," & BL_TrataStringParaBD(CbDestino) & "," & BL_TrataStringParaBD(EcDescrMedico.text) & ",0," & _
        BL_TrataStringParaBD(BL_HandleNull(EcUtilizadorCriacao, gCodUtilizador)) & ")"

    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsStack.Open sql, gConexao, adOpenStatic, adLockOptimistic

    ' Fecha conexao
    If (RsStack.state = adStateOpen) Then: RsStack.Close
    Exit Sub
    
Trata_Erro:
    BG_Mensagem mediMsgBox, "Erro ao inserir em fila de espera!", vbCritical + vbOKOnly, App.ProductName
    BG_LogFile_Erros "FormGestaoRequisicaoPrivado: InsertFilaEspera -> " & Err.Description
    BG_RollbackTransaction
    
    ' Fecha conexao
    If (RsStack.state = adStateOpen) Then: RsStack.Close
    Exit Sub
    Resume Next
End Sub



' PFerreira 08.03.2007
Private Sub CbPrioColheita_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    BG_LimpaOpcao CbPrioColheita, KeyCode
    If (IsChild) Then
        EcPrColheita.text = "4"
    Else
        EcPrColheita.text = "2"
    End If

Exit Sub
TrataErro:
    BG_LogFile_Erros "CbPrioColheita_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbPrioColheita_KeyDown"
    Exit Sub
    Resume Next
End Sub
' PFerreira 08.03.2007
Private Sub CbPrioColheita_Click()
    On Error GoTo TrataErro
    If (CbPrioColheita.ListIndex >= 0) Then: EcPrColheita.text = CbPrioColheita.ItemData(CbPrioColheita.ListIndex)
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbPrioColheita_Click: " & Err.Number & " - " & Err.Description, Me.Name, "CbPrioColheita_Click"
    Exit Sub
    Resume Next
End Sub

' PFerreira 16.03.2007
Private Sub CbSala_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    BG_LimpaOpcao CbSala, KeyCode
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbSala_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbSala_KeyDown"
    Exit Sub
    Resume Next
End Sub

' PFerreira 15.03.2007
Private Function IsChild() As Boolean
    On Error GoTo TrataErro
    If EcDataNasc <> "" Then
        If InStr(1, Trim(Replace(Replace(BG_CalculaIdade(CDate(EcDataNasc)), "anos", ""), "ano", "")), "meses") = 0 Then
            If Trim(Replace(Replace(Replace(BG_CalculaIdade(CDate(EcDataNasc)), "anos", ""), "dia", ""), "ano", "")) <= 13 Then
                IsChild = True
            Else
                IsChild = False
            End If
        Else
            IsChild = True
        End If
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "IsChild: " & Err.Number & " - " & Err.Description, Me.Name, "IsChild"
    IsChild = False
    Exit Function
    Resume Next
End Function



' PFerreira 19.04.2007
' Activa ou desactiva controlos de morada
Private Sub EnableAddress(Enabled As Boolean)
    On Error GoTo TrataErro

    EcMorada.Enabled = Enabled
    EcCodPostal.Enabled = Enabled
    EcRuaPostal.Enabled = Enabled
    EcDescrPostal.Enabled = Enabled
    BtPesqCodPostal.Enabled = Enabled
Exit Sub
TrataErro:
    BG_LogFile_Erros "EnableAddress: " & Err.Number & " - " & Err.Description, Me.Name, "EnableAddress"
    Exit Sub
    Resume Next
End Sub


' PFerreira 19.04.2007
' Limpa controlos de morada
Private Sub LimpaMorada()
    On Error GoTo TrataErro

    EcMorada.text = ""
    EcCodPostal.text = ""
    EcRuaPostal.text = ""
    EcDescrPostal.text = ""
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaMorada: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaMorada"
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesqCodPostal_Click()
    On Error GoTo TrataErro

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    
    If Trim(EcDescrPostal) = "" And EcCodPostal = "" Then
        BG_Mensagem mediMsgBox, "Preencha a descri��o com uma localidade (ou parte da descri��o de uma), para limitar a lista de c�digos postais.", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
        Exit Sub
    End If
    
    PesqRapida = False
    
    If EcDescrPostal <> "" Then
        ChavesPesq(1) = "descr_postal"
        CamposEcran(1) = "descr_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Descri��o"
        
        ChavesPesq(2) = "cod_postal"
        CamposEcran(2) = "cod_postal"
        Tamanhos(2) = 2000
        Headers(2) = "C�digo"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        ChavesPesq(1) = "cod_postal"
        CamposEcran(1) = "cod_postal"
        Tamanhos(1) = 2000
        Headers(1) = "Codigo"
        
        ChavesPesq(2) = "descr_postal"
        CamposEcran(2) = "descr_postal"
        Tamanhos(2) = 2000
        Headers(2) = "Descr��o"
    End If
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_cod_postal"
    If EcDescrPostal <> "" And EcCodPostal = "" Then
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
    ElseIf EcDescrPostal = "" And EcCodPostal <> "" Then
        CWhere = "UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    Else
        CWhere = "UPPER(descr_postal) LIKE '%" & UCase(EcDescrPostal) & "%'"
        CWhere = CWhere & " AND UPPER(cod_postal) LIKE '" & UCase(EcCodPostal) & "%'"
    End If
    CampoPesquisa = "descr_postal"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_postal ", " C�digos Postais")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            i = InStr(1, Resultados(2), "-") - 1
            If i = -1 Then
                i = InStr(1, Resultados(1), "-") - 1
            End If
            If EcDescrPostal <> "" Then
                If i = -1 Then
                    s1 = Resultados(2)
                    s2 = ""
                Else
                    s1 = Mid(Resultados(2), 1, i)
                    s2 = Mid(Resultados(2), InStr(1, Resultados(2), "-") + 1)
                End If
                EcCodPostal.text = s1
                EcRuaPostal.text = s2
                EcCodPostalAux.text = Trim(Resultados(2))
                EcDescrPostal.text = Resultados(1)
                EcDescrPostal.SetFocus
            ElseIf EcDescrPostal = "" Then
                If i = -1 Then
                    s1 = Resultados(1)
                    s2 = ""
                Else
                    s1 = Mid(Resultados(1), 1, i)
                    s2 = Mid(Resultados(1), InStr(1, Resultados(1), "-") + 1)
                End If
                EcCodPostal.text = s1
                EcRuaPostal.text = s2
                EcCodPostalAux.text = Trim(Resultados(1))
                EcDescrPostal.text = Resultados(2)
                EcDescrPostal.SetFocus
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "C�digo postal n�o encontrado!", vbInformation, App.ProductName
        EcDescrPostal.SetFocus
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesqCodPostal_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesqCodPostal_Click"
    Exit Sub
    Resume Next
End Sub
' PFerreira 04.04.2007
Private Sub CbDestino_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    BG_LimpaOpcao CbDestino, KeyCode
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbDestino_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbDestino_KeyDown"
    Exit Sub
    Resume Next
End Sub
' PFerreira 04.04.2007
Private Sub Cbhemodialise_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    BG_LimpaOpcao CbHemodialise, KeyCode
Exit Sub
TrataErro:
    BG_LogFile_Erros "Cbhemodialise_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "Cbhemodialise_KeyDown"
    Exit Sub
    Resume Next
End Sub


Private Sub PesquisaEntFin()
    PA_PesquisaEFR EcCodEfr2, EcDescrEfr2, CStr(gCodLocal)
End Sub


Sub PreencheCodigoPostal()
    On Error GoTo TrataErro
    
    Dim rsCodigo As adodb.recordset
    Dim i As Integer
    Dim s1 As String
    Dim s2 As String
    Dim sql As String
    
    If EcCodPostalAux.text <> "" Then
        Set rsCodigo = New adodb.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal = " & BL_TrataStringParaBD(EcCodPostal & "-" & EcCodPostalAux)
        
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "C�digo do c�digo postal inexistente!", vbExclamation, App.ProductName
        Else
            i = InStr(1, rsCodigo!cod_postal, "-") - 1
            If i = -1 Then
                s1 = rsCodigo!cod_postal
                s2 = ""
            Else
                s1 = Mid(rsCodigo!cod_postal, 1, i)
                s2 = Mid(rsCodigo!cod_postal, InStr(1, rsCodigo!cod_postal, "-") + 1)
            End If
            EcCodPostal.text = s1
            EcRuaPostal.text = s2
            EcDescrPostal.text = Trim(rsCodigo!descr_postal)
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheCodigoPostal: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheCodigoPostal"
    Exit Sub
    Resume Next
End Sub
Private Sub EcRuaPostal_Validate(Cancel As Boolean)
    On Error GoTo TrataErro
    
    Dim Postal As String
    Dim sql As String
    Dim rsCodigo As adodb.recordset
    
    EcRuaPostal.text = UCase(EcRuaPostal.text)
    If Trim(EcCodPostal) <> "" Then
        If Trim(EcRuaPostal) <> "" Then
            Postal = Trim(EcCodPostal.text) & "-" & Trim(EcRuaPostal.text)
        Else
            Postal = EcCodPostal
        End If
        
        sql = "SELECT " & _
              "     cod_postal,descr_postal " & _
              "FROM " & _
              "     sl_cod_postal " & _
              "WHERE " & _
              "     cod_postal=" & BL_TrataStringParaBD(Postal)
        
        Set rsCodigo = New adodb.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        rsCodigo.Open sql, gConexao
        
        If rsCodigo.RecordCount <= 0 Then
            Cancel = True
            'BG_Mensagem mediMsgBox, "C�digo postal inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcDescrPostal = ""
            EcRuaPostal = ""
            EcCodPostal = ""
            EcCodPostalAux = ""
        Else
            EcCodPostalAux.text = Postal
            EcDescrPostal.text = rsCodigo!descr_postal
        End If
        rsCodigo.Close
        Set rsCodigo = Nothing
    Else
        EcDescrPostal.text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcRuaPostal_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcRuaPostal_Validate"
    Exit Sub
    Resume Next
End Sub

Private Sub BtGerarRecibo_click()
    On Error GoTo TrataErro
    Dim ssql As String
   Dim i As Integer
   Dim flg_adiantamentos As Boolean
    If EcEstadoReq = gEstadoReqBloqueada Then
        MsgBox "Requisi��o Bloqueada. Altera��es n�o podem ser gravadas", vbExclamation, " Requisi��o Bloqueada"
        Exit Sub
    End If
    flg_adiantamentos = False
    For i = 1 To RegistosAD.Count
        If RegistosAD.Item(i).Estado = gEstadoReciboPago Then
            MsgBox "Existem adiantamento(s) emitido(s). Todas faturas/recibo emitidas ficar�o em cobran�a. ", vbExclamation, " Adiantamento(s)"
            flg_adiantamentos = True
        End If
    Next i
    If EcDataChegada.text = "" Then
        BG_Mensagem mediMsgBox, "Requisi��o sem data de chegada. ", vbInformation, "Aten��o"
        Sendkeys ("{HOME}+{END}")
        EcDataChegada.text = ""
        EcDataChegada.SetFocus
        Exit Sub
    End If
    If gPassaRecFactus <> mediSim Then
        For i = 1 To RegistosR.Count - 1
            If RegistosR(i).codEntidade = RegistosR(FGRecibos.row).codEntidade And RegistosR(i).SerieDoc = RegistosR(FGRecibos.row).SerieDoc And RegistosR(i).NumDoc = RegistosR(FGRecibos.row).NumDoc Then
                VerificaUtentePagaActoMedico RegistosR(i).codEntidade, RegistosR(i).SerieDoc, RegistosR(i).NumDoc
                If RegistosR(i).Estado = gEstadoReciboCobranca Or RegistosR(i).Estado = gEstadoReciboPago Then
                    Exit Sub
                End If
            End If
        Next
    Else
        FACTUS_AtualizaDataIniReal
        FACTUS_VerificaUtentePagaActoMedico
        AtualizaFgRecibosNew

    End If
    If (EcNumReq <> "" And gNReqPreImpressa <> mediSim) Or (gNReqPreImpressa = mediSim And EcUtilizadorCriacao <> "") Then
        FuncaoModificar True
    ElseIf EcNumReq = "" Or (gNReqPreImpressa = mediSim And EcUtilizadorCriacao = "") Then
        FuncaoInserir True
    End If
    flgEmissaoRecibos = True
    FormEmissaoRecibos.Form = Me.Name
    FormEmissaoRecibos.Show 'vbModal
    FormEmissaoRecibos.flg_adiantamentos = flg_adiantamentos
Exit Sub
TrataErro:
    Call BG_LogFile_Erros("FormGestaoRequisicaoPrivado - CriaRecibo: " & Err.Description)
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------

' LIMPA FGRECIBOS

' -----------------------------------------------------------------------------------------------
Sub LimpaFgRecibos()
     On Error GoTo TrataErro
   Dim j As Long
    
    ExecutaCodigoR = False
    j = FGRecibos.rows - 1
    While j > 0
        If j > 1 Then
            FGRecibos.RemoveItem j
        Else
            FGRecibos.TextMatrix(j, 0) = ""
            FGRecibos.TextMatrix(j, 1) = ""
            FGRecibos.TextMatrix(j, 2) = ""
            FGRecibos.TextMatrix(j, 3) = ""
            FGRecibos.TextMatrix(j, 4) = ""
            FGRecibos.TextMatrix(j, 5) = ""
            FGRecibos.TextMatrix(j, 6) = ""
            BL_MudaCorFg FGRecibos, j, vbWhite
        End If
        j = j - 1
    Wend
    
    ExecutaCodigoR = True
    LastColR = 0
    LastRowR = 1
    CriaNovaClasse RegistosR, 1, "REC", True
    CriaNovaClasse RegistosRM, 1, "REC_MAN", True
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgRecibos: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgRecibos"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------

' LIMPA FGRECIBOS

' -----------------------------------------------------------------------------------------------
Sub LimpaFgAdiantamentos()
   On Error GoTo TrataErro
   Dim j As Long
    Dim i As Integer
    
    j = FgAdiantamentos.rows - 1
    While j > 0
        If j > 1 Then
            FgAdiantamentos.RemoveItem j
        Else
            For i = 0 To FgAdiantamentos.Cols - 1
                FGRecibos.TextMatrix(j, i) = ""
            Next
        End If
        j = j - 1
    Wend
    
    CriaNovaClasse RegistosAD, 1, "ADI", True
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgAdiantamentos: " & Err.Number & " - " & Err.Description, Me.Name, "LimpaFgAdiantamentos"
    Exit Sub
    Resume Next
End Sub



' -----------------------------------------------------------------------------------------------

' QUANDO ADICIONA UMA NOVA ANALISE, ADICIONA TAMBEM RESPECTIVA TAXA NA ESTRUTURA DE RECIBOS

' -----------------------------------------------------------------------------------------------

Private Sub AdicionaAnaliseAoReciboManual(indice As Long, MudaEstadoPerdidas As Boolean)
    On Error GoTo TrataErro
    Dim i As Long
    Dim Flg_inseriuEntidade As Boolean
    Dim Flg_EntidadeJaEmitida As Boolean
    Flg_inseriuEntidade = False
    
    ' ------------------------------------------------------------------------------------------
    ' SE TAXA FOR 0 NAO E NECESSARIO ACRESCENTAR
    ' ------------------------------------------------------------------------------------------

    If RegistosRM(indice).ReciboCodFacturavel <> RegistosRM(indice).ReciboCodAna Then
        For i = 1 To indice
            If RegistosRM(i).ReciboCodFacturavel = RegistosRM(indice).ReciboCodFacturavel Then
                If RegistosRM(i).ReciboCodAna <> RegistosRM(indice).ReciboCodAna Then
                    Exit Sub
                End If
            End If
        Next
    End If
    
    If RegistosR.Count = 0 Then
        CriaNovaClasse RegistosR, i + 1, "REC"
    End If
    Flg_EntidadeJaEmitida = False
    For i = 1 To RegistosR.Count
        ' ------------------------------------------------------------------------------------------
        ' ENTIDADE JA EXISTE NA ESTRUTURA
        ' ------------------------------------------------------------------------------------------
        If RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade Then
            If RegistosR(i).Estado = gEstadoReciboNaoEmitido Or RegistosR(i).Estado = gEstadoReciboNulo Or (RegistosR(i).Estado = gEstadoReciboPerdido And MudaEstadoPerdidas = True) Then
                'NELSONPSILVA GLINTT-HS-21312
                RegistosRM(indice).ReciboTaxa = RegistosRM(indice).ReciboTaxa * RegistosRM(indice).ReciboQuantidade
                '
                RegistosR(i).ValorPagar = RegistosR(i).ValorPagar + Round(((RegistosRM(indice).ReciboTaxa * (100 - RegistosR(i).Desconto)) / 100), 2)
                RegistosR(i).ValorOriginal = RegistosR(i).ValorOriginal + RegistosRM(indice).ReciboTaxa
                RegistosR(i).NumAnalises = RegistosR(i).NumAnalises + 1
                FGRecibos.TextMatrix(i, lColRecibosValorPagar) = BG_CvDecimalParaCalculo(RegistosR(i).ValorPagar)
                'NELSONPSILVA GLINTT-HS-21312
                FGAna.TextMatrix(indice, lColFgAnaPreco) = BG_CvDecimalParaCalculo(RegistosRM(indice).ReciboTaxa)
                '
                If RegistosR(i).ValorPagar > 0 Then
                
                    RegistosR(i).Estado = gEstadoReciboNaoEmitido
                    FGRecibos.TextMatrix(i, lColRecibosEstado) = "N�o Emitido"
                ElseIf RegistosR(i).ValorPagar = 0 Then
                    RegistosR(i).Estado = gEstadoReciboNulo
                    FGRecibos.TextMatrix(i, lColRecibosEstado) = "Recibo Nulo"
                End If
                Flg_inseriuEntidade = True
                If RegistosR(i).ValorPagar > 0 Then
                    RegistosR(i).Estado = gEstadoReciboNaoEmitido
                Else
                    RegistosR(i).Estado = gEstadoReciboNulo
                End If
                If RegistosRM(indice).ReciboIsencao = gTipoIsencaoBorla Then
                    RegistosR(i).FlgBorla = mediSim
                    FGRecibos.TextMatrix(i, lColRecibosBorla) = "Sim"
                End If
            ElseIf RegistosR(i).Estado = gEstadoReciboPerdido Then
                RegistosR(i).ValorPagar = (RegistosR(i).ValorPagar) + (RegistosRM(indice).ReciboTaxa)
                RegistosR(i).ValorOriginal = (RegistosR(i).ValorOriginal) + (RegistosRM(indice).ReciboTaxa)
                RegistosR(i).NumAnalises = RegistosR(i).NumAnalises + 1
                FGRecibos.TextMatrix(i, lColRecibosValorPagar) = RegistosR(i).ValorPagar
                Flg_inseriuEntidade = True
                Flg_EntidadeJaEmitida = True
                If RegistosRM(indice).ReciboIsencao = gTipoIsencaoBorla Then
                    RegistosR(i).FlgBorla = mediSim
                    FGRecibos.TextMatrix(i, lColRecibosBorla) = "Sim"
                End If
            ElseIf RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca Then
                Flg_EntidadeJaEmitida = True
            End If
            
        End If
    Next
    i = i - 1
    ' ------------------------------------------------------------------------------------------
    ' ENTIDADE NAO EXISTE NA ESTRUTURA
    ' ------------------------------------------------------------------------------------------
    If Flg_inseriuEntidade = False Then
        RegistosR(i).SeqRecibo = RECIBO_RetornaSeqRecibo
        RegistosR(i).NumDoc = 0
        RegistosR(i).SerieDoc = "0"
        RegistosR(i).codEntidade = RegistosRM(indice).ReciboEntidade
        RegistosR(i).DescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(indice).ReciboEntidade)
        RegistosR(i).codEmpresa = BL_SelCodigo("SL_EFR", "cod_empresa", "COD_EFR", RegistosRM(indice).ReciboEntidade)
        'NELSONPSILVA GLINTT-HS-21312
        RegistosR(i).ValorPagar = BG_CvDecimalParaCalculo(RegistosRM(indice).ReciboTaxa * RegistosRM(indice).ReciboQuantidade)
        RegistosRM(i).ReciboTaxa = RegistosRM(i).ReciboTaxa * RegistosRM(indice).ReciboQuantidade
        '
        RegistosR(i).ValorOriginal = BG_CvDecimalParaCalculo(RegistosRM(indice).ReciboTaxa)
        RegistosR(i).Caucao = 0
        RegistosR(i).UserEmi = ""
        RegistosR(i).DtEmi = ""
        RegistosR(i).HrEmi = ""
        RegistosR(i).flg_impressao = False
        RegistosR(i).DtPagamento = ""
        RegistosR(i).HrPagamento = ""
        If Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar > 0 Then
            RegistosR(i).Estado = gEstadoReciboNaoEmitido
        ElseIf Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar = 0 Then
            RegistosR(i).Estado = gEstadoReciboNulo
        ElseIf Flg_EntidadeJaEmitida = True Then
            RegistosR(i).Estado = gEstadoReciboPerdido
        End If
        RegistosR(i).NumAnalises = 1
        RegistosR(i).flg_DocCaixa = 0
        If RegistosRM(indice).ReciboIsencao = gTipoIsencaoBorla Then
            RegistosR(i).FlgBorla = mediSim
        Else
            RegistosR(i).FlgBorla = mediNao
        End If
        RegistosR(i).FlgNaoConforme = mediNao
        
        FGRecibos.TextMatrix(i, lColRecibosNumDoc) = RegistosR(i).NumDoc
        FGRecibos.TextMatrix(i, lColRecibosEntidade) = RegistosR(i).DescrEntidade
        FGRecibos.TextMatrix(i, lColRecibosValorPagar) = RegistosR(i).ValorPagar
        FGRecibos.TextMatrix(i, lColRecibosDtEmissao) = RegistosR(i).DtEmi & " " & RegistosR(i).HrEmi
        FGRecibos.TextMatrix(i, lColRecibosDtPagamento) = RegistosR(i).DtPagamento & " " & RegistosR(i).HrPagamento
        If Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar > 0 Then
            FGRecibos.TextMatrix(i, lColRecibosEstado) = "N�o Emitido"
        ElseIf Flg_EntidadeJaEmitida = False And RegistosR(i).ValorPagar = 0 Then
            FGRecibos.TextMatrix(i, lColRecibosEstado) = "Recibo Nulo"
        ElseIf Flg_EntidadeJaEmitida = True Then
            FGRecibos.TextMatrix(i, lColRecibosEstado) = "An�lises Perdidas"
        End If
        If RegistosR(i).FlgBorla = mediSim Then
            FGRecibos.TextMatrix(i, lColRecibosBorla) = "Sim"
        Else
            FGRecibos.TextMatrix(i, lColRecibosBorla) = "N�o"
        End If
        If RegistosR(i).FlgNaoConforme = mediSim Then
            FGRecibos.TextMatrix(i, lColRecibosNaoConfmidade) = "Sim"
        Else
            FGRecibos.TextMatrix(i, lColRecibosNaoConfmidade) = "N�o"
        End If
        'NELSONPSILVA GLINTT-HS-21312
        FGAna.TextMatrix(i, lColFgAnaPreco) = RegistosRM(i).ReciboTaxa * RegistosRM(indice).ReciboQuantidade
        '
        FGRecibos.AddItem ""
        CriaNovaClasse RegistosR, i + 1, "REC"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "AdicionaAnaliseAoReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "AdicionaAnaliseAoReciboManual"
    Exit Sub
    Resume Next
End Sub




' -----------------------------------------------------------------------------------------------

' ELIMINA A TAXA DO RECIBO - APENAS SE O RECIBO AINDA NAO TIVER SIDO EMITIDO

' -----------------------------------------------------------------------------------------------
Private Function EliminaReciboManual(indice As Long, codAna As String, EliminaAnalise As Boolean) As Boolean
    On Error GoTo TrataErro
    Dim i As Long
    Dim ssql As String
    
    EliminaReciboManual = True
    For i = 1 To RegistosR.Count
        If Trim(RegistosRM(indice).ReciboNumDoc) = Trim(RegistosR(i).NumDoc) And Trim(RegistosRM(indice).ReciboSerieDoc) = Trim(RegistosR(i).SerieDoc) And RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade Then
            If RegistosR(i).Estado = gEstadoReciboNaoEmitido Or RegistosR(i).Estado = gEstadoReciboPerdido Or RegistosR(i).Estado = gEstadoReciboNulo Then
                If RegistosR(i).NumAnalises > 0 Then RegistosR(i).NumAnalises = RegistosR(i).NumAnalises - 1
                
                RegistosR(i).ValorPagar = BG_CvDecimalParaCalculo(RegistosR(i).ValorPagar) - BG_CvDecimalParaCalculo(Round(((RegistosRM(indice).ReciboTaxa * (100 - RegistosR(i).Desconto)) / 100), 2))
                RegistosR(i).ValorOriginal = BG_CvDecimalParaCalculo(RegistosR(i).ValorOriginal) - BG_CvDecimalParaCalculo(BL_HandleNull(RegistosRM(indice).ReciboTaxa, 0))
                If RegistosR(i).ValorPagar <= 0 And RegistosR(i).NumAnalises = 0 Then
                    FGRecibos.RemoveItem i
                    LimpaColeccao RegistosR, i
                    Exit For
                ElseIf RegistosR(i).NumAnalises = 0 Then
                    FGRecibos.RemoveItem i
                    LimpaColeccao RegistosR, i
                    Exit For
                Else
                    If RegistosR(i).ValorPagar <= 0 And RegistosR(i).NumAnalises > 0 Then
                        RegistosR(i).Estado = gEstadoReciboNulo
                    End If
                    FGRecibos.TextMatrix(i, lColRecibosValorPagar) = RegistosR(i).ValorPagar
                End If

                EliminaReciboManual = True
            ElseIf (RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca) And EliminaAnalise = True Then
                EliminaReciboManual = False
            ElseIf (RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca) And EliminaAnalise = False Then
                BG_Mensagem mediMsgBox, "J� foi emitido recibo para esta an�lise.N�o Pode alterar valor deste campo", vbExclamation, "Altera��o Recibo."
                EliminaReciboManual = False
            End If
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "EliminaReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "EliminaReciboManual"
    EliminaReciboManual = False
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA A ORDEM DE MARCACAO

' -----------------------------------------------------------------------------------------------
Private Function RetornaOrdemMarcacao(ByVal linha As Long) As Long
    On Error GoTo TrataErro
    Dim codEfr As String
    Dim p1 As String
    Dim i As Integer
    Dim Max As Long
    Max = 0
    For i = 1 To RegistosA.Count - 1
        If RegistosRM(linha).ReciboCodFacturavel = RegistosA(i).codAna Then
            RetornaOrdemMarcacao = i
            Exit Function
        End If
    Next
    
    codEfr = RegistosRM(linha).ReciboEntidade
    p1 = RegistosRM(linha).ReciboP1
    If codEfr <> "" And p1 <> "" Then
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboEntidade = codEfr And RegistosRM(i).ReciboP1 = p1 And linha <> i Then
                If RegistosRM(i).ReciboOrdemMarcacao > Max Then
                    Max = RegistosRM(i).ReciboOrdemMarcacao
                End If
            End If
        Next
    End If
    RetornaOrdemMarcacao = Max + 1
Exit Function
TrataErro:
    BG_LogFile_Erros "RetornaOrdemMarcacao: " & Err.Number & " - " & Err.Description, Me.Name, "RetornaOrdemMarcacao"
    RetornaOrdemMarcacao = 0
    Exit Function
    Resume Next
End Function

' -----------------------------------------------------------------------------------------------

' VERIFICA A ORDEM DE MARCACAO DENTRO DO P1

' -----------------------------------------------------------------------------------------------
Private Function RetornaOrdemP1(ByVal linha As Long) As Long
    On Error GoTo TrataErro
    Dim codEfr As String
    Dim p1 As String
    Dim i As Integer
    Dim Max As Long
    Max = 0
    codEfr = RegistosRM(linha).ReciboEntidade
    p1 = RegistosRM(linha).ReciboP1
    If codEfr <> "" And p1 <> "" Then
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboEntidade = codEfr And RegistosRM(i).ReciboP1 = p1 And linha <> i Then
                If RegistosRM(i).ReciboOrdemMarcacao > Max Then
                    Max = RegistosRM(i).ReciboOrdemMarcacao
                End If
            End If
        Next
    End If
    RetornaOrdemP1 = Max + 1
Exit Function
TrataErro:
    BG_LogFile_Erros "RetornaOrdemP1: " & Err.Number & " - " & Err.Description, Me.Name, "RetornaOrdemP1"
    RetornaOrdemP1 = 0
    Exit Function
    Resume Next
End Function
' --------------------------------------------------------------------------------------------

' SE DA ENTER NA COLUNA DO P1

' --------------------------------------------------------------------------------------------

Private Sub AlteraCorP1()
    On Error GoTo TrataErro
    Dim ent_temp As String
    Dim p1_temp As String
    Dim cor_temp As Long
    Dim cor2_temp As String
    Dim linha_temp As Integer
    Dim iRec As Long
    Dim i As Integer
    
    ent_temp = FGAna.TextMatrix(FGAna.row, lColFgAnaCodEFR)
    p1_temp = FGAna.TextMatrix(FGAna.row, lColFgAnaP1)
    linha_temp = FGAna.row
    If FGAna.CellBackColor = vbWhite Then
        FGAna.CellBackColor = Verde
        cor_temp = Verde
        cor2_temp = "V"
    Else
        FGAna.CellBackColor = vbWhite
        cor_temp = vbWhite
        cor2_temp = "B"
    End If
    For i = 1 To FGAna.rows - 1
        If FGAna.TextMatrix(i, lColFgAnaP1) = p1_temp And FGAna.TextMatrix(i, lColFgAnaCodEFR) = ent_temp Then
            iRec = DevIndice(RegistosA(i).codAna)
            FGAna.row = i
            FGAna.Col = 2
            FGAna.CellBackColor = cor_temp
            RegistosRM(iRec).ReciboCorP1 = cor2_temp
        End If
    Next
    FGAna.row = linha_temp
Exit Sub
TrataErro:
    BG_LogFile_Erros "AlteraCorP1: " & Err.Number & " - " & Err.Description, Me.Name, "AlteraCorP1"
    Exit Sub
    Resume Next
End Sub
Sub Grava_ObsAnaReq()
    On Error GoTo TrataErro

    Dim i As Integer
    Dim ssql As String
    
    ' -----------------------------------------------------------------------------------------------
    ' INSERE OS DADOS DE CADA ANALISE NA TABELA SL_OBS_ANA_REQ
    ' -----------------------------------------------------------------------------------------------
    If RegistosA.Count > 0 And Trim(RegistosA(1).codAna) <> "" Then
        For i = 1 To RegistosA.Count - 1
            If RegistosA(i).codAna <> "" And EcNumReq <> "" And RegistosA(i).ObsAnaReq <> "" Then
                BL_GravaObsAnaReq CStr(EcNumReq), RegistosA(i).codAna, RegistosA(i).ObsAnaReq, CLng(RegistosA(i).seqObsAnaReq), _
                                  0, EcSeqUtente, mediComboValorNull
                PreencheNotas
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_ObsAnaReq: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_ObsAnaReq"
    Exit Sub
    Resume Next
End Sub



Private Function EtiqPrint_Recibo(serie_rec As String, num_recibo As String, data As String, n_req As String, efr As String _
                           , nome As String, num_benef As String, analises As String, _
                           total As String, num_anulacao As String, user_emi As String, descricao As String, dt_chega As String, _
                           vd As String, cod_empresa As String, nome_empresa As String, num_contribuinte As String, _
                           total_original As Double, Desconto As Double, cabecalho As String, Formacao As String, Certificado As String)
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    On Error GoTo TrataErro


    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{SERIE_REC}", serie_rec)
    sWrittenData = Replace(sWrittenData, "{NUM_RECIBO}", num_recibo)
    sWrittenData = Replace(sWrittenData, "{DATA}", data)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{EFR}", efr)
    sWrittenData = Replace(sWrittenData, "{NOME}", BL_RemovePortuguese(nome))
    sWrittenData = Replace(sWrittenData, "{NUM_BENEF}", BL_RemovePortuguese(num_benef))
    sWrittenData = Replace(sWrittenData, "{ANALISES}", BL_RemovePortuguese(analises))
    sWrittenData = Replace(sWrittenData, "{TOTAL}", total)
    sWrittenData = Replace(sWrittenData, "{NUM_ANULACAO}", num_anulacao)
    sWrittenData = Replace(sWrittenData, "{USER_EMI}", user_emi)
    sWrittenData = Replace(sWrittenData, "{DESCRICAO}", BL_RemovePortuguese(descricao))
    sWrittenData = Replace(sWrittenData, "{VD}", vd)
    sWrittenData = Replace(sWrittenData, "{DT_CHEGA}", dt_chega)
    sWrittenData = Replace(sWrittenData, "{COD_EMPRESA}", cod_empresa)
    sWrittenData = Replace(sWrittenData, "{NOME_EMPRESA}", BL_RemovePortuguese(nome_empresa))
    sWrittenData = Replace(sWrittenData, "{NUM_CONTRIBUINTE}", num_contribuinte)
    sWrittenData = Replace(sWrittenData, "{TOTAL_ORIGINAL}", total_original)
    sWrittenData = Replace(sWrittenData, "{DESCONTO}", Desconto)
    sWrittenData = Replace(sWrittenData, "{CABECALHO}", cabecalho)
    sWrittenData = Replace(sWrittenData, "{FORMACAO}", Formacao)
    sWrittenData = Replace(sWrittenData, "{CERTIFICADO}", Certificado)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint_Recibo = True
Exit Function
TrataErro:
    BG_LogFile_Erros "EtiqPrint_Recibo: " & Err.Number & " - " & Err.Description, Me.Name, "EtiqPrint_Recibo"
    EtiqPrint_Recibo = ""
    Exit Function
    Resume Next
End Function

Private Function EtiqPrint_FichaCruzada(NumReq As String, nome As String, morada As String, idade As String, Sexo As String, _
                                         DescrEFR As String, DataChegada As String, destino As String, _
                                         DescrSala As String, DescrMedico As String)
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    On Error GoTo TrataErro


    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{NUM_REQ}", NumReq)
    sWrittenData = Replace(sWrittenData, "{NOME}", nome)
    sWrittenData = Replace(sWrittenData, "{MORADA}", morada)
    sWrittenData = Replace(sWrittenData, "{IDADE}", idade)
    sWrittenData = Replace(sWrittenData, "{DESCR_EFR}", DescrEFR)
    sWrittenData = Replace(sWrittenData, "{DATA_CHEGADA}", DataChegada)
    sWrittenData = Replace(sWrittenData, "{DESTINO}", destino)
    sWrittenData = Replace(sWrittenData, "{DESCR_SALA}", DescrSala)
    sWrittenData = Replace(sWrittenData, "{DESCR_MEDICO}", DescrMedico)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint_FichaCruzada = True
Exit Function
TrataErro:
    BG_LogFile_Erros "EtiqPrint_FichaCruzada: " & Err.Number & " - " & Err.Description, Me.Name, "EtiqPrint_FichaCruzada"
    EtiqPrint_FichaCruzada = ""
    Exit Function
    Resume Next
End Function

'LJMANSO-310
Private Sub AtualizaEstrutFact()
Dim iResp As Integer
Dim iAna As Integer
    
For iResp = 1 To fa_movi_resp_tot
    For iAna = 1 To fa_movi_resp(iResp).totalAna
        fa_movi_resp(iResp).analises(iAna).cod_u_saude = Trim(EcCodProveniencia.text)
        fa_movi_resp(iResp).analises(iAna).cod_medico = Trim(EcCodMedico.text)
    Next iAna
Next iResp
End Sub

Public Sub EcCodProveniencia_Validate(Cancel As Boolean)
     On Error GoTo TrataErro
       
    Dim Tabela As adodb.recordset
    Dim sql As String
    'RGONCALVES 15.07.2016
    EcCodProveniencia.text = UCase(EcCodProveniencia.text)
    '
    If EcCodProveniencia.text <> "" Then
        Set Tabela = New adodb.recordset
    
        sql = "SELECT descr_proven, t_sit, t_urg, cod_efr FROM sl_proven WHERE cod_proven='" & Trim(EcCodProveniencia.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodProveniencia.text = ""
            EcDescrProveniencia.text = ""
            cod_proven_efr = ""
        Else
                'edgar.parada LJMANSO-353 09.01.2018
            'cod_proven_efr = BL_HandleNull(Tabela!cod_efr, "")
            'If BL_HandleNull(Tabela!cod_efr, "") <> "" And BL_HandleNull(Tabela!cod_efr, "") <> EcCodEFR Then
            'EcCodEFR = BL_HandleNull(Tabela!cod_efr, "")
                        EcDescrProveniencia.text = Tabela!descr_proven
            EcCodEFR_Validate False
            EcCodEFR_LostFocus

           ' End If
           '
            
            If Not (IsNull(Tabela!t_urg)) Then
                BG_MostraComboSel BL_HandleNull(Tabela!t_urg, -1), CbUrgencia
            End If
            
            'RGONCALVES 17.02.2015 CHSJ-1785
            Call ActualizaCodTDest
            '
        End If
     
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProveniencia.text = ""
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcCodProveniencia_Validate: " & Err.Number & " - " & Err.Description, Me.Name, "EcCodProveniencia_Validate"
    Exit Sub
    Resume Next
End Sub


Private Sub BtPesquisaProveniencia_Click()
    


    Dim ChavesPesq(1 To 3) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 3) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 3) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 3) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 3) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    ChavesPesq(3) = "cod_efr"
    CamposEcran(3) = "cod_efr"
    Tamanhos(3) = 700
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    Headers(3) = "EFR"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 3

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    ClausulaWhere = " (flg_invisivel IS NULL or flg_invisivel = 0) AND cod_proven IN (SELECT x.cod_proven FROM sl_ass_proven_locais x WHERE x.cod_local = " & BL_HandleNull(EcLocal, CStr(gCodLocal)) & ") "
        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    cod_proven_efr = ""
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.text = Resultados(1)
            EcDescrProveniencia.text = Resultados(2)
            cod_proven_efr = BL_HandleNull(Resultados(3), "")
            If BL_HandleNull(Resultados(3), "") <> "" And BL_HandleNull(Resultados(3), "") <> EcCodEFR Then
                EcCodEFR = BL_HandleNull(Resultados(3), "")
                EcCodEFR_Validate False
                EcCodEFR_LostFocus
            End If
            
            'RGONCALVES 17.02.2015 CHSJ-1785
            Call ActualizaCodTDest
            '
            'LJMANSO-310
            Call AtualizaEstrutFact
            '
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtPesquisaProveniencia_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtPesquisaProveniencia_Click"
    Exit Sub
    Resume Next
End Sub


Private Sub CbCodUrbano_Click()
    On Error GoTo TrataErro

    If CbCodUrbano.ListIndex <> mediComboValorNull Then
        CkCobrarDomicilio.Enabled = True
        If CbCodUrbano.ListIndex <> mediComboValorNull And FrameDomicilio.Visible = True Then
            EcKm.Enabled = True
            EcKm.SetFocus
        Else
            EcKm.text = ""
            EcKm.Enabled = False
        End If
    Else
        CkCobrarDomicilio.value = vbGrayed
        CkCobrarDomicilio.Enabled = False
    End If
    If CbCodUrbano.ListIndex > mediComboValorNull And gPassaRecFactus = mediSim Then
        FACTUS_AtualizaDom CbCodUrbano.ItemData(CbCodUrbano.ListIndex), BL_HandleNull(EcKm.text, 0)
    Else
        FACTUS_AtualizaDom 0, 0
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbCodUrbano_Click: " & Err.Number & " - " & Err.Description, Me.Name, "CbCodUrbano_Click"
    Exit Sub
    Resume Next
End Sub


Private Sub CbCodUrbano_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo TrataErro
    
    If KeyCode = 46 Then
        BG_LimpaOpcao CbCodUrbano, KeyCode
        EcKm.text = ""
    End If
    If CbCodUrbano.ListIndex > mediComboValorNull And gPassaRecFactus = mediSim Then
        FACTUS_AtualizaDom CbCodUrbano.ItemData(CbCodUrbano.ListIndex), BL_HandleNull(EcKm.text, 0)
    Else
        FACTUS_AtualizaDom 0, 0
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "CbCodUrbano_KeyDown: " & Err.Number & " - " & Err.Description, Me.Name, "CbCodUrbano_KeyDown"
    Exit Sub
    Resume Next
End Sub


Public Sub ImprimeAnulRecibo(numAnulacao As Long)
    RECIBO_ImprimeAnulRecibo numAnulacao, RegistosR, FGRecibos.row, EcNumReq, EcPrinterRecibo
    BL_MudaCursorRato mediMP_Activo, Me
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeAnulRecibo: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeAnulRecibo"
    Exit Sub
    Resume Next
End Sub


Public Sub EcCodsala_Validate(Cancel As Boolean)
    Cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
End Sub
Public Sub EcCodlocalentrega_Validate(Cancel As Boolean)
    Cancel = PA_ValidateLocalEntrega(EcCodLocalEntrega, EcDescrLocalEntrega)
End Sub

Private Sub BtPesquisaSala_Click()
    PA_PesquisaSala EcCodSala, EcDescrSala, ""
End Sub




''''''''''''''''''''''''''''''''''''''''''''
' Preenche tabela temporaria de ficha ''''''
''''''''''''''''''''''''''''''''''''''''''''
Private Sub Preenche_SLCRFICHACRUZADA(NReq As String, nome As String, morada As String, idade As String, Sexo As String, _
                            Entidade As String, data As String, destino As String, sala As String, Medico As String)
    On Error GoTo TrataErro
    Dim ssql As String
    ssql = "INSERT INTO sl_cr_ficha_cruzada (n_req,nome,morada,idade,sexo,entidade,data," & _
                        "destino,sala,medico, nome_computador, num_sessao) VALUES (" & NReq & "," & BL_TrataStringParaBD(nome) & "," & BL_TrataStringParaBD(morada) & "," & BL_TrataStringParaBD(idade) & "," & _
                        BL_TrataStringParaBD(Sexo) & "," & BL_TrataStringParaBD(Entidade) & "," & BL_TrataDataParaBD(data) & "," & BL_TrataStringParaBD(destino) & "," & BL_TrataStringParaBD(sala) & "," & BL_TrataStringParaBD(Medico) & ", " & _
                        BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ")"
    ' Insere os dados da ficha cruzada na tabela temporaria
    BG_ExecutaQuery_ADO ssql
    BG_Trata_BDErro
Exit Sub
TrataErro:
    BG_LogFile_Erros "Preenche_SLCRFICHACRUZADA: " & Err.Number & " - " & Err.Description, Me.Name, "Preenche_SLCRFICHACRUZADA"
    Exit Sub
    Resume Next
End Sub


' --------------------------------------------------------

' PREENCHE TABELA CRYSTAL PARA FICHA CRUZADA

' --------------------------------------------------------

Private Sub Preenche_SLCRETIQFICHACRUZADA(n_req, cod_bar1 As String, legenda1 As String, _
                                          cod_bar2 As String, legenda2 As String, _
                                          cod_bar3 As String, legenda3 As String, _
                                          cod_bar4 As String, legenda4 As String, _
                                          cod_bar5 As String, legenda5 As String, _
                                          cod_bar6 As String, legenda6 As String, _
                                          cod_bar7 As String, legenda7 As String, _
                                          cod_bar8 As String, legenda8 As String, _
                                          cod_bar9 As String, legenda9 As String, _
                                          cod_bar10 As String, legenda10 As String)
    On Error GoTo TrataErro
    Dim ssql As String
    On Error GoTo TrataErro
    
    
    ' Variaveis auxiliares
    ssql = "INSERT INTO sl_cr_etiq_ficha_cruzada (n_req, nome_computador, num_sessao , cod_bar1,cod_bar_legend1, cod_bar2,cod_bar_legend2, "
    ssql = ssql & "cod_bar3,cod_bar_legend3, cod_bar4,cod_bar_legend4, cod_bar5,cod_bar_legend5, "
    ssql = ssql & " cod_bar6,cod_bar_legend6, cod_bar7,cod_bar_legend7, cod_bar8,cod_bar_legend8, "
    ssql = ssql & " cod_bar9,cod_bar_legend9, cod_bar10,cod_bar_legend10)"
    ssql = ssql & " VALUES (" & n_req & ", " & BL_TrataStringParaBD(gComputador) & ", " & gNumeroSessao & ", "
    ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(cod_bar1, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda1, "")) & ", "
    ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(cod_bar2, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda2, "")) & ", "
    ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(cod_bar3, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda3, "")) & ", "
    ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(cod_bar4, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda4, "")) & ", "
    ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(cod_bar5, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda5, "")) & ", "
    ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(cod_bar6, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda6, "")) & ", "
    ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(cod_bar7, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda7, "")) & ", "
    ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(cod_bar8, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda8, "")) & ", "
    ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(cod_bar9, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda9, "")) & ", "
    ssql = ssql & BL_TrataStringParaBD(BL_HandleNull(cod_bar10, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(legenda10, "")) & ") "
    
    BG_ExecutaQuery_ADO ssql
    BG_Trata_BDErro
Exit Sub
TrataErro:
    BL_LogFile_BD Me.Name, "Preenche_SLCRETIQFICHACRUZADA", Err.Number, Err.Description, ""
    Exit Sub
    Resume Next
End Sub


' --------------------------------------------------------------------------------

' PERMITE MUDAR TIPO DE ISENCAO DE UMA ANALISE ATRAVES DAS TECLAS F8, F9, F10

' --------------------------------------------------------------------------------
Private Sub AlteraIsencao(TipoIsencao As Integer, linha As Long, linhaAna As Integer)
    On Error GoTo TrataErro
    Dim taxa As String
    Dim codAnaEfr As String
    Dim mens As String
    Dim ssql As String
    Dim flg_FdsValFixo As Integer
    Dim isencaoAnterior As Integer
    Dim taxaEfr As String
    Dim valorUnitEFR As Double
    Dim descrAnaEfr As String
    Dim j As Integer
    
    
    For j = 1 To RegistosR.Count
        If Trim(RegistosRM(linha).ReciboNumDoc) = Trim(RegistosR(j).NumDoc) And RegistosRM(linha).ReciboEntidade = RegistosR(j).codEntidade Then
            If (RegistosR(j).Estado = gEstadoReciboPago Or RegistosR(j).Estado = gEstadoReciboCobranca) Then
                Exit Sub
            End If
        End If
    Next
    
    
    isencaoAnterior = BL_HandleNull(RegistosRM(linha).ReciboIsencao, gTipoIsencaoIsento)
    
    If IF_ContaMembros(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade) <= 0 Then
        RegistosRM(linha).ReciboQuantidade = BL_HandleNull(RetornaQtd(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade), 1)
        RegistosRM(linha).ReciboQuantidadeEFR = RegistosRM(linha).ReciboQuantidade
    End If
    
    
    If RegistosRM(linha).ReciboCodFacturavel = "" And linhaAna > mediComboValorNull Then
        RegistosRM(linha).ReciboEntidade = EcCodEFR
        RegistosRM(linha).ReciboCodEfrEnvio = EcCodEFR
        RegistosRM(linha).ReciboNumDoc = "0"
        RegistosRM(linha).ReciboSerieDoc = "0"
        RegistosRM(linha).ReciboCodFacturavel = RegistosA(linhaAna).codAna
        RegistosRM(linha).ReciboFlgAdicionado = "0"
        If linha = 1 Then
            RegistosRM(linha).ReciboP1 = 1
            RegistosRM(linha).ReciboCodMedico = ""
        ElseIf linha > 1 Then
            RegistosRM(linha).ReciboP1 = RegistosRM(linha - 1).ReciboP1
            RegistosRM(linha).ReciboCodMedico = RegistosRM(linha - 1).ReciboCodMedico
        End If
        RegistosRM(linha).ReciboOrdemMarcacao = RetornaOrdemMarcacao(linha)
        RegistosRM(linha).ReciboFlgAdicionado = "0"
        If BG_DaComboSel(CbTipoIsencao) = gTipoIsencaoIsento Then
             RegistosRM(linha).ReciboIsencao = gTipoIsencaoIsento
        Else
             RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento
        End If
    End If
    
    ' MUDAR ANALISE PARA ISENTA
    '---------------------------------------
    If TipoIsencao = gTipoIsencaoIsento Then
        If RegistosRM(linha).ReciboIsencao = gTipoIsencaoIsento Then
            Exit Sub
        ElseIf RegistosRM(linha).ReciboIsencao = gTipoIsencaoBorla Then
            EliminaReciboManual linha, RegistosRM(linha).ReciboCodFacturavel, False
            RegistosRM(linha).ReciboIsencao = gTipoIsencaoIsento
            AdicionaAnaliseAoReciboManual linha, False
        ElseIf RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento Then
            EliminaReciboManual linha, RegistosRM(linha).ReciboCodFacturavel, False
            RegistosRM(linha).ReciboTaxa = 0
            RegistosRM(linha).ReciboTaxa = 0
            RegistosRM(linha).ReciboIsencao = gTipoIsencaoIsento
            AdicionaAnaliseAoReciboManual linha, False
        End If
    
    ' MUDAR ANALISE PARA NAO ISENTA
    '-------------------------------------------
    ElseIf TipoIsencao = gTipoIsencaoNaoIsento Then
        If isencaoAnterior = gTipoIsencaoIsento Or isencaoAnterior = gTipoIsencaoBorla Then
            EliminaReciboManual linha, RegistosRM(linha).ReciboCodFacturavel, False
            taxa = IF_RetornaTaxaAnalise(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, CbTipoUtente, EcUtente, EcDataChegada, codAnaEfr, RegistosRM(linha).ReciboDescrAnaEFR, False)
            taxa = Replace(taxa, ".", ",")
            
            taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, _
                                         EcDataPrevista, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, RegistosRM(linha).ReciboQuantidadeEFR, "", False, descrAnaEfr, mediComboValorNull)
            taxaEfr = Replace(taxaEfr, ".", ",")
            valorUnitEFR = Replace(valorUnitEFR, ".", ",")
            If Mid(taxa, 1, 1) = "," Then
                taxa = "0" & taxa
            End If
            Select Case taxa
                Case "-1"
                    mens = " An�lise n�o est� mapeada para o FACTUS!"
                Case "-2"
                    mens = "Impossivel abrir conex�o com o FACTUS!"
                Case "-3"
                    mens = "Impossivel seleccionar portaria activa!"
                Case "-4"
                    mens = "N�o existe taxa codificada para a an�lise em causa!"
                Case "-5"
                    mens = "N�o existe tabela codificada para a entidade em causa!"
                Case Else
                    mens = ""
            End Select
            RegistosRM(linha).ReciboCodAnaEFR = codAnaEfr
            If mens <> "" Then
                BG_Mensagem mediMsgBox, mens, vbOKOnly + vbInformation, "Taxas"
                If CDbl(taxa) <= 0 And BG_DaComboSel(CbTipoIsencao) <> gTipoIsencaoIsento Then
                    RegistosRM(linha).ReciboEntidade = gEfrParticular
                    RegistosRM(linha).ReciboCodEfrEnvio = gEfrParticular
                    taxa = IF_RetornaTaxaAnalise(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, CbTipoUtente, EcUtente, EcDataChegada, codAnaEfr, RegistosRM(linha).ReciboDescrAnaEFR, False)
                    taxa = Replace(taxa, ".", ",")
                    
                    taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, _
                                                 EcDataPrevista, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, RegistosRM(linha).ReciboQuantidadeEFR, "", False, descrAnaEfr, mediComboValorNull)
                    taxaEfr = Replace(taxaEfr, ".", ",")
                    valorUnitEFR = Replace(valorUnitEFR, ".", ",")
                    If CLng(taxa) < 0 Then
                        taxa = 0
                    End If
                End If
                RegistosRM(linha).ReciboTaxa = "0,0"
            Else
                RegistosRM(linha).ReciboTaxa = taxa
            End If
            RegistosRM(linha).ReciboTaxaUnitario = RegistosRM(linha).ReciboTaxa
            RegistosRM(linha).ReciboTaxa = RegistosRM(linha).ReciboTaxaUnitario * RegistosRM(linha).ReciboQuantidade
            RegistosRM(linha).ReciboValorUnitEFR = valorUnitEFR
            RegistosRM(linha).ReciboCodAnaEFR = codAnaEfr
            RegistosRM(linha).ReciboDescrAnaEFR = descrAnaEfr
            RegistosRM(linha).ReciboValorEFR = Replace(RegistosRM(linha).ReciboValorUnitEFR, ".", ",") * RegistosRM(linha).ReciboQuantidadeEFR
            RegistosRM(linha).ReciboValorEFR = Round(RegistosRM(linha).ReciboValorEFR, 2)
            
            'REQUISI��ES DE FIM DE SEMANA DOBRA O PRE�O
            'SE ENTIDADE COM VALOR FIXO PARA FINS DE SEMANA - COLOCA A ZERO
            flg_FdsValFixo = VerificaFdsValFixo(linha)
            If RegistosRM(linha).ReciboFDS = mediSim And flg_FdsValFixo = 0 Then
                RegistosRM(linha).ReciboTaxa = RegistosRM(linha).ReciboTaxa * 2 * RegistosRM(linha).ReciboQuantidade
            ElseIf RegistosRM(linha).ReciboFDS = mediSim And flg_FdsValFixo = 1 Then
                RegistosRM(linha).ReciboTaxa = "0"
            End If
            
            RegistosRM(linha).ReciboTaxa = Round(RegistosRM(linha).ReciboTaxa, 2)
            RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento
            AdicionaAnaliseAoReciboManual linha, False
        ElseIf RegistosRM(linha).ReciboIsencao = mediComboValorNull Then
            RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento
            AdicionaAnaliseAoReciboManual linha, False
        ElseIf RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento Then
            Exit Sub
        End If
        
    ' MUDAR ANALISE PARA BORLA
    '-------------------------------------------
    ElseIf TipoIsencao = gTipoIsencaoBorla Then
        If RegistosRM(linha).ReciboIsencao = gTipoIsencaoIsento Or RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento Then
            EliminaReciboManual linha, RegistosRM(linha).ReciboCodFacturavel, False
            RegistosRM(linha).ReciboTaxa = 0
            RegistosRM(linha).ReciboTaxa = 0
            RegistosRM(linha).ReciboIsencao = gTipoIsencaoBorla
            AdicionaAnaliseAoReciboManual linha, False
        ElseIf RegistosRM(linha).ReciboIsencao = gTipoIsencaoBorla Then
            Exit Sub
        End If
    End If

    
    If EcNumReq <> "" And RegistosRM(linha).ReciboCodFacturavel <> "" And RegistosRM(linha).ReciboIsencao <> "" Then
        ssql = "UPDATE sl_recibos_det SET isencao = " & RegistosRM(linha).ReciboIsencao & " WHERE n_req = " & EcNumReq
        ssql = ssql & " AND cod_efr = " & RegistosRM(linha).ReciboEntidade & " AND cod_facturavel = " & BL_TrataStringParaBD(RegistosRM(linha).ReciboCodFacturavel)
        BG_ExecutaQuery_ADO (ssql)
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "AlteraIsencao: " & Err.Number & " - " & Err.Description, Me.Name, "AlteraIsencao"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------------------------------------

' ELIMINA A TAXA DO RECIBO - APENAS SE O RECIBO AINDA NAO TIVER SIDO EMITIDO

' -----------------------------------------------------------------------------------------------
Private Function VerificaPodeAlterarRecibo(indice As Long) As Boolean
    On Error GoTo TrataErro
    Dim i As Long
    Dim ssql As String
    VerificaPodeAlterarRecibo = True
    For i = 1 To RegistosR.Count
        If Trim(RegistosRM(indice).ReciboNumDoc) = Trim(RegistosR(i).NumDoc) And RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade Then
            If (RegistosR(i).Estado = gEstadoReciboPago Or RegistosR(i).Estado = gEstadoReciboCobranca) Then
                If BG_Mensagem(mediMsgBox, "J� foi emitido recibo para esta an�lise.Confirma a elimina��o?", vbYesNo + vbQuestion, "Eliminar an�lise.") = vbYes Then
                    VerificaPodeAlterarRecibo = False
                Else
                    VerificaPodeAlterarRecibo = True
                End If
            End If
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaPodeAlterarRecibo: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaPodeAlterarRecibo"
    VerificaPodeAlterarRecibo = False
    Exit Function
    Resume Next
End Function




' --------------------------------------------------------

' VERIFICA SE EXISTEM NOTAS PARA REQUISI��O EM CAUSA

' --------------------------------------------------------
Private Sub PreencheNotas()
    On Error GoTo TrataErro
    Dim ssql As String
    Dim RsNotas As New adodb.recordset
    
    If EcNumReq = "" Then Exit Sub
    ssql = "SELECT count(*) total FROM sl_obs_ana_req WHERE n_req = " & EcNumReq

    RsNotas.CursorType = adOpenStatic
    RsNotas.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    RsNotas.Open ssql, gConexao
    If RsNotas.RecordCount > 0 Then
        If RsNotas!total > 0 Then
            BtNotas.Visible = False
            BtNotasVRM.Visible = True
            
        Else
            BtNotas.Visible = True
            BtNotasVRM.Visible = False
        End If
    End If
    RsNotas.Close
    Set RsNotas = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheNotas: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheNotas"
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------

' UTILIZADORES DOS POSTOS PREENCHE POR DEFEITO A SALA

' --------------------------------------------------------
Private Sub PreencheSalaDefeito()
     On Error GoTo TrataErro
   EcCodSala.Enabled = True
    BtPesquisaSala.Enabled = True
    If gCodSalaAssocUser > 0 Then
        EcCodSala = gCodSalaAssocUser
        EcCodsala_Validate True
        EcCodSala.Enabled = False
        BtPesquisaSala.Enabled = False
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheSalaDefeito: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheSalaDefeito"
    Exit Sub
    Resume Next
End Sub
Private Sub ImprimeEtiqNovo()
    'Limpar parametros
        BG_LimpaPassaParams
    'Passar parametros necessarios o ao form de impress�o
        gPassaParams.Param(0) = EcNumReq.text
    
    'Abrir form de impress�o
    FormNEtiqNTubos.Show
    DoEvents
    'FormGestaoRequisicaoPrivado.SetFocus

End Sub

' pferreira 2010.08.10
' Selecciona campos "abrev_ute" da tabela sl_identif.
Sub ImprimeEtiq()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim k As Integer
    Dim j As Integer
    Dim sql As String
    Dim CmdDadosTubo As adodb.Command
    Dim RsDadosTubo As adodb.recordset
    Dim CmdDadosGhostM As adodb.Command
    Dim RsDadosGhostM As adodb.recordset
    Dim CodTuboBarLoc As String
    Dim encontrou As Boolean
    Dim CalculaCapaci As Double
    Dim ordem As Integer
    Dim GhostsJaTratados() As String
    Dim TotalGhostsJaTratados As Integer
    Dim Especiais As Long
    Dim EtqAdministrativas As Long
    Dim NumReqBar As String
    Dim continua As Boolean
    Dim EtqCrystal As String
    Dim EtqPreview As String
    Dim rsReq As adodb.recordset
    Dim TotalP1s As Integer
    Dim NrReqARS As String
    Dim numEtiq As Long
    Dim dataImpressao As String
    Dim episodio As String
    Dim situacao As String
    Dim rv As Integer
    
    Dim idade As String
    Dim CodProd_aux As String
    
    idade = ""


    Especiais = 0
    
    i = 1



    If gTotalTubos <> 0 Then
        EcEtqNTubos = gTotalTubos
        EcEtqNEsp = Especiais
        
        'Aten��o - O FormNEtiq retorna nos campos deste form :
        ' EcEtqNAdm   = n� etiquetas administrativas
        ' EcEtqNCopias  = n� copias de etiquetas tubos
        ' EcEtqTipo   = tipo de impress�o
        '   0 - imprimir etq. para tubos e administrativas
        '   1 - imprimir etq. administrativas
        '   2 - imprimir etq. para tubos
        '  -1 - quando se cancela a impress�o
        '
        ' EcEtqNEsp envia para o form o n� etiquetas especiais (com o c�digo do produto ou ordem de an�lises)
        ' EcEtqNTubos envia para o form o n� etiquetas para tubos
        
        If EFR_Verifica_ARS(EcCodEFR.text) = True Then
            ReDim p1(0)
            Dim ExisteP1 As Boolean
            ExisteP1 = False
            For i = 1 To RegistosA.Count - 1
                
                For j = 1 To UBound(p1)
                    If RegistosA(i).NReqARS = p1(j).NrP1 Then
                        ExisteP1 = True
                    End If
                Next j
                If ExisteP1 = False Then
                    ReDim Preserve p1(UBound(p1) + 1)
                    p1(UBound(p1)).NrP1 = RegistosA(i).NReqARS
                End If
                ExisteP1 = False
            Next i
            
            TotalP1s = UBound(p1)
            gNumEtiqAdminDefeito = TotalP1s + 1
        Else
            TotalP1s = 0
            gNumEtiqAdminDefeito = Trim(BG_DaParamAmbiente_NEW(mediAmbitoGeral, "NumEtiqAdminDefeito"))
        End If
        FormNEtiq.Show vbModal
        
        FormGestaoRequisicaoPrivado.SetFocus
                
        If Trim(EcEtqNAdm) = "" Then EcEtqNAdm = "0"
        If Trim(EcEtqNCopias) = "" Then EcEtqNCopias = "0"
        If Trim(EcEtqNEsp) = "" Then EcEtqNEsp = "0"
        If Trim(EcEtqNTubos) = "" Then EcEtqNTubos = "0"
        
        If EcEtqTipo <> "-1" Then
        
            EtqAdministrativas = EcEtqNAdm
        
            If gTotalTubos <> 0 And EtqAdministrativas <> 0 Then
            'FeriasFernando
                If gLAB = "HOVAR" Then
                    EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoComputador, "ETIQ_CRYSTAL")
                Else
                    EtqCrystal = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "ETIQ_CRYSTAL")
                
                If EtqCrystal = "-1" Or EtqCrystal = "" Then EtqCrystal = "1"
                End If
                
                If EtqCrystal = "1" Then
                    If BL_PreviewAberto("Etiquetas") = True Then Exit Sub
            
                    continua = BL_IniciaReport("Etiqueta", "Etiquetas", crptToPrinter, False)
                    If continua = False Then Exit Sub
            
                    Call Cria_TmpEtq
                End If
                
                NumReqBar = ""
                For j = 1 To (7 - Len(EcNumReq))
                    NumReqBar = NumReqBar & "0"
                Next j
                NumReqBar = NumReqBar & EcNumReq
                
                If EcEtqTipo <> "1" Then
                    'Tubos
                    'N� de c�pias das etiquetas
                    For j = 1 To EcEtqNCopias
                        For i = 1 To gTotalTubos
                            If EtqCrystal = "1" Then
                        'Bruno 28-01-2003
                                If (gEstruturaTubos(i).Especial = True And gEstruturaTubos(i).CodTubo = CodProd_aux) Then
                                'Se etiqueta especial e codigo de tubo = anterior nao imprime etiqueta
                                Else
                                    CodProd_aux = gEstruturaTubos(i).CodTubo
                                    gEstruturaTubos(i).CodTubo = ""
                                
                                    dataImpressao = BL_HandleNull(gEstruturaTubos(i).dt_chega, BL_HandleNull(EcDataChegada, EcDataPrevista))
                                    sql = "INSERT INTO sl_cr_etiq" & gNumeroSessao & _
                                        " ( gr_ana, abr_ana, t_utente, utente, n_req, t_urg, dt_req, produto, nome_ute, n_req_bar, descr_tubo, situacao, episodio, idade) " & _
                                        " VALUES (" & BL_TrataStringParaBD(gEstruturaTubos(i).GrAna) & "," & BL_TrataStringParaBD(gEstruturaTubos(i).abrAna) & "," & BL_TrataStringParaBD(CbTipoUtente.text) & "," & BL_TrataStringParaBD(EcUtente) & _
                                        "'," & BL_TrataStringParaBD(EcNumReq) & ",'" & IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " ") & _
                                        "'," & BL_TrataDataParaBD(dataImpressao) & "," & _
                                        "'" & gEstruturaTubos(i).CodProd & IIf(gEstruturaTubos(i).CodTubo <> "", " (", " ") & gEstruturaTubos(i).CodTubo & IIf(gEstruturaTubos(i).CodTubo <> "", ")'", "'") & ", " & _
                                        BL_TrataStringParaBD(BL_AbreviaNome(EcNome, 40)) & ",'" & gEstruturaTubos(i).codTuboBar & NumReqBar & "', '" & BG_CvPlica(gEstruturaTubos(i).Designacao_tubo) & "', '" & situacao & "', '" & episodio & "', '" & idade & "')"
                                    
                                    BG_ExecutaQuery_ADO sql
                            
                                    If gSQLError <> 0 Then
                                        BG_Mensagem mediMsgBox, "Erro a gerar etiquetas!", vbExclamation + vbOKOnly, "Etiquetas"
                                        Set CmdDadosTubo = Nothing
                                        Set CmdDadosGhostM = Nothing
                                        Exit Sub
                                    End If
                                End If
                            Else
                                If (gEstruturaTubos(i).Especial = True And gEstruturaTubos(i).CodTubo = CodProd_aux) Then
                                'Se etiqueta especial e codigo de tubo = anterior nao imprime etiqueta
                                Else
                                    
                                    If Not LerEtiqInI Then
                                        MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                                        Exit Sub
                                    End If
                                    If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                                        MsgBox "Imposs�vel abrir impressora etiquetas"
                                        Exit Sub
                                    End If
                                    
                                    dataImpressao = BL_HandleNull(gEstruturaTubos(i).dt_chega, BL_HandleNull(EcDataChegada, EcDataPrevista))
                                    numEtiq = BL_ArredondaCima(gEstruturaTubos(i).num_ana / gEstruturaTubos(i).num_max_ana) * gEstruturaTubos(i).num_copias
                                    Call EtiqPrint(gEstruturaTubos(i).GrAna, gEstruturaTubos(i).abrAna, _
                                                   CbTipoUtente.text, EcUtente, _
                                                   "", EcNumReq, IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " "), _
                                                   dataImpressao, _
                                                   gEstruturaTubos(i).CodProd & IIf(gEstruturaTubos(i).CodTubo <> "", " (", " ") & gEstruturaTubos(i).CodTubo & IIf(gEstruturaTubos(i).CodTubo <> "", ")", ""), _
                                                   EcNome, EcAbrevUte.text, gEstruturaTubos(i).codTuboBar & NumReqBar, _
                                                   BG_CvPlica(gEstruturaTubos(i).Designacao_tubo), gEstruturaTubos(i).inf_complementar, numEtiq, gEstruturaTubos(i).Descr_etiq_ana, , EcNumReqAssoc, gEstruturaTubos(i).codTuboBar)
                                    
                                    EtiqClosePrinter
                                    
                                    BL_RegistaImprEtiq EcNumReq, gEstruturaTubos(i).CodTubo
                                End If
                            End If
                        Next i
                    Next j
                End If
                
                
                If EcEtqTipo <> "2" Then
                    'Administrativas
                    For k = 1 To EtqAdministrativas
                        If EtqCrystal = "1" Then
                        
                            dataImpressao = BL_HandleNull(EcDataChegada, EcDataPrevista)
                            sql = "INSERT INTO sl_cr_etiq" & gNumeroSessao & _
                                " ( t_utente, utente, n_req, t_urg, dt_req, ordem, produto, nome_ute, n_req_bar, descr_tubo, situacao, episodio, idade) " & _
                                " VALUES (" & BL_TrataStringParaBD(CbTipoUtente.text) & "," & BL_TrataStringParaBD(EcUtente) & _
                                "'," & BL_TrataStringParaBD(EcNumReq) & ",'" & IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " ") & _
                                "'," & BL_TrataDataParaBD(dataImpressao) & ",null,null, " & _
                                BL_TrataStringParaBD(BL_AbreviaNome(EcNome, 40)) & ",'" & NumReqBar & "', 'Administrativa', '" & situacao & "', '" & episodio & "', '" & idade & "')"
                            BG_ExecutaQuery_ADO sql
                    
                            If gSQLError <> 0 Then
                                BG_Mensagem mediMsgBox, "Erro a gerar etiquetas!", vbExclamation + vbOKOnly, "Etiquetas"
                                Set CmdDadosTubo = Nothing
                                Set CmdDadosGhostM = Nothing
                                Exit Sub
                            End If
                        Else
                            ' --------
                            'Tubos(k).GrAna = "Administrat."
                            
                            If Not LerEtiqInI Then
                                MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                                Exit Sub
                            End If
                            If Not EtiqOpenPrinter(EcPrinterEtiq) Then
                                MsgBox "Imposs�vel abrir impressora etiquetas"
                                Exit Sub
                            End If
                            
                            If TotalP1s >= k Then
                                NrReqARS = p1(k).NrP1
                            Else
                                NrReqARS = ""
                            End If
                            
                            dataImpressao = BL_HandleNull(EcDataChegada, EcDataPrevista)
                            Call EtiqPrint("", _
                                           "", _
                                           CbTipoUtente.text, _
                                           EcUtente, "", _
                                           EcNumReq, _
                                           IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " "), _
                                           dataImpressao, _
                                           "", EcNome, EcAbrevUte.text, "", "Administrativa", "", "1", "", NrReqARS, EcNumReqAssoc, "")
                                           
                            EtiqClosePrinter
                            ' --------
                        
                            ' imprime etiq fim
                            'soliveira arunce
                            'chave que indica se queremos etiqueta de fim ou n�o
                            If gEtiqFim = 1 Then
                                If Not LerEtiqIni_fim Then
                                    MsgBox "Ficheiro de inicializa��o de etiquetas ausente!"
                                    Exit Sub
                                End If
                                If Not BL_EtiqOpenPrinter(EcPrinterEtiq) Then
                                    MsgBox "Imposs�vel abrir impressora etiquetas"
                                    Exit Sub
                                End If
                                
                                dataImpressao = BL_HandleNull(EcDataChegada, EcDataPrevista)
                                Call EtiqPrint("", _
                                               "", _
                                               CbTipoUtente.text, _
                                               EcUtente, "", _
                                               EcNumReq, _
                                               IIf(CbUrgencia.ListIndex <> -1, Mid(CbUrgencia.text, 1, 3), " "), _
                                               dataImpressao, _
                                               "", EcNome, EcAbrevUte.text, "", "Administrativa", "", "1", "", NrReqARS, EcNumReqAssoc, "")
                                               
                                BL_EtiqClosePrinter
                            End If
                        End If
                    Next k
                End If
                
                
                
                If EtqCrystal = "1" Then
                    'Imprime o relat�rio no Crystal Reports
            
                    Dim Report As CrystalReport
                    Set Report = forms(0).Controls("Report")
            
                    Report.SQLQuery = "SELECT " & _
                                      "     T_UTENTE, " & _
                                      "     UTENTE, " & _
                                      "     N_PROC, " & _
                                      "     N_REQ, " & _
                                      "     T_URG, " & _
                                      "     DT_REQ, " & _
                                      "     ORDEM, " & _
                                      "     PRODUTO, " & _
                                      "     NOME_UTE, " & _
                                      "     N_REQ_BAR " & _
                                      "FROM " & _
                                      "     SL_CR_ETIQ" & gNumeroSessao & " SL_CR_ETIQ"
                                        
                    For i = 0 To Printers.Count - 1
                        If Printers(i).DeviceName = EcPrinterEtiq Then
                            Report.PrinterName = Printers(i).DeviceName
                            Report.PrinterDriver = Printers(i).DriverName
                            Report.PrinterPort = Printers(i).Port
                        End If
                    Next i
                    
                    If Trim(gEtiqPreview) Then
                        Report.Destination = crptToWindow
                        Report.WindowState = crptMaximized
                        Report.PageShow Report.ReportStartPage
                    Else
                        Report.Destination = crptToPrinter
                    End If
                    
                    Call BL_ExecutaReport
                    
                    If Trim(gEtiqPreview) Then
                        Report.PageZoom (200)
                    End If
                    
                    'Elimina as Tabelas criadas para a impress�o dos relat�rios
                    Call BL_RemoveTabela("sl_cr_etiq" & gNumeroSessao)
                Else
                    'EtiqClosePrinter
                End If
            End If
            
            If Me.EcImprimirResumo = "1" Then ImprimeResumo
        End If
    End If
    
    Set CmdDadosTubo = Nothing
    Set CmdDadosGhostM = Nothing
     
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeEtiq: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeEtiq"
    Exit Sub
    Resume Next
End Sub

Sub Cria_TmpEtq()
    On Error GoTo TrataErro

    Dim TmpEtq(1 To 16) As DefTable
    
    TmpEtq(1).NomeCampo = "t_utente"
    TmpEtq(1).tipo = "STRING"
    TmpEtq(1).tamanho = 5
    
    TmpEtq(2).NomeCampo = "utente"
    TmpEtq(2).tipo = "STRING"
    TmpEtq(2).tamanho = 20
    
    TmpEtq(3).NomeCampo = "n_proc"
    TmpEtq(3).tipo = "STRING"
    TmpEtq(3).tamanho = 100
    
    TmpEtq(4).NomeCampo = "n_req"
    TmpEtq(4).tipo = "STRING"
    TmpEtq(4).tamanho = 9
    
    TmpEtq(5).NomeCampo = "t_urg"
    TmpEtq(5).tipo = "STRING"
    TmpEtq(5).tamanho = 3
    
    TmpEtq(6).NomeCampo = "dt_req"
    TmpEtq(6).tipo = "DATE"
   
    TmpEtq(7).NomeCampo = "ordem"
    TmpEtq(7).tipo = "STRING"
    TmpEtq(7).tamanho = 3
    
    TmpEtq(8).NomeCampo = "produto"
    TmpEtq(8).tipo = "STRING"
    TmpEtq(8).tamanho = 25
    
    TmpEtq(9).NomeCampo = "nome_ute"
    TmpEtq(9).tipo = "STRING"
    TmpEtq(9).tamanho = 80
    
    TmpEtq(10).NomeCampo = "n_req_bar"
    TmpEtq(10).tipo = "STRING"
    TmpEtq(10).tamanho = 20

    TmpEtq(11).NomeCampo = "gr_ana"
    TmpEtq(11).tipo = "STRING"
    TmpEtq(11).tamanho = 40

    TmpEtq(12).NomeCampo = "abr_ana"
    TmpEtq(12).tipo = "STRING"
    TmpEtq(12).tamanho = 10
    
    TmpEtq(13).NomeCampo = "descr_tubo"
    TmpEtq(13).tipo = "STRING"
    TmpEtq(13).tamanho = 40

    TmpEtq(14).NomeCampo = "situacao"
    TmpEtq(14).tipo = "STRING"
    TmpEtq(14).tamanho = 7
    
    TmpEtq(15).NomeCampo = "episodio"
    TmpEtq(15).tipo = "STRING"
    TmpEtq(15).tamanho = 15
    
    TmpEtq(16).NomeCampo = "idade"
    TmpEtq(16).tipo = "STRING"
    TmpEtq(16).tamanho = 3
    
    Call BL_CriaTabela("sl_cr_etiq" & gNumeroSessao, TmpEtq)
Exit Sub
TrataErro:
    BG_LogFile_Erros "Cria_TmpEtq: " & Err.Number & " - " & Err.Description, Me.Name, "Cria_TmpEtq"
    Exit Sub
    Resume Next
End Sub

Private Function EtiqPrint( _
    ByVal gr_ana As String, _
    ByVal abr_ana As String, _
    ByVal t_utente As String, _
    ByVal Utente As String, _
    ByVal n_proc As String, _
    ByVal n_req As String, _
    ByVal t_urg As String, _
    ByVal dt_req As String, _
    ByVal Produto As String, _
    ByVal nome_ute As String, _
    ByVal abrev_ute As String, _
    ByVal N_req_bar As String, _
    ByVal Designacao_tubo As String, _
    ByVal inf_complementar As String, _
    ByVal num_copias As Integer, _
    ByVal Descr_etiq_ana As String, _
    Optional ByVal N_REQ_ARS As String, _
    Optional ByVal n_Req_assoc As String, _
    Optional ByVal cod_etiq As String _
    ) As Boolean
     On Error GoTo TrataErro
   
    'soliveira terrugem n_req_assoc
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    If (Trim(UCase(Designacao_tubo)) = "ADMINISTRATIVA") Then
        N_req_bar = n_req
    End If

    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{GR_ANA}", gr_ana)
    sWrittenData = Replace(sWrittenData, "{ABR_ANA}", abr_ana)
    sWrittenData = Replace(sWrittenData, "{T_UTENTE}", t_utente)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{N_PROC}", n_proc)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{T_URG}", t_urg)
    sWrittenData = Replace(sWrittenData, "{DT_REQ}", dt_req)
    sWrittenData = Replace(sWrittenData, "{PRODUTO}", Produto)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", BL_AbreviaNome(BL_RemovePortuguese(StrConv(nome_ute, vbProperCase)), 40))
    sWrittenData = Replace(sWrittenData, "{ABREV_UTE}", abrev_ute)
    sWrittenData = Replace(sWrittenData, "{N_REQ_BAR}", N_req_bar)
    sWrittenData = Replace(sWrittenData, "{DS_TUBO}", Designacao_tubo)
    sWrittenData = Replace(sWrittenData, "{N_REQ_ARS}", N_REQ_ARS)
    sWrittenData = Replace(sWrittenData, "{INF_COMPLEMENTAR}", inf_complementar)
    sWrittenData = Replace(sWrittenData, "{DESCR_ETIQ_ANA}", Descr_etiq_ana)
    sWrittenData = Replace(sWrittenData, "{N_REQ_ASSOC}", n_Req_assoc)
    sWrittenData = Replace(sWrittenData, "{NUM_COPIAS}", num_copias)
    sWrittenData = Replace(sWrittenData, "{COD_ETIQ}", cod_etiq)

    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    EtiqPrint = True
    If gImprimeEPL = mediSim Then
        EtiqEndJob = Replace(EtiqEndJob, "{NUM_COPIAS}", num_copias)
    End If
Exit Function
TrataErro:
    BG_LogFile_Erros "EtiqPrint: " & Err.Number & " - " & Err.Description, Me.Name, "EtiqPrint"
    Exit Function
    Resume Next
End Function
Sub PreencheDescMedico()
    On Error GoTo TrataErro
        
    Dim Tabela As adodb.recordset
    Dim sql As String

    If EcCodMedico.text <> "" Then
        Set Tabela = New adodb.recordset
        sql = "SELECT nome_med FROM sl_medicos WHERE cod_med='" & Trim(EcCodMedico.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "M�dico inexistente!", vbOKOnly + vbInformation, "M�dicos"
            EcCodMedico.text = ""
            EcDescrMedico.text = ""
            EcCodMedico.SetFocus
        Else
            EcDescrMedico.text = Tabela!nome_med
            EcNomeMedico.text = Tabela!nome_med
        End If
        Tabela.Close
        Set Tabela = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDescMedico: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDescMedico"
    Exit Sub
    Resume Next
End Sub
Sub AcrescentaAnalise(linha As Integer)
    
    Dim i As Integer
    Dim k As Long
    Dim iRec As Integer
    Dim lastCodAgrup As String
    On Error GoTo Trata_Erro
    If gPassaRecFactus = mediSim Then
        BG_Mensagem mediMsgBox, "N�o � possivel adicionar an�lise nessa posi��o", vbOKOnly + vbInformation, "Aviso"
        Exit Sub
    End If
    'Cria linha vazia
    FGAna.AddItem "", FGAna.row
    CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
    ExecutaCodigoA = True
    Fgana_RowColChange
    
    ReDim Preserve MaReq(UBound(MaReq) + 1)
    TotalAnalisesFact = TotalAnalisesFact + 1
    ReDim Preserve AnalisesFact(TotalAnalisesFact)
    If UBound(MaReq) = 0 Then ReDim MaReq(1)
    
'    i = UBound(MaReq)
    LastRowA = linha
    LastColA = 0
    i = RegistosA.Count
    
    While i > linha
            
            RegistosA(i).codAna = RegistosA(i - 1).codAna
            RegistosA(i).descrAna = RegistosA(i - 1).descrAna
            RegistosA(i).Estado = RegistosA(i - 1).Estado
            RegistosA(i).ObsAnaReq = RegistosA(i - 1).ObsAnaReq
            RegistosA(i).seqObsAnaReq = RegistosA(i - 1).seqObsAnaReq
            RegistosA(i).NReqARS = RegistosA(i - 1).NReqARS
                        
        
        i = i - 1
    Wend
    
    'Actualiza ord_marca da estrutura MaReq
    k = 1
    lastCodAgrup = ""
    While k <= UBound(MaReq)
        If MaReq(k).Ord_Marca >= linha Then
            MaReq(k).Ord_Marca = MaReq(k).Ord_Marca + 1
            If lastCodAgrup <> MaReq(k).cod_agrup Then
                iRec = DevIndice(MaReq(k).cod_agrup)
                If iRec > -1 Then
                    RegistosRM(iRec).ReciboOrdemMarcacao = RegistosRM(iRec).ReciboOrdemMarcacao + 1
                End If
                lastCodAgrup = MaReq(k).cod_agrup
            End If
        End If
        k = k + 1
    Wend
    
    
    RegistosA(linha).codAna = ""
    RegistosA(linha).descrAna = ""
    RegistosA(linha).Estado = "-1"
    RegistosA(linha).ObsAnaReq = ""
    RegistosA(linha).seqObsAnaReq = 0
    RegistosA(linha).NReqARS = ""
    RegistosA(linha).p1 = ""
    RegistosA(linha).NReqARS = ""
    

    FGAna.TextMatrix(LastRowA, 2) = RegistosA(linha).NReqARS
    FGAna.TextMatrix(LastRowA, 3) = RegistosA(linha).p1
    
    
    
    ExecutaCodigoA = True
    FGAna.RowSel = linha
    FGAna.ColSel = 0
    Call Fgana_RowColChange
    FGAna.row = linha
    LastRowA = linha
    LastColA = 0
    
    For i = 1 To FGAna.rows - 2
        AtualisaDadosMoviFact i
    Next i
    Exit Sub

Trata_Erro:
    If Err.Number <> 35601 Then
        BG_LogFile_Erros Me.Name & "AcrescentaAnalise: " & Err.Description
    End If
    
    Resume Next


End Sub


' --------------------------------------------------------

' ALTERA ENTIDADE DE UMA ANALISE NO RECIBO MANUAL

' --------------------------------------------------------
Private Sub AlteraEntidadeReciboManual(linha As Long, codEntidade As String, DescrEFR As String, CodPai As String, linhaAna As Long)
    On Error GoTo TrataErro
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    Dim taxa As String
    Dim taxaEfr As String
    Dim valorUnitEFR As Double
    Dim mens As String
    Dim codigo As String
    Dim flg_FdsValFixo As Integer
    Dim numMembros As Integer
    Dim i As Integer
    'codigo = RegistosA(linhaAna).codAna
    
    
    If linha > 0 Then
        If RegistosRM(linha).flgCodBarras = 1 Then
            Exit Sub
        End If
        If EliminaReciboManual(linha, RegistosRM(linha).ReciboCodFacturavel, False) = False Then
            Exit Sub
        End If
    End If
    
    If codEntidade <> "" Then
        If RegistosRM.Count = 0 Or linha = 0 Then
            'linha = RegistosRM.Count
            CriaNovaClasse RegistosRM, linha, "REC_MAN"
        End If
        If RegistosRM(linha).ReciboEntidade = "" And linhaAna > mediComboValorNull Then
            RegistosRM(linha).ReciboEntidade = codEntidade
            RegistosRM(linha).ReciboCodEfrEnvio = codEntidade
            RegistosRM(linha).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", codEntidade)
            RegistosRM(linha).ReciboQuantidade = 1
            RegistosRM(linha).ReciboQuantidadeEFR = RegistosRM(linha).ReciboQuantidade
            RegistosRM(linha).ReciboNumDoc = "0"
            RegistosRM(linha).ReciboSerieDoc = "0"
            RegistosRM(linha).ReciboCodFacturavel = RegistosA(linhaAna).codAna
            RegistosRM(linha).ReciboFlgAdicionado = "0"
            RegistosRM(linha).ReciboOrdemMarcacao = 0
            If linha = 1 Then
                RegistosRM(linha).ReciboP1 = 1
                RegistosRM(linha).ReciboCodMedico = ""
            ElseIf linha > 1 Then
                RegistosRM(linha).ReciboP1 = 1
                RegistosRM(linha).ReciboCodMedico = ""
            End If
            RegistosRM(linha).ReciboFlgAdicionado = "0"
            If codEntidade = gEfrParticular Then
                RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento
            Else
                RegistosRM(linha).ReciboIsencao = BG_DaComboSel(CbTipoIsencao)
            End If
        Else
            RegistosRM(linha).ReciboEntidade = codEntidade
            RegistosRM(linha).ReciboCodEfrEnvio = codEntidade
            RegistosRM(linha).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", codEntidade)
            If RegistosRM(linha).ReciboOrdemMarcacao = 0 Then
                RegistosRM(linha).ReciboOrdemMarcacao = 0
            End If
            
        End If
        ' RECALCULA A TAXA
        codAnaEfr = ""
        If RegistosRM(linha).ReciboIsencao = gTipoIsencaoNaoIsento Then
            taxa = IF_RetornaTaxaAnalise(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, CbTipoUtente, EcUtente, BL_HandleNull(EcDataChegada, dataAct), codAnaEfr, RegistosRM(linha).ReciboDescrAnaEFR, False)
            
            taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, _
                                         EcDataPrevista, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, RegistosRM(linha).ReciboQuantidadeEFR, "", False, descrAnaEfr, mediComboValorNull)
            RegistosRM(linha).ReciboValorUnitEFR = valorUnitEFR
            RegistosRM(linha).ReciboCodAnaEFR = codAnaEfr
            RegistosRM(linha).ReciboDescrAnaEFR = descrAnaEfr
            
        Else
            taxa = 0
        End If
        Select Case taxa
            Case -1
                mens = " An�lise n�o est� mapeada para o FACTUS!"
                taxa = 0
            Case -2
                mens = "Impossivel abrir conex�o com o FACTUS!"
                taxa = 0
            Case -3
                mens = "Impossivel seleccionar portaria activa para a entidade em causa!"
                taxa = 0
            Case -4
                mens = "N�o existe taxa codificada para a an�lise " & RegistosRM(linha).ReciboCodFacturavel & "!"
                taxa = 0
            Case Else
                mens = ""
        End Select
        taxa = BG_CvDecimalParaCalculo(taxa)
        
        RegistosRM(linha).ReciboCodAnaEFR = codAnaEfr
        If mens <> "" Then
            If codEntidade <> gCodEFRSemCredencial Then
                BG_Mensagem mediMsgBox, mens, vbOKOnly + vbInformation, "Taxas"
            End If
            If CDbl(taxa) <= 0 And (BG_DaComboSel(CbTipoIsencao) = gTipoIsencaoNaoIsento Or BG_DaComboSel(CbTipoIsencao) = gTipoIsencaoIsento) And codEntidade <> gCodEFRSemCredencial Then
                RegistosRM(linha).ReciboEntidade = gEfrParticular
                RegistosRM(linha).ReciboCodEfrEnvio = gEfrParticular
                taxa = IF_RetornaTaxaAnalise(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, CbTipoUtente, EcUtente, EcDataChegada, codAnaEfr, RegistosRM(linha).ReciboDescrAnaEFR, False)
            
                taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade, _
                                             EcDataPrevista, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, RegistosRM(linha).ReciboQuantidadeEFR, "", False, descrAnaEfr, mediComboValorNull)
                RegistosRM(linha).ReciboValorUnitEFR = valorUnitEFR
                RegistosRM(linha).ReciboCodAnaEFR = codAnaEfr
                RegistosRM(linha).ReciboDescrAnaEFR = descrAnaEfr
                
                If taxa > 0 Then
                    RegistosRM(linha).ReciboTaxa = taxa
                Else
                    RegistosRM(linha).ReciboTaxa = "0,0"
                End If
            Else
                RegistosRM(linha).ReciboTaxa = "0,0"
            End If
        Else
            RegistosRM(linha).ReciboTaxa = taxa
        End If
        
        RegistosRM(linha).ReciboQuantidade = BL_HandleNull(RetornaQtd(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade), 1)
        If IF_ContaMembros(RegistosRM(linha).ReciboCodFacturavel, RegistosRM(linha).ReciboEntidade) > 0 Then
            numMembros = 0
            For i = 1 To UBound(MaReq)
                If MaReq(i).cod_agrup = RegistosRM(linha).ReciboCodFacturavel And MaReq(i).cod_ana_s <> gGHOSTMEMBER_S Then
                    numMembros = numMembros + 1
                End If
            Next i
            RegistosRM(linha).ReciboQuantidade = RegistosRM(linha).ReciboQuantidade * numMembros
        End If
            
        RegistosRM(linha).ReciboQuantidadeEFR = RegistosRM(linha).ReciboQuantidade
        RegistosRM(linha).ReciboFDS = BL_HandleNull(EcFimSemana, "0")
        RegistosRM(linha).ReciboCodigoPai = CodPai
        RegistosRM(linha).ReciboTaxaUnitario = RegistosRM(linha).ReciboTaxa
        RegistosRM(linha).ReciboTaxa = Replace(RegistosRM(linha).ReciboTaxaUnitario, ".", ",") * RegistosRM(linha).ReciboQuantidade
        RegistosRM(linha).ReciboTaxa = Round(RegistosRM(linha).ReciboTaxa, 2)
        
        RegistosRM(linha).ReciboValorEFR = Replace(RegistosRM(linha).ReciboValorUnitEFR, ".", ",") * RegistosRM(linha).ReciboQuantidadeEFR
        RegistosRM(linha).ReciboValorEFR = Round(RegistosRM(linha).ReciboValorEFR, 2)
        RegistosRM(linha).flgCodBarras = 0
        
        'REQUISI��ES DE FIM DE SEMANA DOBRA O PRE�O
        'SE ENTIDADE COM VALOR FIXO PARA FINS DE SEMANA - COLOCA A ZERO
        flg_FdsValFixo = VerificaFdsValFixo(linha)
        If RegistosRM(linha).ReciboFDS = mediSim And flg_FdsValFixo = 0 Then
            RegistosRM(linha).ReciboTaxa = RegistosRM(linha).ReciboTaxa * 2
        ElseIf RegistosRM(linha).ReciboFDS = mediSim And flg_FdsValFixo = 1 Then
            RegistosRM(linha).ReciboTaxa = "0"
        End If
        
        AdicionaAnaliseAoReciboManual linha, False
        If linhaAna > 0 Then
            FGAna.TextMatrix(linhaAna, lColFgAnaP1) = RegistosRM(linha).ReciboP1
            FGAna.TextMatrix(linhaAna, lColFgAnaCodEFR) = RegistosRM(linha).ReciboEntidade
            FGAna.TextMatrix(linhaAna, lColFgAnaDescrEFR) = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(linha).ReciboEntidade)
            FGAna.TextMatrix(linhaAna, lColFgAnaPreco) = RegistosRM(linha).ReciboTaxa
            'NELSONPSILVA GLINTT-HS-21312
            FGAna.TextMatrix(linhaAna, lColFgAnaNAmostras) = RegistosRM(linha).ReciboQuantidade
            '
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "AlteraEntidadeReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "AlteraEntidadeReciboManual"
    Exit Sub
    Resume Next
End Sub

Private Function LerEtiqInI() As Boolean
    Dim aux As String
    On Error GoTo erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Etiq.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Etiq.ini"
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqInI = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While Mid(aux, 1, 1) <> "P"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqInI = True
Exit Function
erro:
    If Err.Number = 55 Then
        Close 1
        Open s For Input As 1
        Resume Next
    End If
    BG_LogFile_Erros "Erro ao abrir Ficheiro:" & s & " " & Err.Description, Me.Name, "LerEtiqIni", True
    Exit Function
    Resume Next
End Function
Private Function LerEtiqIni_fim() As Boolean
    Dim aux As String
    On Error GoTo erro
    
    Dim s As String
    
    EtiqStartJob = ""
    EtiqJob = ""
    EtiqEndJob = ""
    
    s = gDirCliente
    If Right(s, 1) <> "\" Then s = s + "\"
    s = s + "Bin\Etiq_fim.ini"
    If Dir(s) = "" Then
        s = gDirServidor
        If Right(s, 1) <> "\" Then s = s + "\"
        s = s + "Bin\Etiq_fim.ini"
    End If

    If gImprimeEPL <> 1 Then
        Open s For Input As 1
        Line Input #1, EtiqStartJob
        Line Input #1, EtiqJob
        Line Input #1, EtiqEndJob
        Close 1
        EtiqStartJob = Trim(EtiqStartJob)
        EtiqJob = Trim(EtiqJob)
        EtiqEndJob = Trim(EtiqEndJob)
        LerEtiqIni_fim = True
    Else
            Open s For Input As 1
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            Line Input #1, aux
            EtiqStartJob = aux & vbCrLf
            
            Line Input #1, aux
            While Mid(aux, 1, 1) <> "P"
                EtiqJob = EtiqJob & aux & vbCrLf
                Line Input #1, aux
            Wend
            EtiqEndJob = EtiqEndJob & aux & vbCrLf
            Close 1
            
    End If
    EtiqStartJob = Trim(EtiqStartJob)
    EtiqJob = Trim(EtiqJob)
    EtiqEndJob = Trim(EtiqEndJob)
    LerEtiqIni_fim = True
    Exit Function
erro:
End Function
Private Function TrocaBinarios(ByVal s As String) As String
    On Error GoTo TrataErro
    
    Dim p As Long
    Dim k As Byte
    
    Do
        p = InStr(s, "&H")
        If p = 0 Then Exit Do
        k = val(Mid(s, p, 4))
        s = left(s, p - 1) + Chr(k) + Mid(s, p + 4)
    Loop
    TrocaBinarios = s
Exit Function
TrataErro:
    BG_LogFile_Erros "TrocaBinarios: " & Err.Number & " - " & Err.Description, Me.Name, "TrocaBinarios"
    Exit Function
    Resume Next
End Function

Private Function EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    On Error GoTo TrataErro
    
    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function
    
    MyDocInfo.pDocName = "Etiquetas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function
    
    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    sWrittenData = TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    
    EtiqOpenPrinter = True
Exit Function
TrataErro:
    BG_LogFile_Erros "EtiqOpenPrinter: " & Err.Number & " - " & Err.Description, Me.Name, "EtiqOpenPrinter"
    Exit Function
    Resume Next
End Function

Private Function EtiqClosePrinter() As Boolean
    On Error GoTo TrataErro
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    EtiqClosePrinter = True
Exit Function
TrataErro:
    BG_LogFile_Erros "EtiqClosePrinter: " & Err.Number & " - " & Err.Description, Me.Name, "EtiqClosePrinter"
    Exit Function
    Resume Next
End Function

Private Sub EcNome_LostFocus()
     On Error GoTo TrataErro
   
    If gNomeCapitalizado = 1 Then
        EcNome.text = StrConv(EcNome.text, vbProperCase)
    Else
        EcNome.text = UCase(EcNome.text)
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "EcNome_LostFocus: " & Err.Number & " - " & Err.Description, Me.Name, "EcNome_LostFocus"
    Exit Sub
    Resume Next
End Sub

' --------------------------------------------------------

' VERIFICA SE ANALISE JA FOI MARCADA NOUTRA REQUUISICAO NO PROPRIO DIA

' --------------------------------------------------------
Private Function VerificaAnaliseJaMarcada(n_req As String, codAgrup As String) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsAna As New adodb.recordset
    Dim data As Integer
    Dim Data2 As String
    data = -1
    ssql = "SELECT prazo_Val FROM slv_analises WHERE cod_ana = " & BL_TrataStringParaBD(codAgrup) & " AND prazo_val IS NOT NULL "
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAna.Open ssql, gConexao
    If rsAna.RecordCount > 0 Then
        data = BL_HandleNull(rsAna!prazo_val, 0)
    End If
    rsAna.Close
    If data = -1 Then
        data = 0
    End If
    Data2 = CStr(CDate(dataAct) - data)
    ssql = "SELECT distinct x2.n_Req FROM  sl_requis x2, sl_marcacoes x3 WHERE x2.dt_previ >= " & BL_TrataDataParaBD(Data2)
    ssql = ssql & " AND x2.dt_previ <= " & BL_TrataStringParaBD(dataAct)
    ssql = ssql & " AND x2.seq_utente = " & EcSeqUtente & " AND x2.n_Req = x3.n_req   AND x3.cod_agrup = " & BL_TrataStringParaBD(codAgrup)
    If n_req <> "" Then
        ssql = ssql & " AND x2.n_req <> " & n_req
    End If
    ssql = ssql & " UNION SELECT distinct x2.n_Req FROM  sl_requis x2, sl_realiza x3 WHERE x2.dt_previ >= " & BL_TrataDataParaBD(Data2)
    ssql = ssql & " AND x2.dt_previ <= " & BL_TrataStringParaBD(dataAct)
    ssql = ssql & " AND x2.seq_utente = " & EcSeqUtente & " AND x2.n_Req = x3.n_req AND x3.cod_agrup = " & BL_TrataStringParaBD(codAgrup)
    If n_req <> "" Then
        ssql = ssql & " AND x2.n_req <> " & n_req
    End If
    rsAna.CursorType = adOpenStatic
    rsAna.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAna.Open ssql, gConexao
    If rsAna.RecordCount > 0 Then
        VerificaAnaliseJaMarcada = True
        MsgBox "A an�lise j� foi marcada  para o Utente na requisi��o:" & rsAna!n_req & " !"
    Else
        VerificaAnaliseJaMarcada = False
    End If
    rsAna.Close
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaAnaliseJaMarcada: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaAnaliseJaMarcada"
    Exit Function
    Resume Next
End Function


' --------------------------------------------------------

' ADICIONA ANALISE AO RECIBO MANUAL

' --------------------------------------------------------
Private Function AdicionaReciboManual(ByVal flg_manual As Integer, ByVal codEfr As String, ByVal nRec As String, ByVal sRec As String, ByVal CodFacturavel As String, _
                                 ByVal quantidade As Integer, ByVal CodigoPai As String, ByVal codAna As String, ByVal codBarras As Boolean) As Integer
    On Error GoTo TrataErro
    Dim anaFacturar As String
    Dim taxa As String
    Dim taxaEfr As String
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    Dim mens As String
    Dim indice As Long
    Dim flg_FdsValFixo As Integer
    Dim AnaRegra As String
    Dim p1 As String
    Dim corP1 As String
    Dim valorUnitEFR As Double
    Dim iAux As Integer
    If CodFacturavel = "" Then Exit Function
    If IF_AnaliseMapeada(codEfr, BL_HandleNull(codAna, CodFacturavel)) = False Then
        BG_Mensagem mediMsgBox, " An�lise n�o est� mapeada para o FACTUS!", vbOKOnly + vbInformation, "Taxas"
        Exit Function
    End If
    
    indice = BL_HandleNull(RegistosRM.Count, 0)
    If indice = 0 Then
        CriaNovaClasse RegistosRM, 1, "REC_MAN"
        indice = 1
    End If
    ' -----------------------------------------
    ' ENTIDADE E NUMERO DE DOCUMENTO
    ' -----------------------------------------
    RegistosRM(indice).ReciboEntidade = codEfr
    RegistosRM(indice).ReciboCodEfrEnvio = codEfr
    RegistosRM(indice).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", codEfr)
    RegistosRM(indice).ReciboNumDoc = nRec
    RegistosRM(indice).ReciboSerieDoc = sRec
    RegistosRM(indice).ReciboCodAna = BL_HandleNull(codAna, CodFacturavel)
    RegistosRM(indice).ReciboCodFacturavel = BL_HandleNull(UCase(CodFacturavel), "0")
    If VerificaCodFacturavel(RegistosRM(indice).ReciboCodFacturavel) = False Then
        RegistosRM(indice).ReciboCodFacturavel = codAna
    End If
    RegistosRM(indice).ReciboFlgAdicionado = "0"
    RegistosRM(indice).ReciboAnaNaoFact = IF_Verifica_Regra_PRIVADO(RegistosRM(indice).ReciboCodFacturavel, codEfr)
    anaFacturar = IF_VerificaFacturarNovaAnalise(RegistosRM(indice).ReciboCodFacturavel, codEfr)
    If anaFacturar <> "" Then
        gMsgTitulo = "Inserir"
        gMsgMsg = "Quer facturar a an�lise: " & BL_SelCodigo("SLV_ANALISES_FACTUS", "DESCR_ANA", "COD_ANA", anaFacturar) & "?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
        If gMsgResp = vbYes Then
            CriaNovaClasse RegistosRM, indice, "REC_MAN"
            AdicionaReciboManual 1, codEfr, "0", "0", anaFacturar, 1, RegistosRM(indice).ReciboCodAna, anaFacturar, codBarras
        End If
    End If

    
    ' -----------------------------------------
    ' TAXA
    ' -----------------------------------------
    codAnaEfr = ""
    descrAnaEfr = ""
    If RegistosRM(indice).ReciboEntidade <> gCodEFRSemCredencial Then
        If BG_DaComboSel(CbTipoIsencao) = gTipoIsencaoNaoIsento Or BG_DaComboSel(CbTipoIsencao) = gTipoIsencaoIsento Or _
            codAna = GCodAnaActMed Then
            taxa = IF_RetornaTaxaAnalise(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade, _
                                         CbTipoUtente, EcUtente, EcDataPrevista, codAnaEfr, descrAnaEfr, False)
    
            taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade, _
                                         EcDataPrevista, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, quantidade, "", False, descrAnaEfr, mediComboValorNull)
            RegistosRM(indice).ReciboValorUnitEFR = valorUnitEFR
            taxa = Replace(taxa, ".", ",")
            taxaEfr = Replace(taxaEfr, ".", ",")
            If Mid(taxa, 1, 1) = "," Then
                taxa = "0" & taxa
            End If
            
            If BG_DaComboSel(CbTipoIsencao) = gTipoIsencaoIsento And CDbl(taxa) > 0 Then
                taxa = "0,0"
            End If
            
            Select Case taxa
                Case "-1"
                    mens = " An�lise n�o est� mapeada para o FACTUS!"
                    taxa = 0
                Case "-2"
                    mens = "Impossivel abrir conex�o com o FACTUS!"
                    taxa = 0
                Case "-3"
                    mens = "Impossivel seleccionar portaria activa!"
                    taxa = 0
                Case "-4"
                    mens = "An�lise n�o comparticipada. Pre�o PARTICULAR!"
                    taxa = 0
                Case "-5"
                    mens = "N�o existe tabela codificada para a entidade em causa!"
                    taxa = 0
                Case Else
                    mens = ""
            End Select
        Else
            taxa = "0,0"
        End If
    Else
        taxa = "0,0"
    End If
    taxa = BG_CvDecimalParaCalculo(taxa)
    RegistosRM(indice).ReciboCodAnaEFR = codAnaEfr
    RegistosRM(indice).ReciboDescrAnaEFR = descrAnaEfr
    If mens <> "" And gEfrParticular <> mediComboValorNull Then
        BG_Mensagem mediMsgBox, mens, vbOKOnly + vbInformation, "Taxas"
        If CDbl(taxa) <= 0 And (BG_DaComboSel(CbTipoIsencao) = gTipoIsencaoNaoIsento Or BG_DaComboSel(CbTipoIsencao) = gTipoIsencaoIsento) Then
            RegistosRM(indice).ReciboEntidade = gEfrParticular
            RegistosRM(indice).ReciboCodEfrEnvio = gEfrParticular
            RegistosRM(indice).ReciboDescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", gEfrParticular)
        End If
        taxa = IF_RetornaTaxaAnalise(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade, CbTipoUtente, EcUtente, EcDataChegada, codAnaEfr, RegistosRM(indice).ReciboDescrAnaEFR, False)
        taxaEfr = IF_RetornaTaxaAnaliseEntidade(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade, _
                                     EcDataPrevista, codAnaEfr, CbTipoUtente, EcUtente, valorUnitEFR, quantidade, "", False, descrAnaEfr, mediComboValorNull)
        RegistosRM(indice).ReciboValorUnitEFR = valorUnitEFR
        If taxa > 0 Then
            RegistosRM(indice).ReciboTaxa = Replace(taxa, ".", ",")
        Else
            RegistosRM(indice).ReciboTaxa = "0,0"
        End If
    Else
        RegistosRM(indice).ReciboTaxa = taxa
    End If
    taxa = BG_CvDecimalParaCalculo(taxa)
    taxaEfr = BG_CvDecimalParaCalculo(taxaEfr)
    
    RegistosRM(indice).ReciboFDS = BL_HandleNull(EcFimSemana, "0")
    RegistosRM(indice).ReciboCodigoPai = BL_HandleNull(CodigoPai, "")
    If quantidade <= 1 Then
        RegistosRM(indice).ReciboQuantidade = BL_HandleNull(RetornaQtd(RegistosRM(indice).ReciboCodFacturavel, RegistosRM(indice).ReciboEntidade), 1)
    Else
        RegistosRM(indice).ReciboQuantidade = quantidade
    End If
    RegistosRM(indice).ReciboQuantidadeEFR = RegistosRM(indice).ReciboQuantidade
    RegistosRM(indice).ReciboTaxaUnitario = RegistosRM(indice).ReciboTaxa
    RegistosRM(indice).ReciboTaxa = RegistosRM(indice).ReciboTaxaUnitario * RegistosRM(indice).ReciboQuantidade
    RegistosRM(indice).ReciboValorEFR = RegistosRM(indice).ReciboValorUnitEFR * RegistosRM(indice).ReciboQuantidadeEFR
    
    'REQUISI��ES DE FIM DE SEMANA DOBRA O PRE�O
    'SE TIVER VALOR FIXO - COLOCA PRE�O A ZERO
    flg_FdsValFixo = VerificaFdsValFixo(indice)
    If RegistosRM(indice).ReciboFDS = mediSim And flg_FdsValFixo = 0 Then
        RegistosRM(indice).ReciboTaxa = RegistosRM(indice).ReciboTaxa * 2 * RegistosRM(indice).ReciboQuantidade
    ElseIf RegistosRM(indice).ReciboFDS = mediSim And flg_FdsValFixo = 1 Then
        RegistosRM(indice).ReciboTaxa = "0"
    End If
    
    RegistosRM(indice).ReciboTaxa = Round(RegistosRM(indice).ReciboTaxa, 2)
    RegistosRM(indice).ReciboFlgReciboManual = flg_manual
    RegistosRM(indice).ReciboFlgMarcacaoRetirada = "0"
    
    ' -----------------------------------------
    ' VERIFICA SE APLICA A REGRA
    ' -----------------------------------------
    AnaRegra = ""
    AnaRegra = VerificaRegra(indice)
    If AnaRegra <> "" Then
        RegistosRM(indice).ReciboTaxa = "0,0"
        RegistosRM(indice).ReciboFlgRetirado = "1"
        BG_Mensagem mediMsgBox, "An�lise gratuita. An�lise " & AnaRegra & " marcada.", vbInformation, "An�lise"
    End If
    
    ' -----------------------------------------
    ' P1
    ' -----------------------------------------
    If indice = 1 Then
        If EcCredAct.text <> "" Then
            p1 = BL_HandleNull(EcCredAct.text, "1")
        Else
            p1 = 1
        End If
        corP1 = "B"
        RegistosRM(indice).ReciboCodMedico = BL_HandleNull(EcCodMedico, "")
        RegistosRM(indice).ReciboNrBenef = BL_HandleNull(EcNumBenef, "")
    ElseIf indice > 1 Then
        If flg_manual = 0 Then
            If FGAna.row > 1 Then
                iAux = -1
                iAux = DevIndice(FGAna.TextMatrix(FGAna.row - 1, 0))
                If iAux > -1 Then
                    If EcCredAct.text <> "" Then
                        p1 = BL_HandleNull(EcCredAct.text, BL_HandleNull(Trim(RegistosRM(iAux).ReciboP1), "-1"))
                    Else
                        p1 = BL_HandleNull(Trim(RegistosRM(iAux).ReciboP1), "-1")
                    End If
                    If Flg_IncrementaP1 = True Then
                        If IsNumeric(p1) Then
                            p1 = p1 + 1
                            corP1 = "B"
                        End If
                    End If
                End If
            End If
        Else
            If EcCredAct.text <> "" Then
                p1 = BL_HandleNull(EcCredAct.text, BL_HandleNull(Trim(RegistosRM(indice - 1).ReciboP1), "-1"))
            Else
                p1 = BL_HandleNull(Trim(RegistosRM(indice - 1).ReciboP1), "-1")
            End If
            corP1 = BL_HandleNull(RegistosRM(indice - 1).ReciboCorP1, "B")
        End If
        If p1 = "" Then
            If indice > 1 Then
                If EcCredAct.text <> "" Then
                    p1 = BL_HandleNull(EcCredAct.text, BL_HandleNull(Trim(RegistosRM(indice - 1).ReciboP1), "1"))
                Else
                    p1 = BL_HandleNull(Trim(RegistosRM(indice - 1).ReciboP1), "1")
                End If
            End If
        End If
                
                
        RegistosRM(indice).ReciboCodMedico = RegistosRM(indice - 1).ReciboCodMedico
        RegistosRM(indice).ReciboNrBenef = RegistosRM(indice - 1).ReciboNrBenef
    End If
    RegistosRM(indice).ReciboP1 = p1
    RegistosRM(indice).ReciboCorP1 = corP1
    RegistosRM(indice).ReciboFlgAutomatico = 0
    RegistosRM(indice).flgCodBarras = 0
    'ORDEM DE MARCACAO
    RegistosRM(indice).ReciboOrdemMarcacao = RetornaOrdemP1(indice)
    ' -----------------------------------------
    ' PERFIL MARCACAO - GUARDADO NA TABELA SL_RECIBOS_DET
    ' -----------------------------------------
    RegistosRM(indice).ReciboFlgAdicionado = "0"
    'soliveira lacto **
    If BG_DaComboSel(CbTipoIsencao) = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "Deve indicar primeiro o tipo de isen��o!", vbInformation, "Tipo de Isen��o"
        SSTGestReq.Tab = 0
        Exit Function
    End If
    RegistosRM(indice).ReciboIsencao = BG_DaComboSel(CbTipoIsencao)
    'RegistosRM(indice).indiceAna = indiceAna
        
    ' -----------------------------------------
    ' ADICIONA ANALISE � ESTRUTURA DE RECIBOS SE NAO FOR ISENTO
    ' -----------------------------------------
    If RegistosRM(indice).ReciboEntidade <> mediComboValorNull Then
        AdicionaAnaliseAoReciboManual indice, False
    End If
    AdicionaReciboManual = indice
    CriaNovaClasse RegistosRM, indice, "REC_MAN"
Exit Function
TrataErro:
    AdicionaReciboManual = -1
    BG_LogFile_Erros "AdicionaReciboManual: " & Err.Number & " - " & Err.Description, Me.Name, "AdicionaReciboManual"
    Exit Function
    Resume Next
End Function

' --------------------------------------------------------

' ADICIONA ANALISE AO RECIBO MANUAL

' --------------------------------------------------------
Private Function VerificaRegra(indice As Long) As String
    On Error GoTo TrataErro
    Dim i As Long
    Dim iRec As Long
    
    VerificaRegra = ""
    
    For i = 1 To RegistosA.Count
        iRec = DevIndice(RegistosA(i).codAna)
        If RegistosA(i).codAna <> "" And iRec > -1 Then
        
            If RegistosA(i).codAna = RegistosRM(indice).ReciboAnaNaoFact Then
                If RegistosRM(iRec).ReciboIsencao = gTipoIsencaoNaoIsento And RegistosRM(iRec).ReciboEntidade = RegistosRM(iRec).ReciboEntidade Then
                    VerificaRegra = RegistosA(i).descrAna
                End If
            End If
            
            If RegistosRM(indice).ReciboCodFacturavel = RegistosRM(iRec).ReciboAnaNaoFact Then
                If RegistosRM(iRec).ReciboIsencao = gTipoIsencaoNaoIsento And RegistosRM(iRec).ReciboEntidade = RegistosA(i).ReciboEntidade Then
                    If VerificaPodeAlterarRecibo(i) = True Then
                        If EliminaReciboManual(iRec, RegistosA(i).codAna, False) = True Then
                            RegistosRM(iRec).ReciboTaxa = "0,0"
                            RegistosRM(iRec).ReciboFlgRetirado = "1"
                            FGAna.TextMatrix(i, lColFgAnaPreco) = "0,0"
                            BG_Mensagem mediMsgBox, "An�lise gratuita. An�lise " & RegistosA(indice).descrAna & " marcada.", vbInformation, "An�lise"
                            AdicionaAnaliseAoReciboManual iRec, False
                        End If
                    End If
                End If
            End If
        End If
    Next
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaRegra: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaRegra"
    VerificaRegra = ""
    Exit Function
    Resume Next
End Function


Private Function VerificaRegraSexoMarcar(codAgrup As String, seq_utente As String) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsAux As New adodb.recordset
    Dim Sexo As String
    
    If Mid(codAgrup, 1, 1) = "S" Then
        ssql = "SELECT flg_sexo FROM sl_ana_s where cod_ana_S = " & BL_TrataStringParaBD(codAgrup)
    ElseIf Mid(codAgrup, 1, 1) = "C" Then
        ssql = "SELECT flg_sexo FROM sl_ana_c where cod_ana_c = " & BL_TrataStringParaBD(codAgrup)
    ElseIf Mid(codAgrup, 1, 1) = "P" Then
        ssql = "SELECT flg_sexo FROM sl_perfis where cod_perfis = " & BL_TrataStringParaBD(codAgrup)
    End If
    rsAux.CursorType = adOpenStatic
    rsAux.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAux.Open ssql, gConexao
    If rsAux.RecordCount > 0 Then
        If BL_HandleNull(rsAux!flg_sexo, "") <> "" Then
            Sexo = rsAux!flg_sexo
            rsAux.Close
            
            ssql = "SELECT sexo_ute FROM sl_identif where seq_utente = " & seq_utente
            rsAux.CursorType = adOpenStatic
            rsAux.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros ssql
            rsAux.Open ssql, gConexao
            If rsAux.RecordCount > 0 Then
                If BL_HandleNull(rsAux!sexo_ute, "") = Sexo Then
                    rsAux.Close
                    VerificaRegraSexoMarcar = True
                    Exit Function
                Else
                    rsAux.Close
                    VerificaRegraSexoMarcar = False
                    Exit Function
                End If
            Else
                rsAux.Close
                VerificaRegraSexoMarcar = False
                Exit Function
            End If
            
        Else
            rsAux.Close
            VerificaRegraSexoMarcar = True
            Exit Function
        End If
    
    End If
    Set rsAux = Nothing
 Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaRegraSexoMarcar: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaRegraSexoMarcar"
    VerificaRegraSexoMarcar = False
    Exit Function
    Resume Next
   
End Function

Public Sub ImprimeRecibo(descricao As String)
    On Error GoTo TrataErro
    If gPassaRecFactus <> mediSim Then
        RECIBO_ImprimeRecibo descricao, EcNumReq, RegistosR(FGRecibos.row).NumDoc, RegistosR(FGRecibos.row).SerieDoc, _
                             RegistosR(FGRecibos.row).codEntidade, EcPrinterRecibo, RegistosR(FGRecibos.row).SeqUtente
    Else
        FACTUS_ImprimeRecibo descricao, EcNumReq.text, RecibosNew(FGRecibos.row).n_Fac_doe, RecibosNew(FGRecibos.row).serie_fac_doe, _
                            RecibosNew(FGRecibos.row).cod_efr, EcPrinterRecibo.text, RecibosNew(FGRecibos.row).seq_utente, RecibosNew(FGRecibos.row).tipo
    End If
BL_MudaCursorRato mediMP_Activo, Me
Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeRecibo: " & Err.Number & " - " & Err.Description, Me.Name, "ImprimeRecibo"
    Exit Sub
    Resume Next
End Sub

Sub VerificaUtentePagaActoMedico(codEfr As String, serieRec As String, NumDoc As String)
    On Error GoTo TrataErro
    Dim RsAM As adodb.recordset
    Dim sql As String
    Dim i As Integer
    Dim Flg_ExisteActoMed As Boolean
    
    Set RsAM = New adodb.recordset
    RsAM.CursorLocation = adUseServer
    RsAM.CursorType = adOpenStatic
    sql = "select flg_acto_med_ute from sl_efr where cod_efr = " & Trim(codEfr)
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsAM.Open sql, gConexao
    If RsAM.RecordCount > 0 Then
        If BL_HandleNull(RsAM!flg_acto_med_ute, 0) = 1 Then
            For i = 1 To RegistosRM.Count
                If Trim(RegistosRM(i).ReciboCodFacturavel) <> "" Then
                    If Trim(RegistosRM(i).ReciboEntidade) = codEfr And Trim(RegistosRM(i).ReciboSerieDoc) = serieRec And Trim(RegistosRM(i).ReciboNumDoc) = NumDoc And RegistosRM(i).ReciboCodFacturavel = GCodAnaActMed Then
                        Flg_ExisteActoMed = True
                    End If
                End If
            Next i
            
            If Flg_ExisteActoMed = False Then
                AdicionaReciboManual 1, codEfr, NumDoc, serieRec, GCodAnaActMed, 1, "", GCodAnaActMed, False
                FGRecibos_DblClick
            End If
        End If
    End If
 Exit Sub
TrataErro:
    BG_LogFile_Erros "VerificaUtentePagaActoMedico: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaUtentePagaActoMedico"
    Exit Sub
    Resume Next
End Sub


Private Function VerificaObrigaNrBenef(codEfr As String) As Boolean
    On Error GoTo TrataErro
    Dim RsAM As adodb.recordset
    Dim sql As String
    Dim i As Integer
    Dim Flg_ExisteActoMed As Boolean
    
    Set RsAM = New adodb.recordset
    RsAM.CursorLocation = adUseServer
    RsAM.CursorType = adOpenStatic
    sql = "select FLG_OBRIGA_NR_BENEF from sl_efr where cod_efr = " & Trim(codEfr)
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsAM.Open sql, gConexao
    If RsAM.RecordCount > 0 Then
        If BL_HandleNull(RsAM!FLG_OBRIGA_NR_BENEF, 0) = 1 Then
            VerificaObrigaNrBenef = True
        Else
            VerificaObrigaNrBenef = False
        End If
    Else
        VerificaObrigaNrBenef = False
    End If
    RsAM.Close
 Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaObrigaNrBenef: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaObrigaNrBenef"
    VerificaObrigaNrBenef = False
    Exit Function
    Resume Next
End Function

Function VerificaCodFacturavel(CodFacturavel As String) As Boolean
    On Error GoTo TrataErro
    Dim RsVerf As adodb.recordset
    Dim sql As String
    Set RsVerf = New adodb.recordset
    RsVerf.CursorLocation = adUseServer
    RsVerf.CursorType = adOpenStatic
    sql = "select * from sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(CodFacturavel)
    sql = sql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, dataAct)) & " BETWEEN "
    sql = sql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsVerf.Open sql, gConexao
    If RsVerf.RecordCount = 0 Then
        VerificaCodFacturavel = False
    Else
        VerificaCodFacturavel = True
    End If
    RsVerf.Close
    Set RsVerf = Nothing
 Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaCodFacturavel: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaCodFacturavel"
    VerificaCodFacturavel = False
    Exit Function
    Resume Next
End Function


' -------------------------------------------------------------------

' VERIFICA SE CODIGO � CODIGO PAI DE ALGUMA ANALISE DO RECIBO MANUAL

' -------------------------------------------------------------------
Private Sub VerificaEFRCodigoFilho(CodPai As String, cod_efr As String, descr_efr As String, linhaAna As Long)
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodigoPai = CodPai Then
            AlteraEntidadeReciboManual i, cod_efr, descr_efr, CodPai, linhaAna
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "erro ao verificar codigos pai: " & Err.Number & " - " & Err.Description, Me.Name, "VerificacodigoPai"
    Exit Sub
    Resume Next
End Sub


' -------------------------------------------------------------------

' VERIFICA SE CODIGO � CODIGO PAI DE ALGUMA ANALISE DO RECIBO MANUAL

' -------------------------------------------------------------------
Private Sub EliminaCodigoFilho(CodPai As String)
    Dim i As Long
    On Error GoTo TrataErro
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodigoPai = CodPai Then
            EliminaReciboManual i, RegistosRM(i).ReciboCodFacturavel, True
            LimpaColeccao RegistosRM, i
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "erro ao Eliminar codigos filhos: " & Err.Number & " - " & Err.Description, Me.Name, "EliminaCodigoFilho"
    Exit Sub
    Resume Next
End Sub


Sub ActualizaFlgAparTrans()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim RsTrans As adodb.recordset
    Dim sql As String
    
    For i = 1 To UBound(MaReq)
        Set RsTrans = New adodb.recordset
        RsTrans.CursorLocation = adUseServer
        RsTrans.CursorType = adOpenStatic
        sql = "SELECT flg_apar_trans,n_folha_trab FROM sl_marcacoes WHERE n_req = " & EcNumReq.text & " and cod_perfil = " & UCase(BL_TrataStringParaBD(MaReq(i).Cod_Perfil)) & " And cod_ana_c = " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_c)) & " and cod_ana_s= " & UCase(BL_TrataStringParaBD(MaReq(i).cod_ana_s))
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        RsTrans.Open sql, gConexao
        If Not RsTrans.EOF Then
            MaReq(i).Flg_Apar_Transf = BL_HandleNull(RsTrans!flg_apar_trans, 0)
            MaReq(i).N_Folha_Trab = BL_HandleNull(RsTrans!N_Folha_Trab, 0)
        Else
            MaReq(i).Flg_Apar_Transf = 0
            MaReq(i).N_Folha_Trab = 0
        End If
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "ActualizaFlgAparTrans: " & Err.Number & " - " & Err.Description, Me.Name, "ActualizaFlgAparTrans"
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------------------------------------

' RETORNA QUANTIDADE DE MAPEADA PARA FACTURACAO

' -----------------------------------------------------------------------------------------------

Public Function RetornaQtd(codAna As String, cod_efr As String) As String
    Dim ssql As String
    Dim rsAna As New adodb.recordset
    On Error GoTo TrataErro
        ssql = "SELECT qtd FROM sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(codAna) & " AND cod_efr = " & cod_efr
        ssql = ssql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, dataAct)) & " BETWEEN "
        ssql = ssql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsAna.Open ssql, gConexao
        If rsAna.RecordCount > 0 Then
            RetornaQtd = BL_HandleNull(rsAna!qtd, "")
        Else
            rsAna.Close
            ssql = "SELECT qtd FROM sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(codAna) & " AND cod_efr IS NULL "
            ssql = ssql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, dataAct)) & " BETWEEN "
            ssql = ssql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros ssql
            rsAna.Open ssql, gConexao
            If rsAna.RecordCount > 0 Then
                RetornaQtd = BL_HandleNull(rsAna!qtd, "")
            End If
        End If
        rsAna.Close
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "RetornaQtd"
    RetornaQtd = ""
    Exit Function
    Resume Next
End Function


Private Sub FgTubos_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    FgTubos_DevolveColunaActual X
    FgTubos_DevolveLinhaActual Y
    If TuboLinhaActual > 0 Then
        If TuboLinhaActual <= gTotalTubos Then
            FGTubos.ToolTipText = gEstruturaTubos(TuboLinhaActual).normaTubo
        End If
    Else
        FGTubos.ToolTipText = ""
    End If
End Sub



' ----------------------------------------------------------------------

' RETORNA A COLUNA ACTUAL

' ----------------------------------------------------------------------
Private Sub FgTubos_DevolveColunaActual(X As Single)
    Dim coluna As Integer
    For coluna = 0 To FGTubos.Cols - 1
        If FGTubos.ColPos(coluna) <= X And FGTubos.ColPos(coluna) + FGTubos.ColWidth(coluna) >= X Then
            TuboColunaActual = coluna
            Exit Sub
        End If
    Next
    TuboColunaActual = -1
End Sub


' ----------------------------------------------------------------------

' RETORNA A LINHA ACTUAL

' ----------------------------------------------------------------------
Private Sub FgTubos_DevolveLinhaActual(Y As Single)
    Dim linha As Integer
        For linha = 0 To FGTubos.rows - 1
            If FGTubos.RowPos(linha) <= Y And FGTubos.RowPos(linha) + FGTubos.RowHeight(linha) >= Y Then
                TuboLinhaActual = linha
                Exit Sub
            End If
        Next
        TuboLinhaActual = -1
End Sub

' ----------------------------------------------------------------------

' PARA CADA ANALISE MARCADA NO TOP, REGISTA-A!

' ----------------------------------------------------------------------
Public Sub PreencheTopAna()
    Dim CamposRetorno As New ClassPesqResultados
    Dim cancelou As Boolean
    Dim total As Integer
    Dim Resultados(100) As Variant
    Dim i As Integer
    On Error GoTo TrataErro
    
    CamposRetorno.InicializaResultados 1
    FormMarcAnaTop.Inicializa CamposRetorno
    FormMarcAnaTop.Show vbModal
    CamposRetorno.RetornaResultados Resultados(), cancelou, total
    For i = 1 To total
        EcAuxAna.Enabled = True
        EcAuxAna = Resultados(i)
        EcAuxAna_KeyDown 13, 0
    Next i
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheTopAna: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheTopAna"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' VERIFICA SE ENTIDADE TEM VALOR FIXO PARA FINS DE SEMANA

' ----------------------------------------------------------------------
Private Function VerificaFdsValFixo(indice As Long) As Long
    Dim i As Long
    Dim ssql As String
    Dim RsEFR As New adodb.recordset
    On Error GoTo TrataErro
    
    For i = 1 To RegistosR.Count
        If RegistosRM(indice).ReciboEntidade = RegistosR(i).codEntidade And RegistosRM(indice).ReciboNumDoc = RegistosR(i).NumDoc And _
           RegistosRM(indice).ReciboSerieDoc = RegistosR(i).SerieDoc Then
           VerificaFdsValFixo = RegistosR(i).flg_FdsFixo
           Exit Function
        End If
    Next
    
    ssql = "SELECT * FROM sl_efr where cod_efr = " & RegistosRM(indice).ReciboEntidade
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    RsEFR.Open ssql, gConexao
    If RsEFR.RecordCount > 0 Then
        VerificaFdsValFixo = BL_HandleNull(RsEFR!flg_fds_fixo, 0)
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaFdsValFixo: " & Err.Number & " - " & Err.Description, Me.Name, "VerificaFdsValFixo"
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------

' PREENCHE ALGUNS DADOS DA ENTIDADE COM APENAS UM ACESSO � BD

' ----------------------------------------------------------------------
Private Sub PreencheDadosEntidade(ByRef RegistosR As Collection, indice As Long)
    Dim ssql As String
    Dim RsEFR As New adodb.recordset
    On Error GoTo TrataErro
    
    ssql = "SELECT * FROM sl_efr where cod_efr = " & RegistosR(indice).codEntidade
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    RsEFR.Open ssql, gConexao
    If RsEFR.RecordCount > 0 Then
        RegistosR(indice).DescrEntidade = BL_HandleNull(RsEFR!descr_efr, "")
        RegistosR(indice).codEmpresa = BL_HandleNull(RsEFR!cod_empresa, "")
        RegistosR(indice).flg_FdsFixo = BL_HandleNull(RsEFR!flg_fds_fixo, 0)
        RegistosR(indice).NumCopias = BL_HandleNull(RsEFR!num_copias_rec, 0)
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDadosEntidade: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDadosEntidade"
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' RECALCULA VALOR DO RECIBO

' ----------------------------------------------------------------------
Public Sub RecalculaRecibo(indice As Long)
    On Error GoTo TrataErro
    Dim i As Long
    Dim taxa As Double
    If RegistosR(indice).Estado = gEstadoReciboNaoEmitido Or RegistosR(indice).Estado = gEstadoReciboNulo Or _
        RegistosR(indice).Estado = gEstadoReciboPerdido Then
        taxa = 0
        For i = 1 To RegistosRM.Count
            If RegistosRM(i).ReciboEntidade = RegistosR(indice).codEntidade And RegistosRM(i).ReciboNumDoc = RegistosR(indice).NumDoc And RegistosRM(i).ReciboSerieDoc = RegistosR(indice).SerieDoc Then
                taxa = taxa + RegistosRM(i).ReciboTaxa
            End If
        Next
        RegistosR(indice).ValorPagar = taxa
        RegistosR(indice).ValorOriginal = taxa
        FGRecibos.TextMatrix(indice, lColRecibosValorPagar) = taxa
        If RegistosR(indice).ValorPagar = 0 Then
            RegistosR(indice).Estado = gEstadoReciboNulo
            FGRecibos.TextMatrix(indice, lColRecibosEstado) = "Recibo Nulo"
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "RecalculaRecibo: " & Err.Number & " - " & Err.Description, Me.Name, "RecalculaRecibo"
    Exit Sub
    Resume Next
End Sub


Private Sub RegistaAlteracoesRequis(num_requis As String)
    Dim i As Integer
    Dim ValorControloNovo As String
    Dim seq_alteracao As Integer
    seq_alteracao = BL_ContaAltRequis(num_requis) + 1

    For i = 0 To NumCampos - 1
        ValorControloNovo = ""
        'TextBox, Label, MaskEdBox, RichTextBox
        If TypeOf CamposEc(i) Is TextBox Or _
           TypeOf CamposEc(i) Is Label Or _
           TypeOf CamposEc(i) Is MaskEdBox Or _
           TypeOf CamposEc(i) Is RichTextBox Then
           
            ValorControloNovo = CamposEc(i)
        'DataCombo
        ElseIf TypeOf CamposEc(i) Is DataCombo Then
            ValorControloNovo = BG_DataComboSel(CamposEc(i))
            If ValorControloNovo = mediComboValorNull Then
                ValorControloNovo = ""
            End If
        'ComboBox
        ElseIf TypeOf CamposEc(i) Is ComboBox Then
            ValorControloNovo = BG_DaComboSel(CamposEc(i))
            If ValorControloNovo = mediComboValorNull Then
                ValorControloNovo = ""
            End If
        'CheckBox
        ElseIf TypeOf CamposEc(i) Is CheckBox Then
            If CamposEc(i).value = 0 Then
                ValorControloNovo = "0"
            ElseIf CamposEc(i).value = 1 Then
                ValorControloNovo = "1"
            Else
                ValorControloNovo = ""
            End If
        Else
        End If
        
        If CamposEcBckp(i) <> ValorControloNovo Then
            BL_RegistaAltRequis num_requis, CamposBD(i), CamposEcBckp(i), ValorControloNovo, seq_alteracao
        End If
    Next
End Sub
Private Function VerificaAnaliseJaExisteRealiza(n_req As Long, Cod_Perfil As String, cod_ana_c As String, cod_ana_s As String, flg_apaga As Boolean) As Long
    Dim ssql As String
    Dim rsReal As New adodb.recordset
    Dim RsRes As New adodb.recordset
    On Error GoTo TrataErro
    
    VerificaAnaliseJaExisteRealiza = 0
    
    ssql = "SELECT seq_realiza FROM sl_realiza WHERE n_req = " & n_req & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil)
    ssql = ssql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c) & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    ssql = ssql & " UNION SELECT seq_realiza FROM sl_realiza_h WHERE n_req = " & n_req & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil)
    ssql = ssql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c) & " AND cod_ana_s = " & BL_TrataStringParaBD(cod_ana_s)
    rsReal.CursorLocation = adUseServer
    rsReal.CursorType = adOpenStatic
    If gModoDebug = mediSim Then
        BG_LogFile_Erros ssql
    End If
    rsReal.Open ssql, gConexao
    If rsReal.RecordCount > 0 Then
        VerificaAnaliseJaExisteRealiza = rsReal!seq_realiza
        ssql = "SELECT seq_Realiza FROM sl_res_alfan where seq_realiza = " & rsReal!seq_realiza
        ssql = ssql & " UNION SELECT seq_Realiza FROM sl_res_frase where seq_realiza = " & rsReal!seq_realiza
        ssql = ssql & " UNION SELECT seq_Realiza FROM sl_res_micro where seq_realiza = " & rsReal!seq_realiza
        ssql = ssql & " UNION SELECT seq_Realiza FROM sl_res_alfan_h where seq_realiza = " & rsReal!seq_realiza
        ssql = ssql & " UNION SELECT seq_Realiza FROM sl_res_frase_h where seq_realiza = " & rsReal!seq_realiza
        ssql = ssql & " UNION SELECT seq_Realiza FROM sl_res_micro_h where seq_realiza = " & rsReal!seq_realiza
        RsRes.CursorLocation = adUseServer
        RsRes.CursorType = adOpenStatic
        If gModoDebug = mediSim Then
            BG_LogFile_Erros ssql
        End If
        RsRes.Open ssql, gConexao
        If RsRes.RecordCount > 0 Or cod_ana_s = "S99999" Then
            VerificaAnaliseJaExisteRealiza = rsReal!seq_realiza
        Else
            If flg_apaga = True Then
                ssql = "DELETE FROM sl_realiza WHERE seq_realiza = " & rsReal!seq_realiza
                BG_ExecutaQuery_ADO ssql
            End If
            VerificaAnaliseJaExisteRealiza = 0
        End If
        RsRes.Close
        Set RsRes = Nothing
    Else
        VerificaAnaliseJaExisteRealiza = 0
    End If
    rsReal.Close
    Set rsReal = Nothing
    
Exit Function
TrataErro:
    VerificaAnaliseJaExisteRealiza = 0
    BG_LogFile_Erros "Erro  ao VerificaAnaliseJaExisteRealiza: " & Err.Description, Me.Name, "VerificaAnaliseJaExisteRealiza", True
    Exit Function
    Resume Next
End Function


Public Sub PreencheFgRecibos()
    Dim i As Long
    Dim j As Integer
    Dim cor  As Long
    Dim txtEstado As String
    
    ExecutaCodigoR = False
    j = FGRecibos.rows - 1
    While j > 0
        If j > 1 Then
            FGRecibos.RemoveItem j
        Else
            FGRecibos.TextMatrix(j, 0) = ""
            FGRecibos.TextMatrix(j, 1) = ""
            FGRecibos.TextMatrix(j, 2) = ""
            FGRecibos.TextMatrix(j, 3) = ""
            FGRecibos.TextMatrix(j, 4) = ""
            FGRecibos.TextMatrix(j, 5) = ""
            FGRecibos.TextMatrix(j, 6) = ""
            FGRecibos.TextMatrix(j, 7) = ""
            FGRecibos.TextMatrix(j, 8) = ""
            FGRecibos.TextMatrix(j, 9) = ""
            BL_MudaCorFg FGRecibos, CLng(j), vbWhite
        End If
        j = j - 1
    Wend
    
    ExecutaCodigoR = True
    For i = 1 To RegistosR.Count - 1
        If RegistosR(i).Estado = gEstadoReciboAnulado Then
            cor = vermelho
            txtEstado = "Anulado"
        ElseIf RegistosR(i).Estado = gEstadoReciboPago Then
            cor = Verde
            txtEstado = "Pago"
        ElseIf RegistosR(i).Estado = gEstadoReciboCobranca Then
            cor = Amarelo
            txtEstado = "Cobran�a"
        ElseIf RegistosR(i).Estado = gEstadoReciboPerdido Then
            cor = vbWhite
            txtEstado = "An�lises Perdidas"
        ElseIf RegistosR(i).Estado = gEstadoReciboNulo Then
            cor = vbWhite
            txtEstado = "Recibo Nulo"
        Else
            cor = vbWhite
            txtEstado = "N�o Emitido"
        End If
        If RegistosR(i).NumDoc <> "" Then
        ' -----------------------------------------------------------------------------------------------
            ' PREENCHCE A FLEXGRID
            ' -----------------------------------------------------------------------------------------------
            If BL_HandleNull(RegistosR(i).NumDoc, "0") <> "0" Then
                FGRecibos.TextMatrix(i, lColRecibosNumDoc) = RegistosR(i).SerieDoc & "/" & RegistosR(i).NumDoc
            Else
                FGRecibos.TextMatrix(i, lColRecibosNumDoc) = RegistosR(i).NumDoc
                RegistosR(i).CodFormaPag = gModoPagNaoPago
            End If
            FGRecibos.TextMatrix(i, lColRecibosEntidade) = RegistosR(i).DescrEntidade
            If RegistosR(i).SeqUtente <> -1 Then
                FGRecibos.TextMatrix(i, lColRecibosEntidade) = FGRecibos.TextMatrix(i, lColRecibosEntidade) & " (" & RegistosR(i).SeqUtente & ")"
            End If
            FGRecibos.TextMatrix(i, lColRecibosValorPagar) = RegistosR(i).ValorPagar
            FGRecibos.TextMatrix(i, lColRecibosPercentagem) = Round(CDbl(100 - RegistosR(i).Desconto), 3)
            FGRecibos.TextMatrix(i, lColRecibosDtEmissao) = RegistosR(i).DtEmi & " " & RegistosR(i).HrEmi
            FGRecibos.TextMatrix(i, lColRecibosDtPagamento) = RegistosR(i).DtPagamento & " " & RegistosR(i).HrPagamento
            FGRecibos.TextMatrix(i, lColRecibosEstado) = txtEstado
            If RegistosR(i).CodFormaPag = gModoPagNaoPago Then
                FGRecibos.TextMatrix(i, lColRecibosModoPag) = ""
            Else
                FGRecibos.TextMatrix(i, lColRecibosModoPag) = BL_SelCodigo("sl_forma_pag", "DESCR_FORMA_PAG", "COD_FORMA_PAG", RegistosR(i).CodFormaPag)
            End If
            If RegistosR(i).FlgBorla = mediSim Then
                FGRecibos.TextMatrix(i, lColRecibosBorla) = "Sim"
            Else
                FGRecibos.TextMatrix(i, lColRecibosBorla) = "N�o"
            End If
            If RegistosR(i).FlgNaoConforme = mediSim Then
                FGRecibos.TextMatrix(i, lColRecibosNaoConfmidade) = "N�o"
            Else
                FGRecibos.TextMatrix(i, lColRecibosNaoConfmidade) = "Sim"
            End If
            BL_MudaCorFg FGRecibos, i, cor
            FGRecibos.AddItem ""
        End If
    Next

End Sub
Public Sub PreencheFgAdiantamentos()
    Dim i As Long
    Dim j As Integer
    Dim cor  As Long
    Dim txtEstado As String
    
    j = FgAdiantamentos.rows - 1
    While j > 0
        If j > 1 Then
            FgAdiantamentos.RemoveItem j
        Else
            For i = 0 To FgAdiantamentos.Cols - 1
                FgAdiantamentos.TextMatrix(j, i) = ""
            Next
            BL_MudaCorFg FgAdiantamentos, CLng(j), vbWhite
        End If
        j = j - 1
    Wend
    
    For i = 1 To RegistosAD.Count - 1
        If RegistosAD(i).Estado = gEstadoReciboAnulado Then
            cor = vermelho
            txtEstado = "Anulado"
        ElseIf RegistosAD(i).Estado = gEstadoReciboPago Then
            cor = Verde
            txtEstado = "Pago"
        ElseIf RegistosAD(i).Estado = gEstadoReciboCobranca Then
            cor = Amarelo
            txtEstado = "Cobran�a"
        ElseIf RegistosAD(i).Estado = gEstadoReciboPerdido Then
            cor = vbWhite
            txtEstado = "An�lises Perdidas"
        ElseIf RegistosAD(i).Estado = gEstadoReciboNulo Then
            cor = vbWhite
            txtEstado = "Recibo Nulo"
        Else
            cor = vbWhite
            txtEstado = "N�o Emitido"
        End If
        ' -----------------------------------------------------------------------------------------------
        ' PREENCHCE A FLEXGRID
        ' -----------------------------------------------------------------------------------------------
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosNumDoc) = RegistosAD(i).NumAdiantamento
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosDtEmissao) = RegistosAD(i).DtEmi & " " & RegistosAD(i).HrEmi
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosDtAnulacao) = RegistosAD(i).DtAnul & " " & RegistosAD(i).HrAnul
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosEstado) = txtEstado
        FgAdiantamentos.TextMatrix(i, lColAdiantamentosValor) = RegistosAD(i).valor
        BL_MudaCorFg FgAdiantamentos, i, cor
        FgAdiantamentos.AddItem ""
    Next

End Sub

Private Sub GereFrameAdiantamentos(flg_Aberto As Boolean)
    If flg_Aberto = True Then
        FrameAdiantamentos.top = 360
        FrameAdiantamentos.Visible = True
    ElseIf flg_Aberto = False Then
        FrameAdiantamentos.Visible = False
    End If
End Sub


Private Sub VerificaDoenteHemodialise()
    Dim codAna As String
    Dim ssql As String
    Dim rsHemo As New adodb.recordset
    Dim i As Integer
    Dim serieRec As String
    Dim numRec As String
    serieRec = "0"
    numRec = "0"
    If EcHemodialise <> "" Then
    
        ssql = "SELECT cod_ana FROM sl_cod_hemodialise WHERE cod_hemodialise = " & BL_TrataStringParaBD(EcHemodialise)
        rsHemo.CursorLocation = adUseServer
        rsHemo.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros ssql
        rsHemo.Open ssql, gConexao
        If rsHemo.RecordCount > 0 Then
            If BL_HandleNull(rsHemo!cod_ana, "") <> "" Then
                If VerificaCodAnaJaMarcadoRM(rsHemo!cod_ana) = False Then
                    For i = 1 To RegistosR.Count - 1
                        If RegistosR(i).Estado = gEstadoReciboNulo And RegistosR(i).codEntidade = EcCodEFR Then
                            numRec = RegistosR(i).NumDoc
                            serieRec = RegistosR(i).SerieDoc
                            Exit For
                        End If
                    Next
                    AdicionaReciboManual 1, RegistosR(i).codEntidade, numRec, serieRec, rsHemo!cod_ana, 1, "", rsHemo!cod_ana, False
                    FGRecibos_DblClick
                End If
            End If
        
        End If
        rsHemo.Close
        Set rsHemo = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao VerificaDoenteHemodialise: " & Err.Description, Me.Name, "VerificaDoenteHemodialise", False
    Exit Sub
    Resume Next
End Sub

Private Function VerificaCodAnaJaMarcadoRM(cod_ana As String) As Boolean
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodFacturavel = cod_ana Then
            VerificaCodAnaJaMarcadoRM = True
            Exit Function
        End If
    Next
    VerificaCodAnaJaMarcadoRM = False
Exit Function
TrataErro:
    VerificaCodAnaJaMarcadoRM = False
    BG_LogFile_Erros "Erro  ao VerificaCodAnaJaMarcadoRM: " & Err.Description, Me.Name, "VerificaCodAnaJaMarcadoRM", False
    Exit Function
    Resume Next
End Function

Private Function VerificaEfrHemodialise(cod_efr As String) As Boolean
    Dim ssql As String
    Dim RsEFR As New adodb.recordset
    On Error GoTo TrataErro
    VerificaEfrHemodialise = False
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    ssql = "select flg_hemodialise from sl_efr where cod_efr = " & Trim(cod_efr)
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    RsEFR.Open ssql, gConexao
    If RsEFR.RecordCount > 0 Then
        If BL_HandleNull(RsEFR!flg_hemodialise, 0) = 1 Then
            VerificaEfrHemodialise = True
        End If
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Function
TrataErro:
    VerificaEfrHemodialise = False
    BG_LogFile_Erros "Erro  ao VerificaEfrHemodialise: " & Err.Description, Me.Name, "VerificaEfrHemodialise", False
    Exit Function
    Resume Next
End Function

Private Sub InicializaFrameAnaRecibos()
    If gPassaRecFactus = mediSim Then
        EcCodAnaAcresc.Visible = False
        EcDescrAnaAcresc.Visible = False
        EcQtdAnaAcresc.Visible = False
        EcTaxaAnaAcresc.Visible = False
        BtAnaAcresc.Visible = False
        BtFgAnaRecRecalcula.Visible = False
        FgAnaRec.Visible = False
        FgMoviFact.Visible = True
        With FgMoviFact
            .rows = 2
            .FixedRows = 1
            .Cols = 18
            
            .FixedCols = 0
            .AllowBigSelection = False
            .ScrollBars = flexScrollBarBoth
            .HighLight = flexHighlightAlways
            .FocusRect = flexFocusHeavy
            .MousePointer = flexDefault
            .FillStyle = flexFillSingle
            .SelectionMode = flexSelectionFree
            .AllowUserResizing = flexResizeNone
            .GridLines = flexGridFlat
            .GridLinesFixed = flexGridInset
            .TextStyle = flexTextFlat
            .TextStyleFixed = flexTextInsetLight
            .MergeCells = flexMergeRestrictColumns
            .PictureType = flexPictureColor
            .RowHeightMin = 285
            .row = 0
            
            .ColWidth(lColFgMoviFactCodRubr) = 600
            .Col = lColFgMoviFactCodRubr
            .ColAlignment(lColFgMoviFactCodRubr) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactCodRubr) = "Rubr."
            
            .ColWidth(lColFgMoviFactDescrRubr) = 1500
            .Col = lColFgMoviFactDescrRubr
            .ColAlignment(lColFgMoviFactDescrRubr) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactDescrRubr) = "Descr."
            
            .ColWidth(lColFgMoviFactCodRubrEfr) = 700
            .Col = lColFgMoviFactCodRubrEfr
            .TextMatrix(0, lColFgMoviFactCodRubrEfr) = "Rubr.EFR"
            
            .ColWidth(lColFgMoviFactQtd) = 500
            .Col = lColFgMoviFactQtd
            .TextMatrix(0, lColFgMoviFactQtd) = "QTD"
                        
            .ColWidth(lColFgMoviFactValPrUDoente) = 600
            .Col = lColFgMoviFactValPrUDoente
            .TextMatrix(0, lColFgMoviFactValPrUDoente) = "VPUDoe"
            
            .ColWidth(lColFgMoviFactValPagDoe) = 600
            .Col = lColFgMoviFactValPagDoe
            .TextMatrix(0, lColFgMoviFactValPagDoe) = "VPDoe"
            
            
            .ColWidth(lColFgMoviFactEstadoDoe) = 600
            .Col = lColFgMoviFactEstadoDoe
            .ColAlignment(lColFgMoviFactEstadoDoe) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactEstadoDoe) = "FEDoe"
            
            .ColWidth(lColFgMoviFactNFacDoe) = 600
            .Col = lColFgMoviFactNFacDoe
            .ColAlignment(lColFgMoviFactNFacDoe) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactNFacDoe) = "NFDoe"
            
            .ColWidth(lColFgMoviFactSerieFacDoe) = 800
            .Col = lColFgMoviFactSerieFacDoe
            .ColAlignment(lColFgMoviFactSerieFacDoe) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactSerieFacDoe) = "SFDoe"
            
            .ColWidth(lColFgMoviFactEstadoDoe) = 600
            .Col = lColFgMoviFactEstadoDoe
            .ColAlignment(lColFgMoviFactEstadoDoe) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactEstadoDoe) = "FEDoe"
            
            .ColWidth(lColFgMoviFactValTaxa) = 600
            .Col = lColFgMoviFactValTaxa
            .ColAlignment(lColFgMoviFactValTaxa) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactValTaxa) = "VTx"
            
            .ColWidth(lColFgMoviFactEstadoTx) = 600
            .Col = lColFgMoviFactEstadoTx
            .ColAlignment(lColFgMoviFactEstadoTx) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactEstadoTx) = "FETx"
            
            .ColWidth(lColFgMoviFactSerieFacTx) = 800
            .Col = lColFgMoviFactSerieFacTx
            .ColAlignment(lColFgMoviFactSerieFacTx) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactSerieFacTx) = "SFTx"
            
            .ColWidth(lColFgMoviFactNFacTx) = 600
            .Col = lColFgMoviFactNFacTx
            .ColAlignment(lColFgMoviFactNFacTx) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactNFacTx) = "NFTx"
            
            .ColWidth(lColFgMoviFactValPrUnit) = 600
            .Col = lColFgMoviFactValPrUnit
            .ColAlignment(lColFgMoviFactValPrUnit) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactValPrUnit) = "VPUnt"
            
            .ColWidth(lColFgMoviFactValTotal) = 700
            .Col = lColFgMoviFactValTotal
            .ColAlignment(lColFgMoviFactValTotal) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactValTotal) = "VTtl"
            
            .ColWidth(lColFgMoviFactEstado) = 700
            .Col = lColFgMoviFactEstado
            .ColAlignment(lColFgMoviFactEstado) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactEstado) = "FEst"
            
            .ColWidth(lColFgMoviFactSerieFac) = 800
            .Col = lColFgMoviFactSerieFac
            .ColAlignment(lColFgMoviFactSerieFac) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactSerieFac) = "SFact"
            
            .ColWidth(lColFgMoviFactNFac) = 700
            .Col = lColFgMoviFactNFac
            .ColAlignment(lColFgMoviFactNFac) = flexAlignLeftCenter
            .TextMatrix(0, lColFgMoviFactNFac) = "NFact"
            '
            .WordWrap = False
            .row = 1
            .Col = 0
        End With
    Else
        FgMoviFact.Visible = False
        EcCodAnaAcresc.Visible = True
        EcDescrAnaAcresc.Visible = True
        EcQtdAnaAcresc.Visible = True
        EcTaxaAnaAcresc.Visible = True
        BtAnaAcresc.Visible = True
        BtFgAnaRecRecalcula.Visible = True
        FgAnaRec.Visible = True
        
        EcCodAnaAcresc.left = FgAnaRec.left
        EcCodAnaAcresc.Width = FgAnaRec.ColWidth(lColFgAnaRecCodAna)
        EcDescrAnaAcresc.left = EcCodAnaAcresc.left + EcCodAnaAcresc.Width
        EcDescrAnaAcresc.Width = FgAnaRec.ColWidth(lColFgAnaRecDescrAna)
        EcQtdAnaAcresc.left = EcDescrAnaAcresc.left + EcDescrAnaAcresc.Width
        EcQtdAnaAcresc.Width = FgAnaRec.ColWidth(lColFgAnaRecQtd)
        EcTaxaAnaAcresc.left = EcQtdAnaAcresc.left + EcQtdAnaAcresc.Width
        EcTaxaAnaAcresc.Width = FgAnaRec.ColWidth(lColFgAnaRecTaxa)
        If RegistosR(FGRecibos.row).Estado = gEstadoReciboAnulado Or RegistosR(FGRecibos.row).Estado = gEstadoReciboCobranca Or _
           RegistosR(FGRecibos.row).Estado = gEstadoReciboPago Then
                BtAnaAcresc.Enabled = False
        Else
                BtAnaAcresc.Enabled = True
        End If
    End If
End Sub

Private Function RetornaPrecoAnaRec(cod_ana As String, cod_efr As String, qtd As Integer) As String
    Dim taxa As String
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    Dim mens As String
    On Error GoTo TrataErro
    
    taxa = IF_RetornaTaxaAnalise(cod_ana, cod_efr, CbTipoUtente, EcUtente, EcDataPrevista, codAnaEfr, descrAnaEfr, False)
    taxa = Replace(taxa, ".", ",")
    If Mid(taxa, 1, 1) = "," Then
        taxa = "0" & taxa
    End If
            
    Select Case taxa
        Case "-1"
            mens = " An�lise n�o est� mapeada para o FACTUS!"
            taxa = 0
        Case "-2"
            mens = "Impossivel abrir conex�o com o FACTUS!"
            taxa = 0
        Case "-3"
            mens = "Impossivel seleccionar portaria activa!"
            taxa = 0
        Case "-4"
            mens = "An�lise n�o comparticipada. Pre�o PARTICULAR!"
            
            taxa = 0
        Case "-5"
            mens = "N�o existe tabela codificada para a entidade em causa!"
            taxa = 0
        Case Else
            mens = ""
    End Select
    
    If mens <> "" Then
        taxa = "0,0"
    End If
    taxa = BG_CvDecimalParaCalculo(taxa) * qtd
    RetornaPrecoAnaRec = taxa
Exit Function
TrataErro:
    RetornaPrecoAnaRec = "0"
    BG_LogFile_Erros "Erro  ao RetornaPrecoAnaRec: " & Err.Description, Me.Name, "RetornaPrecoAnaRec", False
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------------

' PREENCHE AUTOMATICAMENT A SALA AO INTRODUZIR UM NUMERO REQ PRE IMPRESSO

' -------------------------------------------------------------------------
Private Sub PreencheSalaAuto(n_req As String)
    On Error GoTo TrataErro
    If n_req = "" Then Exit Sub
    Dim ssql As String
    Dim rsSalas As New adodb.recordset
    ssql = "SELECT cod_sala FROM sl_etiq_salas WHERE n_req = " & n_req
    rsSalas.CursorLocation = adUseServer
    rsSalas.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsSalas.Open ssql, gConexao
    'BRUNODSANTOS LRV-432
    If rsSalas.RecordCount = 1 And BL_HandleNull(rsSalas!cod_sala, "") <> "" Then 'And EcCodSala = ""
        EcCodSala = BL_HandleNull(rsSalas!cod_sala, "")
        EcCodsala_Validate False
    End If
    rsSalas.Close
    Set rsSalas = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao PreencheSalaAuto: " & Err.Description, Me.Name, "PreencheSalaAuto", False
    Exit Sub
    Resume Next
End Sub


' -------------------------------------------------------------------------

' COLOCA UM RECIBO EM COBRANCA/PAGO

' -------------------------------------------------------------------------
    
Private Sub ColocaCobrancaPago(linha As Integer, serieRec As String, nRec As String, tipo As Integer)
    Dim ssql As String
    On Error GoTo TrataErro
    If tipo = 0 Then ' COLOCAR EM PAGAMENTO
        BG_BeginTransaction
        ssql = "UPDATE sl_recibos set user_emi = " & BL_TrataStringParaBD(RegistosR(linha).UserEmi) & ","
        ssql = ssql & " dt_pagamento = " & BL_TrataDataParaBD(RegistosR(linha).DtPagamento) & ", "
        ssql = ssql & " hr_pagamento = " & BL_TrataStringParaBD(RegistosR(linha).HrPagamento) & ", "
        ssql = ssql & " estado ='P' WHERE serie_rec = " & BL_TrataStringParaBD(serieRec) & " AND n_rec = " & nRec

    ElseIf tipo = 1 Then
        ssql = "UPDATE sl_recibos set dt_pagamento = null, hr_pagamento = null, estado = 'C' WHERE serie_rec = " & BL_TrataStringParaBD(serieRec) & " AND n_rec = " & nRec
    End If
    BG_ExecutaQuery_ADO ssql
    ssql = "UPDATE sl_recibos_det SET n_req = n_req  WHERE serie_rec = " & BL_TrataStringParaBD(serieRec) & " AND n_rec = " & nRec
    BG_ExecutaQuery_ADO ssql
    BG_CommitTransaction
Exit Sub
TrataErro:
BG_RollbackTransaction
    BG_LogFile_Erros "Erro  ao ColocaCobrancapago: " & Err.Description, Me.Name, "ColocaCobrancapago", False
    Exit Sub
    Resume Next
End Sub


Private Sub FlgIncrementa(Ind As Long, qtd As Integer)
    Dim i As Integer
    Dim flg_FdsValFixo As Long
    On Error GoTo TrataErro
    
    For i = 1 To RegistosRM.Count
        If RegistosA(Ind).codAna = RegistosRM(i).ReciboCodAna Then
            Exit For
        End If
    Next
    EliminaReciboManual CLng(i), RegistosA(Ind).codAna, False
    RegistosRM(i).ReciboQuantidade = qtd
    RegistosRM(i).ReciboQuantidadeEFR = RegistosRM(i).ReciboQuantidade
    RegistosRM(i).ReciboTaxa = Replace(RegistosRM(i).ReciboTaxaUnitario, ".", ",") * qtd
    
    'PARA FIM DE SEMANA DOBRA O PRE�O
    'SE ENTIDADE COM VALOR FIXO PARA FINS DE SEMANA - COLOCA A ZERO
    flg_FdsValFixo = VerificaFdsValFixo(Ind)
    If RegistosRM(i).ReciboFDS = mediSim And flg_FdsValFixo = 0 Then
        RegistosRM(i).ReciboTaxa = RegistosRM(i).ReciboTaxa * 2
    ElseIf RegistosRM(Ind).ReciboFDS = mediSim And flg_FdsValFixo = 1 Then
        RegistosRM(i).ReciboTaxa = "0"
    End If
    
    FGAna.TextMatrix(Ind, lColFgAnaPreco) = RegistosRM(i).ReciboTaxa
    AdicionaAnaliseAoReciboManual CLng(i), False
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  aFlgIncrementa: " & Err.Description, Me.Name, "FlgIncrementa", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' PREENCHE GRELHA DE ANALISES COM INFORMACAO DO PRE�O ASSOCIADO

' -------------------------------------------------------------------------
Private Sub ActualizaLinhasRecibos(linha As Integer)
    Dim i As Integer
    Dim j As Integer
    On Error GoTo TrataErro
    
    If linha > 0 Then
        
        FGAna.TextMatrix(linha, lColFgAnaP1) = ""
        FGAna.TextMatrix(linha, lColFgAnaMedico) = ""
        FGAna.TextMatrix(linha, lColFgAnaCodEFR) = ""
        FGAna.TextMatrix(linha, lColFgAnaDescrEFR) = ""
        FGAna.TextMatrix(linha, lColFgAnaNrBenef) = ""
        FGAna.TextMatrix(linha, lColFgAnaPreco) = ""
        'NELSONPSILVA GLINTT-HS-21312
        FGAna.TextMatrix(j, lColFgAnaNAmostras) = ""
        '
        
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboCodAna = RegistosA(linha).codAna Then
                If RegistosRM(i).ReciboCodAna <> "" And RegistosRM(i).ReciboFlgReciboManual = 0 And RegistosRM(i).flgCodBarras = 0 Then
                    If RegistosRM(i).ReciboCorP1 = "V" Then
                        FGAna.row = linha
                        FGAna.Col = 2
                        FGAna.CellBackColor = Verde
                        FGAna.Col = 0
                    Else
                        FGAna.row = linha
                        FGAna.Col = 2
                        FGAna.CellBackColor = vbWhite
                        FGAna.Col = 0
                    End If
        
                    FGAna.TextMatrix(linha, lColFgAnaP1) = RegistosRM(i).ReciboP1
                    FGAna.TextMatrix(linha, lColFgAnaMedico) = RegistosRM(i).ReciboCodMedico
                    FGAna.TextMatrix(linha, lColFgAnaCodEFR) = RegistosRM(i).ReciboEntidade
                    FGAna.TextMatrix(linha, lColFgAnaDescrEFR) = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(i).ReciboEntidade)
                    FGAna.TextMatrix(linha, lColFgAnaNrBenef) = RegistosRM(i).ReciboNrBenef
                    FGAna.TextMatrix(linha, lColFgAnaPreco) = RegistosRM(i).ReciboTaxa
                    'NELSONPSILVA GLINTT-HS-21312
                    FGAna.TextMatrix(j, lColFgAnaNAmostras) = RegistosRM(i).ReciboQuantidade
                    '
                    If RegistosRM(i).ReciboIsencao = gTipoIsencaoBorla Then
                        FGAna.Col = lColFgAnaPreco
                        FGAna.CellBackColor = CorBorla
                    ElseIf RegistosRM(i).ReciboIsencao = gTipoIsencaoIsento Then
                        FGAna.Col = lColFgAnaPreco
                        FGAna.CellBackColor = CorIsento
                    Else
                        FGAna.Col = lColFgAnaPreco
                        FGAna.CellBackColor = vbWhite
                    End If
                    FGAna.Col = lColFgAnaCodigo
                    Exit For
                End If
            End If
        Next
    Else
        For i = 1 To RegistosA.Count - 1
            FGAna.TextMatrix(i, lColFgAnaP1) = ""
            FGAna.TextMatrix(i, lColFgAnaMedico) = ""
            FGAna.TextMatrix(i, lColFgAnaCodEFR) = ""
            FGAna.TextMatrix(i, lColFgAnaDescrEFR) = ""
            FGAna.TextMatrix(i, lColFgAnaNrBenef) = ""
            FGAna.TextMatrix(i, lColFgAnaPreco) = ""
            'NELSONPSILVA GLINTT-HS-21312
            FGAna.TextMatrix(i, lColFgAnaNAmostras) = ""
            '
        Next
        For i = 1 To RegistosRM.Count - 1
            If RegistosRM(i).ReciboCodAna <> "" And RegistosRM(i).ReciboFlgReciboManual = 0 And RegistosRM(i).flgCodBarras = 0 Then
                For j = 1 To RegistosA.Count - 1
                    If RegistosRM(i).ReciboCodAna = RegistosA(j).codAna Then
                
                        If RegistosRM(i).ReciboCorP1 = "V" Then
                            FGAna.row = j
                            FGAna.Col = 2
                            FGAna.CellBackColor = Verde
                            FGAna.Col = 0
                        Else
                            FGAna.row = j
                            FGAna.Col = 2
                            FGAna.CellBackColor = vbWhite
                            FGAna.Col = 0
                        End If
            
                        FGAna.TextMatrix(j, lColFgAnaP1) = RegistosRM(i).ReciboP1
                        FGAna.TextMatrix(j, lColFgAnaMedico) = RegistosRM(i).ReciboCodMedico
                        FGAna.TextMatrix(j, lColFgAnaCodEFR) = RegistosRM(i).ReciboEntidade
                        FGAna.TextMatrix(j, lColFgAnaDescrEFR) = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRM(i).ReciboEntidade)
                        FGAna.TextMatrix(j, lColFgAnaNrBenef) = RegistosRM(i).ReciboNrBenef
                        FGAna.TextMatrix(j, lColFgAnaPreco) = RegistosRM(i).ReciboTaxa
                        'NELSONPSILVA GLINTT-HS-21312
                        FGAna.TextMatrix(j, lColFgAnaNAmostras) = RegistosRM(i).ReciboQuantidade
                        '
                        If RegistosRM(i).ReciboIsencao = gTipoIsencaoBorla Then
                            FGAna.Col = lColFgAnaPreco
                            FGAna.CellBackColor = CorBorla
                        ElseIf RegistosRM(i).ReciboIsencao = gTipoIsencaoIsento Then
                            FGAna.Col = lColFgAnaPreco
                            FGAna.CellBackColor = CorIsento
                        Else
                            FGAna.Col = lColFgAnaPreco
                            FGAna.CellBackColor = vbWhite
                        End If
                        FGAna.Col = lColFgAnaCodigo
                        Exit For
                    End If
                Next
            End If
        Next
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ActualizarLinhasRecibos: " & Err.Description, Me.Name, "ActualizarLinhasRecibos", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' RETORNA O INDICE DA ESTRUTURA DE RECIBOS PARA ANALISE EM CAUSA

' -------------------------------------------------------------------------

Public Function DevIndice(codAna As String) As Long
    Dim i As Integer
    On Error GoTo TrataErro
    DevIndice = -1
    If codAna = "" Then Exit Function
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodFacturavel = codAna And BL_HandleNull(RegistosRM(i).flgCodBarras, 0) = 0 Then
            DevIndice = i
            Exit Function
        End If
    Next
Exit Function
TrataErro:
    DevIndice = -1
    BG_LogFile_Erros "Erro  DevIndice: " & Err.Description, Me.Name, "DevIndice", False
    Exit Function
    Resume Next
End Function



' -------------------------------------------------------------------------

' ADICIONA ANALISE � ESTRUTURA DE ANALISES

' -------------------------------------------------------------------------

Public Sub AdicionaAnalise(ByVal analise As String, codBarras As Boolean, ultimaLinha As Boolean, multiAna As String)
    On Error GoTo TrataErro
    Dim AnaFacturavel As String
    Dim cod_Agrup_aux As String
    Dim d As String
    DoEvents
    If ultimaLinha = True Then
        FGAna.row = FGAna.rows - 1
        LastRowA = FGAna.row
    End If
    AnaFacturavel = ""
    analise = BL_RetornaCodigoMarcar(analise, AnaFacturavel)
    DoEvents
    
    If Verifica_Ana_ja_Existe(LastRowA, analise) = False Then
        If Trim(UCase(analise)) <> Trim(UCase(RegistosA(LastRowA).codAna)) Then
            DoEvents
            If Insere_Nova_Analise(LastRowA, analise, AnaFacturavel, codBarras, multiAna) = True Then
                cod_Agrup_aux = EcAuxAna
                d = SELECT_Descr_Ana(cod_Agrup_aux)
                If LastRowA < FGAna.rows And codBarras = False Then
                    VerificaAnaliseJaMarcada EcNumReq, cod_Agrup_aux
                End If
                'o estado deixa de estar "Sem Analises"
                If Trim(EcEstadoReq) = Trim(gEstadoReqSemAnalises) Then
                    EcEstadoReq = gEstadoReqEsperaProduto
                End If
                EcDescrEstado = BL_DevolveEstadoReq(EcEstadoReq)
                
                If FGAna.row = FGAna.rows - 1 And Trim(FGAna.TextMatrix(FGAna.row, FGAna.Col)) <> "" Then
                    'Cria linha vazia
                    FGAna.AddItem ""
                    CriaNovaClasse RegistosA, FGAna.row + 1, "ANA"
                End If
             
            
            End If
        End If
    Else
        Beep
        anaExistente = False
        BG_Mensagem mediMsgStatus, "An�lise j� indicada!"
    End If
    FGAna.Col = 0
    Flg_IncrementaP1 = False
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaAnalise: " & Err.Description, Me.Name, "AdicionaAnalise", False
    Exit Sub
    Resume Next

End Sub



Private Sub PreencheEntidadesDefeito()
    Dim ssql As String
    Dim RsEFR As New adodb.recordset
    totalEFR = 0
    ReDim EstrutEFR(0)
    ssql = "SELECT  * FROM sl_efr"
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    RsEFR.Open ssql, gConexao
    If RsEFR.RecordCount >= 1 Then
        While Not RsEFR.EOF
            totalEFR = totalEFR + 1
            ReDim Preserve EstrutEFR(totalEFR)
            EstrutEFR(totalEFR).cod_efr = BL_HandleNull(RsEFR!cod_efr, "")
            EstrutEFR(totalEFR).descr_efr = BL_HandleNull(RsEFR!descr_efr, "")
            EstrutEFR(totalEFR).cod_empresa = BL_HandleNull(RsEFR!cod_empresa, "")
            EstrutEFR(totalEFR).deducao_ute = BL_HandleNull(RsEFR!deducao_ute, "")
            EstrutEFR(totalEFR).flg_65anos = BL_HandleNull(RsEFR!flg_65anos, "0")
            EstrutEFR(totalEFR).flg_acto_med = BL_HandleNull(RsEFR!flg_acto_med_efr, "0")
            EstrutEFR(totalEFR).flg_compart_dom = BL_HandleNull(RsEFR!flg_compart_dom, "0")
            EstrutEFR(totalEFR).flg_controla_isencao = BL_HandleNull(RsEFR!flg_controla_isencao, "0")
            EstrutEFR(totalEFR).flg_dom_defeito = BL_HandleNull(RsEFR!flg_dom_defeito, "0")
            EstrutEFR(totalEFR).flg_insere_cod_rubr_efr = BL_HandleNull(RsEFR!flg_insere_cod_rubr_efr, "0")
            EstrutEFR(totalEFR).flg_nao_urbano = BL_HandleNull(RsEFR!flg_nao_urbano, "0")
            EstrutEFR(totalEFR).flg_obriga_recibo = BL_HandleNull(RsEFR!flg_obriga_recibo, "0")
            EstrutEFR(totalEFR).flg_perc_facturar = BL_HandleNull(RsEFR!flg_perc_facturar, "0")
            EstrutEFR(totalEFR).flg_separa_dom = BL_HandleNull(RsEFR!flg_separa_dom, "0")
            EstrutEFR(totalEFR).flg_novaArs = BL_HandleNull(RsEFR!flg_nova_ars, 0)
            EstrutEFR(totalEFR).flgActivo = False
            RsEFR.MoveNext
        Wend
    End If
    RsEFR.Close
    Set RsEFR = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheEntidadesDefeito: " & Err.Description, Me.Name, "PreencheEntidadesDefeito", False
    Exit Sub
    Resume Next
End Sub



' -------------------------------------------------------------------------

' AP�S ANALISES TEREM SIDO APAGAAS, SE RECIBO CANCELADO, RETIRA ESSAS ANALISES

' -------------------------------------------------------------------------
Public Sub RetiraAnalisesApagadas()
    Dim i As Integer
    Dim j As Long
    Dim flg_apaga As Boolean
    On Error GoTo TrataErro
    For j = 1 To RegistosRM.Count - 1
        flg_apaga = True
        If RegistosRM(j).ReciboFlgReciboManual <> "1" Then
            For i = 1 To RegistosA.Count - 1
                If RegistosA(i).codAna = RegistosRM(j).ReciboCodAna Then
                    flg_apaga = False
                    Exit For
                End If
            Next
        Else
            flg_apaga = False
        End If
        If flg_apaga = True Then
            If EliminaReciboManual(j, RegistosRM(j).ReciboCodAna, True) = True Then
                LimpaColeccao RegistosRM, j
                j = j - 1
            End If

        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  RetiraAnalisesApagadas: " & Err.Description, Me.Name, "RetiraAnalisesApagadas", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' PREENCHE INFORMA��O ASSOCIADA �S ANALISES QUE FORAM REGISTADAS

' -------------------------------------------------------------------------
Private Sub PreencheInformacao(n_req)
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsInfo As New adodb.recordset
    totalInfo = 0
    ReDim EstrutInfo(0)
    
    ssql = "SELECT x2.descr_informacao, x2.cod_informacao, informacao FROM sl_req_informacao x1, sl_informacao x2 "
    ssql = ssql & " WHERE n_req = " & n_req & " and x1.cod_informacao = x2.cod_informacao "
    
    rsInfo.CursorLocation = adUseServer
    rsInfo.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsInfo.Open ssql, gConexao
    If rsInfo.RecordCount >= 1 Then
        While Not rsInfo.EOF
            totalInfo = totalInfo + 1
            ReDim Preserve EstrutInfo(totalInfo)
            EstrutInfo(totalInfo).cod_informacao = BL_HandleNull(rsInfo!cod_informacao, "")
            EstrutInfo(totalInfo).descr_informacao = BL_HandleNull(rsInfo!descr_informacao, "")
            EstrutInfo(totalInfo).informacao = BL_HandleNull(rsInfo!informacao, "")
            FgInformacao.TextMatrix(totalInfo, 0) = BL_HandleNull(rsInfo!descr_informacao, "")
            
            EcInfo.TextRTF = BL_HandleNull(rsInfo!informacao, "")
            FgInformacao.TextMatrix(totalInfo, 1) = EcInfo.text
            FgInformacao.AddItem ""
            rsInfo.MoveNext
        Wend
    End If
    rsInfo.Close
    Set rsInfo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheInformacao: " & Err.Description, Me.Name, "PreencheInformacao", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' PREENCHE Quest�es ASSOCIADA �S ANALISES QUE FORAM REGISTADAS

' -------------------------------------------------------------------------
Private Sub PreencheQuestao(n_req)
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsInfo As New adodb.recordset
    totalQuestao = 0
    ReDim EstrutQuestao(0)
    
    ssql = "SELECT x2.descr_frase, x2.cod_frase, x1.resposta FROM sl_req_questoes x1, sl_dicionario x2 "
    ssql = ssql & " WHERE n_req = " & n_req & " and x1.cod_frase = x2.cod_Frase "
    
    rsInfo.CursorLocation = adUseServer
    rsInfo.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsInfo.Open ssql, gConexao
    If rsInfo.RecordCount >= 1 Then
        While Not rsInfo.EOF
            totalQuestao = totalQuestao + 1
            ReDim Preserve EstrutQuestao(totalQuestao)
            EstrutQuestao(totalQuestao).cod_questao = BL_HandleNull(rsInfo!cod_frase, "")
            EstrutQuestao(totalQuestao).descr_questao = BL_HandleNull(rsInfo!descr_frase, "")
            EstrutQuestao(totalQuestao).Resposta = BL_HandleNull(rsInfo!Resposta, "")
            FgQuestoes.TextMatrix(totalQuestao, 0) = BL_HandleNull(rsInfo!descr_frase, "")
            FgQuestoes.TextMatrix(totalQuestao, 1) = BL_HandleNull(rsInfo!Resposta, "")
            FgQuestoes.AddItem ""
            rsInfo.MoveNext
        Wend
    End If
    rsInfo.Close
    Set rsInfo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheQuestao: " & Err.Description, Me.Name, "PreencheQuestao", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' AO ADICIONAR ANALISES, ADICIONA INFORMACAO DA ANALISE

' -------------------------------------------------------------------------
Private Sub AdicionaInformacaoAna(cod_ana As String)
    Dim ssql As String
    Dim flg_existe As Boolean
    Dim rsInfo As New adodb.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    ssql = "SELECT x1.cod_informacao, x2.descr_informacao, x2.informacao FROM sl_ana_informacao x1, sl_informacao x2 WHERE x1.cod_informacao = x2.cod_informacao AND "
    ssql = ssql & " x1.cod_ana = " & BL_TrataStringParaBD(cod_ana)
    rsInfo.CursorLocation = adUseServer
    rsInfo.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsInfo.Open ssql, gConexao
    If rsInfo.RecordCount >= 1 Then
        While Not rsInfo.EOF
            flg_existe = False
            For i = 1 To totalInfo
                If BL_HandleNull(rsInfo!cod_informacao, "") = EstrutInfo(i).cod_informacao Then
                    flg_existe = True
                    Exit For
                End If
            Next
            If flg_existe = False Then
                totalInfo = totalInfo + 1
                ReDim Preserve EstrutInfo(totalInfo)
                EstrutInfo(totalInfo).cod_informacao = BL_HandleNull(rsInfo!cod_informacao, "")
                EstrutInfo(totalInfo).descr_informacao = BL_HandleNull(rsInfo!descr_informacao, "")
                EstrutInfo(totalInfo).informacao = BL_HandleNull(rsInfo!informacao, "")
                FgInformacao.TextMatrix(totalInfo, 0) = BL_HandleNull(rsInfo!descr_informacao, "")
                
                EcInfo.TextRTF = BL_HandleNull(rsInfo!informacao, "")
                FgInformacao.TextMatrix(totalInfo, 1) = EcInfo.text
                FgInformacao.AddItem ""
            End If
            rsInfo.MoveNext
        Wend
    End If
    rsInfo.Close
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaInformacaoAna: " & Err.Description, Me.Name, "AdicionaInformacaoAna", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' GRAVA INFORMACAO DA ANALISE

' -------------------------------------------------------------------------
Private Function GravaInfoAna(n_req As String) As Boolean
    Dim ssql As String
    Dim i As Integer
    Dim registos As Integer
    On Error GoTo TrataErro
    GravaInfoAna = False
    ssql = "DELETE FROM sl_req_informacao WHERE n_req = " & n_req
    BG_ExecutaQuery_ADO ssql
    
    For i = 1 To totalInfo
        ssql = "INSERT INTO sl_req_informacao (n_req, cod_informacao) VALUES("
        ssql = ssql & n_req & ", " & BL_TrataStringParaBD(EstrutInfo(i).cod_informacao) & ")"
        registos = BG_ExecutaQuery_ADO(ssql)
        If registos <= 0 Then
            GoTo TrataErro
        End If
    Next
    GravaInfoAna = True
Exit Function
TrataErro:
    GravaInfoAna = False
    BG_LogFile_Erros "Erro  GravaInfoAna: " & Err.Description, Me.Name, "GravaInfoAna", False
    Exit Function
    Resume Next
End Function


' -------------------------------------------------------------------------

' AO ADICIONAR ANALISES, ADICIONA Questoes DA ANALISE

' -------------------------------------------------------------------------
Private Sub AdicionaQuestaoAna(cod_ana As String)
    Dim ssql As String
    Dim flg_existe As Boolean
    Dim rsQuest As New adodb.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    ssql = "SELECT x1.cod_frase, x2.descr_frase FROM sl_ana_questoes x1, sl_dicionario x2 WHERE x1.cod_frase = x2.cod_Frase AND "
    ssql = ssql & " x1.cod_ana = " & BL_TrataStringParaBD(cod_ana)
    rsQuest.CursorLocation = adUseServer
    rsQuest.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsQuest.Open ssql, gConexao
    If rsQuest.RecordCount >= 1 Then
        While Not rsQuest.EOF
            flg_existe = False
            For i = 1 To totalQuestao
                If BL_HandleNull(rsQuest!cod_frase, "") = EstrutQuestao(i).cod_questao Then
                    flg_existe = True
                    Exit For
                End If
            Next
            If flg_existe = False Then
                totalQuestao = totalQuestao + 1
                ReDim Preserve EstrutQuestao(totalQuestao)
                EstrutQuestao(totalQuestao).cod_questao = BL_HandleNull(rsQuest!cod_frase, "")
                EstrutQuestao(totalQuestao).descr_questao = BL_HandleNull(rsQuest!descr_frase, "")

                FgQuestoes.TextMatrix(totalQuestao, 0) = BL_HandleNull(rsQuest!descr_frase, "")
                FgQuestoes.AddItem ""
            End If
            rsQuest.MoveNext
        Wend
    End If
    rsQuest.Close
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaQuestaoAna: " & Err.Description, Me.Name, "AdicionaQuestaoAna", False
    Exit Sub
    Resume Next
End Sub


' -------------------------------------------------------------------------

' GRAVA Questoes DA ANALISE

' -------------------------------------------------------------------------
Private Function GravaQuestoesAna(n_req As String) As Boolean
    Dim ssql As String
    Dim i As Integer
    Dim registos As Integer
    On Error GoTo TrataErro
    GravaQuestoesAna = False
    ssql = "DELETE FROM sl_req_questoes WHERE n_req = " & n_req
    BG_ExecutaQuery_ADO ssql
    
    For i = 1 To totalQuestao
        ssql = "INSERT INTO sl_req_questoes (n_req, cod_frase, resposta) VALUES("
        ssql = ssql & n_req & ", " & BL_TrataStringParaBD(EstrutQuestao(i).cod_questao) & ", "
        ssql = ssql & BL_TrataStringParaBD(EstrutQuestao(i).Resposta) & " )"
        registos = BG_ExecutaQuery_ADO(ssql)
        If registos <= 0 Then
            GoTo TrataErro
        End If
    Next
    GravaQuestoesAna = True
Exit Function
TrataErro:
    GravaQuestoesAna = False
    BG_LogFile_Erros "Erro  GravaQuestoesAna: " & Err.Description, Me.Name, "GravaQuestoesAna", False
    Exit Function
    Resume Next
End Function
Private Sub EcCodpais_Validate(Cancel As Boolean)
    
    Dim Tabela As adodb.recordset
    Dim sql As String
    
    EcCodPais.text = UCase(EcCodPais.text)
    If EcCodPais.text <> "" Then
        Set Tabela = New adodb.recordset
    
        sql = "SELECT descr_pais FROM sl_pais WHERE cod_pais='" & Trim(EcCodPais.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Pa�s inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodPais.text = ""
            EcDescrPais.text = ""
        Else
            EcDescrPais.text = BL_HandleNull(Tabela!descr_pais)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrPais.text = ""
    End If
End Sub


Private Sub BtPesqPais_click()

    
    FormPesquisaRapida.InitPesquisaRapida "sl_pais", _
                        "descr_pais", "cod_pais", _
                        EcCodPais
    EcCodpais_Validate False
End Sub

' ----------------------------------------------------------------------------------

' VERIFICA SE DADOS ESTAO COERENTES PARA FACTURACAO ARS

' ----------------------------------------------------------------------------------

Private Function ValidaDadosFactARS() As Boolean
    On Error GoTo TrataErro
    ValidaDadosFactARS = False
    If CbCodUrbano.ListIndex > mediComboValorNull Then
        If EcCodPostal = "" Or EcRuaPostal = "" And EcDescrPostal = "" Then
            BG_Mensagem mediMsgBox, "Domicilio preenchido. C�digo Postal completo obrigat�rio.", vbExclamation + vbOKOnly, App.ProductName
            ValidaDadosFactARS = False
            Exit Function
        End If
    End If
    If CbConvencao.ListIndex > mediComboValorNull Then
        If CbConvencao.ItemData(CbConvencao.ListIndex) = gConvencaoInternacional Then
            If EcCodPais = "" Then
                BG_Mensagem mediMsgBox, "Conven��o Internacional. Pa�s obrigat�rio.", vbExclamation + vbOKOnly, App.ProductName
                ValidaDadosFactARS = False
                Exit Function
            End If
        End If
        If CbConvencao.ItemData(CbConvencao.ListIndex) = gConvencaoProfissional Then
            If EcDoenteProf = "" Then
                BG_Mensagem mediMsgBox, "Conven��o Profissional. N�mero de Doente Profissional obrigat�rio.", vbExclamation + vbOKOnly, App.ProductName
                ValidaDadosFactARS = False
                Exit Function
            End If
        End If
    End If
    ValidaDadosFactARS = True
Exit Function
TrataErro:
    ValidaDadosFactARS = False
    BG_LogFile_Erros "Erro  ValidaDadosFactARS: " & Err.Description, Me.Name, "ValidaDadosFactARS", False
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------------------------------

' VERIFICA SE JA EXISTE REGISTO PARA DETERMINADO PERFIL DE MARCACAO NA ESTRUTURA DE RECIBOS

' -------------------------------------------------------------------------------------------
Private Function VerificaPerfilMarcacaoJaMarcado(CodPerfil As String) As Boolean
    On Error GoTo TrataErro
    Dim i As Integer
    For i = 1 To RegistosRM.Count
        If RegistosRM(i).ReciboCodFacturavel = CodPerfil And RegistosRM(i).ReciboCodAna = CodPerfil Then
            VerificaPerfilMarcacaoJaMarcado = True
            Exit Function
        End If
    Next
    VerificaPerfilMarcacaoJaMarcado = False
    
Exit Function
TrataErro:
    VerificaPerfilMarcacaoJaMarcado = False
    BG_LogFile_Erros "Erro  VerificaPerfilMarcacaoJaMarcado: " & Err.Description, Me.Name, "VerificaPerfilMarcacaoJaMarcado", False
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------

' CONSULTA AS VARIAS ALTERACOES DE HISTORICO (FLG_SEGUINTE = 1 SEGUINTE, FLG_SEGUINTE = -1 ANTERIOR)

' ---------------------------------------------------------------------------------------------------
Private Function ConsultaHistoricoAlteracoes(flg_seguinte As Integer) As Boolean
    On Error GoTo TrataErro
    BtHistAnt.Enabled = True
    BtHistSeg.Enabled = True
    
    If IndiceHistorico = 0 And flg_seguinte = 0 Then
        numHistoricos = BL_ContaAltRequis(EcNumReq)
    End If
    
    IndiceHistorico = IndiceHistorico + flg_seguinte
    
    If IndiceHistorico > 0 Then
        DesactivaGravacao False
        PreencheHistorico EcNumReq, IndiceHistorico
    Else
        DesactivaGravacao True
        If flg_seguinte <> 0 Then
            PreencheCampos True
        End If
    End If
    If IndiceHistorico <= 0 Then
        IndiceHistorico = 0
        BtHistAnt.Enabled = False
    Else
        BtHistAnt.Enabled = True
    End If
    If IndiceHistorico >= numHistoricos Then
        IndiceHistorico = numHistoricos
        BtHistSeg.Enabled = False
    Else
        BtHistSeg.Enabled = True
    End If
Exit Function
TrataErro:
    ConsultaHistoricoAlteracoes = False
    BG_LogFile_Erros "Erro  ConsultaHistoricoAlteracoes: " & Err.Description, Me.Name, "ConsultaHistoricoAlteracoes", False
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------

' DESACTIVA GRAVACAO, ASSIM COMO TABS DAS ANALISES, PRODUTOS, ETC.

' ---------------------------------------------------------------------------------------------------
Private Sub DesactivaGravacao(esconde As Boolean)
    On Error GoTo TrataErro
    SSTGestReq.TabEnabled(1) = esconde
    SSTGestReq.TabEnabled(2) = esconde
    SSTGestReq.TabEnabled(3) = esconde
    SSTGestReq.TabEnabled(4) = esconde
    SSTGestReq.TabEnabled(5) = esconde
    SSTGestReq.TabEnabled(6) = esconde
    
    If esconde = False Then
        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
    Else
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Remover", "Activo"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  DesactivaGravacao: " & Err.Description, Me.Name, "DesactivaGravacao", False
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------

' CONTA O NUMERO DE XS QUE UMA REQUISICAO FOI ALTERADA

' ---------------------------------------------------------------------------------------------------
Private Sub PreencheHistorico(n_req As String, indice As Integer)
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsAlt As New adodb.recordset
    Dim i As Integer
    For i = 0 To NumCampos - 1
        CamposEc(i).BackColor = CoresCamposEc(i)
    Next
    ssql = "SELECT * FROM sl_req_alteracoes WHERE n_Req = " & n_req & " AND seq_alteracao = " & numHistoricos - (indice - 1)
    rsAlt.CursorType = adOpenStatic
    rsAlt.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAlt.Open ssql, gConexao
    If rsAlt.RecordCount > 0 Then
        While Not rsAlt.EOF
            For i = 0 To NumCampos - 1
                If UCase(CamposBD(i)) = UCase(BL_HandleNull(rsAlt!campo, "")) Then
                    PreencheCampoHist i, BL_HandleNull(rsAlt!valor_ant, "")
                    Exit For
                End If
            Next
            rsAlt.MoveNext
        Wend
        PreencheCampos False
    End If
    rsAlt.Close
    Set rsAlt = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheHistorico: " & Err.Description, Me.Name, "PreencheHistorico", False
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------

' preenche um controlo com o conteudo dum campo da BD

' ---------------------------------------------------------------------------------------------------
Function PreencheCampoHist(indice As Integer, valor As String) As Integer
'

    On Error GoTo TrataErro
    
    'TextBox, Label, MaskEdBox, RichTextBox
    If TypeOf CamposEc(indice) Is TextBox Or TypeOf CamposEc(indice) Is Label Or TypeOf CamposEc(indice) Is MaskEdBox Or TypeOf CamposEc(indice) Is RichTextBox Then
        If IsNull(valor) Then
            CamposEc(indice) = ""
        Else
            CamposEc(indice) = Trim(valor)
            
            If left(CamposEc(indice).Tag, 2) = adDecimal Or left(CamposEc(indice).Tag, 1) = adDouble Or left(CamposEc(indice).Tag, 3) = adNumeric Then
                CamposEc(indice) = BG_CvDecimalParaDisplay_ADO(CamposEc(indice))
            ElseIf CamposEc(indice).Tag = mediTipoData Then
                CamposEc(indice) = CamposEc(indice)
            ElseIf CamposEc(indice).Tag = mediTipoHora Then
                CamposEc(indice) = BG_CvHora(CamposEc(indice))
            End If
        End If
    'ComboBox
    ElseIf TypeOf CamposEc(indice) Is ComboBox Then
        If IsNull(valor) Or Trim(valor) = "" Then
            CamposEc(indice).ListIndex = -1
        Else
            BG_MostraComboSel valor, CamposEc(indice)
        End If
    'DataCombo
    ElseIf TypeOf CamposEc(indice) Is DataCombo Then
            If IsNull(valor) Then
                CamposEc(indice).Index = -1
            Else
                BG_MostraDataComboSel valor, CamposEc(indice)
            End If
    'CheckBox
    ElseIf TypeOf CamposEc(indice) Is CheckBox Then
        If IsNull(valor) Then
            CamposEc(indice).value = vbGrayed
        Else
            If valor = "" Then
                CamposEc(indice).value = vbGrayed
            Else
                CamposEc(indice).value = valor
            End If
        End If
    ' Nada
    Else
    End If
    CamposEc(indice).BackColor = vermelhoClaro
    PreencheCampoHist = 0
    Exit Function
TrataErro:
    PreencheCampoHist = -1
    Exit Function

End Function

' ---------------------------------------------------------------------------------------------------

' REGISTA NA BASE DADOSTUBOS ELIMINADOS

' ---------------------------------------------------------------------------------------------------
Private Sub RegistaTubosEliminados()
    Dim ssql As String
    Dim i As Integer
    On Error GoTo TrataErro
    For i = 1 To TotalTubosEliminados
        TB_ApagaTuboBD BL_HandleNull(tubosEliminados(i).n_req, EcNumReq), tubosEliminados(i).seq_req_tubo
    Next
    TotalTubosEliminados = 0
    ReDim tubosEliminados(0)
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  RegistaTubosEliminados: " & Err.Description, Me.Name, "RegistaTubosEliminados", False
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------------------------

' ACRESCENTA A ESTRUTURA OS TUBOS ELIMINADOS.

' ---------------------------------------------------------------------------------------------------
Private Sub AcrescentaTubosEliminados(n_req As String, seq_req_tubo As Long)
    On Error GoTo TrataErro
    TotalTubosEliminados = TotalTubosEliminados + 1
    ReDim Preserve tubosEliminados(TotalTubosEliminados)
    tubosEliminados(TotalTubosEliminados).n_req = n_req
    tubosEliminados(TotalTubosEliminados).seq_req_tubo = seq_req_tubo
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AcrescentaTubosEliminados: " & Err.Description, Me.Name, "AcrescentaTubosEliminados", False
    Exit Sub
    Resume Next
End Sub


Private Sub EcAnaEfr_GotFocus()
        cmdOK.Default = False
End Sub

Private Sub EcAnaEfr_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        If Len(EcAnaEfr) >= 13 Then
            EcCredAct = EcAnaEfr
        ElseIf (Len(EcAnaEfr)) >= 1 Then
            FGAna.row = FGAna.rows - 1
            LastRowA = FGAna.row
            MarcacaoCodigoBarrasP1 EcAnaEfr, False
        End If
        EcAnaEfr = ""
        EcAnaEfr.SetFocus
    End If
End Sub


' ------------------------------------------------------------------------------------------------

' VERIFICA SE ANALISE FOI MARCACDA PELO CODIGO DE BARRAS DO P1

' ------------------------------------------------------------------------------------------------
Private Function MarcacaoCodigoBarrasP1(codAna As String, ByVal ultimaLinha As Boolean) As String
    On Error GoTo TrataErro
    Dim codRubrEfr As String
    Dim rsAnaFact As New adodb.recordset
    Dim ssql As String
    Dim temp  As String
    Dim i As Integer
    Dim MultAna As String
    If codAna <> "" Then
        FGAna.Enabled = False
        codRubrEfr = IF_ObtemRubrParaEFR(EcCodEFR, codAna)
        If codRubrEfr <> "" Then
            ssql = "SELECT x1.cod_ana FROM sl_ana_facturacao x1, slv_analises_factus x2 WHERE replace(x1.cod_ana_gh,'A','') IN " & codRubrEfr
            ssql = ssql & " AND x1.cod_ana = x2.cod_ana and (x2.flg_invisivel = 0 or x2.flg_invisivel is null) AND (x2.inibe_marcacao is null or x2.inibe_marcacao = 0)"
            ssql = ssql & " AND x2.cod_ana IN (SELECT cod_ana FROM Sl_ana_locais WHERE cod_local = " & gCodLocal & ")"
            ssql = ssql & " AND cod_portaria IN (Select x3.cod_portaria FROM sl_portarias x3 WHERE " & BL_TrataDataParaBD(BL_HandleNull(EcDataChegada.text, dataAct)) & " BETWEEN "
            ssql = ssql & " dt_ini and NVL(dt_fim, '31-12-2099')) "
            rsAnaFact.CursorLocation = adUseServer
            rsAnaFact.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros ssql
            rsAnaFact.Open ssql, gConexao
            If rsAnaFact.RecordCount = 0 Then
                MarcacaoCodigoBarrasP1 = ""
                rsAnaFact.Close
                BG_Mensagem mediMsgBox, "Mapeamento inexistente!", vbOKOnly + vbInformation, "An�lises"
                Set rsAnaFact = Nothing
                                'edgar.parada
                anaExistente = False
                '
                Exit Function
            ElseIf rsAnaFact.RecordCount >= 1 Then
                If rsAnaFact.RecordCount > 1 Then
                    'MsgBox codRubrEfr
                    MultAna = "("
                    While Not rsAnaFact.EOF
                        MultAna = MultAna & BL_TrataStringParaBD(rsAnaFact!cod_ana) & ","
                        rsAnaFact.MoveNext
                    Wend
                    MultAna = Mid(MultAna, 1, Len(MultAna) - 1) & ")"
                    'edgar.parada Glintt-HS-21197 16.05.2019 Parametro mcdtDesct
                    'AdicionaAnaliseMulti MultAna, BL_HandleNull(EcCredAct, "1"), estrutOperation(indexCred).req.DetReq(indexAna).mcdtDescr
                    'tania.oliveira 15.10.2020 GX-61420 MANSO
                    AdicionaAnaliseMulti MultAna, BL_HandleNull(EcCredAct, "1"), estrutOperation(indexCred).req.DetReq(indexAna).mcdtDescr & IIf(estrutOperation(indexCred).req.DetReq(indexAna).ClinicalInfoDet <> "", "-" & estrutOperation(indexCred).req.DetReq(indexAna).ClinicalInfoDet, "") & IIf(estrutOperation(indexCred).req.DetReq(indexAna).productDescr <> "", "-" & estrutOperation(indexCred).req.DetReq(indexAna).productDescr, "")
                    '
                Else
                MultAna = ""
                rsAnaFact.MoveFirst
                MarcacaoCodigoBarrasP1 = BL_HandleNull(rsAnaFact!cod_ana, "")
                'edgar.parada LJMANSO-351 10.01.2019
                estrutOperation(indexCred).req.DetReq(indexAna).mcdtCodSISLAB = MarcacaoCodigoBarrasP1
                '
                AdicionaAnalise MarcacaoCodigoBarrasP1, False, ultimaLinha, ""
                
                End If
            End If
            rsAnaFact.Close
            Set rsAnaFact = Nothing
            FGAna.Enabled = True
        Else
            BG_Mensagem mediMsgBox, "R�brica inexistente!", vbOKOnly + vbInformation, "An�lises"
                        'edgar.parada
            anaExistente = False
            '
        End If
    End If
Exit Function
TrataErro:
    FGAna.Enabled = True
    BG_LogFile_Erros "MarcacaoCodigoBarrasP1: " & Err.Number & " - " & Err.Description, Me.Name, "MarcacaoCodigoBarrasP1"
    MarcacaoCodigoBarrasP1 = ""
    Exit Function
    Resume Next
End Function

'edgar.parada Glintt-HS-21197 16.05.2019 parametro descrMcdt
Private Sub AdicionaAnaliseMulti(multiAna As String, credencial As String, descrMcdt As String)

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1000) As Variant
    Dim PesqRapida As Boolean
    'edgar.parada LJMANSO-351 10.01.2019
    Dim codAna As String
    '
    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposEcran(3) = "prod.descr_produto"
    Tamanhos(3) = 1820
    Headers(3) = "Produto"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "slv_analises_factus ana LEFT JOIN sl_produto prod on ana.cod_produto = prod.cod_produto "
    CWhere = " cod_ana in (select cod_ana from sl_ana_locais where cod_local = " & gCodLocal & ") AND (flg_invisivel is null or flg_invisivel = 0) AND (inibe_marcacao is null or inibe_marcacao = 0) AND cod_ana IN " & multiAna
    CampoPesquisa = "descr_ana"
    'edgar.parada Glintt-HS-21197 16.05.2019 parametro descrMcdt
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana ", _
                                                                           " An�lises", _
                                                                           descrMcdt, _
                                                                           IIf(UBound(estrutOperation) > 0, True, False))

    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            FGAna.row = FGAna.rows - 1
            LastRowA = FGAna.row
            'edgar.parada LJMANSO-351 10.01.2019
            codAna = BL_SelCodigo("SLV_ANALISES_APENAS", "COD_ANA", "SEQ_ANA", Resultados(i))
            estrutOperation(indexCred).req.DetReq(indexAna).mcdtCodSISLAB = codAna
            AdicionaAnalise codAna, False, False, ""
            '
        Next i
    Else
        anaExistente = False
    End If
    '
End Sub
'RGONCALVES 17.02.2015 CHSJ-1785
Private Sub ActualizaCodTDest()
    On Error GoTo TrataErro
    Dim cod_t_dest As String
    
    If A_PREENCHER_CAMPOS = True Then
       Exit Sub
    End If
    
    If IsNumeric(Trim(EcCodProveniencia.text)) Then
        cod_t_dest = BL_DevolveCodTDest(Trim(EcCodProveniencia.text))
        If cod_t_dest <> "" Then
            BG_MostraComboSel cod_t_dest, CbDestino
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ActualizaCodTDest: " & Err.Description, Me.Name, "ActualizaCodTDest", False
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheDadosNovoRecibo(linha As Long)
    Dim iResp As Integer
    Dim iAna As Integer
    'On Error GoTo TrataErro
    For iResp = 1 To fa_movi_resp_tot
        For iAna = 1 To fa_movi_resp(iResp).totalAna
            If fa_movi_resp(iResp).analises(iAna).iAnalise = linha And fa_movi_resp(iResp).analises(iAna).flg_estado <> 2 And fa_movi_resp(iResp).analises(iAna).flg_estado_doe <> 2 And fa_movi_resp(iResp).analises(iAna).flg_estado_tx <> 2 Then
                fa_movi_resp(iResp).analises(iAna).iAnalise = linha
                AnalisesFact(linha).cod_rubr = fa_movi_resp(iResp).analises(iAna).cod_rubr
                AnalisesFact(linha).iAnaFact = iAna
                AnalisesFact(linha).iResp = iResp
                AnalisesFact(linha).n_seq_prog = fa_movi_resp(iResp).analises(iAna).n_seq_prog
                
                FGAna.TextMatrix(linha, lColFgAnaP1) = fa_movi_resp(iResp).analises(iAna).nr_req_ars
                FGAna.TextMatrix(linha, lColFgAnaMedico) = fa_movi_resp(iResp).analises(iAna).cod_medico
                FGAna.TextMatrix(linha, lColFgAnaCodEFR) = fa_movi_resp(iResp).cod_efr
                FGAna.TextMatrix(linha, lColFgAnaDescrEFR) = fa_movi_resp(iResp).descr_efr
                FGAna.TextMatrix(linha, lColFgAnaNrBenef) = fa_movi_resp(iResp).n_benef_doe
                If fa_movi_resp(iResp).analises(iAna).val_Taxa > 0 Then
                    FGAna.TextMatrix(linha, lColFgAnaPreco) = Round(fa_movi_resp(iResp).analises(iAna).val_Taxa * fa_movi_resp(iResp).analises(iAna).qtd, 2)
                Else
                    FGAna.TextMatrix(linha, lColFgAnaPreco) = fa_movi_resp(iResp).analises(iAna).val_pag_doe
                End If
                'NELSONPSILVA GLINTT-HS-21312
                FGAna.TextMatrix(linha, lColFgAnaNAmostras) = fa_movi_resp(iResp).analises(iAna).qtd
            End If
        Next iAna
    Next iResp
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheDadosNovoRecibo: " & Err.Description, Me.Name, "PreencheDadosNovoRecibo", True
    Exit Sub
    Resume Next
End Sub

Private Sub GravaDadosAnaFact()
    Dim i As Integer
    Dim seq As Long
    Dim ssql As String
    Dim res As Integer
    On Error GoTo TrataErro
    ssql = "DELETE from sl_req_ana_movi_fact where n_req = " & EcNumReq.text
    BG_ExecutaQuery_ADO ssql
    
    For i = 1 To TotalAnalisesFact
        'edgar.parada UALIA-900 14.03.2019
        If AnalisesFact(i).iResp > 0 Then
            If AnalisesFact(i).n_seq_prog <= 0 Then
                AnalisesFact(i).n_seq_prog = fa_movi_resp(AnalisesFact(i).iResp).analises(AnalisesFact(i).iAnaFact).n_seq_prog
            End If
            If AnalisesFact(i).cod_rubr = mediComboValorNull Then
                AnalisesFact(i).cod_rubr = fa_movi_resp(AnalisesFact(i).iResp).analises(AnalisesFact(i).iAnaFact).cod_rubr
            End If
            If AnalisesFact(i).seq_ana_fact = mediComboValorNull Then
                seq = BL_RetornaSequencia("SEQ_ANA_FACT")
                AnalisesFact(i).seq_ana_fact = seq
            End If
            ssql = "INSERT INTO sl_req_ana_movi_fact (seq_ana_fact , n_req , cod_agrup, n_Seq_prog, cod_rubr) VALUES("
            ssql = ssql & AnalisesFact(i).seq_ana_fact & "," & EcNumReq.text & "," & BL_TrataStringParaBD(AnalisesFact(i).cod_ana) & " ,"
            ssql = ssql & BL_TrataNumberParaBD(CStr(AnalisesFact(i).n_seq_prog)) & "," & BL_TrataNumberParaBD(CStr(AnalisesFact(i).cod_rubr)) & ")"
            res = BG_ExecutaQuery_ADO(ssql)
            If res = 1 Then
            End If
        End If
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  GravaDadosAnaFact: " & Err.Description, Me.Name, "GravaDadosAnaFact", False
    Exit Sub
    Resume Next
End Sub

Private Sub AtualizaDadosMoviFact(linha As Integer)
    Dim iResp As Integer
    Dim iAna As Integer
    For iResp = 1 To fa_movi_resp_tot
        For iAna = 1 To fa_movi_resp(iResp).totalAna
            If fa_movi_resp(iResp).analises(iAna).cod_rubr = AnalisesFact(linha).cod_rubr Then
                AnalisesFact(linha).iAnaFact = iAna
                AnalisesFact(linha).iResp = iResp
                AnalisesFact(linha).n_seq_prog = fa_movi_resp(iResp).analises(iAna).n_seq_prog
            End If
        Next iAna
    Next iResp
End Sub

Private Sub AdicionaDadosMoviFact(cod_agrup As String, linha As Integer, n_seq_prog As Long, seq_ana_fact As Long, cod_rubr As Long)
    Dim i As Integer
    Dim iResp As Integer
    Dim iAnaFact As Integer
    On Error GoTo TrataErro
    For i = 1 To TotalAnalisesFact
        If AnalisesFact(i).cod_ana = cod_agrup Then
            If AnalisesFact(i).seq_ana_fact <= 0 Then
                
                AnalisesFact(i).seq_ana_fact = seq_ana_fact
                AnalisesFact(i).n_seq_prog = n_seq_prog
                AnalisesFact(i).cod_rubr = cod_rubr
            End If
            For iResp = 1 To fa_movi_resp_tot
                For iAnaFact = 1 To fa_movi_resp(iResp).totalAna
                    If fa_movi_resp(iResp).analises(iAnaFact).cod_rubr = AnalisesFact(i).cod_rubr And cod_rubr > mediComboValorNull And fa_movi_resp(iResp).analises(iAnaFact).n_seq_prog = n_seq_prog Then
                        AnalisesFact(i).n_seq_prog = fa_movi_resp(iResp).analises(iAnaFact).n_seq_prog
                        AnalisesFact(i).iAnaFact = iAnaFact
                        AnalisesFact(i).iResp = iResp
                        AnalisesFact(i).cod_rubr = fa_movi_resp(iResp).analises(iAnaFact).cod_rubr
                        fa_movi_resp(iResp).analises(iAnaFact).iAnalise = i
                        Exit For
                    End If
                Next iAnaFact
            Next iResp
                        'edgar.parada Glintt-HS-19834 22.10.2018 novo parametro (cod_agrup) => analise
            AtualisaDadosMoviFact CLng(i), cod_agrup
                        '
            Exit Sub
        End If
    Next i
    If linha = mediComboValorNull Then
        For i = 1 To UBound(MaReq)
            If MaReq(i).cod_agrup = cod_agrup Then
                linha = i
                Exit For
            End If
        Next i
    End If
    TotalAnalisesFact = TotalAnalisesFact + 1
    ReDim Preserve AnalisesFact(TotalAnalisesFact)
    AnalisesFact(TotalAnalisesFact).cod_ana = cod_agrup
    AnalisesFact(TotalAnalisesFact).iAna = linha
    AnalisesFact(TotalAnalisesFact).n_seq_prog = n_seq_prog
    AnalisesFact(TotalAnalisesFact).seq_ana_fact = seq_ana_fact
    AnalisesFact(TotalAnalisesFact).cod_rubr = cod_rubr
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaDadosMoviFact: " & Err.Description, Me.Name, "AdicionaDadosMoviFact", False
    Exit Sub
    Resume Next

End Sub


Private Sub PreencheDadosMoviFact()
    Dim ssql As String
    Dim rsAnaFact As New adodb.recordset
    Dim i As Integer
        'edgar.parada Glintt-HS-19834 24.10.2018 ordem n_ord_ins
    ssql = "SELECT * FROM sl_req_ana_movi_fact WHERE n_req = " & EcNumReq.text & " ORDER BY n_seq_prog"
    '
    rsAnaFact.CursorType = adOpenStatic
    rsAnaFact.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAnaFact.Open ssql, gConexao
    If rsAnaFact.RecordCount >= 1 Then
        While Not rsAnaFact.EOF
            AdicionaDadosMoviFact BL_HandleNull(rsAnaFact!cod_agrup, ""), mediComboValorNull, BL_HandleNull(rsAnaFact!n_seq_prog, mediComboValorNull), _
                                    BL_HandleNull(rsAnaFact!seq_ana_fact, mediComboValorNull), BL_HandleNull(rsAnaFact!cod_rubr, mediComboValorNull)
            rsAnaFact.MoveNext
        Wend
    End If
    AtualizaFgRecibosNew
    rsAnaFact.Close
    Set rsAnaFact = Nothing

End Sub
 
Public Sub PreencheDadosDocumentos()
    Dim ssql As String
    Dim rsAnaFact As New adodb.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    
    ssql = "SELECT * FROM slv_documentos_Doente WHERE episodio = " & BL_TrataStringParaBD(EcNumReq.text)
    ssql = ssql & " ORDER BY DT_CRI ASC "
    rsAnaFact.CursorType = adOpenStatic
    rsAnaFact.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    rsAnaFact.Open ssql, gConexao
    totalDodFinanceiros = 0
    ReDim docFinanceiros(totalDodFinanceiros)
    If rsAnaFact.RecordCount >= 1 Then
        While Not rsAnaFact.EOF
            totalDodFinanceiros = totalDodFinanceiros + 1
            ReDim Preserve docFinanceiros(totalDodFinanceiros)
            docFinanceiros(totalDodFinanceiros).tipo = BL_HandleNull(rsAnaFact!tipo, "")
            docFinanceiros(totalDodFinanceiros).cod_efr = BL_HandleNull(rsAnaFact!cod_efr, mediComboValorNull)
            docFinanceiros(totalDodFinanceiros).Desconto = BL_HandleNull(rsAnaFact!Desconto, 0)
            docFinanceiros(totalDodFinanceiros).descr_efr = BL_HandleNull(rsAnaFact!descr_efr, "")
            docFinanceiros(totalDodFinanceiros).descr_pagamento = BL_HandleNull(rsAnaFact!descr_frm_pag, "")
            docFinanceiros(totalDodFinanceiros).dt_emissao = BL_HandleNull(rsAnaFact!dt_emiss_doc, "")
            docFinanceiros(totalDodFinanceiros).dt_pagamento = BL_HandleNull(rsAnaFact!dt_pagamento, "")
            docFinanceiros(totalDodFinanceiros).iEmiRec = mediComboValorNull
            docFinanceiros(totalDodFinanceiros).modo_pagamento = BL_HandleNull(rsAnaFact!cod_frm_pag, mediComboValorNull)
            docFinanceiros(totalDodFinanceiros).n_Fac_doe = BL_HandleNull(rsAnaFact!n_doc, mediComboValorNull)
            docFinanceiros(totalDodFinanceiros).serie_fac_doe = BL_HandleNull(rsAnaFact!serie_doc, "")
            docFinanceiros(totalDodFinanceiros).seq_utente = BL_HandleNull(rsAnaFact!seq_utente, mediComboValorNull)
            docFinanceiros(totalDodFinanceiros).valor = BL_HandleNull(rsAnaFact!val_doc, 0)
            docFinanceiros(totalDodFinanceiros).flg_estado = BL_HandleNull(rsAnaFact!flg_estado, "F")
            docFinanceiros(totalDodFinanceiros).empresa_id = BL_HandleNull(rsAnaFact!empresa_id, "")
            docFinanceiros(totalDodFinanceiros).flg_borla = BL_HandleNull(rsAnaFact!flg_borla, "N")
            
            rsAnaFact.MoveNext
        Wend
    End If
    rsAnaFact.Close
    Set rsAnaFact = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDadosDocumentos: " & ssql & " " & Err.Number & " - " & Err.Description, Me.Name, "PreencheDadosDocumentos"
    Exit Sub
    Resume Next
End Sub
'edgar.parada Glintt-HS-19834 22.10.2018 novo parametro (analise)
Private Sub AtualisaDadosMoviFact(linha As Integer, Optional ByVal analise As String)
    Dim iResp As Integer
    Dim iAna As Integer
    On Error GoTo TrataErro
    For iResp = 1 To fa_movi_resp_tot
        For iAna = 1 To fa_movi_resp(iResp).totalAna
            'edgar.parada Glintt-HS-19834 22.10.2018  nova condicao
            'LJMANSO-343 -> trocado indice iAna por linha
            If fa_movi_resp(iResp).analises(iAna).cod_rubr = AnalisesFact(linha).cod_rubr And (fa_movi_resp(iResp).analises(iAna).n_seq_prog = AnalisesFact(linha).n_seq_prog Or fa_movi_resp(iResp).analises(iAna).n_seq_prog = mediComboValorNull) And _
                                AnalisesFact(linha).cod_ana = analise And fa_movi_resp(iResp).analises(iAna).flg_estado <> 2 And fa_movi_resp(iResp).analises(iAna).flg_estado_doe <> 2 And fa_movi_resp(iResp).analises(iAna).flg_estado_tx <> 2 Then
                fa_movi_resp(iResp).analises(iAna).iAnalise = linha
                AnalisesFact(linha).iAnaFact = iAna
                AnalisesFact(linha).iResp = iResp
                AnalisesFact(linha).n_seq_prog = fa_movi_resp(iResp).analises(iAna).n_seq_prog
                FGAna.TextMatrix(linha, lColFgAnaP1) = fa_movi_resp(iResp).analises(iAna).nr_req_ars
                FGAna.TextMatrix(linha, lColFgAnaMedico) = fa_movi_resp(iResp).analises(iAna).cod_medico
                FGAna.TextMatrix(linha, lColFgAnaCodEFR) = fa_movi_resp(iResp).cod_efr
                FGAna.TextMatrix(linha, lColFgAnaDescrEFR) = fa_movi_resp(iResp).descr_efr
                FGAna.TextMatrix(linha, lColFgAnaNrBenef) = fa_movi_resp(iResp).n_benef_doe
                If fa_movi_resp(iResp).analises(iAna).val_Taxa > 0 Then
                    FGAna.TextMatrix(linha, lColFgAnaPreco) = Round(fa_movi_resp(iResp).analises(iAna).val_Taxa * fa_movi_resp(iResp).analises(iAna).qtd, 2)
                Else
                    FGAna.TextMatrix(linha, lColFgAnaPreco) = fa_movi_resp(iResp).analises(iAna).val_pag_doe
                End If
                'NELSONPSILVA GLINTT-HS-21312
                FGAna.TextMatrix(linha, lColFgAnaNAmostras) = fa_movi_resp(iResp).analises(iAna).qtd
                '
            End If
        Next iAna
    Next iResp
    Exit Sub
TrataErro:
    BG_LogFile_Erros "AtualisaDadosMoviFact: " & Err.Number & " - " & Err.Description, Me.Name, "AtualisaDadosMoviFact"
    Exit Sub
    Resume Next
End Sub

Public Sub AtualizaFgRecibosNew()
    Dim iResp As Integer
    Dim iAnaFact As Integer
    Dim iCol As Integer
    Dim iRec As Integer
    Dim flgInseriu As Boolean
    Dim i As Integer
    Dim cor As Long
    FGRecibos.rows = 2
    'LJMANSO-302
    Dim totalDet As Double
    On Error GoTo TrataErro
    '
    For iCol = 0 To FGRecibos.Cols - 1
        FGRecibos.TextMatrix(1, iCol) = ""
    Next iCol
    totalRecibosNew = 0
    ReDim RecibosNew(0)
    totalRecibosNew = totalDodFinanceiros
    ReDim RecibosNew(totalRecibosNew)
     RecibosNew = docFinanceiros
    
    For iResp = 1 To fa_movi_resp_tot
        For iAnaFact = 1 To fa_movi_resp(iResp).totalAna
            If (fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe = 1 Or fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe = mediComboValorNull) Or _
                (fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx = 1 Or fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx = mediComboValorNull) Then
                flgInseriu = False
                For iRec = 1 To totalRecibosNew
                    If RecibosNew(iRec).cod_efr = fa_movi_resp(iResp).cod_efr Then
                        If RecibosNew(iRec).n_Fac_doe = fa_movi_resp(iResp).analises(iAnaFact).n_Fac_doe And RecibosNew(iRec).serie_fac_doe = fa_movi_resp(iResp).analises(iAnaFact).serie_fac_doe Then
                            flgInseriu = True
                            Exit For
                        End If
                    End If
                Next iRec
                If flgInseriu = False Then
                    totalRecibosNew = totalRecibosNew + 1
                    ReDim Preserve RecibosNew(totalRecibosNew)
                    iRec = totalRecibosNew
                End If
                If RecibosNew(iRec).flg_estado = gEstadoFactNaoFaturado Or RecibosNew(iRec).flg_estado = "" Then
                    RecibosNew(iRec).cod_efr = fa_movi_resp(iResp).cod_efr
                    RecibosNew(iRec).descr_efr = fa_movi_resp(iResp).descr_efr
                    RecibosNew(iRec).n_Fac_doe = fa_movi_resp(iResp).analises(iAnaFact).n_Fac_doe
                    RecibosNew(iRec).serie_fac_doe = fa_movi_resp(iResp).analises(iAnaFact).serie_fac_doe
                    RecibosNew(iRec).empresa_id = fa_movi_resp(iResp).analises(iAnaFact).empresa_id
                    RecibosNew(iRec).valor = RecibosNew(totalRecibosNew).valor + Round(fa_movi_resp(iResp).analises(iAnaFact).val_Taxa * fa_movi_resp(iResp).analises(iAnaFact).qtd, 2) + fa_movi_resp(iResp).analises(iAnaFact).val_pag_doe
                    RecibosNew(iRec).Desconto = fa_movi_resp(iResp).analises(iAnaFact).perc_pag_doe
                    RecibosNew(iRec).dt_emissao = ""
                    RecibosNew(iRec).dt_pagamento = ""
                    RecibosNew(iRec).flg_estado = gEstadoFactNaoFaturado
                    'LJMANSO-302
                    If colRec = lColRecibosValorPagar Then
                        RecibosNew(iRec).totalDet = RecibosNew(iRec).totalDet + fa_movi_resp(iResp).analises(iAnaFact).val_pag_doe
                        'totalDet = totalDet + fa_movi_resp(iResp).analises(iAnaFact).val_pag_doe
                    End If
                    '
                End If
            End If
        Next iAnaFact
    Next iResp
    
    'LJMANSO-302
    If colRec = lColRecibosValorPagar Then
        For iResp = 1 To fa_movi_resp_tot
            If fa_movi_resp(iResp).flg_estado = 8 Then Exit For
                If fa_movi_resp(iResp).flg_preco_alterado = True Then
                    For iRec = 1 To totalRecibosNew
                        'UALIA-903
                        If fa_movi_resp(iResp).cod_efr = RecibosNew(iRec).cod_efr And (RecibosNew(iRec).flg_estado <> gEstadoFactCreditado And RecibosNew(iRec).tipo <> gEstadoFactCreditado And RecibosNew(iRec).tipo <> gEstadoFactFaturado And RecibosNew(iRec).flg_estado <> gEstadoFactFaturado) Then
                            While (Round(RecibosNew(iRec).totalDet, 2) <> Round(CDbl(fa_movi_resp(iResp).valor_marcado_utilizador), 2))
                                For iAnaFact = 1 To fa_movi_resp(iResp).totalAna
                                        If Round(RecibosNew(iRec).totalDet, 2) = Round(CDbl(fa_movi_resp(iResp).valor_marcado_utilizador), 2) Then Exit For
                                        
                                        If Round(RecibosNew(iRec).totalDet, 2) > Round(CDbl(fa_movi_resp(iResp).valor_marcado_utilizador), 2) Then
                                            fa_movi_resp(iResp).analises(iAnaFact).val_pag_doe = fa_movi_resp(iResp).analises(iAnaFact).val_pag_doe - 0.01
                                            RecibosNew(iRec).totalDet = RecibosNew(iRec).totalDet - 0.01
                                            fa_movi_resp(iResp).analises(iAnaFact).val_pr_unit = fa_movi_resp(iResp).analises(iAnaFact).val_pr_unit + 0.01
                                            fa_movi_resp(iResp).analises(iAnaFact).val_total = fa_movi_resp(iResp).analises(iAnaFact).val_pr_unit * fa_movi_resp(iResp).analises(iAnaFact).qtd
                                        Else
                                            fa_movi_resp(iResp).analises(iAnaFact).val_pag_doe = fa_movi_resp(iResp).analises(iAnaFact).val_pag_doe + 0.01
                                            RecibosNew(iRec).totalDet = RecibosNew(iRec).totalDet + 0.01
                                            fa_movi_resp(iResp).analises(iAnaFact).val_pr_unit = fa_movi_resp(iResp).analises(iAnaFact).val_pr_unit - 0.01
                                            fa_movi_resp(iResp).analises(iAnaFact).val_total = fa_movi_resp(iResp).analises(iAnaFact).val_pr_unit * fa_movi_resp(iResp).analises(iAnaFact).qtd
                                        End If
                                Next iAnaFact
                            Wend
                        End If
                    Next iRec
                    For iRec = 1 To totalRecibosNew
                        If fa_movi_resp(iResp).cod_efr = RecibosNew(iRec).cod_efr And RecibosNew(iRec).n_Fac_doe = mediComboValorNull Then
                            Exit For
                        End If
                    Next iRec
                    RecibosNew(iRec).valor = Round(CDbl(fa_movi_resp(iResp).valor_marcado_utilizador), 2)
              End If
        Next iResp
    End If
   
    For iRec = 1 To totalRecibosNew
        If RecibosNew(iRec).n_Fac_doe <> mediComboValorNull Then
            FGRecibos.TextMatrix(iRec, lColRecibosNumDoc) = RecibosNew(iRec).serie_fac_doe & "/" & RecibosNew(iRec).n_Fac_doe
        Else
            FGRecibos.TextMatrix(iRec, lColRecibosNumDoc) = ""
        End If
        FGRecibos.TextMatrix(iRec, lColRecibosEntidade) = RecibosNew(iRec).descr_efr
        
        'BRUNODSANTOS - LJMANSO-302
'        If colRec = lColRecibosValorPagar Then
'            FGRecibos.TextMatrix(iRec, lColRecibosValorPagar) = AuxRecVal
'        Else
            FGRecibos.TextMatrix(iRec, lColRecibosValorPagar) = RecibosNew(iRec).valor
'        End If
        
        FGRecibos.TextMatrix(iRec, lColRecibosPercentagem) = RecibosNew(iRec).Desconto
        FGRecibos.TextMatrix(iRec, lColRecibosDtEmissao) = RecibosNew(iRec).dt_emissao
        FGRecibos.TextMatrix(iRec, lColRecibosDtPagamento) = RecibosNew(iRec).dt_pagamento
        FGRecibos.TextMatrix(iRec, lColRecibosModoPag) = RecibosNew(iRec).descr_pagamento
        FGRecibos.TextMatrix(iRec, lColRecibosTipo) = RecibosNew(iRec).tipo
        FGRecibos.TextMatrix(iRec, lColRecibosBorla) = RecibosNew(iRec).flg_borla
        If RecibosNew(iRec).flg_estado = gEstadoFactFaturado Then
            If RecibosNew(iRec).dt_pagamento <> "" Then
                FGRecibos.TextMatrix(iRec, lColRecibosEstado) = "Pago"
                If RecibosNew(iRec).tipo = "NC" Then
                    cor = vermelhoClaro
                Else
                    cor = Verde
                End If
            Else
                FGRecibos.TextMatrix(iRec, lColRecibosEstado) = "Em cobran�a"
                cor = Amarelo
            End If
        ElseIf RecibosNew(iRec).flg_estado = gEstadoFactNaoFaturado Then
            If RecibosNew(iRec).valor > 0 Then
                FGRecibos.TextMatrix(iRec, lColRecibosEstado) = "N�o emitido"
            Else
                FGRecibos.TextMatrix(iRec, lColRecibosEstado) = "Nulo"
            End If
            cor = vbWhite
        ElseIf RecibosNew(iRec).flg_estado = gEstadoFactCreditado Then
            FGRecibos.TextMatrix(iRec, lColRecibosEstado) = "Creditado"
            cor = vermelho
        ElseIf RecibosNew(iRec).flg_estado = gEstadoFactCancelado Then
            FGRecibos.TextMatrix(iRec, lColRecibosEstado) = "ANULADO"
            cor = vermelho
        End If
        BL_MudaCorFg FGRecibos, CLng(iRec), cor

        FGRecibos.AddItem ""
    Next iRec
    
    Exit Sub
TrataErro:
    BG_LogFile_Erros "AtualizaFgRecibosNew: " & Err.Number & " - " & Err.Description, Me.Name, "AtualizaFgRecibosNew"
    Exit Sub
    Resume Next
End Sub

Private Sub CreditaFaturaRecibo()
    FormGestaoRequisicaoPrivado.Enabled = False
    FormNotaCred.EcSerieDoc = RecibosNew(FGRecibos.row).serie_fac_doe
    FormNotaCred.EcNumDoc = RecibosNew(FGRecibos.row).n_Fac_doe
    FormNotaCred.EcDataDoc = RecibosNew(FGRecibos.row).dt_emissao
    FormNotaCred.Show 'vbModal
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtAnularRecibo_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtAnularRecibo_Click"
    Exit Sub
    Resume Next
End Sub

'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Sub ChangeCheckboxState(ByVal linha As Integer, ByVal coluna As Integer, ByVal EstadoCheck As String)
    
   If FGAna.TextMatrix(linha, lColFgAnaP1) = "" Then Exit Sub
    
    With FGAna
        .Col = coluna
        .ColAlignment(lColFgAnaConsenteEnvioPDS) = flexAlignCenterCenter
        .CellFontName = "Wingdings"
        .CellForeColor = vbBlack
        .CellFontSize = 15
        '.Text = EstadoCheck
        .FillStyle = flexFillSingle
    End With
    FGAna.TextMatrix(linha, lColFgAnaConsenteEnvioPDS) = EstadoCheck
End Sub

'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Sub CheckAnalisesMesmaCredencial(ByVal EstadoCheck As String)
    Dim credencial As String
    Dim i As Integer
    
    credencial = BL_HandleNull(FGAna.TextMatrix(FGAna.RowSel, lColFgAnaP1), "")
    If credencial = "" Then Exit Sub
    
    For i = 1 To FGAna.rows - 2
        If i <> FGAna.RowSel Then
             If FGAna.TextMatrix(i, lColFgAnaP1) = credencial Then
                Call ChangeCheckboxState(i, lColFgAnaConsenteEnvioPDS, EstadoCheck)
            End If
        End If
    Next

End Sub

'BRUNODSANTOS GLINTT-HS-15750 04.01.2018
Private Sub VerificaAnalisesEviarPDS(ByVal NReq As String)
    Dim i As Integer
      
    For i = 1 To FGAna.rows - 2
        'If (FGAna.TextMatrix(i, lColFgAnaConsenteEnvioPDS) <> "") Then
             Call UpdateAnaAcrescentadasEnvioPDS(NReq, FGAna.TextMatrix(i, lColFgAnaCodigo))
        'End If
    Next
    
End Sub


'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Function UpdateAnaAcrescentadasEnvioPDS(ByVal NReq As String, ByVal ArrCodAgrp As String) As Boolean
    Dim ssql As String
    Dim iReg As Long
     
    On Error GoTo TrataErro
    
    ssql = "UPDATE sl_ana_acrescentadas SET flg_enviar_pds = " & ConverteCheckPDS(DevolveEstadoCheckEnvioPDS_FgAna(ArrCodAgrp)) & " WHERE n_req = " & BL_TrataStringParaBD(EcNumReq.text) & " AND cod_agrup = " & BL_TrataStringParaBD(ArrCodAgrp)
    
    iReg = BG_ExecutaQuery_ADO(ssql)
    If iReg <> 1 Then
        UpdateAnaAcrescentadasEnvioPDS = False
    Else
        UpdateAnaAcrescentadasEnvioPDS = True
    End If
    
    Exit Function
    
TrataErro:
    UpdateAnaAcrescentadasEnvioPDS = False
    BG_LogFile_Erros "Erro  UpdateAnaAcrescentadasEnvioPDS: " & ssql & " " & Err.Description, Me.Name, "UpdateAnaAcrescentadasEnvioPDS"
    Exit Function
    Resume Next
    
End Function

'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Function ConverteCheckPDS(ByVal estadoCheckPDS As String) As Integer

    If estadoCheckPDS = EstadoCheckboxEnvioPDS_Checked Then
        ConverteCheckPDS = 1
    Else
        ConverteCheckPDS = 0
    End If

End Function

'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Function DevolveEstadoCheckEnvioPDS_FgAna(ByVal codAna As String) As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    For i = 1 To FGAna.rows - 2
        If FGAna.TextMatrix(i, lColFgAnaCodigo) = codAna Then
            DevolveEstadoCheckEnvioPDS_FgAna = FGAna.TextMatrix(i, lColFgAnaConsenteEnvioPDS)
            Exit For
        End If
    Next
    
    Exit Function
    
TrataErro:

DevolveEstadoCheckEnvioPDS_FgAna = ""
    
    
End Function

'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Function DevolveEstadoEnvioPDS(ByVal codAna As String, ByVal NReq As String) As String
    Dim rs As New adodb.recordset
    Dim sql As String
    DevolveEstadoEnvioPDS = EstadoCheckboxEnvioPDS_Unchecked
    
    sql = "SELECT * FROM sl_ana_acrescentadas WHERE n_req = " & BL_TrataStringParaBD(NReq) & " AND cod_agrup = " & BL_TrataStringParaBD(codAna)
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    
    If rs.RecordCount > 0 Then
        DevolveEstadoEnvioPDS = IIf(BL_HandleNull(rs.Fields("flg_enviar_pds").value, 0) = 1, EstadoCheckboxEnvioPDS_Checked, EstadoCheckboxEnvioPDS_Unchecked)
    End If

    rs.Close
    Set rs = Nothing
End Function

'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
Private Sub AtualizaColEnvioPDS(ByVal flg_ESP As Boolean, ByVal consentEnvioPDS As String)
Dim i As Integer
Dim estadoCheckConsent As String
estadoCheckConsent = EstadoCheckboxEnvioPDS_Unchecked

    If flg_ESP = True Then

        If consentEnvioPDS = "S" Then
            estadoCheckConsent = EstadoCheckboxEnvioPDS_Checked
                                                                                                                           
                                                                                                                                          
        End If

        'For i = 1 To FGAna.rows - 2
            Call ChangeCheckboxState(LastRowA, lColFgAnaConsenteEnvioPDS, estadoCheckConsent)
        'Next i

    Else
        If (LastColA = lColFgAnaP1) Then
            If (FGAna.TextMatrix(LastRowA, LastColA) <> "") Then
                Call ChangeCheckboxState(LastRowA, lColFgAnaConsenteEnvioPDS, EstadoCheckboxEnvioPDS_Unchecked)
            ElseIf (FGAna.TextMatrix(LastRowA, LastColA) = "") Then
                FGAna.TextMatrix(LastRowA, lColFgAnaConsenteEnvioPDS) = ""
            End If
        End If
    End If

                                    
End Sub


Private Sub PreencheFgMoviFact()
    Dim iResp As Integer
    Dim iAnaFact As Integer
    Dim flg_confere As Boolean
    Dim i As Integer
    FgMoviFact.rows = 2
    For i = 0 To FgMoviFact.Cols - 1
        FgMoviFact.TextMatrix(1, i) = ""
    Next i
    
    For iResp = 1 To fa_movi_resp_tot
        If RecibosNew(FGRecibos.row).cod_efr = fa_movi_resp(iResp).cod_efr Then
            For iAnaFact = 1 To fa_movi_resp(iResp).totalAna
                flg_confere = False
                If RecibosNew(FGRecibos.row).tipo = "R" And RecibosNew(FGRecibos.row).n_Fac_doe = fa_movi_resp(iResp).analises(iAnaFact).n_Fac_doe Then
                    flg_confere = True
                'LJMANSO-290
                'ElseIf RecibosNew(FGRecibos.row).tipo = "CC" And RecibosNew(FGRecibos.row).n_Fac_doe = fa_movi_resp(iResp).analises(iAnaFact).n_fac_tx Then
                 ElseIf (RecibosNew(FGRecibos.row).tipo = "C" Or RecibosNew(FGRecibos.row).tipo = "CC") And RecibosNew(FGRecibos.row).n_Fac_doe = fa_movi_resp(iResp).analises(iAnaFact).n_fac_tx Then
                    flg_confere = True
                ElseIf RecibosNew(FGRecibos.row).tipo = "" And fa_movi_resp(iResp).analises(iAnaFact).n_fac_tx = mediComboValorNull And fa_movi_resp(iResp).analises(iAnaFact).n_Fac_doe = mediComboValorNull Then
                    flg_confere = True
                Else
                    flg_confere = False
                End If
                If flg_confere = True Then
                    FgMoviFact.row = FgMoviFact.rows - 1
                    FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactCodRubr) = fa_movi_resp(iResp).analises(iAnaFact).cod_rubr
                    FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactDescrRubr) = fa_movi_resp(iResp).analises(iAnaFact).descr_rubr
                    FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactCodRubrEfr) = fa_movi_resp(iResp).analises(iAnaFact).cod_rubr_efr
                    FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactQtd) = fa_movi_resp(iResp).analises(iAnaFact).qtd
                    If fa_movi_resp(iResp).analises(iAnaFact).val_pr_u_doe <> mediComboValorNull Then
                        'LJMANSO-302
                        fa_movi_resp(iResp).analises(iAnaFact).val_pr_u_doe = fa_movi_resp(iResp).analises(iAnaFact).val_pag_doe / fa_movi_resp(iResp).analises(iAnaFact).qtd
                        '
                        FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactValPrUDoente) = fa_movi_resp(iResp).analises(iAnaFact).val_pr_u_doe
                    End If
                    If fa_movi_resp(iResp).analises(iAnaFact).val_pag_doe <> mediComboValorNull Then
                        FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactValPagDoe) = fa_movi_resp(iResp).analises(iAnaFact).val_pag_doe
                    End If
                    FgMoviFact.Col = lColFgMoviFactEstadoDoe
                    FgMoviFact.CellBackColor = vbWhite
                    If fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe <> mediComboValorNull Then
                        FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactEstadoDoe) = fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe
                        If fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe = 3 Then
                            FgMoviFact.CellBackColor = Verde
                        ElseIf fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe = 8 Then
                            FgMoviFact.CellBackColor = vermelho
                        End If
                    End If
                    
                    FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactSerieFacDoe) = fa_movi_resp(iResp).analises(iAnaFact).serie_fac_doe
                    FgMoviFact.Col = lColFgMoviFactSerieFacDoe
                    FgMoviFact.CellBackColor = vbWhite
                    If fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe = 3 Then
                        FgMoviFact.CellBackColor = Verde
                    ElseIf fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe = 8 Then
                        FgMoviFact.CellBackColor = vermelho
                    End If
                    
                    If fa_movi_resp(iResp).analises(iAnaFact).n_Fac_doe <> mediComboValorNull Then
                        FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactNFacDoe) = fa_movi_resp(iResp).analises(iAnaFact).n_Fac_doe
                    End If
                    FgMoviFact.Col = lColFgMoviFactNFacDoe
                    FgMoviFact.CellBackColor = vbWhite
                    If fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe = 3 Then
                        FgMoviFact.CellBackColor = Verde
                    ElseIf fa_movi_resp(iResp).analises(iAnaFact).flg_estado_doe = 8 Then
                        FgMoviFact.CellBackColor = vermelho
                    End If
                    
                    If fa_movi_resp(iResp).analises(iAnaFact).val_Taxa <> mediComboValorNull Then
                        FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactValTaxa) = fa_movi_resp(iResp).analises(iAnaFact).val_Taxa
                    End If
                    If fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx <> mediComboValorNull Then
                        FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactEstadoTx) = fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx
                        FgMoviFact.Col = lColFgMoviFactEstadoTx
                        If fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx = 3 Then
                            FgMoviFact.CellBackColor = Verde
                        ElseIf fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx = 8 Then
                            FgMoviFact.CellBackColor = vermelho
                        End If
                    End If
                    FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactSerieFacTx) = fa_movi_resp(iResp).analises(iAnaFact).serie_fac_tx
                    FgMoviFact.Col = lColFgMoviFactSerieFacTx
                    FgMoviFact.CellBackColor = vbWhite
                    If fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx = 3 Then
                        FgMoviFact.CellBackColor = Verde
                    ElseIf fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx = 8 Then
                        FgMoviFact.CellBackColor = vermelho
                    End If
                    
                    If fa_movi_resp(iResp).analises(iAnaFact).n_fac_tx <> mediComboValorNull Then
                        FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactNFacTx) = fa_movi_resp(iResp).analises(iAnaFact).n_fac_tx
                    End If
                    FgMoviFact.Col = lColFgMoviFactNFacTx
                    If fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx = 3 Then
                        FgMoviFact.CellBackColor = Verde
                    ElseIf fa_movi_resp(iResp).analises(iAnaFact).flg_estado_tx = 8 Then
                        FgMoviFact.CellBackColor = vermelho
                    End If
                    
                    If fa_movi_resp(iResp).analises(iAnaFact).val_pr_unit <> mediComboValorNull Then
                        FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactValPrUnit) = fa_movi_resp(iResp).analises(iAnaFact).val_pr_unit
                    End If
                    If fa_movi_resp(iResp).analises(iAnaFact).val_total <> mediComboValorNull Then
                        FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactValTotal) = fa_movi_resp(iResp).analises(iAnaFact).val_total
                    End If
                    If fa_movi_resp(iResp).analises(iAnaFact).flg_estado <> mediComboValorNull Then
                        FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactEstado) = fa_movi_resp(iResp).analises(iAnaFact).flg_estado
                    End If
                    FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactSerieFac) = fa_movi_resp(iResp).analises(iAnaFact).serie_fac
                    If fa_movi_resp(iResp).analises(iAnaFact).n_fac <> mediComboValorNull Then
                        FgMoviFact.TextMatrix(FgMoviFact.rows - 1, lColFgMoviFactNFac) = fa_movi_resp(iResp).analises(iAnaFact).n_fac
                    End If
                    FgMoviFact.AddItem ""
                End If
            Next iAnaFact
        End If
    Next iResp
End Sub
                                                                                   


' CARREGA ANALISES MARCADAS PELA CATIVA��O

' ------------------------------------------------------------------------------------------------
Friend Sub CarregaCativacao(ByRef estrutOp() As ESPOperation)

    'For i = 0 To UBound(estrutOperation)
    '    estrutOperation(i).codLocal = n_re
    'Next i
    Dim codRubrEfr As String
    Dim rsAna As New adodb.recordset
    Dim sql As String
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim MultAna As String
    Dim CodigoAna As String
    Dim codAnalise As String

    On Error GoTo TrataErro
    'For i = 1 To UBound(estrutOperation)
    '    codRubrEfr = IF_ObtemRubrParaEFR(EcCodEFR, estrutOperation(i).req.Entity)
    'Next i

    estrutOperation = estrutOp

    totalInfo = 0

    For i = 1 To UBound(estrutOperation)
          'edgar.parada LJMANSO-351 10.09.2018
      indexCred = i
      '
      If estrutOperation(i).OpState = "SUCESSO" And estrutOperation(i).ExistFgAna = "" Then
          'edgar.parada Glintt-HS-19165 * Apenas na informa��o geral informa��o das primeira credencial
          If estrutOperation(i).first = "S" Then
                EcCodEFR.text = estrutOperation(i).req.Entity.codEntitySISLAB
                EcDescrEFR.text = estrutOperation(i).req.Entity.EntityDescrSISLAB
                EcCodMedico.text = estrutOperation(i).req.NumCodProfSISLAB
                EcNomeMedico.text = estrutOperation(i).req.nomeMedico
                EcCodProveniencia.text = estrutOperation(i).req.PrescrLocal.LPrescCodeSISLAB
                EcDescrProveniencia.text = estrutOperation(i).req.PrescrLocal.LPrescDescrSISLAB
        
                'edgar.parada Glintt-HS-19165 * Preenche campo urg�ncia
                If estrutOperation(i).req.UrgentExam = "N" Then
                   'CbUrgencia = "Normal"
                                        CbUrgencia.ListIndex = 0
                ElseIf estrutOperation(i).req.UrgentExam = "S" Then
                   'CbUrgencia = "Urgente"
                                        CbUrgencia.ListIndex = 1
                End If
           End If
                'edgar.parada Glintt-HS-19165 * Destaca campo domicilio (bot�o)
                'Campos Codigo urbano, kms e c�digo postal obrigat�rio
                If estrutOperation(i).req.DomicileExam = "S" Then
                        BtDomicilio.BackColor = vermelhoClaro
                        'NELSONPSILVA Glintt-HS-21031 * Fun��o para devolver KM domic�lio
                        CbCodUrbano.ListIndex = 2
                        EcKm.text = Esp_GetKmDomicilio(EcCodPostalAux, Bg_DaData_ADO)
                        FrameDomicilio.Visible = True
                        EcKm.Enabled = True
                        CbCodUrbano.BackColor = corObrigatorio
                        EcKm.BackColor = corObrigatorio
                        EcCodPostal.BackColor = corObrigatorio
                        EcRuaPostal.BackColor = corObrigatorio
                        EcDescrPostal.BackColor = corObrigatorio

                        'edgar.parada Glintt-HS-19165 * campos obrigat�rios
                        TextoCamposObrigatorios(38) = "C�digo urbano do domicilio"
                        TextoCamposObrigatorios(39) = "N�mero de kms do domicilio"
                        '
                 End If
                        
             'End If
      
      
            If EcinfComplementar.text <> "" Then
                BtInfClinica.BackColor = vermelhoClaro
                EcinfComplementar.text = EcinfComplementar.text & vbCrLf & "Credencial: " & estrutOperation(i).ReqNumber & vbCrLf
                EcinfComplementar.text = EcinfComplementar.text & "Informa��o Cl�nica: " & estrutOperation(i).req.clinicalInfo & vbCrLf
                For k = 1 To UBound(estrutOperation(i).req.DetReq)
                    EcinfComplementar.text = EcinfComplementar.text & "Informa��o Complementar (" & estrutOperation(i).req.DetReq(k).CodigoMSP & "): " & estrutOperation(i).req.DetReq(k).ClinicalInfoDet & vbCrLf
                Next k
                If estrutOperation(i).req.UrgentExamJust <> "" Then
                   EcinfComplementar.text = EcinfComplementar.text & "Justifica��o Urg�ncia: " & estrutOperation(i).req.UrgentExamJust & vbCrLf
                End If
                
                If estrutOperation(i).req.DomicileExamJust <> "" Then
                   EcinfComplementar.text = EcinfComplementar.text & "Justifica��o Domicilio: " & estrutOperation(i).req.DomicileExamJust & vbCrLf
                End If
            Else
                    BtInfClinica.BackColor = vermelhoClaro
                EcinfComplementar.text = "Credencial: " & estrutOperation(i).ReqNumber & vbCrLf
                EcinfComplementar.text = EcinfComplementar.text & "Informa��o Cl�nica: " & estrutOperation(i).req.clinicalInfo & vbCrLf
                For k = 1 To UBound(estrutOperation(i).req.DetReq)
                    EcinfComplementar.text = EcinfComplementar.text & "Informa��o Complementar (" & estrutOperation(i).req.DetReq(k).CodigoMSP & "): " & estrutOperation(i).req.DetReq(k).ClinicalInfoDet & vbCrLf
                Next k
                If estrutOperation(i).req.UrgentExamJust <> "" Then
                   EcinfComplementar.text = EcinfComplementar.text & "Justifica��o Urg�ncia: " & estrutOperation(i).req.UrgentExamJust & vbCrLf
                End If
                
                If estrutOperation(i).req.DomicileExamJust <> "" Then
                   EcinfComplementar.text = EcinfComplementar.text & "Justifica��o Domicilio: " & estrutOperation(i).req.DomicileExamJust & vbCrLf
                End If
            End If

            For j = 1 To UBound(estrutOperation(i).req.DetReq)
                'edgar.parada LJMANSO-351 10.09.2018
                 indexAna = j
                '
                                                                                                          
                If estrutOperation(i).first = "S" Then
                    If estrutOperation(i).req.DetReq(j).InsetTax = "N" Then
                       EcCodIsencao.text = estrutOperation(i).req.DetReq(j).cod_isencao
                       BL_ColocaTextoCombo "sl_isencao", "seq_isencao", "cod_isencao", EcCodIsencao, CbTipoIsencao
                    ElseIf estrutOperation(i).req.DetReq(j).InsetTax = "S" Then
                                           EcCodIsencao.text = estrutOperation(i).req.DetReq(j).cod_isencao
                       BL_ColocaTextoCombo "sl_isencao", "seq_isencao", "cod_isencao", EcCodIsencao, CbTipoIsencao
                                           BtGerarRecibo.Visible = False
                    End If
                End If
                'edgar.parada LOTE 97 - Conven��o
                CbConvencao.ListIndex = 3
                '
                'NELSONPSILVA UALIA-856 31.01.2019
                BG_LimpaPassaParams
                gPassaParams.id = "CarregaCativacao"
                '
                Call MarcacaoCodigoBarrasP1(estrutOperation(i).req.DetReq(j).CodigoMSP, True)
               
                codAnalise = BL_HandleNull(estrutOperation(i).req.DetReq(j).mcdtCodSISLAB, "")
                'LJMANSO-356
                If estrutOperation(i).req.DetReq(j).InsetTax = "S" Then
                    Call FgAna_KeyDown(119, 0)
                ElseIf estrutOperation(i).req.DetReq(j).InsetTax = "N" Then
                    Call FgAna_KeyDown(120, 0)
                End If
                '
                
                'edgar.parada Glintt-HS-19165 * indica que an�lise ja existe na grid
                estrutOperation(i).ExistFgAna = "S"
                '
                If anaExistente Then
                    Call TrataCredencialESP(estrutOperation(i).ReqNumber, codAnalise, True, estrutOperation(i).req.consent, estrutOperation(i).req.NumCodProfSISLAB, estrutOperation(i).req.Entity.EntityDescrSISLAB, _
                    estrutOperation(i).req.DetReq(j).mcdtID, estrutOperation(i).req.DetReq(j).nSamples)
                    'brunoesp
                    If gPassaRecFactus = mediSim Then
                     fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).id_unico_externo = estrutOperation(i).req.DetReq(j).mcdtID
                    'Tania.Oliveira 27.11.2019
                    
                     fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).qtd = estrutOperation(i).req.DetReq(j).nSamples
                    '
                     AtualizaQtdRecibos
                    End If
                    'edgar.parada LOTE 97 - Conven��o
                    'edgar.parada  LJMANSO-351 11.01.2019 comentado
                    'fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).convencao = CbConvencao.text
                    '
                End If
                EcDataChegada.text = Format(estrutOperation(i).OperationDate, cFormatoDataBD)
                anaExistente = True
            Next j
            BloqueiaCampos
        End If
    Next i
    'MultAna = Mid(MultAna, 1, Len(MultAna) - 1) & ")"
    'FormGestaoRequisicaoPrivado.AdicionaAnalise BL_TrataStringParaBD(MultAna), False, False, MultAna
    'AdicionaAnaliseMulti MultAna, BL_HandleNull(EcCredAct, "1")
     BG_LimpaPassaParams
Exit Sub
TrataErro:
    FGAna.Enabled = True
    BG_LogFile_Erros "CarregaCativacao: " & Err.Number & " - " & Err.Description, Me.Name, "CarregaCativacao"
    Exit Sub
    Resume Next
End Sub
'edgar.parada Glintt-HS-19165
Private Sub TrataCredencialESP(ByVal CodigoMarcado As String, ByVal codAna As String, ByVal flg_ESP As Boolean, ByVal consentEnvioPDS As String, _
                                 ByVal codMedico As String, ByVal codEfr As String, ByVal mcdtID As String, ByVal NAmostras As String)
Dim iRec As Long
Dim i As Integer

On Error GoTo TrataErro

'numCredencial = ""

If gPassaRecFactus <> mediSim Then
    iRec = DevIndice(codAna)
    If FGAna.TextMatrix(LastRowA, lColFgAnaCodigo) <> "" Then
        If iRec > -1 Then
            For i = 1 To RegistosRM.Count - 1
                If RegistosRM(i).ReciboCodFacturavel = RegistosRM(iRec).ReciboCodFacturavel Then
                    RegistosRM(i).ReciboP1 = CodigoMarcado
                    FGAna.TextMatrix(LastRowA, lColFgAnaP1) = CodigoMarcado
                    RegistosRM(i).ReciboOrdemMarcacao = RetornaOrdemP1(i)
                    'brunoesp
                    FGAna.TextMatrix(LastRowA, lColFgAnaP1) = CodigoMarcado
                    FGAna.TextMatrix(LastRowA, lColFgAnaMedico) = codMedico
                    FGAna.TextMatrix(LastRowA, lColFgAnaCodEFR) = codEfr
                    ReDim Preserve CredenciaisARS(totalCredArs)
                    CredenciaisARS(totalCredArs).codAna = FGAna.TextMatrix(LastRowA, lColFgAnaCodigo)
                    CredenciaisARS(totalCredArs).CredencialARS = CodigoMarcado
                    CredenciaisARS(totalCredArs).mcdtID = mcdtID
                    totalCredArs = totalCredArs + 1
                    '
                    'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
                    If gUsaEnvioPDS = mediSim Then
                        Call AtualizaColEnvioPDS(flg_ESP, consentEnvioPDS)
                    End If
                    '
                End If
            Next
        End If
    End If
ElseIf gPassaRecFactus = mediSim Then
        If fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).flg_estado_doe <> 3 And _
        fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).flg_estado_tx <> 3 Then
            fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).nr_req_ars = CodigoMarcado
                        fa_movi_resp(AnalisesFact(LastRowA).iResp).analises(AnalisesFact(LastRowA).iAnaFact).n_doc = CodigoMarcado
            'numCredencial = CodigoMarcado
            FGAna.TextMatrix(LastRowA, lColFgAnaP1) = CodigoMarcado
            FGAna.TextMatrix(LastRowA, lColFgAnaMedico) = codMedico
            FGAna.TextMatrix(LastRowA, lColFgAnaCodEFR) = codEfr
            ReDim Preserve CredenciaisARS(totalCredArs)
            CredenciaisARS(totalCredArs).codAna = FGAna.TextMatrix(LastRowA, lColFgAnaCodigo)
            CredenciaisARS(totalCredArs).CredencialARS = CodigoMarcado
            CredenciaisARS(totalCredArs).mcdtID = mcdtID
            totalCredArs = totalCredArs + 1
            
            'BRUNODSANTOS GLINTT-HS-15750 03.01.2018
            If gUsaEnvioPDS = mediSim Then
                Call AtualizaColEnvioPDS(flg_ESP, consentEnvioPDS)
                
            End If
            '
            'NELSONPSILVA GLINTT-HS-21312
            FGAna.TextMatrix(LastRowA, lColFgAnaNAmostras) = NAmostras
            '
        End If
    End If
    Exit Sub
TrataErro:
    BG_LogFile_Erros "TrataCredencialESP: " & Err.Number & " - " & Err.Description, Me.Name, "TrataCredencialESP"
    Exit Sub
    Resume Next
End Sub
'
'edgar.parada Glintt-HS-19165 * inserir credencial
Private Sub Grava_Ana_ARS()
    Dim i As Integer
    Dim j As Integer
    Dim totalCredArsAux As Integer
    Dim sql As String
    
    On Error GoTo TrataErro
    totalCredArsAux = totalCredArs - 1
    i = RegistosA.Count
    While i > 0
        If RegistosA(i).NReqARS <> "" And RegistosA(i).codAna <> "" Then
            For j = totalCredArsAux To LBound(CredenciaisARS) + 1 Step -1
               If i = j And CredencialESP(UCase(CredenciaisARS(j).CredencialARS)) = True Then
                    sql = "INSERT INTO sl_credenciais " & _
                          "(n_req_orig, " & _
                           "n_req, " & _
                           "cod_ana, " & _
                           "p1, cod_u_saude,n_mecan_ext,mcdt_id_ext,cod_local_prestador,cod_local) " & _
                          "VALUES " & _
                          "(" & CLng(EcNumReq.text) & "," & _
                            UCase(BL_TrataStringParaBD(CredenciaisARS(j).CredencialARS)) & "," & _
                            UCase(BL_TrataStringParaBD(RegistosA(i).codAna)) & "," & _
                            BL_TrataStringParaBD(RegistosA(i).p1) & "," & _
                            BL_TrataStringParaBD(esp_devolve_cod_prescritor(CredenciaisARS(j).CredencialARS)) & "," & _
                            BL_TrataStringParaBD(RegistosA(i).MedUnidSaude) & "," & _
                            UCase(BL_TrataStringParaBD(CredenciaisARS(j).mcdtID)) & "," & gEntidadeESP & "," & gCodLocal & ")"
                    
                    BG_ExecutaQuery_ADO sql
                     
                    
                    If gSQLError <> 0 Then
                        i = 0
                    End If
                 End If
                 Exit For
            Next j
        End If
        If i = j Then
          totalCredArsAux = totalCredArsAux - 1
        End If
        i = i - 1
    Wend
        
Exit Sub
TrataErro:
    BG_LogFile_Erros "Grava_Ana_ARS: " & Err.Number & " - " & Err.Description, Me.Name, "Grava_Ana_ARS"
    Exit Sub
    Resume Next

End Sub

'edgar.parada Glintt-HS-19165 * bloquear campos de ecr�
Private Sub BloqueiaCampos()
 
 Dim i As Integer
 Dim locked As Integer
 
'EcCodEFR.Enabled = False Backlog 10959
'EcDescrEFR.Enabled = False Backlog 10959
'BtPesquisaEntFin.Enabled = False Backlog 10959
'edgar.parada  LJMANSO-357 06-02-2019
' EcCodMedico.Enabled = False
' EcNomeMedico.Enabled = False
 'BtPesquisaMedico.Enabled = False
'
 'EcCodProveniencia.Enabled = False
 'EcDescrProveniencia.Enabled = False
' BtPesquisaProveniencia.Enabled = False

 CbTipoIsencao.Enabled = False

 CbUrgencia.Enabled = False

 EcDataChegada.Enabled = False
 CbConvencao.Enabled = False
 'locked = fa_movi_resp(i).analises(i).nr_c
' For i = 1 To FGAna.rows - 2
'  If FGAna.TextMatrix(LastRowA, lColFgAnaP1) <> Empty Then
'     FGAna.Curr
'     FGAna.rows(i).locked = True
'  End If
 'FGAna.Enabled = False
 'BtPesquisaAna.Enabled = False
' Next i
End Sub

'edgar.parada Glintt-HS-19165 * Pesquisa analise na estrutura ESP
Private Function searchAnaliseESP(analise As String) As Boolean
Dim i As Integer
Dim j As Integer

On Error GoTo TrataErro
   
For i = 1 To UBound(estrutOperation)
    If estrutOperation(i).OpState = cSUCESSO Then
        For j = 1 To UBound(estrutOperation(i).req.DetReq)
           If estrutOperation(i).req.DetReq(j).mcdtCodSISLAB = analise Then
              searchAnaliseESP = True
           End If
        Next j
    End If
 Next i
Exit Function
TrataErro:
    BG_LogFile_Erros "searchAnaliseESP: " & Err.Number & " - " & Err.Description, Me.Name, "searchAnaliseESP"
    searchAnaliseESP = False
    Exit Function
    Resume Next
End Function

'edgar.parada Glintt-HS-19165 * Atualizar EFR em ESP
Private Sub atualizaEFRESP(ByVal credencial As String, ByVal efrNew As String)

Dim i As Integer
 
On Error GoTo TrataErro

If credencial = "" And efrNew = "" Then Exit Sub

For i = 1 To FGAna.rows - 1
   If FGAna.TextMatrix(i, lColFgAnaCodigo) <> "" And FGAna.TextMatrix(i, lColFgAnaP1) = credencial And FGAna.TextMatrix(i, lColFgAnaCodEFR) <> efrNew Then
       escolheEnt = False
       FGAna.row = i
       FgAna_KeyDown 13, 0
   End If
Next i
escolheEnt = True
notAtualizouEFR = True
Exit Sub
TrataErro:
    BG_LogFile_Erros "atualizaEFRESP: " & Err.Number & " - " & Err.Description, Me.Name, "atualizaEFRESP"
    Exit Sub
    Resume Next
End Sub

'edgar.parada LJMANSO-351 10.01.2019
Private Function obtemConvencao(ByVal Index As Integer, ByVal cod_ana As String, ByVal credencial As String) As Integer

Dim i As Integer
 
On Error GoTo TrataErro

If cod_ana = "" Then Exit Function

If UBound(estrutOperation) > 0 Then
    For i = 1 To UBound(estrutOperation(Index).req.DetReq)
       If estrutOperation(Index).req.DetReq(i).mcdtCodSISLAB = cod_ana Then
         obtemConvencao = lCod97
         Exit Function
       End If
    Next i
    
    If credencial = "" Then
      obtemConvencao = lCodNormal
      Exit Function
    Else
       If Mid(credencial, 3, 1) = 4 Then
         obtemConvencao = lCodNormal
       ElseIf Mid(credencial, 3, 1) = 0 Then
         obtemConvencao = lCodNormalEspecial
       End If
    End If
Else
    If credencial = "" Then
        obtemConvencao = lCodNormal
        Exit Function
    Else
        If Mid(credencial, 3, 1) = 4 Then
          obtemConvencao = lCodNormal
        ElseIf Mid(credencial, 3, 1) = 0 Then
          If CbConvencao.ItemData(CbConvencao.ListIndex) = lCodNormal Then
             obtemConvencao = lCodNormalEspecial
          ElseIf CbConvencao.ItemData(CbConvencao.ListIndex) = lCodProfissional Then
             obtemConvencao = lCodProfissionalEspecial
          ElseIf CbConvencao.ItemData(CbConvencao.ListIndex) = lCodInternacional Then
             obtemConvencao = lCodInternacionalEspecial
          End If
        End If
    End If
End If
Exit Function
TrataErro:
    BG_LogFile_Erros "atualizaEFRESP: " & Err.Number & " - " & Err.Description, Me.Name, "atualizaEFRESP"
    Exit Function
    Resume Next
End Function

'NELSONPSILVA 30.04.2019 Glintt-HS-21232
Sub EliminaAnaEspEstrut(codAna As String)
    Dim k As Integer
    On Error GoTo TrataErro

    k = 1
    While k <= UBound(CredenciaisARS)
        If Trim(UCase(codAna)) = Trim(UCase(CredenciaisARS(k).codAna)) Then
            Actualiza_Estrutura_Ana_Esp k
        Else
            k = k + 1
        End If
    Wend
        
    If UBound(CredenciaisARS) = 0 Then ReDim CredenciaisARS(1)

Exit Sub
TrataErro:
    If Err.Number <> 35601 Then
        BG_LogFile_Erros Me.Name & "EliminaAnaEspEstrut: " & Err.Description
    End If
    Resume Next
End Sub

'NELSONPSILVA 30.04.2019 Glintt-HS-21232
Private Sub Actualiza_Estrutura_Ana_Esp(inicio As Integer)
    Dim j As Integer
    On Error GoTo TrataErro
    
    If inicio <> UBound(CredenciaisARS) Then
        For j = inicio To UBound(CredenciaisARS) - 1
            CredenciaisARS(j).codAna = CredenciaisARS(j + 1).codAna
            CredenciaisARS(j).CredencialARS = CredenciaisARS(j + 1).CredencialARS
            CredenciaisARS(j).mcdtID = CredenciaisARS(j + 1).mcdtID
        Next
    End If
    
    totalCredArs = totalCredArs - 1
    ReDim Preserve CredenciaisARS(UBound(CredenciaisARS) - 1)
            
Exit Sub
TrataErro:
    BG_LogFile_Erros "Actualiza_Estrutura_Ana_Esp: " & Err.Number & " - " & Err.Description, Me.Name, "Actualiza_Estrutura_Ana_Esp"
    Exit Sub
    Resume Next
End Sub

'NELSONPSILVA 30.04.2019 Glintt-HS-21232
Sub Atualiza_Sl_Credenciais()
   On Error GoTo TrataErro
   Dim sql As String
   Dim i As Integer
    
    gSQLError = 0
    If EcNumReq <> "" Then
        For i = 1 To UBound(Eliminadas)
             If CredencialESP(Eliminadas(i).credencial) Then
                'Glintt-HS-21507 -> update estado para removido
                 sql = "UPDATE sl_credenciais SET estado = 'R' WHERE n_req_orig = " & EcNumReq & " and cod_ana = " & BL_TrataStringParaBD(Eliminadas(i).codAna)
                 BG_ExecutaQuery_ADO sql
                 
                 If gSQLError <> 0 Then
                     GoTo TrataErro
                 End If
             End If
        Next i
    End If
   
Exit Sub
TrataErro:
    BG_LogFile_Erros "Atualiza_Sl_Credenciais: " & Err.Number & " - " & Err.Description, Me.Name, "Atualiza_Sl_Credenciais"
    Exit Sub
    Resume Next
End Sub
'

Private Sub AtualizaQtdRecibos()

    FACTUS_CalculaQuantidade AnalisesFact(LastRowA).iResp, AnalisesFact(LastRowA).iAnaFact, False, 0, False, "", 0
    PreencheDadosNovoRecibo LastRowA
    AtualizaFgRecibosNew

End Sub

VERSION 5.00
Begin VB.UserControl ControloTimeline 
   ClientHeight    =   3990
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11790
   ScaleHeight     =   3990
   ScaleWidth      =   11790
   Begin SISLAB.ControloTimeLineQuebra QuebraHoje 
      Height          =   3615
      Left            =   120
      TabIndex        =   22
      Top             =   120
      Width           =   495
      _ExtentX        =   873
      _ExtentY        =   6376
   End
   Begin VB.CommandButton BtEsquerdaFast 
      Height          =   495
      Left            =   120
      Picture         =   "controloTimeline.ctx":0000
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   1560
      Width           =   495
   End
   Begin SISLAB.ControloTooltip ControloTooltip1 
      DragMode        =   1  'Automatic
      Height          =   735
      Left            =   -360
      TabIndex        =   25
      Top             =   240
      Visible         =   0   'False
      Width           =   975
      _ExtentX        =   450
      _ExtentY        =   661
   End
   Begin VB.CommandButton BtEsquerda 
      Height          =   495
      Left            =   120
      Picture         =   "controloTimeline.ctx":6852
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   2160
      Width           =   495
   End
   Begin VB.CommandButton BtDireita 
      Height          =   735
      Left            =   11040
      Picture         =   "controloTimeline.ctx":D0A4
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   1320
      Width           =   615
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   720
      TabIndex        =   17
      Top             =   3600
      Width           =   3735
      Begin VB.OptionButton OptData 
         Caption         =   "Hora"
         Height          =   255
         Index           =   3
         Left            =   2640
         TabIndex        =   21
         Top             =   120
         Width           =   735
      End
      Begin VB.OptionButton OptData 
         Caption         =   "Dia"
         Height          =   255
         Index           =   2
         Left            =   1800
         TabIndex        =   20
         Top             =   120
         Width           =   735
      End
      Begin VB.OptionButton OptData 
         Caption         =   "M�s"
         Height          =   255
         Index           =   1
         Left            =   1080
         TabIndex        =   19
         Top             =   120
         Width           =   735
      End
      Begin VB.OptionButton OptData 
         Caption         =   "Ano"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   18
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000002&
      BorderWidth     =   2
      Index           =   1
      X1              =   720
      X2              =   720
      Y1              =   120
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   2
      X1              =   1320
      X2              =   1320
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   15
      X1              =   9120
      X2              =   9120
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   16
      X1              =   9720
      X2              =   9720
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   17
      X1              =   10320
      X2              =   10320
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   5
      X1              =   3120
      X2              =   3120
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   6
      X1              =   3720
      X2              =   3720
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   7
      X1              =   4320
      X2              =   4320
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   8
      X1              =   4920
      X2              =   4920
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   9
      X1              =   5520
      X2              =   5520
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   10
      X1              =   6120
      X2              =   6120
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   11
      X1              =   6720
      X2              =   6720
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   12
      X1              =   7320
      X2              =   7320
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   13
      X1              =   7920
      X2              =   7920
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   14
      X1              =   8520
      X2              =   8520
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   4
      X1              =   2520
      X2              =   2520
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Sep 
      BorderColor     =   &H80000010&
      BorderStyle     =   3  'Dot
      Index           =   3
      X1              =   1920
      X2              =   1920
      Y1              =   360
      Y2              =   3120
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000002&
      BorderWidth     =   2
      Index           =   0
      X1              =   720
      X2              =   10920
      Y1              =   2880
      Y2              =   2880
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   16
      Left            =   10320
      TabIndex        =   16
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   15
      Left            =   9720
      TabIndex        =   15
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   14
      Left            =   9120
      TabIndex        =   14
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   13
      Left            =   8520
      TabIndex        =   13
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   12
      Left            =   7920
      TabIndex        =   12
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   11
      Left            =   7320
      TabIndex        =   11
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   6720
      TabIndex        =   10
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   9
      Left            =   6120
      TabIndex        =   9
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   8
      Left            =   5520
      TabIndex        =   8
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   4920
      TabIndex        =   7
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   4320
      TabIndex        =   6
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   3720
      TabIndex        =   5
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   3120
      TabIndex        =   4
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   2520
      TabIndex        =   3
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   1920
      TabIndex        =   2
      Top             =   2880
      Width           =   735
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   1320
      TabIndex        =   1
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label LbLegenda 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   720
      TabIndex        =   0
      Top             =   2880
      Width           =   615
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderStyle     =   0  'Transparent
      Height          =   2775
      Index           =   3
      Left            =   720
      Top             =   120
      Width           =   10215
   End
End
Attribute VB_Name = "ControloTimeline"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

Option Explicit

Const largura = 600
Const X1 = 720
Const numColunas = 17
Const Altura = 600

Const lAno = 0
Const lMes = 1
Const lDia = 2
Const lHora = 3

Const corHoje = &H80000002
Const corQuebra = &H8080FF

Dim PrimValor As Integer
Dim quebras() As Control
Dim totalQuebras

Private Type posicao
    valor As String
    Abreviatura As String
    hoje As Boolean
    quebra As String
    num_req As Integer
End Type
Dim posicoes() As posicao
Dim totalPosicoes As Integer

Private Type requisicao
    data As String
    ano As String
    mes As String
    hora As String
    n_req As String
    toolTip As String
    controlo As Control
End Type
Dim totalReq As Integer
Dim requisicoes() As requisicao

Public Sub Inicializa()
    totalQuebras = 0
    ReDim quebras(totalQuebras)
    PreencheReqDummy
    
    totalPosicoes = numColunas
    ReDim posicoes(totalPosicoes)
    
    OptData(2).Value = True
    
End Sub



Private Sub IncrementaTimeline()
    Dim i As Integer
    If OptData(lDia) Then
        For i = 1 To numColunas
            posicoes(i).valor = CStr(CDate(posicoes(i).valor) + 1)
            posicoes(i).Abreviatura = DevolveAbreviatura(posicoes(i).valor)
            posicoes(i).quebra = VerificaQuebra(i)
            If posicoes(i).valor = Date Then
                posicoes(i).hoje = True
            Else
                posicoes(i).hoje = False
            End If
        Next
    ElseIf OptData(lHora).Value = True Then
        For i = 1 To numColunas
            posicoes(i).valor = CStr(CDate(posicoes(i).valor) + (1 / 24))
            posicoes(i).Abreviatura = DevolveAbreviatura(posicoes(i).valor)
            posicoes(i).quebra = VerificaQuebra(i)
            If posicoes(i).valor = Date Then
                posicoes(i).hoje = True
            Else
                posicoes(i).hoje = False
            End If
        Next
    ElseIf OptData(lMes).Value = True Then
        For i = 1 To numColunas
            posicoes(i).valor = CStr(CDate(posicoes(i).valor) + 31)
            posicoes(i).Abreviatura = DevolveAbreviatura(posicoes(i).valor)
            posicoes(i).quebra = VerificaQuebra(i)
            If posicoes(i).valor = Date Then
                posicoes(i).hoje = True
            Else
                posicoes(i).hoje = False
            End If
        Next
    ElseIf OptData(lAno).Value = True Then
        For i = 1 To numColunas
            posicoes(i).valor = CStr(CDate(posicoes(i).valor) + 366)
            posicoes(i).Abreviatura = DevolveAbreviatura(posicoes(i).valor)
            posicoes(i).quebra = VerificaQuebra(i)
            If posicoes(i).valor = Date Then
                posicoes(i).hoje = True
            Else
                posicoes(i).hoje = False
            End If
        Next
    
    End If
    DesenhaTimeline
End Sub
Private Sub DecrementaTimeline(fast As Boolean)
    Dim i As Integer
    Dim decremento As Integer
    If OptData(lDia) Then
        If fast = True Then
            decremento = 31
        Else
            decremento = 1
        End If
        For i = 1 To numColunas

            posicoes(i).valor = CStr(CDate(posicoes(i).valor) - decremento)
            posicoes(i).Abreviatura = DevolveAbreviatura(posicoes(i).valor)
            posicoes(i).quebra = VerificaQuebra(i)
            If posicoes(i).valor = Date Then
                posicoes(i).hoje = True
            Else
                posicoes(i).hoje = False
            End If
        Next
    ElseIf OptData(lHora) Then
        If fast = True Then
            decremento = 24
        Else
            decremento = 1
        End If
        For i = 1 To numColunas
            posicoes(i).valor = CStr(CDate(posicoes(i).valor) - (decremento / 24))
            posicoes(i).Abreviatura = DevolveAbreviatura(posicoes(i).valor)
            posicoes(i).quebra = VerificaQuebra(i)
            If posicoes(i).valor = Date Then
                posicoes(i).hoje = True
            Else
                posicoes(i).hoje = False
            End If
        Next
    ElseIf OptData(lMes) Then
        If fast = True Then
            decremento = 12
        Else
            decremento = 1
        End If
        
        For i = 1 To numColunas
            posicoes(i).valor = CStr(CDate(posicoes(i).valor) - (decremento * 31))
            posicoes(i).Abreviatura = DevolveAbreviatura(posicoes(i).valor)
            posicoes(i).quebra = VerificaQuebra(i)
            If posicoes(i).valor = Date Then
                posicoes(i).hoje = True
            Else
                posicoes(i).hoje = False
            End If
        Next
    ElseIf OptData(lAno) Then
        For i = 1 To numColunas
            posicoes(i).valor = CStr(CDate(posicoes(i).valor) - 365)
            posicoes(i).Abreviatura = DevolveAbreviatura(posicoes(i).valor)
            posicoes(i).quebra = VerificaQuebra(i)
            If posicoes(i).valor = Date Then
                posicoes(i).hoje = True
            Else
                posicoes(i).hoje = False
            End If
        Next
    End If
    DesenhaTimeline
End Sub

Private Sub BtDireita_Click()
    IncrementaTimeline
End Sub

Private Sub BtEsquerda_Click()
    DecrementaTimeline False
End Sub

Private Sub BtEsquerdaFast_Click()
    DecrementaTimeline True
    
End Sub

Private Sub OptData_Click(Index As Integer)
    InicializaLabels
    
End Sub



Private Sub UserControl_Initialize()
    Inicializa
End Sub


Private Sub InicializaLabels()
    Dim i As Integer
    Dim k As Integer
    If OptData(lDia).Value = True Then
        posicoes(numColunas).valor = FormatDateTime(Date, vbShortDate)
        posicoes(numColunas).Abreviatura = DevolveAbreviatura(posicoes(numColunas).valor)
        posicoes(numColunas).hoje = True
        posicoes(numColunas).quebra = ""
       
        k = 16
        For i = 1 To numColunas - 1
            posicoes(i).valor = FormatDateTime(Date - k, vbShortDate)
            posicoes(i).Abreviatura = DevolveAbreviatura(posicoes(i).valor)
            posicoes(i).hoje = True
            posicoes(i).quebra = VerificaQuebra(i)

            k = k - 1
        Next i
    ElseIf OptData(lHora).Value = True Then
        posicoes(numColunas).valor = Now
        posicoes(numColunas).Abreviatura = DevolveAbreviatura(posicoes(numColunas).valor)
        posicoes(numColunas).hoje = True
        posicoes(numColunas).quebra = ""
        k = 16
        For i = 1 To numColunas - 1
            posicoes(i).valor = Now - (k / 24)
            posicoes(i).Abreviatura = DevolveAbreviatura(posicoes(i).valor)
            posicoes(i).hoje = True
            posicoes(i).quebra = VerificaQuebra(i)
            k = k - 1
        Next i
    ElseIf OptData(lMes).Value = True Then
        posicoes(numColunas).valor = "01" & Mid(Date, 3)
        posicoes(numColunas).Abreviatura = DevolveAbreviatura(posicoes(numColunas).valor)
        posicoes(numColunas).hoje = True
        posicoes(numColunas).quebra = ""
        k = 16
        For i = 1 To numColunas - 1
            posicoes(i).valor = CDate("01" & Mid(Date, 3)) - (k * 28)
            posicoes(i).Abreviatura = DevolveAbreviatura(posicoes(i).valor)
            posicoes(i).hoje = True
            posicoes(i).quebra = VerificaQuebra(i)
            k = k - 1
        Next i
    ElseIf OptData(lAno).Value = True Then
        posicoes(numColunas).valor = "01-01" & Mid(Date, 6)
        posicoes(numColunas).Abreviatura = DevolveAbreviatura(posicoes(numColunas).valor)
        posicoes(numColunas).hoje = True
        posicoes(numColunas).quebra = ""
        k = 16
        For i = 1 To numColunas - 1
            posicoes(i).valor = CDate("01-01" & Mid(Date, 6)) - (k * 365)
            posicoes(i).Abreviatura = DevolveAbreviatura(posicoes(i).valor)
            posicoes(i).hoje = True
            posicoes(i).quebra = VerificaQuebra(i)
            k = k - 1
        Next i
    
    
    End If
    DesenhaTimeline
End Sub
Private Sub DesenhaTimeline()
    Dim i As Integer
    For i = 1 To totalQuebras
        Controls.Remove "quebras" & CStr(i)
    Next
    totalQuebras = 0
    ReDim quebras(totalQuebras)
    QuebraHoje.Visible = False
    For i = 1 To numColunas
        If posicoes(i).quebra <> "" Then
            desenhaQuebra i
        End If
        If posicoes(i).hoje = True Then
            QuebraHoje.Visible = False
            QuebraHoje.Left = X1 + ((i - 1) * largura) - QuebraHoje.DevolveInicio
            QuebraHoje.ColocaTexto "Hoje"
            QuebraHoje.AtualizaCor corHoje
            QuebraHoje.Visible = True
        End If
        LbLegenda(i - 1).caption = posicoes(i).Abreviatura
    Next
    DesenhaRequisicoes
End Sub

Private Sub desenhaQuebra(indice As Integer)
    Dim i As Integer
    
    totalQuebras = totalQuebras + 1
    ReDim Preserve quebras(totalQuebras)
    Set quebras(totalQuebras) = Controls.Add("SISLAB.ControloTimeLineQuebra", "quebras" & CStr(totalQuebras), Me)
    quebras(totalQuebras).Left = X1 + ((indice - 1) * largura) - QuebraHoje.DevolveInicio
    quebras(totalQuebras).Top = 0
    quebras(totalQuebras).ColocaTexto posicoes(indice).quebra
    quebras(totalQuebras).Visible = True

End Sub
Private Function DevolveAbreviatura(data As String)
    If OptData(lDia).Value = True Then
        DevolveAbreviatura = Mid(FormatDateTime(data, vbShortDate), 1, 5)
    ElseIf OptData(lHora).Value = True Then
        DevolveAbreviatura = Hour(data)
    ElseIf OptData(lMes).Value = True Then
        DevolveAbreviatura = Mid(FormatDateTime(data, vbShortDate), 4)
    ElseIf OptData(lAno).Value = True Then
        DevolveAbreviatura = Mid(FormatDateTime(data, vbShortDate), 7)
    End If
End Function

Private Function VerificaQuebra(indice As Integer) As String

    If indice > 1 Then
        If OptData(lDia).Value = True Then
            If Month(CDate(posicoes(indice).valor)) <> Month(CDate(posicoes(indice - 1).valor)) Then
                VerificaQuebra = Mid(posicoes(indice).valor, 4)
            End If
        ElseIf OptData(lMes).Value = True Then
            If Year(CDate(posicoes(indice - 1).valor)) <> Year(CDate(posicoes(indice).valor)) Then
                VerificaQuebra = Mid(posicoes(indice).valor, 7)
            End If
        ElseIf OptData(lHora).Value = True Then
            If FormatDateTime(CDate(posicoes(indice - 1).valor), vbShortDate) <> FormatDateTime(CDate(posicoes(indice).valor), vbShortDate) Then
                VerificaQuebra = Mid(posicoes(indice).valor, 1, 5)
            End If
        End If
    Else
        VerificaQuebra = ""
    End If
    
End Function

Private Sub PreencheReqDummy()
    totalReq = 5
    ReDim requisicoes(totalReq)
    requisicoes(1).ano = 2014
    requisicoes(1).data = "09-10-2014"
    requisicoes(1).hora = "15:00"
    requisicoes(1).mes = "10-2014"
    requisicoes(1).n_req = "1"
    requisicoes(1).toolTip = "asdasdasdasdads"

    requisicoes(2).ano = 2014
    requisicoes(2).data = "09-10-2014"
    requisicoes(2).hora = "16:00"
    requisicoes(2).mes = "10-2014"
    requisicoes(2).n_req = "2"
    requisicoes(2).toolTip = "asdasdasdasdadasdadasdasdads"
    
    requisicoes(3).ano = 2014
    requisicoes(3).data = "09-10-2014"
    requisicoes(3).hora = "17:00"
    requisicoes(3).mes = "10-2014"
    requisicoes(3).n_req = "3"
    requisicoes(3).toolTip = "asdasdasdasdads"

    requisicoes(4).ano = 2014
    requisicoes(4).data = "10-10-2014"
    requisicoes(4).hora = "15:00"
    requisicoes(4).mes = "10-2014"
    requisicoes(4).n_req = "4"
    requisicoes(4).toolTip = "asdasdasdasdads"

    requisicoes(5).ano = 2014
    requisicoes(5).data = "11-10-2014"
    requisicoes(5).hora = "15:00"
    requisicoes(5).mes = "10-2014"
    requisicoes(5).n_req = "5"
    requisicoes(5).toolTip = "asdasdasdasdads"

End Sub

Private Sub DesenhaRequisicoes()
    Dim i As Integer
    Dim j As Integer
    ApagaRequisicoes
    
    For i = 1 To totalReq
        If OptData(lDia).Value = True Then
            For j = 1 To totalPosicoes
                If posicoes(j).valor = requisicoes(i).data Then
                    posicoes(j).num_req = posicoes(j).num_req + 1
                    Set requisicoes(i).controlo = Controls.Add("SISLAB.ControloTooltip", "tooltip" & CStr(i), Me)
                    requisicoes(i).controlo.Left = X1 + ((j - 1) * (largura)) + (largura / 2)
                    If posicoes(j).num_req > 4 Then
                        requisicoes(i).controlo.Top = 4 * Altura
                    Else
                        requisicoes(i).controlo.Top = (posicoes(j).num_req - 1) * Altura
                    End If
                    requisicoes(i).controlo.ColocaTextoPrincipal requisicoes(i).n_req
                    requisicoes(i).controlo.ColocaTextoSecund requisicoes(i).data
                    requisicoes(i).controlo.ZOrder 0
                    requisicoes(i).controlo.Visible = True
                    
                End If
            Next j
        ElseIf OptData(lHora).Value = True Then
            For j = 1 To totalPosicoes
                If Day(posicoes(j).valor) = Day(requisicoes(i).data) And Hour(posicoes(j).valor) = Hour(requisicoes(i).hora) Then
                    posicoes(j).num_req = posicoes(j).num_req + 1
                    Set requisicoes(i).controlo = Controls.Add("SISLAB.ControloTooltip", "tooltip" & CStr(i), Me)
                    requisicoes(i).controlo.Left = X1 + ((j - 1) * (largura)) + (largura / 2)
                    If posicoes(j).num_req > 4 Then
                        requisicoes(i).controlo.Top = 4 * Altura
                    Else
                        requisicoes(i).controlo.Top = (posicoes(j).num_req - 1) * Altura
                    End If
                    requisicoes(i).controlo.ColocaTextoPrincipal requisicoes(i).n_req
                    requisicoes(i).controlo.ColocaTextoSecund requisicoes(i).data
                    requisicoes(i).controlo.ZOrder 0
                    requisicoes(i).controlo.Visible = True
                    
                End If
            Next j
        End If
    Next
End Sub

Private Sub ApagaRequisicoes()
    On Error Resume Next
    Dim i As Integer
    For i = 1 To totalReq
        Set requisicoes(i).controlo = Nothing
        Controls.Remove "tooltip" & CStr(i)
    Next
    For i = 1 To totalPosicoes
        posicoes(i).num_req = 0
    Next i
End Sub

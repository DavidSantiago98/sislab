VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form FormFactusFichPT 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFactusFichPT"
   ClientHeight    =   8535
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   15360
   Icon            =   "FormFactusFichPT.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8535
   ScaleWidth      =   15360
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtAbrir 
      Caption         =   "Abrir"
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   855
   End
   Begin VB.CommandButton BtGravar 
      Caption         =   "Gravar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1080
      TabIndex        =   2
      Top             =   0
      Width           =   975
   End
   Begin VB.TextBox EcAux 
      Height          =   375
      Left            =   480
      TabIndex        =   0
      Top             =   1800
      Visible         =   0   'False
      Width           =   1095
   End
   Begin MSFlexGridLib.MSFlexGrid FgRequisicoes 
      Height          =   10335
      Left            =   360
      TabIndex        =   1
      Top             =   720
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   18230
      _Version        =   393216
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      BorderStyle     =   0
   End
   Begin MSComDlg.CommonDialog NomeFicheiro 
      Left            =   12000
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label LbFactura 
      Caption         =   "Factura: "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2640
      TabIndex        =   6
      Top             =   120
      Width           =   3255
   End
   Begin VB.Label LbTotalRequisicoes 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10680
      TabIndex        =   5
      Top             =   1080
      Width           =   5175
   End
   Begin VB.Label LbTotalUtente 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10680
      TabIndex        =   4
      Top             =   1560
      Width           =   5175
   End
End
Attribute VB_Name = "FormFactusFichPT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Dim sNomeFicheiro As String
Dim CabAB As String
Dim gColuna As Integer

Private Type Requisicao
    Campo1 As String
    Campo2 As String
    Campo3 As String
    Campo4 As String
    Campo5 As String
    Campo6 As String
    Campo7 As String
    Campo8 As String
    Campo9 As String
    Campo10 As String
End Type

Dim EstrutRequisicao() As Requisicao
Dim TotalLinhas As Integer
Dim OLDNGuia As String

Public Sub Main()

End Sub

Private Sub BtAbrir_Click()
    Dim iFile As Integer
    Dim sStr1 As String
    Dim conteudo() As String
    Dim TotGuia As Integer
    Dim StrAux As String
    Dim i As Integer
    Dim FlgM As Integer

    NomeFicheiro.Filter = "Ficheiro Texto (*.TXT)|*.TXT"

    sNomeFicheiro = DevolveNomeFicheiro
    If sNomeFicheiro = "" Then
        Exit Sub
    End If

    BtGravar.Enabled = True
    
    FormFactusFichPT.caption = "PTACS - "
    LbFactura.caption = "Factura: "
    TotalLinhas = 0
    TotGuia = 0
    OLDNGuia = "XXXXXXXXX"
    ReDim EstrutRequisicao(TotalLinhas)
    
    FgRequisicoes.rows = 2
    FgRequisicoes.TextMatrix(1, 0) = ""
    FgRequisicoes.TextMatrix(1, 1) = ""
    FgRequisicoes.TextMatrix(1, 2) = ""
    FgRequisicoes.TextMatrix(1, 3) = ""
    FgRequisicoes.TextMatrix(1, 4) = ""
    FgRequisicoes.TextMatrix(1, 5) = ""
    FgRequisicoes.TextMatrix(1, 6) = ""
    
    iFile = FreeFile
    Open sNomeFicheiro For Input As #iFile
        Do While Not EOF(iFile)
            Line Input #1, sStr1
        
            If Mid(sStr1, 1, 2) = "HC" Then
                CabAB = sStr1
                FormFactusFichPT.caption = FormFactusFichPT.caption & Mid(sStr1, 14, 150)
            Else
                If OLDNGuia = "XXXXXXXXX" Then
                    LbFactura.caption = LbFactura.caption & Right(Mid(sStr1, 2, 18), 8)
                End If
                TotalLinhas = TotalLinhas + 1
                ReDim Preserve EstrutRequisicao(TotalLinhas)
                EstrutRequisicao(TotalLinhas).Campo1 = Mid(sStr1, 1, 1)
                EstrutRequisicao(TotalLinhas).Campo2 = Mid(sStr1, 2, 18)
                EstrutRequisicao(TotalLinhas).Campo3 = Mid(sStr1, 20, 9)
                EstrutRequisicao(TotalLinhas).Campo4 = Mid(sStr1, 29, 11)
                EstrutRequisicao(TotalLinhas).Campo5 = Mid(sStr1, 40, 6)
                EstrutRequisicao(TotalLinhas).Campo6 = Mid(sStr1, 46, 11)
                EstrutRequisicao(TotalLinhas).Campo7 = Mid(sStr1, 57, 9)
                EstrutRequisicao(TotalLinhas).Campo8 = Mid(sStr1, 66, 8)
                EstrutRequisicao(TotalLinhas).Campo9 = Mid(sStr1, 74, 6)
                EstrutRequisicao(TotalLinhas).Campo10 = Mid(sStr1, 80, 105)

                FgRequisicoes.TextMatrix(TotalLinhas, 0) = TotalLinhas
                FgRequisicoes.TextMatrix(TotalLinhas, 1) = EstrutRequisicao(TotalLinhas).Campo3
                FgRequisicoes.TextMatrix(TotalLinhas, 2) = EstrutRequisicao(TotalLinhas).Campo5
                FgRequisicoes.TextMatrix(TotalLinhas, 3) = EstrutRequisicao(TotalLinhas).Campo6
                FgRequisicoes.TextMatrix(TotalLinhas, 4) = EstrutRequisicao(TotalLinhas).Campo7
                FgRequisicoes.TextMatrix(TotalLinhas, 5) = Mid(EstrutRequisicao(TotalLinhas).Campo8, 1, 4) & "/" & Mid(EstrutRequisicao(TotalLinhas).Campo8, 5, 2) & "/" & Mid(EstrutRequisicao(TotalLinhas).Campo8, 7)
                FgRequisicoes.TextMatrix(TotalLinhas, 6) = ""
                
                StrAux = ""
                For i = 1 To Len(Trim(EstrutRequisicao(TotalLinhas).Campo3))
                If Mid(Trim(EstrutRequisicao(TotalLinhas).Campo3), i, 1) <> 0 Then
                    StrAux = Mid(Trim(EstrutRequisicao(TotalLinhas).Campo3), i)
                    i = Len(Trim(EstrutRequisicao(TotalLinhas).Campo3))
                End If
                Next
                If Len(StrAux) < 5 Then ' --- n�guia deve ter 5 ou + digitos ---
                    FgRequisicoes.TextMatrix(TotalLinhas, 6) = "G"
                End If

                FlgM = 0
                For i = 1 To Len(Trim(EstrutRequisicao(TotalLinhas).Campo5))
                If Mid(Trim(EstrutRequisicao(TotalLinhas).Campo5), i, 1) = "M" Then
                    FlgM = 1
                    i = Len(Trim(EstrutRequisicao(TotalLinhas).Campo5))
                End If
                Next
                If FlgM = 0 Then ' --- n�c�dula tem de ter a letra "M" ---
                    FgRequisicoes.TextMatrix(TotalLinhas, 6) = FgRequisicoes.TextMatrix(TotalLinhas, 6) & "C"
                End If

                FgRequisicoes.AddItem ""
                
                If OLDNGuia <> EstrutRequisicao(TotalLinhas).Campo6 Then
                    TotGuia = TotGuia + 1
                    OLDNGuia = EstrutRequisicao(TotalLinhas).Campo6
                End If
            
            End If
        Loop
    Close #iFile

    LbTotalRequisicoes.caption = TotGuia & " Guias " & TotalLinhas & " An�lises "
    FgRequisicoes.Row = 1
    FgRequisicoes.Col = 1
    FgRequisicoes_Click
    FgRequisicoes.SetFocus
    OLDNGuia = "XXXXXXXXX"
End Sub

Private Function DevolveNomeFicheiro() As String
    DevolveNomeFicheiro = ""
    
    NomeFicheiro.FilterIndex = 1
    NomeFicheiro.CancelError = True
    NomeFicheiro.InitDir = "C:\GLINTTHS\"
    NomeFicheiro.FileName = ""
    On Error Resume Next
    NomeFicheiro.DialogTitle = "Nome do ficheiro"
    NomeFicheiro.ShowOpen
    
    DevolveNomeFicheiro = NomeFicheiro.FileName

End Function

Private Sub FgRequisicoes_Click()
    Dim i As Integer
    Dim TotAna As Integer
    
    EcAux = ""
    gColuna = -1
    TotAna = 0
    If OLDNGuia <> FgRequisicoes.TextMatrix(FgRequisicoes.Row, 1) Then
        OLDNGuia = FgRequisicoes.TextMatrix(FgRequisicoes.Row, 1)
        For i = FgRequisicoes.Row To TotalLinhas
            If OLDNGuia <> FgRequisicoes.TextMatrix(i, 1) Then
                i = TotalLinhas
            Else
                TotAna = TotAna + 1
            End If
        Next
        LbTotalUtente = TotAna & " An�lises "
    End If
End Sub


Private Sub FgRequisicoes_RowColChange()
    FgRequisicoes_Click
End Sub


Private Sub FGRequisicoes_DblClick()
    Dim i As Integer
    
    gColuna = FgRequisicoes.Col
    If gColuna = 1 Then
        For i = 1 To Len(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo3))
            If Mid(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo3), i, 1) <> 0 Then
                EcAux = Mid(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo3), i)
                i = Len(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo3))
            End If
        Next
        EcAux.left = FgRequisicoes.left + FgRequisicoes.CellLeft
        EcAux.top = FgRequisicoes.top + FgRequisicoes.CellTop
        EcAux.Width = FgRequisicoes.CellWidth + 10
        EcAux.Height = FgRequisicoes.CellHeight + 20
        EcAux.Visible = True
        EcAux.SetFocus
    ElseIf gColuna = 2 Then
        For i = 1 To Len(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo5))
            If Mid(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo5), i, 1) = "M" Or _
               Mid(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo5), i, 1) = "m" Then
                EcAux = Mid(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo5), i)
                i = Len(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo5))
            End If
        Next
        EcAux.left = FgRequisicoes.left + FgRequisicoes.CellLeft
        EcAux.top = FgRequisicoes.top + FgRequisicoes.CellTop
        EcAux.Width = FgRequisicoes.CellWidth + 10
        EcAux.Height = FgRequisicoes.CellHeight + 20
        EcAux.Visible = True
        EcAux.SetFocus
    ElseIf gColuna = 3 Then
        EcAux = LTrim(EstrutRequisicao(FgRequisicoes.Row).Campo6)
        EcAux.left = FgRequisicoes.left + FgRequisicoes.CellLeft
        EcAux.top = FgRequisicoes.top + FgRequisicoes.CellTop
        EcAux.Width = FgRequisicoes.CellWidth + 10
        EcAux.Height = FgRequisicoes.CellHeight + 20
        EcAux.Visible = True
        EcAux.SetFocus
    End If
End Sub

Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        EcAux_LostFocus
    End If
End Sub

Private Sub EcAux_LostFocus()
    Dim StrAux As String
    Dim i As Integer
    Dim FlgM As Integer
    
    If gColuna = 1 Then
        EcAux = Right("000000000" & UCase(EcAux), 9)
        EstrutRequisicao(FgRequisicoes.Row).Campo3 = EcAux
        FgRequisicoes.TextMatrix(FgRequisicoes.Row, 1) = EcAux
        
        StrAux = ""
        FgRequisicoes.TextMatrix(FgRequisicoes.Row, 6) = ""
        For i = 1 To Len(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo3))
        If Mid(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo3), i, 1) <> 0 Then
            StrAux = Mid(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo3), i)
            i = Len(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo3))
        End If
        Next
        If Len(StrAux) < 5 Then ' --- n�guia deve ter 5 ou + digitos ---
            FgRequisicoes.TextMatrix(FgRequisicoes.Row, 6) = "G"
        End If

        FlgM = 0
        For i = 1 To Len(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo5))
        If Mid(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo5), i, 1) = "M" Then
            FlgM = 1
            i = Len(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo5))
        End If
        Next
        If FlgM = 0 Then ' --- n�c�dula tem de ter a letra "M" ---
            FgRequisicoes.TextMatrix(FgRequisicoes.Row, 6) = FgRequisicoes.TextMatrix(FgRequisicoes.Row, 6) & "C"
        End If
    ElseIf gColuna = 2 Then
        EcAux = Right("000000" & UCase(EcAux), 6)
        EstrutRequisicao(FgRequisicoes.Row).Campo5 = EcAux
        FgRequisicoes.TextMatrix(FgRequisicoes.Row, 2) = EcAux
                
        StrAux = ""
        FgRequisicoes.TextMatrix(FgRequisicoes.Row, 6) = ""
        For i = 1 To Len(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo3))
        If Mid(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo3), i, 1) <> 0 Then
            StrAux = Mid(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo3), i)
            i = Len(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo3))
        End If
        Next
        If Len(StrAux) < 5 Then ' --- n�guia deve ter 5 ou + digitos ---
            FgRequisicoes.TextMatrix(FgRequisicoes.Row, 6) = "G"
        End If

        FlgM = 0
        For i = 1 To Len(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo5))
        If Mid(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo5), i, 1) = "M" Then
            FlgM = 1
            i = Len(Trim(EstrutRequisicao(FgRequisicoes.Row).Campo5))
        End If
        Next
        If FlgM = 0 Then ' --- n�c�dula tem de ter a letra "M" ---
            FgRequisicoes.TextMatrix(FgRequisicoes.Row, 6) = FgRequisicoes.TextMatrix(FgRequisicoes.Row, 6) & "C"
        End If
    ElseIf gColuna = 3 Then
        EcAux = Right("           " & UCase(EcAux), 11)
        EstrutRequisicao(FgRequisicoes.Row).Campo6 = EcAux
        FgRequisicoes.TextMatrix(FgRequisicoes.Row, 3) = EcAux
    End If
    EcAux.Visible = False
    FgRequisicoes.SetFocus
End Sub

Private Sub FgRequisicoes_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        FGRequisicoes_DblClick
    End If
End Sub

Private Sub BtGravar_Click()
    Dim iFile As Integer
    Dim i As Integer
    Dim sStr1 As String
        
    On Error GoTo TrataErro

    FileCopy sNomeFicheiro, Replace(sNomeFicheiro, ".txt", ".bak")
    Kill sNomeFicheiro
    
    iFile = FreeFile
    Open sNomeFicheiro For Append As #iFile
        Print #iFile, CabAB
        For i = 1 To TotalLinhas
            sStr1 = EstrutRequisicao(i).Campo1 & EstrutRequisicao(i).Campo2 & _
                EstrutRequisicao(i).Campo3 & EstrutRequisicao(i).Campo4 & _
                EstrutRequisicao(i).Campo5 & EstrutRequisicao(i).Campo6 & _
                EstrutRequisicao(i).Campo7 & EstrutRequisicao(i).Campo8 & _
                EstrutRequisicao(i).Campo9 & EstrutRequisicao(i).Campo10
            Print #iFile, sStr1
        Next
    Close #iFile
    BG_Mensagem mediMsgBox, "Opera��o efectuada com sucesso.", vbInformation, "Gravar"
Exit Sub

TrataErro:
    BG_Mensagem mediMsgBox, "Erro ao gravar o ficheiro - " & Err.Description, vbInformation, "Gravar"

End Sub




Private Sub Form_Activate()
    
    Call EventoActivate
    
End Sub

Private Sub Form_Load()
    
    Call EventoLoad
    
End Sub



Private Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    
    Call BL_InicioProcessamento(Me, "Inicializar �cran.")
    
    'Preenche Combos
    Call PreencheValoresDefeito
    
    'Define Campos do ecran e das tabelas
    Call Inicializacoes
    
    'Define o tipo de campos
    Call DefTipoCampos
    
    Call BG_ParametrizaPermissoes_ADO(Me.Name)
    
    Estado = 1
    Call BG_StackJanelas_Push(Me)
    Call BL_FimProcessamento(Me)
    
End Sub

Private Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormFactusFichPT = Nothing

End Sub

Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub

Private Sub Inicializacoes()
        
    Me.caption = " PTACS "
    
    Me.left = 800
    Me.top = 800
    Me.Width = 12960
    Me.Height = 8610
    
    
    Set CampoDeFocus = BtAbrir
    
    NomeTabela = ""
    
    NumCampos = 0
        
End Sub
Sub DefTipoCampos()
        With FgRequisicoes
        .Cols = 7
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        
        .ColAlignment(0) = flexAlignLeftCenter
        .ColWidth(0) = 990
        .TextMatrix(0, 0) = "N� da Linha"
        
        .ColAlignment(1) = flexAlignLeftCenter
        .ColWidth(1) = 990
        .TextMatrix(0, 1) = "N� da Guia"
        
        .ColAlignment(2) = flexAlignLeftCenter
        .ColWidth(2) = 1125
        .TextMatrix(0, 2) = "N� da C�dula"
        
        .ColAlignment(3) = flexAlignLeftCenter
        .ColWidth(3) = 1470
        .TextMatrix(0, 3) = "N� do Benefici�rio"
    
        .ColAlignment(4) = flexAlignLeftCenter
        .ColWidth(4) = 1440
        .TextMatrix(0, 4) = "C�digo da An�lise"
        
        .ColAlignment(5) = flexAlignLeftCenter
        .ColWidth(5) = 1290
        .TextMatrix(0, 5) = "Data da An�lise"
    
        .ColAlignment(6) = flexAlignLeftCenter
        .ColWidth(6) = 375
        .TextMatrix(0, 6) = "Erro"
    
        .Width = .ColWidth(0) + .ColWidth(1) + .ColWidth(2) + .ColWidth(3) + .ColWidth(4) + .ColWidth(5) + .ColWidth(6) + 275
    End With


End Sub

Public Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
    
End Sub

Public Sub FuncaoInserir()
    
  ' nada

    
End Sub
Private Function ValidaCamposEc() As Integer

' nada


End Function
Public Sub FuncaoProcurar()
    
' nada

End Sub

Public Sub FuncaoModificar()
    
' nada

    
End Sub

Public Sub FuncaoRemover()
' nada

    
End Sub

Public Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoEstadoAnterior()
    
     If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Private Sub PreencheValoresDefeito()
    
' nada
End Sub

Private Sub BD_Insert()
' nada
    

End Sub

Private Sub BD_Delete()
' nada
    

    
End Sub

Private Sub BD_Update()
' nada
    

End Sub

Private Sub LimpaCampos()
' nada

    
End Sub

Private Sub PreencheCampos()
    ' nada

End Sub




VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormEstatResMicro 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   8025
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7500
   Icon            =   "FormEstatResMicro.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8025
   ScaleWidth      =   7500
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcUtente 
      Height          =   285
      Left            =   1080
      TabIndex        =   40
      Top             =   7680
      Width           =   975
   End
   Begin VB.CheckBox Flg_DescrRequis 
      Caption         =   "Descriminar Requisi��es e Utentes"
      Height          =   195
      Left            =   4560
      TabIndex        =   39
      Top             =   7320
      Width           =   2895
   End
   Begin VB.TextBox EcPesqRapEspecif 
      Height          =   285
      Left            =   4200
      TabIndex        =   22
      Top             =   8760
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapGrupo 
      Height          =   285
      Left            =   3000
      TabIndex        =   21
      Top             =   8760
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapProduto 
      Height          =   285
      Left            =   360
      TabIndex        =   18
      Top             =   8760
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapFrase 
      Height          =   285
      Left            =   1800
      TabIndex        =   17
      Top             =   8760
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Condi��es de Pesquisa"
      Height          =   6375
      Left            =   120
      TabIndex        =   7
      Top             =   960
      Width           =   7215
      Begin VB.ListBox ListaGrAna 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   52
         Top             =   1200
         Width           =   4800
      End
      Begin VB.ListBox ListaEspecif 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   36
         Top             =   2760
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaEspecif 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicro.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   35
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   2760
         Width           =   375
      End
      Begin VB.ListBox ListaProdutos 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   34
         Top             =   2040
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaProdutos 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicro.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   33
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   2040
         Width           =   375
      End
      Begin VB.ListBox ListaProven 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   32
         Top             =   3480
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaProven 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicro.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   31
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   3480
         Width           =   375
      End
      Begin VB.ListBox ListaGrPerfis 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   29
         Top             =   4920
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaGrPerfis 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicro.frx":10AA
         Style           =   1  'Graphical
         TabIndex        =   28
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Grupos de Exames"
         Top             =   4920
         Width           =   375
      End
      Begin VB.ListBox ListaFrases 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   27
         Top             =   4200
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaPerfis 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicro.frx":1634
         Style           =   1  'Graphical
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Exames"
         Top             =   5640
         Width           =   375
      End
      Begin VB.ListBox ListaPerfis 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   24
         Top             =   5640
         Width           =   4800
      End
      Begin VB.CommandButton BtPesquisaGrupo 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicro.frx":1BBE
         Style           =   1  'Graphical
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos "
         Top             =   1200
         Width           =   375
      End
      Begin VB.ComboBox EcDescrSexo 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   240
         Width           =   1455
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   4920
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   360
         Width           =   1455
      End
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   720
         Width           =   1455
      End
      Begin VB.CommandButton BtPesquisaFrase 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResMicro.frx":2148
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   4200
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "Pro&duto"
         Height          =   255
         Left            =   360
         TabIndex        =   38
         Top             =   2040
         Width           =   975
      End
      Begin VB.Label Label11 
         Caption         =   "Especifica��o"
         Height          =   255
         Left            =   360
         TabIndex        =   37
         Top             =   2760
         Width           =   1095
      End
      Begin VB.Label Label8 
         Caption         =   "&Grupos Exames"
         Height          =   255
         Left            =   360
         TabIndex        =   30
         Top             =   4920
         Width           =   1215
      End
      Begin VB.Label Label7 
         Caption         =   "&Exames"
         Height          =   255
         Left            =   360
         TabIndex        =   26
         Top             =   5760
         Width           =   1215
      End
      Begin VB.Label Label10 
         Caption         =   "&Grupo An�lises"
         Height          =   255
         Left            =   360
         TabIndex        =   20
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Se&xo"
         Height          =   195
         Left            =   360
         TabIndex        =   16
         Top             =   360
         Width           =   360
      End
      Begin VB.Label Label6 
         Caption         =   "Prio&ridade"
         Height          =   255
         Left            =   4080
         TabIndex        =   15
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   360
         TabIndex        =   14
         Top             =   720
         Width           =   735
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Left            =   360
         TabIndex        =   13
         Top             =   3525
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "&Frase"
         Height          =   255
         Left            =   360
         TabIndex        =   12
         Top             =   4200
         Width           =   1215
      End
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   7215
      Begin VB.TextBox EcHoraMin 
         Height          =   285
         Left            =   1560
         TabIndex        =   48
         Text            =   "00"
         Top             =   480
         Width           =   315
      End
      Begin VB.TextBox EcMinutoMin 
         Height          =   285
         Left            =   2040
         TabIndex        =   46
         Text            =   "00"
         Top             =   480
         Width           =   315
      End
      Begin VB.TextBox EcMinutoMax 
         Height          =   285
         Left            =   3600
         TabIndex        =   45
         Text            =   "59"
         Top             =   480
         Width           =   300
      End
      Begin VB.TextBox EcHoraMax 
         Height          =   285
         Left            =   3120
         TabIndex        =   44
         Text            =   "23"
         Top             =   480
         Width           =   300
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Dt. Valida��o"
         Height          =   255
         Index           =   1
         Left            =   4440
         TabIndex        =   43
         Top             =   480
         Width           =   1335
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Dt. Chegada"
         Height          =   255
         Index           =   0
         Left            =   4440
         TabIndex        =   42
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton BtReportLista 
         Caption         =   "Lista"
         Height          =   725
         Left            =   6480
         Picture         =   "FormEstatResMicro.frx":26D2
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   120
         Width           =   735
      End
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   3120
         TabIndex        =   4
         Top             =   120
         Width           =   1095
      End
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   1560
         TabIndex        =   3
         Top             =   120
         Width           =   1095
      End
      Begin MSComCtl2.UpDown BtHoraMin 
         Height          =   285
         Left            =   1800
         TabIndex        =   47
         Top             =   480
         Width           =   255
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         AutoBuddy       =   -1  'True
         BuddyControl    =   "Label8"
         BuddyDispid     =   196636
         OrigLeft        =   4560
         OrigTop         =   2280
         OrigRight       =   4815
         OrigBottom      =   2535
         Max             =   23
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown BtMinutoMin 
         Height          =   285
         Left            =   2310
         TabIndex        =   49
         Top             =   480
         Width           =   255
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         AutoBuddy       =   -1  'True
         BuddyControl    =   "Label11"
         BuddyDispid     =   196635
         OrigLeft        =   5040
         OrigTop         =   2280
         OrigRight       =   5295
         OrigBottom      =   2535
         Max             =   59
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown BtHoraMax 
         Height          =   285
         Left            =   3390
         TabIndex        =   50
         Top             =   480
         Width           =   255
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         BuddyControl    =   "BtPesquisaFrase"
         BuddyDispid     =   196633
         OrigLeft        =   4560
         OrigTop         =   2280
         OrigRight       =   4815
         OrigBottom      =   2535
         Max             =   23
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown BtMinutoMax 
         Height          =   285
         Left            =   3870
         TabIndex        =   51
         Top             =   480
         Width           =   255
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         BuddyControl    =   "Label1"
         BuddyDispid     =   196634
         OrigLeft        =   5040
         OrigTop         =   2280
         OrigRight       =   5295
         OrigBottom      =   2535
         Max             =   59
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   720
         TabIndex        =   6
         Top             =   120
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2850
         TabIndex        =   5
         Top             =   120
         Width           =   255
      End
   End
   Begin VB.CheckBox Flg_OrderProven 
      Caption         =   "Agrupar por Proveni�ncia"
      Height          =   255
      Left            =   2280
      TabIndex        =   1
      Top             =   7320
      Width           =   2295
   End
   Begin VB.CheckBox Flg_OrderProd 
      Caption         =   "Agrupar por Produto"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   7320
      Width           =   1815
   End
   Begin VB.Label Label12 
      Caption         =   "Utente"
      Height          =   255
      Left            =   240
      TabIndex        =   41
      Top             =   7680
      Width           =   615
   End
End
Attribute VB_Name = "FormEstatResMicro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim ListaOrigem As Control
Dim ListaDestino As Control
Public rs As ADODB.recordset
Public RSListaDestino As ADODB.recordset
Public RSListaOrigem As ADODB.recordset

Sub Preenche_Estatistica()
    
    Dim sql As String
    Dim continua As Boolean
    Dim Produtos As String
    Dim especificacoes As String
    Dim proveniencias As String
    Dim frase As String
    Dim i As Integer
    Dim grPerfis As String
    Dim perfis As String
    Dim grupos As String
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Estat�stica Resultados Microbiologia") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("EstatisticaResMicro", "Estat�stica Resultados Microbiologia", crptToPrinter)
    Else
        continua = BL_IniciaReport("EstatisticaResMicro", "Estat�stica Resultados Microbiologia", crptToWindow)
    End If
    If continua = False Then Exit Sub
        
    'Call Cria_TmpRec_Estatistica
    
    PreencheTabelaTemporaria
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
'    Report.SQLQuery = "SELECT SL_CR_ESTATRESMICRO.DESCR_MICRORG, SL_CR_ESTATMICRORG.DESCR_ANTIBIO, SL_CR_ESTATMICRORG.RES_SENSIB, SL_CR_ESTATMICRORG.DESCR_PROVEN, SL_CR_ESTATMICRORG.DESCR_PRODUTO " & _
'        " FROM SL_CR_ESTATMICRORG" & gNumeroSessao & " SL_CR_ESTATMICRORG ORDER BY DESCR_PROVEN,DESCR_PRODUTO,DESCR_MICRORG, DESCR_ANTIBIO, RES_SENSIB "
    Report.SelectionFormula = "{SL_CR_ESTATRESMICRO.nome_computador} = '" & BG_SYS_GetComputerName & "'"
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    
    If ListaFrases.ListCount > 0 Then
        frase = ""
        For i = 0 To ListaFrases.ListCount - 1
            frase = frase & ListaFrases.List(i) & "; "
        Next
        If Len(frase) > 200 Then
            frase = left(frase, 200) & "..."
        End If
        Report.formulas(2) = "Frase=" & BL_TrataStringParaBD("" & frase)
    Else
        Report.formulas(2) = "Frase=" & BL_TrataStringParaBD("Todas")

    End If
    
    If ListaProven.ListCount = 0 Then
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("Todas")
    Else
        proveniencias = ""
        For i = 0 To ListaProven.ListCount - 1
            proveniencias = proveniencias & ListaProven.List(i) & "; "
        Next
        If Len(proveniencias) > 200 Then
            proveniencias = left(proveniencias, 200) & "..."
        End If
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("" & proveniencias)
    End If
    
    If ListaProdutos.ListCount = 0 Then
        Report.formulas(4) = "Produto=" & BL_TrataStringParaBD("Todos")
    Else
        Produtos = ""
        For i = 0 To ListaProdutos.ListCount - 1
           Produtos = Produtos & ListaProdutos.List(i) & "; "
        Next
        If Len(Produtos) > 200 Then
            Produtos = left(Produtos, 200) & "..."
        End If
        Report.formulas(4) = "Produto=" & BL_TrataStringParaBD("" & Produtos)
    End If
    If CbUrgencia.ListIndex = -1 Then
        Report.formulas(5) = "Urgencia=" & BL_TrataStringParaBD("-")
    Else
        Report.formulas(5) = "Urgencia=" & BL_TrataStringParaBD("" & CbUrgencia.Text)
    End If
    If CbSituacao.ListIndex = -1 Then
        Report.formulas(6) = "Situacao=" & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(6) = "Situacao=" & BL_TrataStringParaBD("" & CbSituacao.Text)
    End If
    If EcDescrSexo.ListIndex = -1 Then
        Report.formulas(7) = "Sexo=" & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(7) = "Sexo=" & BL_TrataStringParaBD("" & EcDescrSexo.Text)
    End If
    If Flg_OrderProd.value = 1 Then
        Report.formulas(8) = "OrderProd=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(8) = "OrderProd=" & BL_TrataStringParaBD("N")
    End If
    If Flg_OrderProven.value = 1 Then
        Report.formulas(9) = "OrderProven=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(9) = "OrderProven=" & BL_TrataStringParaBD("N")
    End If
    
    If ListaEspecif.ListCount = 0 Then
        Report.formulas(10) = "Especif=" & BL_TrataStringParaBD("Todas")
    Else
        especificacoes = ""
        For i = 0 To ListaEspecif.ListCount - 1
            especificacoes = especificacoes & ListaEspecif.List(i) & "; "
        Next
        If Len(especificacoes) > 200 Then
            especificacoes = left(especificacoes, 200) & "..."
        End If
        Report.formulas(10) = "Especif=" & BL_TrataStringParaBD("" & especificacoes)
    End If
    
    If ListaGrPerfis.ListCount = 0 Then
        Report.formulas(11) = "GrPerfis=" & BL_TrataStringParaBD("Todos")
    Else
        grPerfis = ""
        For i = 0 To ListaGrPerfis.ListCount - 1
            grPerfis = grPerfis & ListaGrPerfis.List(i) & "; "
        Next
        If Len(grPerfis) > 200 Then
            grPerfis = left(grPerfis, 200) & "..."
        End If
        Report.formulas(11) = "GrPerfis=" & BL_TrataStringParaBD("" & grPerfis)
    End If
    
    If ListaPerfis.ListCount = 0 Then
        Report.formulas(12) = "Perfis=" & BL_TrataStringParaBD("Todos")
    Else
        perfis = ""
        For i = 0 To ListaPerfis.ListCount - 1
            perfis = perfis & ListaPerfis.List(i) & "; "
        Next
        If Len(perfis) > 200 Then
            perfis = left(perfis, 200) & "..."
        End If
        Report.formulas(12) = "Perfis=" & BL_TrataStringParaBD("" & perfis)
    End If
    
    If Flg_DescrRequis.value = 1 Then
        Report.formulas(13) = "DescrRequis=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(13) = "DescrRequis=" & BL_TrataStringParaBD("N")
    End If
    If ListaGrAna.ListCount > 0 Then
        For i = 0 To ListaGrAna.ListCount - 1
            grupos = grupos & ListaGrAna.List(i) & "; "
        Next
        If Len(grupos) > 200 Then
            grupos = left(grupos, 200) & "..."
        End If
        Report.formulas(14) = "GrAnalises=" & BL_TrataStringParaBD(grupos)
    Else
        Report.formulas(14) = "GrAnalises=" & BL_TrataStringParaBD("Todos")
    End If
    
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
 
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    sql = "delete from sl_cr_estatresmicro where nome_computador = '" & BG_SYS_GetComputerName & "'"
    BG_ExecutaQuery_ADO sql
    
End Sub

'Sub Cria_TmpRec_Estatistica()
'
'    Dim TmpRec(8) As DefTable
'
'    TmpRec(1).NomeCampo = "DESCR_MICRORG"
'    TmpRec(1).Tipo = "STRING"
'    TmpRec(1).Tamanho = 60
'
'    TmpRec(2).NomeCampo = "DESCR_ANTIBIO"
'    TmpRec(2).Tipo = "STRING"
'    TmpRec(2).Tamanho = 60
'
'    TmpRec(3).NomeCampo = "RES_SENSIB"
'    TmpRec(3).Tipo = "STRING"
'    TmpRec(3).Tamanho = 1
'
'    TmpRec(4).NomeCampo = "N_REQ"
'    TmpRec(4).Tipo = "INTEGER"
'    TmpRec(4).Tamanho = 9
'
'    TmpRec(5).NomeCampo = "SEQ_REALIZA"
'    TmpRec(5).Tipo = "INTEGER"
'    TmpRec(5).Tamanho = 20
'
'    TmpRec(6).NomeCampo = "N_RES"
'    TmpRec(6).Tipo = "INTEGER"
'    TmpRec(6).Tamanho = 1
'
'    TmpRec(7).NomeCampo = "DESCR_PROVEN"
'    TmpRec(7).Tipo = "STRING"
'    TmpRec(7).Tamanho = 40
'
'    TmpRec(8).NomeCampo = "DESCR_PRODUTO"
'    TmpRec(8).Tipo = "STRING"
'    TmpRec(8).Tamanho = 40
'
'    Call BL_CriaTabela("SL_CR_ESTATMICRORG" & gNumeroSessao, TmpRec)
'
'End Sub

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Private Sub BtHoraMin_DownClick()
    If CLng(EcHoraMin.Text) > 0 Then EcHoraMin.Text = Right("00" & CLng(EcHoraMin.Text) - 1, 2)
End Sub

Private Sub BtHoraMin_UpClick()
    If CLng(EcHoraMin.Text) < 23 Then EcHoraMin.Text = Right("00" & CLng(EcHoraMin.Text) + 1, 2)
End Sub

Private Sub BtHoraMax_DownClick()
    If CLng(EcHoraMax.Text) > 0 Then EcHoraMax.Text = Right("00" & CLng(EcHoraMax.Text) - 1, 2)
End Sub

Private Sub BtHoraMax_UpClick()
    If CLng(EcHoraMax.Text) < 23 Then EcHoraMax.Text = Right("00" & CLng(EcHoraMax.Text) + 1, 2)
End Sub

Private Sub BtMinutoMin_downclick()
    If CLng(EcMinutoMin.Text) > 0 Then EcMinutoMin.Text = Right("00" & CLng(EcMinutoMin.Text) - 1, 2)
End Sub

Private Sub BtMinutoMin_upclick()
    If CLng(EcMinutoMin.Text) < 59 Then EcMinutoMin.Text = Right("00" & CLng(EcMinutoMin.Text) + 1, 2)
End Sub
Private Sub BtMinutoMax_downclick()
    If CLng(EcMinutoMax.Text) > 0 Then EcMinutoMax.Text = Right("00" & CLng(EcMinutoMax.Text) - 1, 2)
End Sub

Private Sub BtMinutoMax_upclick()
    If CLng(EcMinutoMax.Text) < 59 Then EcMinutoMax.Text = Right("00" & CLng(EcMinutoMax.Text) + 1, 2)
End Sub

Private Sub BtPesquisaFrase_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_frase"
    CamposEcran(1) = "cod_frase"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_frase"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_dicionario "
    CWhere = ""
    CampoPesquisa = "descr_frase"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_frase ", _
                                                                           " Frases")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaFrases.ListCount = 0 Then
                ListaFrases.AddItem BL_SelCodigo("sl_dicionario", "descr_frase", "seq_frase", resultados(i))
                ListaFrases.ItemData(0) = resultados(i)
            Else
                ListaFrases.AddItem BL_SelCodigo("sl_dicionario", "descr_frase", "seq_frase", resultados(i))
                ListaFrases.ItemData(ListaFrases.NewIndex) = resultados(i)
            End If
        Next i
        

    End If

End Sub

Private Sub BtPesquisaPerfis_Click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    
    If ListaGrPerfis.ListCount > 0 Then
        CFrom = "sl_perfis, sl_gr_perfis "
    Else
        CFrom = "sl_perfis "
    End If
    If ListaGrPerfis.ListCount > 0 Then
        CWhere = ""
        
'        If EcCodGrupo.Text <> "" Then
'            CWhere = CWhere & " and gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.Text)
'        End If
        
        CWhere = CWhere & " flg_activo = 1 AND sl_gr_perfis.cod_gr_perfis = sl_perfis.cod_gr_perfis "
        CWhere = CWhere & " AND sl_gr_perfis.seq_gr_perfis IN ("
        For i = 0 To ListaGrPerfis.ListCount - 1
            CWhere = CWhere & ListaGrPerfis.ItemData(i) & ", "
        Next i
        CWhere = Mid(CWhere, 1, Len(CWhere) - 2) & ") "
    Else
        CWhere = " flg_activo = 1  "
'        If EcCodGrupo.Text <> "" Then
'            CWhere = CWhere & " and gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.Text)
'        End If
   End If
    
    ' se tem grupo de analises preenchido
    If ListaGrAna.ListCount > 0 Then
        CWhere = CWhere & " AND sl_perfis.gr_ana IN ( "
        For i = 0 To ListaGrAna.ListCount - 1
            CWhere = CWhere & ListaGrAna.ItemData(i) & ", "
        Next i
        CWhere = Mid(CWhere, 1, Len(CWhere) - 2) & ") "
    End If
    CampoPesquisa = "descr_perfis"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_perfis ", _
                                                                           " Exames")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaPerfis.ListCount = 0 Then
                ListaPerfis.AddItem BL_SelCodigo("sl_perfis", "descr_perfis", "seq_perfis", resultados(i))
                ListaPerfis.ItemData(0) = resultados(i)
            Else
                ListaPerfis.AddItem BL_SelCodigo("sl_perfis", "descr_perfis", "seq_perfis", resultados(i))
                ListaPerfis.ItemData(ListaPerfis.NewIndex) = resultados(i)
            End If
        Next i
        

    End If
End Sub
Private Sub BtPesquisaGrPerfis_click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_gr_perfis"
    CamposEcran(1) = "cod_gr_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_gr_perfis"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_gr_perfis "
    CWhere = ""
    CampoPesquisa = "descr_gr_perfis"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_gr_perfis ", _
                                                                           " Grupos de Exames")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaGrPerfis.ListCount = 0 Then
                ListaGrPerfis.AddItem BL_SelCodigo("sl_gr_perfis", "descr_gr_perfis", "seq_gr_perfis", resultados(i))
                ListaGrPerfis.ItemData(0) = resultados(i)
            Else
                ListaGrPerfis.AddItem BL_SelCodigo("sl_gr_perfis", "descr_gr_perfis", "seq_gr_perfis", resultados(i))
                ListaGrPerfis.ItemData(ListaGrPerfis.NewIndex) = resultados(i)
            End If
        Next i
        

    End If
End Sub


Private Sub BtPesquisaProduto_click()
    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
                        "descr_produto", "seq_produto", _
                         EcPesqRapProduto
End Sub




Private Sub BtPesquisaProdutos_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_produto "
    CWhere = ""
    CampoPesquisa = "descr_produto"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_produto ", _
                                                                           " Produtos")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaProdutos.ListCount = 0 Then
                ListaProdutos.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                ListaProdutos.ItemData(0) = resultados(i)
            Else
                ListaProdutos.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", resultados(i))
                ListaProdutos.ItemData(ListaProdutos.NewIndex) = resultados(i)
            End If
        Next i
    End If

End Sub


Private Sub BtReportLista_Click()
    'MsgBox "Queria...mas s� amanh�!! " & vbCrLf & "Para j� tem que usar o velho bot�o..."
    Call Preenche_Estatistica
End Sub

Private Sub CbUrgencia_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1
    
End Sub

Private Sub EcCodFrase_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub
      
Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub



Private Sub EcHoraMin_Validate(Cancel As Boolean)
    If Not IsNumeric(EcHoraMin.Text) Then
        EcHoraMin.Text = "00"
        Exit Sub
    End If
    If CLng(EcHoraMin.Text) < 0 Or CLng(EcHoraMin.Text) > 23 Then
        EcHoraMin.Text = "00"
        Exit Sub
    End If
    EcHoraMin.Text = Right("00" & EcHoraMin.Text, 2)
End Sub

Private Sub EcHoraMax_Validate(Cancel As Boolean)
    If Not IsNumeric(EcHoraMax.Text) Then
        EcHoraMax.Text = "00"
        Exit Sub
    End If
    If CLng(EcHoraMax.Text) < 0 Or CLng(EcHoraMax.Text) > 23 Then
        EcHoraMax.Text = "00"
        Exit Sub
    End If
    EcHoraMax.Text = Right("00" & EcHoraMax.Text, 2)
End Sub

Private Sub EcMinutoMin_Validate(Cancel As Boolean)
    If Not IsNumeric(EcMinutoMin.Text) Then
        EcMinutoMin.Text = "00"
        Exit Sub
    End If
    If CLng(EcMinutoMin.Text) < 0 Or CLng(EcHoraMin.Text) > 59 Then
        EcMinutoMin.Text = "00"
        Exit Sub
    End If
    EcMinutoMin.Text = Right("00" & EcMinutoMin.Text, 2)
    
End Sub

Private Sub EcMinutoMax_Validate(Cancel As Boolean)
    If Not IsNumeric(EcMinutoMax.Text) Then
        EcMinutoMax.Text = "00"
        Exit Sub
    End If
    If CLng(EcMinutoMax.Text) < 0 Or CLng(EcMinutoMax.Text) > 59 Then
        EcMinutoMax.Text = "00"
        Exit Sub
    End If
    EcMinutoMax.Text = Right("00" & EcMinutoMax.Text, 2)
End Sub


Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Estat�stica de Resultados da Microbiologia"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 7590
    Me.Height = 8460 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcDtIni
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de Microrganismos")
    
    Set FormEstatMicrorg = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    'CbAnalises.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    CbSituacao.ListIndex = mediComboValorNull
    EcDescrSexo.ListIndex = mediComboValorNull
    'CbGrupo.ListIndex = mediComboValorNull
    ListaProven.Clear
    ListaProdutos.Clear
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    Flg_OrderProd.value = 0
    Flg_OrderProven.value = 0
    ListaGrAna.Clear
    ListaEspecif.Clear
    ListaFrases.Clear
    ListaPerfis.Clear
    ListaGrPerfis.Clear
    Option1(0).value = True
End Sub

Sub DefTipoCampos()


    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
'    EcNumFolhaTrab.MaxLength = 10
    Option1(0).value = True
        
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao

    'Preenche Combo Urgencia
    CbUrgencia.AddItem "Normal"
    CbUrgencia.AddItem "Urgente"
    
    If gCodGrupoMicrobiologia > mediComboValorNull Then
        ListaGrAna.AddItem BL_SelCodigo("SL_GR_ANA", "DESCR_GR_ANA", "COD_GR_ANA", gCodGrupoMicrobiologia)
        ListaGrAna.ItemData(ListaGrAna.NewIndex) = gCodGrupoMicrobiologia
    End If

    
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub


Sub PreencheTabelaTemporaria()
    Dim sql As String
    Dim i As Integer
    Dim SqlIns As String
    Dim SqlSel As String
    Dim SqlSel2 As String
    Dim SqlSel3 As String
    Dim SqlC As String
    Dim SqlO As String
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
    
    sql = "delete from sl_cr_estatresmicro where nome_computador = '" & BG_SYS_GetComputerName & "'"
    BG_ExecutaQuery_ADO sql
    
    SqlIns = "INSERT INTO SL_CR_ESTATRESMICRO ("
    
    If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
        SqlIns = SqlIns & " DESCR_PRODUTO,DESCR_PROVEN,"
    ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
        SqlIns = SqlIns & " DESCR_PRODUTO,"
    ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
        SqlIns = SqlIns & " DESCR_PROVEN,"
    End If
    
    SqlIns = SqlIns & " NOME_COMPUTADOR,N_REQ,COD_PERFIL,DESCR_PERFIS,COD_ANA_C,DESCR_ANA_C,COD_ANA_S,DESCR_ANA_S,COD_FRASE,DESCR_FRASE, COD_GR_PERFIS,DESCR_GR_PERFIS, NOME_UTE, UTENTE) "
    
    SqlSel = " select "
    
    If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
        SqlSel = SqlSel & " DESCR_PRODUTO,DESCR_PROVEN,"
    ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
        SqlSel = SqlSel & " DESCR_PRODUTO,"
    ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
        SqlSel = SqlSel & " DESCR_PROVEN,"
    End If
    
    SqlSel2 = SqlSel2 & " '" & BG_SYS_GetComputerName & "', sl_requis.n_req,sl_realiza.cod_perfil,descr_perfis,sl_realiza.cod_ana_c,descr_ana_c,sl_realiza.cod_ana_s,descr_ana_s,sl_res_frase.cod_frase,descr_frase, sl_gr_perfis.cod_gr_perfis, sl_gr_perfis.descr_gr_perfis, " & tabela_aux & ".nome_ute, " & tabela_aux & ".utente " & _
                " From sl_requis, sl_realiza,sl_res_alfan, sl_res_frase, sl_dicionario, sl_proven, sl_req_prod, sl_produto, " & tabela_aux & ", sl_ana_s, sl_ana_c, sl_perfis, sl_gr_perfis, sl_gr_ana, sl_especif ,slv_analises " & _
                " where sl_requis.n_req = sl_realiza.n_req and " & _
                " sl_requis.seq_utente = " & tabela_aux & ".seq_utente and " & _
                " sl_requis.cod_proven = sl_proven.cod_proven(+) and " & _
                " sl_requis.n_req = sl_req_prod.n_req(+) and " & _
                " sl_req_prod.cod_prod = sl_produto.cod_produto(+) and " & _
                " sl_realiza.seq_realiza = sl_res_frase.seq_realiza(+) and " & _
                " sl_realiza.seq_realiza = sl_res_alfan.seq_realiza(+) and " & _
                " sl_realiza.cod_perfil = sl_perfis.cod_perfis(+) and " & _
                " sl_realiza.cod_ana_c = sl_ana_c.cod_ana_c(+) and " & _
                " sl_realiza.cod_ana_s = sl_ana_s.cod_ana_s(+) and " & _
                " sl_res_frase.cod_frase = sl_dicionario.cod_frase(+) and " & _
                " slv_analises.gr_ana = sl_gr_ana.cod_gr_ana and " & _
                " sl_perfis.cod_gr_perfis = sl_gr_perfis.cod_gr_perfis(+) and " & _
                " sl_realiza.seq_realiza not in (Select seq_realiza from sl_res_alfan ) AND " & _
                " sl_realiza.seq_realiza not in (Select seq_realiza from sl_Res_micro where flg_imp = 'N'  and seq_realiza not in (Select seq_realiza from sl_res_micro where flg_imp = 'S')) AND " & _
                " sl_req_prod.cod_especif = sl_especif.cod_especif (+) and slv_analises.cod_ana = sl_realiza.cod_agrup and sl_realiza.cod_ana_s <> 'S99999' "
                
    SqlSel2 = SqlSel2 & " AND sl_requis.n_req NOT IN (SELECT n_req FROM sl_marcacoes) "
    
    
    
                
    'verifica os campos preenchidos
    
    'se sexo preenchido
    If (EcDescrSexo.ListIndex <> -1) Then
        SqlC = SqlC & " AND " & tabela_aux & ".sexo_ute = " & EcDescrSexo.ListIndex
    End If
    
    'se especificacao preenchida
    If ListaEspecif.ListCount > 0 Then
        SqlC = SqlC & " AND sl_especif.seq_especif IN ( "
        For i = 0 To ListaEspecif.ListCount - 1
            SqlC = SqlC & ListaEspecif.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    ' se tem grupo de analises preenchido
    If ListaGrAna.ListCount > 0 Then
        SqlC = SqlC & " AND sl_gr_ana.cod_gr_ana IN ( "
        For i = 0 To ListaGrAna.ListCount - 1
            SqlC = SqlC & ListaGrAna.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    SqlC = SqlC & " AND slv_analises.gr_ana = sl_gr_ana.cod_gr_ana "
        
        
    
    'Data preenchida
    If Option1(0).value = True Then
        SqlC = SqlC & " AND  ( (sl_requis.dt_chega = " & BL_TrataDataParaBD(EcDtIni) & " AND sl_Requis.hr_chega >= " & BL_TrataStringParaBD(EcHoraMin.Text & ":" & EcMinutoMin.Text) & ")"
        SqlC = SqlC & " OR sl_requis.dt_chega > " & BL_TrataDataParaBD(EcDtIni) & ")"
        SqlC = SqlC & " AND  ( (sl_requis.dt_chega = " & BL_TrataDataParaBD(EcDtFim) & " AND sl_Requis.hr_chega <= " & BL_TrataStringParaBD(EcHoraMax.Text & ":" & EcMinutoMax.Text) & ")"
        SqlC = SqlC & " OR sl_requis.dt_chega < " & BL_TrataDataParaBD(EcDtFim) & ")"
    Else
        SqlC = SqlC & " AND  ( (sl_realiza.dt_val = " & BL_TrataDataParaBD(EcDtIni) & " AND sl_realiza.hr_val >= " & BL_TrataStringParaBD(EcHoraMin.Text & ":" & EcMinutoMin.Text) & ")"
        SqlC = SqlC & " OR sl_realiza.dt_val > " & BL_TrataDataParaBD(EcDtIni) & ")"
        SqlC = SqlC & " AND  ( (sl_realiza.dt_val = " & BL_TrataDataParaBD(EcDtFim) & " AND sl_realiza.hr_val <= " & BL_TrataStringParaBD(EcHoraMax.Text & ":" & EcMinutoMax.Text) & ")"
        SqlC = SqlC & " OR sl_realiza.dt_val < " & BL_TrataDataParaBD(EcDtFim) & ")"
        
    End If
    
    'Situa��o preenchida?
    If (CbSituacao.ListIndex <> -1) Then
        SqlC = SqlC & " AND sl_requis.T_sit= " & CbSituacao.ListIndex
    End If
    
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> -1) Then
        SqlC = SqlC & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(left(CbUrgencia.Text, 1))
    End If
    
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    If ListaProven.ListCount > 0 Then
        SqlC = SqlC & " AND sl_proven.seq_proven IN ("
        For i = 0 To ListaProven.ListCount - 1
            SqlC = SqlC & ListaProven.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    'C�digo do produto preenchido?
    If ListaProdutos.ListCount > 0 Then
        SqlC = SqlC & " AND sl_produto.seq_produto IN ("
        For i = 0 To ListaProdutos.ListCount - 1
           SqlC = SqlC & ListaProdutos.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    'C�digo da frase preenchida?
    If ListaFrases.ListCount > 0 Then
        SqlC = SqlC & " AND sl_dicionario.seq_frase IN ("
        For i = 0 To ListaFrases.ListCount - 1
            SqlC = SqlC & ListaFrases.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    If ListaPerfis.ListCount > 0 Then
        SqlC = SqlC & " AND sl_perfis.seq_perfis IN ("
        For i = 0 To ListaPerfis.ListCount - 1
            SqlC = SqlC & ListaPerfis.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    If ListaGrPerfis.ListCount > 0 Then
        SqlC = SqlC & " AND sl_gr_perfis.seq_gr_perfis IN ("
        For i = 0 To ListaGrPerfis.ListCount - 1
            SqlC = SqlC & ListaGrPerfis.ItemData(i) & ", "
        Next i
        SqlC = Mid(SqlC, 1, Len(SqlC) - 2) & ") "
    End If
    
    If Trim(EcUtente) <> "" Then
        SqlC = SqlC & " AND " & tabela_aux & ".utente = " & BL_TrataStringParaBD(EcUtente)
    End If
    
    If gSGBD = gOracle Then
        SqlO = " ORDER BY  ord_ana, cod_agrup, cod_perfil, "
        SqlO = SqlO & " ord_ana_perf, cod_ana_c, ord_ana_compl, cod_ana_s "
    ElseIf gSGBD = gSqlServer Then
        SqlO = " ORDER BY  ord_ana, cod_agrup, cod_perfil, "
        SqlO = SqlO & " ord_ana_perf, sl_ana_c.cod_ana_c, ord_ana_compl, sl_ana_s.cod_ana_s "
    End If
    
    sql = SqlIns & SqlSel & SqlSel2 & SqlC & SqlO
        
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    
End Sub

Private Sub BtPesquisaGrupo_Click()
    PA_PesquisaGrAnaMultiSel ListaGrAna
End Sub


Private Sub ListaGrAna_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaGrAna.ListCount > 0 Then     'Delete
        ListaGrAna.RemoveItem (ListaGrAna.ListIndex)
    End If
End Sub

Private Sub Listaperfis_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaPerfis.ListCount > 0 Then     'Delete
        ListaPerfis.RemoveItem (ListaPerfis.ListIndex)
    End If
End Sub
Private Sub Listafrases_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaFrases.ListCount > 0 Then     'Delete
        ListaFrases.RemoveItem (ListaFrases.ListIndex)
    End If
End Sub
Private Sub ListaGrPerfis_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaGrPerfis.ListCount > 0 Then     'Delete
        ListaGrPerfis.RemoveItem (ListaGrPerfis.ListIndex)
    End If
End Sub

Private Sub BtPesquisaProven_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven "
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Provini�ncias")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaProven.ListCount = 0 Then
                ListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                ListaProven.ItemData(0) = resultados(i)
            Else
                ListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", resultados(i))
                ListaProven.ItemData(ListaProven.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub

Private Sub BtPesquisaEspecif_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_especif"
    CamposEcran(1) = "cod_especif"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_especif"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_especif "
    CWhere = ""
    CampoPesquisa = "descr_especif"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_especif ", _
                                                                           " Especifica��es")

    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaEspecif.ListCount = 0 Then
                ListaEspecif.AddItem BL_SelCodigo("sl_especif", "descr_especif", "seq_especif", resultados(i))
                ListaEspecif.ItemData(0) = resultados(i)
            Else
                ListaEspecif.AddItem BL_SelCodigo("sl_especif", "descr_especif", "seq_especif", resultados(i))
                ListaEspecif.ItemData(ListaEspecif.NewIndex) = resultados(i)
            End If
        Next i
    End If
End Sub


Private Sub ListaProven_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaProven.ListCount > 0 Then     'Delete
        ListaProven.RemoveItem (ListaProven.ListIndex)
    End If
End Sub
Private Sub Listaespecif_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaEspecif.ListCount > 0 Then     'Delete
        ListaEspecif.RemoveItem (ListaEspecif.ListIndex)
    End If
End Sub
Private Sub ListaProdutos_keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaProdutos.ListCount > 0 Then     'Delete
        ListaProdutos.RemoveItem (ListaProdutos.ListIndex)
    End If
End Sub



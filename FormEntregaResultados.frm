VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormEntregaResultados 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormEntregaResultados"
   ClientHeight    =   6720
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11730
   Icon            =   "FormEntregaResultados.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   11730
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtNotas 
      Height          =   600
      Left            =   7800
      Picture         =   "FormEntregaResultados.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   28
      ToolTipText     =   "Notas"
      Top             =   5880
      Width           =   735
   End
   Begin VB.CommandButton BtNotasVRM 
      Height          =   600
      Left            =   7800
      Picture         =   "FormEntregaResultados.frx":0776
      Style           =   1  'Graphical
      TabIndex        =   27
      ToolTipText     =   "Notas"
      Top             =   5880
      Width           =   735
   End
   Begin VB.CommandButton BtGestReq 
      Enabled         =   0   'False
      Height          =   600
      Left            =   8760
      Picture         =   "FormEntregaResultados.frx":0EE0
      Style           =   1  'Graphical
      TabIndex        =   23
      ToolTipText     =   "Requisi��o"
      Top             =   5880
      Width           =   615
   End
   Begin VB.TextBox EcSeqUtente 
      Height          =   285
      Left            =   1200
      TabIndex        =   20
      Top             =   7800
      Width           =   855
   End
   Begin VB.TextBox EcHrEntrega 
      Height          =   285
      Left            =   1200
      TabIndex        =   16
      Top             =   7080
      Width           =   855
   End
   Begin VB.TextBox EcUserEntrega 
      Height          =   285
      Left            =   1200
      TabIndex        =   15
      Top             =   7440
      Width           =   855
   End
   Begin VB.TextBox EcDtEntrega 
      Height          =   285
      Left            =   1200
      TabIndex        =   14
      Top             =   6720
      Width           =   855
   End
   Begin VB.CommandButton BtConfirmar 
      Height          =   615
      Left            =   3960
      Picture         =   "FormEntregaResultados.frx":1BAA
      Style           =   1  'Graphical
      TabIndex        =   12
      ToolTipText     =   "Confirmar"
      Top             =   5880
      Width           =   2055
   End
   Begin VB.Frame Frame1 
      Height          =   1575
      Left            =   240
      TabIndex        =   2
      Top             =   0
      Width           =   11295
      Begin VB.TextBox EcNumReq 
         Height          =   285
         Left            =   960
         TabIndex        =   0
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox EcUtente 
         Height          =   285
         Left            =   2520
         TabIndex        =   11
         Top             =   1080
         Width           =   1335
      End
      Begin VB.ComboBox CbTipoUtente 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   1080
         Width           =   1455
      End
      Begin VB.TextBox EcResponsavel 
         Height          =   285
         Left            =   4800
         TabIndex        =   8
         Top             =   240
         Width           =   2895
      End
      Begin VB.TextBox EcNumBenef 
         Height          =   285
         Left            =   5760
         TabIndex        =   6
         Top             =   1080
         Width           =   1935
      End
      Begin VB.TextBox EcNome 
         Height          =   285
         Left            =   960
         TabIndex        =   4
         Top             =   600
         Width           =   6735
      End
      Begin VB.Label Label1 
         Caption         =   "Requisi��o"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Utente"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   9
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Respons�vel:"
         Height          =   255
         Index           =   2
         Left            =   3720
         TabIndex        =   7
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "N� Benef."
         Height          =   255
         Index           =   1
         Left            =   4680
         TabIndex        =   5
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Nome"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   600
         Width           =   495
      End
   End
   Begin MSFlexGridLib.MSFlexGrid FgReqAna 
      Height          =   4095
      Left            =   240
      TabIndex        =   1
      Top             =   1680
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   7223
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
   End
   Begin VB.Label Label4 
      Caption         =   "Total"
      Height          =   255
      Left            =   360
      TabIndex        =   26
      Top             =   6360
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Requisi��o"
      Height          =   255
      Left            =   360
      TabIndex        =   25
      Top             =   5880
      Width           =   855
   End
   Begin VB.Label Lbtotal 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   1320
      TabIndex        =   24
      Top             =   6360
      Width           =   1335
   End
   Begin VB.Label LbDivida 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   1320
      TabIndex        =   22
      Top             =   5880
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "EcSeqUtente"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   21
      Top             =   7800
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcUserEntrega"
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   19
      Top             =   7440
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "EcHrEntrega"
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   18
      Top             =   7080
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "EcDtEntrega"
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   6720
      Width           =   855
   End
End
Attribute VB_Name = "FormEntregaResultados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

' --------------------------------------
' Constantes de cor
' --------------------------------------
Private Const CC_GRAY = &HE0E0E0
Private Const CC_FORMGRAY = &H8000000F
Private Const CC_LIGHTBLUE = &HFFD3A4
Private Const CC_LIGHTYELLOW = &H80000018

Const NUM_LINHAS = 14


Dim TrataReq As Boolean
Dim TrataAna As Boolean

' --------------------------------------
' Variaveis para acesso a BD '
' --------------------------------------
Dim NumCampos As Integer
Dim CamposBD() As String
Dim CamposEc() As Object
Dim ChaveBD As String
Dim ChaveEc As Object
Dim NomeTabela As String
Dim CriterioTabela As String
Public rs As ADODB.recordset

' --------------------------------------
' Estrutura das analises
' --------------------------------------
Private Type ana
    n_req As String
    cod_agrup As String
    descr_agrup As String
    Estado_Ana As String
    
    user_entrega As String
    dt_entrega As String
    hr_entrega As String
    resp_entrega As String
End Type

' --------------------------------------
' Estrutura da requisi��o
' --------------------------------------
Private Type requisicao
    seq_utente As String
    n_req As String
    estado_req As String
    t_utente As String
    Utente As String
    dt_chega As String
    nome_ute As String
    num_benef As String
    cod_efr As String
    cod_sala As String
    
    user_entrega As String
    dt_entrega As String
    hr_entrega As String
    resp_entrega As String
    valorDeve As Double
    
    estrutAnalises() As ana
    TotalEstrutAnalises As Long
End Type

Const ColREQ_NReq = 0
Const ColREQ_Estado = 1
Const ColREQ_DtChega = 2
Const ColREQ_DtEntrega = 4
Const ColREQ_HrEntrega = 5
Const ColREQ_CodEFR = 7
Const ColREQ_UserEntrega = 6
Const ColREQ_RespEntrega = 3


Const ColANA_NReq = 0
Const ColANA_Analise = 1
Const ColANA_Estado = 2
Const ColANA_DtEntrega = 3
Const ColANA_HrEntrega = 4
Const ColANA_UserEntrega = 5
' --------------------------------------
' Vector de requisicoes
' --------------------------------------
Dim estrutRequis() As requisicao
Dim totalEstrutRequis As Long

Dim linhaActual As Long


Const Verde = &H808000
Const VerdeClaro = &HC0FFC0
Dim CampoDeFocus As Object

'NELSONPSILVA Glintt-HS-18011 09.02.2018
Dim tabela_aux As String

Private Sub BtConfirmar_Click()
    Dim i  As Long
    Dim FLG_ReqParcialmenteEntregue As Boolean
    Dim FLG_Entrega As Boolean
    Dim sSql As String
    Dim mens As String
    
    On Error GoTo TrataErro
    
    FLG_ReqParcialmenteEntregue = False
    FLG_Entrega = False
    
    If EcNumReq = "" Then Exit Sub
    If Trim(EcResponsavel) = "" Then
        mens = " Ter� de indicar o Respons�vel!"
        BG_Mensagem mediMsgBox, mens, vbOKOnly + vbInformation, "Entrega de Resultados"
        Exit Sub
    End If
    If estrutRequis(linhaActual).estado_req <> "Hist�rico" And estrutRequis(linhaActual).estado_req <> "Impress�o parcial" And estrutRequis(linhaActual).estado_req <> "Todas impressas" Then
        mens = "A Requisi��o n�o est� impressa. Ter� de imprimir para poder entregar."
        BG_Mensagem mediMsgBox, mens, vbOKOnly + vbInformation, "Entrega de Resultados"
            
        Exit Sub
    End If
    
    
    ' --------------------------------------------
    ' REQUISICAO DE OUTRO POSTO
    ' --------------------------------------------
    If gTipoInstituicao = gTipoInstituicaoPrivada Then
    
        If BL_VerificaPosto(CInt(gCodSala)) = True And gCodSala <> BL_HandleNull(estrutRequis(linhaActual).cod_sala, "0") Then
            gMsgTitulo = " Posto"
            gMsgMsg = "Requisi��o pertence a outro Posto. Tem a certeza que deseja entregar os resultados?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbNo Then
                Exit Sub
            End If
        End If
    End If
    
    ' --------------------------------------------
    ' ENTIDADE 'SEM CREDENCIAL' E DEVE TAXAS
    ' --------------------------------------------
    If gTipoInstituicao = gTipoInstituicaoPrivada Then

        If estrutRequis(linhaActual).cod_efr = gCodEFRSemCredencial And BL_VerificaTaxasUtente(estrutRequis(linhaActual).seq_utente, "", gCodEFRSemCredencial) Then
            gMsgTitulo = "Divida"
            gMsgMsg = "Utente tem recibos em atraso para a entidade SEM CREDENCIAL. Tem a certeza que deseja entregar os resultados?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbNo Then
                Exit Sub
            End If
        End If
    End If
    
    If gTipoInstituicao = gTipoInstituicaoPrivada Then
        If LbTotal > 0 Then
            gMsgTitulo = " D�vida"
            gMsgMsg = "Existem recibos por emitir. Tem a certeza que deseja entregar os resultados "
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbNo Then
                Exit Sub
            End If
        End If
    End If
    
    BG_BeginTransaction
    
    ' --------------------------------------------
    ' VERIFICA QUAIS SAO AS ANALISES PARA ENTREGAR
    ' --------------------------------------------
    For i = 1 To estrutRequis(linhaActual).TotalEstrutAnalises
        If estrutRequis(linhaActual).estrutAnalises(i).Estado_Ana = gEstadoAnaImpressa Then
            If estrutRequis(linhaActual).estrutAnalises(i).user_entrega = "" Then
                GravaEntregaResultados i
                FLG_Entrega = True
                
                ' --------------------------------------------
                ' ACTUALIZA GRELHA
                ' --------------------------------------------
                If TrataAna = True Then
                    FgReqAna.TextMatrix(i, ColANA_UserEntrega) = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", estrutRequis(linhaActual).estrutAnalises(i).user_entrega)
                    FgReqAna.TextMatrix(i, ColANA_DtEntrega) = estrutRequis(linhaActual).estrutAnalises(i).dt_entrega
                    FgReqAna.TextMatrix(i, ColANA_HrEntrega) = estrutRequis(linhaActual).estrutAnalises(i).hr_entrega
                    EcResponsavel = BL_HandleNull(estrutRequis(linhaActual).estrutAnalises(i).resp_entrega, "")
                End If
            Else
                FLG_ReqParcialmenteEntregue = True
            End If
        End If
    Next
    
    ' --------------------------------------------
    ' ALTERA ENTREGA DA REQUISICAO
    ' --------------------------------------------
    If FLG_ReqParcialmenteEntregue = True And FLG_Entrega = True Then
        estrutRequis(linhaActual).dt_entrega = Bg_DaData_ADO
        estrutRequis(linhaActual).hr_entrega = Bg_DaHora_ADO
        estrutRequis(linhaActual).user_entrega = gCodUtilizador
        estrutRequis(linhaActual).resp_entrega = Trim(EcResponsavel)
        
        sSql = "UPDATE sl_entrega_resultados SET resp_entrega = " & BL_TrataStringParaBD(estrutRequis(linhaActual).estrutAnalises(i).resp_entrega) & ", "
        sSql = sSql & " dt_entrega = " & BL_TrataDataParaBD(estrutRequis(linhaActual).dt_entrega) & ", "
        sSql = sSql & " hr_entrega = " & BL_TrataStringParaBD(estrutRequis(linhaActual).hr_entrega) & ", "
        sSql = sSql & " user_entrega = " & BL_TrataStringParaBD(estrutRequis(linhaActual).user_entrega) & ", "
        sSql = sSql & " WHERE n_req = " & estrutRequis(linhaActual).n_req & ")"
        
    ElseIf FLG_Entrega = True Then
        estrutRequis(linhaActual).dt_entrega = Bg_DaData_ADO
        estrutRequis(linhaActual).hr_entrega = Bg_DaHora_ADO
        estrutRequis(linhaActual).user_entrega = gCodUtilizador
        estrutRequis(linhaActual).resp_entrega = Trim(EcResponsavel)
        
        sSql = "INSERT INTO sl_entrega_resultados (n_req,  resp_entrega, dt_entrega, hr_entrega,"
        sSql = sSql & " user_entrega ) VALUES ( " & estrutRequis(linhaActual).n_req & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutRequis(linhaActual).resp_entrega) & ", "
        sSql = sSql & BL_TrataDataParaBD(estrutRequis(linhaActual).dt_entrega) & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutRequis(linhaActual).hr_entrega) & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutRequis(linhaActual).user_entrega) & ")"
    End If
    BG_ExecutaQuery_ADO sSql
    
    If TrataReq = True Then
        FgReqAna.TextMatrix(linhaActual, ColREQ_UserEntrega) = estrutRequis(linhaActual).user_entrega
        FgReqAna.TextMatrix(linhaActual, ColREQ_DtEntrega) = estrutRequis(linhaActual).dt_entrega
        FgReqAna.TextMatrix(linhaActual, ColREQ_HrEntrega) = estrutRequis(linhaActual).hr_entrega
        FgReqAna.TextMatrix(linhaActual, ColREQ_UserEntrega) = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", estrutRequis(linhaActual).user_entrega)
        EcResponsavel = BL_HandleNull(estrutRequis(linhaActual).resp_entrega, "")
    End If
    
    If gSQLError = 0 Then
        BG_CommitTransaction
                 
        'NELSONPSILVA 04.04.2018 Glintt-HS-18011
        If gATIVA_LOGS_RGPD = mediSim Then
            Dim requestJson As String
            Dim responseJson As String
                              
            requestJson = "{" & Chr(34) & "n_req" & Chr(34) & ":" & Chr(34) & estrutRequis(linhaActual).n_req & ", " & Chr(34) & "resp_entrega" & Chr(34) & ":" & Chr(34) & BL_TrataStringParaBD(estrutRequis(linhaActual).resp_entrega) & Chr(34) & _
            ", " & Chr(34) & "dt_entrega" & Chr(34) & ":" & Chr(34) & BL_TrataDataParaBD(estrutRequis(linhaActual).dt_entrega) & ", " & Chr(34) & "hr_entrega" & Chr(34) & ":" & Chr(34) & BL_TrataStringParaBD(estrutRequis(linhaActual).hr_entrega) & Chr(34) & _
            ", " & Chr(34) & "user_entrega" & Chr(34) & ":" & Chr(34) & BL_TrataStringParaBD(estrutRequis(linhaActual).user_entrega) & Chr(34) & "}"
            

            BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Insercao) & "/" & BL_DevolveTipOperecao(TipoOperacao.Alteracao) & " - " & gFormActivo.Name, _
            Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
        End If
        
    Else
        BG_RollbackTransaction
    End If
Exit Sub
TrataErro:
    BL_LogFile_BD Me.Name, "BtConfirmar_Click", Err.Number, Err.Description, ""
    Exit Sub
    Resume Next
End Sub

Private Sub BtGestReq_Click()
    If EcNumReq <> "" Then
        FormGestaoRequisicaoPrivado.Show
        Set gFormActivo = FormGestaoRequisicaoPrivado
        FormGestaoRequisicaoPrivado.EcNumReq = EcNumReq
        FormGestaoRequisicaoPrivado.FuncaoProcurar
        FormGestaoRequisicaoPrivado.SSTGestReq.Tab = 4
     End If
End Sub

Private Sub BtNotas_Click()
    If EcNumReq <> "" Then
        FormEntregaResultados.Enabled = False
        FormNotasReq.Show
        FormNotasReq.EcNumReq = EcNumReq
        FormNotasReq.EcSeqUtente = EcSeqUtente
        FormNotasReq.FuncaoProcurar
    End If
End Sub

Private Sub BtNotasVRM_Click()
    If EcNumReq <> "" Then
        FormEntregaResultados.Enabled = False
        FormNotasReq.Show
        FormNotasReq.EcNumReq = EcNumReq
        FormNotasReq.EcSeqUtente = EcSeqUtente
        FormNotasReq.FuncaoProcurar
    End If
End Sub

Private Sub EcNumBenef_Validate(Cancel As Boolean)
    Dim sSql As String
    If EcNumBenef <> "" Then
        sSql = "SELECT * FROM " & tabela_aux & " WHERE n_benef_ute = " & BL_TrataStringParaBD(EcNumBenef)
        PreencheDadosUtente sSql
    End If
End Sub

Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        EcNumReq_Validate False
        FuncaoProcurar
    End If
End Sub

Private Sub EcUtente_Validate(Cancel As Boolean)
    Dim sSql As String
    If EcUtente <> "" And CbTipoUtente.ListIndex <> mediComboValorNull Then
        sSql = "SELECT * FROM " & tabela_aux & " WHERE utente = " & BL_TrataStringParaBD(EcUtente)
        sSql = sSql & " AND t_utente = " & BL_TrataStringParaBD(CbTipoUtente)
        PreencheDadosUtente sSql
    End If
End Sub
Public Sub EcNumReq_Validate(Cancel As Boolean)
    Dim sSql As String
    If EcNumReq <> "" Then
        sSql = "SELECT * FROM " & tabela_aux & " WHERE seq_utente = ( SELECT seq_utente FROM "
        sSql = sSql & " sl_requis WHERE n_req = " & EcNumReq & ")"
        PreencheDadosUtente sSql
    End If
End Sub

Private Sub FgReqAna_Click()
    If FgReqAna.row > 0 And FgReqAna.row <= totalEstrutRequis And TrataReq = True Then
        EcNumReq = estrutRequis(FgReqAna.row).n_req
        PreencheNotas
        If gTipoInstituicao = gTipoInstituicaoPrivada Then
            If estrutRequis(FgReqAna.row).valorDeve = 0 Then
                LbDivida.ForeColor = Verde
                LbDivida = "PAGO"
            Else
                LbDivida.ForeColor = vbRed
                LbDivida = FormatCurrency(estrutRequis(FgReqAna.row).valorDeve, 2)
            End If
        End If
        linhaActual = FgReqAna.row
        EcResponsavel.SetFocus
    ElseIf TrataReq = True Then
        EcNumReq = ""
    End If
End Sub

Private Sub FgReqAna_dblClick()
    If FgReqAna.row > 0 And FgReqAna.row <= totalEstrutRequis Then
        If TrataAna = False Then
            PreencheFgReqAna FgReqAna.row
        Else
            PreencheFgReqAna
            FgReqAna.Col = 0
            FgReqAna.row = 1
            FgReqAna_Click
        End If
    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    ' Propriedades do form
    Me.caption = "Entrega de Resultados"
    Me.left = 540
    Me.top = 450
    Me.Width = 11820
    Me.Height = 7110
    Set CampoDeFocus = EcNumReq

    ' Configuracao de parametros para BD
    NomeTabela = "sl_entrega_resultados"
    NumCampos = 5
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim estrutRequis(0)
    
    ' Campos de BD
    CamposBD(0) = "n_req"
    CamposBD(1) = "user_entrega"
    CamposBD(2) = "dt_entrega"
    CamposBD(3) = "hr_entrega"
    CamposBD(4) = "resp_entrega"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcNumReq
    Set CamposEc(1) = EcUserEntrega
    Set CamposEc(2) = EcDtEntrega
    Set CamposEc(3) = EcHrEntrega
    Set CamposEc(4) = EcResponsavel
    
    ' Activa recordset
    Set rs = New ADODB.recordset
    
End Sub
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    ' Verifica se a sala ja esta definida
    
        
    ' Padrao
    Estado = 0
    gF_ENTREGA_RESULTADOS = mediSim
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    
    
End Sub

' --------------------------------------
' Evento form activo
' --------------------------------------
Sub EventoActivate()
    CampoDeFocus.SetFocus
    
    ' Padrao
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
End Sub

' --------------------------------------
' Evento de saida do form
' --------------------------------------
Sub EventoUnload()
    
    ' Padrao
    LimpaCampos
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    gF_ENTREGA_RESULTADOS = mediNao

End Sub

' --------------------------------------
' Limpa campos do form '
' --------------------------------------
Sub LimpaCampos()
    
    BG_LimpaCampo_Todos CamposEc
    
    
    EcNome = ""
    CbTipoUtente.ListIndex = mediComboValorNull
    EcNome = ""
    EcUtente = ""
    EcNumBenef = ""
    EcSeqUtente = ""
    
    EcNome.locked = False
    CbTipoUtente.locked = False
    LbDivida = ""
    LbTotal = ""
    EcNome.locked = False
    EcNumBenef.locked = False
    EcUtente.locked = False
    BtGestReq.Enabled = False
    LimpaCampos2
    CampoDeFocus.SetFocus
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
End Sub

' ------------------------------------------------------------------------------------------------------------------

' LIMPA A ESTRUTURA E A FLEXGRID

' ------------------------------------------------------------------------------------------------------------------
Private Sub LimpaCampos2()
    TrataReq = True
    TrataAna = False
    
    ' Limpa estrutura e lista de utentes
    ReDim estrutRequis(0)
    totalEstrutRequis = 0
    FgReqAna.Clear
    DefTipoCampos_FgReqAna
End Sub
' --------------------------------------
' Define campos do form
' --------------------------------------
Sub DefTipoCampos()
    BtNotas.Visible = True
    BtNotasVRM.Visible = False
    EcDtEntrega.Tag = adDate
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
    DefTipoCampos_FgReqAna
End Sub

' --------------------------------------
' Procura utentes em fila de espera
' --------------------------------------
Public Sub FuncaoProcurar()
    
    Dim sSql As String
    Dim RsRequis As New ADODB.recordset
    Dim valorDeve As Double
    Dim totalDeve As Double
    On Error GoTo Trata_Erro
    gSQLError = 0
    
    LimpaCampos2
    
    If EcSeqUtente = "" Then
        Exit Sub
    End If
    
    ' ----------------------------------------
    ' PROCURA REQUISICOES ASSOCIADAS AO UTENTE
    ' ----------------------------------------
    sSql = "SELECT x1.n_req,x1.cod_efr, x1.cod_sala, x1.dt_chega, x1.estado_req, x2.user_entrega, x2.resp_entrega,x2.dt_entrega,x2.hr_entrega FROM "
    If gSGBD = gOracle Then
        sSql = sSql & " sl_requis x1, sl_entrega_resultados x2 "
        sSql = sSql & " WHERE  x1.n_req  = x2.n_req (+)  AND x1.seq_utente = " & EcSeqUtente
        sSql = sSql & " AND x1.estado_Req not in (" & BL_TrataStringParaBD(gEstadoReqCancelada) & "," & BL_TrataStringParaBD(gEstadoReqBloqueada) & ")  "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " sl_requis x1 LEFT OUTER JOIN sl_entrega_resultados x2  ON x1.n_req = x2.n_req"
        sSql = sSql & " WHERE x1.seq_utente = " & EcSeqUtente
        sSql = sSql & " AND x1.estado_Req not in (" & BL_TrataStringParaBD(gEstadoReqCancelada) & "," & BL_TrataStringParaBD(gEstadoReqBloqueada) & ")"
    End If
    sSql = sSql & " ORDER BY dt_chega DESC "
    
    RsRequis.CursorType = adOpenStatic
    RsRequis.CursorLocation = adUseServer
    RsRequis.Open sSql, gConexao
    totalDeve = 0
    If RsRequis.RecordCount > 0 Then
        While Not RsRequis.EOF
            valorDeve = RetornaDivida(RsRequis!n_req)
            PreencheEstrutRequis RsRequis!n_req, BL_HandleNull(RsRequis!dt_chega, "---"), RsRequis!estado_req, BL_HandleNull(RsRequis!resp_entrega, ""), _
                                 BL_HandleNull(RsRequis!user_entrega, ""), BL_HandleNull(RsRequis!dt_entrega, ""), BL_HandleNull(RsRequis!hr_entrega, ""), _
                                 BL_HandleNull(RsRequis!cod_efr, ""), BL_HandleNull(RsRequis!cod_sala, ""), valorDeve
            totalDeve = totalDeve + valorDeve
            RsRequis.MoveNext
        Wend
        If gTipoInstituicao = gTipoInstituicaoPrivada Then
            LbTotal = totalDeve
        End If
        PreencheFgReqAna
        BtGestReq.Enabled = True
        FgReqAna.row = 1
        FgReqAna_Click
        'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
        Dim requestJson As String
        Dim responseJson As String
        Dim CamposEcra() As Object
        Dim NumCampos As Integer
        NumCampos = 6
        ReDim CamposEcra(0 To NumCampos - 1)
         Set CamposEcra(0) = EcNumReq
         Set CamposEcra(1) = EcResponsavel
         Set CamposEcra(2) = EcNome
         Set CamposEcra(3) = CbTipoUtente
         Set CamposEcra(4) = EcUtente
         Set CamposEcra(5) = EcNumBenef
    
        Dim CamposBDparaListBox
        CamposBDparaListBox = Array("n_req", "estado_req", "dt_chega", "resp_entrega", "user_entrega", "dt_entrega", "resp_entrega", "hr_entrega", "cod_efr", "cod_sala")
        
        requestJson = BL_TrataCamposRGPD(CamposEcra)
        responseJson = BL_TrataCamposRGPD(CamposEcra) & "," & BL_TrataListBoxRGPD(CamposBDparaListBox, RsRequis, True)
        BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
        Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
    End If
    Else
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo!", vbInformation, "Procurar"
    End If
    RsRequis.Close
    Set RsRequis = Nothing
Exit Sub
Trata_Erro:
    BL_LogFile_BD Me.Name, "FuncaoProcurar", Err.Number, Err.Description, ""
    Me.MousePointer = vbArrow
    Exit Sub
    Resume Next
End Sub


' --------------------------------------
' Funcao limpar do form
' --------------------------------------
Public Sub FuncaoLimpar()
    If Estado = 2 Then
        'FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
End Sub


' ------------------------------------------------------------------------------------------------------------------

' DEFINE A FLEX GRID PARA REQUISICOES E PARA ANALISES. TRABALHAMOS SEMPRE NA MESMA ESTRUTURA

' ------------------------------------------------------------------------------------------------------------------
Private Sub DefTipoCampos_FgReqAna()
    With FgReqAna
        .rows = NUM_LINHAS
        .FixedRows = 1
        .Cols = 8
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        If TrataReq = True Then
            .ColWidth(0) = 700
            .Col = 0
            .TextMatrix(0, 0) = "N.Req"
            .ColWidth(1) = 2000
            .Col = 1
            .TextMatrix(0, 1) = "Estado"
            .ColWidth(2) = 950
            .Col = 2
            .TextMatrix(0, 2) = "Chegada"
            .ColWidth(3) = 3000
            .Col = 3
            .TextMatrix(0, 3) = "Respons�vel"
            .ColWidth(4) = 950
            .Col = 4
            .TextMatrix(0, 4) = "Data Entrega"
            .ColWidth(5) = 1000
            .Col = 5
            .TextMatrix(0, 5) = "Hora Entrega"
            .ColWidth(6) = 900
            .Col = 6
            .TextMatrix(0, 6) = "Util. Entrega"
            .ColWidth(7) = 450
            .Col = 7
            .TextMatrix(0, 7) = "EFR"
        Else
            .ColWidth(0) = 900
            .Col = 0
            .TextMatrix(0, 0) = "N.Req"
            .ColWidth(1) = 4000
            .Col = 1
            .TextMatrix(0, 1) = "An�lise"
            .ColWidth(2) = 1290
            .Col = 2
            .TextMatrix(0, 2) = "Estado"
            .ColWidth(3) = 950
            .Col = 3
            .TextMatrix(0, 3) = "Data Entrega"
            .ColWidth(4) = 950
            .Col = 4
            .TextMatrix(0, 4) = "Hora Entrega"
            .ColWidth(5) = 900
            .Col = 5
            .TextMatrix(0, 5) = "Util. Entrega"
            .ColWidth(6) = 0
            .ColWidth(7) = 0
            End If
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
End Sub

Sub PreencheDadosUtente(sSql As String)
    Dim RsUtente As New ADODB.recordset
    If Not IsNumeric(EcNumReq) And (CbTipoUtente.ListIndex = mediComboValorNull Or EcUtente = "") Then
        Exit Sub
    End If
    
    RsUtente.CursorType = adOpenStatic
    RsUtente.CursorLocation = adUseServer
    RsUtente.Open sSql, gConexao
    If RsUtente.RecordCount <= 0 Then
        LimpaCampos
        Exit Sub
    ElseIf RsUtente.RecordCount = 1 Then
        EcUtente.text = BL_HandleNull(RsUtente!Utente, "")
        CbTipoUtente.text = BL_HandleNull(RsUtente!t_utente, "")
        EcNome.text = BL_HandleNull(RsUtente!nome_ute, "")
        EcNumBenef = BL_HandleNull(RsUtente!n_benef_ute, "")
        EcSeqUtente = RsUtente!seq_utente
    End If
    EcNome.locked = True
    EcUtente.locked = True
    CbTipoUtente.locked = True
    EcNome.locked = True
    EcNumBenef.locked = True
    RsUtente.Close
    Set RsUtente = Nothing
End Sub

Sub PreencheValoresDefeito()
    'LimpaCampos
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
End Sub


' ------------------------------------------------------------------------------------------------------------------

' PREENCHE A ESTRUTURA DE REQUISICOES

' ------------------------------------------------------------------------------------------------------------------
Private Sub PreencheEstrutRequis(n_req As String, dt_chega As String, Estado As String, resp_entrega As String, _
                                user_entrega As String, dt_entrega As String, hr_entrega As String, cod_efr As String, _
                                cod_sala As String, valorDeve As Double)
                                
    On Error GoTo TrataErro
    totalEstrutRequis = totalEstrutRequis + 1
    ReDim Preserve estrutRequis(totalEstrutRequis)

    estrutRequis(totalEstrutRequis).n_req = n_req
    estrutRequis(totalEstrutRequis).seq_utente = EcSeqUtente
    estrutRequis(totalEstrutRequis).dt_chega = dt_chega
    If Estado = gEstadoReqSemAnalises Then
        estrutRequis(totalEstrutRequis).estado_req = "Sem An�lises"
    Else
        estrutRequis(totalEstrutRequis).estado_req = BL_DevolveEstadoReq(CStr(Estado))
    End If
    estrutRequis(totalEstrutRequis).resp_entrega = resp_entrega
    estrutRequis(totalEstrutRequis).user_entrega = user_entrega
    estrutRequis(totalEstrutRequis).dt_entrega = dt_entrega
    estrutRequis(totalEstrutRequis).hr_entrega = hr_entrega
    estrutRequis(totalEstrutRequis).cod_efr = cod_efr
    estrutRequis(totalEstrutRequis).cod_sala = cod_sala
    
    estrutRequis(totalEstrutRequis).nome_ute = EcNome
    estrutRequis(totalEstrutRequis).num_benef = EcNumBenef
    estrutRequis(totalEstrutRequis).t_utente = CbTipoUtente
    estrutRequis(totalEstrutRequis).Utente = EcUtente
    estrutRequis(totalEstrutRequis).valorDeve = valorDeve
    
    estrutRequis(totalEstrutRequis).TotalEstrutAnalises = 0
    ReDim estrutRequis(totalEstrutRequis).estrutAnalises(0)
    
    PreencheEstrutAnalises
Exit Sub
TrataErro:
    BL_LogFile_BD Me.Name, "PreencheEstrutRequis", Err.Number, Err.Description, ""
    Me.MousePointer = vbArrow
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------------------------

' PARA CADA REQUISICAO PREENCHE A ESTRUTURA DAS  ANALISES COM RESPECTIVOS ESTADOS

' ------------------------------------------------------------------------------------------------------------------
Private Sub PreencheEstrutAnalises()
    Dim sSql As String
    Dim rsAnalises As New ADODB.recordset
    Dim totalAux As Long
    Dim Tabela As String
    
    On Error GoTo TrataErro
    
    If UCase(estrutRequis(totalEstrutRequis).estado_req) = "HIST�RICO" Then
        Tabela = " sl_realiza_h"
    Else
        Tabela = " sl_realiza "
    End If
    If gSGBD = gSqlServer Then
        sSql = "SELECT x1.cod_agrup, x1.flg_estado,x2.user_entrega, x2.dt_entrega, x2.hr_entrega, x2.resp_entrega "
        sSql = sSql & " FROM " & Tabela & " x1 LEFT OUTER JOIN sl_entrega_resultados_det x2 ON x1.cod_agrup = x2.cod_agrup "
        sSql = sSql & " AND x1.n_req = x2.n_req "
        sSql = sSql & " WHERE x1.n_req = " & BL_TrataStringParaBD(estrutRequis(totalEstrutRequis).n_req)
        sSql = sSql & " GROUP BY x1.cod_agrup, flg_estado,user_entrega, dt_entrega, hr_entrega, resp_entrega "
        sSql = sSql & " UNION SELECT x1.cod_agrup, -1 flg_estado,x2.user_entrega, x2.dt_entrega, x2.hr_entrega, x2.resp_entrega "
        sSql = sSql & " FROM sl_marcacoes x1 LEFT OUTER JOIN sl_entrega_resultados_det x2 ON x1.cod_agrup = x2.cod_agrup "
        sSql = sSql & " AND x1.n_req = x2.n_req "
        sSql = sSql & " WHERE x1.n_req = " & BL_TrataStringParaBD(estrutRequis(totalEstrutRequis).n_req)
        sSql = sSql & " GROUP BY x1.cod_agrup, user_entrega, dt_entrega, hr_entrega, resp_entrega "
    ElseIf gSGBD = gOracle Then
        sSql = "SELECT x1.cod_agrup, x1.flg_estado,x2.user_entrega, x2.dt_entrega, x2.hr_entrega, x2.resp_entrega "
        sSql = sSql & " FROM " & Tabela & " x1, sl_entrega_resultados_det x2 WHERE x1.n_req = " & estrutRequis(totalEstrutRequis).n_req
        sSql = sSql & " AND x1.cod_agrup = x2.cod_agrup (+) "
        sSql = sSql & " AND x1.n_req = x2.n_req (+)"
        sSql = sSql & " GROUP BY x1.cod_agrup, flg_estado,user_entrega, dt_entrega, hr_entrega, resp_entrega "
        sSql = sSql & " UNION SELECT x1.cod_agrup, '-1' flg_estado,x2.user_entrega, x2.dt_entrega, x2.hr_entrega, x2.resp_entrega "
        sSql = sSql & " FROM sl_marcacoes x1, sl_entrega_resultados_det x2 WHERE x1.n_req = " & estrutRequis(totalEstrutRequis).n_req
        sSql = sSql & " AND x1.cod_agrup = x2.cod_agrup (+) "
        sSql = sSql & " AND x1.n_req = x2.n_req (+)"
        sSql = sSql & " GROUP BY x1.cod_agrup, user_entrega, dt_entrega, hr_entrega, resp_entrega "
    End If
    rsAnalises.CursorLocation = adUseClient
    rsAnalises.Open sSql, gConexao
    If rsAnalises.RecordCount > 0 Then
        While Not rsAnalises.EOF
            totalAux = estrutRequis(totalEstrutRequis).TotalEstrutAnalises + 1
            estrutRequis(totalEstrutRequis).TotalEstrutAnalises = totalAux
            ReDim Preserve estrutRequis(totalEstrutRequis).estrutAnalises(totalAux)
            estrutRequis(totalEstrutRequis).estrutAnalises(totalAux).cod_agrup = BL_HandleNull(rsAnalises!cod_agrup, "")
            estrutRequis(totalEstrutRequis).estrutAnalises(totalAux).Estado_Ana = BL_HandleNull(rsAnalises!flg_estado, "-1")
            estrutRequis(totalEstrutRequis).estrutAnalises(totalAux).descr_agrup = BL_SelCodigo("SLV_ANALISES", "DESCR_ANA", "COD_ANA", BL_HandleNull(rsAnalises!cod_agrup, ""))
            estrutRequis(totalEstrutRequis).estrutAnalises(totalAux).resp_entrega = BL_HandleNull(rsAnalises!resp_entrega, "")
            estrutRequis(totalEstrutRequis).estrutAnalises(totalAux).user_entrega = BL_HandleNull(rsAnalises!user_entrega, "")
            estrutRequis(totalEstrutRequis).estrutAnalises(totalAux).dt_entrega = BL_HandleNull(rsAnalises!dt_entrega, "")
            estrutRequis(totalEstrutRequis).estrutAnalises(totalAux).hr_entrega = BL_HandleNull(rsAnalises!hr_entrega, "")
            rsAnalises.MoveNext
        Wend
    End If
    rsAnalises.Close
    Set rsAnalises = Nothing
Exit Sub
TrataErro:
    BL_LogFile_BD Me.Name, "PreencheEstrutAnalises", Err.Number, Err.Description, ""
    Me.MousePointer = vbArrow
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------------------------

' PREENCHE A FLEXGRID COM OS DADOS DAS REQUISICOES/ANALISES

' ------------------------------------------------------------------------------------------------------------------
Private Sub PreencheFgReqAna(Optional linha As Integer)
    Dim i As Long
    
    ' ------------------
    ' LIMPA A GRID
    ' ------------------
    If Not IsMissing(linha) And linha <> 0 Then
        TrataAna = True
        TrataReq = False
    Else
        TrataAna = False
        TrataReq = True
    End If
    
    FgReqAna.Clear
    DefTipoCampos_FgReqAna
    
    If TrataReq = True Then
        For i = 1 To totalEstrutRequis
            FgReqAna.TextMatrix(i, ColREQ_NReq) = estrutRequis(i).n_req
            FgReqAna.TextMatrix(i, ColREQ_Estado) = estrutRequis(i).estado_req
            FgReqAna.TextMatrix(i, ColREQ_DtChega) = estrutRequis(i).dt_chega
            FgReqAna.TextMatrix(i, ColREQ_DtEntrega) = estrutRequis(i).dt_entrega
            FgReqAna.TextMatrix(i, ColREQ_HrEntrega) = estrutRequis(i).hr_entrega
            FgReqAna.TextMatrix(i, ColREQ_UserEntrega) = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", estrutRequis(i).user_entrega)
            FgReqAna.TextMatrix(i, ColREQ_RespEntrega) = estrutRequis(i).resp_entrega
            FgReqAna.Col = ColREQ_CodEFR
            FgReqAna.row = i
            If estrutRequis(i).cod_efr = gCodEFRSemCredencial Then
                FgReqAna.CellBackColor = &HFFC0FF
            Else
                FgReqAna.CellBackColor = vbWhite
            End If
            FgReqAna.TextMatrix(i, ColREQ_CodEFR) = estrutRequis(i).cod_efr
            If i = FgReqAna.rows - 1 Then
                FgReqAna.AddItem ""
            End If
        Next
    ElseIf TrataAna = True Then
        For i = 1 To estrutRequis(linha).TotalEstrutAnalises
            FgReqAna.TextMatrix(i, ColANA_NReq) = estrutRequis(linha).n_req
            FgReqAna.TextMatrix(i, ColANA_Analise) = estrutRequis(linha).estrutAnalises(i).descr_agrup
            If estrutRequis(linha).estrutAnalises(i).Estado_Ana = "-1" Then
                FgReqAna.TextMatrix(i, ColANA_Estado) = cSemResultado
            Else
                FgReqAna.TextMatrix(i, ColANA_Estado) = estrutRequis(linha).estrutAnalises(i).Estado_Ana
            End If
            
            FgReqAna.TextMatrix(i, ColANA_DtEntrega) = estrutRequis(linha).estrutAnalises(i).dt_entrega
            FgReqAna.TextMatrix(i, ColANA_HrEntrega) = estrutRequis(linha).estrutAnalises(i).hr_entrega
            FgReqAna.TextMatrix(i, ColANA_UserEntrega) = BL_SelCodigo("SL_IDUTILIZADOR", "NOME", "COD_UTILIZADOR", estrutRequis(linha).estrutAnalises(i).user_entrega)
            
            If i = FgReqAna.rows - 1 Then
                FgReqAna.AddItem ""
            End If
        Next
    End If
End Sub

' ------------------------------------------------------------------------------------------------------------------

' GRAVA DETALHES DA ENTREGA DE RESULTADOS

' ------------------------------------------------------------------------------------------------------------------
Private Sub GravaEntregaResultados(LinhaAnalise As Long)
    Dim sSql As String
    On Error GoTo TrataErro
    
    estrutRequis(linhaActual).estrutAnalises(LinhaAnalise).resp_entrega = Trim(EcResponsavel)
    estrutRequis(linhaActual).estrutAnalises(LinhaAnalise).dt_entrega = Bg_DaData_ADO
    estrutRequis(linhaActual).estrutAnalises(LinhaAnalise).hr_entrega = Bg_DaHora_ADO
    estrutRequis(linhaActual).estrutAnalises(LinhaAnalise).user_entrega = gCodUtilizador
    
    sSql = "INSERT INTO sl_entrega_resultados_det (n_req, cod_agrup, resp_entrega, dt_entrega, hr_entrega,"
    sSql = sSql & " user_entrega ) VALUES ( " & estrutRequis(linhaActual).n_req & ", "
    sSql = sSql & BL_TrataStringParaBD(estrutRequis(linhaActual).estrutAnalises(LinhaAnalise).cod_agrup) & ", "
    sSql = sSql & BL_TrataStringParaBD(estrutRequis(linhaActual).estrutAnalises(LinhaAnalise).resp_entrega) & ", "
    sSql = sSql & BL_TrataDataParaBD(estrutRequis(linhaActual).estrutAnalises(LinhaAnalise).dt_entrega) & ", "
    sSql = sSql & BL_TrataStringParaBD(estrutRequis(linhaActual).estrutAnalises(LinhaAnalise).hr_entrega) & ","
    sSql = sSql & BL_TrataStringParaBD(estrutRequis(linhaActual).estrutAnalises(LinhaAnalise).user_entrega) & ")"
    BG_ExecutaQuery_ADO sSql
    
Exit Sub
TrataErro:
    BL_LogFile_BD Me.Name, "GravaEntregaResultados", Err.Number, Err.Description, ""
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------------------------------------------

' RETORNA A DIVIDA DE UMA DETERMINADA REQUISI��O

' ------------------------------------------------------------------------------------------------------------------

Private Function RetornaDivida(n_req As String) As Double
    Dim sSql As String
    Dim rsDivida As New ADODB.recordset
    Dim total As Double
    
    sSql = "SELECT estado, total_pagar, n_req FROM sl_recibos WHERE n_req = " & BL_TrataStringParaBD(n_req) & " AND cod_efr NOT IN"
    sSql = sSql & " (SELECT cod_efr FROM SL_EFR where FLG_MOSTRA_DIVIDA  = 0 )"
    'UALIA-925
    sSql = sSql & " UNION select 'N' estado , valor total_pagar, slv_divida_utente.n_req n_req from slv_divida_utente JOIN sl_requis on slv_divida_utente.n_req = sl_requis.n_req "
    sSql = sSql & " JOIN sl_identif on sl_identif.seq_utente = sl_requis.seq_utente WHERE slv_divida_utente.n_req = " & BL_TrataStringParaBD(n_req) & " "
    sSql = sSql & " AND sl_requis.cod_efr NOT IN (SELECT cod_efr FROM SL_EFR where FLG_MOSTRA_DIVIDA  = 0 )"
    '
    rsDivida.CursorLocation = adUseClient
    rsDivida.Open sSql, gConexao
    If rsDivida.RecordCount > 0 Then
        total = 0
        While Not rsDivida.EOF
            If rsDivida!Estado = gEstadoReciboNaoEmitido Or rsDivida!Estado = gEstadoReciboCobranca Or rsDivida!Estado = gEstadoReciboPerdido Then
                total = total + CDbl(BL_HandleNull(rsDivida!total_pagar, CDec("0,00")))
            End If
            rsDivida.MoveNext
        Wend
    End If
    rsDivida.Close
    Set rsDivida = Nothing
    RetornaDivida = total
End Function

' --------------------------------------------------------

' VERIFICA SE EXISTEM NOTAS PARA REQUISI��O EM CAUSA

' --------------------------------------------------------
Private Sub PreencheNotas()
    Dim sSql As String
    Dim RsNotas As New ADODB.recordset
    
    If EcNumReq = "" Then Exit Sub
    sSql = "SELECT count(*) total FROM sl_obs_ana_req WHERE n_req = " & EcNumReq

    RsNotas.CursorType = adOpenStatic
    RsNotas.CursorLocation = adUseServer
    RsNotas.Open sSql, gConexao
    If RsNotas.RecordCount > 0 Then
        If RsNotas!total > 0 Then
            BtNotas.Visible = False
            BtNotasVRM.Visible = True
            
        Else
            BtNotas.Visible = True
            BtNotasVRM.Visible = False
        End If
    End If
    RsNotas.Close
    Set RsNotas = Nothing

End Sub



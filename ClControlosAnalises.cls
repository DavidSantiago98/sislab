VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClControlosAnalises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public CtlCodAna As Object
Public CtlDescrAna As Object
Public CtlNReqARS As Object
Public CtlP1 As Object
Public CtlEcFraccao As Object

' constantes para as cores
Private Const FORECOLOR_SELEC_ENABLED = &H80000008
Private Const BACKCOLOR_SELEC_ENABLED = &H80000005
Private Const FORECOLOR_SELEC_DISABLED = &H80000008
Private Const BACKCOLOR_SELEC_DISABLED = &H8000000F
Private Const FORECOLOR_ENABLED = &H80000008
Private Const BACKCOLOR_ENABLED = &H80000009
Private Const FORECOLOR_DISABLED = &H80000008
Private Const BACKCOLOR_DISABLED = &H8000000F '&HE0E0E0

Public Sub EscondeLinha(Indice As Integer)
    CtlCodAna(Indice).Visible = False
    CtlDescrAna(Indice).Visible = False
    CtlEcFraccao(Indice).Visible = False
    CtlNReqARS(Indice).Visible = False
    CtlP1(Indice).Visible = False
    LimpaLinha Indice
End Sub
Public Sub LimpaLinha(Indice As Integer)
    CtlCodAna(Indice).Text = ""
    CtlDescrAna(Indice).Text = ""
    CtlEcFraccao(Indice).Text = ""
    CtlNReqARS(Indice).Text = ""
    CtlP1(Indice).Text = ""
End Sub
Public Sub MostraLinha(Indice As Integer)
    CtlCodAna(Indice).Visible = True
    CtlDescrAna(Indice).Visible = True
    CtlNReqARS(Indice).Visible = True
    CtlP1(Indice).Visible = True
End Sub
Public Sub ActivaLinha(Indice As Integer)
    CtlCodAna(Indice).BackColor = BACKCOLOR_SELEC_ENABLED
    CtlCodAna(Indice).ForeColor = FORECOLOR_SELEC_ENABLED
    CtlCodAna(Indice).FontBold = True
    CtlDescrAna(Indice).BackColor = BACKCOLOR_SELEC_DISABLED
    CtlDescrAna(Indice).ForeColor = FORECOLOR_SELEC_DISABLED
    CtlDescrAna(Indice).FontBold = True
    CtlEcFraccao(Indice).BackColor = BACKCOLOR_SELEC_ENABLED
    CtlEcFraccao(Indice).ForeColor = FORECOLOR_SELEC_ENABLED
    CtlEcFraccao(Indice).FontBold = True
    CtlNReqARS(Indice).BackColor = BACKCOLOR_SELEC_ENABLED
    CtlNReqARS(Indice).ForeColor = FORECOLOR_SELEC_ENABLED
    CtlNReqARS(Indice).FontBold = True
    CtlP1(Indice).BackColor = BACKCOLOR_SELEC_DISABLED
    CtlP1(Indice).ForeColor = FORECOLOR_SELEC_DISABLED
    CtlP1(Indice).FontBold = True
End Sub
Public Sub DesactivaLinha(Indice As Integer)
    CtlCodAna(Indice).BackColor = BACKCOLOR_ENABLED
    CtlCodAna(Indice).ForeColor = FORECOLOR_ENABLED
    CtlCodAna(Indice).FontBold = False
    CtlDescrAna(Indice).BackColor = BACKCOLOR_DISABLED
    CtlDescrAna(Indice).ForeColor = FORECOLOR_DISABLED
    CtlDescrAna(Indice).FontBold = False
    CtlEcFraccao(Indice).BackColor = BACKCOLOR_ENABLED
    CtlEcFraccao(Indice).ForeColor = FORECOLOR_ENABLED
    CtlEcFraccao(Indice).FontBold = False
    CtlNReqARS(Indice).BackColor = BACKCOLOR_ENABLED
    CtlNReqARS(Indice).ForeColor = FORECOLOR_ENABLED
    CtlNReqARS(Indice).FontBold = False
    CtlP1(Indice).BackColor = BACKCOLOR_DISABLED
    CtlP1(Indice).ForeColor = FORECOLOR_DISABLED
    CtlP1(Indice).FontBold = False
End Sub

Public Sub CarregaControlo(Indice As Integer, Deslocacao As Long)
    'On Error Resume Next
    Load CtlCodAna(Indice)
    CtlCodAna(Indice).Top = CtlCodAna(Indice).Top + (Indice * Deslocacao)
    Load CtlDescrAna(Indice)
    CtlDescrAna(Indice).Top = CtlDescrAna(Indice).Top + (Indice * Deslocacao)
    Load CtlEcFraccao(Indice)
    CtlEcFraccao(Indice).Top = CtlEcFraccao(Indice).Top + (Indice * Deslocacao)
    Load CtlNReqARS(Indice)
    CtlNReqARS(Indice).Top = CtlNReqARS(Indice).Top + (Indice * Deslocacao)
    Load CtlP1(Indice)
    CtlP1(Indice).Top = CtlP1(Indice).Top + (Indice * Deslocacao)
    'BL_DesactivaCampo CtlDescricaoRubrica(Indice)
End Sub

Public Sub DescarregaControlo(Indice As Integer)
    On Error Resume Next
    Unload CtlCodAna(Indice)
    Unload CtlDescrAna(Indice)
    Unload CtlEcFraccao(Indice)
    Unload CtlNReqARS(Indice)
    Unload CtlP1(Indice)
End Sub

Public Sub MudaLinhaCursor(CampoActivo As Control, Optional NovaLinha)
    Dim IndiceActivo As Integer
    
    If CampoActivo.Enabled And CampoActivo.Visible Then
            CampoActivo.SetFocus
        End If
    IndiceActivo = CampoActivo.Index
    ActivaLinha IndiceActivo
 End Sub

Public Sub MudaLinhaActiva(Indice As Integer)
    If CtlCodAna(Indice).Enabled And CtlCodAna(Indice).Visible Then
        CtlCodAna(Indice).SetFocus
    End If
End Sub
Sub PreencheValoresDefeito(PosicaoListaControlos As Integer)

End Sub

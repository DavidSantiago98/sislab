Attribute VB_Name = "BibliotecaGenerica"
Option Explicit

' Actualiza��o : 15/02/2002
' T�cnico Paulo Costa

'
' Biblioteca de Rotinas Gen�ricas
'

' Declara��es globais

'##### Inicio: Tratamento do Registry #####
Public Const HKEY_CLASSES_ROOT = &H80000000
Public Const HKEY_CURRENT_CONFIG = &H80000005
Public Const HKEY_CURRENT_USER = &H80000001
Public Const HKEY_DYN_DATA = &H80000006
Public Const HKEY_LOCAL_MACHINE = &H80000002
Public Const HKEY_PERFORMANCE_DATA = &H80000004
Public Const HKEY_USERS = &H80000003

Public Const KEY_QUERY_VALUE = &H1
Public Const KEY_ENUMERATE_SUB_KEYS = &H8
Public Const KEY_NOTIFY = &H10
Public Const READ_CONTROL = &H20000
Public Const STANDARD_RIGHTS_READ = (READ_CONTROL)
Public Const SYNCHRONIZE = &H100000
Public Const KEY_READ = ((STANDARD_RIGHTS_READ Or KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY) And (Not SYNCHRONIZE))

Public Const REG_SZ = 1                         ' Unicode nul terminated string
Public Const REG_BINARY = 3                     ' Free form binary
Public Const REG_DWORD = 4                      ' 32-bit number
Public Const REG_NONE = 0                       ' No value type

Public Const KEY_SET_VALUE = &H2

Public Const REG_OPTION_NON_VOLATILE = 0       ' Key is preserved when system is rebooted

Type SECURITY_ATTRIBUTES
        nLength As Long
        lpSecurityDescriptor As Long
        bInheritHandle As Long
End Type

'##### Fim: Tratamento do Registry #####

' Inicio: Fun��es de Base para a Biblioteca Gen�rica (N�o conv�m utilizar estas)
Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, ByVal lpData As String, lpcbData As Long) As Long         ' Note that if you declare the lpData parameter as String, you must pass it By Value.
Declare Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, ByVal lpData As String, ByVal cbData As Long) As Long          ' Note that if you declare the lpData parameter as String, you must pass it By Value.
Declare Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal Reserved As Long, ByVal lpClass As String, ByVal dwOptions As Long, ByVal samDesired As Long, lpSecurityAttributes As SECURITY_ATTRIBUTES, phkResult As Long, lpdwDisposition As Long) As Long
Declare Function RegDeleteKey Lib "advapi32.dll" Alias "RegDeleteKeyA" (ByVal hKey As Long, ByVal lpSubKey As String) As Long
Declare Function RegDeleteValue Lib "advapi32.dll" Alias "RegDeleteValueA" (ByVal hKey As Long, ByVal lpValueName As String) As Long
' Fim: Fun��es de Base para a Biblioteca Gen�rica (N�o conv�m utilizar estas)

'-------------------
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hWndLock As Long) As Long
Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Declare Function GetSystemDirectory Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Declare Function GetWindowsDirectory Lib "kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'-------------------

Global Const mediRegLeft = 1  ' Para utilizar nas fun��es do Registry
Global Const mediRegRight = 2 ' Para utilizar nas fun��es do Registry

'------------------------------------
Global Const bgNome = "Biblioteca Gen�rica"
Global Const bgVersao = "1.14"
Global Const bgData = "19-04-1999"
Global Const bgAutor = "Copyright (c) 1997-1999, CPCis"

Public Type RGB
    t_R As Integer
    t_G As Integer
    t_B As Integer
    
End Type

Public Enum ApplicationHexadecimalColors
    
    e_LightYellow = &H80000018
    e_LightGray = &H8000000F
    e_White = &HFFFFFF
    e_Green = &HC000&
    e_LightGreen = &HC0FFC0
    e_Red = &HC0&
    e_LightRed = &H8080FF
    e_Blue = &HC0C000
    e_Gray = &H808080
    e_Yellow = &HFFFF&
    e_Orange = &H80C0FF
    e_Violet = &HFF00FF
    e_olive = &HC0C0&
End Enum

'Sub BG_LogFile(msg As String)
'
'    On Error Resume Next
'
'    Dim iLogFile As Integer
'
'    iLogFile = FreeFile
'    Open gDirCliente & "\Bin\Logs\SISLAB_" & "LogFile.Log" For Append As #iLogFile
'    Print #iLogFile, "-----------------------------------------------------------------"
'    Print #iLogFile, gCodUtilizador & " - " & gComputador & " - " & gNumeroSessao
'    Print #iLogFile, " "
'    Print #iLogFile, msg
'    Print #iLogFile, "-----------------------------------------------------------------"
'    Close #iLogFile
'
'End Sub
'
Public Function BG_NomeFichHoje() As String

    On Error GoTo ErrorHandler
    
    Dim aux_data  As Date
    Dim ret As String
    
    aux_data = Date
    
    ret = ""
    ret = ret & CStr(DatePart("yyyy", aux_data))
    ret = ret & "_" & Right("0" & CStr(DatePart("m", aux_data)), 2)
    ret = ret & "_" & Right("0" & CStr(DatePart("d", aux_data)), 2)
    
    BG_NomeFichHoje = ret

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_NomeFichHoje = "LogFile_Erros"
            Exit Function
    End Select
End Function

Sub BG_LogFile_Erros(msg As String, Optional nomeForm As String, Optional nomeProc As String, Optional MostraMensagem As Boolean)
    
    On Error Resume Next
    
    Dim iLogFile As Integer

    iLogFile = FreeFile
    Open gDirCliente & "\Bin\Logs\SISLAB_" & BG_NomeFichHoje & ".Log" For Append As #iLogFile
    Print #iLogFile, "---------------------------------------------------------------------------------------------"
    Print #iLogFile, Date & " - " & time
    Print #iLogFile, " "
    If nomeForm <> "" And nomeProc <> "" Then
        Print #iLogFile, nomeForm & " - " & nomeProc
    End If
    Print #iLogFile, msg
    Print #iLogFile, "---------------------------------------------------------------------------------------------"
    Close #iLogFile

    If MostraMensagem = True Then
        BG_Mensagem mediMsgBox, "Erro: " & msg, vbOKOnly + vbCritical, nomeForm & " - " & nomeProc
    End If
    If gConexao.state = adStateOpen Then
        BL_LogFile_BD nomeForm, nomeProc, "", msg, ""
    End If
End Sub

Sub BG_Escr_Fich(fich As String, msg As String)
    
    Dim iLogFile As Integer

    On Error Resume Next

    iLogFile = FreeFile
    Open fich For Append As #iLogFile
    Print #iLogFile, msg
    Close #iLogFile

End Sub

Sub BG_DaCoordenadas(nomeForm As Form)
    
    Dim StrAux As String

    StrAux = "Coordenadas de: '" & nomeForm.caption & "'" & vbCrLf & vbCrLf

    StrAux = StrAux & "Left .......:" & nomeForm.left & vbCrLf
    StrAux = StrAux & "Top .......:" & nomeForm.top & vbCrLf
    StrAux = StrAux & "Width ....:" & nomeForm.Width & vbCrLf
    StrAux = StrAux & "Height ...:" & nomeForm.Height

    BG_Mensagem mediMsgBox, StrAux

End Sub

Sub BG_DefTipoCampoEc(NomeTabela As String, ByVal NomeCampoBD As String, ByVal NomeControl As Control, TipoCampo As Integer, Optional DecimalLength As Variant, Optional DecimalScale As Variant)
    
    Dim nTipo As Integer
    Dim BD As Database
    
    If TypeOf NomeControl Is TextBox Or TypeOf NomeControl Is Label Or TypeOf NomeControl Is MaskEdBox Or TypeOf NomeControl Is RichTextBox Then
        ' Continua a defini��o do tipo de campo.
    ElseIf TypeOf NomeControl Is ComboBox Then
        Exit Sub
    ElseIf TypeOf NomeControl Is CheckBox Then
        Exit Sub
    Else
        Exit Sub
    End If

    If NomeControl.Tag <> "" Then Exit Sub ' Por causa de 'BG_DefTipoCampoEc_Todos'
    
    If TypeOf NomeControl Is MaskEdBox Then
        NomeControl.AllowPrompt = True
        NomeControl.ClipMode = mskExcludeLiterals
        NomeControl.FontUnderline = False
        NomeControl.PromptInclude = False
    
        NomeControl.PromptChar = "_"
        NomeControl.AutoTab = False
    End If

    nTipo = BGAux_DaFieldObjectProperties(BD, NomeTabela, NomeCampoBD, "Type")
    
    If TipoCampo = mediTipoDefeito Then
        NomeControl.Tag = nTipo
    Else
        NomeControl.Tag = TipoCampo
    End If
    
    Select Case TipoCampo
    Case mediTipoDefeito
        Select Case nTipo
        Case dbBoolean
            ' Tratado por defeito
        Case dbByte
            ' Tratado por defeito
        Case dbInteger
            ' Tratado por defeito
        Case dbLong
            ' Tratado por defeito
        Case dbCurrency
            ' N�o tratado!
        Case dbSingle
            If (IsMissing(DecimalLength) And Not IsMissing(DecimalScale)) Or (Not IsMissing(DecimalLength) And IsMissing(DecimalScale)) Then
                BG_Mensagem mediMsgBox, "Falta par�metro 'DecimalLength' ou 'DecimalScale' em <DefTipoCampos> !", vbExclamation, "BG_DefTipoCampoEc - '" & NomeControl.Name & "'"
            ElseIf (Not IsMissing(DecimalLength) And Not IsMissing(DecimalScale)) Then
                NomeControl.Tag = NomeControl.Tag & "(" & CInt(DecimalLength) & "," & CInt(DecimalScale) & ")"
                NomeControl.MaxLength = DecimalLength + 1
                If DecimalLength <= DecimalScale Then
                    BG_Mensagem mediMsgBox, "'DecimalLength' ter� de ser maior do que 'DecimalScale' em <DefTipoCampos> !", vbExclamation, "BG_DefTipoCampoEc - '" & NomeControl.Name & "'"
                End If
            End If
        Case dbDouble
            If (IsMissing(DecimalLength) And Not IsMissing(DecimalScale)) Or (Not IsMissing(DecimalLength) And IsMissing(DecimalScale)) Then
                BG_Mensagem mediMsgBox, "Falta par�metro 'DecimalLength' ou 'DecimalScale' em <DefTipoCampos> !", vbExclamation, "BG_DefTipoCampoEc - '" & NomeControl.Name & "'"
            ElseIf (Not IsMissing(DecimalLength) And Not IsMissing(DecimalScale)) Then
                NomeControl.Tag = NomeControl.Tag & "(" & CInt(DecimalLength) & "," & CInt(DecimalScale) & ")"
                NomeControl.MaxLength = DecimalLength + 1
                If DecimalLength <= DecimalScale Then
                    BG_Mensagem mediMsgBox, "'DecimalLength' ter� de ser maior do que 'DecimalScale' em <DefTipoCampos> !", vbExclamation, "BG_DefTipoCampoEc - '" & NomeControl.Name & "'"
                End If
            End If
        Case dbDate
            BG_Mensagem mediMsgBox, "Em vez de utilizar 'mediTipoDefeito' para o campo Data ou Hora, utilizar 'mediTipoData' ou 'mediTipoHora', consoante o caso!", vbExclamation, "BG_DefTipoCampoEc - '" & NomeControl.Name & "'"
        Case dbText
            'NomeControl.Mask = String(BD.TableDefs(NomeTabela).Fields(NomeCampoBD).Size, "C")
            NomeControl.MaxLength = BGAux_DaFieldObjectProperties(BD, NomeTabela, NomeCampoBD, "Size")
        Case dbLongBinary
            ' N�o tratado!
        Case dbMemo
            ' Tratado por defeito
        Case Else
            ' N�o tratado!
        End Select
    Case mediTipoMaiusculas
        If nTipo = dbText Then
            NomeControl.MaxLength = BD.TableDefs(NomeTabela).Fields(NomeCampoBD).Size
        Else
            NomeControl = "(Erro na defini��o do tipo de campo)"
        End If
    Case mediTipoMinusculas
        If nTipo = dbText Then
            NomeControl.MaxLength = BD.TableDefs(NomeTabela).Fields(NomeCampoBD).Size
        Else
            NomeControl = "(Erro na defini��o do tipo de campo)"
        End If
    Case mediTipoCapitalizado
        If nTipo = dbText Then
            NomeControl.MaxLength = BD.TableDefs(NomeTabela).Fields(NomeCampoBD).Size
        Else
            NomeControl = "(Erro na defini��o do tipo de campo)"
        End If
    Case mediTipoData
        If nTipo = dbText Or nTipo = dbDate Then
            ' Tratado por defeito
        Else
            NomeControl = "(Erro na defini��o do tipo de campo)"
        End If
    Case mediTipoHora
        If nTipo = dbText Or nTipo = dbDate Then
            ' Tratado por defeito
        Else
            NomeControl = "(Erro na defini��o do tipo de campo)"
        End If
    Case Else
        ' N�o tratado!
    End Select
End Sub

Sub BG_TrataErro(NomeRotina As String, NumErro As Long, Optional Comentario As Variant)

    gSQLError = 0
    gSQLISAM = 0

    If IsMissing(Comentario) Then
        Comentario = ""
    Else
        Comentario = "(" & Comentario & ")"
    End If

    If gModoDebug = 1 Then
        Beep
        BG_Mensagem mediMsgBox, NumErro & " - " & Error(NumErro) & vbCrLf & vbCrLf & Comentario, vbExclamation, "Erro na rotina: " & NomeRotina
    End If

    
    If Not gConexao Is Nothing Then
    
        If gConexao.Errors.Count <> 0 Then
    '        Debug.Print "0 NUMBER :" & gConexao.Errors(0).Number
    '        Debug.Print "0 DESCRIPTION :" & gConexao.Errors(0).Description
    '        Debug.Print "0 HELPCONTEXT :" & gConexao.Errors(0).HelpContext
    '        Debug.Print "0 HELPFILE :" & gConexao.Errors(0).HelpFile
    '        Debug.Print "0 NATIVEERROR :" & gConexao.Errors(0).NativeError
    '        Debug.Print "0 SOURCE :" & gConexao.Errors(0).Source
    '        Debug.Print "0 SQLSTATE :" & gConexao.Errors(0).SQLState
        
            gSQLError = gConexao.Errors(0).NativeError
            BG_LogFile_Erros NomeRotina & ": SQL Error : " & gConexao.Errors(0).NativeError & "  " & gConexao.Errors(0).Description
            If gConexao.Errors.Count > 1 Then
    '            Debug.Print "1 NUMBER :" & gConexao.Errors(1).Number
    '            Debug.Print "1 DESCRIPTION :" & gConexao.Errors(1).Description
    '            Debug.Print "1 HELPCONTEXT :" & gConexao.Errors(1).HelpContext
    '            Debug.Print "1 HELPFILE :" & gConexao.Errors(1).HelpFile
    '            Debug.Print "1 NATIVEERROR :" & gConexao.Errors(1).NativeError
    '            Debug.Print "1 SOURCE :" & gConexao.Errors(1).Source
    '            Debug.Print "1 SQLSTATE :" & gConexao.Errors(1).SQLState
    
                gSQLISAM = gConexao.Errors(1).NativeError
                BG_LogFile_Erros NomeRotina & ": ISAM Error : " & gConexao.Errors(1).NativeError & "  " & gConexao.Errors(1).Description
            End If
            gConexao.Errors.Clear
        Else
            gSQLError = 0
            gSQLISAM = 0
        End If
        
    End If
    BG_LogFile_Erros NomeRotina & ": " & NumErro & " - " & Error(NumErro) & " " & Comentario
End Sub

Function BG_ProcuraEmFicheiro(FicheiroProc As String, StringProc As String, ModoDeComparacao As Integer, ByRef resNumLinha() As Integer, ByRef resStrLinha() As String, ByRef NumOcorrencias As Integer) As Integer
'   Exemplo:
'   -------
'
'    Dim iRes As Integer
'    Dim iNumLinha() As Integer
'    Dim sStrLinha() As String
'    Dim iNumOc As Integer
'    Dim i As Integer
'
'    iRes = BG_ProcuraEmFicheiro("C:\autoexec.bat", "path", 1, iNumLinha, sStrLinha, iNumOc)
'    MsgBox "Resultado: " & iRes & vbCrLf & vbCrLf & "Numero de Ocorr�ncias: " & iNumOc, , "..."
'    Text1 = ""
'    For i = 0 To iNumOc - 1
'        Text1 = Text1 & iNumLinha(i) & " - " & sStrLinha(i) & vbCrLf
'    Next i
'
    Dim iFile As Integer
    Dim iContaLinhas, iContaPosicoes As Integer
    Dim tam, TamOffset As Integer
    Dim sStr1 As String

    On Error GoTo TrataErro

    tam = 10
    TamOffset = 10
    ReDim resNumLinha(tam)
    ReDim resStrLinha(tam)
    
    iContaPosicoes = 0
    iContaLinhas = 0
    iFile = FreeFile
    Open FicheiroProc For Input As #iFile
        Do While Not EOF(iFile)
            Line Input #1, sStr1
            iContaLinhas = iContaLinhas + 1
            If (InStr(1, sStr1, StringProc, ModoDeComparacao) > 0) Then
                resNumLinha(iContaPosicoes) = iContaLinhas
                resStrLinha(iContaPosicoes) = sStr1
                iContaPosicoes = iContaPosicoes + 1
                
                If iContaPosicoes = tam Then
                    tam = tam + TamOffset
                    ReDim Preserve resNumLinha(tam)
                    ReDim Preserve resStrLinha(tam)
                End If
            End If
        Loop
    Close #iFile

    NumOcorrencias = iContaPosicoes
    BG_ProcuraEmFicheiro = 0 ' Correu tudo bem!
    
    Exit Function

TrataErro:
    BG_TrataErro "BG_ProcuraEmFicheiro", Err

    Select Case Err
    Case 53 'File Not Found
        BG_ProcuraEmFicheiro = -1
    Case 76 'Path Not Found
        BG_ProcuraEmFicheiro = -2
    Case Else
    End Select

    Exit Function

End Function

Function BG_PreencheCampoEc(NomeRecordset As recordset, ByVal NomeCampoBD As String, ByVal NomeControl As Control) As Integer
    On Error GoTo TrataErro
    
    If TypeOf NomeControl Is TextBox Or TypeOf NomeControl Is Label Or TypeOf NomeControl Is MaskEdBox Or TypeOf NomeControl Is RichTextBox Then
        
        If IsNull(NomeRecordset(NomeCampoBD)) Then
            NomeControl = ""
        Else
            NomeControl = NomeRecordset(NomeCampoBD)

            If left(NomeControl.Tag, 1) = dbSingle Or left(NomeControl.Tag, 1) = dbDouble Then
                NomeControl = BG_CvDecimalParaDisplay(NomeControl)
            ElseIf NomeControl.Tag = mediTipoData Then
                NomeControl = BG_CvData(NomeControl)
            ElseIf NomeControl.Tag = mediTipoHora Then
                NomeControl = BG_CvHora(NomeControl)
            End If
        End If
        
    ElseIf TypeOf NomeControl Is ComboBox Then
    
        If IsNull(NomeRecordset(NomeCampoBD)) Then
            NomeControl.ListIndex = -1
        Else
            BG_MostraComboSel NomeRecordset(NomeCampoBD), NomeControl
        End If
    
    ElseIf TypeOf NomeControl Is CheckBox Then
    
        If IsNull(NomeRecordset(NomeCampoBD)) Then
            NomeControl.value = 2
        Else
            NomeControl.value = NomeRecordset(NomeCampoBD)
        End If
    
    Else
        ' Nada
    End If
    
    
    BG_PreencheCampoEc = 0
    Exit Function

TrataErro:
    BG_PreencheCampoEc = -1
    Exit Function

End Function

Sub BG_PreencheComboBD(NomeTabelaBD As String, NomeCampoBD_codigo As String, NomeCampoBD_Descr As String, NomeControl As Control, Optional TipoOrdenacao1 As Variant, Optional TipoOrdenacao2 As Variant)
    
    Dim obTabelaAux As Dynaset
    Dim CriterioAux As String
    Dim CriterioInicial As String
    Dim iTabelaOuQuery As Integer
    Dim i As Integer
    Dim BD As Database
    
    On Error GoTo TrataErro
    
    'Inicio: Verificar se trata sobre Tabela ou sobre Query
    iTabelaOuQuery = InStr(StrConv(NomeTabelaBD, vbUpperCase), "SELECT ")
    If iTabelaOuQuery = 1 Then
        CriterioInicial = NomeTabelaBD
    Else
        CriterioInicial = "SELECT " & NomeCampoBD_codigo & ", " & NomeCampoBD_Descr & " FROM " & NomeTabelaBD
    End If
    'Fim: Verificar se trata sobre Tabela ou sobre Query
        
    'Inicio: Crit�rio de Ordena��o
    If IsMissing(TipoOrdenacao1) Then
        CriterioAux = CriterioInicial
    Else
        If TipoOrdenacao1 = mediAscComboCodigo Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_codigo & " ASC"
        ElseIf TipoOrdenacao1 = mediDescComboCodigo Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_codigo & " DESC"
        ElseIf TipoOrdenacao1 = mediAscComboDesignacao Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_Descr & " ASC"
        ElseIf TipoOrdenacao1 = mediDescComboDesignacao Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_Descr & " DESC"
        End If
        
        If Not IsMissing(TipoOrdenacao2) Then
            If TipoOrdenacao2 = mediAscComboCodigo Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_codigo & " ASC"
            ElseIf TipoOrdenacao2 = mediDescComboCodigo Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_codigo & " DESC"
            ElseIf TipoOrdenacao2 = mediAscComboDesignacao Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_Descr & " ASC"
            ElseIf TipoOrdenacao2 = mediDescComboDesignacao Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_Descr & " DESC"
            End If
        End If
    End If
    'Fim: Crit�rio de Ordena��o

    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioAux
    Set obTabelaAux = BD.OpenRecordset(CriterioAux, dbOpenDynaset, dbReadOnly)

    NomeControl.Clear

    i = 0
    Do Until obTabelaAux.EOF
        NomeControl.AddItem obTabelaAux(NomeCampoBD_Descr)
        NomeControl.ItemData(i) = CLng(obTabelaAux(NomeCampoBD_codigo))
        obTabelaAux.MoveNext
        i = i + 1
    Loop

    obTabelaAux.Close

    Exit Sub

TrataErro:
    
    MsgBox "Teste"
    
    If Err = 6 Then
        NomeControl.List(i) = "!!Sup. 32767: " & NomeControl.List(i)
        NomeControl.ItemData(i) = -10
        BG_TrataErro "BG_PreencheComboBD", Err, "'" & NomeTabelaBD & "': Valor Superior a 32767!"
    Else
        BG_TrataErro "BG_PreencheComboBD", Err
    End If
    Resume Next

End Sub

Sub BG_PreencheCombo(ValoresCombo As Variant, NomeControl As Control, Optional Apagar As Variant)
    
    Dim i As Integer, r As Integer
    Dim indice As Integer, ValTemp As Integer
    Dim valor As Variant
    Dim MeuDebug As Integer
    Dim sTemp As String

    MeuDebug = 0
    If MeuDebug = 1 Then BG_LogFile_Erros "BG_PreencheCombo: " & "'" & NomeControl.Name & "'"

    If Not IsMissing(Apagar) Then
        sTemp = valor
        If StrComp(sTemp, "Clear", 0) Then
            NomeControl.Clear
            If MeuDebug = 1 Then BG_LogFile_Erros "  <Clear> - Limpa ComboBox"
        End If
    End If

    indice = 0: i = 0
    For Each valor In ValoresCombo
        r = i Mod 2
        If r = 0 Then
            If IsNumeric(valor) Then
                ValTemp = CInt(valor)
            Else
                Exit For
            End If
        Else
            If MeuDebug = 1 Then BG_LogFile_Erros "    (" & CStr(indice) & ")          '" & CStr(ValTemp) & "' - '" & valor & "'"
            NomeControl.AddItem valor
            NomeControl.ItemData(indice) = ValTemp
            indice = indice + 1
        End If
        
        i = i + 1
    Next

End Sub
Sub BG_MostraComboSel(ByVal ValorCampoBD As Variant, NomeControl As Control)
    Dim i As Integer
    Dim Seleccionado As Integer

    If IsNull(ValorCampoBD) Or Not IsNumeric(ValorCampoBD) Then
        NomeControl.ListIndex = -1
        Exit Sub
    End If

    i = 0
    Seleccionado = False
    While (Seleccionado = False) And (i < NomeControl.ListCount)
        If NomeControl.ItemData(i) = CLng(ValorCampoBD) Then
            NomeControl.ListIndex = i
            Seleccionado = True
        End If
        i = i + 1
    Wend

    If Seleccionado = False Then
        NomeControl.ListIndex = -1
    End If
End Sub
Function BG_DaComboSel(ByVal NomeControl As Control) As Long
    
    If NomeControl.ListIndex = -1 Then
        BG_DaComboSel = mediComboValorNull
    Else
        BG_DaComboSel = NomeControl.ItemData(NomeControl.ListIndex)
    End If

End Function

Function BG_ConstroiCriterio_INSERT(NomeTabela As String, NomesCampo As Variant, NomesControl As Variant) As String
    Dim Criterio, CriterioAux As String
    Dim ValorCampo, ValorControl As Variant
    Dim i, nPassou, nTipo As Integer
    Dim BD As Database

    i = 0
    nPassou = 0
    Criterio = "INSERT INTO " & NomeTabela & " ("
    CriterioAux = ") VALUES ("
    For Each ValorCampo In NomesCampo
        If TypeOf NomesControl(i) Is TextBox Or TypeOf NomesControl(i) Is Label Or TypeOf NomesControl(i) Is MaskEdBox Or TypeOf NomesControl(i) Is RichTextBox Then
            ValorControl = NomesControl(i)
            ValorControl = BG_CvPlica(ValorControl)
        ElseIf TypeOf NomesControl(i) Is ComboBox Then
            ValorControl = BG_DaComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        ElseIf TypeOf NomesControl(i) Is CheckBox Then
            If NomesControl(i).value = 0 Then
                ValorControl = "0"
            ElseIf NomesControl(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        Else
            ' Nada
        End If
        
        If gBD_LocalOuRemota = "LOCAL" Then
            If StrComp(ValorCampo, "percent", 0) = 0 Then
                ValorCampo = "[PERCENT]"
            End If
        End If
        
        If nPassou <> 0 Then
            Criterio = Criterio & ", "
            CriterioAux = CriterioAux & ", "
        End If
        
        Criterio = Criterio & ValorCampo
        
        nTipo = BGAux_DaFieldObjectProperties(BD, NomeTabela, ValorCampo, "Type")
        
        If ValorControl <> "" Then
            If nTipo = dbSingle Or nTipo = dbDouble Then
                CriterioAux = CriterioAux & "'" & BG_CvDecimalParaWhere(ValorControl) & "'"
            ElseIf nTipo = dbDate Then
                If NomesControl(i).Tag = mediTipoData Then CriterioAux = CriterioAux & "'" & BG_CvData(ValorControl) & "'"
                If NomesControl(i).Tag = mediTipoHora Then CriterioAux = CriterioAux & "'" & BG_CvHora(ValorControl) & "'"
            Else
                CriterioAux = CriterioAux & "'" & ValorControl & "'"
            End If
        Else
            CriterioAux = CriterioAux & "Null"
        End If
        
        nPassou = 1
        i = i + 1
    Next
    Criterio = Criterio & CriterioAux & ")"

    ' Mensagens
'    BG_Mensagem mediMsgBox, Criterio, , "BG_ConstroiCriterio_INSERT"
    BG_LogFile_Erros "BG_ConstroiCriterio_INSERT:"
    BG_LogFile_Erros "    " & Criterio

    ' Retorno de resultados
    BG_ConstroiCriterio_INSERT = Criterio

End Function

Public Function BG_CalculaIdade(DataNasc As Date, Optional DataChega As Date) As String
    Dim mes As Integer
    Dim dia As Integer
    Dim idade As Integer
    Dim data As Date
    On Error GoTo TrataErro
    If IsNull(DataChega) Or IsMissing(DataChega) Or DataChega = "0:00:00" Then
        data = Now
    Else
        data = DataChega
    End If
    
    If Month(DataNasc) = Month(data) Then
        mes = -1
    ElseIf Month(DataNasc) < Month(data) Then
        mes = 1
    Else
        mes = 0
    End If
    
    If Day(DataNasc) <= Day(data) Then
        dia = 1
    Else
        dia = 0
    End If
    
    Select Case 2 * mes + dia
        Case -2, 0, 1
            idade = DateDiff("yyyy", DataNasc, data) - 1
            BG_CalculaIdade = idade & IIf(idade = 1, " ano", " anos")
            If idade < 1 Then
                If dia = 1 Then
                    idade = DateDiff("m", DataNasc, data)
                Else
                    idade = DateDiff("m", DataNasc, data) - 1
                End If
                BG_CalculaIdade = idade & IIf(idade = 1, " m�s", " meses")
            End If
            If idade < 1 Then
                idade = DateDiff("d", DataNasc, data) - 1
                BG_CalculaIdade = idade & IIf(idade = 1, " dia", " dias")
            End If
        Case -1, 2, 3
            idade = DateDiff("yyyy", DataNasc, data)
            BG_CalculaIdade = idade & IIf(idade = 1, " ano", " anos")
            If idade < 1 Then
                If dia = 0 Then
                    idade = DateDiff("m", DataNasc, data) - 1
                Else
                    idade = DateDiff("m", DataNasc, data)
                End If
                BG_CalculaIdade = idade & IIf(idade = 1, " m�s", " meses")
            End If
            If idade < 1 Then
                idade = DateDiff("d", DataNasc, data)
                BG_CalculaIdade = idade & IIf(idade = 1, " dia", " dias")
            End If
    End Select
    Exit Function
TrataErro:
    BG_CalculaIdade = -1
    BG_LogFile_Erros "Erro  Calcular Idade DtN: " & DataNasc & " DtC:" & DataChega & " : " & Err.Description, "BG", "BG_CalculaIdade", True
    Exit Function
    Resume Next
End Function

Function BG_CvPlica(ByVal sEntrada As String, Optional iArg As Variant) As String

    Dim sSaida As String
    Dim sRestante As String
    Dim sPlica As String
    Dim sPlicaNova As String
    Dim i As Integer
    
    If IsMissing(iArg) = True Then
        iArg = 2
    End If
    
    Select Case iArg
    
    Case 1
        sPlica = "'"
        sPlicaNova = "" ' Nada -> Tira Plica
        sSaida = ""
        sRestante = sEntrada
        
        i = InStr(sRestante, sPlica)
        While i <> 0
            sSaida = sSaida & left(sRestante, i - 1) & sPlicaNova
            sRestante = Right(sRestante, Len(sRestante) - i)
            i = InStr(sRestante, sPlica)
        Wend
        sSaida = sSaida & sRestante
    Case 2
        sPlica = "'"
        sPlicaNova = "'" ' Coloca outra plica para tirar o significado que ela tem de fechar uma string.
        sSaida = ""
        sRestante = sEntrada
        
        i = InStr(sRestante, sPlica)
        While i <> 0
            sSaida = sSaida & left(sRestante, i) & sPlicaNova
            sRestante = Right(sRestante, Len(sRestante) - i)
            i = InStr(sRestante, sPlica)
        Wend
        sSaida = sSaida & sRestante
    Case 3
        sPlica = "'"
        sPlicaNova = "�" ' Substitui plica por acento (N�o funciona bem com as RichText Boxes)
        sSaida = ""
        sRestante = sEntrada
        
        i = InStr(sRestante, sPlica)
        While i <> 0
            sSaida = sSaida & left(sRestante, i - 1) & sPlicaNova
            sRestante = Right(sRestante, Len(sRestante) - i)
            i = InStr(sRestante, sPlica)
        Wend
        sSaida = sSaida & sRestante
    End Select
    
    BG_CvPlica = sSaida

'----------------------------------------------------

'    Dim sSaida, sRestante As String
'    Dim sPlica, sPlicaNova As String
'    Dim i As Integer
'
'    sPlica = "'"
'    sPlicaNova = "'" ' Coloca outra plica para tirar o significado que ela tem de fechar uma string.
'    sSaida = ""
'    sRestante = sEntrada
'
'    i = InStr(sRestante, sPlica)
'    While i <> 0
'        sSaida = sSaida & Left(sRestante, i) & sPlicaNova
'        sRestante = Right(sRestante, Len(sRestante) - i)
'        i = InStr(sRestante, sPlica)
'    Wend
'    sSaida = sSaida & sRestante
'
'    BG_CvPlica = sSaida

'----------------------------------------------------
    'Dim sSaida, sRestante As String
    'Dim sPlica, sPlicaNova As String
    'Dim i As Integer
    
    'sPlica = "'"
    'sPlicaNova = "" ' Nada -> Tira Plica
    'sSaida = ""
    'sRestante = sEntrada
    
    'i = InStr(sRestante, sPlica)
    'While i <> 0
    '    sSaida = sSaida & Left(sRestante, i - 1) & sPlicaNova
    '    sRestante = Right(sRestante, Len(sRestante) - i)
    '    i = InStr(sRestante, sPlica)
    'Wend
    'sSaida = sSaida & sRestante
    
    'BG_CvPlica = sSaida

'----------------------------------------------------
    'Dim sSaida, sRestante As String
    'Dim sPlica, sPlicaNova As String
    'Dim i As Integer
    
    'sPlica = "'"
    'sPlicaNova = "�" ' Substitui plica por acento (N�o funciona bem com as RichText Boxes)
    'sSaida = ""
    'sRestante = sEntrada
    
    'i = InStr(sRestante, sPlica)
    'While i <> 0
    '    sSaida = sSaida & Left(sRestante, i - 1) & sPlicaNova
    '    sRestante = Right(sRestante, Len(sRestante) - i)
    '    i = InStr(sRestante, sPlica)
    'Wend
    'sSaida = sSaida & sRestante
    
    'BG_CvPlica = sSaida
End Function

Public Function BG_PrecoExtenso(ByVal preco) As String
    
    Dim PrecoStr As String
    Dim PosCur, posicao, Grupo As Integer
    Dim i, Digit As Integer

    If preco >= 1000000000 Then
        Exit Function
    End If

    PrecoStr = CStr(preco)
    Grupo = CInt(Len(PrecoStr) / 3 + 0.45)
    PosCur = 1
    
    Do While (Grupo)
        Select Case Grupo
            Case 1
                posicao = Len(PrecoStr)
                BG_PrecoExtenso = BG_PrecoExtenso & BG_PrecoExtenso_Aux(PrecoStr, Grupo, posicao, (PosCur))
            Case 2
                If CInt(Mid(PrecoStr, 1, 3)) <> 0 Then
                    posicao = Len(PrecoStr) - 3
                    BG_PrecoExtenso = BG_PrecoExtenso & BG_PrecoExtenso_Aux(PrecoStr, Grupo, posicao, (PosCur))
                    BG_PrecoExtenso = BG_PrecoExtenso & " MIL"
                    Digit = 0
                    For i = PosCur To Len(PrecoStr)
                        If CInt(Mid(PrecoStr, i, 1)) <> 0 Then
                            Digit = Digit + 1
                        End If
                    Next i
                    Select Case Digit
                        Case 0
                            Grupo = 1
                        Case 1
                            BG_PrecoExtenso = BG_PrecoExtenso & " E"
                        Case 2
                            If CInt(Mid(PrecoStr, PosCur, 1)) = 0 Then
                                BG_PrecoExtenso = BG_PrecoExtenso & " E"
                            End If
                    End Select
                Else
                    PosCur = PosCur + 3
                End If
            Case 3
                posicao = Len(PrecoStr) - 6
                BG_PrecoExtenso = BG_PrecoExtenso & BG_PrecoExtenso_Aux(PrecoStr, Grupo, posicao, (PosCur))
                If Len(PrecoStr) = 7 And CInt(Mid(PrecoStr, 1, 1)) = 1 Then
                    BG_PrecoExtenso = BG_PrecoExtenso & " MILH�O"
                Else
                    BG_PrecoExtenso = BG_PrecoExtenso & " MILH�ES"
                End If
                Digit = 0
                For i = PosCur To Len(PrecoStr)
                        If CInt(Mid(PrecoStr, i, 1)) <> 0 Then
                            Digit = Digit + 1
                        End If
                Next i
                Select Case Digit
                    Case 0
                        Grupo = 1
                        BG_PrecoExtenso = BG_PrecoExtenso & " DE"
                        PosCur = PosCur + 6
                    Case 1
                        BG_PrecoExtenso = BG_PrecoExtenso & " E"
                        PosCur = PosCur + 3
                    Case 2
                        If CInt(Mid(PrecoStr, 4, 3)) = 0 And CInt(Mid(PrecoStr, PosCur, 1)) = 0 Then
                            BG_PrecoExtenso = BG_PrecoExtenso & " E"
                        End If
                   End Select
                   PrecoStr = Right(PrecoStr, 6)
        End Select
        Grupo = Grupo - 1
    Loop
    
    If preco = 0 Then
        BG_PrecoExtenso = " ZERO ESCUDOS"
    ElseIf preco = 1 Then
        BG_PrecoExtenso = BG_PrecoExtenso & " ESCUDO."
    Else
        BG_PrecoExtenso = BG_PrecoExtenso & " ESCUDOS."
    End If

End Function

Public Function BG_CryptStringDecode(ByVal sEntrada As String, Optional ChaveDeConversao As Variant) As String
'   Exemplos:
'
'   sRes = BG_CryptStringDecode("d+xd-����")
'   sRes = BG_CryptStringDecode("d+xd-����", "CHAVE_EXEMPLO")

    Dim i, j As Integer
    Dim sSaida, cTemp As String
    Dim CHAVE_DE_CONVERSAO As String

    'Tem que ser a mesma chave para encriptar e para desencriptar.
    If Not IsMissing(ChaveDeConversao) Then
        If ChaveDeConversao <> "" Then
            CHAVE_DE_CONVERSAO = CStr(ChaveDeConversao)
        Else
            CHAVE_DE_CONVERSAO = "Porto_2001"
        End If
    Else
        CHAVE_DE_CONVERSAO = "Porto_2001"
    End If

    sSaida = ""
    i = 1
    While (i <= Len(sEntrada))
        j = 1
        While ((i <= Len(sEntrada)) And (j <= Len(CHAVE_DE_CONVERSAO)))
            ' Subtrai-se 1, porque sen�o, se sEntrada tivesse alguma letra
            ' de ChaveDeConversao, dava uma string vazia, erradamente.
            cTemp = Chr((Asc(Mid(CHAVE_DE_CONVERSAO, j, 1))) Xor ((Asc(Mid(sEntrada, i, 1))) - 1))
            sSaida = sSaida & cTemp
            j = j + 1
            i = i + 1
        Wend
    Wend

    BG_CryptStringDecode = sSaida
    
End Function

Public Function BG_CryptStringEncode(ByVal sEntrada As String, Optional ChaveDeConversao As Variant) As String
'   Exemplos:
'
'   sRes = BG_CryptStringEncode("Porto")
'   sRes = BG_CryptStringEncode("Porto", "CHAVE_EXEMPLO")
    
    Dim i, j As Integer
    Dim sSaida, cTemp As String
    Dim CHAVE_DE_CONVERSAO As String

    'Tem que ser a mesma chave para encriptar e para desencriptar.
    If Not IsMissing(ChaveDeConversao) Then
        If ChaveDeConversao <> "" Then
            CHAVE_DE_CONVERSAO = CStr(ChaveDeConversao)
        Else
            CHAVE_DE_CONVERSAO = "Porto_2001"
        End If
    Else
        CHAVE_DE_CONVERSAO = "Porto_2001"
    End If

    sSaida = ""
    i = 1
    While (i <= Len(sEntrada))
        j = 1
        While ((i <= Len(sEntrada)) And (j <= Len(CHAVE_DE_CONVERSAO)))
            ' Soma-se 1, porque sen�o, se sEntrada tivesse alguma letra
            ' de ChaveDeConversao, dava uma string vazia, erradamente.
            cTemp = Chr(((Asc(Mid(CHAVE_DE_CONVERSAO, j, 1))) Xor ((Asc(Mid(sEntrada, i, 1))))) + 1)
            sSaida = sSaida & cTemp
            j = j + 1
            i = i + 1
        Wend
    Wend

    BG_CryptStringEncode = sSaida
    
End Function

Public Function BG_PrecoExtenso_Aux(ByVal PrecoStr As String, ByVal Grupo As Integer, ByVal posicao As Integer, ByRef PosCur As Integer) As String
    Dim Palavras(9, 3), PalavrasUnit(9) As String
    Dim CompPreco As Integer
    
    Palavras(1, 1) = " UM"
    Palavras(2, 1) = " DOIS"
    Palavras(3, 1) = " TR�S"
    Palavras(4, 1) = " QUATRO"
    Palavras(5, 1) = " CINCO"
    Palavras(6, 1) = " SEIS"
    Palavras(7, 1) = " SETE"
    Palavras(8, 1) = " OITO"
    Palavras(9, 1) = " NOVE"
    
    Palavras(1, 2) = " DEZ"
    Palavras(2, 2) = " VINTE"
    Palavras(3, 2) = " TRINTA"
    Palavras(4, 2) = " QUARENTA"
    Palavras(5, 2) = " CINQUENTA"
    Palavras(6, 2) = " SESSENTA"
    Palavras(7, 2) = " SETENTA"
    Palavras(8, 2) = " OITENTA"
    Palavras(9, 2) = " NOVENTA"
    
    Palavras(1, 3) = " CENTO"
    Palavras(2, 3) = " DUZENTOS"
    Palavras(3, 3) = " TREZENTOS"
    Palavras(4, 3) = " QUATROCENTOS"
    Palavras(5, 3) = " QUINHENTOS"
    Palavras(6, 3) = " SEISCENTOS"
    Palavras(7, 3) = " SETECENTOS"
    Palavras(8, 3) = " OITOCENTOS"
    Palavras(9, 3) = " NOVECENTOS"
    
    PalavrasUnit(1) = " ONZE"
    PalavrasUnit(2) = " DOZE"
    PalavrasUnit(3) = " TREZE"
    PalavrasUnit(4) = " CATORZE"
    PalavrasUnit(5) = " QUINZE"
    PalavrasUnit(6) = " DEZASSEIS"
    PalavrasUnit(7) = " DEZASSETE"
    PalavrasUnit(8) = " DEZOITO"
    PalavrasUnit(9) = " DEZANOVE"
    
    CompPreco = Len(PrecoStr)
    PosCur = 1
    
    Do While (posicao)
        Select Case posicao
            Case 1
                If Not (CompPreco = 4 And Grupo = 2 And InStr(PrecoStr, "1") = 1) Then
                    BG_PrecoExtenso_Aux = BG_PrecoExtenso_Aux & Palavras(CInt(Mid(PrecoStr, PosCur, 1)), 1)
                End If
            Case 2
                If CInt(Mid(PrecoStr, PosCur, 1)) <> 0 Then
                    If CInt(Mid(PrecoStr, PosCur + 1, 1)) = 0 Then
                        BG_PrecoExtenso_Aux = BG_PrecoExtenso_Aux & Palavras(CInt(Mid(PrecoStr, PosCur, 1)), 2)
                        posicao = 1
                        PosCur = PosCur + 1
                    ElseIf CInt(Mid(PrecoStr, PosCur, 1)) = 1 Then
                        PosCur = PosCur + 1
                        BG_PrecoExtenso_Aux = BG_PrecoExtenso_Aux & PalavrasUnit(CInt(Mid(PrecoStr, PosCur, 1)))
                        posicao = 1
                    Else
                        BG_PrecoExtenso_Aux = BG_PrecoExtenso_Aux & Palavras(CInt(Mid(PrecoStr, PosCur, 1)), 2)
                        BG_PrecoExtenso_Aux = BG_PrecoExtenso_Aux & " E"
                    End If
                End If
            Case 3
                If CInt(Mid(PrecoStr, PosCur, 1)) <> 0 Then
                    If CInt(Mid(PrecoStr, PosCur + 1, 2)) = 0 Then
                        If CInt(Mid(PrecoStr, PosCur, 1)) = 1 Then
                            BG_PrecoExtenso_Aux = BG_PrecoExtenso_Aux & " CEM"
                        Else
                            BG_PrecoExtenso_Aux = BG_PrecoExtenso_Aux & Palavras(CInt(Mid(PrecoStr, PosCur, 1)), 3)
                            posicao = 1
                            PosCur = PosCur + 2
                        End If
                    Else
                        BG_PrecoExtenso_Aux = BG_PrecoExtenso_Aux & Palavras(CInt(Mid(PrecoStr, PosCur, 1)), 3)
                        BG_PrecoExtenso_Aux = BG_PrecoExtenso_Aux & " E"
                    End If
                End If
        End Select
        posicao = posicao - 1
        PosCur = PosCur + 1
    Loop
End Function

Function BG_ExecutaQuery(MyDatabase As Database, SQLQuery As String, gBD_LocalOuRemota As String, Optional iMensagem) As Integer
'   O argumento 'iMensagem' serve para que seja apresentada uma mensagem caso a
' opera��o que se pretende n�o seja efectuada.
'
'   Hip�teses:
'       <Missing>     -> Aparece uma MsgBox
'       mediMsgBox    -> Aparece uma MsgBox
'       mediMsgStatus -> Aparece uma MsgStatus
'       -1            -> N�o aparece mensagem

    Dim sMsg As String
    Dim iRes As Integer
    
    On Error GoTo TrataErro
    
    If gBD_LocalOuRemota = "LOCAL" Then
        ' (dbSeeChanges por causa de tabelas com IDENTITY)
        MyDatabase.Execute SQLQuery, dbSeeChanges
    Else
        ' (dbSeeChanges por causa de tabelas com IDENTITY)
        MyDatabase.Execute SQLQuery, dbSQLPassThrough + dbSeeChanges ' Ver Transaccoes (ver help em Execute)
    End If
    
    If MyDatabase.RecordsAffected = 0 Then
        iRes = -1
        BG_LogFile_Erros "(BG_ExecutaQuery: RecordsAffected = 0) -> " & SQLQuery
        sMsg = "Esta opera��o pode n�o ter sido efectuada." & vbCrLf & vbCrLf & "Raz�o prov�vel: Integridade dos Dados."
        If IsMissing(iMensagem) Then
            BG_Mensagem mediMsgBox, sMsg, vbExclamation, "..."
        Else
            If iMensagem = mediMsgBox Then
                BG_Mensagem mediMsgBox, sMsg, vbExclamation, "..."
            ElseIf iMensagem = mediMsgStatus Then
                BG_Mensagem mediMsgStatus, sMsg, mediMsgBeep + 10
            ElseIf iMensagem = -1 Then
                ' N�o d� mensagem.
            End If
        End If
    Else
        iRes = 0
    End If
    
    BG_ExecutaQuery = iRes
    Exit Function

TrataErro:
    BG_TrataErro "BG_ExecutaQuery", Err
    BG_ExecutaQuery = -1
    Exit Function

End Function

Sub BG_TrataCheckBoxTripleState(gFormActivo As Object, Optional CodigoDeTecla)
    ' Chamar esta rotina nos eventos 'MouseDown' dos bot�es 'CheckBox'
    Dim CheckBoxActivo As Object
    
    If Not IsMissing(CodigoDeTecla) Then
        If CodigoDeTecla = 32 Then
            Exit Sub
        End If
    End If
    
    Set CheckBoxActivo = gFormActivo.ActiveControl
    CheckBoxActivo.Enabled = False
        
        If CheckBoxActivo.value = 0 Then
            CheckBoxActivo.value = 1
        ElseIf CheckBoxActivo.value = 1 Then
            CheckBoxActivo.value = 2
        ElseIf CheckBoxActivo.value = 2 Then
            CheckBoxActivo.value = 0
        End If
    
    CheckBoxActivo.Enabled = True
    CheckBoxActivo.SetFocus
End Sub

Sub BG_LogFile_Clear()

    On Error Resume Next
    
    Dim sStr As Variant

    sStr = Dir(gDirCliente & "\bin\" & "LogFile.Log")
    If sStr <> "" Then Kill gDirCliente & "\bin\" & "LogFile.Log"

End Sub

Function BG_ValidaCampo(sTitulo As String, NomeControl As Control, DesigCampo As String) As Integer
    
    On Error Resume Next
    
    'ALTERACAO: fazer o trim do conteudo do controlo para evitar
    '           inserir dados s� com espa�os
    ' M�rcio barreto
    If Trim(NomeControl) = "" Or (TypeOf NomeControl Is CheckBox And Trim(NomeControl) = vbGrayed) Then
        gMsgMsg = "Campo obrigat�rio: '" & DesigCampo & "'" & "!"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbOKOnly + vbExclamation, sTitulo)
        
        NomeControl.SetFocus
        
        BG_ValidaCampo = gMsgResp
    Else
        BG_ValidaCampo = 100
    End If

End Function

Sub BG_MensagemSeguinte()
    BG_Mensagem mediMsgStatus, "N�o existem mais registos !", mediMsgBeep, "Registo Seguinte"
'    Beep
'    BG_Mensagem mediMsgBox, "N�o existem mais registos !", vbExclamation, "Registo Seguinte"
End Sub

Sub BG_MensagemAnterior()
    BG_Mensagem mediMsgStatus, "N�o existem mais registos !", mediMsgBeep, "Registo Anterior"
'    Beep
'    BG_Mensagem mediMsgBox, "N�o existem mais registos !", vbExclamation, "Registo Anterior"
End Sub

Function BG_ConstroiCriterio_UPDATE(NomeTabela As String, NomesCampo As Variant, NomesControl As Variant, condicao As String) As String
    Dim Criterio As String
    Dim ValorCampo, ValorControl As Variant
    Dim i, nPassou, nTipo As Integer
    Dim BD As Database

    i = 0
    nPassou = 0
    Criterio = "UPDATE " & NomeTabela & " SET "
    For Each ValorCampo In NomesCampo
        If TypeOf NomesControl(i) Is TextBox Or TypeOf NomesControl(i) Is Label Or TypeOf NomesControl(i) Is MaskEdBox Or TypeOf NomesControl(i) Is RichTextBox Then
            ValorControl = NomesControl(i)
            ValorControl = BG_CvPlica(ValorControl)
        ElseIf TypeOf NomesControl(i) Is ComboBox Then
            ValorControl = BG_DaComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        ElseIf TypeOf NomesControl(i) Is CheckBox Then
            If NomesControl(i).value = 0 Then
                ValorControl = "0"
            ElseIf NomesControl(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        Else
            ' Nada
        End If
        
        If gBD_LocalOuRemota = "LOCAL" Then
            If StrComp(ValorCampo, "percent", 1) = 0 Then
                ValorCampo = "[PERCENT]"
            End If
        End If
        
        If nPassou <> 0 Then
            Criterio = Criterio & ", "
        End If
        
        nTipo = BGAux_DaFieldObjectProperties(BD, NomeTabela, ValorCampo, "Type")
        
        If ValorControl <> "" Then
            If nTipo = dbSingle Or nTipo = dbDouble Then
                Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvDecimalParaCalculo(ValorControl) & "'"
            ElseIf nTipo = dbDate Then
                If NomesControl(i).Tag = mediTipoData Then Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvData(ValorControl) & "'"
                If NomesControl(i).Tag = mediTipoHora Then Criterio = Criterio & ValorCampo & " = " & "'" & BG_CvHora(ValorControl) & "'"
            Else
                Criterio = Criterio & ValorCampo & " = " & "'" & ValorControl & "'"
            End If
        Else
            Criterio = Criterio & ValorCampo & " = " & "Null"
        End If
        
        nPassou = 1
        i = i + 1
    Next
    Criterio = Criterio & " WHERE " & condicao

    ' Mensagens
'    BG_Mensagem mediMsgBox, Criterio, , "BG_ConstroiCriterio_UPDATE"
    BG_LogFile_Erros "BG_ConstroiCriterio_UPDATE:"
    BG_LogFile_Erros "    " & Criterio

    ' Retorno de resultados
    BG_ConstroiCriterio_UPDATE = Criterio

End Function

Function BG_ConstroiCriterio_SELECT(NomeTabela As String, NomesCampo As Variant, NomesControl As Variant, SelTotal As Boolean) As String
    Dim Criterio As String
    Dim ValorCampo, ValorControl As Variant
    Dim nTipo, i, nPassou, iCount As Integer
    Dim res As Integer
    Dim sTempDecimal As String
    Dim iTempDecimal As Integer
    Dim BD As Database

    i = 0
    iCount = 0
    nPassou = 0
    Criterio = "SELECT * FROM " & NomeTabela & " WHERE "
    For Each ValorCampo In NomesCampo
        nTipo = BGAux_DaFieldObjectProperties(BD, NomeTabela, ValorCampo, "Type")
        
        If TypeOf NomesControl(i) Is TextBox Or TypeOf NomesControl(i) Is Label Or TypeOf NomesControl(i) Is MaskEdBox Or TypeOf NomesControl(i) Is RichTextBox Then
            ValorControl = NomesControl(i)
            ValorControl = BG_CvPlica(ValorControl)
        ElseIf TypeOf NomesControl(i) Is ComboBox Then
            ValorControl = BG_DaComboSel(NomesControl(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        ElseIf TypeOf NomesControl(i) Is CheckBox Then
            If NomesControl(i).value = 0 Then
                ValorControl = "0"
            ElseIf NomesControl(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        Else
            ' Nada
        End If
        
        If gBD_LocalOuRemota = "LOCAL" Then
            If StrComp(ValorCampo, "percent", 0) = 0 Then
                ValorCampo = "[PERCENT]"
            End If
        End If

        If ValorControl = "" Then
            iCount = iCount + 1
        Else
            If nPassou <> 0 Then
                Criterio = Criterio & " AND "
            End If

            Select Case nTipo
            Case dbSingle, dbDouble
                Criterio = Criterio & ValorCampo & " = " & BG_CvDecimalParaWhere(ValorControl)
            Case dbDate
                If NomesControl(i).Tag = CStr(mediTipoHora) Then
                    Criterio = Criterio & ValorCampo & " = " & "#" & BG_CvHora(ValorControl) & "#"
                Else ' Tipo Data
                    If gBD_LocalOuRemota = "LOCAL" Then
                        Criterio = Criterio & ValorCampo & " = " & BG_CvDataParaWhere(ValorControl)
                    Else
                        res = BG_DaValorData(ValorControl, "DIA")
                        If res > 12 Then
                            Criterio = Criterio & ValorCampo & " LIKE " & "#" & BG_CvData(ValorControl) & "#"
                        Else
                            Criterio = Criterio & ValorCampo & " = " & "#" & BG_CvData(ValorControl) & "#"
                        End If
                    End If
                End If
            Case dbText, dbMemo
                If InStr(ValorControl, "*") <> 0 Then
                    Criterio = Criterio & ValorCampo & " LIKE " & "'" & ValorControl & "'"
                Else
                    Criterio = Criterio & ValorCampo & " = " & "'" & ValorControl & "'"
                End If
            Case Else
                Criterio = Criterio & ValorCampo & " = " & ValorControl
            End Select
    
            nPassou = 1
        End If
        
        i = i + 1
    Next

    If iCount = i Then
        Criterio = "SELECT * FROM " & NomeTabela
        SelTotal = True
    End If
    
    ' Mensagens
'    BG_Mensagem mediMsgBox, Criterio, , "BG_ConstroiCriterio_SELECT"
    BG_LogFile_Erros "BG_ConstroiCriterio_SELECT:"
    BG_LogFile_Erros "    " & Criterio
    
    ' Retorno de resultados
    BG_ConstroiCriterio_SELECT = Criterio

End Function

Sub BG_MostraEstruturaBD(PathBD As String, CamposSimNao As Integer)
    Dim BD As Database
    Dim Tdf As TableDef
    Dim Fld As Field
    Dim i As Integer
    
    On Error GoTo TrataErro
    
    BG_LogFile_Erros ""
    BG_LogFile_Erros "********** BG_MostraEstruturaBD (In�cio) ******************************"
    
    Set BD = Workspaces(0).OpenDatabase(PathBD)

    For Each Tdf In BD.TableDefs
      If (Tdf.Attributes And dbAttachedTable) = dbAttachedTable Then
        If left(Tdf.Connect, 1) = ";" Then
          'must be a jet attached table
          BG_LogFile_Erros Tdf.Name & " -> Jet"
        Else
          'must be an ISAM attached table
          BG_LogFile_Erros Tdf.Name & " -> " & left(Tdf.Connect, InStr(Tdf.Connect, ";") - 1)
        End If
      ElseIf (Tdf.Attributes And dbAttachedODBC) = dbAttachedODBC Then
        BG_LogFile_Erros Tdf.Name & " -> ODBC"
      Else
        If (Tdf.Attributes And dbSystemObject) = 0 Then
            BG_LogFile_Erros Tdf.Name
        Else
            BG_LogFile_Erros Tdf.Name & " -> Tabela de Sistema"
        End If
      End If
      
      If CamposSimNao = 1 Then GoSub MostraCampos

    Next
    
    BD.Close

    BG_LogFile_Erros "********** BG_MostraEstruturaBD (Fim) *********************************"
    BG_LogFile_Erros ""
    
    Exit Sub
    
MostraCampos:
'    For i = 0 To tdf.Fields.Count - 1
'        BG_LogFile_Erros "    " & tdf.Fields(i).Name & " - " & tdf.Fields(i).Type
'    Next i
                
    For Each Fld In BD.TableDefs(Tdf.Name).Fields
        BG_LogFile_Erros "    " & Fld.Name & " - " & Fld.Type
    Next
    
    Return

TrataErro:
    Beep
    Resume Next

End Sub
Function BG_ValidaTipoCampo(nomeForm As Form, NomeControl As Control) As Boolean
    Dim nTipo, i As Integer
    Dim sTipos As Variant
    Dim Var1, Var2 As Variant
    Dim nCorrecto As Boolean
    Dim DecimalLength, DecimalScale As Integer
    Dim pos, pos1 As Integer
    Dim s1, s2 As String
    Dim Decimal_tag As String
    Dim ErroDecimal As Integer

    Dim iComp As Integer
    Dim PrimeiraLetra, TemEspaco As Boolean

    '--------------------------------------
    
    On Error GoTo TrataErro
    
    nCorrecto = True
    sTipos = Array(dbBoolean, "Booleano", dbByte, "Byte (0..255)", dbInteger, "Inteiro", dbLong, "Longo Inteiro", dbCurrency, "Currency", dbSingle, "Precis�o Simples", dbDouble, "Dupla Precis�o", dbDate, "Data", dbText, "Texto", dbLongBinary, "Longo Bin�rio", dbMemo, "Memo", mediTipoMaiusculas, "Maiusculas", mediTipoMinusculas, "Minusculas", mediTipoCapitalizado, "Capitalizado", mediTipoData, "Data (" & gFormatoData & ")", mediTipoHora, "Hora (" & gFormatoHora & ")")
    
    '--------------------------------------
    If TypeOf NomeControl Is TextBox Or TypeOf NomeControl Is Label Or TypeOf NomeControl Is MaskEdBox Or TypeOf NomeControl Is RichTextBox Then
        ' Continua o tratamento da valida��o do tipo de campo.
    ElseIf TypeOf NomeControl Is ComboBox Then
        GoTo FimDaValidacao
    ElseIf TypeOf NomeControl Is CheckBox Then
        GoTo FimDaValidacao
    Else
        GoTo FimDaValidacao
    End If
    '--------------------------------------
    
    If NomeControl.Tag = "" Then
        BG_Mensagem mediMsgBox, "N�o foi definido tipo de campo.", vbExclamation, "BG_ValidaTipoCampo - '" & NomeControl.Name & "'"
        nCorrecto = False
        GoTo FimDaValidacao
    End If
    
    If NomeControl = "" Then GoTo FimDaValidacao ' Campo sem nada n�o precisa de tratamento.
    
    '--------------------------------------
    
    ' In�cio: Por causa do Campo Decimal do Informix!, sub-tratar a string que indica tipo decimal.
    DecimalLength = 0 ' Tamanho da parte decimal e fraccion�ria
    DecimalScale = 0 ' Tamanho da parte fraccion�ria
    Decimal_tag = "" ' Utilizado como vari�vel tempor�ria para o tipo decimal

    pos = InStr(NomeControl.Tag, "(")
    If pos <> 0 Then
        s1 = Mid(NomeControl.Tag, pos + 1, 1)
        s2 = Mid(NomeControl.Tag, pos + 2, 1)
        If s2 = "," Then
            DecimalLength = CInt(s1)
        Else
            DecimalLength = CInt(s1 & s2)
        End If

        pos1 = InStr(NomeControl.Tag, ",")
        s1 = Mid(NomeControl.Tag, pos1 + 1, 1)
        s2 = Mid(NomeControl.Tag, pos1 + 2, 1)
        If s2 = ")" Then
            DecimalScale = CInt(s1)
        Else
            DecimalScale = CInt(s1 & s2)
        End If
        
        Decimal_tag = NomeControl.Tag
        NomeControl.Tag = left(NomeControl.Tag, pos - 1)
    End If
    ' Fim: Por causa do Campo Decimal do Informix!, sub-tratar a string que indica tipo decimal.

    '--------------------------------------
    
    If IsNumeric(NomeControl.Tag) Then
        nTipo = CInt(NomeControl.Tag)

        Select Case nTipo
        Case dbBoolean
            NomeControl = CBool(NomeControl)
        Case dbByte
            NomeControl = CByte(NomeControl)
        Case dbInteger
            NomeControl = CInt(NomeControl)
        Case dbLong
            NomeControl = CLng(NomeControl)
        Case dbCurrency
            NomeControl = CCur(NomeControl)
        Case dbSingle
            If Decimal_tag <> "" Then
                GoSub TratarDecimal
                If ErroDecimal = 0 Then
                    NomeControl = BG_CvDecimalParaDisplay(CSng(BG_CvDecimalParaCalculo(NomeControl)))
                End If
            Else
                NomeControl = BG_CvDecimalParaDisplay(CSng(BG_CvDecimalParaCalculo(NomeControl)))
            End If
        Case dbDouble
            If Decimal_tag <> "" Then
                GoSub TratarDecimal
                If ErroDecimal = 0 Then
                    NomeControl = BG_CvDecimalParaDisplay(BL_String2Double(BG_CvDecimalParaCalculo(NomeControl)))
                End If
            Else
                NomeControl = BG_CvDecimalParaDisplay(BL_String2Double(BG_CvDecimalParaCalculo(NomeControl)))
            End If
        Case dbDate
            ' O tipo deste campo dever� ser mudado para 'mediTipoData', no entanto,
            ' � tratado caso entre aqui.
            NomeControl = BG_CvData(NomeControl)
        Case dbText
            ' Tratado por defeito
        Case dbLongBinary
            ' N�o tratado!
        Case dbMemo
            ' Tratado por defeito
        Case mediTipoMaiusculas
            NomeControl = UCase(NomeControl)
        Case mediTipoMinusculas
            NomeControl = LCase(NomeControl)
        Case mediTipoCapitalizado
            s1 = LCase(NomeControl)
            s1 = LTrim(s1)
            s1 = RTrim(s1)
            iComp = Len(s1)
            s2 = ""
            PrimeiraLetra = True
            TemEspaco = False
            For i = 1 To iComp
                If (Mid(s1, i, 1) <> " ") Then
                    TemEspaco = False
                    If PrimeiraLetra = True Then
                        s2 = s2 & UCase(Mid(s1, i, 1))
                        PrimeiraLetra = False
                    Else
                        s2 = s2 & Mid(s1, i, 1)
                    End If
                ElseIf (TemEspaco = False) Then
                    PrimeiraLetra = True
                    TemEspaco = True
                    s2 = s2 & Mid(s1, i, 1)
                End If
            Next i
            NomeControl = s2
        Case mediTipoData
            NomeControl = BG_CvData(NomeControl)
        Case mediTipoHora
            NomeControl = BG_CvHora(NomeControl)
        Case Else
            ' N�o tratado!
        End Select
    Else
        BG_Mensagem mediMsgBox, "Programa��o: O tipo de campo foi alterado.", vbExclamation, "BG_ValidaTipoCampo - '" & NomeControl.Name & "'"
    End If

    '--------------------------------------
    If Decimal_tag <> "" Then
        NomeControl.Tag = Decimal_tag
        If ErroDecimal <> 0 Then
            BG_Mensagem mediMsgBox, "Erro Decimal (" & ErroDecimal & ")" & vbCrLf & vbCrLf & "Este campo dever� ter " & DecimalLength - DecimalScale & " n�mero(s) � esquerda do sinal decimal e " & DecimalScale & " n�mero(s) � direita!" & vbCrLf & vbCrLf & "Exemplo:   " & String(DecimalLength - DecimalScale, "x") & gSimboloDecimal & String(DecimalScale, "x"), vbExclamation, "Aten��o"
            ErroDecimal = "Provoca erro para aparecer mensagem. (Propositado)"
        End If
    End If
    '--------------------------------------

    GoTo FimDaValidacao
    

FimDaValidacao:
    BG_ValidaTipoCampo = nCorrecto
    Exit Function
    
TratarDecimal:
    ErroDecimal = 0
    pos = InStr(NomeControl, gSimboloDecimal)

    If pos = 0 And Len(NomeControl) > (DecimalLength - DecimalScale) Then ErroDecimal = -1
    If pos <> 0 And pos > (DecimalLength - DecimalScale + 1) Then ErroDecimal = -2
    If pos <> 0 And (Len(NomeControl) - pos) > DecimalScale Then ErroDecimal = -3
    'A mensagem de erro � mostrada no fim da fun��o.
    Return

TrataErro:
    i = 0
    For Each Var1 In sTipos
        If Var1 = nTipo Then Var2 = sTipos(i + 1)
        i = i + 1
    Next
    Beep
    BG_Mensagem mediMsgBox, "O valor atribuido n�o � correcto para este campo!      " & vbCrLf & vbCrLf & "Este campo � do tipo '" & Var2 & "'", vbExclamation, "Aten��o"
    nCorrecto = False
    Resume Next

End Function

Function BG_DaValorData(data As Variant, tipo As String, Optional ByRef StrDiaOuMes As Variant) As Integer
'   Dada uma 'Data', deve ser indicado em 'Tipo' se se quer
' retornado o 'ANO', o 'MES', ou o 'DIA'.
'   Para al�m disso, se se pretender saber o nome do dia ou do
' m�s, deve-se invocar a fun��o com mais uma vari�vel passada
' por refer�ncia, onde ser� colocado o nome do dia ou do m�s.
'
' Exemplos:
'
' Res = BG_DaValorData("15-10-1997","DIA")  ==> Res = 15
' Res = BG_DaValorData("15-10-1997","DIA",StrDia)  ==> Res = 15; StrMes = "Quarta"
' Res = Left (BG_DaValorData("15-10-1997","DIA",StrDia), 3)  ==> Res = 15; StrMes = "Qua"
' Res = BG_DaValorData("15-10-1997","MES")  ==> Res = 10
' Res = BG_DaValorData("15-10-1997","MES",StrMes)  ==> Res = 10; StrMes = "Outubro"
' Res = UCase (BG_DaValorData("15-10-1997","MES",StrMes))  ==> Res = 10; StrMes = "OUTUBRO"
' Res = BG_DaValorData("15-10-1997","ANO")  ==> Res = 1997

    Dim Data1 As String
    Dim d1, m1, Y1 As String

    If Not IsDate(data) Then
        BG_DaValorData = -1
        Exit Function
    End If
    
    If (StrComp("DIA", tipo, 1) <> 0) And (StrComp("MES", tipo, 1) <> 0) And (StrComp("ANO", tipo, 1) <> 0) Then
        BG_DaValorData = -1
        Exit Function
    End If

    Data1 = Format(data, "dd-mm-yyyy")
    
    If (StrComp("DIA", tipo, 1) = 0) Then
        d1 = left(Data1, 2)
        
        If Not IsMissing(StrDiaOuMes) Then
            Select Case CInt(Format(Data1, "w"))
            Case 1
                StrDiaOuMes = "Domingo"
            Case 2
                StrDiaOuMes = "Segunda"
            Case 3
                StrDiaOuMes = "Ter�a"
            Case 4
                StrDiaOuMes = "Quarta"
            Case 5
                StrDiaOuMes = "Quinta"
            Case 6
                StrDiaOuMes = "Sexta"
            Case 7
                StrDiaOuMes = "S�bado"
            End Select
        End If
        
        BG_DaValorData = CInt(d1)
        Exit Function
    End If
    
    If (StrComp("MES", tipo, 1) = 0) Then
        m1 = Mid(Data1, 4, 2)
        
        If Not IsMissing(StrDiaOuMes) Then
            Select Case CInt(m1)
            Case 1
                StrDiaOuMes = "Janeiro"
            Case 2
                StrDiaOuMes = "Fevereiro"
            Case 3
                StrDiaOuMes = "Mar�o"
            Case 4
                StrDiaOuMes = "Abril"
            Case 5
                StrDiaOuMes = "Maio"
            Case 6
                StrDiaOuMes = "Junho"
            Case 7
                StrDiaOuMes = "Julho"
            Case 8
                StrDiaOuMes = "Agosto"
            Case 9
                StrDiaOuMes = "Setembro"
            Case 10
                StrDiaOuMes = "Outubro"
            Case 11
                StrDiaOuMes = "Novembro"
            Case 12
                StrDiaOuMes = "Dezembro"
            End Select
        End If
        
        BG_DaValorData = CInt(m1)
        Exit Function
    End If
    
    If (StrComp("ANO", tipo, 1) = 0) Then
        Y1 = Right(Data1, 4)
        BG_DaValorData = CInt(Y1)
        Exit Function
    End If
    
End Function

Sub BG_PermiteStringVaziaEmText(PathBD As String)
    Dim BD As Database
    Dim Tdf As TableDef
    Dim Fld As Variant
    Dim CamposSimNao As String
    Dim s1 As String
    Dim i As Integer
    
    On Error GoTo TrataErro
    
    CamposSimNao = 1
    
    BG_LogFile_Erros ""
    BG_LogFile_Erros "********** BG_PermiteStringVaziaEmText (In�cio) ******************************"
    
    Set BD = Workspaces(0).OpenDatabase(PathBD)

    For Each Tdf In BD.TableDefs
      If (Tdf.Attributes And dbAttachedTable) = dbAttachedTable Then
        If left(Tdf.Connect, 1) = ";" Then
          'must be a jet attached table
          BG_LogFile_Erros Tdf.Name & " -> Jet"
        Else
          'must be an ISAM attached table
          BG_LogFile_Erros Tdf.Name & " -> " & left(Tdf.Connect, InStr(Tdf.Connect, ";") - 1)
        End If
      ElseIf (Tdf.Attributes And dbAttachedODBC) = dbAttachedODBC Then
        BG_LogFile_Erros Tdf.Name & " -> ODBC"
      Else
        If (Tdf.Attributes And dbSystemObject) = 0 Then
            BG_LogFile_Erros Tdf.Name
        Else
            BG_LogFile_Erros Tdf.Name & " -> Tabela de Sistema"
        End If
      End If
      
      If CamposSimNao = 1 Then GoSub MostraCampos

    Next
    
    BD.Close

    BG_LogFile_Erros "********** BG_PermiteStringVaziaEmText (Fim) *********************************"
    BG_LogFile_Erros ""
    
    Exit Sub
    
MostraCampos:
'    For i = 0 To tdf.Fields.Count - 1
'        BG_LogFile_Erros "    " & tdf.Fields(i).Name & " - " & tdf.Fields(i).Type
'    Next i
                
    For Each Fld In BD.TableDefs(Tdf.Name).Fields
        If Fld.Type = dbText Then
            BG_LogFile_Erros "    " & Fld.Name & " - " & Fld.Type & " (" & Fld.AllowZeroLength & ")"
            s1 = Fld.AllowZeroLength
            Fld.AllowZeroLength = True
            BG_LogFile_Erros "    ..........................    " & UCase(s1) & " para " & UCase(Fld.AllowZeroLength)
        Else
            BG_LogFile_Erros "    " & Fld.Name & " - " & Fld.Type
        End If
    Next
    
    Return

TrataErro:
    Beep
    Resume Next

End Sub

Function BGAux_DaFieldObjectProperties(BD As Database, _
                                       NomeTabela As String, _
                                       NomeCampoBD As Variant, _
                                       propriedade As String) As Variant
'NOTA:
'   Esta fun��o retorna o valor da propriedade indicada no parametro de entrada 'Propriedade'.
'   Propriedades dispon�veis: Type; Size; Name
'
'   Esta fun��o � necess�ria, porque se se pretende obter uma propriedade de um
'   campo de uma tabela ou de um campo de uma Query, � imediato, mas o problema
'   � saber se se est� a lidar com tabela ou Query.
'
    Dim Erro As Integer
    Dim MyQuery As QueryDef
    Dim resultado As Variant
    
    On Error GoTo TrataErro

    Erro = 0
    
    If StrComp(propriedade, "Type", 1) = 0 Then
        resultado = BD.TableDefs(NomeTabela).Fields(NomeCampoBD).Type
    ElseIf StrComp(propriedade, "Size", 1) = 0 Then
        resultado = BD.TableDefs(NomeTabela).Fields(NomeCampoBD).Size
    ElseIf StrComp(propriedade, "Name", 1) = 0 Then
        resultado = BD.TableDefs(NomeTabela).Fields(NomeCampoBD).Name
    End If
    
    If Erro = cItemNotFoundInThisCollection Then
        ' Tratamento da Query
        
        ' Obtem a Query
        Set MyQuery = BD.OpenQueryDef(NomeTabela)

        If StrComp(propriedade, "Type", 1) = 0 Then
            resultado = MyQuery.Fields(NomeCampoBD).Type
        ElseIf StrComp(propriedade, "Size", 1) = 0 Then
            resultado = MyQuery.Fields(NomeCampoBD).Size
        ElseIf StrComp(propriedade, "Name", 1) = 0 Then
            resultado = MyQuery.Fields(NomeCampoBD).Name
        End If
       
    End If

'BG_Mensagem mediMsgBox, NomeTabela & vbCrLf & NomeCampoBD, , Propriedade & " - " & Resultado

    BGAux_DaFieldObjectProperties = resultado
    Exit Function

TrataErro:
    If Err = cItemNotFoundInThisCollection Then
        Erro = cItemNotFoundInThisCollection
        Resume Next
    Else
        'BG_TrataErro "BGAux_DaFieldObjectProperties", Err, NomeTabela & " - " & NomeCampoBD
        Exit Function
    End If

End Function

Sub BG_LimpaCampo_Todos(CamposEc)
    Dim TamArray As Integer
    Dim i As Integer
    
    On Error GoTo TrataErro
    
    TamArray = UBound(CamposEc) - LBound(CamposEc) + 1

    For i = 0 To TamArray - 1
        If TypeOf CamposEc(i) Is TextBox Or TypeOf CamposEc(i) Is Label Or TypeOf CamposEc(i) Is MaskEdBox Or TypeOf CamposEc(i) Is RichTextBox Then
            CamposEc(i) = ""
        ElseIf TypeOf CamposEc(i) Is ComboBox Then
            CamposEc(i).ListIndex = -1
        ElseIf TypeOf CamposEc(i) Is CheckBox Then
            CamposEc(i).value = 2
        Else
            ' Nada
        End If
    Next i

    Exit Sub
    
TrataErro:
    BG_TrataErro "BG_LimpaCampo_Todos ('" & CamposEc(i).Name & "')", Err
    Resume Next

End Sub

Sub BG_DefTipoCampoEc_Todos(NomeTabela As String, CamposBD, CamposEc, TipoCampo As Integer)
    Dim TamArray As Integer
    Dim i As Integer
    
    On Error GoTo TrataErro
    
    TamArray = UBound(CamposEc) - LBound(CamposEc) + 1

    For i = 0 To TamArray - 1
        BG_DefTipoCampoEc NomeTabela, CamposBD(i), CamposEc(i), TipoCampo
    Next i

    Exit Sub
    
TrataErro:
    BG_TrataErro "BG_DefTipoCampoEc_Todos ('" & CamposEc(i).Name & "')", Err
    Resume Next

End Sub

Sub BG_PreencheCampoEc_Todos(NomeRecordset As Dynaset, CamposBD, CamposEc)
    Dim TamArray As Integer
    Dim i As Integer
    
    On Error GoTo TrataErro
    
    TamArray = UBound(CamposEc) - LBound(CamposEc) + 1

    For i = 0 To TamArray - 1
        BG_PreencheCampoEc NomeRecordset, CamposBD(i), CamposEc(i)
    Next i

    Exit Sub
    
TrataErro:
    BG_TrataErro "BG_PreencheCampoEc_Todos ('" & CamposEc(i).Name & "')", Err
    Resume Next

End Sub

Public Sub BG_PreencheListBoxMultipla(NomeControl As Object, NomeRecordset As recordset, CamposRequeridos, NumEspacos, CamposBD, CamposEc, Localizacao As String, Optional ValoresCombo As Variant)
    Dim NomeCampoBD As Variant
    Dim Marca As Variant
    Dim sLinha As String
    Dim bValorNaCombo As Boolean
    Dim sValorParaMostrar As String
    Dim i As Long
    Dim j, k, l, m As Integer
    Dim iAux As Integer
    Dim CampoAux As Variant
    Dim bEncontrou As Boolean
    Dim lPosicao, lNumTotal As Long

    On Error GoTo TrataErro

    lPosicao = NomeControl.ListIndex
    lNumTotal = NomeControl.ListCount
    
    NomeControl.Clear
    Marca = NomeRecordset.Bookmark
    i = 0
    Do Until NomeRecordset.EOF
        sLinha = ""
        j = 0
        For Each NomeCampoBD In CamposRequeridos
            If (IsNull(NomeRecordset(NomeCampoBD))) Or (NomeRecordset(NomeCampoBD) = "") Then
                sLinha = sLinha & String(NumEspacos(j), " ")
            Else
                bValorNaCombo = False

                If Not IsMissing(ValoresCombo) Then
                    If ValoresCombo = mediOff Then GoTo IGNORA_COMBO
                End If

                ' In�cio: Trata Campos do tipo ComboBox
                k = 0
                bEncontrou = False
                For Each CampoAux In CamposBD
                    If NomeCampoBD = CampoAux Then
                        bEncontrou = True
                        Exit For
                    End If
                    k = k + 1
                Next
                If bEncontrou = True Then
                    If TypeOf CamposEc(k) Is ComboBox Then
                        iAux = CInt(NomeRecordset(NomeCampoBD))
                        For l = 0 To CamposEc(k).ListCount - 1
                            If iAux = CamposEc(k).ItemData(l) Then
                                bValorNaCombo = True
                                sValorParaMostrar = CamposEc(k).List(l)
                                Exit For
                            End If
                        Next
                    End If
                End If
                ' Fim: Trata Campos do tipo ComboBox
IGNORA_COMBO:
                If bValorNaCombo = False Then
                    sValorParaMostrar = NomeRecordset(NomeCampoBD)

                    ' In�cio: Trata convers�es para Decimal, Data e Hora
                    m = 0
                    For Each CampoAux In CamposBD
                        If NomeCampoBD = CampoAux Then

                            If left(CamposEc(m).Tag, 1) = dbSingle Or left(CamposEc(m).Tag, 1) = dbDouble Then
                                sValorParaMostrar = BG_CvDecimalParaDisplay(sValorParaMostrar)
                            ElseIf CamposEc(m).Tag = mediTipoData Then
                                sValorParaMostrar = BG_CvData(sValorParaMostrar)
                            ElseIf CamposEc(m).Tag = mediTipoHora Then
                                sValorParaMostrar = BG_CvHora(sValorParaMostrar)
                            End If
                            
                            Exit For
                        End If
                        m = m + 1
                    Next
                    ' Fim: Trata convers�es para Decimal, Data e Hora
                End If

                sLinha = sLinha & left(sValorParaMostrar, NumEspacos(j))
                If Len(sValorParaMostrar) < NumEspacos(j) Then
                    sLinha = sLinha & String(NumEspacos(j) - Len(sValorParaMostrar), " ")
                End If
            End If
            sLinha = sLinha & " "
            j = j + 1
        Next
        NomeControl.AddItem sLinha
        NomeRecordset.MoveNext
        i = i + 1
        If i > 32767 Then
            BG_Mensagem mediMsgBox, "N�o � poss�vel mostrar nesta lista mais do que " & i - 1 & " registos!", vbExclamation
            Exit Do
        End If
    Loop
    
    If Localizacao = "SELECT" Then
        NomeControl.ListIndex = 0
    ElseIf Localizacao = "UPDATE" Then
        NomeControl.ListIndex = lPosicao
    ElseIf Localizacao = "DELETE" Then
        If lNumTotal = lPosicao + 1 Then
            NomeControl.ListIndex = lPosicao - 1
        Else
            NomeControl.ListIndex = lPosicao
        End If
    End If
    
    If Localizacao <> "SELECT" And Localizacao <> "UPDATE" And Localizacao <> "DELETE" Then
        NomeControl.ListIndex = 0
        BG_Mensagem mediMsgBox, "BG_PreencheListBoxMultipla: Necess�rio indicar SELECT, UPDATE ou DELETE"
    End If
    
    NomeRecordset.Bookmark = Marca
    
    Exit Sub
    
TrataErro:
    BG_Mensagem mediMsgBox, "Problema na rotina 'BG_PreencheListBoxMultipla'" & vbCrLf & vbCrLf & Err & " - " & Error(Err), vbExclamation, "BG " & bgVersao
    Resume Next

End Sub

Public Sub BG_StackJanelas_Actualiza(nomeForm As Form)
    
    On Error GoTo ErrorHandler
    
    Dim X
    Dim i, j, aux As Integer

    i = 0
    'Encontra janela indicada em 'NomeForm'
    For Each X In StackJanelas
        If X.Name = nomeForm.Name Then Exit For
        i = i + 1
    Next
    
    aux = pStackJanelas
    If i = aux Then
        ' Fica tudo como est�.
        ' Debug.Print "="
    ElseIf i < aux Then
        'Termina janelas da Stack at� � indicada em 'NomeForm'
        For j = aux To i + 1 Step -1
            Unload StackJanelas(j)
        Next
        ' Debug.Print "<"
    ElseIf i > aux Then
        ' Debug.Print ">"
    End If

Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            MsgBox "Erro : " & Err.Description, vbCritical, "DEBUG : BG_StackJanelas_Actualiza"
    End Select
End Sub

Sub BG_AbreForm(IdForm As Form, nomeForm As String)

' � necess�rio o 'Id' e o 'Nome' do Form,
' porque, para se verificar se o Form j� est�
' aberto ou n�o ter� que se indicar a string com
' o nome do Form. Se se fizesse atrav�s de
' 'IdForm.Name', ent�o, o Form
' antes de mais abrir-se-ia automaticamente
' (s� por indicar 'IdForm.Name').
' Isto acontesse, porque para saber qualquer
' propriedade de um Form, � preciso que ele
' esteja carregado(Load).
    
    Dim X
    Dim bExiste As Boolean
    
    bExiste = False
    For Each X In StackJanelas
        If X.Name = nomeForm Then
            bExiste = True
            Exit For
        End If
    Next
    
    If bExiste = True Then
        BG_StackJanelas_Actualiza IdForm
    Else
        BG_StackJanelas_Reset
        
        MDIFormInicio.MousePointer = vbHourglass
        IdForm.Show
        BL_FimProcessamento MDIFormInicio
    End If
End Sub


Function BG_VfValor(valor As Variant, Optional sDelimitador As Variant) As String
' Fun��o para verificar se um dado valor � Null ou n�o.
' Consoante o caso, o retorno � com ou sem delimitador.

    Dim sRes As String
    
    If IsNull(valor) Or valor = "" Then
        sRes = "Null"
    Else
        If IsMissing(sDelimitador) Then
            sRes = valor
        Else
            sRes = sDelimitador & valor & sDelimitador
        End If
    End If
    
    BG_VfValor = sRes
End Function

Public Sub BG_MostraValoresDaLista(obLista As Object)
' Mostra as propriedade 'Text' e 'ItemData' de cada posicao de uma ListBox.

    Dim i As Integer
    Dim sTemp As String
    
    If obLista.ListCount = 0 Then
        BG_Mensagem mediMsgBox, "Lista Vazia!", vbInformation, "Mostra Valores da Lista: " & obLista.Name
    Else
        For i = 0 To obLista.ListCount - 1
            sTemp = "Posi��o: " & i + 1 & " / " & obLista.ListCount & vbCrLf & vbCrLf
            sTemp = sTemp & "Texto........: " & obLista.List(i) & vbCrLf & "ItemData...: " & obLista.ItemData(i)
            
            If TypeOf obLista Is ListBox Then
                If obLista.Selected(i) = True Then
                    sTemp = sTemp & vbCrLf & vbCrLf & "(Seleccionado)"
                End If
            End If
            BG_Mensagem mediMsgBox, sTemp, vbInformation, "Mostra Valores da Lista: " & obLista.Name
        Next i
    End If
End Sub

Public Sub BG_PassaElementoEntreListas(ListaOrigem As Object, _
                                       ListaDestino As Object, _
                                       iIndiceOrigem As Integer, _
                                       bRepetidos As Boolean, _
                                       bRetira As Boolean)
' Objectivo: Passagem de elementos entre ListBoxes ou ComboBoxes

    Dim i As Integer
    
    ' Verifica se est� dentro dos limites
    If (iIndiceOrigem < 0) Or (iIndiceOrigem > ListaOrigem.ListCount - 1) Then
        'MsgBox "�ndice fora dos limites!" & vbCrLf & vbCrLf & iIndiceOrigem, vbExclamation, "Passagem de um elemento entre Listas"
        Exit Sub
    End If
    
    ' Verifica se o elemento pedido est� seleccionado
    If TypeOf ListaOrigem Is ComboBox Then
        If ListaOrigem.ListIndex <> iIndiceOrigem Then
            'MsgBox "Elemento n�o seleccionado!" & vbCrLf & vbCrLf & ListaOrigem.List(iIndiceOrigem), vbExclamation, "Passagem de um elemento entre Listas"
            Exit Sub
        End If
    Else
        If ListaOrigem.Selected(iIndiceOrigem) = False Then
            'MsgBox "Elemento n�o seleccionado!" & vbCrLf & vbCrLf & ListaOrigem.List(iIndiceOrigem), vbExclamation, "Passagem de um elemento entre Listas"
            Exit Sub
        End If
    End If
    
    If bRepetidos = False Then
        For i = 0 To ListaDestino.ListCount - 1
            If ListaDestino.List(i) = ListaOrigem.List(iIndiceOrigem) Then
                If ListaDestino.ItemData(i) = ListaOrigem.ItemData(iIndiceOrigem) Then
                    'MsgBox "Elemento Repetido!" & vbCrLf & vbCrLf & ListaOrigem.List(iIndiceOrigem), vbExclamation, "Passagem de um elemento entre Listas"
                    Exit Sub
                End If
            End If
        Next i
    End If
    
    'Copia elemento
    ListaDestino.AddItem ListaOrigem.List(iIndiceOrigem)
    ListaDestino.ItemData(ListaDestino.NewIndex) = ListaOrigem.ItemData(iIndiceOrigem)
    
    'Apaga elemento
    If bRetira = True Then
        ListaOrigem.RemoveItem iIndiceOrigem
        'MsgBox "Elemento Removido!" & vbCrLf & vbCrLf & ListaOrigem.List(iIndiceOrigem), vbInformation, "Passagem de um elemento entre Listas"
    End If
    
End Sub

Sub BG_PreencheComboBD_Ext(ObjectoBD As String, NomeTabelaBD As String, NomeCampoBD_codigo As String, NomeCampoBD_Descr As String, NomeControl As Control, Optional TipoOrdenacao1 As Variant, Optional TipoOrdenacao2 As Variant)
'   Esta rotina � identica � 'BG_PreencheComboBD', com o acr�scimo do argumento
' de entrada 'ObjectoBD'.
'   Com esta rotina � poss�vel preencher uma ListBox ou ComboBox de uma tabela
' de uma BD diferente da que se trabalha por defeito.

' ***************Parametros da funcao*********************
' ObjectoBD - > nome do Data Source para nova conexao
' NomeTabelaBD - > nome da tabela a listar
' NomeCampoBD_codigo - > Campo da BD correspondente ao codigo
' NomeCampoBD_Descr - > Campo da BD correspondente a descricao
' NomeControl - > nome da Combo ou ListBox
' TipoOrdenacao1 - >
' TipoOrdenacao2 - >
    
    Dim cNovaConexao As ADODB.Connection
    
    Dim obTabelaAux As ADODB.recordset
    Dim CriterioAux As String
    Dim CriterioInicial As String
    Dim iTabelaOuQuery As Integer
    Dim i As Integer
    
    On Error GoTo TrataErro
    
    ' Abrir nova CONEXAO
    Set cNovaConexao = New ADODB.Connection
    
    cNovaConexao.Provider = "MSDASQL"
    cNovaConexao.ConnectionString = "DSN=" & ObjectoBD
    cNovaConexao.Open
    ' FIM
    
    'Inicio: Verificar se trata sobre Tabela ou sobre Query
    iTabelaOuQuery = InStr(StrConv(NomeTabelaBD, vbUpperCase), "SELECT ")
    If iTabelaOuQuery = 1 Then
        CriterioInicial = NomeTabelaBD
    Else
        CriterioInicial = "SELECT " & NomeCampoBD_codigo & ", " & NomeCampoBD_Descr & " FROM " & NomeTabelaBD
    End If
    'Fim: Verificar se trata sobre Tabela ou sobre Query
        
    'Inicio: Crit�rio de Ordena��o
    If IsMissing(TipoOrdenacao1) Then
        CriterioAux = CriterioInicial
    Else
        If TipoOrdenacao1 = mediAscComboCodigo Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_codigo & " ASC"
        ElseIf TipoOrdenacao1 = mediDescComboCodigo Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_codigo & " DESC"
        ElseIf TipoOrdenacao1 = mediAscComboDesignacao Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_Descr & " ASC"
        ElseIf TipoOrdenacao1 = mediDescComboDesignacao Then
            CriterioAux = CriterioInicial & " ORDER BY " & NomeCampoBD_Descr & " DESC"
        End If
        
        If Not IsMissing(TipoOrdenacao2) Then
            If TipoOrdenacao2 = mediAscComboCodigo Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_codigo & " ASC"
            ElseIf TipoOrdenacao2 = mediDescComboCodigo Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_codigo & " DESC"
            ElseIf TipoOrdenacao2 = mediAscComboDesignacao Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_Descr & " ASC"
            ElseIf TipoOrdenacao2 = mediDescComboDesignacao Then
                CriterioAux = CriterioAux & ", " & NomeCampoBD_Descr & " DESC"
            End If
        End If
    End If
    'Fim: Crit�rio de Ordena��o

    'Set obTabelaAux = ObjectoBD.OpenRecordset(CriterioAux, dbOpenDynaset, dbReadOnly)
    Set obTabelaAux = New ADODB.recordset
    obTabelaAux.CursorType = adOpenStatic
    obTabelaAux.CursorLocation = adUseServer
    obTabelaAux.Open CriterioAux, cNovaConexao
    

    NomeControl.Clear

    i = 0
    Do Until obTabelaAux.EOF
        NomeControl.AddItem obTabelaAux(NomeCampoBD_Descr)
        NomeControl.ItemData(i) = CLng(obTabelaAux(NomeCampoBD_codigo))
        obTabelaAux.MoveNext
        i = i + 1
    Loop

    obTabelaAux.Close
    Set obTabelaAux = Nothing
    
    Exit Sub

TrataErro:
    If Err = 6 Then
        NomeControl.List(i) = "!!Sup. 32767: " & NomeControl.List(i)
        NomeControl.ItemData(i) = -10
        BG_TrataErro "BG_PreencheComboBD", Err, "'" & NomeTabelaBD & "': Valor Superior a 32767!"
    Else
        BG_TrataErro "BG_PreencheComboBD", Err
    End If
    Resume Next


End Sub

Public Sub BG_RegDeleteSetting(hLocation As Long, sSection As String, sKey As String, iSide As Integer)
'   Exemplos:
'   -------
'    Dim hLocation As Long
'    Dim sSection As String
'    Dim sKey As String
'    Dim sSetting As String
'
'    hLocation = HKEY_CURRENT_USER
'    sSection = "Software\VB and VBA Program Settings\chip\gSGBD"
'
'    ' OBTER...
'    sKey = "LoginTimeout"
'    MsgBox BG_RegGetSetting(hLocation, sSection, sKey, "Nada")
'    MsgBox BG_RegGetSetting(hLocation, sSection, sKey)
'
'    ' CRIAR...
'    sKey = "zzTESTE"
'    sSetting = "Testando............."
'    BG_RegSaveSetting hLocation, sSection, sKey, sSetting
'    BG_RegSaveSetting hLocation, sSection, sKey
'
'    ' APAGAR
'    BG_RegDeleteSetting hLocation, sSection, sKey, mediRegLeft
'    BG_RegDeleteSetting hLocation, sSection, sKey, mediRegRight
'

    Dim res
    
    Dim phkResult As Long
    
    res = RegOpenKeyEx(hLocation, sSection, 0, KEY_SET_VALUE, phkResult)
    If res = 0 Then
        If iSide = mediRegLeft Then
            res = RegDeleteKey(phkResult, sKey)
        ElseIf iSide = mediRegRight Then
            res = RegDeleteValue(phkResult, sKey)
        End If

        res = RegCloseKey(phkResult)
    End If
End Sub

Public Function BG_RegGetSetting(hLocation As Long, sSection As String, sKey As String, Optional sDefault As Variant) As String
'   Exemplos:
'   -------
'    Dim hLocation As Long
'    Dim sSection As String
'    Dim sKey As String
'    Dim sSetting As String
'
'    hLocation = HKEY_CURRENT_USER
'    sSection = "Software\VB and VBA Program Settings\chip\gSGBD"
'
'    ' OBTER...
'    sKey = "LoginTimeout"
'    MsgBox BG_RegGetSetting(hLocation, sSection, sKey, "Nada")
'    MsgBox BG_RegGetSetting(hLocation, sSection, sKey)
'
'    ' CRIAR...
'    sKey = "zzTESTE"
'    sSetting = "Testando............."
'    BG_RegSaveSetting hLocation, sSection, sKey, sSetting
'    BG_RegSaveSetting hLocation, sSection, sKey
'
'    ' APAGAR
'    BG_RegDeleteSetting hLocation, sSection, sKey, mediRegLeft
'    BG_RegDeleteSetting hLocation, sSection, sKey, mediRegRight
'
    
    Dim res
    Dim sRes As String
    
    Dim phkResult As Long
    Dim lpType As Long
    Dim lpData As String
    Dim lpcbData As Long
    
    If Not IsMissing(sDefault) Then
        sRes = sDefault
    Else
        sRes = ""
    End If
    
    res = RegOpenKeyEx(hLocation, sSection, 0, KEY_QUERY_VALUE, phkResult)
    If res = 0 Then
        lpcbData = 255
        lpData = Space(lpcbData)
        res = RegQueryValueEx(phkResult, sKey, 0, lpType, (lpData), lpcbData)
    
        If res = 0 Then sRes = left(lpData, lpcbData - 1)
        
        res = RegCloseKey(phkResult)
    End If
    
    BG_RegGetSetting = sRes
End Function

Public Sub BG_RegSaveSetting(hLocation As Long, sSection As String, sKey As String, Optional sSetting As Variant)
'   Exemplos:
'   -------
'    Dim hLocation As Long
'    Dim sSection As String
'    Dim sKey As String
'    Dim sSetting As String
'
'    hLocation = HKEY_CURRENT_USER
'    sSection = "Software\VB and VBA Program Settings\chip\gSGBD"
'
'    ' OBTER...
'    sKey = "LoginTimeout"
'    MsgBox BG_RegGetSetting(hLocation, sSection, sKey, "Nada")
'    MsgBox BG_RegGetSetting(hLocation, sSection, sKey)
'
'    ' CRIAR...
'    sKey = "zzTESTE"
'    sSetting = "Testando............."
'    BG_RegSaveSetting hLocation, sSection, sKey, sSetting
'    BG_RegSaveSetting hLocation, sSection, sKey
'
'    ' APAGAR
'    BG_RegDeleteSetting hLocation, sSection, sKey, mediRegLeft
'    BG_RegDeleteSetting hLocation, sSection, sKey, mediRegRight
'
    
    Dim res
    
    Dim phkResult As Long
    Dim lpType As Long
    Dim lpData As String
    Dim lpcbData As Long
    Dim saType As SECURITY_ATTRIBUTES
    On Error GoTo TrataErro
    res = RegOpenKeyEx(hLocation, sSection, 0, KEY_SET_VALUE, phkResult)
    If res = 0 Then
        If Not IsMissing(sSetting) Then
            lpType = REG_SZ
            lpData = sSetting
            lpcbData = Len(lpData) + 1
            res = RegSetValueEx(phkResult, sKey, 0, lpType, lpData, lpcbData)
        Else
            res = RegCreateKeyEx(phkResult, sKey, 0, "CLASS STRING ??", REG_OPTION_NON_VOLATILE, KEY_SET_VALUE, saType, phkResult, lpcbData)
        End If
        res = RegCloseKey(phkResult)
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaGenerica: BG_RegSaveSetting: " & Err.Description, "BG", "BG_RegSaveSetting", False
    Exit Sub
    Resume Next
End Sub

Sub BG_LimpaPassaParams()
    
    Dim i, j As Integer
    Dim X, Y As Variant
    
    j = 0
    gPassaParams.id = ""
    For Each Y In gPassaParams.Param
        gPassaParams.Param(j) = ""
        j = j + 1
    Next

End Sub

Public Sub BG_StackJanelas_Inicializa()
    
    Dim FormBase As Form
    Dim X
    Dim i As Integer
    
    Set FormBase = MDIFormInicio 'Podia ser outro qualquer
    
    i = 0
    For Each X In StackJanelas
        Set StackJanelas(i) = FormBase
        i = i + 1
    Next
    
    pStackJanelas = 0
    
    BG_StackJanelas_MostraStackLog

End Sub

Public Sub BG_StackJanelas_Pop()
    
    Set StackJanelas(pStackJanelas) = StackJanelas(0)
    
    pStackJanelas = pStackJanelas - 1
    
    BG_StackJanelas_MostraStackLog
    
    'NELSONPSILVA 04.04.2018 Glintt-HS-18011
    If gATIVA_LOGS_RGPD = mediSim Then
        If gFormActivo.Name <> MDIFormInicio.Name Then
            BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.sa�da_form) & " - " & gFormActivo.Name, _
            Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", "{}", "{}"
        End If
    End If

End Sub

Public Sub BG_StackJanelas_Push(nomeForm As Form)
    
    pStackJanelas = pStackJanelas + 1
'    Set StackJanelas(pStackJanelas) = BG_StackJanelas_MostraStackLog
    Set StackJanelas(pStackJanelas) = nomeForm
    
    BG_StackJanelas_MostraStackLog

End Sub

Public Sub BG_StackJanelas_Reset()
    Dim i, j As Integer
    
    j = pStackJanelas
    If j > 0 Then
        For i = j To 1 Step -1
            Unload StackJanelas(i)
'            BG_StackJanelas_Pop
        Next
    End If
End Sub

Public Sub BG_StackJanelas_MostraStackLog()
       
    On Error Resume Next
    
    Dim i, j As Integer
    
Exit Sub
    
'    BG_LogFile_Erros vbCrLf & "*** StackJanelas ***"
    Debug.Print vbCrLf & "*** StackJanelas ***"
    j = pStackJanelas
    For i = j To 0 Step -1
'        BG_LogFile_Erros i & " - " & StackJanelas(i).Name
        Debug.Print i & " - " & StackJanelas(i).Name
    Next
'    BG_LogFile_Erros "____________________"
    On Error Resume Next
    Debug.Print "____________________"

End Sub

Public Sub BG_MostraTipoCampo(campo)
    
    Dim sTipos As Variant
    Dim sAux1 As String
    Dim i, j As Integer
    
    sTipos = Array(dbBoolean, "Booleano", dbByte, "Byte (0..255)", dbInteger, "Inteiro", dbLong, "Longo Inteiro", dbCurrency, "Currency", dbSingle, "Precis�o Simples", dbDouble, "Dupla Precis�o", dbDate, "Data", dbText, "Texto", dbLongBinary, "Longo Binario", dbMemo, "Memo", mediTipoMaiusculas, "Maiusculas", mediTipoMinusculas, "Minusculas", mediTipoCapitalizado, "Capitalizado", mediTipoData, "Data (" & gFormatoData & ")", mediTipoHora, "Hora (" & gFormatoHora & ")")
    
    sAux1 = ""
    j = 0
    For i = 0 To ((UBound(sTipos) - LBound(sTipos) - 1) / 2)
        sAux1 = sAux1 & vbCrLf & sTipos(j) & " - " & sTipos(j + 1)
        j = j + 2
    Next

    BG_Mensagem mediMsgBox, "Tipo = " & campo.Tag & vbCrLf & sAux1, , campo.Name

End Sub

Public Sub BG_MostraTipoCampo_Todos(CamposEc)
    Dim X As Variant
    
    For Each X In CamposEc
        BG_MostraTipoCampo X
    Next
End Sub

Public Function BG_Mensagem(tipo As Integer, mensagem As String, Optional Botoes As Variant, Optional Titulo As Variant) As Integer
' Exemplos:
'
'   iRes = BG_Mensagem(mediMsgBox, "Ol�")
'   iRes = BG_Mensagem(mediMsgStatus, "Ol�")
'
'   BG_Mensagem mediMsgBox, "Ol�"
'   BG_Mensagem mediMsgBox, "Ol�", vbOKCancel + vbExclamation
'   BG_Mensagem mediMsgBox, "Ol�", , "Hello"
'   BG_Mensagem mediMsgBox, "Ol�", vbYesNo + vbInformation, "Experi�ncia!"
'
'   BG_Mensagem mediMsgStatus, "Ol�", ,"T�tulo da Mensagem"
'   BG_Mensagem mediMsgStatus, "Ol�", mediMsgBeep
'   BG_Mensagem mediMsgStatus, "Ol�", mediMsgPermanece
'   BG_Mensagem mediMsgStatus, "Ol�", mediMsgBeep + mediMsgPermanece
'   BG_Mensagem mediMsgStatus, "Ol�", 10 'Mensagem com dura��o de 10 segundos. Por omiss�o, a dura��o da mensagem � dos segundos que estiverem indicados em 'IntervaloPorDefeitoParaClockMensagem'.
'   BG_Mensagem mediMsgStatus, "Ol�", mediMsgBeep + 10
'

    Dim IntervaloPorDefeitoParaClockMensagem As Integer
    
    IntervaloPorDefeitoParaClockMensagem = 3 ' Em segundos.
    
    If tipo = mediMsgBox Then
        If IsMissing(Botoes) And IsMissing(Titulo) Then
            BG_Mensagem = MsgBox(mensagem)
        End If
        
        If Not IsMissing(Botoes) And IsMissing(Titulo) Then
            BG_Mensagem = MsgBox(mensagem, Botoes)
        End If
        
        If IsMissing(Botoes) And Not IsMissing(Titulo) Then
            BG_Mensagem = MsgBox(mensagem, , Titulo)
        End If
        
        If Not IsMissing(Botoes) And Not IsMissing(Titulo) Then
            BG_Mensagem = MsgBox(mensagem, Botoes, Titulo)
                End If
    
    Else
        MDIFormInicio.Timers(TimersEnum.e_timer_mensagens).Enabled = False ' � preciso ter estas 2 linhas, para a mensagem durar o tempo exacto, mesmo que se enviem v�rias vezes mensagens.
        MDIFormInicio.Timers(TimersEnum.e_timer_mensagens).Enabled = True ' � preciso ter estas 2 linhas, para a mensagem durar o tempo exacto, mesmo que se enviem v�rias vezes mensagens.
        If Not IsMissing(Botoes) Then
            If Botoes = mediMsgBeep Then
                Beep
                MDIFormInicio.Timers(TimersEnum.e_timer_mensagens).Interval = IntervaloPorDefeitoParaClockMensagem * 1000
            ElseIf Botoes = mediMsgPermanece Then
                MDIFormInicio.Timers(TimersEnum.e_timer_mensagens).Interval = 0
            ElseIf Botoes = (mediMsgBeep + mediMsgPermanece) Then
                Beep
                MDIFormInicio.Timers(TimersEnum.e_timer_mensagens).Interval = 0
            ElseIf Botoes < mediMsgPermanece Then
                MDIFormInicio.Timers(TimersEnum.e_timer_mensagens).Interval = Botoes * 1000
            ElseIf (Botoes > mediMsgBeep) And (Botoes < (mediMsgBeep + mediMsgPermanece)) Then
                Beep
                MDIFormInicio.Timers(TimersEnum.e_timer_mensagens).Interval = (Botoes - mediMsgBeep) * 1000
            End If
        Else
            MDIFormInicio.Timers(TimersEnum.e_timer_mensagens).Interval = 2 * 1000
        End If
        
        If Not IsMissing(Titulo) Then
            MDIFormInicio.StatusBar1.Panels.item("Mensagem").Text = Titulo
            MDIFormInicio.StatusBar1.Panels.item("Mensagem").Text = MDIFormInicio.StatusBar1.Panels.item("Mensagem").Text & ": "
            MDIFormInicio.StatusBar1.Panels.item("Mensagem").Text = MDIFormInicio.StatusBar1.Panels.item("Mensagem").Text & mensagem
        Else
            MDIFormInicio.StatusBar1.Panels.item("Mensagem").Text = mensagem
        End If
        
        BG_Mensagem = 0
    End If
End Function


Public Sub BG_LimpaMensagem()
    MDIFormInicio.StatusBar1.Panels.item("Mensagem").Text = ""
End Sub


Public Function BG_CvData(ByVal sArg1 As String) As String
    If sArg1 <> "" Then
        BG_CvData = Format(CDate(sArg1), gFormatoData)
    Else
        BG_CvData = ""
    End If
End Function

Public Function BG_CvHora(ByVal sArg1 As String) As String
    If sArg1 <> "" Then
        BG_CvHora = Format(CDate(sArg1), gFormatoHora)
    Else
        BG_CvHora = ""
    End If
End Function

Public Function BG_CvDecimalParaCalculo(ByVal sArg1 As String) As String
    
    Dim sDecimalDoWindows As String
    Dim StrTemp As String
    Dim NumTemp As Double
    Dim iPos As Integer
    Dim sTemp As String
    
    ' In�cio: Obter Simbolo Decimal do Windows
    On Error Resume Next
    StrTemp = "2.5"
    NumTemp = CDbl(StrTemp)
    If NumTemp <> 2.5 Then
        sDecimalDoWindows = ","
    Else
        sDecimalDoWindows = "."
    End If
    On Error GoTo -1
    ' Fim: Obter Simbolo Decimal do Windows

    iPos = InStr(sArg1, gSimboloDecimal)
    If iPos <> 0 Then
        sTemp = Mid(sArg1, 1, iPos - 1) & sDecimalDoWindows & Mid(sArg1, iPos + 1)
    Else
        sTemp = sArg1
    End If
    
    If sTemp = sDecimalDoWindows Then sTemp = ""
    If sTemp <> "" Then sTemp = BL_String2Double(sTemp) 'Convertendo para Double, tamb�m d� para Single

    BG_CvDecimalParaCalculo = sTemp
    
End Function

Public Function BG_CvDecimalParaDisplay(ByVal sArg1 As String) As String
    Dim iPos As Integer
    Dim sTemp As String
    
    iPos = InStr(sArg1, cFormatoDecBD)
    If iPos <> 0 Then
        sTemp = Mid(sArg1, 1, iPos - 1) & gSimboloDecimal & Mid(sArg1, iPos + 1)
    Else
        sTemp = sArg1
    End If

    BG_CvDecimalParaDisplay = sTemp
    
End Function

Public Function BG_CvDataParaWhere(ByVal sArg1 As String) As String
    
    Dim iDia, iMes, iAno As Integer
    Dim sTemp As String
    
    If sArg1 = "" Or Not IsDate(sArg1) Then
        sTemp = sArg1
    Else
        iDia = BG_DaValorData(sArg1, "DIA")
        iMes = BG_DaValorData(sArg1, "MES")
        iAno = BG_DaValorData(sArg1, "ANO")
        
        sTemp = "#" & iMes & "/" & iDia & "/" & iAno & "#"
    End If
    
    BG_CvDataParaWhere = sTemp

End Function


Public Sub BG_VersaoBG()
    Dim sTemp As String
    
    sTemp = bgNome & vbCrLf & vbCrLf & vbCrLf & "Vers�o: " & bgVersao & "    (" & bgData & ")" & vbCrLf & vbCrLf & bgAutor
    
    BG_Mensagem mediMsgBox, sTemp, vbInformation, "Sobre " & bgNome
End Sub


Public Function BG_CvDecimalParaWhere(ByVal sArg1 As String) As String
    Dim iTemp As Integer
    Dim sTemp As String
    
    If sArg1 = "" Then
        sTemp = sArg1
    Else
        iTemp = InStr(sArg1, gSimboloDecimal)
        If iTemp <> 0 Then
            sTemp = Mid(sArg1, 1, iTemp - 1) & "." & Mid(sArg1, iTemp + 1)
        Else
            sTemp = sArg1
        End If
    End If

    BG_CvDecimalParaWhere = sTemp

End Function

Public Function BG_SYS_GetComputerName() As String
    Dim sBuffer As String
    Dim lSize As Long
    Dim res
    
    On Error GoTo TrataErro
    
    lSize = 250
    sBuffer = Space(lSize)
    res = GetComputerName(sBuffer, lSize)
    
    BG_SYS_GetComputerName = left(sBuffer, Len(RTrim(sBuffer)) - 1)
    Exit Function
    
TrataErro:
    BG_TrataErro "BG_SYS_GetComputerName", Err
End Function

Public Sub BG_SYS_Sleep(ByVal dwMilliseconds As Long)
    On Error GoTo TrataErro
    
    Sleep dwMilliseconds
    Exit Sub
    
TrataErro:
    BG_TrataErro "BG_SYS_Sleep", Err
End Sub

Public Function BG_SYS_GetWindowsDirectory() As String
    Dim sBuffer As String
    Dim lSize As Long
    Dim res
    
    On Error GoTo TrataErro
    
    lSize = 250
    sBuffer = Space(lSize)
    res = GetWindowsDirectory(sBuffer, lSize)

    BG_SYS_GetWindowsDirectory = left(sBuffer, Len(RTrim(sBuffer)) - 1)
    Exit Function
    
TrataErro:
    BG_TrataErro "BG_SYS_GetWindowsDirectory", Err
End Function

Public Function BG_SYS_GetSystemDirectory() As String
    Dim sBuffer As String
    Dim lSize As Long
    Dim res
    
    On Error GoTo TrataErro
    
    lSize = 250
    sBuffer = Space(lSize)
    res = GetSystemDirectory(sBuffer, lSize)

    BG_SYS_GetSystemDirectory = left(sBuffer, Len(RTrim(sBuffer)) - 1)
    Exit Function
    
TrataErro:
    BG_TrataErro "BG_SYS_GetSystemDirectory", Err
End Function

Public Function BG_SYS_GetTempPath() As String
    Dim sBuffer As String
    Dim lSize As Long
    Dim res
    
    On Error GoTo TrataErro
    
    lSize = 250
    sBuffer = Space(lSize)
    res = GetTempPath(lSize, sBuffer)

    BG_SYS_GetTempPath = left(sBuffer, Len(RTrim(sBuffer)) - 2)
    Exit Function
    
TrataErro:
    BG_TrataErro "BG_SYS_GetTempPath", Err
End Function

Public Function BG_SYS_GetUserName() As String
    Dim sBuffer As String
    Dim lSize As Long
    Dim res
    
    On Error GoTo TrataErro
    
    lSize = 250
    sBuffer = Space(lSize)
    res = GetUserName(sBuffer, lSize)

    BG_SYS_GetUserName = left(sBuffer, Len(RTrim(sBuffer)) - 1)
    Exit Function
    
TrataErro:
    BG_TrataErro "BG_SYS_GetUserName", Err
End Function



Public Function BG_SYS_GetTerminalName() As String

Dim i As Integer

For i = 1 To 33
    'Debug.Print Environ(i)
    If Mid(Environ(i), 1, 11) = "CLIENTNAME=" Then
        BG_SYS_GetTerminalName = UCase(Mid(Environ(i), 12))
        If BG_SYS_GetTerminalName = "CONSOLE" Or BG_SYS_GetTerminalName = "CONSOLA" Then
            BG_SYS_GetTerminalName = ""
        End If
    End If
Next i
End Function

Public Function BG_DaRegionalSetting(sArg As String) As String
    Dim hLocation As Long
    Dim sSection As String
    Dim sKey As String
    Dim sSetting As String
    Dim res
    Dim sSistema As String
    Dim sMensagem As String

        Dim iRes As Integer
        Dim iNumLinha() As Integer
        Dim sStrLinha() As String
        Dim iNumOc As Integer


    ' Inicio: Obt�m Sistema
    hLocation = HKEY_LOCAL_MACHINE
    sSection = "SOFTWARE\Microsoft\Windows NT\CurrentVersion"
    sKey = "CurrentVersion"
    
    res = BG_RegGetSetting(hLocation, sSection, sKey, "Nada")
    If res <> "Nada" Then
        sSistema = "Win NT"
    Else
        sSistema = "Win 95"
    End If
    ' Fim: Obt�m Sistema

    ' Inicio: Obt�m Pedido
    hLocation = HKEY_CURRENT_USER
    sSection = "Control Panel\International"
    sKey = sArg
    
    res = BG_RegGetSetting(hLocation, sSection, sKey, "Nada")
    ' Fim: Obt�m Pedido
    
    sMensagem = sSistema & ":" & vbCrLf & "-----------" & vbCrLf & vbCrLf
    
    If res = "Nada" Then
        sMensagem = sMensagem & "� necess�rio colocar no REGISTRY o valor '" & sArg & "' para poder continuar, em:" & vbCrLf & vbCrLf & "     HKEY_CURRENT_USER\Control Panel\International"
        Beep
        BG_Mensagem mediMsgBox, sMensagem, vbCritical, "ATEN��O"
        End
    Else
        If sSistema = "Win 95" Then
            iRes = BG_ProcuraEmFicheiro(BG_SYS_GetWindowsDirectory & "\win.ini", sArg, 1, iNumLinha, sStrLinha, iNumOc)
            If iNumOc = 0 Then
                sMensagem = sMensagem & "� necess�rio colocar no WIN.INI o valor '" & sArg & "' para poder continuar, em:" & vbCrLf & vbCrLf & "     [Intl]"
                Beep
                BG_Mensagem mediMsgBox, sMensagem, vbCritical, "ATEN��O"
                End
            Else
                If Trim(res) <> Trim(Right(sStrLinha(0), Len(sStrLinha(0)) - InStr(sStrLinha(0), "="))) Then
                    sMensagem = sMensagem & "� necess�rio que o valor '" & sArg & "' seja igual no REGISTRY e no WIN.INI, em:" & vbCrLf & vbCrLf & "     REGISTRY:  HKEY_CURRENT_USER\Control Panel\International" & vbCrLf & vbCrLf & "     WIN.INI:  [Intl]"
                    Beep
                    BG_Mensagem mediMsgBox, sMensagem, vbCritical, "ATEN��O"
                    End
                Else
                    BG_DaRegionalSetting = res
                End If
            End If
        Else
            BG_DaRegionalSetting = res
        End If
    End If
End Function


Public Function BG_ExecutaSELECT(Query As String, Optional status As Boolean) As ADODB.recordset
    
    Dim rs As ADODB.recordset
    On Error GoTo TrataErro
  
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros Query
    rs.Open Query, gConexao

    Set BG_ExecutaSELECT = rs
    status = True
    Exit Function
TrataErro:
    BG_LogFile_Erros "BaseDados.BG_ExecutaSELECT" & Err.Number & " - " & Err.Description
    status = False
End Function

Public Sub BG_DestroiRecordSet(rs As ADODB.recordset)
    
    If (rs.state = adStateOpen) Then
        rs.Close
        Set rs = Nothing
    End If
    
End Sub

' Pinta uma lista com linhas intercaladas com duas cores.
Public Sub BG_SetListViewColor(�lvCtrlListView� As ListView, �pCtrlPictureBox� As PictureBox, �lColorOne� As Long, Optional �lColorTwo� As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo ErrorHandler
    lColor1 = �lColorOne�
    lColor2 = �lColorTwo�
    �lvCtrlListView�.Picture = LoadPicture("")
    �lvCtrlListView�.Refresh
    �pCtrlPictureBox�.Cls
    �pCtrlPictureBox�.AutoRedraw = True
    �pCtrlPictureBox�.BorderStyle = vbBSNone
    �pCtrlPictureBox�.ScaleMode = vbTwips
    �pCtrlPictureBox�.Visible = False
    �lvCtrlListView�.PictureAlignment = lvwTile
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    �pCtrlPictureBox�.top = �lvCtrlListView�.top
    �pCtrlPictureBox�.Font = �lvCtrlListView�.Font
    With �pCtrlPictureBox�.Font
        .Size = �lvCtrlListView�.Font.Size + 2
        .Bold = �lvCtrlListView�.Font.Bold
        .Charset = �lvCtrlListView�.Font.Charset
        .Italic = �lvCtrlListView�.Font.Italic
        .Name = �lvCtrlListView�.Font.Name
        .Strikethrough = �lvCtrlListView�.Font.Strikethrough
        .Underline = �lvCtrlListView�.Font.Underline
        .Weight = �lvCtrlListView�.Font.Weight
    End With
    �pCtrlPictureBox�.Refresh
    lLineHeight = �pCtrlPictureBox�.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = �lvCtrlListView�.Width
    �pCtrlPictureBox�.Height = lBarHeight * 2
    �pCtrlPictureBox�.Width = lBarWidth
    �pCtrlPictureBox�.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    �pCtrlPictureBox�.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    �pCtrlPictureBox�.AutoSize = True
    �lvCtrlListView�.Picture = �pCtrlPictureBox�.Image
    �lvCtrlListView�.Refresh
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'BG_SetListViewColor' (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Public Function BG_newRound(ByVal NumberIn As Double, Optional ByVal RoundOff As Long = 0) As Variant
' NOTA: Foi detectado um problema na fun��o Round!!! Round(0.525,2) = 0.52 e n�o 0.53!!!
'       Por essa raz�o utiliza-se agora a rotina abaixo - n�o depende do S.Operativo!
    Dim nFactor As Double
    Dim nTemp As Double
'    myRound = Format(NumberIn, "#" & IIf(RoundOff, ".", "") & String(RoundOff, "0"))

    nFactor = 10 ^ RoundOff
    nTemp = (NumberIn * nFactor) + 0.5
    BG_newRound = Int(CDec(nTemp)) / nFactor
End Function

' pferreira 2011.06.16
Public Sub BG_MostraListaSel(ByVal ValorCampoBD As Variant, NomeControl As Control)
    
    Dim i As Integer
    
    If IsNull(ValorCampoBD) Or Not IsNumeric(ValorCampoBD) Then
        NomeControl.ListIndex = -1
        Exit Sub
    End If

    i = 0
    While (i < NomeControl.ListCount)
        If NomeControl.ItemData(i) = CLng(ValorCampoBD) Then
            NomeControl.Selected(i) = True
        Else
            NomeControl.Selected(i) = False
        End If
        i = i + 1
    Wend

End Sub

'BRUNODSANTOS glintt-hs-18011
Public Function BG_EncodeBase64(ByRef arrData() As Byte) As String
    Dim objXML As MSXML2.DOMDocument
    Dim objNode As MSXML2.IXMLDOMElement
    
    Set objXML = New MSXML2.DOMDocument
    
    Set objNode = objXML.createElement("b64")
    objNode.dataType = "bin.base64"
    objNode.nodeTypedValue = arrData
    BG_EncodeBase64 = objNode.Text
 
    Set objNode = Nothing
    Set objXML = Nothing
End Function

VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormFactContaCorrente 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormFactContaCorrente"
   ClientHeight    =   5130
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   9645
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5130
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcTUtente 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   1320
      Locked          =   -1  'True
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   5400
      Width           =   1215
   End
   Begin VB.TextBox EcSeqUtente 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   5400
      Width           =   1215
   End
   Begin VB.TextBox EcNomeUte 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   2400
      TabIndex        =   5
      Top             =   120
      Width           =   6735
   End
   Begin VB.TextBox EcUtente 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   1080
      TabIndex        =   4
      Top             =   120
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Height          =   855
      Left            =   5640
      TabIndex        =   1
      Top             =   4080
      Width           =   3495
      Begin VB.Label LbSaldo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1800
         TabIndex        =   7
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Saldo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   720
         TabIndex        =   2
         Top             =   240
         Width           =   735
      End
   End
   Begin MSFlexGridLib.MSFlexGrid FgMovimentos 
      Height          =   3135
      Left            =   0
      TabIndex        =   0
      Top             =   840
      Width           =   9135
      _ExtentX        =   16113
      _ExtentY        =   5530
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
      Appearance      =   0
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7320
      Top             =   3360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Label Label2 
      Caption         =   "Utente"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "FormFactContaCorrente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Estado As Integer

Const vermelho = &HC0C0FF
Const Verde = &HC0FFC0
Const Amarelo = &HC0FFFF


Private Type contaCorr
    seq_utente As Long
    Utente As String
    t_utente As String
    nome_ute As String
    n_req As Long
    dt_chega As String
    n_doc As String
    serie_doc As String
    dt_doc As String
    hr_doc As String
    user_doc As String
    nome_user_doc As String
    operacao As String
    valor As Double
    cod_efr As Long
    descr_efr As String
    Estado As String
End Type
Dim estrutContaCorr() As contaCorr
Dim totalContaCorr As Integer

Const lColRequis = 0
Const lColData = 1
Const lColDoc = 4
Const lColUtil = 5
Const lColTipo = 3
Const lColValor = 6

Const lColEntidade = 2

Const lOperRecibo = "Fact.Recibo"
Const lOperDivida = "Divida"
Const lOperCancRecibo = "Canc. Fact.Recibo"
Const lOperAdiantamento = "Adiantamento"
Const lOperCancAdiantamento = "Canc. Adiantamento"

Private Function ConstroiQuery() As String
    Dim sSql As String

If gPassaRecFactus <> mediSim Then
    sSql = "SELECT x0.seq_utente,x0.t_utente, x0.utente, x0.nome_ute, x1.n_req, x1.dT_chega, x1.hr_chega, x2.serie_rec, x2.n_Rec,"
    sSql = sSql & "  x2.total_pagar, x2.user_emi, x2.dt_emi, x2.hr_emi, estado, x3.cod_efr, x3.descr_efr, " & BL_TrataStringParaBD(lOperRecibo) & " operacao, x5.nome nome_utilizador "
    sSql = sSql & " FROM sl_identif x0, sl_requis x1, sl_recibos x2 LEFT OUTER JOIN  sl_idutilizador x5 ON x2.user_emi = x5.cod_utilizador, sl_efr x3 "
    sSql = sSql & " WHERE x1.n_req = x2.n_req and x2.cod_efr  = x3.cod_efr  AND x1.seq_utente = " & EcSeqUtente.text
    sSql = sSql & " AND x0.seq_utente = x1.seq_utente AND x1.seq_utente = " & EcSeqUtente.text & " AND x2.total_pagar >0 "
    sSql = sSql & " AND x2.estado IN (" & BL_TrataStringParaBD(gEstadoReciboPago) & ")"
        
    sSql = sSql & "UNION SELECT x0.seq_utente,x0.t_utente, x0.utente, x0.nome_ute, x1.n_req, x1.dT_chega, x1.hr_chega, x2.serie_rec, x2.n_Rec,"
    sSql = sSql & " 0 - x2.total_pagar, x2.user_emi, x2.dt_emi, x2.hr_emi, estado, x3.cod_efr, x3.descr_efr, " & BL_TrataStringParaBD(lOperDivida) & " operacao, x5.nome nome_utilizador "
    sSql = sSql & " FROM sl_identif x0, sl_requis x1, sl_recibos x2 LEFT OUTER JOIN  sl_idutilizador x5 ON x2.user_emi = x5.cod_utilizador, sl_efr x3 "
    sSql = sSql & " WHERE x1.n_req = x2.n_req and x2.cod_efr  = x3.cod_efr  AND x1.seq_utente = " & EcSeqUtente.text
    sSql = sSql & " AND x0.seq_utente = x1.seq_utente AND x1.seq_utente = " & EcSeqUtente.text & " AND x2.total_pagar >0  "
    sSql = sSql & " AND x2.estado IN (" & BL_TrataStringParaBD(gEstadoReciboCobranca) & "," & BL_TrataStringParaBD(gEstadoReciboNaoEmitido) & "," & BL_TrataStringParaBD(gEstadoReciboPerdido) & ")"
    
    
    sSql = sSql & " UNION SELECT x0.seq_utente, x0.t_utente, x0.utente, x0.nome_ute, x1.n_req, x1.dT_chega, x1.hr_chega, x4.serie_rec, "
    sSql = sSql & " x4.n_Rec,0- x2.total_pagar, x4.user_canc user_emi, x4.dt_canc dt_emi, x4.hr_canc hr_emi, x2.estado, x3.cod_efr, x3.descr_efr, "
    sSql = sSql & BL_TrataStringParaBD(lOperCancRecibo) & " operacao, x5.nome nome_utilizador "
    sSql = sSql & " FROM sl_identif x0, sl_requis x1, sl_recibos x2, sl_efr x3, sl_recibos_canc x4 LEFT OUTER JOIN  sl_idutilizador x5 ON x4.user_canc = x5.cod_utilizador "
    sSql = sSql & " WHERE x1.n_req = x2.n_req and x2.cod_efr  = x3.cod_efr  AND x2.n_rec = x4.n_Rec and x2.serie_rec = x4.serie_rec "
    sSql = sSql & " AND x0.seq_utente = x1.seq_utente AND x2.n_rec <> '0' AND x1.seq_utente = " & EcSeqUtente.text
    
    sSql = sSql & " UNION SELECT x0.seq_utente, x0.t_utente, x0.utente, x0.nome_ute, x1.n_req, x1.dT_chega, x1.hr_chega, null serie_rec,"
    sSql = sSql & "  x2.n_adiantamento n_Rec, x2.valor total_pagar, x2.user_emi, x2.dt_emi, x2.hr_emi, x2.estado, null cod_efr, null descr_efr,"
    sSql = sSql & BL_TrataStringParaBD(lOperAdiantamento) & " operacao, x5.nome nome_utilizador "
    sSql = sSql & " FROM sl_identif x0, sl_requis x1, sl_adiantamentos x2 LEFT OUTER JOIN  sl_idutilizador x5 ON x2.user_emi = x5.cod_utilizador "
    sSql = sSql & " WHERE x1.n_req = x2.n_req  and x2.dt_anul IS NULL"
    sSql = sSql & " AND x0.seq_utente = x1.seq_utente AND x1.seq_utente = " & EcSeqUtente.text
    
    sSql = sSql & " UNION SELECT x0.seq_utente, x0.t_utente, x0.utente, x0.nome_ute, x1.n_req, x1.dT_chega, x1.hr_chega, null serie_rec, "
    sSql = sSql & " x2.n_adiantamento n_Rec,0- x2.valor total_pagar, x2.user_anul user_emi, x2.dt_anul dt_emi, x2.hr_anul hr_emi, x2.estado, "
    sSql = sSql & " null cod_efr, null descr_efr, " & BL_TrataStringParaBD(lOperCancAdiantamento) & " operacao, x5.nome nome_utilizador "
    sSql = sSql & " FROM sl_identif x0, sl_requis x1, sl_adiantamentos x2 LEFT OUTER JOIN  sl_idutilizador x5 ON x2.user_anul = x5.cod_utilizador "
    sSql = sSql & " WHERE x1.n_req = x2.n_req  and x2.dt_anul IS NOT NULL"
    sSql = sSql & " AND x0.seq_utente = x1.seq_utente AND x1.seq_utente = " & EcSeqUtente.text
    sSql = sSql & " ORDER BY dt_chega DESC,  n_req DESC, n_rec DESC, dt_emi ASC "
    
Else
    sSql = "  SELECT x0.seq_utente,x0.t_utente, x0.utente, x0.nome_ute, x1.n_req, x1.dT_chega, x1.hr_chega, x2.serie_doc serie_rec, x2.n_doc n_rec,"
    sSql = sSql & "  x2.val_doc total_pagar, null user_emi, x2.dt_emiss_doc dt_emi,null hr_emi, x2.flg_estado estado, x3.cod_efr, x3.descr_efr, " & BL_TrataStringParaBD(lOperRecibo) & " operacao, null nome_utilizador "
    sSql = sSql & " FROM sl_identif x0, sl_requis x1, slv_documentos_doente x2, sl_efr x3 "
    sSql = sSql & " WHERE x1.n_req = x2.episodio and x2.cod_efr  = x3.cod_efr  "
    sSql = sSql & " AND x0.seq_utente = x1.seq_utente AND x1.seq_utente = " & EcSeqUtente.text & " AND x2.val_doc >0 AND x2.t_doente = x0.t_utente and x2.doente = x0.utente "
    sSql = sSql & " AND x2.t_doente = " & BL_TrataStringParaBD(EcTUtente.text) & " AND x2.doente = " & BL_TrataStringParaBD(EcUtente.text) & " and x2.tipo <> 'NC' "

    sSql = sSql & " UNION SELECT x0.seq_utente,x0.t_utente, x0.utente, x0.nome_ute, x1.n_req, x1.dT_chega, x1.hr_chega, null serie_rec, null n_rec,"
    sSql = sSql & "  x2.valor total_pagar, null user_emi, null dt_emi,null hr_emi, null estado, x3.cod_efr, x3.descr_efr, " & BL_TrataStringParaBD(lOperDivida) & " operacao, null nome_utilizador "
    sSql = sSql & " FROM sl_identif x0, sl_requis x1, slv_divida_utente x2, sl_efr x3 "
    sSql = sSql & " WHERE x1.n_req = x2.n_req and x1.cod_efr  = x3.cod_efr  "
    sSql = sSql & " AND x0.seq_utente = x1.seq_utente AND x1.seq_utente = " & EcSeqUtente.text & " AND x2.valor >0 AND x2.t_utente = x0.t_utente and x2.utente = x0.utente "
    sSql = sSql & " AND x2.t_utente = " & BL_TrataStringParaBD(EcTUtente.text) & " AND x2.utente = " & BL_TrataStringParaBD(EcUtente.text)
    
    sSql = sSql & " UNION SELECT x0.seq_utente,x0.t_utente, x0.utente, x0.nome_ute, x1.n_req, x1.dT_chega, x1.hr_chega, x2.serie_doc serie_rec, x2.n_doc n_rec,"
    sSql = sSql & "  x2.val_doc total_pagar, null user_emi, x2.dt_emiss_doc dt_emi,null hr_emi, x2.flg_estado estado, x3.cod_efr, x3.descr_efr, " & BL_TrataStringParaBD(lOperCancRecibo) & " operacao, null nome_utilizador "
    sSql = sSql & " FROM sl_identif x0, sl_requis x1, slv_documentos_doente x2, sl_efr x3 "
    sSql = sSql & " WHERE x1.n_req = x2.episodio and x2.cod_efr  = x3.cod_efr  "
    sSql = sSql & " AND x0.seq_utente = x1.seq_utente AND x1.seq_utente = " & EcSeqUtente.text & " AND x2.val_doc >0 AND x2.t_doente = x0.t_utente and x2.doente = x0.utente "
    sSql = sSql & " AND x2.t_doente = " & BL_TrataStringParaBD(EcTUtente.text) & " AND x2.doente = " & BL_TrataStringParaBD(EcUtente.text) & " and x2.tipo = 'NC' "


    sSql = sSql & " ORDER BY dt_chega DESC,  n_req DESC, n_rec DESC, dt_emi ASC "
End If
    ConstroiQuery = sSql
End Function

Private Sub LimpaFgMovimentos()
    Dim i As Integer
    FgMovimentos.rows = 2
    For i = 0 To FgMovimentos.Cols - 1
        FgMovimentos.TextMatrix(1, i) = ""
    Next
End Sub
Private Sub PreencheFgMovimentos()
    Dim iMov As Integer
    Dim cor As Long
    LimpaFgMovimentos
    EcUtente.text = estrutContaCorr(1).t_utente & "/" & estrutContaCorr(1).Utente
    EcNomeUte.text = estrutContaCorr(1).nome_ute
    For iMov = 1 To totalContaCorr
        FgMovimentos.row = iMov
        If estrutContaCorr(iMov).dt_doc <> "" Then
            FgMovimentos.TextMatrix(iMov, lColData) = estrutContaCorr(iMov).dt_doc
        Else
            FgMovimentos.TextMatrix(iMov, lColData) = estrutContaCorr(iMov).dt_chega
        End If
        If estrutContaCorr(iMov).n_doc <> "0" Then
            FgMovimentos.TextMatrix(iMov, lColDoc) = estrutContaCorr(iMov).serie_doc & "/" & estrutContaCorr(iMov).n_doc
        Else
            FgMovimentos.TextMatrix(iMov, lColDoc) = ""
        End If
        
        FgMovimentos.TextMatrix(iMov, lColEntidade) = estrutContaCorr(iMov).descr_efr
        
        If estrutContaCorr(iMov).operacao = lOperDivida Then
            cor = vermelho
        ElseIf estrutContaCorr(iMov).operacao = lOperRecibo Then
            cor = Verde
        ElseIf estrutContaCorr(iMov).operacao = lOperCancRecibo Then
            cor = Amarelo
        ElseIf estrutContaCorr(iMov).operacao = lOperAdiantamento Then
            cor = Verde
        ElseIf estrutContaCorr(iMov).operacao = lOperCancAdiantamento Then
            cor = Amarelo
        End If
        FgMovimentos.TextMatrix(iMov, lColRequis) = estrutContaCorr(iMov).n_req
        FgMovimentos.TextMatrix(iMov, lColTipo) = estrutContaCorr(iMov).operacao
        FgMovimentos.TextMatrix(iMov, lColUtil) = estrutContaCorr(iMov).nome_user_doc
        FgMovimentos.TextMatrix(iMov, lColValor) = Format(estrutContaCorr(iMov).valor, "0.00") & " �"
        colocaCorFgMovimentos cor
        FgMovimentos.AddItem ""
    Next iMov
End Sub
Private Sub colocaCorFgMovimentos(cor As Long)
    Dim iMov As Integer
    For iMov = 0 To FgMovimentos.Cols - 1
        FgMovimentos.Col = iMov
        FgMovimentos.CellBackColor = cor
    Next iMov
End Sub
Private Sub PreencheEstrutContaCorr(seq_utente As Long, Utente As String, t_utente As String, nome_ute As String, n_req As Long, _
                                    dt_chega As String, dt_doc As String, hr_doc As String, user_doc As String, nome_user_doc As String, _
                                    operacao As String, valor As Double, cod_efr As Long, descr_efr As String, serie_doc As String, _
                                    n_doc As String, Estado As String)
    totalContaCorr = totalContaCorr + 1
    ReDim Preserve estrutContaCorr(totalContaCorr)
    estrutContaCorr(totalContaCorr).seq_utente = seq_utente
    estrutContaCorr(totalContaCorr).Utente = Utente
    estrutContaCorr(totalContaCorr).t_utente = t_utente
    estrutContaCorr(totalContaCorr).nome_ute = nome_ute
    estrutContaCorr(totalContaCorr).n_req = n_req
    estrutContaCorr(totalContaCorr).dt_chega = dt_chega
    estrutContaCorr(totalContaCorr).dt_doc = dt_doc
    estrutContaCorr(totalContaCorr).hr_doc = hr_doc
    estrutContaCorr(totalContaCorr).user_doc = user_doc
    estrutContaCorr(totalContaCorr).nome_user_doc = nome_user_doc
    estrutContaCorr(totalContaCorr).operacao = operacao
    estrutContaCorr(totalContaCorr).valor = valor
    estrutContaCorr(totalContaCorr).cod_efr = cod_efr
    estrutContaCorr(totalContaCorr).descr_efr = descr_efr
    estrutContaCorr(totalContaCorr).serie_doc = serie_doc
    estrutContaCorr(totalContaCorr).n_doc = n_doc
    estrutContaCorr(totalContaCorr).Estado = Estado
End Sub

Private Sub EcDescrTubo_Change()

End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Conta Corrente Utente"
    Me.left = 50
    Me.top = 50
    Me.Width = 9345
    Me.Height = 5475 ' Normal
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormFactContaCorrente = Nothing
End Sub



Sub DefTipoCampos()
    With FgMovimentos
        .rows = 2
        .FixedRows = 1
        .Cols = 7
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        
        .ColWidth(lColRequis) = 800
        .Col = lColRequis
        .TextMatrix(0, lColRequis) = "Requis."
        
        .ColWidth(lColData) = 1600
        .Col = lColData
        .TextMatrix(0, lColData) = "Data"
        
        .ColWidth(lColEntidade) = 1700
        .TextMatrix(0, lColEntidade) = "Entidade"
                
        .ColWidth(lColTipo) = 1000
        .TextMatrix(0, lColTipo) = "Opera��o"
        
        
         .ColWidth(lColDoc) = 1700
        .TextMatrix(0, lColDoc) = "Documento"
         
         .ColWidth(lColUtil) = 1000
        .TextMatrix(0, lColUtil) = "Operador"
         
         .ColWidth(lColValor) = 1000
        .TextMatrix(0, lColValor) = "Valor"
        
        .Col = 0
    End With

End Sub

Sub PreencheCampos()
    

End Sub


Sub FuncaoLimpar()
    

End Sub

Sub FuncaoEstadoAnterior()

End Sub



Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsConta As New ADODB.recordset
    Dim saldo As Double
    totalContaCorr = 0
    ReDim estrutContaCorr(totalContaCorr)
    
    saldo = 0
    If EcSeqUtente.text <> "" Then
        sSql = ConstroiQuery
        rsConta.CursorType = adOpenStatic
        rsConta.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsConta.Open sSql, gConexao
        If rsConta.RecordCount > 0 Then
            While Not rsConta.EOF
                    PreencheEstrutContaCorr BL_HandleNull(rsConta!seq_utente, mediComboValorNull), BL_HandleNull(rsConta!Utente, ""), _
                                            BL_HandleNull(rsConta!t_utente, ""), BL_HandleNull(rsConta!nome_ute, ""), _
                                            BL_HandleNull(rsConta!n_req, mediComboValorNull), BL_HandleNull(rsConta!dt_chega, ""), _
                                            BL_HandleNull(rsConta!dt_emi, ""), BL_HandleNull(rsConta!hr_emi, ""), BL_HandleNull(rsConta!user_emi, ""), _
                                            BL_HandleNull(rsConta!nome_utilizador, ""), BL_HandleNull(rsConta!operacao, ""), _
                                            BL_HandleNull(rsConta!total_pagar, ""), BL_HandleNull(rsConta!cod_efr, ""), _
                                            BL_HandleNull(rsConta!descr_efr, ""), BL_HandleNull(rsConta!serie_rec, ""), _
                                            BL_HandleNull(rsConta!n_rec, ""), BL_HandleNull(rsConta!Estado, "")
                    If estrutContaCorr(totalContaCorr).operacao = lOperDivida Or estrutContaCorr(totalContaCorr).operacao = lOperCancAdiantamento Or _
                        estrutContaCorr(totalContaCorr).operacao = lOperAdiantamento Then
                            saldo = saldo + estrutContaCorr(totalContaCorr).valor
                    End If
                rsConta.MoveNext
            Wend
        End If
        rsConta.Close
        Set rsConta = Nothing
        If totalContaCorr > 0 Then
            PreencheFgMovimentos
            If saldo < 0 Then
                LbSaldo.ForeColor = vbRed
            Else
                LbSaldo.ForeColor = vbBlack
            End If
            LbSaldo.caption = Format(saldo, "0.00") & " �"
        Else
            EventoUnload
        End If
    Else
        EventoUnload
    End If
    
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()


End Sub



Sub FuncaoModificar()


End Sub



Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub







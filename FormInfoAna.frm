VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormInfoAna 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormInfoAna"
   ClientHeight    =   8925
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   10755
   Icon            =   "FormInfoAna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8925
   ScaleWidth      =   10755
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcCodTInformacao 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   2640
      Locked          =   -1  'True
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   7320
      Width           =   615
   End
   Begin VB.TextBox EcNReq 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   7320
      Width           =   615
   End
   Begin RichTextLib.RichTextBox RtInformacao 
      Height          =   1455
      Left            =   120
      TabIndex        =   5
      Top             =   3960
      Width           =   10455
      _ExtentX        =   18441
      _ExtentY        =   2566
      _Version        =   393217
      BackColor       =   14737632
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      Appearance      =   0
      TextRTF         =   $"FormInfoAna.frx":000C
   End
   Begin VB.TextBox EcSeqReqTubo 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   840
      Locked          =   -1  'True
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   7320
      Width           =   615
   End
   Begin MSFlexGridLib.MSFlexGrid FgAnaInfo 
      Height          =   3255
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   5741
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
      Appearance      =   0
   End
   Begin VB.TextBox EcCodTubo 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   1080
      TabIndex        =   2
      Top             =   120
      Width           =   615
   End
   Begin VB.TextBox EcDescrTubo 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   1800
      TabIndex        =   1
      Top             =   120
      Width           =   4575
   End
   Begin VB.Label Label1 
      Caption         =   "Tubo"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "FormInfoAna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Type infoAna
    seq_req_tubo As Long
    cod_tubo As String
    descR_tubo As String
    cod_ana As String
    descr_ana As String
    cod_informacao As String
    descr_informacao As String
    informacao_rtf As String
    cod_t_informacao As Integer
    descr_t_informacao As String
End Type
Dim estrutInfoAna() As infoAna
Dim totalInfoAna As Integer

Const lColCodTubo = 0
Const lColDescrTubo = 1
Const lColCodAna = 2
Const lColDescrAna = 3
Const lColCodTInfo = 4
Const lColDescrInfo = 5

Dim estado As Integer
Private Sub PreencheEstrutInfoAna(seq_req_tubo As Long, cod_tubo As String, descR_tubo As String, cod_ana As String, descr_ana As String, _
                                    cod_informacao As String, descr_informacao As String, informacao_rtf As String, cod_t_informacao As Integer, _
                                    descr_t_informacao As String)
    totalInfoAna = totalInfoAna + 1
    ReDim Preserve estrutInfoAna(totalInfoAna)
    estrutInfoAna(totalInfoAna).seq_req_tubo = seq_req_tubo
    estrutInfoAna(totalInfoAna).cod_tubo = cod_tubo
    estrutInfoAna(totalInfoAna).descR_tubo = descR_tubo
    estrutInfoAna(totalInfoAna).cod_ana = cod_ana
    estrutInfoAna(totalInfoAna).descr_ana = descr_ana
    estrutInfoAna(totalInfoAna).cod_informacao = cod_informacao
    estrutInfoAna(totalInfoAna).descr_informacao = descr_informacao
    estrutInfoAna(totalInfoAna).informacao_rtf = informacao_rtf
    estrutInfoAna(totalInfoAna).cod_t_informacao = cod_t_informacao
    estrutInfoAna(totalInfoAna).descr_t_informacao = descr_t_informacao
End Sub

Private Sub LimpaFgAnaInfo()
    Dim i As Integer
    FgAnaInfo.rows = 2
    For i = 0 To FgAnaInfo.Cols - 1
        FgAnaInfo.TextMatrix(1, i) = ""
    Next
End Sub
Private Sub PreencheFgAnaInfo()
    Dim iInfo As Integer
    LimpaFgAnaInfo
    If totalInfoAna > 0 Then
        EcCodTubo.Text = estrutInfoAna(1).cod_tubo
        EcDescrTubo.Text = estrutInfoAna(1).descR_tubo
    End If
    For iInfo = 1 To totalInfoAna
        FgAnaInfo.TextMatrix(iInfo, lColCodAna) = estrutInfoAna(iInfo).cod_ana
        FgAnaInfo.TextMatrix(iInfo, lColCodTInfo) = estrutInfoAna(iInfo).descr_t_informacao
        FgAnaInfo.TextMatrix(iInfo, lColCodTubo) = estrutInfoAna(iInfo).cod_tubo
        FgAnaInfo.TextMatrix(iInfo, lColDescrAna) = estrutInfoAna(iInfo).descr_ana
        FgAnaInfo.TextMatrix(iInfo, lColDescrInfo) = estrutInfoAna(iInfo).descr_informacao
        FgAnaInfo.TextMatrix(iInfo, lColDescrTubo) = estrutInfoAna(iInfo).descR_tubo
        FgAnaInfo.AddItem ""
    Next iInfo
End Sub
                                    
Private Sub FgAnaInfo_Click()
    RtInformacao.TextRTF = ""
    If FgAnaInfo.row <= totalInfoAna Then
        RtInformacao.TextRTF = estrutInfoAna(FgAnaInfo.row).informacao_rtf
       'CHUC-12788 06.09.2019
        EcCodTubo.Text = estrutInfoAna(FgAnaInfo.row).cod_tubo
        EcDescrTubo.Text = estrutInfoAna(FgAnaInfo.row).descR_tubo
        '
    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Informa��es Associadas"
    Me.left = 50
    Me.top = 50
    Me.Width = 10845
    Me.Height = 6000 ' Normal
    'CHUC-12788 06.09.2019
    EcCodTubo.Enabled = False
    EcDescrTubo.Enabled = False					   
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormInfoAna = Nothing
End Sub



Sub DefTipoCampos()
    With FgAnaInfo
        .rows = 2
        .FixedRows = 1
        .Cols = 6
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarNone
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLinesFixed = flexGridInset
        .GridColorFixed = flexTextInsetLight
        .MergeCells = flexMergeFree
        .PictureType = flexPictureColor
        .RowHeightMin = 280
        .row = 0
        
        .ColWidth(lColCodTubo) = 0
        .Col = lColCodTubo
        .TextMatrix(0, lColCodTubo) = "C�d.Tubo"
        
        .ColWidth(lColDescrTubo) = 2000
        .Col = lColDescrTubo
        .TextMatrix(0, lColDescrTubo) = "Desc.Tubo"
        
        .ColWidth(lColCodAna) = 1000
        .TextMatrix(0, lColCodAna) = "C�d.Ana"
                
        .ColWidth(lColDescrAna) = 2000
        .TextMatrix(0, lColDescrAna) = "Desc.Ana"
        
         .ColWidth(lColCodTInfo) = 2000
        .TextMatrix(0, lColCodTInfo) = "Tipo"
        
         .ColWidth(lColDescrInfo) = 5000
        .TextMatrix(0, lColDescrInfo) = "Informa��o"
        
        .Col = 0
    End With
End Sub

Sub PreencheCampos()
    

End Sub


Sub FuncaoLimpar()
    

End Sub

Sub FuncaoEstadoAnterior()

End Sub



Sub FuncaoProcurar()
    Dim sSql As String
    Dim rsInfo As New ADODB.recordset
    totalInfoAna = 0
    ReDim estrutInfoAna(totalInfoAna)
    
    If EcNReq.Text <> "" Then
        sSql = " SELECT DISTINCT x1.seq_req_tubo, x5.cod_tubo, x5.descr_tubo, x6.cod_ana, x6.descr_ana, x4.descr_informacao, x7.descr_t_informacao, x4.informacao, "
        sSql = sSql & " x7.cod_t_informacao, x7.descR_t_informacao, x4.cod_informacao "
        sSql = sSql & " FROM sl_req_tubo  x1, sl_marcacoes x2, sl_ana_informacao x3, sl_informacao x4, sl_tubo x5, slv_analises_apenas x6, sl_tbf_t_informacao x7"
        sSql = sSql & " WHERE  X1.seq_req_tubo = X2.seq_req_tubo And X2.cod_ana_s = X3.cod_ana And X3.cod_informacao = x4.cod_informacao"
        sSql = sSql & " AND x1.cod_tubo = x5.cod_tubo and x2.cod_ana_s = x6.cod_ana and x4.cod_t_informacao = x7.cod_t_informacao"
        If EcNReq.Text <> "" Then
            sSql = sSql & " AND x1.n_req = " & EcNReq.Text
        End If
        If EcSeqReqTubo.Text <> "" Then
            sSql = sSql & " AND x1.seq_req_tubo = " & EcSeqReqTubo.Text
        End If
        If EcCodTInformacao.Text <> "" Then
            sSql = sSql & " AND x4.cod_t_informacao = " & EcCodTInformacao.Text
        End If
        sSql = sSql & " UNION SELECT DISTINCT x1.seq_req_tubo, x5.cod_tubo, x5.descr_tubo, x6.cod_ana, x6.descr_ana, x4.descr_informacao, x7.descr_t_informacao, x4.informacao, "
        sSql = sSql & " x7.cod_t_informacao, x7.descR_t_informacao, x4.cod_informacao "
        sSql = sSql & " FROM sl_req_tubo  x1, sl_realiza x2, sl_ana_informacao x3, sl_informacao x4, sl_tubo x5, slv_analises_apenas x6, sl_tbf_t_informacao x7"
        sSql = sSql & " WHERE  X1.seq_req_tubo = X2.seq_req_tubo And X2.cod_ana_s = X3.cod_ana And X3.cod_informacao = x4.cod_informacao"
        sSql = sSql & " AND x1.cod_tubo = x5.cod_tubo and x2.cod_ana_s= x6.cod_ana and x4.cod_t_informacao = x7.cod_t_informacao"
        If EcSeqReqTubo.Text <> "" Then
            sSql = sSql & " AND x1.seq_req_tubo = " & EcSeqReqTubo.Text
        End If
        If EcNReq.Text <> "" Then
            sSql = sSql & " AND x1.n_req = " & EcNReq.Text
        End If
        If EcCodTInformacao.Text <> "" Then
            sSql = sSql & " AND x4.cod_t_informacao = " & EcCodTInformacao.Text
        End If
        sSql = sSql & " ORDER BY cod_tubo, cod_t_informacao, cod_ana"
        rsInfo.CursorType = adOpenStatic
        rsInfo.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsInfo.Open sSql, gConexao
        If rsInfo.RecordCount > 0 Then
            While Not rsInfo.EOF
                PreencheEstrutInfoAna BL_HandleNull(rsInfo!seq_req_tubo, mediComboValorNull), BL_HandleNull(rsInfo!cod_tubo, ""), _
                    BL_HandleNull(rsInfo!descR_tubo, ""), BL_HandleNull(rsInfo!cod_ana, ""), BL_HandleNull(rsInfo!descr_ana, ""), _
                    BL_HandleNull(rsInfo!cod_informacao, ""), BL_HandleNull(rsInfo!descr_informacao, ""), _
                    BL_HandleNull(rsInfo!informacao, ""), BL_HandleNull(rsInfo!cod_t_informacao, mediComboValorNull), _
                    BL_HandleNull(rsInfo!descr_t_informacao, "")
                rsInfo.MoveNext
            Wend
        End If
        rsInfo.Close
        Set rsInfo = Nothing
        PreencheFgAnaInfo
        FgAnaInfo.row = FgAnaInfo.rows - 1
        FgAnaInfo_Click
    Else
        EventoUnload
    End If
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()


End Sub



Sub FuncaoModificar()


End Sub



Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub







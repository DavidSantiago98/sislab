VERSION 5.00
Begin VB.Form FormPesquisaRapida 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pesquisa R�pida"
   ClientHeight    =   7575
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7875
   Icon            =   "FormPesquisaRapida.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MousePointer    =   1  'Arrow
   ScaleHeight     =   7575
   ScaleWidth      =   7875
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtCanc 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   3240
      TabIndex        =   4
      Top             =   4320
      Width           =   1335
   End
   Begin VB.CommandButton BtOk 
      Caption         =   "&OK"
      Height          =   375
      Left            =   1440
      TabIndex        =   3
      Top             =   4320
      Width           =   1335
   End
   Begin VB.ListBox EcLista 
      Height          =   3180
      Left            =   240
      TabIndex        =   2
      Top             =   960
      Width           =   5535
   End
   Begin VB.TextBox EcPesquisa 
      Height          =   285
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   5535
   End
   Begin VB.Label Label1 
      Caption         =   "Digite o texto que pretende pesquisar"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   5535
   End
End
Attribute VB_Name = "FormPesquisaRapida"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico
    
Dim TTabela As ADODB.recordset
Dim TTabelaAux As ADODB.recordset
Dim TSQLQuery As String
Dim NomeControl As Control
Dim TCampoChave As String, TCampoPesquisa As String, TNomeTabela As String
Dim TCampoEcra2 As String ' este campo serve para aparecer no ecra um codigo
Dim TCampoRetornoCodigo, TCampoRetornoDesignacao As Variant
Dim TNumeroArgumentosRetorno As Integer
Dim TClausulaWhere As String
Dim TClausulaFrom As String

Function InitPesquisaRapida(NomeTabela As String, _
                            CampoPesquisa As String, CampoChave As String, _
                            ByRef CampoRetornoCodigo As Variant, _
                            Optional ByRef CampoRetornoDesignacao As Variant, _
                            Optional ClausWhere As Variant, _
                            Optional ClausFrom As Variant, _
                            Optional campoCodigo As Variant) As Boolean
    
    Dim i As Integer
    Dim Str1 As String
    
    Me.caption = " Pesquisa R�pida"
    Me.left = 100
    Me.top = 100
    Me.Width = 6165
    Me.Height = 5295
    Set NomeControl = EcLista
    
    InitPesquisaRapida = True
    
    TCampoChave = CampoChave
    TCampoPesquisa = CampoPesquisa
    TNomeTabela = NomeTabela
    TNumeroArgumentosRetorno = 0
    
    ' passagem dos valores de referencia
    If Not IsMissing(CampoRetornoCodigo) Then
       If TypeOf CampoRetornoCodigo Is TextBox Or TypeOf CampoRetornoCodigo Is ComboBox Then
        Set TCampoRetornoCodigo = CampoRetornoCodigo
        TNumeroArgumentosRetorno = TNumeroArgumentosRetorno + 1
      Else
        MsgBox "Tem de passar um objecto no argumento de c�digo de retorno"
        End
      End If
    End If
    If Not IsMissing(CampoRetornoDesignacao) Then
       If TypeOf CampoRetornoDesignacao Is TextBox Or TypeOf CampoRetornoDesignacao Is ComboBox Then
          Set TCampoRetornoDesignacao = CampoRetornoDesignacao
          TNumeroArgumentosRetorno = TNumeroArgumentosRetorno + 1
       Else
          MsgBox "Tem de passar um objecto na argumento de descri��o de retorno"
          End
      End If
    End If
    If Not IsMissing(campoCodigo) Then
        TCampoEcra2 = campoCodigo
    End If
    
    If Not IsMissing(ClausWhere) Then
        TClausulaWhere = ClausWhere
    End If
    If Not IsMissing(ClausFrom) Then
        TClausulaFrom = ClausFrom
    End If
    
    
    TSQLQuery = "Select " & CampoChave
    If TCampoEcra2 <> "" And UCase(TCampoEcra2) <> UCase(TCampoChave) Then
        TSQLQuery = TSQLQuery & "," & TCampoEcra2
    End If
    If UCase(CampoPesquisa) <> UCase(TCampoChave) Then
        TSQLQuery = TSQLQuery & " , " & CampoPesquisa & " FROM " & NomeTabela
    Else
        TSQLQuery = TSQLQuery & " FROM " & NomeTabela
    End If
    If TClausulaFrom <> "" Then
        TSQLQuery = TSQLQuery & "," & TClausulaFrom
    End If
    If TClausulaWhere <> "" Then
        TSQLQuery = TSQLQuery & " WHERE " & TClausulaWhere
    End If
    TSQLQuery = TSQLQuery & " order by " & CampoPesquisa & " ASC"
    
    Set TTabela = New ADODB.recordset
    
    TTabela.CursorType = adOpenStatic
    TTabela.CursorLocation = adUseServer
    
    TTabela.Open TSQLQuery, gConexao
    
    If TTabela.RecordCount <= 0 Then
        InitPesquisaRapida = False
        Unload FormPesquisaRapida
        Set FormPesquisaRapida = Nothing
        Exit Function
    End If
    
    i = 0
    Do Until TTabela.EOF
        Str1 = ""
        If TCampoEcra2 <> "" Then
            Str1 = TTabela(TCampoEcra2) & "   "
        End If
        Str1 = Str1 & TTabela(CampoPesquisa)

        NomeControl.AddItem Str1
        NomeControl.ItemData(i) = CLng(BL_HandleNull(TTabela(CampoChave), 0))
        TTabela.MoveNext
        i = i + 1
    Loop
    TTabela.Close
    Set TTabela = Nothing
    
    Me.Show vbModal
    
End Function

Private Sub BtCanc_Click()
    
    Set TCampoRetornoCodigo = Nothing
    Set TCampoRetornoDesignacao = Nothing
    
    Unload FormPesquisaRapida
    
    Set FormPesquisaRapida = Nothing

End Sub

Private Sub BtOk_Click()
    
    RetornaValores
    
End Sub

Function RetornaValores()
    
    Dim TIndice As Integer
    
    TIndice = EcLista.ListIndex
        
    Me.Visible = False
        
    If EcLista.ListIndex >= 0 Then
        If TNumeroArgumentosRetorno = 2 Then
            TCampoRetornoCodigo.Text = EcLista.ItemData(TIndice)
            TCampoRetornoDesignacao.Text = EcLista.List(TIndice)
        Else
            TCampoRetornoCodigo.Text = EcLista.ItemData(TIndice)
        End If
    End If
    
    Set FormPesquisaRapida = Nothing
    
End Function

Private Sub EcLista_Click()

    RetornaValores

End Sub

Private Sub EcLista_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then
       EcLista_Click
    End If

End Sub

Private Sub EcPesquisa_Change()
     
     RefinaPesquisa

End Sub

Sub RefinaPesquisa()
    
    Dim i As Integer
    Dim TSQLQueryAux As String
    Dim Str1 As String
    TSQLQueryAux = "SELECT " & TCampoChave
    
    If TCampoEcra2 <> "" And UCase(TCampoEcra2) <> UCase(TCampoChave) Then
        TSQLQueryAux = TSQLQueryAux & "," & TCampoEcra2
    End If
    TSQLQueryAux = TSQLQueryAux & " , " & TCampoPesquisa & " FROM " & TNomeTabela
    If TClausulaFrom <> "" Then
        TSQLQueryAux = TSQLQueryAux & "," & TClausulaFrom
    End If
    
    If EcPesquisa <> "" Then
        EcPesquisa = Replace(EcPesquisa, "*", "%")
        EcPesquisa = Replace(EcPesquisa, "?", "_")
        
        If gPesquisaDentroCampo Then
            TSQLQueryAux = TSQLQueryAux & " WHERE UPPER(" & TCampoPesquisa & ") LIKE '%" & UCase(EcPesquisa) & "%'"
        Else
            TSQLQueryAux = TSQLQueryAux & " WHERE UPPER(" & TCampoPesquisa & ") LIKE '" & UCase(EcPesquisa) & "%'"
        End If
        
        If TClausulaWhere <> "" Then
            TSQLQueryAux = TSQLQueryAux & " AND " & TClausulaWhere
        End If
    Else
        If TClausulaWhere <> "" Then
            TSQLQueryAux = TSQLQueryAux & " WHERE " & TClausulaWhere
        End If
    End If
    
    TSQLQueryAux = TSQLQueryAux & " ORDER BY " & TCampoPesquisa
    
    Set TTabelaAux = New ADODB.recordset
    
    TTabelaAux.CursorType = adOpenStatic
    TTabelaAux.CursorLocation = adUseServer
    
    TTabelaAux.Open TSQLQueryAux, gConexao

    NomeControl.ListIndex = mediComboValorNull
    NomeControl.Clear
    i = 0
    Do Until TTabelaAux.EOF
        Str1 = ""
        If TCampoEcra2 <> "" Then
            Str1 = TTabelaAux(TCampoEcra2) & "  "
        End If
        Str1 = Str1 & TTabelaAux(TCampoPesquisa)
        NomeControl.AddItem Str1
        NomeControl.ItemData(i) = CLng(TTabelaAux(TCampoChave))
        TTabelaAux.MoveNext
        i = i + 1
    Loop
    TTabelaAux.Close
    Set TTabelaAux = Nothing

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set FormPesquisaRapida = Nothing
    
End Sub

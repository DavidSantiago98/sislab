VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form FormMBFichMov 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormMBFichMov"
   ClientHeight    =   7800
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   10995
   Icon            =   "FormMBFichMov.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7800
   ScaleWidth      =   10995
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtProcessaFicheiro 
      Height          =   615
      Left            =   10080
      Picture         =   "FormMBFichMov.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   16
      ToolTipText     =   "Processar Ficheiro"
      Top             =   7080
      Width           =   855
   End
   Begin VB.TextBox EcValorTotal 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   6600
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox EcNumRef 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4440
      Locked          =   -1  'True
      TabIndex        =   13
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox EcEntidade 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox EcNumAnterior 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4440
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   840
      Width           =   735
   End
   Begin VB.TextBox EcNumAtual 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   4440
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   480
      Width           =   735
   End
   Begin VB.TextBox EcDtFichAnterior 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   840
      Width           =   975
   End
   Begin VB.TextBox EcDtFichAtual 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   480
      Width           =   975
   End
   Begin MSFlexGridLib.MSFlexGrid FgRef 
      Height          =   5655
      Left            =   120
      TabIndex        =   1
      Top             =   1320
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   9975
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
      Appearance      =   0
   End
   Begin VB.CommandButton BtImportarFicheiro 
      Height          =   615
      Left            =   10080
      Picture         =   "FormMBFichMov.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Importar Ficheiro"
      Top             =   120
      Width           =   855
   End
   Begin MSComDlg.CommonDialog NomeFicheiro 
      Left            =   0
      Top             =   8280
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label LbErro 
      Alignment       =   1  'Right Justify
      Caption         =   "Existem ficheiros por tratar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   7680
      TabIndex        =   17
      Top             =   720
      Visible         =   0   'False
      Width           =   3255
   End
   Begin VB.Label Label1 
      Caption         =   "Valor Total"
      Height          =   255
      Index           =   6
      Left            =   5760
      TabIndex        =   14
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "N� Refer�ncias"
      Height          =   255
      Index           =   5
      Left            =   3240
      TabIndex        =   12
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Entidade "
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "N� Anterior"
      Height          =   255
      Index           =   3
      Left            =   3240
      TabIndex        =   5
      Top             =   840
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Data do Fich.Anterior"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   4
      Top             =   840
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "N� Atual"
      Height          =   255
      Index           =   1
      Left            =   3240
      TabIndex        =   3
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Data do Fich.Atual"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   1695
   End
End
Attribute VB_Name = "FormMBFichMov"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ficheiro As mbFicheiro
Dim estado As Integer

Private Sub BtImportarFicheiro_Click()

    On Error GoTo TrataErro
    Dim Width As Long

    
    Dim Height As Long
    Dim seqFicheiro As Long
    NomeFicheiro.Filter = "*.*"
    
    NomeFicheiro.FilterIndex = 0
    NomeFicheiro.CancelError = True
    NomeFicheiro.InitDir = gDirCliente & "\"
    NomeFicheiro.FileName = "*.*"
    NomeFicheiro.DialogTitle = "Nome do ficheiro"
    NomeFicheiro.ShowOpen
    If NomeFicheiro.FileName <> "" Then
        seqFicheiro = MB_TrataFicheiro(NomeFicheiro.FileName)
        If seqFicheiro > mediComboValorNull Then
            Call MB_PreencheFicheiro(seqFicheiro, ficheiro)
        End If
    End If
    If MB_VerificaFicheiroTratado(ficheiro.fic_seq, ficheiro.fic_seq_ult) = True Then
        PreencheCampos
    Else
        limpaFicheiro ficheiro
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "BtImportarFicheiro_Click: " & Err.Number & " - " & Err.Description, Me.Name, "BtImportarFicheiro_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtProcessaFicheiro_Click()
    If ficheiro.seq_ficheiro > 0 Then
        MB_ProcessaFicheiro ficheiro
    End If
    LimpaCampos
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Importa��o de Ficheiro de Movimentos"
    Me.Left = 5
    Me.Top = 5
    Me.Width = 11085
    Me.Height = 8130 ' Normal
    
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormScriptFactus = Nothing
End Sub



Sub DefTipoCampos()
    VerificaFicheiros
    With FgRef
        .rows = 2
        .FixedRows = 1
        .Cols = 7
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        
        .ColWidth(0) = 1300
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, 0) = "Utente"
        
        .ColWidth(1) = 3500
        .Col = 1
        .ColAlignment(1) = flexAlignLeftCenter
        .TextMatrix(0, 1) = "Nome"
        
        .ColWidth(2) = 1000
        .Col = 2
        .ColAlignment(2) = flexAlignLeftCenter
        .TextMatrix(0, 2) = "Refer�ncia"
        
        .ColWidth(3) = 1350
        .Col = 3
        .ColAlignment(3) = flexAlignLeftCenter
        .TextMatrix(0, 3) = "Data Emiss�o"
        
        .ColWidth(4) = 1300
        .Col = 4
        .ColAlignment(4) = flexAlignLeftCenter
        .TextMatrix(0, 4) = "Estado"
        
        .ColWidth(5) = 1350
        .Col = 5
        .ColAlignment(5) = flexAlignLeftCenter
        .TextMatrix(0, 5) = "Data Pagamento"
        
        .ColWidth(6) = 700
        .Col = 6
        .ColAlignment(6) = flexAlignLeftCenter
        .TextMatrix(0, 6) = "Valor Ficheiro"
        
        .WordWrap = False
        .row = 0
        .Col = 0
    End With

End Sub

Sub PreencheCampos()
    LimpaCampos
    Dim i As Integer
    LimpaFgRef
    EcDtFichAtual.Text = Mid(ficheiro.fic_seq, 7, 2) & "-" & Mid(ficheiro.fic_seq, 5, 2) & "-" & Mid(ficheiro.fic_seq, 1, 4)
    EcNumAtual.Text = Mid(ficheiro.fic_seq, 9, 1)
    If Mid(ficheiro.fic_seq_ult, 1, 1) <> "0" Then
        EcDtFichAnterior.Text = Mid(ficheiro.fic_seq_ult, 7, 2) & "-" & Mid(ficheiro.fic_seq_ult, 5, 2) & "-" & Mid(ficheiro.fic_seq_ult, 1, 4)
        EcNumAnterior.Text = Mid(ficheiro.fic_seq_ult, 9, 1)
        If MB_VerificaFicheiroAnteriorTratado(ficheiro.fic_seq_ult) = False Then
            EcDtFichAnterior.BackColor = vbRed
            EcNumAnterior.BackColor = vbRed
            BG_Mensagem mediMsgBox, "N�o existe registo do processamento do �ltimo ficheiro indicado!", vbExclamation, "C�digo da Simples"
        Else
            EcDtFichAnterior.BackColor = vbWhite
            EcNumAnterior.BackColor = vbWhite
        End If
    Else
        EcDtFichAnterior.Text = ""
        EcNumAnterior.Text = ""
    End If
    EcEntidade.Text = ficheiro.EPS_NUM
    If ficheiro.EPS_NUM <> gEntidadeMultibanco Then
        EcEntidade.BackColor = vbRed
        EcEntidade.ToolTipText = "Entidade n�o corresponde com o parametrizado"
    Else
        EcEntidade.BackColor = vbWhite
        EcEntidade.ToolTipText = "Entidade"
    End If
    EcNumRef.Text = CLng(ficheiro.FIC_REGQNT)
    EcValorTotal.Text = CLng(Mid(ficheiro.EPS_TOTMNT_FIC, 1, 15)) & gSimboloDecimal & Mid(ficheiro.EPS_TOTMNT_FIC, 16, 2)
    For i = 1 To ficheiro.totalRef
        FgRef.row = i
        FgRef.TextMatrix(i, 0) = ficheiro.referencias(i).t_utente & "/" & ficheiro.referencias(i).Utente
        FgRef.TextMatrix(i, 1) = ficheiro.referencias(i).nome_ute
        FgRef.TextMatrix(i, 2) = ficheiro.referencias(i).EPS_REF
        FgRef.TextMatrix(i, 3) = ficheiro.referencias(i).dt_ref
        If ficheiro.referencias(i).estado_ref = "0" Then
            FgRef.TextMatrix(i, 4) = "Ref. Emitida"
        ElseIf ficheiro.referencias(i).estado_ref = "1" Then
            FgRef.TextMatrix(i, 4) = "Ref. Paga"
        Else
            FgRef.TextMatrix(i, 4) = ""
        End If
        FgRef.TextMatrix(i, 5) = Mid(ficheiro.referencias(i).SIS_DTHN01, 7, 2) & "-" & Mid(ficheiro.referencias(i).SIS_DTHN01, 5, 2) & "-" & Mid(ficheiro.referencias(i).SIS_DTHN01, 1, 4) & " " & Mid(ficheiro.referencias(i).SIS_DTHN01, 9, 2) & ":" & Mid(ficheiro.referencias(i).SIS_DTHN01, 11, 2)
        FgRef.TextMatrix(i, 6) = ficheiro.referencias(i).valor_ficheiro
        If ficheiro.referencias(i).valor_ficheiro <> ficheiro.referencias(i).valor_ref Then
            FgRef.Col = 6
            FgRef.CellBackColor = vbRed
        End If
        FgRef.AddItem ""
    Next i
End Sub


Sub FuncaoLimpar()
    LimpaCampos
    
End Sub

Private Sub LimpaCampos()
    EcDtFichAtual.Text = ""
    EcDtFichAnterior.Text = ""
    EcNumAnterior.Text = ""
    EcNumAtual.Text = ""
    EcEntidade.Text = ""
    EcNumRef.Text = ""
    EcValorTotal.Text = ""
    EcEntidade.BackColor = vbWhite
    EcDtFichAnterior.BackColor = vbWhite
    EcNumAnterior.BackColor = vbWhite
    LimpaFgRef
End Sub
Private Sub LimpaFgRef()
    Dim i As Integer
    FgRef.rows = 2
    For i = 0 To FgRef.Cols - 1
        FgRef.TextMatrix(1, i) = ""
        FgRef.Col = i
        FgRef.CellBackColor = vbWhite
    Next i
End Sub
Sub FuncaoEstadoAnterior()

End Sub

Sub FuncaoProcurar()

End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()


End Sub

Sub FuncaoModificar()


End Sub



Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub

Private Sub limpaFicheiro(ficheiro As mbFicheiro)
    ficheiro.seq_ficheiro = ""
    ficheiro.totalRef = 0
    ReDim ficheiro.referencias(0)
End Sub

Private Sub VerificaFicheiros()
    Dim sSql As String
    Dim rsFich As New ADODB.recordset
    
    sSql = "select * from sl_mb_ficheiros_mov where flg_tratado = 1 and fic_seq_ult <> '000000000' and fic_seq_ult not in (select fic_seq from sl_mb_ficheiros_mov where flg_tratado = 1 )"
    rsFich.CursorType = adOpenKeyset
    rsFich.LockType = adLockOptimistic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsFich.Open sSql, gConexao
    If rsFich.RecordCount >= 1 Then
        LbErro.Visible = True
    Else
        LbErro.Visible = False
    End If
    rsFich.Close
    Set rsFich = Nothing
End Sub



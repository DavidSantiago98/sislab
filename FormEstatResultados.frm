VERSION 5.00
Begin VB.Form FormEstatResultados 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   5370
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7605
   Icon            =   "FormEstatResultados.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   7605
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   11
      Top             =   0
      Width           =   7335
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   3120
         TabIndex        =   13
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   1560
         TabIndex        =   12
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Left            =   720
         TabIndex        =   15
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Left            =   2850
         TabIndex        =   14
         Top             =   360
         Width           =   255
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Condi��es de Pesquisa"
      Height          =   4335
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Width           =   7335
      Begin VB.Frame Frame2 
         Height          =   1575
         Left            =   360
         TabIndex        =   24
         Top             =   2640
         Width           =   6375
         Begin VB.OptionButton Opt 
            Caption         =   "Frases"
            Height          =   255
            Index           =   1
            Left            =   3120
            TabIndex        =   47
            Top             =   120
            Width           =   1815
         End
         Begin VB.OptionButton Opt 
            Caption         =   "AlfaNum�ricos"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   46
            Top             =   120
            Width           =   1815
         End
         Begin VB.TextBox EcCodFrase1 
            Height          =   315
            Left            =   5400
            TabIndex        =   42
            Top             =   480
            Width           =   735
         End
         Begin VB.TextBox EcCodFrase2 
            Height          =   315
            Left            =   5400
            TabIndex        =   41
            Top             =   840
            Width           =   735
         End
         Begin VB.TextBox EcCodFrase3 
            Height          =   315
            Left            =   5400
            TabIndex        =   40
            Top             =   1200
            Width           =   735
         End
         Begin VB.TextBox EcCond3Max 
            Height          =   315
            Left            =   3720
            TabIndex        =   33
            Top             =   1200
            Width           =   735
         End
         Begin VB.TextBox EcCond2Max 
            Height          =   315
            Left            =   3720
            TabIndex        =   32
            Top             =   840
            Width           =   735
         End
         Begin VB.TextBox EcCond1Max 
            Height          =   315
            Left            =   3720
            TabIndex        =   31
            Top             =   480
            Width           =   735
         End
         Begin VB.TextBox EcCond3Min 
            Height          =   315
            Left            =   2040
            TabIndex        =   28
            Top             =   1200
            Width           =   735
         End
         Begin VB.TextBox EcCond2Min 
            Height          =   315
            Left            =   2040
            TabIndex        =   27
            Top             =   840
            Width           =   735
         End
         Begin VB.TextBox EcCond1Min 
            Height          =   315
            Left            =   2040
            TabIndex        =   26
            Top             =   480
            Width           =   735
         End
         Begin VB.Label Label19 
            Caption         =   "Frase"
            Height          =   255
            Left            =   4680
            TabIndex        =   45
            Top             =   480
            Width           =   1095
         End
         Begin VB.Label Label18 
            Caption         =   "Frase"
            Height          =   255
            Left            =   4680
            TabIndex        =   44
            Top             =   1200
            Width           =   1095
         End
         Begin VB.Label Label17 
            Caption         =   "Frase"
            Height          =   255
            Left            =   4680
            TabIndex        =   43
            Top             =   840
            Width           =   1095
         End
         Begin VB.Label Label16 
            Caption         =   "M�ximo"
            Height          =   255
            Left            =   3000
            TabIndex        =   39
            Top             =   840
            Width           =   1095
         End
         Begin VB.Label Label15 
            Caption         =   "M�ximo"
            Height          =   255
            Left            =   3000
            TabIndex        =   38
            Top             =   1200
            Width           =   1095
         End
         Begin VB.Label Label14 
            Caption         =   "M�ximo"
            Height          =   255
            Left            =   3000
            TabIndex        =   37
            Top             =   480
            Width           =   1095
         End
         Begin VB.Label Label13 
            Caption         =   "M�nimo"
            Height          =   255
            Left            =   1320
            TabIndex        =   36
            Top             =   1200
            Width           =   1095
         End
         Begin VB.Label Label12 
            Caption         =   "M�nimo"
            Height          =   255
            Left            =   1320
            TabIndex        =   35
            Top             =   840
            Width           =   1095
         End
         Begin VB.Label Label11 
            Caption         =   "M�nimo"
            Height          =   255
            Left            =   1320
            TabIndex        =   34
            Top             =   480
            Width           =   1095
         End
         Begin VB.Label Label8 
            Caption         =   "Condi��o 3:"
            Height          =   255
            Left            =   120
            TabIndex        =   30
            Top             =   1200
            Width           =   1095
         End
         Begin VB.Label Label7 
            Caption         =   "Condi��o 2:"
            Height          =   255
            Left            =   120
            TabIndex        =   29
            Top             =   840
            Width           =   1095
         End
         Begin VB.Label Label5 
            Caption         =   "Condi��o 1:"
            Height          =   255
            Left            =   120
            TabIndex        =   25
            Top             =   480
            Width           =   1095
         End
      End
      Begin VB.TextBox EcCodAnaS 
         Height          =   315
         Left            =   1560
         TabIndex        =   22
         Top             =   360
         Width           =   735
      End
      Begin VB.TextBox EcDescrAnaS 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   360
         Width           =   4095
      End
      Begin VB.CommandButton BtPesqRapS 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResultados.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   20
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   360
         Width           =   375
      End
      Begin VB.TextBox EcCodEFR 
         Height          =   315
         Left            =   1560
         TabIndex        =   18
         Top             =   2280
         Width           =   735
      End
      Begin VB.TextBox EcDescrEFR 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   2280
         Width           =   4095
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResultados.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   2280
         Width           =   375
      End
      Begin VB.ComboBox EcDescrSexo 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   840
         Width           =   1455
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   5280
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1320
         Width           =   1455
      End
      Begin VB.ComboBox CbSituacao 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1320
         Width           =   1455
      End
      Begin VB.TextBox EcCodProveniencia 
         Height          =   315
         Left            =   1560
         TabIndex        =   3
         Top             =   1800
         Width           =   735
      End
      Begin VB.TextBox EcDescrProveniencia 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   1800
         Width           =   4095
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   6360
         Picture         =   "FormEstatResultados.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1800
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "An�lise Simples"
         Height          =   255
         Left            =   360
         TabIndex        =   23
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label10 
         Caption         =   "E.&F.R."
         Height          =   255
         Left            =   360
         TabIndex        =   19
         Top             =   2280
         Width           =   495
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Se&xo"
         Height          =   195
         Left            =   360
         TabIndex        =   10
         Top             =   840
         Width           =   360
      End
      Begin VB.Label Label6 
         Caption         =   "Prio&ridade"
         Height          =   255
         Left            =   4440
         TabIndex        =   9
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   360
         TabIndex        =   8
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Left            =   360
         TabIndex        =   7
         Top             =   1800
         Width           =   975
      End
   End
End
Attribute VB_Name = "FormEstatResultados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset
Dim TotalResCond1 As Double
Dim TotalResCond2 As Double
Dim TotalResCond3 As Double
Dim Cond1 As String
Dim Cond2 As String
Dim Cond3 As String
Dim MinResCond1 As String
Dim MinResCond2 As String
Dim MinResCond3 As String
Dim MaxResCond1 As String
Dim MaxResCond2 As String
Dim MaxResCond3 As String

Sub Preenche_Estatistica()
    
    Dim sql As String
    Dim continua As Boolean
            
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Estat�stica por Servi�o Requisitante") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    If EcCodAnaS.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a An�lise para a pesquisa de resultados.", vbOKOnly + vbExclamation, App.ProductName)
        EcCodAnaS.SetFocus
        Exit Sub
    End If
    If EcCond1Min.Text = "" And EcCond1Max.Text = "" And EcCodFrase1 = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique uma condi��o para a pesquisa de resultados.", vbOKOnly + vbExclamation, App.ProductName)
        EcCond1Min.SetFocus
        Exit Sub
    End If

    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("EstatisticaResultados", "Estat�stica de Resultados", crptToPrinter)
    Else
        continua = BL_IniciaReport("EstatisticaResultados", "Estat�stica de Resultados", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    Call Cria_TmpRec_Estatistica
    
    If PreencheTabelaTemporaria = -1 Then
        BG_Mensagem mediMsgBox, "N�o foi encontrado nenhum resultado para estas condi��es de pesquisa!", vbExclamation, App.ProductName
        Exit Sub
    End If
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    

    Report.SQLQuery = "SELECT " & _
                      "     SL_CR_ESTATRES.N_REQ, " & _
                      "     SL_CR_ESTATRES.NOME_UTE, " & _
                      "     SL_CR_ESTATRES.DT_CHEGA, " & _
                      "     SL_CR_ESTATRES.CONDICAO1, " & _
                      "     SL_CR_ESTATRES.CONDICAO2, " & _
                      "     SL_CR_ESTATRES.CONDICAO3, " & _
                      "FROM " & _
                      "     SL_CR_ESTATRES" & gNumeroSessao & " SL_CR_ESTATRES " & _
                      "ORDER BY NOME_UTE "
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.Text)
    If EcCodEFR = "" Then
        Report.formulas(2) = "EFR=" & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(2) = "EFR=" & BL_TrataStringParaBD("" & EcDescrEFR.Text)
    End If
    If EcCodProveniencia = "" Then
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("" & EcDescrProveniencia.Text)
    End If
    If CbUrgencia.ListIndex = -1 Then
        Report.formulas(4) = "Urgencia=" & BL_TrataStringParaBD("-")
    Else
        Report.formulas(4) = "Urgencia=" & BL_TrataStringParaBD("" & CbUrgencia.Text)
    End If
    If CbSituacao.ListIndex = -1 Then
        Report.formulas(5) = "Situacao=" & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(5) = "Situacao=" & BL_TrataStringParaBD("" & CbSituacao.Text)
    End If
    If EcDescrSexo.ListIndex = -1 Then
        Report.formulas(6) = "Sexo=" & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(6) = "Sexo=" & BL_TrataStringParaBD("" & EcDescrSexo.Text)
    End If
    If EcDescrAnaS.Text = "" Then
        Report.formulas(7) = "Analise= " & BL_TrataStringParaBD("Todas")
    Else
        Report.formulas(7) = "Analise= " & BL_TrataStringParaBD("" & EcDescrAnaS.Text)
    End If
    Report.formulas(8) = "TotalResCond1= " & BL_TrataStringParaBD("" & TotalResCond1)
    Report.formulas(9) = "TotalResCond2= " & BL_TrataStringParaBD("" & TotalResCond2)
    Report.formulas(10) = "TotalResCond3= " & BL_TrataStringParaBD("" & TotalResCond3)
    Report.formulas(11) = "Cond1= " & BL_TrataStringParaBD("" & Cond1)
    Report.formulas(12) = "Cond2= " & BL_TrataStringParaBD("" & Cond2)
    Report.formulas(13) = "Cond3= " & BL_TrataStringParaBD("" & Cond3)
    Report.formulas(14) = "MinResCond1= " & BL_TrataStringParaBD("" & Replace(MinResCond1, ",", "."))
    Report.formulas(15) = "MinResCond2= " & BL_TrataStringParaBD("" & Replace(MinResCond2, ",", "."))
    Report.formulas(16) = "MinResCond3= " & BL_TrataStringParaBD("" & Replace(MinResCond3, ",", "."))
    Report.formulas(17) = "MaxResCond1= " & BL_TrataStringParaBD("" & Replace(MaxResCond1, ",", "."))
    Report.formulas(18) = "MaxResCond2= " & BL_TrataStringParaBD("" & Replace(MaxResCond2, ",", "."))
    Report.formulas(19) = "MaxResCond3= " & BL_TrataStringParaBD("" & Replace(MaxResCond3, ",", "."))
    
    'Me.SetFocus
    
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
    Report.PageZoom (100)
 
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    
    Call BL_RemoveTabela("SL_CR_ESTATRES" & gNumeroSessao)
    
End Sub

Function PreencheTabelaTemporaria() As Integer
    
    On Error Resume Next
    
    Dim sql As String
    Dim rsReq As ADODB.recordset
    Dim result As String
    Dim Cond1Min As Double
    Dim Cond1Max As Double
    Dim Cond2Min As Double
    Dim Cond2Max As Double
    Dim Cond3Min As Double
    Dim Cond3Max As Double
    Dim ResCond1 As String
    Dim ResCond2 As String
    Dim ResCond3 As String
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
    ' Selecciona todas as requisi��es para os campos preenchidos
    If Opt(0).value = True Then
        sql = "     SELECT DISTINCT " & _
              "     SL_REQUIS.N_REQ, NOME_UTE,DT_NASC_UTE, SL_REQUIS.DT_CHEGA, SL_RES_ALFAN.RESULT " & _
              "FROM " & _
              "     SL_REQUIS, " & tabela_aux & ", SL_REALIZA, SL_RES_ALFAN " & _
              "WHERE " & _
              "     SL_REQUIS.SEQ_UTENTE = " & tabela_aux & ".SEQ_UTENTE AND " & _
              "     SL_REQUIS.N_REQ = SL_REALIZA.N_REQ AND " & _
              "     SL_REALIZA.SEQ_REALIZA = SL_RES_ALFAN.SEQ_REALIZA "
    
    ElseIf Opt(1).value = True Then
        sql = "     SELECT DISTINCT " & _
              "     SL_REQUIS.N_REQ, NOME_UTE,DT_NASC_UTE, SL_REQUIS.DT_CHEGA, SL_RES_FRASE.cod_frase " & _
              "FROM " & _
              "     SL_REQUIS, " & tabela_aux & ", SL_REALIZA, sl_res_frase " & _
              "WHERE " & _
              "     SL_REQUIS.SEQ_UTENTE = " & tabela_aux & ".SEQ_UTENTE AND " & _
              "     SL_REQUIS.N_REQ = SL_REALIZA.N_REQ AND " & _
              "     SL_REALIZA.SEQ_REALIZA = sl_res_frase.SEQ_REALIZA "
    End If
    'verifica os campos preenchidos
    'codigo da analise preenchida
    If EcCodAnaS.Text <> "" Then
        sql = sql & " AND sl_realiza.cod_ana_s = " & BL_TrataStringParaBD(EcCodAnaS.Text)
    End If
    
    'se sexo preenchido
    If (EcDescrSexo.ListIndex <> -1) Then
        sql = sql & " AND " & tabela_aux & ".sexo_ute = " & EcDescrSexo.ListIndex
    End If
    
    'Data preenchida
    sql = sql & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    
    'Situa��o preenchida?
    If (CbSituacao.ListIndex <> -1) Then
        sql = sql & " AND sl_requis.T_sit= " & CbSituacao.ListIndex
    End If
    
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> -1) Then
        sql = sql & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(left(CbUrgencia.Text, 1))
    End If
    
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    If EcCodProveniencia.Text <> "" Then
        sql = sql & " AND sl_requis.Cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.Text)
    End If
    
    'C�digo da EFR preenchido?
    If EcCodEFR.Text <> "" Then
        sql = sql & " AND sl_requis.Cod_efr=" & BL_TrataStringParaBD(EcCodEFR.Text)
    End If
    

    Cond1Min = EcCond1Min.Text
    Cond1Max = EcCond1Max.Text
    Cond2Min = EcCond2Min.Text
    Cond2Max = EcCond2Max.Text
    Cond3Min = EcCond3Min.Text
    Cond3Max = EcCond3Max.Text
    
    TotalResCond1 = 0
    TotalResCond2 = 0
    TotalResCond3 = 0
    
    MinResCond1 = 999999
    MinResCond2 = 999999
    MinResCond3 = 999999
    MaxResCond1 = -999999
    MaxResCond2 = -999999
    MaxResCond3 = -999999
    
    Cond1 = ""
    Cond2 = ""
    Cond3 = ""
    
    Set rsReq = New ADODB.recordset
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    rsReq.Open sql, gConexao
    If rsReq.RecordCount = 0 Then
        PreencheTabelaTemporaria = -1
        Exit Function
    End If
    While Not rsReq.EOF
        If Opt(0).value = True Then
            If rsReq!result <> "{Ver Observa��es}" And rsReq!result <> cSemResultado Then
                result = rsReq!result
                ResCond1 = ""
                ResCond2 = ""
                ResCond3 = ""
                
                'resultado inserido na condi��o 2
                If EcCond1Min.Text <> "" And EcCond1Max.Text <> "" Then
                    Cond1 = EcCond1Min.Text & " =< X >= " & EcCond1Max.Text
                    If BL_String2Double(result) >= BL_String2Double(EcCond1Min.Text) And BL_String2Double(result) <= BL_String2Double(EcCond1Max.Text) Then
                        ResCond1 = result
                    End If
                ElseIf EcCond1Min.Text <> "" And IsNumeric(result) Then
                    Cond1 = "X >= " & EcCond1Min.Text
                    If BL_String2Double(result) >= BL_String2Double(EcCond1Min.Text) Then
                        ResCond1 = result
                    End If
                ElseIf EcCond1Min.Text <> "" And Not IsNumeric(result) Then
                    Cond1 = "X = " & EcCond1Min.Text
                    If UCase(result) = UCase(EcCond1Min.Text) Then
                        ResCond1 = result
                    End If
                ElseIf EcCond1Max.Text <> "" Then
                    Cond1 = "X <= " & EcCond1Max.Text
                    If BL_String2Double(result) <= BL_String2Double(EcCond1Max.Text) Then
                        ResCond1 = result
                    End If
                End If
                'resultado inserido na condi��o 2
                If EcCond2Min.Text <> "" And EcCond2Max.Text <> "" Then
                    Cond2 = EcCond2Min.Text & " =< X >= " & EcCond2Max.Text
                    If BL_String2Double(result) >= BL_String2Double(EcCond2Min.Text) And BL_String2Double(result) <= BL_String2Double(EcCond2Max.Text) Then
                        ResCond2 = result
                    End If
                ElseIf EcCond2Min.Text <> "" Then
                    Cond2 = "X >= " & EcCond2Min.Text
                    If BL_String2Double(result) >= BL_String2Double(EcCond2Min.Text) Then
                        ResCond2 = result
                    End If
                ElseIf EcCond2Max.Text <> "" Then
                    Cond2 = "X <= " & EcCond2Max.Text
                    If BL_String2Double(result) <= BL_String2Double(EcCond2Max.Text) Then
                        ResCond2 = result
                    End If
                End If
                'resultado inserido na condi��o 3
                If EcCond3Min.Text <> "" And EcCond3Max.Text <> "" Then
                    Cond3 = EcCond3Min.Text & " =< X >= " & EcCond3Max.Text
                    If BL_String2Double(result) >= BL_String2Double(EcCond3Min.Text) And BL_String2Double(result) <= BL_String2Double(EcCond3Max.Text) Then
                        ResCond3 = result
                    End If
                ElseIf EcCond3Min.Text <> "" Then
                    Cond3 = "X >= " & EcCond3Min.Text
                    If BL_String2Double(result) >= BL_String2Double(EcCond3Min.Text) Then
                        ResCond3 = result
                    End If
                ElseIf EcCond3Max.Text <> "" Then
                    Cond3 = "X <= " & EcCond3Max.Text
                    If BL_String2Double(result) <= BL_String2Double(EcCond3Max.Text) Then
                        ResCond3 = result
                    End If
                End If
                
                If ResCond1 <> "" Or ResCond2 <> "" Or ResCond3 <> "" Then
                    sql = "insert into sl_cr_estatres" & gNumeroSessao & "(n_req,nome_ute,dt_chega,condicao1,condicao2,condicao3, idade) values( " & _
                          rsReq!n_req & "," & BL_TrataStringParaBD(rsReq!nome_ute) & ", " & _
                          BL_TrataDataParaBD(rsReq!dt_chega) & ",'" & ResCond1 & "', '" & _
                          ResCond2 & "','" & ResCond3 & "'," & _
                          DateDiff("yyyy", rsReq!dt_nasc_ute, BL_HandleNull(rsReq!dt_chega, Bg_DaData_ADO)) & " )"
                    BG_ExecutaQuery_ADO sql
                    BG_Trata_BDErro
                End If
                
                If ResCond1 <> "" Then
                    TotalResCond1 = TotalResCond1 + BL_String2Double(ResCond1)
                    If BL_String2Double(ResCond1) > BL_String2Double(MaxResCond1) Then MaxResCond1 = BL_String2Double(ResCond1)
                    If BL_String2Double(ResCond1) < BL_String2Double(MinResCond1) Then MinResCond1 = BL_String2Double(ResCond1)
                End If
                If ResCond2 <> "" Then
                    TotalResCond2 = TotalResCond2 + BL_String2Double(ResCond2)
                    If BL_String2Double(ResCond2) > BL_String2Double(MaxResCond2) Then MaxResCond2 = BL_String2Double(ResCond2)
                    If BL_String2Double(ResCond2) < BL_String2Double(MinResCond2) Then MinResCond2 = BL_String2Double(ResCond2)
                End If
                If ResCond3 <> "" Then
                    TotalResCond3 = TotalResCond3 + BL_String2Double(ResCond3)
                    If BL_String2Double(ResCond3) > BL_String2Double(MaxResCond3) Then MaxResCond3 = BL_String2Double(ResCond3)
                    If BL_String2Double(ResCond3) < BL_String2Double(MinResCond3) Then MinResCond3 = BL_String2Double(ResCond3)
                End If
            End If
        ElseIf Opt(1).value = True Then
            result = UCase(BL_HandleNull(rsReq!cod_frase, ""))
            ResCond1 = ""
            ResCond2 = ""
            ResCond3 = ""
            If UCase(EcCodFrase1) = result Then
                ResCond1 = result
            ElseIf UCase(EcCodFrase2) = result Then
                ResCond2 = result
            ElseIf UCase(EcCodFrase3) = result Then
                ResCond3 = result
            End If
            
            If ResCond1 <> "" Or ResCond2 <> "" Or ResCond3 <> "" Then
                sql = "insert into sl_cr_estatres" & gNumeroSessao & "(n_req,nome_ute,dt_chega,condicao1,condicao2,condicao3, idade) values( " & _
                      rsReq!n_req & "," & BL_TrataStringParaBD(rsReq!nome_ute) & ", " & _
                      BL_TrataDataParaBD(rsReq!dt_chega) & ",'" & ResCond1 & "', '" & _
                      ResCond2 & "','" & ResCond3 & "'," & _
                      DateDiff("yyyy", rsReq!dt_nasc_ute, BL_HandleNull(rsReq!dt_chega, Bg_DaData_ADO)) & " )"
                BG_ExecutaQuery_ADO sql
                BG_Trata_BDErro
            End If
                
                If ResCond1 <> "" Then
                    TotalResCond1 = TotalResCond1 + BL_String2Double(ResCond1)
                End If
                If ResCond2 <> "" Then
                    TotalResCond2 = TotalResCond2 + BL_String2Double(ResCond2)
                End If
                If ResCond3 <> "" Then
                    TotalResCond3 = TotalResCond3 + BL_String2Double(ResCond3)
                End If
        End If
        rsReq.MoveNext
    Wend

    PreencheTabelaTemporaria = 1
    
End Function

Sub Cria_TmpRec_Estatistica()

    Dim TmpRec(7) As DefTable
    
    TmpRec(1).NomeCampo = "N_REQ"
    TmpRec(1).tipo = "INTEGER"
    TmpRec(1).tamanho = 13
    
    TmpRec(2).NomeCampo = "NOME_UTE"
    TmpRec(2).tipo = "STRING"
    TmpRec(2).tamanho = 80
    
    TmpRec(3).NomeCampo = "DT_CHEGA"
    TmpRec(3).tipo = "DATE"
    
    TmpRec(4).NomeCampo = "CONDICAO1"
    TmpRec(4).tipo = "STRING"
    TmpRec(4).tamanho = 10
    
    TmpRec(5).NomeCampo = "CONDICAO2"
    TmpRec(5).tipo = "STRING"
    TmpRec(5).tamanho = 10
    
    TmpRec(6).NomeCampo = "CONDICAO3"
    TmpRec(6).tipo = "STRING"
    TmpRec(6).tamanho = 10
    
    TmpRec(7).NomeCampo = "IDADE"
    TmpRec(7).tipo = "INTEGER"
    TmpRec(7).tamanho = 3
    
    Call BL_CriaTabela("SL_CR_ESTATRES" & gNumeroSessao, TmpRec)
    
End Sub

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function

Private Sub BtPesqRapS_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_s"
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_s"
    CWhere = " "
    CampoPesquisa = "descr_ana_s"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Simples")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaS.Text = resultados(2)
            EcCodAnaS.Text = resultados(1)
            EcCodAnaS.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises simples com resultado num�rico", vbExclamation, "ATEN��O"
        EcCodAnaS.SetFocus
    End If
End Sub

Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
End Sub

Private Sub BtPesquisaProveniencia_Click()

    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Proveni�ncias")
    
    mensagem = "N�o foi encontrada nenhuma Proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.Text = resultados(1)
            EcDescrProveniencia.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub

Private Sub CbUrgencia_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1
    
End Sub

Private Sub EcCodProveniencia_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodEFR_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodEFR_Validate(Cancel As Boolean)
    
    Cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, "")
End Sub

Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProveniencia.Text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_proven FROM sl_proven WHERE cod_proven= " & BL_TrataStringParaBD(EcCodProveniencia.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrProveniencia.Text = "" & RsDescrProv!descr_proven
        Else
            Cancel = True
            EcDescrProveniencia.Text = ""
            BG_Mensagem mediMsgBox, "A Proveni�ncia indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrProveniencia.Text = ""
    End If
    
End Sub

Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl

End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
      
End Sub

Sub Inicializacoes()

    Me.caption = " Estat�stica de Resultados"
    Me.left = 540
    Me.top = 450
    Me.Width = 7695
    Me.Height = 5700 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcCodProveniencia
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de Resultados")
    
    Set FormEstatResultados = Nothing
    
End Sub

Sub LimpaCampos()
    
    Me.SetFocus
    
    CbUrgencia.ListIndex = mediComboValorNull
    CbSituacao.ListIndex = mediComboValorNull
    EcDescrSexo.ListIndex = mediComboValorNull
    EcCodProveniencia.Text = ""
    EcDescrProveniencia.Text = ""
    EcCodEFR.Text = ""
    EcDescrEFR.Text = ""
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    EcCodAnaS.Text = ""
    EcDescrAnaS.Text = ""
    EcCond1Max.Text = ""
    EcCond1Min.Text = ""
    EcCond2Max.Text = ""
    EcCond2Min.Text = ""
    EcCond3Max.Text = ""
    EcCond3Min.Text = ""
    EcCodFrase1 = ""
    EcCodFrase2 = ""
    EcCodFrase3 = ""
    Opt(0).value = True
End Sub

Sub DefTipoCampos()

    'Tipo VarChar
    EcCodProveniencia.Tag = "200"
    EcCodEFR.Tag = "200"

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    EcCodProveniencia.MaxLength = 5
    EcCodEFR.MaxLength = 9
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
'    EcNumFolhaTrab.MaxLength = 10
    
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    
    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    
    'Preenche Combo Urgencia
    CbUrgencia.AddItem "Normal"
    CbUrgencia.AddItem "Urgente"
    Opt(0).value = True

End Sub

Sub FuncaoProcurar()
       
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica
    
End Sub

Sub ImprimirVerAntes()
    
    Call Preenche_Estatistica

End Sub

Private Sub EcCodAnaS_LostFocus()

    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    If EcCodAnaS.Text <> "" Then
        sql = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s=" & BL_TrataStringParaBD(UCase(EcCodAnaS.Text))
        Set rsAna = New ADODB.recordset
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        rsAna.Open sql, gConexao
        If rsAna.RecordCount > 0 Then
            EcDescrAnaS.Text = BL_HandleNull(rsAna!descr_ana_s, "")
            EcCodAnaS.Text = UCase(EcCodAnaS)
        Else
            BG_Mensagem mediMsgBox, "A an�lise n�o existe", vbExclamation, "SISLAB"
            EcCodAnaS.Text = ""
            EcCodAnaS.SetFocus
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
    
End Sub

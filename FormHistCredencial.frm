VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormHistCredencial 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormHistCredencial"
   ClientHeight    =   7440
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   16845
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7440
   ScaleWidth      =   16845
   ShowInTaskbar   =   0   'False
   Begin MSFlexGridLib.MSFlexGrid FgInfoCredencial 
      Height          =   975
      Left            =   120
      TabIndex        =   0
      Top             =   1400
      Width           =   16335
      _ExtentX        =   28813
      _ExtentY        =   1720
      _Version        =   393216
      BackColorBkg    =   -2147483633
      Redraw          =   -1  'True
      BorderStyle     =   0
   End
   Begin MSFlexGridLib.MSFlexGrid FgInfoMcdt 
      Height          =   3955
      Left            =   120
      TabIndex        =   1
      Top             =   2880
      Width           =   16335
      _ExtentX        =   28813
      _ExtentY        =   6985
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
   End
   Begin MSFlexGridLib.MSFlexGrid FgCredencial 
      Height          =   975
      Left            =   120
      TabIndex        =   2
      Top             =   360
      Width           =   10080
      _ExtentX        =   17780
      _ExtentY        =   1720
      _Version        =   393216
      BackColorBkg    =   -2147483633
      Redraw          =   -1  'True
      BorderStyle     =   0
   End
   Begin VB.Label Label2 
      Caption         =   "Lista MCDT"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   2640
      Width           =   2775
   End
   Begin VB.Label Label1 
      Caption         =   "Detalhe da Credencial"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   100
      Width           =   2775
   End
End
Attribute VB_Name = "FormHistCredencial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Dim CampoDeFocus As Object

Private Type CredencialInfoMCDT
    codMcdt As String
    descrMcdt As String
    codConvencao As String
    descrConvencao As String
    mcdtID As String
    descrTotalMcdt As String
    estadoMcdt As String
    descrEstado As String
    lateralidade As String
    produto As String
    numAmostras As String
    infoClinComplementar As String
    infoIsencao As String
End Type

Private Type CredencialInfo
    num_credencial As String
    dt_req As String
    dt_pesq_cred As String
    cod_agendamento As String
    cod_prestacao As String
    cod_entidade As String
    descr_entidade As String
    cod_presc As String
    descr_presc As String
    num_professional As String
    info_clinica As String
    consentimento As String
    urgente_exam As String
    domicilio_exam As String
    just_domicilio As String
    just_urgente As String
    DetReq() As CredencialInfoMCDT
    reqID As Long
End Type

Dim EstrutInfoCredencial As CredencialInfo
Dim TotalCredencialInfoMCDT As Integer
    
Const lColNReq = 0
Const lcolDtReq = 1
Const lColDtPesqCred = 2
Const lColCodAgen = 3
Const lColCodPrest = 4

Const lColCodEnt = 0
Const lColCodPresc = 1
Const lColNumProf = 2
Const lColInfClin = 3
Const lcolConsentimento = 4
Const lColUrg = 5
Const lColDom = 6
Const lColJustUrg = 7
Const lColJustDom = 8

Const lcolDescrMcdt = 0
Const lColDescrConven = 1
Const lColDescrTotalMcdt = 2
Const lColEstadoMcdt = 3
Const lColLateralidade = 4
'Const lColProduto = 5
'Const lColNAmostras = 6
Const lColinfoClinComp = 5
Const lColinfoIsencao = 6
Const emDash As String = "�"

Const lVermelho = 12632319

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub
Private Sub Form_UnLoad(cancel As Integer)
    
    EventoUnload

End Sub


Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos

    BG_ParametrizaPermissoes_ADO Me.Name
    
    BL_ToolbarEstadoN 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    Call WheelHook(Me.hwnd)

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    'BL_Toolbar_BotaoEstado "Procurar", "Inactivo"
    'BL_Toolbar_BotaoEstado "Limpar", "Inactivo"
    'BL_Toolbar_BotaoEstado "DataActual", "Inactivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = FormGestaoRequisicaoPrivado
    FormGestaoRequisicaoPrivado.Enabled = True
        
    Set FormHistCredencial = Nothing
  
End Sub

Sub DefTipoCampos()

  With FgCredencial
        .rows = 2
        .FixedRows = 1
        .Cols = 5
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .CellAlignment = flexAlignLeftCenter
        .RowHeightMin = 450
        '.row = lColNReq
        
        .ColAlignment(lColNReq) = flexAlignLeftCenter
        .ColWidth(lColNReq) = 2000
        .Col = lColNReq
        .TextMatrix(0, lColNReq) = "Credencial"
        
        .ColAlignment(lcolDtReq) = flexAlignLeftCenter
        .ColWidth(lcolDtReq) = 1700
        .Col = lcolDtReq
        .TextMatrix(0, lcolDtReq) = "Data Requisi��o"
        
        .ColAlignment(lColDtPesqCred) = flexAlignLeftCenter
        .ColWidth(lColDtPesqCred) = 1700
        .Col = lColDtPesqCred
        .TextMatrix(0, lColDtPesqCred) = "Data Pesquisa Credencial"
        
        .ColAlignment(lColCodAgen) = flexAlignLeftCenter
        .ColWidth(lColCodAgen) = 1700
        .Col = lColCodAgen
        .TextMatrix(0, lColCodAgen) = "C�digo Agendamento"
        
        .ColAlignment(lColCodPrest) = flexAlignLeftCenter
        .ColWidth(lColCodPrest) = 1700
        .Col = lColCodPrest
        .TextMatrix(0, lColCodPrest) = "C�digo Presta��o"
        
        .WordWrap = True
        .row = 1
        .Col = 0
    End With

With FgInfoCredencial
        .rows = 2
        .FixedRows = 1
        .Cols = 9
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .CellAlignment = flexAlignLeftCenter
        .RowHeightMin = 450
        '.row = lColNReq
        
        .ColAlignment(lColCodEnt) = flexAlignLeftCenter
        .ColWidth(lColCodEnt) = 2515
        .Col = lColCodEnt
        .TextMatrix(0, lColCodEnt) = "Entidade"
        
        .ColAlignment(lColCodPresc) = flexAlignLeftCenter
        .ColWidth(lColCodPresc) = 2515
        .Col = lColCodPresc
        .TextMatrix(0, lColCodPresc) = "Local de Prescri��o"
        
        .ColAlignment(lColNumProf) = flexAlignLeftCenter
        .ColWidth(lColNumProf) = 900
        .Col = lColNumProf
        .TextMatrix(0, lColNumProf) = "Profissional"
        
        .ColAlignment(lColInfClin) = flexAlignLeftCenter
        .ColWidth(lColInfClin) = 2500
        .Col = lColInfClin
        .TextMatrix(0, lColInfClin) = "Info. Cl�nica"
        
        .ColAlignment(lcolConsentimento) = flexAlignLeftCenter
        .ColWidth(lcolConsentimento) = 1150
        .Col = lcolConsentimento
        .TextMatrix(0, lcolConsentimento) = "Consentimento"
        
        .ColAlignment(lColUrg) = flexAlignLeftCenter
        .ColWidth(lColUrg) = 700
        .Col = lColUrg
        .TextMatrix(0, lColUrg) = "Urgente"
        
        .ColAlignment(lColDom) = flexAlignLeftCenter
        .ColWidth(lColDom) = 750
        .Col = lColDom
        .TextMatrix(0, lColDom) = "Domicilio"
        
        .ColAlignment(lColJustUrg) = flexAlignLeftCenter
        .ColWidth(lColJustUrg) = 2500
        .Col = lColJustUrg
        .TextMatrix(0, lColJustUrg) = "Justifica��o Urgente"
        
        .ColAlignment(lColJustDom) = flexAlignLeftCenter
        .ColWidth(lColJustDom) = 2500
        .Col = lColJustDom
        .TextMatrix(0, lColJustDom) = "Justifica��o Domic�lio"
        
        .WordWrap = True
        .row = 1
        .Col = 0
    End With
  
  With FgInfoMcdt
        .rows = 2
        .FixedRows = 1
        .Cols = 7
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .CellAlignment = flexAlignLeftCenter
        .RowHeightMin = 650
        '.row = lColNReq
        
        .ColAlignment(lcolDescrMcdt) = flexAlignLeftCenter
        .ColWidth(lcolDescrMcdt) = 3800
        .Col = lcolDescrMcdt
        .TextMatrix(0, lcolDescrMcdt) = "Exame"
        
        .ColAlignment(lColDescrConven) = flexAlignLeftCenter
        .ColWidth(lColDescrConven) = 2000
        .Col = lColDescrConven
        .TextMatrix(0, lColDescrConven) = "�rea de Conven��o"
        
        .ColAlignment(lColDescrTotalMcdt) = flexAlignLeftCenter
        .ColWidth(lColDescrTotalMcdt) = 5100
        .Col = lColDescrTotalMcdt
        .TextMatrix(0, lColDescrTotalMcdt) = "MCDT/Descri��o"
        
        .ColAlignment(lColEstadoMcdt) = flexAlignLeftCenter
        .ColWidth(lColEstadoMcdt) = 1000
        .Col = lColEstadoMcdt
        .TextMatrix(0, lColEstadoMcdt) = "Estado"
        
        .ColAlignment(lColLateralidade) = flexAlignLeftCenter
        .ColWidth(lColLateralidade) = 1500
        .Col = lColLateralidade
        .TextMatrix(0, lColLateralidade) = "Caracteriza��o MCDT (N� Amostras)"
        
        '.ColAlignment(lColProduto) = flexAlignLeftCenter
        '.ColWidth(lColProduto) = 1000
        '.Col = lColProduto
        '.TextMatrix(0, lColProduto) = "Produto"
        
        '.ColAlignment(lColNAmostras) = flexAlignLeftCenter
        '.ColWidth(lColNAmostras) = 800
        '.Col = lColNAmostras
        '.TextMatrix(0, lColNAmostras) = "N� de Amostras"
        
        .ColAlignment(lColinfoClinComp) = flexAlignLeftCenter
        .ColWidth(lColinfoClinComp) = 2000
        .Col = lColinfoClinComp
        .TextMatrix(0, lColinfoClinComp) = "Info. Cl�nica"
        
        .ColAlignment(lColinfoIsencao) = flexAlignLeftCenter
        .ColWidth(lColinfoIsencao) = 680
        .Col = lColinfoIsencao
        .TextMatrix(0, lColinfoIsencao) = "Isen��o de Taxa"
        
        .WordWrap = True
        .row = 1
        .Col = 0
    End With
  
End Sub



Sub Inicializacoes()
    
    Me.caption = "Informa��o de Credencial Cativada"
    Me.left = 5
    Me.top = 5
    Me.Width = 16800
    Me.Height = 7500 ' Normal
    'Me.Height = 5505 ' Campos Extras
    
    'Set CampoDeFocus = EcPrescricao
        
    'LimpaCampos
End Sub

Sub FuncaoProcurar(credencial As String)
    Dim ssql As String
    Dim rsReq As New ADODB.recordset
    On Error GoTo TrataErro

    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql
    
    
    ssql = " SELECT req.req_number, req.req_date, req.search_date, req.cod_acesso, req.cod_prestacao, ent.entity_id, ent.entity_descr, presc.local_presc_code, presc.local_presc_descr, req.num_c_professional, "
    ssql = ssql & " req.clinical_info, req.consent, req.domicile_exam, req.urgent_exam, req.domicile_exam_just, req.urgent_exam_just, "
    ssql = ssql & " req.req_id  FROM esp_requisition req LEFT JOIN esp_entity ent on req.entity_id = ent.entity_id LEFT JOIN esp_prescription_local presc on req.local_presc_id = presc.local_presc_id "
    ssql = ssql & " WHERE req_number = " & BL_TrataStringParaBD(credencial)

    'rsReq.Open sSql, gConexao
    Set rsReq = BG_ExecutaSELECT(ssql)
    
    If rsReq.RecordCount > 0 Then
        While Not rsReq.EOF
            EstrutInfoCredencial.num_credencial = BL_HandleNull(rsReq!req_number, "")
            EstrutInfoCredencial.dt_req = BL_HandleNull(rsReq!req_date, "")
            EstrutInfoCredencial.dt_pesq_cred = BL_HandleNull(rsReq!search_date, "")
            EstrutInfoCredencial.cod_agendamento = BL_HandleNull(rsReq!cod_acesso, "")
            EstrutInfoCredencial.cod_prestacao = BL_HandleNull(rsReq!cod_prestacao, "")
            EstrutInfoCredencial.cod_entidade = BL_HandleNull(rsReq!entity_id, "")
            EstrutInfoCredencial.descr_entidade = BL_HandleNull(rsReq!entity_descr, "")
            EstrutInfoCredencial.cod_presc = BL_HandleNull(rsReq!local_presc_code, "")
            EstrutInfoCredencial.descr_presc = BL_HandleNull(rsReq!local_presc_descr, "")
            EstrutInfoCredencial.num_professional = BL_HandleNull(rsReq!num_c_professional, "")
            EstrutInfoCredencial.info_clinica = BL_HandleNull(rsReq!clinical_info, "")
            EstrutInfoCredencial.consentimento = BL_HandleNull(rsReq!consent, "")
            EstrutInfoCredencial.urgente_exam = BL_HandleNull(rsReq!urgent_exam, "")
            EstrutInfoCredencial.domicilio_exam = BL_HandleNull(rsReq!domicile_exam, "")
            EstrutInfoCredencial.just_domicilio = BL_HandleNull(rsReq!domicile_exam_just, "")
            EstrutInfoCredencial.just_urgente = BL_HandleNull(rsReq!urgent_exam_just, "")
            EstrutInfoCredencial.reqID = BL_HandleNull(rsReq!req_id, "")
            rsReq.MoveNext
        Wend
        rsReq.Close
        FgInfoCredencial.Visible = False
        PreencheFgInfoCredencial
        FgInfoCredencial.Visible = True
        
        'NELSONPSILVA 04.04.2018 Glintt-HS-18011
'        If gATIVA_LOGS_RGPD = mediSim Then
'            Dim responseJson As String
'            Dim requestJson As String
'            Dim CamposEcra() As Object
'            Dim NumCampos As Integer
'            NumCampos = 6
'            ReDim CamposEcra(0 To NumCampos - 1)
'         Set CamposEcra(0) = CbTUtente
'         Set CamposEcra(1) = EcUtente
'         Set CamposEcra(2) = CbSituacao
'         Set CamposEcra(3) = EcEpisodio
'         Set CamposEcra(4) = EcNome
'         Set CamposEcra(5) = EcSNS
'
'            requestJson = BL_TrataCamposRGPD(CamposEcra)
'            If CkDatas.value <> vbUnchecked Or requestJson <> "" Then
'                If requestJson <> "" Then
'                    requestJson = left$(requestJson, Len(requestJson) - 1)
'                    requestJson = requestJson & ", " & Chr(34) & "EcDtIni" & Chr(34) & ":" & Chr(34) & EcDtIni.value & Chr(34) & ", " & Chr(34) & "EcDtFim" & Chr(34) & ":" & Chr(34) & EcDtFinal.value & Chr(34) & "}"
'                Else
'                    requestJson = "{" & Chr(34) & "EcDtIni" & Chr(34) & ":" & Chr(34) & EcDtIni.value & Chr(34) & ", " & Chr(34) & "EcDtFim" & Chr(34) & ":" & Chr(34) & EcDtFinal.value & Chr(34) & "}"
'                End If
'            End If
'            'requestJson = left$(requestJson, Len(requestJson) - 1)
'            'requestJson = requestJson & ", " & Chr(34) & "EcDtIni" & Chr(34) & ":" & Chr(34) & EcDtIni.value & Chr(34) & ", " & Chr(34) & "EcDtFinal" & Chr(34) & ":" & Chr(34) & EcDtFinal.value & Chr(34) & "} "
'
'            responseJson = BL_TrataListBoxRGPD(Nothing, rsReq, False)
'            BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.Procura) & " - " & gFormActivo.Name, _
'            Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", IIf(requestJson = vbNullString, "{}", requestJson), IIf(responseJson = vbNullString, "{}", responseJson)
'        End If
'        rsReq.Close
    Else
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi encontrado nenhum registo!", vbExclamation, "Procurar"
        EventoUnload
    End If
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoProcurar: " & Err.Number & " - " & Err.Description, Me.Name, "FuncaoProcurar", True
    Exit Sub
    Resume Next
End Sub


Sub FuncaoLimpar()
    

End Sub


Private Sub PreencheFgInfoCredencial()
    Dim i As Integer
    On Error GoTo TrataErro
    
        FgCredencial.TextMatrix(1, lColNReq) = EstrutInfoCredencial.num_credencial
        FgCredencial.TextMatrix(1, lcolDtReq) = EstrutInfoCredencial.dt_req
        FgCredencial.TextMatrix(1, lColDtPesqCred) = EstrutInfoCredencial.dt_pesq_cred
        FgCredencial.TextMatrix(1, lColCodAgen) = EstrutInfoCredencial.cod_agendamento
        FgCredencial.TextMatrix(1, lColCodPrest) = EstrutInfoCredencial.cod_prestacao
        
        FgInfoCredencial.TextMatrix(1, lColCodEnt) = EstrutInfoCredencial.cod_entidade & " - " & EstrutInfoCredencial.descr_entidade
        'FgInfoCredencial.TextMatrix(1, lColDescrEnt) = EstrutInfoCredencial.descr_entidade
        FgInfoCredencial.TextMatrix(1, lColCodPresc) = EstrutInfoCredencial.cod_presc & " - " & EstrutInfoCredencial.descr_presc
        'FgInfoCredencial.TextMatrix(1, lColDescrPresc) = EstrutInfoCredencial.descr_presc
        FgInfoCredencial.TextMatrix(1, lColNumProf) = EstrutInfoCredencial.num_professional
        
        If EstrutInfoCredencial.info_clinica = "" Then
            FgInfoCredencial.ColAlignment(lColInfClin) = flexAlignCenterTop
            FgInfoCredencial.TextMatrix(1, lColInfClin) = "__"
        Else
            FgInfoCredencial.TextMatrix(1, lColInfClin) = EstrutInfoCredencial.info_clinica
        End If
        
        If EstrutInfoCredencial.consentimento = "" Then
            FgInfoCredencial.ColAlignment(lcolConsentimento) = flexAlignCenterTop
            FgInfoCredencial.TextMatrix(1, lcolConsentimento) = "__"
        Else
            FgInfoCredencial.TextMatrix(1, lcolConsentimento) = EstrutInfoCredencial.consentimento
        End If
        
        If EstrutInfoCredencial.urgente_exam = "" Then
            FgInfoCredencial.ColAlignment(lColUrg) = flexAlignCenterTop
            FgInfoCredencial.TextMatrix(1, lColUrg) = "__"
        Else
            FgInfoCredencial.TextMatrix(1, lColUrg) = EstrutInfoCredencial.urgente_exam
        End If
        
        If EstrutInfoCredencial.domicilio_exam = "" Then
            FgInfoCredencial.ColAlignment(lColDom) = flexAlignCenterTop
            FgInfoCredencial.TextMatrix(1, lColDom) = "__"
        Else
            FgInfoCredencial.TextMatrix(1, lColDom) = EstrutInfoCredencial.domicilio_exam
        End If
        
        If EstrutInfoCredencial.just_urgente = "" Then
            FgInfoCredencial.ColAlignment(lColJustUrg) = flexAlignCenterTop
            FgInfoCredencial.TextMatrix(1, lColJustUrg) = "__"
        Else
            FgInfoCredencial.TextMatrix(1, lColJustUrg) = EstrutInfoCredencial.just_urgente
        End If
        
        If EstrutInfoCredencial.just_domicilio = "" Then
            FgInfoCredencial.ColAlignment(lColJustDom) = flexAlignCenterTop
            FgInfoCredencial.TextMatrix(1, lColJustDom) = "__"
        Else
            FgInfoCredencial.TextMatrix(1, lColJustDom) = EstrutInfoCredencial.just_domicilio
        End If
  
        ProcuraCredencialInfoMCDT EstrutInfoCredencial.reqID
        
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheFgInfoCredencial: " & Err.Number & " - " & Err.Description, Me.Name, "PreencheFgInfoCredencial", True
    Exit Sub
    Resume Next
End Sub

Private Sub ProcuraCredencialInfoMCDT(ByVal reqID As String)
    Dim ssql As String
    Dim rsReqDet As New ADODB.recordset
    On Error GoTo TrataErro
    
    TotalCredencialInfoMCDT = 0
        
    rsReqDet.CursorLocation = adUseServer
    rsReqDet.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros ssql

    ssql = " select det.codigo_msp, det.msp_descr, det.codigo_areamsp, det.areamsp_descr, det.mcdt_id, det.mcdt_description, det.mcdt_state_code, det.mcdt_state_descr, det.laterality_descr, "
    ssql = ssql & " det.product_description, det.n_samples, det.clinical_info_det, det.isent_tax from esp_requisition_det det where req_id = " & BL_TrataStringParaBD(reqID) & ""

    Set rsReqDet = BG_ExecutaSELECT(ssql)
    'rsReqDet.Open sSql, gConexao
    
    If rsReqDet.RecordCount > 0 Then
        While Not rsReqDet.EOF
            PreencheEstrutInfoMCDT BL_HandleNull(rsReqDet!codigo_msp, ""), BL_HandleNull(rsReqDet!msp_descr, ""), _
                                     BL_HandleNull(rsReqDet!codigo_areamsp, ""), BL_HandleNull(rsReqDet!areamsp_descr, ""), BL_HandleNull(rsReqDet!mcdt_id, ""), _
                                     BL_HandleNull(rsReqDet!mcdt_description, ""), BL_HandleNull(rsReqDet!mcdt_state_code, ""), BL_HandleNull(rsReqDet!mcdt_state_descr, ""), BL_HandleNull(rsReqDet!laterality_descr, ""), _
                                     BL_HandleNull(rsReqDet!product_description, ""), BL_HandleNull(rsReqDet!n_samples, ""), BL_HandleNull(rsReqDet!clinical_info_det, ""), _
                                     BL_HandleNull(rsReqDet!isent_tax, "")

            rsReqDet.MoveNext
        Wend
        rsReqDet.Close
        FgInfoCredencial.Visible = False
        PreencheFgInfoMcdt
        FgInfoCredencial.Visible = True
        
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "ProcuraCredencialInfoMCDT: " & Err.Number & " - " & Err.Description, Me.Name, "ProcuraCredencialInfoMCDT", True
    Exit Sub
    Resume Next
End Sub


Private Sub PreencheEstrutInfoMCDT(codigo_msp As String, msp_descr As String, codigo_areamsp As String, _
                                 areamsp_descr As String, mcdt_id As String, mcdt_description As String, _
                                 mcdt_state_code As String, descrEstado As String, laterality_descr As String, product_description As String, _
                                 n_samples As String, clinical_info_det As String, isent_tax As String)
                                 
    On Error GoTo TrataErro
        
    TotalCredencialInfoMCDT = TotalCredencialInfoMCDT + 1
    ReDim Preserve EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT)
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).codMcdt = codigo_msp
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).descrMcdt = msp_descr
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).codConvencao = codigo_areamsp
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).descrConvencao = areamsp_descr
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).mcdtID = mcdt_id
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).descrTotalMcdt = mcdt_description
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).estadoMcdt = mcdt_state_code
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).descrEstado = descrEstado
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).lateralidade = laterality_descr
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).produto = product_description
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).numAmostras = n_samples
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).infoClinComplementar = clinical_info_det
    EstrutInfoCredencial.DetReq(TotalCredencialInfoMCDT).infoIsencao = isent_tax
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "ProcuraCredencialInfoMCDT: " & Err.Number & " - " & Err.Description, Me.Name, "ProcuraCredencialInfoMCDT", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheFgInfoMcdt()
    Dim i As Integer
    On Error GoTo TrataErro
    
    For i = 1 To TotalCredencialInfoMCDT
        FgInfoMcdt.TextMatrix(i, lcolDescrMcdt) = LTrim(EstrutInfoCredencial.DetReq(i).codMcdt & " - " & EstrutInfoCredencial.DetReq(i).descrMcdt)
        'FgInfoMcdt.TextMatrix(i, lcolDescrMcdt) = EstrutInfoCredencial.DetReq(i).descrMcdt
        'FgInfoMcdt.TextMatrix(i, lColCodConven) = EstrutInfoCredencial.DetReq(i).codConvencao
        FgInfoMcdt.TextMatrix(i, lColDescrConven) = LTrim(EstrutInfoCredencial.DetReq(i).codConvencao & " - " & EstrutInfoCredencial.DetReq(i).descrConvencao)
        'FgInfoMcdt.TextMatrix(i, lColMcdtId) = EstrutInfoCredencial.DetReq(i).mcdtID
        FgInfoMcdt.TextMatrix(i, lColDescrTotalMcdt) = LTrim(EstrutInfoCredencial.DetReq(i).mcdtID & " - " & EstrutInfoCredencial.DetReq(i).descrTotalMcdt)
        FgInfoMcdt.TextMatrix(i, lColEstadoMcdt) = EstrutInfoCredencial.DetReq(i).estadoMcdt & " - " & EstrutInfoCredencial.DetReq(i).descrEstado
        FgInfoMcdt.TextMatrix(i, lColLateralidade) = EstrutInfoCredencial.DetReq(i).lateralidade & ", " & EstrutInfoCredencial.DetReq(i).produto & " (" & EstrutInfoCredencial.DetReq(i).numAmostras & ")"
        'FgInfoMcdt.TextMatrix(i, lColProduto) = EstrutInfoCredencial.DetReq(i).produto
        'FgInfoMcdt.TextMatrix(i, lColNAmostras) = EstrutInfoCredencial.DetReq(i).numAmostras
        FgInfoMcdt.TextMatrix(i, lColinfoClinComp) = EstrutInfoCredencial.DetReq(i).infoClinComplementar
        FgInfoMcdt.TextMatrix(i, lColinfoIsencao) = EstrutInfoCredencial.DetReq(i).infoIsencao
        FgInfoMcdt.AddItem ""
    Next
    
    FgInfoMcdt.RemoveItem FgInfoMcdt.rows - 1
    FormHistCredencial.Show
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "ProcuraCredencialInfoMCDT: " & Err.Number & " - " & Err.Description, Me.Name, "ProcuraCredencialInfoMCDT", True
    Exit Sub
    Resume Next
End Sub

' Here you can add scrolling support to controls that don't normally respond
Public Sub MouseWheel(ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
  Dim ctl As Control
  
  For Each ctl In Me.Controls
    If TypeOf ctl Is MSFlexGrid Then
      If IsOver(ctl.hwnd, Xpos, Ypos) Then FlexGridScroll ctl, MouseKeys, Rotation, Xpos, Ypos
    End If
  Next ctl
End Sub



Attribute VB_Name = "PerfisMarcacao"
Option Explicit

' ---------------------------------------------------------------------------------------------------

' CARREGA PERFIS MARCADOS DA BD

' ---------------------------------------------------------------------------------------------------
Public Function PM_CarregaPerfisMarcados(n_req As String) As Boolean
    Dim sSql As String
    Dim rsPm As New ADODB.recordset
    gTotalPerfisMarcados = 0
    ReDim gEstrutPerfisMarcados(gTotalPerfisMarcados)
    sSql = "SELECT * FROM sl_marcacoes_perfis WHERE n_req = " & n_req
    rsPm.CursorLocation = adUseServer
    rsPm.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsPm.Open sSql, gConexao
    If rsPm.RecordCount >= 1 Then
        While Not rsPm.EOF
            If PM_AdicionaItemEstrutura(BL_HandleNull(rsPm!seq_marcacao_perfil, mediComboValorNull), BL_HandleNull(rsPm!n_req, mediComboValorNull), _
                                        BL_HandleNull(rsPm!Cod_Perfil, ""), BL_HandleNull(rsPm!cod_agrup, ""), BL_HandleNull(rsPm!user_cri, ""), _
                                        BL_HandleNull(rsPm!dt_cri, ""), BL_HandleNull(rsPm!hr_cri, "")) = False Then
                GoTo TrataErro
            End If
            rsPm.MoveNext
        Wend
    End If
    rsPm.Close
    Set rsPm = Nothing
    PM_CarregaPerfisMarcados = True
    
Exit Function
TrataErro:
    PM_CarregaPerfisMarcados = False
    BG_LogFile_Erros "PM_CarregaPerfisMarcados: " & Err.Number & " - " & Err.Description, "PM", "PM_CarregaPerfisMarcados"
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------

' GRAVA PERFIS MARCADOS DA BD

' ---------------------------------------------------------------------------------------------------

Public Function PM_GravaPerfisMarcados(n_req As String) As Boolean
    Dim sSql As String
    Dim rsPm As New ADODB.recordset
    Dim i As Integer
    For i = 1 To gTotalPerfisMarcados
        If gEstrutPerfisMarcados(i).seq_marcacao_perfil = mediComboValorNull Then
            If gSGBD = gOracle Then
                sSql = "SELECT seq_marcacao_perfil.nextval prox FROM dual "
            ElseIf gSGBD = gPostGres Then
                sSql = "SELECT nextval('seq_marcacao_perfil') prox "
            End If
            rsPm.CursorLocation = adUseServer
            rsPm.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsPm.Open sSql, gConexao
            If rsPm.RecordCount = 1 Then
                gEstrutPerfisMarcados(i).seq_marcacao_perfil = BL_HandleNull(rsPm!prox, -1)
                gEstrutPerfisMarcados(i).user_cri = BL_HandleNull(rsPm!prox, -1)
                gEstrutPerfisMarcados(i).dt_cri = Bg_DaData_ADO
                gEstrutPerfisMarcados(i).hr_cri = Bg_DaHora_ADO
                gEstrutPerfisMarcados(i).n_req = n_req
            End If
           rsPm.Close
           Set rsPm = Nothing
            
            If gEstrutPerfisMarcados(i).seq_marcacao_perfil > mediComboValorNull Then
                sSql = "INSERT INTO sl_marcacoes_perfis (seq_marcacao_perfil, n_req, cod_perfil, cod_Agrup, user_cri, dt_cri, hr_cri) VALUES("
                sSql = sSql & gEstrutPerfisMarcados(i).seq_marcacao_perfil & ","
                sSql = sSql & gEstrutPerfisMarcados(i).n_req & ","
                sSql = sSql & BL_TrataStringParaBD(gEstrutPerfisMarcados(i).Cod_Perfil) & ","
                sSql = sSql & BL_TrataStringParaBD(gEstrutPerfisMarcados(i).cod_agrup) & ","
                sSql = sSql & BL_TrataStringParaBD(gEstrutPerfisMarcados(i).user_cri) & ","
                sSql = sSql & BL_TrataDataParaBD(gEstrutPerfisMarcados(i).dt_cri) & ","
                sSql = sSql & BL_TrataStringParaBD(gEstrutPerfisMarcados(i).hr_cri) & ")"
                BG_ExecutaQuery_ADO sSql
            End If
        End If
    Next i
    PM_GravaPerfisMarcados = True
Exit Function
TrataErro:
    PM_GravaPerfisMarcados = False
    BG_LogFile_Erros "PM_GravaPerfisMarcados: " & Err.Number & " - " & Err.Description, "PM", "PM_GravaPerfisMarcados"
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------------------------

' ACTUALIZA ESTRUTURA DE PERFIS MARCADOS

' ---------------------------------------------------------------------------------------------------

Public Function PM_AdicionaItemEstrutura(seq_marcacao_perfil As Long, n_req As Long, Cod_Perfil As String, cod_agrup As String, _
                                          user_cri As String, dt_cri As String, hr_cri As String) As Boolean

    gTotalPerfisMarcados = gTotalPerfisMarcados + 1
    ReDim Preserve gEstrutPerfisMarcados(gTotalPerfisMarcados)
    
    gEstrutPerfisMarcados(gTotalPerfisMarcados).seq_marcacao_perfil = seq_marcacao_perfil
    gEstrutPerfisMarcados(gTotalPerfisMarcados).n_req = n_req
    gEstrutPerfisMarcados(gTotalPerfisMarcados).Cod_Perfil = Cod_Perfil
    gEstrutPerfisMarcados(gTotalPerfisMarcados).cod_agrup = cod_agrup
    gEstrutPerfisMarcados(gTotalPerfisMarcados).user_cri = user_cri
    gEstrutPerfisMarcados(gTotalPerfisMarcados).dt_cri = dt_cri
    gEstrutPerfisMarcados(gTotalPerfisMarcados).hr_cri = hr_cri
    PM_AdicionaItemEstrutura = True
End Function
    

' ---------------------------------------------------------------------------------------------------

' ACTUALIZA ESTRUTURA DE PERFIS MARCADOS

' ---------------------------------------------------------------------------------------------------

Public Function PM_LimpaEstrutura() As Boolean
    gTotalPerfisMarcados = 0
    ReDim gEstrutPerfisMarcados(gTotalPerfisMarcados)
    PM_LimpaEstrutura = True
End Function
    




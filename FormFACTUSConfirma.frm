VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormFACTUSConfirma 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFACTUSConfirma"
   ClientHeight    =   8805
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   15705
   Icon            =   "FormFACTUSConfirma.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8805
   ScaleWidth      =   15705
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtApagar 
      Caption         =   "Retirar"
      Height          =   855
      Left            =   5520
      Picture         =   "FormFACTUSConfirma.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   39
      ToolTipText     =   "Apagar Registos do FACTUS"
      Top             =   7800
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Caption         =   "An�lises "
      Height          =   5295
      Left            =   1200
      TabIndex        =   1
      Top             =   2400
      Width           =   13095
      Begin VB.CommandButton BExpandir 
         Height          =   495
         Left            =   12480
         Picture         =   "FormFACTUSConfirma.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   1920
         Width           =   495
      End
      Begin VB.CommandButton BtMinimizar 
         Height          =   495
         Left            =   12480
         Picture         =   "FormFACTUSConfirma.frx":19A0
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   2880
         Width           =   495
      End
      Begin VB.TextBox EcAux 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   0
         TabIndex        =   2
         Top             =   480
         Width           =   615
      End
      Begin MSFlexGridLib.MSFlexGrid FGAnalises 
         Height          =   4860
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   12255
         _ExtentX        =   21616
         _ExtentY        =   8573
         _Version        =   393216
         FixedCols       =   0
         ForeColor       =   0
         BackColorSel    =   -2147483643
         ForeColorSel    =   0
         BackColorBkg    =   -2147483633
         ScrollBars      =   2
         BorderStyle     =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Dados da Requisi��o"
      Height          =   2175
      Left            =   1200
      TabIndex        =   12
      Top             =   120
      Width           =   13095
      Begin VB.TextBox EcCredencial 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   1080
         TabIndex        =   40
         ToolTipText     =   "Credencial"
         Top             =   1800
         Width           =   3495
      End
      Begin VB.TextBox EcDataFinal 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   6000
         TabIndex        =   37
         Top             =   1080
         Width           =   1215
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2400
         TabIndex        =   34
         Top             =   240
         Width           =   1215
      End
      Begin VB.TextBox EcTipoUte 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   33
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   600
         Width           =   7335
      End
      Begin VB.TextBox EcDescrEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   1485
         Width           =   4815
      End
      Begin VB.TextBox EcCodEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   1080
         TabIndex        =   20
         Top             =   1485
         Width           =   975
      End
      Begin VB.TextBox EcKm 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   10800
         TabIndex        =   19
         Top             =   1560
         Width           =   615
      End
      Begin VB.ComboBox CbCodUrbano 
         BackColor       =   &H80000018&
         Height          =   315
         Left            =   10800
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   1080
         Width           =   1455
      End
      Begin VB.TextBox EcNumBenef 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   8160
         TabIndex        =   17
         Top             =   1530
         Width           =   1335
      End
      Begin VB.TextBox EcEstado 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   1080
         Width           =   2055
      End
      Begin VB.TextBox EcNumReq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   285
         Left            =   1080
         TabIndex        =   15
         Top             =   1080
         Width           =   975
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   6840
         Picture         =   "FormFACTUSConfirma.frx":266A
         Style           =   1  'Graphical
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   1460
         Width           =   375
      End
      Begin VB.TextBox EcDataInicial 
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   4560
         TabIndex        =   13
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Credencial"
         Height          =   255
         Index           =   19
         Left            =   120
         TabIndex        =   41
         Top             =   1800
         Width           =   975
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "e"
         Height          =   195
         Index           =   2
         Left            =   5790
         TabIndex        =   38
         Top             =   1080
         Width           =   90
      End
      Begin VB.Label Label1 
         Caption         =   "Total Fac.Rec."
         Height          =   255
         Index           =   1
         Left            =   7320
         TabIndex        =   36
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label LbTotalRecibo 
         Height          =   255
         Left            =   8520
         TabIndex        =   35
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         Height          =   195
         Left            =   120
         TabIndex        =   32
         Top             =   600
         Width           =   420
      End
      Begin VB.Label Label10 
         Caption         =   "E.F.R."
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   1485
         Width           =   495
      End
      Begin VB.Label Label4 
         Caption         =   "Km"
         Height          =   255
         Left            =   9600
         TabIndex        =   28
         Top             =   1560
         Width           =   615
      End
      Begin VB.Label Label3 
         Caption         =   "C�digo Urbano"
         Height          =   255
         Left            =   9600
         TabIndex        =   27
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label Label11 
         Caption         =   "Nr. Benef."
         Height          =   255
         Left            =   7320
         TabIndex        =   26
         Top             =   1530
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "N� Req."
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   25
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Data"
         Height          =   195
         Index           =   14
         Left            =   4200
         TabIndex        =   24
         Top             =   1095
         Width           =   345
      End
      Begin VB.Label LbCredenciais 
         Height          =   255
         Left            =   6360
         TabIndex        =   23
         Top             =   1560
         Width           =   2415
      End
      Begin VB.Label LbLote 
         Height          =   255
         Left            =   9480
         TabIndex        =   22
         Top             =   1560
         Width           =   1695
      End
   End
   Begin VB.CommandButton BtCima 
      Enabled         =   0   'False
      Height          =   495
      Left            =   14520
      Picture         =   "FormFACTUSConfirma.frx":2BF4
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Mudar Ordem"
      Top             =   5160
      Width           =   855
   End
   Begin VB.CommandButton BtBaixo 
      Enabled         =   0   'False
      Height          =   495
      Left            =   14520
      Picture         =   "FormFACTUSConfirma.frx":38BE
      Style           =   1  'Graphical
      TabIndex        =   10
      ToolTipText     =   "Mudar Ordem"
      Top             =   5760
      Width           =   855
   End
   Begin VB.CommandButton BtValida 
      Caption         =   "Confirmar"
      Enabled         =   0   'False
      Height          =   855
      Left            =   7560
      Picture         =   "FormFACTUSConfirma.frx":4588
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Actualizar Factura��o "
      Top             =   7800
      Width           =   1215
   End
   Begin VB.CommandButton BtRetira 
      Enabled         =   0   'False
      Height          =   615
      Left            =   14400
      Picture         =   "FormFACTUSConfirma.frx":5252
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Retirar An�lise"
      Top             =   6960
      Width           =   1095
   End
   Begin VB.CommandButton BtAdiciona 
      Enabled         =   0   'False
      Height          =   615
      Left            =   14400
      Picture         =   "FormFACTUSConfirma.frx":5F1C
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Adicionar An�lise"
      Top             =   3960
      Width           =   1095
   End
   Begin VB.TextBox EcCodAna 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   14400
      TabIndex        =   6
      Top             =   3120
      Width           =   1095
   End
   Begin MSFlexGridLib.MSFlexGrid FgReq 
      Height          =   7695
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   13573
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
   End
   Begin VB.Label LbTotalFactEFR 
      Height          =   255
      Left            =   3000
      TabIndex        =   30
      Top             =   8760
      Width           =   735
   End
End
Attribute VB_Name = "FormFACTUSConfirma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

'**** Emanuel Sousa 15.05.2009 *****************
'**** Vars para armazenar intervalo datas pedidas no Report ******'
Dim dataInicialReport As String
Dim dataFinalReport As String
'***********************************************'

Const NumLinhas = 16
'Linha da grid selecionada.
Dim LINHA_SEL As Integer
Dim COLUNA_SEL As Integer

'Linha da grid selecionada.
Dim LINHA_EDITA As Integer
Dim COLUNA_EDITA As Integer


Public rs As ADODB.recordset

' ------------------------------------------------------------------------------------
' ESTRUTURA QUE GUARDA AS DESCRICOES DAS ENTIDADES EFR
' ------------------------------------------------------------------------------------
Private Type DescEntidadesEFR
    codEntidade As String
    descEntidade As String
 
End Type
Dim DescEntidades() As DescEntidadesEFR
Dim TotalEntidadesEFR As Long

Private Type p1
    nr_req_ars As String
End Type
' ------------------------------------------------------------------------------------
' ESTRUTURA QUE GUARDA OS DADOS SOBRE A REQUISICAO A FACTURAR
' ------------------------------------------------------------------------------------
Private Type RequisicaoFacturar
    n_ord As Long
    t_episodio As String
    cod_empresa As String
    NReq As String
    user_cri As String
    hr_chega As String
    n_benef As String
    estadoFact As Integer
    codUrbano As Long
    km As Long
    codEfr As String
    NomeUtente As String
    Utente As String
    TipoUtente As String
    dt_cri As String
    
    n_lote As String
    n_fac As String
    TotalRecibos As String
    FacturaPercent As Boolean
    flg_insere_cod_rubr_efr As String
    flg_novaArs As Integer
    
    estrutP1() As p1
    totalP1 As Integer
End Type
Dim ReqFact() As RequisicaoFacturar
Dim TotalRequisicoes As Long

' ------------------------------------------------------------------------------------
' ESTRUTURA QUE GUARDA AS ANALISES A FACTURAR PARA UMA REQUISICAO
' ------------------------------------------------------------------------------------
Private Type AnalisesFacturar
    episodio As String
    n_seq_prog As Long
    n_ord As Long
    dt_ini_real As String
    cod_rubr As String
    cod_rubr_efr As String
    descr_rubr As String
    val_pr_unit As String
    qtd As Integer
    val_total_acto As String
    val_total As String
    perc_pag_efr As String
    val_pag_efr As String
    perc_pag_doe As String
    val_pr_u_doe As String
    val_pag_doe As String
    cod_isen_doe As Long
    flg_pagou_taxa As String
    nr_req_ars As String
    flg_estado As Integer
    chave_prog As String
    
    n_ord_ins As Long
    flg_tip_req As String
    flg_percentagem As Boolean
    
    p1 As String
    convencao As String
    cor As String
    cod_serv_exec As String
    flg_estado_doe As Long
    serie_fac_doe As String
    n_Fac_doe As Long
    nr_c As String
End Type
Dim EstrutAnalisesFact() As AnalisesFacturar
Dim TotalAnalisesFact As Integer

' -------------------------------------------------------------------
' COLUNAS NA GRELHA ENVIO PARA FACTURACAO
' -------------------------------------------------------------------
Const lColFactP1 = 0
Const lColFactData = 1
Const lColFactCodRubr = 2
Const lColFactDescrRubr = 3
Const lColFactCodRubrEFR = 4
Const lColFactQTD = 5
Const lColFactValActo = 6
Const lColFactValEFR = 7
Const lColFactValDoe = 8
Const lColFactPercDoe = 9
Const lColFactIsencao = 10
Const lColFactConvencao = 11
Const lColFactCor = 12



Const lCodDomicilio = 3
Const lCodInternacional = 2
Const lCodNormal = 1

Dim lCredencialActiva  As Long
Dim lPosicaoActual As Long
Dim ReqActual As Long

Dim FLG_ExecutaFGAna As Boolean
Const vermelhoClaro = &HBDC8F4
Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    
    Call Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    Call PreencheValoresDefeito
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
    gF_FACTUSENVIO = 1
    
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    'Emanuel Sousa - 13.05.2009
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    gF_FACTUSENVIO = 0
    
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    ' Fecha a conex�o secund�ria.
    BL_Fecha_Conexao_Secundaria
    
    Set FormFACTUSConfirma = Nothing
    
End Sub

Sub Inicializacoes()

    Me.caption = " Confirma��o da Factura��o"
    
    Me.left = 100
    Me.top = 20
    Me.Width = 15795
    Me.Height = 9500 ' Normal
    'Me.Height = 8700 ' Campos Extras
    
    Set CampoDeFocus = EcNumReq
    
    EcKm.Enabled = False
    
    LINHA_SEL = 1
    
    ' Abre a liga��o ao Factus.
    Call BL_Abre_Conexao_Secundaria(gSGBD_SECUNDARIA)
    ReqActual = 1
    DoEvents
    DoEvents
    DoEvents
    FLG_ExecutaFGAna = False
End Sub

 Sub DefTipoCampos()
    
    With FGAnalises
        ' pferreira 2011.09.06
        .Width = 12555
        .Cols = 13
        .rows = NumLinhas
        .FixedCols = 0
        .FixedRows = 1
        .Col = lColFactP1
        .ColWidth(lColFactP1) = 1800
        .TextMatrix(0, lColFactP1) = "Cred."
        
        .Col = lColFactData
        .ColWidth(lColFactData) = 900
        .TextMatrix(0, lColFactData) = "Data"
        
        .Col = lColFactCodRubr
        .ColWidth(lColFactCodRubr) = 700
        .TextMatrix(0, lColFactCodRubr) = "R�brica"
        
        .Col = lColFactDescrRubr
        .ColWidth(lColFactDescrRubr) = 2300
        .TextMatrix(0, lColFactDescrRubr) = "Descri��o da R�brica"
        
        .Col = lColFactCodRubrEFR
        .ColWidth(lColFactCodRubrEFR) = 800
        .TextMatrix(0, lColFactCodRubrEFR) = "Rubr EFR"
        
        .Col = lColFactQTD
        .ColWidth(lColFactQTD) = 400
        .TextMatrix(0, lColFactQTD) = "Qtd."
        
        .Col = lColFactValActo
        .ColWidth(lColFactValActo) = 700
        .TextMatrix(0, lColFactValActo) = "Val.Acto"
        
        .Col = lColFactValEFR
        .ColWidth(lColFactValEFR) = 700
        .TextMatrix(0, lColFactValEFR) = "Val Ent."
        
        .Col = lColFactValDoe
        .ColWidth(lColFactValDoe) = 700
        .TextMatrix(0, lColFactValDoe) = "Val Doe."
        
        .Col = lColFactPercDoe
        .ColWidth(lColFactPercDoe) = 500
        .TextMatrix(0, lColFactPercDoe) = "%Doe"
        
        .Col = lColFactIsencao
        .ColWidth(lColFactIsencao) = 900
        .TextMatrix(0, lColFactIsencao) = "Isen��o"
        
        .Col = lColFactConvencao
        .ColWidth(lColFactConvencao) = 1100
        .TextMatrix(0, lColFactConvencao) = "Conven��o"
        
        .Col = lColFactCor
        .ColWidth(lColFactCor) = 700
        .TextMatrix(0, lColFactCor) = "Cor"
        
        .ColAlignment(0) = 4
        .ColAlignment(1) = flexAlignLeftCenter
        .ColAlignment(2) = 4
        .ColAlignment(3) = 4
        .ColAlignment(4) = 4
        .ColAlignment(5) = flexAlignLeftCenter
        .DragMode = 0
    End With
    
    With FgReq
        .Cols = 1
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        .Col = 0
        .ColAlignment(0) = flexAlignLeftCenter
        .ColWidth(0) = 1000
        .TextMatrix(0, 0) = "Requisi��es"
    End With
    
    EcKm.Tag = adDouble
    EcKm.MaxLength = 6
    EcDataFinal.Tag = adDate
    EcDataInicial.Tag = adDate
    EcCodEFR.Tag = adNumeric
    
    
 End Sub
 
Private Sub BExpandir_Click()
    FGAnalises.Height = 7380
    Frame2.Height = 7695
    Frame2.top = 0

End Sub

Private Sub BtAdiciona_Click()
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(100) As Variant
    Dim PesqRapida As Boolean
    Dim sSql As String
    Dim i As Integer
    Dim TotalElementosSel As Integer
    Dim Portaria As String
    Dim seq_fact As Long
    Dim valEntidade As String
    Dim cod_rubr_efr As String
    Dim descr_rubr As String
    Dim j As String
    
    On Error GoTo TrataErro
    PesqRapida = False
    
    ChavesPesq(1) = "cod_rubr"
    CamposEcran(1) = "cod_rubr"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_rubr_efr"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposEcran(3) = "cod_rubr_efr"
    Tamanhos(3) = 1500
    Headers(3) = "C�digo EFR"
    
    If EcCodEFR = "" Then
        Exit Sub
    End If
    Portaria = IF_RetornaPortariaActiva(EcCodEFR)
    If Portaria = -1 Then
        Exit Sub
    End If
    CamposRetorno.InicializaResultados 1
    
    CFrom = "fa_pr_rubr "
    CWhere = " portaria = " & Portaria
    
    CampoPesquisa = "descr_rubr_efr"
           
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexaoSecundaria, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_rubr_efr ", _
                                                                           " R�bricas")
        
    
    If PesqRapida = True Then
        FormPesqRapidaAvancadaMultiSel.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa, TotalElementosSel
        
        If Not CancelouPesquisa Then
        
            For i = 1 To TotalElementosSel
                TotalAnalisesFact = TotalAnalisesFact + 1
                ReDim Preserve EstrutAnalisesFact(TotalAnalisesFact)
                
                ' --------------------------------------
                ' GERA SEQUENCIAL PARA FACTURACAO
                ' --------------------------------------
                seq_fact = -1
                j = 1
                While seq_fact = -1 And j <= 10
                    seq_fact = BL_GeraNumeroFact("SEQ_FACT")
                    j = j + 1
                Wend
                
                If seq_fact = -1 Then
                    Exit Sub
                End If
                cod_rubr_efr = ""
                valEntidade = ""
                
                'DADOS DA CODIFICACAO DA RUBRICA NA PORTARIA
                Dim RsEFR As New ADODB.recordset
                sSql = "SELECT x1.descr_rubr, x2.cod_rubr_efr,x2.val_taxa, x2.val_pag_doe,x2.val_pag_ent,x2.perc_pag_doe,100 - perc_pag_doe perc_pag_ent "
                sSql = sSql & " FROM fa_rubr x1, fa_pr_rubr x2 WHERE x1.cod_rubr = x2.cod_rubr and "
                sSql = sSql & " x1.cod_rubr = " & resultados(i) & " AND portaria = " & IF_RetornaPortariaActiva(ReqFact(ReqActual).codEfr)
                Set RsEFR = New ADODB.recordset
                RsEFR.CursorLocation = adUseServer
                RsEFR.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                RsEFR.Open sSql, gConexaoSecundaria
                If RsEFR.RecordCount < 1 Then
                    Exit Sub
                End If
                
                ' ---------------------------------------------------------------------------
                ' PREENCHE A ESTRUTURA
                ' ---------------------------------------------------------------------------
                EstrutAnalisesFact(TotalAnalisesFact).n_seq_prog = seq_fact
                EstrutAnalisesFact(TotalAnalisesFact).episodio = ReqFact(ReqActual).NReq
                EstrutAnalisesFact(TotalAnalisesFact).n_ord = ReqFact(ReqActual).n_ord
                If TotalAnalisesFact > 1 Then
                    EstrutAnalisesFact(TotalAnalisesFact).dt_ini_real = EstrutAnalisesFact(1).dt_ini_real
                Else
                    EstrutAnalisesFact(TotalAnalisesFact).dt_ini_real = Bg_DaData_ADO
                End If
                EstrutAnalisesFact(TotalAnalisesFact).cod_rubr = resultados(i)
                EstrutAnalisesFact(TotalAnalisesFact).cod_rubr_efr = BL_HandleNull(RsEFR!cod_rubr_efr, "")
                EstrutAnalisesFact(TotalAnalisesFact).cod_isen_doe = gTipoIsencaoNaoIsento
                EstrutAnalisesFact(TotalAnalisesFact).descr_rubr = BL_HandleNull(RsEFR!descr_rubr, "")
                EstrutAnalisesFact(TotalAnalisesFact).qtd = 1
                If IF_EfrFacturaPercent(ReqFact(ReqActual).codEfr) = True Then
                    EstrutAnalisesFact(TotalAnalisesFact).val_pr_unit = CDbl(BL_HandleNull(RsEFR!val_pag_ent, 0) + BL_HandleNull(RsEFR!val_pag_doe, 0))
                    EstrutAnalisesFact(TotalAnalisesFact).perc_pag_efr = CDbl(BL_HandleNull(RsEFR!perc_pag_ent, 100))
                    EstrutAnalisesFact(TotalAnalisesFact).val_total = CDbl(BL_HandleNull(RsEFR!val_pag_ent, 0) + BL_HandleNull(RsEFR!val_pag_doe, 0))
                    EstrutAnalisesFact(TotalAnalisesFact).perc_pag_doe = CDbl(BL_HandleNull(RsEFR!perc_pag_doe, 100))
                    EstrutAnalisesFact(TotalAnalisesFact).val_pag_doe = BL_HandleNull(RsEFR!val_Taxa, BL_HandleNull(RsEFR!val_pag_doe, 0))
                    EstrutAnalisesFact(TotalAnalisesFact).val_pr_u_doe = EstrutAnalisesFact(TotalAnalisesFact).val_pag_doe
                Else
                    EstrutAnalisesFact(TotalAnalisesFact).val_pr_unit = ""
                    EstrutAnalisesFact(TotalAnalisesFact).perc_pag_efr = ""
                    EstrutAnalisesFact(TotalAnalisesFact).val_total = ""
                    EstrutAnalisesFact(TotalAnalisesFact).perc_pag_doe = ""
                    EstrutAnalisesFact(TotalAnalisesFact).val_pag_doe = ""
                    EstrutAnalisesFact(TotalAnalisesFact).val_pr_u_doe = ""
                End If
                
                EstrutAnalisesFact(TotalAnalisesFact).val_total_acto = IF_RetornaTaxaRubricaEntidade(EstrutAnalisesFact(TotalAnalisesFact).cod_rubr, _
                                                                        ReqFact(ReqActual).codEfr, EstrutAnalisesFact(TotalAnalisesFact).dt_ini_real, _
                                                                        ReqFact(ReqActual).TipoUtente, ReqFact(ReqActual).Utente, _
                                                                        EstrutAnalisesFact(TotalAnalisesFact).nr_c, EstrutAnalisesFact(TotalAnalisesFact).cod_rubr_efr)
                EstrutAnalisesFact(TotalAnalisesFact).p1 = "1"
                EstrutAnalisesFact(TotalAnalisesFact).chave_prog = ReqFact(ReqActual).NReq
                EstrutAnalisesFact(TotalAnalisesFact).n_ord_ins = TotalAnalisesFact
                EstrutAnalisesFact(TotalAnalisesFact).flg_tip_req = "NB"
                EstrutAnalisesFact(TotalAnalisesFact).flg_estado = "1"
                EstrutAnalisesFact(TotalAnalisesFact).cod_serv_exec = CStr(gCodServExec)
                EstrutAnalisesFact(TotalAnalisesFact).flg_estado_doe = -1
                EstrutAnalisesFact(TotalAnalisesFact).serie_fac_doe = ""
                EstrutAnalisesFact(TotalAnalisesFact).n_Fac_doe = -1
                ' pferreira 2011.09.06
                If (ReqFact(ReqActual).flg_novaArs <> mediSim) Then: EstrutAnalisesFact(TotalAnalisesFact).nr_req_ars = ReqFact(ReqActual).NReq & EstrutAnalisesFact(TotalAnalisesFact).p1
                EstrutAnalisesFact(TotalAnalisesFact).flg_percentagem = ReqFact(ReqActual).FacturaPercent
                If Len(EstrutAnalisesFact(TotalAnalisesFact).flg_tip_req) > 0 Then
                    EstrutAnalisesFact(TotalAnalisesFact).convencao = Mid(EstrutAnalisesFact(TotalAnalisesFact).flg_tip_req, 1, 1)
                    If EstrutAnalisesFact(TotalAnalisesFact).convencao = "I" Then
                        EstrutAnalisesFact(TotalAnalisesFact).convencao = lCodInternacional
                    ElseIf EstrutAnalisesFact(TotalAnalisesFact).convencao = "D" Then
                        EstrutAnalisesFact(TotalAnalisesFact).convencao = lCodDomicilio
                    Else
                        EstrutAnalisesFact(TotalAnalisesFact).convencao = lCodNormal
                    End If
                End If
                If Len(EstrutAnalisesFact(TotalAnalisesFact).flg_tip_req) = 2 Then
                    EstrutAnalisesFact(TotalAnalisesFact).cor = Mid(EstrutAnalisesFact(TotalAnalisesFact).flg_tip_req, 2, 1)
                End If
                
                
                ' ---------------------------------------------------------------------------
                ' PREENCHE A GRELHA
                ' ---------------------------------------------------------------------------
                PreencheFGAnalises CLng(TotalAnalisesFact)
                
                ' ---------------------------------------------------------------------------
                ' AUMENTA GRELHA
                ' ---------------------------------------------------------------------------
                If TotalAnalisesFact >= (NumLinhas - 1) Then
                    FGAnalises.AddItem ""
                End If
                
                AdicionaAnalise
                RsEFR.Close
            Next
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "BtAdiciona_click"
    Exit Sub
    Resume Next
End Sub


Private Sub BtApagar_Click()
    Dim sSql As String
    Dim RsFact As New ADODB.recordset
    Dim i As Integer
    Dim linhasAfectadas As Integer
    On Error GoTo TrataErro
    

    If (BG_Mensagem(mediMsgBox, "Deseja n�o facturar a requisi��o em causa? ", _
                                vbQuestion + vbYesNo + vbDefaultButton2, _
                                " " & cAPLICACAO_NOME_CURTO) = vbNo) Then
        ' Sa�.
        Exit Sub
    End If
    sSql = "SELECT * FROM fa_movi_fact WHERE t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(ReqFact(ReqActual).NReq)
    sSql = sSql & " AND flg_estado = 3 " & " AND n_ord IN (select n_ord FROM fa_movi_resp WHERE t_episodio = 'SISLAB' AND episodio = "
    sSql = sSql & BL_TrataStringParaBD(ReqFact(ReqActual).NReq) & " AND cod_efr = " & ReqFact(ReqActual).codEfr & ")"
    RsFact.CursorLocation = adUseServer
    RsFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsFact.Open sSql, gConexaoSecundaria
    If RsFact.RecordCount > 0 Then
        BG_Mensagem mediMsgBox, "Existem registos j� facturados. N�o � poss�vel eliminar do FACTUS", vbExclamation, "EFR"
        Exit Sub
    End If
    RsFact.Close
    Set RsFact = Nothing
    gConexaoSecundaria.BeginTrans
    
    sSql = "DELETE FROM fa_movi_fact WHERE t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(ReqFact(ReqActual).NReq)
    sSql = sSql & " AND n_ord IN (select n_ord FROM fa_movi_resp WHERE t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(ReqFact(ReqActual).NReq)
    sSql = sSql & " AND cod_efr = " & ReqFact(ReqActual).codEfr & ")"
    gConexaoSecundaria.Execute sSql, linhasAfectadas
    If linhasAfectadas <= 0 Then
        GoTo TrataErro
    End If
    
    sSql = "DELETE FROM fa_movi_resp WHERE t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(ReqFact(ReqActual).NReq)
    sSql = sSql & " AND cod_efr = " & ReqFact(ReqActual).codEfr
    gConexaoSecundaria.Execute sSql, linhasAfectadas
    If linhasAfectadas <= 0 Then
        GoTo TrataErro
    End If
    
    BG_BeginTransaction
    sSql = "UPDATE sl_recibos_Det SET flg_facturado = 0 WHERE n_req = " & ReqFact(ReqActual).NReq
    sSql = sSql & " AND cod_efr = " & ReqFact(ReqActual).codEfr

    BG_ExecutaQuery_ADO sSql
    sSql = "UPDATE sl_requis SET flg_facturado = 0 WHERE n_req = " & ReqFact(ReqActual).NReq
    sSql = sSql & " AND cod_efr = " & ReqFact(ReqActual).codEfr
    BG_ExecutaQuery_ADO sSql
    
    sSql = "DELETE FROM sl_movi_fact WHERE t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(ReqFact(ReqActual).NReq)
    sSql = sSql & " AND n_ord IN (select n_ord FROM sl_movi_resp WHERE t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(ReqFact(ReqActual).NReq)
    sSql = sSql & " AND cod_efr = " & ReqFact(ReqActual).codEfr & ")"
    BG_ExecutaQuery_ADO sSql
    
    sSql = "DELETE FROM sl_movi_resp WHERE t_episodio = 'SISLAB' AND episodio = " & BL_TrataStringParaBD(ReqFact(ReqActual).NReq)
    sSql = sSql & " AND cod_efr = " & ReqFact(ReqActual).codEfr
    BG_ExecutaQuery_ADO sSql
    
    'APAGA REGISTOS DA SL_REQ_ARS
    If ReqFact(ReqActual).flg_novaArs = mediSim Then
        For i = 1 To ReqFact(ReqActual).totalP1
            IF_ApagaReqARS ReqFact(ReqActual).NReq, ReqFact(ReqActual).estrutP1(i).nr_req_ars
        Next
    End If
    
    gConexaoSecundaria.CommitTrans
    BG_CommitTransaction
    FuncaoLimpar
Exit Sub
TrataErro:
    gConexaoSecundaria.RollbackTrans
    BG_RollbackTransaction
    BG_LogFile_Erros "Erro ao apagar registos do FACTUS:" & Err.Number & " - " & Err.Description, Me.Name, "BtApagar_Click", True
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------

' TROCA A ORDEM COM A LINHA DE BAIXO

' ---------------------------------------------------------------------------
Private Sub BtBaixo_Click()
    
    On Error Resume Next
    
    Dim rv As Integer
    Dim l1 As Integer
    Dim l2 As Integer
    On Error GoTo TrataErro
    
        
    If (LINHA_SEL >= TotalAnalisesFact) Then
        Exit Sub
    End If
    
    l1 = LINHA_SEL
'    If EstrutAnalisesFact(l1).Flg_Estado <> 1 Or EstrutAnalisesFact(l1 + 1).Flg_Estado <> 1 Then
'        Exit Sub
'    End If
    FLG_ExecutaFGAna = False
    rv = Troca_Linha(l1 + 1, l1)
    If (rv = 1) Then
        LINHA_SEL = l1 + 1
    End If
    FLG_ExecutaFGAna = True
    FGAnalises.row = LINHA_SEL + 1
    FGAnalises.Col = 0
    FGAnalises_click
    FGAnalises.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "BtBaixo_click"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------

' TROCA A ORDEM COM A LINHA DE CIMA

' ---------------------------------------------------------------------------
Private Sub BtCima_Click()

    On Error GoTo TrataErro
    
    Dim rv As Integer
    Dim l1 As Integer
    Dim l2 As Integer

    
    If LINHA_SEL <= 1 Or LINHA_SEL > TotalAnalisesFact Then Exit Sub
    l1 = LINHA_SEL
    'If EstrutAnalisesFact(l1).Flg_Estado <> 1 Or EstrutAnalisesFact(l1 - 1).Flg_Estado <> 1 Then
    '    Exit Sub
    'End If
    FLG_ExecutaFGAna = False
        
    rv = Troca_Linha(l1 - 1, l1)
    If (rv = 1) Then
        LINHA_SEL = l1 - 1
    End If
    
    
    FLG_ExecutaFGAna = True
    FGAnalises.row = LINHA_SEL - 1
    FGAnalises.Col = 0
    FGAnalises_click
    FGAnalises.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "BtCima_click"
    Exit Sub
    Resume Next
End Sub

Private Sub BtMinimizar_Click()
    FGAnalises.Height = 4260
    Frame2.Height = 4695
    Frame2.top = 3000
End Sub

Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEFR, EcDescrEFR, ""
End Sub

Public Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEFR, EcDescrEFR, "")
End Sub


' ---------------------------------------------------------------------------

' RETIRA A AN�LISE DO ENVIO PARA FACTURACAO

' ---------------------------------------------------------------------------
Private Sub BtRetira_Click()
    
    On Error GoTo TrataErro
    
    If LINHA_SEL > TotalAnalisesFact Then Exit Sub
    If EstrutAnalisesFact(LINHA_SEL).flg_estado <> 1 Then
        Exit Sub
    End If

    If (BG_Mensagem(mediMsgBox, "Deseja n�o facturar " & _
                                FGAnalises.TextMatrix(LINHA_SEL, lColFactDescrRubr) & " ?      ", _
                                vbQuestion + vbYesNo + vbDefaultButton2, _
                                " " & cAPLICACAO_NOME_CURTO) = vbNo) Then
        ' Sa�.
        Exit Sub
    Else
        RetiraAnalise (LINHA_SEL)
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "BtRetira_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub CbCodUrbano_Click()
    If ReqActual <= TotalRequisicoes Then
        If CbCodUrbano.ListIndex <> mediComboValorNull Then
            EcKm.Enabled = True
            EcKm.SetFocus
        Else
            EcKm.Text = ""
            EcKm.Enabled = False
            ReqFact(ReqActual).km = 0
        End If
        ReqFact(ReqActual).codUrbano = BG_DaComboSel(CbCodUrbano)
    End If
End Sub

Private Sub CbCodUrbano_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 Then
        BG_LimpaOpcao CbCodUrbano, KeyCode
        EcKm.Text = ""
    End If
    
End Sub

Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        
        EcAux_LostFocus
    End If
End Sub

Private Sub EcAux_LostFocus()
    Dim i As Integer
    On Error GoTo TrataErro
    EcAux = UCase(EcAux)
    
    If COLUNA_EDITA = lColFactP1 Then
        For i = LINHA_EDITA To TotalAnalisesFact
            If EcAux <> "" Then
                AlteraP1 CLng(i), EcAux
            End If
        Next
        CalculaCredenciais
        
    ElseIf COLUNA_EDITA = lColFactQTD Then
        If EcAux <> "" Then
            AlteraQTD CLng(LINHA_EDITA), EcAux
        End If
        
    ElseIf COLUNA_EDITA = lColFactCodRubrEFR Then
            FGAnalises.TextMatrix(LINHA_EDITA, lColFactCodRubrEFR) = BL_HandleNull(EcAux, "0")
            EstrutAnalisesFact(LINHA_EDITA).cod_rubr_efr = BL_HandleNull(EcAux, "0")
            
    ElseIf COLUNA_EDITA = lColFactCodRubr Then
        Dim sSql As String
        Dim RsEFR As New ADODB.recordset
        sSql = "SELECT descr_rubr FROM fa_rubr WHERE cod_rubr = " & BL_HandleNull(EcAux, "0")
        Set RsEFR = New ADODB.recordset
        RsEFR.CursorLocation = adUseServer
        RsEFR.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        RsEFR.Open sSql, gConexaoSecundaria
        If RsEFR.RecordCount = 1 Then
            EstrutAnalisesFact(LINHA_EDITA).cod_rubr = BL_HandleNull(EcAux, "0")
            EstrutAnalisesFact(LINHA_EDITA).descr_rubr = BL_HandleNull(RsEFR!descr_rubr, "")
            EstrutAnalisesFact(LINHA_EDITA).val_total_acto = IF_RetornaTaxaRubricaEntidade(EstrutAnalisesFact(LINHA_EDITA).cod_rubr, _
                                                            ReqFact(ReqActual).codEfr, EstrutAnalisesFact(LINHA_EDITA).dt_ini_real, _
                                                            ReqFact(ReqActual).TipoUtente, ReqFact(ReqActual).Utente, EstrutAnalisesFact(LINHA_EDITA).nr_c, EstrutAnalisesFact(LINHA_EDITA).cod_rubr_efr)
            FGAnalises.TextMatrix(LINHA_EDITA, lColFactCodRubr) = BL_HandleNull(EcAux, "0")
            FGAnalises.TextMatrix(LINHA_EDITA, lColFactDescrRubr) = BL_HandleNull(RsEFR!descr_rubr, "")
            FGAnalises.TextMatrix(LINHA_EDITA, lColFactValActo) = EstrutAnalisesFact(LINHA_EDITA).val_total_acto
            AlteraQTD CLng(LINHA_EDITA), CLng(EstrutAnalisesFact(LINHA_EDITA).qtd)
        End If
        
    ElseIf COLUNA_EDITA = lColFactPercDoe Then
        If EcAux <> "" And CLng(EcAux) >= 0 And CLng(EcAux) <= 100 Then
            If (BG_Mensagem(mediMsgBox, "Deseja alterar para todas an�lises?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
                For i = 1 To TotalAnalisesFact
                    AlteraPercentDoente CLng(i), CDbl(EcAux)
                Next
            Else
                AlteraPercentDoente CLng(LINHA_EDITA), CDbl(EcAux)
            End If
        End If
    End If
    
    EcAux = ""
    EcAux.Visible = False
    FGAnalises.SetFocus
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "EcAux_LostFocus"
    Exit Sub
    Resume Next
End Sub

Private Sub EcCodAna_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 13 Then
        AdicionaAnalisePos EcCodAna, False
        EcCodAna = ""
    End If
End Sub

Private Sub EcKm_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcKm)
    If EcKm.Text = "" Then
        EcKm.Text = "0"
    End If
    ReqFact(ReqActual).km = IIf(EcKm.Text = "", 0, EcKm.Text)
    
End Sub


Private Sub EcNumBenef_Validate(Cancel As Boolean)
    ReqFact(ReqActual).n_benef = EcNumBenef.Text
End Sub

Private Sub EcNumReq_Click()

    'EcNumReq.text = ""
    DoEvents
    
End Sub

Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    If EcNumReq <> "" And KeyCode = 13 Then
        FuncaoProcurar
    End If
End Sub

Private Sub EcNumReq_LostFocus()
    
    EcNumReq.Tag = adDecimal
    If Not BG_ValidaTipoCampo_ADO(Me, EcNumReq) Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub FgAnalises_dblClick()
    Dim i As Long
    On Error GoTo TrataErro
    If FGAnalises.row <= TotalAnalisesFact Then
        If FGAnalises.Col = lColFactP1 Then
            If EstrutAnalisesFact(FGAnalises.row).flg_estado = 1 Then
                EcAux = EstrutAnalisesFact(FGAnalises.row).p1
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        ElseIf FGAnalises.Col = lColFactQTD Then
            If FGAnalises.TextMatrix(FGAnalises.row, lColFactQTD) <> "" And EstrutAnalisesFact(FGAnalises.row).flg_estado = 1 Then
                EcAux = EstrutAnalisesFact(FGAnalises.row).qtd
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        ElseIf FGAnalises.Col = lColFactPercDoe Then
            If ReqFact(ReqActual).FacturaPercent = True And EstrutAnalisesFact(FGAnalises.row).flg_estado = 1 Then
                EcAux = EstrutAnalisesFact(FGAnalises.row).perc_pag_doe
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        ElseIf FGAnalises.Col = lColFactCodRubrEFR Then
            If EstrutAnalisesFact(FGAnalises.row).flg_estado = 1 Then
                EcAux = EstrutAnalisesFact(FGAnalises.row).cod_rubr_efr
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        ElseIf FGAnalises.Col = lColFactCodRubr Then
            If EstrutAnalisesFact(FGAnalises.row).flg_estado = 1 Then
                EcAux = EstrutAnalisesFact(FGAnalises.row).cod_rubr
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        ElseIf FGAnalises.Col = lColFactP1 Then
            If EstrutAnalisesFact(FGAnalises.row).flg_estado = 1 Then
                EcAux = EstrutAnalisesFact(FGAnalises.row).p1
                EcAux.left = FGAnalises.CellLeft + 100
                EcAux.top = FGAnalises.CellTop + 220
                EcAux.Width = FGAnalises.CellWidth + 20
                EcAux.Visible = True
                EcAux.SetFocus
                LINHA_EDITA = FGAnalises.row
                COLUNA_EDITA = FGAnalises.Col
            End If
        ElseIf FGAnalises.Col = lColFactIsencao Then
            If FGAnalises.row <= TotalAnalisesFact And EstrutAnalisesFact(FGAnalises.row).flg_estado = 1 Then
                If (BG_Mensagem(mediMsgBox, "Deseja alterar para todas an�lises?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
                    
                    If EstrutAnalisesFact(FGAnalises.row).cod_isen_doe = 1 Then
                        For i = 1 To TotalAnalisesFact
                            EstrutAnalisesFact(i).cod_isen_doe = 0
                            FGAnalises.TextMatrix(i, lColFactIsencao) = "N�o Isento"
                        Next
                    Else
                        For i = 1 To TotalAnalisesFact
                            EstrutAnalisesFact(i).cod_isen_doe = 1
                            FGAnalises.TextMatrix(i, lColFactIsencao) = "Isento"
                        Next
                    End If
                Else
                    If EstrutAnalisesFact(FGAnalises.row).cod_isen_doe = 1 Then
                        EstrutAnalisesFact(FGAnalises.row).cod_isen_doe = 0
                        FGAnalises.TextMatrix(FGAnalises.row, lColFactIsencao) = "N�o Isento"
                    Else
                        EstrutAnalisesFact(FGAnalises.row).cod_isen_doe = 1
                        FGAnalises.TextMatrix(FGAnalises.row, lColFactIsencao) = "Isento"
                    End If
                End If
            End If
        ElseIf FGAnalises.Col = lColFactCor Then
            If FGAnalises.row <= TotalAnalisesFact And EstrutAnalisesFact(FGAnalises.row).flg_estado = 1 Then
                If (BG_Mensagem(mediMsgBox, "Deseja alterar para todas an�lises?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
                    For i = 1 To TotalAnalisesFact
                        AlteraCor i
                    Next
                Else
                    AlteraCor FGAnalises.row
                End If
            End If
        ElseIf FGAnalises.Col = lColFactConvencao Then
            If FGAnalises.row <= TotalAnalisesFact And EstrutAnalisesFact(FGAnalises.row).flg_estado = 1 Then
                If (BG_Mensagem(mediMsgBox, "Deseja alterar para todas an�lises?", vbQuestion + vbYesNo + vbDefaultButton2, " " & cAPLICACAO_NOME_CURTO) = vbYes) Then
                    Dim convencao As String
                    convencao = EstrutAnalisesFact(FGAnalises.row).convencao
                    For i = 1 To TotalAnalisesFact
                        AlteraConvencao i, convencao
                    Next
                Else
                    AlteraConvencao FGAnalises.row, EstrutAnalisesFact(FGAnalises.row).convencao
                End If
            End If
        End If
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "FGAnalises_DblClick"
    Exit Sub
    Resume Next
End Sub


Private Sub FGAnalises_RowColChange()
    If FLG_ExecutaFGAna = True Then
        FGAnalises_click
    End If
End Sub

Private Sub FgReq_Click()
    Dim i As Long
    On Error GoTo TrataErro
    LimpaCamposEcra
    LimpaFgAnalises
    TotalAnalisesFact = 0
    ReDim EstrutAnalisesFact(0)
    
    If FgReq.row > TotalRequisicoes Then
        Exit Sub
    End If
    ReqActual = FgReq.row
    
    EcNumReq = ReqFact(ReqActual).NReq
    EcTipoUte = ReqFact(ReqActual).TipoUtente
    EcUtente = ReqFact(ReqActual).Utente
    EcNome = ReqFact(ReqActual).NomeUtente
    EcCodEFR = ReqFact(ReqActual).codEfr
    EcDescrEFR = BL_SelCodigo("SL_EFR", "descr_efr", "cod_efr", ReqFact(ReqActual).codEfr)
    EcDataInicial = ReqFact(ReqActual).dt_cri
    EcDataFinal = ReqFact(ReqActual).dt_cri
    EcNumBenef = ReqFact(ReqActual).n_benef
    EcKm = ReqFact(ReqActual).km
    LbLote.caption = ReqFact(ReqActual).n_fac & " " & ReqFact(ReqActual).n_lote
    LbTotalRecibo.caption = ReqFact(ReqActual).TotalRecibos
    ' ---------
    ' URBANO
    ' ---------
    For i = 0 To CbCodUrbano.ListCount - 1
        If ReqFact(ReqActual).codUrbano = CbCodUrbano.ItemData(i) Then
            CbCodUrbano.ListIndex = i
            Exit For
        End If
    Next
                                                
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    
    DoEvents
    DoEvents
    DoEvents
    FuncaoProcurar_continua FgReq.row
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "FgReq_Click"
    Exit Sub
    Resume Next
End Sub

Private Sub FuncaoProcurar_continua(linha As Long)
    FLG_ExecutaFGAna = False
    PreencheEstrutAnalises ReqFact(linha).NReq
    CalculaCredenciais
            
    BtRetira.Enabled = True
    BtAdiciona.Enabled = True
    BtCima.Enabled = True
    BtBaixo.Enabled = True
    BtValida.Enabled = True
    DoEvents
    
    lCredencialActiva = 1
    lPosicaoActual = 1
    FLG_ExecutaFGAna = True
End Sub

Private Sub FgReq_RowColChange()
    'FgReq_Click
End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub FuncaoProcurar()
    
    On Error Resume Next
    
    Dim sRet As String
    
    Me.BtValida.Enabled = False
    DoEvents
    
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    DoEvents
    TotalRequisicoes = 0
    ReDim EstrutAnalisesFact(TotalRequisicoes)
    
            
    Preenche_Dados_Requisicao
    FLG_ExecutaFGAna = True
End Sub
Private Function Preenche_Entidades_EFR() As Boolean
'Emanuel Sousa 05.2009
On Error GoTo TrataErro

Dim sSql As String
Dim rsDescEFR As ADODB.recordset

 sSql = "SELECT cod_efr, descr_efr"
 sSql = sSql & " FROM sl_efr "
 sSql = sSql & " ORDER BY cod_efr asc "
 
 Set rsDescEFR = New ADODB.recordset
     rsDescEFR.CursorLocation = adUseServer
     rsDescEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
     rsDescEFR.Open sSql, gConexao
        
 If (rsDescEFR.RecordCount <= 0) Then
        MsgBox "Entidades EFR inexistentes.    ", vbExclamation, Me.caption
        rsDescEFR.Close
        Set rsDescEFR = Nothing
        Exit Function
 Else
        
        While Not rsDescEFR.EOF
            
            TotalEntidadesEFR = TotalEntidadesEFR + 1
            ReDim Preserve DescEntidades(TotalEntidadesEFR)
            
            ' ---------------------------------------------------------------------------
            ' PREENCHE ESTRUTURA COM DADOS DAS ENTIDADES EFR
            ' ---------------------------------------------------------------------------
            DescEntidades(TotalEntidadesEFR).codEntidade = BL_HandleNull(rsDescEFR!cod_efr, "")
            DescEntidades(TotalEntidadesEFR).descEntidade = BL_HandleNull(rsDescEFR!descr_efr, "")
            rsDescEFR.MoveNext
        Wend
        
        rsDescEFR.Close
        Set rsDescEFR = Nothing
        
    End If
    
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "Preenche_Entidades_EFR"
    BG_Mensagem mediMsgBox, "Erro ao preencher entidade EFR: " & Err.Description, vbOKOnly + vbInformation, "Preencher Entidade EFR"
    Exit Function
    Resume Next
    
End Function

    
' ---------------------------------------------------------------------------

' PREENCHE TODOS OS DADOS DA REQUISICAO E DO UTENTE EM CAUSA

' ---------------------------------------------------------------------------

Function Preenche_Dados_Requisicao() As Boolean
    
    On Error GoTo TrataErro
    
    Dim sSql As String
    Dim rsReq As ADODB.recordset
    Dim rsUte As New ADODB.recordset
    Dim i As Integer
    Dim flg_criterio As Boolean
    Dim iBD_Aberta As Long
    
    iBD_Aberta = BL_Abre_Conexao_Secundaria(gOracle)
    If iBD_Aberta = 0 Then
        Exit Function
    End If
    ' ---------------------------------------------------------------------------
    ' LIMPA ESTRUTURA DAS REQUISICOES A FACTURAR
    ' ---------------------------------------------------------------------------
    LimpaReqFact
    
    Preenche_Dados_Requisicao = True
    flg_criterio = False
    
    sSql = "SELECT x1.n_ord, x1.t_episodio, x1.episodio, x1.cod_efr, x2.descr_efr, x1.n_benef_doe,x1.user_cri,  x1.dt_cri,"
    sSql = sSql & " x1.cod_dom , x1.qtd_dom, x1.serie_fac, x1.n_fac,x1.t_doente, x1.Doente, x1.empresa_id, x2.flg_insere_cod_rubr_efr, x2.flg_nova_ars "
    sSql = sSql & " FROM fa_movi_resp x1, sl_efr x2 "
    sSql = sSql & " Where x1.cod_efr = x2.cod_efr and x1.episodio in (select distinct episodio from fa_movi_fact where episodio = x1.episodio AND "
    sSql = sSql & "  n_ord = x1.n_ord and (flg_estado = 1 or flg_estado = 8  )"
    
    If EcCredencial.Text <> "" Then
        flg_criterio = True
        sSql = sSql & " AND fa_movi_fact.nr_req_ars = " & BL_TrataStringParaBD(EcCredencial.Text)
    End If
    sSql = sSql & ") "
        
    If EcNumReq <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.episodio = " & BL_TrataStringParaBD(EcNumReq)
    End If
    
    If EcTipoUte <> "" And EcUtente <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.t_doente = " & BL_TrataStringParaBD(EcTipoUte) & " AND x1.doente = " & BL_TrataStringParaBD(EcUtente)
    End If
    
        
    If EcCodEFR <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.cod_Efr = " & EcCodEFR
    End If
        
    If EcNumBenef <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.n_benef_doe = " & BL_TrataStringParaBD(EcNumBenef)
    End If
            
    If EcDataInicial <> "" And EcDataFinal <> "" Then
        flg_criterio = True
        '***** Emanuel Sousa 15.05.2009 ********
        dataInicialReport = EcDataInicial.Text
        dataFinalReport = EcDataFinal.Text
        '***************************************
        sSql = sSql & " AND trunc(x1.dt_cri) between " & BL_TrataDataParaBD(EcDataInicial) & " AND " & BL_TrataDataParaBD(EcDataFinal)
    End If
    
    If flg_criterio = False Then
        BG_Mensagem mediMsgBox, "Tem que indicar um crit�rio.", vbOKOnly + vbInformation, "Crit�rios"
        Exit Function
    End If
    sSql = sSql & " ORDER BY episodio asc  "
    Set rsReq = New ADODB.recordset
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsReq.Open sSql, gConexao
        
    If (rsReq.RecordCount <= 0) Then
        Preenche_Dados_Requisicao = False
        MsgBox "Requisi��o inexistente.    ", vbExclamation, Me.caption
        rsReq.Close
        Set rsReq = Nothing
        Exit Function
    Else
        'EcCodEFR.Enabled = False
        'BtPesquisaEntFin.Enabled = False
        While Not rsReq.EOF
            
            TotalRequisicoes = TotalRequisicoes + 1
            ReDim Preserve ReqFact(TotalRequisicoes)
            
            ' ---------------------------------------------------------------------------
            ' PREENCHE ESTRUTURA COM DADOS DA REQUISICAO
            ' ---------------------------------------------------------------------------
            ReqFact(TotalRequisicoes).NReq = BL_HandleNull(rsReq!episodio, "")
            ReqFact(TotalRequisicoes).n_benef = BL_HandleNull(rsReq!n_benef_doe, "")
            If BL_HandleNull(rsReq!n_fac, "") <> "" Then
                ReqFact(TotalRequisicoes).estadoFact = 1
            Else
                ReqFact(TotalRequisicoes).estadoFact = 0
            End If
            ReqFact(TotalRequisicoes).totalP1 = 0
            ReDim ReqFact(TotalRequisicoes).estrutP1(0)
            
            ReqFact(TotalRequisicoes).codUrbano = BL_HandleNull(rsReq!cod_dom, 0)
            ReqFact(TotalRequisicoes).km = BL_HandleNull(rsReq!qtd_dom, 0)
            ReqFact(TotalRequisicoes).codEfr = BL_HandleNull(rsReq!cod_efr, "")
            ReqFact(TotalRequisicoes).TipoUtente = BL_HandleNull(rsReq!t_doente, "")
            ReqFact(TotalRequisicoes).Utente = BL_HandleNull(rsReq!doente, "")
            sSql = "SELECT * FROM sl_identif WHERE t_utente = " & BL_TrataStringParaBD(ReqFact(TotalRequisicoes).TipoUtente)
            sSql = sSql & " AND utente = " & BL_TrataStringParaBD(ReqFact(TotalRequisicoes).Utente)
            rsUte.CursorLocation = adUseServer
            rsUte.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsUte.Open sSql, gConexao
            If rsUte.RecordCount = 1 Then
                ReqFact(TotalRequisicoes).NomeUtente = BL_HandleNull(rsUte!nome_ute, "")
            End If
            rsUte.Close
            Set rsUte = Nothing
            ReqFact(TotalRequisicoes).dt_cri = BL_HandleNull(rsReq!dt_cri, Bg_DaData_ADO)
            ReqFact(TotalRequisicoes).FacturaPercent = IF_EfrFacturaPercent(BL_HandleNull(rsReq!cod_efr, ""))
            
            ReqFact(TotalRequisicoes).n_ord = BL_HandleNull(rsReq!n_ord, "1")
            ReqFact(TotalRequisicoes).t_episodio = BL_HandleNull(rsReq!t_episodio, "SISLAB")
            ReqFact(TotalRequisicoes).cod_empresa = BL_HandleNull(rsReq!empresa_id, "")
            ReqFact(TotalRequisicoes).flg_insere_cod_rubr_efr = BL_HandleNull(rsReq!flg_insere_cod_rubr_efr, "0")
            ReqFact(TotalRequisicoes).flg_novaArs = BL_HandleNull(rsReq!flg_nova_ars, 0)
            
            ReqFact(TotalRequisicoes).TotalRecibos = TotalRecibos()
            ' ---------------------------------------------------------------------------
            ' PREENCHE CAMPOS DO ECRA
            ' ---------------------------------------------------------------------------
            ReqFact(TotalRequisicoes).Utente = ReqFact(TotalRequisicoes).Utente
            PreencheLoteFac TotalRequisicoes
            
            FgReq.TextMatrix(TotalRequisicoes, 0) = ReqFact(TotalRequisicoes).NReq
            FgReq.AddItem ""
            rsReq.MoveNext
        Wend
        rsReq.Close
        Set rsReq = Nothing
        FgReq.row = 1
        FgReq_Click
    End If
    Preenche_Dados_Requisicao = True
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "Preenche_Dados_Requisicao"
    BG_Mensagem mediMsgBox, "Erro ao preencher Requisi��o: " & Err.Description, vbOKOnly + vbInformation, "Preencher Requisi��o"
    Preenche_Dados_Requisicao = False
    Exit Function
    Resume Next
End Function

Sub FuncaoLimpar()
    
    EcNumReq.Text = ""
    EcCredencial.Text = ""
    Me.EcNumReq.ForeColor = &H8000&
    LimpaCamposEcra
    EcNumReq.SetFocus
    
    LimpaFgAnalises
    
    Me.BtRetira.Enabled = False
    Me.BtAdiciona.Enabled = False
    Me.BtCima.Enabled = False
    Me.BtBaixo.Enabled = False
    Me.BtValida.Enabled = False
    LINHA_SEL = 0
    TotalAnalisesFact = 0
    
    DoEvents
    DoEvents
    'soliveira correccao lacto
    'EcNumReq.SetFocus
    LbTotalFactEFR = ""
    LimpaReqFact
    'EcCodEFR.Enabled = True
    'BtPesquisaEntFin.Enabled = True
End Sub

Sub FuncaoModificar()
    On Error GoTo TrataErro
    
    Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "FuncaoModificar"
End Sub

Sub PreencheValoresDefeito()
    BG_PreencheComboBD_ADO "sl_tbf_t_urbano", "cod_t_urbano", "descr_t_urbano", CbCodUrbano
    EcAux.Visible = False
End Sub

' Seleciona uma linha da grid.

Private Sub FGAnalises_click()

    On Error Resume Next
    Dim coluna As Integer
    Dim msf As Object
    Dim i As Integer
    Dim linha As Integer
    If FGAnalises.CellFontBold = True Then
        Exit Sub
    End If
    If FLG_ExecutaFGAna = True Then
        FLG_ExecutaFGAna = False
                
        'FGAnalises.Col = coluna
        LINHA_SEL = FGAnalises.row
        COLUNA_SEL = FGAnalises.Col
        FLG_ExecutaFGAna = True
    End If
End Sub

' ---------------------------------------------------------------------------

' TROCA DUAS LINHAS NA ESTRUTURA E NA GRELHA

' ---------------------------------------------------------------------------

Private Function Troca_Linha(linha1 As Integer, linha2 As Integer) As Integer
    Dim n_seq_prog As String
    Dim episodio As String
    Dim n_ord As String
    Dim dt_ini_real As String
    Dim cod_rubr As String
    Dim cod_rubr_efr As String
    Dim cod_isen_doe As Integer
    Dim descr_rubr As String
    Dim val_pr_unit As String
    Dim qtd As Integer
    Dim perc_pag_efr As String
    Dim val_total As String
    Dim val_total_acto As String
    Dim perc_pag_doe As String
    Dim val_pag_doe As String
    Dim chave_prog As String
    Dim p1 As String
    Dim n_ord_ins As String
    Dim flg_tip_req As String
    Dim flg_estado As Integer
    Dim convencao As String
    Dim cor As String
    Dim val_pr_u_doente As String
    Dim val_pag_efr As String
    On Error GoTo TrataErro
    
    n_seq_prog = EstrutAnalisesFact(linha1).n_seq_prog
    episodio = EstrutAnalisesFact(linha1).episodio
    n_ord = EstrutAnalisesFact(linha1).n_ord
    dt_ini_real = EstrutAnalisesFact(linha1).dt_ini_real
    cod_rubr = EstrutAnalisesFact(linha1).cod_rubr
    cod_rubr_efr = EstrutAnalisesFact(linha1).cod_rubr_efr
    cod_isen_doe = EstrutAnalisesFact(linha1).cod_isen_doe
    descr_rubr = EstrutAnalisesFact(linha1).descr_rubr
    val_pr_unit = EstrutAnalisesFact(linha1).val_pr_unit
    qtd = EstrutAnalisesFact(linha1).qtd
    perc_pag_efr = EstrutAnalisesFact(linha1).perc_pag_efr
    val_total = EstrutAnalisesFact(linha1).val_total
    val_total_acto = EstrutAnalisesFact(linha1).val_total_acto
    perc_pag_doe = EstrutAnalisesFact(linha1).perc_pag_doe
    val_pag_doe = EstrutAnalisesFact(linha1).val_pag_doe
    chave_prog = EstrutAnalisesFact(linha1).chave_prog
    p1 = EstrutAnalisesFact(linha1).p1
    n_ord_ins = EstrutAnalisesFact(linha1).n_ord_ins
    flg_tip_req = EstrutAnalisesFact(linha1).flg_tip_req
    flg_estado = EstrutAnalisesFact(linha1).flg_estado
    convencao = EstrutAnalisesFact(linha1).convencao
    cor = EstrutAnalisesFact(linha1).cor
    val_pr_u_doente = EstrutAnalisesFact(linha1).val_pr_u_doe
    val_pag_efr = EstrutAnalisesFact(linha1).val_pag_efr

    
    EstrutAnalisesFact(linha1).n_seq_prog = EstrutAnalisesFact(linha2).n_seq_prog
    EstrutAnalisesFact(linha1).episodio = EstrutAnalisesFact(linha2).episodio
    EstrutAnalisesFact(linha1).n_ord = EstrutAnalisesFact(linha2).n_ord
    EstrutAnalisesFact(linha1).dt_ini_real = EstrutAnalisesFact(linha2).dt_ini_real
    EstrutAnalisesFact(linha1).cod_rubr = EstrutAnalisesFact(linha2).cod_rubr
    EstrutAnalisesFact(linha1).cod_rubr_efr = EstrutAnalisesFact(linha2).cod_rubr_efr
    EstrutAnalisesFact(linha1).cod_isen_doe = EstrutAnalisesFact(linha2).cod_isen_doe
    EstrutAnalisesFact(linha1).descr_rubr = EstrutAnalisesFact(linha2).descr_rubr
    EstrutAnalisesFact(linha1).val_pr_unit = EstrutAnalisesFact(linha2).val_pr_unit
    EstrutAnalisesFact(linha1).val_pr_u_doe = EstrutAnalisesFact(linha2).val_pr_u_doe
    EstrutAnalisesFact(linha1).qtd = EstrutAnalisesFact(linha2).qtd
    EstrutAnalisesFact(linha1).perc_pag_efr = EstrutAnalisesFact(linha2).perc_pag_efr
    EstrutAnalisesFact(linha1).val_total = EstrutAnalisesFact(linha2).val_total
    EstrutAnalisesFact(linha1).val_total_acto = EstrutAnalisesFact(linha2).val_total_acto
    EstrutAnalisesFact(linha1).perc_pag_doe = EstrutAnalisesFact(linha2).perc_pag_doe
    EstrutAnalisesFact(linha1).val_pag_doe = EstrutAnalisesFact(linha2).val_pag_doe
    EstrutAnalisesFact(linha1).chave_prog = EstrutAnalisesFact(linha2).chave_prog
    EstrutAnalisesFact(linha1).p1 = EstrutAnalisesFact(linha2).p1
    EstrutAnalisesFact(linha1).n_ord_ins = linha1
    EstrutAnalisesFact(linha1).flg_tip_req = EstrutAnalisesFact(linha2).flg_tip_req
    EstrutAnalisesFact(linha1).flg_estado = EstrutAnalisesFact(linha2).flg_estado
    EstrutAnalisesFact(linha1).convencao = EstrutAnalisesFact(linha2).convencao
    EstrutAnalisesFact(linha1).cor = EstrutAnalisesFact(linha2).cor
    EstrutAnalisesFact(linha1).val_pag_efr = EstrutAnalisesFact(linha2).val_pag_efr
    
    EstrutAnalisesFact(linha2).n_seq_prog = n_seq_prog
    EstrutAnalisesFact(linha2).episodio = episodio
    EstrutAnalisesFact(linha2).n_ord = n_ord
    EstrutAnalisesFact(linha2).dt_ini_real = dt_ini_real
    EstrutAnalisesFact(linha2).cod_rubr = cod_rubr
    EstrutAnalisesFact(linha2).cod_rubr_efr = cod_rubr_efr
    EstrutAnalisesFact(linha2).cod_isen_doe = cod_isen_doe
    EstrutAnalisesFact(linha2).descr_rubr = descr_rubr
    EstrutAnalisesFact(linha2).val_pr_unit = val_pr_unit
    EstrutAnalisesFact(linha2).val_pr_u_doe = val_pr_u_doente
    EstrutAnalisesFact(linha2).qtd = qtd
    EstrutAnalisesFact(linha2).perc_pag_efr = perc_pag_efr
    EstrutAnalisesFact(linha2).val_total = val_total
    EstrutAnalisesFact(linha2).val_total_acto = val_total_acto
    EstrutAnalisesFact(linha2).perc_pag_doe = perc_pag_doe
    EstrutAnalisesFact(linha2).val_pag_doe = val_pag_doe
    EstrutAnalisesFact(linha2).chave_prog = chave_prog
    EstrutAnalisesFact(linha2).p1 = p1
    EstrutAnalisesFact(linha2).n_ord_ins = linha2
    EstrutAnalisesFact(linha2).flg_tip_req = flg_tip_req
    EstrutAnalisesFact(linha2).flg_estado = flg_estado
    EstrutAnalisesFact(linha2).convencao = convencao
    EstrutAnalisesFact(linha2).cor = cor
    EstrutAnalisesFact(linha2).val_pag_efr = val_pag_efr
    
    
    PreencheFGAnalises CLng(linha1)
    PreencheFGAnalises CLng(linha2)

    
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "Troca_Linha"
    Troca_Linha = -1
    Exit Function
End Function

' ---------------------------------------------------------------------------

' PREENCHE A ESTRUTURA COM AS ANALISES DA REQUISI��O

' ---------------------------------------------------------------------------

Public Function PreencheEstrutAnalises(requisicao As String, Optional NaoActualizaGrid As Boolean) As Integer
    Dim sSql As String
    Dim rsAnalises As New ADODB.recordset
    Dim TotalFactEnt As Double
    Dim i As Integer
    Dim flg_encontrouP1 As Boolean
    On Error GoTo TrataErro
    
    TotalAnalisesFact = 0
    ReDim EstrutAnalisesFact(0)
    
    TotalFactEnt = 0
    
    ' ---------------------------------------------------------------------------
    ' ANALISES EXISTENTES NA TABELA SL_RECIBOS_DET
    ' ---------------------------------------------------------------------------
    sSql = "SELECT x1.n_seq_prog, x1.cod_prog, x1.t_doente, x1.doente, x1.episodio, x1.n_ord, x1.dt_ini_real,"
    sSql = sSql & " x1.cod_rubr, x1.cod_rubr_efr, x2.descr_rubr, x1.val_pr_unit, x1.qtd, x1.perc_pag_efr,"
    sSql = sSql & " x1.val_total, x1.perc_pag_doe, x1.val_pag_doe, x1.cod_isen_doe, x1.flg_pagou_taxa,"
    sSql = sSql & " x1.nr_req_ars, x1.flg_estado, x1.flg_estado_doe, x1.chave_prog, x1.n_ord_ins,x1.flg_tip_req,"
    sSql = sSql & " x1.cod_serv_exec, x1.serie_fac_doe, x1.n_fac_doe,x1.val_pr_u_doe "
    sSql = sSql & " FROM fa_movi_fact x1, fa_rubr x2 WHERE x1.episodio = " & BL_TrataStringParaBD(requisicao)
    sSql = sSql & " AND x1.n_ord = " & ReqFact(ReqActual).n_ord
    sSql = sSql & " AND x1.cod_rubr = x2.cod_rubr  AND (x1.flg_estado = 1 OR x1.flg_estado = 8 ) "
    sSql = sSql & " ORDER by nr_req_Ars ASC, n_ord_ins "
    
    rsAnalises.CursorLocation = adUseServer
    rsAnalises.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnalises.Open sSql, gConexaoSecundaria
    
    
    If rsAnalises.RecordCount > 0 Then
        While Not (rsAnalises.EOF)
            
            TotalAnalisesFact = TotalAnalisesFact + 1
            ReDim Preserve EstrutAnalisesFact(TotalAnalisesFact)
            
            ' ---------------------------------------------------------------------------
            ' PREENCHE A ESTRUTURA
            ' ---------------------------------------------------------------------------
            EstrutAnalisesFact(TotalAnalisesFact).n_seq_prog = BL_HandleNull(rsAnalises!n_seq_prog, 0)
            EstrutAnalisesFact(TotalAnalisesFact).episodio = Trim(requisicao)
            EstrutAnalisesFact(TotalAnalisesFact).n_ord = BL_HandleNull(rsAnalises!n_ord, 1)
            EstrutAnalisesFact(TotalAnalisesFact).dt_ini_real = BL_HandleNull(rsAnalises!dt_ini_real, "")
            EstrutAnalisesFact(TotalAnalisesFact).cod_rubr = BL_HandleNull(rsAnalises!cod_rubr, "")
            EstrutAnalisesFact(TotalAnalisesFact).cod_rubr_efr = BL_HandleNull(rsAnalises!cod_rubr_efr, "")
            EstrutAnalisesFact(TotalAnalisesFact).cod_isen_doe = BL_HandleNull(rsAnalises!cod_isen_doe, 0)
            EstrutAnalisesFact(TotalAnalisesFact).descr_rubr = BL_HandleNull(rsAnalises!descr_rubr, "")
            EstrutAnalisesFact(TotalAnalisesFact).val_pr_unit = BL_HandleNull(rsAnalises!val_pr_unit, 0)
            EstrutAnalisesFact(TotalAnalisesFact).val_pr_u_doe = BL_HandleNull(rsAnalises!val_pr_u_doe, 0)
            EstrutAnalisesFact(TotalAnalisesFact).qtd = BL_HandleNull(rsAnalises!qtd, "1")
            
            If ReqFact(ReqActual).FacturaPercent = True Then
                EstrutAnalisesFact(TotalAnalisesFact).perc_pag_efr = BL_HandleNull(rsAnalises!perc_pag_efr, "")
                EstrutAnalisesFact(TotalAnalisesFact).val_total = BL_HandleNull(rsAnalises!val_total, "")
                EstrutAnalisesFact(TotalAnalisesFact).perc_pag_doe = BL_HandleNull(rsAnalises!perc_pag_doe, "")
                EstrutAnalisesFact(TotalAnalisesFact).val_pag_doe = BL_HandleNull(rsAnalises!val_pag_doe, "")
            Else
                EstrutAnalisesFact(TotalAnalisesFact).val_pr_unit = IF_RetornaTaxaRubricaEntidade(EstrutAnalisesFact(TotalAnalisesFact).cod_rubr, _
                                                                    ReqFact(ReqActual).codEfr, EstrutAnalisesFact(TotalAnalisesFact).dt_ini_real, _
                                                                    ReqFact(ReqActual).TipoUtente, ReqFact(ReqActual).Utente, EstrutAnalisesFact(TotalAnalisesFact).nr_c, EstrutAnalisesFact(TotalAnalisesFact).cod_rubr_efr)
                                                                    
                EstrutAnalisesFact(TotalAnalisesFact).val_pag_efr = CDbl(Replace(EstrutAnalisesFact(TotalAnalisesFact).val_pr_unit, ".", ",")) * EstrutAnalisesFact(TotalAnalisesFact).qtd
                EstrutAnalisesFact(TotalAnalisesFact).perc_pag_efr = ""
                EstrutAnalisesFact(TotalAnalisesFact).val_pr_u_doe = IF_RetornaTaxaRubrica(EstrutAnalisesFact(TotalAnalisesFact).cod_rubr, ReqFact(ReqActual).codEfr, ReqFact(ReqActual).TipoUtente, ReqFact(ReqActual).Utente, EstrutAnalisesFact(TotalAnalisesFact).dt_ini_real)
                EstrutAnalisesFact(TotalAnalisesFact).val_pag_doe = CDbl(Replace(EstrutAnalisesFact(TotalAnalisesFact).val_pr_u_doe, ".", ",")) * EstrutAnalisesFact(TotalAnalisesFact).qtd
                EstrutAnalisesFact(TotalAnalisesFact).perc_pag_doe = "0"
                EstrutAnalisesFact(TotalAnalisesFact).val_total = CDbl(Replace(EstrutAnalisesFact(TotalAnalisesFact).val_pag_doe, ".", ",")) + CDbl(Replace(EstrutAnalisesFact(TotalAnalisesFact).val_pag_efr, ".", ","))
            End If
            EstrutAnalisesFact(TotalAnalisesFact).val_total_acto = CDbl(Replace(EstrutAnalisesFact(TotalAnalisesFact).val_pr_u_doe, ".", ",")) + CDbl(Replace(EstrutAnalisesFact(TotalAnalisesFact).val_pr_unit, ".", ","))
            EstrutAnalisesFact(TotalAnalisesFact).chave_prog = BL_HandleNull(rsAnalises!chave_prog, "")
            EstrutAnalisesFact(TotalAnalisesFact).p1 = Replace(BL_HandleNull(rsAnalises!nr_req_ars, ""), ReqFact(ReqActual).NReq, "")
            EstrutAnalisesFact(TotalAnalisesFact).n_ord_ins = BL_HandleNull(rsAnalises!n_ord_ins, 0)
            EstrutAnalisesFact(TotalAnalisesFact).flg_tip_req = BL_HandleNull(rsAnalises!flg_tip_req, "")
            EstrutAnalisesFact(TotalAnalisesFact).flg_estado = BL_HandleNull(rsAnalises!flg_estado, "0")
            EstrutAnalisesFact(TotalAnalisesFact).cod_serv_exec = BL_HandleNull(rsAnalises!cod_serv_exec, "0")
            EstrutAnalisesFact(TotalAnalisesFact).flg_estado_doe = BL_HandleNull(rsAnalises!flg_estado_doe, -1)
            EstrutAnalisesFact(TotalAnalisesFact).serie_fac_doe = BL_HandleNull(rsAnalises!serie_fac_doe, "")
            EstrutAnalisesFact(TotalAnalisesFact).n_Fac_doe = BL_HandleNull(rsAnalises!n_Fac_doe, -1)
            ' pferreira 2011.09.06
            If (ReqFact(ReqActual).flg_novaArs = mediSim) Then
                EstrutAnalisesFact(TotalAnalisesFact).nr_req_ars = BL_HandleNull(rsAnalises!nr_req_ars, Trim(requisicao))
            Else
                EstrutAnalisesFact(TotalAnalisesFact).nr_req_ars = BL_HandleNull(rsAnalises!nr_req_ars, Trim(requisicao) & EstrutAnalisesFact(TotalAnalisesFact).p1)
            End If
            EstrutAnalisesFact(TotalAnalisesFact).flg_percentagem = ReqFact(ReqActual).FacturaPercent
            
            If Len(BL_HandleNull(rsAnalises!flg_tip_req, "")) > 0 Then
                EstrutAnalisesFact(TotalAnalisesFact).convencao = Mid(BL_HandleNull(rsAnalises!flg_tip_req, ""), 1, 1)
                If EstrutAnalisesFact(TotalAnalisesFact).convencao = "I" Then
                    EstrutAnalisesFact(TotalAnalisesFact).convencao = lCodInternacional
                ElseIf EstrutAnalisesFact(TotalAnalisesFact).convencao = "D" Then
                    EstrutAnalisesFact(TotalAnalisesFact).convencao = lCodDomicilio
                Else
                    EstrutAnalisesFact(TotalAnalisesFact).convencao = lCodNormal
                End If
            End If
            If Len(BL_HandleNull(rsAnalises!flg_tip_req, "")) = 2 Then
                EstrutAnalisesFact(TotalAnalisesFact).cor = Mid(BL_HandleNull(rsAnalises!flg_tip_req, ""), 2, 1)
            End If
            ' -------------- Emanuel Sousa 15.05.2009 -----------------------------------
            If (NaoActualizaGrid = False) Then
                    PreencheFGAnalises CLng(TotalAnalisesFact)
                    DoEvents
                    FLG_ExecutaFGAna = False
                    FLG_ExecutaFGAna = True
                    ' ---------------------------------------------------------------------------
                    ' AUMENTA GRELHA E ESTRUTURA
                    ' ---------------------------------------------------------------------------
                If TotalAnalisesFact >= (NumLinhas - 1) Then
                    FGAnalises.AddItem ""
                End If
            End If
            
            
            flg_encontrouP1 = False
            For i = 1 To ReqFact(ReqActual).totalP1
                If ReqFact(ReqActual).estrutP1(i).nr_req_ars = EstrutAnalisesFact(TotalAnalisesFact).p1 Then
                    flg_encontrouP1 = True
                    Exit For
                End If
            Next
            If flg_encontrouP1 = False Then
                ReqFact(ReqActual).totalP1 = ReqFact(ReqActual).totalP1 + 1
                ReDim Preserve ReqFact(ReqActual).estrutP1(ReqFact(ReqActual).totalP1)
                ReqFact(ReqActual).estrutP1(ReqFact(ReqActual).totalP1).nr_req_ars = EstrutAnalisesFact(TotalAnalisesFact).p1
            End If
            ' ---------------------------------------------------------------------------
            rsAnalises.MoveNext
        Wend
    End If
    rsAnalises.Close
    Set rsAnalises = Nothing
    FGAnalises.row = 1
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "PreencheEstrutAnalises"
    BG_Mensagem mediMsgBox, "Erro ao preencher An�lises: " & Err.Description, vbOKOnly + vbInformation, "Preencher An�lises"
    PreencheEstrutAnalises = -1
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------

' PREENCHE A ESTRUTURA COM AS ANALISES DA REQUISI��O

' ---------------------------------------------------------------------------

Private Sub BtValida_Click()
    Dim sSql As String
    Dim codDom As Integer
    Dim flg_insere_cod_rubr_efr As Boolean
    Dim i As Integer
    Dim actP1 As String
    Dim n_ord_ins As Integer
    On Error GoTo TrataErro
    
    If VerificaTotalAnaCred = False Then
        BG_Mensagem mediMsgBox, "Existem credenciais da ARS com mais que 6 an�lises", vbOKOnly + vbInformation, "Factura��o"
        Exit Sub
    End If
    
    gConexaoSecundaria.BeginTrans
    ' APAGA DA TABELA FA_MOVI_RESP
    sSql = "DELETE FROM fa_movi_resp WHERE episodio = " & BL_TrataStringParaBD(ReqFact(ReqActual).NReq)
    sSql = sSql & " AND n_ord = " & ReqFact(ReqActual).n_ord
    BL_ExecutaQuery_Secundaria sSql
    If gSQLError <> 0 Then
        GoTo TrataErro
     End If

    ' APAGA DA TABELA FA_MOVI_FACT
    For i = 1 To TotalAnalisesFact
        sSql = "DELETE FROM fa_movi_fact WHERE episodio = " & BL_TrataStringParaBD(ReqFact(ReqActual).NReq)
        sSql = sSql & " AND n_seq_prog = " & EstrutAnalisesFact(i).n_seq_prog
        BL_ExecutaQuery_Secundaria sSql
        If gSQLError <> 0 Then
            GoTo TrataErro
         End If
    Next
     
    If CbCodUrbano.ListIndex > mediComboValorNull Then
        codDom = CbCodUrbano.ItemData(CbCodUrbano.ListIndex)
    Else
        codDom = "0"
    End If
    
    'VERIFICA SE INSERE OU NAO O CODIGO DA RUBRICA DA ENTIDADE
    If ReqFact(ReqActual).flg_insere_cod_rubr_efr = "1" Then
        flg_insere_cod_rubr_efr = True
    Else
        flg_insere_cod_rubr_efr = False
    End If
    
    IF_Factura_RequisicaoPrivado CLng(ReqFact(ReqActual).n_ord), ReqFact(ReqActual).t_episodio, ReqFact(ReqActual).NReq, _
                                 ReqFact(ReqActual).n_benef, ReqFact(ReqActual).codEfr, ReqFact(ReqActual).user_cri, _
                                 CInt(codDom), BL_HandleNull(EcKm, 0), ReqFact(ReqActual).TipoUtente, ReqFact(ReqActual).Utente, ReqFact(ReqActual).cod_empresa
                                 
     For i = 1 To TotalAnalisesFact
        If actP1 <> EstrutAnalisesFact(i).p1 Then
            actP1 = EstrutAnalisesFact(i).p1
            n_ord_ins = 0
        End If
        n_ord_ins = n_ord_ins + 1
        IF_Factura_AnalisePrivado gCodProg, CLng(EstrutAnalisesFact(i).n_seq_prog), CLng(ReqFact(ReqActual).n_ord), ReqFact(ReqActual).t_episodio, _
                                  ReqFact(ReqActual).NReq, ReqFact(ReqActual).TipoUtente, ReqFact(ReqActual).Utente, _
                                  EstrutAnalisesFact(i).dt_ini_real, EstrutAnalisesFact(i).cod_rubr, EstrutAnalisesFact(i).nr_req_ars, _
                                  EstrutAnalisesFact(i).flg_pagou_taxa, EstrutAnalisesFact(i).qtd, EstrutAnalisesFact(i).cod_serv_exec, _
                                  EstrutAnalisesFact(i).flg_estado, EstrutAnalisesFact(i).chave_prog, BL_HandleNull(ReqFact(ReqActual).user_cri, gCodUtilizador), _
                                  BL_HandleNull(ReqFact(ReqActual).dt_cri, Bg_DaData_ADO), CLng(EstrutAnalisesFact(i).cod_isen_doe), EstrutAnalisesFact(i).cor, _
                                  EstrutAnalisesFact(i).flg_tip_req, CLng(n_ord_ins), CLng(EstrutAnalisesFact(i).flg_estado_doe), _
                                  EstrutAnalisesFact(i).serie_fac_doe, CLng(EstrutAnalisesFact(i).n_Fac_doe), EstrutAnalisesFact(i).cod_rubr_efr, _
                                  EstrutAnalisesFact(i).val_total_acto, BL_HandleNull(EstrutAnalisesFact(i).perc_pag_doe, "0"), ReqFact(ReqActual).cod_empresa, _
                                  False, EstrutAnalisesFact(i).flg_percentagem, flg_insere_cod_rubr_efr
                                  
                                  
                                  
                                  
     Next
     FgReq.CellBackColor = vbGreen

     gConexaoSecundaria.CommitTrans
     BG_Mensagem mediMsgBox, "Requisi��o alterada no FACTUS. ", vbOKOnly + vbInformation, "Preencher Requisi��o"
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "BtValida_Click"
    BG_Mensagem mediMsgBox, "Requisi��o alterada no FACTUS. ", vbOKOnly + vbInformation, "Preencher Requisi��o"
    gConexaoSecundaria.RollbackTrans
    Exit Sub
    Resume Next
End Sub


' ---------------------------------------------------------------------------

' LIMPA A ESTRUTURA QUE CONTEM OS DADOS DA REQUISICAO A FACTURAR

' ---------------------------------------------------------------------------


Private Sub LimpaReqFact()
    FgReq.rows = 2
    FgReq.TextMatrix(1, 0) = ""
    FgReq.CellBackColor = vbWhite
End Sub




' ---------------------------------------------------------------------------

' LIMPA A GRELHA DAS ANALISES

' ---------------------------------------------------------------------------

Private Sub LimpaFgAnalises()
    Dim i As Integer
    For i = 1 To FGAnalises.rows - 2
        FGAnalises.RemoveItem 1
    Next
    FLG_ExecutaFGAna = False
    FGAnalises.rows = 1
    FGAnalises.rows = NumLinhas
    FLG_ExecutaFGAna = True
End Sub

Private Sub Ecdatareq_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub



' ---------------------------------------------------------------------------

' CALCULA QUANTAS AN�LISES EXISTEM POR CREDENCIAL

' ---------------------------------------------------------------------------
Private Sub CalculaCredenciais()
    Dim credenciais() As Integer
    Dim tot_credenciais() As Integer
    Dim i As Integer
    Dim j As Integer
        
    On Error GoTo TrataErro
    
    ReDim credenciais(0)
    ReDim tot_credenciais(0)
    
    For i = 1 To TotalAnalisesFact
        For j = 1 To UBound(credenciais)
            If EstrutAnalisesFact(i).p1 = credenciais(j) Then
                tot_credenciais(j) = tot_credenciais(j) + 1
                Exit For
            End If
        Next
        If j > UBound(credenciais) Or UBound(credenciais) = 0 Then
            If UBound(credenciais) = 0 Then
                ReDim credenciais(1)
                ReDim tot_credenciais(1)
                credenciais(1) = BL_HandleNull(EstrutAnalisesFact(i).p1, -1)
                tot_credenciais(1) = 1
            Else
                ReDim Preserve credenciais(UBound(credenciais) + 1)
                ReDim Preserve tot_credenciais(UBound(credenciais) + 1)
                credenciais(UBound(credenciais)) = EstrutAnalisesFact(i).p1
                tot_credenciais(UBound(credenciais)) = 1
            End If
        End If
    Next
    LbCredenciais = "Credenciais: "
    For i = 1 To UBound(credenciais)
        LbCredenciais = LbCredenciais & tot_credenciais(i) & "   "
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "CalculaCredenciais"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------

' ALTERA NUMERO DO P1

' ---------------------------------------------------------------------------
Private Sub AlteraP1(linha As Long, NovoP1 As String)
    Dim sSql As String
    On Error GoTo TrataErro
    EstrutAnalisesFact(linha).p1 = NovoP1
    ' pferreira 2011.09.06
    If (ReqFact(ReqActual).flg_novaArs = mediSim) Then
        EstrutAnalisesFact(linha).nr_req_ars = NovoP1
    Else
        EstrutAnalisesFact(linha).nr_req_ars = ReqFact(ReqActual).NReq & NovoP1
    End If
    EstrutAnalisesFact(linha).chave_prog = ReqFact(ReqActual).NReq
    FGAnalises.TextMatrix(linha, lColFactP1) = NovoP1
    
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "AlteraP1"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------

' ALTERA QUANTIDADE DO DOENTE

' ---------------------------------------------------------------------------
Private Sub AlteraQTD(linha As Long, qtd As Long)
    On Error GoTo TrataErro

    EstrutAnalisesFact(linha).qtd = qtd
    If ReqFact(ReqActual).FacturaPercent = True Then
        
        EstrutAnalisesFact(linha).val_pag_doe = Round(((CDbl((EstrutAnalisesFact(linha).val_total_acto) * EstrutAnalisesFact(linha).perc_pag_doe) / 100)) * EstrutAnalisesFact(linha).qtd, 2)
        EstrutAnalisesFact(linha).val_pag_efr = Round((CDbl(EstrutAnalisesFact(linha).val_total_acto) - EstrutAnalisesFact(linha).val_pr_u_doe), 2)
        EstrutAnalisesFact(linha).val_total = Round((CDbl(EstrutAnalisesFact(linha).val_pag_efr) - (EstrutAnalisesFact(linha).val_pag_doe)), 2)
    Else
        EstrutAnalisesFact(linha).val_pag_doe = Round(CDbl(EstrutAnalisesFact(linha).val_pr_u_doe) * EstrutAnalisesFact(linha).qtd, 2)
        EstrutAnalisesFact(linha).val_pag_efr = Round(CDbl(EstrutAnalisesFact(linha).val_pr_unit) * EstrutAnalisesFact(linha).qtd, 2)
        EstrutAnalisesFact(linha).val_total = Round((CDbl(EstrutAnalisesFact(linha).val_pag_efr) + (EstrutAnalisesFact(linha).val_pag_doe)), 2)
    End If
    PreencheFGAnalises linha
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "AlteraQTD"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------

' ALTERA PERCENTAGEM DO DOENTE

' ---------------------------------------------------------------------------
Private Sub AlteraPercentDoente(linha As Long, percentagem As Double)
    On Error GoTo TrataErro

    EstrutAnalisesFact(linha).perc_pag_doe = percentagem
    EstrutAnalisesFact(linha).perc_pag_efr = CStr(100 - percentagem)
    EstrutAnalisesFact(linha).val_pr_u_doe = Round(((CDbl(EstrutAnalisesFact(linha).val_total_acto) * percentagem) / 100), 2)
    EstrutAnalisesFact(linha).val_pag_doe = Round(((CDbl((EstrutAnalisesFact(linha).val_total_acto) * percentagem) / 100)) * EstrutAnalisesFact(linha).qtd, 2)
    EstrutAnalisesFact(linha).val_pr_unit = Round((CDbl(EstrutAnalisesFact(linha).val_total_acto) - EstrutAnalisesFact(linha).val_pr_u_doe), 2)
    EstrutAnalisesFact(linha).val_total = Round((CDbl(EstrutAnalisesFact(linha).val_total_acto) - EstrutAnalisesFact(linha).val_pr_u_doe) * EstrutAnalisesFact(linha).qtd, 2)
    PreencheFGAnalises linha
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "AlteraPercentDoente"
    Exit Sub
    Resume Next
End Sub


' ---------------------------------------------------------------------------

' ALTERA TIPO ISEN��O

' ---------------------------------------------------------------------------
Private Sub AlteraIsencao(linha As Long)
    On Error GoTo TrataErro
    
    If EstrutAnalisesFact(linha).cod_isen_doe = gTipoIsencaoIsento Then
        EstrutAnalisesFact(linha).cod_isen_doe = 0
        FGAnalises.TextMatrix(linha, lColFactIsencao) = "N�o Isento"
    Else
        EstrutAnalisesFact(linha).cod_isen_doe = gTipoIsencaoIsento
        FGAnalises.TextMatrix(linha, lColFactIsencao) = 1
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "AlteraIsencao"
    Exit Sub
    Resume Next
End Sub


' ---------------------------------------------------------------------------

' ALTERA COR

' ---------------------------------------------------------------------------
Private Sub AlteraCor(linha As Long)
    On Error GoTo TrataErro
    
    If EstrutAnalisesFact(linha).cor = "V" Then
        EstrutAnalisesFact(linha).cor = "B"
        FGAnalises.TextMatrix(linha, lColFactCor) = "Branco"
    Else
        EstrutAnalisesFact(linha).cor = "V"
        FGAnalises.TextMatrix(linha, lColFactCor) = "Verde"
    End If

    If EstrutAnalisesFact(linha).convencao = lCodInternacional Then
        EstrutAnalisesFact(linha).flg_tip_req = "I" & EstrutAnalisesFact(linha).cor
    ElseIf EstrutAnalisesFact(linha).convencao = lCodNormal Then
        EstrutAnalisesFact(linha).flg_tip_req = "N" & EstrutAnalisesFact(linha).cor
    Else
        EstrutAnalisesFact(linha).flg_tip_req = "D" & EstrutAnalisesFact(linha).cor
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "AlteraCor"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------

' ALTERA TIPO CONVENCAO

' ---------------------------------------------------------------------------
Private Sub AlteraConvencao(linha As Long, convencao As String)
    On Error GoTo TrataErro
    
    If convencao = lCodInternacional Then
        EstrutAnalisesFact(linha).convencao = lCodNormal
        EstrutAnalisesFact(linha).flg_tip_req = "N" & EstrutAnalisesFact(linha).cor
        FGAnalises.TextMatrix(linha, lColFactConvencao) = "Normal"
    ElseIf convencao = lCodNormal Then
        EstrutAnalisesFact(linha).convencao = lCodDomicilio
        EstrutAnalisesFact(linha).flg_tip_req = "D" & EstrutAnalisesFact(linha).cor
        FGAnalises.TextMatrix(linha, lColFactConvencao) = "Domicilio"
    Else
        EstrutAnalisesFact(linha).convencao = lCodInternacional
        EstrutAnalisesFact(linha).flg_tip_req = "I" & EstrutAnalisesFact(linha).cor
        FGAnalises.TextMatrix(linha, lColFactConvencao) = "Internacional"
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "AlteraConvencao"
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------

' INSERE DIRECTAMENTE A LINHA NA BASE DADOS REFERENTE A NOVA ANALISE

' ---------------------------------------------------------------------------
Private Sub AdicionaAnalise()
    On Error GoTo TrataErro
    
    CalculaCredenciais
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "AdicionaAnalise"
    Exit Sub
    Resume Next
End Sub


Private Sub EcDataInicial_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
   If Trim(EcDataInicial.Text) = "" Then
        EcDataInicial.Text = Bg_DaData_ADO
        'SendKeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDataInicial_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataInicial)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    Else
    End If
    
End Sub

Private Sub EcDataFinal_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
   If Trim(EcDataFinal.Text) = "" Then
        EcDataFinal.Text = Bg_DaData_ADO
        'SendKeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDataFinal_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataFinal)
    If Cancel Then
        'SendKeys ("{HOME}+{END}")
    Else
    End If
    
End Sub
Private Sub LimpaCamposEcra()
    EcNumReq = ""
    EcCredencial.Text = ""
    EcTipoUte = ""
    EcUtente = ""
    EcNome = ""
    EcCodEFR = ""
    EcDescrEFR = ""
    EcNumBenef = ""
    EcDataFinal = ""
    EcDataInicial = ""
    CbCodUrbano.ListIndex = mediComboValorNull
    EcEstado = ""
    LbLote = ""
    EcKm.Text = ""
    LbTotalRecibo.caption = ""
End Sub


Sub AcrescentaLinha(linha As Integer)
    Dim seq_fact As Long
    Dim i As Integer
    Dim linha2 As Long
    Dim j As Long
    
    On Error GoTo TrataErro
    
    ' --------------------------------------
    ' GERA SEQUENCIAL PARA FACTURACAO
    ' --------------------------------------
    seq_fact = -1
    j = 1
    While seq_fact = -1 And j <= 10
        seq_fact = BL_GeraNumeroFact("SEQ_FACT")
        j = j + 1
    Wend
    
    If seq_fact = -1 Then
        Exit Sub
    End If
    
    'Cria linha vazia
    FGAnalises.AddItem "", FGAnalises.row
    
    TotalAnalisesFact = TotalAnalisesFact + 1
    ReDim Preserve EstrutAnalisesFact(TotalAnalisesFact)
    If FGAnalises.row > 1 Then
        linha2 = FGAnalises.row - 1
    ElseIf TotalAnalisesFact > 1 Then
        linha2 = FGAnalises.row + 1
    Else
        Exit Sub
    End If
    i = TotalAnalisesFact
    
    While i > linha

        EstrutAnalisesFact(i).n_seq_prog = EstrutAnalisesFact(i - 1).n_seq_prog
        EstrutAnalisesFact(i).episodio = EstrutAnalisesFact(i - 1).episodio
        EstrutAnalisesFact(i).n_ord = EstrutAnalisesFact(i - 1).n_ord
        EstrutAnalisesFact(i).dt_ini_real = EstrutAnalisesFact(i - 1).dt_ini_real
        EstrutAnalisesFact(i).cod_rubr = EstrutAnalisesFact(i - 1).cod_rubr
        EstrutAnalisesFact(i).cod_rubr_efr = EstrutAnalisesFact(i - 1).cod_rubr_efr
        EstrutAnalisesFact(i).cod_isen_doe = EstrutAnalisesFact(i - 1).cod_isen_doe
        EstrutAnalisesFact(i).descr_rubr = EstrutAnalisesFact(i - 1).descr_rubr
        EstrutAnalisesFact(i).val_pr_unit = EstrutAnalisesFact(i - 1).val_pr_unit
        EstrutAnalisesFact(i).val_pr_u_doe = EstrutAnalisesFact(i - 1).val_pr_u_doe
        EstrutAnalisesFact(i).qtd = EstrutAnalisesFact(i - 1).qtd
        EstrutAnalisesFact(i).perc_pag_efr = EstrutAnalisesFact(i - 1).perc_pag_efr
        EstrutAnalisesFact(i).val_total = EstrutAnalisesFact(i - 1).val_total
        EstrutAnalisesFact(i).val_total_acto = EstrutAnalisesFact(i - 1).val_total_acto
        EstrutAnalisesFact(i).perc_pag_doe = EstrutAnalisesFact(i - 1).perc_pag_doe
        EstrutAnalisesFact(i).val_pag_doe = EstrutAnalisesFact(i - 1).val_pag_doe
        EstrutAnalisesFact(i).chave_prog = EstrutAnalisesFact(i - 1).chave_prog
        EstrutAnalisesFact(i).p1 = EstrutAnalisesFact(i - 1).p1
        EstrutAnalisesFact(i).n_ord_ins = i - 1
        EstrutAnalisesFact(i).flg_tip_req = EstrutAnalisesFact(i - 1).flg_tip_req
        EstrutAnalisesFact(i).flg_estado = EstrutAnalisesFact(i - 1).flg_estado
        EstrutAnalisesFact(i).convencao = EstrutAnalisesFact(i - 1).convencao
        EstrutAnalisesFact(i).cor = EstrutAnalisesFact(i - 1).cor
        i = i - 1
    Wend
    
    
    EstrutAnalisesFact(FGAnalises.row).n_seq_prog = seq_fact
    EstrutAnalisesFact(FGAnalises.row).episodio = EcNumReq
    EstrutAnalisesFact(FGAnalises.row).n_ord = EstrutAnalisesFact(linha2).n_ord
    EstrutAnalisesFact(FGAnalises.row).dt_ini_real = EstrutAnalisesFact(linha2).dt_ini_real
    EstrutAnalisesFact(FGAnalises.row).cod_rubr = ""
    EstrutAnalisesFact(FGAnalises.row).cod_rubr_efr = ""
    EstrutAnalisesFact(FGAnalises.row).cod_isen_doe = EstrutAnalisesFact(linha2).cod_isen_doe
    EstrutAnalisesFact(FGAnalises.row).descr_rubr = ""
    EstrutAnalisesFact(FGAnalises.row).val_pr_unit = ""
    EstrutAnalisesFact(FGAnalises.row).val_pr_u_doe = ""
    EstrutAnalisesFact(FGAnalises.row).qtd = 1
    EstrutAnalisesFact(FGAnalises.row).perc_pag_efr = ""
    EstrutAnalisesFact(FGAnalises.row).val_total = ""
    EstrutAnalisesFact(FGAnalises.row).val_total_acto = ""
    EstrutAnalisesFact(FGAnalises.row).perc_pag_doe = ""
    EstrutAnalisesFact(FGAnalises.row).val_pag_doe = ""
    EstrutAnalisesFact(FGAnalises.row).p1 = EstrutAnalisesFact(linha2).p1
    EstrutAnalisesFact(FGAnalises.row).chave_prog = EcNumReq
    EstrutAnalisesFact(FGAnalises.row).n_ord_ins = linha
    EstrutAnalisesFact(FGAnalises.row).flg_tip_req = EstrutAnalisesFact(linha2).flg_tip_req
    EstrutAnalisesFact(FGAnalises.row).flg_estado = EstrutAnalisesFact(linha2).flg_estado
    EstrutAnalisesFact(FGAnalises.row).convencao = EstrutAnalisesFact(linha2).convencao
    EstrutAnalisesFact(FGAnalises.row).cor = EstrutAnalisesFact(linha2).cor
    
Exit Sub

TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "AcrescentaLinha"
    Exit Sub
    Resume Next
End Sub


Sub AdicionaAnalisePos(codAna As String, DeterminaPos As Boolean, Optional PosicaoAna As Integer)
    Dim sSql As String
    Dim rsAnalises As New ADODB.recordset
    Dim i As Long
    Dim seq_fact As Long
    Dim j As Integer
    
    sSql = "SELECT * FROM fa_rubr WHERE cod_rubr = " & BL_TrataStringParaBD(UCase(codAna))
    rsAnalises.CursorLocation = adUseServer
    rsAnalises.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnalises.Open sSql, gConexaoSecundaria

    If rsAnalises.RecordCount > 0 Then
        If DeterminaPos = True Then
            i = PosicaoAna
        Else
            TotalAnalisesFact = TotalAnalisesFact + 1
            ReDim Preserve EstrutAnalisesFact(TotalAnalisesFact)
            i = TotalAnalisesFact
        End If

        ' --------------------------------------
        ' GERA SEQUENCIAL PARA FACTURACAO
        ' --------------------------------------
        seq_fact = -1
        j = 1
        While seq_fact = -1 And j <= 10
            seq_fact = BL_GeraNumeroFact("SEQ_FACT")
            j = j + 1
        Wend
        
        If seq_fact = -1 Then
            Exit Sub
        End If
        
        'DADOS DA CODIFICACAO DA RUBRICA NA PORTARIA
        Dim RsEFR As New ADODB.recordset
        sSql = "SELECT x1.descr_rubr, x2.cod_rubr_efr,x2.val_taxa, x2.val_pag_doe,x2.val_pag_ent,x2.perc_pag_doe,100 - perc_pag_doe perc_pag_ent "
        sSql = sSql & " FROM fa_rubr x1, fa_pr_rubr x2 WHERE x1.cod_rubr = x2.cod_rubr and "
        sSql = sSql & " x1.cod_rubr = " & codAna & " AND portaria = " & BL_HandleNull(IF_RetornaPortariaActiva(ReqFact(ReqActual).codEfr), "-1")
        Set RsEFR = New ADODB.recordset
        RsEFR.CursorLocation = adUseServer
        RsEFR.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        RsEFR.Open sSql, gConexaoSecundaria
        If RsEFR.RecordCount < 1 Then
            Exit Sub
        End If
                
        ' ---------------------------------------------------------------------------
        ' PREENCHE A ESTRUTURA
        ' ---------------------------------------------------------------------------
        EstrutAnalisesFact(i).n_seq_prog = -1
        EstrutAnalisesFact(i).episodio = ReqFact(ReqActual).NReq
        EstrutAnalisesFact(i).n_ord = ReqFact(ReqActual).n_ord
        If i > 1 Then
            EstrutAnalisesFact(i).dt_ini_real = EstrutAnalisesFact(1).dt_ini_real
        Else
            EstrutAnalisesFact(i).dt_ini_real = Bg_DaData_ADO
        End If
        EstrutAnalisesFact(i).cod_rubr = codAna
        EstrutAnalisesFact(i).cod_rubr_efr = BL_HandleNull(RsEFR!cod_rubr_efr, "")
        EstrutAnalisesFact(i).cod_isen_doe = gTipoIsencaoNaoIsento
        EstrutAnalisesFact(i).descr_rubr = BL_HandleNull(RsEFR!descr_rubr, "")
        EstrutAnalisesFact(i).qtd = 1
        If IF_EfrFacturaPercent(ReqFact(ReqActual).codEfr) = True Then
            EstrutAnalisesFact(i).val_pr_unit = CDbl(BL_HandleNull(RsEFR!val_pag_ent, 0) + BL_HandleNull(RsEFR!val_pag_doe, 0))
            EstrutAnalisesFact(i).perc_pag_efr = CDbl(BL_HandleNull(RsEFR!perc_pag_ent, 100))
            EstrutAnalisesFact(i).val_total = CDbl(BL_HandleNull(RsEFR!val_pag_ent, 0) + BL_HandleNull(RsEFR!val_pag_doe, 0))
            EstrutAnalisesFact(i).perc_pag_doe = CDbl(BL_HandleNull(RsEFR!perc_pag_doe, 100))
            EstrutAnalisesFact(i).val_pag_doe = BL_HandleNull(RsEFR!val_Taxa, BL_HandleNull(RsEFR!val_pag_doe, 0))
            EstrutAnalisesFact(i).val_pr_u_doe = BL_HandleNull(RsEFR!val_Taxa, BL_HandleNull(RsEFR!val_pag_doe, 0))
        Else
            EstrutAnalisesFact(i).val_pr_unit = ""
            EstrutAnalisesFact(i).perc_pag_efr = ""
            EstrutAnalisesFact(i).val_total = ""
            EstrutAnalisesFact(i).perc_pag_doe = ""
            EstrutAnalisesFact(i).val_pag_doe = ""
            EstrutAnalisesFact(i).val_pr_u_doe = ""
        End If
        EstrutAnalisesFact(i).val_total_acto = IF_RetornaTaxaRubricaEntidade(EstrutAnalisesFact(i).cod_rubr, ReqFact(ReqActual).codEfr, EstrutAnalisesFact(i).dt_ini_real, _
                                               ReqFact(ReqActual).TipoUtente, ReqFact(ReqActual).Utente, EstrutAnalisesFact(i).nr_c, EstrutAnalisesFact(i).cod_rubr_efr)
        EstrutAnalisesFact(i).p1 = "1"
        EstrutAnalisesFact(i).chave_prog = ReqFact(ReqActual).NReq
        EstrutAnalisesFact(i).n_ord_ins = i
        If ReqFact(ReqActual).flg_novaArs = 0 Then
            EstrutAnalisesFact(i).flg_tip_req = "NB"
        Else
            EstrutAnalisesFact(i).flg_tip_req = "0"
        End If
        EstrutAnalisesFact(i).flg_estado = "1"
        EstrutAnalisesFact(i).cod_serv_exec = CStr(gCodServExec)
        EstrutAnalisesFact(i).flg_estado_doe = -1
        EstrutAnalisesFact(i).serie_fac_doe = ""
        EstrutAnalisesFact(i).n_Fac_doe = -1
        ' pferreira 2011.09.06
        If (ReqFact(ReqActual).flg_novaArs <> mediSim) Then
            EstrutAnalisesFact(i).nr_req_ars = ReqFact(ReqActual).NReq & EstrutAnalisesFact(i).p1
        Else
            EstrutAnalisesFact(i).nr_req_ars = EstrutAnalisesFact(i).p1
        End If
        EstrutAnalisesFact(i).flg_percentagem = ReqFact(ReqActual).FacturaPercent
        If Len(EstrutAnalisesFact(i).flg_tip_req) > 0 Then
            EstrutAnalisesFact(i).convencao = Mid(EstrutAnalisesFact(i).flg_tip_req, 1, 1)
            If EstrutAnalisesFact(i).convencao = "I" Then
                EstrutAnalisesFact(i).convencao = lCodInternacional
            ElseIf EstrutAnalisesFact(i).convencao = "D" Then
                EstrutAnalisesFact(i).convencao = lCodDomicilio
            Else
                EstrutAnalisesFact(i).convencao = lCodNormal
            End If
        End If
        If Len(EstrutAnalisesFact(i).flg_tip_req) = 2 Then
            EstrutAnalisesFact(i).cor = Mid(EstrutAnalisesFact(i).flg_tip_req, 2, 1)
        End If
        
        
        ' ---------------------------------------------------------------------------
        ' PREENCHE A GRELHA
        ' ---------------------------------------------------------------------------
        PreencheFGAnalises i


        ' ---------------------------------------------------------------------------
        ' AUMENTA GRELHA E ESTRUTURA
        ' ---------------------------------------------------------------------------
        If TotalAnalisesFact >= (NumLinhas - 1) Then
            FGAnalises.AddItem ""
        End If

        AdicionaAnalise
    End If

End Sub

Private Sub PreencheLoteFac(linha As Long)
    Dim sSql As String
    Dim rsLote As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "select serie_fac, n_fac, n_lote from fa_lin_fact where t_episodio = 'SISLAB' AND  episodio = " & BL_TrataStringParaBD(ReqFact(linha).NReq)
    rsLote.CursorLocation = adUseServer
    rsLote.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsLote.Open sSql, gConexaoSecundaria
    If rsLote.RecordCount > 0 Then
        ReqFact(linha).n_fac = BL_HandleNull(rsLote!serie_fac, "") & "/" & BL_HandleNull(rsLote!n_fac, "")
        ReqFact(linha).n_lote = BL_HandleNull(rsLote!n_lote, "")
    Else
    ReqFact(linha).n_fac = ""
    ReqFact(linha).n_lote = ""
    End If
    rsLote.Close
    Set rsLote = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "PreencheLoteFac"
    Exit Sub
    Resume Next
End Sub

Private Sub RetiraAnalise(linha As Long)
    Dim i As Integer
    Dim sSql As String
    On Error GoTo TrataErro

    sSql = "DELETE FROM fa_movi_fact WHERE episodio = " & BL_TrataStringParaBD(ReqFact(ReqActual).NReq)
    sSql = sSql & " AND n_seq_prog = " & EstrutAnalisesFact(linha).n_seq_prog
    BL_ExecutaQuery_Secundaria sSql
    
    For i = linha To TotalAnalisesFact - 1
        Troca_Linha i, i + 1
    Next
    For i = 0 To FGAnalises.Cols - 1
        FGAnalises.TextMatrix(TotalAnalisesFact, i) = ""
    Next
    TotalAnalisesFact = TotalAnalisesFact - 1
    ReDim Preserve EstrutAnalisesFact(TotalAnalisesFact)
    
Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "RetiraAnalise"
    Exit Sub
    Resume Next
End Sub


' ---------------------------------------------------------------------------

' PREENCHE A GRELHA

' ---------------------------------------------------------------------------

Private Sub PreencheFGAnalises(linha As Long)
    On Error GoTo TrataErro
    Dim cor As Long
    If EstrutAnalisesFact(linha).flg_estado = 3 Then
        cor = vermelhoClaro
    Else
        cor = vbWhite
    End If
    FGAnalises.row = linha
    
    FGAnalises.TextMatrix(linha, lColFactP1) = EstrutAnalisesFact(linha).p1
    FGAnalises.Col = lColFactP1
    FGAnalises.CellBackColor = cor
    FGAnalises.TextMatrix(linha, lColFactData) = EstrutAnalisesFact(linha).dt_ini_real
    FGAnalises.Col = lColFactData
    FGAnalises.CellBackColor = cor
    FGAnalises.TextMatrix(linha, lColFactCodRubr) = EstrutAnalisesFact(linha).cod_rubr
    FGAnalises.Col = lColFactCodRubr
    FGAnalises.CellBackColor = cor
    FGAnalises.TextMatrix(linha, lColFactDescrRubr) = EstrutAnalisesFact(linha).descr_rubr
    FGAnalises.Col = lColFactDescrRubr
    FGAnalises.CellBackColor = cor
    FGAnalises.TextMatrix(linha, lColFactCodRubrEFR) = EstrutAnalisesFact(linha).cod_rubr_efr
    FGAnalises.Col = lColFactCodRubrEFR
    FGAnalises.CellBackColor = cor
    FGAnalises.TextMatrix(linha, lColFactQTD) = EstrutAnalisesFact(linha).qtd
    FGAnalises.Col = lColFactQTD
    FGAnalises.CellBackColor = cor
    FGAnalises.TextMatrix(linha, lColFactValActo) = (BL_HandleNull((EstrutAnalisesFact(linha).val_total), 0))
    FGAnalises.Col = lColFactValActo
    FGAnalises.CellBackColor = cor
    FGAnalises.TextMatrix(linha, lColFactValEFR) = (BL_HandleNull((EstrutAnalisesFact(linha).val_pag_efr), 0))
    FGAnalises.Col = lColFactValEFR
    FGAnalises.CellBackColor = cor
    FGAnalises.TextMatrix(linha, lColFactValDoe) = (BL_HandleNull(EstrutAnalisesFact(linha).val_pag_doe, 0))
    FGAnalises.Col = lColFactValDoe
    FGAnalises.CellBackColor = cor
    FGAnalises.TextMatrix(linha, lColFactPercDoe) = EstrutAnalisesFact(linha).perc_pag_doe
    FGAnalises.Col = lColFactPercDoe
    FGAnalises.CellBackColor = cor
    'ISEN��O
    If EstrutAnalisesFact(linha).cod_isen_doe = gTipoIsencaoIsento Then
        FGAnalises.TextMatrix(linha, lColFactIsencao) = "Isento"
    Else
        FGAnalises.TextMatrix(linha, lColFactIsencao) = "N�o Isento"
    End If
    FGAnalises.Col = lColFactIsencao
    FGAnalises.CellBackColor = cor
    
    'CONVENCAO
    If EstrutAnalisesFact(linha).convencao = lCodInternacional Then
        FGAnalises.TextMatrix(linha, lColFactConvencao) = "Internacional"
    ElseIf EstrutAnalisesFact(linha).convencao = lCodDomicilio Then
        FGAnalises.TextMatrix(linha, lColFactConvencao) = "Domicilio"
    Else
        FGAnalises.TextMatrix(linha, lColFactConvencao) = "Normal"
    End If
    FGAnalises.Col = lColFactConvencao
    FGAnalises.CellBackColor = cor
    
    'COR
    If EstrutAnalisesFact(linha).cor = "V" Then
        FGAnalises.TextMatrix(linha, lColFactCor) = "Verde"
    Else
        FGAnalises.TextMatrix(linha, lColFactCor) = "Branco"
    End If
    FGAnalises.Col = lColFactCor
    FGAnalises.CellBackColor = cor

Exit Sub
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "PreencheFGAnalises"
    Exit Sub
    Resume Next
End Sub


' ---------------------------------------------------------------------------

' VERIFICA SE PODE MUDAR ENTIDADE PARA REQUISICAO - SE JA FOI FACTURADA

' ---------------------------------------------------------------------------
Private Function VeriricaPodeMudarEFR() As Boolean
    Dim sSql As String
    Dim rsLinFact As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT * FROM fa_lin_Fact WHERE episodio = " & BL_TrataStringParaBD(ReqFact(ReqActual).NReq)
    rsLinFact.CursorLocation = adUseServer
    rsLinFact.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsLinFact.Open sSql, gConexaoSecundaria
    If rsLinFact.RecordCount > 0 Then
        VeriricaPodeMudarEFR = False
    Else
        VeriricaPodeMudarEFR = True
    End If
    rsLinFact.Close
    Set rsLinFact = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "VeriricaPodeMudarEFR"
    Exit Function
    Resume Next
End Function

Private Function TotalRecibos() As String

    Dim sSql As String
    Dim rsRecibos As New ADODB.recordset
    Dim total
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_recibos WHERE n_req = " & BL_TrataStringParaBD(ReqFact(ReqActual).NReq)
    sSql = sSql & " AND cod_Efr = " & ReqFact(ReqActual).codEfr & " AND estado = 'P' "
    
    rsRecibos.CursorLocation = adUseServer
    rsRecibos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsRecibos.Open sSql, gConexao
    If rsRecibos.RecordCount > 0 Then
        total = 0
        While Not rsRecibos.EOF
            total = total + CDbl(Replace(BL_HandleNull(rsRecibos!total_pagar, ""), ".", ","))
            rsRecibos.MoveNext
        Wend
        TotalRecibos = CStr(total)
    Else
        TotalRecibos = ""
    End If
    rsRecibos.Close
    Set rsRecibos = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "TotalRecibos"
    Exit Function
    Resume Next
End Function
Private Function TrataBool(Param As Boolean) As Integer
'Emanuel Sousa 05.2009
    Dim res As Integer
    res = 0

        If (Param = True) Then
            res = 1
        Else
            res = 0
        End If

End Function
Private Function RetornaDescricaoEFR(codEfr As String) As String
'Emanuel Sousa 05.2009
    Dim res As String
    Dim i As Integer
    
    For i = 0 To TotalEntidadesEFR
        If (DescEntidades(i).codEntidade = codEfr) Then
            RetornaDescricaoEFR = DescEntidades(i).descEntidade
            Exit Function
        Else
            res = ""
        End If
    Next

End Function

Public Sub FuncaoImprimir()
'Emanuel Sousa 05.2009
    Dim sSql As String
    Dim i, j As Integer
    Dim continua As Boolean
    Dim NomeEFR As String
     
    sSql = "DELETE FROM SL_CR_CONFIRMACAO_FACTURACAO WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    
    Preenche_Entidades_EFR

    For i = 1 To TotalRequisicoes
    
        'Actualiza a estrutura das analises para a requisi��o iterada
        
        PreencheEstrutAnalises (ReqFact(i).NReq), True
        
        For j = 1 To TotalAnalisesFact
 
            '********************* SQL campos a inserir *********************
    
            sSql = "INSERT INTO SL_CR_CONFIRMACAO_FACTURACAO (n_ord_req, t_episodio_req, "
            sSql = sSql & "cod_empresa_req, nreq_req, user_cri_req, hr_chega_req, n_benef_req, "
            sSql = sSql & "estadofact_req, codurbano_req, km_req, codefr_req, nomeutente_req, "
            sSql = sSql & "utente_req, tipoutente_req, dt_cri_req, n_lote_req, n_fac_req, "
            sSql = sSql & "totalrecibos_req, facturapercent_req, episodio_ana, n_seq_prog_ana, n_ord_ana, "
            sSql = sSql & "dt_ini_real_ana, cod_rubr_ana, cod_rubr_efr_ana, descr_rubr_ana, val_pr_unit_ana, "
            sSql = sSql & "val_pag_efr, qtd_ana, val_total_acto_ana, val_total_ana, perc_pag_efr_ana, perc_pag_doe_ana, "
            sSql = sSql & "val_pr_u_doe_ana, val_pag_doe_ana, cod_isen_doe_ana, flg_pagou_taxa_ana, "
            sSql = sSql & "nr_req_ars_ana, flg_estado_ana, chave_prog_ana, n_ord_ins_ana, "
            sSql = sSql & "flg_tip_req_ana, p1_ana, convencao_ana, cor_ana, "
            sSql = sSql & "cod_serv_exec_ana, flg_estado_doe_ana, serie_fac_doe_ana, n_fac_doe_ana, nome_computador, desc_efr, nr_c) VALUES ("
            
            '**************** SQL valores dos campos a inserir Requisicao ***************
            
            sSql = sSql & ReqFact(i).n_ord & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).t_episodio) & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).cod_empresa) & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).NReq) & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).user_cri) & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).hr_chega) & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).n_benef) & ", "
            sSql = sSql & ReqFact(i).estadoFact & ", "
            sSql = sSql & ReqFact(i).codUrbano & ", "
            sSql = sSql & ReqFact(i).km & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).codEfr) & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).NomeUtente) & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).Utente) & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).TipoUtente) & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).dt_cri) & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).n_lote) & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).n_fac) & ", "
            sSql = sSql & BL_TrataStringParaBD(ReqFact(i).TotalRecibos) & ", "
            sSql = sSql & TrataBool(ReqFact(i).FacturaPercent) & ", "
            
            '**************** SQL valores dos campos a inserir Analises ***************
            
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).episodio) & ", "
            sSql = sSql & EstrutAnalisesFact(j).n_seq_prog & ", "
            sSql = sSql & EstrutAnalisesFact(j).n_ord & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).dt_ini_real) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).cod_rubr) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).cod_rubr_efr) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).descr_rubr) & ", "
            sSql = sSql & BL_HandleNull(Replace((EstrutAnalisesFact(j).val_pr_unit), ",", "."), 0) & ", "
            sSql = sSql & BL_HandleNull(Replace((EstrutAnalisesFact(j).val_pag_efr), ",", "."), 0) & ", "
            sSql = sSql & EstrutAnalisesFact(j).qtd & ", "
            sSql = sSql & BL_HandleNull(Replace(EstrutAnalisesFact(j).val_total_acto, ",", "."), 0) & ", "
            sSql = sSql & BL_HandleNull(Replace(EstrutAnalisesFact(j).val_total, ",", "."), 0) & ", "
            sSql = sSql & BL_HandleNull(Replace(EstrutAnalisesFact(j).perc_pag_efr, ",", "."), 0) & ", "
            sSql = sSql & BL_HandleNull(Replace(EstrutAnalisesFact(j).perc_pag_doe, ",", "."), 0) & ", "
            sSql = sSql & BL_HandleNull(Replace(EstrutAnalisesFact(j).val_pr_u_doe, ",", "."), 0) & ", "
            sSql = sSql & BL_HandleNull(Replace(EstrutAnalisesFact(j).val_pag_doe, ",", "."), 0) & ", "
            sSql = sSql & EstrutAnalisesFact(j).cod_isen_doe & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).flg_pagou_taxa) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).nr_req_ars) & ", "
            sSql = sSql & EstrutAnalisesFact(j).flg_estado & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).chave_prog) & ", "
            sSql = sSql & EstrutAnalisesFact(j).n_ord_ins & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).flg_tip_req) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).p1) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).convencao) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).cor) & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).cod_serv_exec) & ", "
            sSql = sSql & EstrutAnalisesFact(j).flg_estado_doe & ", "
            sSql = sSql & BL_TrataStringParaBD(EstrutAnalisesFact(j).serie_fac_doe) & ", "
            sSql = sSql & EstrutAnalisesFact(j).n_Fac_doe & ", "
            
            '**************** SQL para gravar o computador q pediu a listagem ***************
            
            sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
            sSql = sSql & BL_TrataStringParaBD(RetornaDescricaoEFR(ReqFact(i).codEfr)) & ", "
            sSql = sSql & BL_HandleNull(Replace(EstrutAnalisesFact(j).nr_c, ",", "."), 0) & ") "
    
            BG_ExecutaQuery_ADO sSql
            BG_Trata_BDErro
        Next
        j = 0
    Next
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("ListagemConfirmacaoFacturacao", "Listagem de Confirma��o de Factura��o", crptToPrinter)
    Else
        continua = BL_IniciaReport("ListagemConfirmacaoFacturacao", "Listagem de Confirma��o de Factura��o", crptToWindow)
    End If
    
    If continua = False Then Exit Sub
        Dim ReportDetalhado As CrystalReport
        Set ReportDetalhado = forms(0).Controls("Report")
                
           ReportDetalhado.SQLQuery = "SELECT CODEFR_REQ," & _
                                      "     COD_EMPRESA_REQ, " & _
                                      "     NREQ_REQ, " & _
                                      "     N_BENEF_REQ, " & _
                                      "     NOMEUTENTE_REQ, " & _
                                      "     COD_RUBR_ANA, " & _
                                      "     DESCR_RUBR_ANA, " & _
                                      "     QTD_ANA, " & _
                                      "     VAL_TOTAL_ACTO_ANA, " & _
                                      "     VAL_PAG_DOE_ANA " & _
                                      "FROM " & _
                                      "     SL_CR_CONFIRMACAO_FACTURACAO " & _
                                      "WHERE " & _
                                      "     NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador) & _
                                      " ORDER BY CODEFR_REQ, NREQ_REQ,N_ORD_INS_ANA "
                                      
            'F�rmulas do Report
            ReportDetalhado.formulas(1) = "DataInicioListagem=" & BL_TrataDataParaBD(BL_HandleNull(dataInicialReport, EcDataInicial))
            ReportDetalhado.formulas(2) = "DataFimListagem=" & BL_TrataDataParaBD(BL_HandleNull(dataFinalReport, EcDataFinal))
                    
            'Pergunta se pretende imprimir o relat�rio detalhado ou resumido
            If BG_Mensagem(mediMsgBox, "Ser� impressa a listagem detalhada!" & vbNewLine & "Deseja imprimir apenas a resumida?", vbQuestion + vbYesNo + vbDefaultButton2, App.ProductName) = vbNo Then
                ReportDetalhado.formulas(3) = "SuprimeDetalhe=" & BL_TrataStringParaBD("False")
            'Neste caso ir� imprimir o report resumido
            Else
                ReportDetalhado.formulas(3) = "SuprimeDetalhe=" & BL_TrataStringParaBD("True")
            End If
            
            Me.SetFocus
            
            Call BL_ExecutaReport
            '        'Report.PageShow Report.ReportLatestPage
            '        Report.PageZoom (100)

    sSql = "DELETE FROM SL_CR_CONFIRMACAO_FACTURACAO WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    
End Sub

' --------------------------------------------------------------------------------------------

' VERIFICA SE P1 tem mais que 6 analises

' --------------------------------------------------------------------------------------------
Private Function VerificaTotalAnaCred() As Boolean
    Dim i As Integer
    Dim credAct As String
    Dim totalAna As Integer
    VerificaTotalAnaCred = False
    totalAna = 0
    credAct = ""
    For i = 1 To TotalAnalisesFact
        If ReqFact(ReqActual).flg_novaArs = mediSim Then
            If credAct <> EstrutAnalisesFact(i).p1 Then
                credAct = EstrutAnalisesFact(i).p1
                totalAna = 0
            End If
            totalAna = totalAna + 1
            If totalAna > 6 Then
                VerificaTotalAnaCred = False
                Exit Function
            End If
        End If
    Next
    VerificaTotalAnaCred = True
Exit Function
TrataErro:
     VerificaTotalAnaCred = False
    BG_LogFile_Erros "Erro ao VerificaTotalAnaCred : " & " " & Err.Number & " - " & Err.Description, Me.Name, "VerificaTotalAnaCred", True
    Exit Function
    Resume Next
End Function


VERSION 5.00
Begin VB.Form FormEstatMicrorgTempo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   8775
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7950
   Icon            =   "FormEstatMicrorgTempo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8775
   ScaleWidth      =   7950
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcIntervaloTempo 
      Height          =   285
      Left            =   1440
      TabIndex        =   71
      Top             =   10680
      Width           =   495
   End
   Begin VB.TextBox EcErrosMajor 
      Height          =   225
      Left            =   1320
      TabIndex        =   68
      Top             =   11040
      Width           =   375
   End
   Begin VB.TextBox EcErrosMinor 
      Height          =   225
      Left            =   1320
      TabIndex        =   67
      Top             =   10800
      Width           =   375
   End
   Begin VB.Frame Frame4 
      Height          =   1215
      Left            =   120
      TabIndex        =   48
      Top             =   7440
      Width           =   7695
      Begin VB.CheckBox Flg_DescrRequis 
         Caption         =   "Descriminar Requisi��es e Utentes"
         Height          =   195
         Left            =   120
         TabIndex        =   56
         Top             =   600
         Width           =   2895
      End
      Begin VB.CheckBox Flg_ApresGrafico 
         Caption         =   "Apresentar gr�fico"
         Height          =   195
         Left            =   5520
         TabIndex        =   55
         Top             =   360
         Visible         =   0   'False
         Width           =   2055
      End
      Begin VB.CheckBox Flg_OrderProd 
         Caption         =   "Agrupar por Produto"
         Height          =   195
         Left            =   120
         TabIndex        =   54
         Top             =   840
         Width           =   1815
      End
      Begin VB.CheckBox Flg_OrderProven 
         Caption         =   "Agrupar por Proveni�ncia"
         Height          =   195
         Left            =   3000
         TabIndex        =   53
         Top             =   360
         Width           =   2295
      End
      Begin VB.CheckBox Flg_DescrAntibio 
         Caption         =   "Descriminar Antibi�ticos"
         Height          =   195
         Left            =   120
         TabIndex        =   52
         Top             =   360
         Width           =   2055
      End
      Begin VB.CheckBox CkAgruparResistentes 
         Caption         =   "Agrupar Resist�ncias "
         Height          =   195
         Left            =   3000
         TabIndex        =   51
         Top             =   600
         Width           =   2175
      End
      Begin VB.CheckBox CkContarSemFact 
         Caption         =   "Contar S/Fact"
         Height          =   255
         Left            =   5520
         TabIndex        =   50
         Top             =   600
         Width           =   1935
      End
      Begin VB.TextBox EcUtente 
         Height          =   285
         Left            =   3000
         TabIndex        =   49
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label8 
         Caption         =   "Utente"
         Height          =   255
         Index           =   1
         Left            =   3840
         TabIndex        =   57
         Top             =   840
         Width           =   855
      End
   End
   Begin VB.TextBox EcPesqRapGrupo 
      Height          =   285
      Left            =   4680
      TabIndex        =   42
      Top             =   9840
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Height          =   5895
      Left            =   120
      TabIndex        =   20
      Top             =   1440
      Width           =   7695
      Begin VB.CommandButton BtPesquisaCarac 
         Height          =   315
         Left            =   6120
         Picture         =   "FormEstatMicrorgTempo.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   76
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Caracteristicas Micro"
         Top             =   3960
         Width           =   375
      End
      Begin VB.ListBox EcListaCarac 
         Height          =   645
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   75
         Top             =   3960
         Width           =   4545
      End
      Begin VB.CheckBox CkRestringeAntibio 
         Caption         =   "Restringir Pesquisa"
         Height          =   375
         Left            =   6600
         TabIndex        =   47
         Top             =   4680
         Width           =   975
      End
      Begin VB.CommandButton BtPesquisaSituacao 
         Height          =   315
         Left            =   6120
         Picture         =   "FormEstatMicrorgTempo.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   46
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos "
         Top             =   1200
         Width           =   375
      End
      Begin VB.ListBox ListaSituacao 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   6
         Top             =   1200
         Width           =   4545
      End
      Begin VB.ListBox ListaPerfis 
         Height          =   645
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   12
         Top             =   2760
         Width           =   4560
      End
      Begin VB.CommandButton BtPesquisaPerfis 
         Height          =   315
         Left            =   6120
         Picture         =   "FormEstatMicrorgTempo.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   43
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   2760
         Width           =   375
      End
      Begin VB.ListBox ListaMicrorg 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   9
         Top             =   3480
         Width           =   4545
      End
      Begin VB.ListBox ListaProven 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   8
         Top             =   2280
         Width           =   4545
      End
      Begin VB.ListBox ListaProduto 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   7
         Top             =   1800
         Width           =   4545
      End
      Begin VB.ComboBox CbSensib 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   5400
         Width           =   1455
      End
      Begin VB.CommandButton BtPesquisaAntibio 
         Height          =   315
         Left            =   6120
         Picture         =   "FormEstatMicrorgTempo.frx":10AA
         Style           =   1  'Graphical
         TabIndex        =   32
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Antibi�ticos"
         Top             =   4680
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaMicro 
         Height          =   315
         Left            =   6120
         Picture         =   "FormEstatMicrorgTempo.frx":1634
         Style           =   1  'Graphical
         TabIndex        =   31
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Microrganismos"
         Top             =   3480
         Width           =   375
      End
      Begin VB.TextBox EcDescrMicro 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   3480
         Width           =   3495
      End
      Begin VB.TextBox EcCodMicro 
         Height          =   315
         Left            =   1560
         TabIndex        =   29
         Top             =   3480
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   6120
         Picture         =   "FormEstatMicrorgTempo.frx":1BBE
         Style           =   1  'Graphical
         TabIndex        =   28
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncias "
         Top             =   2280
         Width           =   375
      End
      Begin VB.TextBox EcDescrProveniencia 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   2400
         Width           =   3495
      End
      Begin VB.TextBox EcCodProveniencia 
         Height          =   315
         Left            =   1560
         TabIndex        =   26
         Top             =   2400
         Width           =   735
      End
      Begin VB.ComboBox CbUrgencia 
         Height          =   315
         Left            =   4200
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   285
         Width           =   2295
      End
      Begin VB.ComboBox EcDescrSexo 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   285
         Width           =   1455
      End
      Begin VB.CommandButton BtPesquisaProduto 
         Height          =   315
         Left            =   6120
         Picture         =   "FormEstatMicrorgTempo.frx":2148
         Style           =   1  'Graphical
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos "
         Top             =   1800
         Width           =   375
      End
      Begin VB.TextBox EcDescrProduto 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   1920
         Width           =   3735
      End
      Begin VB.TextBox EcCodProduto 
         Height          =   315
         Left            =   1560
         TabIndex        =   23
         Top             =   1920
         Width           =   735
      End
      Begin VB.ListBox ListaAntibio 
         Height          =   450
         Left            =   1560
         MultiSelect     =   2  'Extended
         TabIndex        =   10
         Top             =   4680
         Width           =   4545
      End
      Begin VB.TextBox EcCodGrupo 
         Height          =   315
         Left            =   1560
         TabIndex        =   5
         Top             =   840
         Width           =   735
      End
      Begin VB.TextBox EcDescrGrupo 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   840
         Width           =   3855
      End
      Begin VB.CommandButton BtPesquisaGrupo 
         Height          =   315
         Left            =   6120
         Picture         =   "FormEstatMicrorgTempo.frx":26D2
         Style           =   1  'Graphical
         TabIndex        =   21
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Produtos "
         Top             =   840
         Width           =   375
      End
      Begin VB.CheckBox CkAgruparMicrorg 
         Caption         =   "Agrupar "
         Height          =   255
         Left            =   6600
         TabIndex        =   45
         Top             =   3480
         Width           =   855
      End
      Begin VB.Label Label12 
         Caption         =   "Caracteristicas"
         Height          =   255
         Left            =   120
         TabIndex        =   77
         Top             =   3960
         Width           =   1215
      End
      Begin VB.Label Label7 
         Caption         =   "&Exames"
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Top             =   2760
         Width           =   1215
      End
      Begin VB.Label LaSensib 
         Caption         =   "Sensibilidade"
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   5400
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "&Microrganismo"
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   3480
         Width           =   1215
      End
      Begin VB.Label Label9 
         Caption         =   "&Proveni�ncia"
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "&Situa��o"
         Height          =   255
         Left            =   120
         TabIndex        =   38
         Top             =   1200
         Width           =   735
      End
      Begin VB.Label Label6 
         Caption         =   "Prio&ridade"
         Height          =   255
         Left            =   3240
         TabIndex        =   37
         Top             =   285
         Width           =   735
      End
      Begin VB.Label LbSexo 
         AutoSize        =   -1  'True
         Caption         =   "Se&xo"
         Height          =   195
         Left            =   120
         TabIndex        =   36
         Top             =   285
         Width           =   360
      End
      Begin VB.Label Label1 
         Caption         =   "Pro&duto"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   35
         Top             =   1800
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "&Grupo An�lises"
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label LaAntibio 
         Caption         =   "&Antibi�tico"
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   4680
         Width           =   1215
      End
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   15
      Top             =   0
      Width           =   7695
      Begin VB.OptionButton Option1 
         Caption         =   "Dt. Chegada"
         Height          =   255
         Index           =   0
         Left            =   4800
         TabIndex        =   66
         Top             =   240
         Width           =   1335
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Dt. Valida��o"
         Height          =   255
         Index           =   1
         Left            =   4800
         TabIndex        =   65
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox EcDtFim 
         Height          =   285
         Left            =   3600
         TabIndex        =   2
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox EcDtIni 
         Height          =   285
         Left            =   2040
         TabIndex        =   1
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton BtReportLista 
         Caption         =   "Lista"
         Height          =   725
         Left            =   6960
         Picture         =   "FormEstatMicrorgTempo.frx":2C5C
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   120
         Width           =   735
      End
      Begin VB.CommandButton BtReportTabela 
         Caption         =   "Tabela"
         Height          =   725
         Left            =   6240
         Picture         =   "FormEstatMicrorgTempo.frx":2DA6
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   120
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Da Data "
         Height          =   255
         Index           =   0
         Left            =   1200
         TabIndex        =   19
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Index           =   0
         Left            =   3330
         TabIndex        =   18
         Top             =   240
         Width           =   255
      End
   End
   Begin VB.TextBox EcPesqRapProduto 
      Height          =   285
      Left            =   600
      TabIndex        =   14
      Top             =   9840
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapMicro 
      Height          =   285
      Left            =   2040
      TabIndex        =   13
      Top             =   9840
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapAntibio 
      Height          =   285
      Left            =   3360
      TabIndex        =   0
      Top             =   9840
      Width           =   975
   End
   Begin VB.Frame Frame2 
      Height          =   495
      Left            =   120
      TabIndex        =   58
      Top             =   840
      Width           =   7695
      Begin VB.ComboBox CbLocal 
         Height          =   315
         Left            =   5280
         Style           =   2  'Dropdown List
         TabIndex        =   73
         Top             =   140
         Width           =   2295
      End
      Begin VB.OptionButton Opt1 
         Caption         =   "Dias"
         Height          =   195
         Index           =   1
         Left            =   2640
         TabIndex        =   62
         Top             =   200
         Width           =   615
      End
      Begin VB.OptionButton Opt1 
         Caption         =   "Anos"
         Height          =   195
         Index           =   0
         Left            =   1920
         TabIndex        =   61
         Top             =   200
         Width           =   735
      End
      Begin VB.TextBox EcIdadeIni 
         Height          =   285
         Left            =   720
         TabIndex        =   60
         Top             =   160
         Width           =   375
      End
      Begin VB.TextBox EcIdadeFim 
         Height          =   285
         Left            =   1440
         TabIndex        =   59
         Top             =   160
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "Local"
         Height          =   195
         Index           =   0
         Left            =   4800
         TabIndex        =   74
         Top             =   200
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "a"
         Height          =   255
         Index           =   1
         Left            =   1170
         TabIndex        =   64
         Top             =   195
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "Idade"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   63
         Top             =   200
         Width           =   735
      End
   End
   Begin VB.Label Label11 
      Caption         =   "Intervalo dupl(dias)"
      Height          =   255
      Left            =   0
      TabIndex        =   72
      Top             =   10680
      Width           =   1455
   End
   Begin VB.Label Label8 
      Caption         =   "N� Erros Major"
      Height          =   255
      Index           =   2
      Left            =   1920
      TabIndex        =   70
      Top             =   11040
      Width           =   1215
   End
   Begin VB.Label Label8 
      Caption         =   "N� Erros Minor"
      Height          =   255
      Index           =   0
      Left            =   1920
      TabIndex        =   69
      Top             =   10800
      Width           =   1215
   End
End
Attribute VB_Name = "FormEstatMicrorgTempo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 03/09/2002
' T�cnico

' Vari�veis Globais para este Form.

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object
Dim ListaOrigem As Control
Dim ListaDestino As Control

Public rs As ADODB.recordset

' -----------------------------------------------------------------------
' ESTRUTURAS USADAS PARA COMPARAR MICRORGANISMOS
' -----------------------------------------------------------------------
Private Type reqAux
    NReq() As String
    totalReq As Long
End Type

Private Type MicroUtente
    cod_micro As String
    numMicro As Integer
    numInfecoes As Integer
    requisicoes() As reqAux
    totalReq As Integer
    SemFac As Integer
End Type

Private Type Antibiotico
    cod_antib As String
    descr_antib As String
    abrev_antib As String
    res_sensib As String
End Type

Private Type Microrganismo
    seq_realiza As String
    cod_micro As String
    descr_micro As String
    id_prova As String
    N_Res As String
    flg_tsq As String
    descr_produto As String
    cod_carac_micro As Integer
    descr_carac_micro As String
    TotalAntibioticos As Long
    EstrutAntib() As Antibiotico
End Type

Private Type requisicao
    n_req As String
    dt_chega As String
    descr_proven As String
    
    totalMicro As Long
    EstrutMicro() As Microrganismo
End Type


Private Type Utente
    seq_utente As String
    nome_ute As String
    Utente As String
    
    totalReq As Long
    EstrutReq() As requisicao
    
    TotalMiUte As Long
    EstrutMicroUte() As MicroUtente
End Type

Dim EstrutUte() As Utente
Dim TotalUte As Long
' -----------------------------------------------------------------------

'Estruturas utilizadas para gerar as etiquetas
Private Type Antibiograma
    seq_utente As String
    cod_micro As String
    cod_antib As String
    Sensib As String
End Type
Dim antib() As Antibiograma

Sub Preenche_Estatistica(TipoReport As String)
    
    Dim sql As String
    Dim continua As Boolean
    Dim i As Integer
    Dim totalMicro As Long
    Dim TotalDupl As Long
    Dim TotalSemFact As Long
    Dim TotalNaoDupl As Long
    Dim idxU As Long
    Dim idxM As Long
    Dim antibios As String
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Estat�stica de Microrganismos") = True Then Exit Sub
    
    '2. Verifica Campos Obrigat�rios
    If EcDtIni.text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If
    If EcDtFim.text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da chegada dos produtos das an�lises.", vbOKOnly + vbExclamation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    'Printer Common Dialog
    gImprimirDestino = 0
    If gImprimirDestino = 1 Then
        If TipoReport = "Lista" Then
            If Flg_DescrRequis.value = 1 And Flg_DescrAntibio.value = 0 Then
                continua = BL_IniciaReport("EstatisticaMicrorg_OrderRequis", "Estat�stica de Microrganismos", crptToPrinter)
            Else
                continua = BL_IniciaReport("EstatisticaMicrorg", "Estat�stica de Microrganismos", crptToPrinter)
            End If
        ElseIf TipoReport = "Tabela" Then
            continua = BL_IniciaReport("EstatisticaMicrorg_Tabela", "Estat�stica de Microrganismos", crptToPrinter)
        End If
    Else
        If TipoReport = "Lista" Then
            If Flg_DescrRequis.value = 1 And Flg_DescrAntibio.value = 0 Then
                continua = BL_IniciaReport("EstatisticaMicrorg_OrderRequis", "Estat�stica de Microrganismos", crptToWindow)
            Else
                continua = BL_IniciaReport("EstatisticaMicrorg_new", "Estat�stica de Microrganismos", crptToWindow)
            End If
        ElseIf TipoReport = "Tabela" Then
            continua = BL_IniciaReport("EstatisticaMicrorg_Tabela", "Estat�stica de Microrganismos", crptToWindow)
        End If
    End If
    If continua = False Then Exit Sub
        
    'Call Cria_TmpRec_Estatistica
    
     PreencheTabelaTemporariaNEW
    totalMicro = 0
    TotalSemFact = 0
    TotalDupl = 0
    TotalNaoDupl = 0
    For idxU = 1 To TotalUte
        For idxM = 1 To EstrutUte(idxU).TotalMiUte
            totalMicro = totalMicro + EstrutUte(idxU).EstrutMicroUte(idxM).numMicro
            TotalNaoDupl = TotalNaoDupl + EstrutUte(idxU).EstrutMicroUte(idxM).numInfecoes
            TotalSemFact = TotalSemFact + EstrutUte(idxU).EstrutMicroUte(idxM).SemFac
            TotalDupl = totalMicro - TotalNaoDupl
            'Debug.Print EstrutUte(idxU).seq_utente & " - "; EstrutUte(idxU).EstrutMicroUte(idxM).cod_micro & " - " & totalMicro & " - " & TotalDupl
        Next
    Next
    If totalMicro = 0 Then
        BG_Mensagem mediMsgBox, "N�o existem registos para abrir a estat�stica!", vbInformation, Me.caption
        Exit Sub
    End If
    
    antibios = ";"
    For i = 0 To ListaAntibio.ListCount - 1
        antibios = antibios & BL_SelCodigo("SL_ANTIBIO", "COD_ANTIBIO", "SEQ_ANTIBIO", ListaAntibio.ItemData(i), "V") & ";"
    Next
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    If TipoReport = "Lista" Then
        Report.SQLQuery = "SELECT SL_CR_ESTATMICRORGDET.DESCR_MICRORG, SL_CR_ESTATMICRORGDET.DESCR_ANTIBIO, SL_CR_ESTATMICRORGDET.RES_SENSIB, SL_CR_ESTATMICRORGDET.DESCR_PROVEN, SL_CR_ESTATMICRORGDET.DESCR_PRODUTO, SL_CR_ESTATMICRORGDET.N_REQ " & _
            " FROM SL_CR_ESTATMICRORGDET  WHERE SL_CR_ESTATMICRORGDET.nome_computador =  " & BL_TrataStringParaBD(BG_SYS_GetComputerName)

    ElseIf TipoReport = "Tabela" Then
        Report.SQLQuery = "SELECT SL_CR_ESTATMICRORGDET.DESCR_MICRORG, SL_CR_ESTATMICRORGDET.DESCR_ANTIBIO, SL_CR_ESTATMICRORGDET.RES_SENSIB, SL_CR_ESTATMICRORGDET.DESCR_PROVEN, SL_CR_ESTATMICRORGDET.DESCR_PRODUTO, SL_CR_ESTATMICRORGDET.N_REQ " & _
            " FROM SL_CR_ESTATMICRORGDET WHERE SL_CR_ESTATMICRORGDET.nome_computador =  " & BL_TrataStringParaBD(BG_SYS_GetComputerName)
    End If
    If Flg_DescrRequis.value = 1 And Flg_DescrAntibio.value = 0 Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY SL_CR_ESTATMICRORGDET.DESCR_PROVEN,SL_CR_ESTATMICRORGDET.DESCR_PRODUTO,SL_CR_ESTATMICRORGDET.DESCR_MICRORG, SL_CR_ESTATMICRORGDET.N_REQ, SL_CR_ESTATMICRORGDET.DESCR_ANTIBIO, SL_CR_ESTATMICRORGDET.RES_SENSIB  "
    ElseIf Flg_DescrRequis.value = 0 And ListaAntibio.ListCount = 0 And Flg_DescrAntibio.value = 1 Then
        Report.SQLQuery = Report.SQLQuery & " ORDER BY SL_CR_ESTATMICRORGDET.DESCR_PROVEN,SL_CR_ESTATMICRORGDET.DESCR_PRODUTO,SL_CR_ESTATMICRORGDET.DESCR_MICRORG, SL_CR_ESTATMICRORGDET.DESCR_ANTIBIO,SL_CR_ESTATMICRORGDET.RES_SENSIB, SL_CR_ESTATMICRORGDET.N_REQ "
    Else
        Report.SQLQuery = Report.SQLQuery & " ORDER BY SL_CR_ESTATMICRORGDET.DESCR_PROVEN,SL_CR_ESTATMICRORGDET.DESCR_PRODUTO,DESCR_MICRORG, SL_CR_ESTATMICRORGDET.DESCR_ANTIBIO,RES_SENSIB, SL_CR_ESTATMICRORGDET.N_REQ "
    End If
    
    

    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDtIni.text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDtFim.text)
    
    Dim StrProven As String
    For i = 0 To ListaProven.ListCount - 1
        StrProven = StrProven & ListaProven.List(i) & "; "
    Next i
    If StrProven <> "" Then
        StrProven = Mid(StrProven, 1, Len(StrProven) - 2)
        If Len(StrProven) > 250 Then StrProven = Mid(StrProven, 1, 240) & " ..."
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("" & StrProven)
    Else
        Report.formulas(3) = "Proveniencia=" & BL_TrataStringParaBD("Todas")
    End If
    
    Dim STRProduto As String
    For i = 0 To ListaProduto.ListCount - 1
        STRProduto = STRProduto & ListaProduto.List(i) & "; "
    Next i
    If STRProduto <> "" Then
        STRProduto = Mid(STRProduto, 1, Len(STRProduto) - 2)
        If Len(STRProduto) > 250 Then STRProduto = Mid(STRProduto, 1, 240) & " ..."
        Report.formulas(4) = "Produto=" & BL_TrataStringParaBD("" & STRProduto)
    Else
        Report.formulas(4) = "Produto=" & BL_TrataStringParaBD("Todos")
    End If
    
    If CbUrgencia.ListIndex = -1 Then
        Report.formulas(5) = "Urgencia=" & BL_TrataStringParaBD("-")
    Else
        Report.formulas(5) = "Urgencia=" & BL_TrataStringParaBD("" & CbUrgencia.text)
    End If
    
    Dim strSituacao As String
    For i = 0 To ListaSituacao.ListCount - 1
        strSituacao = strSituacao & ListaSituacao.List(i) & "; "
    Next i
    If strSituacao <> "" Then
        strSituacao = Mid(strSituacao, 1, Len(strSituacao) - 2)
        If Len(strSituacao) > 250 Then strSituacao = Mid(strSituacao, 1, 240) & " ..."
        Report.formulas(6) = "Situacao=" & BL_TrataStringParaBD("" & strSituacao)
    Else
        Report.formulas(6) = "Situacao=" & BL_TrataStringParaBD("Todos")
    End If
    
    If EcDescrSexo.ListIndex = -1 Then
        Report.formulas(7) = "Sexo=" & BL_TrataStringParaBD("Todos")
    Else
        Report.formulas(7) = "Sexo=" & BL_TrataStringParaBD("" & EcDescrSexo.text)
    End If
    
    If Flg_DescrAntibio.value = 1 Then
        Report.formulas(8) = "DescrAntibio=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(8) = "DescrAntibio=" & BL_TrataStringParaBD("N")
    End If

    Dim StrMicrorg As String
    For i = 0 To ListaMicrorg.ListCount - 1
        StrMicrorg = StrMicrorg & ListaMicrorg.List(i) & "; "
    Next i
    If StrMicrorg <> "" Then
        StrMicrorg = Mid(StrMicrorg, 1, Len(StrMicrorg) - 2)
        If Len(StrMicrorg) > 250 Then StrMicrorg = Mid(StrMicrorg, 1, 240) & " ..."
        Report.formulas(9) = "Microrganismo=" & BL_TrataStringParaBD("" & StrMicrorg)
    Else
        Report.formulas(9) = "Microrganismo=" & BL_TrataStringParaBD("Todos")
    End If
    
    If Flg_OrderProd.value = 1 Then
        Report.formulas(10) = "OrderProd=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(10) = "OrderProd=" & BL_TrataStringParaBD("N")
    End If
    If Flg_OrderProven.value = 1 Then
        Report.formulas(11) = "OrderProven=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(11) = "OrderProven=" & BL_TrataStringParaBD("N")
    End If
    If Flg_ApresGrafico.value = 1 Then
        Report.formulas(12) = "ApresGrafico=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(12) = "ApresGrafico=" & BL_TrataStringParaBD("N")
    End If
    If Flg_DescrRequis.value = 1 Then
        Report.formulas(13) = "DescrRequis=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(13) = "DescrRequis=" & BL_TrataStringParaBD("N")
    End If
    'Report.Formulas(14) = "TotalMicro=" & BL_TrataStringParaBD("" & TotalMicro)
    If CkAgruparResistentes.value = vbChecked Then
        Report.formulas(15) = "AgruparResistentes='S'"
    Else
        Report.formulas(15) = "AgruparResistentes='N'"
    End If
    Report.formulas(16) = "IntervaloDuplicados=" & BL_TrataStringParaBD("" & EcIntervaloTempo.text)
    
    If EcCodGrupo.text <> "" Then
        Report.formulas(17) = "GrupoAnalises=" & BL_TrataStringParaBD("" & EcDescrGrupo.text)
    Else
        Report.formulas(17) = "GrupoAnalises=" & BL_TrataStringParaBD("Todos")
    End If
    
    Dim StrExames As String
    For i = 0 To ListaPerfis.ListCount - 1
        StrExames = StrExames & ListaPerfis.List(i) & "; "
    Next i
    If StrExames <> "" Then
        StrExames = Mid(StrExames, 1, Len(StrExames) - 2)
        If Len(StrExames) > 250 Then StrExames = Mid(StrExames, 1, 240) & " ..."
        Report.formulas(18) = "Exame=" & BL_TrataStringParaBD("" & StrExames)
    Else
        Report.formulas(18) = "Exame=" & BL_TrataStringParaBD("Todos")
    End If
    
    Dim StrAntib As String
    For i = 0 To ListaAntibio.ListCount - 1
        StrAntib = StrAntib & ListaAntibio.List(i) & "; "
    Next i
    If StrAntib <> "" Then
        StrAntib = Mid(StrAntib, 1, Len(StrAntib) - 2)
        If Len(StrAntib) > 250 Then StrAntib = Mid(StrAntib, 1, 240) & " ..."
        Report.formulas(19) = "Antibiotico=" & BL_TrataStringParaBD("" & StrAntib)
    Else
        Report.formulas(19) = "Antibiotico=" & BL_TrataStringParaBD("Todos")
    End If
    
    
    If CbSensib.ListIndex <> mediComboValorNull Then
        Report.formulas(20) = "ResSensib=" & BL_TrataStringParaBD(UCase(Mid(CbSensib.text, 1, 1)))
    Else
        Report.formulas(20) = "ResSensib='-'"
    End If
    Report.formulas(21) = "TotalMicro=" & BL_TrataStringParaBD(CStr(totalMicro))
    Report.formulas(22) = "TotalDupl=" & BL_TrataStringParaBD(CStr(TotalDupl))
    Report.formulas(23) = "TotalNaoDupl=" & BL_TrataStringParaBD(CStr(TotalNaoDupl))
    Report.formulas(24) = "Utente=" & BL_TrataStringParaBD(CStr(EcUtente))
    Report.formulas(25) = "ErrosMinor=" & BL_TrataStringParaBD(CStr(EcErrosMinor))
    
    If CkContarSemFact.value = vbChecked Then
        Report.formulas(26) = "ContaSemFac=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(26) = "ContaSemFac=" & BL_TrataStringParaBD("N")
    End If
    Report.formulas(27) = "ErrosMajor=" & BL_TrataStringParaBD(CStr(EcErrosMajor))
    Report.formulas(28) = "TotalSemFact=" & BL_TrataStringParaBD(CStr(TotalSemFact))
    Report.formulas(29) = "Idade=" & "'Entre " & CStr(EcIdadeIni) & " e " & CStr(EcIdadeFim) & " anos'"
    Report.formulas(30) = "Antibios=" & BL_TrataStringParaBD(Mid(antibios, 1, 250))
    
    StrAntib = ""
    For i = 0 To EcListaCarac.ListCount - 1
        StrAntib = StrAntib & EcListaCarac.List(i) & "; "
    Next i
    If StrAntib <> "" Then
        StrAntib = Mid(StrAntib, 1, Len(StrAntib) - 2)
        If Len(StrAntib) > 250 Then StrAntib = Mid(StrAntib, 1, 240) & " ..."
        Report.formulas(31) = "CaracMicro=" & BL_TrataStringParaBD("" & StrAntib)
    Else
        Report.formulas(31) = "CaracMicro=" & BL_TrataStringParaBD("Todos")
    End If
    
    'Me.SetFocus
    
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
'    sql = "DELETE FROM sl_cr_estatmicrorgdet WHERE nome_computador = " & BL_TrataStringParaBD(BG_SYS_GetComputerName)
'    BG_ExecutaQuery_ADO sql
'    BG_Trata_BDErro
'    sql = "DELETE FROM sl_cr_estatmicrorg WHERE nome_computador = " & BL_TrataStringParaBD(BG_SYS_GetComputerName)
'    BG_ExecutaQuery_ADO sql
'    sql = "DELETE FROM sl_cr_estatmicrorg_carac WHERE nome_computador = " & BL_TrataStringParaBD(BG_SYS_GetComputerName)
'    BG_ExecutaQuery_ADO sql
'    BG_Trata_BDErro

    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    'Call BL_RemoveTabela("SL_CR_ESTATMICRORG" & gNumeroSessao)
    
End Sub

Function Funcao_DataActual()

    Select Case UCase(CampoActivo.Name)
        Case "ECDTFIM"
            EcDtFim = Bg_DaData_ADO
        Case "ECDTINI"
            EcDtIni = Bg_DaData_ADO
    End Select
    
End Function


Private Sub BtPesquisaAntibio_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_antibio", _
'                        "abrev_antibio", "seq_antibio", _
'                         EcPesqRapAntibio
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_antibio"
    CamposEcran(1) = "cod_antibio"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "abrev_antibio"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_antibio"
    If EcCodGrupo.text <> "" Then
        CWhere = " gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.text)
    Else
        CWhere = ""
    End If
    CampoPesquisa = "abrev_antibio"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY abrev_antibio ", _
                                                                           " Antibi�ticos")

    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaAntibio.ListCount = 0 Then
                ListaAntibio.AddItem BL_SelCodigo("sl_antibio", "abrev_antibio", "seq_antibio", Resultados(i))
                ListaAntibio.ItemData(0) = Resultados(i)
            Else
                ListaAntibio.AddItem BL_SelCodigo("sl_antibio", "abrev_antibio", "seq_antibio", Resultados(i))
                ListaAntibio.ItemData(ListaAntibio.NewIndex) = Resultados(i)
            End If
        Next i
    End If


End Sub

'Private Sub BtPesquisaEntFin_Click()
'
'    Dim ChavesPesq(1 To 2) As String
'    Dim CampoPesquisa As String
'    Dim CamposEcran(1 To 2) As String
'    Dim CWhere As String
'    Dim CFrom As String
'    Dim CamposRetorno As New ClassPesqResultados
'    Dim Tamanhos(1 To 2) As Long
'    Dim Headers(1 To 2) As String
'    Dim CancelouPesquisa As Boolean
'    Dim Resultados(1 To 2)  As Variant
'    Dim PesqRapida As Boolean
'
'    PesqRapida = False
'
'    ChavesPesq(1) = "descr_efr"
'    CamposEcran(1) = "descr_efr"
'    Tamanhos(1) = 4000
'    Headers(1) = "Descri��o"
'
'    ChavesPesq(2) = "cod_efr"
'    CamposEcran(2) = "cod_efr"
'    Tamanhos(2) = 1000
'    Headers(2) = "C�digo"
'
'    CamposRetorno.InicializaResultados 2
'
'    CFrom = "sl_efr"
'    CampoPesquisa = "descr_efr"
'
'    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
'            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Entidades Finaceiras")
'
'    If PesqRapida = True Then
'        FormPesqRapidaAvancada.Show vbModal
'        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
'        If Not CancelouPesquisa Then
'            EcCodEFR.Text = Resultados(2)
'            EcDescrEFR.Text = Resultados(1)
'            BtPesquisaEntFin.SetFocus
'        End If
'    Else
'        BG_Mensagem mediMsgBox, "N�o existem entidades financeiras", vbExclamation, "EFR"
'        EcCodEFR.SetFocus
'    End If
'
'End Sub

Private Sub BtPesquisaCarac_Click()
    PA_PesquisaCaracMicroMultiSel EcListaCarac
End Sub

Private Sub EcListaCarac_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        PA_ApagaItemListaMultiSel EcListaCarac, KeyCode, Shift
    End If
End Sub

Private Sub BtPesquisaGrupo_Click()

'    FormPesquisaRapida.InitPesquisaRapida "sl_gr_ana", _
'                        "descr_gr_ana", "seq_gr_ana", _
'                         EcPesqRapGrupo, , "cod_local = " & gCodLocal

    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    ClausulaWhere = "cod_local = " & gCodLocal
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupo de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lises."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodGrupo.text = Resultados(1)
            EcDescrGrupo.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub BtPesquisaMicro_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_microrg", _
'                        "descr_microrg", "seq_microrg", _
'                         EcPesqRapMicro

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_microrg"
    CamposEcran(1) = "cod_microrg"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_microrg"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_microrg"
    If EcCodGrupo.text <> "" Then
        CWhere = " gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.text)
    End If
    CampoPesquisa = "descr_microrg"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_microrg ", _
                                                                           " Microrganismos")

    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaMicrorg.ListCount = 0 Then
                ListaMicrorg.AddItem BL_SelCodigo("sl_microrg", "descr_microrg", "seq_microrg", Resultados(i))
                ListaMicrorg.ItemData(0) = Resultados(i)
            Else
                ListaMicrorg.AddItem BL_SelCodigo("sl_microrg", "descr_microrg", "seq_microrg", Resultados(i))
                ListaMicrorg.ItemData(ListaMicrorg.NewIndex) = Resultados(i)
            End If
        Next i
    End If

End Sub

Private Sub BtPesquisaPerfis_Click()
'    FormPesquisaRapida.InitPesquisaRapida "sl_produto", _
'                        "descr_produto", "seq_produto", _
'                         EcPesqRapProduto

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_perfis "
    CWhere = " flg_activo = 1 "
    If EcCodGrupo.text <> "" Then
        CWhere = CWhere & " and gr_ana = " & BL_TrataStringParaBD(EcCodGrupo.text)
    End If
    CampoPesquisa = "descr_perfis"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_perfis ", _
                                                                           " Exames")

    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaPerfis.ListCount = 0 Then
                ListaPerfis.AddItem BL_SelCodigo("sl_perfis", "descr_perfis", "seq_perfis", Resultados(i))
                ListaPerfis.ItemData(0) = Resultados(i)
            Else
                ListaPerfis.AddItem BL_SelCodigo("sl_perfis", "descr_perfis", "seq_perfis", Resultados(i))
                ListaPerfis.ItemData(ListaPerfis.NewIndex) = Resultados(i)
            End If
        Next i
        

    End If
    




End Sub

Private Sub BtPesquisaProduto_click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_produto"
    CWhere = ""
    CampoPesquisa = "descr_produto"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_produto ", _
                                                                           " Produtos")

    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaProduto.ListCount = 0 Then
                ListaProduto.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", Resultados(i))
                ListaProduto.ItemData(0) = Resultados(i)
            Else
                ListaProduto.AddItem BL_SelCodigo("sl_produto", "descr_produto", "seq_produto", Resultados(i))
                ListaProduto.ItemData(ListaProduto.NewIndex) = Resultados(i)
            End If
        Next i
    End If
End Sub

Private Sub BtPesquisaProveniencia_Click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "seq_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_proven"
    CWhere = ""
    CampoPesquisa = "descr_proven"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_proven ", _
                                                                           " Proveni�ncias")

    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If Resultados(i) = 71 Then
               'MsgBox ""
            End If
            If ListaProven.ListCount = 0 And BL_HandleNull(Resultados(i), "") <> "" Then
                ListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", Resultados(i))
                ListaProven.ItemData(0) = Resultados(i)
            ElseIf BL_HandleNull(Resultados(i), "") <> "" Then
                ListaProven.AddItem BL_SelCodigo("sl_proven", "descr_proven", "seq_proven", Resultados(i))
                ListaProven.ItemData(ListaProven.NewIndex) = Resultados(i)
            End If
        Next i
        

    End If
        
End Sub

Private Sub BtReportLista_Click()
    If EcIntervaloTempo = "" Then
        EcIntervaloTempo = "0"
    End If
    Call Preenche_Estatistica("Lista")
End Sub

Private Sub BtReportTabela_Click()
    Call Preenche_Estatistica("Tabela")
End Sub


Private Sub CbSensib_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbSensib_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbSensib.ListIndex = -1
    
End Sub



Private Sub CbUrgencia_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbUrgencia_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then CbUrgencia.ListIndex = -1
    
End Sub

Private Sub CkAgruparResistentes_Click()
    If CkAgruparResistentes = vbChecked Then
        CbSensib.Enabled = False
        CbSensib.ListIndex = -1
    Else
        CbSensib.Enabled = True
        CbSensib.ListIndex = -1
    End If
End Sub

Private Sub Command1_Click()

End Sub

Private Sub EcCodGrupo_Validate(Cancel As Boolean)
    Dim RsDescrGrupo As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodGrupo.text) <> "" Then
        Set RsDescrGrupo = New ADODB.recordset
        
        With RsDescrGrupo
            .Source = "SELECT cod_gr_ana, descr_gr_ana FROM sl_gr_ana WHERE upper(cod_gr_ana)= " & BL_TrataStringParaBD(UCase(EcCodGrupo.text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrupo.RecordCount > 0 Then
            EcCodGrupo.text = "" & RsDescrGrupo!cod_gr_ana
            EcDescrGrupo.text = "" & RsDescrGrupo!descr_gr_ana
        Else
            Cancel = True
            EcDescrGrupo.text = ""
            BG_Mensagem mediMsgBox, "O grupo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrGrupo.Close
        Set RsDescrGrupo = Nothing
    Else
        EcDescrGrupo.text = ""
    End If

End Sub


Private Sub EcCodMicro_Validate(Cancel As Boolean)
    Dim RsDescrMicrorg As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodMicro.text) <> "" Then
        Set RsDescrMicrorg = New ADODB.recordset
        
        With RsDescrMicrorg
            .Source = "SELECT cod_microrg, descr_microrg FROM sl_microrg WHERE upper(cod_microrg)= " & BL_TrataStringParaBD(UCase(EcCodMicro.text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrMicrorg.RecordCount > 0 Then
            EcCodMicro.text = "" & RsDescrMicrorg!cod_microrg
            EcDescrMicro.text = "" & RsDescrMicrorg!descr_microrg
        Else
            Cancel = True
            EcCodMicro.text = ""
            EcDescrMicro.text = ""
            BG_Mensagem mediMsgBox, "O microrganismo indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrMicrorg.Close
        Set RsDescrMicrorg = Nothing
    Else
        EcDescrMicro.text = ""
    End If

End Sub

Private Sub EcCodProduto_Validate(Cancel As Boolean)
    Dim RsDescrProd As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProduto.text) <> "" Then
        Set RsDescrProd = New ADODB.recordset
        
        With RsDescrProd
            .Source = "SELECT cod_produto, descr_produto FROM sl_produto WHERE upper(cod_produto)= " & BL_TrataStringParaBD(UCase(EcCodProduto.text))
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProd.RecordCount > 0 Then
            EcCodProduto.text = "" & RsDescrProd!cod_produto
            EcDescrProduto.text = "" & RsDescrProd!descr_produto
        Else
            Cancel = True
            EcDescrProduto.text = ""
            BG_Mensagem mediMsgBox, "O Produto indicado n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrProd.Close
        Set RsDescrProd = Nothing
    Else
        EcDescrProduto.text = ""
    End If

End Sub

Private Sub EcCodProveniencia_GotFocus()

    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcCodEFR_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

'Private Sub EcCodEFR_Validate(Cancel As Boolean)
'
'    Dim RsDescrEFR As ADODB.recordset
'
'    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
'    If Cancel = True Then
'        Exit Sub
'    End If
'
'    If Trim(EcCodEFR.Text) <> "" Then
'        Set RsDescrEFR = New ADODB.recordset
'
'        With RsDescrEFR
'            .Source = "SELECT descr_efr FROM sl_efr WHERE cod_efr= " & BL_TrataStringParaBD(EcCodEFR.Text)
'            .CursorLocation = adUseServer
'            .CursorType = adOpenStatic
'            .ActiveConnection = gConexao
'            .Open
'        End With
'
'        If RsDescrEFR.RecordCount > 0 Then
'            EcDescrEFR.Text = "" & RsDescrEFR!descr_efr
'        Else
'            Cancel = True
'            EcDescrEFR.Text = ""
'            BG_Mensagem mediMsgBox, "Entidade Financeira inexistente!", vbOKOnly + vbExclamation, App.ProductName
'            SendKeys ("{HOME}+{END}")
'        End If
'        RsDescrEFR.Close
'        Set RsDescrEFR = Nothing
'    Else
'        EcDescrEFR.Text = ""
'    End If
'
'End Sub
      
Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim RsDescrProv As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodProveniencia.text) <> "" Then
        Set RsDescrProv = New ADODB.recordset
        
        With RsDescrProv
            .Source = "SELECT descr_proven FROM sl_proven WHERE cod_proven= " & BL_TrataStringParaBD(EcCodProveniencia.text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrProv.RecordCount > 0 Then
            EcDescrProveniencia.text = "" & RsDescrProv!descr_proven
        Else
            Cancel = True
            EcDescrProveniencia.text = ""
            BG_Mensagem mediMsgBox, "A Proveni�ncia indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrProv.Close
        Set RsDescrProv = Nothing
    Else
        EcDescrProveniencia.text = ""
    End If
    
End Sub

Private Sub EcDescrSexo_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDescrSexo_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 And Shift = 0 Then EcDescrSexo.ListIndex = -1
    
End Sub

Private Sub EcDtFim_GotFocus()

    If Trim(EcDtFim.text) = "" Then
        EcDtFim.text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcDtIni_GotFocus()

    If Trim(EcDtIni.text) = "" Then
        EcDtIni.text = Bg_DaData_ADO
    End If
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub


Private Sub EcPesqRapAntibio_Change()
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapAntibio.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT seq_antibio, abrev_antibio  FROM sl_antibio WHERE seq_antibio = " & EcPesqRapAntibio, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar o antibi�tico!", vbExclamation, "Pesquisa r�pida"
        Else
            If ListaAntibio.ListCount = 0 Then
                ListaAntibio.AddItem rsCodigo!abrev_antibio
                ListaAntibio.ItemData(0) = rsCodigo!seq_antibio
            Else
                ListaAntibio.AddItem rsCodigo!abrev_antibio
                ListaAntibio.ItemData(ListaAntibio.NewIndex) = rsCodigo!seq_antibio
            End If
        End If
    End If
End Sub

Private Sub EcPesqRapGrupo_Change()
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapGrupo.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT cod_gr_ana,descr_gr_ana FROM sl_gr_ana WHERE seq_gr_ana = " & EcPesqRapGrupo, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do grupo!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodGrupo.text = BL_HandleNull(rsCodigo!cod_gr_ana, "")
            EcDescrGrupo.text = BL_HandleNull(rsCodigo!descr_gr_ana, "")
        End If
    End If
End Sub

Private Sub EcPesqRapMicro_Change()
    Dim rsCodigo As ADODB.recordset
    
    If EcPesqRapMicro.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT cod_microrg,descr_microrg FROM sl_microrg WHERE seq_microrg = " & EcPesqRapMicro, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do microrganismo!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodMicro.text = BL_HandleNull(rsCodigo!cod_microrg, "")
            EcDescrMicro.text = BL_HandleNull(rsCodigo!descr_microrg, "")
        End If
    End If
End Sub

Private Sub EcPesqRapProduto_Change()
    
    Dim rsCodigo As ADODB.recordset
    Dim indice As Integer
    
    If EcPesqRapProduto.text <> "" Then
        Set rsCodigo = New ADODB.recordset
        rsCodigo.CursorLocation = adUseServer
        rsCodigo.CursorType = adOpenStatic

        rsCodigo.Open "SELECT cod_produto,descr_produto FROM sl_produto WHERE seq_produto = " & EcPesqRapProduto, gConexao
        If rsCodigo.RecordCount <= 0 Then
            BG_Mensagem mediMsgBox, "Erro a seleccionar c�digo do produto!", vbExclamation, "Pesquisa r�pida"
        Else
            EcCodProduto.text = BL_HandleNull(rsCodigo!cod_produto, "")
            EcDescrProduto.text = BL_HandleNull(rsCodigo!descr_produto, "")
        End If
    End If

End Sub

Private Sub Flg_DescrRequis_Click()
    If Flg_DescrRequis.value = 1 Then
'        CbSensib.ListIndex = mediComboValorNull
'        ListaAntibio.Clear
'        CbSensib.Enabled = False
'        ListaAntibio.Enabled = False
'        BtPesquisaAntibio.Enabled = False
'        LaAntibio.Enabled = False
'        LaSensib.Enabled = False

'        Flg_DescrAntibio.Value = 0
'        Flg_DescrAntibio.Enabled = False
        EcIntervaloTempo.text = "0"
    Else
        CbSensib.Enabled = True
        ListaAntibio.Enabled = True
        BtPesquisaAntibio.Enabled = True
        LaAntibio.Enabled = True
        LaSensib.Enabled = True
        Flg_DescrAntibio.Enabled = True
        Flg_DescrAntibio.Enabled = True
    End If
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Estat�stica de Microrganismos"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 7950
    Me.Height = 9780 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcCodProveniencia
      
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
'    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
'    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    Call BL_FechaPreview("Estat�stica de Microrganismos")
    
    Set FormEstatMicrorg = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    CbLocal.ListIndex = mediComboValorNull
    'CbAnalises.ListIndex = mediComboValorNull
    CbUrgencia.ListIndex = mediComboValorNull
    ListaSituacao.Clear
    EcDescrSexo.ListIndex = mediComboValorNull
    'CbGrupo.ListIndex = mediComboValorNull
    EcCodProveniencia.text = ""
    EcDescrProveniencia.text = ""
    EcCodProduto.text = ""
    EcDescrProduto.text = ""
    EcCodMicro.text = ""
    EcDescrMicro.text = ""
'    EcCodEFR.Text = ""
'    EcDescrEFR.Text = ""
    EcDtFim.text = ""
    EcDtIni.text = ""
    Flg_DescrAntibio.value = 0
    Flg_OrderProd.value = 0
    Flg_OrderProven.value = 0
    Flg_DescrRequis.value = 0
    ListaAntibio.Clear
    CbSensib.ListIndex = mediComboValorNull
    ListaProduto.Clear
    ListaProven.Clear
    ListaMicrorg.Clear
    ListaPerfis.Clear
    CkAgruparResistentes.value = vbUnchecked
    CkContarSemFact.value = vbUnchecked
    Opt1(0).value = True
    Option1(0).value = True
End Sub

Sub DefTipoCampos()

    'Tipo VarChar
    EcCodProveniencia.Tag = "200"

    'Tipo Data
    EcDtIni.Tag = "104"
    EcDtFim.Tag = "104"
    
    'Tipo Inteiro
'    EcNumFolhaTrab.Tag = "3"
    
    EcCodProveniencia.MaxLength = 5
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
'    EcNumFolhaTrab.MaxLength = 10

    EcIntervaloTempo.Tag = "3"
    EcErrosMinor = "2"
    EcErrosMajor = "0"
    
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If Estado = 2 Then
        Estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    Opt1(0).value = True

    BG_PreencheComboBD_ADO "sl_tbf_sexo", "cod_sexo", "descr_sexo", EcDescrSexo

    'Preenche Combo Urgencia
    CbUrgencia.AddItem "Normal"
    CbUrgencia.AddItem "Urgente"
    
    With CbSensib
        .AddItem "Resistente"
        .AddItem "Interm�dio"
        .AddItem "Sens�vel"
        .AddItem "P"
        .AddItem "N"
    End With
    
        
    EcIntervaloTempo.text = BL_HandleNull(gIntervaloTempoRepetidosMicro, "365")
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", CbLocal
    Option1(0).value = True
    PreencheCaracDefeito
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()

    Call Preenche_Estatistica("Lista")
    
End Sub

Sub ImprimirVerAntes()
    Call Preenche_Estatistica("Lista")
End Sub

Private Function PreencheTabelaTemporariaNEW() As Long
    Dim sSql As String
    Dim rsSel As New ADODB.recordset
    Dim rsAntib As New ADODB.recordset
    
    sSql = "DELETE FROM sl_cr_estatmicrorgdet WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro
    sSql = "DELETE FROM sl_cr_estatmicrorg WHERE nome_computador = " & BL_TrataStringParaBD(gComputador)
    BG_ExecutaQuery_ADO sSql
    sSql = "DELETE FROM sl_cr_estatmicrorg_carac WHERE nome_computador = " & BL_TrataStringParaBD(BG_SYS_GetComputerName)
    BG_ExecutaQuery_ADO sSql
    BG_Trata_BDErro

    ReDim EstrutUte(0)
    TotalUte = 0
    ' -----------------------------------------------
    ' SELECCIONA E PREENCHE AS ESTRUTURAS
    ' -----------------------------------------------
    sSql = RetornaQuery
    rsSel.CursorLocation = adUseServer
    rsSel.CursorType = adOpenStatic
    rsSel.Open sSql, gConexao
    If rsSel.RecordCount > 0 Then
        While Not rsSel.EOF
            PreencheEstruturas BL_HandleNull(rsSel!Prova, ""), rsSel!seq_utente, rsSel!n_req, _
                rsSel!seq_realiza, rsSel!cod_micro, rsSel!descr_microrg, _
                rsSel!N_Res, rsSel!nome_ute, rsSel!flg_tsq, _
                BL_HandleNull(rsSel!descr_produto, ""), BL_HandleNull(rsSel!descr_proven, ""), _
                BL_HandleNull(rsSel!Utente, ""), BL_HandleNull(rsSel!dt_chega, ""), _
                BL_HandleNull(rsSel!cod_carac_micro, mediComboValorNull), _
                BL_HandleNull(rsSel!descr_carac_micro, "")

            rsSel.MoveNext
        Wend
    End If
    rsSel.Close
    Set rsSel = Nothing

    ' -----------------------------------------------------------
    ' CONTA OS MICRORGANISMOS(INFEC��ES) E PREENCHE TABELA REPORT
    ' -----------------------------------------------------------
    ContaInfeccoes
End Function

Private Sub ListaAntibio_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaAntibio.ListCount > 0 Then     'Delete
        ListaAntibio.RemoveItem (ListaAntibio.ListIndex)
    End If
End Sub

Private Sub ListaMicrorg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaMicrorg.ListCount > 0 Then     'Delete
        ListaMicrorg.RemoveItem (ListaMicrorg.ListIndex)
    End If
End Sub
Private Sub Listaperfis_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaPerfis.ListCount > 0 Then     'Delete
        ListaPerfis.RemoveItem (ListaPerfis.ListIndex)
    End If
End Sub
Private Sub ListaProduto_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaProduto.ListCount > 0 Then     'Delete
        ListaProduto.RemoveItem (ListaProduto.ListIndex)
    End If
End Sub
Private Sub ListaSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaSituacao.ListCount > 0 Then     'Delete
        ListaSituacao.RemoveItem (ListaSituacao.ListIndex)
    End If
End Sub

Private Sub ListaProven_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And ListaProven.ListCount > 0 Then     'Delete
        ListaProven.RemoveItem (ListaProven.ListIndex)
    End If
End Sub


Function InsereNovoRegisto(Prova As String, seq_utente As Long, n_req As Long, seq_realiza As Long, _
                            cod_micro As String, descr_microrg As String, _
                            N_Res As Integer, nome_ute As String, flg_tsq As String, _
                            descr_produto As String, descr_proven As String, Utente As String) As Boolean
                            
    'insere novo microrganismo e estrutura de antibiograma
    Dim SqlIns As String
    Dim sql As String
    Dim rsAntib As ADODB.recordset
    Dim indice As Long
    Dim ListaAntibioticos As String
    Dim i As Long
    
    On Error GoTo TrataErro
    
    SqlIns = "INSERT INTO SL_CR_ESTATMICRORGDET ("
    
    If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
        SqlIns = SqlIns & " DESCR_PRODUTO,DESCR_PROVEN,"
    ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
        SqlIns = SqlIns & " DESCR_PRODUTO,"
    ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
        SqlIns = SqlIns & " DESCR_PROVEN,"
    End If
    
'    If Flg_DescrRequis.Value = 1 Then
'        SqlIns = SqlIns & " N_REQ,SEQ_REALIZA,COD_MICRORG, DESCR_MICRORG, N_RES, NOME_UTE) "
'    Else
        SqlIns = SqlIns & " N_REQ,SEQ_REALIZA,COD_MICRORG, DESCR_MICRORG, N_RES, COD_ANTIBIO, DESCR_ANTIBIO, RES_SENSIB, NOME_UTE, NOME_COMPUTADOR,UTENTE) "
'    End If
    
    sql = ""
    
'    If Flg_DescrRequis.Value = 1 Then
'        Sql = SqlIns & " values ( "
'    Else
        sql = SqlIns & " select "
'    End If
    
    If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
        sql = sql & BL_TrataStringParaBD(descr_produto) & "," & BL_TrataStringParaBD(descr_proven) & ","
    ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
        sql = sql & BL_TrataStringParaBD(descr_produto) & ","
    ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
        sql = sql & BL_TrataStringParaBD(descr_proven) & ","
    End If

    
    ' se prova = "Sem Fact" n�o conta o microrganismo estatisticamente
    If (Prova <> "4" And CkContarSemFact.value = vbUnchecked) Or CkContarSemFact.value = vbChecked Then 'Or (gLAB = "CHVNG" And CDate(EcDtIni.Text) >= CDate("01-01-2014")) Then
    
            If flg_tsq = "S" Then
                
                'carrega estrutura de antibiograma
                Set rsAntib = New ADODB.recordset
                rsAntib.CursorLocation = adUseServer
                rsAntib.CursorType = adOpenStatic
                'tania.oliveira 13.04.2020 CHVNG-11007|Gx-10607
                rsAntib.Source = "select sl_res_tsq.cod_antib, abrev_antibio descr_antibio, res_sensib from sl_res_tsq, sl_antibio where sl_res_tsq.cod_antib = sl_antibio.cod_antibio AND sl_res_tsq.res_sensib in ('R','S','P','N','I','-','+', 'E') and seq_realiza = " & seq_realiza & " and cod_micro = " & BL_TrataStringParaBD(cod_micro) & " ORDER BY COD_ANTIB"
                rsAntib.Open , gConexao
                indice = 1
                If rsAntib.RecordCount > 0 Then
                    
                    ReDim Preserve antib(rsAntib.RecordCount)
                    While Not rsAntib.EOF
                        antib(indice).seq_utente = seq_utente
                        antib(indice).cod_micro = cod_micro
                        antib(indice).cod_antib = rsAntib!cod_antib
                        antib(indice).Sensib = rsAntib!res_sensib
                        indice = indice + 1
                        rsAntib.MoveNext
                    Wend
                    
'                    If Flg_DescrRequis.Value = 1 Then
'                        'n�o insere antibiograma, apenas microrganismo
'                        Sql = Sql & n_req & "," & seq_realiza & "," & BL_TrataStringParaBD(cod_micro) & "," & BL_TrataStringParaBD(Descr_Microrg) & _
'                                    "," & N_Res & "," & BL_TrataStringParaBD(nome_ute) & ")"
'                        BG_ExecutaQuery_ADO Sql
'                        BG_Trata_BDErro
'
'                    Else
                        'insere microrganismo e antibiograma
                        If CkAgruparMicrorg.value Then
                            sql = sql & n_req & "," & seq_realiza & "," & BL_TrataStringParaBD("CODMICRO") & "," & BL_TrataStringParaBD(descr_microrg) & _
                                        "," & N_Res & ",cod_antibio, abrev_antibio descr_antibio, " & IIf(CkAgruparResistentes = vbChecked, "replace (res_sensib,'I','R') res_sensib", "res_sensib") & ", " & BL_TrataStringParaBD(nome_ute) & ", " & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(Utente) & _
                                        " from sl_res_tsq, sl_antibio where sl_res_tsq.cod_antib = sl_antibio.cod_antibio and " & _
                                        " seq_realiza = " & seq_realiza & " and cod_micro = " & BL_TrataStringParaBD(cod_micro)
                        Else
                            sql = sql & n_req & "," & seq_realiza & "," & BL_TrataStringParaBD(cod_micro) & "," & BL_TrataStringParaBD(descr_microrg) & _
                                        "," & N_Res & ",cod_antibio, abrev_antibio descr_antibio, " & IIf(CkAgruparResistentes = vbChecked, "replace (res_sensib,'I','R') res_sensib", "res_sensib") & ", " & BL_TrataStringParaBD(nome_ute) & ", " & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(Utente) & _
                                        " from sl_res_tsq, sl_antibio where sl_res_tsq.cod_antib = sl_antibio.cod_antibio and " & _
                                        " seq_realiza = " & seq_realiza & " and cod_micro = " & BL_TrataStringParaBD(cod_micro)
                        End If
                        
                        If gLAB <> "HSMARTA" Then
                            If CbSensib.ListIndex <> mediComboValorNull Then
                                sql = sql & " AND sl_res_tsq.res_sensib = " & BL_TrataStringParaBD(UCase(Mid(CbSensib.text, 1, 1)))
                            End If
                            'tania.oliveira 13.04.2020 CHVNG-11007|Gx-10607
                            sql = sql & " AND sl_res_tsq.res_sensib in 'R','S','P','N','I','-','+', 'E') "
                            
                            If ListaAntibio.ListCount > 0 Then
                                'LISTA DE ANTIBIOTICOS:
                    
                                'Forma a Lista de antibioticos seleccionados para o crit�rio do sqlsel
                                ListaAntibioticos = ""
                                For i = 0 To ListaAntibio.ListCount - 1
                                    ListaAntibioticos = ListaAntibioticos & ListaAntibio.ItemData(i) & ","
                                Next i
                                'Remove a virgula do final da express�o
                                
                                ListaAntibioticos = Mid(ListaAntibioticos, 1, Len(ListaAntibioticos) - 1)
                    
                                sql = sql & " AND sl_antibio.seq_antibio in (" & ListaAntibioticos & ")"
                            End If
                        End If
                            
                        BG_ExecutaQuery_ADO sql
                        BG_Trata_BDErro
                        
                        sql = "INSERT INTO SL_CR_ESTATMICRORG ("
                        If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
                            sql = sql & " DESCR_PRODUTO,DESCR_PROVEN,"
                        ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
                            sql = sql & " DESCR_PRODUTO,"
                        ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
                            sql = sql & " DESCR_PROVEN,"
                        End If
                        sql = sql & "N_REQ,SEQ_REALIZA,COD_MICRORG, DESCR_MICRORG,N_RES,NOME_UTE,NOME_COMPUTADOR, UTENTE) VALUES ("
                        If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
                            sql = sql & BL_TrataStringParaBD(descr_produto) & "," & BL_TrataStringParaBD(descr_proven) & ","
                        ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
                            sql = sql & BL_TrataStringParaBD(descr_produto) & ","
                        ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
                            sql = sql & BL_TrataStringParaBD(descr_proven) & ","
                        End If
                        If CkAgruparMicrorg.value Then
                            sql = sql & n_req & "," & seq_realiza & "," & BL_TrataStringParaBD("CODMICRO") & "," & BL_TrataStringParaBD(descr_microrg) & _
                                "," & N_Res & "," & BL_TrataStringParaBD(nome_ute) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(Utente) & ")"
                        Else
                            sql = sql & n_req & "," & seq_realiza & "," & BL_TrataStringParaBD(cod_micro) & "," & BL_TrataStringParaBD(descr_microrg) & _
                                "," & N_Res & "," & BL_TrataStringParaBD(nome_ute) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(Utente) & ")"
                        End If
                        BG_ExecutaQuery_ADO sql
                        BG_Trata_BDErro
                        
'                    End If
                Else
                    'se afinal nao tem antibiograma,
                    'insere apenas microrganismo
                    sql = "INSERT INTO SL_CR_ESTATMICRORGDET " & _
                            "(N_REQ,SEQ_REALIZA,COD_MICRORG, DESCR_MICRORG,N_RES,NOME_UTE, NOME_COMPUTADOR, UTENTE) VALUES (" & _
                            n_req & "," & seq_realiza & "," & BL_TrataStringParaBD(cod_micro) & "," & BL_TrataStringParaBD(descr_microrg) & _
                            "," & N_Res & "," & BL_TrataStringParaBD(nome_ute) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(Utente) & ")"
                    BG_ExecutaQuery_ADO sql
                    BG_Trata_BDErro
                    
                    sql = "INSERT INTO SL_CR_ESTATMICRORG ("
                    If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
                        sql = sql & " DESCR_PRODUTO,DESCR_PROVEN,"
                    ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
                        sql = sql & " DESCR_PRODUTO,"
                    ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
                        sql = sql & " DESCR_PROVEN,"
                    End If
                    sql = sql & "N_REQ,SEQ_REALIZA,COD_MICRORG, DESCR_MICRORG,N_RES,NOME_UTE,NOME_COMPUTADOR,UTENTE) VALUES ("
                    If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
                        sql = sql & BL_TrataStringParaBD(descr_produto) & "," & BL_TrataStringParaBD(descr_proven) & ","
                    ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
                        sql = sql & BL_TrataStringParaBD(descr_produto) & ","
                    ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
                        sql = sql & BL_TrataStringParaBD(descr_proven) & ","
                    End If
                    If CkAgruparMicrorg.value Then
                        sql = sql & n_req & "," & seq_realiza & "," & BL_TrataStringParaBD("CODMICRO") & "," & BL_TrataStringParaBD(descr_microrg) & _
                            "," & N_Res & "," & BL_TrataStringParaBD(nome_ute) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(Utente) & ")"
                    Else
                        sql = sql & n_req & "," & seq_realiza & "," & BL_TrataStringParaBD(cod_micro) & "," & BL_TrataStringParaBD(descr_microrg) & _
                            "," & N_Res & "," & BL_TrataStringParaBD(nome_ute) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(Utente) & ")"
                    End If
                    BG_ExecutaQuery_ADO sql
                    BG_Trata_BDErro
                    
                End If
            Else
                'n�o tem antibiograma
                'insere apenas microrganismo
                sql = "INSERT INTO SL_CR_ESTATMICRORGDET" & _
                        "(N_REQ,SEQ_REALIZA,COD_MICRORG,DESCR_MICRORG,N_RES,NOME_UTE, NOME_COMPUTADOR,UTENTE) VALUES (" & n_req & _
                        "," & seq_realiza & "," & BL_TrataStringParaBD(cod_micro) & "," & BL_TrataStringParaBD(descr_microrg) & _
                        "," & N_Res & "," & BL_TrataStringParaBD(nome_ute) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(Utente) & ")"
                BG_ExecutaQuery_ADO sql
                BG_Trata_BDErro
                
                    sql = "INSERT INTO SL_CR_ESTATMICRORG ("
                    If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
                        sql = sql & " DESCR_PRODUTO,DESCR_PROVEN,"
                    ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
                        sql = sql & " DESCR_PRODUTO,"
                    ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
                        sql = sql & " DESCR_PROVEN,"
                    End If
                    sql = sql & "N_REQ,SEQ_REALIZA,COD_MICRORG, DESCR_MICRORG,N_RES,NOME_UTE,NOME_COMPUTADOR, UTENTE) VALUES ("
                    If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
                        sql = sql & BL_TrataStringParaBD(descr_produto) & "," & BL_TrataStringParaBD(descr_proven) & ","
                    ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
                        sql = sql & BL_TrataStringParaBD(descr_produto) & ","
                    ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
                        sql = sql & BL_TrataStringParaBD(descr_proven) & ","
                    End If
                    If CkAgruparMicrorg.value Then
                        sql = sql & n_req & "," & seq_realiza & "," & BL_TrataStringParaBD("CODMICRO") & "," & BL_TrataStringParaBD(descr_microrg) & _
                            "," & N_Res & "," & BL_TrataStringParaBD(nome_ute) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(Utente) & ")"
                    Else
                        sql = sql & n_req & "," & seq_realiza & "," & BL_TrataStringParaBD(cod_micro) & "," & BL_TrataStringParaBD(descr_microrg) & _
                            "," & N_Res & "," & BL_TrataStringParaBD(nome_ute) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(Utente) & ")"
                    End If
                BG_ExecutaQuery_ADO sql
                BG_Trata_BDErro
                
                ReDim Preserve antib(0)
            End If

        InsereNovoRegisto = True
    Else
        InsereNovoRegisto = False
    End If
    Exit Function
    
TrataErro:

    BG_LogFile_Erros "FormEstatMicrorgTempo (InsereNovoRegisto): " & vbCrLf & " Sql = " & sql & vbCrLf & Err.Number & "-" & Err.Description
    InsereNovoRegisto = False
    Exit Function
    Resume Next
End Function


Private Function VerificaErroMinor(sensib1 As String, sensib2 As String) As Boolean
    If sensib1 = "I" Then
        If sensib2 = "R" Or sensib2 = "S" Then
            VerificaErroMinor = True
        Else
            VerificaErroMinor = False
        End If
    ElseIf sensib1 = "R" Then
        If sensib2 = "I" Then
            VerificaErroMinor = True
        Else
            VerificaErroMinor = False
        End If
    ElseIf sensib1 = "S" Then
        If sensib2 = "I" Then
            VerificaErroMinor = True
        Else
            VerificaErroMinor = False
        End If
    Else
        VerificaErroMinor = False
    End If
End Function

Private Function VerificaErroMajor(sensib1 As String, sensib2 As String) As Boolean
    If sensib1 = "R" Then
        If sensib2 = "S" Then
            VerificaErroMajor = True
        Else
            VerificaErroMajor = False
        End If
    ElseIf sensib1 = "S" Then
        If sensib2 = "R" Then
            VerificaErroMajor = True
        Else
            VerificaErroMajor = False
        End If
    Else
        VerificaErroMajor = False
    End If
End Function

' -----------------------------------------------------------------------------------

' RETORNA O SELECT INICIAL PARA SABER OS MICRORGANISMOS EXISTENTES NO INTERVALO DATAS

' -----------------------------------------------------------------------------------
Private Function RetornaQuery() As String
    Dim sSql As String
    Dim dt_nasc_ini As String
    Dim dt_nasc_fim As String
    Dim i As Long
    Dim ListaAntibioticos As String
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    Dim tabela_aux As String
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
    '
    
    If CkAgruparMicrorg.value = vbChecked Then
        sSql = "SELECT DISTINCT sl_requis.n_req,sl_res_micro.seq_realiza,sl_res_micro.cod_micro, "
        sSql = sSql & " sl_res_micro.n_res,  'Agrupamento Microrganismos' descr_microrg ,nome_ute, "
        sSql = sSql & " sl_requis.dt_chega, sl_realiza.dt_val,sl_requis.seq_utente, sl_res_micro.flg_tsq,"
        sSql = sSql & " sl_res_micro.prova, descr_produto, descr_proven, " & tabela_aux & ".utente, sl_res_micro.cod_carac_micro,  "
        sSql = sSql & " sl_tbf_carac_micro.descr_carac_micro "
    Else
        sSql = "SELECT DISTINCT sl_requis.n_req,sl_res_micro.seq_realiza,sl_res_micro.cod_micro, "
        sSql = sSql & " sl_res_micro.n_res, descr_microrg,nome_ute,sl_requis.dt_chega, sl_realiza.dt_val, "
        sSql = sSql & " sl_requis.seq_utente, sl_res_micro.flg_tsq, sl_res_micro.prova, "
        sSql = sSql & " descr_produto, descr_proven, " & tabela_aux & ".utente, sl_res_micro.cod_carac_micro, "
        sSql = sSql & " sl_tbf_carac_micro.descr_carac_micro "
    End If
    
    sSql = sSql & " FROM sl_requis,sl_realiza, sl_req_prod, sl_res_micro, sl_microrg," & tabela_aux & ",sl_produto,sl_proven, sl_tbf_carac_micro "
    
    
    If ListaPerfis.ListCount > 0 Then
        sSql = sSql & " , sl_perfis "
    End If
    If CbSensib.ListIndex <> mediComboValorNull Or (ListaAntibio.ListCount > 0 And CkRestringeAntibio.value = vbChecked) Then
        sSql = sSql & " , sl_res_tsq, sl_antibio "
    End If
    
    sSql = sSql & " WHERE "
    sSql = sSql & " sl_requis.n_req = sl_realiza.n_req AND sl_realiza.seq_realiza = sl_res_micro.seq_realiza and "
    sSql = sSql & " sl_requis.n_req = sl_req_prod.n_req(+) and "
    sSql = sSql & " sl_req_prod.cod_prod = sl_produto.cod_produto(+) and "
    sSql = sSql & " sl_requis.cod_proven = sl_proven.cod_proven (+) AND "
    sSql = sSql & " sl_requis.seq_utente = " & tabela_aux & ".seq_utente and "
    sSql = sSql & " sl_res_micro.cod_micro = sl_microrg.cod_microrg and  sl_realiza.dt_val is not null AND "
    sSql = sSql & " sl_res_micro.cod_carac_micro = sl_tbf_carac_micro.cod_carac_micro (+)"
    sSql = sSql & " AND sl_res_micro.flg_imp = 'S' "
    
    If CbSensib.ListIndex <> mediComboValorNull Or (ListaAntibio.ListCount > 0 And CkRestringeAntibio.value = vbChecked) Then
        'tania.oliveira 13.04.2020 CHVNG-11007|Gx-10607
        sSql = sSql & " AND sl_res_tsq.res_sensib in 'R','S','P','N','I','-','+', 'E')"
    End If
    'verifica os campos preenchidos
    
    'se perfis preenchidos
    If ListaPerfis.ListCount > 0 Then
        sSql = sSql & " AND sl_realiza.cod_perfil = sl_perfis.cod_perfis AND sl_perfis.seq_perfis IN ( "
        For i = 0 To ListaPerfis.ListCount - 1
            sSql = sSql & ListaPerfis.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    'se caracteristicas de microrganismo preenchido
    If EcListaCarac.ListCount > 0 Then
        sSql = sSql & " AND sl_res_micro.cod_carac_micro IN ( "
        For i = 0 To EcListaCarac.ListCount - 1
            sSql = sSql & EcListaCarac.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    'se sexo preenchido
    If (EcDescrSexo.ListIndex <> -1) Then
        sSql = sSql & " AND " & tabela_aux & ".sexo_ute = " & EcDescrSexo.ListIndex
    End If
    
    'Data preenchida
    If Option1(0).value = True Then
        sSql = sSql & " AND sl_requis.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    Else
        sSql = sSql & " AND sl_realiza.dt_val BETWEEN " & BL_TrataDataParaBD(EcDtIni) & " AND " & BL_TrataDataParaBD(EcDtFim)
    End If
    sSql = sSql & " AND sl_realiza.flg_estado in(" & gEstadoAnaImpressa & "," & gEstadoAnaValidacaoMedica & ")"
    
    'Situa��o preenchida?
    If ListaSituacao.ListCount > 0 Then
        sSql = sSql & " AND sl_requis.T_sit IN ("
        For i = 0 To ListaSituacao.ListCount - 1
            sSql = sSql & ListaSituacao.ItemData(i) & ", "
        Next i
        sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
    End If
    
    'Urg�ncia preenchida?
    If (CbUrgencia.ListIndex <> -1) Then
        sSql = sSql & " AND sl_requis.T_urg=" & BL_TrataStringParaBD(left(CbUrgencia.text, 1))
    End If
    
    'Urg�ncia preenchida?
    If Trim(EcUtente) <> "" Then
        sSql = sSql & " AND " & tabela_aux & ".utente = " & BL_TrataStringParaBD(EcUtente)
    End If
    
    'C�digo da Proveni�ncia do pedido de an�lises preenchido?
    Dim StrProven As String
    StrProven = ""
    For i = 0 To ListaProven.ListCount - 1
        StrProven = StrProven & BL_TrataStringParaBD(BL_SelCodigo("sl_proven", "cod_proven", "seq_proven", ListaProven.ItemData(i))) & ","
    Next i
    If StrProven <> "" Then
        'Remove a virgula do final da express�o
        StrProven = Mid(StrProven, 1, Len(StrProven) - 1)
        sSql = sSql & " AND sl_requis.cod_proven in ( " & StrProven & " )"
    End If
    
    'C�digo do produto preenchido?
    Dim STRProduto As String
    STRProduto = ""
    For i = 0 To ListaProduto.ListCount - 1
        STRProduto = STRProduto & BL_TrataStringParaBD(BL_SelCodigo("sl_produto", "cod_produto", "seq_produto", ListaProduto.ItemData(i))) & ","
    Next i
    If STRProduto <> "" Then
        'Remove a virgula do final da express�o
        STRProduto = Mid(STRProduto, 1, Len(STRProduto) - 1)
        sSql = sSql & " AND sl_req_prod.cod_prod in ( " & STRProduto & " )"
    End If
    
    'C�digo do microrganismo preenchido?
    Dim StrMicrorg As String
    StrMicrorg = ""
    For i = 0 To ListaMicrorg.ListCount - 1
        StrMicrorg = StrMicrorg & BL_TrataStringParaBD(BL_SelCodigo("sl_microrg", "cod_microrg", "seq_microrg", ListaMicrorg.ItemData(i))) & ","
    Next i
    If StrMicrorg <> "" Then
        'Remove a virgula do final da express�o
        StrMicrorg = Mid(StrMicrorg, 1, Len(StrMicrorg) - 1)
        sSql = sSql & " AND sl_res_micro.cod_micro in ( " & StrMicrorg & " )"
    End If
    
    'Sensibilidade ou lista de antibioticos preenchida?
    If CbSensib.ListIndex <> mediComboValorNull Or (ListaAntibio.ListCount > 0 And CkRestringeAntibio.value = vbChecked) Then
    
        sSql = sSql & " AND sl_res_micro.seq_realiza = sl_res_tsq.seq_realiza and sl_res_micro.cod_micro = sl_res_tsq.cod_micro and " & _
                " sl_res_tsq.cod_antib = sl_antibio.cod_antibio(+) "
        
        If CbSensib.ListIndex <> mediComboValorNull Then
            sSql = sSql & " AND sl_res_tsq.res_sensib = " & BL_TrataStringParaBD(UCase(Mid(CbSensib.text, 1, 1)))
        End If
        
        If ListaAntibio.ListCount > 0 And CkRestringeAntibio.value = vbChecked Then
            'LISTA DE ANTIBIOTICOS:

            'Forma a Lista de antibioticos seleccionados para o crit�rio do ssql
            ListaAntibioticos = ""
            For i = 0 To ListaAntibio.ListCount - 1
                ListaAntibioticos = ListaAntibioticos & ListaAntibio.ItemData(i) & ","
            Next i
            'Remove a virgula do final da express�o
            ListaAntibioticos = Mid(ListaAntibioticos, 1, Len(ListaAntibioticos) - 1)

            sSql = sSql & " AND sl_antibio.seq_antibio in (" & ListaAntibioticos & ")"
        End If
    End If
    
    'local preenchido?
    If CbLocal.ListIndex <> -1 Then
        sSql = sSql & " AND sl_requis.cod_local=" & CbLocal.ItemData(CbLocal.ListIndex)
    End If
    
    ' SE IDADE PREENCHIDA
    If EcIdadeFim <> "" And EcIdadeIni <> "" And Opt1(0).value = True Then
        dt_nasc_ini = DateAdd("YYYY", -EcIdadeIni, Bg_DaData_ADO)
        dt_nasc_fim = DateAdd("YYYY", -EcIdadeFim, Bg_DaData_ADO)
        sSql = sSql & " AND (" & tabela_aux & ".dt_nasc_ute between " & BL_TrataDataParaBD(dt_nasc_fim) & " AND " & BL_TrataDataParaBD(dt_nasc_ini) & ")"
    ElseIf EcIdadeFim <> "" And EcIdadeIni <> "" And Opt1(1).value = True Then
        sSql = sSql & " AND (" & tabela_aux & ".dt_nasc_ute between (SL_REQUIS.DT_CHEGA - " & EcIdadeFim & ") AND (SL_REQUIS.DT_CHEGA -" & EcIdadeIni & "))"
    End If
    'Se Grupo preenchido
    If EcCodGrupo.text <> "" Then
        sSql = sSql & " and (sl_requis.gr_ana = '" & EcCodGrupo.text & "'  or sl_requis.gr_ana like '%;" & EcCodGrupo.text & "' or sl_requis.gr_ana like '" & EcCodGrupo.text & ";%'"
        sSql = sSql & " or sl_requis.gr_ana like '%;" & EcCodGrupo.text & ";%')"
    End If
    If gSGBD = gOracle Then
        RetornaQuery = sSql & " ORDER BY seq_utente, cod_micro, dt_chega, n_req "
    ElseIf gSGBD = gSqlServer Then
        RetornaQuery = sSql & " ORDER BY sl_Requis.seq_utente, cod_micro, sl_requis.dt_chega, sl_requis.n_req "
    End If
End Function


' -----------------------------------------------------------------------

' PREENCHE AS ESTRUTURAS UTENTES,REQUISICOES E MICRORGANISMOS

' -----------------------------------------------------------------------
Private Sub PreencheEstruturas(Prova As String, seq_utente As Long, n_req As Long, seq_realiza As Long, _
                            cod_micro As String, descr_microrg As String, _
                            N_Res As Integer, nome_ute As String, flg_tsq As String, _
                            descr_produto As String, descr_proven As String, Utente As String, dt_chega As String, _
                            cod_carac_micro As Integer, descr_carac_micro As String)
    Dim idxU As Long
    Dim idxR As Long
    Dim idxM As Long
    Dim existe_ute As Boolean
    Dim existe_req As Boolean
    Dim existe_micro As Boolean
    Dim sSql As String
    Dim rsAntib As New ADODB.recordset
    Dim agrupaResistencias As String
    
    'If CkAgruparResistentes.Value = vbChecked Then
    '    agrupaResistencias = " Decode(sl_res_tsq.res_sensib, 'R','R','I','R','S','S',sl_res_tsq.res_sensib) "
    'Else
        agrupaResistencias = " sl_res_tsq.res_sensib "
    'End If
    
    ' --------------------------------------------------
    ' UTENTES
    ' --------------------------------------------------
    existe_ute = False
    For idxU = 1 To TotalUte
        If EstrutUte(idxU).seq_utente = seq_utente Then
            existe_ute = True
            Exit For
        End If
    Next
    If existe_ute = False Then
        TotalUte = TotalUte + 1
        idxU = TotalUte
        ReDim Preserve EstrutUte(idxU)
        EstrutUte(idxU).nome_ute = nome_ute
        EstrutUte(idxU).seq_utente = seq_utente
        EstrutUte(idxU).Utente = Utente
        
        EstrutUte(idxU).totalReq = 0
        ReDim EstrutUte(idxU).EstrutReq(0)
        
        EstrutUte(idxU).TotalMiUte = 0
        ReDim EstrutUte(idxU).EstrutMicroUte(0)
    End If
    
    ' --------------------------------------------------
    ' REQUISICOES
    ' --------------------------------------------------
    existe_req = False
    For idxR = 1 To EstrutUte(idxU).totalReq
        If EstrutUte(idxU).EstrutReq(idxR).n_req = n_req Then
            existe_req = True
            Exit For
        End If
    Next
    If existe_req = False Then
        EstrutUte(idxU).totalReq = EstrutUte(idxU).totalReq + 1
        idxR = EstrutUte(idxU).totalReq
        ReDim Preserve EstrutUte(idxU).EstrutReq(idxR)
        EstrutUte(idxU).EstrutReq(idxR).n_req = n_req
        EstrutUte(idxU).EstrutReq(idxR).descr_proven = descr_proven
        EstrutUte(idxU).EstrutReq(idxR).dt_chega = dt_chega
        EstrutUte(idxU).EstrutReq(idxR).totalMicro = 0
        ReDim EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(0)
    End If

    ' --------------------------------------------------
    ' MICRORGANISMOS
    ' --------------------------------------------------
    existe_micro = False
    For idxM = 1 To EstrutUte(idxU).EstrutReq(idxR).totalMicro
        If EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).cod_micro = cod_micro Then
            existe_micro = True
            Exit For
        End If
    Next
    If existe_micro = False Then
        EstrutUte(idxU).EstrutReq(idxR).totalMicro = EstrutUte(idxU).EstrutReq(idxR).totalMicro + 1
        idxM = EstrutUte(idxU).EstrutReq(idxR).totalMicro
        ReDim Preserve EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM)
        EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).seq_realiza = seq_realiza
        EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).cod_micro = cod_micro
        EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_micro = descr_microrg
        EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_produto = descr_produto
        EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).flg_tsq = flg_tsq
        EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).id_prova = Prova
        EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).N_Res = N_Res
        EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).cod_carac_micro = cod_carac_micro
        EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_carac_micro = descr_carac_micro
        EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).TotalAntibioticos = 0
        ReDim EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(0)
    End If
    
    ' -----------------------------------------------
    ' SELECCIONA OS ANTIBIOTICOS DESSE MICRORGANISMO
    ' -----------------------------------------------
    rsAntib.CursorLocation = adUseServer
    rsAntib.CursorType = adOpenStatic
    sSql = "SELECT  sl_res_tsq.cod_antib, abrev_antibio, descr_antibio," & agrupaResistencias & " res_sensib "
    sSql = sSql & "FROM sl_res_tsq, sl_antibio "
    'tania.oliveira 13.04.2020 CHVNG-11007|Gx-10607
    sSql = sSql & "WHERE sl_res_tsq.res_sensib in ('R','S','P','N','I','-','+', 'E') AND sl_res_tsq.res_sensib in ('R','S','P','N','I','-','+', 'E') AND  sl_res_tsq.cod_antib = sl_antibio.cod_antibio and "
    sSql = sSql & "seq_realiza = " & seq_realiza & " and cod_micro = " & BL_TrataStringParaBD(cod_micro)
    
        
    If ListaAntibio.ListCount > 0 And CkRestringeAntibio.value = vbChecked Then
        Dim ListaAntibioticos As String
        Dim i As Integer
        'LISTA DE ANTIBIOTICOS:

        'Forma a Lista de antibioticos seleccionados para o crit�rio do ssql
        ListaAntibioticos = ""
        For i = 0 To ListaAntibio.ListCount - 1
            ListaAntibioticos = ListaAntibioticos & ListaAntibio.ItemData(i) & ","
        Next i
        'Remove a virgula do final da express�o
        ListaAntibioticos = Mid(ListaAntibioticos, 1, Len(ListaAntibioticos) - 1)

        sSql = sSql & " AND sl_antibio.seq_antibio in (" & ListaAntibioticos & ")"
    End If
    
    
    sSql = sSql & " ORDER BY cod_antib"
    
    rsAntib.Open sSql, gConexao
    If rsAntib.RecordCount > 0 Then
        While Not rsAntib.EOF
            PreencheEstrutAntibio idxU, idxR, idxM, rsAntib!cod_antib, BL_HandleNull(rsAntib!abrev_antibio, "---"), _
                                BL_HandleNull(rsAntib!descr_antibio, ""), BL_HandleNull(rsAntib!res_sensib, "")
            rsAntib.MoveNext
        Wend
    End If
    rsAntib.Close
    Set rsAntib = Nothing
End Sub

' -----------------------------------------------------------------------

' PREENCHE A ESTRUTURA COM ANTIBIOTICOS

' -----------------------------------------------------------------------
Private Sub PreencheEstrutAntibio(idxU As Long, idxR As Long, idxM As Long, cod_antib As String, _
                                  abrev_antibio As String, descr_antibio As String, res_sensib As String)
    Dim idxA As Long
    For idxA = 1 To EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).TotalAntibioticos
        If EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(idxA).cod_antib = cod_antib Then
            Exit Sub
        End If
    Next
    EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).TotalAntibioticos = EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).TotalAntibioticos + 1
    idxA = EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).TotalAntibioticos
    ReDim Preserve EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(idxA)
    EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(idxA).cod_antib = cod_antib
    EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(idxA).abrev_antib = abrev_antibio
    EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(idxA).descr_antib = descr_antibio
    EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(idxA).res_sensib = res_sensib
End Sub


' -----------------------------------------------------------------------

' PRECORRE A ESTRUTURA DE UTENTES � PROCURA DE INFEC��ES

' -----------------------------------------------------------------------
Private Sub ContaInfeccoes()
    Dim idxU As Long
    Dim idxR As Long
    Dim idxM As Long
    Dim micro As Long
    Dim flg_inseriu As Boolean
    Dim iR As Long
    Dim iM As Long
    Dim j As Long
    Dim sSql As String
    
    'PARA CADA UTENTE.....
    For idxU = 1 To TotalUte
        'PARA CADA REQUISICAO....
        For idxR = 1 To EstrutUte(idxU).totalReq
            'PARA CADA MICRORGANISMO....
            For idxM = 1 To EstrutUte(idxU).EstrutReq(idxR).totalMicro
                
                ' SE PROVA FOR  <> SEM FACT OU SE UTIL QUISER CONTAR COM AS SEM FACT
                If (EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).id_prova = "4" And CkContarSemFact.value = vbChecked) Or EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).id_prova <> "4" Then
                    'ADICIONA MICRO A ESTRUTURA DE MICRORGANISMOS EXISTENTES PARA UTENTE
                    micro = AdicionaMicroUtente(idxU, EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).cod_micro)
                    
                    If ExisteInfeccao(idxU, idxR, idxM, micro) = True Then
                        ' INSERE NA BD REGISTO
                        flg_inseriu = InsereBD(idxU, idxR, idxM)
                        EstrutUte(idxU).EstrutMicroUte(micro).numInfecoes = EstrutUte(idxU).EstrutMicroUte(micro).numInfecoes + 1
                        'Debug.Print "I " & EstrutUte(idxU).seq_utente & " - "; EstrutUte(idxU).EstrutMicroUte(micro).cod_micro
                    Else
                        'Debug.Print "D " & EstrutUte(idxU).seq_utente & " - "; EstrutUte(idxU).EstrutMicroUte(micro).cod_micro
                    End If
                    sSql = "INSERT INTO sl_cr_estatmicrorg_carac(n_req, seq_realiza, cod_microrg, cod_carac_micro, descr_carac_micro, nome_computador, num_sessao) VALUES("
                    sSql = sSql & EstrutUte(idxU).EstrutReq(idxR).n_req & "," & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).seq_realiza & ","
                    sSql = sSql & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).cod_micro) & ","
                    sSql = sSql & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).cod_carac_micro & ","
                    sSql = sSql & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_carac_micro) & ","
                    sSql = sSql & BL_TrataStringParaBD(gComputador) & ","
                    sSql = sSql & gNumeroSessao & ") "
                    BG_ExecutaQuery_ADO sSql
                    
                    If EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).id_prova = "4" Then
                        EstrutUte(idxU).EstrutMicroUte(micro).SemFac = EstrutUte(idxU).EstrutMicroUte(micro).SemFac + 1
                    End If
                    
                    EstrutUte(idxU).EstrutMicroUte(micro).numMicro = EstrutUte(idxU).EstrutMicroUte(micro).numMicro + 1
                End If
            Next
        Next
        
'        '  MARTELADA - SE NUM INFECCOES  = 0 MAS NUM MICRO > 0 ENTAO EXISTE 1 INFECCAO E OS ANTIBIOGRAMAS SAO TODOS IGUAIS
'        For j = 1 To EstrutUte(idxU).TotalMiUte
'            If EstrutUte(idxU).EstrutMicroUte(j).numMicro > 0 And EstrutUte(idxU).EstrutMicroUte(j).numInfecoes = 0 Then
'                ' INSERE NA BD REGISTO
'                For iR = 1 To EstrutUte(idxU).totalReq
'                    For iM = 1 To EstrutUte(idxU).EstrutReq(iR).totalMicro
'                        If EstrutUte(idxU).EstrutReq(iR).EstrutMicro(iM).cod_micro = EstrutUte(idxU).EstrutMicroUte(j).cod_micro And EstrutUte(idxU).EstrutMicroUte(j).numInfecoes = 0 Then
'                            flg_inseriu = InsereBD(idxU, iR, iM)
'                            EstrutUte(idxU).EstrutMicroUte(j).numInfecoes = 1
'                        End If
'                    Next
'                Next
'            End If
'        Next
    Next
End Sub

' -----------------------------------------------------------------------

' VERIFICA SE PARA MICRO + ANTIB EM CAUSA SE E CONSIDERADO INFECCAO
' SE EXISTIR ALGUM MICRO+ANTIB DE OUTRA REQUISICAO ONDE ESTE SE ENCAIXE,
' � CONSIDERADO O MESMO MICRO PELO QUE N�O � CONSIDERADO INFECCAO.

' -----------------------------------------------------------------------
Private Function ExisteInfeccao(idxU As Long, idxR As Long, idxM As Long, idxMU As Long) As Boolean
    Dim total As Long
    Dim i As Long
    Dim j As Long
    Dim k As Long
    Dim idxA As Long
    Dim numErrosMinor As Long
    Dim numErrosMajor As Long
    Dim flg_antibioticoExiste As Boolean
    Dim flg_antibiogramaExiste As Boolean
    
    If gLAB = "CHVNG" And CDate(EcDtIni.text) >= CDate("01-01-2014") Then
        If EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).cod_carac_micro = 2 Then
            ExisteInfeccao = False
        Else
            ExisteInfeccao = True
        End If
        Exit Function
    End If
    
    If (EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).id_prova = "4" And CkContarSemFact.value = vbUnchecked) Then
        ExisteInfeccao = False
        Exit Function
    ElseIf EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).TotalAntibioticos = 0 And EcIntervaloTempo > 0 Then
        ExisteInfeccao = False
        Exit Function
    End If
        
    'PARA CADA REQUISICAO DO UTENTE EM CAUSA......
    For i = 1 To EstrutUte(idxU).totalReq
        ' EXCEPTO A QUE ESTAMOS A ANALISAR....
        If i <> idxR Then
            flg_antibiogramaExiste = False
            numErrosMinor = 0
            numErrosMajor = 0
            ' APENAS ANALISE AS REQUISICOES QUE TENHAM DIFEREN�A DE DIAS DE ACORDO COM INDICADO.
            'HCVP-2472 - correcao com as datas para duplicados
            If AvaliaDatas(EstrutUte(idxU).EstrutReq(i).dt_chega, EstrutUte(idxU).EstrutReq(idxR).dt_chega) = True Then
                'VERIFICAMOS SE TEM MICRORGANISMO EM CAUSA....
                For j = 1 To EstrutUte(idxU).EstrutReq(i).totalMicro
                    If EstrutUte(idxU).EstrutReq(i).EstrutMicro(j).cod_micro = EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).cod_micro Then
                        ' SE PROVA FOR  <> SEM FACT OU SE UTIL QUISER CONTAR COM AS SEM FACT
                        If (EstrutUte(idxU).EstrutReq(i).EstrutMicro(j).id_prova = "4" And CkContarSemFact.value = vbChecked) Or EstrutUte(idxU).EstrutReq(i).EstrutMicro(j).id_prova <> "4" Then
                            If EstrutUte(idxU).EstrutReq(i).EstrutMicro(j).TotalAntibioticos > 0 Then
                                ' ANALISA CADA ANTIBIOTICO
                                For idxA = 1 To EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).TotalAntibioticos
                                    flg_antibioticoExiste = True
                                    For k = 1 To EstrutUte(idxU).EstrutReq(i).EstrutMicro(j).TotalAntibioticos
                                        ' SE ANTIBIOTICO EXISTIR E SENSIBILIDADE FOR IGUAL.....COLOCA FLAG A TRUE
                                        If EstrutUte(idxU).EstrutReq(i).EstrutMicro(j).EstrutAntib(k).cod_antib = EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(idxA).cod_antib Then
                                            If EstrutUte(idxU).EstrutReq(i).EstrutMicro(j).EstrutAntib(k).res_sensib = EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(idxA).res_sensib Then
                                                flg_antibioticoExiste = True
                                                Exit For
                                            Else
                                                If VerificaErroMinor(EstrutUte(idxU).EstrutReq(i).EstrutMicro(j).EstrutAntib(k).res_sensib, EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(idxA).res_sensib) = True Then
                                                    numErrosMinor = numErrosMinor + 1
                                                    If numErrosMinor > BL_HandleNull(EcErrosMinor, 0) Then
                                                        flg_antibioticoExiste = False
                                                        Exit For
                                                    Else
                                                        flg_antibioticoExiste = True
                                                    End If
                                                ElseIf VerificaErroMajor(EstrutUte(idxU).EstrutReq(i).EstrutMicro(j).EstrutAntib(k).res_sensib, EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(idxA).res_sensib) = True Then
                                                    numErrosMajor = numErrosMajor + 1
                                                    If numErrosMajor > BL_HandleNull(EcErrosMajor, 0) Then
                                                        flg_antibioticoExiste = False
                                                        Exit For
                                                    Else
                                                        flg_antibioticoExiste = True
                                                    End If
                                                Else
                                                    flg_antibioticoExiste = False
                                                    Exit For
                                                End If
                                                Exit For
                                            End If
                                        End If
                                    Next
                                    
                                    If flg_antibioticoExiste = False Then
                                        flg_antibiogramaExiste = False
                                        Exit For
                                    Else
                                        flg_antibiogramaExiste = True
                                    End If
                                        
                                Next
                            Else
                                flg_antibiogramaExiste = True
                            End If
                        Else
                            flg_antibiogramaExiste = False
                        End If
                        
                        ' SE ANTIBIOGRAMA EXISTE NOUTRA REQUISICAO E � IGUAL ENTAO NAO SE TRATA DE UMA INFEC��O.SAI.
                        If flg_antibiogramaExiste = True Then
                            
                            ExisteInfeccao = AcrescentaReqAoMicro(idxU, idxMU, EstrutUte(idxU).EstrutReq(idxR).n_req, EstrutUte(idxU).EstrutReq(i).n_req)
                            Exit Function
                        End If
                    End If
                Next
            Else
                flg_antibiogramaExiste = False
            End If
        End If
    Next
    If flg_antibiogramaExiste = False Then
        ExisteInfeccao = True
    End If
End Function


' -----------------------------------------------------------------------

' ADICIONA MICRO COMO 'ANALISADO' PARA UTENTE EM CAUSA

' -----------------------------------------------------------------------
Private Function AdicionaMicroUtente(idxU As Long, cod_micro As String) As Long
    Dim i As Long
    For i = 1 To EstrutUte(idxU).TotalMiUte
        If EstrutUte(idxU).EstrutMicroUte(i).cod_micro = cod_micro Then
            AdicionaMicroUtente = i
            Exit Function
        End If
    Next
    
    EstrutUte(idxU).TotalMiUte = EstrutUte(idxU).TotalMiUte + 1
    ReDim Preserve EstrutUte(idxU).EstrutMicroUte(EstrutUte(idxU).TotalMiUte)
    
    EstrutUte(idxU).EstrutMicroUte(EstrutUte(idxU).TotalMiUte).cod_micro = cod_micro
    EstrutUte(idxU).EstrutMicroUte(EstrutUte(idxU).TotalMiUte).numInfecoes = 0
    EstrutUte(idxU).EstrutMicroUte(EstrutUte(idxU).TotalMiUte).numMicro = 0
    EstrutUte(idxU).EstrutMicroUte(EstrutUte(idxU).TotalMiUte).SemFac = 0
    EstrutUte(idxU).EstrutMicroUte(EstrutUte(idxU).TotalMiUte).totalReq = 0
    ReDim EstrutUte(idxU).EstrutMicroUte(EstrutUte(idxU).TotalMiUte).requisicoes(0)
    AdicionaMicroUtente = EstrutUte(idxU).TotalMiUte
End Function


' -----------------------------------------------------------------------

' INSERE REGISTOS NA TABELA DO REPORT.

' -----------------------------------------------------------------------

Private Function InsereBD(idxU As Long, idxR As Long, idxM As Long) As Boolean
                            
    'insere novo microrganismo e estrutura de antibiograma
    Dim SqlIns As String
    Dim sql As String
    Dim rsAntib As ADODB.recordset
    Dim indice As Long
    Dim ListaAntibioticos As String
    Dim i As Long
    
    On Error GoTo TrataErro
    
    If EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).TotalAntibioticos > 0 Then
        'INSERE TABELA DE CABE�ALHO
        SqlIns = "INSERT INTO SL_CR_ESTATMICRORG ("
        If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
            SqlIns = SqlIns & " DESCR_PRODUTO,DESCR_PROVEN,"
        ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
            SqlIns = SqlIns & " DESCR_PRODUTO,"
        ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
            SqlIns = SqlIns & " DESCR_PROVEN,"
        End If
        SqlIns = SqlIns & "N_REQ,SEQ_REALIZA,COD_MICRORG, DESCR_MICRORG,N_RES,NOME_UTE,NOME_COMPUTADOR, UTENTE, NUM_SESSAO) VALUES ("
        If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_produto) & "," & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).descr_proven) & ","
        ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_produto) & ","
        ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).descr_proven) & ","
        End If
        If CkAgruparMicrorg.value Then
            SqlIns = SqlIns & EstrutUte(idxU).EstrutReq(idxR).n_req & "," & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).seq_realiza & "," & BL_TrataStringParaBD("CODMICRO") & "," & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_micro) & _
                "," & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).N_Res & "," & BL_TrataStringParaBD(EstrutUte(idxU).nome_ute) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(EstrutUte(idxU).Utente) & ","
        Else
            SqlIns = SqlIns & EstrutUte(idxU).EstrutReq(idxR).n_req & "," & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).seq_realiza & "," & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).cod_micro) & "," & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_micro) & _
                "," & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).N_Res & "," & BL_TrataStringParaBD(EstrutUte(idxU).nome_ute) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(EstrutUte(idxU).Utente) & ","
        End If
        SqlIns = SqlIns & gNumeroSessao & ")"
        
        BG_ExecutaQuery_ADO SqlIns
        BG_Trata_BDErro
        
        For i = 1 To EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).TotalAntibioticos
            SqlIns = "INSERT INTO SL_CR_ESTATMICRORGDET ("
            If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
                SqlIns = SqlIns & " DESCR_PRODUTO,DESCR_PROVEN,"
            ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
                SqlIns = SqlIns & " DESCR_PRODUTO,"
            ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
                SqlIns = SqlIns & " DESCR_PROVEN,"
            End If
            SqlIns = SqlIns & " N_REQ,SEQ_REALIZA,COD_MICRORG, DESCR_MICRORG, N_RES, COD_ANTIBIO, DESCR_ANTIBIO,"
            SqlIns = SqlIns & " RES_SENSIB, NOME_UTE,UTENTE, NOME_COMPUTADOR, NUM_SESSAO) VALUES( "
            
            If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
                SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_produto) & ", "
                SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).descr_proven) & ", "
            ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
                SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_produto) & ", "
            ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
                SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).descr_proven) & ", "
            End If
            SqlIns = SqlIns & EstrutUte(idxU).EstrutReq(idxR).n_req & ", "
            SqlIns = SqlIns & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).seq_realiza & ", "
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).cod_micro) & ", "
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_micro) & ", "
            SqlIns = SqlIns & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).N_Res & ", "
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(i).cod_antib) & ", "
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(i).abrev_antib) & ", "
            If CkAgruparResistentes.value = vbChecked Then
                SqlIns = SqlIns & BL_TrataStringParaBD(Replace(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(i).res_sensib, "I", "R")) & ", "
            Else
                SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).EstrutAntib(i).res_sensib) & ", "
            End If
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).nome_ute) & ", "
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).Utente) & ", "
            SqlIns = SqlIns & BL_TrataStringParaBD(gComputador) & ", "
            SqlIns = SqlIns & gNumeroSessao & ") "
            
            BG_ExecutaQuery_ADO SqlIns
        Next
    Else
            'INSERE TABELA DE CABE�ALHO
            SqlIns = "INSERT INTO SL_CR_ESTATMICRORG ("
            If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
                SqlIns = SqlIns & " DESCR_PRODUTO,DESCR_PROVEN,"
            ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
                SqlIns = SqlIns & " DESCR_PRODUTO,"
            ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
                SqlIns = SqlIns & " DESCR_PROVEN,"
            End If
            SqlIns = SqlIns & "N_REQ,SEQ_REALIZA,COD_MICRORG, DESCR_MICRORG,N_RES,NOME_UTE,NOME_COMPUTADOR, UTENTE, num_sessao) VALUES ("
            If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
                SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_produto) & "," & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).descr_proven) & ","
            ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
                SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_produto) & ","
            ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
                SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).descr_proven) & ","
            End If
            If CkAgruparMicrorg.value Then
                SqlIns = SqlIns & EstrutUte(idxU).EstrutReq(idxR).n_req & "," & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).seq_realiza & "," & BL_TrataStringParaBD("CODMICRO") & "," & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_micro) & _
                    "," & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).N_Res & "," & BL_TrataStringParaBD(EstrutUte(idxU).nome_ute) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(EstrutUte(idxU).Utente) & ","
            Else
                SqlIns = SqlIns & EstrutUte(idxU).EstrutReq(idxR).n_req & "," & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).seq_realiza & "," & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).cod_micro) & "," & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_micro) & _
                    "," & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).N_Res & "," & BL_TrataStringParaBD(EstrutUte(idxU).nome_ute) & "," & BL_TrataStringParaBD(BG_SYS_GetComputerName) & "," & BL_TrataStringParaBD(EstrutUte(idxU).Utente) & ","
            End If
            SqlIns = SqlIns & gNumeroSessao & ")"
            BG_ExecutaQuery_ADO SqlIns
            BG_Trata_BDErro
                
            
            SqlIns = "INSERT INTO SL_CR_ESTATMICRORGDET ("
            If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
                SqlIns = SqlIns & " DESCR_PRODUTO,DESCR_PROVEN,"
            ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
                SqlIns = SqlIns & " DESCR_PRODUTO,"
            ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
                SqlIns = SqlIns & " DESCR_PROVEN,"
            End If
            SqlIns = SqlIns & " N_REQ,SEQ_REALIZA,COD_MICRORG, DESCR_MICRORG, N_RES, "
            SqlIns = SqlIns & " NOME_UTE,UTENTE, NOME_COMPUTADOR, NUM_SESSAO) VALUES( "
            
            If Flg_OrderProd.value = 1 And Flg_OrderProven.value = 1 Then
                SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_produto) & ", "
                SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).descr_proven) & ", "
            ElseIf Flg_OrderProd.value = 1 And Flg_OrderProven.value = 0 Then
                SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_produto) & ", "
            ElseIf Flg_OrderProd.value = 0 And Flg_OrderProven.value = 1 Then
                SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).descr_proven) & ", "
            End If
            SqlIns = SqlIns & EstrutUte(idxU).EstrutReq(idxR).n_req & ", "
            SqlIns = SqlIns & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).seq_realiza & ", "
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).cod_micro) & ", "
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).descr_micro) & ", "
            SqlIns = SqlIns & EstrutUte(idxU).EstrutReq(idxR).EstrutMicro(idxM).N_Res & ", "
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).nome_ute) & ", "
            SqlIns = SqlIns & BL_TrataStringParaBD(EstrutUte(idxU).Utente) & ", "
            SqlIns = SqlIns & BL_TrataStringParaBD(gComputador) & ", "
            SqlIns = SqlIns & gNumeroSessao & ")"
            
            BG_ExecutaQuery_ADO SqlIns
    End If
    Exit Function
    
TrataErro:

    BG_LogFile_Erros "FormEstatMicrorgTempo (InsereBD): " & vbCrLf & " Sql = " & sql & vbCrLf & Err.Number & "-" & Err.Description
    InsereBD = False
    Exit Function
    Resume Next
End Function

Private Function AvaliaDatas(Data1 As String, Data2 As String) As Boolean
    If Abs(DateDiff("d", Data1, Data2)) > CInt(BL_HandleNull(CLng(EcIntervaloTempo.text), 0)) Then
        AvaliaDatas = False
    Else
        AvaliaDatas = True
    End If
End Function


Private Function AcrescentaReqAoMicro(idxU As Long, idxMU As Long, n_req_new As String, n_req As String) As Boolean
    Dim i As Long
    Dim j As Long
    Dim flg_inseriu As Boolean
    AcrescentaReqAoMicro = True
    flg_inseriu = False
    
    '  SE REQUISICAO JA EXISTE SAI.
    For i = 1 To EstrutUte(idxU).EstrutMicroUte(idxMU).totalReq
        For j = 1 To EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(i).totalReq
            If EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(i).NReq(j) = n_req_new Then
                AcrescentaReqAoMicro = False
                Exit Function
            End If
        Next
    Next
    
    ' VERIFICA SE A OUTRA JA FOI ENCARADA COMO INFECAO
    For i = 1 To EstrutUte(idxU).EstrutMicroUte(idxMU).totalReq
        For j = 1 To EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(i).totalReq
            If EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(i).NReq(j) = n_req Then
                AcrescentaReqAoMicro = False
                EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(i).totalReq = EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(i).totalReq + 1
                ReDim Preserve EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(i).NReq(EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(i).totalReq)
                EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(i).NReq(EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(i).totalReq) = n_req_new
                Exit Function
            End If
        Next
    Next
    
    EstrutUte(idxU).EstrutMicroUte(idxMU).totalReq = EstrutUte(idxU).EstrutMicroUte(idxMU).totalReq + 1
    ReDim Preserve EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(EstrutUte(idxU).EstrutMicroUte(idxMU).totalReq)
    EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(EstrutUte(idxU).EstrutMicroUte(idxMU).totalReq).totalReq = 1
    ReDim Preserve EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(EstrutUte(idxU).EstrutMicroUte(idxMU).totalReq).NReq(1)
    EstrutUte(idxU).EstrutMicroUte(idxMU).requisicoes(EstrutUte(idxU).EstrutMicroUte(idxMU).totalReq).NReq(1) = n_req_new
End Function
Private Sub BtPesquisaSituacao_Click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1000) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_t_sit"
    CamposEcran(1) = "cod_t_sit"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_t_sit"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_tbf_t_sit"
    CWhere = ""
    CampoPesquisa = "descr_t_sit"
        
    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_t_sit ", _
                                                                           " Situa��o")

    FormPesqRapidaAvancadaMultiSel.CkTodos.Visible = True
    FormPesqRapidaAvancadaMultiSel.Show vbModal
    
    CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
    If Not CancelouPesquisa Then
          
        For i = 1 To TotalElementosSel
            If ListaSituacao.ListCount = 0 Then
                ListaSituacao.AddItem BL_SelCodigo("sl_tbf_t_sit", "descr_t_sit", "cod_t_sit", Resultados(i))
                ListaSituacao.ItemData(0) = Resultados(i)
            Else
                ListaSituacao.AddItem BL_SelCodigo("sl_tbf_t_sit", "descr_t_sit", "cod_t_sit", Resultados(i))
                ListaSituacao.ItemData(ListaSituacao.NewIndex) = Resultados(i)
            End If
        Next i
    End If
End Sub




Private Sub PreencheCaracDefeito()
    Dim sSql As String
    Dim rsCarac As New ADODB.recordset
    sSql = "SELECT * FROM SL_TBF_CARAC_MICRO WHERE flg_estat_micro = 1 "
    rsCarac.Source = sSql
    If gModoDebug Then BG_LogFile_Erros sSql
    rsCarac.CursorLocation = adUseServer
    rsCarac.CursorType = adOpenStatic
    rsCarac.ActiveConnection = gConexao
    rsCarac.Open
        While Not rsCarac.EOF
            EcListaCarac.AddItem BL_HandleNull(rsCarac!descr_carac_micro, "")
            EcListaCarac.ItemData(EcListaCarac.NewIndex) = BL_HandleNull(rsCarac!cod_carac_micro, mediComboValorNull)
            rsCarac.MoveNext
        Wend
End Sub

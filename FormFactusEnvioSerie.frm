VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormFactusEnvioSerie 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFactusEnvioSerie"
   ClientHeight    =   9225
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13785
   Icon            =   "FormFactusEnvioSerie.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9225
   ScaleWidth      =   13785
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtValida 
      Caption         =   "Enviar"
      Enabled         =   0   'False
      Height          =   855
      Left            =   6000
      Picture         =   "FormFactusEnvioSerie.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   58
      ToolTipText     =   " Enviar para a Factura��o "
      Top             =   8280
      Width           =   1335
   End
   Begin VB.Frame Frame2 
      Height          =   4815
      Left            =   120
      TabIndex        =   56
      Top             =   3360
      Width           =   13575
      Begin VB.Frame FrameAccao 
         Height          =   1095
         Left            =   4560
         TabIndex        =   59
         Top             =   1440
         Width           =   3495
         Begin VB.Label LbAccao 
            Caption         =   " A Preencher Requisi��es...."
            Height          =   495
            Left            =   600
            TabIndex        =   60
            Top             =   360
            Width           =   2415
         End
      End
      Begin MSFlexGridLib.MSFlexGrid FgReq 
         Height          =   4455
         Left            =   240
         TabIndex        =   57
         Top             =   240
         Width           =   13095
         _ExtentX        =   23098
         _ExtentY        =   7858
         _Version        =   393216
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
         Appearance      =   0
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3255
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   13575
      Begin VB.TextBox EcDtFim 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   2280
         TabIndex        =   61
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcNome 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   33
         TabStop         =   0   'False
         Top             =   195
         Width           =   5655
      End
      Begin VB.TextBox EcUtente 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   2040
         TabIndex        =   32
         Top             =   540
         Width           =   1215
      End
      Begin VB.TextBox EcTipoUte 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   31
         Top             =   540
         Width           =   975
      End
      Begin VB.TextBox EcIdade 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   12600
         TabIndex        =   30
         Top             =   195
         Width           =   855
      End
      Begin VB.TextBox EcDataNasc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "d/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2070
            SubFormatType   =   3
         EndProperty
         Height          =   285
         Left            =   11160
         TabIndex        =   29
         Top             =   195
         Width           =   1455
      End
      Begin VB.CommandButton BtAlteraUtente 
         Height          =   375
         Left            =   6720
         Picture         =   "FormFactusEnvioSerie.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   28
         ToolTipText     =   "Alterar utente a facturar"
         Top             =   120
         Width           =   615
      End
      Begin VB.TextBox EcSexo 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   8400
         TabIndex        =   27
         Top             =   195
         Width           =   1335
      End
      Begin VB.TextBox EcDoenteProf 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8400
         TabIndex        =   26
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox EcReqAssoc 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   8400
         TabIndex        =   25
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox EcNumBenef 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   8400
         TabIndex        =   24
         Top             =   1440
         Width           =   1335
      End
      Begin VB.ComboBox CbTipoIsencao 
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         Height          =   315
         Left            =   8400
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   1005
         Width           =   1335
      End
      Begin VB.ComboBox CbHemodialise 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   11160
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   1035
         Width           =   1455
      End
      Begin VB.ComboBox CbConvencao 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   11160
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   600
         Width           =   1455
      End
      Begin VB.TextBox EcKm 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11160
         TabIndex        =   20
         Top             =   1800
         Width           =   615
      End
      Begin VB.ComboBox CbCodUrbano 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   11160
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   1440
         Width           =   1455
      End
      Begin VB.TextBox EcDescrPais 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   2480
         Width           =   3855
      End
      Begin VB.TextBox EcCodPais 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   17
         Top             =   2480
         Width           =   975
      End
      Begin VB.CommandButton BtPesqPais 
         Height          =   315
         Left            =   5880
         Picture         =   "FormFactusEnvioSerie.frx":1260
         Style           =   1  'Graphical
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   2480
         Width           =   375
      End
      Begin VB.TextBox EcNomeMed 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   2140
         Width           =   3855
      End
      Begin VB.TextBox EcCodMed 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   14
         Top             =   2140
         Width           =   975
      End
      Begin VB.CommandButton BtPesquisaMed 
         Height          =   315
         Left            =   5880
         Picture         =   "FormFactusEnvioSerie.frx":17EA
         Style           =   1  'Graphical
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de M�dicos  "
         Top             =   2140
         Width           =   375
      End
      Begin VB.TextBox EcDtIni 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   12
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcCodSala 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   11
         Top             =   1440
         Width           =   975
      End
      Begin VB.TextBox EcDescrSala 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   1440
         Width           =   3855
      End
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   5880
         Picture         =   "FormFactusEnvioSerie.frx":1D74
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   1440
         Width           =   375
      End
      Begin VB.TextBox EcNumReq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   285
         Left            =   4320
         TabIndex        =   8
         Top             =   540
         Width           =   975
      End
      Begin VB.TextBox EcEstado 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   5280
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   540
         Width           =   2055
      End
      Begin VB.TextBox EcCodEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   1800
         Width           =   975
      End
      Begin VB.TextBox EcDescrEFR 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   1800
         Width           =   3855
      End
      Begin VB.TextBox EcCodProven 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   4
         Top             =   2820
         Width           =   975
      End
      Begin VB.TextBox EcDescrProven 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   2820
         Width           =   3855
      End
      Begin VB.CommandButton BtPesqProven 
         Height          =   315
         Left            =   5880
         Picture         =   "FormFactusEnvioSerie.frx":22FE
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Salas ou Postos de Colheita "
         Top             =   2820
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   5880
         Picture         =   "FormFactusEnvioSerie.frx":2888
         Style           =   1  'Graphical
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   1800
         Width           =   375
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "e"
         Height          =   195
         Index           =   1
         Left            =   2100
         TabIndex        =   62
         Top             =   960
         Width           =   90
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         Height          =   195
         Left            =   120
         TabIndex        =   55
         Top             =   195
         Width           =   420
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "N� Interno"
         Height          =   195
         Left            =   120
         TabIndex        =   54
         Top             =   540
         Width           =   720
      End
      Begin VB.Label LbDataNasc 
         AutoSize        =   -1  'True
         Caption         =   "Data &Nasc."
         Height          =   195
         Left            =   9960
         TabIndex        =   53
         ToolTipText     =   "Data Nascimento"
         Top             =   195
         Width           =   810
      End
      Begin VB.Label LbFimSemana 
         Caption         =   "FIM SEMANA"
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   6960
         TabIndex        =   52
         Top             =   2880
         Width           =   1695
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Sexo"
         Height          =   195
         Left            =   7440
         TabIndex        =   51
         ToolTipText     =   "Data Nascimento"
         Top             =   195
         Width           =   360
      End
      Begin VB.Label Labe41 
         Caption         =   "Doente Prof."
         Height          =   255
         Index           =   2
         Left            =   7440
         TabIndex        =   50
         Top             =   1800
         Width           =   1500
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Nr. Posto"
         Height          =   195
         Index           =   16
         Left            =   7440
         TabIndex        =   49
         Top             =   600
         Width           =   660
      End
      Begin VB.Label Label11 
         Caption         =   "Nr. Benef."
         Height          =   255
         Left            =   7440
         TabIndex        =   48
         Top             =   1410
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Isen��o"
         Height          =   255
         Index           =   13
         Left            =   7440
         TabIndex        =   47
         Top             =   1005
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Hemodialise"
         Height          =   255
         Index           =   1
         Left            =   9960
         TabIndex        =   46
         Top             =   1035
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Conven��o"
         Height          =   255
         Index           =   0
         Left            =   9960
         TabIndex        =   45
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "C�digo Urbano"
         Height          =   255
         Left            =   9960
         TabIndex        =   44
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "Km"
         Height          =   255
         Left            =   9960
         TabIndex        =   43
         Top             =   1800
         Width           =   615
      End
      Begin VB.Label LbUtenteFact 
         Caption         =   "UTENTE ALTERADO PARA FACTURA��O"
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   8400
         TabIndex        =   42
         Top             =   2880
         Visible         =   0   'False
         Width           =   3615
      End
      Begin VB.Label LbCredenciais 
         Caption         =   "LbCredenciais"
         Height          =   255
         Left            =   11400
         TabIndex        =   41
         Top             =   2880
         Width           =   2055
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Pa�s"
         Height          =   195
         Index           =   36
         Left            =   120
         TabIndex        =   40
         Top             =   2480
         Width           =   330
      End
      Begin VB.Label Label5 
         Caption         =   "M�dico"
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   2140
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Posto"
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   38
         Top             =   1440
         Width           =   1155
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Data"
         Height          =   195
         Index           =   14
         Left            =   120
         TabIndex        =   37
         Top             =   975
         Width           =   345
      End
      Begin VB.Label Label1 
         Caption         =   "N� Req."
         Height          =   255
         Index           =   0
         Left            =   3600
         TabIndex        =   36
         Top             =   540
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "E.F.R."
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   1800
         Width           =   495
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Proveni�ncia"
         Height          =   195
         Index           =   18
         Left            =   120
         TabIndex        =   34
         Top             =   2820
         Width           =   930
      End
   End
End
Attribute VB_Name = "FormFactusEnvioSerie"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim NumCampos As Integer

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim ListarRemovidos As Boolean

Const Verde = &HC0FFC0
Const vermelho = &HC0C0FF
Const azul = &HFFC0C0
Const Amarelo = &HC0FFFF
Const rosa = &HFFC0FF
Const azul2 = &HFFFFC0

Const lColEnvio = 0
Const lColReq = 1
Const lColData = 2
Const lColNome = 3
Const lColNomeDono = 4
Const lColNomeEntidade = 5


Const NumLinhas = 17
Public rs As ADODB.recordset
Dim ReqActual As Long
Dim flg_pergunta As Boolean

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    
    Call Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    Call PreencheValoresDefeito
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
    gF_FACTUSENVIO = 1
    
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Procurar", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    gF_FACTUSENVIO = 0
    
    Set gFormActivo = MDIFormInicio
    
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    ' Fecha a conex�o secund�ria.
    BL_Fecha_Conexao_Secundaria
    
    Set FormFactusEnvioSerie = Nothing
    
End Sub

Sub Inicializacoes()

    Me.caption = " Envio de Dados para Factura��o em S�rie"
    
    Me.left = 100
    Me.top = 20
    Me.Width = 14760
    Me.Height = 9645 ' Normal
    'Me.Height = 8700 ' Campos Extras
    
    Set CampoDeFocus = EcNumReq
    
    EcKm.Enabled = False
    
    ' Abre a liga��o ao Factus.
    Call BL_Abre_Conexao_Secundaria(gSGBD_SECUNDARIA)
    ' -----------------------
    ' ABRE CONEXAO COM FACTUS
    ' -----------------------
    BL_Abre_Conexao_HIS gConexaoSecundaria, gOracle
    ReqActual = 1
    
End Sub

 Sub DefTipoCampos()
    
    With FgReq
        .Cols = 6
        .rows = NumLinhas
        .FixedCols = 0
        .FixedRows = 1
        
        .Col = lColEnvio
        .ColWidth(lColEnvio) = 0
        .TextMatrix(0, lColEnvio) = "->"
        
        .Col = lColReq
        .ColWidth(lColReq) = 800
        .TextMatrix(0, lColReq) = "Req."
    
        .Col = lColData
        .ColWidth(lColData) = 900
        .TextMatrix(0, lColData) = "Data"
        
        .Col = lColNome
        .ColWidth(lColNome) = 3000
        .TextMatrix(0, lColNome) = "Nome"
        
        .Col = lColNomeDono
        .ColWidth(lColNomeDono) = 3000
        .TextMatrix(0, lColNomeDono) = "Dono"
        
        .Col = lColNomeEntidade
        .ColWidth(lColNomeEntidade) = 4000
        .TextMatrix(0, lColNomeEntidade) = "Entidade"
    End With
    
    
    EcKm.Tag = adDouble
    EcKm.MaxLength = 6
    EcDtIni.Tag = adDate
    EcDtFim.Tag = adDate
    EcCodEfr.Tag = adNumeric
    
 End Sub
 
Private Sub BtAlteraUtente_Click()
    FormIdUtentePesq.Show
    FormFactusEnvio.Enabled = False
    Set gFormActivo = FormIdUtentePesq
End Sub

Private Sub BtPesquisaEntFin_Click()
    PA_PesquisaEFR EcCodEfr, EcDescrEfr, ""
End Sub

Private Sub BtPesquisaMed_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim resultados(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    PesqRapida = False
    
    ChavesPesq(1) = "nome_med"
    CamposEcran(1) = "nome_med"
    Tamanhos(1) = 4000
    Headers(1) = "Nome"
    
    ChavesPesq(2) = "cod_med"
    CamposEcran(2) = "cod_med"
    Tamanhos(2) = 1000
    Headers(2) = "C�digo"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_medicos"
    CampoPesquisa = "nome_med"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY nome_med ", " M�dico")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMed.Text = resultados(2)
            EcNomeMed.Text = resultados(1)
            BtPesquisaMed.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem m�dicos", vbExclamation, "M�dicos"
        EcCodMed.SetFocus
    End If

End Sub

Private Sub BtPesquisaSala_Click()
    PA_PesquisaSala EcCodSala, EcDescrSala, ""

End Sub


Private Sub CbConvencao_Click()
    If CbConvencao.ListIndex > -1 Then
        EF_ReqFact(ReqActual).convencao = CbConvencao.ItemData(CbConvencao.ListIndex)
    End If
End Sub


Public Sub EcCodMed_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    If EcCodMed.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT nome_med FROM sl_medicos WHERE cod_med=" & BL_TrataStringParaBD(Trim(EcCodMed.Text))
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            DoEvents
            EcCodMed.Text = ""
            EcNomeMed.Text = ""
            EF_ReqFact(ReqActual).codMed = ""
        Else
            EcNomeMed.Text = Tabela!nome_med
            EF_ReqFact(ReqActual).codMed = EcCodMed
        End If
        Tabela.Close
        Set Tabela = Nothing
        
    Else
        EcNomeMed.Text = ""
    End If
    
End Sub

Public Sub EcCodsala_Validate(Cancel As Boolean)

    Cancel = PA_ValidateSala(EcCodSala, EcDescrSala, "")
    
End Sub

Public Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEfr, EcDescrEfr, "")
End Sub

Private Sub CbCodUrbano_Click()

    If CbCodUrbano.ListIndex <> mediComboValorNull Then
        If CbCodUrbano.ListIndex <> mediComboValorNull Then
            EcKm.Enabled = True
            EcKm.SetFocus
        Else
            EcKm.Text = ""
            EcKm.Enabled = False
            EF_ReqFact(ReqActual).km = 0
        End If
        EF_ReqFact(ReqActual).codUrbano = BG_DaComboSel(CbCodUrbano)
    End If

End Sub

Private Sub CbCodUrbano_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 Then
        BG_LimpaOpcao CbCodUrbano, KeyCode
        EcKm.Text = ""
    End If
    
End Sub



Private Sub EcKm_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcKm)
    If EcKm.Text = "" Then
        EcKm = "0"
    End If
    EF_ReqFact(ReqActual).km = IIf(EcKm.Text = "", 0, EcKm.Text)
    
End Sub


Private Sub EcNumBenef_Validate(Cancel As Boolean)
    If ReqActual <= EF_TotalRequisicoes Then
        EF_ReqFact(ReqActual).n_benef = EcNumBenef.Text
    End If
End Sub

Private Sub EcNumReq_Click()
    DoEvents
    
End Sub

Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    If EcNumReq <> "" And KeyCode = 13 Then
        FuncaoProcurar
    End If
End Sub

Private Sub EcNumReq_LostFocus()
    
    EcNumReq.Tag = adDecimal
    If Not BG_ValidaTipoCampo_ADO(Me, EcNumReq) Then
        Sendkeys ("{HOME}+{END}")
    End If

End Sub


Private Sub FgReq_Click()
    Dim i As Long
    LimpaCamposEcra
    
    If FgReq.row > EF_TotalRequisicoes Then
        Exit Sub
    End If
    ReqActual = FgReq.row
    BtValida.Enabled = True
    EcNumReq = EF_ReqFact(ReqActual).NReq
    EcTipoUte = EF_ReqFact(ReqActual).TipoUtente
    EcUtente = EF_ReqFact(ReqActual).Utente
    EcNome = EF_ReqFact(ReqActual).NomeUtente
    EcCodSala = EF_ReqFact(ReqActual).cod_sala
    EcCodsala_Validate False
    EcCodEfr = EF_ReqFact(ReqActual).codEfr
    EcDescrEfr = EF_ReqFact(ReqActual).DescrEFR
    EcCodMed = EF_ReqFact(ReqActual).codMed
    EcNomeMed = BL_SelCodigo("SL_MEDICOS", "nome_med", "cod_med", EF_ReqFact(ReqActual).codMed, "V")
    EcNumBenef = EF_ReqFact(ReqActual).n_benef
    EcDataNasc = EF_ReqFact(ReqActual).DataNasc
    EcReqAssoc = EF_ReqFact(ReqActual).NReqAssoc
    If EF_ReqFact(ReqActual).DataNasc <> "" Then
        EcIdade = BG_CalculaIdade(CDate(EcDataNasc.Text), CDate(EF_ReqFact(ReqActual).dt_chega))
    End If
    EcKm = EF_ReqFact(ReqActual).km
    EcCodPais = EF_ReqFact(ReqActual).codPais
    EcCodpais_Validate False
    EcDoenteProf = EF_ReqFact(ReqActual).numDoeProf
    EcCodProven = EF_ReqFact(ReqActual).cod_proven
    EcCodProven_Validate False
    If EF_ReqFact(ReqActual).Sexo = gT_Masculino Then
        EcSexo = "Masculino"
    ElseIf EF_ReqFact(ReqActual).Sexo = gT_Feminino Then
        EcSexo = "Feminino"
    End If

    If EF_ReqFact(ReqActual).SeqUtenteFact > -1 Then
        LbUtenteFact.Visible = True
    Else
        LbUtenteFact.Visible = False
    End If
    
    If EF_ReqFact(ReqActual).fimSemana = 1 Then
        LbFimSemana.Visible = True
    Else
        LbFimSemana.Visible = False
    End If
    
    
    
    ' ---------
    ' ISENCAO
    ' --------
    If EF_ReqFact(ReqActual).codIsencao <> "" Then
        For i = 0 To CbTipoIsencao.ListCount - 1
            If EF_ReqFact(ReqActual).codIsencao = CbTipoIsencao.ItemData(i) Then
                CbTipoIsencao.ListIndex = i
                Exit For
            End If
        Next
    End If
    
    ' ---------
    ' CONVENCAO
    ' ---------
    For i = 0 To CbConvencao.ListCount - 1
        If EF_ReqFact(ReqActual).convencao = CbConvencao.ItemData(i) Then
            CbConvencao.ListIndex = i
            Exit For
        End If
    Next
    
    ' ---------
    ' URBANO
    ' ---------
    For i = 0 To CbCodUrbano.ListCount - 1
        If EF_ReqFact(ReqActual).codUrbano = CbCodUrbano.ItemData(i) Then
            CbCodUrbano.ListIndex = i
            Exit For
        End If
    Next
    
    ' ---------
    ' HEMODIALISE
    ' ---------
    For i = 0 To CbHemodialise.ListCount - 1
        If EF_ReqFact(ReqActual).hemodialise = CbHemodialise.ItemData(i) Then
            CbHemodialise.ListIndex = i
            Exit For
        End If
    Next
    
    EcEstado = BL_DevolveEstadoReq(EF_ReqFact(ReqActual).EstadoReq)
                                                
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    
    DoEvents
    DoEvents
    DoEvents
    FrameAccao.Visible = False
    flg_pergunta = True
End Sub

Private Sub Form_Activate()
    
    EventoActivate

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub FuncaoProcurar()
    
    On Error Resume Next
    
    Dim sRet As String
    
    Me.BtValida.Enabled = False
    
    DoEvents
    
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    DoEvents
    EF_TotalRequisicoes = 0
    ReDim EF_ReqFact(0)
    
            
    Preenche_Dados_Requisicao
End Sub
    
' ---------------------------------------------------------------------------

' PREENCHE TODOS OS DADOS DA REQUISICAO E DO UTENTE EM CAUSA

' ---------------------------------------------------------------------------

Function Preenche_Dados_Requisicao() As Boolean
        
    On Error GoTo TrataErro
    
    Dim sSql As String
    Dim rsReq As ADODB.recordset
    Dim i As Integer
    Dim flg_criterio As Boolean
    FrameAccao.Visible = True
    LbAccao = " A Preencher Requisi��es...."
    DoEvents
    ' ---------------------------------------------------------------------------
    ' LIMPA ESTRUTURA DAS REQUISICOES A FACTURAR
    ' ---------------------------------------------------------------------------
    LimpaEF_ReqFact
    
    Preenche_Dados_Requisicao = True
    flg_criterio = False
    
    sSql = ConstroiCriterio(flg_criterio)
    Set rsReq = New ADODB.recordset
    rsReq.CursorLocation = adUseServer
    rsReq.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsReq.Open sSql, gConexao
        
    If (rsReq.RecordCount <= 0) Then
        Preenche_Dados_Requisicao = False
        MsgBox "Requisi��o inexistente.    ", vbExclamation, Me.caption
        rsReq.Close
        Set rsReq = Nothing
        Exit Function
    Else
        EcCodEfr.Enabled = False
        BtPesquisaEntFin.Enabled = False
        While Not rsReq.EOF
            
            Call EF_PreencheRequisicao(BL_HandleNull(rsReq!n_req, ""), BL_HandleNull(rsReq!dt_chega, ""), BL_HandleNull(rsReq!user_cri, ""), _
                                        BL_HandleNull(rsReq!hr_chega, ""), BL_HandleNull(rsReq!n_benef, ""), BL_HandleNull(rsReq!cod_isencao, ""), _
                                        BL_HandleNull(rsReq!Flg_Facturado, -1), BL_HandleNull(rsReq!cod_urbano, 0), BL_HandleNull(rsReq!km, 0), _
                                        BL_HandleNull(rsReq!cod_efr, ""), BL_HandleNull(rsReq!cod_med, ""), BL_HandleNull(rsReq!seq_utente, ""), _
                                        BL_HandleNull(rsReq!t_utente, ""), BL_HandleNull(rsReq!Utente, ""), BL_HandleNull(rsReq!nome_ute, ""), _
                                        BL_HandleNull(rsReq!dt_nasc_ute, ""), BL_HandleNull(rsReq!sexo_ute, ""), BL_HandleNull(rsReq!n_cartao_ute, ""), _
                                        BL_HandleNull(rsReq!convencao, -1), BL_HandleNull(rsReq!hemodialise, -1), BL_HandleNull(rsReq!fim_semana, 0), _
                                        BL_HandleNull(rsReq!cod_sala, ""), BL_HandleNull(rsReq!seq_utente_fact, -1), BL_HandleNull(rsReq!descr_efr, ""), _
                                        BL_HandleNull(rsReq!flg_perc_facturar, "0"), BL_HandleNull(rsReq!flg_controla_isencao, "0"), _
                                        BL_HandleNull(rsReq!flg_obriga_recibo, "0"), BL_HandleNull(rsReq!deducao_ute, ""), BL_HandleNull(rsReq!flg_acto_med_efr, "0"), _
                                        BL_HandleNull(rsReq!cod_empresa, ""), BL_HandleNull(rsReq!flg_compart_dom, "0"), BL_HandleNull(rsReq!flg_dom_defeito, "0"), _
                                        BL_HandleNull(rsReq!flg_nao_urbano, "0"), BL_HandleNull(rsReq!flg_65anos, "0"), BL_HandleNull(rsReq!flg_nova_ars, 0), _
                                        BL_HandleNull(rsReq!flg_separa_dom, "0"), BL_HandleNull(rsReq!flg_insere_cod_rubr_efr, "0"), BL_HandleNull(rsReq!flg_adse, 0), _
                                        BL_HandleNull(rsReq!n_Req_assoc, ""), BL_HandleNull(rsReq!estado_req, " "), BL_HandleNull(rsReq!cod_pais, ""), _
                                        BL_HandleNull(rsReq!num_doe_prof, ""), BL_HandleNull(rsReq!cod_postal, ""), BL_HandleNull(rsReq!descr_postal, ""), _
                                        BL_HandleNull(rsReq!cod_proven, ""), BL_HandleNull(rsReq!cod_proven_efr, ""), BL_HandleNull(rsReq!descr_proven, ""), _
                                        BL_HandleNull(rsReq!nome_dono, ""))
            EF_PreencheAnalises EF_TotalRequisicoes
            FgReq.TextMatrix(EF_TotalRequisicoes, lColEnvio) = "S"
            FgReq.TextMatrix(EF_TotalRequisicoes, lColReq) = EF_ReqFact(EF_TotalRequisicoes).NReq
            FgReq.TextMatrix(EF_TotalRequisicoes, lColData) = EF_ReqFact(EF_TotalRequisicoes).dt_chega
            FgReq.TextMatrix(EF_TotalRequisicoes, lColNome) = EF_ReqFact(EF_TotalRequisicoes).NomeUtente
            FgReq.TextMatrix(EF_TotalRequisicoes, lColNomeDono) = EF_ReqFact(EF_TotalRequisicoes).NomeDono
            FgReq.TextMatrix(EF_TotalRequisicoes, lColNomeEntidade) = EF_ReqFact(EF_TotalRequisicoes).DescrEFR
            FgReq.AddItem ""
            rsReq.MoveNext
        Wend
        FgReq.row = 1
        FgReq_Click
    End If
    rsReq.Close
    Set rsReq = Nothing
    Preenche_Dados_Requisicao = True
    FrameAccao.Visible = False
Exit Function
TrataErro:
    BG_LogFile_Erros Me.Name & "Preenche_Dados_Requisicao :" & Err.Description
    BG_Mensagem mediMsgBox, "Erro ao preencher Requisi��o: " & Err.Description, vbOKOnly + vbInformation, "Preencher Requisi��o"
    Preenche_Dados_Requisicao = False
    Exit Function
    Resume Next
End Function


Sub FuncaoLimpar()
    
    EcNumReq.Text = ""
    Me.EcNumReq.ForeColor = &H8000&
    LimpaCamposEcra
    EcNumReq.SetFocus
    Me.BtValida.Enabled = False
    
    ReDim EF_ReqFact(0)
    EF_TotalRequisicoes = 0
    
    DoEvents
    DoEvents
    LimpaEF_ReqFact
    EcCodEfr.Enabled = True
    BtPesquisaEntFin.Enabled = True
    FrameAccao.Visible = False
    
End Sub

Sub FuncaoModificar()
    
    Dim Erro As String
    Dim sql As String
    
    On Error GoTo Trata_Erro
    
    'Actualizar codigo urbano e km na tabela de requisi��es
    If CbCodUrbano.ListIndex <> mediComboValorNull Then
                
        If (Len(Trim(EcKm.Text)) = 0) Then
            EcKm.Text = "0"
        End If
        
        sql = "UPDATE sl_requis " & _
              "SET    cod_urbano = " & CbCodUrbano.ItemData(CbCodUrbano.ListIndex) & ", " & _
              "       km = " & IIf((EcKm.Text = ""), 0, EcKm.Text) & " " & _
              "WHERE  n_req =" & EcNumReq.Text
        
        BG_ExecutaQuery_ADO sql
    End If
    
    Exit Sub
Trata_Erro:
    BG_LogFile_Erros "FuncaoModificar (FormFACTUSEnvio):" & Err.Number & "-" & Err.Description & "(" & Erro & ")"
End Sub

Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_tbf_t_urbano", "cod_t_urbano", "descr_t_urbano", CbCodUrbano
    BG_PreencheComboBD_ADO "sl_isencao", "cod_isencao", "descr_isencao", CbTipoIsencao
    BG_PreencheComboBD_ADO "sl_tbf_convencao", "cod_convencao", "descr_convencao", CbConvencao
    BG_PreencheComboBD_ADO "sl_cod_hemodialise", "cod_hemodialise", "descr_hemodialise", CbHemodialise
    LbFimSemana.Visible = False
    FrameAccao.Visible = False
    LbCredenciais = ""
    
End Sub

' ---------------------------------------------------------------------------

' PREENCHE A ESTRUTURA COM AS ANALISES DA REQUISI��O

' ---------------------------------------------------------------------------

Private Sub BtValida_Click()
    Dim i As Integer
    On Error GoTo TrataErro
    Me.BtValida.Enabled = False
    FrameAccao.Visible = True
    DoEvents
    For i = 1 To EF_TotalRequisicoes
        LbAccao = "A Enviar dados: " & EF_ReqFact(i).NReq
        DoEvents
        Call EF_EnviaDados(i)
    Next i
    FrameAccao.Visible = False
    FuncaoLimpar
Exit Sub
TrataErro:
    BL_LogFile_BD Me.Name, "BtValida_Click", Err.Number, Err.Description, ""
    BG_RollbackTransaction
    Exit Sub
    Resume Next
End Sub

Private Function Get_EstadoFact(REQ_ACTUAL As String) As Integer
    Dim sSql As String
    Dim RsEstado As ADODB.recordset
    Dim estadoFact As Integer
    
    On Error GoTo ErrorHandler
    
    Set RsEstado = New ADODB.recordset
    sSql = "select max(flg_estado) MaxEstado from fa_movi_fact where episodio = '" & Trim(REQ_ACTUAL) & "'"
    RsEstado.CursorLocation = adUseServer
    RsEstado.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEstado.Open sSql, gConexaoSecundaria
    estadoFact = BL_HandleNull(RsEstado!MaxEstado, 0)
    If estadoFact = 3 Then
        Me.EcNumReq.ForeColor = &HFF&
        MsgBox "N�o � poss�vel enviar esta requisi��o para a factura��o. Deve primeiro rejeita-la.   ", vbExclamation, Me.caption
    End If
    Get_EstadoFact = estadoFact
    
Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            BG_LogFile_Erros "Erro Inesperado : Get_EstadoFact (FormFACTUSEnvio) -> " & Err.Description
            
            Get_EstadoFact = 3
            Exit Function
    End Select
End Function

' ---------------------------------------------------------------------------

' LIMPA A ESTRUTURA QUE CONTEM OS DADOS DA REQUISICAO A FACTURAR

' ---------------------------------------------------------------------------


Private Sub LimpaEF_ReqFact()
    Dim i As Integer
    FgReq.rows = 2
    For i = 0 To FgReq.Cols - 1
        FgReq.TextMatrix(1, i) = ""
    Next i
    FgReq.CellBackColor = vbWhite
End Sub


' ---------------------------------------------------------------------------

' VERIFICA SE UTENTE PAGOU TODAS AS TAXAS

' ---------------------------------------------------------------------------
Public Function RequisicaoIsenta(n_req As String) As Boolean
        
    On Error GoTo TrataErro
    
    Dim sSql As String
    Dim RsTaxa As ADODB.recordset
    Dim ret As Boolean
    
    n_req = Trim(n_req)
    
    If (Len(n_req) = 0) Then
        RequisicaoIsenta = False
        Exit Function
    End If
        
    sSql = "SELECT cod_isencao FROM sl_requis WHERE n_req = " & n_req
    Set RsTaxa = New ADODB.recordset
    RsTaxa.CursorLocation = adUseServer
    RsTaxa.CursorType = adOpenForwardOnly
    RsTaxa.LockType = adLockReadOnly
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsTaxa.Open sSql, gConexao
    
    
    If (RsTaxa.RecordCount > 0) Then
        If BL_HandleNull(RsTaxa!cod_isencao, "2") <> "2" Then
            ret = False
        Else
            ret = True
        End If
    End If
    RsTaxa.Close
    Set RsTaxa = Nothing

    RequisicaoIsenta = ret

Exit Function
TrataErro:
    Call BL_LogFile_BD(Me.Name, "RequisicaoIsenta", Err.Number, Err.Description, "")
    RequisicaoIsenta = False
    Exit Function
    Resume Next
End Function


' ---------------------------------------------------------------------------

' CALCULA QUANTAS AN�LISES EXISTEM POR CREDENCIAL

' ---------------------------------------------------------------------------
Private Sub CalculaCredenciais()
    Dim credenciais() As Integer
    Dim tot_credenciais() As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    On Error GoTo TrataErro
    
    ReDim credenciais(0)
    ReDim tot_credenciais(0)
    
    For k = 1 To EF_TotalRequisicoes
        For i = 1 To EF_ReqFact(k).TotalAnalisesFact
            For j = 1 To UBound(credenciais)
                If EF_ReqFact(k).AnalisesFact(i).p1 = credenciais(j) Then
                    tot_credenciais(j) = tot_credenciais(j) + 1
                    Exit For
                End If
            Next j
            If j > UBound(credenciais) Or UBound(credenciais) = 0 Then
                If UBound(credenciais) = 0 Then
                    ReDim credenciais(1)
                    ReDim tot_credenciais(1)
                    credenciais(1) = BL_HandleNull(EF_ReqFact(k).AnalisesFact(i).p1, -1)
                    tot_credenciais(1) = 1
                Else
                    ReDim Preserve credenciais(UBound(credenciais) + 1)
                    ReDim Preserve tot_credenciais(UBound(credenciais) + 1)
                    credenciais(UBound(credenciais)) = EF_ReqFact(k).AnalisesFact(i).p1
                    tot_credenciais(UBound(credenciais)) = 1
                End If
            End If
        Next i
    Next k
    LbCredenciais = "Credenciais: "
    For i = 1 To UBound(credenciais)
        LbCredenciais = LbCredenciais & tot_credenciais(i) & "   "
    Next
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

Private Sub EcDtIni_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
   If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
        Sendkeys ("{HOME}+{END}")
    End If
End Sub

Private Sub EcDtFim_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
   If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
        Sendkeys ("{HOME}+{END}")
    End If

End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    Dim i As Integer
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtIni)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub


Private Sub LimpaCamposEcra()
    EcNumReq = ""
    EcTipoUte = ""
    EcUtente = ""
    EcNome = ""
    EcCodSala = ""
    EcDescrSala = ""
    EcCodEfr = ""
    EcDescrEfr = ""
    EcCodMed = ""
    EcNomeMed = ""
    EcNumBenef = ""
    EcDataNasc = ""
    EcReqAssoc = ""
    EcIdade = ""
    LbFimSemana.Visible = False
    EcDtIni.Text = ""
    EcDtFim.Text = ""
    CbTipoIsencao.ListIndex = mediComboValorNull
    CbConvencao.ListIndex = mediComboValorNull
    CbCodUrbano.ListIndex = mediComboValorNull
    EcEstado = ""
    
    EcKm.Text = ""
    LbUtenteFact.Visible = False
    CbHemodialise.ListIndex = mediComboValorNull
    EcDescrPais = ""
    EcCodPais = ""
    EcDoenteProf = ""
    EcDescrProven = ""
    EcCodProven = ""
    
End Sub


Private Sub BtPesqPais_click()

    
    FormPesquisaRapida.InitPesquisaRapida "sl_pais", _
                        "descr_pais", "cod_pais", _
                        EcCodPais
    EcCodpais_Validate False
End Sub

Private Sub BtPesqproven_click()

    
    FormPesquisaRapida.InitPesquisaRapida "sl_proven", _
                        "descr_proven", "cod_proven", _
                        EcCodProven
    EcCodProven_Validate False
End Sub
Private Sub EcCodpais_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodPais.Text = UCase(EcCodPais.Text)
    If EcCodPais.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descr_pais FROM sl_pais WHERE cod_pais='" & Trim(EcCodPais.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Pa�s inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodPais.Text = ""
            EcDescrPais.Text = ""
        Else
            EcDescrPais.Text = BL_HandleNull(Tabela!descr_pais)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrPais.Text = ""
    End If
End Sub
Private Sub EcCodProven_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim sql As String
    
    EcCodProven.Text = UCase(EcCodProven.Text)
    If EcCodProven.Text <> "" Then
        Set Tabela = New ADODB.recordset
    
        sql = "SELECT descR_proven FROM sl_proven WHERE cod_proven ='" & Trim(EcCodProven.Text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseServer
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        Tabela.Open sql, gConexao
    
        If Tabela.RecordCount <= 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "Proveni�ncia inexistente!", vbInformation + vbOKOnly, App.ProductName
            EcCodProven.Text = ""
            EcDescrProven.Text = ""
        Else
            EcDescrProven.Text = BL_HandleNull(Tabela!descr_proven)
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProven.Text = ""
    End If
End Sub

Private Function ConstroiCriterio(ByRef flg_criterio As Boolean) As String
    Dim sSql As String
    
    sSql = "SELECT DISTINCT x1.n_req,x1.n_req_assoc, x1.n_epis, x1.seq_utente, x4.cod_efr, x1.n_benef, x1.cod_med, x1.estado_req,  x1.cod_urbano,  x1.km,  x1.flg_facturado, "
    sSql = sSql & " x1.estado_req, x1.dt_chega, x1.user_cri, x1.hr_chega, x2.utente, x2.t_utente, x2.n_cartao_ute, x2.nome_ute, x1.cod_isencao, "
    sSql = sSql & " x1.convencao, x1.hemodialise, x1.fim_semana, x2.dt_nasc_ute, x1.cod_sala, x1.seq_utente_fact,x3.descr_efr, x3.flg_perc_facturar, "
    sSql = sSql & " x3.flg_controla_isencao, x3.flg_obriga_recibo, x3.deducao_ute, x3.FLG_ACTO_MED_EFR, x3.cod_empresa,x3.flg_compart_dom, "
    sSql = sSql & " x3.flg_nao_urbano, x3.flg_65anos, x2.dt_nasc_ute, x3.flg_separa_dom, x3.flg_dom_defeito, x3.flg_fds_fixo, x3.flg_insere_cod_rubr_efr, x2.sexo_ute, "
    sSql = sSql & " x1.cod_postal, x1.num_doe_prof, x1.cod_pais, x3.flg_nova_ars, x5.descr_postal, x3.flg_adse, x6.cod_proven ,x6.cod_efr cod_proven_efr, x6.descr_proven, x2.nome_dono  "
    sSql = sSql & " FROM sl_requis x1 LEFT OUTER JOIN sl_cod_postal x5 ON x1.cod_postal = x5.cod_postal LEFT OUTER JOIN SL_PROVEN x6 "
    sSql = sSql & " ON x1.cod_proven  = x6.cod_proven, "
    sSql = sSql & " sl_identif x2, sl_efr x3, sl_recibos x4 WHERE x1.seq_utente = x2.seq_utente AND x1.n_req = x4.n_req "
    sSql = sSql & " AND x4.cod_efr = x3.cod_efr AND x4.estado <> " & BL_TrataStringParaBD(gEstadoReciboAnulado)
    'RGONCALVES 08.07.2014
    'sSql = sSql & " AND( x1.cod_isencao <> " & gTipoIsencaoNaoFacturar & " OR x1.cod_isencao IS NULL)"
    If gLAB <> "ICIL" Then
        sSql = sSql & " AND( x1.cod_isencao <> " & gTipoIsencaoNaoFacturar & " OR x1.cod_isencao IS NULL)"
    End If
    '
    If gTipoInstituicao = gTipoInstituicaoVet Then
        sSql = sSql & " AND x1.estado_req IN(" & BL_TrataStringParaBD(gEstadoReqTodasImpressas) & "," & BL_TrataStringParaBD(gEstadoReqValicacaoMedicaCompleta) & "," & BL_TrataStringParaBD(gEstadoReqImpressaoParcial) & ")"
        sSql = sSql & " AND x1.n_req NOT IN (Select n_req FROM sl_marcacoes UNION select n_req FROM sl_realiza "
        sSql = sSql & " WHERE flg_estado not in (" & BL_TrataStringParaBD(gEstadoAnaValidacaoMedica) & "," & BL_TrataStringParaBD(gEstadoAnaImpressa) & "))"
    End If
        
    If EcNumReq <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.n_req = " & EcNumReq
    End If
    
    If EcTipoUte <> "" And EcUtente <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x2.t_utente = " & BL_TrataStringParaBD(EcTipoUte) & " AND x2.utente = " & BL_TrataStringParaBD(EcUtente)
    End If
    
    If EcCodSala <> "" Then
        sSql = sSql & " AND x1.cod_sala = " & EcCodSala
        flg_criterio = True
    End If
    
    If EcDtIni.Text <> "" And EcDtFim.Text <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text)
    End If
        
    If EcCodEfr <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.cod_Efr = " & EcCodEfr
    End If
    
    If EcCodMed <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.cod_med = " & BL_TrataStringParaBD(EcCodMed)
    End If
    
    If EcCodProven <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.cod_proven = " & BL_TrataStringParaBD(EcCodProven)
    End If
    
    If CbConvencao.ListIndex <> mediComboValorNull Then
        flg_criterio = True
        sSql = sSql & " AND x1.convencao = " & BG_DaComboSel(CbConvencao)
    End If
    
    If CbCodUrbano.ListIndex <> mediComboValorNull Then
        flg_criterio = True
        sSql = sSql & " AND x1.cod_urbano = " & BG_DaComboSel(CbCodUrbano)
    End If
    
    If CbTipoIsencao.ListIndex <> mediComboValorNull Then
        flg_criterio = True
        sSql = sSql & " AND x1.cod_isencao = " & BG_DaComboSel(CbTipoIsencao)
    End If
    
    If EcNumBenef <> "" Then
        flg_criterio = True
        sSql = sSql & " AND x1.n_benef = " & BL_TrataStringParaBD(EcNumBenef)
    End If
            
    If CbHemodialise.ListIndex <> mediComboValorNull Then
        flg_criterio = True
        sSql = sSql & " AND x1.hemodialise = " & BG_DaComboSel(CbHemodialise)
    End If
    
            
    sSql = sSql & " AND (x1.flg_facturado = 0  OR x1.flg_facturado IS NULL)"
    flg_criterio = True
    
    If flg_criterio = False Then
        BG_Mensagem mediMsgBox, "Tem que indicar um crit�rio.", vbOKOnly + vbInformation, "Crit�rios"
        Exit Function
    End If
    
    If gSGBD = gOracle Then
        sSql = sSql & " ORDER BY n_req "
    Else
        sSql = sSql & " ORDER BY x1.n_req "
    End If
    ConstroiCriterio = sSql
End Function


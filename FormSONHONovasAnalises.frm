VERSION 5.00
Begin VB.Form FormSONHONovasAnalises 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormFacturacaoNovasAnalises"
   ClientHeight    =   7320
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8550
   Icon            =   "FormSONHONovasAnalises.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7320
   ScaleWidth      =   8550
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcProva 
      Alignment       =   2  'Center
      Height          =   405
      Left            =   1440
      Locked          =   -1  'True
      TabIndex        =   43
      Top             =   7440
      Width           =   855
   End
   Begin VB.TextBox EcSeqAna 
      Height          =   285
      Left            =   3720
      TabIndex        =   21
      Top             =   8160
      Width           =   855
   End
   Begin VB.Frame Frame3 
      Caption         =   "R�brica "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4695
      Left            =   120
      TabIndex        =   14
      Top             =   120
      Width           =   8295
      Begin VB.TextBox EcCentroCusto 
         Appearance      =   0  'Flat
         ForeColor       =   &H00008000&
         Height          =   285
         Left            =   3360
         TabIndex        =   50
         Top             =   1260
         Width           =   1815
      End
      Begin VB.CommandButton BtListaAnaNaoMapeadas 
         BackColor       =   &H00C0E0FF&
         Height          =   525
         Left            =   7320
         Picture         =   "FormSONHONovasAnalises.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   48
         ToolTipText     =   "Pr�-Visualizar an�lises n�o mapeadas"
         Top             =   240
         Width           =   615
      End
      Begin VB.CheckBox CkFacturaMembros 
         Caption         =   "Factura tamb�m membros da complexa"
         Height          =   255
         Left            =   1440
         TabIndex        =   47
         Top             =   600
         Width           =   3615
      End
      Begin VB.TextBox EcCodAnaRegra 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5160
         TabIndex        =   46
         Top             =   280
         Width           =   1335
      End
      Begin VB.CheckBox CkFlgRegra 
         Caption         =   "Usa regra. S� factura se n�o marcada a an�lise"
         Height          =   195
         Left            =   1440
         TabIndex        =   45
         Top             =   280
         Width           =   3975
      End
      Begin VB.ComboBox EcMetodoTSQ 
         Height          =   315
         Left            =   3450
         Style           =   2  'Dropdown List
         TabIndex        =   37
         Top             =   4200
         Width           =   1635
      End
      Begin VB.ComboBox CbProva 
         Height          =   315
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   35
         Top             =   4200
         Width           =   1515
      End
      Begin VB.TextBox EcCodProduto 
         Appearance      =   0  'Flat
         ForeColor       =   &H00008000&
         Height          =   285
         Left            =   6600
         TabIndex        =   33
         Top             =   1260
         Width           =   975
      End
      Begin VB.CommandButton BtPesqRapProduto 
         Height          =   285
         Left            =   7560
         Picture         =   "FormSONHONovasAnalises.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   32
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1260
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapSONHO 
         Height          =   285
         Left            =   7560
         Picture         =   "FormSONHONovasAnalises.frx":1108
         Style           =   1  'Graphical
         TabIndex        =   30
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1560
         Width           =   375
      End
      Begin VB.CommandButton BtInserirAnalise 
         Height          =   375
         Left            =   7560
         Picture         =   "FormSONHONovasAnalises.frx":153A
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   4200
         Width           =   375
      End
      Begin VB.ListBox ListDescrSONHO 
         Appearance      =   0  'Flat
         Height          =   1395
         Left            =   1320
         TabIndex        =   28
         Top             =   1860
         Width           =   6615
      End
      Begin VB.ListBox ListCodSONHO 
         Appearance      =   0  'Flat
         Height          =   1395
         Left            =   360
         TabIndex        =   27
         Top             =   1860
         Width           =   975
      End
      Begin VB.TextBox EcDescrSONHO 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1320
         TabIndex        =   20
         Top             =   1560
         Width           =   6255
      End
      Begin VB.TextBox EcCodSONHO 
         Appearance      =   0  'Flat
         ForeColor       =   &H00008000&
         Height          =   285
         Left            =   360
         TabIndex        =   19
         Top             =   1560
         Width           =   975
      End
      Begin VB.TextBox EcCodAna 
         Appearance      =   0  'Flat
         ForeColor       =   &H00C00000&
         Height          =   285
         Left            =   360
         TabIndex        =   16
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox EcDescrAna 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   960
         Width           =   6615
      End
      Begin VB.ListBox EcLocais 
         Appearance      =   0  'Flat
         Height          =   705
         Left            =   1320
         Style           =   1  'Checkbox
         TabIndex        =   51
         Top             =   3315
         Width           =   6615
      End
      Begin VB.Label LbLocal 
         AutoSize        =   -1  'True
         Caption         =   "Locais"
         Height          =   195
         Index           =   0
         Left            =   360
         TabIndex        =   52
         Top             =   3360
         Width           =   465
      End
      Begin VB.Label Label14 
         Caption         =   "CENTRO CUSTO"
         Height          =   255
         Left            =   2040
         TabIndex        =   49
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label Label10 
         Caption         =   "M�todo p/ TSQ"
         Height          =   495
         Left            =   2850
         TabIndex        =   38
         Top             =   4155
         Width           =   615
      End
      Begin VB.Label Label13 
         Caption         =   "Prova"
         Height          =   255
         Left            =   360
         TabIndex        =   36
         Top             =   4230
         Width           =   855
      End
      Begin VB.Label Label9 
         Caption         =   "COD PRODUTO"
         Height          =   255
         Left            =   5280
         TabIndex        =   34
         Top             =   1305
         Width           =   1215
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Associar nova r�brica do Sonho"
         Height          =   255
         Left            =   5040
         TabIndex        =   31
         Top             =   4305
         Width           =   2415
      End
      Begin VB.Label Label3 
         Caption         =   "SONHO"
         Height          =   255
         Left            =   360
         TabIndex        =   18
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "SISLAB"
         Height          =   255
         Left            =   360
         TabIndex        =   17
         Top             =   720
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "N�o facturar a R�brica para "
      Height          =   2480
      Left            =   240
      TabIndex        =   10
      Top             =   7320
      Width           =   6495
      Begin VB.CommandButton ButSelNada 
         Caption         =   "Nenhuma"
         Height          =   315
         Left            =   5280
         TabIndex        =   13
         Top             =   2040
         Width           =   975
      End
      Begin VB.CommandButton ButSelTudo 
         Caption         =   "Todas"
         Height          =   315
         Left            =   4200
         TabIndex        =   12
         Top             =   2040
         Width           =   975
      End
      Begin VB.ListBox ListaEFR 
         Height          =   1635
         Left            =   240
         Style           =   1  'Checkbox
         TabIndex        =   11
         Top             =   315
         Width           =   6015
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Pesquisa R�pida "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   120
      TabIndex        =   0
      Top             =   4920
      Width           =   8295
      Begin VB.TextBox EcCodMicro 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   41
         Top             =   1560
         Width           =   855
      End
      Begin VB.TextBox EcDescrMicro 
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   40
         Top             =   1560
         Width           =   5415
      End
      Begin VB.CommandButton BtPesqRapM 
         Height          =   320
         Left            =   7560
         Picture         =   "FormSONHONovasAnalises.frx":1ABC
         Style           =   1  'Graphical
         TabIndex        =   39
         ToolTipText     =   "Procurar An�lises Simples"
         Top             =   1560
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapP 
         Height          =   320
         Left            =   7560
         Picture         =   "FormSONHONovasAnalises.frx":1E7E
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Procurar An�lises Simples"
         Top             =   1200
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapC 
         Height          =   320
         Left            =   7560
         Picture         =   "FormSONHONovasAnalises.frx":2240
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Procurar An�lises Simples"
         Top             =   840
         Width           =   375
      End
      Begin VB.CommandButton BtPesqRapS 
         Height          =   320
         Left            =   7560
         Picture         =   "FormSONHONovasAnalises.frx":2602
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Procurar An�lises Simples"
         Top             =   480
         Width           =   375
      End
      Begin VB.TextBox EcDescrAnaP 
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   1200
         Width           =   5415
      End
      Begin VB.TextBox EcDescrAnaC 
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   840
         Width           =   5415
      End
      Begin VB.TextBox EcCodAnaP 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   1200
         Width           =   855
      End
      Begin VB.TextBox EcCodAnaC 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   840
         Width           =   855
      End
      Begin VB.TextBox EcCodAnaS 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   480
         Width           =   855
      End
      Begin VB.TextBox EcDescrAnaS 
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   480
         Width           =   5415
      End
      Begin VB.Label Label11 
         Caption         =   "Microrganismo"
         Height          =   255
         Left            =   240
         TabIndex        =   42
         Top             =   1560
         Width           =   1095
      End
      Begin VB.Label Label7 
         Caption         =   "Perfis"
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   1200
         Width           =   735
      End
      Begin VB.Label Label6 
         Caption         =   "Complexas"
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "Simples"
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   480
         Width           =   735
      End
   End
   Begin VB.Label Label12 
      Caption         =   "EcProva"
      Height          =   255
      Left            =   720
      TabIndex        =   44
      Top             =   7440
      Width           =   615
   End
   Begin VB.Label Label4 
      Caption         =   "Esta an�lise do SISLAB corresponde a v�rias r�bricas do SONHO."
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   2520
      TabIndex        =   26
      Top             =   7080
      Visible         =   0   'False
      Width           =   4815
   End
   Begin VB.Label Label1 
      Caption         =   "EcSeqAna"
      Height          =   255
      Left            =   2760
      TabIndex        =   22
      Top             =   8160
      Width           =   855
   End
End
Attribute VB_Name = "FormSONHONovasAnalises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 22/07/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim nomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Public rs As ADODB.recordset

Private Sub BtInserirAnalise_Click()

    Dim cod_aux As String
    Dim descr_aux As String
    
    cod_aux = EcCodAna.text
    descr_aux = EcDescrAna.text
    
    Call LimpaCampos

    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "Activo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    EcCodAna.text = cod_aux
    EcDescrAna.text = descr_aux
    EcCodSONHO.SetFocus

End Sub


Private Sub BtListaAnaNaoMapeadas_Click()
    Dim continua As Boolean
    Dim sql As String
    
    'Printer Common Dialog
    continua = BL_IniciaReport("ListaAnaNaoMapeadas", "Listagem de An�lises N�o Mapeadas", crptToWindow)
    If continua = False Then Exit Sub
    BL_MudaCursorRato mediMP_Espera, Me

    BG_BeginTransaction
    
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    sql = "DELETE FROM sl_cr_anafacturacao where nome_computador = '" & BG_SYS_GetComputerName & "'"
    BG_ExecutaQuery_ADO sql
    
    sql = "insert into sl_cr_anafacturacao(cod_ana,descr_ana,cod_rubr,flg_regra,cod_ana_regra,factura_membros,cod_efr,nome_computador) " & _
            " select cod_ana_s cod_ana, descr_ana_s descr_ana,cod_ana_gh,flg_regra,cod_ana_regra, factura_membros,cod_efr,'" & BG_SYS_GetComputerName & "'" & _
            " from sl_ana_facturacao f, sl_ana_s s " & _
            " where s.cod_ana_s = f.cod_ana(+) and flg_facturar = '1' " & _
            " Union " & _
            " select cod_ana_c cod_ana, descr_ana_c descr_ana,cod_ana_gh,flg_regra,cod_ana_regra, factura_membros,cod_efr,'" & BG_SYS_GetComputerName & "'" & _
            " from sl_ana_facturacao f, sl_ana_c c " & _
            " where c.cod_ana_c = f.cod_ana(+) and flg_facturar = '1' " & _
            " Union " & _
            " select cod_perfis cod_ana, descr_perfis descr_ana,cod_ana_gh,flg_regra,cod_ana_regra, factura_membros,cod_efr,'" & BG_SYS_GetComputerName & "'" & _
            " from sl_ana_facturacao f, sl_perfis p " & _
            " where p.cod_perfis = f.cod_ana(+) and flg_facturar = '1' " & _
            " order by descr_ana "
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    
    BG_CommitTransaction
    
    Report.SelectionFormula = "{sl_cr_anafacturacao.nome_computador} = '" & BG_SYS_GetComputerName & "'"

    
    Report.Connect = "DSN=" & gDSN
    
    Call BL_ExecutaReport

    BL_MudaCursorRato mediMP_Activo, Me

End Sub

Private Sub BtPesqRapC_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean
    Dim campoPesquisaCod As String
    
    ' ---------------------------------------
    EcCodAna.text = ""
    EcDescrAna.text = ""
    EcCodSONHO.text = ""
    EcDescrSONHO.text = ""
    ' ---------------------------------------
    
    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_c"
    CamposEcran(1) = "cod_ana_c"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_c"
    CamposEcran(2) = "descr_ana_c"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_c"
    CampoPesquisa = "descr_ana_c"
    campoPesquisaCod = "cod_ana_c"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Complexas", campoPesquisaCod)

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaC.text = Resultados(2)
            EcCodAnaC.text = Resultados(1)
            EcCodAna.text = Resultados(1)
            EcDescrAna.text = Resultados(2)
            EcCodAnaC.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises complexas", vbExclamation, "ATEN��O"
        EcCodAnaC.SetFocus
    End If

End Sub

Private Sub BtPesqRapM_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    ' ---------------------------------------
    EcCodAna.text = ""
    EcDescrAna.text = ""
    EcCodSONHO.text = ""
    EcDescrSONHO.text = ""
    ' ---------------------------------------
    
    PesqRapida = False
    
    ChavesPesq(1) = "cod_microrg"
    CamposEcran(1) = "cod_microrg"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_microrg"
    CamposEcran(2) = "descr_microrg"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_microrg"
    CampoPesquisa = "descr_microrg"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Microrganismos")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMicro.text = Resultados(1)
            EcDescrMicro.text = Resultados(2)
            EcCodAna.text = Resultados(1)
            EcDescrAna.text = Resultados(2)
            EcCodMicro.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem microrganismos", vbExclamation, "ATEN��O"
        EcCodMicro.SetFocus
    End If

End Sub

Private Sub BtPesqRapP_Click()

    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean
    Dim campoPesquisaCod As String
    
    ' ---------------------------------------
    EcCodAna.text = ""
    EcDescrAna.text = ""
    EcCodSONHO.text = ""
    EcDescrSONHO.text = ""
    ' ---------------------------------------
    
    PesqRapida = False
    
    ChavesPesq(1) = "cod_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_perfis"
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_perfis"
    CampoPesquisa = "descr_perfis"
    campoPesquisaCod = "cod_perfis"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Perfis", campoPesquisaCod)

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAnaP.text = Resultados(1)
            EcDescrAnaP.text = Resultados(2)
            EcCodAna.text = Resultados(1)
            EcDescrAna.text = Resultados(2)
            EcCodAnaP.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem perfis", vbExclamation, "ATEN��O"
        EcCodAnaP.SetFocus
    End If

End Sub

Private Sub BtPesqRapProduto_Click()
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1) As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "cod_produto"
    CamposEcran(1) = "cod_produto"
    Tamanhos(1) = 800
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_produto"
    CamposEcran(2) = "descr_produto"
    Tamanhos(2) = 4500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "sl_produto"
    CampoPesquisa = "descr_produto"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                 CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "ORDER BY descr_produto", " R�bricas do SONHO")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProduto = Resultados(1)
            EcCodProduto.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem produtos codificados.       ", vbExclamation, " ATEN��O"
        EcCodProduto.SetFocus
    End If

End Sub

Private Sub BtPesqRapS_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim campoPesquisaCod As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean

    ' ---------------------------------------
    EcCodAna.text = ""
    EcDescrAna.text = ""
    EcCodSONHO.text = ""
    EcDescrSONHO.text = ""
    ' ---------------------------------------
    
    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_ana_s"
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3000
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_ana_s"
    CampoPesquisa = "descr_ana_s"
    campoPesquisaCod = "cod_ana_s"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Simples", campoPesquisaCod)

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrAnaS.text = Resultados(2)
            EcCodAnaS.text = Resultados(1)
            EcCodAna.text = Resultados(1)
            EcDescrAna.text = Resultados(2)
            EcCodAnaS.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises simples", vbExclamation, "ATEN��O"
        EcCodAnaS.SetFocus
    End If

End Sub

Private Sub BtPesqRapSONHO_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(1 To 2) As Variant
    Dim PesqRapida As Boolean
    
    PesqRapida = False
    
    ChavesPesq(1) = "cod_sonho"
    CamposEcran(1) = "cod_sonho"
    Tamanhos(1) = 800
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_sonho"
    CamposEcran(2) = "descr_sonho"
    Tamanhos(2) = 4500
    Headers(2) = "Descri��o"
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_analises_sonho"
    CampoPesquisa = "descr_sonho"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                 CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "ORDER BY descr_sonho", " R�bricas do SONHO")

    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcDescrSONHO = Resultados(2)
            EcCodSONHO.text = Resultados(1)
            EcCodSONHO.SetFocus
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem r�bricas do SONHO.       ", vbExclamation, " ATEN��O"
        EcCodAnaS.SetFocus
    End If

End Sub

Private Sub ButSelNada_Click()
   
    On Error Resume Next
    
    Dim i As Integer
    
    Me.ListaEFR.Visible = False
    For i = Me.ListaEFR.ListCount To 0 Step -1
        Me.ListaEFR.Selected(i) = False
    Next
    Me.ListaEFR.ListIndex = 0
    Me.ListaEFR.Visible = True

End Sub

Private Sub ButSelTudo_Click()
    
    On Error Resume Next
    
    Dim i As Integer
    
    Me.ListaEFR.Visible = False
    For i = Me.ListaEFR.ListCount To 0 Step -1
        Me.ListaEFR.Selected(i) = True
    Next
    Me.ListaEFR.ListIndex = 0
    Me.ListaEFR.Visible = True

End Sub

Private Sub CbProva_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao CbProva, KeyCode

End Sub

Private Sub EcCodana_Validate(Cancel As Boolean)
    
    Dim descrAna As String
        
    EcCodAna.text = Trim(EcCodAna.text)
    
    If Trim(EcCodAna.text) <> "" Then
        
        EcCodAna.text = UCase(EcCodAna.text)
        
        EcDescrAna = ValidaAnalise(EcCodAna.text)
        If Trim(EcDescrAna.text) = "" Then
            Cancel = True
            'SendKeys ("{HOME}+{END}")
        End If
    End If

End Sub

Private Sub EcCodAnaC_LostFocus()

    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    EcCodAnaC.text = Trim(UCase(EcCodAnaC.text))
    
    If EcCodAnaC.text <> "" Then
        sql = "SELECT descr_ana_c FROM sl_ana_c WHERE cod_ana_c=" & BL_TrataStringParaBD(EcCodAnaC.text)
        Set rsAna = New ADODB.recordset
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        rsAna.Open sql, gConexao
        If rsAna.RecordCount > 0 Then
            EcDescrAnaC.text = BL_HandleNull(rsAna!Descr_Ana_C, "")
            EcCodAna.text = EcCodAnaC.text
            EcDescrAna.text = EcDescrAnaC.text
        Else
            BG_Mensagem mediMsgBox, "A an�lise n�o existe", vbExclamation, "SISLAB"
            EcCodAnaC.text = ""
            EcCodAnaC.SetFocus
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
    
End Sub

Private Sub EcCodAnaP_LostFocus()

    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    EcCodAnaP.text = Trim(UCase(EcCodAnaP.text))
    
    If EcCodAnaP.text <> "" Then
        sql = "SELECT descr_perfis FROM sl_perfis WHERE cod_perfis=" & BL_TrataStringParaBD(EcCodAnaP.text)
        Set rsAna = New ADODB.recordset
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        rsAna.Open sql, gConexao
        If rsAna.RecordCount > 0 Then
            EcDescrAnaP.text = BL_HandleNull(rsAna!descr_perfis, "")
            EcCodAna.text = EcCodAnaP.text
            EcDescrAna.text = EcDescrAnaP.text
        Else
            BG_Mensagem mediMsgBox, "A an�lise n�o existe", vbExclamation, "SISLAB"
            EcCodAnaP.text = ""
            EcCodAnaP.SetFocus
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
    
End Sub

Private Sub EcCodAnaS_LostFocus()

    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    EcCodAnaS.text = Trim(UCase(EcCodAnaS.text))
    
    If EcCodAnaS.text <> "" Then
        sql = "SELECT descr_ana_s FROM sl_ana_s WHERE cod_ana_s=" & BL_TrataStringParaBD(EcCodAnaS.text)
        Set rsAna = New ADODB.recordset
        rsAna.CursorLocation = adUseClient
        rsAna.CursorType = adOpenStatic
        rsAna.Open sql, gConexao
        If rsAna.RecordCount > 0 Then
            EcDescrAnaS.text = BL_HandleNull(rsAna!descr_ana_s, "")
            EcCodAna.text = EcCodAnaS.text
            EcDescrAna.text = EcDescrAnaS.text
        Else
            BG_Mensagem mediMsgBox, "A an�lise n�o existe", vbExclamation, "SISLAB"
            EcCodAnaS.text = ""
            EcCodAnaS.SetFocus
        End If
        rsAna.Close
        Set rsAna = Nothing
    End If
    
End Sub


Private Sub EcCodProduto_Validate(Cancel As Boolean)
    Dim sql As String
    Dim RsProd As ADODB.recordset
        
    EcCodProduto.text = Trim(EcCodProduto.text)
    
    If Trim(EcCodProduto.text) <> "" Then
    
        EcCodProduto.text = UCase(EcCodProduto.text)
        sql = "SELECT cod_produto from sl_produto WHERE cod_produto = " & BL_TrataStringParaBD(EcCodProduto.text)
        Set RsProd = New ADODB.recordset
        RsProd.CursorLocation = adUseClient
        RsProd.CursorType = adOpenStatic
        RsProd.Open sql, gConexao
        If RsProd.RecordCount > 0 Then
            EcCodProduto.text = BL_HandleNull(RsProd!cod_produto, "")
        Else
            BG_Mensagem mediMsgBox, "Produto inexistente!", vbInformation
        End If
        RsProd.Close
        Set RsProd = Nothing
    End If

End Sub

Private Sub EcCodSONHO_Validate(Cancel As Boolean)

    Dim sql As String
    Dim RsDescrGH As ADODB.recordset
        
    EcCodSONHO.text = Trim(EcCodSONHO.text)
    
    If Trim(EcCodSONHO.text) <> "" Then
    
        EcCodSONHO.text = UCase(EcCodSONHO.text)
        sql = "SELECT descr_sonho from sl_analises_sonho WHERE cod_sonho = " & BL_TrataStringParaBD(EcCodSONHO.text)
        'Sql = "SELECT descr_ana_facturacao FROM sl_ana_facturacao WHERE cod_ana_gh=" & BL_TrataStringParaBD(EcCodSONHO.Text)
        Set RsDescrGH = New ADODB.recordset
        RsDescrGH.CursorLocation = adUseClient
        RsDescrGH.CursorType = adOpenStatic
        RsDescrGH.Open sql, gConexao
        If RsDescrGH.RecordCount > 0 Then
            'EcDescrSONHO.Text = BL_HandleNull(RsDescrGH!descr_ana_facturacao, "")
            EcDescrSONHO.text = BL_HandleNull(RsDescrGH!descr_sonho, "")
        End If
        RsDescrGH.Close
        Set RsDescrGH = Nothing
    End If

End Sub

Private Sub EcMetodoTSQ_KeyDown(KeyCode As Integer, Shift As Integer)
    
    BG_LimpaOpcao EcMetodoTSQ, KeyCode

End Sub

Private Sub EcSeqAna_Validate(Cancel As Boolean)

    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcSeqAna)

End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate
    
End Sub

Sub Form_Unload(Cancel As Integer)

    EventoUnload
    
End Sub

Sub Inicializacoes()
    
    Me.caption = " Mapeamento de R�bricas para o SONHO"
    
    Me.Left = 400
    Me.Top = 20
    Me.Width = 8640
    Me.Height = 7740 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    nomeTabela = "sl_ana_facturacao"
    Set CampoDeFocus = EcCodAna
    
    NumCampos = 10
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_ana"
    CamposBD(1) = "cod_ana"
'    CamposBD(2) = "descr_ana"
    CamposBD(2) = "cod_ana_gh"
'    CamposBD(4) = "descr_ana_facturacao"
    CamposBD(3) = "cod_prod"
    CamposBD(4) = "prova"
    CamposBD(5) = "metodo_tsq"
    CamposBD(6) = "flg_regra"
    CamposBD(7) = "cod_ana_regra"
    CamposBD(8) = "factura_membros"
    CamposBD(9) = "centro_custo"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcSeqAna
    Set CamposEc(1) = EcCodAna
'    Set CamposEc(2) = EcDescrAna
    Set CamposEc(2) = EcCodSONHO
'    Set CamposEc(4) = Me.EcDescrSONHO
    Set CamposEc(3) = EcCodProduto
    Set CamposEc(4) = EcProva
    Set CamposEc(5) = EcMetodoTSQ
    Set CamposEc(6) = CkFlgRegra
    Set CamposEc(7) = EcCodAnaRegra
    Set CamposEc(8) = CkFacturaMembros
    Set CamposEc(9) = EcCentroCusto
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    '...
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_ana"
    Set ChaveEc = EcSeqAna
    
'    EcDescrAnaS.Enabled = False
'    EcCodAnaS.Enabled = False
    
    BtPesqRapS.Enabled = True
    
'    EcDescrAnaC.Enabled = False
'    EcCodAnaC.Enabled = False
    
    BtPesqRapC.Enabled = True
    
'    EcDescrAnaP.Enabled = False
'    EcCodAnaP.Enabled = False
    
    BtPesqRapP.Enabled = True
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocais, mediAscComboCodigo
    
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    
    CbProva.AddItem "AUTOMATIZADA"
    CbProva.AddItem "MANUAL"
    CbProva.AddItem "PCR"
    CbProva.AddItem "IMA"
    
    BG_PreencheComboBD_ADO "sl_tbf_metodo_tsq", "cod_metodo_tsq", "descr_metodo_tsq", EcMetodoTSQ
    
    BG_ParametrizaPermissoes_ADO Me.Name
    
    Estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormFACTUSNovasAnalises = Nothing
    
End Sub

Sub LimpaCampos()
    Dim i As Integer
    Me.SetFocus
    For i = 0 To EcLocais.ListCount - 1
        EcLocais.Selected(i) = False
    Next
    BG_LimpaCampo_Todos CamposEc
    
    EcCodAnaS.text = ""
    EcCodAnaC.text = ""
    EcCodAnaP.text = ""
    
    EcDescrAnaS.text = ""
    EcDescrAnaC.text = ""
    EcDescrAnaP.text = ""
    
    EcCodMicro.text = ""
    EcDescrMicro.text = ""
    
    CbProva.ListIndex = mediComboValorNull
    EcMetodoTSQ.ListIndex = mediComboValorNull
    EcProva.text = ""
    
    EcCodAna.text = ""
    EcDescrAna.text = ""
    EcDescrSONHO.text = ""
    
'    EcDescrAnaS.Enabled = False
'    EcCodAnaS.Enabled = False
    
'    BtPesqRapS.Enabled = False
    
'    EcDescrAnaC.Enabled = False
'    EcCodAnaC.Enabled = False
    
'    BtPesqRapC.Enabled = False

'    EcDescrAnaP.Enabled = False
'    EcCodAnaP.Enabled = False
    
'    BtPesqRapP.Enabled = False
    
'    Me.ListaEFR.Clear
    
    Call Me.ListCodSONHO.Clear
    Call Me.ListDescrSONHO.Clear
    
    CkFlgRegra.Value = vbGrayed
    CkFacturaMembros.Value = vbGrayed
    
    DoEvents
    
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_Todos_ADO nomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
    EcSeqAna.Tag = adNumeric
    CkFlgRegra.Value = vbGrayed
    CkFacturaMembros.Value = vbGrayed
    
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        CarregaLocaisMapeam EcSeqAna
'        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Remover", "Activo"
       ' BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
        ' Preenche a listbox com entidades a n�o facturar.
'        Me.ListaEFR.Visible = False
'        Call Carrega_EFRs(Me.ListaEFR, Trim(EcCodAna))
'        Me.ListaEFR.Visible = True

        Call Preenche_Listas_SONHO(Me.EcCodAna, Me.EcSeqAna)
    
        If rs!Prova = "A" Then
            CbProva.ListIndex = 0
        ElseIf rs!Prova = "M" Then
            CbProva.ListIndex = 1
        ElseIf rs!Prova = "P" Then
            CbProva.ListIndex = 2
        ElseIf rs!Prova = "I" Then
            CbProva.ListIndex = 3
        End If
    End If
    
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
'    If ((Len(Trim(Me.EcSeqAna.Text)) = 0) And _
'        (Len(Trim(Me.EcDescrAna.Text)) = 0) And _
'        (Len(Trim(Me.EcCodSONHO.Text)) = 0) And _
'        (Len(Trim(Me.EcCodAna.Text)) = 0)) Then

'        MsgBox "Deve Indicar um crit�rio de pesquisa.    ", vbInformation, Me.Caption
'        Exit Sub
'    End If
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(nomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & "  ORDER BY seq_ana ASC"
    Else
        CriterioTabela = CriterioTabela & "  ORDER BY descr_ana, descr_ana_facturacao ASC"
    End If
              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseClient
    
    rs.Open CriterioTabela, gConexao
    
    If (rs.RecordCount <= 0) Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        Estado = 2
        Call LimpaCampos
        Call PreencheCampos
    
        BL_ToolbarEstadoN Estado
'        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
        BL_Toolbar_BotaoEstado "Modificar", "Activo"
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
        
End Sub

Sub FuncaoAnterior()

    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    gMsgTitulo = " Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
   
        ' ----------------------------------------------------
        
        Dim cod_aux As String
        Dim descr_aux As String
    
        cod_aux = Me.EcCodAna.text
        descr_aux = Me.EcDescrAna.text

        ' ----------------------------------------------------
            
            sql = "SELECT cod_ana_gh FROM sl_ana_facturacao WHERE (cod_ana_gh=" & BL_TrataStringParaBD(EcCodSONHO.text) & _
                " AND cod_ana=" & BL_TrataStringParaBD(EcCodAna.text) & ") OR (" & _
                " cod_ana_gh=" & BL_TrataStringParaBD(EcCodSONHO.text) & _
                " AND flg_so_facturacao <> 1)"
            
            Set rsAna = New ADODB.recordset
            rsAna.CursorLocation = adUseClient
            rsAna.CursorType = adOpenStatic
            rsAna.Open sql, gConexao
            If rsAna.RecordCount > 0 Then
                'BG_Mensagem mediMsgBox, "J� existe uma rubrica para essa an�lise.", vbExclamation, App.ProductName
                'RsAna.Close
                'Set RsAna = Nothing
                'Exit Sub
            End If
            rsAna.Close
            Set rsAna = Nothing
    
        BL_InicioProcessamento Me, "A inserir registo."
        iRes = ValidaCamposEc
        EcProva.text = Mid(CbProva.text, 1, 1)
        If iRes = True Then
            BD_Insert
            GravaLocaisMapeam CLng(EcSeqAna)
        End If
                                
        BL_FimProcessamento Me
    
        ' ----------------------------------------------------
    
        Call LimpaCampos

'        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
'        BL_Toolbar_BotaoEstado "Inserir", "Activo"
'        BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
        EcCodAna.text = cod_aux
        EcDescrAna.text = descr_aux
        
        Call FuncaoProcurar
        
        ' ----------------------------------------------------
    
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    Dim rv As Integer
    
    gSQLError = 0
    gSQLISAM = 0
    
    EcSeqAna.text = BG_DaMAX(nomeTabela, "seq_ana") + 1
        
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(nomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
'    rv = Grava_EFRs_Nao_Facturar(EcCodAna.Text, Me.ListaEFR)
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    Dim sql As String
    Dim rsAna As ADODB.recordset
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
   
            Dim str_aux As String
            
            str_aux = Trim(EcSeqAna.text)
            If (str_aux = "") Then
                str_aux = -1
            End If
            
'            Sql = "SELECT cod_ana_gh FROM sl_ana_facturacao WHERE ((cod_ana_gh=" & BL_TrataStringParaBD(EcCodSONHO.text) & _
'                " AND cod_ana=" & BL_TrataStringParaBD(EcCodAna.text) & ") OR (" & _
'                " cod_ana_gh=" & BL_TrataStringParaBD(EcCodSONHO.text) & _
'                " AND flg_so_facturacao <> 1)) AND seq_ana <> " & str_aux
'            Set RsAna = New ADODB.recordset
'            RsAna.CursorLocation = adUseClient
'            RsAna.CursorType = adOpenStatic
'            RsAna.Open Sql, gConexao
'            If RsAna.RecordCount > 0 Then
'                BG_Mensagem mediMsgBox, "J� existe uma rubrica para essa an�lise.", vbExclamation, App.ProductName
'                RsAna.Close
'                Set RsAna = Nothing
'                Exit Sub
'            End If
'            RsAna.Close
'            Set RsAna = Nothing
        
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            EcProva.text = Mid(CbProva.text, 1, 1)
            BD_Update
            GravaLocaisMapeam CLng(EcSeqAna)
        End If
        
        ' ----------------------------------------------------
        
        Dim cod_aux As String
        Dim descr_aux As String
    
        cod_aux = Me.EcCodAna.text
        descr_aux = Me.EcDescrAna.text
    
        Call LimpaCampos

'        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
'        BL_Toolbar_BotaoEstado "Inserir", "Activo"
'        BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
        EcCodAna.text = cod_aux
        EcDescrAna.text = descr_aux
        
        Call FuncaoProcurar
        
        ' ----------------------------------------------------
        
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    Dim rv As Integer
        
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(nomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
    
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
   
'    rv = Grava_EFRs_Nao_Facturar(EcCodAna.Text, Me.ListaEFR)

End Sub

Sub FuncaoRemover()

    Dim iRes As Integer
    
    gMsgTitulo = " Apagar"
    gMsgMsg = "Tem a certeza que quer Apagar estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
     
        ' ----------------------------------------------------
        
        Dim cod_aux As String
        Dim descr_aux As String
    
        cod_aux = Me.EcCodAna.text
        descr_aux = Me.EcDescrAna.text

        ' ----------------------------------------------------
        
        BL_InicioProcessamento Me, "A apagar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Delete
        End If
        
        BL_FimProcessamento Me
    
        ' ----------------------------------------------------
    
        Call LimpaCampos

'        BL_Toolbar_BotaoEstado "Modificar", "InActivo"
'        BL_Toolbar_BotaoEstado "Inserir", "Activo"
'        BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
        EcCodAna.text = cod_aux
        EcDescrAna.text = descr_aux
        
        Call FuncaoProcurar
        
        ' ----------------------------------------------------
    
    End If

End Sub

Sub BD_Delete()
   
    Dim condicao As String
    Dim SQLQuery As String
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & nomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    FuncaoLimpar

End Sub

Private Function Carrega_EFRs(lista As ListBox, _
                              cod_ana As String) As Integer

    ' Carrega a lista de EFRs.
    
    On Error GoTo ErrorHandler
    
    Dim rs_aux As ADODB.recordset
    Dim sql As String
    Dim ret As String
    Dim cod_efr As String
    
    ret = ""
    
    cod_ana = UCase(Trim(cod_ana))
    
    If (Len(cod_ana) = 0) Then
        Carrega_EFRs = -2
        Exit Function
    End If
        
    sql = "SELECT " & _
          "     cod_efr, " & _
          "     descr_efr " & _
          "FROM " & _
          "     sl_efr " & _
          "ORDER BY " & _
          "     descr_efr"
    
    Set rs_aux = New ADODB.recordset
    rs_aux.CursorLocation = adUseServer
    rs_aux.CursorType = adOpenForwardOnly
    rs_aux.Open sql, gConexao
        
    lista.Clear
    DoEvents
    DoEvents
    
    While Not (rs_aux.EOF)
        If (Not IsNull(rs_aux(1))) Then
            
            If (Not IsNull(rs_aux(0))) Then
               cod_efr = Trim(rs_aux(0))
               
               lista.AddItem Trim(rs_aux(1))
               lista.ItemData(lista.ListCount - 1) = CLng(cod_efr)
               
               If (ANALISE_Para_Facturar(Trim(EcCodAna.text), cod_efr) = True) Then
                    lista.Selected(lista.ListCount - 1) = False
               Else
                    lista.Selected(lista.ListCount - 1) = True
               End If
               
            End If
        
        End If
        rs_aux.MoveNext
    Wend
    lista.ListIndex = 0
    
    rs_aux.Close
    Set rs_aux = Nothing
    
    Carrega_EFRs = 1

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Carrega_EFRs (FormSONHONovasAnalises) -> " & Err.Description)
            Carrega_EFRs = -1
            Exit Function
    End Select
End Function

Private Function Grava_EFRs_Nao_Facturar(cod_ana As String, _
                                         lista As ListBox) As Integer
    
    On Error GoTo ErrorHandler
    
    Dim i As Integer
    Dim sql As String
    Dim cmd As ADODB.Command

    cod_ana = Trim(cod_ana)
    If (Len(cod_ana) = 0) Then
        Grava_EFRs_Nao_Facturar = -2
        Exit Function
    End If
    
    Call BG_BeginTransaction
    
    ' Apaga as refer�ncias existentes na tabela para a an�lise.
    
    Set cmd = New ADODB.Command
    
    sql = "DELETE " & _
          "FROM " & _
          "     sl_nao_facturar " & _
          "WHERE " & _
          "     cod_ana = '" & cod_ana & "'"
    
    cmd.ActiveConnection = gConexao
    cmd.CommandText = sql
    cmd.Execute
        
    
    ' Insere cada uma das linhas da lista.
    
    For i = 0 To lista.ListCount - 1
        If (lista.Selected(i)) Then
            sql = "INSERT INTO sl_nao_facturar " & vbCrLf & _
                  "(cod_ana, cod_efr) " & vbCrLf & _
                  "VALUES " & vbCrLf & _
                  "('" & cod_ana & "', " & lista.ItemData(i) & ")"
        
            cmd.CommandText = sql
            cmd.Execute
        End If
    Next
    
    Set cmd = Nothing
    
    Call BG_CommitTransaction
    
    Grava_EFRs_Nao_Facturar = 1

Exit Function
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("Erro Inesperado : Grava_EFRs_Nao_Facturar = 1 (FormSONHONovasAnalises) -> " & Err.Description)
            Call BG_RollbackTransaction
            Grava_EFRs_Nao_Facturar = -1
            Exit Function
    End Select
End Function

Function ValidaAnalise(codAna As String) As String
    
    Dim sql As String
    Dim rsAna As ADODB.recordset

    ValidaAnalise = ""
    sql = "(SELECT descr_ana_s descricao FROM sl_ana_s WHERE cod_ana_s=" & BL_TrataStringParaBD(codAna) & _
    ") UNION " & _
    "(SELECT descr_ana_c descricao FROM sl_ana_c WHERE cod_ana_c=" & BL_TrataStringParaBD(codAna) & _
    ") UNION " & _
    "(SELECT descr_perfis descricao FROM sl_perfis WHERE cod_perfis=" & BL_TrataStringParaBD(codAna) & _
    ") UNION " & _
    "(SELECT descr_microrg descricao FROM sl_microrg WHERE cod_microrg=" & BL_TrataStringParaBD(codAna) & ")"
    Set rsAna = New ADODB.recordset
    rsAna.CursorLocation = adUseClient
    rsAna.CursorType = adOpenStatic
    rsAna.Open sql, gConexao
    If rsAna.RecordCount > 0 Then
        ValidaAnalise = BL_HandleNull(rsAna!descricao, "Sem Descricao")
    End If
    rsAna.Close
    Set rsAna = Nothing
    
End Function

Sub Preenche_Listas_SONHO(cod_SISLAB As String, _
                          seq_ana As String)

    On Error GoTo ErrorHandler
    
    Dim sql As String
    Dim rs As ADODB.recordset

    cod_SISLAB = UCase(Trim(cod_SISLAB))
    seq_ana = UCase(Trim(seq_ana))
    
    If ((Len(cod_SISLAB) = 0) Or _
        (Len(seq_ana) = 0)) Then
        Exit Sub
    End If
    
    Call Me.ListCodSONHO.Clear
    Call Me.ListDescrSONHO.Clear
    
    sql = "SELECT " & vbCrLf & _
          "    cod_ana_gh, " & vbCrLf & _
          "    descr_ana_facturacao " & vbCrLf & _
          "FROM " & vbCrLf & _
          "    sl_ana_facturacao " & vbCrLf & _
          "WHERE " & vbCrLf & _
          "    cod_ana = '" & cod_SISLAB & "' AND " & vbCrLf & _
          "    seq_ana <> " & seq_ana & " " & vbCrLf & _
          "ORDER BY " & vbCrLf & _
          "    descr_ana_facturacao "

    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseClient
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    
    rs.Open sql, gConexao
    
    While Not (rs.EOF)
        Me.ListCodSONHO.AddItem (BL_HandleNull(rs!cod_ana_gh, ""))
        Me.ListDescrSONHO.AddItem (BL_HandleNull(rs!descr_ana_facturacao, ""))
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
    
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            Set rs = Nothing
            Call Me.ListCodSONHO.Clear
            Call Me.ListDescrSONHO.Clear
            ' Erro inesperado.
            Call BG_LogFile_Erros("Preenche_Listas_SONHO (FormSONHONovasAnalises) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
    End Select
End Sub



Private Sub ListCodSONHO_Click()
    
    Dim cod_aux As String
    Dim descr_aux As String
    
    cod_aux = Me.ListCodSONHO.List(Me.ListCodSONHO.ListIndex)
    descr_aux = Me.ListDescrSONHO.List(Me.ListCodSONHO.ListIndex)
    
    Call LimpaCampos

    Me.EcCodSONHO.text = cod_aux
    Me.EcDescrSONHO.text = descr_aux
    
    Call FuncaoProcurar
    
    EcCodSONHO.SetFocus

End Sub

Private Sub ListDescrSONHO_Click()
    
    Dim cod_aux As String
    Dim descr_aux As String
    
    cod_aux = Me.ListCodSONHO.List(Me.ListDescrSONHO.ListIndex)
    descr_aux = Me.ListDescrSONHO.List(Me.ListDescrSONHO.ListIndex)
    
    Call LimpaCampos

    Me.EcCodSONHO.text = cod_aux
    Me.EcDescrSONHO.text = descr_aux
    
    Call FuncaoProcurar
    
    EcCodSONHO.SetFocus

End Sub

' ------------------------------------------------------------------------------------------------

' CARREGA LOCAIS ASSOCIADOS A PROVENIENCIA

' ------------------------------------------------------------------------------------------------
Public Sub CarregaLocaisMapeam(seq_ana As String)
    Dim sSql As String
    Dim rsAnaLocais As New ADODB.recordset
    On Error GoTo TrataErro
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_ass_ana_fact_locais WHERE seq_ana = " & seq_ana
    Set rsAnaLocais = BG_ExecutaSELECT(sSql)
    If rsAnaLocais.RecordCount > 0 Then
        While Not rsAnaLocais.EOF
            For i = 0 To EcLocais.ListCount - 1
                If EcLocais.ItemData(i) = BL_HandleNull(rsAnaLocais!cod_local, "") Then
                    EcLocais.Selected(i) = True
                End If
            Next
            rsAnaLocais.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao carregar locais: " & sSql & " " & Err.Description, Me.Name, "CarregaLocaisMapeam", False
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------------------------

' GRAVA LOCAIS ASSOCIADOS A

' ------------------------------------------------------------------------------------------------

Public Sub GravaLocaisMapeam(seq_ana As Long)
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    sSql = "DELETE FROM sl_ass_ana_fact_locais WHERE seq_ana = " & seq_ana
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To EcLocais.ListCount - 1
        If EcLocais.Selected(i) = True Then
            sSql = "INSERT into sl_ass_ana_fact_locais (seq_ana, cod_local) VALUES("
            sSql = sSql & (seq_ana) & ","
            sSql = sSql & EcLocais.ItemData(i) & ")"
            BG_ExecutaQuery_ADO sSql
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Gravar locais: " & sSql & " " & Err.Description, Me.Name, "GravaLocaisMapeam", False
    Exit Sub
    Resume Next
End Sub



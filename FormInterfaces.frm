VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormInterfaces 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " "
   ClientHeight    =   9060
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13605
   Icon            =   "FormInterfaces.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9060
   ScaleWidth      =   13605
   ShowInTaskbar   =   0   'False
   Begin RichTextLib.RichTextBox RText 
      Height          =   1215
      Left            =   600
      TabIndex        =   28
      Top             =   9120
      Visible         =   0   'False
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   2143
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"FormInterfaces.frx":000C
   End
   Begin MSFlexGridLib.MSFlexGrid FgInterfaces 
      Height          =   5295
      Left            =   240
      TabIndex        =   27
      Top             =   2760
      Width           =   13095
      _ExtentX        =   23098
      _ExtentY        =   9340
      _Version        =   393216
      BackColorBkg    =   -2147483633
      GridLinesFixed  =   1
      BorderStyle     =   0
   End
   Begin VB.CheckBox CkSMS 
      Caption         =   "Avisar Utente por SMS"
      Height          =   195
      Left            =   2160
      TabIndex        =   26
      Top             =   2160
      Width           =   1935
   End
   Begin VB.CheckBox CkImpress�o 
      Caption         =   "Imprimir Resultados"
      Height          =   255
      Left            =   360
      MaskColor       =   &H8000000F&
      TabIndex        =   25
      Top             =   2160
      Width           =   1815
   End
   Begin VB.PictureBox Pic1 
      Height          =   495
      Left            =   1440
      ScaleHeight     =   435
      ScaleWidth      =   795
      TabIndex        =   22
      Top             =   9120
      Width           =   855
   End
   Begin VB.CommandButton BtValidar 
      Height          =   615
      Left            =   5760
      Picture         =   "FormInterfaces.frx":008E
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   8160
      Width           =   1695
   End
   Begin VB.Frame FrameEmail 
      Caption         =   "Email"
      Height          =   615
      Left            =   4320
      TabIndex        =   4
      Top             =   2000
      Width           =   9015
      Begin VB.ComboBox CbTipoEnvio 
         Height          =   315
         Left            =   7560
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   240
         Width           =   1335
      End
      Begin VB.CheckBox CkMarcarImpressa 
         Caption         =   "Marcar Como Impressa"
         Height          =   255
         Left            =   4440
         TabIndex        =   11
         Top             =   240
         Width           =   1935
      End
      Begin VB.CheckBox CkAgrupar 
         Caption         =   "Agrupar Anexos"
         Height          =   195
         Left            =   2640
         TabIndex        =   10
         Top             =   240
         Width           =   1455
      End
      Begin VB.CheckBox CkEnvioMail 
         Caption         =   "Envio Resultados porEmail"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   2415
      End
      Begin VB.Label Label1 
         Caption         =   "Destinat�rio"
         Height          =   255
         Index           =   3
         Left            =   6600
         TabIndex        =   24
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame FramePesq 
      Caption         =   "Tipo Pesquisa"
      Height          =   1815
      Left            =   360
      TabIndex        =   0
      Top             =   120
      Width           =   8655
      Begin VB.CommandButton BtPesquisaSala 
         Height          =   315
         Left            =   8160
         Picture         =   "FormInterfaces.frx":0D58
         Style           =   1  'Graphical
         TabIndex        =   21
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   1440
         Width           =   375
      End
      Begin VB.TextBox EcDescrSala 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   5160
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   1440
         Width           =   3015
      End
      Begin VB.TextBox EcCodSala 
         Height          =   315
         Left            =   4560
         TabIndex        =   19
         Top             =   1440
         Width           =   615
      End
      Begin VB.TextBox EcCodMedico 
         Height          =   315
         Left            =   4560
         TabIndex        =   18
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox EcDescrMedico 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   5160
         Locked          =   -1  'True
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   960
         Width           =   3015
      End
      Begin VB.CommandButton BtPesquisaMedico 
         Height          =   315
         Left            =   8160
         Picture         =   "FormInterfaces.frx":12E2
         Style           =   1  'Graphical
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox EcCodProveniencia 
         Height          =   315
         Left            =   4560
         TabIndex        =   14
         Top             =   480
         Width           =   615
      End
      Begin VB.TextBox EcDescrProveniencia 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   5160
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   480
         Width           =   3015
      End
      Begin VB.CommandButton BtPesquisaProveniencia 
         Height          =   315
         Left            =   8160
         Picture         =   "FormInterfaces.frx":186C
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
         Top             =   480
         Width           =   375
      End
      Begin VB.TextBox EcNumReq 
         Height          =   285
         Left            =   2040
         TabIndex        =   6
         Top             =   1440
         Width           =   1095
      End
      Begin VB.OptionButton OptPesquisa 
         Caption         =   "Por Requisi��o"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   3
         Top             =   1440
         Width           =   1575
      End
      Begin VB.OptionButton OptPesquisa 
         Caption         =   "Requisi��es Prontas"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   2
         Top             =   960
         Width           =   1935
      End
      Begin VB.OptionButton OptPesquisa 
         Caption         =   "Requisi��es Completas"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "M�dico"
         Height          =   255
         Index           =   0
         Left            =   3480
         TabIndex        =   15
         Top             =   975
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Posto"
         Height          =   255
         Index           =   2
         Left            =   3480
         TabIndex        =   9
         Top             =   1440
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Proveni�ncia"
         Height          =   255
         Index           =   1
         Left            =   3480
         TabIndex        =   8
         Top             =   495
         Width           =   1095
      End
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   3120
      Top             =   9120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormInterfaces.frx":1DF6
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormInterfaces.frx":2190
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormInterfaces.frx":252A
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormInterfaces"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Vari�veis Globais para este Form.
Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Const LinhasDefeito = 18
Const ColunasDefeito = 8

Dim ColunaActual As Integer
Dim LinhaActual As Integer


' ------------------------------------------------------
' COLUNAS DISPONIVEIS
' ------------------------------------------------------
Const lColNumReq = 0
Const lColUtente = 1
Const lColNome = 2
Const lColProven = 3
Const lColData = 4
Const lColImpressao = 5
Const lColEmail = 6
Const lColSMS = 7


' ------------------------------------------------------
' CORES DISPONIVEIS
' ------------------------------------------------------
Const Amarelo = &HC0C0&
Const AmareloClaro = &HC0FFFF
Const azul = &HFFC0C0
Const CastanhoClaro = &H80FF&
Const CastanhoEscuro = &H4080&
Const Cinza = &HE0E0E0
Const Cinza2 = &HC0C0C0
Const Cinza3 = &H404040
Const cinzento = &H8000000F
Const Laranja = &HC0E0FF
Const Verde = &H808000
Const VerdeClaro = &HC0FFC0
Const VermelhoClaro = &HBDC8F4
Const Vermelho = &HC0&

' ------------------------------------------------------
' CORES USADAS NA GRELHA INICIAL
' ------------------------------------------------------
Const cor2 = vbWhite
Const cor1 = AmareloClaro


' ------------------------------------------------------
' ESTRUTURA USADA PARA REQUISICOES
' ------------------------------------------------------
Private Type Interfaces
    n_req As String
    utente As String
    t_utente As String
    nome As String
    cod_proven As String
    cod_medico As String
    cod_sala As String
    descr_proven As String
    Dt_Chega As String
    flg_imprimir As Boolean
    flg_email As Boolean
    flg_sms As Boolean
    seq_utente As String
    estado_req As String
    telemovel As Long
    email_ute As String
    email_medico As String
    email_sala As String
    
End Type
Dim EstrutReq() As Interfaces
Dim totalReq As Integer



Private Sub BtValidar_Click()
    Dim i As Integer
    Dim flg_processa As Boolean
    
    For i = 1 To totalReq
        If EstrutReq(i).flg_email = True Or EstrutReq(i).flg_imprimir = True Or EstrutReq(i).flg_sms = True Then
            flg_processa = True
            Exit For
        End If
    Next
    If flg_processa = False Then
        BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
        Exit Sub
    End If
    
    Processa_email
    Processa_sms
    Processa_Impressoes
    
    totalReq = 0
    ReDim EstrutReq(0)
    Limpa_FgInterfaces
End Sub

Private Sub CkEnvioMail_Click()

    If CkEnvioMail.Value = vbChecked Then
        CkAgrupar.Enabled = True
        CkMarcarImpressa.Enabled = True
        CbTipoEnvio.Enabled = True
    Else
        CkAgrupar.Value = vbUnchecked
        CkMarcarImpressa.Value = vbUnchecked
        CbTipoEnvio.ListIndex = mediComboValorNull
        CkAgrupar.Enabled = False
        CkMarcarImpressa.Enabled = False
        CbTipoEnvio.Enabled = False
    End If
End Sub



Private Sub FgInterfaces_DblClick()
    Dim ColLinha
    If FgInterfaces.TextMatrix(FgInterfaces.Row, lColNumReq) <> "" Then
        If FgInterfaces.Col = lColImpressao Then
            If EstrutReq(FgInterfaces.Row).flg_imprimir = True Then
                FgInterfaces.TextMatrix(FgInterfaces.Row, lColImpressao) = ""
                FgInterfaces.Col = lColNumReq
                ColLinha = FgInterfaces.CellBackColor
                FgInterfaces.Col = lColImpressao
                FgInterfaces.CellBackColor = ColLinha
                EstrutReq(FgInterfaces.Row).flg_imprimir = False
            Else
                FgInterfaces.TextMatrix(FgInterfaces.Row, lColImpressao) = "Imprimir"
                FgInterfaces.CellBackColor = VerdeClaro
                EstrutReq(FgInterfaces.Row).flg_imprimir = True
            End If
        End If
    
        If FgInterfaces.Col = lColEmail Then
            If EstrutReq(FgInterfaces.Row).flg_email = True Then
                FgInterfaces.TextMatrix(FgInterfaces.Row, lColEmail) = ""
                FgInterfaces.Col = lColNumReq
                ColLinha = FgInterfaces.CellBackColor
                FgInterfaces.Col = lColEmail
                FgInterfaces.CellBackColor = ColLinha
                EstrutReq(FgInterfaces.Row).flg_email = False
            Else
                FgInterfaces.TextMatrix(FgInterfaces.Row, lColEmail) = "Enviar"
                FgInterfaces.CellBackColor = VerdeClaro
                EstrutReq(FgInterfaces.Row).flg_email = True
            End If
        End If
        
        If FgInterfaces.Col = lColSMS Then
            If EstrutReq(FgInterfaces.Row).flg_sms = True Then
                FgInterfaces.TextMatrix(FgInterfaces.Row, lColSMS) = ""
                FgInterfaces.Col = lColNumReq
                ColLinha = FgInterfaces.CellBackColor
                FgInterfaces.Col = lColSMS
                FgInterfaces.CellBackColor = ColLinha
                EstrutReq(FgInterfaces.Row).flg_sms = False
            Else
                FgInterfaces.TextMatrix(FgInterfaces.Row, lColSMS) = "Enviar"
                FgInterfaces.CellBackColor = VerdeClaro
                EstrutReq(FgInterfaces.Row).flg_sms = True
            End If
        End If
    End If
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    DefTipoCampos
    PreencheValoresDefeito
    Set CampoActivo = Me.ActiveControl
    BG_ParametrizaPermissoes_ADO Me.Name
    Estado = 1
    
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub Inicializacoes()

    Me.Caption = "Interfaces"
    Me.Left = 10
    Me.Top = 0
    Me.Width = 13695
    Me.Height = 9330  ' Normal
    Set CampoDeFocus = OptPesquisa(0)
End Sub

Sub EventoActivate()

    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    BL_ToolbarEstadoN Estado
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    'BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    'BL_Toolbar_BotaoEstado "Procurar", "InActivo"
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    Set FormInterfaces = Nothing
End Sub

Sub LimpaCampos()
    EcNumReq.Visible = False
    OptPesquisa(0).Value = True
    OptPesquisa(1).Value = False
    OptPesquisa(2).Value = False
    EcCodMedico = ""
    EcDescrMedico = ""
    EcCodProveniencia = ""
    EcDescrProveniencia = ""
    EcCodSala = ""
    EcDescrSala = ""
    CkAgrupar.Value = vbUnchecked
    CkEnvioMail.Value = vbUnchecked
    CkImpress�o.Value = vbUnchecked
    CkImpress�o.Value = vbUnchecked
    CkMarcarImpressa.Value = vbUnchecked
    CkSMS.Value = vbUnchecked
    CbTipoEnvio.ListIndex = mediComboValorNull
    Limpa_FgInterfaces
    EcNumReq.Visible = False
    CkAgrupar.Enabled = False
    CkMarcarImpressa.Enabled = False
    CbTipoEnvio.Enabled = False
End Sub

Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        Me.Caption = "Interfaces"
        LimpaCampos
    End If
End Sub

Sub FuncaoEstadoAnterior()

    If Estado = 2 Then
        Estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
    Else
        Unload Me
    End If
    
    
End Sub

Sub PreencheValoresDefeito()
End Sub

Sub FuncaoProcurar()
    Dim sSql As String
    Dim RsRequis As New ADODB.recordset

    totalReq = 0
    ReDim EstrutReq(0)

    Limpa_FgInterfaces
    sSql = ConstroiCriterioREQUIS
    Set RsRequis = New ADODB.recordset
    RsRequis.CursorType = adOpenStatic
    RsRequis.CursorLocation = adUseClient
    RsRequis.Open sSql, gConexao, adOpenStatic
    If RsRequis.RecordCount > 0 Then
        While Not RsRequis.EOF
            ' ---------------------------------------------------------------
            ' PARA CADA REQ. PREENCHE ESTRUTURA
            ' ---------------------------------------------------------------
            PreencheEstrutReq RsRequis!n_req, RsRequis!utente, RsRequis!t_utente, RsRequis!nome_ute, BL_HandleNull(RsRequis!cod_proven, ""), _
                              CkImpress�o.Value, CkEnvioMail.Value, CkSMS.Value, RsRequis!seq_utente, RsRequis!estado_req, BL_HandleNull(RsRequis!telemovel, 0), _
                              BL_HandleNull(RsRequis!Email, ""), BL_HandleNull(RsRequis!cod_med, ""), BL_HandleNull(RsRequis!cod_sala, "")
                              
            RsRequis.MoveNext
        Wend
        ' ---------------------------------------------------------------
        ' NO FIM PREENCHE A GRELHA
        ' ---------------------------------------------------------------
        PreencheGrid
    End If
    RsRequis.Close
    Set RsRequis = Nothing
End Sub

Sub FuncaoImprimir()

End Sub
    
Sub DefTipoCampos()
    Dim contador As Integer
    With FgInterfaces
        .rows = LinhasDefeito
        .FixedRows = 1
        .Cols = ColunasDefeito
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .Row = 0
        
        .ColWidth(0) = 1000
        .Col = 0
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 0) = "Requis."
        
        .ColWidth(1) = 1000
        .Col = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 1) = "Utente"
        
        .ColWidth(2) = 4000
        .Col = 2
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 2) = "Nome"
        
        .ColWidth(3) = 2000
        .Col = 3
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 3) = "Proveni�ncia"
        
        .ColWidth(4) = 1000
        .Col = 4
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 4) = "Data"
        
        .ColWidth(5) = 1000
        .Col = 5
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 5) = "Impress�o"
        
        .ColWidth(6) = 1000
        .Col = 6
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 6) = "Email"
        
        .ColWidth(7) = 1000
        .Col = 7
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 7) = "SMS"
        .Col = 0
        .Row = 1
    End With
    FgInterfaces_Cor 1
    
    BG_PreencheComboBD_ADO "sl_tbf_t_envio", "cod_t_envio", "descr_t_envio", CbTipoEnvio
    
    EcNumReq.Visible = False
    CkAgrupar.Enabled = False
    CkMarcarImpressa.Enabled = False
    CbTipoEnvio.Enabled = False
End Sub


' ----------------------------------------------------------------------------------

' PREENCHE ESTRURURA DE RESULTADOS DE REQUISICOES

' ----------------------------------------------------------------------------------
Private Sub PreencheEstrutReq(n_req As String, utente As String, t_utente As String, nome As String, cod_proven As String, _
                              flg_imprimir As Boolean, flg_email As Boolean, flg_sms As Boolean, seq_utente As String, _
                              estado_req As String, telemovel As Long, email_ute As String, cod_medico As String, _
                              cod_sala As String)
    
    totalReq = totalReq + 1
    ReDim Preserve EstrutReq(totalReq)
    
    EstrutReq(totalReq).n_req = n_req
    EstrutReq(totalReq).utente = utente
    EstrutReq(totalReq).t_utente = t_utente
    EstrutReq(totalReq).nome = nome
    EstrutReq(totalReq).cod_proven = cod_proven
    EstrutReq(totalReq).descr_proven = BL_SelCodigo("SL_PROVEN", "DESCR_PROVEN", "COD_PROVEN", cod_proven)
    EstrutReq(totalReq).estado_req = estado_req
    EstrutReq(totalReq).seq_utente = seq_utente
    EstrutReq(totalReq).telemovel = telemovel
    EstrutReq(totalReq).cod_medico = cod_medico
    EstrutReq(totalReq).cod_sala = cod_sala
    
    EstrutReq(totalReq).email_ute = email_ute
    If cod_sala <> "" Then
        EstrutReq(totalReq).email_sala = BL_SelCodigo("SL_COD_SALAS", "EMAIL", "COD_SALA", cod_sala)
    End If
    If cod_medico <> "" Then
        EstrutReq(totalReq).email_medico = BL_SelCodigo("SL_MEDICOS", "EMAIL", "COD_MED", cod_medico)
    End If
    
    EstrutReq(totalReq).flg_imprimir = flg_imprimir
    EstrutReq(totalReq).flg_email = flg_email
    EstrutReq(totalReq).flg_sms = flg_sms
    
End Sub

Private Function DevolveSeqUtente(utente As String, TUtente As String) As Long
    Dim sSql As String
    Dim RsIdentif As New ADODB.recordset
    
    On Error GoTo TrataErro
    
    sSql = " SELECT seq_utente FROM sl_identif WHERE utente = " & BL_TrataStringParaBD(utente)
    sSql = sSql & " AND t_utente = " & BL_TrataStringParaBD(TUtente)
    
    Set RsIdentif = New ADODB.recordset
    RsIdentif.CursorType = adOpenStatic
    RsIdentif.CursorLocation = adUseClient
    RsIdentif.Open sSql, gConexao, adOpenStatic
    If RsIdentif.RecordCount > 0 Then
        DevolveSeqUtente = BL_HandleNull(RsIdentif!seq_utente, -1)
    Else
        DevolveSeqUtente = -1
    End If
    RsIdentif.Close
    Set RsIdentif = Nothing
    
Exit Function
TrataErro:
    BL_LogFile_BD Me.Name, "DevolveSeqUtente", Err.Number, Err.Description, ""
    Exit Function
    Resume Next
End Function






' ----------------------------------------------------------------------------------



' ----------------------------------------------------------------------------------
Private Function ConstroiCriterioREQUIS() As String
    Dim sSql As String
    Dim sSqlEstadoReq As String
    Dim sSqlEstadoAna As String
    
    If OptPesquisa(0).Value = True Then
        sSqlEstadoReq = " AND sl_requis.estado_req='D' "
        sSqlEstadoAna = ""
        
    ElseIf OptPesquisa(1).Value = True Then
        sSqlEstadoReq = " AND sl_requis.estado_req IN ( 'D', '3') "
        sSqlEstadoAna = " AND '3' IN ( SELECT flg_estado FROM sl_realiza WHERE sl_realiza.n_req = sl_requis.n_req ) "
    End If
    
    sSql = " SELECT sl_requis.n_req,sl_requis.seq_utente, sl_requis.estado_Req,sl_identif.email, "
    sSql = sSql & " sl_identif.seq_utente,sl_identif.utente,sl_identif.t_utente,sl_identif.nome_ute,"
    sSql = sSql & " sl_requis.cod_proven, sl_identif.telemovel, sl_requis.cod_sala, sl_requis.cod_med"
    sSql = sSql & " FROM sl_requis,sl_identif"
    sSql = sSql & " WHERE sl_requis.seq_utente=sl_identif.seq_utente "
    sSql = sSql & sSqlEstadoReq & sSqlEstadoAna
    
    If Trim(EcCodProveniencia.text) <> "" Then
        sSql = sSql + " AND sl_requis.cod_proven=" & BL_TrataStringParaBD(EcCodProveniencia.text)
    End If
    
    If Trim(EcCodSala.text) <> "" Then
        sSql = sSql + " AND sl_requis.cod_sala=" & BL_TrataStringParaBD(EcCodSala.text)
    End If
    If Trim(EcCodMedico.text) <> "" Then
        sSql = sSql + " AND sl_requis.cod_medico=" & BL_TrataStringParaBD(EcCodMedico.text)
    End If
    
    sSql = sSql & " ORDER BY sl_requis.dt_chega ASC"
    

    ConstroiCriterioREQUIS = sSql
End Function



Private Sub FgInterfaces_Cor(indice As Long)
    For indice = 1 To FgInterfaces.rows - 1
        If (indice Mod 2) = 0 Then
            BL_MudaCorFg FgInterfaces, indice, cor1
        Else
            BL_MudaCorFg FgInterfaces, indice, cor2
        End If
    Next
End Sub




Private Sub OptPesquisa_Click(Index As Integer)
    If OptPesquisa(2).Value = True Then
        EcNumReq = ""
        EcNumReq.Visible = True
        EcNumReq.SetFocus
    Else
        EcNumReq.Visible = False
    End If
End Sub
Private Sub EcCodProveniencia_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim Sql As String
    
    If EcCodProveniencia.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        Sql = "SELECT descr_proven FROM sl_proven WHERE cod_proven='" & Trim(EcCodProveniencia.text) & "'"
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        Tabela.Open Sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodProveniencia.text = ""
            EcDescrProveniencia.text = ""
        Else
            EcDescrProveniencia.text = Tabela!descr_proven
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrProveniencia.text = ""
    End If
    
End Sub

Private Sub BtPesquisaProveniencia_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_proven"
    CamposEcran(1) = "cod_proven"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_proven"
    CamposEcran(2) = "descr_proven"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_proven"
    CampoPesquisa1 = "descr_proven"
    
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", "Pesquisar Proveniencias")
    
    mensagem = "N�o foi encontrada nenhuma proveni�ncia."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodProveniencia.text = Resultados(1)
            EcDescrProveniencia.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
    
    EcCodProveniencia.SetFocus
    
End Sub

Private Sub EcCodMedico_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim Sql As String
    
    If EcCodMedico.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        Sql = "SELECT nome_med FROM sl_medicos WHERE cod_med=" & Trim(EcCodMedico.text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        Tabela.Open Sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodMedico.text = ""
            EcDescrMedico.text = ""
        Else
            EcDescrMedico.text = Tabela!nome_med
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrMedico.text = ""
    End If
    
End Sub

Private Sub BtPesquisaMedico_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_med"
    CamposEcran(1) = "cod_med"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "nome_med"
    CamposEcran(2) = "nome_med"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_medicos"
    CampoPesquisa1 = "nome_med"
    
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", "Pesquisar M�dicos")
    
    mensagem = "N�o foi encontrada nenhum registo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodMedico.text = Resultados(1)
            EcDescrMedico.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
    
    EcCodMedico.SetFocus
    
End Sub


Private Sub EcCodsala_Validate(Cancel As Boolean)
    
    Dim Tabela As ADODB.recordset
    Dim Sql As String
    
    If EcCodSala.text <> "" Then
        Set Tabela = New ADODB.recordset
    
        Sql = "SELECT descr_sala FROM sl_cod_salas WHERE cod_sala=" & Trim(EcCodSala.text)
        Tabela.CursorType = adOpenStatic
        Tabela.CursorLocation = adUseClient
        Tabela.Open Sql, gConexao
    
        If Tabela.RecordCount = 0 Then
            Cancel = True
            BG_Mensagem mediMsgBox, "N�o existe registo com esse c�digo !", vbInformation, ""
            EcCodSala.text = ""
            EcDescrSala.text = ""
        Else
            EcDescrSala.text = Tabela!descr_sala
        End If
        Tabela.Close
        Set Tabela = Nothing
    Else
        EcDescrSala.text = ""
    End If
    
End Sub

Private Sub BtPesquisasala_click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim Resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_sala"
    CamposEcran(1) = "cod_sala"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_sala"
    CamposEcran(2) = "descr_sala"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_cod_salas"
    CampoPesquisa1 = "descr_sala"
    
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", "Pesquisar Salas/Postos")
    
    mensagem = "N�o foi encontrada nenhum registo."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodSala.text = Resultados(1)
            EcDescrSala.text = Resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
    
    EcCodSala.SetFocus
    
End Sub


Private Sub PreencheGrid()
    Dim i As Integer
     
     For i = 1 To totalReq
        FgInterfaces.Row = i
        FgInterfaces.TextMatrix(i, lColNumReq) = EstrutReq(i).n_req
        FgInterfaces.TextMatrix(i, lColUtente) = EstrutReq(i).t_utente & "/" & EstrutReq(i).utente
        FgInterfaces.TextMatrix(i, lColNome) = EstrutReq(i).nome
        FgInterfaces.TextMatrix(i, lColProven) = EstrutReq(i).descr_proven
        FgInterfaces.TextMatrix(i, lColData) = EstrutReq(i).Dt_Chega
            
        If EstrutReq(i).flg_imprimir = True Then
            FgInterfaces.Col = lColImpressao
            FgInterfaces.TextMatrix(i, lColImpressao) = "Imprimir"
            FgInterfaces.CellBackColor = VerdeClaro
        Else
            FgInterfaces.TextMatrix(i, lColImpressao) = ""
        End If
        
        If EstrutReq(i).flg_email = True Then
            FgInterfaces.Col = lColEmail
            FgInterfaces.TextMatrix(i, lColEmail) = "Enviar"
            FgInterfaces.CellBackColor = VerdeClaro
        Else
            FgInterfaces.TextMatrix(i, lColEmail) = ""
        End If
        
        If EstrutReq(i).flg_sms = True Then
            FgInterfaces.Col = lColSMS
            FgInterfaces.TextMatrix(i, lColSMS) = "Enviar"
            FgInterfaces.CellBackColor = VerdeClaro
        Else
            FgInterfaces.TextMatrix(i, lColSMS) = ""
        End If
        FgInterfaces.Col = lColNumReq
    Next
End Sub

' ---------------------------------------------------------------------------------------------------------

' MANDA IMPRIMIR TUDO AQUILO QUE ESTA MARCADO PARA IMPRIMIR

' ---------------------------------------------------------------------------------------------------------
Private Sub Processa_Impressoes()
    Dim i As Integer
    
    gImprimirDestino = crptToPrinter
    For i = 1 To totalReq
        If EstrutReq(i).flg_imprimir = True Then
            BL_ImprimeResultados False, False, EstrutReq(i).n_req, EstrutReq(i).estado_req, _
            EstrutReq(i).seq_utente
        End If
    Next
End Sub

' ---------------------------------------------------------------------------------------------------------

' ENVIA POR MAIL TUDO AQUILO QUE ESTA MARCADO PARA ENVIAR EMAIL

' ---------------------------------------------------------------------------------------------------------
Private Sub Processa_email()
    Dim i As Integer
    For i = 1 To totalReq
        If EstrutReq(i).flg_email = True Then
            If EstrutReq(i).email_ute <> "" Then
                EMAIL_CriaMensagem EstrutReq(i).n_req, EstrutReq(i).n_req, EstrutReq(i).email_ute, "C:\", 1
            End If
        End If
    Next
End Sub

' ---------------------------------------------------------------------------------------------------------

' COLOCA NA TABELA SMS_MENSAGEM TODAS AS MENSAGENS QUE S�O PARA ENVIAR

' ---------------------------------------------------------------------------------------------------------
Private Sub Processa_sms()
    Dim i As Integer
    
    For i = 1 To totalReq
        If EstrutReq(i).flg_sms = True Then
            If EstrutReq(i).telemovel > 1 Then
                SMS_CriaMensagem EstrutReq(i).telemovel, "TESTE", Bg_DaData_ADO
            End If
        End If
    Next
End Sub

Private Sub Limpa_FgInterfaces()
    Dim i As Integer
    Dim l As Integer
    For i = FgInterfaces.rows - 1 To 1 Step -1
        For l = 0 To FgInterfaces.Cols - 1
            FgInterfaces.TextMatrix(i, l) = ""
        Next
    Next
    FgInterfaces_Cor 1
End Sub

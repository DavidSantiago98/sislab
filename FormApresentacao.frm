VERSION 5.00
Begin VB.Form FormApresentacao 
   AutoRedraw      =   -1  'True
   BackColor       =   &H80000000&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   4980
   ClientLeft      =   255
   ClientTop       =   1410
   ClientWidth     =   7485
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "FormApresentacao.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MousePointer    =   1  'Arrow
   Picture         =   "FormApresentacao.frx":000C
   ScaleHeight     =   4980
   ScaleWidth      =   7485
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer3 
      Interval        =   5
      Left            =   0
      Top             =   0
   End
   Begin VB.Timer Timer2 
      Interval        =   5
      Left            =   360
      Top             =   0
   End
   Begin VB.PictureBox EcImagem 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   5070
      Left            =   8640
      Picture         =   "FormApresentacao.frx":5BF7
      ScaleHeight     =   5010
      ScaleWidth      =   7515
      TabIndex        =   2
      Top             =   3960
      Visible         =   0   'False
      Width           =   7575
   End
   Begin VB.Timer Timer1 
      Left            =   6240
      Top             =   0
   End
   Begin VB.Label lblProductName 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Product"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   27.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   630
      Left            =   105
      TabIndex        =   1
      Top             =   2040
      Width           =   7245
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblVersion 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Vers�o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   180
      Left            =   75
      TabIndex        =   0
      Top             =   2880
      Width           =   7365
   End
End
Attribute VB_Name = "FormApresentacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit   ' Pada obrigar a definir as vari�veis.

Private m_cDibR As New cDIBSectionRegion

Dim TLevel As Integer
Private Sub CarregaFundo()
    Dim sCaminhoFicheiro As String
    Dim sFicheiro As String
    Dim sCor As Variant

    Dim cDib As New cDIBSection
    
    
    On Error GoTo TrataErro
    
    sCaminhoFicheiro = App.Path & "\SISLAB_REG.ini"
    
    sFicheiro = DevolveValorINI(sCaminhoFicheiro, "Arranque", "Imagem")
    If Dir(sFicheiro) <> "" And sFicheiro <> "" Then
        'Carregar imagem
        
        Set EcImagem.Picture = LoadPicture(sFicheiro)
    End If
    

    cDib.CreateFromPicture EcImagem.Picture
    m_cDibR.Create cDib
    m_cDibR.Applied(Me.hWnd) = True
    
    Set Me.Picture = EcImagem
    
    sCor = DevolveValorINI(sCaminhoFicheiro, "Arranque", "Cor")
    If sCor <> "(Desconhecido)" And sCor <> "" Then
        'preencher cor
        
        Me.BackColor = CLng(sCor)
        lblProductName.BackColor = CLng(sCor)
        lblVersion.BackColor = CLng(sCor)
        
    End If
    
     Set cDib = Nothing
    
    Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro em <CarregaFundo>: " & Err.Number & " - " & Err.Description
    Resume Next
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Unload Me
End Sub

Private Sub Form_Load()

    Me.caption = ""
    
    gAPLICACAO_VERSAO = gMajorRelease & "." & App.Major & "." & App.Minor & "." & App.Revision
    lblVersion.ForeColor = RGB(38, 38, 38)
    lblVersion.caption = "Vers�o  " & gAPLICACAO_VERSAO
    lblProductName.ForeColor = RGB(38, 38, 38)
    lblProductName.caption = cAPLICACAO_NOME_CURTO

    Timer1.Interval = 1500
    
    CarregaFundo
    
    Timer2.Enabled = False

    TLevel = 5
    
    TransForm Me, val(TLevel)
End Sub

Private Sub Frame1_Click()
    Unload Me
End Sub





Private Sub Form_Unload(Cancel As Integer)
    Set m_cDibR = Nothing
End Sub

Private Sub Timer1_Timer()
    Unload Me
End Sub

Private Sub Timer3_Timer()

TLevel = TLevel + 5

TransForm Me, val(TLevel)

    If TLevel = 255 Then
        Timer3.Enabled = False
    End If

End Sub


VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form FormParametrizacaoAcessos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionParametrizacaoAcessos"
   ClientHeight    =   7995
   ClientLeft      =   2775
   ClientTop       =   3120
   ClientWidth     =   9615
   ClipControls    =   0   'False
   Icon            =   "FormParamAcessos.frx":0000
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7995
   ScaleWidth      =   9615
   ShowInTaskbar   =   0   'False
   Begin RichTextLib.RichTextBox RTBAux 
      Height          =   615
      Left            =   360
      TabIndex        =   27
      Top             =   7320
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   1085
      _Version        =   393217
      TextRTF         =   $"FormParamAcessos.frx":000C
   End
   Begin MSComctlLib.ImageList ListaImagens 
      Left            =   8760
      Top             =   2400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ListView LwPropriedades 
      Height          =   3735
      Left            =   4440
      TabIndex        =   26
      Top             =   2400
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   6588
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.TreeView TwObjectosTodos 
      Height          =   3735
      Left            =   240
      TabIndex        =   25
      Top             =   2400
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   6588
      _Version        =   393217
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin VB.TextBox EcCodigoUtilizador 
      Height          =   285
      Left            =   1680
      TabIndex        =   21
      Top             =   6360
      Width           =   975
   End
   Begin VB.TextBox EcCodigoGrupo 
      Height          =   285
      Left            =   4560
      TabIndex        =   20
      Top             =   6360
      Width           =   975
   End
   Begin VB.ComboBox EcUtilizadorCombo 
      Height          =   315
      Left            =   480
      TabIndex        =   1
      Text            =   "EcUtilizadorCombo"
      Top             =   480
      Width           =   3255
   End
   Begin VB.TextBox EcModoEdicao 
      Height          =   300
      Left            =   7680
      TabIndex        =   18
      Tag             =   "Modo de Edicao"
      Top             =   6360
      Width           =   855
   End
   Begin VB.Frame Frame3 
      Caption         =   " Pesquisa "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   16
      Top             =   1680
      Width           =   8415
      Begin VB.CommandButton BtPesquisa 
         Caption         =   "&Pesquisar"
         Height          =   300
         Left            =   7320
         TabIndex        =   10
         Top             =   240
         Width           =   975
      End
      Begin VB.ComboBox EcTipoObjectosPesq 
         Height          =   315
         Left            =   4080
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Tag             =   "Tipo de Objectos a Pesquisar"
         Top             =   240
         Width           =   3135
      End
      Begin VB.TextBox EcChavePesquisa 
         Height          =   300
         Left            =   1200
         TabIndex        =   8
         Tag             =   "Chave de Pesquisa"
         Text            =   "EcChavePesquisa"
         Top             =   240
         Width           =   2895
      End
      Begin VB.Label Label5 
         Caption         =   "Chave Pesq."
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   260
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Informa��o Acessos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   4080
      TabIndex        =   11
      Top             =   0
      Width           =   4575
      Begin VB.ComboBox EcPropriedade 
         Height          =   315
         Left            =   960
         TabIndex        =   6
         Tag             =   "Nome da Propriedade que se est� a parametrizar"
         Text            =   "EcPropriedade"
         Top             =   960
         Width           =   3495
      End
      Begin VB.TextBox EcValorProp 
         Height          =   300
         Left            =   960
         TabIndex        =   7
         Tag             =   "Valor da Propriedade"
         Text            =   "EcValorProp"
         Top             =   1320
         Width           =   3495
      End
      Begin VB.ComboBox EcObjecto 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Tag             =   "Nome do Objecto que se est� a parametrizar"
         Top             =   600
         Width           =   3495
      End
      Begin VB.ComboBox EcContentor 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Tag             =   "Contentor do Objecto que se est� a parametrizar"
         Top             =   240
         Width           =   3495
      End
      Begin VB.Label Label4 
         Caption         =   "Prop."
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Tag             =   "Geralmente o contentor ser� o form, mas pode ser uma Toolbar..."
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Valor Prop."
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Tag             =   "Geralmente o contentor ser� o form, mas pode ser uma Toolbar..."
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Objecto"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Tag             =   "Geralmente o contentor ser� o form, mas pode ser uma Toolbar..."
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Contentor"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Tag             =   "Geralmente o contentor ser� o form, mas pode ser uma Toolbar..."
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.CheckBox ChkGrupo 
      Caption         =   "&Grupo"
      Height          =   375
      Left            =   720
      TabIndex        =   2
      Top             =   1080
      Width           =   855
   End
   Begin VB.Frame Frame1 
      Caption         =   " Grupo/Utilizador "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   240
      TabIndex        =   0
      Top             =   0
      Width           =   3735
      Begin VB.ComboBox EcGrupoCombo 
         Height          =   315
         Left            =   240
         TabIndex        =   19
         Text            =   "EcGrupoCombo"
         Top             =   480
         Width           =   3255
      End
      Begin VB.CheckBox ChkUtilizador 
         Caption         =   "&Utilizador"
         Height          =   375
         Left            =   1920
         TabIndex        =   3
         Top             =   1080
         Width           =   975
      End
   End
   Begin VB.Label Label8 
      Caption         =   "EcModoEdicao"
      Height          =   255
      Left            =   6360
      TabIndex        =   24
      Top             =   6360
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "EcCodigoGrupo"
      Height          =   255
      Left            =   3240
      TabIndex        =   23
      Top             =   6360
      Width           =   1215
   End
   Begin VB.Label Label6 
      Caption         =   "EcCodigoUtilizador"
      Height          =   255
      Left            =   240
      TabIndex        =   22
      Top             =   6360
      Width           =   1455
   End
End
Attribute VB_Name = "FormParametrizacaoAcessos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 21/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

' constantes do modulo
Private Const Grupo = "GRUPO"
Private Const utilizador = "UTILIZADOR"
Private Const NOME_TABELA_PARAM_ACESSOS = "SL_PARAM_ACESS"
Private Const NOME_TABELA_ID_UTILIZADORES = "SL_IDUTILIZADOR"
Private Const NOME_TABELA_GRUPOS = "SL_GR_UTIL"

'defini��o de tipos de nodos
Private Const NODO_GRUPO = "G"
Private Const NODO_UTILIZADOR = "U"
Private Const NODO_CONTENTOR = "C"
Private Const NODO_OBJECTO = "O"
Private Const NODO_PROPRIEDADE = "P"

' modos de edi��o
Private Const MODO_EDITA_PERMISSOES = "EDITA_PERMISSOES"
Private Const MODO_CRIA_PERMISSOES = "CRIA_PERMISSOES"

' constantes para pesquisa
Private Const PESQUISA_TUDO = 0
Private Const PESQUISA_UTILIZADORES = 1
Private Const PESQUISA_GRUPOS = 2
Private Const PESQUISA_CONTENTORES = 3
Private Const PESQUISA_OBJECTOS = 4

' tipos de
'variaveis do modulo
Private ModoEdicao As String
Private CodUtilAntigo As String
Private CodGrupoAntigo As String
Private ValorPropriedadeMudou As Boolean
Private LimpaPropriedades As Boolean

' Variavel para gravar o Numero de pesquisas com o crit�rio actual, para permitir encontrar
' segundos ocorrencias...
Private NumPesqCriterio As Integer
Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.
Dim CamposBD() As String
Dim valores() As Variant
Dim obTabela As ADODB.recordset

'Informa��es sobre os forms do projecto
Private Type T_OBJINFO
    nome As String
    caption As String
    indice As Integer
End Type
Private Type T_FORMINFO
    nome As String
    Objs() As T_OBJINFO
    NumObjs As Integer
End Type
Dim FormInfo() As T_FORMINFO
Dim NumForms As Integer
Dim Props() As String
Dim NumProps As Integer

Sub CarregaInfoForms()
    
    Dim nomeForm As String
    Dim NomeObj As String
    Dim NomeProp As String
    Dim FormsInf As String
    Dim pos1 As Single
    Dim pos2 As Single
    Dim pos3 As Single
    Dim pos4 As Single
    Dim pos5 As Single
    Dim PosFimForm As Single
    Dim PosFimObj As Single
    Dim PosFimProp As Single
    
    On Error GoTo Trata_Erro
    
    NumForms = 0
    ReDim FormInfo(NumForms)
    FormInfo(NumForms).NumObjs = 0
    ReDim FormInfo(NumForms).Objs(FormInfo(NumForms).NumObjs)
    
    RTBAux.LoadFile gDirServidor & "\bin\Forms.ini", rtfCFText
    FormsInf = RTBAux.Text
    
    pos1 = InStr(1, FormsInf, "INICIO_FORMS")
    PosFimForm = InStr(pos1, FormsInf, "FIM_FORMS")
    FormsInf = Mid(FormsInf, pos1 + 13)
    
    pos2 = InStr(1, FormsInf, "FORM(")
    While pos2 <> 0 And pos2 < PosFimForm
        pos3 = InStr(pos2, FormsInf, ")")
        
        If pos2 <> 0 Then
            nomeForm = Mid(FormsInf, pos2 + 5, pos3 - pos2 - 5)
            
            FormInfo(NumForms).nome = nomeForm
            
            pos2 = InStr(1, FormsInf, "INICIO_OBJS")
            PosFimObj = InStr(pos2, FormsInf, "FIM_OBJS")
            FormsInf = Mid(FormsInf, pos2 + 12)
            
            pos3 = InStr(1, FormsInf, "OBJ(")
            While pos3 <> 0 And pos3 < PosFimObj
                pos4 = InStr(pos3, FormsInf, ")")
            
                If pos3 <> 0 Then
                    NomeObj = Mid(FormsInf, pos3 + 4, pos4 - pos3 - 4)
                    
                    FormInfo(NumForms).Objs(FormInfo(NumForms).NumObjs).nome = NomeObj
                    FormInfo(NumForms).NumObjs = FormInfo(NumForms).NumObjs + 1
                    ReDim Preserve FormInfo(NumForms).Objs(FormInfo(NumForms).NumObjs)
                    
                    FormsInf = Mid(FormsInf, pos4 + 1)
                Else
                    FormsInf = Mid(FormsInf, pos3 + 1)
                End If
                
                pos3 = InStr(1, FormsInf, "OBJ(")
                PosFimObj = InStr(1, FormsInf, "FIM_OBJS")
            Wend
            
            pos2 = InStr(1, FormsInf, "FORM(")
            PosFimForm = InStr(1, FormsInf, "FIM_FORMS")
            
            NumForms = NumForms + 1
            ReDim Preserve FormInfo(NumForms)
            FormInfo(NumForms).NumObjs = 0
            ReDim FormInfo(NumForms).Objs(FormInfo(NumForms).NumObjs)
        End If
    Wend
    
    NumProps = 0
    ReDim Props(NumProps)
    pos1 = InStr(1, FormsInf, "INICIO_PROPS")
    PosFimProp = InStr(pos1, FormsInf, "FIM_PROPS")
    FormsInf = Mid(FormsInf, pos1 + 12)
    
    pos2 = InStr(1, FormsInf, "PROP(")
    While pos2 <> 0 And pos2 < PosFimProp
        pos3 = InStr(pos2, FormsInf, ")")
    
        If pos3 <> 0 Then
            NomeProp = Mid(FormsInf, pos2 + 5, pos3 - pos2 - 5)
            
            Props(NumProps) = NomeProp
            NumProps = NumProps + 1
            ReDim Preserve Props(NumProps)
            
            FormsInf = Mid(FormsInf, pos3 + 1)
        Else
            FormsInf = Mid(FormsInf, pos2 + 1)
        End If
    
        pos2 = InStr(1, FormsInf, "PROP(")
    Wend
    
Exit Sub

Trata_Erro:
    BG_LogFile_Erros "CarregaInfoForms: " & Err.Number & "-" & Err.Description

End Sub

Private Sub CriaPermissao()
    
    Dim sql As String
    Dim SQLTeste As String
    Dim NumSeqInt As Long
    Dim NodoTmp As Node
    Dim ColeccaoNodos As Nodes
    Dim ChaveNodoRetorno As String
    
    ' testar os campos, tem de existir ou um utilizador ou um grupo
    If Not (ChkUtilizador.value = 1 Or ChkGrupo.value = 1) Then
        MsgBox "Tem de Seleccionar um utilizador/grupo."
        Exit Sub
    End If
    If Not (EcCodigoGrupo <> "" Or EcCodigoUtilizador <> "") Then
        MsgBox "Tem de Seleccionar um utilizador/grupo."
        Exit Sub
    Else
        If ChkGrupo.value Then
            SQLTeste = "cod_grupo=" & EcCodigoGrupo
        Else
            SQLTeste = "cod_util=" & EcCodigoUtilizador
        End If
    End If
    If EcContentor.ListIndex = -1 Then
        MsgBox "Tem de especificar um contentor."
        Exit Sub
    Else
        SQLTeste = SQLTeste & " AND nome_form='" & Trim(FormInfo(EcContentor.ListIndex + 1).nome) & "'"
    End If
    If EcObjecto.ListIndex = -1 Then
        MsgBox "Tem de especificar o objecto a parametrizar."
        Exit Sub
    Else
        SQLTeste = SQLTeste & " AND nome_obj='" & Trim(FormInfo(EcContentor.ListIndex + 1).Objs(EcObjecto.ListIndex + 1).nome) & "'"
        SQLTeste = SQLTeste & " AND indice='" & Trim(FormInfo(EcContentor.ListIndex + 1).Objs(EcObjecto.ListIndex + 1).indice) & "'"
    End If
    If Trim(EcValorProp) = "" Then
        MsgBox "O valor parametrizado � inv�lido."
        Exit Sub
    Else
        SQLTeste = SQLTeste & " AND val_prop='" & Trim(EcValorProp) & "'"
    End If
    If Trim(EcPropriedade) = "" Then
        If Trim(UCase(EcContentor.Text)) <> "PERMISSOES_ESPECIAIS" Then
            MsgBox "Tem de especificar a propriedade a parametrizar."
            Exit Sub
        End If
    Else
        SQLTeste = SQLTeste & " AND descr_prop='" & EcPropriedade & "'"
    End If
    
    sql = "SELECT * FROM " & NOME_TABELA_PARAM_ACESSOS & _
        " WHERE " & SQLTeste
        
    Set obTabela = New ADODB.recordset
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    obTabela.CursorType = adOpenStatic
    obTabela.CursorLocation = adUseServer
    
    obTabela.Open sql, gConexao
    
    If obTabela.RecordCount > 0 Then ' caso a parametriza��o j� exista...
        MsgBox "J� existe um registo com essa parametriza��o, seq_param_acess:" & obTabela!seq_param_acess
        obTabela.Close
        Set obTabela = Nothing
        Exit Sub
    End If
    
    NumSeqInt = BG_DaMAX(NOME_TABELA_PARAM_ACESSOS, "seq_param_acess") + 1
        
    ' construir o SQL
    ' valores
    valores(1) = NumSeqInt
    valores(2) = EcCodigoGrupo
    valores(3) = EcCodigoUtilizador
    valores(4) = Trim(FormInfo(EcContentor.ListIndex + 1).nome)
    valores(5) = Trim(FormInfo(EcContentor.ListIndex + 1).Objs(EcObjecto.ListIndex + 1).nome)
    valores(6) = EcPropriedade
    valores(7) = EcValorProp
    valores(8) = gCodUtilizador
    valores(9) = Bg_DaData_ADO
    valores(10) = UCase(mediValorNulo)
    valores(11) = UCase(mediValorNulo)
    valores(12) = Trim(FormInfo(EcContentor.ListIndex + 1).Objs(EcObjecto.ListIndex + 1).indice)

    sql = BL_Constroi_INSERT_Simples(NOME_TABELA_PARAM_ACESSOS, CamposBD, valores)
    BG_ExecutaQuery_ADO sql
    
    ' definir o nodo de retorno para que a arvore depois de actualizada
    ' apare�a aberta no mesmo nodo
    If Not (TwObjectosTodos.SelectedItem Is Nothing) Then
        ChaveNodoRetorno = TwObjectosTodos.SelectedItem.Key
    End If
    
    ' Actualizar a �rvore para reflectir os registos inseridos
    PreencheTwObjectosTodos
    ' voltar ao nodo que estava previamente seleccionado...
    SeleccionaNodo ChaveNodoRetorno, TwObjectosTodos

End Sub

Private Sub CopiaPermissoesOLEDragDrop(NodoDestino As Node)

    ' esta sub faz a copia de permissoes entre nodos atrav�s de drag-and-drop
    Dim TotalNodos As Long
    Dim Nodo As ListItem
    Dim NodoTmp As ListItem
    Dim ListaOrigem As ListItems
    Dim PosicaoOrdenada As Long
    Dim TipoNodoDest As String
    Dim sql As String
    Dim NumSeqInt As Long
    Dim ChaveNodoDestino As String
    Dim PosicoesAIgnorar() As Integer
    
    If NodoDestino Is Nothing Then
        Exit Sub
    End If
    ChaveNodoDestino = NodoDestino.Key
    TipoNodoDest = left(ChaveNodoDestino, 1)
    
    ' n�o se pode arrastar permissoes para um utilizador ou um grupo, � muito vago,
    ' tem de se for�ar a seleccionar um objecto que suporte directamente as
    ' permiss�es arrastadas
    If TipoNodoDest = NODO_UTILIZADOR Or TipoNodoDest = NODO_GRUPO Then
        Exit Sub
    End If
    
    NumSeqInt = BG_DaMAX(NOME_TABELA_PARAM_ACESSOS, "seq_param_acess") + 1
    
    Set ListaOrigem = LwPropriedades.ListItems
    ' buscar o novo numero de sequencia
    sql = "SELECT * FROM " & NOME_TABELA_PARAM_ACESSOS & _
            " WHERE seq_param_acess= " & Mid(NodoDestino.Key, 2)
            
    Set obTabela = New ADODB.recordset
    obTabela.CursorType = adOpenStatic
    obTabela.CursorLocation = adUseServer
    
    obTabela.Open sql, gConexao
    
    ' configura��o dos valores standart para inser��o
    ReDim PosicoesAIgnorar(1 To 6)
    PosicoesAIgnorar(1) = 1
    PosicoesAIgnorar(2) = 7
    PosicoesAIgnorar(3) = 8
    PosicoesAIgnorar(4) = 9
    PosicoesAIgnorar(5) = 10
    PosicoesAIgnorar(6) = 11
    valores(4) = BL_HandleNull(obTabela!nome_form, "")
    valores(5) = BL_HandleNull(obTabela!nome_obj, "")
    valores(8) = gCodUtilizador
    valores(9) = Bg_DaData_ADO
    valores(10) = UCase(mediValorNulo)
    valores(11) = UCase(mediValorNulo)
    
    If BL_HandleNull(obTabela!cod_grupo, 0) = 0 Then ' � uma permiss�o de utilizador
        valores(2) = UCase(mediValorNulo)
        valores(3) = obTabela!cod_util
    Else
        valores(2) = obTabela!cod_grupo
        valores(3) = UCase(mediValorNulo)
    End If
    
    obTabela.Close
    
    ' construir o SQL
    ' valores
    For Each NodoTmp In ListaOrigem
        valores(1) = NumSeqInt
        valores(6) = NodoTmp.Text
        valores(7) = NodoTmp.SubItems(1)
        
        sql = BL_Constroi_SELECT_Simples(NOME_TABELA_PARAM_ACESSOS, CamposBD, valores, PosicoesAIgnorar)
        
        obTabela.CursorType = adOpenStatic
        obTabela.CursorLocation = adUseServer
    
        obTabela.Open sql, gConexao
        
        If obTabela.RecordCount > 0 Then
            MsgBox " A Propriedade " & valores(6) & " j� existe no destino."
        Else
            sql = BL_Constroi_INSERT_Simples(NOME_TABELA_PARAM_ACESSOS, CamposBD, valores)
            BG_ExecutaQuery_ADO sql
            NumSeqInt = NumSeqInt + 1
        End If
        obTabela.Close
    Next NodoTmp
    
    Set obTabela = Nothing
    
    'actualizar a arvore
    PreencheTwObjectosTodos
    ' seleccionar o nodo de destino
    SeleccionaNodo ChaveNodoDestino, TwObjectosTodos

End Sub

Sub ProcuraContentor(Contentor As String)
    
    Dim i As Integer
    Dim encontrou As Boolean
    
    For i = 0 To EcContentor.ListCount - 1
        If UCase(Trim(EcContentor.List(i))) = Trim(UCase(Contentor)) Then
            EcContentor.ListIndex = i
            encontrou = True
            Exit For
        End If
    Next i

    If Not encontrou Then
        EcContentor.AddItem Trim(UCase(Contentor))
        EcContentor.ListIndex = EcContentor.ListCount - 1
    End If

End Sub

Sub ProcuraObjecto(Objecto As String)
    
    Dim i As Integer
    Dim encontrou As Boolean
    
    For i = 0 To EcObjecto.ListCount - 1
        If UCase(Trim(EcObjecto.List(i))) = Trim(UCase(Objecto)) Then
            encontrou = True
            EcObjecto.ListIndex = i
            Exit For
        End If
    Next i

    If Not encontrou Then
        EcObjecto.AddItem Trim(UCase(Objecto))
        EcObjecto.ListIndex = EcObjecto.ListCount - 1
    End If

End Sub

Private Sub BtPesquisa_Click()
    
    Dim TpNodo As String
    Dim encontrou As Boolean
    
    NumPesqCriterio = NumPesqCriterio + 1
    Select Case EcTipoObjectosPesq.ListIndex
        Case PESQUISA_TUDO
            TpNodo = "*"
        Case PESQUISA_UTILIZADORES
            TpNodo = NODO_UTILIZADOR
        Case PESQUISA_GRUPOS
            TpNodo = NODO_GRUPO
        Case PESQUISA_CONTENTORES
            TpNodo = NODO_CONTENTOR
        Case PESQUISA_OBJECTOS
            TpNodo = NODO_OBJECTO
    End Select
    encontrou = PesquisaTwArvore(TwObjectosTodos, EcChavePesquisa, TpNodo, NumPesqCriterio)
    If Not encontrou Then
        If NumPesqCriterio <= 1 Then
            MsgBox "N�o foram encontrados dados para este crit�rio de pesquisa."
        ElseIf NumPesqCriterio > 1 Then
            MsgBox "N�o foram encontrados mais ocorr�ncias para este crit�rio de pesquisa."
            NumPesqCriterio = 0
        End If
    End If

End Sub

Function PesquisaTwArvore(Tw As TreeView, ChavePesquisa As String, TpNodo As String, _
                          UltimaOcorrencia As Integer) As Boolean
    
    Dim ColNodos As Nodes
    Dim Nodo As Node
    Dim NumeroOcorrencia As Integer
    Dim EncontrouNodo As Boolean
    
    Set ColNodos = Tw.Nodes
    If ColNodos Is Nothing Then
        Exit Function
    End If
    ' por defeito n�o se encontrou nada
    PesquisaTwArvore = False
    For Each Nodo In ColNodos
        If TpNodo <> "*" Then
            If TipoNodo(Nodo) = TpNodo And _
                InStr(UCase(Nodo.Text), UCase(ChavePesquisa)) > 0 Then
                EncontrouNodo = True
            End If
        Else
            If InStr(UCase(Nodo.Text), UCase(ChavePesquisa)) > 0 Then
                EncontrouNodo = True
            End If
        End If
        If EncontrouNodo Then
            NumeroOcorrencia = NumeroOcorrencia + 1
            If NumeroOcorrencia >= UltimaOcorrencia Then
                Set Tw.SelectedItem = Nodo
                Exit For
            End If
        End If
        EncontrouNodo = False
    
    Next Nodo
    PesquisaTwArvore = EncontrouNodo

End Function

Private Sub ChkGrupo_Click()
    
    EcCodigoUtilizador = ""
    EcUtilizadorCombo = ""
    
    If ChkGrupo.value Then
        ChkUtilizador.value = False
        EcGrupoCombo.Visible = True
        EcGrupoCombo.Enabled = True
        EcUtilizadorCombo.Visible = False
    Else
        EcCodigoGrupo = ""
        EcGrupoCombo = ""
        EcGrupoCombo.Enabled = False
        EcUtilizadorCombo.Enabled = False
    End If

End Sub

Private Sub ChkUtilizador_Click()
    
    EcCodigoGrupo = ""
    EcGrupoCombo = ""
    
    If ChkUtilizador.value Then
        ChkGrupo.value = False
        EcGrupoCombo.Visible = False
        EcUtilizadorCombo.Visible = True
        EcUtilizadorCombo.Enabled = True
    Else
        EcCodigoUtilizador = ""
        EcUtilizadorCombo = ""
        EcGrupoCombo.Enabled = False
        EcUtilizadorCombo.Enabled = False
    End If

End Sub

Private Sub EcChavePesquisa_Change()
    
    NumPesqCriterio = 0

End Sub

Sub PesquisaRapidaGruposUtil(TipoObjecto As String, ByRef campoCodigo, _
                             ByRef campoDescricao)
    
    If TipoObjecto = Grupo Then
        Call FormPesquisaRapida.InitPesquisaRapida(NOME_TABELA_GRUPOS, "descr_gr_util", "cod_gr_util", campoCodigo, campoDescricao)
    Else
        Call FormPesquisaRapida.InitPesquisaRapida(NOME_TABELA_ID_UTILIZADORES, "nome", "cod_utilizador", campoCodigo, campoDescricao, _
         " flg_removido=0 ")
    End If
        
End Sub

Sub PrencheListaImagens()
    
    Dim lista As ListImages
    
    Set lista = ListaImagens.ListImages
    ' indices 1,2
    BL_CarregaImagemNaLista lista, "GrupoUtil16.ico"
    BL_CarregaImagemNaLista lista, "GrupoUtilSelec16.ico"
    ' indices 3,4
    BL_CarregaImagemNaLista lista, "Util16.ico"
    BL_CarregaImagemNaLista lista, "UtilSelec16.ico"
    ' indices 5,6
    BL_CarregaImagemNaLista lista, "Contentor.ico"
    BL_CarregaImagemNaLista lista, "ContentorSelec.ico"
    ' indices 7,8
    BL_CarregaImagemNaLista lista, "Objecto.ico"
    BL_CarregaImagemNaLista lista, "ObjectoSelec.ico"
    ' indices 9,10
    BL_CarregaImagemNaLista lista, "Permissao.ico"
    BL_CarregaImagemNaLista lista, "PermissaoSelec.ico"
    
End Sub

Private Function TipoNodo(Nodo As Node) As String
    
    If Not (Nodo Is Nothing) Then
        TipoNodo = left(Nodo.Key, 1)
    End If

End Function

Private Sub PreencheLwPropriedades(Nodo As Node)
    
    ' Esta fun��o vai preencher a Listview com as propriedades
    ' parametrizadas em NOME_TABELA_PARAM_ACESSOS para um determinado
    ' objecto.
    
    Dim sql As String
    Dim ListaProp As ListItems
    ' Nodo Principal, ie grupo ou utilizador
    Dim CodigoGrupoUtil As Long
    Dim TipoNodoPrincipal As String, ChaveNodoPrincipal As String
    ' Nodo Pai, ie contentor
    Dim ChaveNodoPai As String
    Dim NumeroSequenciaPai As Long
    ' proprio objecto
    Dim ChaveObjecto As String
    Dim NumeroSequenciaObjecto As Long
    ' Nodos a acrescentar na Listview
    Dim TextoNodoTmp As String, ChaveNodoTmp As String
    Dim ItemLista As ListItem
    
    If Nodo Is Nothing Then
        Exit Sub
    End If
    
    ChaveObjecto = Nodo.Key
    NumeroSequenciaObjecto = Mid(ChaveObjecto, 2)
    
    ' nodo principal
    ChaveNodoPrincipal = Nodo.parent.parent.Key
    TipoNodoPrincipal = left(ChaveNodoPrincipal, 1)
    CodigoGrupoUtil = CLng(Mid(ChaveNodoPrincipal, 2))
    
    ' nodo pai
    ChaveNodoPai = Nodo.parent.Key
    NumeroSequenciaPai = Mid(ChaveNodoPai, 2)
    
    'statement SQL
    sql = "SELECT seq_param_acess,descr_prop,val_prop, indice FROM " & NOME_TABELA_PARAM_ACESSOS & _
            " WHERE nome_form=(SELECT nome_form FROM " & NOME_TABELA_PARAM_ACESSOS & _
                                " WHERE seq_param_acess=" & NumeroSequenciaPai & ")" & _
            " AND nome_obj=(SELECT nome_obj FROM " & NOME_TABELA_PARAM_ACESSOS & _
                            " WHERE seq_param_acess=" & NumeroSequenciaObjecto & ")"
            
    Select Case TipoNodoPrincipal
        Case NODO_UTILIZADOR
            sql = sql & " AND cod_util=" & CodigoGrupoUtil
        Case NODO_GRUPO
            sql = sql & " AND cod_grupo=" & CodigoGrupoUtil
    End Select
    
    Set obTabela = New ADODB.recordset
    
    obTabela.CursorType = adOpenStatic
    obTabela.CursorLocation = adUseServer
    
    obTabela.Open sql, gConexao
    
    Set ListaProp = LwPropriedades.ListItems
    ListaProp.Clear
    If obTabela.RecordCount <= 0 Then ' se n�o houver registos
        obTabela.Close
        Set obTabela = Nothing
        Exit Sub
    End If
    
    ' atribuir o objecto listitems da LwPropriedades para acelerar o processo de inser��o
    Do While Not obTabela.EOF
        TextoNodoTmp = BL_HandleNull(obTabela!descr_prop, " Sem Nome ")
        ChaveNodoTmp = NODO_PROPRIEDADE & obTabela!seq_param_acess
        Set ItemLista = ListaProp.Add(, ChaveNodoTmp, TextoNodoTmp, 9, 9)
        ItemLista.SubItems(1) = BL_HandleNull(obTabela!val_prop, "")
        obTabela.MoveNext
    Loop
    obTabela.Close
    Set obTabela = Nothing
    
End Sub

Private Sub PreencheTwObjectosTodos()
    
    ' Esta fun��o vai preencher a treeview com os objectos todos.
    Dim sql As String
    
    ' variaveis para controlo de mudan�a de nodo
    Dim GrupoActual As Long, GrupoOld As Long
    Dim UtilizadorActual As Long, UtilizadorOld As Long
    Dim ContentorActual As String, ContentorOld As String ' geralmente s�o forms
    Dim ObjectoActual As String, ObjectoOld As String ' objectos parametrizados
    Dim TextoNodo As String
    Dim ChaveNodo As String
    Dim ChaveNodoPai As String
    Dim ChaveContentorActual As String
    Dim CriouNodoNovo As Boolean
    
    ' limpar a treeview
    TwObjectosTodos.Nodes.Clear
    CriouNodoNovo = False
    
    sql = "SELECT * FROM slv_codif_pacess ORDER BY " & _
            "cod_grupo, cod_util, nome_form, nome_obj"
    
    Set obTabela = New ADODB.recordset
        
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    obTabela.CursorType = adOpenStatic
    obTabela.CursorLocation = adUseServer
    
    obTabela.Open sql, gConexao
        
    If obTabela.RecordCount <= 0 Then
        MsgBox "N�o existem permiss�es parametrizadas."
        Exit Sub
    End If
    
    Do While Not obTabela.EOF
        'cria��o de grupos
        GrupoActual = BL_HandleNull(obTabela!cod_grupo, 0)
        CriouNodoNovo = False
        If GrupoActual <> 0 And GrupoActual <> GrupoOld Then
            GrupoOld = GrupoActual
            TextoNodo = BL_HandleNull(obTabela!descr_gr_util, " Descri��o Inexistente ")
            ChaveNodo = NODO_GRUPO & obTabela!cod_grupo
            BL_AcrescentaNodoPai TwObjectosTodos, TextoNodo, ChaveNodo, 1, 2
            CriouNodoNovo = True
        End If
        
        'cria��o dos utilizadores
        UtilizadorActual = BL_HandleNull(obTabela!cod_util, 0)
        If UtilizadorActual <> UtilizadorOld And UtilizadorActual <> 0 Then
            UtilizadorOld = UtilizadorActual
            TextoNodo = BL_HandleNull(obTabela!utilizador, " Nome Inexistente ")
            ChaveNodo = NODO_UTILIZADOR & obTabela!cod_util
            BL_AcrescentaNodoPai TwObjectosTodos, TextoNodo, ChaveNodo, 3, 4
            CriouNodoNovo = True
        End If
        
        'cria��o dos contentores
        ContentorActual = BL_HandleNull(obTabela!nome_form, "")
        If (ContentorActual <> ContentorOld And ContentorActual <> "") _
            Or CriouNodoNovo Then
            ContentorOld = ContentorActual
            TextoNodo = ContentorActual
            ChaveNodo = NODO_CONTENTOR & obTabela!seq_param_acess
            ChaveContentorActual = ChaveNodo
            If BL_HandleNull(obTabela!cod_util, 0) <> 0 Then
                ChaveNodoPai = NODO_UTILIZADOR & obTabela!cod_util
            Else
                ChaveNodoPai = NODO_GRUPO & obTabela!cod_grupo
            End If
            BL_AcrescentaNodoFilho TwObjectosTodos, TextoNodo, ChaveNodoPai, ChaveNodo, 5, 6
            CriouNodoNovo = True
        End If
        
        'cria��o dos objectos
        ObjectoActual = BL_HandleNull(obTabela!nome_obj, "")
        If (ObjectoActual <> ObjectoOld And ObjectoActual <> "") Or CriouNodoNovo Then
            ObjectoOld = ObjectoActual
            TextoNodo = BL_HandleNull(obTabela!indice, -1) & "-" & BL_HandleNull(obTabela!descricao, obTabela!nome_obj)
            ChaveNodo = NODO_OBJECTO & obTabela!seq_param_acess
            ChaveNodoPai = ChaveContentorActual
            BL_AcrescentaNodoFilho TwObjectosTodos, TextoNodo, ChaveNodoPai, ChaveNodo, 7, 8
        End If
        obTabela.MoveNext
    Loop
    obTabela.Close
    Set obTabela = Nothing

End Sub

Private Function ProcuraGruposUtil(TipoCodigo As String, CodigoPesquisa As String, ByRef campoCodigo, _
                                   ByRef campoDescricao) As Boolean
    Dim sql As String
    Dim tipo As String
    
    ProcuraGruposUtil = True
    
    Select Case TipoCodigo
        Case Grupo
            tipo = " grupo "
            sql = "SELECT descr_gr_util  as descricao FROM " & NOME_TABELA_GRUPOS & _
                " WHERE cod_gr_util=" & CodigoPesquisa
        Case utilizador
            tipo = " utilizador "
            sql = "SELECT utilizador as descricao FROM " & NOME_TABELA_ID_UTILIZADORES & _
                " WHERE cod_utilizador=" & CodigoPesquisa
    End Select
    
    Set obTabela = New ADODB.recordset
    
    obTabela.CursorType = adOpenStatic
    obTabela.CursorLocation = adUseServer
    
    obTabela.Open sql, gConexao
    
    If obTabela.RecordCount > 0 Then
        campoCodigo.Text = CodigoPesquisa
        campoDescricao.Text = BL_HandleNull(obTabela!descricao, "")
    Else
        MsgBox tipo & " n�o existe."
        campoCodigo.SetFocus
        ProcuraGruposUtil = False
    End If
    obTabela.Close
    Set obTabela = Nothing

End Function

Private Sub EcCodigoUtilizador_Change()
    
    BL_ColocaTextoCombo "sl_idutilizador", "cod_utilizador", "cod_utilizador", EcCodigoUtilizador, EcUtilizadorCombo

End Sub

Private Sub EcCodigoGrupo_Change()
    
    BL_ColocaTextoCombo "sl_gr_util", "cod_gr_util", "cod_gr_util", EcCodigoGrupo, EcGrupoCombo

End Sub

Private Sub EcContentor_Click()
    
    Dim i As Integer
    
    EcObjecto.Clear
    If EcContentor.ListIndex <> -1 Then
        If EcContentor.ListIndex < NumForms Then
            For i = 1 To FormInfo(EcContentor.ListIndex + 1).NumObjs
                'EcObjecto.AddItem Trim(FormInfo(EcContentor.ListIndex + 1).Objs(i).indice & "-" & BL_HandleNull(FormInfo(EcContentor.ListIndex + 1).Objs(i).caption, FormInfo(EcContentor.ListIndex + 1).Objs(i).nome))
                EcObjecto.AddItem Trim(FormInfo(EcContentor.ListIndex + 1).Objs(i).indice & "-" & BL_HandleNull(FormInfo(EcContentor.ListIndex + 1).Objs(i).caption, FormInfo(EcContentor.ListIndex + 1).Objs(i).nome))
            Next i
        End If
    End If

End Sub

Private Sub EcUtilizadorCombo_Click()
    
    BL_ColocaComboTexto "sl_idutilizador", "cod_utilizador", "cod_utilizador", EcCodigoUtilizador, EcUtilizadorCombo

End Sub

Private Sub EcGrupoCombo_Click()
    
    BL_ColocaComboTexto "sl_gr_util", "cod_gr_util", "cod_gr_util", EcCodigoGrupo, EcGrupoCombo

End Sub

Private Sub EcContentor_DropDown()
    
    EcModoEdicao = MODO_CRIA_PERMISSOES

End Sub

Private Sub EcContentor_LostFocus()
    
    EcModoEdicao = MODO_CRIA_PERMISSOES
    LwPropriedades.ListItems.Clear
    
End Sub

Private Sub EcModoEdicao_Change()
    
    ActivaCampos (EcModoEdicao)

End Sub

Private Sub EcObjecto_Change()
    
    LwPropriedades.ListItems.Clear

End Sub

Private Sub EcObjecto_DropDown()
    
    EcModoEdicao = MODO_CRIA_PERMISSOES

End Sub

Private Sub EcObjecto_LostFocus()
    
    EcModoEdicao = MODO_CRIA_PERMISSOES

End Sub

Private Sub EcPropriedade_Change()
    
    If LimpaPropriedades Then
        LwPropriedades.ListItems.Clear
    End If

End Sub

Private Sub EcPropriedade_DropDown()
    
    EcModoEdicao = MODO_CRIA_PERMISSOES

End Sub

Private Sub EcPropriedade_LostFocus()
    
    EcModoEdicao = MODO_CRIA_PERMISSOES

End Sub

Private Sub EcValorProp_Change()
    
    ValorPropriedadeMudou = True

End Sub

Private Sub EcValorProp_LostFocus()
    
    If ValorPropriedadeMudou Then
        EcValorProp = Trim(UCase(EcValorProp))
        ValorPropriedadeMudou = False
        'GravaPermissao
    End If

End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = "Parametriza��o de Acessos"
    Me.left = 540
    Me.top = 135
    Me.Width = 8985
    Me.Height = 6700
    
    ' Ambos Est�o sobrepostos
    EcGrupoCombo.Enabled = False
    EcUtilizadorCombo.Enabled = False
    
    Call PrencheListaImagens
    Call InicializaTreeViews
    Call InicializaListViews
    Call PreencheTiposObjectosPesquisa
    Call InicializaVectoresInsercao

End Sub

Sub InicializaVectoresInsercao()
    
    ReDim CamposBD(1 To 12)
    ReDim valores(1 To 12)
    
    CamposBD(1) = "seq_param_acess"
    CamposBD(2) = "cod_grupo"
    CamposBD(3) = "cod_util"
    CamposBD(4) = "nome_form"
    CamposBD(5) = "nome_obj"
    CamposBD(6) = "descr_prop"
    CamposBD(7) = "val_prop"
    CamposBD(8) = "user_cri"
    CamposBD(9) = "dt_cri"
    CamposBD(10) = "user_act"
    CamposBD(11) = "dt_act"
    CamposBD(12) = "indice"

End Sub

Sub PreencheTiposObjectosPesquisa()
    
    EcTipoObjectosPesq.AddItem "Toda a �rvore", PESQUISA_TUDO
    EcTipoObjectosPesq.AddItem "Utilizadores", PESQUISA_UTILIZADORES
    EcTipoObjectosPesq.AddItem "Grupos", PESQUISA_GRUPOS
    EcTipoObjectosPesq.AddItem "Contentores/Forms", PESQUISA_CONTENTORES
    EcTipoObjectosPesq.AddItem "Objectos", PESQUISA_OBJECTOS
    EcTipoObjectosPesq.ListIndex = 0

End Sub

Sub InicializaTreeViews()
    
    Set TwObjectosTodos.ImageList = ListaImagens
    TwObjectosTodos.Indentation = 0
    TwObjectosTodos.OLEDropMode = ccOLEDropManual

End Sub
Sub InicializaListViews()
    
    ' falta atribuir a lista de imagens
    LwPropriedades.View = lvwReport
    Set LwPropriedades.Icons = ListaImagens
    Set LwPropriedades.SmallIcons = ListaImagens
    LwPropriedades.ColumnHeaders.Add , NODO_PROPRIEDADE & "1", "Propriedade"
    LwPropriedades.ColumnHeaders.Add , NODO_PROPRIEDADE & "2", "Valor"
    LwPropriedades.OLEDragMode = ccOLEDragAutomatic

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    
    Call Inicializacoes
    Call PreencheValoresDefeito
    
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    estado = 1
    BL_ToolbarEstadoN estado
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    Set FormParametrizacaoAcessos = Nothing
    BL_ToolbarEstadoN 0

End Sub

Sub LimpaCampos()
    
    ChkGrupo.value = False
    ChkUtilizador.value = False
    EcContentor.ListIndex = -1
    EcObjecto.Clear
    EcValorProp = ""
    EcPropriedade = ""
    EcCodigoGrupo = ""
    EcGrupoCombo = ""
    EcCodigoUtilizador = ""
    EcUtilizadorCombo = ""
    EcChavePesquisa = ""
    EcModoEdicao = MODO_CRIA_PERMISSOES

End Sub

Sub GravaPermissao()
    
    Dim sql As String
    Dim i As Integer
    Dim PosicaoSelec As Integer
    Dim NumeroElementosSeleccionados As Integer
    Dim NumeroSequencia As Long
    Dim NumeroItems As Integer
    Dim lista As ListItems
    If LwPropriedades.ListItems Is Nothing Then
        Exit Sub
    End If
    Set lista = LwPropriedades.ListItems
    NumeroItems = lista.Count
    For i = 1 To NumeroItems
        If lista.item(i).Selected Then
            PosicaoSelec = i
            NumeroElementosSeleccionados = NumeroElementosSeleccionados + 1
        End If
    Next i
    If NumeroElementosSeleccionados = 1 Then
        gMsgTitulo = "Actualizar parametriza��o"
        gMsgMsg = "Tem a certeza que deseja actualizar a parametriza��o?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp <> vbYes Then
            Exit Sub
        End If
        NumeroSequencia = Mid(lista.item(PosicaoSelec).Key, 2)
        sql = "UPDATE " & NOME_TABELA_PARAM_ACESSOS & _
                " SET val_prop='" & Trim(EcValorProp) & "'," & _
                " user_act='" & gCodUtilizador & "',dt_act='" & Bg_DaData_ADO & "' " & _
                " WHERE seq_param_acess=" & NumeroSequencia
        BG_ExecutaQuery_ADO sql
        PreencheLwPropriedades TwObjectosTodos.SelectedItem
        For i = 1 To NumeroItems
            If lista.item(i).Key = NODO_PROPRIEDADE & NumeroSequencia Then
                lista.item(i).Selected = True
            End If
        Next i
    End If
        
End Sub

Sub PreencheValoresDefeito()
    
    Dim sql As String
    LimpaCampos
    
    BG_PreencheComboBD_ADO "sl_idutilizador", "cod_utilizador", "utilizador", EcUtilizadorCombo, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "sl_gr_util", "cod_gr_util", "descr_gr_util", EcGrupoCombo, mediAscComboDesignacao
    
    PreencheCombosPermissoes
    PreencheTwObjectosTodos

End Sub

Private Sub PreencheCombosPermissoes()
    
    Dim i As Integer
    Dim sql As String
    
    'CarregaInfoForms
    CarregaInfoForms2
    
    For i = 1 To NumForms
        EcContentor.AddItem Trim(FormInfo(i).nome)
        
    Next i
    
    EcObjecto.Clear
    
    For i = 0 To NumProps - 1
        EcPropriedade.AddItem Trim(Props(i))
    Next i
    
    
    If NumForms = 0 Then
        sql = "SELECT DISTINCT nome_form FROM " & NOME_TABELA_PARAM_ACESSOS
        BL_PreencheCombo sql, EcContentor, "nome_form"
    
        sql = "SELECT DISTINCT nome_obj FROM " & NOME_TABELA_PARAM_ACESSOS
        BL_PreencheCombo sql, EcObjecto, "nome_obj"

        sql = "SELECT DISTINCT descr_prop FROM " & NOME_TABELA_PARAM_ACESSOS & _
            " WHERE descr_prop IS NOT NULL"
        BL_PreencheCombo sql, EcPropriedade, "descr_prop"
    End If
    
End Sub

Sub PreencheCampos()
    
    Me.SetFocus
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
    End If

End Sub

Public Sub ApagaPermissao(ColNodos As ListItems)
    
    Dim sql As String
    Dim ChaveNodoRetorno As String
    Dim ChaveNodoRetorno2 As String ' chave para o pai do nosso nodo
    Dim NumSeqInt As Long
    Dim lItem As ListItem
    
    If ColNodos Is Nothing Then
        Exit Sub
    End If
    gMsgTitulo = "Apagar"
    gMsgMsg = "Tem a certeza que deseja apagar estas parametrizacoes?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If gMsgResp <> vbYes Then
        Exit Sub
    End If
    For Each lItem In ColNodos
        If lItem.Selected Then
            NumSeqInt = Mid(lItem.Key, 2)
            sql = "DELETE FROM " & NOME_TABELA_PARAM_ACESSOS & _
                    " WHERE seq_param_acess=" & NumSeqInt
            BG_ExecutaQuery_ADO sql
        End If
    Next lItem
    If Not (TwObjectosTodos.SelectedItem Is Nothing) Then
        ChaveNodoRetorno = TwObjectosTodos.SelectedItem.Key
        ChaveNodoRetorno2 = TwObjectosTodos.SelectedItem.parent.Key
    Else
        ChaveNodoRetorno = ""
    End If
    ColNodos.Clear
    PreencheTwObjectosTodos
    If ChaveNodoRetorno <> "" Then
        If Not SeleccionaNodo(ChaveNodoRetorno, TwObjectosTodos) Then
            ' se o nosso nodo j� n�o existe ent�o tentar seleccionar o pai, se este n�o existir
            ' ent�o a arvore fica comprimida...
            SeleccionaNodo ChaveNodoRetorno2, TwObjectosTodos
        Else
            If left(TwObjectosTodos.SelectedItem.Key, 1) = NODO_OBJECTO Then
                PreencheLwPropriedades TwObjectosTodos.SelectedItem
            End If
        End If
    End If

End Sub

Public Sub ApagaPermissoesObjectos(Nodo As Node)
    
    Dim TipoNodo As String
    Dim sql As String
    Dim ChaveNodo As String
    Dim ChavePai As String
    Dim CodigoNodo As String
    Dim CodigoPai As String
    Dim NodoTmp As Node
    Dim ColeccaoNodos As Nodes
    Dim ChaveNodoRetorno As String
    Dim rs As ADODB.recordset
    Dim sNomeForm As String
    Dim sNomeObj As String
    
    gMsgTitulo = "Apagar"
    gMsgMsg = "Tem a certeza que deseja apagar estas parametrizacoes?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If gMsgResp <> vbYes Then
        Exit Sub
    End If
    
    ChaveNodo = Nodo.Key
    CodigoNodo = Mid(ChaveNodo, 2)
    TipoNodo = left(ChaveNodo, 1)
    ChaveNodoRetorno = ""
    
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    
    If TipoNodo = NODO_CONTENTOR Then
        sql = "SELECT nome_form FROM " & NOME_TABELA_PARAM_ACESSOS & _
                " WHERE seq_param_acess=" & CodigoNodo
        rs.Open sql, gConexao
        
        If rs.RecordCount > 0 Then
            sNomeForm = Trim(rs!nome_form)
        Else
            sNomeForm = "erro_a_selecionar_nome_form"
        End If
        
        rs.Close
        Set rs = Nothing
        
    ElseIf TipoNodo = NODO_OBJECTO Then
        sql = "SELECT nome_form FROM " & NOME_TABELA_PARAM_ACESSOS & _
                " WHERE seq_param_acess=" & CodigoNodo
        rs.Open sql, gConexao
        
        If rs.RecordCount > 0 Then
            sNomeForm = Trim(rs!nome_form)
        Else
            sNomeForm = "erro_a_selecionar_nome_form"
        End If
        
        rs.Close
        rs.CursorLocation = adUseServer
        rs.CursorType = adOpenStatic
        
        sql = "SELECT nome_obj FROM " & NOME_TABELA_PARAM_ACESSOS & _
            " WHERE seq_param_acess =" & CodigoNodo
        rs.Open sql, gConexao
        
        If rs.RecordCount > 0 Then
            sNomeObj = Trim(rs!nome_obj)
        Else
            sNomeObj = "erro_a_selecionar_nome_obj"
        End If
        
        rs.Close
        Set rs = Nothing
        
    End If
    
    Select Case TipoNodo
        Case NODO_UTILIZADOR
             sql = "DELETE FROM " & NOME_TABELA_PARAM_ACESSOS & _
                    " WHERE cod_util=" & CodigoNodo
        Case NODO_GRUPO
             sql = "DELETE FROM " & NOME_TABELA_PARAM_ACESSOS & _
                    " WHERE cod_grupo=" & CodigoNodo & _
                    "AND cod_util IN (NULL,0)"
        Case NODO_CONTENTOR
            ChavePai = Nodo.parent.Key
            ChaveNodoRetorno = ChavePai
            CodigoPai = Mid(ChavePai, 2)
            sql = "DELETE FROM " & NOME_TABELA_PARAM_ACESSOS & _
                    " WHERE nome_form = '" & sNomeForm & "'"
            If left(ChavePai, 1) = NODO_GRUPO Then
                    sql = sql & " AND cod_grupo=" & CodigoPai
            ElseIf left(ChavePai, 1) = NODO_UTILIZADOR Then
                    sql = sql & " AND cod_util=" & CodigoPai
            End If
        Case NODO_OBJECTO
            ChaveNodoRetorno = Nodo.parent.Key
            ChavePai = Nodo.parent.parent.Key
            CodigoPai = Mid(ChavePai, 2)
            sql = "DELETE FROM " & NOME_TABELA_PARAM_ACESSOS & _
                    " WHERE nome_form = '" & sNomeForm & "'" & _
                    " AND nome_obj = '" & sNomeObj & "'"
            If left(ChavePai, 1) = NODO_GRUPO Then
                    sql = sql & " AND cod_grupo=" & CodigoPai
            ElseIf left(ChavePai, 1) = NODO_UTILIZADOR Then
                    sql = sql & " AND cod_util=" & CodigoPai
            End If
        
    End Select
    BG_ExecutaQuery_ADO sql
    ' fazer refresh � treeview
    PreencheTwObjectosTodos
    'seleccionar o pai do objecto apagado
    SeleccionaNodo ChaveNodoRetorno, TwObjectosTodos

End Sub

Private Function SeleccionaNodo(ChaveNodo As String, Tw As TreeView) As Boolean
    
    Dim ColeccaoNodos As Nodes
    Dim Nodo As Node
    
    Set ColeccaoNodos = Tw.Nodes
    SeleccionaNodo = False
    If ChaveNodo <> "" Then
        For Each Nodo In ColeccaoNodos
            If Nodo.Key = ChaveNodo Then
                Nodo.Expanded = True
                Set Tw.SelectedItem = Nodo
                SeleccionaNodo = True
                Exit Function
            End If
        Next Nodo
    End If

End Function

Private Sub ActualizaCabecalhoPermissoes(lista As ListItems)
    
    Dim i As Long
    Dim NumeroElementosSeleccionados As Long
    Dim NumeroItems As Long
    Dim PosicaoSelec As Long
    
    If lista Is Nothing Then
        Exit Sub
    End If
    LimpaPropriedades = False ' Para que a Listview das propriedades n�o seja limpa
    EcPropriedade = ""
    EcValorProp = ""
    NumeroItems = lista.Count
    For i = 1 To NumeroItems
        If lista.item(i).Selected Then
            PosicaoSelec = i
            NumeroElementosSeleccionados = NumeroElementosSeleccionados + 1
        End If
    Next i
    
    If NumeroElementosSeleccionados = 1 Then
        EcPropriedade = Trim(lista.item(PosicaoSelec))
        EcValorProp = Trim(lista.item(PosicaoSelec).SubItems(1))
    End If
    LimpaPropriedades = True

End Sub

Private Sub ActualizaCabecalhoObjectos(Nodo As Node, Optional ListItem As ListItem)
    
    Dim TipoObjecto As String
    Dim ChaveNodo As String
    Dim CodigoObjecto As Long
    ' Informacoes do Nodo Principal (Grupo ou Utilizador)
    Dim ChaveNodoPrinc As String
    Dim TipoNodoPrinc As String
    Dim CodigoPrinc As Long
    ' Informacoes de um nodo Pai... (contentor)
    Dim ChaveNodoPai As String
    Dim CodigoPai As Long
    Dim i As Integer
    
    'limpar os campos de dados
    EcContentor.ListIndex = -1
    EcObjecto.Clear
    EcPropriedade = ""
    EcValorProp = ""
    'in�cio
    ChaveNodo = Nodo.Key
    TipoObjecto = left(ChaveNodo, 1) ' a primeira letra d�-nos o tipo de nodo
    CodigoObjecto = CLng(Mid(ChaveNodo, 2)) ' o restante varia de acordo com o nodo
    Select Case TipoObjecto                 ' ie se for um utilizador ou grupo o restante
        Case NODO_GRUPO                     ' � o c�digo, nos outros casos o restante
            ChkGrupo.value = 1              ' � o n� se sequ�ncia Interna
            EcCodigoGrupo = CodigoObjecto
            EcGrupoCombo = Nodo.Text
            
        Case NODO_UTILIZADOR
            ChkUtilizador.value = 1
            EcCodigoUtilizador = CodigoObjecto
            EcUtilizadorCombo = Nodo.Text
        
        Case NODO_CONTENTOR
            ' no caso do objecto contentor, o nodo pai � um utilizador
            ' ou um grupo...
            ChaveNodoPrinc = Nodo.parent.Key
            TipoNodoPrinc = left(ChaveNodoPrinc, 1)
            CodigoPrinc = Mid(ChaveNodoPrinc, 2)
            Select Case TipoNodoPrinc
                Case NODO_UTILIZADOR
                    ChkUtilizador.value = 1
                    EcCodigoUtilizador = CodigoPrinc
                Case NODO_GRUPO
                    ChkGrupo.value = 1
                    EcCodigoGrupo = CodigoPrinc
            End Select
            
            'EcDescr = Nodo.Parent.Text
            
            ProcuraContentor Nodo.Text
        Case NODO_OBJECTO
            ' no caso do objecto contentor, o nodo pai � um utilizador
            ' ou um grupo...
            ChaveNodoPrinc = Nodo.parent.parent.Key
            TipoNodoPrinc = left(ChaveNodoPrinc, 1)
            CodigoPrinc = Mid(ChaveNodoPrinc, 2)
            
            Select Case TipoNodoPrinc
                Case NODO_UTILIZADOR
                    ChkUtilizador.value = 1
                    EcCodigoUtilizador = CodigoPrinc
                Case NODO_GRUPO
                    ChkGrupo.value = 1
                    EcCodigoGrupo = CodigoPrinc
            End Select
            
            'EcDescr = Nodo.Parent.Parent.Text
            
            ProcuraContentor Nodo.parent.Text
            ProcuraObjecto Nodo.Text
    End Select

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        LimpaCampos
    Else
        Unload Me
    End If

End Sub

Sub FuncaoProcurar()

End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        CriaPermissao
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()

End Sub

Sub FuncaoModificar()

End Sub

Sub BD_Update()

End Sub

Sub FuncaoRemover()

End Sub

Sub BD_Delete()

End Sub

Private Sub LwPropriedades_Click()
    
    EcModoEdicao = MODO_EDITA_PERMISSOES
    ActualizaCabecalhoPermissoes LwPropriedades.ListItems
    
    ' esta flag serve para so actualizar se o valor
    ' da propriedade for alterado
    ValorPropriedadeMudou = False

End Sub

Private Sub LwPropriedades_KeyUp(KeyCode As Integer, Shift As Integer)
    
    EcModoEdicao = MODO_EDITA_PERMISSOES
    ActualizaCabecalhoPermissoes LwPropriedades.ListItems

End Sub

Private Sub ActivaCampos(ModoEdicao As String)
    
    Select Case ModoEdicao
        Case MODO_CRIA_PERMISSOES
            ChkGrupo.Enabled = True
            ChkUtilizador.Enabled = True
            EcContentor.Enabled = True
            EcObjecto.Enabled = True
            EcPropriedade.Enabled = True
            EcValorProp.Enabled = True
            If ChkGrupo.value Then
                EcGrupoCombo.Enabled = True
            End If
            If ChkUtilizador.value Then
                EcUtilizadorCombo.Enabled = True
            End If
        Case MODO_EDITA_PERMISSOES
            ChkGrupo.Enabled = False
            ChkUtilizador.Enabled = False
            EcContentor.Enabled = False
            EcObjecto.Enabled = False
            EcPropriedade.Enabled = False
            EcValorProp.Enabled = True
            EcGrupoCombo.Enabled = False
            EcUtilizadorCombo.Enabled = False
    End Select

End Sub

Private Sub LwPropriedades_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Dim TItem  As ListItem
    Dim ColItems As ListItems
    Dim CountSelec As Integer
    
    If Button <> 2 Then
        Exit Sub
    End If
    Set ColItems = LwPropriedades.ListItems
    For Each TItem In ColItems
        If TItem.Selected Then
            CountSelec = CountSelec + 1
            Exit For
        End If
    Next TItem
    PopupMenu MDIFormInicio.HIDE_PROP

End Sub

Private Sub TwObjectosTodos_Click()
    
    Dim Nodo As Node
    
    Set Nodo = TwObjectosTodos.SelectedItem
    If Nodo Is Nothing Then
        Exit Sub
    End If
    EcModoEdicao = MODO_CRIA_PERMISSOES
    ActualizaCabecalhoObjectos Nodo
    If TipoNodo(Nodo) = NODO_OBJECTO Then
        PreencheLwPropriedades Nodo
    End If

End Sub

Private Sub TwObjectosTodos_KeyUp(KeyCode As Integer, Shift As Integer)
    
    Dim Nodo As Node
    
    Set Nodo = TwObjectosTodos.SelectedItem
    If Nodo Is Nothing Then
        Exit Sub
    End If
    ActualizaCabecalhoObjectos Nodo
    If TipoNodo(Nodo) = NODO_OBJECTO Then
        PreencheLwPropriedades Nodo
    End If

End Sub

Private Sub TwObjectosTodos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Dim Nodo As Node

    If Button = 1 Then
        Exit Sub
    End If
    Set TwObjectosTodos.SelectedItem = TwObjectosTodos.HitTest(X, Y)
    Set Nodo = TwObjectosTodos.SelectedItem
    If Nodo Is Nothing Then
        Exit Sub
    End If

    Select Case TipoNodo(Nodo)
        Case NODO_GRUPO, NODO_UTILIZADOR
            If Nodo.Expanded Then
                MDIFormInicio.HIDE_OBJ_OPT(0).Enabled = False
                MDIFormInicio.HIDE_OBJ_OPT(1).Enabled = True
            Else
                MDIFormInicio.HIDE_OBJ_OPT(0).Enabled = True
                MDIFormInicio.HIDE_OBJ_OPT(1).Enabled = False
            End If
            MDIFormInicio.HIDE_OBJ_OPT(2).Visible = True
            MDIFormInicio.HIDE_OBJ_OPT(3).Visible = False
            MDIFormInicio.HIDE_OBJ_OPT(4).Visible = False
        Case NODO_CONTENTOR
            If Nodo.Expanded Then
                MDIFormInicio.HIDE_OBJ_OPT(0).Enabled = False
                MDIFormInicio.HIDE_OBJ_OPT(1).Enabled = True
            Else
                MDIFormInicio.HIDE_OBJ_OPT(0).Enabled = True
                MDIFormInicio.HIDE_OBJ_OPT(1).Enabled = False
            End If
            MDIFormInicio.HIDE_OBJ_OPT(2).Visible = False
            MDIFormInicio.HIDE_OBJ_OPT(3).Visible = True
            MDIFormInicio.HIDE_OBJ_OPT(4).Visible = False
        Case NODO_OBJECTO
            MDIFormInicio.HIDE_OBJ_OPT(0).Enabled = False
            MDIFormInicio.HIDE_OBJ_OPT(1).Enabled = True
            MDIFormInicio.HIDE_OBJ_OPT(2).Visible = False
            MDIFormInicio.HIDE_OBJ_OPT(3).Visible = False
            MDIFormInicio.HIDE_OBJ_OPT(4).Visible = True
    End Select
    PopupMenu MDIFormInicio.HIDE_OBJ

End Sub

Private Sub TwObjectosTodos_OLEDragDrop(data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    CopiaPermissoesOLEDragDrop TwObjectosTodos.SelectedItem

End Sub

Private Sub TwObjectosTodos_OLEDragOver(data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single, state As Integer)
    
    TwObjectosTodos.SelectedItem = TwObjectosTodos.HitTest(X, Y)
    If TwObjectosTodos.SelectedItem Is Nothing Then
        Exit Sub
    End If
    TwObjectosTodos.SelectedItem.Expanded = True

End Sub

Private Sub PreencheProposicoes()
    Dim NomeProp As String
    Dim FormsInf As String
    Dim pos1 As Single
    Dim pos2 As Single
    Dim pos3 As Single
    Dim pos4 As Single
    Dim pos5 As Single
    Dim PosFimProp As Single

    FormsInf = "INICIO_PROPS" & vbCrLf
    FormsInf = FormsInf & " PROP (Enabled)" & vbCrLf
    FormsInf = FormsInf & " PROP (FontBold)" & vbCrLf
    FormsInf = FormsInf & " PROP (FontItalic)" & vbCrLf
    FormsInf = FormsInf & " PROP (FontName)" & vbCrLf
    FormsInf = FormsInf & " PROP (FontSize)" & vbCrLf
    FormsInf = FormsInf & " PROP (FontStrikethru)" & vbCrLf
    FormsInf = FormsInf & " PROP (FontUnderline)" & vbCrLf
    FormsInf = FormsInf & " PROP (ForeColor)" & vbCrLf
    FormsInf = FormsInf & " PROP (Height)" & vbCrLf
    FormsInf = FormsInf & " PROP (Left)" & vbCrLf
    FormsInf = FormsInf & " PROP (Top)" & vbCrLf
    FormsInf = FormsInf & " PROP (Visible)" & vbCrLf
    FormsInf = FormsInf & " PROP (Width)" & vbCrLf
    FormsInf = FormsInf & "FIM_PROPS" & vbCrLf
    
    NumProps = 0
    ReDim Props(NumProps)
    pos1 = InStr(1, FormsInf, "INICIO_PROPS")
    PosFimProp = InStr(pos1, FormsInf, "FIM_PROPS")
    FormsInf = Mid(FormsInf, pos1 + 12)
    
    pos2 = InStr(1, FormsInf, "PROP (")
    While pos2 <> 0 And pos2 < PosFimProp
        pos3 = InStr(pos2, FormsInf, ")")
    
        If pos3 <> 0 Then
            NomeProp = Trim(Mid(FormsInf, pos2 + 6, pos3 - pos2 - 6))
            
            Props(NumProps) = NomeProp
            NumProps = NumProps + 1
            ReDim Preserve Props(NumProps)
            
            FormsInf = Mid(FormsInf, pos3 + 1)
        Else
            FormsInf = Mid(FormsInf, pos2 + 1)
        End If
    
        pos2 = InStr(1, FormsInf, "PROP (")
    Wend
End Sub


Private Sub CarregaInfoForms2()
    Dim nomeForm As String
    Dim NomeObj As String
    Dim NomeProp As String
    Dim FormsInf As String
    Dim sSql As String
    Dim rsObj As ADODB.recordset
    Dim pos1 As Single
    Dim pos2 As Single
    Dim pos3 As Single
    Dim pos4 As Single
    Dim pos5 As Single
    Dim PosFimForm As Single
    Dim PosFimObj As Single
    Dim PosFimProp As Single
    
    On Error GoTo Trata_Erro
    
    sSql = "SELECT * FROM SL_PARAM_ACESS_OBJECTOS ORDER BY form "
    Set rsObj = BG_ExecutaSELECT(sSql)
    
    NumForms = 0
    ReDim FormInfo(NumForms)
    FormInfo(NumForms).NumObjs = 0
    ReDim FormInfo(NumForms).Objs(FormInfo(NumForms).NumObjs)
    
    If rsObj.RecordCount > 0 Then
        While Not rsObj.EOF
            If FormInfo(NumForms).nome <> BL_HandleNull(rsObj!Form, "") Then
                NumForms = NumForms + 1
                ReDim Preserve FormInfo(NumForms)
                FormInfo(NumForms).nome = BL_HandleNull(rsObj!Form, "")
                FormInfo(NumForms).NumObjs = 0
                ReDim FormInfo(NumForms).Objs(FormInfo(NumForms).NumObjs)
            End If
            FormInfo(NumForms).NumObjs = FormInfo(NumForms).NumObjs + 1
            ReDim Preserve FormInfo(NumForms).Objs(FormInfo(NumForms).NumObjs)
            FormInfo(NumForms).Objs(FormInfo(NumForms).NumObjs).nome = BL_HandleNull(rsObj!Objecto, "")
            FormInfo(NumForms).Objs(FormInfo(NumForms).NumObjs).caption = BL_HandleNull(rsObj!caption, "")
            FormInfo(NumForms).Objs(FormInfo(NumForms).NumObjs).indice = BL_HandleNull(rsObj!indice, -1)
            rsObj.MoveNext
        Wend
    End If
    BG_DestroiRecordSet rsObj
    PreencheProposicoes
    
Exit Sub

Trata_Erro:
    BG_LogFile_Erros "CarregaInfoForms: " & Err.Number & "-" & Err.Description

End Sub

VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form FormAutomatizaResultadosSerie 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   3450
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5205
   Icon            =   "FormAutomatizaResultadosSerie.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3450
   ScaleWidth      =   5205
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtSelRes 
      BackColor       =   &H00C0E0FF&
      Height          =   525
      Left            =   4200
      Picture         =   "FormAutomatizaResultadosSerie.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Pr�-Visualizar"
      Top             =   2040
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.CommandButton BtValida 
      BackColor       =   &H00C0FFC0&
      Height          =   525
      Left            =   4200
      MaskColor       =   &H00C0FFC0&
      Picture         =   "FormAutomatizaResultadosSerie.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Gravar, Validar e Disponibilizar online"
      Top             =   2640
      Width           =   615
   End
   Begin VB.CommandButton BtPesquisaRapidaAutoRes 
      Height          =   375
      Left            =   4800
      Picture         =   "FormAutomatizaResultadosSerie.frx":19A0
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   120
      Width           =   375
   End
   Begin VB.TextBox EcDescrAuto 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   1920
      Locked          =   -1  'True
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   120
      Width           =   2895
   End
   Begin VB.TextBox EcCodAuto 
      BackColor       =   &H80000018&
      Height          =   285
      Left            =   960
      TabIndex        =   0
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton BtAcrecenta 
      Height          =   525
      Left            =   4200
      Picture         =   "FormAutomatizaResultadosSerie.frx":1F2A
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   600
      Width           =   615
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      ItemData        =   "FormAutomatizaResultadosSerie.frx":877C
      Left            =   120
      List            =   "FormAutomatizaResultadosSerie.frx":877E
      TabIndex        =   7
      Top             =   1080
      Width           =   3735
   End
   Begin VB.TextBox EcRequisicao 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   960
      TabIndex        =   3
      Top             =   600
      Width           =   945
   End
   Begin RichTextLib.RichTextBox RText 
      Height          =   615
      Left            =   3120
      TabIndex        =   8
      Top             =   3720
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   1085
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"FormAutomatizaResultadosSerie.frx":8780
   End
   Begin VB.Label Label1 
      Caption         =   "Automatiz. Resultados"
      Height          =   495
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Requisi��o"
      Height          =   225
      Left            =   120
      TabIndex        =   9
      Top             =   615
      Width           =   975
   End
End
Attribute VB_Name = "FormAutomatizaResultadosSerie"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Private Type DadosRequisLista
    num_req As String
    seq_utente As String
    estado_req As String
End Type

Dim ListaRequis() As DadosRequisLista
Dim TamListaRequis As Long

Public rs As ADODB.recordset

Private Sub BtAcrecenta_Click()
    AcrescentaLista
End Sub

Private Sub BtPesquisaRapidaAutoRes_Click()
    
    Dim ChavesPesq(1 To 2) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 2) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 2) As Long
    Dim Headers(1 To 2) As String
    Dim CancelouPesquisa As Boolean
    Dim ResultadosPesq(1 To 2)  As Variant
    Dim PesqRapida As Boolean
    

    
    PesqRapida = False
    
    ChavesPesq(1) = "cod_auto_res"
    CamposEcran(1) = "cod_auto_res"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    ChavesPesq(2) = "descr_auto_res"
    CamposEcran(2) = "descr_auto_res"
    Tamanhos(2) = 4000
    Headers(2) = "Descri��o"
    
    
    CamposRetorno.InicializaResultados 2
    
    CFrom = "sl_auto_res"
    CampoPesquisa = "descr_auto_res"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Automatiza��o de Resultados")
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados ResultadosPesq, CancelouPesquisa

        If Not CancelouPesquisa Then
            EcCodAuto.Text = ResultadosPesq(1)
            EcDescrAuto.Text = ResultadosPesq(2)
        End If

    Else
        BG_Mensagem mediMsgBox, "N�o existem automatiza��es de resultados codificadas", vbExclamation, "ATEN��O"
    End If

End Sub


Private Sub BtValida_Click()
    If EcCodAuto.Text <> "" And TamListaRequis > 0 Then
        AutomatizaResultados
    End If
End Sub

Private Sub EcCodAuto_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub
Private Sub EcCodAuto_Validate(Cancel As Boolean)
    Dim RsDescrAuto As ADODB.recordset
    
    EcCodAuto.Text = UCase(EcCodAuto.Text)
    If Trim(EcCodAuto.Text) <> "" Then
        Set RsDescrAuto = New ADODB.recordset
        
        With RsDescrAuto
            .Source = "SELECT descr_auto_res FROM sl_auto_res WHERE cod_auto_res= " & BL_TrataStringParaBD(EcCodAuto.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAuto.RecordCount > 0 Then
            EcDescrAuto.Text = RsDescrAuto!descr_AUTO_RES
            RsDescrAuto.Close
            Set RsDescrAuto = Nothing
        Else
            RsDescrAuto.Close
            Set RsDescrAuto = Nothing
            EcDescrAuto.Text = ""
            BG_Mensagem mediMsgBox, "A automatiza��o de resultados indicada n�o existe!", vbOKOnly + vbExclamation, "Aten��o!"
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrAuto.Text = ""
    End If
    
End Sub





Private Sub BtSelRes_Click()
    Dim i As Integer
    Dim Criterio As String
    Dim AutoRes As String
    
    Criterio = ""
    
    For i = 1 To TamListaRequis
        Criterio = Criterio & ListaRequis(i).num_req & ","
    Next i
     'Remove a virgula do final da express�o
    Criterio = Mid(Criterio, 1, Len(Criterio) - 1)


    If (gSGBD = gOracle) Then
        gSqlSelResSELECT1 = "SELECT -1 as seq_realiza, marca.n_req n_req, marca.cod_perfil, marca.cod_ana_c, marca.cod_ana_s, " & _
                        " marca.dt_chega, marca.ord_ana, marca.ord_marca, " & _
                        " marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, marca.n_folha_trab, req.n_epis,req.t_sit, req.n_req_assoc, " & _
                        " req.dt_chega as dt_chega_req,  req.hr_chega as hr_chega_req, " & _
                        " req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute, " & _
                        " id.dt_nasc_ute, id.sexo_ute, " & _
                        " 'Marcacoes' as FLG_UNID_ACT1, 'Marcacoes' as FLG_UNID_ACT2, 'Marcacoes' as UNID_1_RES1, 'Marcacoes' as UNID_2_RES1, 'Marcacoes' as FAC_CONV_UNID1, 'Marcacoes' as UNID_1_RES2, 'Marcacoes' as UNID_2_RES2, 'Marcacoes' as FAC_CONV_UNID2, req.inf_complementar " & _
                    "flg_apar_trans, marca.flg_facturado,req.inf_complementar , slv_analises.cod_gr_ana "
        gSqlSelResFROM1 = " FROM sl_marcacoes marca, sl_requis req, sl_identif id, slv_analises "
        gSqlSelResWHERE1 = " WHERE req.n_req = marca.n_req " & _
                    " AND   id.seq_utente = req.seq_utente " & _
                    " AND   marca.dt_chega IS NOT NULL "
    ElseIf (gSGBD = gSqlServer) Then
        gSqlSelResSELECT1 = "SELECT -1 as seq_realiza, marca.n_req n_req, marca.cod_perfil, marca.cod_ana_c, marca.cod_ana_s, " & _
                        " marca.dt_chega, marca.ord_ana, marca.ord_marca, " & _
                        " marca.ord_ana_compl, marca.ord_ana_perf, marca.cod_agrup, marca.n_folha_trab, req.n_epis,req.t_sit, req.n_req_assoc, " & _
                        " req.dt_chega as dt_chega_req,  req.hr_chega as hr_chega_req, " & _
                        " req.estado_req, id.seq_utente, id.t_utente, id.utente, id.nome_ute, " & _
                        " id.dt_nasc_ute, id.sexo_ute, " & _
                        " 'Marcacoes' as FLG_UNID_ACT1, 'Marcacoes' as FLG_UNID_ACT2, 'Marcacoes' as UNID_1_RES1, 'Marcacoes' as UNID_2_RES1, 'Marcacoes' as FAC_CONV_UNID1, 'Marcacoes' as UNID_1_RES2, 'Marcacoes' as UNID_2_RES2, 'Marcacoes' as FAC_CONV_UNID2, req.inf_complementar " & _
                    "flg_apar_trans, marca.flg_facturado,req.inf_complementar , slv_analises.cod_gr_ana  "
        gSqlSelResFROM1 = " FROM sl_marcacoes marca, sl_requis req, sl_identif id "
        gSqlSelResWHERE1 = " WHERE req.n_req = marca.n_req " & _
                    " AND   id.seq_utente = req.seq_utente " & _
                    " AND   marca.dt_chega IS NOT NULL "
    End If
    gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND marca.n_req in (" & Criterio & ") "
    
    gSqlSelResWHERE1 = gSqlSelResWHERE1 & " AND slv_analises.cod_ana = marca.cod_agrup "
    gSqlSelResORDER = " ORDER BY marca.n_req, cod_gr_ana , marca.ord_ana, marca.cod_agrup, marca.cod_perfil, marca.ord_ana_perf, marca.cod_ana_c, marca.ord_ana_compl, marca.cod_ana_s"
    
    gSqlSelRes = gSqlSelResSELECT1 & gSqlSelResFROM1 & gSqlSelResWHERE1 & gSqlSelResSELECT2 & gSqlSelResFROM2 & gSqlSelResWHERE2 & gSqlSelResORDER
    
    EcLista.Clear
    TamListaRequis = 0
    EcRequisicao.SetFocus
    
    gModoRes = cModoResIns
    
    
'    If gModoRes = cModoResMicro Then
'        FormResMicro.Show
'    Else
            AutoRes = EcCodAuto.Text
            FormAutomatizaResultadosSerie.Enabled = False
            FormResultados.Show
            Set gFormActivo = FormResultados
'            FormResultados.EventoActivate
            FormResultados.AutomatizaResultados (AutoRes)
            
'    End If
    


End Sub

Private Sub EcRequisicao_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        AcrescentaLista
    End If
End Sub
Sub AcrescentaLista()
    Dim rs As ADODB.recordset
    Dim sql As String
    
    If EcRequisicao.Text <> "" Then
        If EcCodAuto.Text <> "" Then
            sql = "SELECT sl_requis.N_REQ,SL_MARCACOES.COD_ANA_S from sl_ana_res, sl_marcacoes, sl_requis where sl_marcacoes.cod_ana_s = sl_ana_res.cod_ana_s " & _
                    " and sl_Requis.n_req = " & EcRequisicao.Text & " AND cod_auto_res = " & BL_TrataStringParaBD(EcCodAuto.Text) & _
                    " AND sl_requis.n_req = sl_marcacoes.n_req AND sl_requis.estado_Req NOT IN (" & BL_TrataStringParaBD(gEstadoReqBloqueada) & _
                    "," & BL_TrataStringParaBD(gEstadoReqCancelada) & ") " & _
                    " union " & _
                    " SELECT sl_requis.n_req, sl_marcacoes.cod_ana_s from sl_marcacoes, sl_membro, sl_ana_res, sl_requis " & _
                    " where sl_marcacoes.cod_ana_c = sl_membro.cod_ana_c and " & _
                    " sl_membro.cod_membro = sl_ana_res.cod_ana_s and " & _
                    " sl_marcacoes.cod_ana_s = 'S99999' and sl_requis.n_req = " & EcRequisicao & _
                    " AND sl_requis.n_Req = sl_marcacoes.n_Req AND sl_requis.estado_Req NOT IN (" & BL_TrataStringParaBD(gEstadoReqBloqueada) & _
                    "," & BL_TrataStringParaBD(gEstadoReqCancelada) & ") " & _
                    " AND cod_auto_res = " & BL_TrataStringParaBD(EcCodAuto.Text)
            Set rs = New ADODB.recordset
            rs.CursorLocation = adUseServer
            rs.CursorType = adOpenStatic
            rs.Open sql, gConexao
            
            If rs.RecordCount > 0 Then
                TamListaRequis = TamListaRequis + 1
                ReDim Preserve ListaRequis(TamListaRequis)
                ListaRequis(TamListaRequis).num_req = EcRequisicao.Text

                
                EcLista.AddItem EcRequisicao.Text
                EcRequisicao.Text = ""
            Else
                BG_Mensagem mediMsgBox, "REQUISI��O SEM AN�LISES ASSOCIADAS � AUTOMATIZA��O SELECCIONADA!", vbInformation, App.ProductName
                EcRequisicao.Text = ""
                EcRequisicao.SetFocus
            End If
            
            rs.Close
            Set rs = Nothing
        Else
            BG_Mensagem mediMsgBox, "Seleccione primeiro a automatiza��o de resultados pretendida!", vbInformation, App.ProductName
        End If
    End If
End Sub
Private Sub EcRequisicao_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    
End Sub

Private Sub EcRequisicao_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Form Automatiza��o de Resultados em S�rie"
    Me.left = 540
    Me.top = 450
    Me.Width = 5385
    Me.Height = 4770 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    Set CampoDeFocus = EcCodAuto
    
    TamListaRequis = 0
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    gF_AUTORESSERIE = 1
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    gF_AUTORESSERIE = 0
    Set FormAutomatizaResultadosSerie = Nothing
    
End Sub

Sub LimpaCampos()
    Me.SetFocus

    EcCodAuto.Text = ""
    EcDescrAuto.Text = ""
    EcLista.Clear
    TamListaRequis = 0
    EcCodAuto.SetFocus
    
End Sub

Sub DefTipoCampos()
    EcRequisicao.Tag = adInteger
End Sub

Sub PreencheCampos()
    
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
End Sub

Sub FuncaoAnterior()

End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()

End Sub

Sub BD_Insert()
    
End Sub

Sub FuncaoModificar()

End Sub

Sub BD_Update()

End Sub

Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub


Sub AutomatizaResultados()
    Dim sql As String
    Dim RsAuto As ADODB.recordset
    Dim i As Long
    Dim k As Long
    Dim j As Long
    Dim Str1 As String
    Dim Str2 As String
    Dim CmdFr As New ADODB.Command
    Dim PmtFr As ADODB.Parameter
    Dim RsFr As ADODB.recordset
    Dim CmdMemb As New ADODB.Command
    Dim PmtMemb As ADODB.Parameter
    Dim PmtCodAuto As ADODB.Parameter
    Dim RsMemb As ADODB.recordset
    Dim CodAnaCAnt As String
    Dim ReqAnt As Long
    Dim Criterio As String
    Dim seqRealiza As Long
    Dim SqlDel As String
    Dim CmdPerf As New ADODB.Command
    Dim PmtPerf As ADODB.Parameter
    Dim RsPerf As ADODB.recordset
    
    
    'Inicializa��o do comando que selecciona a descri��o da frase
    CmdFr.ActiveConnection = gConexao
    CmdFr.CommandType = adCmdText
    CmdFr.CommandText = "SELECT descr_frase FROM sl_dicionario WHERE cod_frase=?"
    CmdFr.Prepared = True
    Set PmtFr = CmdFr.CreateParameter("COD_FRASE", adVarChar, adParamInput, 10)
    CmdFr.Parameters.Append PmtFr
    
    'Inicializa��o do comando que selecciona os membros de uma complexa
    CmdMemb.ActiveConnection = gConexao
    CmdMemb.CommandType = adCmdText
    'CmdMemb.CommandText = "SELECT cod_membro FROM sl_membro WHERE cod_ana_c=? and cod_membro in (Select cod_ana_s from sl_ana_res where cod_auto_res =? )"
    If gSGBD = gOracle Then
        CmdMemb.CommandText = "select cod_ana_c,cod_membro,t_result, t_membro,sl_membro.ordem,cod_auto_res,descr_res " & _
                               " from sl_membro, sl_ana_res, sl_ana_s where " & _
                               " sl_membro.cod_membro = sl_ana_s.cod_ana_s and " & _
                               " sl_membro.cod_membro = sl_ana_res.cod_ana_s and cod_ana_c = ? and cod_auto_res =? order by ordem"
    Else
        CmdMemb.CommandText = "select cod_ana_c,cod_membro,t_result, t_membro,sl_membro.ordem,cod_auto_res,descr_res " & _
                               " from sl_membro, sl_ana_res, sl_ana_s where " & _
                               " sl_membro.cod_membro = sl_ana_s.cod_ana_s and " & _
                               " sl_membro.cod_membro = sl_ana_res.cod_ana_s and cod_ana_c = ? and cod_auto_res =? order by sl_membro.ordem"
    End If
    CmdMemb.Prepared = True
    Set PmtMemb = CmdMemb.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 8)
    Set PmtCodAuto = CmdMemb.CreateParameter("COD_AUTO_RES", adVarChar, adParamInput, 8)
    CmdMemb.Parameters.Append PmtMemb
    CmdMemb.Parameters.Append PmtCodAuto
    
    
    'Inicializa��o do comando que selecciona os membros de um perfis
    CmdPerf.ActiveConnection = gConexao
    CmdPerf.CommandText = "select cod_perfis,cod_ana_c,cod_membro,t_result,t_membro,sl_membro.ordem,cod_auto_res,descr_res " & _
                            " From sl_ana_perfis, sl_membro, sl_ana_res, sl_ana_s " & _
                            " Where sl_ana_perfis.cod_analise = sl_membro.Cod_Ana_C " & _
                            " and sl_membro.cod_membro = sl_ana_s.cod_ana_s " & _
                            " and sl_ana_s.cod_ana_s = sl_ana_res.cod_ana_s " & _
                            " and sl_ana_perfis.cod_perfis = ? and sl_ana_res.cod_auto_res = ? "
    CmdPerf.Prepared = True
    Set PmtPerf = CmdPerf.CreateParameter("COD_PERFIS", adVarChar, adParamInput, 8)
    Set PmtCodAuto = CmdMemb.CreateParameter("COD_AUTO_RES", adVarChar, adParamInput, 8)
    CmdPerf.Parameters.Append PmtPerf
    CmdPerf.Parameters.Append PmtCodAuto
    
    Criterio = ""
    
    For i = 1 To TamListaRequis
        Criterio = Criterio & ListaRequis(i).num_req & ","
    Next i
     'Remove a virgula do final da express�o
    Criterio = Mid(Criterio, 1, Len(Criterio) - 1)

    'Seleccionar as an�lises simples e resultados para a automatiza��o escolhida
    If gLAB = "LHL" And EcCodAuto = "01" Then
        sql = "SELECT m.n_req,r.seq_utente,r.dt_chega dt_chega_req,r.hr_chega hr_chega_req, cod_perfil,cod_ana_c,m.cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup, " & _
               " n_folha_trab,flg_apar_trans,m.dt_chega,ord_marca,m.flg_facturado,m.hr_colheita,m.dt_colheita,hr_ult_admin,m.hr_chega, " & _
               " n_folha_tubo , cod_auto_res, descr_Res " & _
               " FROM sl_requis r, sl_marcacoes m  LEFT OUTER JOIN  sl_ana_res ar ON m.cod_ana_s = ar.cod_ana_s " & _
               " WHERE m.n_req = r.n_req AND " & _
               " m.n_Req in (" & Criterio & ") and (cod_auto_Res = '" & EcCodAuto.Text & "' or m.cod_ana_s = 'S99999') AND COD_PERFIL = 'P276' " & _
               " ORDER BY r.n_req,ord_ana,cod_agrup,cod_perfil,ord_ana_perf,cod_ana_c,ord_ana_compl,m.cod_ana_s"
    ElseIf gSGBD = gOracle Then
        sql = "SELECT m.n_req,r.seq_utente,r.dt_chega dt_chega_req,r.hr_chega hr_chega_req, cod_perfil,cod_ana_c,m.cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup, " & _
               " n_folha_trab,flg_apar_trans,m.dt_chega,ord_marca,m.flg_facturado,m.hr_colheita,m.dt_colheita,hr_ult_admin,m.hr_chega, " & _
               " n_folha_tubo , cod_auto_res, descr_Res " & _
               " FROM sl_marcacoes m,sl_requis r,  sl_ana_res r " & _
               " WHERE m.n_req = r.n_req AND " & _
               " m.cod_ana_s = r.cod_ana_s(+) and " & _
               " m.n_Req in (" & Criterio & ") and (cod_auto_Res = '" & EcCodAuto.Text & "' or m.cod_ana_s = 'S99999') " & _
               " ORDER BY n_req,ord_ana,cod_agrup,cod_perfil,ord_ana_perf,cod_ana_c,ord_ana_compl,cod_ana_s"
    ElseIf gSGBD = gSqlServer Then
        sql = "SELECT m.n_req,r.seq_utente,r.dt_chega dt_chega_req,r.hr_chega hr_chega_req, cod_perfil,cod_ana_c,m.cod_ana_s,ord_ana,ord_ana_compl,ord_ana_perf,cod_agrup, " & _
               " n_folha_trab,flg_apar_trans,m.dt_chega,ord_marca,m.flg_facturado,m.hr_colheita,m.dt_colheita,hr_ult_admin,m.hr_chega, " & _
               " n_folha_tubo , cod_auto_res, descr_Res " & _
               " FROM sl_requis r, sl_marcacoes m  LEFT OUTER JOIN  sl_ana_res ar ON m.cod_ana_s = ar.cod_ana_s " & _
               " WHERE m.n_req = r.n_req AND " & _
               " m.n_Req in (" & Criterio & ") and (cod_auto_Res = '" & EcCodAuto.Text & "' or m.cod_ana_s = 'S99999') " & _
               " ORDER BY r.n_req,ord_ana,cod_agrup,cod_perfil,ord_ana_perf,cod_ana_c,ord_ana_compl,m.cod_ana_s"
    End If
    Set RsAuto = New ADODB.recordset
    RsAuto.CursorLocation = adUseServer
    RsAuto.CursorType = adOpenStatic
    RsAuto.Open sql, gConexao
    If RsAuto.RecordCount > 0 Then
        CodAnaCAnt = "0"
        ReqAnt = 0
        BG_BeginTransaction
        
        On Error GoTo TrataErro
        
        While Not RsAuto.EOF
            If RsAuto!cod_ana_c = "C99999" And RsAuto!cod_ana_s = "S99999" Then
                'Verifica se este perfis tem membros para esta automatiza��o
                CmdPerf.Parameters(0).value = RsAuto!Cod_Perfil
                CmdPerf.Parameters(1).value = EcCodAuto.Text
                Set RsPerf = CmdPerf.Execute
                If Not RsPerf.EOF Then
                    'Insere C99999
                    seqRealiza = InsereRealiza(RsAuto!n_req, RsAuto!seq_utente, RsAuto!Cod_Perfil, RsAuto!cod_ana_c, RsAuto!cod_ana_s, RsAuto!Ord_Ana, RsAuto!Ord_Ana_Compl, RsAuto!Ord_Ana_Perf, RsAuto!cod_agrup, _
                                                RsAuto!N_Folha_Trab, RsAuto!flg_apar_trans, RsAuto!dt_chega, RsAuto!Ord_Marca, RsAuto!Flg_Facturado, BL_HandleNull(RsAuto!hr_colheita, ""), BL_HandleNull(RsAuto!dt_colheita, ""), BL_HandleNull(RsAuto!hr_ult_admin, ""), IIf(BL_HandleNull(RsAuto!hr_chega, "") = "", BL_HandleNull(RsAuto!hr_chega_req, ""), RsAuto!hr_chega), _
                                                BL_HandleNull(RsAuto!n_folha_tubo, ""))
                    If seqRealiza = -1 Then
                        GoTo TrataErro
                    End If
                    'Apaga do sl_marcacoes a C99999
                    SqlDel = "Delete from sl_marcacoes where n_req = " & RsAuto!n_req & " and cod_perfil = " & BL_TrataStringParaBD(RsAuto!Cod_Perfil) & " and cod_ana_c = " & BL_TrataStringParaBD(RsAuto!cod_ana_c) & " and cod_ana_s = " & BL_TrataStringParaBD(RsAuto!cod_ana_s)
                    BL_RegistaAnaEliminadas CStr(RsAuto!n_req), "", RsAuto!Cod_Perfil, RsAuto!cod_ana_c, RsAuto!cod_ana_s
                    BG_ExecutaQuery_ADO SqlDel
                End If
            ElseIf RsAuto!cod_ana_c = "0" Or RsAuto!cod_ana_c <> CodAnaCAnt Or RsAuto!n_req <> ReqAnt Then
                If RsAuto!cod_ana_s = gGHOSTMEMBER_S Then
                        j = 1
                        'Verifica membros da complexa se pertencem a esta automatiza��o
                        'Seleccionar as an�lises dentro do GhostMember
                        CmdMemb.Parameters(0).value = RsAuto!cod_ana_c
                        CmdMemb.Parameters(1).value = EcCodAuto.Text
                        Set RsMemb = CmdMemb.Execute
                        While Not RsMemb.EOF
                            If j = 1 Then
                                'Insere S99999
                                seqRealiza = InsereRealiza(RsAuto!n_req, RsAuto!seq_utente, RsAuto!Cod_Perfil, RsAuto!cod_ana_c, RsAuto!cod_ana_s, RsAuto!Ord_Ana, RsAuto!Ord_Ana_Compl, BL_HandleNull(RsAuto!Ord_Ana_Perf), RsAuto!cod_agrup, _
                                                            RsAuto!N_Folha_Trab, RsAuto!flg_apar_trans, RsAuto!dt_chega, RsAuto!Ord_Marca, RsAuto!Flg_Facturado, BL_HandleNull(RsAuto!hr_colheita, ""), BL_HandleNull(RsAuto!dt_colheita, ""), BL_HandleNull(RsAuto!hr_ult_admin, ""), IIf(BL_HandleNull(RsAuto!hr_chega, "") = "", BL_HandleNull(RsAuto!hr_chega_req, ""), RsAuto!hr_chega), _
                                                            BL_HandleNull(RsAuto!n_folha_tubo, ""))
                                If seqRealiza = -1 Then
                                    GoTo TrataErro
                                End If
                                'Apaga do sl_marcacoes a S99999
                                If gLAB = "LHL" And EcCodAuto = "01" Then
                                    SqlDel = "Delete from sl_marcacoes where n_req = " & RsAuto!n_req & " and cod_perfil = " & BL_TrataStringParaBD(RsAuto!Cod_Perfil) & " and cod_ana_c = " & BL_TrataStringParaBD(RsAuto!cod_ana_c)
                                Else
                                    SqlDel = "Delete from sl_marcacoes where n_req = " & RsAuto!n_req & " and cod_perfil = " & BL_TrataStringParaBD(RsAuto!Cod_Perfil) & " and cod_ana_c = " & BL_TrataStringParaBD(RsAuto!cod_ana_c) & " and cod_ana_s = " & BL_TrataStringParaBD(RsAuto!cod_ana_s)
                                
                                End If
                                BL_RegistaAnaEliminadas CStr(RsAuto!n_req), "", RsAuto!Cod_Perfil, RsAuto!cod_ana_c, RsAuto!cod_ana_s
                                BG_ExecutaQuery_ADO SqlDel
                            End If
                            'Insere cod_ana_s membro
                            seqRealiza = InsereRealiza(RsAuto!n_req, RsAuto!seq_utente, RsAuto!Cod_Perfil, RsAuto!cod_ana_c, RsMemb!cod_membro, RsAuto!Ord_Ana, RsMemb!ordem, BL_HandleNull(RsAuto!Ord_Ana_Perf, ""), RsAuto!cod_agrup, _
                                                        RsAuto!N_Folha_Trab, RsAuto!flg_apar_trans, RsAuto!dt_chega, RsAuto!Ord_Marca, RsAuto!Flg_Facturado, BL_HandleNull(RsAuto!hr_colheita, ""), BL_HandleNull(RsAuto!dt_colheita, ""), BL_HandleNull(RsAuto!hr_ult_admin, ""), IIf(BL_HandleNull(RsAuto!hr_chega, "") = "", BL_HandleNull(RsAuto!hr_chega_req, ""), RsAuto!hr_chega), _
                                                        BL_HandleNull(RsAuto!n_folha_tubo, ""))
                            If seqRealiza = -1 Then
                                GoTo TrataErro
                            End If
                            Str1 = Mid(RsMemb!descr_res, 1, 2)
                            Str2 = Mid(RsMemb!descr_res, 3, Len(RsMemb!descr_res) - 2)
                            If Str1 = "\\" Then
                                If RsMemb!t_result = gT_Frase Then
                                    
                                    'Insere na sl_res_frase
                                    If InsereResFrase(seqRealiza, 1, Str2, 0) = False Then
                                        GoTo TrataErro
                                    End If
                                Else
                                    'Resultado Frase, seleccionar a descri��o
                                    CmdFr.Parameters(0).value = Str2
                                    Set RsFr = CmdFr.Execute
                                    If Len(Trim(RsFr!descr_frase)) > 20 Then
                                        'Insere resultado frase
                                        If InsereResFrase(seqRealiza, 1, Str2, 0) = False Then
                                            GoTo TrataErro
                                        End If
                                        
                                    Else
                                        'Insere resultado alfanum�rico
                                        If InsereResAlfan(seqRealiza, 1, RsMemb!descr_res) = False Then
                                            GoTo TrataErro
                                        End If
                                    End If
                                End If
                            Else
                                'Insere resultado alfanum�rico
                                If InsereResAlfan(seqRealiza, 1, RsMemb!descr_res) = False Then
                                    GoTo TrataErro
                                End If
                            End If
    
                            RsMemb.MoveNext
                            j = j + 1
                        Wend
                        'j� inseriu todos os membros da complexa
                        CodAnaCAnt = RsAuto!cod_ana_c
                        ReqAnt = RsAuto!n_req
                Else
                    
                    'An�lises simples
                    'Insere cod_ana_s membro
                    CmdMemb.Parameters(0).value = RsAuto!cod_ana_c
                    CmdMemb.Parameters(1).value = EcCodAuto.Text
                    Set RsMemb = CmdMemb.Execute
                    While Not RsMemb.EOF
                        seqRealiza = InsereRealiza(RsAuto!n_req, RsAuto!seq_utente, RsAuto!Cod_Perfil, RsAuto!cod_ana_c, RsAuto!cod_ana_s, RsAuto!Ord_Ana, RsAuto!Ord_Ana_Compl, RsAuto!Ord_Ana_Perf, RsAuto!cod_agrup, _
                                                    RsAuto!N_Folha_Trab, RsAuto!flg_apar_trans, RsAuto!dt_chega, RsAuto!Ord_Marca, RsAuto!Flg_Facturado, BL_HandleNull(RsAuto!hr_colheita, ""), BL_HandleNull(RsAuto!dt_colheita, ""), BL_HandleNull(RsAuto!hr_ult_admin, ""), IIf(BL_HandleNull(RsAuto!hr_chega, "") = "", BL_HandleNull(RsAuto!hr_chega_req, ""), RsAuto!hr_chega), _
                                                    BL_HandleNull(RsAuto!n_folha_tubo, ""))
                        If seqRealiza = -1 Then
                            GoTo TrataErro
                        End If
                        Str1 = Mid(RsAuto!descr_res, 1, 2)
                        Str2 = Mid(RsAuto!descr_res, 3, Len(RsAuto!descr_res) - 2)
                        If Str1 = "\\" Then
                            If RsMemb!t_result = gT_Frase Then
                                'Insere na sl_res_frase
                                If InsereResFrase(seqRealiza, 1, Str2, 0) = False Then
                                    GoTo TrataErro
                                End If
                            Else
                                'Resultado Frase, seleccionar a descri��o
                                CmdFr.Parameters(0).value = Str2
                                Set RsFr = CmdFr.Execute
                                If Len(Trim(RsFr!descr_frase)) > 20 Then
                                    'Insere resultado frase
                                    If InsereResFrase(seqRealiza, 1, Str2, 0) = False Then
                                        GoTo TrataErro
                                    End If
                                    
                                Else
                                    'Insere resultado alfanum�rico
                                    If InsereResAlfan(seqRealiza, 1, RsMemb!descr_res) = False Then
                                        GoTo TrataErro
                                    End If
                                End If
                            End If
                        Else
                                                                
                            'Insere resultado alfanum�rico
                            If InsereResAlfan(seqRealiza, 1, RsAuto!descr_res) = False Then
                                GoTo TrataErro
                            End If
                        End If
                    RsMemb.MoveNext
                    Wend
                End If

            End If
            RsAuto.MoveNext
        Wend
        
        'Apaga an�lises simples da automatiza��o de resultados da lista de requisi��es
        If gLAB = "LHL" And EcCodAuto = "01" Then
            SqlDel = "Delete from sl_marcacoes where n_req IN (" & Criterio & ") And cod_ana_c ='CEBT'"
        Else
            SqlDel = "delete from sl_marcacoes where n_req in (" & Criterio & ") and cod_ana_s in (" & _
                     "select cod_ana_s from sl_ana_res where cod_auto_res = '" & EcCodAuto.Text & "')"
        End If
        BG_ExecutaQuery_ADO SqlDel
        
        BG_CommitTransaction
        
        BG_Mensagem mediMsgBox, "Resultados validados com sucesso!", vbInformation, App.title
        LimpaCampos
    End If
    
    If Not RsFr Is Nothing Then
        RsFr.Close
        Set RsFr = Nothing
    End If
    If Not RsMemb Is Nothing Then
        RsMemb.Close
        Set RsMemb = Nothing
    End If
    Set CmdFr = Nothing
    Set CmdMemb = Nothing
    
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_Mensagem mediMsgBox, "Erro a gravar resultados!" & vbCrLf & "Valida��o n�o efectuada!", vbExclamation, "ERRO"
    Exit Sub
    Resume Next
End Sub


Function InsereRealiza(NReq As Long, SeqUte As Long, CodPerfil As String, CodAnaC As String, codAnaS As String, OrdAna As String, OrdAnaC As String, OrdAnaP As String, codAgrup As String, _
                        NFolhaTrab As String, FlgAparTrans As String, DtChega As String, OrdMarca As String, FlgFacturado As String, HrColheita As String, DtColheita As String, HrUltAdmin As String, HrChega As String, _
                        NFolhaTubo As String)
    Dim i As Integer
    Dim seq As Long
    Dim EstadoAna As String
    Dim EstadoVal As String
    Dim EstadoValTec As String
    Dim sql As String
    On Error GoTo TrataErro
    
    i = 0
    seq = -1
    While seq = -1 And i <= 10
        seq = BL_GeraNumero("SEQ_REALIZA")
        i = i + 1
    Wend

    If seq = -1 Then
        GoTo TrataErro
    End If
    
    If gPermResUtil = cValMedRes Then
        'Valida��o de resultados
        EstadoAna = "3"
    ElseIf gPermResUtil = cValTecRes Then
        'Valida��o T�cnica
        EstadoAna = "5"
    Else
        'Introdu��o
        EstadoAna = "1"
    End If
    
    
    'Gravar na tabela sl_realiza
    sql = "INSERT INTO sl_realiza (seq_realiza, n_req, cod_perfil, cod_ana_c, cod_ana_s, " & _
        "ord_ana, ord_ana_compl, ord_ana_perf, cod_agrup, n_folha_trab, flg_estado, user_val, " & _
        "dt_val, hr_val, dt_cri, user_cri, hr_cri, dt_chega,  " & _
        "seq_utente, user_tec_val, dt_tec_val, hr_tec_val, " & _
        "ord_marca, flg_facturado) " & _
        "VALUES (" & seq & ", " & NReq & "," & BL_TrataStringParaBD(CodPerfil) & ", " & _
        BL_TrataStringParaBD(CodAnaC) & ", " & BL_TrataStringParaBD(codAnaS) & ", " & _
        OrdAna & ", " & OrdAnaC & ", " & OrdAnaP & ", " & BL_TrataStringParaBD(codAgrup) & ", " & _
        NFolhaTrab & ", " & BL_TrataStringParaBD(EstadoAna) & ", '" & IIf(gPermResUtil = cValMedRes, gCodUtilizador, "") & "', '" & IIf(gPermResUtil = cValMedRes, Bg_DaData_ADO, "") & "','" & IIf(gPermResUtil = cValMedRes, Bg_DaHora_ADO, "") & "'," & _
        BL_TrataDataParaBD(Bg_DaData_ADO) & ", '" & gCodUtilizador & "', "
    sql = sql & BL_TrataStringParaBD(Bg_DaHora_ADO) & ", " & _
        BL_TrataDataParaBD(DtChega) & ", " & _
        SeqUte & ", '" & IIf(gPermResUtil = cValTecRes, gCodUtilizador, "") & "','" & IIf(gPermResUtil = cValTecRes, Bg_DaData_ADO, "") & "','" & IIf(gPermResUtil = cValTecRes, Bg_DaHora_ADO, "") & "'," & _
        IIf(OrdMarca = "", "NULL", OrdMarca) & ", " & FlgFacturado & ")"
        
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro

    
    InsereRealiza = seq

Exit Function
TrataErro:
    InsereRealiza = -1
    BG_LogFile_Erros "Erro  gravar Resultados : " & Err.Description, Me.Name, "InsereRealiza", True
    Exit Function
    Resume Next
End Function


Function InsereResFrase(seqRealiza As Long, nres As String, CodFrase As String, OrdFrase As String) As Boolean
    Dim sql As String
    On Error GoTo TrataErro
    
    sql = "insert into sl_res_frase (seq_realiza, n_res,cod_frase,ord_frase) values (" & _
            seqRealiza & "," & nres & "," & BL_TrataStringParaBD(CodFrase) & "," & OrdFrase & ")"
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    InsereResFrase = True
    
Exit Function
TrataErro:
    InsereResFrase = False
    BG_LogFile_Erros "Erro  gravar frases : " & Err.Description, Me.Name, "InsereResFrase", True
    Exit Function
    Resume Next
End Function

Function InsereResAlfan(seqRealiza As Long, nres As String, result As String) As Boolean
    Dim sql As String
    
    sql = "INSERT into sl_res_alfan (seq_realiza, n_res,result) VALUES (" & _
            seqRealiza & "," & nres & "," & BL_TrataStringParaBD(result) & ")"
    BG_ExecutaQuery_ADO sql
    BG_Trata_BDErro
    InsereResAlfan = True
Exit Function
TrataErro:
    InsereResAlfan = False
    BG_LogFile_Erros "Erro  gravar alfanum�ricos : " & Err.Description, Me.Name, "InsereResAlfan", True
    Exit Function
    Resume Next
End Function

VERSION 5.00
Begin VB.Form FormMarcAnaTop 
   BackColor       =   &H00FFD0D0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormMarcAnaTop"
   ClientHeight    =   7305
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   12465
   Icon            =   "FormMarcAnaTop.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7305
   ScaleWidth      =   12465
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtSair 
      Height          =   615
      Left            =   11400
      Picture         =   "FormMarcAnaTop.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   61
      ToolTipText     =   "Cancelar"
      Top             =   6600
      Width           =   735
   End
   Begin VB.CommandButton BtConfirmar 
      Height          =   615
      Left            =   10200
      Picture         =   "FormMarcAnaTop.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   60
      ToolTipText     =   "Confirmar"
      Top             =   6600
      Width           =   735
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFC0&
      Caption         =   "59"
      Height          =   735
      Index           =   59
      Left            =   11160
      Style           =   1  'Graphical
      TabIndex        =   59
      Top             =   5520
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFC0&
      Caption         =   "58"
      Height          =   735
      Index           =   58
      Left            =   9960
      Style           =   1  'Graphical
      TabIndex        =   58
      Top             =   5520
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFC0&
      Caption         =   "57"
      Height          =   735
      Index           =   57
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   57
      Top             =   5520
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFC0&
      Caption         =   "56"
      Height          =   735
      Index           =   56
      Left            =   7560
      Style           =   1  'Graphical
      TabIndex        =   56
      Top             =   5520
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFC0&
      Caption         =   "55"
      Height          =   735
      Index           =   55
      Left            =   6360
      Style           =   1  'Graphical
      TabIndex        =   55
      Top             =   5520
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFC0&
      Caption         =   "54"
      Height          =   735
      Index           =   54
      Left            =   5160
      Style           =   1  'Graphical
      TabIndex        =   54
      Top             =   5520
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFC0&
      Caption         =   "53"
      Height          =   735
      Index           =   53
      Left            =   3960
      Style           =   1  'Graphical
      TabIndex        =   53
      Top             =   5520
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFC0&
      Caption         =   "52"
      Height          =   735
      Index           =   52
      Left            =   2760
      Style           =   1  'Graphical
      TabIndex        =   52
      Top             =   5520
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFC0&
      Caption         =   "51"
      Height          =   735
      Index           =   51
      Left            =   1560
      Style           =   1  'Graphical
      TabIndex        =   51
      Top             =   5520
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFC0C0&
      Caption         =   "49"
      Height          =   735
      Index           =   49
      Left            =   11160
      Style           =   1  'Graphical
      TabIndex        =   50
      Top             =   4440
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFC0C0&
      Caption         =   "48"
      Height          =   735
      Index           =   48
      Left            =   9960
      Style           =   1  'Graphical
      TabIndex        =   49
      Top             =   4440
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFC0C0&
      Caption         =   "47"
      Height          =   735
      Index           =   47
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   48
      Top             =   4440
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFC0C0&
      Caption         =   "46"
      Height          =   735
      Index           =   46
      Left            =   7560
      Style           =   1  'Graphical
      TabIndex        =   47
      Top             =   4440
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFC0C0&
      Caption         =   "45"
      Height          =   735
      Index           =   45
      Left            =   6360
      Style           =   1  'Graphical
      TabIndex        =   46
      Top             =   4440
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFC0C0&
      Caption         =   "44"
      Height          =   735
      Index           =   44
      Left            =   5160
      Style           =   1  'Graphical
      TabIndex        =   45
      Top             =   4440
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFC0C0&
      Caption         =   "43"
      Height          =   735
      Index           =   43
      Left            =   3960
      Style           =   1  'Graphical
      TabIndex        =   44
      Top             =   4440
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFC0C0&
      Caption         =   "42"
      Height          =   735
      Index           =   42
      Left            =   2760
      Style           =   1  'Graphical
      TabIndex        =   43
      Top             =   4440
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFC0C0&
      Caption         =   "41"
      Height          =   735
      Index           =   41
      Left            =   1560
      Style           =   1  'Graphical
      TabIndex        =   42
      Top             =   4440
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFCCFF&
      Caption         =   "39"
      Height          =   735
      Index           =   39
      Left            =   11160
      Style           =   1  'Graphical
      TabIndex        =   41
      Top             =   3360
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFCCFF&
      Caption         =   "38"
      Height          =   735
      Index           =   38
      Left            =   9960
      Style           =   1  'Graphical
      TabIndex        =   40
      Top             =   3360
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFCCFF&
      Caption         =   "37"
      Height          =   735
      Index           =   37
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   39
      Top             =   3360
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFCCFF&
      Caption         =   "36"
      Height          =   735
      Index           =   36
      Left            =   7560
      Style           =   1  'Graphical
      TabIndex        =   38
      Top             =   3360
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFCCFF&
      Caption         =   "35"
      Height          =   735
      Index           =   35
      Left            =   6360
      Style           =   1  'Graphical
      TabIndex        =   37
      Top             =   3360
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFCCFF&
      Caption         =   "34"
      Height          =   735
      Index           =   34
      Left            =   5160
      Style           =   1  'Graphical
      TabIndex        =   36
      Top             =   3360
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFCCFF&
      Caption         =   "33"
      Height          =   735
      Index           =   33
      Left            =   3960
      Style           =   1  'Graphical
      TabIndex        =   35
      Top             =   3360
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFCCFF&
      Caption         =   "32"
      Height          =   735
      Index           =   32
      Left            =   2760
      Style           =   1  'Graphical
      TabIndex        =   34
      Top             =   3360
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFCCFF&
      Caption         =   "31"
      Height          =   735
      Index           =   31
      Left            =   1560
      Style           =   1  'Graphical
      TabIndex        =   33
      Top             =   3360
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0C0FF&
      Caption         =   "29"
      Height          =   735
      Index           =   29
      Left            =   11160
      Style           =   1  'Graphical
      TabIndex        =   32
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0C0FF&
      Caption         =   "28"
      Height          =   735
      Index           =   28
      Left            =   9960
      Style           =   1  'Graphical
      TabIndex        =   31
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0C0FF&
      Caption         =   "27"
      Height          =   735
      Index           =   27
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   30
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0C0FF&
      Caption         =   "26"
      Height          =   735
      Index           =   26
      Left            =   7560
      Style           =   1  'Graphical
      TabIndex        =   29
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0C0FF&
      Caption         =   "25"
      Height          =   735
      Index           =   25
      Left            =   6360
      Style           =   1  'Graphical
      TabIndex        =   28
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0C0FF&
      Caption         =   "24"
      Height          =   735
      Index           =   24
      Left            =   5160
      Style           =   1  'Graphical
      TabIndex        =   27
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0C0FF&
      Caption         =   "23"
      Height          =   735
      Index           =   23
      Left            =   3960
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0C0FF&
      Caption         =   "22"
      Height          =   735
      Index           =   22
      Left            =   2760
      Style           =   1  'Graphical
      TabIndex        =   25
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0C0FF&
      Caption         =   "21"
      Height          =   735
      Index           =   21
      Left            =   1560
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0E0FF&
      Caption         =   "19"
      Height          =   735
      Index           =   19
      Left            =   11160
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0E0FF&
      Caption         =   "18"
      Height          =   735
      Index           =   18
      Left            =   9960
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0E0FF&
      Caption         =   "17"
      Height          =   735
      Index           =   17
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0E0FF&
      Caption         =   "16"
      Height          =   735
      Index           =   16
      Left            =   7560
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0E0FF&
      Caption         =   "15"
      Height          =   735
      Index           =   15
      Left            =   6360
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0E0FF&
      Caption         =   "14"
      Height          =   735
      Index           =   14
      Left            =   5160
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0E0FF&
      Caption         =   "13"
      Height          =   735
      Index           =   13
      Left            =   3960
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0E0FF&
      Caption         =   "12"
      Height          =   735
      Index           =   12
      Left            =   2760
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0E0FF&
      Caption         =   "11"
      Height          =   735
      Index           =   11
      Left            =   1560
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFFF&
      Caption         =   "9"
      Height          =   735
      Index           =   9
      Left            =   11160
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFFF&
      Caption         =   "8"
      Height          =   735
      Index           =   8
      Left            =   9960
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFFF&
      Caption         =   "7"
      Height          =   735
      Index           =   7
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFFF&
      Caption         =   "6"
      Height          =   735
      Index           =   6
      Left            =   7560
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFFF&
      Caption         =   "5"
      Height          =   735
      Index           =   5
      Left            =   6360
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFFF&
      Caption         =   "4"
      Height          =   735
      Index           =   4
      Left            =   5160
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFFF&
      Caption         =   "3"
      Height          =   735
      Index           =   3
      Left            =   3960
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFFF&
      Caption         =   "2"
      Height          =   735
      Index           =   2
      Left            =   2760
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFFF&
      Caption         =   "1"
      Height          =   735
      Index           =   1
      Left            =   1560
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFC0&
      Caption         =   "50"
      Height          =   735
      Index           =   50
      Left            =   360
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   5520
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFC0C0&
      Caption         =   "40"
      Height          =   735
      Index           =   40
      Left            =   360
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   4440
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00FFCCFF&
      Caption         =   "30"
      Height          =   735
      Index           =   30
      Left            =   360
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3360
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0C0FF&
      Caption         =   "20"
      Height          =   735
      Index           =   20
      Left            =   360
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0E0FF&
      Caption         =   "10"
      Height          =   735
      Index           =   10
      Left            =   360
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton BtAna 
      BackColor       =   &H00C0FFFF&
      Caption         =   "0"
      Height          =   735
      Index           =   0
      Left            =   360
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   120
      Width           =   975
   End
End
Attribute VB_Name = "FormMarcAnaTop"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

' Actualiza��o : 25/01/2002
' T�cnico Sandra Oliveira

' Vari�veis Globais para este Form.

Dim NumCampos As Integer


Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object


Const Cor_0_normal = &HC0FFFF
Const cor_1_normal = &HC0E0FF
Const cor_2_normal = &HC0C0FF
Const cor_3_normal = &HFFCCFF
Const cor_4_normal = &HFFC0C0
Const cor_5_normal = &HC0FFC0

Private Type analises
    codAna As String
    descrAna As String
    abrevAna As String
    ordem As Integer
    estado As Integer
    indice As Integer
End Type
Dim estrutAnalises(59) As analises
Dim totalAnalises As Integer
Private CamposRetorno As ClassPesqResultados
Dim indiceActual As Integer

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    PreencheValoresDefeito
    
    
        
    BL_FimProcessamento Me
    
End Sub

Sub FuncaoProcurar()

End Sub

Sub PreencheCampos()

End Sub
 
Sub FuncaoLimpar()
        
        
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
    End If
    
End Sub

Sub LimpaCampos()

End Sub

Sub FuncaoEstadoAnterior()
    

End Sub

Sub PreencheValoresDefeito()
    Dim i As Integer
    Dim sSql As String
    Dim rsTop As New ADODB.recordset
    indiceActual = 1
    For i = 0 To 59
        BtAna(i).Visible = False
        BtAna(i).FontBold = False
        BtAna(i).caption = i
    Next
    sSql = "SELECT x1.cod_analise, x1.ordem, x2.descr_ana, x2.abr_ana FROM Sl_ana_top X1, slv_analises x2 "
    sSql = sSql & " WHERE x1.cod_analise = x2.cod_ana ORDER by ordem "
    rsTop.CursorLocation = adUseServer
    rsTop.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTop.Open sSql, gConexao
    If rsTop.RecordCount >= 1 Then
        While Not rsTop.EOF
            If BL_HandleNull(rsTop!ordem, -1) >= 0 And BL_HandleNull(rsTop!ordem, -1) < 60 Then
                estrutAnalises(rsTop!ordem).codAna = BL_HandleNull(rsTop!cod_analise, "")
                estrutAnalises(rsTop!ordem).abrevAna = BL_HandleNull(rsTop!abr_ana, "")
                estrutAnalises(rsTop!ordem).descrAna = BL_HandleNull(rsTop!descr_ana, "")
                estrutAnalises(rsTop!ordem).ordem = BL_HandleNull(rsTop!ordem, -1)
                estrutAnalises(rsTop!ordem).estado = 0
                estrutAnalises(rsTop!ordem).indice = -1
                BtAna(rsTop!ordem).caption = estrutAnalises(rsTop!ordem).abrevAna
                BtAna(rsTop!ordem).Visible = True
            End If
            rsTop.MoveNext
        Wend
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises codificadas.", vbOKOnly + vbCritical, "An�lises Mais Pedidas"
    End If
    
End Sub

Sub EventoUnload()
    
    If gF_REQUIS_PRIVADO = 1 Then
        BG_StackJanelas_Pop
        Set gFormActivo = FormGestaoRequisicaoPrivado
        'BL_ToolbarEstadoN 0
    ElseIf gF_REQUIS = 1 Then
        BG_StackJanelas_Pop
        Set gFormActivo = FormGestaoRequisicao
    End If
    'BL_ToolbarEstadoN 0
    Set FormMarcAnaTop = Nothing
End Sub

Sub FuncaoAnterior()

    
End Sub

Sub DefTipoCampos()
    
End Sub


Private Sub BtAna_Click(Index As Integer)
    Dim i As Integer
    If estrutAnalises(Index).codAna <> "" Then
        If estrutAnalises(Index).estado = 0 Then
            estrutAnalises(Index).estado = 1
            BtAna(Index).FontBold = True
            estrutAnalises(Index).indice = indiceActual
            indiceActual = indiceActual + 1
        Else
            If estrutAnalises(Index).estado = 1 Then
                For i = 1 To totalAnalises
                    If estrutAnalises(i).indice > estrutAnalises(Index).indice Then
                        estrutAnalises(i).indice = estrutAnalises(i).indice - 1
                    End If
                Next
                estrutAnalises(Index).indice = -1
                indiceActual = indiceActual - 1
            End If
            estrutAnalises(Index).estado = 0
            BtAna(Index).FontBold = False
        End If
    End If
End Sub

Private Sub BtConfirmar_Click()
    Dim i As Integer
    Dim j As Integer
    Dim numPosVector As Integer
    For j = 1 To indiceActual
        For i = 0 To 59
            If estrutAnalises(i).estado = 1 And estrutAnalises(i).indice = j Then
                CamposRetorno.InicializaResultados numPosVector
                CamposRetorno.ResPesquisaLet numPosVector, estrutAnalises(i).codAna
                numPosVector = numPosVector + 1
            End If
        Next i
    Next
    DoEvents
    Unload Me
End Sub

Private Sub BtSair_Click()
    Unload Me
    
End Sub

Private Sub Form_Activate()

    EventoActivate
    
End Sub

Sub EventoActivate()


End Sub

Sub FuncaoModificar()
End Sub

Sub BD_Update()

End Sub

Sub FuncaoInserir()

End Sub

Function ValidaCamposEc() As Integer
End Function

Sub BD_Insert()

End Sub

Private Sub Form_Load()
    
    EventoLoad

End Sub

Public Sub Inicializacoes()

    Me.caption = "Marca��o R�pida"
    Me.left = 1000
    Me.top = 1000
    Me.Width = 12555
    Me.Height = 7695 '+ 2000
         
End Sub

Sub FuncaoRemover()
    
End Sub

Sub FuncaoSeguinte()

    
End Sub

Sub BD_Delete()
    
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Public Sub Inicializa(TCamposRetorno As ClassPesqResultados)
    Me.caption = "A"
    Set CamposRetorno = TCamposRetorno
    TCamposRetorno.PesquisaCancelada True
    Inicializacoes
End Sub

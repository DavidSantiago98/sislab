VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormSerGestaoCaixas 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormSerGestaoCaixas"
   ClientHeight    =   29100
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   15135
   Icon            =   "FormSerGestaoCaixas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   29100
   ScaleWidth      =   15135
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FramePendentes 
      Caption         =   "An�lises para arquivar"
      Height          =   9015
      Left            =   0
      TabIndex        =   41
      Top             =   10800
      Width           =   15015
      Begin VB.CommandButton BtRefresh 
         Height          =   615
         Left            =   12600
         Picture         =   "FormSerGestaoCaixas.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   51
         Top             =   8280
         Width           =   615
      End
      Begin VB.CommandButton BtPesquisaCaixa2 
         Height          =   315
         Left            =   12360
         Picture         =   "FormSerGestaoCaixas.frx":0CD6
         Style           =   1  'Graphical
         TabIndex        =   49
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Suportes"
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox EcDescrCaixa2 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   9240
         TabIndex        =   48
         Top             =   240
         Width           =   3015
      End
      Begin VB.TextBox EcCodCaixa2 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8520
         TabIndex        =   47
         Top             =   240
         Width           =   735
      End
      Begin MSComCtl2.DTPicker EcDtIni 
         Height          =   255
         Left            =   2160
         TabIndex        =   45
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   450
         _Version        =   393216
         Format          =   162529281
         CurrentDate     =   41065
      End
      Begin VB.CommandButton BtArquivarAutomatico 
         Height          =   615
         Left            =   13320
         Picture         =   "FormSerGestaoCaixas.frx":1060
         Style           =   1  'Graphical
         TabIndex        =   44
         ToolTipText     =   "Arquivar Requisi��es Seleccionadas"
         Top             =   8280
         Width           =   615
      End
      Begin MSFlexGridLib.MSFlexGrid FgAnaPendentes 
         Height          =   7575
         Left            =   240
         TabIndex        =   43
         Top             =   600
         Width           =   14655
         _ExtentX        =   25850
         _ExtentY        =   13361
         _Version        =   393216
         Rows            =   18
         Cols            =   15
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   400
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
      End
      Begin VB.CommandButton BtSairPendentes 
         Height          =   615
         Left            =   14040
         Picture         =   "FormSerGestaoCaixas.frx":1D2A
         Style           =   1  'Graphical
         TabIndex        =   42
         ToolTipText     =   "Sair Sem Gravar"
         Top             =   8280
         Width           =   615
      End
      Begin MSComCtl2.DTPicker EcDtFim 
         Height          =   255
         Left            =   4680
         TabIndex        =   46
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   450
         _Version        =   393216
         Format          =   162529281
         CurrentDate     =   41065
      End
      Begin VB.Label Label1 
         Caption         =   "Suporte"
         Height          =   255
         Index           =   12
         Left            =   7560
         TabIndex        =   50
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.ListBox EcListaEliminados 
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      Height          =   1395
      Left            =   13320
      TabIndex        =   66
      Top             =   6720
      Width           =   1695
   End
   Begin VB.Frame Frame5 
      Caption         =   "Pesquisa"
      Height          =   1815
      Left            =   120
      TabIndex        =   55
      Top             =   0
      Width           =   5295
      Begin VB.CommandButton BtPesquisaCaixa 
         Height          =   315
         Left            =   4800
         Picture         =   "FormSerGestaoCaixas.frx":29F4
         Style           =   1  'Graphical
         TabIndex        =   60
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Suportes"
         Top             =   840
         Width           =   375
      End
      Begin VB.TextBox EcDescrCaixa 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1800
         TabIndex        =   59
         Top             =   840
         Width           =   3015
      End
      Begin VB.ComboBox CbSeroteca 
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   58
         Top             =   360
         Width           =   4095
      End
      Begin VB.TextBox EcCodCaixa 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   57
         Top             =   840
         Width           =   735
      End
      Begin VB.TextBox EcReqPesq 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   56
         Top             =   1200
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Suporte"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   63
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Arquivo"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   62
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Requisi��o"
         Height          =   255
         Index           =   11
         Left            =   240
         TabIndex        =   61
         Top             =   1200
         Width           =   855
      End
   End
   Begin VB.Frame Frame4 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Adicionar"
      Height          =   1335
      Left            =   5520
      TabIndex        =   33
      Top             =   5400
      Width           =   9495
      Begin VB.CommandButton BtGravarObs 
         Height          =   495
         Left            =   9000
         Picture         =   "FormSerGestaoCaixas.frx":2D7E
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   720
         Width           =   375
      End
      Begin VB.TextBox EcObservacao 
         Appearance      =   0  'Flat
         Height          =   495
         Left            =   1080
         MultiLine       =   -1  'True
         TabIndex        =   39
         Top             =   720
         Width           =   7935
      End
      Begin VB.CommandButton BtAdicionar 
         Height          =   375
         Left            =   2100
         Picture         =   "FormSerGestaoCaixas.frx":3108
         Style           =   1  'Graphical
         TabIndex        =   35
         ToolTipText     =   "Anexar Posi��o Disponivel"
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox EcNumReq 
         Appearance      =   0  'Flat
         DragMode        =   1  'Automatic
         Height          =   285
         Left            =   1080
         TabIndex        =   34
         Top             =   285
         Width           =   975
      End
      Begin VB.Label Label1 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Observa��o"
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   37
         Top             =   765
         Width           =   975
      End
      Begin VB.Label Label1 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Requisi��o"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   36
         Top             =   315
         Width           =   855
      End
   End
   Begin VB.Frame Frame3 
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   855
      Left            =   5640
      TabIndex        =   13
      Top             =   8160
      Width           =   9495
      Begin VB.Label Label1 
         BackColor       =   &H00FFC0C0&
         Height          =   255
         Index           =   5
         Left            =   5280
         TabIndex        =   31
         Top             =   120
         Width           =   255
      End
      Begin VB.Label LbImpressa 
         Caption         =   "Congelada"
         Height          =   255
         Left            =   5640
         TabIndex        =   30
         Top             =   120
         Width           =   855
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0FFFF&
         Height          =   255
         Index           =   20
         Left            =   6600
         TabIndex        =   29
         Top             =   120
         Width           =   255
      End
      Begin VB.Label Label5 
         Caption         =   "Descongelada"
         Height          =   255
         Index           =   0
         Left            =   6960
         TabIndex        =   28
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label1 
         BackColor       =   &H008080FF&
         Height          =   255
         Index           =   21
         Left            =   8520
         TabIndex        =   27
         Top             =   120
         Width           =   255
      End
      Begin VB.Label Label5 
         Caption         =   "Lixo"
         Height          =   255
         Index           =   1
         Left            =   8880
         TabIndex        =   26
         Top             =   120
         Width           =   495
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0C0C0&
         Height          =   255
         Index           =   3
         Left            =   2160
         TabIndex        =   25
         Top             =   120
         Width           =   255
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0FFC0&
         Height          =   255
         Index           =   4
         Left            =   3840
         TabIndex        =   24
         Top             =   120
         Width           =   255
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   2
         Left            =   0
         TabIndex        =   23
         Top             =   120
         Width           =   255
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0E0FF&
         Height          =   255
         Index           =   7
         Left            =   0
         TabIndex        =   22
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFC0&
         Height          =   255
         Index           =   8
         Left            =   2160
         TabIndex        =   21
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FF80FF&
         Height          =   255
         Index           =   9
         Left            =   3840
         TabIndex        =   20
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Label7 
         Caption         =   "N�o Disponivel"
         Height          =   255
         Left            =   2520
         TabIndex        =   19
         Top             =   120
         Width           =   1335
      End
      Begin VB.Label LbValidacao 
         Caption         =   "Atriu�da"
         Height          =   255
         Left            =   4200
         TabIndex        =   18
         Top             =   120
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Posi��o Livre"
         Height          =   255
         Left            =   360
         TabIndex        =   17
         Top             =   120
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "Sem An�lises Pendentes"
         Height          =   255
         Left            =   360
         TabIndex        =   16
         Top             =   480
         Width           =   1935
      End
      Begin VB.Label Label4 
         Caption         =   "Data Ultrapassada"
         Height          =   255
         Left            =   2520
         TabIndex        =   15
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label Label6 
         Caption         =   "Data Pr�xima"
         Height          =   255
         Left            =   4200
         TabIndex        =   14
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H80000018&
      BorderStyle     =   0  'None
      Height          =   1455
      Left            =   5520
      TabIndex        =   2
      Top             =   6720
      Width           =   7695
      Begin VB.Label LbObservacao 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000010&
         Height          =   495
         Left            =   0
         TabIndex        =   38
         Top             =   960
         Width           =   7335
      End
      Begin VB.Label LbFrase 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000010&
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   720
         Width           =   7335
      End
      Begin VB.Label LbNome 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000010&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   7335
      End
      Begin VB.Label LbSexoUtente 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000010&
         Height          =   255
         Left            =   6840
         TabIndex        =   6
         Top             =   0
         Width           =   1455
      End
      Begin VB.Label LbEstadoReq 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000010&
         Height          =   255
         Left            =   4440
         TabIndex        =   5
         Top             =   0
         Width           =   2535
      End
      Begin VB.Label LbDtChega 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000010&
         Height          =   255
         Left            =   3240
         TabIndex        =   4
         Top             =   0
         Width           =   975
      End
      Begin VB.Label LbNReq 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000010&
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   0
         Width           =   5055
      End
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   3960
      Picture         =   "FormSerGestaoCaixas.frx":3872
      ScaleHeight     =   375
      ScaleWidth      =   495
      TabIndex        =   1
      Top             =   9480
      Visible         =   0   'False
      Width           =   495
   End
   Begin MSFlexGridLib.MSFlexGrid FgCaixa 
      Height          =   5295
      Left            =   5640
      TabIndex        =   0
      Top             =   120
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   9340
      _Version        =   393216
      Rows            =   10
      Cols            =   10
      FixedRows       =   0
      FixedCols       =   0
      BackColorBkg    =   -2147483644
      ScrollBars      =   0
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList IL 
      Left            =   120
      Top             =   195
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormSerGestaoCaixas.frx":3FDC
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormSerGestaoCaixas.frx":4756
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormSerGestaoCaixas.frx":4AF0
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormSerGestaoCaixas.frx":4E8A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TV 
      Height          =   5895
      Left            =   120
      TabIndex        =   64
      Top             =   1920
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   10398
      _Version        =   393217
      Style           =   7
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin VB.Frame Frame2 
      Height          =   1215
      Left            =   120
      TabIndex        =   9
      Top             =   7800
      Width           =   5295
      Begin MSComCtl2.DTPicker EcDtImp 
         Height          =   255
         Left            =   3960
         TabIndex        =   54
         Top             =   880
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   450
         _Version        =   393216
         Format          =   162594817
         CurrentDate     =   41736
      End
      Begin VB.CommandButton BtImprimirSuporte 
         Height          =   495
         Left            =   3360
         Picture         =   "FormSerGestaoCaixas.frx":5B64
         Style           =   1  'Graphical
         TabIndex        =   53
         ToolTipText     =   "Imprimir Suporte"
         Top             =   650
         Width           =   495
      End
      Begin VB.CommandButton BtAnaPendentes 
         Height          =   495
         Left            =   1320
         Picture         =   "FormSerGestaoCaixas.frx":682E
         Style           =   1  'Graphical
         TabIndex        =   40
         ToolTipText     =   "An�lises para arquivar"
         Top             =   650
         Width           =   495
      End
      Begin VB.CommandButton BtNovaReutil 
         Height          =   495
         Left            =   120
         Picture         =   "FormSerGestaoCaixas.frx":74F8
         Style           =   1  'Graphical
         TabIndex        =   32
         ToolTipText     =   "Reutilizar Suporte"
         Top             =   120
         Width           =   495
      End
      Begin VB.CommandButton BtCongelar 
         Height          =   495
         Left            =   120
         Picture         =   "FormSerGestaoCaixas.frx":81C2
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Congelar"
         Top             =   650
         Width           =   495
      End
      Begin VB.CommandButton BtDescongelar 
         Height          =   495
         Left            =   720
         Picture         =   "FormSerGestaoCaixas.frx":8E8C
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Descongelar"
         Top             =   650
         Width           =   495
      End
      Begin VB.CommandButton BtEnviarLixo 
         Height          =   495
         Left            =   720
         Picture         =   "FormSerGestaoCaixas.frx":9B56
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Enviar para Lixo"
         Top             =   120
         Width           =   495
      End
      Begin VB.CheckBox CkImpData 
         Caption         =   "Apenas Data:"
         Height          =   255
         Left            =   3960
         TabIndex        =   65
         Top             =   650
         Width           =   1305
      End
   End
End
Attribute VB_Name = "FormSerGestaoCaixas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Private Type arquivo
    seq_arquivo As Long
    n_req As Long
    cod_tubo As String
    cod_etiq As String
    aliquota As String
    pos_x As Integer
    pos_y As Integer
    posicao As Integer
    estado As Integer
    user_cri As String
    dt_cri As String
    hr_cri As String
    user_act As String
    dt_act As String
    hr_act As String
    nome_ute As String
    sexo_ute As String
    dt_chega As String
    estado_req As String
    obs As String
    flg_negrito As Boolean
    
    dt_descongelamento As String
    hr_descongelamento As String
    user_descongelamento As String
    
    dt_lixo As String
    hr_lixo As String
    user_lixo As String
    
    dt_congelamento As String
    hr_congelamento As String
    user_congelamento As String
    
    dt_anexa As String
    hr_anexa As String
    user_anexa As String
    
    congelada As Boolean
    descongelada As Boolean
    lixo As Boolean
    semAna As Boolean
    dataUltrapassada As Boolean
    dataProxima As Boolean
    frase As String
End Type

Private Type reutilizacao
    seq_reutil As Long
    dt_cri As String
    hr_cri As String
    user_cri As String
    flg_activo As Integer
    dt_act As String
    hr_act As String
    user_act As String
    estrutArquivo() As arquivo
    totalArquivos As Integer
End Type

Private Type Caixa
    cod_caixa As Long
    descr_caixa As String
    cod_seroteca As Long
    tamanho As Integer
    horiz As Integer
    vert As Integer
    livre As Integer
    dt_cri As String
    hr_cri As String
    dt_act As String
    hr_act As String
    prazo_cong As Integer
    prazo_aviso As Integer
    reutil() As reutilizacao
    totalReutil As Integer
End Type
Dim estrutCaixas() As Caixa
Dim totalCaixas As Long

Private Type anaPendente
    cod_ana As String
    descr_ana As String
End Type

Private Type ReqPendente
    n_req As String
    linha As Integer
    coluna As Integer
    toolTip As String
    analises() As anaPendente
    totalAnalises As Integer
    numero As Integer
End Type
Dim estrutPendentes() As ReqPendente
Dim totalEstrutPEnd As Integer

Public rs As ADODB.recordset
Const lCorAtribuida = &HC0FFC0
Const lCorNaoDisponivel = &HC0C0C0
Const lCorCongelada = &HFFC0C0
Const lCorDescongelada = &HC0FFFF
Const lCorLixo = &H8080FF
Const lCorSemAna = &HC0E0FF
Const lCorDataUltrapassada = &HFFFFC0
Const lcorDataProxima = &HFF80FF

Dim indiceCaixa As Integer
Dim indiceReutil As Integer

Dim req_pesquisa As String
Dim tubo_pesquisa As String
Dim aliq_pesquisa As String

Const keyCaixa = "KeyC"
Const keyReutil = "KeyR"
Const keyArq = "KeyA"
Const keyDesac = "KeyD"

Const linhasPend = 18
Const colunasPend = 15

Dim linhaToolTip As Integer
Dim colunaTooltip As Integer
Const vermelhoClaro = &HBDC8F4
Const VerdeClaro = &HC0FFC0

' Estrutura para Guardar dados para imprimir resumo do arquivamento
Private Type ArquivoImpressao
    seqArquivo As Long
End Type
Dim estrutArquivoImpr() As ArquivoImpressao
Dim totalArquivoImpr As Integer
Dim seqArquivoAux As Long

Private Sub BtAdicionar_Click()
    Dim req_aux As String
    Dim tubo_aux As String
    Dim aliquota_aux As String
    If EcNumReq <> "" Then
        If Len(EcNumReq) <= 7 And InStr(1, EcNumReq, ".") = 0 Then
            req_aux = EcNumReq
            tubo_aux = ""
            aliquota_aux = ""
        ElseIf InStr(1, EcNumReq, ".") > 0 Then
            If Len(Mid(EcNumReq, 1, InStr(1, EcNumReq, ".") - 1)) <= 7 Then
                req_aux = Mid(EcNumReq, 1, InStr(1, EcNumReq, ".") - 1)
                aliquota_aux = Mid(EcNumReq, InStr(1, EcNumReq, ".") + 1)
                tubo_aux = ""
            ElseIf Len(Mid(EcNumReq, 1, InStr(1, EcNumReq, ".") - 1)) = 9 Then
                req_aux = Mid(EcNumReq, 1, InStr(1, EcNumReq, ".") - 1)
                aliquota_aux = Mid(EcNumReq, InStr(1, EcNumReq, ".") + 1)
                tubo_aux = Mid(req_aux, 1, 2)
                req_aux = CStr(CLng(Mid(req_aux, 3)))
            End If
        ElseIf Len(EcNumReq) = 9 And InStr(1, EcNumReq, ".") = 0 Then
            req_aux = Mid(EcNumReq, 3)
            tubo_aux = Mid(EcNumReq, 1, 2)
            aliquota_aux = ""
        End If
        GuardaReqPosicaoDisp req_aux, indiceCaixa, indiceReutil, tubo_aux, aliquota_aux
    End If
    Tv_Click
    EcNumReq.Text = ""
    EcNumReq.SetFocus
End Sub

Private Sub BtAnaPendentes_Click()
    If indiceCaixa > totalCaixas Then
        Exit Sub
    End If
    FramePendentes.Visible = True
    FramePendentes.top = 120
    FramePendentes.left = 120
    EcCodCaixa2 = estrutCaixas(indiceCaixa).cod_caixa
    EcCodCaixa2_Change
    FuncaoProcurarPendentes CStr(estrutCaixas(indiceCaixa).cod_caixa)
    EcDtIni.value = Bg_DaData_ADO - 10
    EcDtFim.value = Bg_DaData_ADO
End Sub

Private Sub BtArquivarAutomatico_Click()
    Dim i As Integer
    Dim j As Integer
    Dim seqArquivoAtribuido As Long
    Dim flgImpr As Boolean
    
    totalArquivoImpr = 0
    ReDim estrutArquivoImpr(totalArquivoImpr)
    
    flgImpr = False
    If BG_Mensagem(mediMsgBox, "Deseja imprimir Resumo?", vbYesNo + vbQuestion, "Arquivamento") = vbYes Then
         flgImpr = True
    End If
    
    If indiceCaixa > 0 Then
        For i = 1 To totalEstrutPEnd
            For j = 1 To estrutPendentes(i).numero
                seqArquivoAtribuido = GuardaReqPosicaoDisp(estrutPendentes(i).n_req, indiceCaixa, estrutCaixas(indiceCaixa).totalReutil, "", "")
                If seqArquivoAtribuido = mediComboValorNull Then
                    BtNovaReutil_Click
                    seqArquivoAtribuido = GuardaReqPosicaoDisp(estrutPendentes(i).n_req, indiceCaixa, estrutCaixas(indiceCaixa).totalReutil, "", "")
                End If
                If flgImpr = True Then
                    totalArquivoImpr = totalArquivoImpr + 1
                    ReDim Preserve estrutArquivoImpr(totalArquivoImpr)
                    estrutArquivoImpr(totalArquivoImpr).seqArquivo = seqArquivoAtribuido
                End If
            Next j
        Next i
    End If
    If totalArquivoImpr > 0 And flgImpr = True Then
        ImprimeResumoArquivo
    End If
    BtSairPendentes_Click
End Sub

Private Sub BtCongelar_Click()
    Dim i As Integer
    gMsgTitulo = "Congelar"
    gMsgMsg = "Tem a certeza que quer Congelar todos os tubos?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If gMsgResp = vbYes Then
        For i = 1 To estrutCaixas(indiceCaixa).reutil(indiceReutil).totalArquivos
        If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaAtribuido Then
            estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaCongelado
            FgCaixa.row = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_y
            FgCaixa.Col = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_x
            FgCaixa.CellBackColor = lCorCongelada
            GravaArquivo indiceCaixa, indiceReutil, CLng(i)
        End If
     Next
    End If
End Sub

Private Sub BtDescongelar_Click()
    Dim i As Integer
    gMsgTitulo = "Descongelar"
    gMsgMsg = "Tem a certeza que quer Descongelar todos os tubos?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If gMsgResp = vbYes Then
        For i = 1 To estrutCaixas(indiceCaixa).reutil(indiceReutil).totalArquivos
        If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaCongelado Then
            estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaDescongelado
            FgCaixa.row = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_y
            FgCaixa.Col = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_x
            FgCaixa.CellBackColor = lCorDescongelada
            GravaArquivo indiceCaixa, indiceReutil, CLng(i)
        End If
     Next
    End If

End Sub



Private Sub BtEnviarLixo_Click()
    Dim i As Integer
    gMsgTitulo = "Enviar Para Lixo"
    gMsgMsg = "Tem a certeza que quer enviar para lixo todos os tubos?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    If gMsgResp = vbYes Then
        For i = 1 To estrutCaixas(indiceCaixa).reutil(indiceReutil).totalArquivos
        If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado <> gEstadoSerotecaLixo Then
            estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaLixo
            FgCaixa.row = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_y
            FgCaixa.Col = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_x
            FgCaixa.CellBackColor = lCorLixo
            GravaArquivo CLng(indiceCaixa), indiceReutil, CLng(i)
        End If
     Next
    End If


End Sub

Private Sub BtImprimirSuporte_Click()
    Dim i As Integer
    If indiceCaixa > 0 And indiceCaixa <= totalCaixas Then
        If indiceReutil > 0 And indiceReutil <= estrutCaixas(indiceCaixa).totalReutil Then
            gMsgTitulo = "Imprimir"
            gMsgMsg = "Tem a certeza que quer imprimir suporte em causa?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp = vbYes Then
                totalArquivoImpr = 0
                ReDim estrutArquivoImpr(totalArquivoImpr)
                For i = 1 To estrutCaixas(indiceCaixa).reutil(indiceReutil).totalArquivos
                    If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).dt_cri = EcDtImp.value Or CkImpData.value <> vbChecked Then
                        totalArquivoImpr = totalArquivoImpr + 1
                        ReDim Preserve estrutArquivoImpr(totalArquivoImpr)
                        estrutArquivoImpr(totalArquivoImpr).seqArquivo = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).seq_arquivo
                    End If
                Next
                If estrutCaixas(indiceCaixa).reutil(indiceReutil).totalArquivos > 0 Then
                    ImprimeResumoArquivo
                End If
            End If
        End If
    End If
End Sub

Private Sub BtGravarObs_Click()
    Dim sSql As String
    Dim i As Integer
    If seqArquivoAux > mediComboValorNull Then
        For i = 1 To estrutCaixas(indiceCaixa).reutil(indiceReutil).totalArquivos
            If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).seq_arquivo = seqArquivoAux Then
                estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).obs = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).obs & EcObservacao.Text & vbCrLf
                sSql = "UPDATE sl_ser_arquivo SET obs = " & BL_TrataStringParaBD(estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).obs) & " WHERE seq_arquivo = " & seqArquivoAux
                BG_ExecutaQuery_ADO sSql
            End If
        Next i
        EcObservacao = ""
        FgCaixa_Click
    End If
End Sub



Private Sub BtNovaReutil_Click()
    Dim i As Integer
    Dim sSql As String
    
    On Error GoTo TrataErro
    
    If indiceCaixa > mediComboValorNull Then
        gMsgTitulo = "Congelar"
        gMsgMsg = "Tem a certeza que quer reutilizar suporte?"
        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
        If gMsgResp = vbYes Then
            BG_BeginTransaction
            For i = 1 To estrutCaixas(indiceCaixa).totalReutil
                If estrutCaixas(indiceCaixa).reutil(i).flg_activo = 1 Then
                    If estrutCaixas(indiceCaixa).reutil(i).totalArquivos = 0 Then
                    'CHMA-1389
'                        BG_Mensagem mediMsgBox, "O suporte est� vazio!", vbOKOnly + vbExclamation, App.ProductName
'                        Exit Sub
                        gMsgTitulo = App.ProductName
                        gMsgMsg = "O suporte est� vazio! Quer alterar a data?"
                        gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                        If gMsgResp <> vbYes Then
                            Exit Sub
                        Else
                            sSql = "UPDATE sl_ser_reutil_caixa SET dt_cri = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", hr_cri = " & BL_TrataStringParaBD(Bg_DaHora_ADO)
                            sSql = sSql & ", user_cri = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "  where seq_reutil = " & estrutCaixas(indiceCaixa).reutil(i).seq_reutil
                            BG_ExecutaQuery_ADO sSql
                            BG_CommitTransaction
                            PreenhceTV
                            Exit Sub
                        End If
                    End If
                    estrutCaixas(indiceCaixa).reutil(i).flg_activo = 0
                    sSql = "UPDATE sl_ser_reutil_caixa SET flg_activo = 0, dt_act = " & BL_TrataDataParaBD(Bg_DaData_ADO) & ", hr_act = " & BL_TrataStringParaBD(Bg_DaHora_ADO)
                    sSql = sSql & ", user_Act = " & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "  where seq_reutil = " & estrutCaixas(indiceCaixa).reutil(i).seq_reutil
                    BG_ExecutaQuery_ADO sSql
                End If
            Next
            
            estrutCaixas(indiceCaixa).totalReutil = estrutCaixas(indiceCaixa).totalReutil + 1
            ReDim Preserve estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil)
            estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil).dt_cri = Bg_DaData_ADO
            estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil).user_cri = CStr(gCodUtilizador)
            estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil).flg_activo = 1
            estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil).hr_cri = Bg_DaHora_ADO
            estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil).seq_reutil = SER_DevolveIndice("REUTIL", estrutCaixas(indiceCaixa).cod_caixa, CLng(estrutCaixas(indiceCaixa).cod_seroteca))
    
            
            estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil).totalArquivos = 0
            ReDim estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil).estrutArquivo(0)
            
            sSql = "INSERT INTO sl_ser_reutil_caixa (cod_seroteca, cod_caixa, seq_reutil, dt_cri, hr_cri, user_cri,flg_activo) VALUES("
            sSql = sSql & estrutCaixas(indiceCaixa).cod_seroteca & ","
            sSql = sSql & estrutCaixas(indiceCaixa).cod_caixa & ","
            sSql = sSql & estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil).seq_reutil & ","
            sSql = sSql & BL_TrataDataParaBD(estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil).dt_cri) & ","
            sSql = sSql & BL_TrataStringParaBD(estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil).hr_cri) & ","
            sSql = sSql & BL_TrataStringParaBD(estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil).user_cri) & ","
            sSql = sSql & estrutCaixas(indiceCaixa).reutil(estrutCaixas(indiceCaixa).totalReutil).flg_activo & ")"
            BG_ExecutaQuery_ADO sSql
            PreenhceTV
            BG_CommitTransaction
        End If
    End If
Exit Sub
TrataErro:
    BG_RollbackTransaction
    BG_LogFile_Erros "BtNovaReutil_Click: " & Err.Description, Me.Name, "BtNovaReutil_Click", False
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaCaixa2_Click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_caixa"
    CamposEcran(1) = "cod_caixa"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_caixa"
    CamposEcran(2) = "descr_caixa"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_ser_caixas"
    CampoPesquisa1 = "descr_caixa"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Suportes Seroteca")
    
    mensagem = "N�o foi encontrada nenhuma Suporte."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodCaixa2.Text = resultados(1)
            EcDescrCaixa2.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
End Sub

Private Sub BtRefresh_Click()
    FuncaoProcurarPendentes EcCodCaixa2
End Sub

Private Sub BtSairPendentes_Click()
    FramePendentes.Visible = False
    
End Sub


Private Sub EcCodCaixa2_Change()
    Dim Cancel As Boolean
    Dim RsDescrCaixa As ADODB.recordset
    
    'CANCEL = Not BG_ValidaTipoCampo_ADO(Me, EcCodCaixa2)
    If Cancel = True Then
       ' Exit Sub
    End If
    
    If Trim(EcCodCaixa2.Text) <> "" Then
        Set RsDescrCaixa = New ADODB.recordset
        
        With RsDescrCaixa
            .Source = "SELECT cod_caixa, descr_caixa FROM sl_ser_caixas WHERE cod_caixa= " & EcCodCaixa.Text
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrCaixa.RecordCount > 0 Then
            EcCodCaixa2.Text = "" & RsDescrCaixa!cod_caixa
            EcDescrCaixa2.Text = "" & RsDescrCaixa!descr_caixa
        Else
            Cancel = True
            EcDescrCaixa2.Text = ""
            BG_Mensagem mediMsgBox, "A caixa indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrCaixa.Close
        Set RsDescrCaixa = Nothing
    Else
        EcDescrCaixa2.Text = ""
    End If

End Sub

Private Sub EcNumReq_DragDrop(Source As Control, X As Single, Y As Single)
    EcNumReq.SetFocus
End Sub

Private Sub EcNumReq_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        BtAdicionar_Click
    End If
End Sub





Private Sub FgAnaPendentes_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim i As Integer
    
    If FgAnaPendentes.Text <> "" Then
        For i = 1 To totalEstrutPEnd
            If estrutPendentes(i).linha = FgAnaPendentes.row And estrutPendentes(i).coluna = FgAnaPendentes.Col Then
                If Button = 1 Then
                    estrutPendentes(i).numero = estrutPendentes(i).numero + 1
                Else
                    estrutPendentes(i).numero = estrutPendentes(i).numero - 1
                    If estrutPendentes(i).numero <= 0 Then
                        estrutPendentes(i).numero = 0
                    End If
                End If
                FgAnaPendentes.TextMatrix(estrutPendentes(i).linha, estrutPendentes(i).coluna) = estrutPendentes(i).n_req & "(" & estrutPendentes(i).numero & ")"
                Exit For
            End If
        Next
    End If

End Sub

Private Sub FgAnaPendentes_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim i As Integer
    FgAnaPendentes.ToolTipText = ""
    FgPend_DevolveLinhaActual Y
    FgPend_DevolveColunaActual X
    For i = 1 To totalEstrutPEnd
        If estrutPendentes(i).linha = linhaToolTip And estrutPendentes(i).coluna = colunaTooltip Then
            FgAnaPendentes.ToolTipText = estrutPendentes(i).toolTip
            Exit For
        End If
    Next i
End Sub

Private Sub FgCaixa_Click()
    Dim i As Integer
    LbNome = ""
    LbNReq = ""
    LbEstadoReq = ""
    LbSexoUtente = ""
    LbDtChega = ""
    LbFrase = ""
    LbObservacao = ""
    EcObservacao = ""
    EcListaEliminados.Clear
    seqArquivoAux = mediComboValorNull
    BtGravarObs.Enabled = False
    If indiceCaixa > 0 And indiceReutil > 0 Then
        For i = 1 To estrutCaixas(indiceCaixa).reutil(indiceReutil).totalArquivos
            If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_x = FgCaixa.Col And _
                estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_y = FgCaixa.row Then
                    'If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado <> gEstadoSerotecaLixo Then
                        seqArquivoAux = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).seq_arquivo
                        LbNome = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).nome_ute
                        LbNReq = "Req.:" & estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).n_req & " Tubo: " & estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).cod_etiq
                        LbNReq = LbNReq & " Aliq.:" & estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).aliquota
                        LbFrase = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).frase
                        Select Case estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado_req
                            Case gEstadoReqCancelada
                                LbEstadoReq = "Cancelada"
                            Case gEstadoReqEsperaProduto
                                LbEstadoReq = "Espera Produto"
                            Case gEstadoReqEsperaResultados
                                LbEstadoReq = "Espera de Resultados"
                            Case gEstadoReqEsperaValidacao
                                LbEstadoReq = "Espera de Valida��o"
                            Case gEstadoReqHistorico
                                LbEstadoReq = "Hist�rico"
                            Case gEstadoReqImpressaoParcial
                                LbEstadoReq = "Impress�o Parcial"
                            Case gEstadoReqResultadosParciais
                                LbEstadoReq = "Resultados Parciais"
                            Case gEstadoReqTodasImpressas
                                LbEstadoReq = "Todas Impressas"
                            Case gEstadoReqValicacaoMedicaCompleta
                                LbEstadoReq = "Valida��o M�dica Completa"
                            Case gEstadoReqValidacaoMedicaParcial
                                LbEstadoReq = "Valida��o M�dica Parcial"
                        End Select
                        If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).sexo_ute = gT_Masculino Then
                            LbSexoUtente = "Masculino"
                        ElseIf estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).sexo_ute = gT_Feminino Then
                            LbSexoUtente = "Feminino"
                        End If
                        LbDtChega = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).dt_chega
                        LbObservacao = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).obs
                        BtGravarObs.Enabled = True
                    'Else
                        If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaLixo Then
                            EcListaEliminados.AddItem estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).n_req
                        End If
                    'End If
            End If
        Next
    End If

End Sub

Private Sub FgCaixa_DblClick()
    If FgCaixa.Text <> "" Then
        gRequisicaoActiva = FgCaixa.Text
        FormSerGestaoCaixas.Enabled = False
        FormConsNovo.Show
    End If
End Sub

Private Sub FgCaixa_DragDrop(Source As Control, X As Single, Y As Single)
    Dim flg_col As Boolean
    Dim flg_lin As Boolean
    Dim i As Integer
    Dim req_aux As String
    Dim tubo_aux As String
    Dim aliquota_aux As String
    
    flg_col = False
    flg_lin = False
    If EcNumReq = "" Then Exit Sub
    For i = 1 To FgCaixa.Cols - 2
        If X >= FgCaixa.ColPos(i) And X < FgCaixa.ColPos(i + 1) Then
            FgCaixa.Col = i
            flg_col = True
            Exit For
        End If
    Next
    If flg_col = False Then
        If X >= FgCaixa.ColPos(FgCaixa.Cols - 1) Then
            FgCaixa.Col = i
            flg_col = True
        End If
    End If
    
    For i = 1 To FgCaixa.rows - 2
        If Y >= FgCaixa.RowPos(i) And Y < FgCaixa.RowPos(i + 1) Then
            FgCaixa.row = i
            flg_lin = True
            Exit For
        End If
    Next
    If flg_lin = False Then
        If Y >= FgCaixa.RowPos(FgCaixa.rows - 1) Then
            FgCaixa.row = i
            flg_lin = True
        End If
    End If
    
    If EcNumReq <> "" Then
        If Len(EcNumReq) <= 7 And InStr(1, EcNumReq, ".") = 0 Then
            req_aux = EcNumReq
            tubo_aux = ""
            aliquota_aux = ""
        ElseIf InStr(1, EcNumReq, ".") > 0 Then
            If Len(Mid(EcNumReq, 1, InStr(1, EcNumReq, ".") - 1)) <= 7 Then
                req_aux = Mid(EcNumReq, 1, InStr(1, EcNumReq, ".") - 1)
                aliquota_aux = Mid(EcNumReq, InStr(1, EcNumReq, ".") + 1)
                tubo_aux = ""
            ElseIf Len(Mid(EcNumReq, 1, InStr(1, EcNumReq, ".") - 1)) = 9 Then
                req_aux = Mid(EcNumReq, 1, InStr(1, EcNumReq, ".") - 1)
                aliquota_aux = Mid(EcNumReq, InStr(1, EcNumReq, ".") + 1)
                tubo_aux = Mid(req_aux, 1, 2)
                req_aux = CStr(CLng(Mid(req_aux, 3)))
            End If
        ElseIf Len(EcNumReq) = 9 And InStr(1, EcNumReq, ".") = 0 Then
            req_aux = Mid(EcNumReq, 3)
            tubo_aux = Mid(EcNumReq, 1, 2)
            req_aux = CStr(CLng(Mid(req_aux, 3)))
            aliquota_aux = ""
        End If
    End If
    
    AdicionaReqCaixa indiceCaixa, indiceReutil, req_aux, FgCaixa.Col, FgCaixa.row, tubo_aux, aliquota_aux, BL_HandleNull(EcObservacao, "")
End Sub

Private Sub FgCaixa_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        If FgCaixa.Text <> "" Then
            MDIFormInicio.PopupMenu MDIFormInicio.IDM_SEROTECA_HIDE
        End If
    End If
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Set CampoDeFocus = EcCodCaixa
    Me.caption = " Arquivo de Amostras - Gest�o"
    Me.left = 50
    Me.top = 0
    Me.Width = 15225
    Me.Height = 9420 ' Normal
    
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    gF_SEROTECA_GESTAO = 1
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    gF_SEROTECA_GESTAO = 0
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormSerGestaoCaixas = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus
    FgCaixaLimpa
    CbSeroteca.ListIndex = mediComboValorNull
    EcCodCaixa = ""
    BG_LimpaCampo_Todos CamposEc
    LbNome = ""
    LbNReq = ""
    LbEstadoReq = ""
    LbSexoUtente = ""
    LbDtChega = ""
    EcNumReq = ""
    LbFrase = ""
    EcDescrCaixa = ""
    EcObservacao = ""
    LbObservacao = ""
    indiceReutil = mediComboValorNull
    indiceCaixa = mediComboValorNull
    TV.Nodes.Clear
    FramePendentes.Visible = False
    BtGravarObs.Enabled = False
    
End Sub

Sub DefTipoCampos()
    Dim i As Integer
    TV.LineStyle = tvwRootLines
    TV.Nodes.Clear
    TV.ImageList = IL
      EcCodCaixa.Tag = adVarChar
    BG_PreencheComboBD_ADO "sl_serotecas", "cod_seroteca", "descr_seroteca", CbSeroteca, mediAscComboCodigo
    With FgCaixa
        .rows = 11
        .FixedRows = 1
        .Cols = 11
        .FixedCols = 1
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeColumns
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .Font.Size = 10
        For i = 1 To .Cols - 1
            If i > 0 Then
                .TextMatrix(0, i) = i
            End If
            .ColWidth(i) = 900
            .ColAlignment(i) = flexAlignCenterCenter
        Next
        For i = 1 To .rows - 1
            If i > 0 Then
                .TextMatrix(i, 0) = Chr(64 + i)
            End If
            .RowHeight(i) = 500
        Next
        .ColWidth(0) = 250
        .RowHeight(0) = 200
        .row = 0
        .Col = 0
        .Width = .ColWidth(1) * 10 + 270
        .Height = .RowHeight(1) * 10 + 220
    End With
    totalCaixas = 0
    ReDim estrutCaixas(0)
    FramePendentes.Visible = False
    FgAnaPendentes.rows = 18
    FgAnaPendentes.Cols = 15
    BtGravarObs.Enabled = False
    EcDtImp.value = Bg_DaData_ADO
    
End Sub

Sub PreencheCampos()
    On Error GoTo TrataErro
    FgCaixaLimpa
    Me.SetFocus
    PreencheEstrutCaixas
    BtGravarObs.Enabled = False
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher campos " & Err.Description, Me.Name, "PReencheCampos", True
    Exit Sub
    Resume Next
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim req_aux As String
    Dim tubo_aux As String
    Dim aliquota_aux As String
    Dim i As Integer
    TV.Nodes.Clear
    req_pesquisa = ""
    tubo_pesquisa = ""
    aliq_pesquisa = ""
    
    If CbSeroteca.ListIndex = mediComboValorNull And EcCodCaixa = "" And EcReqPesq = "" Then
        Exit Sub
    End If
    sSql = "SELECT DISTINCT x1.cod_caixa, x1.descr_caixa, x1.cod_seroteca, x1.tamanho, x1.user_cri, "
    sSql = sSql & " x1.dt_cri, x1.hr_cri, x1.user_act, x1.dt_act, x1.hr_act, x1.hor, x1.ver,x1.prazo_cong, "
    sSql = sSql & " x3.seq_reutil, x3.user_cri, x3.dt_cri, x3.hr_cri, x3.flg_activo, x3.dt_act dar, x3.hr_act har, x3.user_act uar "
    sSql = sSql & " FROM sl_ser_caixas x1,  sl_ser_reutil_caixa x3 WHERE x1.cod_seroteca = x3.cod_seroteca AND x1.cod_caixa = x3.cod_caixa  "
    
    'SE PREENCHEU SEROTECA
    If CbSeroteca.ListIndex <> mediComboValorNull Then
        sSql = sSql & " AND x1.COD_SEROTECA = " & CbSeroteca.ItemData(CbSeroteca.ListIndex)
    End If
    
    'SE PREENCHEU CAIXA
    If EcCodCaixa <> "" Then
        sSql = sSql & " AND x1.cod_caixa = " & EcCodCaixa
    End If
    
    If EcReqPesq <> "" Then
        sSql = sSql & " AND (x3.cod_caixa, x3.seq_reutil ) IN ( select x4.cod_caixa, x4.seq_reutil from sl_ser_arquivo x4 where n_req = " & EcReqPesq & ")"
    End If
    sSql = sSql & " ORDER BY x1.cod_seroteca ASC, x1.cod_caixa ASC,x3.dt_Cri ASC "
    If EcNumReq <> "" Then
        req_pesquisa = req_aux
        tubo_pesquisa = tubo_aux
        aliq_pesquisa = aliquota_aux
    End If
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
    If rs.RecordCount > 0 Then
        PreencheCampos
    Else
        BG_Mensagem mediMsgBox, "N�o foi encontrado nehum registo.", vbExclamation, "Procura"
    End If
    Tv_Click
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Procurar " & Err.Description, Me.Name, "FuncaoProcurar", True
    Exit Sub
    Resume Next
 End Sub

Sub FuncaoAnterior()
End Sub

Sub FuncaoSeguinte()

End Sub

Sub FuncaoInserir()

End Sub

Sub BD_Insert()
        
End Sub

Sub FuncaoModificar()
End Sub

Sub BD_Update()
End Sub

Sub FuncaoRemover()
    
End Sub

Sub BD_Delete()
    
End Sub

Private Sub FgCaixaLimpa()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim j As Integer
    For i = 1 To FgCaixa.rows - 1
        FgCaixa.row = i
        For j = 1 To FgCaixa.Cols - 1
            FgCaixa.Col = j
            FgCaixa.CellBackColor = vbWhite
            FgCaixa.Text = ""
        Next
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Limpar FlexGrid " & Err.Description, Me.Name, "FgCaixaLimpa", True
    Exit Sub
    Resume Next
End Sub



Private Sub PreencheEstrutCaixas()
    Dim flg_existe As Boolean
    Dim flg_existe2 As Boolean
    Dim sSql As String
    Dim rsMax As New ADODB.recordset
    Dim i As Integer
    Dim iCaixa As Integer
    Dim iReutil As Integer
    Dim j As Integer
    Dim root As Node
    On Error GoTo TrataErro
    totalCaixas = 0
    ReDim estrutCaixas(0)
    While Not rs.EOF
        flg_existe = False
        For i = 1 To totalCaixas
            If estrutCaixas(i).cod_caixa = BL_HandleNull(rs!cod_caixa, -1) Then
                flg_existe = True
                iCaixa = i
                Exit For
            End If
        Next
        If flg_existe = False Then
            totalCaixas = totalCaixas + 1
            ReDim Preserve estrutCaixas(totalCaixas)
            iCaixa = totalCaixas
            
            estrutCaixas(totalCaixas).cod_caixa = BL_HandleNull(rs!cod_caixa, -1)
            estrutCaixas(totalCaixas).cod_seroteca = BL_HandleNull(rs!cod_seroteca, -1)
            estrutCaixas(totalCaixas).descr_caixa = BL_HandleNull(rs!descr_caixa, "")
            estrutCaixas(totalCaixas).dt_act = BL_HandleNull(rs!dt_act, "")
            estrutCaixas(totalCaixas).dt_cri = BL_HandleNull(rs!dt_cri, "")
            estrutCaixas(totalCaixas).hr_cri = BL_HandleNull(rs!hr_cri, "")
            estrutCaixas(totalCaixas).hr_act = BL_HandleNull(rs!hr_act, "")
            estrutCaixas(totalCaixas).livre = BL_HandleNull(rs!tamanho, 0)
            estrutCaixas(totalCaixas).tamanho = BL_HandleNull(rs!tamanho, 0)
            estrutCaixas(totalCaixas).horiz = BL_HandleNull(rs!hor, 0)
            estrutCaixas(totalCaixas).vert = BL_HandleNull(rs!ver, 0)
            estrutCaixas(totalCaixas).prazo_cong = BL_HandleNull(rs!prazo_cong, 0)
            estrutCaixas(totalCaixas).prazo_aviso = estrutCaixas(totalCaixas).prazo_cong / 3
            estrutCaixas(totalCaixas).totalReutil = 0
            ReDim estrutCaixas(totalCaixas).reutil(0)
        
        
        End If
        iReutil = -1
        flg_existe2 = False
        For j = 1 To estrutCaixas(iCaixa).totalReutil
            If estrutCaixas(iCaixa).reutil(j).seq_reutil = BL_HandleNull(rs!seq_reutil, 0) Then
                flg_existe2 = True
                iReutil = j
                Exit For
            End If
        Next
        If flg_existe2 = False And BL_HandleNull(rs!seq_reutil, 0) > 0 Then
            estrutCaixas(iCaixa).totalReutil = estrutCaixas(iCaixa).totalReutil + 1
            ReDim Preserve estrutCaixas(iCaixa).reutil(estrutCaixas(iCaixa).totalReutil)
            iReutil = estrutCaixas(iCaixa).totalReutil
            
            
            estrutCaixas(iCaixa).cod_seroteca = BL_HandleNull(rs!cod_seroteca, -1)
            estrutCaixas(iCaixa).cod_caixa = BL_HandleNull(rs!cod_caixa, -1)
            estrutCaixas(iCaixa).reutil(estrutCaixas(iCaixa).totalReutil).dt_cri = BL_HandleNull(rs!dt_cri, "")
            estrutCaixas(iCaixa).reutil(estrutCaixas(iCaixa).totalReutil).user_cri = BL_HandleNull(rs!user_cri, "")
            estrutCaixas(iCaixa).reutil(estrutCaixas(iCaixa).totalReutil).hr_cri = BL_HandleNull(rs!hr_cri, "")
            estrutCaixas(iCaixa).reutil(estrutCaixas(iCaixa).totalReutil).dt_act = BL_HandleNull(rs!dar, "")
            estrutCaixas(iCaixa).reutil(estrutCaixas(iCaixa).totalReutil).user_act = BL_HandleNull(rs!uar, "")
            estrutCaixas(iCaixa).reutil(estrutCaixas(iCaixa).totalReutil).hr_act = BL_HandleNull(rs!har, "")
            estrutCaixas(iCaixa).reutil(estrutCaixas(iCaixa).totalReutil).flg_activo = BL_HandleNull(rs!flg_activo, 0)
            estrutCaixas(iCaixa).reutil(estrutCaixas(iCaixa).totalReutil).seq_reutil = BL_HandleNull(rs!seq_reutil, -1)
            estrutCaixas(iCaixa).reutil(estrutCaixas(iCaixa).totalReutil).totalArquivos = 0
            ReDim estrutCaixas(iCaixa).reutil(estrutCaixas(iCaixa).totalReutil).estrutArquivo(0)
        End If
        If iReutil > -1 Then
            'PreencheEstrutArquivo iCaixa, iReutil
        End If
        rs.MoveNext
    Wend
    PreenhceTV
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher Estrut Caixas " & Err.Description, Me.Name, "PreencheEstrutCaixas", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheEstrutArquivo(iCaixa As Integer, iReutil As Integer)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iArquivo As Integer
    Dim rsArq As New ADODB.recordset
    
    sSql = "SELECT x1.seq_arquivo, x1.cod_caixa, x1.n_Req, x1.pos_x, x1.pos_y, x1.posicao, x1.estado, x1.user_cri, x1.dt_cri ,"
    sSql = sSql & " x1.hr_cri, x1.user_act, x1.dt_act, x1.hr_act, x2.dt_chega, x2.estado_req, x3.nome_ute, x3.sexo_ute, "
    sSql = sSql & " x1.cod_tubo, x1.aliquota, x1.obs, x1.cod_seroteca "
    sSql = sSql & " FROM sl_ser_arquivo x1, sl_requis x2, sl_identif x3 WHERE x1.cod_seroteca = " & estrutCaixas(iCaixa).cod_seroteca
    sSql = sSql & " AND x1.cod_caixa = " & estrutCaixas(iCaixa).cod_caixa & " AND  x1.seq_reutil = " & estrutCaixas(iCaixa).reutil(iReutil).seq_reutil
    sSql = sSql & " AND x1.n_req = x2.n_Req AND x2.seq_utente = x3.seq_utente "
    sSql = sSql & " ORDER by posicao ASC "
    rsArq.CursorType = adOpenStatic
    rsArq.CursorLocation = adUseServer
    
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsArq.Open sSql, gConexao
    If rsArq.RecordCount > 0 Then
        While Not rsArq.EOF
            estrutCaixas(iCaixa).reutil(iReutil).totalArquivos = estrutCaixas(iCaixa).reutil(iReutil).totalArquivos + 1
            ReDim Preserve estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos)
            iArquivo = estrutCaixas(iCaixa).reutil(iReutil).totalArquivos
            
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_act = BL_HandleNull(rsArq!dt_act, "")
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_cri = BL_HandleNull(rsArq!dt_cri, "")
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).estado = BL_HandleNull(rsArq!estado, -1)
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).hr_act = BL_HandleNull(rsArq!hr_act, "")
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).hr_cri = BL_HandleNull(rsArq!hr_cri, "")
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).n_req = BL_HandleNull(rsArq!n_req, -1)
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).cod_tubo = BL_HandleNull(rsArq!cod_tubo, "")
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).cod_etiq = RetornaEtiq(BL_HandleNull(rsArq!cod_tubo, ""))
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).aliquota = BL_HandleNull(rsArq!aliquota, "")
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).pos_x = BL_HandleNull(rsArq!pos_x, -1)
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).pos_y = BL_HandleNull(rsArq!pos_y, -1)
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).posicao = BL_HandleNull(rsArq!posicao, -1)
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).seq_arquivo = BL_HandleNull(rsArq!seq_arquivo, -1)
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).user_act = BL_HandleNull(rsArq!user_act, "")
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).user_cri = BL_HandleNull(rsArq!user_cri, "")
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).obs = BL_HandleNull(rsArq!obs, "")
            
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).nome_ute = BL_HandleNull(rsArq!nome_ute, "")
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_chega = BL_HandleNull(rsArq!dt_chega, "")
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).estado_req = BL_HandleNull(rsArq!estado_req, "")
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).sexo_ute = BL_HandleNull(rsArq!sexo_ute, "")
            
            PreencheDatas estrutCaixas(iCaixa).cod_caixa, estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).n_req, _
                          iCaixa, iReutil, estrutCaixas(iCaixa).reutil(iReutil).totalArquivos, estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).cod_tubo, _
                          estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).aliquota
                          
            GereEstados iCaixa, iReutil, estrutCaixas(iCaixa).reutil(iReutil).totalArquivos
            rsArq.MoveNext
        Wend
    End If
    rsArq.Close
    Set rsArq = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher Estrut Arquivo " & Err.Description, Me.Name, "PreencheEstrutArquivo", True
    Exit Sub
    Resume Next
End Sub

Private Sub BtPesquisaCaixa_click()


    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_caixa"
    CamposEcran(1) = "cod_caixa"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_caixa"
    CamposEcran(2) = "descr_caixa"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods_
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_ser_caixas"
    CampoPesquisa1 = "descr_caixa"
    If CbSeroteca.ListIndex > mediComboValorNull Then
        ClausulaWhere = " cod_seroteca = " & CbSeroteca.ItemData(CbSeroteca.ListIndex)
    End If
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Suportes Seroteca")
    
    mensagem = "N�o foi encontrada nenhuma Suporte."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodCaixa.Text = resultados(1)
            EcDescrCaixa.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If

End Sub

Private Sub EcCodcaixa_Validate(Cancel As Boolean)
    Dim RsDescrCaixa As ADODB.recordset
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcCodCaixa)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodCaixa.Text) <> "" Then
        Set RsDescrCaixa = New ADODB.recordset
        
        With RsDescrCaixa
            .Source = "SELECT cod_caixa, descr_caixa FROM sl_ser_caixas WHERE cod_caixa= " & EcCodCaixa.Text
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            If gModoDebug = mediSim Then BG_LogFile_Erros .Source
            .Open
        End With
    
        If RsDescrCaixa.RecordCount > 0 Then
            EcCodCaixa.Text = "" & RsDescrCaixa!cod_caixa
            EcDescrCaixa.Text = "" & RsDescrCaixa!descr_caixa
        Else
            Cancel = True
            EcDescrCaixa.Text = ""
            BG_Mensagem mediMsgBox, "A caixa indicada n�o existe!", vbOKOnly + vbExclamation, App.ProductName
            Sendkeys ("{HOME}+{END}")
        End If
        RsDescrCaixa.Close
        Set RsDescrCaixa = Nothing
    Else
        EcDescrCaixa.Text = ""
    End If

End Sub


Private Sub PreencheFgCaixa()
    Dim i As Integer
    Dim req_aux As String
    On Error GoTo TrataErro
    FgCaixa.Visible = False
    FgCaixaLimpa
    FgCaixa.rows = estrutCaixas(indiceCaixa).horiz + 1
    FgCaixa.Cols = estrutCaixas(indiceCaixa).vert + 1
    FgCaixa.Visible = False
    For i = 1 To FgCaixa.Cols - 1
        If i > 0 Then
            FgCaixa.TextMatrix(0, i) = i
        End If
        FgCaixa.ColWidth(i) = 900
        FgCaixa.ColAlignment(i) = flexAlignCenterCenter
    Next
    For i = 1 To FgCaixa.rows - 1
        If i > 0 Then
            FgCaixa.TextMatrix(i, 0) = Chr(64 + i)
        End If
        FgCaixa.RowHeight(i) = 500
    Next
    
    If indiceCaixa = mediComboValorNull Or indiceReutil = mediComboValorNull Then
        Exit Sub
    End If
    For i = 1 To estrutCaixas(indiceCaixa).reutil(indiceReutil).totalArquivos
        FgCaixa.row = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_y
        FgCaixa.Col = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_x
        'CHMA-1389
'        If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaLixo Then
'            If Mid(FgCaixa.Text, 1, 1) <> "*" Then
'                FgCaixa.Text = "* " & FgCaixa.Text
'            End If
'        Else
            FgCaixa.Text = FgCaixa.Text & estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).cod_etiq & " "
        
            If Len(estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).n_req) < 7 Then
                req_aux = CStr(estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).n_req)
                While Len(req_aux) < 7
                    req_aux = "0" & req_aux
                Wend
            End If
            FgCaixa.Text = FgCaixa.Text & req_aux
            FgCaixa.Text = FgCaixa.Text & IIf(estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).aliquota <> "", "." & estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).aliquota, "")
        'End If
        
        FgCaixa.CellFontSize = 8
        If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaAtribuido Then
            If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).semAna Then
                FgCaixa.CellBackColor = lCorSemAna
            Else
                FgCaixa.CellBackColor = lCorAtribuida
            End If
        ElseIf estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaCongelado Then
            FgCaixa.CellBackColor = lCorCongelada
            If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).dataUltrapassada Then
                FgCaixa.CellBackColor = lCorDataUltrapassada
            ElseIf estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).dataProxima Then
                FgCaixa.CellBackColor = lcorDataProxima
            ElseIf estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).semAna Then
                FgCaixa.CellBackColor = lCorSemAna
            End If
        ElseIf estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaDescongelado Then
            FgCaixa.CellBackColor = lCorDescongelada
        'CHMA-1389
        ElseIf estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaLixo Then
        '
            FgCaixa.CellBackColor = lCorLixo
        End If
        If BL_HandleNull(EcReqPesq, mediComboValorNull) = estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).n_req Then
            FgCaixa.CellFontBold = True
            FgCaixa.CellForeColor = vbRed
        Else
            FgCaixa.CellFontBold = False
            FgCaixa.CellForeColor = vbBlack
        End If
        

    Next
    FgCaixa.Visible = True
Exit Sub
TrataErro:
    FgCaixa.Visible = True
    BG_LogFile_Erros "Erro  ao Preencher FgCaixa " & Err.Description, Me.Name, "PreencheFgCaixa", True
    Exit Sub
    Resume Next
End Sub

Private Sub Ecnumreq_DragOver(Source As Control, X As Single, Y As Single, state As Integer)
    On Error GoTo TrataErro
    If VerificaReqValida = True Then
        EcNumReq.DragIcon = Picture1.Picture
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Preencher Requisicao " & Err.Description, Me.Name, "Ecnumreq_DragOver", True
    Exit Sub
    Resume Next
End Sub

Private Function AdicionaReqCaixa(iCaixa As Integer, iReutil As Integer, NReq As String, X As Integer, Y As Integer, etiq_tubo As String, aliquota As String, obs As String) As Long
    On Error GoTo TrataErro
    Dim n_Req_aux As String
    Dim req_aux As String
    Dim i As Integer
    Dim flg_ignora_perg As Boolean
    flg_ignora_perg = False
    AdicionaReqCaixa = mediComboValorNull
    For i = 1 To estrutCaixas(iCaixa).reutil(iReutil).totalArquivos
        If estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(i).pos_x = X And estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(i).pos_y = Y Then
            If estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(i).estado = gEstadoSerotecaLixo And estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(i).n_req = NReq Then
                BG_Mensagem mediMsgBox, "A mesma requisi��o n�o pode estar duas vezes atribuida � mesma  posi��o", vbOKOnly + vbExclamation, App.ProductName
                Exit Function
            ElseIf estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(i).estado = gEstadoSerotecaLixo And flg_ignora_perg = False Then
                gMsgTitulo = "Arquivo"
                gMsgMsg = "Essa posi��o cont�m amostras arquivadas anteriormente. Pretende continuar?"
                gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
                If gMsgResp <> vbYes Then
                    Exit Function
                End If
                flg_ignora_perg = True
            Else
                BG_Mensagem mediMsgBox, "Posi��o j� ocupada!", vbOKOnly + vbExclamation, App.ProductName
                Exit Function
            End If
        End If
    Next
    For i = 1 To estrutCaixas(iCaixa).reutil(iReutil).totalArquivos
        If estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(i).n_req = NReq Then
            gMsgTitulo = "Arquivo"
            gMsgMsg = "Requisi��o j� existe na caixa em quest�o. Pretende arquivar?"
            gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
            If gMsgResp <> vbYes Then
                Exit Function
            End If
        End If
    Next

    estrutCaixas(iCaixa).reutil(iReutil).totalArquivos = estrutCaixas(iCaixa).reutil(iReutil).totalArquivos + 1
    ReDim Preserve estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos)
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).dt_act = ""
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).dt_cri = Bg_DaData_ADO
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).estado = 0
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).hr_act = ""
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).hr_cri = Bg_DaHora_ADO
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).n_req = NReq
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).pos_x = X
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).pos_y = Y
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).posicao = ((Y - 1) * 10) + X
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).seq_arquivo = -1
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).user_act = ""
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).user_cri = gCodUtilizador
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).cod_etiq = etiq_tubo
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).cod_tubo = RetornaTubo(etiq_tubo)
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).aliquota = aliquota
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(estrutCaixas(iCaixa).reutil(iReutil).totalArquivos).obs = obs
    
    AdicionaReqCaixa = GravaArquivo(iCaixa, iReutil, estrutCaixas(iCaixa).reutil(iReutil).totalArquivos)

Exit Function
TrataErro:
    AdicionaReqCaixa = mediComboValorNull
    BG_LogFile_Erros "Erro  ao adicionar Requisi��o � caixa " & Err.Description, Me.Name, "AdicionaReqCaixa", True
    Exit Function
    Resume Next
End Function

Public Sub ColocaCongelado()
    Dim i As Integer
    Dim flgEncontrou As Boolean
    flgEncontrou = False
    If indiceCaixa <> mediComboValorNull And indiceReutil <> mediComboValorNull Then
        For i = 1 To estrutCaixas(indiceCaixa).reutil(indiceReutil).totalArquivos
            If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_x = FgCaixa.Col Then
                If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_y = FgCaixa.row Then
                    If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).n_req = FgCaixa.TextMatrix(FgCaixa.row, FgCaixa.Col) Then
                        flgEncontrou = True
                        Exit For
                    End If
                End If
            End If
        Next
    End If
    If flgEncontrou = True Then
        If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaAtribuido Or estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaDescongelado Then
            estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaCongelado
            FgCaixa.CellBackColor = lCorCongelada
            GravaArquivo indiceCaixa, indiceReutil, CLng(i)
        End If
    End If
End Sub


Public Sub ColocaDesCongelado()
    Dim i As Integer
    Dim flgEncontrou As Boolean
    flgEncontrou = False
    If indiceCaixa <> mediComboValorNull And indiceReutil <> mediComboValorNull Then
        For i = 1 To estrutCaixas(indiceCaixa).reutil(indiceReutil).totalArquivos
            If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_x = FgCaixa.Col Then
                If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_y = FgCaixa.row Then
                    If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).n_req = FgCaixa.TextMatrix(FgCaixa.row, FgCaixa.Col) Then
                        flgEncontrou = True
                        Exit For
                    End If
                End If
            End If
        Next
    End If
    If flgEncontrou = True Then
        If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaCongelado Then
            estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaDescongelado
            FgCaixa.CellBackColor = lCorDescongelada
            GravaArquivo (indiceCaixa), indiceReutil, CLng(i)
        End If
    End If

End Sub

Public Sub ColocaLixo()
    Dim i As Integer
    Dim flgEncontrou As Boolean
    flgEncontrou = False
    If indiceCaixa <> mediComboValorNull And indiceReutil <> mediComboValorNull Then
        For i = 1 To estrutCaixas(indiceCaixa).reutil(indiceReutil).totalArquivos
            If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_x = FgCaixa.Col Then
                If estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).pos_y = FgCaixa.row Then
                    If InStr(1, FgCaixa.TextMatrix(FgCaixa.row, FgCaixa.Col), estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).n_req) > 0 Then
                        flgEncontrou = True
                        Exit For
                    End If
                End If
            End If
        Next
    End If
    If flgEncontrou = True Then
        estrutCaixas(indiceCaixa).reutil(indiceReutil).estrutArquivo(i).estado = gEstadoSerotecaLixo
        FgCaixa.CellBackColor = lCorLixo
        GravaArquivo indiceCaixa, indiceReutil, CLng(i)
    End If

End Sub

Private Function GuardaReqPosicaoDisp(NReq As String, iCaixa As Integer, iReutil As Integer, etiq_tubo As String, aliquota As String) As Long
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim flgExiste As Boolean
    On Error GoTo TrataErro
    GuardaReqPosicaoDisp = mediComboValorNull
    
    For i = 1 To estrutCaixas(iCaixa).horiz
        For j = 1 To estrutCaixas(iCaixa).vert
            flgExiste = False
            For k = 1 To estrutCaixas(iCaixa).reutil(iReutil).totalArquivos
                If estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(k).pos_x = j And estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(k).pos_y = i Then
                    flgExiste = True
                    Exit For
                End If
            Next
            If flgExiste = False Then Exit For
        Next
        If flgExiste = False Then Exit For
    Next
    If flgExiste = False And totalCaixas > 0 Then
        GuardaReqPosicaoDisp = AdicionaReqCaixa(iCaixa, iReutil, NReq, j, i, etiq_tubo, aliquota, BL_HandleNull(EcObservacao, ""))
    End If
Exit Function
TrataErro:
    GuardaReqPosicaoDisp = mediComboValorNull
    BG_LogFile_Erros "GuardaReqPosicaoDisp: " & Err.Description, Me.Name, "GuardaReqPosicaoDisp", True
    Exit Function
    Resume Next
End Function

Private Function VerificaReqValida() As Boolean
    Dim sSql As String
    Dim rsReq As New ADODB.recordset
    Dim req_aux As String
    On Error GoTo TrataErro
    
    If indiceCaixa = mediComboValorNull Or indiceReutil = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "N�o existe suporte seleccionado!", vbOKOnly + vbExclamation, App.ProductName
        VerificaReqValida = False
        EcNumReq = ""
        Exit Function
    End If
    If estrutCaixas(indiceCaixa).reutil(indiceReutil).flg_activo = 0 Then
        BG_Mensagem mediMsgBox, "Suporte j� est� fechado!", vbOKOnly + vbExclamation, App.ProductName
        VerificaReqValida = False
        EcNumReq = ""
        Exit Function
    End If
    
    If EcNumReq = "" Then
        EcNumReq.SetFocus
        VerificaReqValida = False
        Exit Function
    End If
    If Len(EcNumReq) <= 7 And InStr(1, EcNumReq, ".") = 0 Then
        req_aux = EcNumReq
    ElseIf InStr(1, EcNumReq, ".") > 0 Then
        If Len(Mid(EcNumReq, 1, InStr(1, EcNumReq, ".") - 1)) <= 7 Then
            req_aux = Mid(EcNumReq, 1, InStr(1, EcNumReq, ".") - 1)
        ElseIf Len(Mid(EcNumReq, 1, InStr(1, EcNumReq, ".") - 1)) = 9 Then
            req_aux = Mid(EcNumReq, 1, InStr(1, EcNumReq, ".") - 1)
            req_aux = CStr(CLng(Mid(req_aux, 3)))
        End If
    ElseIf Len(EcNumReq) = 9 And InStr(1, EcNumReq, ".") = 0 Then
        req_aux = Mid(EcNumReq, 3)
        'req_aux = CStr(CLng(Mid(req_aux, 3)))
    End If
    
    sSql = "SELECT n_req FROM sl_requis WHERE n_req = " & req_aux
    rsReq.CursorType = adOpenStatic
    rsReq.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsReq.Open sSql, gConexao
    If rsReq.RecordCount <> 1 Then
        BG_Mensagem mediMsgBox, "Requisi��o n�o � v�lida!", vbOKOnly + vbExclamation, App.ProductName
        EcNumReq = ""
        EcNumReq.SetFocus
        VerificaReqValida = False
        Exit Function
    End If
    VerificaReqValida = True

Exit Function
TrataErro:
    BG_LogFile_Erros "VerificaReqValida: " & Err.Description, Me.Name, "VerificaReqValida", True
    Exit Function
    Resume Next
End Function

Private Function GravaArquivo(iCaixa As Integer, iReutil As Integer, iArquivo As Integer) As Long
    On Error GoTo TrataErro
    Dim sSql As String
    Dim i As Integer
    GravaArquivo = mediComboValorNull
    If estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).seq_arquivo > 0 Then
       If SER_RegistaAltArquivo(estrutCaixas(iCaixa).cod_seroteca, estrutCaixas(iCaixa).cod_caixa, estrutCaixas(iCaixa).reutil(iReutil).seq_reutil, _
                                 estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).seq_arquivo, estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).n_req, _
                                 estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).estado, estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).cod_tubo, _
                                 estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).aliquota) = False Then
    
            GoTo TrataErro
        End If

    Else
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).seq_arquivo = BG_DaMAX("SL_SER_ARQUIVO", "SEQ_ARQUIVO") + 1
    
        sSql = "INSERT INTO sl_ser_arquivo (seq_arquivo,cod_caixa, n_req, pos_x, pos_y, posicao, estado, user_cri,"
        sSql = sSql & " dt_cri, hr_cri, user_act, dt_act, hr_act,cod_tubo, aliquota, OBS, seq_reutil, cod_seroteca ) VALUES ("
        sSql = sSql & estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).seq_arquivo & ", "
        sSql = sSql & estrutCaixas(iCaixa).cod_caixa & ", "
        sSql = sSql & estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).n_req & ", "
        sSql = sSql & estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).pos_x & ", "
        sSql = sSql & estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).pos_y & ", "
        sSql = sSql & estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).posicao & ", "
        sSql = sSql & estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).estado & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).user_cri) & ", "
        sSql = sSql & BL_TrataDataParaBD(estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_cri) & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).hr_cri) & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).user_act) & ", "
        sSql = sSql & BL_TrataDataParaBD(estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_act) & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).hr_act) & ", "
        sSql = sSql & BL_TrataStringParaBD(CStr(estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).cod_tubo)) & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).aliquota) & ","
        sSql = sSql & BL_TrataStringParaBD(estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).obs) & ","
        sSql = sSql & estrutCaixas(iCaixa).reutil(iReutil).seq_reutil & ", "
        sSql = sSql & estrutCaixas(iCaixa).cod_seroteca & ") "
        i = BG_ExecutaQuery_ADO(sSql)
        If i > 0 Then
            sSql = "UPDATE SL_REQUIS SET flg_seroteca = 1 WHERE n_req = " & estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).n_req
            BG_ExecutaQuery_ADO sSql
            If SER_RegistaAlteracao(estrutCaixas(iCaixa).cod_seroteca, estrutCaixas(iCaixa).cod_caixa, _
                                    estrutCaixas(iCaixa).reutil(iReutil).seq_reutil, estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).seq_arquivo, _
                                    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).n_req, estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).estado, _
                                    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).cod_tubo, estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).aliquota) = False Then
                GoTo TrataErro
            End If
        Else
            GoTo TrataErro
        End If
    End If
    GravaArquivo = estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).seq_arquivo
Exit Function
TrataErro:
    GravaArquivo = mediComboValorNull
    BG_LogFile_Erros "GravaArquivo: " & Err.Description, Me.Name, "GravaArquivo", True
    Exit Function
    Resume Next
End Function


' ---------------------------------------------------------------------------------

' RETORNA O NUMERO DE ANALISES PENDENTES PARA DETERMINADA REQUISICAO

' ---------------------------------------------------------------------------------
Private Function RetornaNumAnaPendentes(n_req As Long, cod_tubo As String) As Integer
    Dim sSql As String
    Dim rsMarca As New ADODB.recordset
    On Error GoTo TrataErro
    If cod_tubo = "" Then
        sSql = "SELECT distinct cod_agrup FROM sl_marcacoes WHERE n_req = " & n_req
    Else
        sSql = "SELECT distinct cod_agrup from sl_marcacoes x1, slv_analises x2, sl_req_tubo x3 WHERE x1.n_reQ = " & n_req
        sSql = sSql & " AND x1.cod_agrup = x2.cod_ana AND x2.cod_tubo = x3.cod_tubo AND x1.n_req = x3.n_req "
        sSql = sSql & " AND x3.cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
    End If
    rsMarca.CursorType = adOpenStatic
    rsMarca.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsMarca.Open sSql, gConexao
    RetornaNumAnaPendentes = rsMarca.RecordCount
    rsMarca.Close
    Set rsMarca = Nothing
Exit Function
TrataErro:
    RetornaNumAnaPendentes = -1
    BG_LogFile_Erros "RetornaNumAnaPendentes: " & Err.Description, Me.Name, "RetornaNumAnaPendentes", True
    Exit Function
    Resume Next
End Function

' ---------------------------------------------------------------------------------

' AVALIA O ESTADO DE CADA ITEM

' ---------------------------------------------------------------------------------
Private Sub GereEstados(iCaixa As Integer, iReutil As Integer, iArquivo As Integer)
    On Error GoTo TrataErro
    If estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).estado = gEstadoSerotecaLixo Then
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).congelada = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).descongelada = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).lixo = True
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).semAna = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataProxima = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataUltrapassada = False
    ElseIf estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).estado = gEstadoSerotecaDescongelado Then
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).congelada = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).descongelada = True
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).lixo = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).semAna = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataProxima = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataUltrapassada = False
    ElseIf estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).estado = gEstadoSerotecaAtribuido Then
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).congelada = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).descongelada = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).lixo = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).semAna = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataProxima = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataUltrapassada = False
    ElseIf estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).estado = gEstadoSerotecaCongelado Then
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).congelada = True
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).descongelada = False
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).lixo = False

        If estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_congelamento <> "" Then
            If (estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_congelamento + CDate(estrutCaixas(iCaixa).prazo_cong)) < Bg_DaData_ADO Then
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataUltrapassada = True
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataProxima = False
            Else
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataUltrapassada = False
                If (estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_congelamento + CDate(estrutCaixas(iCaixa).prazo_aviso)) < Bg_DaData_ADO Then
                    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataProxima = True
                Else
                    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataProxima = False
                End If
            End If
        Else
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataUltrapassada = False
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataProxima = False
        End If
    End If
    
    If gLAB = "HFM" Then
        If RetornaNumAnaPendentes(estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).n_req, _
                                   estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).cod_tubo) > 0 Then
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).semAna = False
        Else
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).semAna = True
        End If
    Else
            estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).semAna = False
    
    End If
    estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase = ""
    If estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).lixo = True Then
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase = estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase & "Tubo no Lixo( " & estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_act & "); "
    ElseIf estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).descongelada = True Then
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase = estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase & "Tubo Desongelado( " & estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_act & "); "
    ElseIf estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).congelada = True Then
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase = estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase & "Tubo Congelado( " & estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_act & "); "
    End If
    If estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).semAna = True Then
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase = estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase & "Sem An�lises Pendentes; "
    End If
    If estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataUltrapassada = True Then
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase = estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase & "Data limite ultrapassada; "
    ElseIf estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dataProxima = True Then
        estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase = estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).frase & "Data limite Pr�xima; "
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "GereEstados: " & Err.Description, Me.Name, "GereEstados", True
    Exit Sub
    Resume Next
End Sub

' ---------------------------------------------------------------------------------

' Preenche datas de congelacao, descongelacao, lixo, etc

' ---------------------------------------------------------------------------------
Private Sub PreencheDatas(seq_reutil As Long, n_req As Long, iCaixa As Integer, iReutil As Integer, _
                            iArquivo As Integer, cod_tubo As String, aliquota As String)
    Dim sSql As String
    Dim rsMarca As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT * FROM sl_ser_alteracoes WHERE n_req = " & n_req & " AND seq_reutil = " & seq_reutil
    sSql = sSql & " AND cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
    If aliquota <> "" Then
        sSql = sSql & " AND aliquota = " & BL_TrataStringParaBD(aliquota)
    End If
    rsMarca.CursorType = adOpenStatic
    rsMarca.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsMarca.Open sSql, gConexao
    If rsMarca.RecordCount > 0 Then
        While Not rsMarca.EOF
            If BL_HandleNull(rsMarca!estado, "") = gEstadoSerotecaAtribuido Then
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_anexa = BL_HandleNull(rsMarca!dt_cri, "")
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).hr_anexa = BL_HandleNull(rsMarca!hr_cri, "")
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).user_anexa = BL_HandleNull(rsMarca!user_cri, "")
            ElseIf BL_HandleNull(rsMarca!estado, "") = gEstadoSerotecaCongelado Then
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_congelamento = BL_HandleNull(rsMarca!dt_cri, "")
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).hr_congelamento = BL_HandleNull(rsMarca!hr_cri, "")
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).user_congelamento = BL_HandleNull(rsMarca!user_cri, "")
            ElseIf BL_HandleNull(rsMarca!estado, "") = gEstadoSerotecaDescongelado Then
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_descongelamento = BL_HandleNull(rsMarca!dt_cri, "")
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).hr_descongelamento = BL_HandleNull(rsMarca!hr_cri, "")
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).user_descongelamento = BL_HandleNull(rsMarca!user_cri, "")
            ElseIf BL_HandleNull(rsMarca!estado, "") = gEstadoSerotecaLixo Then
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).dt_lixo = BL_HandleNull(rsMarca!dt_cri, "")
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).hr_lixo = BL_HandleNull(rsMarca!hr_cri, "")
                estrutCaixas(iCaixa).reutil(iReutil).estrutArquivo(iArquivo).user_lixo = BL_HandleNull(rsMarca!user_cri, "")
            End If
            rsMarca.MoveNext
        Wend
    End If
    rsMarca.Close
    Set rsMarca = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheDatas: " & Err.Description, Me.Name, "PreencheDatas", True
    Exit Sub
    Resume Next
End Sub


Private Function RetornaTubo(etiq_tubo As String) As String
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_tubo WHERE cod_etiq = " & BL_TrataStringParaBD(etiq_tubo)
    rsTubo.CursorType = adOpenStatic
    rsTubo.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTubo.Open sSql, gConexao
    If rsTubo.RecordCount = 1 Then
        RetornaTubo = BL_HandleNull(rsTubo!cod_tubo, "")
    End If
    rsTubo.Close
    Set rsTubo = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "RetornaTubo: " & Err.Description, Me.Name, "RetornaTubo", False
    Exit Function
    Resume Next
End Function

Private Function RetornaEtiq(cod_tubo As String) As String
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_tubo WHERE cod_tubo = " & BL_TrataStringParaBD(cod_tubo)
    rsTubo.CursorType = adOpenStatic
    rsTubo.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTubo.Open sSql, gConexao
    If rsTubo.RecordCount = 1 Then
        RetornaEtiq = BL_HandleNull(rsTubo!cod_etiq, "")
    End If
    rsTubo.Close
    Set rsTubo = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "RetornaEtiq: " & Err.Description, Me.Name, "RetornaEtiq", False
    Exit Function
    Resume Next
End Function

Private Function DevIndiceCaixa(iCaixa As Integer) As Integer
    Dim i As Integer
    For i = 1 To TV.Nodes.Count
        If TV.Nodes(i).Key = keyCaixa & estrutCaixas(iCaixa).cod_seroteca & "_" & estrutCaixas(iCaixa).cod_caixa Then
            DevIndiceCaixa = TV.Nodes(i).Index
        End If
    Next
End Function

Private Function DevIndiceCaixaDesactivos(iCaixa As Integer) As Integer
    Dim i As Integer
    For i = 1 To TV.Nodes.Count
        If TV.Nodes(i).Key = keyDesac & estrutCaixas(iCaixa).cod_seroteca & "_" & estrutCaixas(iCaixa).cod_caixa Then
            DevIndiceCaixaDesactivos = TV.Nodes(i).Index
        End If
    Next
End Function

Private Function DevIndiceReutil(iCaixa As Integer, iReutil As Integer) As Integer
    Dim i As Integer
    For i = 1 To TV.Nodes.Count
        If TV.Nodes(i).Key = keyReutil & estrutCaixas(iCaixa).cod_seroteca & "_" & estrutCaixas(iCaixa).cod_caixa & "_" & estrutCaixas(iCaixa).reutil(iReutil).seq_reutil Then
            DevIndiceReutil = TV.Nodes(i).Index
        End If
    Next
End Function


Private Function DevIndiceCaixaByCodCaixa(cod_caixa As Long, cod_sertoteca As Long) As Integer
    Dim i As Integer
    indiceCaixa = mediComboValorNull
    For i = 1 To totalCaixas
        If estrutCaixas(i).cod_caixa = cod_caixa And estrutCaixas(i).cod_seroteca = cod_sertoteca Then
            DevIndiceCaixaByCodCaixa = i
            Exit Function
        End If
    Next
End Function

Private Function DevIndiceReutilByCod(iCaixa As Integer, seq_reutil As Long) As Integer
    Dim i As Integer
    indiceReutil = mediComboValorNull
    For i = 1 To estrutCaixas(iCaixa).totalReutil
        If estrutCaixas(iCaixa).reutil(i).seq_reutil = seq_reutil Then
            DevIndiceReutilByCod = i
            Exit Function
        End If
    Next
End Function

Private Sub Tv_Click()
    On Error GoTo TrataErro
    Dim cod_caixa As Long
    Dim cod_seroteca As Long
    Dim seq_reutil As Long
    Dim resto As String
    FgCaixa.Visible = False
    DoEvents
    cod_seroteca = Mid(TV.Nodes(TV.SelectedItem.Index).Key, 5, InStr(1, TV.Nodes(TV.SelectedItem.Index).Key, "_") - 5)
    resto = Mid(TV.Nodes(TV.SelectedItem.Index).Key, 5 + Len(CStr(cod_seroteca)) + 1)
    cod_caixa = Mid(resto, 1, InStr(1, resto, "_") - 1)
    If TV.SelectedItem.Index > mediComboValorNull Then
        If Mid(TV.Nodes(TV.SelectedItem.Index).Key, 1, 4) = keyCaixa Then
            indiceCaixa = DevIndiceCaixaByCodCaixa(cod_caixa, cod_seroteca)
            indiceReutil = -1
        Else
            seq_reutil = Mid(TV.Nodes(TV.SelectedItem.Index).Key, InStr(InStr(1, TV.Nodes(TV.SelectedItem.Index).Key, "_") + 1, TV.Nodes(TV.SelectedItem.Index).Key, "_") + 1)
            indiceCaixa = DevIndiceCaixaByCodCaixa(cod_caixa, cod_seroteca)
            indiceReutil = DevIndiceReutilByCod(indiceCaixa, seq_reutil)
        End If
        If indiceCaixa > -1 And indiceReutil > -1 Then
            If estrutCaixas(indiceCaixa).reutil(indiceReutil).totalArquivos = 0 Then
                PreencheEstrutArquivo indiceCaixa, indiceReutil
            End If
        End If
        
        PreencheFgCaixa
    End If
    FgCaixa.Visible = True
    BtAnaPendentes.Enabled = True
Exit Sub
TrataErro:
    BtAnaPendentes.Enabled = False
    BG_LogFile_Erros "TV_Click: " & Err.Description, Me.Name, "TV_Click", False
    Exit Sub
    Resume Next
End Sub


Private Sub PreenhceTV()
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim root As Node
    Dim linha As String
    Dim coluna As String
    On Error GoTo TrataErro
    TV.Nodes.Clear
    
    For i = 1 To totalCaixas
        Set root = TV.Nodes.Add(, tvwChild, keyCaixa & estrutCaixas(i).cod_seroteca & "_" & estrutCaixas(i).cod_caixa, estrutCaixas(i).descr_caixa, 1)
        root.Expanded = False
        
        Set root = TV.Nodes.Add(DevIndiceCaixa(i), tvwChild, keyDesac & estrutCaixas(i).cod_seroteca & "_" & estrutCaixas(i).cod_caixa, "Arquivados", 3)
        
        For j = 1 To estrutCaixas(i).totalReutil
            If estrutCaixas(i).reutil(j).flg_activo = 1 Then
                Set root = TV.Nodes.Add(DevIndiceCaixa(i), tvwChild, keyReutil & estrutCaixas(i).cod_seroteca & "_" & estrutCaixas(i).cod_caixa & "_" & estrutCaixas(i).reutil(j).seq_reutil, estrutCaixas(i).reutil(j).seq_reutil & " - " & estrutCaixas(i).reutil(j).dt_cri & " - " & estrutCaixas(i).reutil(j).dt_act, 2)
            Else
                Set root = TV.Nodes.Add(DevIndiceCaixaDesactivos(i), tvwChild, keyReutil & estrutCaixas(i).cod_seroteca & "_" & estrutCaixas(i).cod_caixa & "_" & estrutCaixas(i).reutil(j).seq_reutil, estrutCaixas(i).reutil(j).seq_reutil & " - " & estrutCaixas(i).reutil(j).dt_cri & " - " & estrutCaixas(i).reutil(j).dt_act, 3)
            End If
            For k = 1 To estrutCaixas(i).reutil(j).totalArquivos
                coluna = estrutCaixas(i).reutil(j).estrutArquivo(k).pos_x
                linha = estrutCaixas(i).reutil(j).estrutArquivo(k).pos_y
                Select Case linha
                    Case "1"
                        linha = "A"
                    Case "2"
                        linha = "B"
                    Case "3"
                        linha = "C"
                    Case "4"
                        linha = "D"
                    Case "5"
                        linha = "E"
                    Case "6"
                        linha = "F"
                    Case "7"
                        linha = "G"
                    Case "8"
                        linha = "H"
                    Case "9"
                        linha = "I"
                    Case "10"
                        linha = "J"
                    Case "11"
                        linha = "K"
                    Case "12"
                        linha = "L"
                    Case "13"
                        linha = "M"
                    Case "14"
                        linha = "N"
                    Case Else
                        linha = linha
                End Select
                Set root = TV.Nodes.Add(DevIndiceReutil(i, j), tvwChild, keyArq & estrutCaixas(i).cod_seroteca & "_" & estrutCaixas(i).cod_caixa & "_" & estrutCaixas(i).reutil(j).seq_reutil & "_" & estrutCaixas(i).reutil(j).estrutArquivo(k).seq_arquivo, _
                           estrutCaixas(i).reutil(j).estrutArquivo(k).n_req & " - " & linha & "-" & coluna & " - " & estrutCaixas(i).reutil(j).estrutArquivo(k).cod_tubo & " - " & estrutCaixas(i).reutil(j).estrutArquivo(k).aliquota, 4)
                
            Next
            root.Expanded = False
        Next
    Next
Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

Private Sub FuncaoProcurarPendentes(cod_caixa As String)
    Dim sSql As String
    Dim rsPend As New ADODB.recordset
    Dim linhaActual As Integer
    Dim colunaActual As Integer
    On Error GoTo TrataErro
    LimpaFgPendentes
    totalEstrutPEnd = 0
    ReDim estrutPendentes(0)
    linhaActual = 0
    colunaActual = 0
    
    sSql = "SELECT distinct x1.n_req, x4.cod_ana, x4.descr_ana FROM sl_Requis x0, sl_marcacoes x1, sl_ser_caixa_ana x2,  slv_analises x4 "
    sSql = sSql & " WHERE x0.n_Req = x1.n_Req AND x1.cod_agrup = x4.cod_Ana and x2.cod_ana = x4.cod_ana and x2.cod_caixa = " & cod_caixa
    sSql = sSql & " AND x1.n_Req NOT IN (Select n_Req from sl_ser_arquivo WHERE cod_Caixa = " & cod_caixa & ")"
    'sSql = sSql & " AND x0.dt_chega BETWEEN " & BL_TrataDataParaBD(EcDtIni.Value) & " AND " & BL_TrataDataParaBD(EcDtFim.Value)
    sSql = sSql & " AND (x1.n_Req, x4.cod_tubo) IN (SELECT n_Req, cod_tubo FROM sl_Req_tubo x5 Where x5.n_Req = x0.n_Req AND x5.dt_chega BETWEEN "
    sSql = sSql & BL_TrataDataParaBD(EcDtIni.value) & " AND " & BL_TrataDataParaBD(EcDtFim.value)
    sSql = sSql & " AND x5.dt_eliminacao IS NULL)"

    sSql = sSql & " ORDER by n_Req ASC "
    rsPend.CursorType = adOpenStatic
    rsPend.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsPend.Open sSql, gConexao
    If rsPend.RecordCount >= 1 Then
        While Not rsPend.EOF
            
            If totalEstrutPEnd = 0 Then
                totalEstrutPEnd = totalEstrutPEnd + 1
                ReDim Preserve estrutPendentes(totalEstrutPEnd)
                estrutPendentes(totalEstrutPEnd).n_req = BL_HandleNull(rsPend!n_req, "")
                estrutPendentes(totalEstrutPEnd).linha = linhaActual
                estrutPendentes(totalEstrutPEnd).coluna = colunaActual
                estrutPendentes(totalEstrutPEnd).toolTip = ""
                estrutPendentes(totalEstrutPEnd).totalAnalises = 0
                estrutPendentes(totalEstrutPEnd).numero = 1
                ReDim estrutPendentes(totalEstrutPEnd).analises(estrutPendentes(totalEstrutPEnd).totalAnalises)
                FgAnaPendentes.TextMatrix(linhaActual, colunaActual) = estrutPendentes(totalEstrutPEnd).n_req
                FgAnaPendentes.TextMatrix(linhaActual, colunaActual) = estrutPendentes(totalEstrutPEnd).n_req & "(" & estrutPendentes(totalEstrutPEnd).numero & ")"
                If colunaActual = FgAnaPendentes.Cols - 1 Then
                    If linhaActual = FgAnaPendentes.rows - 1 Then
                        FgAnaPendentes.AddItem ""
                    End If
                    linhaActual = linhaActual + 1
                    colunaActual = 0
                Else
                    colunaActual = colunaActual + 1
                End If
            ElseIf estrutPendentes(totalEstrutPEnd).n_req <> BL_HandleNull(rsPend!n_req, "") Then
                totalEstrutPEnd = totalEstrutPEnd + 1
                ReDim Preserve estrutPendentes(totalEstrutPEnd)
                estrutPendentes(totalEstrutPEnd).n_req = BL_HandleNull(rsPend!n_req, "")
                estrutPendentes(totalEstrutPEnd).linha = linhaActual
                estrutPendentes(totalEstrutPEnd).coluna = colunaActual
                estrutPendentes(totalEstrutPEnd).toolTip = ""
                estrutPendentes(totalEstrutPEnd).totalAnalises = 0
                estrutPendentes(totalEstrutPEnd).numero = 1
                ReDim estrutPendentes(totalEstrutPEnd).analises(estrutPendentes(totalEstrutPEnd).totalAnalises)
                FgAnaPendentes.TextMatrix(linhaActual, colunaActual) = estrutPendentes(totalEstrutPEnd).n_req & "(" & estrutPendentes(totalEstrutPEnd).numero & ")"
                If colunaActual = FgAnaPendentes.Cols - 1 Then
                    If linhaActual = FgAnaPendentes.rows - 1 Then
                        FgAnaPendentes.AddItem ""
                    End If
                    linhaActual = linhaActual + 1
                    colunaActual = 0
                Else
                    colunaActual = colunaActual + 1
                End If
            End If
            estrutPendentes(totalEstrutPEnd).totalAnalises = estrutPendentes(totalEstrutPEnd).totalAnalises + 1
            ReDim estrutPendentes(totalEstrutPEnd).analises(estrutPendentes(totalEstrutPEnd).totalAnalises)
            estrutPendentes(totalEstrutPEnd).analises(estrutPendentes(totalEstrutPEnd).totalAnalises).cod_ana = BL_HandleNull(rsPend!cod_ana, "")
            estrutPendentes(totalEstrutPEnd).analises(estrutPendentes(totalEstrutPEnd).totalAnalises).descr_ana = BL_HandleNull(rsPend!descr_ana, "")
            estrutPendentes(totalEstrutPEnd).toolTip = estrutPendentes(totalEstrutPEnd).toolTip & estrutPendentes(totalEstrutPEnd).analises(estrutPendentes(totalEstrutPEnd).totalAnalises).descr_ana & vbCrLf
            rsPend.MoveNext
        Wend
    End If
    rsPend.Close
    Set rsPend = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "FuncaoProcurarPendentes: " & Err.Description, Me.Name, "FuncaoProcurarPendentes", False
    Exit Sub
    Resume Next
End Sub
Private Sub LimpaFgPendentes()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim j As Integer
    FgAnaPendentes.rows = linhasPend
    FgAnaPendentes.Cols = colunasPend
    For i = 0 To linhasPend - 1
        For j = 0 To colunasPend - 1
            FgAnaPendentes.TextMatrix(i, j) = ""
        Next j
    Next i
Exit Sub
TrataErro:
    BG_LogFile_Erros "LimpaFgPendentes: " & Err.Description, Me.Name, "LimpaFgPendentes", False
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' RETORNA A COLUNA ACTUAL

' ----------------------------------------------------------------------
Private Sub FgPend_DevolveColunaActual(X As Single)
    On Error GoTo TrataErro
    Dim coluna As Integer
    Dim posicao As Double
    posicao = FgAnaPendentes.ColPos(coluna)
    If posicao < 0 Then
        posicao = 0
    End If
    For coluna = 0 To FgAnaPendentes.Cols - 1
        If FgAnaPendentes.ColPos(coluna) <= X And FgAnaPendentes.ColPos(coluna) + FgAnaPendentes.ColWidth(coluna) >= X Then
            colunaTooltip = coluna
            Exit Sub
        End If
    Next
    colunaTooltip = -1
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgPend_DevolveColunaActual: " & Err.Description, Me.Name, "FgPend_DevolveColunaActual", False
    Exit Sub
    Resume Next
End Sub


' ----------------------------------------------------------------------

' RETORNA A LINHA ACTUAL

' ----------------------------------------------------------------------
Private Sub FgPend_DevolveLinhaActual(Y As Single)
    On Error GoTo TrataErro
    Dim linha As Integer
        For linha = 0 To FgAnaPendentes.rows - 1
            If FgAnaPendentes.RowPos(linha) <= Y And FgAnaPendentes.RowPos(linha) + FgAnaPendentes.RowHeight(linha) >= Y Then
                linhaToolTip = linha
                Exit Sub
            End If
        Next
        linhaToolTip = -1
Exit Sub
TrataErro:
    BG_LogFile_Erros "FgPend_DevolveLinhaActual: " & Err.Description, Me.Name, "FgPend_DevolveLinhaActual", False
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------

' IMPRIME RESUMO SOBRE DADOS ARQUIVADOS

' ----------------------------------------------------------------------
Private Sub ImprimeResumoArquivo()
    On Error GoTo TrataErro
    Dim i As Integer
    Dim sSql As String
    Dim continua As Boolean
    sSql = " DELETE FROM Sl_cr_seroteca_arquivo WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
    
    For i = 1 To totalArquivoImpr
        sSql = "INSERT INTO sl_cr_seroteca_arquivo(nome_computador, num_sessao, seq_arquivo) VALUES("
        sSql = sSql & BL_TrataStringParaBD(gComputador) & "," & gNumeroSessao & ","
        sSql = sSql & estrutArquivoImpr(i).seqArquivo & ")"
        BG_ExecutaQuery_ADO sSql
    Next i
    
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("SerotecaArquivo", "Seroteca - Arquivo", crptToPrinter)
    Else
        continua = BL_IniciaReport("SerotecaArquivo", "Seroteca - Arquivo", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    
    
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT sl_cr_seroteca_arquivo.nome_computador, sl_cr_seroteca_arquivo.num_sessao, sl_cr_seroteca_arquivo.seq_arquivo, "
    Report.SQLQuery = Report.SQLQuery & " sl_ser_arquivo.n_Req, sl_ser_arquivo.pos_x, sl_ser_arquivo.pos_y,sl_ser_arquivo.posicao,sl_ser_caixas.descr_caixa,"
    Report.SQLQuery = Report.SQLQuery & " sl_identif.nome_ute, sl_tbf_estado_seroteca.descr_estado "
    Report.SQLQuery = Report.SQLQuery & " FROM sl_ser_arquivo, sl_cr_seroteca_arquivo, sl_ser_caixas, sl_requis, sl_identif, sl_tbf_estado_seroteca "
    Report.SQLQuery = Report.SQLQuery & "  WHERE sl_cr_seroteca_arquivo.seq_arquivo = sl_Ser_arquivo.seq_arquivo"
    Report.SQLQuery = Report.SQLQuery & "  AND sl_ser_arquivo.cod_caixa = sl_Ser_caixas.cod_caixa "
    Report.SQLQuery = Report.SQLQuery & "  AND sl_ser_arquivo.cod_seroteca = sl_Ser_caixas.cod_seroteca "
    Report.SQLQuery = Report.SQLQuery & "  AND sl_requis.n_req = sl_ser_arquivo.n_req AND sl_requis.seq_utente = sl_identif.seq_utente "
    Report.SQLQuery = Report.SQLQuery & "  AND sl_cr_seroteca_arquivo.nome_computador = " & BL_TrataStringParaBD(gComputador)
    Report.SQLQuery = Report.SQLQuery & "  AND sl_cr_seroteca_arquivo.num_sessao = " & gNumeroSessao
    Report.SQLQuery = Report.SQLQuery & "  AND sl_ser_arquivo.estado = sl_tbf_estado_seroteca.cod_estado"
    Report.SQLQuery = Report.SQLQuery & "  ORDER BY sl_ser_arquivo.n_req ASC "
    Call BL_ExecutaReport
 
    sSql = " DELETE FROM Sl_cr_seroteca_arquivo WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql

Exit Sub
TrataErro:
    BG_LogFile_Erros "ImprimeResumoArquivo: " & Err.Description, Me.Name, "ImprimeResumoArquivo", False
    Exit Sub
    Resume Next
End Sub




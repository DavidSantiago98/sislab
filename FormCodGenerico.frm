VERSION 5.00
Begin VB.Form FormCodGenerico 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCodGenerico"
   ClientHeight    =   8520
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   7800
   Icon            =   "FormCodGenerico.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8520
   ScaleWidth      =   7800
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox extraCheckbox 
      Height          =   255
      Index           =   0
      Left            =   1680
      TabIndex        =   29
      Top             =   6120
      Value           =   2  'Grayed
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox extraTextbox 
      Height          =   495
      Index           =   0
      Left            =   5640
      TabIndex        =   28
      Top             =   6000
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ComboBox extraComboBox 
      Height          =   315
      Index           =   0
      Left            =   3840
      Style           =   2  'Dropdown List
      TabIndex        =   26
      Top             =   6600
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Frame FrLista 
      Height          =   3615
      Left            =   0
      TabIndex        =   15
      Top             =   960
      Width           =   7815
      Begin VB.ListBox EcLista 
         Appearance      =   0  'Flat
         Height          =   1980
         Left            =   120
         TabIndex        =   23
         Top             =   360
         Width           =   7605
      End
      Begin VB.Frame Frame2 
         Height          =   825
         Left            =   135
         TabIndex        =   16
         Top             =   2640
         Width           =   7605
         Begin VB.Label LaDataAlteracao 
            Enabled         =   0   'False
            Height          =   255
            Left            =   2880
            TabIndex        =   22
            Top             =   480
            Width           =   2175
         End
         Begin VB.Label LaDataCriacao 
            Enabled         =   0   'False
            Height          =   255
            Left            =   2880
            TabIndex        =   21
            Top             =   195
            Width           =   2175
         End
         Begin VB.Label LaUtilAlteracao 
            Enabled         =   0   'False
            Height          =   255
            Left            =   1080
            TabIndex        =   20
            Top             =   480
            Width           =   1695
         End
         Begin VB.Label LaUtilCriacao 
            Enabled         =   0   'False
            Height          =   255
            Left            =   1080
            TabIndex        =   19
            Top             =   200
            Width           =   1695
         End
         Begin VB.Label Label5 
            Caption         =   "Altera��o"
            Height          =   255
            Left            =   240
            TabIndex        =   18
            Top             =   480
            Width           =   705
         End
         Begin VB.Label Label8 
            Caption         =   "Cria��o"
            Height          =   255
            Left            =   240
            TabIndex        =   17
            Top             =   195
            Width           =   675
         End
      End
      Begin VB.Label Label9 
         Caption         =   "Descri��o"
         Height          =   255
         Left            =   1620
         TabIndex        =   25
         Top             =   120
         Width           =   1065
      End
      Begin VB.Label Label7 
         Caption         =   "C�digo"
         Height          =   255
         Left            =   180
         TabIndex        =   24
         Top             =   120
         Width           =   1335
      End
   End
   Begin VB.TextBox EcHoraCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5400
      TabIndex        =   14
      Top             =   7320
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcHoraAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5400
      TabIndex        =   13
      Top             =   7680
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2160
      TabIndex        =   8
      Top             =   6600
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4920
      TabIndex        =   7
      Top             =   7680
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2160
      TabIndex        =   6
      Top             =   7320
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4920
      TabIndex        =   5
      Top             =   7320
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.CheckBox CkInvisivel 
      Appearance      =   0  'Flat
      Caption         =   "Cancelado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   6000
      TabIndex        =   2
      Top             =   120
      Width           =   2535
   End
   Begin VB.TextBox EcDescricao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1380
      TabIndex        =   1
      Top             =   480
      Width           =   4035
   End
   Begin VB.TextBox EcCodigo 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1380
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label extraLabel 
      Height          =   255
      Index           =   0
      Left            =   3720
      TabIndex        =   27
      Top             =   6120
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   375
      Left            =   600
      TabIndex        =   12
      Top             =   7320
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label11 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   375
      Left            =   480
      TabIndex        =   11
      Top             =   6600
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataCriacao"
      Height          =   375
      Left            =   3600
      TabIndex        =   10
      Top             =   7320
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label13 
      Caption         =   "EcDataAlteracao"
      Height          =   375
      Left            =   3480
      TabIndex        =   9
      Top             =   7680
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label LaDescricao 
      Caption         =   "Descricao"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "FormCodGenerico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 24/04/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos

Public rs As ADODB.recordset

'RGONCALVES 26.07.2013 Glintt-HS-4809
Dim NumCamposExtra As Integer
Dim TipoCamposExtra() As String
Dim NomeCamposExtra() As String
Dim CamposExtraEC() As Object
Dim CamposExtraBD() As String
Attribute CamposExtraBD.VB_VarHelpID = -1
'

Private Sub EcLista_Click()
    
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If

End Sub

Private Sub EcCodigo_GotFocus()
    
    Set CampoActivo = EcCodigo

End Sub

Private Sub EcCodigo_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo
    EcCodigo.Text = UCase(EcCodigo.Text)

End Sub

Private Sub EcDescricao_GotFocus()
    
    Set CampoActivo = EcDescricao

End Sub

Private Sub EcDescricao_LostFocus()
    
    BG_ValidaTipoCampo_ADO Me, CampoActivo

End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes(Tabela As String, tituloForm As String, campoCodigo As String, campoDescricao As String)
    Me.caption = tituloForm
    Me.left = 50
    Me.top = 50
    Me.Width = 7980
    Me.Height = FrLista.top + FrLista.Height + 500
    'Me.Height = 5025 ' Normal
    'Me.Height = 8330 ' Campos Extras
    NomeTabela = Tabela
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 9
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = campoCodigo
    CamposBD(1) = campoDescricao
    CamposBD(2) = "user_cri"
    CamposBD(3) = "dt_cri"
    CamposBD(4) = "user_act"
    CamposBD(5) = "dt_act"
    CamposBD(6) = "flg_invisivel"
    CamposBD(7) = "hr_cri"
    CamposBD(8) = "hr_act"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcDescricao
    Set CamposEc(2) = EcUtilizadorCriacao
    Set CamposEc(3) = EcDataCriacao
    Set CamposEc(4) = EcUtilizadorAlteracao
    Set CamposEc(5) = EcDataAlteracao
    Set CamposEc(6) = CkInvisivel
    Set CamposEc(7) = EcHoraCriacao
    Set CamposEc(8) = EcHoraAlteracao
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Descri��o"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    Set ChaveEc = EcCodigo
    ChaveBD = campoCodigo
    CamposBDparaListBox = Array(campoCodigo, campoDescricao)
    
    'CamposBDparaListBox = Array("cod_postal", "area_geografica")
    NumEspacos = Array(13, 100)
    
    'RGONCALVES 26.07.2013 Glintt-HS-4809
    FrLista.BorderStyle = 0

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BG_StackJanelas_Push Me
    Set gFormActivo = Me
    
    BL_InicioProcessamento Me, "Inicializar �cran."

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormCodGenerico = Nothing

End Sub

Sub LimpaCampos()
    
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    CkInvisivel.Visible = True
End Sub

Sub DefTipoCampos()
    
    Dim i As Integer
    'RGONCALVES 26.07.2013 Glintt-HS-4809
    If NumCamposExtra > 0 Then
        For i = LBound(CamposExtraEC) To UBound(CamposExtraEC)
            If UCase(TipoCamposExtra(i)) = "DATA" Then
                 BG_DefTipoCampoEc_ADO NomeTabela, CamposExtraBD(i), CamposExtraEC(i), mediTipoData
            End If
        Next i
    End If
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito

End Sub

Sub PreencheValoresDefeito()
    CkInvisivel.value = vbGrayed
    DoEvents

End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        If CkInvisivel.value = vbChecked Then
            CkInvisivel.Visible = True
        Else
            CkInvisivel.Visible = False
        End If
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao & " " & EcHoraCriacao
        LaDataAlteracao = EcDataAlteracao & " " & EcHoraAlteracao
    End If
    
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    Dim str_aux1 As String
    Dim str_aux2 As String
    Dim str_aux3 As String
    
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
               
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)

              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        
        BG_PreencheListBoxMultipla_ADO EcLista, _
                                       rs, _
                                       CamposBDparaListBox, _
                                       NumEspacos, _
                                       CamposBD, _
                                       CamposEc, _
                                       "SELECT"
        
        ' Fim do preenchimento de 'EcLista'

        PreencheCampos
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = CDate(Bg_DaData_ADO)
        EcHoraCriacao = Bg_DaHora_ADO
        iRes = ValidaCamposEc
        If CkInvisivel.value = vbGrayed Then
            CkInvisivel.value = vbUnchecked
        End If
        If iRes = True Then
            BD_Insert
        End If
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao & " " & EcHoraCriacao
        LaDataAlteracao = EcDataAlteracao & " " & EcHoraAlteracao
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    EcCodigo = BL_HandleNull(BG_DaMAX(NomeTabela, ChaveBD), 0) + 1
    
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = CDate(Bg_DaData_ADO)
        EcHoraAlteracao = Bg_DaHora_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        If CkInvisivel.value = vbGrayed Then
            CkInvisivel.value = vbUnchecked
        End If
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao & " " & EcHoraCriacao
        LaDataAlteracao = EcDataAlteracao & " " & EcHoraAlteracao
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        
    
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
        
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal

End Sub

Sub FuncaoRemover()
    Dim iRes As Boolean
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer cancelar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        CkInvisivel.value = vbChecked
        EcDataAlteracao = CDate(Bg_DaData_ADO & " " & Bg_DaHora_ADO)
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If
    
End Sub




Public Sub InicializaCodificacao(Tabela As String, tituloForm As String, campoCodigo As String, campoDescricao As String)
    Inicializacoes Tabela, tituloForm, campoCodigo, campoDescricao
    'RGONCALVES 26.07.2013 Glintt-HS-4809
    AdicionaControlosExtra (Tabela)
    '
    FuncaoLimpar
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BL_ToolbarEstadoN estado
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub
'RGONCALVES 26.07.2013 Glintt-HS-4809
Private Sub AdicionaControlosExtra(ByVal Tabela As String)
    On Error GoTo TrataErro
    Dim sql As String
    Dim Codigo As String
    Dim tipo_controlo As String
    Dim descricao As String
    Dim nome_campo As String
    Dim tipo_campo As String
    Dim tabela_valores As String
    Dim cod_tabela_valores As String
    Dim descr_tabela_valores As String
    Dim localizacao_livre As Boolean
    Dim rs As ADODB.recordset
    Dim LabelCoordenadaX As Long
    Dim LabelCoordenadaY As Long
    Dim CampoCoordenadaX As Long
    Dim CampoCoordenadaY As Long
    Dim espacamentoVertical As Long
    Dim espacamentoHorizontal As Long
    Dim larguraCampoExtra As Long
    Dim alturaCampoExtra As Long
    
    LabelCoordenadaX = LaDescricao.left
    LabelCoordenadaY = LaDescricao.top
    CampoCoordenadaX = EcDescricao.left
    CampoCoordenadaY = EcDescricao.top
    espacamentoVertical = EcDescricao.top - EcCodigo.top
    espacamentoHorizontal = CampoCoordenadaX - LabelCoordenadaX
    larguraCampoExtra = EcDescricao.Width
    alturaCampoExtra = EcDescricao.Height
    
    sql = "SELECT * FROM sl_ass_controlos_form WHERE UPPER(nome_tabela) = " & BL_TrataStringParaBD(UCase(Trim(Tabela))) & _
        " ORDER BY ordem "
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open sql, gConexao
    
    While Not rs.EOF
        
        Codigo = rs!Codigo
        tipo_controlo = Trim(UCase(rs!tipo_controlo))
        descricao = Trim(BL_HandleNull(rs!descricao, ""))
        nome_campo = Trim(UCase(rs!nome_campo))
        tipo_campo = Trim(UCase(BL_HandleNull(rs!tipo_campo, "")))
        tabela_valores = Trim(UCase(BL_HandleNull(rs!tabela_valores, "")))
        cod_tabela_valores = Trim(UCase(BL_HandleNull(rs!cod_tabela_valores, "")))
        descr_tabela_valores = Trim(UCase(BL_HandleNull(rs!descr_tabela_valores, "")))
        localizacao_livre = IIf(BL_HandleNull(rs!localizacao_livre, 0) = 1, True, False)
        If tipo_controlo = "TEXTBOX" Then
        
            IncrementaCampos (tipo_campo)
            Load extraTextbox(NumCamposExtra)
            'Set extraTextbox = Me.Controls.Add("VB.TextBox", NomeCamposExtra(NumCamposExtra - 1))
            extraTextbox(NumCamposExtra).Move CampoCoordenadaX, CampoCoordenadaY + espacamentoVertical, larguraCampoExtra, alturaCampoExtra
            extraTextbox(NumCamposExtra).Visible = True
            extraTextbox(NumCamposExtra).Appearance = 0
            
            Call AdicionaDetalhePropriedades(Codigo, extraTextbox(NumCamposExtra), tipo_controlo)
            Call AdicionaCamposEC(extraTextbox(NumCamposExtra), nome_campo)
            
            If localizacao_livre = True Then
                Call AdicionaLabel(descricao, extraTextbox(NumCamposExtra).left - espacamentoHorizontal, extraTextbox(NumCamposExtra).top, espacamentoVertical, espacamentoHorizontal)
            Else
                Call AdicionaLabel(descricao, LabelCoordenadaX, LabelCoordenadaY + espacamentoVertical, espacamentoVertical, espacamentoHorizontal)
            
                LabelCoordenadaY = LabelCoordenadaY + espacamentoVertical
                CampoCoordenadaY = CampoCoordenadaY + espacamentoVertical
                
            End If
            
            If extraTextbox(NumCamposExtra).top + extraTextbox(NumCamposExtra).Height > FrLista.top Then
                FrLista.top = extraTextbox(NumCamposExtra).top + extraTextbox(NumCamposExtra).Height
            End If
                
        ElseIf tipo_controlo = "CHECKBOX" Then
            
            IncrementaCampos (tipo_campo)
            Load extraCheckbox(NumCamposExtra)
            'Set extraCheckbox = Me.Controls.Add("VB.CheckBox", NomeCamposExtra(NumCamposExtra - 1))
            extraCheckbox(NumCamposExtra).Move CampoCoordenadaX, CampoCoordenadaY + espacamentoVertical, larguraCampoExtra, alturaCampoExtra
            extraCheckbox(NumCamposExtra).Visible = True
            
            Call AdicionaDetalhePropriedades(Codigo, extraCheckbox(NumCamposExtra), tipo_controlo)
            Call AdicionaCamposEC(extraCheckbox(NumCamposExtra), nome_campo)
            
            If localizacao_livre = True Then
                Call AdicionaLabel(descricao, extraCheckbox(NumCamposExtra).left - espacamentoHorizontal, extraCheckbox(NumCamposExtra).top, espacamentoVertical, espacamentoHorizontal)
            Else
                Call AdicionaLabel(descricao, LabelCoordenadaX, LabelCoordenadaY + espacamentoVertical, espacamentoVertical, espacamentoHorizontal)
            
                LabelCoordenadaY = LabelCoordenadaY + espacamentoVertical
                CampoCoordenadaY = CampoCoordenadaY + espacamentoVertical
                
            End If
            
            If extraCheckbox(NumCamposExtra).top + extraCheckbox(NumCamposExtra).Height > FrLista.top Then
                FrLista.top = extraCheckbox(NumCamposExtra).top + extraCheckbox(NumCamposExtra).Height
            End If
        ElseIf tipo_controlo = "COMBOBOX" Then
            
            IncrementaCampos (tipo_campo)
            Load extraComboBox(NumCamposExtra)
            extraComboBox(NumCamposExtra).Move CampoCoordenadaX, CampoCoordenadaY + espacamentoVertical, larguraCampoExtra
            extraComboBox(NumCamposExtra).Visible = True
            If tabela_valores <> "" And cod_tabela_valores <> "" And descr_tabela_valores <> "" Then
                BG_PreencheComboBD_ADO tabela_valores, cod_tabela_valores, descr_tabela_valores, extraComboBox(NumCamposExtra), mediAscComboDesignacao
            End If
            
            Call AdicionaDetalhePropriedades(Codigo, extraComboBox(NumCamposExtra), tipo_controlo)
            Call AdicionaCamposEC(extraComboBox(NumCamposExtra), nome_campo)
            
            If localizacao_livre = True Then
                Call AdicionaLabel(descricao, extraComboBox(NumCamposExtra).left - espacamentoHorizontal, extraComboBox(NumCamposExtra).top, espacamentoVertical, espacamentoHorizontal)
            Else
                Call AdicionaLabel(descricao, LabelCoordenadaX, LabelCoordenadaY + espacamentoVertical, espacamentoVertical, espacamentoHorizontal)
            
                LabelCoordenadaY = LabelCoordenadaY + espacamentoVertical
                CampoCoordenadaY = CampoCoordenadaY + espacamentoVertical
                
            End If
            
            If extraComboBox(NumCamposExtra).top + extraComboBox(NumCamposExtra).Height > FrLista.top Then
                FrLista.top = extraComboBox(NumCamposExtra).top + extraComboBox(NumCamposExtra).Height
            End If
        End If
        rs.MoveNext
    Wend
    
    Me.Height = FrLista.top + FrLista.Height + espacamentoVertical
    
    Exit Sub
TrataErro:
    BG_LogFile_Erros "AdicionaControlosExtra: " & Err.Number & " - " & Err.Description & " - ", Me.Name, "AdicionaControlosExtra"
    Call BG_Mensagem(mediMsgBox, "Erro ao carregar controlos extra.", vbOKOnly + vbError, App.ProductName)
    Exit Sub
    Resume Next

End Sub

'RGONCALVES 26.07.2013 Glintt-HS-4809
Private Sub IncrementaCampos(ByVal tipo_campo As String)
    NumCamposExtra = NumCamposExtra + 1
    If NumCamposExtra = 1 Then
        ReDim TipoCamposExtra(NumCamposExtra - 1)
        ReDim NomeCamposExtra(NumCamposExtra - 1)
        ReDim CamposExtraEC(NumCamposExtra - 1)
        ReDim CamposExtraBD(NumCamposExtra - 1)
    Else
        ReDim Preserve TipoCamposExtra(NumCamposExtra - 1)
        ReDim Preserve NomeCamposExtra(NumCamposExtra - 1)
        ReDim Preserve CamposExtraEC(NumCamposExtra - 1)
        ReDim Preserve CamposExtraBD(NumCamposExtra - 1)
    End If
    NomeCamposExtra(NumCamposExtra - 1) = "CamposExtra" & NumCamposExtra
    If tipo_campo = "DATA" Then
        TipoCamposExtra(NumCamposExtra - 1) = "DATA"
    Else
        TipoCamposExtra(NumCamposExtra - 1) = "DEFAULT"
    End If
End Sub

'RGONCALVES 26.07.2013 Glintt-HS-4809
Private Sub AdicionaCamposEC(ByRef controlo As Control, ByVal campoBD As String)
    NumCampos = NumCampos + 1
    ReDim Preserve CamposBD(0 To NumCampos - 1)
    ReDim Preserve CamposEc(0 To NumCampos - 1)
    ReDim Preserve TextoCamposObrigatorios(0 To NumCampos - 1)
    CamposBD(UBound(CamposBD)) = campoBD
    CamposExtraBD(UBound(CamposExtraBD)) = campoBD
    Set CamposEc(UBound(CamposEc)) = controlo
    Set CamposExtraEC(UBound(CamposExtraEC)) = controlo
End Sub

'RGONCALVES 26.07.2013 Glintt-HS-4809
Private Sub AdicionaLabel(ByVal descricao As String, ByVal LabelCoordenadaX As Long, ByVal LabelCoordenadaY As Long, ByVal espacamentoVertical As Long, ByVal tamanhoLabelExtra As Long)
    
    If Trim(descricao) = "" Then
        Exit Sub
    End If
    Load extraLabel(NumCamposExtra)
    'Set extraLabel = Me.Controls.Add("VB.Label", "ExtraLabel" & NumCamposExtra - 1)
    extraLabel(NumCamposExtra).Move LabelCoordenadaX, LabelCoordenadaY, tamanhoLabelExtra, espacamentoVertical
    extraLabel(NumCamposExtra).caption = descricao
    extraLabel(NumCamposExtra).Visible = True
    extraLabel(NumCamposExtra).WordWrap = True
End Sub

'RGONCALVES 26.07.2013 Glintt-HS-4809
Private Sub AdicionaDetalhePropriedades(ByVal Codigo As Integer, ByRef controlo As Control, ByVal tipo_controlo As String)
    Dim sql As String
    Dim RsDet As ADODB.recordset
    Dim propriedade As String
    Dim valor As String
    
    sql = "SELECT * FROM sl_ass_controlos_form_det WHERE codigo_ass_controlo = " & Codigo

    Set RsDet = New ADODB.recordset
    RsDet.CursorLocation = adUseServer
    RsDet.CursorType = adOpenStatic
    RsDet.Open sql, gConexao
    While Not RsDet.EOF
        propriedade = Trim(UCase(BL_HandleNull(RsDet!propriedade, "")))
        valor = Trim(BL_HandleNull(RsDet!valor, ""))
        Select Case propriedade
            Case "LEFT"
            controlo.left = valor
            Case "TOP"
            controlo.top = valor
            Case "WIDTH"
            controlo.Width = valor
            Case "HEIGHT"
            controlo.Height = valor
            Case "ENABLED"
            controlo.Enabled = valor
            Case "TEXT"
            controlo.Text = valor
            Case "CAPTION"
            controlo.caption = valor
        End Select
        RsDet.MoveNext
    Wend
End Sub

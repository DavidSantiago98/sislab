VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FormListaFacturacao 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   5220
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4815
   Icon            =   "FormListaFacturacao.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5220
   ScaleWidth      =   4815
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CbRequis 
      Height          =   315
      Left            =   3240
      TabIndex        =   14
      Top             =   2520
      Width           =   1455
   End
   Begin VB.CheckBox Flg_DescrRequis 
      Caption         =   "DescriminarRequisi��es"
      Height          =   255
      Left            =   360
      TabIndex        =   13
      Top             =   4320
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.CommandButton BtFactConsDados 
      Caption         =   "&Consulta Dados Factura��o"
      Height          =   375
      Left            =   120
      TabIndex        =   10
      Top             =   4680
      Width           =   4575
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   120
      MultiSelect     =   2  'Extended
      TabIndex        =   9
      Top             =   360
      Width           =   4590
   End
   Begin VB.TextBox EcCodRequisicao 
      Height          =   285
      Left            =   1320
      TabIndex        =   8
      Top             =   2520
      Width           =   855
   End
   Begin VB.Frame Frame1 
      Caption         =   "Data de Chegada"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      TabIndex        =   0
      Top             =   2880
      Width           =   3675
      Begin VB.TextBox EcDataFim 
         Height          =   285
         Left            =   2280
         TabIndex        =   3
         Top             =   480
         Width           =   975
      End
      Begin VB.TextBox EcDataInicio 
         Height          =   285
         Left            =   600
         TabIndex        =   2
         Top             =   480
         Width           =   975
      End
      Begin VB.CheckBox EcTodas 
         Caption         =   "Incluir todas as entidades"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   840
         Width           =   2415
      End
      Begin MSComCtl2.DTPicker EcEscolhaInicio 
         Height          =   255
         Left            =   1200
         TabIndex        =   4
         Top             =   480
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   450
         _Version        =   393216
         Format          =   145031169
         CurrentDate     =   37720
         MinDate         =   2
      End
      Begin MSComCtl2.DTPicker EcEscolhaFim 
         Height          =   255
         Left            =   2880
         TabIndex        =   5
         Top             =   480
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   450
         _Version        =   393216
         Format          =   145031169
         CurrentDate     =   37720
         MinDate         =   2
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "De"
         Height          =   195
         Left            =   240
         TabIndex        =   7
         Top             =   480
         Width           =   210
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   "a"
         Height          =   195
         Left            =   1920
         TabIndex        =   6
         Top             =   480
         Width           =   225
      End
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Requisi��es"
      Height          =   195
      Left            =   2280
      TabIndex        =   15
      Top             =   2520
      Width           =   870
   End
   Begin VB.Label LaEntidades 
      AutoSize        =   -1  'True
      Caption         =   "Entidades"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Width           =   855
   End
   Begin VB.Label LaNrInscricao 
      AutoSize        =   -1  'True
      Caption         =   "Nr. Requisi��o"
      Height          =   195
      Left            =   120
      TabIndex        =   11
      Top             =   2520
      Width           =   1050
   End
End
Attribute VB_Name = "FormListaFacturacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Private Sub EcEscolhaInicio_CloseUp()
    EcDataInicio = EcEscolhaInicio.value
    EcDataInicio.SetFocus
End Sub

Private Sub EcEscolhaFim_CloseUp()
    EcDataFim = EcEscolhaFim.value
    EcDataFim.SetFocus
End Sub
Sub BtFactConsDados_Click()
    Dim sql As String
    Dim continua As Boolean
    
    If EcCodRequisicao.Text = "" Then
        If (EcDataInicio = "" Or EcDataFim = "") Then
            BG_Mensagem mediMsgBox, "Intervalo de Datas n�o v�lido!", vbExclamation, Me.caption
            Exit Sub
        ElseIf CDate(EcDataInicio) > CDate(EcDataFim) Then
            BG_Mensagem mediMsgBox, "Intervalo de Datas n�o v�lido!", vbExclamation, Me.caption
            Exit Sub
        End If
    End If
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto("Lista Factura��o") = True Then Exit Sub
 
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("ListaFacturacao", "Lista Factura��o", crptToPrinter)
    Else
        continua = BL_IniciaReport("ListaFacturacao", "Lista Factura��o", crptToWindow)
    End If
    If continua = False Then Exit Sub
    
    If gTipoInstituicao = "HOSPITALAR" Then
        If StrComp(UCase(gSISTEMA_FACTURACAO), UCase("SONHO"), vbTextCompare) = 0 Then
            If Consulta_Dados_Fact_SONHO = -1 Then Exit Sub
        ElseIf UCase(gSISTEMA_FACTURACAO) = UCase("FACTUS") Then
            If Consulta_Dados_Fact_FACTUS = -1 Then Exit Sub
        ElseIf UCase(gSISTEMA_FACTURACAO) = UCase("GH") Then
            If Consulta_Dados_Fact_GH = -1 Then Exit Sub
        End If
    Else
        If Consulta_Dados_Fact_PRIVADO = -1 Then Exit Sub
    End If
    'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SelectionFormula = "{SL_CR_LISTA_FACT.nome_computador} = '" & BG_SYS_GetComputerName & "'"
    
    'F�rmulas do Report
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD("" & EcDataInicio.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD("" & EcDataFim.Text)
    Report.formulas(2) = "Requis=" & BL_TrataStringParaBD("" & CbRequis.Text)
    If Flg_DescrRequis.value = 1 Then
        Report.formulas(3) = "DescrRequis=" & BL_TrataStringParaBD("S")
    Else
        Report.formulas(3) = "DescrRequis=" & BL_TrataStringParaBD("N")
    End If
    'Me.SetFocus
    
    Call BL_ExecutaReport
    'Report.PageShow Report.ReportLatestPage
 
    'Elimina as Tabelas criadas para a impress�o dos relat�rios
    sql = "delete from SL_CR_LISTA_FACT where nome_computador = '" & BG_SYS_GetComputerName & "'"
    BG_ExecutaQuery_ADO sql

End Sub

Sub EcDataFim_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Sub EcDataFim_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Sub EcDataInicio_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Sub EcDataInicio_LostFocus()
    BG_ValidaTipoCampo Me, CampoActivo
End Sub

Sub EcTodas_Click()
    Dim i As Integer

    If EcTodas.value = 1 Then
        For i = 0 To EcLista.ListCount - 1
            If EcLista.Selected(i) = True Then
                EcLista.Selected(i) = False
            End If
        Next i
        EcLista.Enabled = False

        Exit Sub
    End If

    EcLista.Enabled = True

End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Function Consulta_Dados_Fact_FACTUS() As Integer
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim lNrFact As Long, lNrReq As Long, lInscricao As Long
    Dim lCount As Long
    Dim lExame As Long, lEntidade As Long
    Dim sTDoente As String, sTEpisodio As String, sEpisodio As String
    Dim sIsento As String, sBenef As String, sServico As String, sEFR As String
    Dim sExame As String, sSigla As String, sNrIncidencias As String, sFlgAmbInt As String
    Dim iCodNaoIsento As Integer, i As Integer, iRes As Integer, j As Integer
    Dim iBD_Aberta As Integer
    Dim bFacturar As Boolean
    Dim CodAnaFACTUS As String
    Dim l As Integer
    Dim ContaRubFact As Long
    Dim HisAberto As Integer
    Dim NReq As Long
    Dim Flg_Facturado As String
    Dim estado_req As String
    Dim flg_estado As String
    Dim sql As String

    On Error GoTo TrataErro

    BL_InicioProcessamento Me, "A gerar impress�o... "
    DoEvents

    sSql = "Select distinct sl_requis.n_req,0 as flg_facturado,estado_req,'0' as flg_estado " & _
         "  FROM sl_requis, sl_marcacoes " & _
         " WHERE " & _
         " sl_marcacoes.n_req = sl_requis.n_req "
         
        If EcCodRequisicao <> "" Then
            sSql = sSql & " AND sl_requis.n_req = " & EcCodRequisicao & " "
    
        Else
            sSql = sSql & " AND sl_requis.dt_chega between " & BL_TrataDataParaBD(EcDataInicio) & " and " & BL_TrataDataParaBD(EcDataFim)
    
            If EcTodas.value = 0 Then
                If EcLista.SelCount > 400 Then
                    BG_Mensagem mediMsgBox, "Demasiadas Entidades seleccionadas!" & vbCrLf & "Pode seleccionar um m�ximo de 400 entidades.", vbOKCancel + vbExclamation, Me.caption
                    BL_FimProcessamento Me
                    Exit Function
                ElseIf EcLista.SelCount = 0 Then
                    BG_Mensagem mediMsgBox, "Nenhuma Entidades seleccionada!" & vbCrLf & "Tem de seleccionar pelo menos uma entidade.", vbOKCancel + vbExclamation, Me.caption
                    BL_FimProcessamento Me
                    Exit Function
                Else
                    sSql = sSql & " AND sl_requis.cod_efr IN ("
                    For i = 0 To EcLista.ListCount - 1
                        If EcLista.Selected(i) = True Then
                            sSql = sSql & EcLista.ItemData(i) & ", "
                        End If
                    Next i
                    sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
                End If
            End If
        End If
            
    sSql = sSql & " UNION SELECT distinct sl_requis.n_req,sl_realiza.flg_facturado, estado_req, flg_estado " & _
         " FROM sl_requis,  sl_realiza " & _
         " WHERE " & _
         " sl_requis.n_req = sl_realiza.n_req "


    If EcCodRequisicao <> "" Then
        sSql = sSql & " AND sl_requis.n_req = " & EcCodRequisicao & " "

    Else
        sSql = sSql & " AND sl_requis.dt_chega between " & BL_TrataDataParaBD(EcDataInicio) & " and " & BL_TrataDataParaBD(EcDataFim)

        If EcTodas.value = 0 Then
            If EcLista.SelCount > 400 Then
                BG_Mensagem mediMsgBox, "Demasiadas Entidades seleccionadas!" & vbCrLf & "Pode seleccionar um m�ximo de 400 entidades.", vbOKCancel + vbExclamation, Me.caption
                BL_FimProcessamento Me
                Exit Function
            ElseIf EcLista.SelCount = 0 Then
                BG_Mensagem mediMsgBox, "Nenhuma Entidades seleccionada!" & vbCrLf & "Tem de seleccionar pelo menos uma entidade.", vbOKCancel + vbExclamation, Me.caption
                BL_FimProcessamento Me
                Exit Function
            Else
                sSql = sSql & " AND sl_requis.cod_efr IN ("
                For i = 0 To EcLista.ListCount - 1
                    If EcLista.Selected(i) = True Then
                        sSql = sSql & EcLista.ItemData(i) & ", "
                    End If
                Next i
                sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
            End If
        End If
    End If

    sSql = sSql & " ORDER BY sl_requis.n_req "

    If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic

    lCount = 0
    If rs.RecordCount <> 0 Then
    
        Do While Not rs.EOF

            If NReq = rs!n_req Then
                If InStr(1, Flg_Facturado, BL_HandleNull(rs!Flg_Facturado, 0)) = 0 Then
                    Flg_Facturado = Flg_Facturado & "," & BL_HandleNull(rs!Flg_Facturado, 0)
                End If
                If InStr(1, estado_req, BL_HandleNull(rs!estado_req, "")) = 0 Then
                    estado_req = estado_req & "," & BL_HandleNull(rs!estado_req, "")
                End If
                If InStr(1, flg_estado, BL_HandleNull(rs!flg_estado, "")) = 0 Then
                    flg_estado = flg_estado & "," & BL_HandleNull(rs!flg_estado, "")
                End If
                NReq = rs!n_req
                If InStr(1, Flg_Facturado, ",") <> 0 Or InStr(1, Flg_Facturado, ",") <> 0 Or InStr(1, Flg_Facturado, ",") <> 0 Then
                    sql = "update sl_cr_lista_fact set flg_facturado = '" & Flg_Facturado & _
                            "', estado_req = '" & estado_req & "',flg_estado = '" & flg_estado & _
                            "' where n_req = " & NReq & " and nome_computador = '" & BG_SYS_GetComputerName & "'"
                    BG_ExecutaQuery_ADO sql
                End If
            Else
                NReq = rs!n_req
                Flg_Facturado = BL_HandleNull(rs!Flg_Facturado, 0)
                estado_req = BL_HandleNull(rs!estado_req, "")
                flg_estado = BL_HandleNull(rs!flg_estado, "")
                
                sql = "insert into sl_cr_lista_fact (n_req, flg_facturado,estado_req, flg_estado, nome_computador) values ( " & _
                        NReq & ",'" & Flg_Facturado & "','" & estado_req & "','" & flg_estado & "','" & BG_SYS_GetComputerName & "')"
                BG_ExecutaQuery_ADO sql
            End If
                    
            rs.MoveNext
        Loop
        
    Else
        BG_Mensagem mediMsgBox, "N�o h� registos seleccionados para as condi��es de procura", vbExclamation, Me.caption
        BL_FimProcessamento Me
        Consulta_Dados_Fact_FACTUS = -1
        rs.Close
        Set rs = Nothing

        Exit Function
    End If

    BL_FimProcessamento Me

    rs.Close
    Set rs = Nothing

    Exit Function

TrataErro:
    MsgBox "Erro na Consulta de Dados da Factura��o do Factus", vbCritical
    BG_LogFile_Erros "Erro nr. " & Err.Number & " - " & Err.Description & " no FormFactGeraDados (consulta_Dados_Fact_FACTUS)"
    Consulta_Dados_Fact_FACTUS = -1
    BL_FimProcessamento Me
    Exit Function
    Resume Next
End Function

Function Consulta_Dados_Fact_SONHO() As Integer
    Dim CodAnaGH As String
    Dim CodAnaSONHO As String
    Dim HisAberto As Integer
    Dim sql As String
    Dim SQLQuery As String
    Dim l As Integer, i As Integer
    Dim rs As ADODB.recordset
    Dim CriterioTabela As String
    Dim ContaRubFact As Long
    
    On Error GoTo TrataErro
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = "SELECT distinct sl_requis.n_req,sl_requis.dt_chega,sl_requis.n_epis,cod_efr,cod_proven, " & _
                     " sl_requis.t_sit,n_benef,estado_req,t_utente,utente, " & _
                     " sl_realiza.seq_realiza, cod_perfil,cod_ana_c,cod_ana_s, cod_agrup, sl_realiza.flg_facturado, " & _
                     " sl_res_micro.n_res,sl_res_micro.cod_micro,sl_res_micro.flg_tsq,sl_res_micro.prova prova,sl_res_micro.cod_gr_antibio,sl_microrg.metodo_tsq, sl_microrg.prova prova_micro " & _
                     " FROM sl_requis, sl_identif, sl_realiza, sl_res_micro, sl_microrg " & _
                     " WHERE " & _
                     " sl_requis.seq_utente = sl_identif.seq_utente and " & _
                     " sl_requis.n_req = sl_realiza.n_req and " & _
                     " sl_realiza.seq_realiza = sl_res_micro.seq_realiza(+) and " & _
                     " sl_res_micro.cod_micro = sl_microrg.cod_microrg(+) and " & _
                     " sl_realiza.flg_estado in (3,4) and (sl_realiza.flg_facturado is null or sl_realiza.flg_facturado = 0) "
    
    
    If EcCodRequisicao <> "" Then
        CriterioTabela = CriterioTabela & " AND sl_requis.n_req = " & EcCodRequisicao & " "

    Else
        CriterioTabela = CriterioTabela & " AND sl_requis.dt_chega between " & BL_TrataDataParaBD(EcDataInicio) & " and " & BL_TrataDataParaBD(EcDataFim)

        If EcTodas.value = 0 Then
            If EcLista.SelCount > 400 Then
                BG_Mensagem mediMsgBox, "Demasiadas Entidades seleccionadas!" & vbCrLf & "Pode seleccionar um m�ximo de 400 entidades.", vbOKCancel + vbExclamation, Me.caption
                BL_FimProcessamento Me
                Exit Function
            Else
                CriterioTabela = CriterioTabela & " AND sl_requis.cod_efr IN ("
                For i = 0 To EcLista.ListCount - 1
                    If EcLista.Selected(i) = True Then
                        CriterioTabela = CriterioTabela & EcLista.ItemData(i) & ", "
                    End If
                Next i
                CriterioTabela = Mid(CriterioTabela, 1, Len(CriterioTabela) - 2) & ") "
            End If
        End If
    End If
    
    
    CriterioTabela = CriterioTabela & " ORDER BY sl_requis.n_req, sl_realiza.seq_realiza, sl_realiza.cod_perfil, sl_realiza.cod_ana_c, sl_realiza.cod_ana_s "
                       
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation
        FuncaoLimpar
    Else

        'Envia an�lises para o SONHO
        If gSISTEMA_FACTURACAO = "SONHO" Then
            
            HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
            
            If ((HisAberto = 1) And (UCase(HIS.nome) = UCase("SONHO"))) Then
            
                While Not rs.EOF
                        
                    If BL_HandleNull(rs!cod_micro, "") <> "" Then
                        'Facturar a identifica��o do microrganismo
                        
                        CodAnaSONHO = Mapeia_Microrg_para_FACTURACAO(rs!cod_micro, BL_HandleNull(rs!Prova, BL_HandleNull(rs!Prova_micro, "")), rs!n_req)
                        
                        If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                            For l = 1 To gQuantidadeAFact
                                If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                            BL_HandleNull(rs!n_epis, ""), _
                                                            rs!Utente, _
                                                            rs!cod_proven, _
                                                            CodAnaSONHO, _
                                                            rs!dt_chega, _
                                                            rs!n_req, _
                                                            rs!cod_agrup) = 1) Then
                                                            
                                    'actualiza flg_facturado
                                    sql = "UPDATE " & _
                                          "     sl_realiza " & _
                                          "SET " & _
                                          "     flg_facturado = 1 " & _
                                          "WHERE " & _
                                          "     seq_realiza = " & rs!seq_realiza
                                    
                                    BG_ExecutaQuery_ADO sql
                                    BG_Trata_BDErro
                                    
                                    ContaRubFact = ContaRubFact + 1
                                End If
                                
                                If l < gQuantidadeAFact Then
                                    gRsFact.MoveNext
                                    CodAnaSONHO = BL_HandleNull(gRsFact!cod_ana_gh, "")
                                End If
                            Next l
                        End If
                        gRsFact.Close
                        Set gRsFact = Nothing
                        
                        If BL_HandleNull(rs!flg_tsq, "") = "S" Then
                            
                            'Facturar o antibiograma de acordo com o(s) grupo(s) de antibioticos registado(s)
                            If BL_HandleNull(rs!Cod_Gr_Antibio, "") <> "" Then
                                Dim PosAux As Integer
                                Dim CodGrAntibio As String
                                Dim CodGrAntibioAux As String
                                Dim AntibFacturado As Boolean
                                Dim UltimaCarta As String
                                CodGrAntibio = rs!Cod_Gr_Antibio
                                PosAux = 1
                                UltimaCarta = ""
                                While Not PosAux = 0
                                    If UltimaCarta <> CodGrAntibio Then
                                    PosAux = InStr(1, CodGrAntibio, ";")
                                    If PosAux <> 0 Then
                                        CodGrAntibioAux = Mid(CodGrAntibio, 1, PosAux - 1)
                                    Else
                                        CodGrAntibioAux = CodGrAntibio
                                    End If
                                    CodAnaSONHO = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, CodGrAntibioAux, , rs!n_req)
                                    
                                    If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                                        If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                                    BL_HandleNull(rs!n_epis, ""), _
                                                                    rs!Utente, _
                                                                    rs!cod_proven, _
                                                                    CodAnaSONHO, _
                                                                    rs!dt_chega, _
                                                                    rs!n_req, _
                                                                    rs!cod_agrup) = 1) Then
                                            ContaRubFact = ContaRubFact + 1
                                            
                                        End If
                                    End If
                                    UltimaCarta = CodGrAntibioAux
                                    CodGrAntibio = Mid(CodGrAntibio, PosAux + 1)
                                    End If
                                Wend
                                
                            'Facturar o antibiograma de acordo com o m�todo da codifica��o de microorganismos
                            ElseIf BL_HandleNull(rs!Metodo_Tsq, "") <> "" And BL_HandleNull(rs!Metodo_Tsq, "") <> "2" And BL_HandleNull(rs!Metodo_Tsq, "") <> "3" Then
                            
                                CodAnaSONHO = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, , BL_HandleNull(rs!Metodo_Tsq, ""), rs!n_req)
                                
                                If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                                    If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                                BL_HandleNull(rs!n_epis, ""), _
                                                                rs!Utente, _
                                                                rs!cod_proven, _
                                                                CodAnaSONHO, _
                                                                rs!dt_chega, _
                                                                rs!n_req, _
                                                                rs!cod_agrup) = 1) Then
                                        ContaRubFact = ContaRubFact + 1
                                    End If
                                End If
                                
                            End If
                            
                            'Verificar se existem antibi�ticos registados cujo seu grupo de antibi�ticos tenham o m�todo TSQ "E-TEST" ou "BK"
                            Dim RsFactAntib As ADODB.recordset
                            Set RsFactAntib = New ADODB.recordset
                            sql = " SELECT sl_res_tsq.cod_antib, cod_metodo from sl_res_tsq, sl_rel_grantib,sl_gr_antibio " & _
                                  " WHERE sl_res_tsq.cod_antib = sl_rel_grantib.cod_antibio " & _
                                  " and sl_rel_grantib.cod_gr_antibio = sl_gr_antibio.cod_gr_antibio " & _
                                  " and seq_realiza = " & rs!seq_realiza & " and n_res = " & rs!N_Res & _
                                  " and cod_micro = " & BL_TrataStringParaBD(rs!cod_micro) & "" & _
                                  " and cod_metodo in ( 2,3) "
                            RsFactAntib.CursorType = adOpenStatic
                            RsFactAntib.CursorLocation = adUseServer
                            RsFactAntib.Open sql, gConexao
                            If RsFactAntib.RecordCount > 0 Then
                                While Not RsFactAntib.EOF
                                    CodAnaSONHO = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, , RsFactAntib!cod_metodo, rs!n_req)
                                    If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then

                                        If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                                    BL_HandleNull(rs!n_epis, ""), _
                                                                    rs!Utente, _
                                                                    rs!cod_proven, _
                                                                    CodAnaSONHO, _
                                                                    rs!dt_chega, _
                                                                    rs!n_req, _
                                                                    rs!cod_agrup) = 1) Then
                                            
                                            ContaRubFact = ContaRubFact + 1
                                            
                                        End If
                                    End If
                                    RsFactAntib.MoveNext
                                Wend
                            End If
                        End If
                    
                    Else
                        
                        CodAnaSONHO = Mapeia_Analises_para_FACTURACAO(rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!n_req, rs!cod_agrup)
                        
                        If ((CodAnaSONHO <> "-1") And (gInsereAnaFact = True)) Then
                            
                            'Facturar a an�lise mapeada
                            BL_InicioProcessamento Me, "A enviar an�lises para o " & HIS.nome
                            
                            'inserir a analise no sonho o mm nr de vezes que esta se encontra na tabela sl_ana_facturacao
                            For l = 1 To gQuantidadeAFact
                                If (SONHO_Nova_ANALISES_IN(rs!t_sit, _
                                                           BL_HandleNull(rs!n_epis, ""), _
                                                           rs!Utente, _
                                                           rs!cod_proven, _
                                                           CodAnaSONHO, _
                                                           rs!dt_chega, _
                                                           rs!n_req, _
                                                           rs!cod_agrup) = 1) Then
                                    
                                    sql = "UPDATE " & _
                                          "     sl_realiza " & _
                                          "SET " & _
                                          "     flg_facturado = 1 " & _
                                          "WHERE " & _
                                          "     seq_realiza = " & rs!seq_realiza
                                    
                                    BG_ExecutaQuery_ADO sql
                                    BG_Trata_BDErro
                                    
                                    ContaRubFact = ContaRubFact + 1
                                
                                End If
                                If l < gQuantidadeAFact Then
                                    gRsFact.MoveNext
                                    CodAnaSONHO = BL_HandleNull(gRsFact!cod_ana_gh, "")
                                End If
                            Next l
                            
                            BL_FimProcessamento Me
                            
                            gRsFact.Close
                            Set gRsFact = Nothing
                        
                        'An�lise j� facturada ou para n�o facturar (com codigo_GH = Null)
                        ElseIf CodAnaSONHO = "-2" Then
                            sql = "Update sl_realiza set flg_facturado = 1 where seq_realiza = " & rs!seq_realiza
                            BG_ExecutaQuery_ADO sql
                            BG_Trata_BDErro
                        
'                        'An�lise sem correspond�ncia no sonho
'                        ElseIf CodAnaSONHO = "-1" Then
'                            EcListaAna.AddItem Rs!cod_ana_s
                        End If
                    
                    End If
                    rs.MoveNext
                Wend
            End If
            BL_Fecha_conexao_HIS
        End If
        
        BL_FimProcessamento Me
        
        Call BG_Mensagem(mediMsgBox, ContaRubFact & " rubricas enviadas para o " & UCase(gSISTEMA_FACTURACAO) & ".", vbOKOnly + vbInformation, App.ProductName)
        
    End If
    
TrataErro:
    BG_LogFile_Erros "Erro nr. " & Err.Number & " - " & Err.Description & " no FormFactGeraDados (Consulta_Dados_Fact_SONHO)"
    Exit Function

End Function

Sub Funcao_DataActual()
    BL_PreencheData CampoActivo, Me.ActiveControl
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = "Consulta de Requisi��es Facturadas/N�o Facturadas"
    Me.left = 540 '1500
    Me.top = 50 '450 '900
    Me.Width = 5175 '3750
    Me.Height = 5595

    Set CampoDeFocus = EcDataInicio

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito

    estado = 1
    BG_StackJanelas_Push Me

End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormGeraDados = Nothing

End Sub

Sub LimpaCampos()

    Me.SetFocus

    EcDataInicio = ""
    EcDataFim = ""

End Sub


Sub DefTipoCampos()

    EcDataInicio.Tag = mediTipoData
    EcDataFim.Tag = mediTipoData
    EcCodRequisicao.Tag = adInteger

End Sub

Sub PreencheValoresDefeito()
    If StrComp(UCase(HIS.nome), UCase("SONHO"), vbTextCompare) <> 0 Then
    'If StrComp(gHIS_Nome, cParamHIS_Sonho, vbTextCompare) <> 0 Then
        BG_PreencheComboBD_ADO "sl_efr", "cod_efr", "descr_efr", EcLista, mediAscComboDesignacao
    Else
        EcLista.List(0) = ""
        EcLista.BackColor = vbInactiveBorder
        LaEntidades.Enabled = False
        EcLista.Enabled = False
        EcTodas.Enabled = False
    End If

    EcDataInicio = Bg_DaData_ADO
    EcDataFim = Bg_DaData_ADO
    EcEscolhaInicio.value = EcDataInicio
    EcEscolhaFim.value = EcDataFim
    
    BG_PreencheComboBD_ADO "SL_TBF_T_REQ_FACT", "COD_T_REQ", "DESCR_T_REQ", CbRequis, mediAscComboDesignacao

End Sub

Sub FuncaoLimpar()
    LimpaCampos
    CampoDeFocus.SetFocus
End Sub

Sub FuncaoEstadoAnterior()
    Unload Me
End Sub

Function Consulta_Dados_Fact_GH() As Integer
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim lNrFact As Long, lNrReq As Long, lInscricao As Long
    Dim lCount As Long
    Dim lExame As Long, lEntidade As Long
    Dim sTDoente As String, sTEpisodio As String, sEpisodio As String
    Dim sIsento As String, sBenef As String, sServico As String, sEFR As String, sNCred As String, sCodServExec As String
    Dim iCodNaoIsento As Integer, i As Integer, iRes As Integer, j As Integer
    Dim iBD_Aberta As Integer
    Dim bFacturar As Boolean
    Dim CodAnaGH As String, sAnalise As String
    Dim l As Integer
    Dim ContaRubFact As Long
    Dim HisAberto As Integer

    On Error GoTo TrataErro

    BL_InicioProcessamento Me, "Gera��o a decorrer! "
    DoEvents


    sSql = "SELECT distinct sl_requis.n_req,sl_requis.n_epis,sl_requis.n_req_assoc, " & _
         " sl_requis.req_aux,sl_requis.dt_chega,sl_requis.n_epis,cod_efr,cod_proven,sl_requis.dt_fact, " & _
         " sl_requis.t_sit,n_benef,estado_req,t_utente,utente,cod_isencao,n_benef, " & _
         " nome_ute,dt_nasc_ute,descr_mor_ute,cod_postal_ute,telef_ute, " & _
         " sl_realiza.seq_realiza, cod_perfil,cod_ana_c,cod_ana_s, cod_agrup, " & _
         " sl_res_micro.n_res,sl_res_micro.cod_micro,sl_res_micro.flg_tsq,sl_res_micro.prova prova, " & _
         " sl_res_micro.cod_gr_antibio,sl_microrg.metodo_tsq, sl_microrg.prova prova_micro " & _
         " FROM sl_requis, sl_identif, sl_realiza, sl_res_micro, sl_microrg " & _
         " WHERE " & _
         " sl_requis.seq_utente = sl_identif.seq_utente and " & _
         " sl_requis.n_req = sl_realiza.n_req and " & _
         " sl_realiza.seq_realiza = sl_res_micro.seq_realiza(+) and " & _
         " sl_res_micro.cod_micro = sl_microrg.cod_microrg(+) and " & _
         " sl_realiza.flg_estado in (3,4) and (sl_realiza.flg_facturado is null or sl_realiza.flg_facturado = 0) and sl_requis.mot_fact_rej is null "


    If EcCodRequisicao <> "" Then
        sSql = sSql & " AND sl_requis.n_req = " & EcCodRequisicao & " "

    Else
        Select Case gLAB
            Case "BIO", "GM", "CITO"
                sSql = sSql & " AND sl_requis.dt_fact between " & BL_TrataDataParaBD(EcDataInicio) & " and " & BL_TrataDataParaBD(EcDataFim)
            Case Else
                sSql = sSql & " AND sl_requis.dt_chega between " & BL_TrataDataParaBD(EcDataInicio) & " and " & BL_TrataDataParaBD(EcDataFim)
        End Select

        If EcTodas.value = 0 Then
            If EcLista.SelCount > 400 Then
                BG_Mensagem mediMsgBox, "Demasiadas Entidades seleccionadas!" & vbCrLf & "Pode seleccionar um m�ximo de 400 entidades.", vbOKCancel + vbExclamation, Me.caption
                BL_FimProcessamento Me
                Exit Function
            Else
                sSql = sSql & " AND sl_requis.cod_efr IN ("
                For i = 0 To EcLista.ListCount - 1
                    If EcLista.Selected(i) = True Then
                        sSql = sSql & EcLista.ItemData(i) & ", "
                    End If
                Next i
                sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
            End If
        End If
    End If

    sSql = sSql & " ORDER BY sl_requis.n_req,sl_realiza.seq_realiza, sl_realiza.cod_perfil, sl_realiza.cod_ana_c, sl_realiza.cod_ana_s "

    If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic

    lCount = 0
    If rs.RecordCount <> 0 Then
       
        HisAberto = BL_Abre_Conexao_HIS(gConnHIS, gOracle)
        
        If ((HisAberto = 1) And (UCase(HIS.nome) = UCase("GH"))) Then
        
        'BL_InicioProcessamento Me, "A enviar an�lises para o " & HIS.Nome
        
        Do While Not rs.EOF

            sTDoente = rs!t_utente
            sTEpisodio = rs!t_sit
            sEpisodio = BL_HandleNull(rs!n_epis, "000")
            Select Case gLAB
                Case "CITO"
                    sServico = rs!n_req
                    sNCred = BL_HandleNull(rs!n_Req_assoc, "")
                    sAnalise = rs!req_aux
                    sCodServExec = 2
                Case "BIO"
                    sServico = rs!n_req
                    sNCred = BL_HandleNull(rs!n_Req_assoc, "")
                    sAnalise = rs!cod_agrup
                    sCodServExec = 3
                Case "GM"
                    sServico = rs!n_req
                    sNCred = BL_HandleNull(rs!n_Req_assoc, "")
                    sAnalise = rs!req_aux
                    sCodServExec = 4
                Case Else
                    sServico = BL_HandleNull(rs!cod_proven, 0)
                    sNCred = ""
                    sAnalise = rs!cod_agrup
                    sCodServExec = gCodServExec
            End Select
            lNrReq = rs!n_req
            sEFR = rs!cod_efr
            
            'Facturar a identifica��o do microrganismo
            If BL_HandleNull(rs!cod_micro, "") <> "" Then
                
                CodAnaGH = Mapeia_Microrg_para_FACTURACAO(rs!cod_micro, BL_HandleNull(rs!Prova, BL_HandleNull(rs!Prova_micro, "")), rs!n_req)
                
                If ((CodAnaGH <> "-1") And (gInsereAnaFact = True)) Then
                    For l = 1 To gQuantidadeAFact
                        If (GH_Nova_ANALISES_IN(sTEpisodio, _
                                                    sEpisodio, _
                                                    sTDoente, _
                                                    rs!Utente, _
                                                    CodAnaGH, _
                                                    lNrReq, _
                                                    rs!dt_chega, _
                                                    BL_HandleNull(rs!dt_fact, ""), _
                                                    sServico, _
                                                    sCodServExec, _
                                                    sNCred, _
                                                    sEFR, _
                                                    sAnalise) = 1) Then
                            
                            'actualiza flg_facturado
                            sSql = "UPDATE sl_realiza SET flg_facturado = 1 " & _
                                  "WHERE seq_realiza = " & rs!seq_realiza
                            BG_ExecutaQuery_ADO sSql
                            BG_Trata_BDErro
                            
                            ContaRubFact = ContaRubFact + 1
                        End If
                            
                        If l < gQuantidadeAFact Then
                            gRsFact.MoveNext
                            CodAnaGH = BL_HandleNull(gRsFact!cod_ana_gh, "")
                        End If
                    Next l
                End If
                gRsFact.Close
                Set gRsFact = Nothing
                    
                If BL_HandleNull(rs!flg_tsq, "") = "S" Then
                        
                    'Facturar o antibiograma de acordo com o(s) grupo(s) de antibioticos registado(s)
                    If BL_HandleNull(rs!Cod_Gr_Antibio, "") <> "" Then
                        Dim PosAux As Integer
                        Dim CodGrAntibio As String
                        Dim CodGrAntibioAux As String
                        Dim UltimaCarta As String
                        CodGrAntibio = rs!Cod_Gr_Antibio
                        PosAux = 1
                        UltimaCarta = ""
                        While Not PosAux = 0
                            If UltimaCarta <> CodGrAntibio Then
                                PosAux = InStr(1, CodGrAntibio, ";")
                                If PosAux <> 0 Then
                                    CodGrAntibioAux = Mid(CodGrAntibio, 1, PosAux - 1)
                                Else
                                    CodGrAntibioAux = CodGrAntibio
                                End If
                                CodAnaGH = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, CodGrAntibioAux, , rs!n_req)
                                
                                If ((CodAnaGH <> "-1") And (gInsereAnaFact = True)) Then
                                    If (GH_Nova_ANALISES_IN(sTEpisodio, _
                                                                sEpisodio, _
                                                                sTDoente, _
                                                                rs!Utente, _
                                                                CodAnaGH, _
                                                                lNrReq, _
                                                                rs!dt_chega, _
                                                                BL_HandleNull(rs!dt_fact, ""), _
                                                                sServico, _
                                                                sCodServExec, _
                                                                sNCred, _
                                                                sEFR, _
                                                                sAnalise) = 1) Then
                                            
                                        ContaRubFact = ContaRubFact + 1
                                    End If
                                End If
                                UltimaCarta = CodGrAntibioAux
                                CodGrAntibio = Mid(CodGrAntibio, PosAux + 1)
                            End If
                        Wend
                            
                    'Facturar o antibiograma de acordo com o m�todo da codifica��o de microorganismos
                    ElseIf BL_HandleNull(rs!Metodo_Tsq, "") <> "" And BL_HandleNull(rs!Metodo_Tsq, "") <> "2" And BL_HandleNull(rs!Metodo_Tsq, "") <> "3" Then
                    
                        CodAnaGH = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, , BL_HandleNull(rs!Metodo_Tsq, ""), rs!n_req)
                        
                        If ((CodAnaGH <> "-1") And (gInsereAnaFact = True)) Then
                            If (GH_Nova_ANALISES_IN(sTEpisodio, _
                                                        sEpisodio, _
                                                        sTDoente, _
                                                        rs!Utente, _
                                                        CodAnaGH, _
                                                        lNrReq, _
                                                        rs!dt_chega, _
                                                        BL_HandleNull(rs!dt_fact, ""), _
                                                        sServico, _
                                                        sCodServExec, _
                                                        sNCred, _
                                                        sEFR, _
                                                        sAnalise) = 1) Then
                                
                                ContaRubFact = ContaRubFact + 1
                            End If
                        End If
                    End If
                            
                    'Verificar se existem antibi�ticos registados cujo seu grupo de antibi�ticos tenham o m�todo TSQ "E-TEST" ou "BK"
                    Dim RsFactAntib As ADODB.recordset
                    Set RsFactAntib = New ADODB.recordset
                    sSql = " SELECT sl_res_tsq.cod_antib, cod_metodo from sl_res_tsq, sl_rel_grantib,sl_gr_antibio " & _
                          " WHERE sl_res_tsq.cod_antib = sl_rel_grantib.cod_antibio " & _
                          " and sl_rel_grantib.cod_gr_antibio = sl_gr_antibio.cod_gr_antibio " & _
                          " and seq_realiza = " & rs!seq_realiza & " and n_res = " & rs!N_Res & _
                          " and cod_micro = " & BL_TrataStringParaBD(rs!cod_micro) & "" & _
                          " and cod_metodo in ( 2,3) "
                    RsFactAntib.CursorType = adOpenStatic
                    RsFactAntib.CursorLocation = adUseServer
                    RsFactAntib.Open sSql, gConexao
                    If RsFactAntib.RecordCount > 0 Then
                        CodAnaGH = Mapeia_Antib_para_FACTURACAO(rs!cod_micro, , RsFactAntib!cod_metodo, rs!n_req)
                        If ((CodAnaGH <> "-1") And (gInsereAnaFact = True)) Then
                            If (GH_Nova_ANALISES_IN(sTEpisodio, _
                                                        sEpisodio, _
                                                        sTDoente, _
                                                        rs!Utente, _
                                                        CodAnaGH, _
                                                        lNrReq, _
                                                        rs!dt_chega, _
                                                        BL_HandleNull(rs!dt_fact, ""), _
                                                        sServico, _
                                                        sCodServExec, _
                                                        sNCred, _
                                                        sEFR, _
                                                        sAnalise) = 1) Then
                                
                                ContaRubFact = ContaRubFact + 1
                            End If
                        End If
                        RsFactAntib.MoveNext
                    End If
                End If
                
            Else
                    
                CodAnaGH = Mapeia_Analises_para_FACTURACAO(rs!Cod_Perfil, rs!cod_ana_c, rs!cod_ana_s, rs!n_req)
                    
                If ((CodAnaGH <> "-1") And (gInsereAnaFact = True)) Then
                        
                    'Facturar a an�lise mapeada
                    
                        
                    'inserir a analise no factus o mm nr de vezes que esta se encontra na tabela sl_ana_facturacao
                    For l = 1 To gQuantidadeAFact
                        If (GH_Nova_ANALISES_IN(sTEpisodio, _
                                                    sEpisodio, _
                                                    sTDoente, _
                                                    rs!Utente, _
                                                    CodAnaGH, _
                                                    lNrReq, _
                                                    rs!dt_chega, _
                                                    BL_HandleNull(rs!dt_fact, ""), _
                                                    sServico, _
                                                    sCodServExec, _
                                                    sNCred, _
                                                    sEFR, _
                                                    sAnalise) = 1) Then
                                
                            sSql = "UPDATE sl_realiza SET flg_facturado = 1 " & _
                                  "WHERE seq_realiza = " & rs!seq_realiza

                            BG_ExecutaQuery_ADO sSql
                            BG_Trata_BDErro

                            ContaRubFact = ContaRubFact + 1
                        
                        End If
                        If l < gQuantidadeAFact Then
                            gRsFact.MoveNext
                            CodAnaGH = BL_HandleNull(gRsFact!cod_ana_gh, "")
                        End If
                    Next l
                            
                    
                    
                    gRsFact.Close
                    Set gRsFact = Nothing
                        
                'An�lise j� facturada ou para n�o facturar (com codigo_GH = Null)
                ElseIf CodAnaGH = "-2" Then
                    sSql = "Update sl_realiza set flg_facturado = 1 where seq_realiza = " & rs!seq_realiza
                    BG_ExecutaQuery_ADO sSql
                    BG_Trata_BDErro
                
                End If
                
            End If
                
            '--------------------------------------------------------------------------------------------------------------------
                    
            rs.MoveNext
        Loop
        BL_FimProcessamento Me
        End If
        BL_Fecha_conexao_HIS
    Else
        BG_Mensagem mediMsgBox, "N�o h� registos seleccionados para as condi��es de procura", vbExclamation, Me.caption
        BL_FimProcessamento Me

        rs.Close
        Set rs = Nothing

        Exit Function
    End If

    BL_FimProcessamento Me

    rs.Close
    Set rs = Nothing

    BG_Mensagem mediMsgBox, "Foram enviados para a Factura��o " & ContaRubFact & " rubricas.", vbInformation, Me.caption

    Exit Function

TrataErro:
    BG_LogFile_Erros "Erro nr. " & Err.Number & " - " & Err.Description & " no FormFactGeraDados (Consulta_Dados_Fact_GH)"
    Exit Function
End Function



Function Consulta_Dados_Fact_PRIVADO() As Integer
    Dim sSql As String
    Dim rs As ADODB.recordset
    Dim lNrFact As Long, lNrReq As Long, lInscricao As Long
    Dim lCount As Long
    Dim lExame As Long, lEntidade As Long
    Dim sTDoente As String, sTEpisodio As String, sEpisodio As String
    Dim sIsento As String, sBenef As String, sServico As String, sEFR As String
    Dim sExame As String, sSigla As String, sNrIncidencias As String, sFlgAmbInt As String
    Dim iCodNaoIsento As Integer, i As Integer, iRes As Integer, j As Integer
    Dim iBD_Aberta As Integer
    Dim bFacturar As Boolean
    Dim CodAnaFACTUS As String
    Dim l As Integer
    Dim ContaRubFact As Long
    Dim HisAberto As Integer
    Dim NReq As Long
    Dim Flg_Facturado As String
    Dim estado_req As String
    Dim flg_estado As String
    Dim sql As String

    On Error GoTo TrataErro

    BL_InicioProcessamento Me, "A gerar impress�o... "
    DoEvents

            
    sSql = " SELECT distinct sl_requis.n_req,sl_recibos_det.flg_facturado, estado_req " & _
         " FROM sl_requis,  sl_recibos_det " & _
         " WHERE sl_recibos_det.n_req = sl_requis.n_req " & _
         " AND flg_retirado = 0 "
    If CbRequis.ListIndex = gEstadoReqFactFacturada Then
        sSql = sSql & " AND sl_recibos_Det.flg_facturado = 1 "
    ElseIf CbRequis.ListIndex = gEstadoReqFactNaoFacturada Then
        sSql = sSql & " AND sl_recibos_Det.flg_facturado = 0 "
    End If
    
    If EcCodRequisicao <> "" Then
        sSql = sSql & " AND sl_requis.n_req = " & EcCodRequisicao & " "

    Else
        sSql = sSql & " AND sl_requis.dt_chega between " & BL_TrataDataParaBD(EcDataInicio) & " and " & BL_TrataDataParaBD(EcDataFim)

        If EcTodas.value = 0 Then
            If EcLista.SelCount > 400 Then
                BG_Mensagem mediMsgBox, "Demasiadas Entidades seleccionadas!" & vbCrLf & "Pode seleccionar um m�ximo de 400 entidades.", vbOKCancel + vbExclamation, Me.caption
                BL_FimProcessamento Me
                Exit Function
            ElseIf EcLista.SelCount = 0 Then
                BG_Mensagem mediMsgBox, "Nenhuma Entidades seleccionada!" & vbCrLf & "Tem de seleccionar pelo menos uma entidade.", vbOKCancel + vbExclamation, Me.caption
                BL_FimProcessamento Me
                Exit Function
            Else
                sSql = sSql & " AND sl_recibos_det.cod_efr IN ("
                For i = 0 To EcLista.ListCount - 1
                    If EcLista.Selected(i) = True Then
                        sSql = sSql & EcLista.ItemData(i) & ", "
                    End If
                Next i
                sSql = Mid(sSql, 1, Len(sSql) - 2) & ") "
            End If
        End If
    End If

    sSql = sSql & " ORDER BY sl_requis.n_req "

    If gModoDebug = 1 Then BG_LogFile_Erros (sSql)
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.Open sSql, gConexao, adOpenStatic

    lCount = 0
    If rs.RecordCount <> 0 Then
    
        Do While Not rs.EOF

            If NReq = rs!n_req Then
                If InStr(1, Flg_Facturado, BL_HandleNull(rs!Flg_Facturado, 0)) = 0 Then
                    Flg_Facturado = Flg_Facturado & "," & BL_HandleNull(rs!Flg_Facturado, 0)
                End If
                If InStr(1, estado_req, BL_HandleNull(rs!estado_req, "")) = 0 Then
                    estado_req = estado_req & "," & BL_HandleNull(rs!estado_req, "")
                End If
                NReq = rs!n_req
                If InStr(1, Flg_Facturado, ",") <> 0 Or InStr(1, Flg_Facturado, ",") <> 0 Then
                    sql = "update sl_cr_lista_fact set flg_facturado = '" & Flg_Facturado & _
                            "', estado_req = '" & estado_req & _
                            "' where n_req = " & NReq & " and nome_computador = '" & BG_SYS_GetComputerName & "'"
                    BG_ExecutaQuery_ADO sql
                End If
            Else
                NReq = rs!n_req
                Flg_Facturado = BL_HandleNull(rs!Flg_Facturado, 0)
                estado_req = BL_HandleNull(rs!estado_req, "")
               ' Flg_Estado = BL_HandleNull(rs!Flg_Estado, "")
                
                sql = "insert into sl_cr_lista_fact (n_req, flg_facturado,estado_req, nome_computador) values ( " & _
                        NReq & ",'" & Flg_Facturado & "','" & estado_req & "','" & BG_SYS_GetComputerName & "')"
                BG_ExecutaQuery_ADO sql
            End If
                    
            rs.MoveNext
        Loop
        
    Else
        BG_Mensagem mediMsgBox, "N�o h� registos seleccionados para as condi��es de procura", vbExclamation, Me.caption
        BL_FimProcessamento Me
        Consulta_Dados_Fact_PRIVADO = -1
        rs.Close
        Set rs = Nothing

        Exit Function
    End If

    BL_FimProcessamento Me

    rs.Close
    Set rs = Nothing

    Exit Function

TrataErro:
    MsgBox "Erro na Consulta de Dados da Factura��o do Factus", vbCritical
    BG_LogFile_Erros "Erro nr. " & Err.Number & " - " & Err.Description & " no FormFactGeraDados (consulta_Dados_Fact_FACTUS)"
    Consulta_Dados_Fact_PRIVADO = -1
    BL_FimProcessamento Me
    Exit Function
    Resume Next
End Function




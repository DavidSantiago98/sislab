VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form FormListarResultados 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "sa"
   ClientHeight    =   4530
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7755
   ForeColor       =   &H8000000D&
   Icon            =   "FormListarResultados.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4530
   ScaleWidth      =   7755
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox CkAnexo 
      Caption         =   "N�o Imprimir Anexos"
      Height          =   195
      Left            =   240
      TabIndex        =   53
      Top             =   4240
      Width           =   2175
   End
   Begin VB.CheckBox CkImprValTec 
      Caption         =   "Imprimir Com Valida��o T�cnica"
      Height          =   255
      Left            =   5040
      TabIndex        =   52
      Top             =   3960
      Width           =   2655
   End
   Begin VB.CheckBox CkSegundaVia 
      Caption         =   "Imprimir 'Segunda Via'"
      Height          =   255
      Left            =   240
      TabIndex        =   51
      Top             =   3600
      Width           =   2295
   End
   Begin VB.TextBox EcCodEFR 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   1320
      TabIndex        =   49
      TabStop         =   0   'False
      Top             =   6360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox CkInibeImpRelARS 
      Caption         =   "N�o Imprimir Relat�rio ARS"
      Height          =   195
      Left            =   240
      TabIndex        =   48
      Top             =   3960
      Width           =   2295
   End
   Begin VB.TextBox EcNumPag 
      Height          =   285
      Left            =   6480
      TabIndex        =   46
      Top             =   3600
      Width           =   495
   End
   Begin VB.CheckBox CkResAnteriores 
      Caption         =   "N�o imprimir Resul. Anter."
      Height          =   255
      Left            =   2640
      TabIndex        =   45
      Top             =   3600
      Width           =   2295
   End
   Begin VB.CheckBox CkAvisoPagamento 
      Caption         =   "Impr. Aviso de Pagamento"
      Height          =   255
      Left            =   2640
      TabIndex        =   44
      Top             =   3960
      Width           =   2295
   End
   Begin VB.CommandButton BteResults 
      Caption         =   "eR"
      Height          =   495
      Left            =   7080
      Picture         =   "FormListarResultados.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   43
      ToolTipText     =   "Renviar para eResults"
      Top             =   3525
      Width           =   615
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   6240
      TabIndex        =   42
      Top             =   4680
      Width           =   1095
   End
   Begin VB.Frame FrDadosAdicionais 
      Caption         =   "Dados adicionais"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   720
      TabIndex        =   33
      Top             =   435
      Visible         =   0   'False
      Width           =   5655
      Begin VB.TextBox EcUtente 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2520
         TabIndex        =   9
         Top             =   1200
         Width           =   2655
      End
      Begin VB.TextBox EcDataChegada 
         Height          =   285
         Left            =   2640
         TabIndex        =   7
         Tag             =   "7"
         Top             =   720
         Width           =   1095
      End
      Begin VB.TextBox EcReqAssociada 
         Height          =   285
         Left            =   1560
         TabIndex        =   6
         Top             =   360
         Width           =   1335
      End
      Begin VB.TextBox EcNome 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   1560
         Width           =   4215
      End
      Begin VB.CommandButton BtPesquisaUtente 
         Height          =   315
         Left            =   5160
         Picture         =   "FormListarResultados.frx":685E
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Pesquisa R�pida de Utentes"
         Top             =   1200
         Width           =   375
      End
      Begin VB.ComboBox CbTipoUtente 
         Height          =   315
         Left            =   1320
         TabIndex        =   8
         Text            =   "CbTipoUtente"
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label Label12 
         Caption         =   "Data &Chegada do(s) produto(s)"
         Height          =   255
         Left            =   240
         TabIndex        =   37
         Top             =   720
         Width           =   2295
      End
      Begin VB.Label Label13 
         Caption         =   "Req. &Associada"
         Height          =   255
         Left            =   240
         TabIndex        =   36
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label LbNome 
         AutoSize        =   -1  'True
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   35
         Top             =   1560
         Width           =   405
      End
      Begin VB.Label LbNrDoe 
         AutoSize        =   -1  'True
         Caption         =   "&Utente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   34
         Top             =   1200
         Width           =   465
      End
   End
   Begin VB.TextBox EcReqAux 
      Height          =   285
      Left            =   1680
      TabIndex        =   41
      Top             =   600
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox EcPesqRapResponsavel 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   1560
      TabIndex        =   38
      TabStop         =   0   'False
      Top             =   5880
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton BtBaixo 
      Height          =   195
      Left            =   240
      Picture         =   "FormListarResultados.frx":6DE8
      Style           =   1  'Graphical
      TabIndex        =   32
      ToolTipText     =   "Dados Adicionais"
      Top             =   360
      Width           =   375
   End
   Begin VB.TextBox EcSeqRel 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   1080
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   7440
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox EcDiasLimite 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   1080
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   6960
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox EcDescrEstado 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   2640
      Locked          =   -1  'True
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   120
      Width           =   4575
   End
   Begin VB.TextBox EcEstadoReq 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   3480
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   6480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox EcSeqUte 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   1080
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   6480
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton BtPesquisaRapidaReq 
      Height          =   375
      Left            =   7200
      Picture         =   "FormListarResultados.frx":6F32
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Pesquisa R�pida de Requisi��es"
      Top             =   120
      Width           =   375
   End
   Begin VB.CommandButton BtPesquisaRapidaGrAnalises 
      Height          =   375
      Left            =   7200
      Picture         =   "FormListarResultados.frx":74BC
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Pesquisa R�pida de Grupos de An�lises"
      Top             =   600
      Width           =   375
   End
   Begin VB.TextBox EcReq 
      Height          =   285
      Left            =   1680
      TabIndex        =   0
      Top             =   120
      Width           =   975
   End
   Begin VB.Frame Frame4 
      Caption         =   "Filtro de An�lises "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Left            =   120
      TabIndex        =   16
      Top             =   1080
      Width           =   7575
      Begin VB.ListBox ListaDestino 
         Height          =   1815
         Left            =   4320
         MultiSelect     =   2  'Extended
         TabIndex        =   15
         Top             =   480
         Width           =   3045
      End
      Begin VB.CommandButton BtRetira 
         Height          =   400
         Left            =   3480
         Picture         =   "FormListarResultados.frx":7A46
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   1560
         Width           =   615
      End
      Begin VB.CommandButton BtInsere 
         Height          =   400
         Left            =   3480
         Picture         =   "FormListarResultados.frx":7D50
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   960
         Width           =   615
      End
      Begin VB.ListBox ListaOrigem 
         Height          =   1815
         Left            =   240
         MultiSelect     =   2  'Extended
         TabIndex        =   12
         Top             =   480
         Width           =   3045
      End
      Begin VB.Label Label4 
         Caption         =   "N�o Imprimir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5280
         TabIndex        =   21
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Imprimir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1320
         TabIndex        =   20
         Top             =   240
         Width           =   855
      End
      Begin VB.Label LbCapacidadeUtil 
         Height          =   255
         Left            =   9000
         TabIndex        =   17
         Top             =   1560
         Width           =   375
      End
   End
   Begin RichTextLib.RichTextBox RText 
      Height          =   1095
      Left            =   3480
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   6960
      Visible         =   0   'False
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   1931
      _Version        =   393217
      BorderStyle     =   0
      Appearance      =   0
      TextRTF         =   $"FormListarResultados.frx":805A
   End
   Begin VB.CommandButton BtCima 
      Height          =   195
      Left            =   240
      Picture         =   "FormListarResultados.frx":80DC
      Style           =   1  'Graphical
      TabIndex        =   40
      ToolTipText     =   "Fechar Dados Adicionais"
      Top             =   360
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox EcGrAnalises 
      Height          =   285
      Left            =   1680
      TabIndex        =   3
      Top             =   600
      Width           =   975
   End
   Begin VB.TextBox EcDescrGrAnalises 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   2640
      Locked          =   -1  'True
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   600
      Width           =   4575
   End
   Begin VB.Label Label9 
      Caption         =   "EcCodEfr"
      Height          =   255
      Left            =   360
      TabIndex        =   50
      Top             =   6360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "P�gina:"
      Height          =   255
      Left            =   5760
      TabIndex        =   47
      Top             =   3600
      Width           =   615
   End
   Begin VB.Label Label14 
      Caption         =   "EcPesqRapResponsavel"
      Height          =   255
      Left            =   120
      TabIndex        =   39
      Top             =   5880
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label10 
      Caption         =   "EcSeqRel:"
      Height          =   255
      Left            =   120
      TabIndex        =   31
      Top             =   7440
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label7 
      Caption         =   "RText"
      Height          =   255
      Left            =   2880
      TabIndex        =   29
      Top             =   6960
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label8 
      Caption         =   "EcDiasLimite"
      Height          =   255
      Left            =   120
      TabIndex        =   27
      Top             =   6960
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label6 
      Caption         =   "EcEstadoReq:"
      Height          =   255
      Left            =   2280
      TabIndex        =   24
      Top             =   6480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label5 
      Caption         =   "EcSeqUte:"
      Height          =   255
      Left            =   120
      TabIndex        =   23
      Top             =   6480
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Requisi��o"
      Height          =   255
      Left            =   240
      TabIndex        =   18
      Top             =   120
      Width           =   975
   End
   Begin VB.Label LaGrAnalises 
      Caption         =   "Grupo de An�lises"
      Height          =   255
      Left            =   240
      TabIndex        =   19
      Top             =   600
      Width           =   1455
   End
End
Attribute VB_Name = "FormListarResultados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 06/06/2002
' T�cnico Sandra Oliveira

'Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.
Dim CampoDeFocus As Object
Dim TemValoresRef As Boolean
Dim ReqAnt As String
Dim Sai As Boolean
Dim sql As String
'Flag que indica a entrada no form de pesquisa de utentes
Public Flg_PesqUtente As Boolean

'ID do documento GesDoc a abrir
Dim sIDDoc As Long
Dim sURL As String
Dim IE As New InternetExplorer
Dim GesDocAberto As Integer

Dim ExisteSegVia As Boolean
Dim DtEmissao As String
Dim HrEmissao As String
Dim DtSVia As String
Dim HrSVia As String
'NELSONPSILVA Glintt-HS-18011 09.02.2018
Dim tabela_aux As String
'

Public Sub FuncaoImprimir(Optional MultiReport As Integer)
    Dim Report As CrystalReport
    Dim i As Integer
    Dim ListaAnalises As String
    Dim DataCalculo As String
    Dim continua As Boolean
    Dim flg_seg_via As Boolean
    Dim flg_com_val_tec As Boolean
    Dim sql As String
    'Dim Sai As Boolean
    Dim flgAnexo As Boolean
    
    If CkAnexo.value = True Then
        flgAnexo = False
    Else
        If gImprimirDestino = 1 Then
            flgAnexo = True
        Else
            flgAnexo = False
        End If
    End If
    
    If CkImprValTec.value = vbChecked Then
        flg_com_val_tec = True
    Else
        flg_com_val_tec = False
    End If
    If CkSegundaVia.value = vbChecked Then
        flg_seg_via = True
    Else
        flg_seg_via = False
    End If
    
    'Verifica os campos nulos
    If Trim(EcReq.Text) = "" Then
        BG_Mensagem mediMsgBox, "Indique a requisi��o! ", vbOKOnly + vbExclamation, "SISLAB"
        EcReq.SetFocus
        Sai = True
        Exit Sub
    Else
        Sai = False
        Call EcReq_Validate(Sai)
        If Sai = True Then
            Exit Sub
        End If
    End If
    
    If gMultiReport <> 1 Then
        gRelActivoFich = "MapaResultadosSimples"
        gRelActivoDescr = "Mapa de Resultados"
    Else
        'ver vers�o IGM
    End If
    
    '1.Verifica se a Form Preview j� estava aberta!!
    If BL_PreviewAberto(gRelActivoDescr) = True Then
        If gImprimirDestino = 1 Then
            BL_FechaPreview (gRelActivoDescr)
        Else
            Exit Sub
        End If
    End If
        
    If ListaOrigem.ListCount = 0 Then
        BG_Mensagem mediMsgBox, "N�o foram seleccionadas an�lises para imprimir.", vbOKOnly + vbExclamation, "SISLAB"
        ListaOrigem.SetFocus
        Sai = True
        Exit Sub
    End If
        
    ' ________________________________________________________________________________________________________________
    
    'LISTA DE AN�LISES:
    
    'Forma a Lista de an�lises seleccionadas para o crit�rio do SQL
    ListaAnalises = ""
    For i = 0 To ListaOrigem.ListCount - 1
        ListaAnalises = ListaAnalises & ListaOrigem.ItemData(i) & ","
    Next i
    'Remove a virgula do final da express�o
    ListaAnalises = Mid(ListaAnalises, 1, Len(ListaAnalises) - 1)
    
    '________________________________________________________________________________________________________________
    
    'DATA DE C�LCULO:
    
    'Calcula a data limite
'    DataCalculo = DateAdd("d", -BL_String2Double(BL_Converte_Para_Dias(EcLimite.Text, EcComboLimite.ListIndex)), Bg_DaData_ADO)
    
    '________________________________________________________________________________________________________________
    If CkResAnteriores = vbChecked Then
        Call IR_ImprimeResultados(False, True, EcReq.Text, EcEstadoReq.Text, EcSeqUte.Text, ListaAnalises, , , , EcGrAnalises.Text, MultiReport, , , , , , , BL_HandleNull(EcNumPag, -1), False, flg_seg_via, flg_com_val_tec, flgAnexo)
    Else
        Call IR_ImprimeResultados(False, False, EcReq.Text, EcEstadoReq.Text, EcSeqUte.Text, ListaAnalises, , , , EcGrAnalises.Text, MultiReport, , , , , , , BL_HandleNull(EcNumPag, -1), False, flg_seg_via, flg_com_val_tec, flgAnexo)
    End If
    
    ' PARA ENTIDADES CODIFICADAS IMPRIME RELAT�RIO PR�PRIO PARA ARS
    If gTipoInstituicao = "PRIVADA" And CkInibeImpRelARS.value = vbUnchecked And gImprimirDestino = 1 Then
        If BL_VerificaEfrImpARS(EcCodEFR) = True Then
            If CkResAnteriores = vbChecked Then
                Call IR_ImprimeResultados(False, True, EcReq.Text, EcEstadoReq.Text, EcSeqUte.Text, ListaAnalises, , , , EcGrAnalises.Text, MultiReport, , , , , , , BL_HandleNull(EcNumPag, -1), True, flg_seg_via, flg_com_val_tec, flgAnexo)
            Else
                Call IR_ImprimeResultados(False, False, EcReq.Text, EcEstadoReq.Text, EcSeqUte.Text, ListaAnalises, , , , EcGrAnalises.Text, MultiReport, , , , , , , BL_HandleNull(EcNumPag, -1), True, flg_seg_via, flg_com_val_tec, flgAnexo)
            End If
        End If
    End If
    
    BL_RegistaImprimeReq EcReq, -1
    
    If gImprimirDestino = 1 And CkAvisoPagamento.value = vbChecked Then
        sql = "DELETE FROM sl_cr_avisos_pagamento WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
        BG_ExecutaQuery_ADO sql
        
        Call BL_AvisosPagamento(EcReq)
    
        ' AVISOS PAGAMENTO
        Set Report = MDIFormInicio.Report
        continua = BL_IniciaReport("AvisosPagamento", "Avisos Pagamento", crptToPrinter, , , , , , , "AvisosPagamento")
        Report.SQLQuery = " SELECT SL_CR_AVISOS_PAGAMENTO.N_REQ, SL_CR_AVISOS_PAGAMENTO.COD_EFR, SL_CR_AVISOS_PAGAMENTO.TAXA, SL_CR_AVISOS_PAGAMENTO.TOTAL_PAGAR, "
        Report.SQLQuery = Report.SQLQuery & " SL_COD_SALAS.DESCR_SALA, SL_EFR.DESCR_EFR, SLV_ANALISES_FACTUS.descr_ana "
        Report.SQLQuery = Report.SQLQuery & " FROM (SL_CR_AVISOS_PAGAMENTO SL_CR_AVISOS_PAGAMENTO INNER JOIN SL_EFR SL_EFR ON SL_CR_AVISOS_PAGAMENTO.COD_EFR = SL_EFR.COD_EFR) "
        Report.SQLQuery = Report.SQLQuery & " INNER JOIN SLV_ANALISES_FACTUS SLV_ANALISES_FACTUS ON SL_CR_AVISOS_PAGAMENTO.COD_FACTURAVEL = SLV_ANALISES_FACTUS.cod_ana "
        Report.SQLQuery = Report.SQLQuery & " INNER JOIN SL_COD_SALAS SL_COD_SALAS ON SL_CR_AVISOS_PAGAMENTO.cod_sala = SL_COD_SALAS.cod_sala "
        Report.SQLQuery = Report.SQLQuery & " WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
        Report.SQLQuery = Report.SQLQuery & " order By SL_CR_AVISOS_PAGAMENTO.N_REQ ASC,"
        Report.SQLQuery = Report.SQLQuery & " SL_CR_AVISOS_PAGAMENTO.cod_efr Asc"
        Call BL_ExecutaReport
    End If
    '_________________________________________________________________________________________________________________
        
    
End Sub

Private Sub BtBaixo_Click()
    
    BtBaixo.Visible = False
    BtCima.Visible = True
    FrDadosAdicionais.Visible = True

End Sub

Private Sub BtCima_Click()
    
    BtBaixo.Visible = True
    BtCima.Visible = False
    FrDadosAdicionais.Visible = False

End Sub

Private Sub BtCallGesDoc_Click()

    If gMultiReport = 1 Then
        FuncaoImprimir (gMultiReport)
    End If
End Sub

Private Sub BteResults_Click()
    If EcReq <> "" Then
        BL_InsereEResults EcReq
    End If
End Sub

Private Sub BtInsere_Click()
        
    Dim i As Integer
    Dim cont As Integer
    Dim v() As Integer
    
    If ListaOrigem.SelCount = 0 Then Exit Sub
    
    ReDim v(0 To ListaOrigem.SelCount - 1)
    
    'Verifica qual o elemento seleccionado
    cont = 0
    For i = 0 To ListaOrigem.ListCount - 1
        If ListaOrigem.Selected(i) = True Then
            'Guarda os �ndices dos elementos seleccionados
            v(cont) = i
            ListaDestino.AddItem ListaOrigem.List(i)
            ListaDestino.ItemData(ListaDestino.NewIndex) = ListaOrigem.ItemData(i)
            cont = cont + 1
        End If
    Next i
    
    
    cont = ListaOrigem.SelCount - 1
    For i = 0 To cont
        ListaOrigem.RemoveItem (v(cont))
        cont = cont - 1
    Next i
    
End Sub

Private Sub BtPesquisaRapidaGrAnalises_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupos de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcGrAnalises.Text = resultados(1)
            EcDescrGrAnalises.Text = resultados(2)
            Call PreencheLista(EcReq.Text, EcGrAnalises.Text)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
    
    EcGrAnalises.SetFocus
    
End Sub

Private Sub BtPesquisaRapidaGrAnalises_GotFocus()
    
    If Trim(EcReq.Text) = "" Then
        MsgBox "Tem de preencher o campo N� da Requisi��o !!", vbExclamation, "Aten��o"
        EcReq.SetFocus
    End If
    
End Sub

Private Sub BtPesquisaRapidaReq_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "sl_requis.N_Req"
    ChavesPesq(2) = "sl_identif.Seq_Utente"
    
    CamposEcran(1) = "sl_requis.N_Req"
    CamposEcran(2) = "sl_identif.Nome_ute"
    
    Tamanhos(1) = 1000
    Tamanhos(2) = 4000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Nome Utente"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_requis,sl_identif"
    ClausulaWhere = "sl_requis.seq_utente=sl_identif.seq_utente AND sl_requis.estado_req NOT IN (' ','B','A','1') "
    CampoPesquisa1 = "sl_requis.N_Req"
    
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Requisi��es")
    
    mensagem = "N�o foi encontrada nenhuma Requisi��o."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcReq.Text = resultados(1)
            EcSeqUte.Text = resultados(2)
'            Call PreencheLista(EcReq.Text, EcGrAnalises.Text)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
    EcReq.SetFocus
    
End Sub

Private Sub BtPesquisaresponsavel_Click()
    
    FormPesquisaRapida.InitPesquisaRapida "sl_responsaveis", _
                        "titulo", "codigo", _
                        EcPesqRapResponsavel
End Sub

Private Sub BtPesquisaUtente_Click()

    Flg_PesqUtente = True
    FormPesquisaUtentes.Show
    FormListarResultados.Enabled = False
    
End Sub

Private Sub BtRetira_Click()
    
    Dim i As Integer
    Dim cont As Integer
    Dim v() As Integer
    
    If ListaDestino.SelCount = 0 Then Exit Sub
    
    ReDim v(0 To ListaDestino.SelCount - 1) As Integer
    
    'Verifica qual o elemento seleccionado
    cont = 0
    For i = 0 To ListaDestino.ListCount - 1
        If ListaDestino.Selected(i) = True Then
            'Guarda os �ndices dos elementos seleccionados
            v(cont) = i
            ListaOrigem.AddItem ListaDestino.List(i)
            ListaOrigem.ItemData(ListaOrigem.NewIndex) = ListaDestino.ItemData(i)
            cont = cont + 1
        End If
    Next i
    
    'Remove os elementos passados=>� parte para n�o baralhar os �ndices
    cont = ListaDestino.SelCount - 1
    For i = 0 To cont
        ListaDestino.RemoveItem (v(cont))
        cont = cont - 1
    Next i
            
End Sub



Private Sub cmdOK_Click()

    gImprimirDestino = 0
    Call FuncaoImprimir

End Sub

Private Sub EcDataChegada_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDataChegada)

End Sub

Private Sub EcGrAnalises_Change()
    
    ListaOrigem.Clear
    ListaDestino.Clear
    EcDescrGrAnalises.Text = ""
    
End Sub

Private Sub EcGrAnalises_GotFocus()
    
    If Trim(EcReq.Text) = "" Then
        MsgBox "Tem de preencher o campo N� da Requisi��o !!", vbExclamation, "Aten��o"
        EcReq.SetFocus
    End If
    
End Sub

Private Sub EcGrAnalises_Validate(Cancel As Boolean)
    
    Dim RsDescrGrAnalises As ADODB.recordset
       
    If Trim(EcGrAnalises.Text) <> "" Then
        Set RsDescrGrAnalises = New ADODB.recordset
        
        With RsDescrGrAnalises
            .Source = "SELECT descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana= " & BL_TrataStringParaBD(EcGrAnalises.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrAnalises.RecordCount <> 0 Then
            EcDescrGrAnalises.Text = RsDescrGrAnalises!descr_gr_ana
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
            Call PreencheLista(EcReq.Text, EcGrAnalises.Text)
        Else
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
            EcDescrGrAnalises.Text = ""
            BG_Mensagem mediMsgBox, "O Grupo de An�lise indicado n�o existe!", vbOKOnly, App.ProductName
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrGrAnalises.Text = ""
        Call PreencheLista(EcReq.Text, EcGrAnalises.Text)
    End If
    
End Sub

Private Sub EcPesqRapresponsavel_Change()
    
    'ver vers�o IGM
End Sub

Private Sub EcReq_Change()
        
    If gCodGrAnaUtilizador = "" Then
        EcGrAnalises.Text = ""
        EcDescrGrAnalises.Text = ""
        ListaOrigem.Clear
        ListaDestino.Clear
    End If
    
End Sub

Private Sub EcReq_GotFocus()

    cmdOK.Default = True

End Sub

Private Sub EcReq_KeyPress(KeyAscii As Integer)
    
    If Not IsNumeric(Chr(KeyAscii)) And KeyAscii <> 8 Then KeyAscii = 0
        
End Sub

Private Sub EcReq_LostFocus()
    
    cmdOK.Default = False

End Sub

Private Sub EcReq_Validate(Cancel As Boolean)

    On Error GoTo ErrorHandler
    
    Dim RsRequis As ADODB.recordset
    Dim RsProven As ADODB.recordset
    
    Dim total As Integer
    Dim sql As String
    
    Dim ListaProv As String
    Dim Vector As Variant
    
    Dim i As Integer
    Dim Existe As Boolean
       
    If Trim(EcReq.Text) <> ReqAnt Then
        ReqAnt = EcReq.Text
    Else
        Exit Sub
    End If
     
    If Trim(EcReq.Text) <> "" Then
        
        Set RsRequis = New ADODB.recordset
        With RsRequis
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
        End With
        

        If CkImprValTec.value = vbChecked Then
            sql = " SELECT sl_requis.n_req,sl_requis.req_aux,sl_requis.estado_req,sl_requis.seq_utente,sl_requis.cod_proven," & tabela_aux & ".nome_ute, " & _
                  " sl_requis.n_req_assoc,sl_requis.dt_chega," & tabela_aux & ".t_utente," & tabela_aux & ".utente, sl_requis.cod_efr" & _
                  " FROM sl_requis," & tabela_aux & " " & _
                  " WHERE sl_requis.n_req= " & EcReq.Text & _
                  " AND sl_requis.n_req IN (SELECT sl_realiza.n_Req FROM sl_realiza WHERE sl_realiza.n_req = sl_requis.n_req AND flg_estado IN('3','4','5')) " & _
                  " AND sl_requis.seq_utente=" & tabela_aux & ".seq_utente "
        
        Else
            'Express�o SQL das Requisi��es
            sql = " SELECT sl_requis.n_req,sl_requis.req_aux,sl_requis.estado_req,sl_requis.seq_utente,sl_requis.cod_proven," & tabela_aux & ".nome_ute, " & _
                  " sl_requis.n_req_assoc,sl_requis.dt_chega," & tabela_aux & ".t_utente," & tabela_aux & ".utente, sl_requis.cod_efr" & _
                  " FROM sl_requis," & tabela_aux & " " & _
                  " WHERE sl_requis.n_req= " & EcReq.Text & _
                  " AND sl_requis.estado_req NOT IN (' ','B','A','1') " & _
                  " AND sl_requis.seq_utente=" & tabela_aux & ".seq_utente "
        End If
        sql = sql & " AND sl_requis.estado_Req not in (" & BL_TrataStringParaBD(gEstadoReqCancelada) & "," & BL_TrataStringParaBD(gEstadoReqBloqueada) & " ) "
        
        RsRequis.Source = sql
        RsRequis.Open
        total = RsRequis.RecordCount
        If total <> 0 Then
            
            'Verifica se o Utilizador tem Proveni�ncias associadas
            ListaProv = ""
            
            Set RsProven = New ADODB.recordset
            With RsProven
                .CursorLocation = adUseServer
                .CursorType = adOpenStatic
                .ActiveConnection = gConexao
            End With
            sql = "SELECT cod_proven FROM sl_util_prov WHERE cod_util=" & gCodUtilizador
            RsProven.Source = sql
            RsProven.Open
            
            total = RsProven.RecordCount
            For i = 1 To total
                ListaProv = ListaProv & RsProven!cod_proven & ","
                RsProven.MoveNext
            Next i
            
            If Len(ListaProv) <> 0 Then
                ListaProv = left(ListaProv, Len(ListaProv) - 1)
            End If
            If ListaProv <> "" Then
                Vector = Split(ListaProv, ",")
                'Verifica se a requisi��o pretendida tem a proveni�ncia na lista de proveni�ncias
                Existe = False
                For i = 0 To UBound(Vector)
                    If Trim("" & RsRequis!cod_proven) = CStr(Vector(i)) Then
                        Existe = True
                        Exit For
                    End If
                Next i
                
                If Existe = False Then
                    Call BG_Mensagem(mediMsgBox, "N�o tem permiss�o para imprimir a requisi��o!", vbOKOnly + vbExclamation, "Requisi��o N� " & Trim(EcReq.Text))
                    Cancel = True
                    Sendkeys ("{HOME}+{END}")
                    RsRequis.Close
                    Set RsRequis = Nothing
                    RsProven.Close
                    Set RsProven = Nothing
                    ReqAnt = ""
                    Exit Sub
                End If
            End If
            
            EcSeqUte.Text = "" & RsRequis!seq_utente
            EcEstadoReq.Text = RsRequis!estado_req
            EcDescrEstado.Text = RsRequis!nome_ute
            EcReqAssociada.Text = BL_HandleNull(RsRequis!n_Req_assoc, "")
            EcDataChegada.Text = RsRequis!dt_chega
            CbTipoUtente.Text = RsRequis!t_utente
            EcUtente.Text = RsRequis!Utente
            EcNome.Text = RsRequis!nome_ute
            EcCodEFR.Text = BL_HandleNull(RsRequis!cod_efr, "")
            
            If (Not IsNull(RsRequis!req_aux)) Then
                EcReqAux.Text = RsRequis!req_aux
            Else
                EcReqAux.Text = ""
            End If

'            Select Case EcEstadoReq.Text
'                Case "2"
'                    EcDescrEstado.Text = "Valida��o m�dica parcial"
'                Case "3"
'                    EcDescrEstado.Text = "Impress�o parcial"
'                Case "D"
'                    EcDescrEstado.Text = "Valida��o m�dica completa"
'                Case "F"
'                    EcDescrEstado.Text = "Todas impressas"
'                Case "H"
'                    EcDescrEstado.Text = "Hist�rico"
'            End Select
            
            RsRequis.Close
            Set RsRequis = Nothing
            
            Call PreencheLista(EcReq.Text, EcGrAnalises.Text)
            
        Else
            ReqAnt = ""
            RsRequis.Close
            Set RsRequis = Nothing
            EcSeqUte.Text = ""
            EcEstadoReq.Text = ""
            EcDescrEstado.Text = ""
            EcReqAssociada.Text = ""
            EcDataChegada.Text = ""
            CbTipoUtente.Text = ""
            EcUtente.Text = ""
            EcNome.Text = ""
            EcCodEFR = ""
            BG_Mensagem mediMsgBox, "Requisi��o inexistente ou sem an�lises com resultados validados!", vbOKOnly + vbExclamation, App.ProductName
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcSeqUte.Text = ""
        EcEstadoReq.Text = ""
        EcDescrEstado.Text = ""
    End If
        
Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            ' Erro inesperado.
            Call BG_LogFile_Erros("EcReq_Validate (FormListarResultados) -> " & Err.Number & " : " & Err.Description)
            Exit Sub
    End Select
End Sub

Private Sub Inicializacoes()
    
    Me.left = 540
    Me.top = 450
    Me.Width = 7935
    Me.Height = 4950 ' Normal
    
    Me.caption = "Listagem de Resultados"
        
    ReqAnt = ""
        
    'Campo com Focus
    Set CampoDeFocus = EcReq
    
    If gCodGrAnaUtilizador <> "" And BL_SeleccionaGrupoAnalises = True Then
        EcGrAnalises = gCodGrAnaUtilizador
        EcGrAnalises_Validate False
    End If
    
    'Preenche combo tipo de utente
    BG_PreencheComboBD_ADO "sl_t_utente", "cod_t_utente", "descr_t_utente", CbTipoUtente
    If geResults = False Then
        BteResults.Visible = False
    End If
    
    If gTipoInstituicao = "HOSPITALAR" Then
        CkAvisoPagamento.Visible = False
        CkInibeImpRelARS.Visible = False
    Else
        CkAvisoPagamento.Visible = True
        CkInibeImpRelARS.Visible = True
    End If
    
    If gImp_Res_Ant <> mediNao Then
        CkResAnteriores.Visible = True
    Else
        CkResAnteriores.Visible = False
    End If
    
    If gImprimeRequisValTec = mediSim Then
        CkImprValTec.Visible = True
    Else
        CkImprValTec.Visible = False
    End If
    CkImprValTec.value = vbUnchecked
    
    'NELSONPSILVA Glintt-HS-18011 09.02.2018
    If UCase(HIS.nome) = UCase("GH") Or UCase(HIS.nome) = UCase("FACTUS") Or UCase(HIS.nome) = UCase("SONHO") Then
        tabela_aux = "slv_identif"
    Else
        tabela_aux = "sl_identif"
    End If
End Sub

Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    
    Set gFormActivo = Me
    
    If CampoDeFocus.Enabled = True Then
        CampoDeFocus.SetFocus
    End If

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    'Foi pesquisado um utente na form requisicoes mas n�o foi encontrado
    If gDUtente.seq_utente = "" And Flg_PesqUtente = True Then
        If gDUtente.descr_sexo_ute <> "" Or gDUtente.dt_nasc_ute <> "" Or _
            gDUtente.n_cartao_ute <> "" Or gDUtente.n_proc_1 <> "" Or _
            gDUtente.n_proc_2 <> "" Or gDUtente.nome_ute <> "" Or _
            gDUtente.sexo_ute <> "" Or gDUtente.t_utente <> "" Or gDUtente.Utente <> "" Then
            
            'Pesquisa de utente falhada
            FuncaoLimpar
            EcNome = gDUtente.nome_ute
            CbTipoUtente = gDUtente.t_utente
            'EcDescrTipoUtente = gDUtente.t_utente
            EcUtente = gDUtente.Utente
            Flg_PesqUtente = False
        End If
    ElseIf gDUtente.seq_utente <> "" And Flg_PesqUtente = True Then
        If gDUtente.seq_utente = "-1" Then
            ' UTENTE HIS
            FuncaoLimpar
            EcNome = gDUtente.nome_ute
            EcSeqUte = gDUtente.seq_utente
            CbTipoUtente = gDUtente.t_utente
            EcUtente = gDUtente.Utente
        Else
            FuncaoLimpar
            EcNome = gDUtente.nome_ute
            EcSeqUte = gDUtente.seq_utente
            CbTipoUtente = gDUtente.t_utente
            EcUtente = gDUtente.Utente
        End If
        Flg_PesqUtente = False
        'preenche campos se apenas houver uma requisi��o associada a este utente
        Dim rsReq As ADODB.recordset
        Set rsReq = New ADODB.recordset
        rsReq.CursorLocation = adUseServer
        rsReq.LockType = adLockReadOnly
        rsReq.CursorType = adOpenForwardOnly
        rsReq.ActiveConnection = gConexao
        sql = "SELECT n_req,seq_utente,dt_chega,n_req_assoc from sl_requis where seq_utente = " & EcSeqUte
        If EcDataChegada.Text <> "" Then
            sql = sql & " and dt_chega = " & BL_TrataDataParaBD(EcDataChegada.Text)
        End If
        rsReq.Open sql, gConexao
        If rsReq.RecordCount = 1 Then
            EcReq = rsReq!n_req
            EcDescrEstado = EcNome
            EcReqAssociada = BL_HandleNull(rsReq!n_Req_assoc, "")
            EcDataChegada = rsReq!dt_chega
        Else
            FuncaoLimpar
        End If
        rsReq.Close
        Set rsReq = Nothing
    End If
    
    Me.MousePointer = vbArrow
    
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
        
    gF_LRES = 1
    Call Inicializacoes
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    
End Sub

Private Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Call BL_FechaPreview("Mapa de Resultados")
    
    BL_ActualizaEstadoImpResAnt
    
    Set FormListarResultados = Nothing
    gF_LRES = 0
         
End Sub

Public Sub FuncaoLimpar()
    
    EcReq.Text = ""
    'EcGrAnalises.Text = ""
    'EcDescrGrAnalises.Text = ""
    ListaOrigem.Clear
    ListaDestino.Clear
    EcDescrEstado = ""
    ReqAnt = ""
    EcReqAssociada = ""
    EcDataChegada = ""
    CbTipoUtente = ""
    EcUtente = ""
    EcNome = ""
'    EcCodResponsavel = ""
'    EcDescrResponsavel = ""
    EcReqAux = ""
    
End Sub

Private Sub EcReqAux_Validate(Cancel As Boolean)
    
    Dim RsRequis As ADODB.recordset
    
    EcReqAux.Text = UCase(EcReqAux.Text)
    If EcReqAux.Text <> "" Then
        Set RsRequis = New ADODB.recordset
        With RsRequis
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
        End With
        
        'Express�o SQL das Requisi��es
        sql = " SELECT sl_requis.n_req,sl_requis.req_aux,sl_requis.estado_req,sl_requis.seq_utente,sl_requis.cod_proven," & tabela_aux & ".nome_ute, " & _
              " sl_requis.n_req_assoc,sl_requis.dt_chega," & tabela_aux & ".t_utente," & tabela_aux & ".utente" & _
              " FROM sl_requis," & tabela_aux & " " & _
              " WHERE sl_requis.req_aux = '" & EcReqAux.Text & "'" & _
              " AND sl_requis.estado_req NOT IN (' ','B','A','1') " & _
              " AND sl_requis.seq_utente=" & tabela_aux & ".seq_utente "
        If EcReq.Text <> "" Then
            sql = sql & " and sl_requis.n_req = " & EcReq.Text
        End If
        If CbTipoUtente.Text <> "" Then
            sql = sql & " and " & tabela_aux & ".t_utente = '" & CbTipoUtente.Text & "'"
        End If
        If EcUtente.Text <> "" Then
            sql = sql & " and " & tabela_aux & ".utente = '" & EcUtente & "'"
        End If
        If EcDataChegada.Text <> "" Then
            sql = sql & " and sl_requis.dt_chega = " & BL_TrataDataParaBD(EcDataChegada.Text)
        End If
        If EcReqAssociada.Text <> "" Then
            sql = sql & " and sl_requis.n_req_assoc = " & EcReqAssociada.Text
        End If
        RsRequis.Open sql, gConexao
        If RsRequis.RecordCount > 0 Then
            'obrigar a ir � funcao validate EcReq
            EcReq = RsRequis!n_req
            EcReq_Validate Cancel
        Else
            FuncaoLimpar
        End If

    End If

End Sub

Private Sub EcUtente_Validate(Cancel As Boolean)
    
    Dim RsRequis As ADODB.recordset
    
    If EcUtente.Text <> "" And CbTipoUtente.ListIndex <> -1 Then
        Set RsRequis = New ADODB.recordset
        With RsRequis
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
        End With
        
        'Express�o SQL das Requisi��es
        sql = " SELECT sl_requis.n_req,sl_requis.estado_req,sl_requis.seq_utente,sl_requis.cod_proven," & tabela_aux & ".nome_ute, " & _
              " sl_requis.n_req_assoc,sl_requis.dt_chega," & tabela_aux & ".t_utente," & tabela_aux & ".utente" & _
              " FROM sl_requis," & tabela_aux & " " & _
              " WHERE " & tabela_aux & ".t_utente = '" & CbTipoUtente.Text & "' and " & tabela_aux & ".utente = '" & EcUtente & "'" & _
              " AND sl_requis.estado_req NOT IN (' ','B','A','1') " & _
              " AND sl_requis.seq_utente=" & tabela_aux & ".seq_utente "
        If EcDataChegada.Text <> "" Then
            sql = sql & " and sl_requis.dt_chega = " & BL_TrataDataParaBD(EcDataChegada.Text)
        End If
        If EcReqAssociada.Text <> "" Then
            sql = sql & " and sl_requis.n_req_assoc = " & EcReqAssociada.Text
        End If
        RsRequis.Open sql, gConexao
        If RsRequis.RecordCount = 1 Then
            'obrigar a ir � funcao validate EcReq
            EcReq = RsRequis!n_req
            EcReq_Validate Cancel
'            EcReq = RsRequis!n_req
'            EcDescrEstado = RsRequis!nome_ute
'            EcNome = RsRequis!nome_ute
'            EcReqAssociada = BL_HandleNull(RsRequis!N_Req_Assoc, "")
'            EcDataChegada = RsRequis!Dt_Chega
        Else
            FuncaoLimpar
        End If

    End If

End Sub

Private Sub Form_Activate()
    
    Call EventoActivate
    
End Sub

Private Sub Form_Load()
    
    Call EventoLoad

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub

Private Sub PreencheLista(ByVal NReq As String, ByVal Grupo As String)
        
        Dim i As Integer
        Dim reg As ADODB.recordset
        Dim n As Integer
        Dim s As String
        Dim tabRes As String
        
        Set reg = New ADODB.recordset
        
        If Trim(NReq) = "" Then Exit Sub
        
        reg.CursorLocation = adUseClient
        reg.CursorType = adOpenForwardOnly
        reg.LockType = adLockReadOnly
        reg.ActiveConnection = gConexao
        If EcEstadoReq.Text <> gEstadoReqHistorico Then
            tabRes = "sl_realiza RES"
        Else
            tabRes = "sl_realiza_h RES"
        End If
        s = "SELECT distinct slv_analises.seq_ana,slv_analises.descr_ana FROM " & tabRes & ",slv_analises " & _
                     "WHERE RES.cod_agrup =slv_analises.cod_ana AND " & _
                     "RES.n_req=" & Trim(NReq) & _
                     " AND RES.flg_estado IN ('3','4','5')  and (seq_sinonimo is null or seq_sinonimo = -1)"
                     
        If Grupo <> "" Then s = s & " AND (slv_analises.gr_ana is not null and slv_analises.gr_ana='" & Trim(Grupo) & "')"
        reg.Source = s
        reg.Open
        
        n = reg.RecordCount
        ListaOrigem.Clear
        For i = 1 To n
            ListaOrigem.AddItem reg!descr_ana
            ListaOrigem.ItemData(i - 1) = reg!seq_ana
            reg.MoveNext
        Next i
        reg.Close
        Set reg = Nothing
        ListaDestino.Clear
        
End Sub

Sub CriaDoc()
    'vers�o IGM
End Sub

Sub EnviaDoc()
    'vers�o IGM
End Sub


Sub MudaEstadoReq()
    '____________________________________________________________________________________
    Dim EstadoReq As String
    
    'Actualiza o Estado da Requisi��o
    '=>A Flg_Estado das an�lises nos resultados j� est�o actualizadas para esta impress�o!!
    EstadoReq = BL_MudaEstadoReq(CLng(EcReq))
    
    
    
    '**** DtEmissao ,HrEmissao ,DtSVia,HrSVia j� obtidos (Podem estar vazios)!!

    'A 2� via � uma impress�o dos resultados quandos estes j� foram todos impressos!!
    If Trim(UCase(EstadoReq)) = gEstadoReqTodasImpressas Then
        'Acabou de ficar F?
        '� a 1� vez que as an�lises da requisi��o em causa est�o a ser todas impressas(ESTADO F) ?
        '=>Qdo n�o existe ainda 2�Via.
        If ExisteSegVia = False Then
            '1� Via: Actualiza novamente os dados
            '2� Via: Actualiza os dados pela 1� Via
            DtEmissao = Bg_DaData_ADO
            HrEmissao = left(BG_CvHora(Bg_DaHora_ADO), 5)
            DtSVia = DtEmissao
            HrSVia = HrEmissao
            sql = "UPDATE sl_requis SET dt_imp='" & BG_CvDataParaWhere_ADO(DtEmissao) & "',hr_imp='" & HrEmissao & "',user_imp='" & gCodUtilizador & "',dt_imp2='" & BG_CvDataParaWhere_ADO(DtSVia) & "',hr_imp2='" & HrSVia & "',user_imp2='" & gCodUtilizador & "' WHERE n_req=" & EcReq
        Else
            'J� estava F!!!
            '1� Via: Mant�m os dados
            '2� Via: Actualiza os dados pela 1� Via
            DtSVia = Bg_DaData_ADO
            HrSVia = left(BG_CvHora(Bg_DaHora_ADO), 5)
            sql = "UPDATE sl_requis SET dt_imp2='" & BG_CvDataParaWhere_ADO(DtSVia) & "',hr_imp2='" & HrSVia & "',user_imp2='" & gCodUtilizador & "' WHERE n_req=" & EcReq
        End If
    Else
        '1� Via: Actualiza novamente os dados
        '2� Via: N�o escreve nada (Mant�m-se nulo)
        DtEmissao = Bg_DaData_ADO
        HrEmissao = left(BG_CvHora(Bg_DaHora_ADO), 5)
        DtSVia = ""
        HrSVia = ""
        sql = "UPDATE sl_requis SET dt_imp='" & BG_CvDataParaWhere_ADO(DtEmissao) & "',hr_imp='" & HrEmissao & "',user_imp='" & gCodUtilizador & "' WHERE n_req=" & EcReq
    End If
    Call BG_ExecutaQuery_ADO(sql)
    
    '____________________________________________________________________________________


End Sub

Sub ListaRelatorios()
    'Campos do RecordSet
    Dim ChavesPesq(4) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 1) As String
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 1) As Long
    'Cabe�alhos
    Dim Headers(1 To 1) As String
    
    'Constru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    Dim resultados(4) As Variant
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    'Query
    ClausulaFrom = "sl_relatorios"
    ClausulaWhere = "flg_lista_rel = 'S'"
    'Crit�rio
    CampoPesquisa1 = "descr_relatorio"
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "seq_relatorio"
    ChavesPesq(2) = "descr_relatorio"
    ChavesPesq(3) = "fich_relatorio"
    ChavesPesq(4) = "tipo_rel"
    
    Headers(1) = "Relat�rios"
    CamposEcran(1) = "descr_relatorio"
    Tamanhos(1) = 3500
    
    
    'N� de Campos a pesquisar na Classe=>Resultados
    CamposRetorno.InicializaResultados 4

        
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Incompatibilidade de Resultados")
    
    mensagem = "N�o foi encontrada nenhuma incompatibilidade de resultados."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcSeqRel.Text = resultados(1)
            gRelActivoDescr = resultados(2)
            gRelActivoFich = resultados(3)
            gRelActivoTipo = resultados(4)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbOKOnly + vbExclamation, "..."
    End If
End Sub


Function GD_ConsultaDoc() As Integer
    On Error GoTo ErrorGesdoc
            
    
    Exit Function

ErrorGesdoc:
    BG_Mensagem mediMsgBox, "Erro: " & Err.Description, vbOKOnly + vbCritical, "Erro"
    Call BG_LogFile_Erros("Erro Inesperado : ConsultaDoc (FormListarResultados) -> " & Err.Description)
    Exit Function

End Function

Sub PesqImpressaoRel()


End Sub

Sub GD_RegistaDoc()
    On Error GoTo ErrorGesdoc
    
    Exit Sub
    
ErrorGesdoc:
    BG_Mensagem mediMsgBox, "Erro: " & Err.Description, vbOKOnly + vbCritical, "Erro GD_RegistaDoc"
    Call BG_LogFile_Erros("Erro Inesperado : GD_RegistaDoc (FormListarResultados) -> " & Err.Description)
    Exit Sub
End Sub

Sub GD_AlteraDoc()
    On Error GoTo ErrorGesdoc
    Exit Sub
    
ErrorGesdoc:
    BG_Mensagem mediMsgBox, "Erro: " & Err.Description, vbOKOnly + vbCritical, "Erro GD_AlteraDoc"
    Call BG_LogFile_Erros("Erro Inesperado : GD_AlteraDoc (FormListarResultados) -> " & Err.Description)
    Exit Sub
End Sub

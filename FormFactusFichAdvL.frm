VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form FormFactusFichAdvL 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   7155
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   12900
   Icon            =   "FormFactusFichAdvL.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7155
   ScaleWidth      =   12900
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtAbrir 
      Caption         =   "Abrir"
      Height          =   375
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   855
   End
   Begin VB.CommandButton BtGravar 
      Caption         =   "Gravar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1080
      TabIndex        =   3
      Top             =   0
      Width           =   975
   End
   Begin VB.TextBox EcAux 
      Height          =   375
      Left            =   480
      TabIndex        =   0
      Top             =   1800
      Visible         =   0   'False
      Width           =   1095
   End
   Begin MSFlexGridLib.MSFlexGrid FgAnalises 
      Height          =   10335
      Left            =   6240
      TabIndex        =   1
      Top             =   720
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   18230
      _Version        =   393216
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      BorderStyle     =   0
   End
   Begin MSFlexGridLib.MSFlexGrid FgRequisicoes 
      Height          =   10335
      Left            =   360
      TabIndex        =   2
      Top             =   720
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   18230
      _Version        =   393216
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      BorderStyle     =   0
   End
   Begin MSComDlg.CommonDialog NomeFicheiro 
      Left            =   12000
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label LbFactura 
      Caption         =   "Factura: "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2640
      TabIndex        =   7
      Top             =   120
      Width           =   3255
   End
   Begin VB.Label LbTotalRequisicoes 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10680
      TabIndex        =   6
      Top             =   1080
      Width           =   5175
   End
   Begin VB.Label LbTotalUtente 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10680
      TabIndex        =   5
      Top             =   1560
      Width           =   5175
   End
End
Attribute VB_Name = "FormFactusFichAdvL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim Estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Public rs As ADODB.recordset

Dim sNomeFicheiro As String
Dim CabAB As String
Dim gColuna As Integer

Private Type Analises
    Campo1 As String
    Campo2 As String
    Campo3 As String
    Campo4 As String
    Campo5 As String
    Campo6 As String
    Campo7 As String
    Campo8 As String
    Campo9 As String
    Campo10 As String
    Campo11 As String
    Campo12 As String
    Campo13 As String
    Campo14 As String
    Campo15 As String
    Campo16 As String
    Campo17 As String
    Campo18 As String
    Campo19 As String
    Campo20 As String
    Campo21 As String
    Campo22 As String
End Type

Private Type Requisicao
    Campo1 As String
    Campo2 As String
    Campo3 As String
    Campo4 As String
    Campo5 As String
    Campo6 As String
    Campo7 As String
    Campo8 As String
    Campo9 As String
    Campo10 As String
    Campo11 As String
    Campo12 As String
    Campo13 As String
    Campo14 As String
    Campo15 As String
    Campo16 As String
    Campo17 As String
    Campo18 As String
    Campo19 As String
    Campo20 As String
    Campo21 As String
    Campo22 As String
    Campo23 As String
    Campo24 As String
    Campo25 As String
    Campo26 As String
    Campo27 As String
    Campo28 As String
    Campo29 As String
    Campo30 As String
    Campo31 As String
    Campo32 As String
    Campo33 As String
    EstrutAnalises() As Analises
    TotalAnalises As Integer

End Type

Dim EstrutRequisicao() As Requisicao
Dim TotalRequisicao As Integer


Public Sub Main()

End Sub

Private Sub BtAbrir_Click()
    Dim iFile As Integer
    Dim sStr1 As String
    Dim conteudo() As String
    Dim TotAna As Integer
    Dim TotVlrReq As Double

    NomeFicheiro.Filter = "Ficheiro Texto (*.TXT)|*.TXT"

    sNomeFicheiro = DevolveNomeFicheiro
    If sNomeFicheiro = "" Then
        Exit Sub
    End If

    BtGravar.Enabled = True
    
    TotalRequisicao = 0
    TotAna = 0
    TotVlrReq = 0
    ReDim EstrutRequisicao(TotalRequisicao)
    
    FgRequisicoes.rows = 2
    FgRequisicoes.TextMatrix(1, 0) = ""
    FgRequisicoes.TextMatrix(1, 1) = ""
    FgRequisicoes.TextMatrix(1, 2) = ""
    
    iFile = FreeFile
    Open sNomeFicheiro For Input As #iFile
        Do While Not EOF(iFile)
            Line Input #1, sStr1
        
            BL_Tokenize sStr1, "|", conteudo
            If Mid(sStr1, 1, 2) = "AB" Then
                CabAB = sStr1
                LbFactura.caption = LbFactura.caption & conteudo(8) & " " & Mid(conteudo(2), 1, 4) & "/" & Mid(conteudo(2), 5)
            ElseIf conteudo(0) = "01" Then
                TotalRequisicao = TotalRequisicao + 1
                ReDim Preserve EstrutRequisicao(TotalRequisicao)
                EstrutRequisicao(TotalRequisicao).Campo1 = conteudo(0)
                EstrutRequisicao(TotalRequisicao).Campo2 = conteudo(1)
                EstrutRequisicao(TotalRequisicao).Campo3 = conteudo(2)
                EstrutRequisicao(TotalRequisicao).Campo4 = conteudo(3)
                EstrutRequisicao(TotalRequisicao).Campo5 = conteudo(4)
                EstrutRequisicao(TotalRequisicao).Campo6 = conteudo(5)
                EstrutRequisicao(TotalRequisicao).Campo7 = conteudo(6)
                EstrutRequisicao(TotalRequisicao).Campo8 = conteudo(7)
                EstrutRequisicao(TotalRequisicao).Campo9 = conteudo(8)
                EstrutRequisicao(TotalRequisicao).Campo10 = conteudo(9)
                EstrutRequisicao(TotalRequisicao).Campo11 = conteudo(10)
                EstrutRequisicao(TotalRequisicao).Campo12 = conteudo(11)
                EstrutRequisicao(TotalRequisicao).Campo13 = conteudo(12)
                EstrutRequisicao(TotalRequisicao).Campo14 = conteudo(13)
                EstrutRequisicao(TotalRequisicao).Campo15 = conteudo(14)
                EstrutRequisicao(TotalRequisicao).Campo16 = conteudo(15)
                EstrutRequisicao(TotalRequisicao).Campo17 = conteudo(16)
                EstrutRequisicao(TotalRequisicao).Campo18 = conteudo(17)
                EstrutRequisicao(TotalRequisicao).Campo19 = conteudo(18)
                EstrutRequisicao(TotalRequisicao).Campo20 = conteudo(19)
                EstrutRequisicao(TotalRequisicao).Campo21 = conteudo(20)
                EstrutRequisicao(TotalRequisicao).Campo22 = conteudo(21)
                EstrutRequisicao(TotalRequisicao).Campo23 = conteudo(22)
                EstrutRequisicao(TotalRequisicao).Campo24 = conteudo(23)
                EstrutRequisicao(TotalRequisicao).Campo25 = conteudo(24)
                EstrutRequisicao(TotalRequisicao).Campo26 = conteudo(25)
                EstrutRequisicao(TotalRequisicao).Campo27 = conteudo(26)
                EstrutRequisicao(TotalRequisicao).Campo28 = conteudo(27)
                EstrutRequisicao(TotalRequisicao).Campo29 = conteudo(28)
                EstrutRequisicao(TotalRequisicao).Campo30 = conteudo(29)
                EstrutRequisicao(TotalRequisicao).Campo31 = conteudo(30)
                EstrutRequisicao(TotalRequisicao).Campo32 = conteudo(31)
                EstrutRequisicao(TotalRequisicao).Campo33 = conteudo(32)
                
                EstrutRequisicao(TotalRequisicao).TotalAnalises = 0
                ReDim EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises)
                
                FgRequisicoes.TextMatrix(TotalRequisicao, 0) = EstrutRequisicao(TotalRequisicao).Campo2
                FgRequisicoes.TextMatrix(TotalRequisicao, 1) = Mid(EstrutRequisicao(TotalRequisicao).Campo3, 1, 4) & "/" & Mid(EstrutRequisicao(TotalRequisicao).Campo3, 5, 2) & "/" & Mid(EstrutRequisicao(TotalRequisicao).Campo3, 7)
                FgRequisicoes.TextMatrix(TotalRequisicao, 2) = EstrutRequisicao(TotalRequisicao).Campo6
                FgRequisicoes.AddItem ""
            
            ElseIf conteudo(0) = "02" Then
                EstrutRequisicao(TotalRequisicao).TotalAnalises = EstrutRequisicao(TotalRequisicao).TotalAnalises + 1
                ReDim Preserve EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo1 = conteudo(0)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo2 = conteudo(1)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo3 = conteudo(2)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo4 = conteudo(3)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo5 = conteudo(4)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo6 = conteudo(5)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo7 = conteudo(6)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo8 = conteudo(7)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo9 = conteudo(8)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo10 = conteudo(9)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo11 = conteudo(10)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo12 = conteudo(11)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo13 = conteudo(12)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo14 = conteudo(13)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo15 = conteudo(14)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo16 = conteudo(15)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo17 = conteudo(16)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo18 = conteudo(17)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo19 = conteudo(18)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo20 = conteudo(19)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo21 = conteudo(20)
                EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo22 = conteudo(21)
                TotAna = TotAna + CInt(EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo8)
                TotVlrReq = TotVlrReq + CDbl(EstrutRequisicao(TotalRequisicao).EstrutAnalises(EstrutRequisicao(TotalRequisicao).TotalAnalises).Campo7)
            End If
        Loop
    Close #iFile

    TotVlrReq = Round(TotVlrReq, 2)
    LbTotalRequisicoes.caption = TotalRequisicao & " Requisi��es " & TotAna & " An�lises " & TotVlrReq & "�"
    FgRequisicoes.Row = 1
    FgRequisicoes.Col = 2
    FgRequisicoes_Click
    FgRequisicoes.SetFocus
    
End Sub

Private Function DevolveNomeFicheiro() As String
    DevolveNomeFicheiro = ""
    
    NomeFicheiro.FilterIndex = 1
    NomeFicheiro.CancelError = True
    NomeFicheiro.InitDir = "C:\GLINTTHS\"
    NomeFicheiro.FileName = ""
    On Error Resume Next
    NomeFicheiro.DialogTitle = "Nome do ficheiro"
    NomeFicheiro.ShowOpen
    
    DevolveNomeFicheiro = NomeFicheiro.FileName

End Function

Private Sub FGRequisicoes_DblClick()
    gColuna = FgRequisicoes.Col
    If gColuna = 2 Then
        EcAux = EstrutRequisicao(FgRequisicoes.Row).Campo6
        EcAux.left = FgRequisicoes.left + FgRequisicoes.CellLeft
        EcAux.top = FgRequisicoes.top + FgRequisicoes.CellTop
        EcAux.Width = FgRequisicoes.CellWidth + 10
        EcAux.Height = FgRequisicoes.CellHeight + 20
        EcAux.Visible = True
        EcAux.SetFocus
    End If
End Sub

Private Sub FgRequisicoes_Click()
    Dim i As Integer
    Dim TotAna As Integer
    Dim TotVlrReq As Double
    
    gColuna = -1
    TotAna = 0
    TotVlrReq = 0
    FgAnalises.rows = 2
    FgAnalises.TextMatrix(1, 0) = ""
    FgAnalises.TextMatrix(1, 1) = ""
    FgAnalises.TextMatrix(1, 2) = ""
    If FgRequisicoes.Row <= TotalRequisicao Then
        For i = 1 To EstrutRequisicao(FgRequisicoes.Row).TotalAnalises
            FgAnalises.TextMatrix(i, 0) = EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo9
            FgAnalises.TextMatrix(i, 1) = EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo8
            FgAnalises.TextMatrix(i, 2) = EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo7
            TotAna = TotAna + CInt(EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo8)
            TotVlrReq = TotVlrReq + CDbl(EstrutRequisicao(FgRequisicoes.Row).EstrutAnalises(i).Campo7)
            FgAnalises.AddItem ""
        Next
    End If
    TotVlrReq = Round(TotVlrReq, 2)
    LbTotalUtente = TotAna & " An�lises " & TotVlrReq & "�"
End Sub


Private Sub FgRequisicoes_RowColChange()
    FgRequisicoes_Click
End Sub

Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        EcAux_LostFocus
    End If
End Sub

Private Sub EcAux_LostFocus()
    Dim i As Integer
    EcAux = UCase(EcAux)
    If gColuna = 2 Then
        EstrutRequisicao(FgRequisicoes.Row).Campo6 = EcAux
        FgRequisicoes.TextMatrix(FgRequisicoes.Row, 2) = EcAux
    End If
    EcAux.Visible = False
    FgRequisicoes.SetFocus
End Sub

Private Sub FgRequisicoes_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        FGRequisicoes_DblClick
    End If
End Sub

Private Sub BtGravar_Click()
    Dim iFile As Integer
    Dim i As Integer
    Dim j As Integer
    Dim sStr1 As String
        
    On Error GoTo TrataErro

    FileCopy sNomeFicheiro, Replace(sNomeFicheiro, ".TXT", ".BAK")
    Kill sNomeFicheiro
    
    iFile = FreeFile
    Open sNomeFicheiro For Append As #iFile
        Print #iFile, CabAB
        For i = 1 To TotalRequisicao
            sStr1 = EstrutRequisicao(i).Campo1 & "|" & EstrutRequisicao(i).Campo2 & "|" & _
                EstrutRequisicao(i).Campo3 & "|" & EstrutRequisicao(i).Campo4 & "|" & _
                EstrutRequisicao(i).Campo5 & "|" & EstrutRequisicao(i).Campo6 & "|" & _
                EstrutRequisicao(i).Campo7 & "|" & EstrutRequisicao(i).Campo8 & "|" & _
                EstrutRequisicao(i).Campo9 & "|" & EstrutRequisicao(i).Campo10 & "|" & _
                EstrutRequisicao(i).Campo11 & "|" & EstrutRequisicao(i).Campo12 & "|" & _
                EstrutRequisicao(i).Campo13 & "|" & EstrutRequisicao(i).Campo14 & "|" & _
                EstrutRequisicao(i).Campo15 & "|" & EstrutRequisicao(i).Campo16 & "|" & _
                EstrutRequisicao(i).Campo17 & "|" & EstrutRequisicao(i).Campo18 & "|" & _
                EstrutRequisicao(i).Campo19 & "|" & EstrutRequisicao(i).Campo20 & "|" & _
                EstrutRequisicao(i).Campo21 & "|" & EstrutRequisicao(i).Campo22 & "|" & _
                EstrutRequisicao(i).Campo23 & "|" & EstrutRequisicao(i).Campo24 & "|" & _
                EstrutRequisicao(i).Campo25 & "|" & EstrutRequisicao(i).Campo26 & "|" & _
                EstrutRequisicao(i).Campo27 & "|" & EstrutRequisicao(i).Campo28 & "|" & _
                EstrutRequisicao(i).Campo29 & "|" & EstrutRequisicao(i).Campo30 & "|" & _
                EstrutRequisicao(i).Campo31 & "|" & EstrutRequisicao(i).Campo32 & "|" & _
                EstrutRequisicao(i).Campo33 & "|"
            Print #iFile, sStr1
            For j = 1 To EstrutRequisicao(i).TotalAnalises
                sStr1 = EstrutRequisicao(i).EstrutAnalises(j).Campo1 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo2 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo3 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo4 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo5 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo6 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo7 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo8 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo9 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo10 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo11 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo12 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo13 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo14 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo15 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo16 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo17 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo18 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo19 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo20 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo21 & "|" & _
                    EstrutRequisicao(i).EstrutAnalises(j).Campo22 & "|"
                Print #iFile, sStr1
            Next
        Next
    Close #iFile
    BG_Mensagem mediMsgBox, "Opera��o efectuada com sucesso.", vbInformation, "Gravar"
Exit Sub

TrataErro:
    BG_Mensagem mediMsgBox, "Erro ao gravar o ficheiro - " & Err.Description, vbInformation, "Gravar"

End Sub



Private Sub Form_Activate()
    
    Call EventoActivate
    
End Sub

Private Sub Form_Load()
    
    Call EventoLoad
    
End Sub



Private Sub EventoLoad() 
  DIC_Inicializacao_Idioma Me, gIdioma
    
    Call BL_InicioProcessamento(Me, "Inicializar �cran.")
    
    'Preenche Combos
    Call PreencheValoresDefeito
    
    'Define Campos do ecran e das tabelas
    Call Inicializacoes
    
    'Define o tipo de campos
    Call DefTipoCampos
    
    Call BG_ParametrizaPermissoes_ADO(Me.Name)
    
    Estado = 1
    Call BG_StackJanelas_Push(Me)
    Call BL_FimProcessamento(Me)
    
End Sub

Private Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    Set FormFactusFichAdvL = Nothing

End Sub

Private Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN Estado
    If Estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call EventoUnload
    
End Sub

Private Sub Inicializacoes()
        
    Me.caption = " Ficheiros ADVCARE Layout "
    
    Me.left = 800
    Me.top = 800
    Me.Width = 12960
    Me.Height = 8610
    
    
    Set CampoDeFocus = BtAbrir
    
    NomeTabela = ""
    
    NumCampos = 0
        
End Sub
Sub DefTipoCampos()
        With FgRequisicoes
        .Cols = 3
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        
        .ColAlignment(0) = flexAlignLeftCenter
        .ColWidth(0) = 1200
        .TextMatrix(0, 0) = "N� Requisi��o"
        
        .ColAlignment(1) = flexAlignLeftCenter
        .ColWidth(1) = 1395
        .TextMatrix(0, 1) = "Data Requisi��o"
        
        .ColAlignment(2) = flexAlignLeftCenter
        .ColWidth(2) = 2730
        .TextMatrix(0, 2) = "N� Benefici�rio"
    
        .Width = .ColWidth(0) + .ColWidth(1) + .ColWidth(2) + 500
    End With
    
    With FgAnalises
        .Cols = 3
        .rows = 2
        .FixedCols = 0
        .FixedRows = 1
        
        .ColAlignment(0) = flexAlignLeftCenter
        .ColWidth(0) = 1230
        .TextMatrix(0, 0) = "An�lises"
        
        .ColAlignment(1) = flexAlignLeftCenter
        .ColWidth(1) = 930
        .TextMatrix(0, 1) = "Quantidade"
        
        .ColAlignment(2) = flexAlignLeftCenter
        .ColWidth(2) = 1335
        .TextMatrix(0, 2) = "Valor"
        
        .Width = .ColWidth(0) + .ColWidth(1) + .ColWidth(2) + 100
    End With

End Sub

Public Sub FuncaoLimpar()
    
    If Estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If
    
End Sub

Public Sub FuncaoInserir()
    
  ' nada

    
End Sub
Private Function ValidaCamposEc() As Integer

' nada


End Function
Public Sub FuncaoProcurar()
    
' nada

End Sub

Public Sub FuncaoModificar()
    
' nada

    
End Sub

Public Sub FuncaoRemover()
' nada

    
End Sub

Public Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        LimpaCampos
        PreencheCampos
        BL_FimProcessamento Me
    End If
    
End Sub

Public Sub FuncaoEstadoAnterior()
    
     If Estado = 2 Then
        Estado = 1
        BL_ToolbarEstadoN Estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Private Sub PreencheValoresDefeito()
    
' nada
End Sub

Private Sub BD_Insert()
' nada
    

End Sub

Private Sub BD_Delete()
' nada
    

    
End Sub

Private Sub BD_Update()
' nada
    

End Sub

Private Sub LimpaCampos()
' nada

    
End Sub

Private Sub PreencheCampos()
    ' nada

End Sub



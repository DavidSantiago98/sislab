VERSION 5.00
Begin VB.Form FormMenusPersonalizados 
   BackColor       =   &H00FFF5F0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FormMenusPersonalizados"
   ClientHeight    =   4665
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   1005
   Icon            =   "FormMenusPersonalizados.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   MousePointer    =   1  'Arrow
   ScaleHeight     =   4665
   ScaleWidth      =   1005
   Begin VB.Frame Frame4 
      Height          =   4695
      Left            =   3120
      TabIndex        =   21
      Top             =   0
      Width           =   1095
      Begin VB.CommandButton Command24 
         Caption         =   "A4"
         Height          =   735
         Left            =   120
         TabIndex        =   27
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton Command23 
         Caption         =   "B4"
         Height          =   735
         Left            =   120
         TabIndex        =   26
         Top             =   960
         Width           =   735
      End
      Begin VB.CommandButton Command22 
         Caption         =   "C4"
         Height          =   735
         Left            =   120
         TabIndex        =   25
         Top             =   1680
         Width           =   735
      End
      Begin VB.CommandButton Command21 
         Caption         =   "D4"
         Height          =   735
         Left            =   120
         TabIndex        =   24
         Top             =   2400
         Width           =   735
      End
      Begin VB.CommandButton Command20 
         Caption         =   "E4"
         Height          =   735
         Left            =   120
         TabIndex        =   23
         Top             =   3120
         Width           =   735
      End
      Begin VB.CommandButton Command19 
         Caption         =   "F4"
         Height          =   735
         Left            =   120
         TabIndex        =   22
         Top             =   3840
         Width           =   735
      End
   End
   Begin VB.Frame Frame3 
      Height          =   4695
      Left            =   2040
      TabIndex        =   14
      Top             =   0
      Width           =   1095
      Begin VB.CommandButton Command18 
         Caption         =   "F3"
         Height          =   735
         Left            =   120
         TabIndex        =   20
         Top             =   3840
         Width           =   735
      End
      Begin VB.CommandButton Command17 
         Caption         =   "E3"
         Height          =   735
         Left            =   120
         TabIndex        =   19
         Top             =   3120
         Width           =   735
      End
      Begin VB.CommandButton Command16 
         Caption         =   "D3"
         Height          =   735
         Left            =   120
         TabIndex        =   18
         Top             =   2400
         Width           =   735
      End
      Begin VB.CommandButton Command15 
         Caption         =   "C3"
         Height          =   735
         Left            =   120
         TabIndex        =   17
         Top             =   1680
         Width           =   735
      End
      Begin VB.CommandButton Command14 
         Caption         =   "B3"
         Height          =   735
         Left            =   120
         TabIndex        =   16
         Top             =   960
         Width           =   735
      End
      Begin VB.CommandButton Command13 
         Caption         =   "A3"
         Height          =   735
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFE0C7&
      Height          =   4695
      Left            =   960
      TabIndex        =   7
      Top             =   0
      Width           =   1095
      Begin VB.CommandButton Command12 
         Caption         =   "A2"
         Height          =   735
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton Command11 
         Caption         =   "B2"
         Height          =   735
         Left            =   120
         TabIndex        =   12
         Top             =   960
         Width           =   735
      End
      Begin VB.CommandButton Command10 
         Caption         =   "C2"
         Height          =   735
         Left            =   120
         TabIndex        =   11
         Top             =   1680
         Width           =   735
      End
      Begin VB.CommandButton Command9 
         Caption         =   "D2"
         Height          =   735
         Left            =   120
         TabIndex        =   10
         Top             =   2400
         Width           =   735
      End
      Begin VB.CommandButton Command8 
         Caption         =   "E2"
         Height          =   735
         Left            =   120
         TabIndex        =   9
         Top             =   3120
         Width           =   735
      End
      Begin VB.CommandButton Command7 
         Caption         =   "F2"
         Height          =   735
         Left            =   120
         TabIndex        =   8
         Top             =   3840
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFE0C7&
      Height          =   4695
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   1005
      Begin VB.CommandButton BtRes 
         BackColor       =   &H00FFE0C7&
         DownPicture     =   "FormMenusPersonalizados.frx":030A
         Height          =   735
         Left            =   120
         Picture         =   "FormMenusPersonalizados.frx":0614
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Resultados."
         Top             =   3840
         Width           =   735
      End
      Begin VB.CommandButton BtFolhasTrab 
         BackColor       =   &H00FFE0C7&
         DownPicture     =   "FormMenusPersonalizados.frx":091E
         Height          =   735
         Left            =   120
         Picture         =   "FormMenusPersonalizados.frx":14E0
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Impressão de Folhas de Trabalho"
         Top             =   3120
         Width           =   735
      End
      Begin VB.CommandButton BtConsReq 
         BackColor       =   &H00FFE0C7&
         DownPicture     =   "FormMenusPersonalizados.frx":1DAA
         Height          =   735
         Left            =   120
         Picture         =   "FormMenusPersonalizados.frx":2674
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Consultar Requisições"
         Top             =   2400
         Width           =   735
      End
      Begin VB.CommandButton BtChega 
         BackColor       =   &H00FFE0C7&
         DownPicture     =   "FormMenusPersonalizados.frx":2F3E
         Height          =   735
         Left            =   120
         Picture         =   "FormMenusPersonalizados.frx":3808
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "Chegada de Produtos/Requisições"
         Top             =   1680
         Width           =   735
      End
      Begin VB.CommandButton BtRequis 
         BackColor       =   &H00FFE0C7&
         DownPicture     =   "FormMenusPersonalizados.frx":40D2
         Height          =   735
         Left            =   120
         Picture         =   "FormMenusPersonalizados.frx":4F9C
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   "Gestão de Requisições"
         Top             =   960
         Width           =   735
      End
      Begin VB.CommandButton BtIdentif 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFE0C7&
         DownPicture     =   "FormMenusPersonalizados.frx":5E66
         Height          =   735
         Left            =   120
         Picture         =   "FormMenusPersonalizados.frx":6D30
         Style           =   1  'Graphical
         TabIndex        =   0
         ToolTipText     =   "Identificação de Utentes"
         Top             =   240
         Width           =   735
      End
   End
End
Attribute VB_Name = "FormMenusPersonalizados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualização : --/--/----
' Técnico

Dim TopIni As Long
Dim LeftIni As Long

Private Sub BtChega_Click()
    
  MDIFormInicio.PopupMenu MDIFormInicio.HIDE_TUBO

End Sub

Private Sub BtConsReq_Click()
    
    MDIFormInicio.PopupMenu MDIFormInicio.IDM_MENU_CONSREQ

End Sub

Private Sub BtFolhasTrab_Click()
    
    MDIFormInicio.PopupMenu MDIFormInicio.IDM_FOLHASTRAB

    'BG_AbreForm FormFolhasTrab, "FormFolhasTrab"

End Sub

Private Sub BtIdentif_Click()
    
    
    If gTipoInstituicao = gTipoInstituicaoHospitalar Then
        BG_AbreForm FormIdentificaUtente, "FormIdentificaUtente"
    ElseIf gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoAguas Then
        BG_AbreForm FormIdUtentePesq, "FormIdUtentePesq"
    ElseIf gTipoInstituicao = gTipoInstituicaoVet Then
        BG_AbreForm FormIdentificaVet, "FormIdentificaVet"
    End If

End Sub

Private Sub BtRequis_Click()
        If gTipoInstituicao = "PRIVADA" Then
            MDIFormInicio.PopupMenu MDIFormInicio.IDM_REQUISICOES_PRIVADO
        ElseIf gTipoInstituicao = "HOSPITALAR" Then
            BG_AbreForm FormGestaoRequisicao, "FormGestaoRequisicao"
        ElseIf gTipoInstituicao = gTipoInstituicaoVet Then
            BG_AbreForm FormGestaoRequisicaoVet, "FormGestaoRequisicaoVet"
        ElseIf gTipoInstituicao = gTipoInstituicaoAguas Then
            BG_AbreForm FormGestaoRequisicaoAguas, "FormGestaoRequisicaoAguas"
        End If

End Sub

Private Sub BtRes_Click()
    MDIFormInicio.PopupMenu MDIFormInicio.IDM_RESULTADOS

End Sub

Private Sub Form_Load()
    
    Inicializacoes

End Sub

Sub Inicializacoes()

    Dim PropTop As Long
    Dim PropLeft As Long
    Dim PropWidht As Long
    Dim PropHeight As Long
    Dim sql As String
    Dim TmpRS As recordset
    Dim ValorProp As Variant
    
    Me.caption = ""
    BtChega.Visible = True
    BtConsReq.Visible = True
    BtFolhasTrab.Visible = True
    BtIdentif.Visible = True
    BtRequis.Visible = True
    BtRes.Visible = True
    
    BG_ParametrizaPermissoes_ADO Me.Name
    TopIni = Me.top
    LeftIni = Me.left

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, _
                             UnloadMode As Integer)
 
    If (UnloadMode = 0) Then
        BL_ActualizaEstadoMenusPersonalizados "FECHADO"
    End If
 
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Dim nomeForm As String
    
    If Me.top <> TopIni Or Me.left <> LeftIni Then
        TopIni = Me.top
        LeftIni = Me.left
        GravaParamForm Me, Me.Name, "TOP", TopIni, gCodGrupo, gCodUtilizador
        GravaParamForm Me, Me.Name, "LEFT", LeftIni, gCodGrupo, gCodUtilizador
    End If

End Sub

Public Sub GravaParamForm(Formulario As Form, _
                          NomeObjecto As String, _
                          propriedade As String, _
                          ValorPropriedade As Variant, _
                          gCodGrupo, gCodUtilizador)
                          
    Dim rs As ADODB.recordset
    Dim sql As String
    Dim nomeForm As String
    Dim NumSeq As Long
    
    nomeForm = Formulario.Name
    
    sql = "SELECT * FROM " & _
            cParamAcessos & " " & _
          "WHERE " & _
          "     (cod_grupo=" & gCodGrupo & " OR " & _
          "     cod_util=" & gCodUtilizador & ") AND " & _
          "     UPPER(nome_form) ='" & UCase(nomeForm) & "' " & "AND " & _
          "     UPPER(nome_obj)  ='" & UCase(NomeObjecto) & "' AND " & _
          "     UPPER(descr_prop)='" & UCase(propriedade) & "'"
        
    Set rs = New ADODB.recordset
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open sql, gConexao
    If rs.RecordCount <= 0 Then
        NumSeq = BG_DaMAX(cParamAcessos, "seq_param_acess") + 1
        
        sql = "INSERT INTO " & cParamAcessos & " " & _
              "(seq_param_acess,cod_util,cod_grupo,nome_obj,nome_form, " & _
              " descr_prop,val_prop,user_cri,dt_cri,user_act,dt_act) " & _
              "VALUES " & _
              "(" & NumSeq & ",'" & gCodUtilizador & "','" & gCodGrupo & "','" & _
                UCase(NomeObjecto) & "','" & UCase(nomeForm) & "','" & propriedade & "','" & _
                UCase(ValorPropriedade) & "'," & gCodUtilizador & ",'" & BG_CvDataParaWhere_ADO(Bg_DaData_ADO) & "',null,null)"
        
        BG_ExecutaQuery_ADO sql
        
    Else
        sql = " UPDATE " & cParamAcessos & " SET val_prop='" & ValorPropriedade & "'" & _
            " WHERE seq_param_acess=" & rs!seq_param_acess
        BG_ExecutaQuery_ADO sql
    End If
    rs.Close
    Set rs = Nothing

End Sub




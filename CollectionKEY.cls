VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CollectionKEY"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'     .................................................................
'    .        Collection class module for simulate an hashtable.       .
'   .                                                                   .
'   .                    Paulo Ferreira 2010.04.21                      .
'    .                         � 2010 GLINTT-HS                        .
'     .................................................................

Option Explicit

Private mCol As Collection

Public Sub Add(Item As Variant, Optional sKey As String, Optional Before As Variant, Optional After As Variant)
    
    Dim objNewMember() As Variant
    
    ReDim objNewMember(0 To 1)
    
    If IsObject(Item) Then
        Set objNewMember(0) = Item
    Else
        objNewMember(0) = Item
    End If
    
    objNewMember(1) = sKey
    
    If Len(sKey) = 0 Then
        mCol.Add objNewMember, , Before, After
    Else
        mCol.Add objNewMember, sKey, Before, After
    End If
End Sub

Public Property Get Item(vntIndexKey As Variant) As Variant
    Dim vItem() As Variant
    vItem = mCol(vntIndexKey)
    
    If IsObject(vItem(0)) Then
        Set Item = vItem(0)
    Else
        Item = vItem(0)
    End If
End Property

Public Property Get ItemKEY(vntIndexKey As Variant) As String
    
    On Error GoTo ErrorHandler
    ItemKEY = mCol(vntIndexKey)(1)
    Exit Property
    
ErrorHandler:
    ItemKEY = Empty
    Exit Property
    
End Property

Public Property Get Count() As Long
    
    Count = mCol.Count
End Property

Public Sub Remove(vntIndexKey As Variant)
    
    mCol.Remove vntIndexKey
End Sub

Public Property Get NewEnum() As IUnknown
  
    Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
  
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
  
    Set mCol = Nothing
End Sub

VERSION 5.00
Begin VB.Form FormRelGrAntibio 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormRelGrAntibio"
   ClientHeight    =   5430
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7995
   Icon            =   "FormRelGrAntibio.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5430
   ScaleWidth      =   7995
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtBaixo 
      Height          =   495
      Left            =   3720
      Picture         =   "FormRelGrAntibio.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   " Alterar ordem na Complexa "
      Top             =   4245
      Width           =   495
   End
   Begin VB.CommandButton BtCima 
      Height          =   495
      Left            =   3720
      Picture         =   "FormRelGrAntibio.frx":0316
      Style           =   1  'Graphical
      TabIndex        =   10
      ToolTipText     =   " Alterar ordem na Complexa "
      Top             =   3720
      Width           =   495
   End
   Begin VB.CommandButton BtInsere 
      Height          =   495
      Left            =   3660
      Picture         =   "FormRelGrAntibio.frx":0620
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2550
      Width           =   615
   End
   Begin VB.CommandButton BtRetira 
      Height          =   495
      Left            =   3660
      Picture         =   "FormRelGrAntibio.frx":092A
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   3150
      Width           =   615
   End
   Begin VB.ListBox EcLista2 
      Height          =   3375
      Left            =   4440
      TabIndex        =   7
      Top             =   1860
      Width           =   3345
   End
   Begin VB.TextBox EcPesquisa 
      Height          =   285
      Left            =   210
      TabIndex        =   3
      Top             =   1710
      Width           =   3345
   End
   Begin VB.ListBox EcLista 
      Height          =   3180
      Left            =   210
      TabIndex        =   4
      Top             =   2070
      Width           =   3345
   End
   Begin VB.Frame Frame1 
      Caption         =   " Grupo de Antibi�ticos "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   240
      TabIndex        =   0
      Top             =   90
      Width           =   7515
      Begin VB.TextBox EcCodGrAntibio 
         Height          =   315
         Left            =   120
         TabIndex        =   1
         Top             =   450
         Width           =   2385
      End
      Begin VB.ComboBox EcCodGrAntibioCombo 
         Height          =   315
         Left            =   2550
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   450
         Width           =   4665
      End
   End
   Begin VB.Label Label2 
      Caption         =   "Antibi�ticos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4440
      TabIndex        =   9
      Top             =   1560
      Width           =   3345
   End
   Begin VB.Label Label1 
      Caption         =   "Digite o texto que pretende pesquisar"
      Height          =   255
      Left            =   210
      TabIndex        =   8
      Top             =   1470
      Width           =   3345
   End
End
Attribute VB_Name = "FormRelGrAntibio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

Dim ListaOrigem As Control
Dim ListaDestino As Control
    

Public rs As ADODB.recordset

Private Sub ChRemovidos_Click()
    If ListarRemovidos = False Then
        ListarRemovidos = True
    Else
        ListarRemovidos = False
    End If
End Sub

Private Sub EcCodPais_LostFocus()

    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Private Sub BtBaixo_Click()
    Dim sdummy As String
    Dim ldummy As Long
    
    If ListaDestino.ListIndex < ListaDestino.ListCount - 1 Then
        sdummy = ListaDestino.List(ListaDestino.ListIndex + 1)
        ldummy = ListaDestino.ItemData(ListaDestino.ListIndex + 1)
        ListaDestino.List(ListaDestino.ListIndex + 1) = ListaDestino.List(ListaDestino.ListIndex)
        ListaDestino.ItemData(ListaDestino.ListIndex + 1) = ListaDestino.ItemData(ListaDestino.ListIndex)
        ListaDestino.List(ListaDestino.ListIndex) = sdummy
        ListaDestino.ItemData(ListaDestino.ListIndex) = ldummy
    
        ListaDestino.ListIndex = ListaDestino.ListIndex + 1
    End If
End Sub

Private Sub BtCima_Click()
    Dim sdummy As String
    Dim ldummy As Long
    
    If ListaDestino.ListIndex > 0 Then
        sdummy = ListaDestino.List(ListaDestino.ListIndex - 1)
        ldummy = ListaDestino.ItemData(ListaDestino.ListIndex - 1)
        ListaDestino.List(ListaDestino.ListIndex - 1) = ListaDestino.List(ListaDestino.ListIndex)
        ListaDestino.ItemData(ListaDestino.ListIndex - 1) = ListaDestino.ItemData(ListaDestino.ListIndex)
        ListaDestino.List(ListaDestino.ListIndex) = sdummy
        ListaDestino.ItemData(ListaDestino.ListIndex) = ldummy
        
        ListaDestino.ListIndex = ListaDestino.ListIndex - 1
    End If
End Sub

Private Sub BtInsere_Click()

    Dim i As Integer
    
    For i = 0 To ListaOrigem.ListCount - 1
        If ListaOrigem.Selected(i) Then
            BG_PassaElementoEntreListas ListaOrigem, ListaDestino, i, False, False
            ListaOrigem.Selected(i) = False
        End If
    Next i
      
End Sub

Private Sub BtRetira_Click()


If ListaDestino.ListIndex <> -1 Then
    ListaDestino.RemoveItem (ListaDestino.ListIndex)
End If

End Sub


Private Sub EcCodGrAntibio_LostFocus()
    EcCodGrAntibio.Text = UCase(EcCodGrAntibio.Text)
    BL_ColocaTextoCombo "sl_gr_antibio", "seq_gr_antibio", "cod_gr_antibio", EcCodGrAntibio, EcCodGrAntibioCombo
End Sub

Private Sub EcCodGrAntibioCombo_Click()
    BL_ColocaComboTexto "sl_gr_antibio", "seq_gr_antibio", "cod_gr_antibio", EcCodGrAntibio, EcCodGrAntibioCombo
    FuncaoProcurar
End Sub

Private Sub EcCodGrAntibioCombo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcCodGrAntibioCombo.ListIndex = -1
End Sub


Private Sub EcPesquisa_Change()
    RefinaPesquisa
End Sub

Sub RefinaPesquisa()
    
    Dim i As Integer
    Dim TSQLQueryAux As String
    Dim TCampoChave As String
    Dim TCampoPesquisa As String
    Dim TNomeTabela As String
    Dim TClausulaWhereAuxiliar As String
    Dim TTabelaAux As ADODB.recordset
    Dim NomeControl As Control
    
    Set TTabelaAux = New ADODB.recordset
    
    Set NomeControl = EcLista
    TCampoChave = "seq_antibio"
    TCampoPesquisa = "descr_antibio"
    TNomeTabela = "sl_antibio"
    
    TSQLQueryAux = "SELECT COD_ANTIBIO, " & TCampoChave & " ," & (TCampoPesquisa) & " FROM " & TNomeTabela
    TSQLQueryAux = TSQLQueryAux & " WHERE UPPER(" & TCampoPesquisa & ") LIKE '" & Trim(UCase(EcPesquisa)) & "%'"
    If TClausulaWhereAuxiliar <> "" Then
        TSQLQueryAux = TSQLQueryAux & " AND " & TClausulaWhereAuxiliar
    End If
    TSQLQueryAux = TSQLQueryAux & " ORDER BY " & TCampoPesquisa
    
    TTabelaAux.CursorLocation = adUseServer
    TTabelaAux.CursorType = adOpenStatic
    TTabelaAux.Open TSQLQueryAux, gConexao
    NomeControl.ListIndex = mediComboValorNull
    NomeControl.Clear
    i = 0
    
    BL_PreencheListBoxMultipla_ADO EcLista, TTabelaAux, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT", , "seq_antibio"
    TTabelaAux.Close
    Set TTabelaAux = Nothing
End Sub
Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Rela��o entre Antibi�ticos e seus Grupos"
    Me.left = 540
    Me.top = 100
    Me.Width = 8085
    Me.Height = 5880 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_rel_grantib"
    Set CampoDeFocus = EcCodGrAntibio
    
    NumCampos = 1
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "cod_gr_antibio"
        
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodGrAntibio
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(0) = "C�digo do Grupo de Antibi�ticos"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "cod_antibio"
    Set ChaveEc = EcCodGrAntibio
    
    'Lista EcLista
    CamposBDparaListBox = Array("cod_antibio", "descr_antibio")
    NumEspacos = Array(15, 22)
    
    
    'Listas
    Set ListaOrigem = EcLista
    Set ListaDestino = EcLista2

End Sub


Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
        
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    'preenche Eclista
    Set rs = New ADODB.recordset
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    rs.Open "SELECT * FROM sl_antibio", gConexao
    BL_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT", , "seq_antibio"
    
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormRelGrAntibio = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    BG_LimpaCampo_Todos CamposEc
    EcLista2.Clear
    EcCodGrAntibioCombo.ListIndex = mediComboValorNull
End Sub

Sub DefTipoCampos()
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub
Sub PreencheValoresDefeito()

    BG_PreencheComboBD_ADO "sl_gr_antibio", "seq_gr_antibio", "descr_gr_antibio", EcCodGrAntibioCombo, mediAscComboDesignacao
    
    'trazer codigo do form de grupos de antibioticos
    'caso a chamada a este form tenha sido efectuada de l�
    If gF_GRANTIB = 1 Then
        EcCodGrAntibio = FormGruposAntibio.EcCodigo
        BL_ColocaTextoCombo "sl_gr_antibio", "seq_gr_antibio", "cod_gr_antibio", EcCodGrAntibio, EcCodGrAntibioCombo
    End If
    
End Sub
Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        estado = 2
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
    End If
    
End Sub


Sub FuncaoLimpar()
    
    LimpaCampos
    CampoDeFocus.SetFocus

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        LimpaCampos
        EcLista2.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    
    If EcCodGrAntibio = "" Then
        BG_Mensagem mediMsgBox, "Tem que escolher um grupo de antibioticos!", vbOKOnly, "Procurar"
        Exit Sub
    End If
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    CriterioTabela = "SELECT seq_antibio,sl_antibio.cod_antibio, descr_antibio FROM sl_antibio,sl_rel_grantib WHERE cod_gr_antibio= '" & Trim(EcCodGrAntibio) & "' AND sl_rel_grantib.cod_antibio=sl_antibio.cod_antibio ORDER by sl_rel_grantib.ordem"
    
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
'        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
'        FuncaoLimpar
    Else
        estado = 2
        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BL_PreencheListBoxMultipla_ADO EcLista2, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT", , "seq_antibio"
        ' Fim do preenchimento de 'EcLista'
        BL_Toolbar_BotaoEstado "Remover", "InActivo"
        BL_FimProcessamento Me
    End If
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoInserir()
    Call FuncaoModificar
End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim SQLAux As String
    Dim SQLAux2 As String
    Dim MarcaLocal As Variant
    Dim i As Integer
    Dim rsAux As ADODB.recordset
        
    SQLAux2 = "DELETE FROM " & NomeTabela & " WHERE cod_gr_antibio='" & Trim(EcCodGrAntibio) & "'"
    BG_ExecutaQuery_ADO SQLAux2
    For i = 0 To EcLista2.ListCount - 1
        Set rsAux = New ADODB.recordset
        SQLAux = "SELECT cod_antibio FROM sl_antibio WHERE seq_antibio=" & EcLista2.ItemData(i)
        rsAux.Open SQLAux, gConexao, adOpenStatic, adLockReadOnly
        SQLQuery = "INSERT INTO " & NomeTabela & " (cod_antibio,cod_gr_antibio,ordem) VALUES ('" & Trim(rsAux!cod_antibio) & "','" & Trim(EcCodGrAntibio) & "'," & i & ")"
        BG_ExecutaQuery_ADO SQLQuery
        rsAux.Close
        Set rsAux = Nothing
    Next i
    
End Sub


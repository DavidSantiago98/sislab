VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormParamAmbienteV2 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   8250
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   11910
   Icon            =   "FormParamAmbienteV2.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8250
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.ListView LvChaves 
      Height          =   4455
      Left            =   120
      TabIndex        =   0
      Top             =   2760
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   7858
      SortOrder       =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   0
   End
   Begin VB.PictureBox PictureListColor 
      Height          =   375
      Left            =   8520
      ScaleHeight     =   315
      ScaleWidth      =   435
      TabIndex        =   39
      Top             =   9120
      Width           =   495
   End
   Begin VB.Frame Frame2 
      Height          =   795
      Left            =   120
      TabIndex        =   32
      Top             =   7320
      Width           =   11685
      Begin VB.Label LaCriacao 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   38
         Top             =   195
         Width           =   675
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2280
         TabIndex        =   37
         Top             =   195
         Width           =   3135
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   6360
         TabIndex        =   36
         Top             =   195
         Width           =   3735
      End
      Begin VB.Label LaDataActualizacao 
         Appearance      =   0  'Flat
         BackColor       =   &H80000013&
         BackStyle       =   0  'Transparent
         Enabled         =   0   'False
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   6360
         TabIndex        =   35
         Top             =   480
         Width           =   3135
      End
      Begin VB.Label LaUtilActualizacao 
         Appearance      =   0  'Flat
         BackColor       =   &H80000013&
         BackStyle       =   0  'Transparent
         Enabled         =   0   'False
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2280
         TabIndex        =   34
         Top             =   480
         Width           =   3735
      End
      Begin VB.Label LaActualizacao 
         Caption         =   "Actualizacao"
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   480
         Width           =   1185
      End
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   27
      Top             =   9000
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3720
      TabIndex        =   26
      Top             =   9000
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.TextBox EcDataActualizacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2160
      TabIndex        =   25
      Top             =   9480
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.TextBox EcUtilizadorActualizacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   960
      TabIndex        =   24
      Top             =   9480
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox EcCodLocal 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5280
      TabIndex        =   23
      Top             =   9240
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.Frame Frame1 
      Height          =   2655
      Left            =   120
      TabIndex        =   12
      Top             =   0
      Width           =   11655
      Begin VB.CommandButton BtRefresh 
         Height          =   495
         Left            =   10920
         Picture         =   "FormParamAmbienteV2.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   40
         ToolTipText     =   "Renova valores das chaves"
         Top             =   1560
         Width           =   615
      End
      Begin VB.ListBox EcLocais 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   930
         Left            =   6600
         Style           =   1  'Checkbox
         TabIndex        =   11
         Top             =   1560
         Width           =   4215
      End
      Begin VB.ComboBox EcNomeComputador 
         Height          =   315
         Left            =   3600
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   1560
         Width           =   2775
      End
      Begin VB.ComboBox EcUtilizador 
         Height          =   315
         Left            =   3600
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   2160
         Width           =   2775
      End
      Begin VB.TextBox EcObs 
         Appearance      =   0  'Flat
         Height          =   885
         Left            =   6600
         MultiLine       =   -1  'True
         TabIndex        =   10
         Top             =   360
         Width           =   4935
      End
      Begin VB.TextBox EcChave 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   120
         TabIndex        =   2
         Top             =   960
         Width           =   3255
      End
      Begin VB.ComboBox EcAmbito 
         Height          =   315
         Left            =   3600
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   960
         Width           =   2775
      End
      Begin VB.ComboBox EcAcesso 
         Height          =   315
         Left            =   3600
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   360
         Width           =   2775
      End
      Begin VB.TextBox EcCodigo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   360
         Width           =   3255
      End
      Begin VB.TextBox EcConteudoFixo 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   5
         Top             =   2160
         Width           =   3255
      End
      Begin VB.ComboBox EcConteudo_Combo 
         Height          =   315
         ItemData        =   "FormParamAmbienteV2.frx":0CD6
         Left            =   120
         List            =   "FormParamAmbienteV2.frx":0CD8
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1560
         Visible         =   0   'False
         Width           =   3255
      End
      Begin VB.TextBox EcConteudo 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Top             =   1560
         Width           =   3255
      End
      Begin VB.Label Label1 
         Caption         =   "Locais"
         Height          =   255
         Left            =   6600
         TabIndex        =   22
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label LaObs 
         Caption         =   "Observa��o"
         Height          =   255
         Left            =   6600
         TabIndex        =   21
         Top             =   120
         Width           =   975
      End
      Begin VB.Label LaConteudo 
         Caption         =   "Conte�do"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label LaUtilizador 
         Caption         =   "Utilizador"
         Height          =   255
         Left            =   3600
         TabIndex        =   19
         Top             =   1920
         Width           =   975
      End
      Begin VB.Label LaNomeComputador 
         Caption         =   "Computador"
         Height          =   255
         Left            =   3600
         TabIndex        =   18
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label LaAmbito 
         Caption         =   "�mbito"
         Height          =   255
         Left            =   3600
         TabIndex        =   17
         Top             =   720
         Width           =   615
      End
      Begin VB.Label LaAcesso 
         Caption         =   "Acesso"
         Height          =   255
         Left            =   3600
         TabIndex        =   16
         Top             =   120
         Width           =   735
      End
      Begin VB.Label LaCodigo 
         Caption         =   "C�digo"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   120
         Width           =   615
      End
      Begin VB.Label LaConteudoFixo 
         Caption         =   "Conte�do fixo (o separador de op��es � o ;)"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   1920
         Width           =   5655
      End
      Begin VB.Label LaChave 
         Caption         =   "Chave"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   720
         Width           =   615
      End
   End
   Begin MSComctlLib.ImageList ImageListIcons 
      Left            =   6360
      Top             =   9360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormParamAmbienteV2.frx":0CDA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormParamAmbienteV2.frx":0E34
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormParamAmbienteV2.frx":0F8E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   2400
      TabIndex        =   31
      Top             =   9000
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label12 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   120
      TabIndex        =   30
      Top             =   9120
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label11 
      Caption         =   "Data Actualizacao"
      Height          =   255
      Left            =   2160
      TabIndex        =   29
      Top             =   9240
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label9 
      Caption         =   "Utiliz. Actualizacao"
      Height          =   255
      Left            =   960
      TabIndex        =   28
      Top             =   9240
      Visible         =   0   'False
      Width           =   1695
   End
End
Attribute VB_Name = "FormParamAmbienteV2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos

Public rs As ADODB.recordset

Const mediAcesso_NaoVisualiza = 0
Const mediAcesso_ApenasVisualiza = 1
Const mediAcesso_VisualizaAltera = 2

Private Type chave
    
    Index As Long
    Codigo As String
    acesso As String
    ambito As String
    cod_computador As String
    cod_utilizador As String
    chave As String
    conteudo As String
    conteudo_fixo As String
    obs As String
    user_cri As String
    dt_cri As String
    user_act As String
    dt_act As String
    cod_local As String
    empresa_id As String
    
End Type

Private eChaves() As chave

Private TotalChaves As Long

Private Enum ListViewIcons
    
    cIconCollapsed = 1
    cIconExpanded = 2
    cIconDetail = 3
    
End Enum

Private Sub BtValidarRegisto_Click()
    BL_Inserir_Modificar
End Sub

Private Sub BtRefresh_Click()
    CarregaParamAmbiente
End Sub

Sub EcAmbito_Click()
    Dim i As Integer

    If BG_DaComboSel(EcAmbito) = mediAmbitoGeral Then
        EcNomeComputador.ListIndex = mediComboValorNull
        LaNomeComputador.Enabled = False
        EcNomeComputador.Enabled = False
        
        EcUtilizador.ListIndex = mediComboValorNull
        LaUtilizador.Enabled = False
        EcUtilizador.Enabled = False
    End If
    
    If BG_DaComboSel(EcAmbito) = mediAmbitoComputador Then
        If gOpParamAmbiente = 2 Then
            i = 0
            While i < EcNomeComputador.ListCount Or Trim(EcNomeComputador.List(i)) = Trim(BG_SYS_GetComputerName)
                If Trim(EcNomeComputador.List(i)) = Trim(BG_SYS_GetComputerName) Then
                    EcNomeComputador.ListIndex = i
                End If
                i = i + 1
            Wend
        End If
        
        LaNomeComputador.Enabled = True
        EcNomeComputador.Enabled = True
        
        EcUtilizador.ListIndex = mediComboValorNull
        LaUtilizador.Enabled = False
        EcUtilizador.Enabled = False
    End If
    
    If BG_DaComboSel(EcAmbito) = mediAmbitoUtilizador Then
        EcNomeComputador.ListIndex = mediComboValorNull
        EcNomeComputador.Enabled = False
        LaNomeComputador.Enabled = False
        
        If gOpParamAmbiente = 2 Then
            EcUtilizador = gIdUtilizador
        End If
        LaUtilizador.Enabled = True
        EcUtilizador.Enabled = True
    End If
    
    If (LvChaves.ListItems.Count > Empty) Then: AlteraCampoEstrutura LvChaves.SelectedItem.Index, EcAmbito.Name, BG_DaComboSel(EcAmbito)
    
End Sub

Private Sub EcChave_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If (LvChaves.ListItems.Count > Empty) Then: AlteraCampoEstrutura LvChaves.SelectedItem.Index, EcChave.Name, EcChave.Text
    
End Sub

Private Sub EcConteudo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If (LvChaves.ListItems.Count > Empty) Then: AlteraCampoEstrutura LvChaves.SelectedItem.Index, EcConteudo.Name, EcConteudo.Text
    
End Sub

Private Sub EcAcesso_Click()
    
    If (LvChaves.ListItems.Count > Empty) Then: AlteraCampoEstrutura LvChaves.SelectedItem.Index, EcAcesso.Name, BG_DaComboSel(EcAcesso)
    
End Sub

Private Sub EcConteudo_Validate(Cancel As Boolean)
    EcConteudo_KeyDown vbKeyReturn, False

End Sub

Private Sub EcNomeComputador_Click()
    
    If (LvChaves.ListItems.Count > Empty) Then: AlteraCampoEstrutura LvChaves.SelectedItem.Index, EcNomeComputador.Name, BG_DaComboSel(EcNomeComputador)
    
End Sub

Private Sub EcObs_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If (LvChaves.ListItems.Count > Empty) Then: AlteraCampoEstrutura LvChaves.SelectedItem.Index, EcObs.Name, EcObs.Text
    
End Sub
    
Private Sub EcChave_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcChave_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Sub EcConteudo_Combo_Click()
    
    EcConteudo = EcConteudo_Combo
    If (LvChaves.ListItems.Count > Empty) Then
        AlteraCampoEstrutura LvChaves.SelectedItem.Index, EcConteudo_Combo.Name, EcConteudo_Combo
    End If
   
End Sub

Private Sub EcConteudo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcConteudo_LostFocus()

    BG_ValidaTipoCampo_ADO Me, CampoActivo
    
End Sub

Private Sub EcConteudoFixo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcConteudoFixo_LostFocus()

    BG_ValidaTipoCampo_ADO Me, CampoActivo
    
End Sub

Private Sub EcNomeComputador_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcUtilizador_Click()
    
    Dim SQLQuery As String
    Dim rsResult As ADODB.recordset
    
    Set rsResult = New ADODB.recordset
    
    If EcUtilizador.ListIndex = mediComboValorNull Then
        EcUtilizador.ToolTipText = ""
    Else
        rsResult.CursorType = adOpenStatic
        rsResult.CursorLocation = adUseServer
        rsResult.Open "SELECT nome FROM " & gBD_PREFIXO_TAB & "idutilizadores WHERE cod_utilizador = " & EcUtilizador.ItemData(EcUtilizador.ListIndex), gConexao
        If rsResult.RecordCount = 0 Then
            ' Nada
        Else
            EcUtilizador.ToolTipText = rsResult("nome")
        End If
        rsResult.Close
        Set rsResult = Nothing
    End If
    
    If (LvChaves.ListItems.Count > Empty) Then: AlteraCampoEstrutura LvChaves.SelectedItem.Index, EcUtilizador.Name, BG_DaComboSel(EcUtilizador)
   
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub
Sub Inicializacoes()
    Me.caption = "Parametriza��es do Ambiente"
    If gOpParamAmbiente = 1 Then
        Me.caption = Me.caption & " (Geral)"
    Else
        Me.caption = Me.caption & " (Local)"
    End If
    Me.left = 5
    Me.top = 5
    Me.Width = 12000
    Me.Height = 8670
    
    NomeTabela = cParamAmbiente
    Set CampoDeFocus = EcChave
    
    NumCampos = 12
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "codigo"
    CamposBD(1) = "acesso"
    CamposBD(2) = "ambito"
    CamposBD(3) = "cod_computador"
    CamposBD(4) = "cod_utilizador"
    CamposBD(5) = "chave"
    CamposBD(6) = "conteudo_fixo"
    CamposBD(7) = "obs"
    CamposBD(8) = "user_cri"
    CamposBD(9) = "dt_cri"
    CamposBD(10) = "user_act"
    CamposBD(11) = "dt_act"
    
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcAcesso
    Set CamposEc(2) = EcAmbito
    Set CamposEc(3) = EcNomeComputador
    Set CamposEc(4) = EcUtilizador
    Set CamposEc(5) = EcChave
    Set CamposEc(6) = EcConteudoFixo
    Set CamposEc(7) = EcObs
    Set CamposEc(8) = EcUtilizadorCriacao
    Set CamposEc(9) = EcDataCriacao
    Set CamposEc(10) = EcUtilizadorActualizacao
    Set CamposEc(11) = EcDataActualizacao
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Acesso"
    TextoCamposObrigatorios(2) = "�mbito"
    TextoCamposObrigatorios(5) = "Chave"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "codigo"
    Set ChaveEc = EcCodigo
    
    CamposBDparaListBox = Array("chave")
    NumEspacos = Array(40)
    
    
    TotalChaves = 0
    ReDim eChaves(TotalChaves)
    
    DefineLvChaves
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    
    estado = 1
    BG_StackJanelas_Push Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    If gOpParamAmbiente <> 1 And gOpParamAmbiente <> 2 Then
        BG_LogFile_Erros "Abertura de Parametriza��es inv�lida! Par�metro errado para 'gOpParamAmbiente'."
        Unload Me
    End If
End Sub

Sub EventoUnload()
    gOpParamAmbiente = 0

    gPassaParams.Param(0) = EcCodigo
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormParamAmbiente = Nothing
End Sub

Sub LimpaCampos(Optional limpa_lista As Boolean)
    Me.SetFocus
    EcCodigo = ""
    BG_LimpaCampo_Todos CamposEc
    
    EcConteudo_Combo.ListIndex = mediComboValorNull
    EcNomeComputador.ListIndex = mediComboValorNull
    
    LaUtilCriacao.caption = Empty
    LaUtilActualizacao.caption = Empty
    LaDataCriacao.caption = Empty
    LaDataActualizacao.caption = Empty
    EcConteudo = ""
    EcConteudo_Combo.Clear
    EcConteudo.Visible = True
    EcConteudo_Combo.Visible = False
    If (limpa_lista) Then
        TotalChaves = 0
        ReDim eChaves(TotalChaves)
        LvChaves.ListItems.Clear
        LimpaSeleccionados
    End If
    
End Sub

Sub DefTipoCampos()
    EcConteudo.Tag = adVarChar
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataActualizacao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    
End Sub

Sub PreencheValoresDefeito()
    
    Dim ValoresComboAcesso As Variant
    Dim ValoresComboAmbito As Variant
    Dim QueryUtilizador As String
    
    If gOpParamAmbiente = 1 Then
        QueryUtilizador = "SELECT cod_utilizador, utilizador FROM " & gBD_PREFIXO_TAB & "idutilizador WHERE flg_removido <> 1"
        BG_PreencheComboBD_ADO QueryUtilizador, "cod_utilizador", "utilizador", EcUtilizador, mediAscComboDesignacao
    
    ElseIf gOpParamAmbiente = 2 Then
        LaConteudoFixo.Visible = False
        EcConteudoFixo.Visible = False
        EcConteudoFixo.Enabled = False
        
        LaObs.BackColor = Me.BackColor
        EcObs.BackColor = Me.BackColor
        EcObs.Enabled = True
    
        ValoresComboAcesso = Array(mediAcesso_ApenasVisualiza, "Apenas visualiza", mediAcesso_VisualizaAltera, "Visualiza e permite altera��es")
        ValoresComboAmbito = Array(mediAmbitoComputador, "Computador", mediAmbitoUtilizador, "Utilizador")
        QueryUtilizador = "SELECT cod_utilizador, utilizador FROM " & gBD_PREFIXO_TAB & "idutilizador WHERE flg_removido <> 1 AND cod_utilizador = " & gCodUtilizador
    End If
    
    
    BG_PreencheComboBD_ADO "" & gBD_PREFIXO_TAB & "tbf_t_acesso", "cod_t_acesso", "descr_t_acesso", EcAcesso
    BG_PreencheComboBD_ADO "" & gBD_PREFIXO_TAB & "tbf_t_ambito", "cod_t_ambito", "descr_t_ambito", EcAmbito
    BG_PreencheComboBD_ADO "" & gBD_PREFIXO_TAB & "computador", "cod_computador", "descr_computador", EcNomeComputador
    BG_PreencheComboBD_ADO "GR_empr_inst", "cod_empr", "nome_empr", EcLocais, mediAscComboCodigo
    
End Sub

Sub PreencheCampos()
    
    Dim i As Integer
    
    On Error Resume Next

    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorActualizacao, LaUtilCriacao, LaUtilActualizacao
        LaDataCriacao = EcDataCriacao
        LaDataActualizacao = EcDataActualizacao
        
        If EcConteudoFixo <> "" Then
            EcConteudo_Combo.left = EcConteudo.left
            EcConteudo_Combo.top = EcConteudo.top
            EcConteudo.Visible = False
            EcConteudo_Combo.Visible = True
            
            PreencheCampos_ComboConteudo
            EcConteudo_Combo = EcConteudo
            
            For i = 0 To EcConteudo_Combo.ListCount
                If EcConteudo_Combo.List(i) = Trim(EcConteudo) Then
                    EcConteudo_Combo.ListIndex = i
                    Exit For
                End If
            Next
        Else
            EcConteudo.Visible = True
            EcConteudo_Combo.Visible = False
        End If
        
        If BG_DaComboSel(EcAcesso) = mediAcesso_ApenasVisualiza Then
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
        Else
            BL_Toolbar_BotaoEstado "Modificar", "Activo"
            BL_Toolbar_BotaoEstado "Remover", "Activo"
        End If
          
    End If
End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        LimpaCampos
        LvChaves.ListItems.Clear
        LimpaSeleccionados
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Sub FuncaoLimpar(Optional limpa_lista As Boolean)
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos limpa_lista
        CampoDeFocus.SetFocus
    End If

End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
    
    Dim SelTotal As Boolean
    Dim sLocais As String
    Dim CriterioTabela2 As String
    
    
    estado = 2
    BL_InicioProcessamento Me, " A procurar registos..."
    BL_ToolbarEstadoN 2
    Me.MousePointer = vbArrowHourglass
    
    Set rs = New ADODB.recordset
    
    If gOpParamAmbiente = 2 Then
        If EcAmbito.ListIndex = mediComboValorNull And EcAmbito.ListCount > 0 Then
            EcAmbito = "Utilizador" '2 "Utilizador"
        End If
    End If
                    
    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & " WHERE acesso <> " & mediAcesso_NaoVisualiza
        If gOpParamAmbiente = 2 Then
            CriterioTabela = CriterioTabela & " AND ambito <> " & mediAmbitoGeral
        End If
    Else
        CriterioTabela = CriterioTabela & " AND acesso <> " & mediAcesso_NaoVisualiza
        If gOpParamAmbiente = 2 Then
            CriterioTabela = CriterioTabela & " AND ambito <> " & mediAmbitoGeral
        End If
    End If
    If EcConteudo <> "" Then
        CriterioTabela = CriterioTabela & " AND codigo in (SELECT cod_paramet FROM sl_paramet_locais WHERE conteudo LIKE " & BL_TrataStringParaBD(Replace(EcConteudo, "*", "%")) & ")"
    End If
    
    CriterioTabela2 = "select sl_paramet.codigo, sl_paramet.acesso, sl_paramet.ambito,sl_paramet.cod_computador, sl_paramet.cod_utilizador," & _
                      "sl_paramet.chave, sl_paramet_locais.conteudo, sl_paramet.conteudo_fixo, sl_paramet.obs, sl_paramet.user_cri," & _
                      "sl_paramet.dt_cri, sl_paramet.user_act, sl_paramet.dt_act, sl_paramet_locais.cod_Local " & _
                      " from sl_paramet, sl_paramet_locais  where sl_paramet_locais.cod_paramet in (" & Replace((CriterioTabela), "SELECT *", "SELECT CODIGO") & ") " & _
                      " and sl_paramet.codigo = sl_paramet_locais.cod_paramet"
    sLocais = DevolveLocaisPesquisa
    If (sLocais <> Empty) Then: CriterioTabela2 = CriterioTabela2 & " AND " & sLocais
    CriterioTabela2 = CriterioTabela2 & " UNION select sl_paramet.codigo, sl_paramet.acesso, sl_paramet.ambito,sl_paramet.cod_computador, sl_paramet.cod_utilizador," & _
                      "sl_paramet.chave,  null conteudo, sl_paramet.conteudo_fixo, sl_paramet.obs, sl_paramet.user_cri," & _
                      "sl_paramet.dt_cri, sl_paramet.user_act, sl_paramet.dt_act, '-1' cod_Local " & _
                      " from sl_paramet where codigo not in (Select cod_paramet from sl_paramet_locais) AND codigo IN   (" & Replace((CriterioTabela), "SELECT *", "SELECT CODIGO") & ") "
    If (sLocais <> Empty) Then: CriterioTabela2 = CriterioTabela2 & " AND " & sLocais
    CriterioTabela = CriterioTabela & " ORDER BY codigo ASC"
    CriterioTabela2 = CriterioTabela2 & " ORDER BY codigo ASC,cod_local ASC"
    
    BG_LogFile_Erros CriterioTabela

    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open CriterioTabela, gConexao
        
    If rs.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, (IDS_MSG_FUNCAO_PROCURAR), vbExclamation, (IDS_MSG_FUNCAO_PROCURAR_TITULO)
        FuncaoLimpar
        BL_FimProcessamento Me
        BL_ToolbarEstadoN 1
        Me.MousePointer = vbArrow
        MDIFormInicio.MousePointer = vbArrow
    
    Else
        estado = 2
        LimpaCampos True
        
        LvChaves.Visible = False
        Dim lCursor As Long
        lCursor = LvChaves.MousePointer
        LvChaves.MousePointer = vbHourglass
        LockWindowUpdate LvChaves.hwnd
        Call LeRecordSet(CriterioTabela2)
        WriteIntoListView
        If (LvChaves.ListItems.Count > Empty) Then: Call LvChaves_ItemClick(LvChaves.SelectedItem)
        LockWindowUpdate 0&
        LvChaves.MousePointer = lCursor
        LvChaves.Visible = True
        BL_ToolbarEstadoN estado ' Tem que estar antes de PreencheCampos, porque nesse procedimento s�o tiradas permiss�es aos bot�es.
        PreencheCampos
    End If
    
    BL_FimProcessamento Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    LvChaves.MousePointer = vbArrow
    
    EcChave.SetFocus
End Sub

Sub FuncaoAnterior()
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BG_MensagemAnterior
    Else
        LvChaves_KeyDown vbKeyUp, False
    End If
End Sub

Sub FuncaoSeguinte()
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BG_MensagemSeguinte
    Else
        LvChaves_KeyDown vbKeyDown, False
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = IDS_MSG_FUNCAO_INSERIR_TITULO
    gMsgMsg = IDS_MSG_FUNCAO_INSERIR
    gMsgResp = vbYes
    Me.SetFocus
    
    If BG_DaComboSel(EcAmbito) = mediAmbitoComputador And EcNomeComputador.ListIndex = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "Obrigat�rio indicar Computador.", vbExclamation, (gMsgTitulo)
        Exit Sub
    End If
    
    If BG_DaComboSel(EcAmbito) = mediAmbitoUtilizador And EcUtilizador = "" Then
        BG_Mensagem mediMsgBox, "Obrigat�rio indicar Utilizador.", vbExclamation, (gMsgTitulo)
        Exit Sub
    End If
    
    If gMsgResp = vbYes Then
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Date
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
        End If
    End If
End Sub

Sub BD_Insert()
    Dim SQLQuery As String
    Set rs = New ADODB.recordset
   
    EcCodigo = BG_DaMAX(NomeTabela, "codigo") + 1
    ' pferreira 2010.03.29
    EcCodLocal.Text = gCodLocal
    
    SQLQuery = "SELECT " & ChaveBD & " FROM " & NomeTabela & " WHERE " & ChaveBD & " = " & ChaveEc
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open SQLQuery, gConexao
    If rs.RecordCount <> 0 Then
        BG_Mensagem mediMsgBox, (IDS_MSG_REGISTO_EXISTENTE), vbExclamation, (IDS_MSG_FUNCAO_INSERIR_TITULO)
    Else
        SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
        BG_ExecutaQuery_ADO SQLQuery
        
        AssociaUtilLocais CLng(EcCodigo.Text)
        BG_Mensagem mediMsgBox, "Registo(s) inserido(s)", vbInformation, (IDS_MSG_FUNCAO_INSERIR_TITULO)
        
    End If
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = IDS_MSG_FUNCAO_MODIFICAR_TITULO
    gMsgMsg = IDS_MSG_FUNCAO_MODIFICAR
    
    'BRUNODSANTOS 20.04.2017 - Glintt-HS-15164
    'gMsgResp = BG_Mensagem(mediMsgBox, (gMsgMsg), vbYesNo + vbDefaultButton2 + vbQuestion, (gMsgTitulo))
    gMsgResp = BG_Mensagem(mediMsgBox, "Tem a certeza que quer validar as altera��es?", vbYesNo + vbDefaultButton2 + vbQuestion, "Modificar")
    '
    
    If BG_DaComboSel(EcAmbito) = mediAmbitoComputador And EcNomeComputador = "" Then
        BG_Mensagem mediMsgBox, "Obrigat�rio indicar Computador.", vbExclamation, (gMsgTitulo)
        Exit Sub
    End If
    
    If BG_DaComboSel(EcAmbito) = mediAmbitoUtilizador And EcUtilizador = "" Then
        BG_Mensagem mediMsgBox, "Obrigat�rio indicar Utilizador.", vbExclamation, gMsgTitulo
        Exit Sub
    End If
    
    If gMsgResp = vbYes Then
        EcDataActualizacao = Date
        EcUtilizadorActualizacao = gCodUtilizador
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Update
        End If
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
                
    ChaveEc = EcCodigo
    condicao = ChaveBD & " = " & ChaveEc
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)

    BG_ExecutaQuery_ADO SQLQuery
    
    AssociaUtilLocais CLng(EcCodigo.Text)
    
    BG_Mensagem mediMsgBox, "Registo(s) modificado(s)", vbInformation, (IDS_MSG_FUNCAO_MODIFICAR_TITULO)
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    FuncaoProcurar
    
End Sub

Sub FuncaoRemover()
    gMsgTitulo = IDS_MSG_FUNCAO_REMOVER_TITULO
    gMsgMsg = IDS_MSG_FUNCAO_REMOVER
    gMsgResp = BG_Mensagem(mediMsgBox, (gMsgMsg), vbYesNo + vbDefaultButton2 + vbQuestion, (gMsgTitulo))
    
    If gMsgResp = vbYes Then
        BD_Delete
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
        
    condicao = ChaveBD & " = " & EcCodigo
    SQLQuery = "DELETE FROM sl_paramet_locais WHERE " & Replace(condicao, "codigo", "Cod_paramet")
    BG_ExecutaQuery_ADO SQLQuery
    
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    BG_Mensagem mediMsgBox, "Parametriza��o removida.", vbInformation, (IDS_MSG_FUNCAO_REMOVER_TITULO)
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
    
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, (IDS_MSG_FUNCAO_REMOVER_REG_UNICO), vbExclamation, (IDS_MSG_FUNCAO_REMOVER_TITULO)
        FuncaoEstadoAnterior
        Exit Sub
    End If
        
    FuncaoProcurar
    
End Sub

Sub PreencheCampos_ComboConteudo()
    Dim i As Integer
    Dim sStr As String
    Dim sSeparador As String
    
    EcConteudo_Combo.Clear
    
    sSeparador = ";"
    sStr = EcConteudoFixo
    i = InStr(sStr, sSeparador)
    If i = 0 Then
        EcConteudo_Combo.AddItem Trim(sStr)
    Else
        sStr = EcConteudoFixo
        While i <> 0
            EcConteudo_Combo.AddItem Trim(left(sStr, i - 1))
            sStr = Right(sStr, Len(sStr) - i)
            
            i = InStr(sStr, sSeparador)
        Wend
        EcConteudo_Combo.AddItem Trim(sStr)
    End If
End Sub

Private Sub InsereItemEstrutura(Codigo As String, Index As Long, chave As String, Optional acesso As String, Optional ambito As String, _
                                Optional cod_computador As String, Optional cod_utilizador As String, Optional conteudo As String, _
                                Optional conteudo_fixo As String, Optional obs As String, Optional user_cri As String, _
                                Optional dt_cri As String, Optional user_act As String, Optional dt_act As String, _
                                Optional cod_local As String, Optional empresa_id As String)

    TotalChaves = TotalChaves + 1
    ReDim Preserve eChaves(TotalChaves)
    eChaves(TotalChaves).Index = Index
    eChaves(TotalChaves).Codigo = Codigo
    eChaves(TotalChaves).acesso = acesso
    eChaves(TotalChaves).ambito = ambito
    eChaves(TotalChaves).cod_computador = cod_computador
    eChaves(TotalChaves).cod_utilizador = cod_utilizador
    eChaves(TotalChaves).chave = chave
    eChaves(TotalChaves).conteudo = conteudo
    eChaves(TotalChaves).conteudo_fixo = conteudo_fixo
    eChaves(TotalChaves).obs = obs
    eChaves(TotalChaves).user_cri = user_cri
    eChaves(TotalChaves).dt_cri = dt_cri
    eChaves(TotalChaves).user_act = user_act
    eChaves(TotalChaves).dt_act = dt_act
    eChaves(TotalChaves).cod_local = cod_local
    eChaves(TotalChaves).user_act = empresa_id
    
End Sub

Private Sub DefineLvChaves()
   
    On Error GoTo ErrorHandler
    With LvChaves
        .ColumnHeaders.Add(, , "C�digo", 1200, lvwColumnLeft).Key = "CODIGO"
        .ColumnHeaders.Add(, , "Chave", 2500, lvwColumnLeft).Key = "CHAVE"
        .ColumnHeaders.Add(, , "Conte�do", 2500, lvwColumnLeft).Key = "CONTEUDO"
        .ColumnHeaders.Add(, , "Local", 1800, lvwColumnLeft).Key = "LOCAL"
        .ColumnHeaders.Add(, , "Obs", 3350, lvwColumnLeft).Key = "OBS"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .AllowColumnReorder = False
        .SmallIcons = ImageListIcons
        .FullRowSelect = True
        .Checkboxes = False
        .ForeColor = &H808080
        .MultiSelect = False
    End With
    SetListViewColor LvChaves, PictureListColor, &H8000000F, vbWhite
    Exit Sub

ErrorHandler:
    Exit Sub
    
End Sub

Private Sub LvChaves_DblClick()

    Dim bParent As Boolean
    Dim lStructureIndex As Long
    
    On Error GoTo ErrorHandler
    lStructureIndex = GetStructureIndex(LvChaves.SelectedItem.Index, bParent)
    If (LvChaves.SelectedItem.Index <> Empty) Then
        If (bParent) Then: HandleListViewTree LvChaves.SelectedItem, Empty
    End If
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub HandleListViewTree(ByVal item As MSComctlLib.ListItem, iManualKey As Variant)
   
    On Error GoTo ErrorHandler
    If (item.SmallIcon = ListViewIcons.cIconCollapsed And BL_HandleNull(iManualKey, vbKeyRight) = vbKeyRight) Then: ExpandListViewItem item: Exit Sub
    If (item.SmallIcon = ListViewIcons.cIconExpanded And BL_HandleNull(iManualKey, vbKeyLeft) = vbKeyLeft) Then: CollapseListViewItem item: Exit Sub
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub CollapseListViewItem(ByVal item As MSComctlLib.ListItem)
   
    Dim lItem As Long
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    
    On Error GoTo ErrorHandler
    Call GetStructureBounds(item.Text, lLowerBound, lUpperBound)
    For lItem = lLowerBound To lUpperBound: Call RemoveListViewItem(item): Next
    item.SmallIcon = ListViewIcons.cIconCollapsed
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub ExpandListViewItem(ByVal item As MSComctlLib.ListItem)

    Dim lItem As Long
    Dim iShift As Integer
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    
    On Error GoTo ErrorHandler
    Call GetStructureBounds(item.Text, lLowerBound, lUpperBound)
    iShift = item.Index + 1
    For lItem = lLowerBound To lUpperBound: Call AddListViewItem(lItem, CLng(iShift), False, True): iShift = iShift + 1: Next
    item.SmallIcon = ListViewIcons.cIconExpanded
    Exit Sub

ErrorHandler:
    Exit Sub

End Sub

Private Sub GetStructureBounds(Codigo As String, Optional ByRef lLowerBound As Long, Optional ByRef lUpperBound As Long)

    Dim lItem As Long
    
    On Error GoTo ErrorHandler
    For lItem = 1 To UBound(eChaves)
       If (eChaves(lItem).Codigo = Codigo) Then
            If (eChaves(lItem).Index = Empty) Then: lLowerBound = lItem + 1
            lUpperBound = lItem
        End If
    Next
    Exit Sub
    
ErrorHandler:
    Exit Sub

End Sub

Private Function GetStructureRecordSetIndex(Codigo As String) As Long

    Dim lIndex As Long
    Dim lItem As Long
    
    On Error GoTo ErrorHandler
    For lItem = 1 To UBound(eChaves)
        If (eChaves(lItem).Codigo = Codigo And eChaves(lItem).Index = Empty) Then
            Exit Function
        ElseIf (eChaves(lItem).Index = Empty) Then
            GetStructureRecordSetIndex = GetStructureRecordSetIndex + 1
        End If
    Next
    Exit Function
    
ErrorHandler:
    Exit Function

End Function

Private Function GetStructureIndex(lItem As Long, Optional ByRef bParent) As Integer
    
    

    On Error GoTo ErrorHandler
    GetStructureIndex = lItem
    GetStructureIndex = CInt(Split(LvChaves.ListItems(lItem).Key, "KEY_")(1))
    bParent = CBool(eChaves(GetStructureIndex).Index = Empty)
    Exit Function
    
ErrorHandler:
    Exit Function
    
End Function

Private Sub AddListViewItem(lItem As Long, lIndex As Long, Optional bParent As Boolean, Optional bAutomatic As Boolean)

    On Error GoTo ErrorHandler
    With LvChaves.ListItems.Add(lIndex, "KEY_" & lItem, IIf(bParent, eChaves(lItem).Codigo, Empty), , IIf(bParent, IIf(bAutomatic, ListViewIcons.cIconExpanded, ListViewIcons.cIconCollapsed), Empty))
        .ListSubItems.Add , , IIf(bParent, UCase(eChaves(lItem).chave), Empty), IIf(Not bParent, ListViewIcons.cIconDetail, Empty)
        .ListSubItems.Add , , IIf(Not bParent, UCase(eChaves(lItem).conteudo), Empty)
        .ListSubItems.Add , , IIf(Not bParent, UCase(BL_DaConteudoCombo(eChaves(lItem).cod_local, EcLocais)), Empty)
        .ListSubItems.Add , , IIf(Not bParent, UCase(eChaves(lItem).obs), Empty)
    End With
    LvChaves.ListItems(lIndex).Bold = True
    Call CleanDispensableSubItems(lIndex)
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub RemoveListViewItem(ByVal item As MSComctlLib.ListItem)
   
    On Error GoTo ErrorHandler
    LvChaves.ListItems.Remove item.Index + 1
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub SetListViewColor(lvCtrlListView As ListView, pCtrlPictureBox As PictureBox, lColorOne As Long, Optional lColorTwo As Long)

    Dim lColor1     As Long
    Dim lColor2     As Long
    Dim lBarWidth   As Long
    Dim lBarHeight  As Long
    Dim lLineHeight As Long
    
    On Error GoTo ErrorHandler
    lColor1 = lColorOne
    lColor2 = lColorTwo
    lvCtrlListView.Picture = LoadPicture("")
    lvCtrlListView.Refresh
    pCtrlPictureBox.Cls
    pCtrlPictureBox.AutoRedraw = True
    pCtrlPictureBox.BorderStyle = vbBSNone
    pCtrlPictureBox.ScaleMode = vbTwips
    pCtrlPictureBox.Visible = False
    lvCtrlListView.PictureAlignment = lvwTile
    pCtrlPictureBox.Font = lvCtrlListView.Font
    pCtrlPictureBox.top = lvCtrlListView.top
    pCtrlPictureBox.Font = lvCtrlListView.Font
    With pCtrlPictureBox.Font
        .Size = lvCtrlListView.Font.Size + 2
        .Bold = lvCtrlListView.Font.Bold
        .Charset = lvCtrlListView.Font.Charset
        .Italic = lvCtrlListView.Font.Italic
        .Name = lvCtrlListView.Font.Name
        .Strikethrough = lvCtrlListView.Font.Strikethrough
        .Underline = lvCtrlListView.Font.Underline
        .Weight = lvCtrlListView.Font.Weight
    End With
    pCtrlPictureBox.Refresh
    lLineHeight = pCtrlPictureBox.TextHeight("W") + Screen.TwipsPerPixelY
    lBarHeight = (lLineHeight * 1)
    lBarWidth = lvCtrlListView.Width
    pCtrlPictureBox.Height = lBarHeight * 2
    pCtrlPictureBox.Width = lBarWidth
    pCtrlPictureBox.Line (0, 0)-(lBarWidth, lBarHeight), lColor1, BF
    pCtrlPictureBox.Line (0, lBarHeight)-(lBarWidth, lBarHeight * 2), lColor2, BF
    pCtrlPictureBox.AutoSize = True
    lvCtrlListView.Picture = pCtrlPictureBox.Image
    lvCtrlListView.Refresh
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub CleanDispensableSubItems(lItem As Long)
    
    Dim vSubItem As Variant
    
    On Error GoTo ErrorHandler
    For Each vSubItem In LvChaves.ListItems(lItem).ListSubItems
        If (UCase(vSubItem.Text) = "NULL") Then: vSubItem.Text = Empty
    Next
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub ExpandListViewAutomatic()
    
    Dim lItem As Long
            
    On Error GoTo ErrorHandler
    For lItem = 1 To UBound(eChaves)
        Call AddListViewItem(lItem, LvChaves.ListItems.Count + 1, (eChaves(lItem).Index = Empty), True)
    Next
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub LvChaves_ItemClick(ByVal item As MSComctlLib.ListItem)
    
    Dim sItem As String
    
    sItem = Split(item.Key, "KEY_")(1)
    If (sItem <> Empty) Then
        SeleccionaItem CLng(sItem)
        SeleccionaLocais item
        MoveRecordSet CLng(sItem)
        If BG_DaComboSel(EcAcesso) = mediAcesso_ApenasVisualiza Then
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
        Else
            BL_Toolbar_BotaoEstado "Modificar", "Activo"
            BL_Toolbar_BotaoEstado "Remover", "Activo"
        End If
    End If
    
End Sub

Private Sub LvChaves_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo ErrorHandler
    If (LvChaves.ListItems.Count = Empty) Then: Exit Sub
    Select Case KeyCode
        Case vbKeyLeft, vbKeyRight: HandleListViewTree LvChaves.SelectedItem, KeyCode
        Case vbKeyUp: If (LvChaves.SelectedItem.Index > 1) Then LvChaves_ItemClick LvChaves.ListItems(CInt(LvChaves.SelectedItem.Index - 1))
        Case vbKeyDown: If (LvChaves.SelectedItem.Index < LvChaves.ListItems.Count) Then LvChaves_ItemClick LvChaves.ListItems(CInt(LvChaves.SelectedItem.Index + 1))
    End Select
    LvChaves.SetFocus
  
ErrorHandler:
    Exit Sub
    
End Sub

Private Sub WriteIntoListView()

    Dim lItem As Long
    Dim vSubItem As Variant
        
    On Error GoTo ErrorHandler
    For lItem = 1 To UBound(eChaves)
        If (eChaves(lItem).Index = 0) Then: Call AddListViewItem(lItem, LvChaves.ListItems.Count + 1, True, False)
    Next
    Exit Sub
    
ErrorHandler:
    Exit Sub

End Sub

Private Sub LeRecordSet(sql As String)
    
    Dim Codigo As String
    Dim Index As Long
    Dim rsChaves As ADODB.recordset
    
    Set rsChaves = New ADODB.recordset
    rsChaves.CursorType = adOpenStatic
    rsChaves.CursorLocation = adUseServer
    rsChaves.Open sql, gConexao
    While (Not rsChaves.EOF)
        If (Codigo = "" Or Codigo <> BL_HandleNull(rsChaves!Codigo)) Then
            Index = 0
            InsereItemEstrutura BL_HandleNull(rsChaves!Codigo, -1), Index, BL_HandleNull(rsChaves!chave, ""), _
                                BL_HandleNull(rsChaves!acesso, ""), BL_HandleNull(rsChaves!ambito, ""), _
                                BL_HandleNull(rsChaves!cod_computador, ""), BL_HandleNull(rsChaves!cod_utilizador, ""), _
                                "", BL_HandleNull(rsChaves!conteudo_fixo, ""), _
                                BL_HandleNull(rsChaves!obs, ""), BL_HandleNull(rsChaves!user_cri, ""), _
                                BL_HandleNull(rsChaves!dt_cri, ""), BL_HandleNull(rsChaves!user_act, ""), _
                                BL_HandleNull(rsChaves!dt_act, ""), "", _
                                Empty
        End If
        Index = Index + 1
        InsereItemEstrutura BL_HandleNull(rsChaves!Codigo, ""), Index, BL_HandleNull(rsChaves!chave, ""), _
                            BL_HandleNull(rsChaves!acesso, ""), BL_HandleNull(rsChaves!ambito, ""), _
                            BL_HandleNull(rsChaves!cod_computador, Empty), BL_HandleNull(rsChaves!cod_utilizador, ""), _
                            BL_HandleNull(rsChaves!conteudo, ""), BL_HandleNull(rsChaves!conteudo_fixo, ""), _
                            BL_HandleNull(rsChaves!obs, ""), BL_HandleNull(rsChaves!user_cri, ""), _
                            BL_HandleNull(rsChaves!dt_cri, ""), BL_HandleNull(rsChaves!user_act, ""), _
                            BL_HandleNull(rsChaves!dt_act, ""), BL_HandleNull(rsChaves!cod_local, ""), _
                            Empty
     
        Codigo = BL_HandleNull(rsChaves!Codigo, "")
        rsChaves.MoveNext
    Wend
    If (rsChaves.state = adStateOpen) Then: rsChaves.Close
    
End Sub

Private Sub LimpaSeleccionados()
    Dim i As Integer

    For i = 0 To EcLocais.ListCount - 1
        EcLocais.Selected(i) = False
    Next i
End Sub

Private Sub AssociaUtilLocais(Codigo As Long)
    
    Dim sSql As String
    Dim i As Integer
    Dim lItem As Long
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    Dim bParent As Boolean
    
    sSql = "DELETE FROM " & gBD_PREFIXO_TAB & "paramet_locais WHERE cod_paramet=" & Codigo
    BG_ExecutaQuery_ADO sSql
    
    If (LvChaves.ListItems.Count > 0) Then
        lItem = GetStructureIndex(LvChaves.SelectedItem.Index, bParent)
        Call GetStructureBounds(eChaves(lItem).Codigo, lLowerBound, lUpperBound)
        For lItem = lLowerBound To lUpperBound
            For i = 0 To EcLocais.ListCount - 1
                If (EcLocais.ItemData(i) = BL_HandleNull(eChaves(lItem).cod_local, Empty) And EcLocais.Selected(i)) Then
                    sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "paramet_locais(cod_paramet,conteudo,cod_local) VALUES(" & _
                           eChaves(lItem).Codigo & "," & BL_TrataStringParaBD(eChaves(lItem).conteudo) & "," & _
                           eChaves(lItem).cod_local & ")"
                    BG_ExecutaQuery_ADO sSql
                ElseIf (EcLocais.Selected(i) And (lUpperBound - lLowerBound) < EcLocais.ListCount And Not LocalDefinido(EcLocais.ItemData(i), lLowerBound, lUpperBound)) Then
                    sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "paramet_locais(cod_paramet,conteudo,cod_local) VALUES(" & _
                           EcCodigo.Text & "," & BL_TrataStringParaBD(EcConteudo.Text) & "," & _
                           EcLocais.ItemData(i) & ")"
                    BG_ExecutaQuery_ADO sSql
                End If
            Next
        Next
    Else
        For i = 0 To EcLocais.ListCount - 1
            If (EcLocais.Selected(i) = True) Then
                sSql = "INSERT INTO " & gBD_PREFIXO_TAB & "paramet_locais(cod_paramet,conteudo,cod_local) VALUES(" & _
                          EcCodigo.Text & "," & BL_TrataStringParaBD(EcConteudo.Text) & "," & _
                          EcLocais.ItemData(i) & ")"
                BG_ExecutaQuery_ADO sSql
            End If
        Next
    End If
    
End Sub


Private Function DevolveLocaisPesquisa() As String
    Dim sRetorno As String
    Dim i As Integer
    
    For i = 0 To EcLocais.ListCount - 1
        If EcLocais.Selected(i) = True Then
            If sRetorno = "" Then
                sRetorno = "" & gBD_PREFIXO_TAB & "paramet.codigo IN(SELECT DISTINCT(cod_paramet) FROM " & gBD_PREFIXO_TAB & "paramet_locais "
                sRetorno = sRetorno & " WHERE cod_local IN(" & EcLocais.ItemData(i) & ","
            Else
                sRetorno = sRetorno & EcLocais.ItemData(i) & ","
            End If
        End If
    Next i
    
    If sRetorno <> "" Then
        sRetorno = Mid(sRetorno, 1, Len(sRetorno) - 1) & "))"
    End If
    
    
    DevolveLocaisPesquisa = sRetorno
End Function

Public Sub ProcuraChave(chave As String)

    Dim oListItem As ListItem

    Set oListItem = LvChaves.FindItem(UCase(chave), lvwSubItem, LvChaves.ColumnHeaders("CHAVE").Index, ListFindItemHowConstants.lvwWhole)
    If (oListItem Is Nothing) = False Then
        Set LvChaves.SelectedItem = oListItem
        LvChaves.SelectedItem.EnsureVisible
        LvChaves.SetFocus
    Else
        BG_Mensagem mediMsgBox, "Chave n�o encontrada", vbExclamation, " Procura"
    End If

End Sub

Private Sub SeleccionaLocais(ByVal item As MSComctlLib.ListItem)

    Dim i As Integer
    Dim lItem As Long
    Dim lLowerBound As Long
    Dim lUpperBound As Long
    Dim bParent As Boolean
    
    lItem = GetStructureIndex(item.Index, bParent)
    If (Not bParent) Then: Exit Sub
    For i = 0 To EcLocais.ListCount - 1
        EcLocais.Selected(i) = False
    Next
    Call GetStructureBounds(eChaves(lItem).Codigo, lLowerBound, lUpperBound)
    For lItem = lLowerBound To lUpperBound
        For i = 0 To EcLocais.ListCount - 1
            If (EcLocais.ItemData(i) = BL_HandleNull(eChaves(lItem).cod_local, Empty)) Then: EcLocais.Selected(i) = True
        Next
    Next
    
End Sub

Private Function ApagaChave(Index As Long) As Boolean

    Dim ssql1 As String
    Dim ssql2 As String
    Dim lItem As Long
    Dim bParent As Boolean
    
    lItem = GetStructureIndex(Index, bParent)
    If (Not bParent) Then
        ssql1 = "delete from " & gBD_PREFIXO_TAB & "paramet_locais where codigo = " & eChaves(lItem).Codigo & " and cod_local = " & eChaves(lItem).cod_local
    Else
        ssql1 = "delete from " & gBD_PREFIXO_TAB & "paramet where codigo = " & eChaves(lItem).Codigo
        ssql2 = "delete from " & gBD_PREFIXO_TAB & "paramet_locais where codigo = " & eChaves(lItem).Codigo
    End If
    
    If (ssql1 <> Empty) Then: BG_ExecutaQuery_ADO ssql1: BG_Trata_BDErro
    If (ssql2 <> Empty) Then: BG_ExecutaQuery_ADO ssql2: BG_Trata_BDErro
    
End Function

Private Sub SeleccionaItem(lItem As Long)
    Dim i As Integer
    On Error GoTo TrataErro
    If (eChaves(lItem).conteudo_fixo <> Empty) Then
        EcConteudo_Combo.Visible = True
        EcConteudo.Visible = False
    Else
        EcConteudo_Combo.Visible = False
        EcConteudo.Visible = True
    End If
    'EcConteudo_Combo.Clear
    EcCodigo.Text = eChaves(lItem).Codigo
    EcChave.Text = eChaves(lItem).chave
    BG_MostraComboSel eChaves(lItem).cod_local, EcLocais
    EcConteudoFixo.Text = eChaves(lItem).conteudo_fixo
    
    BG_MostraComboSel eChaves(lItem).ambito, EcAmbito
    BG_MostraComboSel eChaves(lItem).cod_computador, EcNomeComputador
    BG_MostraComboSel eChaves(lItem).cod_utilizador, EcUtilizador
    EcObs.Text = eChaves(lItem).obs
    EcUtilizadorCriacao.Text = eChaves(lItem).user_cri
    EcUtilizadorActualizacao.Text = eChaves(lItem).user_act
    EcDataCriacao.Text = eChaves(lItem).dt_cri
    EcDataActualizacao.Text = eChaves(lItem).dt_act
    BG_MostraComboSel eChaves(lItem).acesso, EcAcesso
    If eChaves(lItem).conteudo_fixo <> "" Then
        PreencheCampos_ComboConteudo
    End If
    If (eChaves(lItem).Index = 0) Then
        EcConteudo_Combo.ListIndex = mediComboValorNull
        EcConteudo = ""
        Exit Sub
    End If
    EcConteudo.Text = eChaves(lItem).conteudo
    If (EcConteudoFixo.Text <> Empty And EcConteudo.Text <> Empty) Then
        PreencheCampos_ComboConteudo
        i = 0
        While i <= EcConteudo_Combo.ListCount - 1
            If eChaves(lItem).conteudo = EcConteudo_Combo.List(i) Then
                EcConteudo_Combo.ListIndex = i
            End If
            i = i + 1
        Wend
    Else
        EcConteudo_Combo.ListIndex = mediComboValorNull
    End If
    Exit Sub
    
TrataErro:
    BG_Mensagem mediMsgBox, "Erro ao seleccionar registo!", vbExclamation, " Ups!"
    
End Sub

Private Sub MoveRecordSet(lIndex As Long)
    
    Dim lItem As Long
    If eChaves(lIndex).Index = 0 Then Exit Sub
    rs.MoveFirst
    While eChaves(lIndex).Codigo <> rs!Codigo
        rs.MoveNext
    Wend
    
    
End Sub

Private Sub AlteraCampoEstrutura(Index As Long, nome_controlo As String, valor_controlo As String)

    Dim lItem As Long
    Dim i As Long
    lItem = GetStructureIndex(Index)
    If EcCodigo = "" Then Exit Sub
    If eChaves(lItem).Index = 0 Then
        For i = 1 To TotalChaves
            If eChaves(i).Codigo = eChaves(lItem).Codigo And eChaves(i).Index > 0 Then
                Select Case UCase(nome_controlo)
                    Case "ECCONTEUDO"
                        If EcConteudo.Visible = True Then
                            eChaves(i).conteudo = valor_controlo
                        End If
                    Case "ECCONTEUDOFIXO"
                        eChaves(i).conteudo_fixo = valor_controlo
                    Case "ECCONTEUDO_COMBO"
                        If EcConteudo_Combo.Visible = True Then
                            eChaves(i).conteudo = valor_controlo
                        End If
                    Case "ECAMBITO"
                        eChaves(i).ambito = valor_controlo
                    Case "ECACESSO"
                        eChaves(i).acesso = valor_controlo
                    Case "ECNOMECOMPUTADOR"
                        eChaves(i).cod_computador = valor_controlo
                    Case "ECUTILIZADOR"
                        eChaves(i).cod_utilizador = valor_controlo
                    Case "ECCHAVE"
                        eChaves(i).chave = valor_controlo
                    Case "ECOBS"
                        eChaves(i).obs = valor_controlo
                End Select
            End If
        Next
    Else
        Select Case UCase(nome_controlo)
                    Case "ECCONTEUDO"
                        If EcConteudo.Visible = True Then
                            eChaves(lItem).conteudo = valor_controlo
                        End If
                    Case "ECCONTEUDOFIXO"
                        eChaves(lItem).conteudo_fixo = valor_controlo
                    Case "ECCONTEUDO_COMBO"
                        If EcConteudo_Combo.Visible = True Then
                            eChaves(lItem).conteudo = valor_controlo
                        End If
                    Case "ECAMBITO"
                        eChaves(lItem).ambito = valor_controlo
                    Case "ECACESSO"
                        eChaves(lItem).acesso = valor_controlo
                    Case "ECNOMECOMPUTADOR"
                        eChaves(lItem).cod_computador = valor_controlo
                    Case "ECUTILIZADOR"
                        eChaves(lItem).cod_utilizador = valor_controlo
                    Case "ECCHAVE"
                        eChaves(lItem).chave = valor_controlo
                    Case "ECOBS"
                        eChaves(lItem).obs = valor_controlo
        End Select
    End If
    
End Sub

Private Function LocalDefinido(cod_local As Integer, lLowerBound As Long, lUpperBound As Long) As Boolean
    
    Dim lItem As Long
    For lItem = lLowerBound To lUpperBound
        If (eChaves(lItem).cod_local = cod_local) Then: LocalDefinido = True: Exit Function
    Next

End Function


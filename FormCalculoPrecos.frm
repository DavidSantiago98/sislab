VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormCalculoPrecos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormCalculoPrecos"
   ClientHeight    =   6450
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7740
   Icon            =   "FormCalculoPrecos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6450
   ScaleWidth      =   7740
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtImprimir 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Height          =   525
      Left            =   6840
      Picture         =   "FormCalculoPrecos.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   21
      ToolTipText     =   "Limpar"
      Top             =   240
      Width           =   615
   End
   Begin RichTextLib.RichTextBox EcInfo 
      Height          =   615
      Left            =   1680
      TabIndex        =   20
      Top             =   7680
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   1085
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"FormCalculoPrecos.frx":0CD6
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5415
      Left            =   120
      TabIndex        =   11
      Top             =   960
      Width           =   7335
      _ExtentX        =   12938
      _ExtentY        =   9551
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "Pre�os"
      TabPicture(0)   =   "FormCalculoPrecos.frx":0D61
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Image1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "LbTotal"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "FgAna"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Ck65"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "OptCalculo(1)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "OptCalculo(0)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "EcAux"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).ControlCount=   7
      TabCaption(1)   =   "Informa��o"
      TabPicture(1)   =   "FormCalculoPrecos.frx":0D7D
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "FgInformacao"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Quest�es"
      TabPicture(2)   =   "FormCalculoPrecos.frx":0D99
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "FgQuestoes"
      Tab(2).ControlCount=   1
      Begin VB.TextBox EcAux 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   720
         TabIndex        =   17
         Top             =   5160
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.OptionButton OptCalculo 
         Caption         =   "Utente"
         Height          =   195
         Index           =   0
         Left            =   2760
         TabIndex        =   15
         Top             =   480
         Width           =   1095
      End
      Begin VB.OptionButton OptCalculo 
         Caption         =   "Entidade"
         Height          =   255
         Index           =   1
         Left            =   3840
         TabIndex        =   14
         Top             =   480
         Width           =   1095
      End
      Begin VB.CheckBox Ck65 
         Caption         =   "Utente com mais de 65 anos"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   480
         Width           =   2535
      End
      Begin MSFlexGridLib.MSFlexGrid FgAna 
         Height          =   3495
         Left            =   240
         TabIndex        =   12
         Top             =   840
         Width           =   6735
         _ExtentX        =   11880
         _ExtentY        =   6165
         _Version        =   393216
         Cols            =   6
         BackColorBkg    =   -2147483633
         BorderStyle     =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Century Gothic"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid FgQuestoes 
         Height          =   4455
         Left            =   -74880
         TabIndex        =   18
         Top             =   480
         Width           =   6780
         _ExtentX        =   11959
         _ExtentY        =   7858
         _Version        =   393216
         BackColorBkg    =   -2147483644
         BorderStyle     =   0
      End
      Begin MSFlexGridLib.MSFlexGrid FgInformacao 
         Height          =   4695
         Left            =   -74880
         TabIndex        =   19
         Top             =   480
         Width           =   6780
         _ExtentX        =   11959
         _ExtentY        =   8281
         _Version        =   393216
         BackColorBkg    =   -2147483644
         BorderStyle     =   0
      End
      Begin VB.Label LbTotal 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   495
         Left            =   4920
         TabIndex        =   16
         Top             =   4800
         Width           =   1575
      End
      Begin VB.Image Image1 
         Height          =   480
         Left            =   6480
         Picture         =   "FormCalculoPrecos.frx":0DB5
         Top             =   4800
         Width           =   480
      End
   End
   Begin VB.CommandButton BtLimpar 
      BackColor       =   &H80000004&
      Height          =   525
      Left            =   6240
      Picture         =   "FormCalculoPrecos.frx":1A7F
      Style           =   1  'Graphical
      TabIndex        =   10
      ToolTipText     =   "Limpar"
      Top             =   240
      Width           =   615
   End
   Begin VB.CommandButton BtAdicionar 
      Height          =   525
      Left            =   5640
      Picture         =   "FormCalculoPrecos.frx":2749
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Adicionar"
      Top             =   240
      Width           =   615
   End
   Begin VB.CommandButton BtImprimirRecibo 
      Caption         =   "Imprimir"
      Height          =   615
      Left            =   240
      TabIndex        =   8
      Top             =   6720
      Width           =   735
   End
   Begin VB.TextBox EcCodAna 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      Height          =   285
      Left            =   1080
      TabIndex        =   1
      Top             =   480
      Width           =   735
   End
   Begin VB.TextBox EcDescrAna 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   1800
      TabIndex        =   6
      Top             =   480
      Width           =   3375
   End
   Begin VB.CommandButton BtPesquisaAna 
      Height          =   315
      Left            =   5160
      Picture         =   "FormCalculoPrecos.frx":2EB3
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Exames"
      Top             =   480
      Width           =   375
   End
   Begin VB.CommandButton BtPesquisaEntFin 
      Height          =   315
      Left            =   5160
      Picture         =   "FormCalculoPrecos.frx":343D
      Style           =   1  'Graphical
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
      Top             =   120
      Width           =   375
   End
   Begin VB.TextBox EcDescrEFR 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   120
      Width           =   3375
   End
   Begin VB.TextBox EcCodEFR 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1080
      TabIndex        =   0
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "An�lise"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   7
      Top             =   480
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "E.&F.R."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   495
   End
End
Attribute VB_Name = "FormCalculoPrecos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

'Controlo de Recibos
Public RegistosR As New Collection
Public RegistosRA As New Collection
Dim rs As New ADODB.recordset

Private Type informacao
    cod_ana As String
    cod_informacao As String
    descr_informacao As String
    informacao As String
End Type
Dim EstrutInfo() As informacao
Dim totalInfo As Integer

Private Type Questoes
    Resposta As String
    cod_questao As String
    descr_questao As String
End Type
Dim EstrutQuestao() As Questoes
Dim totalQuestao As Integer

Private Sub BtAdicionar_Click()
    Dim taxa As String
    Dim pos As Integer
    Dim i As Integer
    Dim RsDescrAnaS As ADODB.recordset
    Dim alerta As String
    Dim Cancel As Boolean
    Dim flg_65anos As Boolean
    Dim flgVermelho As Boolean
    
    If Ck65.value = vbChecked Then
        flg_65anos = True
    Else
        flg_65anos = False
    End If
    EcCodAna = UCase(EcCodAna)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodAna.Text) <> "" Then
        
        If IF_ContaMembros(EcCodAna, EcCodEfr) > 0 Then
            alerta = "**Dinamica**"
        Else
            alerta = ""
        End If
        
        For i = 1 To FGAna.rows - 1
            If EcDescrAna = FGAna.TextMatrix(i, 0) Then
                Exit Sub
            End If
        Next
        flgVermelho = False
    
        ' -----------------------------------------------------------
        ' TAXA
        ' -----------------------------------------------------------
        If OptCalculo(0).value = True Then
            taxa = Replace(IF_RetornaTaxaAnalise(EcCodAna, EcCodEfr, "", "", Bg_DaData_ADO, Empty, Empty, flg_65anos), ",", ".")
            If CDbl(Replace(taxa, ".", ",")) <= (-1) Then
                BG_Mensagem mediMsgBox, "An�lise n�o comparticipada por este Organismo.", vbOKOnly + vbExclamation, App.ProductName
                taxa = "0.00"
                flgVermelho = True
            End If
            pos = InStr(taxa, ".")
            If pos = 0 Then
                taxa = taxa & ".00"
            ElseIf Len(Mid(taxa, pos + 1)) = 1 Then
                taxa = taxa & "0"
            End If
            taxa = CDbl(Replace(taxa, ".", ","))
        ElseIf OptCalculo(1).value = True Then
            taxa = Replace(IF_RetornaTaxaAnaliseEntidade(EcCodAna, EcCodEfr, Bg_DaData_ADO, "", "", "", 0, 1, "", False, "", mediComboValorNull), ",", ".")
            If CDbl(Replace(taxa, ".", ",")) <= (-1) Then
                BG_Mensagem mediMsgBox, "Erro ao devolver taxa da an�lise!", vbOKOnly + vbExclamation, App.ProductName
                taxa = "0.00"
            End If
            pos = InStr(taxa, ".")
            If pos = 0 Then
                taxa = taxa & ".00"
            ElseIf Len(Mid(taxa, pos + 1)) = 1 Then
                taxa = taxa & "0"
            End If
            taxa = CDbl(Replace(taxa, ".", ","))
        End If
        ' -----------------------------------------------------------
        ' ADICIONA A ESTRUTURA
        ' -----------------------------------------------------------
        RegistosRA(RegistosRA.Count).ReciboCodFacturavel = EcCodAna
        'RegistosRA(RegistosRA.Count).descrAna = EcDescrAna
        RegistosRA(RegistosRA.Count).ReciboP1 = "1"
        RegistosRA(RegistosRA.Count).ReciboFlgFacturado = gEstadoFactRequisNaoFacturado
        RegistosRA(RegistosRA.Count).ReciboFlgRetirado = 0
        RegistosRA(RegistosRA.Count).ReciboFlgAdicionado = 0
        RegistosRA(RegistosRA.Count).ReciboNumDoc = "0"
        RegistosRA(RegistosRA.Count).ReciboSerieDoc = "0"
        RegistosRA(RegistosRA.Count).ReciboEntidade = EcCodEfr
        RegistosRA(RegistosRA.Count).ReciboOrdemMarcacao = RegistosRA.Count
        RegistosRA(RegistosRA.Count).ReciboIsencao = gTipoIsencaoNaoIsento
        RegistosRA(RegistosRA.Count).ReciboTaxaUnitario = BG_CvDecimalParaCalculo(taxa) 'Replace(Taxa, ",", ".")
        RegistosRA(RegistosRA.Count).ReciboQuantidade = BL_HandleNull(RetornaQtd(RegistosRA(RegistosRA.Count).ReciboCodFacturavel, RegistosRA(RegistosRA.Count).ReciboEntidade), 1)
        
        RegistosRA(RegistosRA.Count).ReciboTaxa = CDbl(RegistosRA(RegistosRA.Count).ReciboQuantidade * RegistosRA(RegistosRA.Count).ReciboTaxaUnitario)

        
        AdicionaAnaliseAoRecibo RegistosRA.Count
        
        FGAna.row = FGAna.rows - 1
        
        For i = 0 To FGAna.Cols - 1
            FGAna.Col = i
            If flgVermelho = True Then
                FGAna.CellForeColor = vbRed
            Else
                FGAna.CellForeColor = vbBlack
            End If
        Next
        FGAna.TextMatrix(FGAna.rows - 1, 0) = EcCodAna
        FGAna.TextMatrix(FGAna.rows - 1, 1) = alerta & EcDescrAna
        FGAna.TextMatrix(FGAna.rows - 1, 2) = RegistosRA(RegistosRA.Count).ReciboQuantidade
        FGAna.TextMatrix(FGAna.rows - 1, 3) = RegistosRA(RegistosRA.Count).ReciboTaxa
        CriaNovaClasse RegistosRA, RegistosRA.Count + 1, "REC_ANA"

        FGAna.AddItem ""
        CalculaPrecos
        AdicionaQuestaoAna EcCodAna
        AdicionaInformacaoAna EcCodAna
        EcCodAna = ""
        EcDescrAna = ""
        EcCodAna.SetFocus
    End If
End Sub

Private Sub BtImprimir_Click()
    If RegistosRA.Count <= 1 Then Exit Sub
    FuncaoImprimir
End Sub

Private Sub BtLimpar_Click()
    FuncaoLimpar
End Sub

Private Sub BtPesquisaAna_Click()
    Dim i As Integer
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim CWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    If EcCodEfr = "" Then Exit Sub
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_ana"
    CamposEcran(1) = "cod_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_ana"
    CamposEcran(2) = "descr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "slv_analises_factus"
    CampoPesquisa1 = "descr_ana"
        
    CWhere = " (flg_invisivel IS NULL OR flg_invisivel ='0') "
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, CWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar An�lises")
    
    mensagem = "N�o foi encontrada nenhuma An�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcCodAna.Text = resultados(1)
            EcDescrAna.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub





Private Sub EcAux_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        EcAux.Visible = False
        EcAux = ""
        FGAna.SetFocus
    ElseIf KeyCode = vbKeyReturn Then
        If Not IsNumeric(EcAux) Then
            EcAux = "1"
        End If
        RegistosRA(FGAna.row).ReciboQuantidade = BL_HandleNull(EcAux, 1)
        RegistosRA(FGAna.row).ReciboTaxa = CDbl(RegistosRA(FGAna.row).ReciboQuantidade * RegistosRA(FGAna.row).ReciboTaxaUnitario)
        
        FGAna.TextMatrix(FGAna.row, 2) = RegistosRA(FGAna.row).ReciboQuantidade
        FGAna.TextMatrix(FGAna.row, 3) = RegistosRA(FGAna.row).ReciboTaxa
        EcAux.Visible = False
        EcAux = ""
        FGAna.SetFocus
        CalculaPrecos
    End If
End Sub

Private Sub EcAux_LostFocus()
    EcAux_KeyDown vbKeyEscape, 0
End Sub

Private Sub EcCodAna_GotFocus()
    If EcCodEfr = "" Then
        EcCodEfr.SetFocus
    End If
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodAna_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        EcCodana_Validate True
        EcCodAna = ""
        EcDescrAna = ""
    End If
End Sub


Private Sub Fgana_DblClick()
    If FGAna.row < RegistosRA.Count Then
        EcAux = RegistosRA(FGAna.row).ReciboQuantidade
        EcAux.left = FGAna.CellLeft + FGAna.left
        EcAux.top = FGAna.CellTop + FGAna.top
        EcAux.Width = FGAna.CellWidth + 20
        EcAux.Visible = True
        EcAux.SetFocus
    End If

End Sub

Private Sub FgAna_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    If KeyCode = 46 Then ' DELETE
        If FGAna.row > 0 And FGAna.row < (FGAna.rows - 1) And FGAna.TextMatrix(FGAna.row, 0) <> "" Then
            RegistosRA.Remove (FGAna.row)
            FGAna.RemoveItem (FGAna.row)
            CalculaPrecos
        End If
    End If
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    Set CampoActivo = Me.ActiveControl
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Sub Inicializacoes()

    Me.caption = " Simula��o de Requisi��o"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 7830
    Me.Height = 6900 ' Normal
    'Me.Height = 5700 ' Campos Extras
    
    Set CampoDeFocus = EcCodEfr
    LimpaCampos
End Sub

Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "Procurar", "InActivo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
End Sub

Sub EventoUnload()

    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0

    If Not rs Is Nothing Then

        Set rs = Nothing
    End If
    
    
    Set FormCalculoPrecos = Nothing
    
End Sub

Sub LimpaCampos()
    Dim i As Integer
    'Me.SetFocus
    totalInfo = 0
    totalQuestao = 0
    ReDim EstrutInfo(0)
    ReDim EstrutQuestao(0)

    FgInformacao.rows = 2
    For i = 0 To FgInformacao.Cols - 1
        FgInformacao.TextMatrix(1, i) = ""
    Next
    FgQuestoes.rows = 2
    For i = 0 To FgQuestoes.Cols - 1
        FgQuestoes.TextMatrix(1, i) = ""
    Next
    EcCodAna = ""
    EcDescrAna = ""
    EcDescrEfr = ""
    EcCodEfr = ""
    EcCodEfr.Enabled = True
    BtPesquisaEntFin.Enabled = True
    LbTotal = "0.00"
    LimpaFGAna
    Ck65.value = vbUnchecked
    CriaNovaClasse RegistosR, 1, "REC", True
    CriaNovaClasse RegistosRA, 1, "REC_ANA", True
    SSTab1.Tab = 0
End Sub

Sub DefTipoCampos()
    EcCodAna.Tag = adVarChar
    EcCodEfr.Tag = adVarChar
    totalInfo = 0
    totalQuestao = 0
    ReDim EstrutInfo(0)
    ReDim EstrutQuestao(0)
    
    With FGAna
        .rows = 2
        .FixedRows = 1
        .Cols = 4
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarBoth
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        .ColWidth(0) = 1000
        .Col = 0
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(0, 0) = "C�digo"
        .ColWidth(1) = 3000
        .Col = 1
        .CellAlignment = flexAlignLeftCenter
        .TextMatrix(0, 1) = "An�lise"
        
        .ColWidth(2) = 500
        .Col = 2
        .CellAlignment = flexAlignLeftCenter
        .TextMatrix(0, 2) = "Qtd."
        
        .Col = 3
        .CellAlignment = flexAlignLeftCenter
        .TextMatrix(0, 3) = "Pre�o"
        .row = 1
        .Col = 0
    End With
    
 With FgInformacao
        .rows = 2
        .FixedRows = 1
        .Cols = 2
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        
        .ColWidth(0) = 2000
        .Col = 0
        .TextMatrix(0, 0) = "Nome"
        .ColAlignment(0) = flexAlignLeftCenter
        
        .ColWidth(1) = 8000
        .Col = 1
        .TextMatrix(0, 1) = "Informacao"
        .ColAlignment(1) = flexAlignLeftCenter
         
        .row = 1
        .Col = 0
    End With
    
    With FgQuestoes
        .rows = 2
        .FixedRows = 1
        .Cols = 1
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionFree
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeNever
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        
        
        .ColWidth(0) = 5000
        .Col = 0
        .TextMatrix(0, 0) = "Quest�o"
        .ColAlignment(0) = flexAlignLeftCenter
        
         
        .row = 1
        .Col = 0
    End With
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()

    If estado = 2 Then
        estado = 0
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
    
End Sub

Sub PreencheValoresDefeito()
    CriaNovaClasse RegistosR, 1, "REC", True
    CriaNovaClasse RegistosRA, 1, "REC_ANA", True
    OptCalculo(0).value = True
End Sub

Sub FuncaoProcurar()
          
End Sub

Sub FuncaoImprimir()
    Dim continua As Boolean
    ApagaTabelas
    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        continua = BL_IniciaReport("SimulacaoRequisicao", "Simulacao de Requisicao", crptToPrinter)
    Else
        continua = BL_IniciaReport("SimulacaoRequisicao", "Simulacao de Requisicao", crptToWindow)
    End If
    If continua = False Then Exit Sub
    BG_BeginTransaction
    InsereAna
    InsereInfo
    InsereQuest
    BG_CommitTransaction
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    
    Report.SQLQuery = "SELECT sl_cr_simulacao_ana.nome_computador, sl_cr_simulacao_ana.num_sessao, sl_cr_simulacao_ana.cod_ana,sl_cr_simulacao_ana.descr_ana, "
    Report.SQLQuery = Report.SQLQuery & " sl_cr_simulacao_ana.qtd, sl_cr_simulacao_ana.preco,sl_cr_simulacao_ana.flg_utente,sl_cr_simulacao_ana.cod_efr, "
    Report.SQLQuery = Report.SQLQuery & " sl_cr_simulacao_ana.descr_efr, sl_cr_simulacao_ana.flg_65anos "
    Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_simulacao_ana WHERE sl_cr_simulacao_ana.nome_computador = " & BL_TrataStringParaBD(gComputador)
    Report.SQLQuery = Report.SQLQuery & " AND sl_cr_simulacao_ana.num_sessao = " & gNumeroSessao
    Report.SQLQuery = Report.SQLQuery & " ORDER by sl_cr_simulacao_ana.descr_ana "
    
    Report.SubreportToChange = ""
    Call BL_ExecutaReport
    ApagaTabelas
End Sub

Sub LimpaFGAna()
    Dim j As Long
    
    
    j = FGAna.rows - 1
    While j > 0
        If j > 1 Then
            FGAna.RemoveItem j
        Else
            FGAna.TextMatrix(j, 0) = ""
            FGAna.TextMatrix(j, 1) = ""
            FGAna.TextMatrix(j, 2) = ""
            FGAna.TextMatrix(j, 3) = ""
        End If
        
        j = j - 1
    Wend
    
End Sub

Public Sub EcCodEFR_Validate(Cancel As Boolean)
     Cancel = PA_ValidateEFR(EcCodEfr, EcDescrEfr, "")
     LimpaFGAna
    CalculaPrecos
End Sub



Private Sub CalculaPrecos()
Dim total As String
Dim i As Integer
Dim pos As Integer
total = 0
For i = 1 To FGAna.rows - 2
    total = total + CDbl(Replace(BL_HandleNull(FGAna.TextMatrix(i, 3), 0), ".", ","))
Next
pos = InStr(total, ",")
If pos = 0 Then
    total = total & ".00"
ElseIf Len(Mid(total, pos + 1)) = 1 Then
    total = total & "0"
End If
LbTotal = Replace(total, ",", ".")
BG_CvDecimalParaCalculo (LbTotal)
End Sub

Private Sub BtPesquisaEntFin_Click()
    
    PA_PesquisaEFR EcCodEfr, EcDescrEfr, ""
End Sub





Private Sub CriaNovaClasse(Coleccao As Collection, Index As Long, tipo As String, Optional EmCascata As Variant)

    ' Tipo = ANA ou PROD
    ' EmCascata = True : Cria todos elementos at� o Index
    ' EmCascata = False: Cria apenas o elemento Index

    Dim Cascata As Boolean
    Dim i As Long
    Dim AuxRegEnt As New ClRegRecibos
    Dim AuxRegRecAna As New ClRegRecibosAna
    
    If IsMissing(EmCascata) Then
        Cascata = False
    Else
        Cascata = EmCascata
    End If
    
    If tipo = "REC_ANA" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegRecAna
            Next i
        Else
            Coleccao.Add AuxRegRecAna
        End If
    ElseIf tipo = "REC" Then
        If Cascata = True Then
            LimpaColeccao Coleccao, -1
            For i = 1 To Index
                Coleccao.Add AuxRegEnt
            Next i
        Else
            Coleccao.Add AuxRegEnt
        End If
    End If
    
    Set AuxRegRecAna = Nothing
    Set AuxRegEnt = Nothing
    
End Sub




Private Sub LimpaColeccao(Coleccao As Collection, Index As Long)

    ' Index = -1 : Apaga todos elementos
    
    Dim i As Long
    Dim Tot As Long

    Tot = Coleccao.Count
    If Index = -1 Then
        i = Tot
        While i > 0
            Coleccao.Remove i
            i = i - 1
        Wend
    Else
        Coleccao.Remove Index
    End If

End Sub

' -----------------------------------------------------------------------------------------------

' QUANDO ADICIONA UMA NOVA ANALISE, ADICIONA TAMBEM RESPECTIVA TAXA NA ESTRUTURA DE RECIBOS

' -----------------------------------------------------------------------------------------------

Private Sub AdicionaAnaliseAoRecibo(indice As Integer)
    Dim i As Long
    Dim Flg_inseriuEntidade As Boolean
    Flg_inseriuEntidade = False
    
    ' ------------------------------------------------------------------------------------------
    ' SE TAXA FOR 0 NAO E NECESSARIO ACRESCENTAR
    ' ------------------------------------------------------------------------------------------
    If CDbl(BL_HandleNull(RegistosRA(indice).ReciboTaxa, 0)) = 0 Then
        Exit Sub
    End If
    
    If RegistosR.Count = 0 Then
        CriaNovaClasse RegistosR, i + 1, "REC"
    End If
    For i = 1 To RegistosR.Count
        ' ------------------------------------------------------------------------------------------
        ' ENTIDADE JA EXISTE NA ESTRUTURA
        ' ------------------------------------------------------------------------------------------
        If RegistosRA(indice).ReciboEntidade = RegistosR(i).codEntidade Then
            If RegistosR(i).estado = gEstadoReciboNaoEmitido Then
                RegistosR(i).ValorPagar = RegistosR(i).ValorPagar + RegistosRA(indice).ReciboTaxa
                RegistosR(i).NumAnalises = RegistosR(i).NumAnalises + 1
                Flg_inseriuEntidade = True
            End If
        End If
    Next
    i = i - 1
    ' ------------------------------------------------------------------------------------------
    ' ENTIDADE NAO EXISTE NA ESTRUTURA
    ' ------------------------------------------------------------------------------------------
    If Flg_inseriuEntidade = False Then
        RegistosR(i).NumDoc = 0
        RegistosR(i).codEntidade = RegistosRA(indice).ReciboEntidade
        RegistosR(i).DescrEntidade = BL_SelCodigo("SL_EFR", "DESCR_EFR", "COD_EFR", RegistosRA(indice).ReciboEntidade)
        RegistosR(i).ValorPagar = BG_CvDecimalParaCalculo(RegistosRA(indice).ReciboTaxa)
        RegistosR(i).Caucao = 0
        RegistosR(i).UserEmi = ""
        RegistosR(i).DtEmi = ""
        RegistosR(i).HrEmi = ""
        RegistosR(i).flg_impressao = False
        RegistosR(i).DtPagamento = ""
        RegistosR(i).HrPagamento = ""
        RegistosR(i).estado = gEstadoReciboNaoEmitido
        RegistosR(i).NumAnalises = 1
        
        CriaNovaClasse RegistosR, i + 1, "REC"
    End If

End Sub

Private Sub EcCodana_Validate(Cancel As Boolean)
    Dim taxa As String
    Dim pos As Integer
    Dim i As Integer
    Dim RsDescrAnaS As ADODB.recordset
    Dim alerta As String
    
    EcCodAna = UCase(EcCodAna)
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, CampoActivo)
    If Cancel = True Then
        Exit Sub
    End If
    
    If Trim(EcCodAna.Text) <> "" Then
        Set RsDescrAnaS = New ADODB.recordset
        
        With RsDescrAnaS
            If Mid(EcCodAna, 1, 1) <> "S" And Mid(EcCodAna, 1, 1) <> "C" And Mid(EcCodAna, 1, 1) <> "P" Then
                .Source = "(SELECT cod_ana_s as cod_ana, descr_ana_s as descricao, 'S' Tipo FROM sl_ana_s WHERE " & _
                                "cod_ana_s ='S" & UCase(EcCodAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) )" & _
                            " UNION " & _
                                "(SELECT cod_ana_c as cod_ana,descr_ana_c as descricao, 'C' Tipo FROM sl_ana_c WHERE " & _
                                "cod_ana_c='C" & UCase(EcCodAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) )" & _
                            " UNION " & _
                                "(SELECT cod_perfis as cod_ana, descr_perfis as descricao, 'P' Tipo FROM sl_perfis WHERE " & _
                                "cod_perfis='P" & UCase(EcCodAna) & "' AND (flg_invisivel=0 OR flg_invisivel IS NULL) ) "
            Else
                .Source = "SELECT cod_ana , descr_ana descricao, '' tipo FROM slv_analises WHERE cod_ana= " & BL_TrataStringParaBD(UCase(EcCodAna.Text))
            End If
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrAnaS.RecordCount > 0 Then
            ' -----------------------------------------------------------
            'APOS INSERCAO DA PRIMEIRA ANALISE - NAO DIEXA MUDAR ENTIDADE
            ' -----------------------------------------------------------
            EcCodEfr.Enabled = False
            BtPesquisaEntFin.Enabled = False
            
            ' -----------------------------------------------------------
            ' DESCRICAO DA ANALISE
            ' -----------------------------------------------------------
            EcDescrAna.Text = BL_HandleNull(RsDescrAnaS!descricao, "")
            EcCodAna.Text = BL_HandleNull(RsDescrAnaS!cod_ana, "")
        Else
            EcDescrAna.Text = ""
            EcCodAna.Text = ""
        End If
        RsDescrAnaS.Close
    Else
        EcDescrAna.Text = ""
        EcCodAna.Text = ""
    End If
    If EcCodAna <> "" Then
        BtAdicionar_Click
    End If
End Sub




Private Sub OptCalculo_Click(Index As Integer)
    If EcCodEfr <> "" Then
        FuncaoLimpar
    End If
End Sub

' -----------------------------------------------------------------------------------------------

' RETORNA QUANTIDADE DE MAPEADA PARA FACTURACAO

' -----------------------------------------------------------------------------------------------

Public Function RetornaQtd(codAna As String, cod_efr As String) As String
    Dim sSql As String
    Dim rsAna As New ADODB.recordset
    On Error GoTo TrataErro
        sSql = "SELECT qtd FROM sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(codAna) & " AND cod_efr = " & cod_efr
        sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
        sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
        rsAna.CursorLocation = adUseServer
        rsAna.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAna.Open sSql, gConexao
        If rsAna.RecordCount > 0 Then
            RetornaQtd = BL_HandleNull(rsAna!qtd, "")
        Else
            rsAna.Close
            sSql = "SELECT qtd FROM sl_ana_facturacao where cod_ana = " & BL_TrataStringParaBD(codAna) & " AND cod_efr IS NULL "
            sSql = sSql & " AND cod_portaria IN (Select x2.cod_portaria FROM sl_portarias x2 WHERE " & BL_TrataDataParaBD(Bg_DaData_ADO) & " BETWEEN "
            sSql = sSql & " dt_ini and NVL(dt_fim, '31-12-2099')) AND cod_ana IN (SELECT cod_ana FROM slv_analises_factus)"
            rsAna.CursorLocation = adUseServer
            rsAna.CursorType = adOpenStatic
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsAna.Open sSql, gConexao
            If rsAna.RecordCount > 0 Then
                RetornaQtd = BL_HandleNull(rsAna!qtd, "")
            End If
        End If
        rsAna.Close
Exit Function
TrataErro:
    BG_LogFile_Erros Err.Number & " - " & Err.Description, Me.Name, "RetornaQtd"
    RetornaQtd = ""
    Exit Function
    Resume Next
End Function

' -------------------------------------------------------------------------

' AO ADICIONAR ANALISES, ADICIONA Questoes DA ANALISE

' -------------------------------------------------------------------------
Private Sub AdicionaQuestaoAna(cod_ana As String)
    Dim sSql As String
    Dim flg_existe As Boolean
    Dim rsQuest As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    sSql = "SELECT x1.cod_frase, x2.descr_frase FROM sl_ana_questoes x1, sl_dicionario x2 WHERE x1.cod_frase = x2.cod_Frase AND "
    sSql = sSql & " x1.cod_ana = " & BL_TrataStringParaBD(cod_ana)
    rsQuest.CursorLocation = adUseServer
    rsQuest.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsQuest.Open sSql, gConexao
    If rsQuest.RecordCount >= 1 Then
        While Not rsQuest.EOF
            flg_existe = False
            For i = 1 To totalQuestao
                If BL_HandleNull(rsQuest!cod_frase, "") = EstrutQuestao(i).cod_questao Then
                    flg_existe = True
                    Exit For
                End If
            Next
            If flg_existe = False Then
                totalQuestao = totalQuestao + 1
                ReDim Preserve EstrutQuestao(totalQuestao)
                EstrutQuestao(totalQuestao).cod_questao = BL_HandleNull(rsQuest!cod_frase, "")
                EstrutQuestao(totalQuestao).descr_questao = BL_HandleNull(rsQuest!descr_frase, "")

                FgQuestoes.TextMatrix(totalQuestao, 0) = BL_HandleNull(rsQuest!descr_frase, "")
                FgQuestoes.AddItem ""
            End If
            rsQuest.MoveNext
        Wend
    End If
    rsQuest.Close
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaQuestaoAna: " & Err.Description, Me.Name, "AdicionaQuestaoAna", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' AO ADICIONAR ANALISES, ADICIONA INFORMACAO DA ANALISE

' -------------------------------------------------------------------------
Private Sub AdicionaInformacaoAna(cod_ana As String)
    Dim sSql As String
    Dim flg_existe As Boolean
    Dim rsInfo As New ADODB.recordset
    Dim i As Integer
    On Error GoTo TrataErro
    sSql = "SELECT x1.cod_informacao, x2.descr_informacao, x2.informacao FROM sl_ana_informacao x1, sl_informacao x2 WHERE x1.cod_informacao = x2.cod_informacao AND "
    sSql = sSql & " x1.cod_ana = " & BL_TrataStringParaBD(cod_ana)
    rsInfo.CursorLocation = adUseServer
    rsInfo.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsInfo.Open sSql, gConexao
    If rsInfo.RecordCount >= 1 Then
        While Not rsInfo.EOF
            flg_existe = False
            For i = 1 To totalInfo
                If BL_HandleNull(rsInfo!cod_informacao, "") = EstrutInfo(i).cod_informacao Then
                    flg_existe = True
                    Exit For
                End If
            Next
            If flg_existe = False Then
                totalInfo = totalInfo + 1
                ReDim Preserve EstrutInfo(totalInfo)
                EstrutInfo(totalInfo).cod_informacao = BL_HandleNull(rsInfo!cod_informacao, "")
                EstrutInfo(totalInfo).descr_informacao = BL_HandleNull(rsInfo!descr_informacao, "")
                EstrutInfo(totalInfo).informacao = BL_HandleNull(rsInfo!informacao, "")
                FgInformacao.TextMatrix(totalInfo, 0) = BL_HandleNull(rsInfo!descr_informacao, "")
                
                EcInfo.TextRTF = BL_HandleNull(rsInfo!informacao, "")
                FgInformacao.TextMatrix(totalInfo, 1) = EcInfo.Text
                FgInformacao.AddItem ""
            End If
            rsInfo.MoveNext
        Wend
    End If
    rsInfo.Close
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  AdicionaInformacaoAna: " & Err.Description, Me.Name, "AdicionaInformacaoAna", False
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' INSERE ANALISES NA TABELA DE REPORT

' -------------------------------------------------------------------------
Private Sub InsereAna()
    Dim sSql As String
    Dim i As Integer
    Dim reg As Integer
    On Error GoTo TrataErro
    
    For i = 1 To RegistosRA.Count
        If RegistosRA(i).ReciboCodFacturavel <> "" Then
            sSql = "INSERT INTO sl_cr_simulacao_ana (nome_computador, num_sessao, cod_ana, descr_ana, qtd,preco, "
            sSql = sSql & " flg_utente, cod_efr, descr_efr, flg_65anos) Values("
            sSql = sSql & BL_TrataStringParaBD(gComputador) & ","
            sSql = sSql & (gNumeroSessao) & ","
            sSql = sSql & BL_TrataStringParaBD(RegistosRA(i).ReciboCodFacturavel) & ","
            sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SLV_ANALISES", "DESCR_ANA", "COD_ANA", RegistosRA(i).ReciboCodFacturavel, "V")) & ","
            sSql = sSql & (RegistosRA(i).ReciboQuantidade) & ","
            sSql = sSql & Replace(RegistosRA(i).ReciboTaxa, ",", ".") & ","
            If OptCalculo(0).value = True Then
                sSql = sSql & "1,"
            Else
                sSql = sSql & "0,"
            End If
            sSql = sSql & BL_HandleNull(EcCodEfr, -1) & ","
            sSql = sSql & BL_TrataStringParaBD(EcDescrEfr) & ","
            If Ck65.value = vbChecked Then
                sSql = sSql & "1)"
            Else
                sSql = sSql & "0)"
            End If
            reg = BG_ExecutaQuery_ADO(sSql)
            If reg <= 0 Then GoTo TrataErro
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  InsereAna: " & Err.Description & " " & sSql, Me.Name, "InsereAna", True
    Exit Sub
    Resume Next
End Sub


' -------------------------------------------------------------------------

' INSERE INFORMACAO NA TABELA DE REPORT

' -------------------------------------------------------------------------
Private Sub InsereInfo()
    Dim sSql As String
    Dim i As Integer
    Dim reg As Integer
    On Error GoTo TrataErro
    sSql = "DELETE FROM  sl_cr_simulacao_INFO WHERE nome_computador =" & BL_TrataStringParaBD(gComputador)
    sSql = sSql & " AND num_sessao = " & gNumeroSessao
    reg = BG_ExecutaQuery_ADO(sSql)
    
    For i = 1 To totalInfo
        sSql = "INSERT INTO sl_cr_simulacao_info (nome_computador, num_sessao, descr_informacao, informacao) Values("
        sSql = sSql & BL_TrataStringParaBD(gComputador) & ","
        sSql = sSql & (gNumeroSessao) & ","
        sSql = sSql & BL_TrataStringParaBD(EstrutInfo(i).descr_informacao) & ","
        sSql = sSql & BL_TrataStringParaBD(EstrutInfo(i).informacao) & ")"
        reg = BG_ExecutaQuery_ADO(sSql)
        If reg <= 0 Then GoTo TrataErro
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  InsereInfo: " & Err.Description & " " & sSql, Me.Name, "InsereInfo", True
    Exit Sub
    Resume Next
End Sub

' -------------------------------------------------------------------------

' INSERE QuestRMACAO NA TABELA DE REPORT

' -------------------------------------------------------------------------
Private Sub InsereQuest()
    Dim sSql As String
    Dim i As Integer
    Dim reg As Integer
    On Error GoTo TrataErro
    sSql = "DELETE FROM  sl_cr_simulacao_Quest WHERE nome_computador =" & BL_TrataStringParaBD(gComputador)
    sSql = sSql & " AND num_sessao = " & gNumeroSessao
    reg = BG_ExecutaQuery_ADO(sSql)
    
    For i = 1 To totalQuestao
        sSql = "INSERT INTO sl_cr_simulacao_Quest (nome_computador, num_sessao, questao) Values("
        sSql = sSql & BL_TrataStringParaBD(gComputador) & ","
        sSql = sSql & (gNumeroSessao) & ","
        sSql = sSql & BL_TrataStringParaBD(EstrutQuestao(i).descr_questao) & ")"
        reg = BG_ExecutaQuery_ADO(sSql)
        If reg <= 0 Then GoTo TrataErro
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  InsereQuest: " & Err.Description & " " & sSql, Me.Name, "InsereQuest", True
    Exit Sub
    Resume Next
End Sub

Private Sub ApagaTabelas()
    Dim sSql As String
    Dim reg As Integer
    sSql = "DELETE FROM  sl_cr_simulacao_ana WHERE nome_computador =" & BL_TrataStringParaBD(gComputador)
    sSql = sSql & " AND num_sessao = " & gNumeroSessao
    reg = BG_ExecutaQuery_ADO(sSql)
    
End Sub

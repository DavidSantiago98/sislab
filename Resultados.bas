Attribute VB_Name = "ModResultados"
Public Type VigilanciaEpid
    cod_regra_ve As String
    descr_regra_ve As String
    seq_vig_epid As String
    cod_estado_ve As Integer
    dt_cri As String
    hr_cri As String
    user_cri As String
    cod_micro As String
    seq_ve_email As String
    seq_ve_bin As String
    flg_invisivel  As Integer
    flg_manual As Integer
    seq_vig_epid_apagar As String
    flg_mostra_aviso As Integer
    'BRUNODSANTOS 23.02.2017 - Glintt-HS-14510
    tipo_notificacao As Integer
    GrEpid As Integer
    '
End Type

Private Type Alteracoes
    dt_cri As String
    user_cri As String
    nome_utilizador As String
    resultado As String
    N_Res As Integer
    flg_estado As String
    descr_estado As String
    user_val As String
    nome_val As String
    dt_val As String
    
    user_tec_val As String
    nome_tec_val As String
    dt_tec_val As String
    
    res_frase As String
    res_micro As String
    obs_ana As String
End Type

Private Type FlagApar
    seq_res_flag_apar As Long
    cod_flag_apar As String
    descr_flag_apar As String
    dt_cri As String
    hr_cri As String
    user_cri As String
    flg_invisivel As Integer
End Type

' ------------------------------------------------------
' FRASES ASSOCIADAS A PROVAS
' ------------------------------------------------------
Private Type FraseProvas
    cod_frase As String
    descr_frase As String
End Type
' ------------------------------------------------------
' PROVAS CODIFICADAS
' ------------------------------------------------------
Public Type provasMicro
    seq_prova As Integer
    Cod_Prova As String
    descr_prova As String
    linhaGrid As Integer
    frases() As FraseProvas
    totalFrases As Integer
End Type

Public Type ResAntib
    codAntib As String
    DescrAntib As String
    CMI As String
    sensibilidade As String
    Imprimir As String
    user_val As String
    dt_val As String
    hr_val As String
    flg_antib_fact As Integer
End Type


' ------------------------------------------------------
' ESTRUTURA USADA PARA RESULTADOS
' ------------------------------------------------------
Public Type resultados
    Tabela As String                                        ' TABELA ONDE ESTA A ANALISE
    accao As String                                         ' ACCAO CORRESPONDENTE AO RESULTADO (VALIDAR,REPETIR,ETC)
    Editado As Boolean                                      ' INDICA SE RESULTADO FOI EDITADO
    seq_utente As String                                    ' DADOS DO UTENTE
    idxU As Long                                            ' DADOS DO UTENTE
    n_req As Long                                           ' DADOS DA REQUISICAO
    idxR As Long                                            ' DADOS DA REQUISICAO
    idxS As Long
    idxC As Long
    idxP As Long
    seqRealiza As Long
    codAnaS As String
    DescrAnaS As String
    CodAnaC As String
    DescrAnaC As String
    CodPerfil As String
    DescrPerfil As String
    codAgrup As String
    estado As Integer
    UserCri As String
    DtCri As String
    HrCri As String
    UserAct As String
    DtAct As String
    HrAct As String
    userVal As String
    dtVal As String
    hrVal As String
    UserValTec As String
    DtValTec As String
    HrValTec As String
    obs As Boolean
    ObsAna As String
    ObsAnaRTF As String
    SeqObsAna As Long
    ObsAna_user_cri As String
    ObsAna_dt_cri As String
    ObsAna_hr_cri As String
    ObsAna_user_act As String
    ObsAna_dt_act As String
    ObsAna_hr_act As String
    Flg_Repetido As Boolean
    flg_assinado As Integer
    UserAssin As String
    DtAssin As String
    HrAssin As String
    
    ObsReq As Boolean
    
    ObsAnaReq As String
    seqObsAnaReq As Long
    
    ObsAuto As String
    SeqObsAuto As Long
    ObsAutoAccao As Integer
    ObsAutoActivo As Integer
    
    UsaFormulaVolume As Boolean
    AnaVolume As String
    GravarVolume As Boolean
    Flg_Facturado As String
    flg_aparTrans As String
    transmit_psm As Integer
    n_envio As Long
    N_Folha_Trab As String
    prefixo As String
    CorRes As Variant
    CorResSel As Variant
    check As String
    OrdMarca As String
    ordem As String
    OrdC As String
    OrdP As String
    DtChega As String
        
    res As String                                               ' DADOS PRIMEIRO RESULTADO
    Imprimir As String
    ValRef As String
    ZONA_ValRef As String
    ZONA_Texto As String
    cutoff As Integer
    comCutOff As String
    ResAnter1 As String
    ResAnter2 As String
    ResAnter3 As String
    DtAnter1 As String
    DtAnter2 As String
    DtAnter3 As String
    CorResAnter As Long
    CorResAnter2 As Long
    CorResAnter3 As Long
    Info As String
    
    Seg_Res As String                                           ' DADOS SEGUNDO RESULTADO
    SEG_Imprimir As String
    SEG_ResAnter As String
    SEG_CorRes As Variant
    SEG_CorResSel As Variant
    SEG_Check As String
    SEG_ValRef As String
    SEG_ZONA_ValRef As String
    SEG_ZONA_Texto As String
    SEG_cutOff As Integer
    SEG_comCutOff As String
    SEG_Info As String
    SEG_ResAnter1 As String
    SEG_ResAnter2 As String
    SEG_ResAnter3 As String
    SEG_DtAnter1 As String
    SEG_DtAnter2 As String
    SEG_DtAnter3 As String
    SEG_CorResAnter As Long
    SEG_CorResAnter2 As Long
    SEG_CorResAnter3 As Long

    
    ordem_frase As Integer                                      ' DADOS DO TIPO FRASE
    cod_frase As String
    descr_frase As String
    
    SEG_ordem_frase As Integer                                      ' DADOS DO TIPO FRASE
    SEG_cod_frase As String
    SEG_descr_frase As String

    micro_ordem As Integer                                      ' DADOS DO TIPO MICRO
    micro_codigo As String
    micro_descr As String
    micro_cod_prova As String
    micro_descr_prova As String
    micro_flg_teste As String
    micro_quantif As String
    micro_flg_tsa As String
    micro_flg_imp As String
    micro_gr_antib As String
    micro_obs As String
    micro_cod_carac As String
    micro_descr_carac As String
    micro_codigo_apagar As String
    micro_flg_gr_antib_facturar As Integer
    provas() As provasMicro
    totalProvas As Integer
    antibioticos() As ResAntib
    totalAntib As Long
    
    DC_DeltaCheck As Boolean                                       ' AN�LISE TEM DELTACHECK OU N�O
    DC_ToolTip As String                                           ' TOOLTIP COM RESULTADO DA F�RMULA E AC��O A TOMAR
    DC_descrFormula As String                                      ' DESCRI��O DA AVALIA��O DA F�RMULA
    DC_resFormula As String                                        ' RESULTADO DA AVALIA��O DA F�RMULA
    DC_Accao As String                                             ' CONTEM CODIGO DA AC��O A EXECUTAR CASO VALIDE POR DC
    DC_Activo As Boolean                                           ' POR DEFEITO APARECE ACTIVO. UTILIZADOR PODE DESACTIVAR E VALIDAR ANALISE
    
    
    IF_Interferencia As Boolean                                    ' AN�LISE TEM INTERFERENCIA OU N�O
    IF_ToolTip As String                                           ' TOOLTIP COM RESULTADO DA F�RMULA E AC��O A TOMAR
    IF_descrFormula As String                                      ' DESCRI��O DA AVALIA��O DA F�RMULA
    IF_resFormula As String                                        ' RESULTADO DA AVALIA��O DA F�RMULA
    IF_Accao As String                                             ' CONTEM CODIGO DA AC��O A EXECUTAR CASO VALIDE POR DC
    IF_Activo As Boolean                                           ' POR DEFEITO APARECE ACTIVO. UTILIZADOR PODE DESACTIVAR E VALIDAR ANALISE
    IF_Indice As Integer
    
    FlgApar As Boolean
    descrFlgApar As String
    estrutFlagApar() As FlagApar
    totalFlagApar As Integer
    
    Flg_GravaHistAna As Boolean
    ResAnterActivo As Integer
    FlgAdicionada As Boolean
    totalAlteracoes As Integer
    estrutAlteracoes() As Alteracoes
    ' pferreira 2009.10.08 - Vari�veis que guardam observa��es dos resultados anteriores.
    seq_obs_resanter1 As Long
    seq_obs_resanter2 As Long
    seq_obs_resanter3 As Long
    OBS_ResAnter1 As String
    OBS_ResAnter2 As String
    OBS_ResAnter3 As String
    Flg_UltrapassaLimites As Boolean
    motivoValAuto As String ' indica o motivo se valida��o automatica n�o validar analise
    flg_anexo As Integer
    flg_obs_ana As Integer
    cod_local As String
    seq_req_tubo As Long
    flg_grafico As Integer
    
    flg_ve As Integer
    vigEpid() As VigilanciaEpid
    totalVE As Integer
    
    user_apar As String
    dt_apar As String
    'NELSONPSILVA CHVNG-7461 29.10.2018
    flg_reutil As Integer
    '
End Type

VERSION 5.00
Begin VB.Form FormParamAmbiente 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CaptionParamAmbiente"
   ClientHeight    =   6270
   ClientLeft      =   2340
   ClientTop       =   2310
   ClientWidth     =   9255
   Icon            =   "FormParamAmbiente.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6270
   ScaleWidth      =   9255
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtRefresh 
      Height          =   495
      Left            =   8400
      Picture         =   "FormParamAmbiente.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   36
      ToolTipText     =   "Renova valores das chaves"
      Top             =   4080
      Width           =   615
   End
   Begin VB.ComboBox EcNomeComputador 
      Height          =   315
      Left            =   3360
      Style           =   2  'Dropdown List
      TabIndex        =   35
      Top             =   1080
      Width           =   2295
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5520
      TabIndex        =   30
      Top             =   5400
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3600
      TabIndex        =   29
      Top             =   5400
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5520
      TabIndex        =   28
      Top             =   5760
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3600
      TabIndex        =   27
      Top             =   5760
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Frame Frame2 
      Height          =   795
      Left            =   3360
      TabIndex        =   20
      Top             =   4560
      Width           =   5685
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   26
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   25
         Top             =   200
         Width           =   1095
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   24
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   23
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label7 
         Caption         =   "Altera��o"
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label4 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.ComboBox EcConteudo_Combo 
      Height          =   315
      Left            =   6240
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   1800
      Visible         =   0   'False
      Width           =   2775
   End
   Begin VB.ComboBox EcUtilizador 
      Height          =   315
      Left            =   5760
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   1080
      Width           =   3255
   End
   Begin VB.TextBox EcObs 
      Height          =   765
      Left            =   3360
      MultiLine       =   -1  'True
      TabIndex        =   9
      Top             =   3300
      Width           =   5655
   End
   Begin VB.TextBox EcConteudoFixo 
      Height          =   285
      Left            =   3360
      TabIndex        =   8
      Top             =   2520
      Width           =   5655
   End
   Begin VB.TextBox EcConteudo 
      Height          =   285
      Left            =   6240
      TabIndex        =   6
      Top             =   1800
      Width           =   2775
   End
   Begin VB.TextBox EcChave 
      Height          =   285
      Left            =   3360
      TabIndex        =   5
      Top             =   1800
      Width           =   2775
   End
   Begin VB.ComboBox EcAmbito 
      Height          =   315
      Left            =   7080
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   480
      Width           =   1935
   End
   Begin VB.ComboBox EcAcesso 
      Height          =   315
      Left            =   4440
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   480
      Width           =   2535
   End
   Begin VB.TextBox EcCodigo 
      Height          =   285
      Left            =   3360
      TabIndex        =   1
      Top             =   480
      Width           =   975
   End
   Begin VB.ListBox EcLista 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4890
      Left            =   240
      TabIndex        =   10
      Top             =   480
      Width           =   2895
   End
   Begin VB.Label Label12 
      Caption         =   "EcUtilizadorCriacao"
      Height          =   255
      Left            =   1920
      TabIndex        =   34
      Top             =   5400
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label9 
      Caption         =   "EcUtilizadorAlteracao"
      Height          =   255
      Left            =   1920
      TabIndex        =   33
      Top             =   5760
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label10 
      Caption         =   "EcDataCriacao"
      Height          =   255
      Left            =   4200
      TabIndex        =   32
      Top             =   5400
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "EcDataAlteracao"
      Height          =   255
      Left            =   4200
      TabIndex        =   31
      Top             =   5760
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label LaObs 
      Caption         =   "Observa��o"
      Height          =   255
      Left            =   3360
      TabIndex        =   19
      Top             =   3075
      Width           =   3255
   End
   Begin VB.Label LaConteudoFixo 
      Caption         =   "Conte�do fixo (o separador de op��es � o ;)"
      Height          =   255
      Left            =   3360
      TabIndex        =   18
      Top             =   2280
      Width           =   5655
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      Visible         =   0   'False
      X1              =   3000
      X2              =   3000
      Y1              =   240
      Y2              =   5280
   End
   Begin VB.Label Label8 
      Caption         =   "Chaves"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "Conte�do"
      Height          =   255
      Left            =   6240
      TabIndex        =   17
      Top             =   1560
      Width           =   855
   End
   Begin VB.Label Label5 
      Caption         =   "Chave"
      Height          =   255
      Left            =   3360
      TabIndex        =   16
      Top             =   1560
      Width           =   615
   End
   Begin VB.Label LaUtilizador 
      Caption         =   "Utilizador"
      Height          =   255
      Left            =   5760
      TabIndex        =   15
      Top             =   840
      Width           =   975
   End
   Begin VB.Label LaNomeComputador 
      Caption         =   "Computador"
      Height          =   255
      Left            =   3360
      TabIndex        =   14
      Top             =   840
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "�mbito"
      Height          =   255
      Left            =   7080
      TabIndex        =   13
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "Acesso"
      Height          =   255
      Left            =   4440
      TabIndex        =   12
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   3360
      TabIndex        =   11
      Top             =   240
      Width           =   615
   End
End
Attribute VB_Name = "FormParamAmbiente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 08/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos

Public rs As ADODB.recordset




Private Sub BtRefresh_Click()
    CarregaParamAmbiente
End Sub

Private Sub EcAcesso_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcAcesso.ListIndex = -1
End Sub


Sub EcAmbito_Click()
Dim i As Integer

    If BG_DaComboSel(EcAmbito) = gT_Geral Then
        EcNomeComputador.ListIndex = mediComboValorNull
        LaNomeComputador.Enabled = False
        EcNomeComputador.Enabled = False
        
        EcUtilizador.ListIndex = mediComboValorNull
        LaUtilizador.Enabled = False
        EcUtilizador.Enabled = False
    End If
    
    If BG_DaComboSel(EcAmbito) = gT_Computador Then
        If gOpParamAmbiente = 2 Then
            i = 0
            While i < EcNomeComputador.ListCount Or Trim(EcNomeComputador.List(i)) = Trim(BG_SYS_GetComputerName)
                If Trim(EcNomeComputador.List(i)) = Trim(BG_SYS_GetComputerName) Then
                    EcNomeComputador.ListIndex = i
                End If
                i = i + 1
            Wend
        End If
        
        LaNomeComputador.Enabled = True
        EcNomeComputador.Enabled = True
        
        EcUtilizador.ListIndex = mediComboValorNull
        LaUtilizador.Enabled = False
        EcUtilizador.Enabled = False
    End If
    
    If BG_DaComboSel(EcAmbito) = gT_Utilizador Then
        EcNomeComputador.ListIndex = mediComboValorNull
        EcNomeComputador.Enabled = False
        LaNomeComputador.Enabled = False
        
        If gOpParamAmbiente = 2 Then
            EcUtilizador = gIdUtilizador
        End If
        LaUtilizador.Enabled = True
        EcUtilizador.Enabled = True
    End If
End Sub

Private Sub EcAmbito_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcAmbito.ListIndex = -1
End Sub


Private Sub EcChave_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcChave_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Private Sub EcCodigo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcCodigo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Sub EcConteudo_Combo_Click()
    EcConteudo = EcConteudo_Combo
End Sub

Private Sub EcConteudo_Combo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcConteudo_Combo.ListIndex = -1
End Sub


Private Sub EcConteudo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcConteudo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Private Sub EcConteudoFixo_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcConteudoFixo_LostFocus()
    BG_ValidaTipoCampo_ADO Me, CampoActivo
End Sub

Private Sub EcLista_Click()
    On Error Resume Next
    If EcLista.ListCount > 0 Then
        rs.Move EcLista.ListIndex, MarcaInicial
        LimpaCampos
        PreencheCampos
    End If
End Sub

Private Sub EcNomeComputador_GotFocus()
    Set CampoActivo = Me.ActiveControl
End Sub

Private Sub EcNomeComputador_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcNomeComputador.ListIndex = -1
End Sub


Private Sub EcUtilizador_Click()
    
    Dim SQLQuery As String
    Dim rsResult As ADODB.recordset
    
    Set rsResult = New ADODB.recordset
    
    If EcUtilizador.ListIndex = mediComboValorNull Then
        EcUtilizador.ToolTipText = ""
    Else
        rsResult.CursorType = adOpenStatic
        rsResult.CursorLocation = adUseServer
        rsResult.Open "SELECT nome FROM sl_idutilizador WHERE cod_utilizador = " & EcUtilizador.ItemData(EcUtilizador.ListIndex), gConexao
        If rsResult.RecordCount = 0 Then
            ' Nada
        Else
            EcUtilizador.ToolTipText = rsResult("nome")
        End If
        rsResult.Close
        Set rsResult = Nothing
    End If
End Sub

Private Sub EcUtilizador_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 And Shift = 0 Then EcUtilizador.ListIndex = -1
End Sub


Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()

    Me.caption = " Parametriza��es do Ambiente"
    If gOpParamAmbiente = 1 Then
        Me.caption = Me.caption & " (Geral)"
    Else
        Me.caption = Me.caption & " (Local)"
    End If
    Me.left = 540
    Me.top = 450
    Me.Width = 9350
    Me.Height = 5900
    
    NomeTabela = cParamAmbiente
    Set CampoDeFocus = EcCodigo
    
    NumCampos = 13
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    CamposBD(0) = "codigo"
    CamposBD(1) = "acesso"
    CamposBD(2) = "ambito"
    CamposBD(3) = "cod_computador"
    CamposBD(4) = "cod_utilizador"
    CamposBD(5) = "conteudo"
    CamposBD(6) = "chave"
    CamposBD(7) = "conteudo_fixo"
    CamposBD(8) = "obs"
    CamposBD(9) = "user_cri"
    CamposBD(10) = "dt_cri"
    CamposBD(11) = "user_act"
    CamposBD(12) = "dt_act"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcCodigo
    Set CamposEc(1) = EcAcesso
    Set CamposEc(2) = EcAmbito
    Set CamposEc(3) = EcNomeComputador
    Set CamposEc(4) = EcUtilizador
    Set CamposEc(5) = EcConteudo
    Set CamposEc(6) = EcChave
    Set CamposEc(7) = EcConteudoFixo
    Set CamposEc(8) = EcObs
    Set CamposEc(9) = EcUtilizadorCriacao
    Set CamposEc(10) = EcDataCriacao
    Set CamposEc(11) = EcUtilizadorAlteracao
    Set CamposEc(12) = EcDataAlteracao
    
    ' Texto para a mensagem nos campos obrigat�rios
    ' Os �ndices preenchidos indicam tamb�m que o campo � obrigat�rio
    TextoCamposObrigatorios(1) = "Acesso"
    TextoCamposObrigatorios(2) = "�mbito"
    TextoCamposObrigatorios(6) = "Chave"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "codigo"
    Set ChaveEc = EcCodigo
    
    CamposBDparaListBox = Array("chave")
    NumEspacos = Array(40)
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    If gOpParamAmbiente <> 1 And gOpParamAmbiente <> 2 Then
        BG_LogFile_Erros "Abertura de Parametriza��es inv�lida! Par�metro errado para 'gOpParamAmbiente'."
        Unload Me
    End If
End Sub

Sub EventoUnload()
    gOpParamAmbiente = 0

    'gPassaParams.Param(0) = EcCodigo
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormParamAmbiente = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus
    
    BG_LimpaCampo_Todos CamposEc
    
    EcConteudo_Combo.ListIndex = mediComboValorNull
    EcNomeComputador.ListIndex = mediComboValorNull
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
End Sub

Sub PreencheValoresDefeito()
    
    Dim ValoresComboAcesso As Variant
    Dim ValoresComboAmbito As Variant
    Dim QueryUtilizador As String
    

    
    If gOpParamAmbiente = 1 Then
        QueryUtilizador = "SELECT cod_utilizador, utilizador FROM sl_idutilizador WHERE flg_removido <> 1"
    
    ElseIf gOpParamAmbiente = 2 Then
        LaConteudoFixo.Visible = False
        EcConteudoFixo.Visible = False
        EcConteudoFixo.Enabled = False
        
        LaObs.BackColor = Me.BackColor
        EcObs.BackColor = Me.BackColor
        EcObs.Enabled = True
    
        ValoresComboAcesso = Array(gT_ApenasVisualizar, "Apenas visualiza", gT_VisualizarAlterar, "Visualiza e permite altera��es")
        ValoresComboAmbito = Array(gT_Computador, "Computador", gT_Utilizador, "Utilizador")
        QueryUtilizador = "SELECT cod_utilizador, utilizador FROM sl_idutilizador WHERE flg_removido <> 1 AND cod_utilizador = " & gCodUtilizador
    End If
    

    BG_PreencheComboBD_ADO "sl_tbf_t_acesso", "cod_t_acesso", "descr_t_acesso", EcAcesso
    BG_PreencheComboBD_ADO "sl_tbf_t_ambito", "cod_t_ambito", "descr_t_ambito", EcAmbito
    BG_PreencheComboBD_ADO QueryUtilizador, "cod_utilizador", "utilizador", EcUtilizador, mediAscComboDesignacao
    BG_PreencheComboBD_ADO "sl_computador", "cod_computador", "descr_computador", EcNomeComputador
    

End Sub

Sub PreencheCampos()
    
    Dim i As Integer
    
    On Error Resume Next

    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        FuncaoLimpar
    Else
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao
        LaDataAlteracao = EcDataAlteracao
        
        If EcConteudoFixo <> "" Then
            EcConteudo_Combo.left = EcConteudo.left
            EcConteudo_Combo.top = EcConteudo.top
            EcConteudo.Visible = False
            EcConteudo_Combo.Visible = True
            
            PreencheCampos_ComboConteudo
            EcConteudo_Combo = EcConteudo
            
            i = 0
            While i < EcConteudo_Combo.ListCount And EcConteudo_Combo.List(i) <> Trim(EcConteudo)
                If EcConteudo_Combo.List(i) = Trim(EcConteudo) Then
                    EcConteudo_Combo.ListIndex = i
                End If
                i = i + 1
            Wend
        Else
            EcConteudo.Visible = True
            EcConteudo_Combo.Visible = False
        End If
        
        If BG_DaComboSel(EcAcesso) = gT_ApenasVisualizar Then
            BL_Toolbar_BotaoEstado "Modificar", "InActivo"
            BL_Toolbar_BotaoEstado "Remover", "InActivo"
        Else
            BL_Toolbar_BotaoEstado "Modificar", "Activo"
            BL_Toolbar_BotaoEstado "Remover", "Activo"
        End If
    End If
End Sub

Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        LimpaCampos
        EcLista.Clear
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
    
    Dim SelTotal As Boolean
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
    
    If gOpParamAmbiente = gT_Utilizador Then
        If EcAmbito.ListIndex = mediComboValorNull Then
            EcAmbito.ListIndex = gT_Utilizador
        End If
    End If

    CriterioTabela = BG_ConstroiCriterio_SELECT_ADO(NomeTabela, CamposBD, CamposEc, SelTotal)
    If SelTotal = True Then
        CriterioTabela = CriterioTabela & " WHERE acesso <> " & gT_NaoVisualizar
        If gOpParamAmbiente = 2 Then
            CriterioTabela = CriterioTabela & " AND ambito <> " & gT_Geral
        End If
    Else
        CriterioTabela = CriterioTabela & " AND acesso <> " & gT_NaoVisualizar
        If gOpParamAmbiente = 2 Then
            CriterioTabela = CriterioTabela & " AND ambito <> " & gT_Geral
        End If
    End If
    CriterioTabela = CriterioTabela & " ORDER BY codigo ASC"
    
'    BG_LogFile_Erros CriterioTabela

    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open CriterioTabela, gConexao
        
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = rs.Bookmark
        BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "SELECT"
        ' Fim do preenchimento de 'EcLista'

        BL_ToolbarEstadoN estado ' Tem que estar antes de PreencheCampos, porque nesse procedimento s�o tiradas permiss�es aos bot�es.
        PreencheCampos
        BL_FimProcessamento Me
    End If

    Exit Sub

End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex - 1
    End If
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
        EcLista.ListIndex = EcLista.ListIndex + 1
    End If
End Sub

Sub FuncaoInserir()
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = vbYes
    Me.SetFocus
    
    If BG_DaComboSel(EcAmbito) = gT_Computador And EcNomeComputador.ListIndex = mediComboValorNull Then
        BG_Mensagem mediMsgBox, "Nome do Computador obrigat�rio." & vbCrLf & vbCrLf & "Opera��o n�o efectuada!", vbExclamation, gMsgTitulo
        Exit Sub
    End If
    
    If BG_DaComboSel(EcAmbito) = gT_Utilizador And EcUtilizador = "" Then
        BG_Mensagem mediMsgBox, "Nome do Utilizador obrigat�rio." & vbCrLf & vbCrLf & "Opera��o n�o efectuada!", vbExclamation, gMsgTitulo
        Exit Sub
    End If
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = Bg_DaData_ADO
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Insert
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    Dim SQLQuery As String
    Set rs = New ADODB.recordset
   
    EcCodigo = BG_DaMAX(NomeTabela, "codigo") + 1

    SQLQuery = "SELECT " & ChaveBD & " FROM " & NomeTabela & " WHERE " & ChaveBD & " = " & ChaveEc
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    rs.Open SQLQuery, gConexao
    If rs.RecordCount <> 0 Then
        BG_Mensagem mediMsgBox, "J� existe esse C�digo!", vbExclamation, "Inserir"
    Else
        SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
        BG_ExecutaQuery_ADO SQLQuery
        
        BG_Mensagem mediMsgBox, "Opera��o efectuada com sucesso.", vbInformation, "Inserir"
    End If
End Sub

Sub FuncaoModificar()
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If BG_DaComboSel(EcAmbito) = gT_Computador And EcNomeComputador = "" Then
        BG_Mensagem mediMsgBox, "Nome do Computador obrigat�rio." & vbCrLf & vbCrLf & "Opera��o n�o efectuada!", vbExclamation, gMsgTitulo
        Exit Sub
    End If
    
    If BG_DaComboSel(EcAmbito) = gT_Utilizador And EcUtilizador = "" Then
        BG_Mensagem mediMsgBox, "Nome do Utilizador obrigat�rio." & vbCrLf & vbCrLf & "Opera��o n�o efectuada!", vbExclamation, gMsgTitulo
        Exit Sub
    End If
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = Bg_DaData_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Update
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
                
    ChaveEc = rs(ChaveBD)
    condicao = ChaveBD & " = " & rs(ChaveBD)
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)

    BG_ExecutaQuery_ADO SQLQuery
    BG_Mensagem mediMsgBox, "Opera��o efectuada com sucesso.", vbInformation, "Modificar"
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
        
    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If
    
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "UPDATE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
    If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    
    LimpaCampos
    PreencheCampos
    
End Sub

Sub FuncaoRemover()
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer remover este registo ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A eliminar registo."
        BD_Delete
        BL_FimProcessamento Me
    End If
    
End Sub

Sub BD_Delete()
    
    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
        
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    BG_Mensagem mediMsgBox, "Opera��o efectuada com sucesso.", vbInformation, "Remover"
    
    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery
    
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo apagado era �nico nesta pesquisa!", vbExclamation, "Remover"
        FuncaoEstadoAnterior
        Exit Sub
    End If
        
    ' Inicio do preenchimento de 'EcLista'
    BG_PreencheListBoxMultipla_ADO EcLista, rs, CamposBDparaListBox, NumEspacos, CamposBD, CamposEc, "DELETE"
    ' Fim do preenchimento de 'EcLista'
    
    If MarcaLocal <= EcLista.ListCount Then
        If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
        If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal
    Else
        rs.MoveLast
    End If
    
    If rs.EOF Then rs.MovePrevious
    
    LimpaCampos
    PreencheCampos
End Sub

Sub PreencheCampos_ComboConteudo()
    Dim i As Integer
    Dim sStr As String
    Dim sSeparador As String
    
    EcConteudo_Combo.Clear
    
    sSeparador = ";"
    sStr = EcConteudoFixo
    i = InStr(sStr, sSeparador)
    If i = 0 Then
        EcConteudo_Combo.AddItem Trim(sStr)
    Else
        sStr = EcConteudoFixo
        While i <> 0
            EcConteudo_Combo.AddItem Trim(left(sStr, i - 1))
            sStr = Right(sStr, Len(sStr) - i)
            
            i = InStr(sStr, sSeparador)
        Wend
        EcConteudo_Combo.AddItem Trim(sStr)
    End If
End Sub


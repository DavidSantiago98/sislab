VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form FormVigEpidEmail 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormVigEpidEmail"
   ClientHeight    =   6705
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   13275
   Icon            =   "FormVigEpidEmail.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6705
   ScaleWidth      =   13275
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcDescrImagem 
      Height          =   285
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   18
      Top             =   8160
      Width           =   2055
   End
   Begin VB.TextBox EcHTML 
      Height          =   285
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   17
      Top             =   7800
      Width           =   2055
   End
   Begin VB.Frame Frame3 
      Height          =   4695
      Left            =   10200
      TabIndex        =   14
      Top             =   1560
      Width           =   3015
      Begin VB.ListBox EcListaAnexos 
         Appearance      =   0  'Flat
         Height          =   3540
         Left            =   120
         TabIndex        =   15
         Top             =   960
         Width           =   2775
      End
      Begin VB.Label Label2 
         Caption         =   "Anexos:"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   16
         Top             =   720
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Height          =   4695
      Left            =   0
      TabIndex        =   6
      Top             =   1560
      Width           =   10095
      Begin VB.Frame Frame1 
         Height          =   735
         Left            =   120
         TabIndex        =   10
         Top             =   120
         Width           =   6495
         Begin VB.ComboBox ComboBoxFontType 
            Height          =   315
            ItemData        =   "FormVigEpidEmail.frx":000C
            Left            =   120
            List            =   "FormVigEpidEmail.frx":000E
            Sorted          =   -1  'True
            TabIndex        =   12
            Top             =   240
            Width           =   2415
         End
         Begin VB.ComboBox ComboBoxFontSize 
            Height          =   315
            Left            =   2520
            TabIndex        =   11
            Top             =   240
            Width           =   975
         End
         Begin MSComctlLib.Toolbar ToolbarFormatacao 
            Height          =   390
            Left            =   3600
            TabIndex        =   13
            Top             =   240
            Width           =   2775
            _ExtentX        =   4895
            _ExtentY        =   688
            ButtonWidth     =   609
            ButtonHeight    =   582
            ImageList       =   "ImageListFormatacao"
            DisabledImageList=   "ImageListFormatacao"
            HotImageList    =   "ImageListFormatacao"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   10
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "bold"
                  ImageIndex      =   1
                  Style           =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "italic"
                  ImageIndex      =   2
                  Style           =   1
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "underline"
                  ImageIndex      =   3
                  Style           =   1
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "left"
                  ImageIndex      =   4
                  Style           =   2
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "center"
                  ImageIndex      =   5
                  Style           =   2
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "right"
                  ImageIndex      =   6
                  Style           =   2
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "corrector"
                  Object.ToolTipText     =   "Verificar ortografia"
                  ImageIndex      =   7
               EndProperty
            EndProperty
         End
      End
      Begin MSComctlLib.ImageList ImageListFormatacao 
         Left            =   8520
         Top             =   240
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   7
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FormVigEpidEmail.frx":0010
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FormVigEpidEmail.frx":016A
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FormVigEpidEmail.frx":02C4
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FormVigEpidEmail.frx":041E
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FormVigEpidEmail.frx":0578
               Key             =   ""
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FormVigEpidEmail.frx":06D2
               Key             =   ""
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FormVigEpidEmail.frx":082C
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin RichTextLib.RichTextBox EcMensagem 
         Height          =   3615
         Left            =   120
         TabIndex        =   7
         Top             =   960
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   6376
         _Version        =   393217
         BackColor       =   16777215
         Enabled         =   -1  'True
         Appearance      =   0
         TextRTF         =   $"FormVigEpidEmail.frx":10BE
      End
   End
   Begin VB.Frame Frame4 
      Height          =   1455
      Left            =   960
      TabIndex        =   1
      Top             =   0
      Width           =   12255
      Begin VB.TextBox EcPara 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   4
         Top             =   240
         Width           =   10935
      End
      Begin VB.TextBox EcAssunto 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   3
         Top             =   960
         Width           =   10935
      End
      Begin VB.TextBox EcCC 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   600
         Width           =   10935
      End
      Begin VB.Label Label2 
         Caption         =   "Cc:"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Para:"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "A&ssunto:"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   960
         Width           =   855
      End
   End
   Begin VB.CommandButton BtEnviar 
      Caption         =   "&Enviar"
      Height          =   1335
      Left            =   0
      Picture         =   "FormVigEpidEmail.frx":1140
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   120
      Width           =   855
   End
End
Attribute VB_Name = "FormVigEpidEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit


Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.
Dim iGrEpid As Integer

Private Sub BtEnviar_Click()
    Dim sSql As String
    Dim rsBin As New ADODB.recordset
    Dim anexos() As String
    Dim seq_ve_email As String
    Dim i As Integer
    Dim resEmail As Boolean
    gMsgTitulo = "Envio de email"
    gMsgMsg = "Tem a certeza que quer enviar os dados?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
    
    
        ReDim anexos(EcListaAnexos.ListCount)
        For i = 0 To EcListaAnexos.ListCount - 1
            sSql = "SELECT nome_ficheiro FROM sl_vig_epid_bin WHERE seq_ve_bin = " & BL_TrataStringParaBD(EcListaAnexos.List(i))
            rsBin.CursorType = adOpenStatic
            rsBin.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sSql
            rsBin.Open sSql, gConexao
            If rsBin.RecordCount = 1 Then
                anexos(i) = BL_HandleNull(rsBin!nome_ficheiro, "")
                VE_RetiraExcelBD EcListaAnexos.List(i), False
               
            End If
            rsBin.Close
        Next
        If EcPara.Text <> "" Then
            If EcHTML.Text <> "" Then
                resEmail = BL_EnviaEmail(EcPara.Text, EcAssunto.Text, EcHTML.Text, True, gDirCliente, anexos, 1, gDirCliente & "\" & EcDescrImagem.Text, EcDescrImagem.Text, mediSim, mediSim, "VIGILANCIA", EcCC.Text)
            Else
                resEmail = BL_EnviaEmail(EcPara.Text, EcAssunto.Text, EcMensagem.Text, True, gDirCliente, anexos, 1, "", "", mediSim, mediSim, "VIGILANCIA", EcCC.Text)
            End If
            If resEmail = True Then
                seq_ve_email = VE_InsereRegistoEmail(EcPara.Text, EcCC.Text, EcAssunto.Text, EcMensagem.Text, anexos)
                If seq_ve_email <> "" Then
                    FormVigilanciaEpidimeologica.AtualizaSeqVeEmail iGrEpid, seq_ve_email
                End If
            End If
        End If
        For i = 0 To EcListaAnexos.ListCount - 1
            Kill gDirCliente & "\" & anexos(i)
        Next i
    End If
    Unload Me
End Sub

' ComboBoxFontType_Change.
Sub ComboBoxFontType_Change()

    On Error GoTo ErrorHandler
    EcMensagem.SelFontName = ComboBoxFontType.Text
    Exit Sub
    
ErrorHandler:
    Exit Sub
   
End Sub

' ComboBoxFontType_Click.
Sub ComboBoxFontType_Click()

    On Error GoTo ErrorHandler
    EcMensagem.SelFontName = ComboBoxFontType.Text
    Exit Sub
    
ErrorHandler:
    Exit Sub
   
End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Vigil�ncia Epidemiol�gica - Envio de Email"
    Me.left = 50
    Me.top = 50
    Me.Width = 13365
    Me.Height = 6915 ' Normal
    
    

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set gFormActivo = FormVigilanciaEpidimeologica
    FormVigilanciaEpidimeologica.Enabled = True
    Set FormVigEpidEmail = Nothing

End Sub



Sub DefTipoCampos()
    

    
End Sub

Sub PreencheValoresDefeito()
    Dim iItem As Integer
    
    For iItem = 0 To Screen.FontCount - 1: ComboBoxFontType.AddItem Screen.Fonts(iItem): Next
    For iItem = 8 To 12: ComboBoxFontSize.AddItem iItem: Next
    For iItem = 14 To 28 Step 2: ComboBoxFontSize.AddItem iItem: Next
    ComboBoxFontSize.AddItem 36
    ComboBoxFontSize.AddItem 48
    ComboBoxFontSize.AddItem 72
    TestFontValues

End Sub

' Test message font values.
Private Sub TestFontValues()

    Dim sParameter As String
    Dim iItem As Integer
    Dim iIndex As Integer
    
    On Error GoTo ErrorHandler
    iIndex = cr_mediComboValorNull
    sParameter = BG_DaParamAmbiente_NEW(cr_mediAmbitoGeral, "RelatFonte")
    For iItem = 0 To ComboBoxFontType.ListCount
        If (ComboBoxFontType.ItemData(iItem) = sParameter) Then: iIndex = iItem: Exit For
        If (ComboBoxFontType.ItemData(iItem) = "Tahoma") Then: iIndex = iItem
    Next
    ComboBoxFontType.ListIndex = iIndex
    iIndex = cr_mediComboValorNull
    sParameter = BG_DaParamAmbiente_NEW(cr_mediAmbitoGeral, "RelatTamanho")
    For iItem = 0 To ComboBoxFontSize.ListCount
        If (ComboBoxFontSize.ItemData(iItem) = sParameter) Then: iIndex = iItem: Exit For
        If (ComboBoxFontSize.ItemData(iItem) = "12") Then: iIndex = iItem
    Next
    ComboBoxFontSize.ListIndex = iIndex
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Sub FuncaoLimpar()
    


End Sub

Sub FuncaoEstadoAnterior()
    
End Sub

Function ValidaCamposEc() As Integer


End Function

Sub FuncaoProcurar()

    
End Sub

Sub FuncaoAnterior()
    


End Sub

Sub FuncaoSeguinte()
    


End Sub

Sub FuncaoInserir()

End Sub

Sub FuncaoModificar()

End Sub
Sub FuncaoRemover()
    
End Sub










' ToolbarFormatacao_ButtonClick.
Private Sub ToolbarFormatacao_ButtonClick(ByVal Button As MSComctlLib.Button)

    On Error GoTo ErrorHandler
    Select Case (UCase(Button.Key))
        Case "BOLD":
            If (Button.value = tbrUnpressed) Then
                If (Not IsNull(EcMensagem.SelBold)) Then: EcMensagem.SelBold = False
            Else
                EcMensagem.SelBold = True
            End If
       Case "ITALIC":
            If (Button.value = tbrUnpressed) Then
                If (Not IsNull(EcMensagem.SelItalic)) Then EcMensagem.SelItalic = False
            Else
                EcMensagem.SelItalic = True
            End If
       Case "UNDERLINE":
            If (Button.value = tbrUnpressed) Then
                If (Not IsNull(EcMensagem.SelUnderline)) Then EcMensagem.SelUnderline = False
            Else
                EcMensagem.SelUnderline = True
            End If
       Case "LEFT":
            If (Button.value <> tbrUnpressed) Then: EcMensagem.SelAlignment = rtfLeft
       Case "CENTER":
            If (Button.value <> tbrUnpressed) Then: EcMensagem.SelAlignment = rtfCenter
       Case "RIGHT":
            If (Button.value <> tbrUnpressed) Then: EcMensagem.SelAlignment = rtfRight
       Case "CORRECTOR":
            If (Me.ActiveControl.Name <> "TextBoxSubject" And Me.ActiveControl.Name <> "EcMensagem") Then: Exit Sub
            Dim Corrector As Corrector
            Set Corrector = New Corrector
            Corrector.InicializaCorrector gDirServidor & "\bin" & "\Corrector.cor"
            If (Corrector.LocalizacaoCorrector <> Empty) Then: Corrector.VerificaTextoObjecto Me.ActiveControl
            Set Corrector = Nothing
            DoEvents
            Me.SetFocus
    End Select
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub



' EcMensagem_SelChange.
Public Sub EcMensagem_SelChange()

    On Error GoTo ErrorHandler
    If (IsNull(EcMensagem.SelFontSize)) Then
        ComboBoxFontSize.Text = Empty
    Else
        ComboBoxFontSize.Text = EcMensagem.SelFontSize
    End If
    If (EcMensagem.SelBold) Then
        ToolbarFormatacao.Buttons.item("bold").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("bold").value = tbrUnpressed
    End If
    If (EcMensagem.SelItalic) Then
        ToolbarFormatacao.Buttons.item("italic").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("italic").value = tbrUnpressed
    End If
    If (EcMensagem.SelUnderline) Then
        ToolbarFormatacao.Buttons.item("underline").value = tbrPressed
    Else
        ToolbarFormatacao.Buttons.item("underline").value = tbrUnpressed
    End If
    If (IsNull(EcMensagem.SelAlignment)) Then
        ToolbarFormatacao.Buttons.item("left").value = tbrUnpressed
        ToolbarFormatacao.Buttons.item("center").value = tbrUnpressed
        ToolbarFormatacao.Buttons.item("right").value = tbrUnpressed
    ElseIf (EcMensagem.SelAlignment = rtfLeft) Then
        ToolbarFormatacao.Buttons.item("left").value = tbrPressed
    ElseIf (EcMensagem.SelAlignment = rtfCenter) Then
        ToolbarFormatacao.Buttons.item("center").value = tbrPressed
    ElseIf (EcMensagem.SelAlignment = rtfRight) Then
        ToolbarFormatacao.Buttons.item("right").value = tbrPressed
    End If
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub


' ComboBoxFontsize_Change.
Sub ComboBoxFontsize_Change()

    On Error GoTo ErrorHandler
    EcMensagem.SelFontSize = ComboBoxFontSize.Text
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

' ComboBoxFontsize_Click.
Sub ComboBoxFontsize_Click()

    On Error GoTo ErrorHandler
    EcMensagem.SelFontSize = ComboBoxFontSize.Text
    Exit Sub
    
ErrorHandler:
    Exit Sub
    
End Sub

Public Sub SetiGrEpid(a_iGrEpid As Integer)
    iGrEpid = a_iGrEpid
End Sub

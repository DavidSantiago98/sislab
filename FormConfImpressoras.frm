VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormConfImpressoras 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormConfImpressoras"
   ClientHeight    =   7290
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   9945
   Icon            =   "FormConfImpressoras.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7290
   ScaleWidth      =   9945
   ShowInTaskbar   =   0   'False
   Begin VB.ListBox EcImpressoras 
      Appearance      =   0  'Flat
      Height          =   6660
      Left            =   5640
      TabIndex        =   3
      Top             =   360
      Width           =   4095
   End
   Begin VB.CommandButton BtAssociar 
      Height          =   615
      Left            =   4800
      Picture         =   "FormConfImpressoras.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Associar Profile"
      Top             =   2400
      Width           =   615
   End
   Begin VB.CommandButton BtDessasociar 
      Height          =   615
      Left            =   4800
      Picture         =   "FormConfImpressoras.frx":015C
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Desassociar Profile"
      Top             =   3720
      Width           =   615
   End
   Begin MSComctlLib.TreeView EcComputadores 
      Height          =   6735
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   11880
      _Version        =   393217
      Style           =   7
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   240
      Top             =   6600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormConfImpressoras.frx":02B3
            Key             =   "COMP"
            Object.Tag             =   "COMP"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormConfImpressoras.frx":064D
            Key             =   "TERMINAL2"
            Object.Tag             =   "TERMINAL2"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormConfImpressoras.frx":09E7
            Key             =   "COMP2"
            Object.Tag             =   "COMP2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormConfImpressoras.frx":0D81
            Key             =   "TERMINAL"
            Object.Tag             =   "TERMINAL"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormConfImpressoras.frx":111B
            Key             =   "UTILIZADOR"
            Object.Tag             =   "UTILIZADOR"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormConfImpressoras.frx":14B5
            Key             =   "RELATORIO"
            Object.Tag             =   "RELATORIO"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormConfImpressoras.frx":184F
            Key             =   "IMPRESSORA"
            Object.Tag             =   "IMPRESSORA"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormConfImpressoras.frx":1BE9
            Key             =   "COMP3"
            Object.Tag             =   "COMP3"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormConfImpressoras.frx":1F83
            Key             =   "TERMINAL3"
            Object.Tag             =   "TERMINAL3"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormConfImpressoras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : --/--/----
' T�cnico

' Vari�veis Globais para este Form.

Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos
Dim ListarRemovidos As Boolean

' Utilizada para manter as propriedades dos campos quando
' se sai do Form para outro form
Dim FOPropertiesTemp() As Tipo_FieldObjectProperties
Dim Ind, Max  As Integer

Private Type impressoras
    descr_impressora As String
End Type



Private Type relatorios
    cod_relatorio As Integer
    descr_relatorio As String
    indice As Integer
    descr_impressora As String
    indiceTVImpressora As Integer
End Type
Dim estrutRelatorios() As relatorios
Dim totalRelatorios As Integer

Private Type terminal
    cod_terminal As Integer
    descr_terminal As String
    flgTerminal As Boolean
    indice As Integer
    estrutRelatorios() As relatorios
    totalRelatorios As Integer
    estrutImpressoras() As impressoras
    totalImpressoras As Integer
End Type
Private Type computadores
    cod_computador As Integer
    descr_computador As String
    flgComputador As Boolean
    indice As Integer
    estrutTerminal() As terminal
    totalTerminal As Integer
    estrutRelatorios() As relatorios
    totalRelatorios As Integer
    estrutImpressoras() As impressoras
    totalImpressoras As Integer
End Type
Dim estrutComputadores() As computadores
Dim totalComputadores As Integer

Const KeyComputador = "KeyC"
Const KeyTerminal = "KeyT"
Const KeyUtilizador = "KeyU"
Const KeyRelatorio = "KeyR"
Const KeyImpressora = "KeyI"

Dim indiceTreeView As Integer

Dim indiceSelComp As Integer
Dim indiceSelTerm As Integer
Dim indiceSelRel As Integer

Dim indiceSelImpr As Integer

Const ArvoreExpandida = False

Private Sub BtAssociar_Click()
    Dim root As Node
    If indiceSelComp > mediComboValorNull Then
        If indiceSelImpr > mediComboValorNull Then
            If indiceSelRel = mediComboValorNull Then
                BG_Mensagem mediMsgBox, "Obrigat�rio escolher um tipo de relat�rio.", vbExclamation, "Associar Impressora"
                Exit Sub
            End If
            If indiceSelTerm > mediComboValorNull Then
                If estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).estrutRelatorios(indiceSelRel).indiceTVImpressora = mediComboValorNull Then
                    estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).estrutRelatorios(indiceSelRel).descr_impressora = estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).estrutImpressoras(indiceSelImpr).descr_impressora
                    If AdicionaImpressora(indiceSelComp, indiceSelTerm, indiceSelRel) = True Then
                        indiceTreeView = indiceTreeView + 1
                        estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).estrutRelatorios(indiceSelRel).indiceTVImpressora = indiceTreeView
                        Set root = EcComputadores.Nodes.Add(estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).estrutRelatorios(indiceSelRel).indice, tvwChild, KeyComputador & indiceSelComp & "_" & KeyTerminal & indiceSelTerm & "_" & KeyRelatorio & indiceSelRel & KeyImpressora, estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).estrutRelatorios(indiceSelRel).descr_impressora, "IMPRESSORA")
                        root.Expanded = ArvoreExpandida
                    End If
                
                Else
                    BG_Mensagem mediMsgBox, "Relat�rio j� tem impressora associada.", vbExclamation, "Associar Impressora"
                    Exit Sub
                End If
            Else
                If estrutComputadores(indiceSelComp).estrutRelatorios(indiceSelRel).indiceTVImpressora = mediComboValorNull Then
                    estrutComputadores(indiceSelComp).estrutRelatorios(indiceSelRel).descr_impressora = estrutComputadores(indiceSelComp).estrutImpressoras(indiceSelImpr).descr_impressora
                    If AdicionaImpressora(indiceSelComp, mediComboValorNull, indiceSelRel) = True Then
                        indiceTreeView = indiceTreeView + 1
                        estrutComputadores(indiceSelComp).estrutRelatorios(indiceSelRel).indiceTVImpressora = indiceTreeView
                        Set root = EcComputadores.Nodes.Add(estrutComputadores(indiceSelComp).estrutRelatorios(indiceSelRel).indice, tvwChild, KeyComputador & indiceSelComp & "_" & KeyRelatorio & indiceSelRel & "_" & KeyImpressora, estrutComputadores(indiceSelComp).estrutRelatorios(indiceSelRel).descr_impressora, "IMPRESSORA")
                        root.Expanded = ArvoreExpandida
                    End If
                
                Else
                    BG_Mensagem mediMsgBox, "Relat�rio j� tem impressora associada.", vbExclamation, "Associar Impressora"
                    Exit Sub
                End If
            End If
        Else
            BG_Mensagem mediMsgBox, "N�o escolheu qualquer impressora.", vbExclamation, "Associar Impressora"
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o escolheu qualquer computador.", vbExclamation, "Associar Impressora"
        Exit Sub
    End If

End Sub

Private Sub BtDessasociar_Click()
    If indiceSelComp > mediComboValorNull Then
        If indiceSelRel = mediComboValorNull Then
            BG_Mensagem mediMsgBox, "Obrigat�rio escolher um tipo de relat�rio.", vbExclamation, "Eliminar Impressora"
            Exit Sub
        End If
        If indiceSelTerm > mediComboValorNull Then
            If estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).estrutRelatorios(indiceSelRel).indiceTVImpressora > mediComboValorNull Then
                If EliminaImpressora(indiceSelComp, indiceSelTerm, indiceSelRel) = True Then
                    estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).estrutRelatorios(indiceSelRel).descr_impressora = ""
                    EcComputadores.Nodes.Remove estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).estrutRelatorios(indiceSelRel).indiceTVImpressora
                    estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).estrutRelatorios(indiceSelRel).indiceTVImpressora = mediComboValorNull
                    indiceTreeView = indiceTreeView - 1
                End If
            Else
                BG_Mensagem mediMsgBox, "Relat�rio n�o tem qualquer impressora associada.", vbExclamation, "Eliminar Impressora"
                Exit Sub
            End If
        Else
            If estrutComputadores(indiceSelComp).estrutRelatorios(indiceSelRel).indiceTVImpressora > mediComboValorNull Then
                If EliminaImpressora(indiceSelComp, indiceSelTerm, indiceSelRel) = True Then
                    estrutComputadores(indiceSelComp).estrutRelatorios(indiceSelRel).descr_impressora = ""
                    EcComputadores.Nodes.Remove estrutComputadores(indiceSelComp).estrutRelatorios(indiceSelRel).indiceTVImpressora
                    estrutComputadores(indiceSelComp).estrutRelatorios(indiceSelRel).indiceTVImpressora = mediComboValorNull
                    indiceTreeView = indiceTreeView - 1
                End If
            Else
                BG_Mensagem mediMsgBox, "Relat�rio n�o tem qualquer impressora associada.", vbExclamation, "Eliminar Impressora"
                Exit Sub
            End If
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o escolheu qualquer computador.", vbExclamation, "Eliminar Impressora"
        Exit Sub
    End If
End Sub

Private Sub EcComputadores_Click()
    Dim i As Integer
    Dim chave As String
    Dim novoIndiceComp As Integer
    Dim novoIndiceTerm As Integer
    Dim novoIndiceRel As Integer
    Dim flgActualizaImpressoras As Boolean
    
    chave = CStr(EcComputadores.Nodes.item(EcComputadores.SelectedItem.Index).Key)
    
    'INDICE COMPUTADOR
    If InStr(1, chave, KeyComputador) = 0 Then
        novoIndiceComp = mediComboValorNull
        chave = ""
    Else
        If InStr(1, chave, "_") = 0 Then
            novoIndiceComp = Mid(chave, Len(KeyComputador) + 1)
            chave = ""
        Else
            novoIndiceComp = Mid(chave, Len(KeyComputador) + 1, InStr(1, chave, "_") - Len(KeyComputador) - 1)
            chave = Mid(chave, InStr(1, chave, "_") + 1)
        End If
    End If
    
    'INDICE TERMINAL
    If InStr(1, chave, KeyTerminal) = 0 Then
        novoIndiceTerm = mediComboValorNull
    Else
        If InStr(1, chave, "_") = 0 Then
            novoIndiceTerm = Mid(chave, Len(KeyTerminal) + 1)
        Else
            novoIndiceTerm = Mid(chave, Len(KeyTerminal) + 1, InStr(1, chave, "_") - Len(KeyTerminal) - 1)
            chave = Mid(chave, InStr(1, chave, "_") + 1)
        End If
    End If
    
    'INDICE RELATORIO
    If InStr(1, chave, KeyRelatorio) = 0 Then
        novoIndiceRel = mediComboValorNull
    Else
        If InStr(1, chave, "_") = 0 Then
            novoIndiceRel = Mid(chave, Len(KeyRelatorio) + 1)
        Else
            novoIndiceRel = Mid(chave, Len(KeyRelatorio) + 1, InStr(1, chave, "_") - Len(KeyRelatorio) - 1)
            chave = Mid(chave, InStr(1, chave, "_") + 1)
        End If
    End If
    flgActualizaImpressoras = False
    If novoIndiceComp <> indiceSelComp Then
        flgActualizaImpressoras = True
    End If
    If novoIndiceTerm <> indiceSelTerm Then
        flgActualizaImpressoras = True
    End If
    indiceSelComp = novoIndiceComp
    indiceSelTerm = novoIndiceTerm
    indiceSelRel = novoIndiceRel
    
    If gCodGrupo <> gGrupoAdministradores And estrutComputadores(indiceSelComp).descr_computador <> gComputador Then
        Exit Sub
    ElseIf gCodGrupo <> gGrupoAdministradores And estrutComputadores(indiceSelComp).descr_computador = gComputador Then
        If gTerminal <> "" And indiceSelTerm > mediComboValorNull Then
            If gCodGrupo <> gGrupoAdministradores And estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).descr_terminal <> gTerminal Then
                Exit Sub
            End If
        End If
    End If
    
    If flgActualizaImpressoras = True Then
        LimpaImpressoras
        If indiceSelTerm > mediComboValorNull Then
            For i = 1 To estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).totalImpressoras
                EcImpressoras.AddItem estrutComputadores(indiceSelComp).estrutTerminal(indiceSelTerm).estrutImpressoras(i).descr_impressora
            Next i
        Else
            For i = 1 To estrutComputadores(indiceSelComp).totalImpressoras
                EcImpressoras.AddItem estrutComputadores(indiceSelComp).estrutImpressoras(i).descr_impressora
            Next i
        End If
    End If
End Sub

Private Sub EcImpressoras_Click()
    indiceSelImpr = EcImpressoras.ListIndex + 1
End Sub

Sub Form_Load()
    EventoLoad
End Sub

Sub Form_Activate()
    EventoActivate
End Sub

Sub Form_Unload(Cancel As Integer)
    EventoUnload
End Sub

Sub Inicializacoes()
    Me.caption = " Associa��o de Impressoras"
    Me.left = 5
    Me.top = 5
    Me.Width = 10035
    Me.Height = 7710 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = ""
    Set CampoDeFocus = EcComputadores
    
    NumCampos = 0
    InicializaTreeProfiles
    
End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    BL_InicioProcessamento Me, "Inicializar �cran."
    DefTipoCampos
    Inicializacoes

    Max = -1
    ReDim FOPropertiesTemp(0)

    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
End Sub

Sub EventoActivate()
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
End Sub

Sub EventoUnload()
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormConfImpressoras = Nothing
End Sub

Sub LimpaCampos()
    Me.SetFocus
    LimpaImpressoras
    indiceTreeView = mediComboValorNull
    indiceSelComp = mediComboValorNull
    
    indiceSelRel = mediComboValorNull
    indiceSelTerm = mediComboValorNull
    
End Sub

Sub DefTipoCampos()
    indiceTreeView = mediComboValorNull
    indiceSelComp = mediComboValorNull
    
    indiceSelRel = mediComboValorNull
    indiceSelTerm = mediComboValorNull
End Sub

Sub PreencheCampos()
    'nada
End Sub


Sub FuncaoLimpar()
    
    If estado = 2 Then
        FuncaoEstadoAnterior
    Else
        LimpaCampos
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
    Else
        Unload Me
    End If
End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
      
    'nada
End Sub

Sub FuncaoAnterior()
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
End Sub

Sub FuncaoSeguinte()
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
End Sub

Sub FuncaoInserir()
    'nada
End Sub


Sub FuncaoModificar()
    'nada
End Sub

Sub FuncaoRemover()
    'nada
End Sub


Private Sub PreencheEstrutComputadores()
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iconComputador As String
    Dim rsComputador As New ADODB.recordset
    totalComputadores = 0
    ReDim estrutComputadores(0)
    
    sSql = "SELECT * FROM sl_computador order by descr_computador "
    rsComputador.CursorType = adOpenStatic
    rsComputador.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsComputador.Open sSql, gConexao
    indiceTreeView = 0
    If rsComputador.RecordCount > 0 Then
        While Not rsComputador.EOF
            totalComputadores = totalComputadores + 1
            ReDim Preserve estrutComputadores(totalComputadores)
            indiceTreeView = indiceTreeView + 1
            estrutComputadores(totalComputadores).cod_computador = BL_HandleNull(rsComputador!cod_computador, "")
            estrutComputadores(totalComputadores).descr_computador = BL_HandleNull(rsComputador!descr_computador, "")
            If gComputador = estrutComputadores(totalComputadores).descr_computador Then
                estrutComputadores(totalComputadores).flgComputador = True
            Else
                estrutComputadores(totalComputadores).flgComputador = False
            End If
            estrutComputadores(totalComputadores).indice = indiceTreeView
            estrutComputadores(totalComputadores).totalTerminal = 0
            estrutComputadores(totalComputadores).totalRelatorios = totalRelatorios
            estrutComputadores(totalComputadores).estrutRelatorios = estrutRelatorios
            ReDim estrutComputadores(totalComputadores).estrutTerminal(0)
            Dim root As Node
            If estrutComputadores(totalComputadores).flgComputador = True Then
                iconComputador = "COMP2"
            ElseIf gCodGrupo = gGrupoAdministradores Then
                iconComputador = "COMP"
            Else
                iconComputador = "COMP3"
            End If
            Set root = EcComputadores.Nodes.Add(, tvwChild, KeyComputador & totalComputadores, estrutComputadores(totalComputadores).descr_computador, iconComputador)
            
            root.Expanded = ArvoreExpandida
            PreencheEstrutImpressoras totalComputadores, mediComboValorNull
            PreencheEstrutTerminal totalComputadores
            PreencheEstrutRelatorios totalComputadores, mediComboValorNull
            rsComputador.MoveNext
        Wend
    End If
    rsComputador.Close
    Set rsComputador = Nothing
    
    indiceSelComp = mediComboValorNull
    
    indiceSelRel = mediComboValorNull
    indiceSelTerm = mediComboValorNull
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheEstrutComputadores " & Err.Description, Me.Name, "PreencheEstrutComputadores", True
    Exit Sub
    Resume Next
End Sub

' ----------------------------------------------------------------------------------

' CARREGA A ARVORE DOS PROFILES

' ----------------------------------------------------------------------------------
Private Sub InicializaTreeProfiles()
    On Error GoTo TrataErro
    EcComputadores.LineStyle = tvwRootLines
    EcComputadores.ImageList = ImageList1
    PreencheEstrutComputadores
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  InicializaTreeProfiles " & Err.Description, Me.Name, "InicializaTreeProfiles", True
    Exit Sub
    Resume Next
End Sub




Private Sub PreencheEstrutTerminal(indice As Integer)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iconTerminal As String
    Dim rsTerminal As New ADODB.recordset
    estrutComputadores(indice).totalTerminal = 0
    ReDim Preserve estrutComputadores(indice).estrutTerminal(0)
    
    sSql = "SELECT * FROM sl_terminal WHERE cod_terminal IN (SELECT cod_terminal FROM sl_reg_acessos WHERE cod_computador =  " & estrutComputadores(indice).cod_computador & ") ORDER BY DESCR_TERMINAL"
    rsTerminal.CursorType = adOpenStatic
    rsTerminal.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTerminal.Open sSql, gConexao
    If rsTerminal.RecordCount > 0 Then
        While Not rsTerminal.EOF
            indiceTreeView = indiceTreeView + 1
            Dim root As Node
            estrutComputadores(indice).totalTerminal = estrutComputadores(indice).totalTerminal + 1
            ReDim Preserve estrutComputadores(indice).estrutTerminal(estrutComputadores(indice).totalTerminal)
            estrutComputadores(indice).estrutTerminal(estrutComputadores(indice).totalTerminal).cod_terminal = BL_HandleNull(rsTerminal!cod_terminal, "")
            estrutComputadores(indice).estrutTerminal(estrutComputadores(indice).totalTerminal).descr_terminal = BL_HandleNull(rsTerminal!descr_terminal, "")
            If gTerminal = estrutComputadores(indice).estrutTerminal(estrutComputadores(indice).totalTerminal).descr_terminal Then
                estrutComputadores(indice).estrutTerminal(estrutComputadores(indice).totalTerminal).flgTerminal = True
            Else
                estrutComputadores(indice).estrutTerminal(estrutComputadores(indice).totalTerminal).flgTerminal = False
            End If
            estrutComputadores(indice).estrutTerminal(estrutComputadores(indice).totalTerminal).indice = indiceTreeView
            estrutComputadores(indice).estrutTerminal(estrutComputadores(indice).totalTerminal).totalRelatorios = totalRelatorios
            estrutComputadores(indice).estrutTerminal(estrutComputadores(indice).totalTerminal).estrutRelatorios = estrutRelatorios
            If estrutComputadores(indice).estrutTerminal(estrutComputadores(indice).totalTerminal).flgTerminal = True Then
                iconTerminal = "TERMINAL2"
            ElseIf gCodGrupo = gGrupoAdministradores Then
                iconTerminal = "TERMINAL"
            Else
                iconTerminal = "TERMINAL3"
            End If
            
            Set root = EcComputadores.Nodes.Add(estrutComputadores(indice).indice, tvwChild, KeyComputador & indice & "_" & KeyTerminal & estrutComputadores(indice).totalTerminal, estrutComputadores(indice).estrutTerminal(estrutComputadores(indice).totalTerminal).descr_terminal, iconTerminal)
            root.Expanded = ArvoreExpandida
            PreencheEstrutImpressoras indice, estrutComputadores(indice).totalTerminal
            PreencheEstrutRelatorios indice, estrutComputadores(indice).totalTerminal
            rsTerminal.MoveNext
        Wend
    End If
    rsTerminal.Close
    Set rsTerminal = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheEstrutComputadores " & Err.Description, Me.Name, "PreencheEstrutComputadores", True
    Exit Sub
    Resume Next

End Sub



Private Sub PreencheEstrutRelatorios(indiceComputador As Integer, indiceTerminal As Integer)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsRel As New ADODB.recordset
    Dim root As Node
    
    totalRelatorios = 0
    ReDim estrutRelatorios(0)
    If indiceTerminal = mediComboValorNull Then
        estrutComputadores(indiceComputador).totalRelatorios = 0
        ReDim Preserve estrutComputadores(indiceComputador).estrutRelatorios(0)
    Else
        estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios = 0
        ReDim Preserve estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutRelatorios(0)
    End If
    
    sSql = "SELECT x1.seq_relatorio, x1.descr_relatorio,x2.descr_prt "
    sSql = sSql & " FROM sl_relatorios x1 LEFT OUTER JOIN sl_conf_prt x2 ON x1.descr_relatorio = x2.descr_rpt "
    sSql = sSql & " AND cod_utilizador IS NULL "
    sSql = sSql & " AND cod_computador = " & estrutComputadores(indiceComputador).cod_computador
    If indiceTerminal > mediComboValorNull Then
        sSql = sSql & " AND cod_terminal = " & estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).cod_terminal
    Else
        sSql = sSql & " AND (cod_terminal = -1 OR cod_terminal IS NULL)"
    End If
    RsRel.CursorType = adOpenStatic
    RsRel.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsRel.Open sSql, gConexao
    If RsRel.RecordCount > 0 Then
        While Not RsRel.EOF
            If indiceTerminal = mediComboValorNull Then
            
                indiceTreeView = indiceTreeView + 1
                estrutComputadores(indiceComputador).totalRelatorios = estrutComputadores(indiceComputador).totalRelatorios + 1
                ReDim Preserve estrutComputadores(indiceComputador).estrutRelatorios(estrutComputadores(indiceComputador).totalRelatorios)
                estrutComputadores(indiceComputador).estrutRelatorios(estrutComputadores(indiceComputador).totalRelatorios).cod_relatorio = BL_HandleNull(RsRel!seq_relatorio, mediComboValorNull)
                estrutComputadores(indiceComputador).estrutRelatorios(estrutComputadores(indiceComputador).totalRelatorios).descr_relatorio = BL_HandleNull(RsRel!descr_relatorio, "")
                estrutComputadores(indiceComputador).estrutRelatorios(estrutComputadores(indiceComputador).totalRelatorios).indice = indiceTreeView
                estrutComputadores(indiceComputador).estrutRelatorios(estrutComputadores(indiceComputador).totalRelatorios).descr_impressora = BL_HandleNull(RsRel!descr_prt, "")
                estrutComputadores(indiceComputador).estrutRelatorios(estrutComputadores(indiceComputador).totalRelatorios).indiceTVImpressora = mediComboValorNull
                Set root = EcComputadores.Nodes.Add(estrutComputadores(indiceComputador).indice, tvwChild, KeyComputador & indiceComputador & "_" & KeyRelatorio & estrutComputadores(indiceComputador).totalRelatorios, estrutComputadores(indiceComputador).estrutRelatorios(estrutComputadores(indiceComputador).totalRelatorios).descr_relatorio, "RELATORIO")
                root.Expanded = ArvoreExpandida
                If estrutComputadores(indiceComputador).estrutRelatorios(estrutComputadores(indiceComputador).totalRelatorios).descr_impressora <> "" Then
                    indiceTreeView = indiceTreeView + 1
                    estrutComputadores(indiceComputador).estrutRelatorios(estrutComputadores(indiceComputador).totalRelatorios).indiceTVImpressora = indiceTreeView
                    Set root = EcComputadores.Nodes.Add(estrutComputadores(indiceComputador).estrutRelatorios(estrutComputadores(indiceComputador).totalRelatorios).indice, tvwChild, KeyComputador & indiceComputador & "_" & KeyRelatorio & estrutComputadores(indiceComputador).totalRelatorios & "_" & KeyImpressora, estrutComputadores(indiceComputador).estrutRelatorios(estrutComputadores(indiceComputador).totalRelatorios).descr_impressora, "IMPRESSORA")
                    root.Expanded = ArvoreExpandida
                End If
            Else
                indiceTreeView = indiceTreeView + 1
                
                estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios = estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios + 1
                ReDim Preserve estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutRelatorios(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios)
                estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutRelatorios(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios).cod_relatorio = BL_HandleNull(RsRel!seq_relatorio, mediComboValorNull)
                estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutRelatorios(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios).descr_relatorio = BL_HandleNull(RsRel!descr_relatorio, "")
                estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutRelatorios(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios).indice = indiceTreeView
                estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutRelatorios(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios).descr_impressora = BL_HandleNull(RsRel!descr_prt, "")
                estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutRelatorios(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios).indiceTVImpressora = mediComboValorNull
                Set root = EcComputadores.Nodes.Add(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).indice, tvwChild, KeyComputador & indiceComputador & "_" & KeyTerminal & indiceTerminal & "_" & KeyRelatorio & estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios, estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutRelatorios(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios).descr_relatorio, "RELATORIO")
                root.Expanded = ArvoreExpandida
                If estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutRelatorios(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios).descr_impressora <> "" Then
                    indiceTreeView = indiceTreeView + 1
                    estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutRelatorios(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios).indiceTVImpressora = indiceTreeView
                    Set root = EcComputadores.Nodes.Add(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutRelatorios(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios).indice, tvwChild, KeyComputador & indiceComputador & "_" & KeyTerminal & indiceTerminal & "_" & KeyRelatorio & estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios & "_" & KeyImpressora, estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutRelatorios(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalRelatorios).descr_impressora, "IMPRESSORA")
                    root.Expanded = ArvoreExpandida
                End If
            End If
            RsRel.MoveNext
        Wend
    End If
    RsRel.Close
    Set RsRel = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheEstrutRelatorios " & Err.Description, Me.Name, "PreencheEstrutRelatorios", True
    Exit Sub
    Resume Next
End Sub

Private Sub PreencheEstrutImpressoras(ByVal indiceComputador As Integer, ByVal indiceTerminal As Integer)
    Dim sSql As String
    Dim rsImpr As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM sl_impressora WHERE cod_computador =  " & estrutComputadores(indiceComputador).cod_computador
    If indiceTerminal > mediComboValorNull Then
        sSql = sSql & " AND cod_terminal = " & estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).cod_terminal
    Else
        sSql = sSql & " AND (cod_terminal IS NULL or cod_terminal = -1)"
    End If
    rsImpr.CursorType = adOpenStatic
    rsImpr.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsImpr.Open sSql, gConexao
    If indiceTerminal > mediComboValorNull Then
        estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalImpressoras = 0
        ReDim estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutImpressoras(0)
    Else
        estrutComputadores(indiceComputador).totalImpressoras = 0
        ReDim estrutComputadores(indiceComputador).estrutImpressoras(0)
    End If
    If rsImpr.RecordCount > 0 Then
        While Not rsImpr.EOF
            If indiceTerminal = mediComboValorNull Then
                estrutComputadores(indiceComputador).totalImpressoras = estrutComputadores(indiceComputador).totalImpressoras + 1
                ReDim Preserve estrutComputadores(indiceComputador).estrutImpressoras(estrutComputadores(indiceComputador).totalImpressoras)
                estrutComputadores(indiceComputador).estrutImpressoras(estrutComputadores(indiceComputador).totalImpressoras).descr_impressora = BL_HandleNull(rsImpr!descr_impressora, "")
            Else
                estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalImpressoras = estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalImpressoras + 1
                ReDim Preserve estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutImpressoras(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalImpressoras)
                estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).estrutImpressoras(estrutComputadores(indiceComputador).estrutTerminal(indiceTerminal).totalImpressoras).descr_impressora = BL_HandleNull(rsImpr!descr_impressora, "")
            End If
            rsImpr.MoveNext
        Wend
    End If
    rsImpr.Close
    Set rsImpr = Nothing
    
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  PreencheEstrutImpressoras " & Err.Description, Me.Name, "PreencheEstrutImpressoras", True
    Exit Sub
    Resume Next
End Sub

Private Function EliminaImpressora(iComp As Integer, iTerm As Integer, iRel As Integer) As Boolean
    EliminaImpressora = False
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iReg As Integer
    
    sSql = "DELETE FROM sl_conf_prt WHERE cod_computador  =" & estrutComputadores(iComp).cod_computador & " AND (cod_utilizador IS NULL OR cod_utilizador = -1)"
    If iTerm > mediComboValorNull Then
        sSql = sSql & " AND cod_terminal = " & estrutComputadores(iComp).estrutTerminal(iTerm).cod_terminal
        sSql = sSql & " AND descr_rpt = " & BL_TrataStringParaBD(estrutComputadores(iComp).estrutTerminal(iTerm).estrutRelatorios(iRel).descr_relatorio)
    Else
        sSql = sSql & " AND (cod_terminal = -1 OR cod_terminal IS NULL) "
        sSql = sSql & " AND descr_rpt = " & BL_TrataStringParaBD(estrutComputadores(iComp).estrutRelatorios(iRel).descr_relatorio)
    End If
    iReg = BG_ExecutaQuery_ADO(sSql)
    If iReg = 1 Then
        EliminaImpressora = True
    End If
Exit Function
TrataErro:
    EliminaImpressora = False
    BG_LogFile_Erros "Erro  EliminaImpressora " & Err.Description, Me.Name, "EliminaImpressora", True
    Exit Function
    Resume Next
End Function

Private Function AdicionaImpressora(iComp As Integer, iTerm As Integer, iRel As Integer) As Boolean
    AdicionaImpressora = False
    On Error GoTo TrataErro
    Dim sSql As String
    Dim iReg As Integer
    
    sSql = "INSERT INTO sl_conf_prt (seq_conf_prt, cod_computador, descr_prt, cod_terminal, descr_rpt, user_cri, dt_cri) VALUES("
    sSql = sSql & " nvl((SELECT MAX (seq_conf_prt) + 1 FROM sl_conf_prt),0),"
    sSql = sSql & estrutComputadores(iComp).cod_computador & ", "
    If iTerm > mediComboValorNull Then
        sSql = sSql & BL_TrataStringParaBD(estrutComputadores(iComp).estrutTerminal(iTerm).estrutRelatorios(iRel).descr_impressora) & ", "
        sSql = sSql & estrutComputadores(iComp).estrutTerminal(iTerm).cod_terminal & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutComputadores(iComp).estrutTerminal(iTerm).estrutRelatorios(iRel).descr_relatorio) & ", "
    Else
        sSql = sSql & BL_TrataStringParaBD(estrutComputadores(iComp).estrutRelatorios(iRel).descr_impressora) & ", "
        sSql = sSql & mediComboValorNull & ", "
        sSql = sSql & BL_TrataStringParaBD(estrutComputadores(iComp).estrutRelatorios(iRel).descr_relatorio) & ", "
    End If
    sSql = sSql & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ","
    sSql = sSql & BL_TrataDataParaBD(Bg_DaData_ADO) & ")"
    iReg = BG_ExecutaQuery_ADO(sSql)
    
    If iReg = 1 Then
        AdicionaImpressora = True
    End If
Exit Function
TrataErro:
    AdicionaImpressora = False
    BG_LogFile_Erros "Erro  AdicionaImpressora " & Err.Description, Me.Name, "AdicionaImpressora", True
    Exit Function
    Resume Next
End Function

Private Sub LimpaImpressoras()
    EcImpressoras.Clear
    indiceSelImpr = mediComboValorNull
End Sub

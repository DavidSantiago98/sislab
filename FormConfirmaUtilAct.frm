VERSION 5.00
Object = "{2EF1D294-3C92-4B2E-9781-6205A0F17CDC}#1.0#0"; "FluxCtrl.ocx"
Begin VB.Form FormConfirmaUtilAct 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   4950
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7515
   Icon            =   "FormConfirmaUtilAct.frx":0000
   LinkTopic       =   "FormConfirmaUtilAct"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4950
   ScaleWidth      =   7515
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton ButtonOK 
      Default         =   -1  'True
      Height          =   375
      Left            =   1920
      Picture         =   "FormConfirmaUtilAct.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   3480
      Width           =   1095
   End
   Begin VB.CommandButton ButtonCancelar 
      Height          =   375
      Left            =   4320
      Picture         =   "FormConfirmaUtilAct.frx":0687
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   3480
      Width           =   1095
   End
   Begin VB.TextBox EcUtilizador 
      Height          =   285
      Left            =   1920
      TabIndex        =   1
      Top             =   2520
      Width           =   3495
   End
   Begin VB.TextBox EcSenhaAux 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1920
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   3000
      Width           =   3495
   End
   Begin VB.TextBox EcSenha 
      Height          =   285
      Left            =   1920
      TabIndex        =   0
      Top             =   3240
      Visible         =   0   'False
      Width           =   3495
   End
   Begin FluxCtrl.IDLETimer IDLETimer1 
      Left            =   120
      Top             =   3600
      _ExtentX        =   873
      _ExtentY        =   873
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1920
      Width           =   2295
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Utilizador"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   960
      TabIndex        =   4
      Top             =   2520
      Width           =   810
   End
   Begin VB.Label Label3 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Senha"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   960
      TabIndex        =   3
      Top             =   3000
      Width           =   735
   End
   Begin VB.Image Image1 
      Height          =   5010
      Left            =   0
      Picture         =   "FormConfirmaUtilAct.frx":0D1D
      Top             =   0
      Width           =   7515
   End
End
Attribute VB_Name = "FormConfirmaUtilAct"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Vari�veis Globais para este Form.

Option Explicit   ' Pada obrigar a definir as vari�veis.

Dim valida As Boolean
Dim NumTentativas As Integer



Private Sub ButtonCancelar_Click()
    
    If gTimeOutActivo = True Then
        End
    Else
        Unload Me
    End If
    
End Sub

Private Sub ButtonOK_Click()
    Dim rsTabela As ADODB.recordset
    Dim CriterioTabela As String
    Dim Erro As Boolean
    Dim CodUtil As String
    
    
    On Error GoTo TrataErro
    'RGONCALVES 07.07.2015 CHSJ-2082
    If gUsaAutenticacaoActiveDirectory >= 1 Then
        If BL_AuthenticateActiveDirectoryUser(gDominioActiveDirectory, EcUtilizador.Text, EcSenhaAux.Text) = True Then
            valida = True
            Unload Me
            Exit Sub
        Else
            If gUsaAutenticacaoActiveDirectory <= 1 Then
                NumTentativas = NumTentativas + 1
                If NumTentativas < 3 Then
                    BG_Mensagem mediMsgBox, "Utilizador n�o validado!", vbExclamation, "Verificar Utilizador"
                    EcUtilizador.SetFocus
                    Exit Sub
                Else
                    BG_Mensagem mediMsgBox, "Fim das tentativas permitidas!", vbExclamation, "Verificar Utilizador"
                    If gTimeOutActivo = True Then
                        End
                    Else
                        Unload Me
                    End If
                End If
                Exit Sub
            End If
        End If
    End If
    '
    Set rsTabela = New ADODB.recordset
    
    Erro = False
    NumTentativas = NumTentativas + 1
    
    EcSenha = BL_Encripta(EcSenhaAux)
    ' Case sensitive ou n�o.
    If (gLoginCaseSensitive) Then
        CriterioTabela = "SELECT * FROM sl_idutilizador WHERE utilizador = '" & BG_CvPlica(EcUtilizador) & "' AND senha = "
    Else
        CriterioTabela = "SELECT * FROM sl_idutilizador WHERE UPPER(utilizador) = '" & BG_CvPlica(UCase(EcUtilizador)) & "' AND senha = "
    End If
    If EcSenha = "" Then
        CriterioTabela = CriterioTabela & "Null"
    Else
        CriterioTabela = CriterioTabela & "'" & BG_CvPlica(EcSenha) & "'"
    End If
    
    rsTabela.CursorLocation = adUseServer
    rsTabela.Open CriterioTabela, gConexao, adOpenStatic
    
    If rsTabela.RecordCount > 0 Then
        CodUtil = rsTabela!cod_utilizador
    Else
        CodUtil = CStr(gCodUtilizador)
    End If
    
    If gTimeOutActivo = True And CStr(CodUtil) <> CStr(gCodUtilizador) Then
        GoTo ErroLogin
    End If
    
    If rsTabela.RecordCount = 0 Or Erro = True Then
ErroLogin:
        If NumTentativas < 3 Then
            BG_Mensagem mediMsgBox, "Utilizador n�o validado!", vbExclamation, "Verificar Utilizador"
            
            EcUtilizador.SetFocus
            rsTabela.Close
            Set rsTabela = Nothing
            Exit Sub
        Else
            BG_Mensagem mediMsgBox, "Fim das tentativas permitidas!", vbExclamation, "Verificar Utilizador"
            rsTabela.Close
            Set rsTabela = Nothing
            
            If gTimeOutActivo = True Then
                End
            Else
                Unload Me
            End If
        End If
    Else
        
                    
        valida = True
        rsTabela.Close
        Set rsTabela = Nothing
        Unload Me

    End If
    Exit Sub

TrataErro:
    Erro = True
    Resume Next

End Sub


Private Sub EcSenhaAux_Change()
    'EcSenha = BL_Encripta(EcSenhaAux)
End Sub

Private Sub EcSenhaAux_GotFocus()
    Dim EsteControl As Control

    Set EsteControl = EcSenhaAux
    
    EsteControl.SelStart = 0
    EsteControl.SelLength = Len(EsteControl)
    
    '---------------------------------------
    
    EcSenhaAux.MaxLength = EcSenha.MaxLength
End Sub

Private Sub EcUtilizador_GotFocus()
    Dim EsteControl As Control

    Set EsteControl = EcUtilizador
    
    EsteControl.SelStart = 0
    EsteControl.SelLength = Len(EsteControl)
End Sub


Private Sub Form_Activate()
    EcUtilizador.Text = gIdUtilizador
    'EcUtilizador.SetFocus
    EcSenhaAux.SetFocus
    
End Sub

Private Sub Form_Load()
    Me.caption = "Verifica��o do Utilizador " & cAPLICACAO_NOME_CURTO
    Me.left = 1665
    Me.top = 2160
    Me.Width = 6810
    Me.Height = 4650
    
    'Label1 = "Introduza o Utilizador e a Senha."
    Label1 = "Esta sess�o est� bloqueada para o utilizador " & gIdUtilizador & ". Confirme Senha."
    
    EcUtilizador.MaxLength = 20
    EcSenha.MaxLength = 14
    
    
    NumTentativas = 0
    
    IDLETimer1.MinutosDeIntervalo = gTimeOutActivoTerminaAplicacao
    valida = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If valida = False Then
        BG_Mensagem mediMsgBox, "Fim da aplica��o!", vbExclamation
        End
    End If
End Sub

Private Sub IDLETimer1_Inactividade()
    BG_Mensagem mediMsgBox, "Por raz�es de seguran�a, a sua sess�o expirou." & vbCrLf & vbCrLf & "Ter� que voltar a inicializar a aplica��o.", vbExclamation, "..."
    End
End Sub




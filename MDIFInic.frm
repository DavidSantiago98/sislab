VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{2EF1D294-3C92-4B2E-9781-6205A0F17CDC}#1.0#0"; "FluxCtrl.ocx"
Begin VB.MDIForm MDIFormInicio 
   Appearance      =   0  'Flat
   AutoShowChildren=   0   'False
   BackColor       =   &H00FFE0C7&
   Caption         =   "CaptionMDIFormInicio"
   ClientHeight    =   8115
   ClientLeft      =   165
   ClientTop       =   630
   ClientWidth     =   15240
   Icon            =   "MDIFInic.frx":0000
   MouseIcon       =   "MDIFInic.frx":164A
   MousePointer    =   1  'Arrow
   Picture         =   "MDIFInic.frx":2C94
   WindowState     =   2  'Maximized
   Begin VB.Timer TimerAlertas 
      Left            =   1560
      Top             =   3720
   End
   Begin VB.Timer Timers 
      Index           =   2
      Interval        =   60000
      Left            =   840
      Top             =   3720
   End
   Begin VB.Timer Timers 
      Index           =   1
      Left            =   480
      Top             =   3720
   End
   Begin FluxCtrl.IDLETimer IDLETimer1 
      Left            =   120
      Top             =   4440
      _ExtentX        =   873
      _ExtentY        =   873
   End
   Begin Crystal.CrystalReport Report 
      Left            =   2520
      Top             =   3840
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog CmdDlgPrint 
      Left            =   960
      Top             =   4440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin ComCtl3.CoolBar CoolBar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Visible         =   0   'False
      Width           =   15240
      _ExtentX        =   26882
      _ExtentY        =   1535
      BandCount       =   2
      EmbossPicture   =   -1  'True
      _CBWidth        =   15240
      _CBHeight       =   870
      _Version        =   "6.7.8862"
      Child1          =   "Toolbar1"
      MinHeight1      =   390
      Width1          =   5355
      NewRow1         =   0   'False
      AllowVertical1  =   0   'False
      Child2          =   "Toolbar2"
      MinHeight2      =   390
      Width2          =   5880
      NewRow2         =   -1  'True
      AllowVertical2  =   0   'False
      Begin MSComctlLib.Toolbar Toolbar2 
         Height          =   390
         Left            =   165
         TabIndex        =   3
         Top             =   450
         Width           =   14985
         _ExtentX        =   26432
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Wrappable       =   0   'False
         ImageList       =   "ImageList2"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   25
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "DataActual"
               Object.ToolTipText     =   "Data Actual"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "COPIANUTE"
               Object.ToolTipText     =   "Nr. de Utente activo"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "COPIANREQ"
               Object.ToolTipText     =   "Nr. de Requisi��o activa"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PESQFRASES"
               Object.ToolTipText     =   "Consultar Frases"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "MENUSPERSONALIZADOS"
               Object.ToolTipText     =   "Menus Personalizados"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "GESCOM"
               Object.ToolTipText     =   "Gescom - Gest�o de Comunica��o com Aparelhos"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SistemaInterrogacao"
               Object.ToolTipText     =   "Sistema de Interrogacao"
               ImageIndex      =   7
            EndProperty
            BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "MudarGrAna"
               Object.ToolTipText     =   "Mudar Grupo de An�lises"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button16 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "MudarComputador"
               Object.ToolTipText     =   "Mudar Computador Associado"
               ImageIndex      =   11
            EndProperty
            BeginProperty Button17 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "MUDARPOSTOCOLHE"
               Object.ToolTipText     =   "Mudar Posto de Colheita"
               ImageIndex      =   12
            EndProperty
            BeginProperty Button18 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button19 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Correio"
               Object.ToolTipText     =   "Caixa de Correio"
               ImageIndex      =   10
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   1
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button20 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button21 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Sinave"
               Object.ToolTipText     =   "Integra��o Sinave"
               ImageIndex      =   14
            EndProperty
            BeginProperty Button22 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PRESCRICOES"
               Object.ToolTipText     =   "Prescri��es"
               ImageIndex      =   15
            EndProperty
            BeginProperty Button23 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button24 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Mis_data_discovery"
               Object.ToolTipText     =   "Data Discovery"
               ImageIndex      =   16
            EndProperty
            BeginProperty Button25 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "ALERTAS"
               Object.ToolTipText     =   "Alertas"
               ImageIndex      =   17
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   390
         Left            =   165
         TabIndex        =   2
         Top             =   30
         Width           =   14985
         _ExtentX        =   26432
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Wrappable       =   0   'False
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   17
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Imprimir"
               Object.ToolTipText     =   "Imprimir"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "ImprimirVerAntes"
               Object.ToolTipText     =   "Imprimir (Ver Antes)"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Limpar"
               Object.ToolTipText     =   "Limpar"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Inserir"
               Object.ToolTipText     =   "Validar Inser��o"
               ImageIndex      =   12
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Procurar"
               Object.ToolTipText     =   "Procurar"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Modificar"
               Object.ToolTipText     =   "Modificar"
               ImageIndex      =   12
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Remover"
               Object.ToolTipText     =   "Remover"
               ImageIndex      =   7
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Anterior"
               Object.ToolTipText     =   "Registo Anterior"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Seguinte"
               Object.ToolTipText     =   "Registo Seguinte"
               ImageIndex      =   9
            EndProperty
            BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "EstadoAnterior"
               Object.ToolTipText     =   "Estado Anterior"
               ImageIndex      =   10
            EndProperty
            BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button16 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button17 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Sair"
               Object.ToolTipText     =   "Sair"
               ImageIndex      =   11
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   7740
      Visible         =   0   'False
      Width           =   15240
      _ExtentX        =   26882
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   9
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   5292
            MinWidth        =   5292
            Key             =   "Mensagem"
            Object.ToolTipText     =   "Mensagens da aplica��o"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Object.Width           =   5362
            MinWidth        =   5362
            Key             =   "RES_CRIT"
            Object.ToolTipText     =   "Resultados Criticos"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   2999
            MinWidth        =   2999
            Key             =   "Versao"
            Object.ToolTipText     =   "Vers�o aplica��o"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5292
            MinWidth        =   5292
            Key             =   "Local"
            Object.ToolTipText     =   "Local"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   2646
            MinWidth        =   2646
            Key             =   "Grupo"
            Object.ToolTipText     =   "Grupo"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1402
            MinWidth        =   1411
            Key             =   "Data"
            Object.ToolTipText     =   "Entrada na aplica��o"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1058
            MinWidth        =   1058
            Key             =   "Hora"
            Object.ToolTipText     =   "Entrada na aplica��o"
         EndProperty
         BeginProperty Panel8 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   873
            MinWidth        =   882
            Key             =   "Utilizador"
            Object.ToolTipText     =   "Utilizador"
         EndProperty
         BeginProperty Panel9 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Impress�o remota inactiva"
            TextSave        =   "Impress�o remota inactiva"
            Key             =   "Remota"
            Object.ToolTipText     =   "Duplo click para alterar."
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Timer Timers 
      Index           =   0
      Left            =   0
      Top             =   3720
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   360
      Top             =   2160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":14ACE
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":14C28
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":14D82
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":14EDC
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":15036
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":15190
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":152EA
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":15444
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":1559E
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":156F8
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":15852
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":159AC
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   960
      Top             =   2160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   16777215
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   20
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":15CC6
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":15E20
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":15F7A
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":160D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":163EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":16708
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":16862
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":169BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":17296
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":17F70
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":1E7D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":1EB6C
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":253CE
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":26A28
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":27702
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":27E7C
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":2824B
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":288D5
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":28F9A
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIFInic.frx":2967F
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu IDM_MENU_FICHEIRO 
      Caption         =   "&Ficheiro"
      Begin VB.Menu IDM_MUDAR_SENHA 
         Caption         =   "&Mudar Senha"
      End
      Begin VB.Menu SEP_MENU_FICHEIRO_1 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_DATA_ACTUAL 
         Caption         =   "&Data Actual"
      End
      Begin VB.Menu SEP_MENU_FICHEIRO_2 
         Caption         =   "-"
         Index           =   0
      End
      Begin VB.Menu IDM_CONFIG_IMPRESSORA 
         Caption         =   "&Configura��o de Impressoras..."
      End
      Begin VB.Menu IDM_VER_ANTES 
         Caption         =   "&Ver Antes"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
      Begin VB.Menu IDM_IMPRIMIR_VER_ANTES 
         Caption         =   "Imprimir (&Ver Antes)"
         Shortcut        =   ^{F2}
      End
      Begin VB.Menu IDM_IMPRIMIR 
         Caption         =   "&Imprimir"
         Shortcut        =   {F2}
      End
      Begin VB.Menu SEP_MENU_FICHEIRO_3 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_PREFERENCIAS 
         Caption         =   "&Prefer�ncias"
         Begin VB.Menu IDM_PREF_GERAIS 
            Caption         =   "&Gerais"
            Begin VB.Menu IDE_PARAM_AMBIENTE_GERAL 
               Caption         =   "Parametriza��es do Am&biente"
            End
         End
         Begin VB.Menu IDM_PREF_LOCAIS 
            Caption         =   "&Locais"
            Begin VB.Menu IDE_PARAM_AMBIENTE_LOCAL 
               Caption         =   "Parametriza��es do Am&biente"
            End
         End
         Begin VB.Menu IDM_IDCLIENTE 
            Caption         =   "Identifica��o do Cliente"
         End
         Begin VB.Menu IDM_ADC_CLIENTE 
            Caption         =   "Dados Adicionais do Cliente"
         End
      End
      Begin VB.Menu IDM_ADMINISTRACAO 
         Caption         =   "&Administra��o"
         Begin VB.Menu IDM_INI 
            Caption         =   "&Inicializa��es de Sistema"
         End
         Begin VB.Menu IDM_SEPARA13 
            Caption         =   "-"
         End
         Begin VB.Menu IDM_DESCR_REL 
            Caption         =   "&Descri��o de Relat�rios"
         End
         Begin VB.Menu IDM_SEPARA131 
            Caption         =   "-"
         End
         Begin VB.Menu IDM_GRUPO_UTILIZADOR 
            Caption         =   "&Grupos de Utilizadores"
         End
         Begin VB.Menu IDM_ID_UTILIZADORES 
            Caption         =   "&Utilizadores"
         End
         Begin VB.Menu IDM_SEPARA7 
            Caption         =   "-"
         End
         Begin VB.Menu IDM_PARAM_ACESSOS 
            Caption         =   "&Controlo de Acessos"
            Index           =   0
         End
         Begin VB.Menu IDM_PARAM_ACESSOS 
            Caption         =   "Gerar Controlos"
            Index           =   1
         End
         Begin VB.Menu IDM_TRANS_PERMISSOES 
            Caption         =   "&Transfer�ncia de Permiss�es"
         End
         Begin VB.Menu IDM_REGISTO_ACESSOS 
            Caption         =   "Registo de &Acessos"
         End
         Begin VB.Menu SEP_ADMINISTRACAO_1 
            Caption         =   "-"
         End
         Begin VB.Menu IDM_VER_LOG_FILE 
            Caption         =   "Ver ""&Log File"""
         End
      End
      Begin VB.Menu SEP_MENU_FICHEIRO_4 
         Caption         =   "-"
         Index           =   0
      End
      Begin VB.Menu SEP_MENU_FICHEIRO_4 
         Caption         =   "Mudar Utilizador"
         Index           =   1
      End
      Begin VB.Menu IDM_SAIR 
         Caption         =   "&Sair"
         Index           =   0
      End
      Begin VB.Menu IDM_SAIR 
         Caption         =   "Desbloquear Requisi��es"
         Index           =   1
      End
   End
   Begin VB.Menu IDM_MENU_REGISTO 
      Caption         =   "&Registo"
      Begin VB.Menu IDM_LIMPAR 
         Caption         =   "&Limpar"
         Shortcut        =   ^L
      End
      Begin VB.Menu IDM_INSERIR 
         Caption         =   "&Inserir"
         Shortcut        =   ^I
      End
      Begin VB.Menu SEP_MENU_REGISTO_1 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_PROCURAR 
         Caption         =   "&Procurar"
         Shortcut        =   ^P
      End
      Begin VB.Menu IDM_MODIFICAR 
         Caption         =   "&Modificar"
         Shortcut        =   ^M
      End
      Begin VB.Menu IDM_REMOVER 
         Caption         =   "&Remover"
         Shortcut        =   ^R
      End
      Begin VB.Menu SEP_MENU_REGISTO_2 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_ANTERIOR 
         Caption         =   "Registo &Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu IDM_SEGUINTE 
         Caption         =   "Registo &Seguinte"
         Shortcut        =   ^S
      End
      Begin VB.Menu SEP_MENU_REGISTO_3 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_ESTADO_ANTERIOR 
         Caption         =   "&Estado Anterior"
         Shortcut        =   ^E
      End
   End
   Begin VB.Menu IDM_PRINCIPAL 
      Caption         =   "&Principal"
      Begin VB.Menu IDM_SECRETARIA 
         Caption         =   "&Secretaria"
         Begin VB.Menu IDM_ID_DOENTES 
            Caption         =   "&Identifica��o de Utentes"
            Index           =   0
         End
         Begin VB.Menu IDM_ID_DOENTES 
            Caption         =   "Requi��es Electr�nicas Pedidas"
            Index           =   1
         End
         Begin VB.Menu IDM_ID_DOENTES 
            Caption         =   "Marca��o de Colheitas"
            Index           =   2
         End
         Begin VB.Menu IDM_REUTILIZACAO_COLHEITAS 
            Caption         =   "Reutiliza��o de Colheitas"
            Index           =   0
         End
         Begin VB.Menu IDM_REQUISICOES 
            Caption         =   "Gest�o de &Requisi��es"
            Index           =   0
         End
         Begin VB.Menu IDM_REQUISICOES 
            Caption         =   "Entrega de Resultados"
            Index           =   1
         End
         Begin VB.Menu IDM_REQUISICOES_PRIVADO 
            Caption         =   "Gest�o de Requisi��es"
            Begin VB.Menu IDM_REQUISICOES_PRIV 
               Caption         =   "Gest�o de &Requisi��es"
               Index           =   0
            End
            Begin VB.Menu IDM_REQUISICOES_PRIV 
               Caption         =   "Gest�o de &Requisi��es - Fim de Semana"
               Index           =   1
            End
            Begin VB.Menu IDM_REQUISICOES_PRIV 
               Caption         =   "Entrega Resultados"
               Index           =   2
            End
         End
         Begin VB.Menu IDM_MENU_CONSREQ 
            Caption         =   "&Consulta de Requisi��es"
            Begin VB.Menu IDM_CONS_REQUIS 
               Caption         =   "Requisi��es por &Utente"
               Index           =   0
            End
            Begin VB.Menu IDM_CONS_REQUIS 
               Caption         =   "&Requisi��es"
               Index           =   1
            End
            Begin VB.Menu IDM_CONS_REQUIS 
               Caption         =   "Consulta de Requisi��es"
               Index           =   2
            End
         End
         Begin VB.Menu IDM_REQCANCELADAS 
            Caption         =   "Requisi��es C&anceladas"
         End
         Begin VB.Menu IDM_IMPR_CARTOES 
            Caption         =   "Impress�o de Cart�es - S�rie"
         End
         Begin VB.Menu IDM_MAPAREG 
            Caption         =   "&Mapa de Registo"
            Begin VB.Menu IDM_MAPAREGGLOB 
               Caption         =   "&Global"
            End
            Begin VB.Menu IDM_MAPAREGDETAL 
               Caption         =   "&Detalhado"
            End
         End
         Begin VB.Menu HIDE_TUBO 
            Caption         =   "Gest�o de Tubos"
            Begin VB.Menu IDM_PORTUBO 
               Caption         =   "Chegada de Tubos"
               Index           =   0
            End
            Begin VB.Menu IDM_PORTUBO 
               Caption         =   "Colheita de Tubos"
               Index           =   1
            End
            Begin VB.Menu IDM_PORTUBO 
               Caption         =   "Chamada de Utentes"
               Index           =   2
            End
         End
         Begin VB.Menu IDM_TRANSFNUM 
            Caption         =   "&Transfer�ncia de Utentes"
            Index           =   0
         End
         Begin VB.Menu IDM_TRANSFNUM 
            Caption         =   "&Transfer�ncia de Requisi��es"
            Index           =   1
         End
         Begin VB.Menu IDM_TRANSFNUM 
            Caption         =   "Correc��o de n�meros de requisi��o"
            Index           =   2
         End
         Begin VB.Menu IDM_DOCS 
            Caption         =   "&Documentos"
            Index           =   0
         End
         Begin VB.Menu IDM_DOCS 
            Caption         =   "Observa��es Pendentes"
            Index           =   1
         End
      End
      Begin VB.Menu IDM_FOLHASTRAB 
         Caption         =   "&Folhas de Trabalho "
         Begin VB.Menu IDM_FOLHAS_TRAB 
            Caption         =   "Folhas de Trabalho (An�lises)"
            Index           =   0
         End
         Begin VB.Menu IDM_FOLHAS_TRAB 
            Caption         =   "Folhas de Trabalho (Tubos)"
            Index           =   1
         End
         Begin VB.Menu IDM_FOLHAS_TRAB 
            Caption         =   "Folhas de Trabalho"
            Index           =   2
         End
         Begin VB.Menu IDM_FOLHAS_TRAB 
            Caption         =   "An�lises para Exterior"
            Index           =   3
         End
      End
      Begin VB.Menu IDM_RESULTADOS 
         Caption         =   "&Resultados"
         Begin VB.Menu IDM_RES 
            Caption         =   "&Descontinuado"
            Index           =   0
            Visible         =   0   'False
         End
         Begin VB.Menu IDM_RES 
            Caption         =   "Resultados &Microbiologia"
            Index           =   1
         End
         Begin VB.Menu IDM_RES 
            Caption         =   "Resultados"
            Index           =   3
         End
         Begin VB.Menu IDM_REQPEND_GRUPO 
            Caption         =   "&Automatiza��o de Resultados em S�rie"
            Index           =   1
         End
         Begin VB.Menu IDM_REQPEND_GRUPO 
            Caption         =   "Requisi��es Pendentes Detalhada"
            Index           =   3
         End
         Begin VB.Menu IDM_IMPRES 
            Caption         =   "Im&press�o"
            Begin VB.Menu IDM_IMP_REQ 
               Caption         =   "Por &Requisi��o"
            End
            Begin VB.Menu IDM_IMP_COMPLETAS 
               Caption         =   "Requisi��es &Completas"
            End
            Begin VB.Menu IDM_IMP_PRONTOS 
               Caption         =   "Resultados &Prontos"
            End
            Begin VB.Menu IDM_IMP_JA_LISTADAS 
               Caption         =   "Requisi��es (S�rie)"
               Index           =   0
            End
            Begin VB.Menu IDM_IMP_JA_LISTADAS 
               Caption         =   "Envio Relat�rios por Email"
               Index           =   1
            End
            Begin VB.Menu IDM_IMP_JA_LISTADAS 
               Caption         =   "Resultados para Exterior"
               Index           =   2
            End
         End
         Begin VB.Menu IDM_PROTOCOLOS 
            Caption         =   "Pr&otocolos de Resultados"
            Index           =   0
         End
         Begin VB.Menu IDM_PROTOCOLOS 
            Caption         =   "Elimina��o de Tubos"
            Index           =   1
         End
         Begin VB.Menu IDM_PROTOCOLOS 
            Caption         =   "Bloquear/Desbloquear Requisi��o"
            Index           =   2
         End
         Begin VB.Menu IDM_PROTOCOLOS 
            Caption         =   "Di�rio Microbiologia - Pendentes"
            Index           =   3
         End
      End
      Begin VB.Menu IDM_STOCK 
         Caption         =   "&Stock"
         Begin VB.Menu IDM_STOCK_MENU 
            Caption         =   "Controlo de Stock"
            Index           =   0
         End
         Begin VB.Menu IDM_STOCK_MENU 
            Caption         =   "Movimentos"
            Index           =   1
         End
      End
      Begin VB.Menu IDM_CQ 
         Caption         =   "Controlo de Qualidade"
      End
      Begin VB.Menu IDM_SEROTECA 
         Caption         =   "Arquivo de Amostras"
         Index           =   0
         Begin VB.Menu IDM_SEROTECA_OPT 
            Caption         =   "Gest�o de Suportes"
            Index           =   0
         End
         Begin VB.Menu IDM_SEROTECA_OPT 
            Caption         =   "Altera��o de Estado de Amostras"
            Index           =   1
            Visible         =   0   'False
         End
      End
      Begin VB.Menu IDM_SEROTECA 
         Caption         =   "Vigil�ncia Epidemiol�gica"
         Index           =   1
      End
      Begin VB.Menu IDM_MONITOR 
         Caption         =   "Monitor de Credenciais"
         Index           =   0
         Visible         =   0   'False
      End
   End
   Begin VB.Menu IDM_UTIL 
      Caption         =   "&Utilit�rios"
      Begin VB.Menu IDM_REQ 
         Caption         =   "Requisi��es P&revistas"
         Index           =   0
      End
      Begin VB.Menu IDM_REQ 
         Caption         =   "Requisi��es &Pendentes"
         Index           =   1
         Begin VB.Menu IDM_REQPEND 
            Caption         =   "Por &Resultado"
            Index           =   0
         End
         Begin VB.Menu IDM_REQPEND 
            Caption         =   "Por &Valida��o"
            Index           =   1
         End
         Begin VB.Menu IDM_REQPEND 
            Caption         =   "Por Assinar"
            Index           =   2
         End
         Begin VB.Menu IDM_REQPEND 
            Caption         =   "Assinadas e N�o Impressas"
            Index           =   3
         End
      End
      Begin VB.Menu IDM_REQ 
         Caption         =   "Requisi��es Validadas"
         Index           =   2
      End
      Begin VB.Menu IDM_REQ 
         Caption         =   "Requisi��es por Utilizador"
         Index           =   3
      End
      Begin VB.Menu IDM_REQ 
         Caption         =   "Registo Servico"
         Index           =   4
      End
      Begin VB.Menu IDM_LISTA_RELIMP 
         Caption         =   "Relat�rios &Impressos/Enviados"
      End
      Begin VB.Menu IDM_GESTHIST 
         Caption         =   "Gest�o de &Hist�rico"
         Begin VB.Menu IDM_PASSHIST 
            Caption         =   "Passagem a &Hist�rico"
         End
         Begin VB.Menu IDM_PASSACT 
            Caption         =   "Passagem a &Activo"
         End
      End
      Begin VB.Menu IDM_LISTCODIF 
         Caption         =   "&Listagem de Codifica��es"
         Begin VB.Menu IDM_LISTANA 
            Caption         =   "Lista de &An�lises"
            Index           =   0
         End
         Begin VB.Menu IDM_LISTANA 
            Caption         =   "Lista de &Escal�es/Zonas Refer�ncia"
            Index           =   1
         End
         Begin VB.Menu IDM_LISTANA 
            Caption         =   "Listagens &Gen�ricas"
            Index           =   2
         End
         Begin VB.Menu IDM_LISTANA 
            Caption         =   "Lista Frases Padr�o"
            Index           =   3
         End
      End
      Begin VB.Menu IDM_ESTADOS 
         Caption         =   "Estados"
         Begin VB.Menu IDM_VER_EST 
            Caption         =   "&Verificar estados"
            Index           =   0
         End
         Begin VB.Menu IDM_VER_EST 
            Caption         =   "Estados Requisi��es"
            Index           =   1
         End
      End
      Begin VB.Menu IDM_ETIQ 
         Caption         =   "&Etiquetas"
         Begin VB.Menu IDM_SEP_SOROS 
            Caption         =   "Tubos Prim�rios/&Secund�rios"
            Index           =   0
         End
         Begin VB.Menu IDM_SEP_SOROS 
            Caption         =   "Seroteca"
            Index           =   1
         End
         Begin VB.Menu IDM_SEP_SOROS 
            Caption         =   "Etiquetas Pr�-Impressas"
            Index           =   2
         End
         Begin VB.Menu IDM_SEP_SOROS 
            Caption         =   "Etiquetas Avulso"
            Index           =   3
         End
         Begin VB.Menu IDM_SEP_SOROS 
            Caption         =   "Etiquetas por Tubo"
            Index           =   4
         End
      End
      Begin VB.Menu IDM_PED_NOVA_AMOSTRA 
         Caption         =   "Pedido de Nova &Amostra"
         Index           =   0
      End
      Begin VB.Menu IDM_PED_NOVA_AMOSTRA 
         Caption         =   "Enviar An�lises para Aparelhos"
         Index           =   1
      End
      Begin VB.Menu IDM_PED_NOVA_AMOSTRA 
         Caption         =   "Abrir requisi��es (fechadas/Assinadas)"
         Index           =   2
      End
      Begin VB.Menu IDM_PED_NOVA_AMOSTRA 
         Caption         =   "Interfaces"
         Index           =   3
         Visible         =   0   'False
      End
      Begin VB.Menu IDM_PED_NOVA_AMOSTRA 
         Caption         =   "Simula��o de Requisi��o"
         Index           =   4
      End
      Begin VB.Menu IDM_AGENDA 
         Caption         =   "Consulta de Marca��es"
         Index           =   0
      End
      Begin VB.Menu IDM_AGENDA 
         Caption         =   "Lista Trabalho para Beep"
         Index           =   1
      End
      Begin VB.Menu IDM_AGENDA 
         Caption         =   "Monitoriza��o eResults"
         Index           =   2
      End
      Begin VB.Menu IDM_AGENDA 
         Caption         =   "Alterar Datas Requisi��o"
         Index           =   3
      End
      Begin VB.Menu IDM_AGENDA 
         Caption         =   "C�pia de Resultados"
         Index           =   4
      End
      Begin VB.Menu IDM_AGENDA 
         Caption         =   "Lista de Pedidos de Nova Amostra"
         Index           =   5
      End
      Begin VB.Menu IDM_AGENDA 
         Caption         =   "Lista de Envio de SMS"
         Index           =   6
      End
      Begin VB.Menu IDM_AGENDA 
         Caption         =   "Lista de Entrega de Resultados"
         Index           =   7
      End
      Begin VB.Menu IDM_AGENDA 
         Caption         =   "Lista de Gasimetrias N�o Integradas"
         Index           =   8
      End
   End
   Begin VB.Menu IDM_MENU_FACTURACAO_PR 
      Caption         =   "Fac&tura��o"
      Begin VB.Menu IDM_MENU_FACTURACAO 
         Caption         =   "&Mapear R�bricas"
         Index           =   0
      End
      Begin VB.Menu IDM_MENU_FACTURACAO 
         Caption         =   "&Enviar Dados"
         Index           =   1
      End
      Begin VB.Menu IDM_MENU_FACTURACAO 
         Caption         =   "&Tratamento de Rejeitados"
         Index           =   2
      End
      Begin VB.Menu IDM_MENU_FACTURACAO 
         Caption         =   "Esta&t�stica da Factura��o"
         Index           =   3
      End
      Begin VB.Menu IDM_MENU_FACTURACAO 
         Caption         =   "&Listar Devolvidos"
         Index           =   4
      End
      Begin VB.Menu IDM_MENU_FACTURACAO 
         Caption         =   "&Requisi��es Sem Epis�dio"
         Index           =   5
      End
      Begin VB.Menu IDM_MENU_FACTURACAO 
         Caption         =   "Altera��o Situa��o/Epis�dio"
         Index           =   6
      End
      Begin VB.Menu IDM_MENU_FACTURACAO 
         Caption         =   "Confirma��o de Factura��o"
         Index           =   7
      End
      Begin VB.Menu IDM_MENU_FACTURACAO 
         Caption         =   "Facturac��o Externos"
         Index           =   8
      End
      Begin VB.Menu IDM_MENU_FACTURACAO 
         Caption         =   "Ficheiros"
         Index           =   9
         Begin VB.Menu IDM_FICHEIROS 
            Caption         =   "Eliminar Ficheiros FACTUS"
            Index           =   0
         End
         Begin VB.Menu IDM_FICHEIROS 
            Caption         =   "Editar Ficheiros ADVC Layout"
            Index           =   1
         End
         Begin VB.Menu IDM_FICHEIROS 
            Caption         =   "Editar Lotes ARS"
            Index           =   2
         End
         Begin VB.Menu IDM_FICHEIROS 
            Caption         =   "Editar Ficheiros PT"
            Index           =   3
         End
         Begin VB.Menu IDM_FICHEIROS 
            Caption         =   "Editar Ficheiros ADMG"
            Index           =   4
         End
         Begin VB.Menu IDM_FICHEIROS 
            Caption         =   "Editar Ficheiros ADVC Extrato"
            Index           =   5
         End
      End
      Begin VB.Menu IDM_MENU_FACTURACAO 
         Caption         =   "Envio Dados em S�rie"
         Index           =   10
      End
      Begin VB.Menu IDM_MENU_FACTURACAO 
         Caption         =   "Portarias"
         Index           =   11
      End
      Begin VB.Menu IDM_MENU_FACTURACAO_LISTAGENS 
         Caption         =   "Listagem de Caixa"
         Index           =   0
      End
      Begin VB.Menu IDM_MENU_FACTURACAO_LISTAGENS 
         Caption         =   "Listagem de Factura��o (FACTUS)"
         Index           =   1
      End
      Begin VB.Menu IDM_MENU_FACTURACAO_LISTAGENS 
         Caption         =   "Listagem de Produ��o"
         Index           =   2
      End
      Begin VB.Menu IDM_MENU_FACTURACAO_LISTAGENS 
         Caption         =   "Digitaliza��o de Credenciais"
         Index           =   3
      End
      Begin VB.Menu IDM_MENU_FACTURACAO_LISTAGENS 
         Caption         =   "Emiss�o de Cartas de D�vida"
         Index           =   4
      End
      Begin VB.Menu IDM_MENU_FACTURACAO_LISTAGENS 
         Caption         =   "Guias de Facturas"
         Index           =   5
      End
      Begin VB.Menu IDM_MENU_FACTURACAO_LISTAGENS 
         Caption         =   "Refer�ncias Multibanco"
         Index           =   6
         Begin VB.Menu IDM_REF_MB 
            Caption         =   "Emiss�o de Refer�ncias Multibanco"
            Index           =   0
         End
         Begin VB.Menu IDM_REF_MB 
            Caption         =   "Ficheiro de Movimentos"
            Index           =   1
         End
         Begin VB.Menu IDM_REF_MB 
            Caption         =   "Listagem Refer�ncias"
            Index           =   2
         End
      End
   End
   Begin VB.Menu IDM_MENU_ESTATISTICAS_PR 
      Caption         =   "&Estat�sticas"
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Requisi��o"
         Index           =   0
         Begin VB.Menu IDM_ESTAT_REQ 
            Caption         =   "Contagem An�lises"
            Index           =   0
         End
         Begin VB.Menu IDM_ESTAT_REQ 
            Caption         =   "Contagem An�lises Di�rias"
            Index           =   1
         End
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Servi�o"
         Index           =   1
         Visible         =   0   'False
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Grupo"
         Index           =   2
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Servi�o e Grupo"
         Index           =   3
         Visible         =   0   'False
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Produto"
         Index           =   4
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Grupos de Diagn�stico"
         Index           =   5
         Visible         =   0   'False
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Tempo Realiza��o de An�lise"
         Index           =   6
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Resultado"
         Index           =   7
         Begin VB.Menu IDM_ESTAT_RES 
            Caption         =   "&Estat�stica"
            Index           =   0
         End
         Begin VB.Menu IDM_ESTAT_RES 
            Caption         =   "&Listagem"
            Index           =   1
         End
         Begin VB.Menu IDM_ESTAT_RES 
            Caption         =   "&An�lises - Descontinuado"
            Index           =   2
            Visible         =   0   'False
         End
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Microbiologia"
         Index           =   8
         Begin VB.Menu IDM_ESTAT_MICRO 
            Caption         =   "Exames Microbiol�gicos"
            Index           =   0
         End
         Begin VB.Menu IDM_ESTAT_MICRO 
            Caption         =   "Microrganismos"
            Index           =   1
         End
         Begin VB.Menu IDM_ESTAT_MICRO 
            Caption         =   "Resultados"
            Index           =   2
         End
         Begin VB.Menu IDM_ESTAT_MICRO 
            Caption         =   "Resist�ncias"
            Index           =   3
            Visible         =   0   'False
         End
         Begin VB.Menu IDM_ESTAT_MICRO 
            Caption         =   "Resultados - An�lise"
            Index           =   4
         End
         Begin VB.Menu IDM_ESTAT_MICRO 
            Caption         =   "Positivos"
            Index           =   5
         End
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "SONHO"
         Index           =   9
         Visible         =   0   'False
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Contagem de An�lises"
         Index           =   10
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Estat�stica de Colheitas"
         Index           =   11
         Visible         =   0   'False
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Tempos de Valida��o"
         Index           =   12
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "&Estat�stica de tempos m�dios - Colheita/Valida��o"
         Index           =   13
      End
      Begin VB.Menu IDM_MENU_ESTATISTICAS 
         Caption         =   "Resultados An�lises"
         Index           =   14
      End
   End
   Begin VB.Menu IDM_MENU_CODIFICACOES 
      Caption         =   "&Codifica��es"
      Begin VB.Menu IDM_ANALISES 
         Caption         =   "&An�lises"
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "&Simples"
            Index           =   0
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "&Complexas"
            Index           =   1
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "&Exames"
            Index           =   2
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "&Perfis de Marca��o"
            Index           =   3
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "&Ordena��o de An�lises"
            Index           =   4
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "&Grupos"
            Index           =   5
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "S&ubGrupos"
            Index           =   6
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "Classes"
            Index           =   7
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "&Frases padr�o "
            Index           =   8
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "&Interfer�ncias"
            Index           =   9
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "Grupos de Exames"
            Index           =   10
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "�reas de Trabalho"
            Index           =   11
            Visible         =   0   'False
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "Grupos de Impress�o"
            Index           =   12
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "Informa��es para An�lises"
            Index           =   13
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "Tempos m�ximos"
            Index           =   14
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "Tipos de Relat�rio"
            Index           =   15
         End
         Begin VB.Menu IDM_ANALISES_T 
            Caption         =   "Resultados Autom�ticos"
            Index           =   16
         End
      End
      Begin VB.Menu IDM_SEPARA9_1 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_COLH 
         Caption         =   "Co&lheitas"
         Begin VB.Menu IDM_COLHEITAS 
            Caption         =   "&Salas de Colheita"
            Index           =   0
         End
         Begin VB.Menu IDM_COLHEITAS 
            Caption         =   "Prioridades"
            Index           =   1
         End
         Begin VB.Menu IDM_COLHEITAS 
            Caption         =   "-"
            Index           =   2
         End
         Begin VB.Menu IDM_COLHEITAS 
            Caption         =   "Agenda - Dias Indispon�veis"
            Index           =   3
         End
      End
      Begin VB.Menu IDM_SEPARA9 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_SUB_AUTORES 
         Caption         =   "A&utomatiza��o de Resultados"
         Begin VB.Menu IDM_SUB_AUTORES_OPT 
            Caption         =   "&Codifica��o"
            Index           =   0
         End
         Begin VB.Menu IDM_SUB_AUTORES_OPT 
            Caption         =   "&Atribui��o de Resultados "
            Index           =   1
         End
      End
      Begin VB.Menu IDM_SEPARA10 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_VAL_AUTO 
         Caption         =   "Auxiliar de &Valida��o"
         Begin VB.Menu IDM_CODIFICACAO 
            Caption         =   "&Codifica��o"
         End
         Begin VB.Menu IDM_DELTA_CHECK 
            Caption         =   "&Delta Check"
         End
         Begin VB.Menu IDM_INC_RESULTADOS 
            Caption         =   "&Incompatibilidade de Resultados"
         End
      End
      Begin VB.Menu IDM_SEPARA12 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_TIPOS_TUBOS 
         Caption         =   "&Tipos de Tubos"
         Begin VB.Menu IDM_TUBOS 
            Caption         =   "Prim�rios"
         End
         Begin VB.Menu IDM_TUBOS_SEC 
            Caption         =   "Secund�rios"
         End
      End
      Begin VB.Menu IDM_SUB_PRODUTOS 
         Caption         =   "&Produtos"
         Begin VB.Menu IDM_PRODUTOS 
            Caption         =   "&Codifica��o"
         End
         Begin VB.Menu IDM_ESPECIF 
            Caption         =   "&Especifica��o"
         End
      End
      Begin VB.Menu IDM_REAGENTES 
         Caption         =   "&Reagentes"
      End
      Begin VB.Menu IDM_METODOS 
         Caption         =   "&M�todos"
      End
      Begin VB.Menu IDM_SEPARA 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_INF_CLI 
         Caption         =   "&Informa��o Cl�nica"
         Begin VB.Menu IDM_DIAG 
            Caption         =   "&Diagn�sticos"
         End
         Begin VB.Menu IDM_GR_DIAG 
            Caption         =   "&Grupos de Diagn�stico"
         End
         Begin VB.Menu IDM_TERA_MED 
            Caption         =   "&Terap�uticas e Medica��o"
         End
         Begin VB.Menu IDM_OR_ZONAS 
            Caption         =   "&Outras Raz�es (Zonas Refer�ncia)"
         End
         Begin VB.Menu IDM_MOTIVOS_CLI 
            Caption         =   "&Motivos Cl�nicos"
         End
      End
      Begin VB.Menu IDM_FRASE 
         Caption         =   "Dicion�rio de &Frases"
         Index           =   0
      End
      Begin VB.Menu IDM_FRASE 
         Caption         =   "Flags Aparelhos"
         Index           =   1
      End
      Begin VB.Menu IDM_SEPARA5 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_SUB_MICRO 
         Caption         =   "Mi&crorganismos"
         Begin VB.Menu IDM_MICRO 
            Caption         =   "&Codifica��o"
            Index           =   0
         End
         Begin VB.Menu IDM_MICRO 
            Caption         =   "&Grupos de Micr&organismos"
            Index           =   1
         End
         Begin VB.Menu IDM_MICRO 
            Caption         =   "&Identifica��o de Tipo Prova"
            Index           =   2
         End
         Begin VB.Menu IDM_MICRO 
            Caption         =   "Codifica��o de &Provas"
            Index           =   3
         End
         Begin VB.Menu IDM_MICRO 
            Caption         =   "Frases para Relat�rio"
            Index           =   4
         End
      End
      Begin VB.Menu IDM_SUB_ANTIBIO 
         Caption         =   "Anti&bioticos"
         Begin VB.Menu IDM_ANTIBIO 
            Caption         =   "&Codifica��o"
         End
         Begin VB.Menu IDM_CONC_ANTIBIO 
            Caption         =   "C&oncentra��es"
         End
         Begin VB.Menu IDM_GR_ANTIBIO 
            Caption         =   "&Cartas de Antibi�ticos"
         End
         Begin VB.Menu IDM_REL_GR_ANTIBIO 
            Caption         =   "&Atribui��o de Antibi�ticos aos Grupos "
            Index           =   0
         End
         Begin VB.Menu IDM_REL_GR_ANTIBIO 
            Caption         =   "Classes de Antibi�ticos"
            Index           =   1
         End
         Begin VB.Menu IDM_REL_GR_ANTIBIO 
            Caption         =   "Sub Classes/Categorias de Antibi�ticos"
            Index           =   2
         End
      End
      Begin VB.Menu IDM_SEPARA6 
         Caption         =   "-"
         Index           =   0
      End
      Begin VB.Menu IDM_SUB_PROVEN 
         Caption         =   "Prove&ni�ncias "
         Begin VB.Menu IDM_PROVEN 
            Caption         =   "&Codifica��o"
            Index           =   0
         End
         Begin VB.Menu IDM_PROVEN 
            Caption         =   "C&ontactos"
            Index           =   1
         End
         Begin VB.Menu IDM_SEPUTILPROV 
            Caption         =   "-"
         End
         Begin VB.Menu IDM_UTIL_PROVEN 
            Caption         =   "&Utilizadores\Proveni�ncias"
         End
      End
      Begin VB.Menu IDM_SEPARA8 
         Caption         =   "-"
         Index           =   0
      End
      Begin VB.Menu IDM_SUB_GR_TRAB 
         Caption         =   "&Grupos de Trabalho"
         Begin VB.Menu IDM_GR_TRABALHO 
            Caption         =   "&Codifica��o"
         End
         Begin VB.Menu IDM_ANALISES_GR_TRAB 
            Caption         =   "&Atribui��o de An�lises aos Grupos de Trabalho"
         End
         Begin VB.Menu IDM_MODELOS_LIVRES 
            Caption         =   "&Modelos Livres"
         End
      End
      Begin VB.Menu IDM_SEPARA3 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_CANCELAMENTOS 
         Caption         =   "Cance&lamentos"
      End
      Begin VB.Menu IDM_SEPARA4 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_SUB_SECRET 
         Caption         =   "&Secretaria"
         Begin VB.Menu IDM_POSTAIS 
            Caption         =   "&C�digos Postais"
         End
         Begin VB.Menu IDM_PAISES 
            Caption         =   "&Pa�ses"
         End
         Begin VB.Menu IDM_PROFISSAO 
            Caption         =   "P&rofiss�es"
         End
         Begin VB.Menu SEP_06_16_05 
            Caption         =   "-"
         End
         Begin VB.Menu IDM_ISENCAO 
            Caption         =   "&Tipos de Isen��o"
         End
         Begin VB.Menu SEP_06_16_08 
            Caption         =   "-"
         End
         Begin VB.Menu IDM_MEDICOS 
            Caption         =   "&M�dicos"
         End
         Begin VB.Menu IDM_ARQUIVOS 
            Caption         =   "Ar&quivos"
         End
         Begin VB.Menu IDM_RESPONSAVEIS 
            Caption         =   "&Respons�veis"
         End
         Begin VB.Menu IDM_TIPOANULREC 
            Caption         =   "&Tipo Anula��o Recibos"
            Index           =   0
         End
         Begin VB.Menu IDM_TIPOANULREC 
            Caption         =   "Entidades Financeiras"
            Index           =   1
         End
         Begin VB.Menu IDM_TIPOANULREC 
            Caption         =   "Esp�cies"
            Index           =   2
         End
         Begin VB.Menu IDM_TIPOANULREC 
            Caption         =   "Ra�as"
            Index           =   3
         End
         Begin VB.Menu IDM_TIPOANULREC 
            Caption         =   "Locais de Envio de Resultados"
            Index           =   4
         End
      End
      Begin VB.Menu IDM_CONTROLO_QUALIDADE 
         Caption         =   "Controlo Qualidade"
         Begin VB.Menu IDM_CONTROLO_QUALIDADE_IDX 
            Caption         =   "Lotes"
            Index           =   0
            Visible         =   0   'False
         End
         Begin VB.Menu IDM_CONTROLO_QUALIDADE_IDX 
            Caption         =   "Controlos"
            Index           =   1
         End
         Begin VB.Menu IDM_CONTROLO_QUALIDADE_IDX 
            Caption         =   "Lotes"
            Index           =   2
         End
         Begin VB.Menu IDM_CONTROLO_QUALIDADE_IDX 
            Caption         =   "An�lises"
            Index           =   3
         End
      End
      Begin VB.Menu IDM_CODIFIC 
         Caption         =   "Empresas"
         Index           =   0
      End
      Begin VB.Menu IDM_CODIFIC 
         Caption         =   "Entidades Externas"
         Index           =   1
      End
      Begin VB.Menu IDM_CODIFIC 
         Caption         =   "Utilizadores Externos"
         Index           =   2
      End
      Begin VB.Menu IDM_MENU_CODIFICACOES_SEP 
         Caption         =   "-"
      End
      Begin VB.Menu IDM_MENU_CODIFICACOES_STK 
         Caption         =   "&Stocks"
         Begin VB.Menu IDM_MENU_CODIFICACOES_STK_OPT 
            Caption         =   "Invent�rio de Stocks"
            Index           =   0
         End
         Begin VB.Menu IDM_MENU_CODIFICACOES_STK_OPT 
            Caption         =   "Fornecedores"
            Index           =   1
         End
         Begin VB.Menu IDM_MENU_CODIFICACOES_STK_OPT 
            Caption         =   "Tipos de Artigo"
            Index           =   2
         End
         Begin VB.Menu IDM_MENU_CODIFICACOES_STK_OPT 
            Caption         =   "Tipos de Movimento"
            Index           =   3
         End
         Begin VB.Menu IDM_MENU_CODIFICACOES_STK_OPT 
            Caption         =   "Artigos"
            Index           =   4
         End
         Begin VB.Menu IDM_MENU_CODIFICACOES_STK_OPT 
            Caption         =   "Lotes"
            Index           =   5
         End
      End
      Begin VB.Menu IDM_CODIFICAO_SEROTECA 
         Caption         =   "Arquivo de Amostras"
         Index           =   0
         Begin VB.Menu IDM_CODIFICAO_SEROTECA_OPT 
            Caption         =   "Arquivos"
            Index           =   0
         End
         Begin VB.Menu IDM_CODIFICAO_SEROTECA_OPT 
            Caption         =   "Suportes"
            Index           =   1
         End
      End
      Begin VB.Menu IDM_CODIFICAO_SEROTECA 
         Caption         =   "Di�rio"
         Index           =   1
         Begin VB.Menu IDM_DIARIO 
            Caption         =   "Codifica��o de Actividades"
            Index           =   0
         End
         Begin VB.Menu IDM_DIARIO 
            Caption         =   "Codifica��o de Detalhes de Actividades"
            Index           =   1
         End
      End
      Begin VB.Menu IDM_CODIFICAO_SEROTECA 
         Caption         =   "�guas"
         Index           =   2
         Begin VB.Menu IDM_AGUAS 
            Caption         =   "Tipo de Amostra"
            Index           =   0
         End
         Begin VB.Menu IDM_AGUAS 
            Caption         =   "Origem da Amostra"
            Index           =   1
         End
         Begin VB.Menu IDM_AGUAS 
            Caption         =   "Ponto de Colheita"
            Index           =   2
         End
      End
      Begin VB.Menu IDM_CODIFICAO_SEROTECA 
         Caption         =   "Vigil�ncia Epidemil�gica"
         Index           =   3
         Begin VB.Menu IDM_VIGIL_EPID 
            Caption         =   "Grupos Epidemiol�gicos"
            Index           =   0
         End
         Begin VB.Menu IDM_VIGIL_EPID 
            Caption         =   "Regras Vigil�ncia Epidemiol�gica"
            Index           =   1
         End
      End
      Begin VB.Menu IDM_MAP_TAB_EXT 
         Caption         =   "Mapeamentos � Tabelas externas"
         Index           =   4
      End
   End
   Begin VB.Menu IDM_MENU_FERRAMENTAS_PR 
      Caption         =   "Ferramenta&s"
      Begin VB.Menu IDM_MENU_FERRAMENTAS 
         Caption         =   "&Gescom"
         Index           =   0
      End
      Begin VB.Menu IDM_MENU_FERRAMENTAS 
         Caption         =   "-"
         Index           =   1
      End
      Begin VB.Menu IDM_MENU_FERRAMENTAS 
         Caption         =   "&Sistema de Interroga��o"
         Index           =   2
      End
      Begin VB.Menu IDM_MENU_FERRAMENTAS 
         Caption         =   "-"
         Index           =   3
      End
      Begin VB.Menu IDM_MENU_FERRAMENTAS 
         Caption         =   "&Calculadora"
         Index           =   4
      End
      Begin VB.Menu IDM_MENU_FERRAMENTAS 
         Caption         =   "&Editor"
         Index           =   5
      End
      Begin VB.Menu IDM_MENU_FERRAMENTAS 
         Caption         =   "Correio"
         Index           =   6
         Visible         =   0   'False
         Begin VB.Menu IDM_CORREIO_MARCAR 
            Caption         =   "Marcar Mensagem(s)"
            Index           =   0
            Begin VB.Menu IDM_CORREIO_MARCAR_OPT 
               Caption         =   "Como lida(s)"
               Index           =   0
            End
            Begin VB.Menu IDM_CORREIO_MARCAR_OPT 
               Caption         =   "Como n�o lida(s)"
               Index           =   1
            End
         End
         Begin VB.Menu IDM_CORREIO_MARCAR 
            Caption         =   "-"
            Index           =   1
         End
         Begin VB.Menu IDM_CORREIO_MARCAR 
            Caption         =   "Arquivar mensagem(s)"
            Index           =   2
         End
         Begin VB.Menu IDM_CORREIO_MARCAR 
            Caption         =   "-"
            Index           =   3
         End
         Begin VB.Menu IDM_CORREIO_MARCAR 
            Caption         =   "Eliminar mensagem(s)"
            Index           =   4
         End
         Begin VB.Menu IDM_CORREIO_MARCAR 
            Caption         =   "-"
            Index           =   5
         End
         Begin VB.Menu IDM_CORREIO_MARCAR 
            Caption         =   "Seleccionar Todos"
            Index           =   6
         End
      End
      Begin VB.Menu IDM_MENU_FERRAMENTAS 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu IDM_MENU_FERRAMENTAS 
         Caption         =   "Editor SQL"
         Index           =   8
      End
   End
   Begin VB.Menu IDM_MENU_AJUDA_PR 
      Caption         =   "&Ajuda"
      Begin VB.Menu IDM_MENU_AJUDA 
         Caption         =   "�nd&ice"
         Index           =   0
      End
      Begin VB.Menu IDM_MENU_AJUDA 
         Caption         =   "-"
         Index           =   1
      End
      Begin VB.Menu IDM_MENU_AJUDA 
         Caption         =   "&Acerca de"
         Index           =   2
      End
   End
   Begin VB.Menu HIDE_ANAMARC 
      Caption         =   "Ana"
      Visible         =   0   'False
      Begin VB.Menu HIDE_ANA_MARC 
         Caption         =   "&An�lises"
         Index           =   0
      End
      Begin VB.Menu HIDE_ANA_MARC 
         Caption         =   "&Perfis de Marca��o"
         Index           =   1
      End
      Begin VB.Menu HIDE_ANA_MARC 
         Caption         =   "Top An�lises"
         Index           =   2
      End
   End
   Begin VB.Menu HIDE_ANAREQ 
      Caption         =   "AnaReq"
      Visible         =   0   'False
      Begin VB.Menu HIDE_ANAREQS 
         Caption         =   "An�lises &Simples"
      End
      Begin VB.Menu HIDE_ANAREQC 
         Caption         =   "Analises &Complexas"
      End
      Begin VB.Menu HIDE_ANAREQP 
         Caption         =   "&Perfis"
      End
   End
   Begin VB.Menu HIDE_POPUP 
      Caption         =   "&PopUp"
      Visible         =   0   'False
      Begin VB.Menu MenuList 
         Caption         =   "MenuList"
         Index           =   0
      End
   End
   Begin VB.Menu HIDE_FERRAMENTAS 
      Caption         =   "&Ferramentas"
      Visible         =   0   'False
      Begin VB.Menu HIDE_RESOPERADOR 
         Caption         =   "&Operador"
         Index           =   0
         Visible         =   0   'False
      End
      Begin VB.Menu HIDE_RESOPERADOR 
         Caption         =   "&Inserir Simples"
         Index           =   1
      End
      Begin VB.Menu HIDE_RESOPERADOR 
         Caption         =   "&Colocar 'Sem Resultado'"
         Index           =   2
         Visible         =   0   'False
      End
      Begin VB.Menu HIDE_RESOPERADOR 
         Caption         =   "&Relat�rio do Auxiliar de Valida��o"
         Enabled         =   0   'False
         Index           =   3
         Visible         =   0   'False
      End
      Begin VB.Menu HIDE_RESOPERADOR 
         Caption         =   "Inserir todos os &Membros"
         Index           =   4
      End
   End
   Begin VB.Menu HIDE_RESCOMENTARIOS 
      Caption         =   "&ResComentarios"
      Visible         =   0   'False
      Begin VB.Menu HIDE_RESOBSANA 
         Caption         =   "&Observa��es da An�lise"
         Index           =   0
      End
      Begin VB.Menu HIDE_RESOBSANA 
         Caption         =   "Coment�rio &T�cnico"
         Index           =   1
      End
      Begin VB.Menu HIDE_RESOBSANA 
         Caption         =   "Coment�rio &Final"
         Index           =   2
      End
   End
   Begin VB.Menu HIDE_MENU_INFOANA 
      Caption         =   "&CaixaInfoAna"
      Visible         =   0   'False
      Begin VB.Menu HIDE_INFOANA 
         Caption         =   "&Minimizar Info"
         Index           =   0
      End
      Begin VB.Menu HIDE_INFOANA 
         Caption         =   "E&xpandir Info"
         Index           =   1
      End
      Begin VB.Menu HIDE_INFOANA 
         Caption         =   "&Fechar Info"
         Index           =   2
      End
   End
   Begin VB.Menu HIDE_OBJ 
      Caption         =   "Menu Objectos"
      Visible         =   0   'False
      Begin VB.Menu HIDE_OBJ_OPT 
         Caption         =   "&Expandir Arvore "
         Index           =   0
      End
      Begin VB.Menu HIDE_OBJ_OPT 
         Caption         =   "&Comprimir �rvore "
         Index           =   1
      End
      Begin VB.Menu HIDE_OBJ_OPT 
         Caption         =   "&Apagar Parametriza��es Grupo/Utilizador "
         Index           =   2
      End
      Begin VB.Menu HIDE_OBJ_OPT 
         Caption         =   "A&pagar Parametriza��es Contentor "
         Index           =   3
      End
      Begin VB.Menu HIDE_OBJ_OPT 
         Caption         =   "Apagar Parametriza��es &Objecto "
         Index           =   4
      End
   End
   Begin VB.Menu HIDE_MENU_REQUIS 
      Caption         =   "Menu Requisi��es"
      Visible         =   0   'False
      Begin VB.Menu IDM_REQUIS 
         Caption         =   "Marca��o de Colheitas"
         Index           =   0
      End
      Begin VB.Menu IDM_REQUIS 
         Caption         =   "Gest�o de Requisi��es"
         Index           =   1
      End
   End
   Begin VB.Menu HIDE_PROP 
      Caption         =   "Menu Propriedades "
      Visible         =   0   'False
      Begin VB.Menu HIDE_APAGA_PROP 
         Caption         =   "&Apagar Propriedades "
      End
   End
   Begin VB.Menu HIDE_RES 
      Caption         =   "Resultados"
      Visible         =   0   'False
      Begin VB.Menu IDM_RESU 
         Caption         =   "Repeti��es"
         Index           =   0
      End
      Begin VB.Menu IDM_RESU 
         Caption         =   "Interfer�ncias"
         Index           =   1
      End
      Begin VB.Menu IDM_RESU 
         Caption         =   "Volume"
         Index           =   2
      End
      Begin VB.Menu IDM_RESU 
         Caption         =   "Operador"
         Index           =   3
      End
      Begin VB.Menu IDM_RESU 
         Caption         =   "Observa��o da An�lise"
         Index           =   4
      End
      Begin VB.Menu IDM_RESU 
         Caption         =   "Colocar 'Sem Resultado'"
         Index           =   5
      End
      Begin VB.Menu IDM_RESU 
         Caption         =   "Consultar Gr�fico"
         Index           =   6
      End
   End
   Begin VB.Menu HIDE_ANA 
      Caption         =   "An�lises"
      Visible         =   0   'False
      Begin VB.Menu IDM_ANA 
         Caption         =   "Acrescentar An�lise"
         Index           =   0
      End
      Begin VB.Menu IDM_ANA 
         Caption         =   "Eliminar Linha"
         Index           =   1
      End
   End
   Begin VB.Menu HIDE_RES_NEW 
      Caption         =   "Resultados"
      Visible         =   0   'False
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "An�lise"
         Index           =   0
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "Valida��o"
         Index           =   1
         Begin VB.Menu IDM_RESU_NEW_VAL 
            Caption         =   "N�o Validar An�lise"
            Index           =   0
         End
         Begin VB.Menu IDM_RESU_NEW_VAL 
            Caption         =   "N�o Validar At� Esta An�lise"
            Index           =   1
         End
         Begin VB.Menu IDM_RESU_NEW_VAL 
            Caption         =   "Validar At� Esta An�lise"
            Index           =   2
         End
         Begin VB.Menu IDM_RESU_NEW_VAL 
            Caption         =   "Validar Apenas Esta An�lise"
            Index           =   3
         End
         Begin VB.Menu IDM_RESU_NEW_VAL 
            Caption         =   "Retirar Restri��es"
            Index           =   4
         End
         Begin VB.Menu IDM_RESU_NEW_VAL 
            Caption         =   "Colocar An�lise Pendente"
            Index           =   5
         End
         Begin VB.Menu IDM_RESU_NEW_VAL 
            Caption         =   "Bloquear An�lise"
            Index           =   6
         End
         Begin VB.Menu IDM_RESU_NEW_VAL 
            Caption         =   "Desbloquear An�lise"
            Index           =   7
         End
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "Repeti��es"
         Index           =   2
         Begin VB.Menu IDM_RESU_NEW_REP 
            Caption         =   "Repetir An�lise"
            Index           =   0
         End
         Begin VB.Menu IDM_RESU_NEW_REP 
            Caption         =   "Consultar Repeti��es"
            Index           =   1
         End
         Begin VB.Menu IDM_RESU_NEW_REP 
            Caption         =   "Todas as Repeti��es"
            Index           =   2
         End
         Begin VB.Menu IDM_RESU_NEW_REP 
            Caption         =   "Repetir At� Aqui"
            Index           =   3
         End
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "Observa��es"
         Index           =   3
         Begin VB.Menu IDM_RESU_NEW_OBS 
            Caption         =   "Observa��es do Resultado"
            Index           =   0
         End
         Begin VB.Menu IDM_RESU_NEW_OBS 
            Caption         =   "Observa��es do Registo"
            Index           =   1
         End
         Begin VB.Menu IDM_RESU_NEW_OBS 
            Caption         =   "Enviar Mensagem"
            Index           =   2
         End
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "Operador"
         Index           =   4
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "Resultados"
         Index           =   5
         Begin VB.Menu IDM_RESU_NEW_RES 
            Caption         =   "Colocar 'Sem Resultado'"
            Index           =   0
         End
         Begin VB.Menu IDM_RESU_NEW_RES 
            Caption         =   "Colocar 'Sem Resultado' todas An�lises"
            Index           =   1
         End
         Begin VB.Menu IDM_RESU_NEW_RES 
            Caption         =   "Colocar Valor por Defeito"
            Index           =   2
         End
         Begin VB.Menu IDM_RESU_NEW_RES 
            Caption         =   "Alternar Resultados Anteriores"
            Index           =   3
         End
         Begin VB.Menu IDM_RESU_NEW_RES 
            Caption         =   "N�o Imprimir  Resultados"
            Checked         =   -1  'True
            Index           =   4
         End
         Begin VB.Menu IDM_RESU_NEW_RES 
            Caption         =   "N�o Imprimir 2� Resultado"
            Checked         =   -1  'True
            Index           =   5
         End
         Begin VB.Menu IDM_RESU_NEW_RES 
            Caption         =   "Mostrar Antibiograma"
            Index           =   6
         End
         Begin VB.Menu IDM_RESU_NEW_RES 
            Caption         =   "Altera��es Resultado"
            Index           =   7
         End
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "Volume, Altura e Peso "
         Index           =   6
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "Gr�fico"
         Index           =   7
         Begin VB.Menu IDM_RESU_GRAF 
            Caption         =   "Consultar Gr�fico"
            Index           =   0
         End
         Begin VB.Menu IDM_RESU_GRAF 
            Caption         =   "Novo Gr�fico"
            Index           =   1
         End
         Begin VB.Menu IDM_RESU_GRAF 
            Caption         =   "Apagar Gr�fico(s)"
            Index           =   2
         End
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "Hist�rico da An�lise"
         Index           =   8
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "Adicionar An�lise"
         Index           =   9
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "An�lise 1"
         Index           =   10
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "An�lise2"
         Index           =   11
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "Anexos"
         Index           =   12
         Begin VB.Menu IDM_RESU_New_ANEXO 
            Caption         =   "Anexar Documento"
            Index           =   0
         End
         Begin VB.Menu IDM_RESU_New_ANEXO 
            Caption         =   "Apagar Documento"
            Index           =   1
         End
         Begin VB.Menu IDM_RESU_New_ANEXO 
            Caption         =   "Abrir Documento"
            Index           =   2
         End
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "Di�rio"
         Index           =   13
      End
      Begin VB.Menu IDM_RESU_New 
         Caption         =   "Vigil�ncia Epid."
         Index           =   14
         Begin VB.Menu IDM_RESU_New_VE 
            Caption         =   "Associar Manualmente"
            Index           =   0
         End
         Begin VB.Menu IDM_RESU_New_VE 
            Caption         =   "Retirar Associa��o"
            Index           =   1
         End
         Begin VB.Menu IDM_RESU_New_VE 
            Caption         =   "Consultar Vig. Epid"
            Index           =   2
         End
      End
   End
   Begin VB.Menu HIDE_TUBOS 
      Caption         =   "Tubos"
      Visible         =   0   'False
      Begin VB.Menu IDM_TUBO 
         Caption         =   "Desactiva Chegada do Tubo"
         Index           =   0
      End
      Begin VB.Menu IDM_TUBO 
         Caption         =   "Pedir Nova Amostra"
         Index           =   1
      End
      Begin VB.Menu IDM_TUBO 
         Caption         =   "Desactiva Colheita do Tubo"
         Index           =   2
      End
      Begin VB.Menu IDM_TUBO 
         Caption         =   "Escolher Tubo"
         Index           =   3
      End
   End
   Begin VB.Menu HIDE_FACTANA 
      Caption         =   "FactAna"
      Visible         =   0   'False
      Begin VB.Menu IDM_FACT_ANA 
         Caption         =   "Reactivar an�lise"
         Index           =   0
      End
   End
   Begin VB.Menu HIDE_REC 
      Caption         =   "TipoDocRecibos"
      Visible         =   0   'False
      Begin VB.Menu IDM_NEW_REC_DOC 
         Caption         =   "Recibo"
         Index           =   0
      End
      Begin VB.Menu IDM_NEW_REC_DOC 
         Caption         =   "Documento Caixa"
         Index           =   1
      End
   End
   Begin VB.Menu HIDE_IMPR_REC 
      Caption         =   "Tipo Impressao Recibo"
      Visible         =   0   'False
      Begin VB.Menu IDM_IMPR_RECIBO 
         Caption         =   "Original"
         Index           =   0
      End
      Begin VB.Menu IDM_IMPR_RECIBO 
         Caption         =   "C�pia"
         Index           =   1
      End
      Begin VB.Menu IDM_IMPR_RECIBO 
         Caption         =   "Segunda Via"
         Index           =   2
      End
   End
   Begin VB.Menu IDM_SEROTECA_HIDE 
      Caption         =   "Seroteca"
      Visible         =   0   'False
      Begin VB.Menu IDM_SEROTECA_HIDE_OPT 
         Caption         =   "Congelar Amostra"
         Index           =   0
      End
      Begin VB.Menu IDM_SEROTECA_HIDE_OPT 
         Caption         =   "Descongelar Amostra"
         Index           =   1
      End
      Begin VB.Menu IDM_SEROTECA_HIDE_OPT 
         Caption         =   "Enviar Para Lixo"
         Index           =   2
      End
   End
   Begin VB.Menu IDM_ERESULTS_HIDE 
      Caption         =   "eResults"
      Visible         =   0   'False
      Begin VB.Menu IDM_ERESULTS 
         Caption         =   "Apagar Registo"
         Index           =   0
      End
      Begin VB.Menu IDM_ERESULTS 
         Caption         =   "Enviar Registo"
         Index           =   1
      End
   End
   Begin VB.Menu IDM_CQ_HIDE 
      Caption         =   "Controlo Qualidade"
      Visible         =   0   'False
      Begin VB.Menu IDM_CQ2 
         Caption         =   "Dados Gerais"
         Index           =   0
      End
      Begin VB.Menu IDM_CQ2 
         Caption         =   "Gr�fico"
         Index           =   1
      End
   End
   Begin VB.Menu IDM_RR_HIDE 
      Caption         =   "ResReq"
      Visible         =   0   'False
      Begin VB.Menu IDM_RR 
         Caption         =   "Tubos Associados"
         Index           =   0
      End
   End
   Begin VB.Menu HIDE_CQ_RES 
      Caption         =   "CQ_RES"
      Visible         =   0   'False
      Begin VB.Menu IDM_FGRES 
         Caption         =   "Rejeitar Controlo"
         Index           =   0
      End
      Begin VB.Menu IDM_FGRES 
         Caption         =   "Aceitar Controlo"
         Index           =   1
      End
      Begin VB.Menu IDM_FGRES 
         Caption         =   "Excluir Controlo"
         Index           =   2
      End
      Begin VB.Menu IDM_FGRES 
         Caption         =   "Incluir Controlo"
         Index           =   3
      End
   End
   Begin VB.Menu HIDE_LOCAIS 
      Caption         =   "Locais"
      Visible         =   0   'False
      Begin VB.Menu IDM_LOCAL 
         Caption         =   "."
         Index           =   0
      End
   End
End
Attribute VB_Name = "MDIFormInicio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'********************************************************************
'*
'*      Vers�o Sources      : 19
'*      �ltima Actualiza��o : 23/07/2003
'*
'********************************************************************

' Actualiza��o : 06/03/2003
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Option Explicit   ' Para obrigar a definir as vari�veis.
' pferreira 2010.05.27
Public WithEvents f_cSystray As cSystray
Attribute f_cSystray.VB_VarHelpID = -1

'edgar.parada BLACKLOG-11940 (Colheitas) 24.01.2019
Dim Estado As estado_alertas
'Dim estrutAlertas As alertas
Dim contador_timer_alertas As Long
'
' pferreira 2010.09.28
Private Sub f_cSystray_BalloonClick()

    If (gMensagensNovas And f_cSystray.BalloonText = cTextoNovasMensagens) Then: Call bl_AbreModuloCorreio(gVersaoModuloCorreio)

End Sub

Private Sub HIDE_ANA_MARC_Click(Index As Integer)
    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100)  As Variant
    Dim PesqRapida As Boolean
    Dim i As Integer
    Dim TotalElementosSel As Integer
    
    Select Case Index
        Case 0
                
            If gF_REQUIS = 1 Then
                BL_PesquisaAnalises (FormGestaoRequisicao.EcLocal.text)
            Else
                BL_PesquisaAnalises
            End If
        Case 1
            PesqRapida = False
            
            ChavesPesq(1) = "cod_perfis"
            CamposEcran(1) = "cod_perfis"
            Tamanhos(1) = 1000
            Headers(1) = "Perfil"
            
            CamposEcran(2) = "descr_perfis"
            Tamanhos(2) = 3500
            Headers(2) = "Descri��o"
            
            CamposEcran(3) = "descr_sgr_ana"
            Tamanhos(3) = 1500
            Headers(3) = "Sub-Grupo"
            
            CamposRetorno.InicializaResultados 1
            
            CFrom = "sl_perfis LEFT OUTER JOIN  sl_sgr_ana ON sl_perfis.sgr_ana = sl_sgr_ana.cod_sgr_ana "
            CWhere = " (flg_invisivel<>1 or flg_invisivel IS NULL) AND flg_activo = '0'  and cod_perfis IN (SELECT cod_ana FROM sl_ana_locais) "
            CampoPesquisa = "descr_perfis"
            
            Select Case gFormActivo.Name
                
                Case "FormGestaoRequisicao"
                    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                        CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_perfis ", " Perfis de Marca��o")
                
                Case "FormGestaoRequisicaoPrivado"
                    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                        CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_perfis ", " Perfis de Marca��o")
                Case "FormGestaoRequisicaoVet"
                    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                        CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_perfis ", " Perfis de Marca��o")
                Case "FormGestaoRequisicaoAguas"
                    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                        CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_perfis ", " Perfis de Marca��o")
                
                Case "FormGesReqCons"
                    PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                        CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Perfis de Marca��o")
                
                Case "FormSelRes"
                    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                        CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Perfis de Marca��o")
                
            End Select
            
            If PesqRapida = True Then
                
                Select Case gFormActivo.Name
                    
                    Case "FormGestaoRequisicao"
                        FormPesqRapidaAvancadaMultiSel.Show vbModal
                    
                    Case "FormGestaoRequisicaoPrivado"
                        FormPesqRapidaAvancadaMultiSel.Show vbModal
                    Case "FormGestaoRequisicaoVet"
                        FormPesqRapidaAvancadaMultiSel.Show vbModal
                    Case "FormGestaoRequisicaoAguas"
                        FormPesqRapidaAvancadaMultiSel.Show vbModal
                    
                    Case "FormGesReqCons"
                        FormPesqRapidaAvancadaMultiSel.Show vbModal
                    
                    Case "FormSelRes"
                        FormPesqRapidaAvancada.Show vbModal
                
                End Select
                
                CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
                
                If Not CancelouPesquisa Then
                    
                    Select Case gFormActivo.Name
                    
                        Case "FormGestaoRequisicao"
                            For i = 1 To TotalElementosSel
                                FormGestaoRequisicao.EcAuxAna.Enabled = True
                                FormGestaoRequisicao.EcAuxAna = Resultados(i)
                                FormGestaoRequisicao.EcAuxAna_KeyDown 13, 0
                            Next i
                            FormGestaoRequisicao.FGAna.SetFocus
                        
                        Case "FormGestaoRequisicaoPrivado"
                            For i = 1 To TotalElementosSel
                                FormGestaoRequisicaoPrivado.EcAuxAna.Enabled = True
                                FormGestaoRequisicaoPrivado.EcAuxAna = Resultados(i)
                                FormGestaoRequisicaoPrivado.EcAuxAna_KeyDown 13, 0
                            Next i
                            FormGestaoRequisicaoPrivado.FGAna.SetFocus
                        
                        Case "FormGestaoRequisicaoVet"
                            For i = 1 To TotalElementosSel
                                FormGestaoRequisicaoVet.EcAuxAna.Enabled = True
                                FormGestaoRequisicaoVet.EcAuxAna = Resultados(i)
                                FormGestaoRequisicaoVet.EcAuxAna_KeyDown 13, 0
                            Next i
                            FormGestaoRequisicaoVet.FGAna.SetFocus
                            
                        Case "FormGestaoRequisicaoAguas"
                            For i = 1 To TotalElementosSel
                                FormGestaoRequisicaoAguas.EcAuxAna.Enabled = True
                                FormGestaoRequisicaoAguas.EcAuxAna = Resultados(i)
                                FormGestaoRequisicaoAguas.EcAuxAna_KeyDown 13, 0
                            Next i
                            FormGestaoRequisicaoAguas.FGAna.SetFocus
                        
                        Case "FormGesReqCons"
                            For i = 1 To TotalElementosSel
                                FormGesReqCons.EcAuxAna.Enabled = True
                                FormGesReqCons.EcAuxAna = Resultados(i)
                                FormGesReqCons.EcAuxAna_KeyDown 13, 0
                            Next i
                            FormGesReqCons.FGAna.SetFocus
                        
                        Case "FormSelRes"
                            FormSelRes.EcCodAna.text = Resultados(1)
                            FormSelRes.EcCodAna.SetFocus
                    
                    End Select
                End If
            Else
                BG_Mensagem mediMsgBox, "N�o existem perfis de marca��o", vbExclamation, "ATEN��O"
                If gFormActivo.Name = "FormGestaoRequisicao" Then
                    FormGestaoRequisicao.FGAna.SetFocus
                ElseIf gFormActivo.Name = "FormGestaoRequisicaoPrivado" Then
                    FormGestaoRequisicaoPrivado.FGAna.SetFocus
                ElseIf gFormActivo.Name = "FormGestaoRequisicaoVet" Then
                    FormGestaoRequisicaoVet.FGAna.SetFocus
                ElseIf gFormActivo.Name = "FormGestaoRequisicaoAguas" Then
                    FormGestaoRequisicaoAguas.FGAna.SetFocus
                End If
            End If
        Case 2
            If gF_REQUIS Then
                FormGestaoRequisicao.PreencheTopAna
            ElseIf gF_REQUIS_PRIVADO Then
                FormGestaoRequisicaoPrivado.PreencheTopAna
            End If
       End Select
End Sub


Private Sub HIDE_ANAREQC_Click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean
    Dim indice As Integer
    
    Dim i As Integer
    Dim TotalElementosSel As Integer
    
    PesqRapida = False
        
    Select Case gFormActivo.Name
        
        Case "FormGestaoRequisicao"
            indice = FormGestaoRequisicao.FGAna.row
        
        Case "FormGesReqCons"
            indice = FormGesReqCons.FGAna.row
    
    End Select
    ChavesPesq(1) = "cod_ana_c"
    CamposEcran(1) = "cod_ana_c"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_c"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposEcran(3) = "descr_sgr_ana"
    Tamanhos(3) = 1500
    Headers(3) = "Sub-Grupo"
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "sl_ana_c, sl_sgr_ana "
    CWhere = "sl_ana_c.sgr_ana = sl_sgr_ana.cod_sgr_ana(+) AND (flg_invisivel<>1 or flg_invisivel IS NULL)"
    CampoPesquisa = "descr_ana_c"
    
    
    Select Case gFormActivo.Name
        
        Case "FormGestaoRequisicao"
            PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                                   ChavesPesq, _
                                                                                   CamposEcran, _
                                                                                   CamposRetorno, _
                                                                                   Tamanhos, _
                                                                                   Headers, _
                                                                                   CWhere, _
                                                                                   CFrom, _
                                                                                   "", _
                                                                                   CampoPesquisa, _
                                                                                   " ORDER BY descr_ana_c ", _
                                                                                   " An�lises Complexas")
        
        Case "FormGesReqCons"
            PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Complexas")
        
        Case "FormSelRes"
            PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
            CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Complexas")
    
    End Select
    
    If PesqRapida = True Then
        Select Case gFormActivo.Name
            
            Case "FormGestaoRequisicao"
                FormPesqRapidaAvancadaMultiSel.Show vbModal
            
            Case "FormGesReqCons"
                FormPesqRapidaAvancadaMultiSel.Show vbModal
            
            Case "FormSelRes"
                FormPesqRapidaAvancada.Show vbModal
        
        End Select
    
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
        
        If Not CancelouPesquisa Then
            
            Select Case gFormActivo.Name
            
                Case "FormGestaoRequisicao"
                    For i = 1 To TotalElementosSel
                        FormGestaoRequisicao.EcAuxAna.Enabled = True
                        FormGestaoRequisicao.EcAuxAna.text = Resultados(i)
                        FormGestaoRequisicao.EcAuxAna_KeyDown 13, 0
                    Next i
                    FormGestaoRequisicao.FGAna.SetFocus
                
                Case "FormGesReqCons"
                    For i = 1 To TotalElementosSel
                        FormGesReqCons.EcAuxAna.Enabled = True
                        FormGesReqCons.EcAuxAna.text = Resultados(i)
                        FormGesReqCons.EcAuxAna_KeyDown 13, 0
                    Next i
                    FormGesReqCons.FGAna.SetFocus
                
                
                Case "FormSelRes"
                    FormSelRes.EcCodAna.text = Resultados(1)
                    FormSelRes.EcCodAna.SetFocus
                    
            End Select
        
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises complexas", vbExclamation, "ATEN��O"
        If gFormActivo.Name = "FormGestaoRequisicao" Then
           FormGestaoRequisicao.FGAna.SetFocus
        End If
    End If
    
End Sub

Private Sub HIDE_ANAREQP_Click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100)  As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_perfis"
    CamposEcran(1) = "cod_perfis"
    Tamanhos(1) = 1000
    Headers(1) = "Perfil"
    
    CamposEcran(2) = "descr_perfis"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposEcran(3) = "descr_sgr_ana"
    Tamanhos(3) = 1500
    Headers(3) = "Sub-Grupo"
    
    CamposRetorno.InicializaResultados 1
    
    CFrom = "sl_perfis, sl_sgr_ana "
    CWhere = "sl_perfis.sgr_ana = sl_sgr_ana.cod_sgr_ana(+) AND (flg_invisivel<>1 or flg_invisivel IS NULL)"
    CampoPesquisa = "descr_perfis"
    
    Select Case gFormActivo.Name
        
        Case "FormGestaoRequisicao"
            PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, " ORDER BY descr_perfis ", " An�lises Simples")
        
        Case "FormGesReqCons"
            PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " An�lises Simples")
        
        Case "FormSelRes"
            PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, _
                CamposRetorno, Tamanhos, Headers, CWhere, CFrom, "", CampoPesquisa, "", " Perfis")
        
    End Select
    
    If PesqRapida = True Then
        
        Select Case gFormActivo.Name
            
            Case "FormGestaoRequisicao"
                FormPesqRapidaAvancadaMultiSel.Show vbModal
            
            Case "FormGesReqCons"
                FormPesqRapidaAvancadaMultiSel.Show vbModal
            
            Case "FormSelRes"
                FormPesqRapidaAvancada.Show vbModal
        
        End Select
        
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
        
        If Not CancelouPesquisa Then
            
            Select Case gFormActivo.Name
            
                Case "FormGestaoRequisicao"
                    For i = 1 To TotalElementosSel
                        FormGestaoRequisicao.EcAuxAna.Enabled = True
                        FormGestaoRequisicao.EcAuxAna = Resultados(i)
                        FormGestaoRequisicao.EcAuxAna_KeyDown 13, 0
                    Next i
                    FormGestaoRequisicao.FGAna.SetFocus
                
                Case "FormGesReqCons"
                    For i = 1 To TotalElementosSel
                        FormGesReqCons.EcAuxAna.Enabled = True
                        FormGesReqCons.EcAuxAna = Resultados(i)
                        FormGesReqCons.EcAuxAna_KeyDown 13, 0
                    Next i
                    FormGesReqCons.FGAna.SetFocus
                
                Case "FormSelRes"
                    FormSelRes.EcCodAna.text = Resultados(1)
                    FormSelRes.EcCodAna.SetFocus
            
            End Select
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem perfis", vbExclamation, "ATEN��O"
        If gFormActivo.Name = "FormGestaoRequisicao" Then
            FormGestaoRequisicao.FGAna.SetFocus
        End If
    End If
    
End Sub

Private Sub HIDE_ANAREQS_Click()

    Dim ChavesPesq(1) As String
    Dim CampoPesquisa As String
    Dim CamposEcran(1 To 3) As String
    Dim CWhere As String
    Dim CFrom As String
    Dim CamposRetorno As New ClassPesqResultados
    Dim Tamanhos(1 To 3) As Long
    Dim Headers(1 To 3) As String
    Dim CancelouPesquisa As Boolean
    Dim Resultados(100) As Variant
    Dim PesqRapida As Boolean

    Dim i As Integer
    Dim TotalElementosSel As Integer

    PesqRapida = False
    
    ChavesPesq(1) = "cod_ana_s"
    CamposEcran(1) = "cod_ana_s"
    Tamanhos(1) = 1000
    Headers(1) = "C�digo"
    
    CamposEcran(2) = "descr_ana_s"
    Tamanhos(2) = 3500
    Headers(2) = "Descri��o"
    
    CamposEcran(3) = "descr_produto"
    Tamanhos(3) = 1500
    Headers(3) = "Produto"
    
    
    CamposRetorno.InicializaResultados 1
    
    If gSGBD = gOracle Then
        CFrom = "sl_ana_s, sl_produto "
        CWhere = "sl_ana_s.cod_produto = sl_produto.cod_produto(+) AND (flg_invisivel<>1 or flg_invisivel IS NULL)"
    ElseIf gSGBD = gSqlServer Then
        CFrom = "sl_ana_s LEFT OUTER JOIN sl_produto ON sl_ana_s.cod_produto = sl_produto.cod_produto "
        CWhere = " (flg_invisivel<>1 or flg_invisivel IS NULL)"
    End If
    CampoPesquisa = "descr_ana_s"
    
    Select Case gFormActivo.Name
        
        Case "FormGestaoRequisicao"
            PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                                   ChavesPesq, _
                                                                                   CamposEcran, _
                                                                                   CamposRetorno, _
                                                                                   Tamanhos, _
                                                                                   Headers, _
                                                                                   CWhere, _
                                                                                   CFrom, _
                                                                                   "", _
                                                                                   CampoPesquisa, _
                                                                                   " ORDER BY descr_ana_s ", _
                                                                                   " An�lises Simples")
        
        Case "FormGesReqCons"
            PesqRapida = FormPesqRapidaAvancadaMultiSel.InicializaFormPesqAvancada(gConexao, _
                                                                                   ChavesPesq, _
                                                                                   CamposEcran, _
                                                                                   CamposRetorno, _
                                                                                   Tamanhos, _
                                                                                   Headers, _
                                                                                   CWhere, _
                                                                                   CFrom, _
                                                                                   "", _
                                                                                   CampoPesquisa, _
                                                                                   " ORDER BY descr_ana_s ", _
                                                                                   " An�lises Simples")
        
        Case "FormSelRes"
            PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, _
                                                                           ChavesPesq, _
                                                                           CamposEcran, _
                                                                           CamposRetorno, _
                                                                           Tamanhos, _
                                                                           Headers, _
                                                                           CWhere, _
                                                                           CFrom, _
                                                                           "", _
                                                                           CampoPesquisa, _
                                                                           " ORDER BY descr_ana_s ", _
                                                                           " An�lises Simples")
    End Select
    
    If PesqRapida = True Then
        Select Case gFormActivo.Name
            
            Case "FormGestaoRequisicao"
                FormPesqRapidaAvancadaMultiSel.Show vbModal
            
            Case "FormGesReqCons"
                FormPesqRapidaAvancadaMultiSel.Show vbModal
            
            Case "FormSelRes"
                FormPesqRapidaAvancada.Show vbModal
        
        End Select
        
        CamposRetorno.RetornaResultados Resultados, CancelouPesquisa, TotalElementosSel
        If Not CancelouPesquisa Then
            Select Case gFormActivo.Name
            
                Case "FormGestaoRequisicao"
                
                    For i = 1 To TotalElementosSel
                        FormGestaoRequisicao.EcAuxAna.Enabled = True
                        FormGestaoRequisicao.EcAuxAna = Resultados(i)
                        FormGestaoRequisicao.EcAuxAna_KeyDown 13, 0
                    Next i
                    FormGestaoRequisicao.FGAna.SetFocus
            
                Case "FormGesReqCons"
                
                    For i = 1 To TotalElementosSel
                        FormGesReqCons.EcAuxAna.Enabled = True
                        FormGesReqCons.EcAuxAna = Resultados(i)
                        FormGesReqCons.EcAuxAna_KeyDown 13, 0
                    Next i
                    
                    FormGesReqCons.FGAna.SetFocus
            
                Case "FormSelRes"
                
                    FormSelRes.EcCodAna.text = Resultados(1)
                    FormSelRes.EcCodAna.SetFocus
            
            End Select
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existem an�lises simples", vbExclamation, "ATEN��O"
        If gFormActivo.Name = "FormGestaoRequisicao" Then
            FormGestaoRequisicao.FGAna.SetFocus
        End If
    End If
    
End Sub


Private Sub HIDE_APAGA_PROP_Click()
    
    FormParametrizacaoAcessos.ApagaPermissao FormParametrizacaoAcessos.LwPropriedades.ListItems

End Sub

    




Private Sub HIDE_INFOANA_Click(Index As Integer)
    Select Case Index
        Case 0
            If gModoRes = cModoResMicro Then
                FormResMicro.MinInfoAna
            Else
                FormResultados.MinInfoAna
            End If
        Case 1
            If gModoRes = cModoResMicro Then
                FormResMicro.MaxInfoAna
            Else
                FormResultados.MaxInfoAna
            End If
        Case 2
            If gModoRes = cModoResMicro Then
                FormResMicro.FechaInfoAna
            Else
                FormResultados.FechaInfoAna
            End If
    End Select
    
End Sub





Private Sub HIDE_RESOBSANA_Click(Index As Integer)
    Select Case Index
        Case 0
            If gModoRes = cModoResMicro Then
                FormResMicro.ObservacoesAna
            Else
                FormResultados.ObservacoesAna
            End If
        Case 1
            If gModoRes = cModoResMicro Then
                FormResMicro.ComentarioTecnico
            Else
                FormResultados.ComentarioTecnico
            End If
        Case 2
        
            If gModoRes = cModoResMicro Then
                FormResMicro.ComentarioFinal
            Else
                FormResultados.ComentarioFinal
            End If
    End Select
End Sub

Private Sub HIDE_RESOPERADOR_Click(Index As Integer)
    Select Case Index
        Case 0
            If gModoRes = cModoResMicro Then
                FormResMicro.FuncaoOperador
            Else
                FormResultados.FuncaoOperador
            End If
        Case 1
            If gModoRes = cModoResMicro Then
                FormResMicro.InsercaoMembros
            Else
                FormResultados.InsercaoMembros
            End If
        Case 2
            If gModoRes = cModoResMicro Then
                FormResMicro.ColocaSemRes
            Else
                FormResultados.ColocaSemRes
            End If
        Case 3
            If gModoRes = cModoResMicro Then
                FormResMicro.VerificaValAuto
            Else
                FormResultados.VerificaValAuto
            End If
        Case 4
            If gModoRes = cModoResMicro Then
                FormResMicro.Insercao_TodosMembros
            Else
                FormResultados.Insercao_TodosMembros
            End If
    End Select

End Sub

Private Sub IDLETimer1_Inactividade()
    If gBloqueiaAplicacao = mediSim Then
        gTimeOutActivo = True
        IDLETimer1.MinutosDeIntervalo = 0
                FormConfirmaUtilAct.Show vbModal
        
        gTimeOutActivo = False
        IDLETimer1.MinutosDeIntervalo = gTimeOutMinutos
    Else
        'IDLETimer1.MinutosDeIntervalo = 10000
    End If
End Sub

Private Sub IDM_AGENDA_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormAgenda, "FormAgenda"
            Call FormAgenda.PreencheAgenda(Bg_DaData_ADO, "1")
        Case 1
            BG_AbreForm FormListaTrabalhoBeppSISLAB, "FormListaTrabalhoBeppSISLAB"
        Case 2
            BG_AbreForm FormMonitorEresults, "FormMonitorEresults"
        Case 3
            BG_AbreForm FormAlterDataReq, "FormAlterDataReq"
        Case 4
            BG_AbreForm FormCopiaResultados, "FormCopiaResultados"
        Case 5
            BG_AbreForm FormListagemPedNovaAmostra, "FormListagemPedNovaAmostra"
        Case 6
            BG_AbreForm FormListagemEnvioSMS, "FormListagemEnvioSMS"
        Case 7
            BG_AbreForm FormListagemEntregaResultados, "FormListagemEntregaResultados"
        Case 8
            BG_AbreForm FormGasimProblemas, "FormGasimProblemas"

    End Select
End Sub

Private Sub IDM_AGUAS_Click(Index As Integer)
    BG_AbreForm FormCodGenerico, "FormCodGenerico"
    Select Case Index
        Case 0
            FormCodGenerico.InicializaCodificacao "sl_cod_tipo_amostra", " Codifica��o de Tipo de Amostra", "cod_tipo_amostra", "descr_tipo_amostra"
        Case 1
            FormCodGenerico.InicializaCodificacao "sl_cod_origem_amostra", " Codifica��o de Origem de Amostra", "cod_origem_amostra", "descr_origem_amostra"
        Case 2
            FormCodGenerico.InicializaCodificacao "sl_cod_ponto_colheita", " Codifica��o de Ponto de Colheita", "cod_ponto_colheita", "descr_ponto_colheita"
    End Select
End Sub

Private Sub IDM_CODIFIC_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormEmpresas, "FormEmpresas"
        Case 1
            BG_AbreForm FormCodEntExt, "FormCodEntExt"
            'RGONCALVES 15.03.2016 CEDIVET-160
        Case 2
            BG_AbreForm FormCodUtilizadoresExternos, "FormCodUtilizadoresExternos"
            '
    End Select
End Sub



Private Sub IDM_DIARIO_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormCodMeios, "FormCodMeios"
        Case 1
            BG_AbreForm FormCodGenerico, "FormCodGenerico"
            FormCodGenerico.InicializaCodificacao "SL_COD_ACTIVIDADE", " Codifica��o de Detalhe de Actividades", "COD_ACTIVIDADE", "DESCR_ACTIVIDADE"
    End Select
End Sub

Private Sub IDM_ERESULTS_Click(Index As Integer)
    Select Case Index
        Case 0
            FormMonitorEresults.ApagaRegisto
        Case 1
            FormMonitorEresults.EnviaRegisto
    End Select
End Sub

Private Sub IDM_FGRES_Click(Index As Integer)
    FormCQGeral.AlteraOpcao Index
End Sub

Private Sub IDM_FICHEIROS_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormFACTUSFicheiros, "FormFACTUSFicheiros"
        Case 1
            BG_AbreForm FormFactusFichAdvL, "FormFactusFichAdvL"
        Case 2
            BG_AbreForm FormFactusFichARS, "FormFactusFichARS"
        Case 3
            BG_AbreForm FormFactusFichPT, "FormFactusFichPT"
        Case 4
            BG_AbreForm FormFactusFichADMG, "FormFactusFichADMG"
        Case 5
            BG_AbreForm FormFactusFichADVCE, "FormFactusFichADVCE"
    End Select

End Sub


Private Sub IDM_MAP_TAB_EXT_Click(Index As Integer)
BG_AbreForm FormMapeamentosTabelas, "FormMapeamentosTabelas"
End Sub

'NELSONSPSILVA CHAMADA AO MONITOR
Private Sub IDM_MONITOR_Click(Index As Integer)
  
    Dim url As String
    Dim IE As Object

    If gMonitorESP <> "" Then
        If InStr(1, gMonitorESP, "#LOGIN#") > 0 And InStr(1, gMonitorESP, "#COMPANY#") > 0 Then
            url = Replace(gMonitorESP, "#LOGIN#", gIdUtilizador)
            url = Replace(url, "#COMPANY#", gCompanyDb)
        Else
            url = gMonitorESP
        End If
        If url <> "" Then
            Set IE = CreateObject("internetexplorer.application")
            IE.Navigate url
            IE.Visible = True
            ShowWindow IE.hwnd, 3
        Else
            'BG_TrataErro
            BG_Mensagem mediMsgBox, "Erro ao aceder ao Monitor!", vbOKOnly
        End If
    Else
        BG_Mensagem mediMsgBox, "N�o existe link Parametrizado!", vbOKOnly
    End If

End Sub

'BRUNODSANTOS 23.02.2017 - Glintt-HS-14510
Private Sub IDM_PAISES_Click()

 BG_AbreForm FormPaises, "FormPaises"
 
End Sub

Private Sub IDM_REF_MB_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormMBEmissaoRef, "FormMBEmissaoRef"
        Case 1
            BG_AbreForm FormMBFichMov, "FormMBFichMov"
        Case 2
            BG_AbreForm FormMBListagemRef, "FormMBListagemRef"
    End Select
End Sub

Private Sub IDM_RESU_New_VE_Click(Index As Integer)
    Select Case Index
        Case 0
            FormResultadosNovo.AssociaVEManualmente
        Case 1
            FormResultadosNovo.RemoveAssociaVEManual
        Case 2
            FormResultadosNovo.MostraVigilanciaEpid
    End Select
End Sub

Private Sub IDM_REUTILIZACAO_COLHEITAS_Click(Index As Integer)
 If gAtiva_Reutilizacao_Colheitas = mediSim Then
        BG_AbreForm FormReutilColheitas, "FormReutilColheitas"
 End If
End Sub

Private Sub IDM_RR_Click(Index As Integer)
    Select Case Index
        Case 0
            FormResultadosNovo.MostraTubos
    End Select
End Sub


Private Sub IDM_SEROTECA_Click(Index As Integer)
    Select Case Index
        Case 1
            BG_AbreForm FormVigilanciaEpidimeologica, "FormVigilanciaEpidimeologica"
    End Select
End Sub

Private Sub IDM_SEROTECA_HIDE_OPT_click(Index As Integer)
    Select Case Index
        Case 0
            FormSerGestaoCaixas.ColocaCongelado
        Case 1
            FormSerGestaoCaixas.ColocaDesCongelado
        Case 2
            FormSerGestaoCaixas.ColocaLixo
    End Select
End Sub
Private Sub IDM_CODIFICAO_SEROTECA_OPT_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormSerotecas, "FormSerotecas"
        Case 1
            BG_AbreForm FormSerCodCaixas, "FormSerCodCaixas"
    End Select
End Sub

Private Sub IDM_COLHEITAS_Click(Index As Integer)
    Select Case Index
    
    Case 0
        BG_AbreForm FormSalasColh, "FormSalasColh"
    Case 1
        BG_AbreForm FormCodPrioridades, "FormCodPrioridades"
    Case 3
        BG_AbreForm FormAgendaIndisponiveis, "FormAgendaIndisponiveis"
    End Select
End Sub


Private Sub IDM_CQ_Click()
    BG_AbreForm FormCQGeral, "FormCQGeral"
End Sub

Private Sub IDM_CONTROLO_QUALIDADE_IDX_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormAddLot, "FormAddLot"
        Case 1
            BG_AbreForm FormCQcontrolos, "FormCQcontrolos"
        Case 2
            BG_AbreForm FormCQlotes, "FormCQlotes"
        Case 3
            BG_AbreForm FormCQAnalises, "FormCQAnalises"
    End Select
End Sub

Private Sub IDM_IMPR_CARTOES_Click()
    BG_AbreForm FormImpCartoes, "FormImpCartoes"
End Sub

Private Sub IDM_IMPR_RECIBO_Click(Index As Integer)
    Select Case Index
        Case 0
            FormGestaoRequisicaoPrivado.ImprimeRecibo ""
        Case 1
            FormGestaoRequisicaoPrivado.ImprimeRecibo "C�pia"
        Case 2
            FormGestaoRequisicaoPrivado.ImprimeRecibo "Segunda Via"
    End Select
    
End Sub

' pferreira 2010.02.18
Private Sub IDM_MENU_CODIFICACOES_STK_OPT_Click(Index As Integer)

    Select Case Index
        Case 0: BG_AbreForm FormInventarioStock, "FormInventarioStock"
        Case 1: BG_AbreForm FormStkFornecedores, "FormStkFornecedores"
        Case 2: BG_AbreForm FormStkTipoArtigo, "FormStkTipoArtigo"
        Case 3: BG_AbreForm FormStkTipoMov, "FormStkTipoMov"
        Case 4: BG_AbreForm FormStkArtigos, "FormStkArtigos"
        Case 5: BG_AbreForm FormStkLotes, "FormStkLotes"
    End Select
    
End Sub

Private Sub IDM_MENU_FACTURACAO_LISTAGENS_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormListagemCaixa, "FormListagemCaixa"
        Case 1
            ' pferreira 2010.04.19
           BG_AbreForm FormFactusListagemGeral, "FormFactusListagemGeral"
        Case 2
            BG_AbreForm FormListagemProducao, "FormListagemProducao"
        Case 3
            BL_AplExt "DIGITALIZADOR", ""
        Case 4
            BG_AbreForm FormFactCartaDebito, "FormFactCartaDebito"
        'rcoelho 1.08.2013 ljmanso-115
        Case 5
            BG_AbreForm FormFactusGuiasFacturacao, "FormFactusGuiasFacturacao"
    End Select
        
End Sub

Private Sub IDM_REQUISICOES_PRIV_Click(Index As Integer)
    Select Case Index
        Case 0
            ' PRIVADO
            FormGestaoRequisicaoPrivado.Req_FDS = False
            BG_AbreForm FormGestaoRequisicaoPrivado, "FormGestaoRequisicaoPrivado"
            
        Case 1
            ' PRIVADO FIM DE SEMANA
            FormGestaoRequisicaoPrivado.Req_FDS = True
            BG_AbreForm FormGestaoRequisicaoPrivado, "FormGestaoRequisicaoPrivado"
        Case 2
            BG_AbreForm FormEntregaResultados, "FormEntregaResultados"
            
    End Select

End Sub



Private Sub IDM_RESU_Click(Index As Integer)
    Select Case Index
        Case 0 ' REPETICOES
            FormResultados.PreencheRepeticoes (FormResultados.linhaActual)
        Case 1 ' INTERFERENCIAS
            FormResultados.ProcuraInterferencias
        Case 2 ' VOLUME
            FormResultados.RetornaVolume (FormResultados.linhaActual)
        Case 3 'OPERADOR
            If gModoRes = cModoResMicro Then
                FormResMicro.FuncaoOperador
            Else
                FormResultados.FuncaoOperador
            End If
        Case 4 ' OBS DA ANALISE
            If gModoRes = cModoResMicro Then
                FormResMicro.ObservacoesAna
            Else
                FormResultados.ObservacoesAna
            End If
        Case 5 'SEM RESULTADO
            If gModoRes = cModoResMicro Then
                FormResMicro.ColocaSemRes
            Else
                FormResultados.ColocaSemRes
            End If
        Case 6 'GRAFICO
            FormResultados.ConsultarGrafico
    End Select
End Sub



Private Sub IDE_PARAM_AMBIENTE_GERAL_Click()
    
    ' MENU 01.06.01.01
    gOpParamAmbiente = 1
    Select Case (gVERSAO_PARAM_AMB)
        Case "V1": BG_AbreForm FormParamAmbiente, "FormParamAmbiente"
        Case "V2": BG_AbreForm FormParamAmbienteV2, "FormParamAmbienteV2"
        Case Else: BG_AbreForm FormParamAmbiente, "FormParamAmbiente"
    End Select
End Sub

Private Sub IDE_PARAM_AMBIENTE_LOCAL_Click()
    
    ' MENU 01.06.02.01
    gOpParamAmbiente = 2
    BG_AbreForm FormParamAmbiente, "FormParamAmbiente"

End Sub

Private Sub IDM_ADC_CLIENTE_Click()
    
    ' MENU 01.06.04
    BG_AbreForm FormIdentClienteAdc, "FormIdentClienteAdc"

End Sub



'Private Sub IDM_ALTERACAO_Click()
'
'    ' MENU 03.04.02
'    gModoRes = cModoResAlt
'    BG_AbreForm FormSelRes, "FormSelRes"
'
'End Sub


Private Sub IDM_ANALISES_GR_TRAB_Click()
    
    BG_AbreForm FormAnaGrTrab, "FormAnaGrTrab"

End Sub



Private Sub IDM_DOCS_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormDeclaracaoPres, "FormDeclaracaoPres"
        Case 1
            BG_AbreForm FormPendObs, "FormPendObs"
    End Select
End Sub




Sub IDM_ANTERIOR_Click()
    
    ' MENU 02.06
    gFormActivo.FuncaoAnterior

End Sub

Private Sub IDM_ANTIBIO_Click()
    
    BG_AbreForm FormAntibio, "FormAntibio"

End Sub


Private Sub IDM_ARQUIVOS_Click()
    BG_AbreForm FormArquivos, "FormArquivos"
End Sub



Private Sub IDM_CANCELAMENTOS_Click()
    
    BG_AbreForm FormTipoCancelamento, "FormTipoCancelamento"

End Sub



Private Sub IDM_CODIFICACAO_Click()
    
    BG_AbreForm FormValAuto, "FormValAuto"

End Sub

Private Sub IDM_CONC_ANTIBIO_Click()
    
    BG_AbreForm FormConcAntibio, "FormConcAntibio"

End Sub

Private Sub IDM_CONFIG_IMPRESSORA_Click()
    
    ' MENU 01.03
    Call BG_AbreForm(FormConfImpressoras, "FormConfImpressoras")

End Sub



Private Sub IDM_CONS_REQUIS_Click(Index As Integer)
    
    ' MENU 03.01.03.02
    Select Case Index
        Case 0
            ' MENU 03.01.03.01
            BG_AbreForm FormReqUte, "FormReqUte"
        
        Case 1
            BG_AbreForm FormConsRequis, "FormConsRequis"
            
        Case 2
            BG_AbreForm FormConsNovo, "FormConsNovo"
    End Select

End Sub





Private Sub IDM_DATA_ACTUAL_Click()
    
    ' MENU 01.02
    gFormActivo.Funcao_DataActual
    gFormActivo.SetFocus

End Sub



Private Sub IDM_DESCR_REL_Click()
    
    ' MENU 01.07.02
    BG_AbreForm FormReports, "FormReports"

End Sub
Private Sub IDM_DIAG_Click()
    
    BG_AbreForm FormDiag, "FormDiag"

End Sub



Private Sub IDM_EFR_Click()
    
    BG_AbreForm FormEntFinanceiras, "FormEntFinanceiras"

End Sub

Private Sub IDM_ESPECIF_Click()
    
    BG_AbreForm FormEspecProd, "FormEspecProd"

End Sub

Sub IDM_ESTADO_ANTERIOR_Click()
    
    ' MENU 02.08
    gFormActivo.FuncaoEstadoAnterior

End Sub
    

Private Sub IDM_ESTAT_DOENTES_Click()
    BG_AbreForm FormEstatDoentes, "FormEstatDoentes"
End Sub


Private Sub IDM_ESTAT_MICRO_Click(Index As Integer)
    Select Case Index
        Case 0
            'Exames Microbiol�gicos
            BG_AbreForm FormEstatExMicro, "FormEstatExMicro"
        Case 1
            'Microorganismos
            BG_AbreForm FormEstatMicrorgTempo, "FormEstatMicrorgTempo"
            'BG_AbreForm FormEstatMicrorg, "FormEstatMicrorg"
        Case 2
            'Resultados
            BG_AbreForm FormEstatResMicro, "FormEstatResMicro"
        Case 3
            'Resist�ncias
            BG_AbreForm FormEstMicro, "FormEstMicro"
        Case 4
            BG_AbreForm FormEstatResMicroAna, "FormEstatResMicroAna"
        Case 5
            BG_AbreForm FormEstatMicroPositivos, "FormEstatMicroPositivos"
    End Select
End Sub

Private Sub IDM_ESTAT_REQ_Click(Index As Integer)
    
    Select Case Index
        
        Case 0
            ' MENU 05.01.01
            BG_AbreForm FormAnaPorReq, "FormAnaPorReq"
        Case 1
            ' MENU 05.01.02
            BG_AbreForm FormAnaReqDia, "FormAnaReqDia"
    
    End Select

End Sub



Private Sub IDM_ESTAT_RES_Click(Index As Integer)
    Select Case Index
        Case 0
             BG_AbreForm FormEstatResultados, "FormEstatResultados"
        Case 1
             BG_AbreForm FormListagemResultados, "FormListagemResultados"
        ' pferreira 2010.03.08 (GLINTTHS_253)
        Case 2
             BG_AbreForm FormEstatResAna, "FormEstatResAna"
    End Select
End Sub



Private Sub IDM_ESTAT_SONHO_Click()
    BG_AbreForm FormEstatAnaFact, "FormEstatAnaFact"
End Sub




Private Sub IDM_FOLHAS_TRAB_Click(Index As Integer)
    Select Case Index
        Case 0
            FormFolhasTrab.TipoFolha = "FolhaTrabalhoAna"
            BG_AbreForm FormFolhasTrab, "FormFolhasTrab"
        Case 1
            FormFolhasTrab.TipoFolha = "FolhaTrabalhoTubo"
            BG_AbreForm FormFolhasTrab, "FormFolhasTrab"
        Case 2
            BG_AbreForm FormFolhasTrabNOVA, "FormFolhasTrabNOVA"
        Case 3
            BG_AbreForm FormTrabExt, "FormTrabExt"
    End Select
End Sub


Private Sub IDM_FRASE_Click(Index As Integer)
    
    Select Case Index
        Case 0
            BG_AbreForm FormFrase, "FormFrase"
        Case 1
            BG_AbreForm FormCodFlagApar, "FormCodFlagApar"
    End Select
End Sub



Private Sub IDM_GR_ANTIBIO_Click()
    
    BG_AbreForm FormGruposAntibio, "FormGruposAntibio"

End Sub

Private Sub IDM_GR_DIAG_Click()
    
    BG_AbreForm FormGruposDiag, "FormGruposDiag"

End Sub


Private Sub IDM_GR_TRABALHO_Click()
    
    BG_AbreForm FormGruposTrab, "FormGruposTrab"

End Sub

Private Sub IDM_GRUPO_UTILIZADOR_Click()
    
    ' MENU 01.07.03
    BG_AbreForm FormGruposUtilizador, "FormGruposUtilizador"

End Sub

Private Sub IDM_ID_DOENTES_Click(Index As Integer)
    
    ' MENU 03.01.01
    Select Case Index
        Case 0
            If gTipoInstituicao = gTipoInstituicaoHospitalar Then
                BG_AbreForm FormIdentificaUtente, "FormIdentificaUtente"
            ElseIf gTipoInstituicao = gTipoInstituicaoPrivada Or gTipoInstituicao = gTipoInstituicaoAguas Then
                BG_AbreForm FormIdUtentePesq, "FormIdUtentePesq"
            ElseIf gTipoInstituicao = gTipoInstituicaoVet Then
                BG_AbreForm FormIdentificaVet, "FormIdentificaVet"
            End If
        Case 1
            BG_AbreForm FormReqPedElectr, "FormReqPedElectr"
        Case 2
            ' Marca��o de Consultas
            BG_AbreForm FormGesReqCons, "FormGesReqCons"

    End Select
End Sub

Private Sub IDM_IDCLIENTE_Click()
    
    ' MENU 01.06.03
    BG_AbreForm FormIdentCliente, "FormIdentCliente"

End Sub

Private Sub IDM_IMP_COMPLETAS_Click()
    
    ' MENU 03.04.06.03
    FormListarResMult.TipoImp = Completas
    BG_AbreForm FormListarResMult, "FormListarResMult"

End Sub

Private Sub IDM_IMP_JA_LISTADAS_Click(Index As Integer)
    
    Select Case Index
        Case 0
            FormListarResMult.TipoImp = ParcialmenteListados
            BG_AbreForm FormImprimeListaReq, "FormImprimeListaReq"
        Case 1
            BG_AbreForm FormEnvioEmail, "FormEnvioEmail"
        Case 2
            BG_AbreForm FormImprimeResultadosExterior, "FormImprimeResultadosExterior"
    End Select
    
End Sub

Private Sub IDM_IMP_PRONTOS_Click()
    
    ' MENU 03.04.06.04
    FormListarResMult.TipoImp = Prontos
    BG_AbreForm FormListarResMult, "FormListarResMult"

End Sub

Private Sub IDM_IMP_REQ_Click()
    
    ' MENU 03.04.06.02
    BG_AbreForm FormListarResultados, "FormListarResultados"

End Sub


Private Sub IDM_IMPRIMIR_Click()
    
    ' MENU 01.05
    gImprimirDestino = 1
    gFormActivo.FuncaoImprimir

End Sub

Private Sub IDM_IMPRIMIR_VER_ANTES_Click()
    
    ' MENU 01.04
    gImprimirDestino = 0
    gFormActivo.FuncaoImprimir

End Sub

Private Sub IDM_INC_RESULTADOS_Click()
    
    BG_AbreForm FormIncRes, "FormIncRes"

End Sub


Private Sub IDM_INI_Click()
    
    ' MENU 01.07.01
    Form_Ini.Show

End Sub

Sub IDM_INSERIR_Click()
    
    ' MENU 02.02
    Call Registo("Inserir")

End Sub

Private Sub IDM_INTRO_RES_Click()
    gModoRes = cModoResIns
    BG_AbreForm FormSelRes, "FormSelRes"
End Sub

'Private Sub IDM_INTRO_VAL_MED_Click()
'
'    ' MENU 03.04.05
'    gModoRes = cModoResInsVal
'    BG_AbreForm FormSelRes, "FormSelRes"
'
'End Sub

Private Sub IDM_INTRO_VAL_RES_Click()

End Sub

'Private Sub IDM_INTRODUCAO_Click()
'
'    ' MENU 03.04.01
'    gModoRes = cModoResIns
'    BG_AbreForm FormSelRes, "FormSelRes"
'
'End Sub

Private Sub IDM_ISENCAO_Click()
    
    BG_AbreForm FormIsencao, "FormIsencao"

End Sub

Sub IDM_LIMPAR_Click()
    
    ' MENU 02.01
    Call Registo("Limpar")

End Sub

Private Sub IDM_LISTA_RELIMP_Click()
    BG_AbreForm FormListarRelatoriosImp, "FormListarRelatoriosImp"
End Sub


Private Sub IDM_LISTANA_Click(Index As Integer)
    
    Select Case Index
        Case 0
            ' MENU 04.06.01
            If MDIFormInicio.Tag = "ESCREFZONAS" Then
                Unload FormListarAnalises
            End If
            MDIFormInicio.Tag = "ANALISES"
            BG_AbreForm FormListarAnalises, "FormListarAnalises"
        Case 1
            ' MENU 04.06.02
            If MDIFormInicio.Tag = "ANALISES" Then
                Unload FormListarAnalises
            End If
            MDIFormInicio.Tag = "ESCREFZONAS"
            BG_AbreForm FormListarAnalises, "FormListarAnalises"
        Case 2
            ' MENU 04.06.03
            BG_AbreForm FormImprimeCodificacoes, "FormImprimeCodificacoes"
        Case 3
            BG_AbreForm FormListagemFrasesPadrao, "FormListagemFrasesPadrao"
        
    End Select
End Sub

Private Sub IDM_LISTGEN_Click()

    
End Sub


Private Sub IDM_MAPAREGDETAL_Click()
    
    ' MENU 03.01.05.02
    Call BG_AbreForm(FormFolhasTrabGrupo, "FormFolhasTrabGrupo")
    
End Sub

Private Sub IDM_MAPAREGGLOB_Click()

    ' MENU 03.01.05.01
    BG_AbreForm FormMapaRegisto, "FormMapaRegisto"
    
End Sub

Private Sub IDM_MEDICOS_Click()
    
    BG_AbreForm FormMedicos, "FormMedicos"

End Sub

Private Sub IDM_MENU_AJUDA_Click(Index As Integer)

    Select Case Index
        Case 0
            ' �ndice
            Call IDM_MENU_AJUDA_00
        Case 2
            ' Acerca de
            Call IDM_MENU_AJUDA_02
    End Select

End Sub

Private Sub IDM_MENU_ESTATISTICAS_Click(Index As Integer)
    
    Select Case Index
    
        Case 1
            ' NADA
        Case 2
            ' NADA
        Case 4
            ' MENU 05.05
            BG_AbreForm FormEstatProduto, "FormEstatProduto"
        
        Case 6
            BG_AbreForm FormEstTempoMediaAna, "FormEstTempoMediaAna"
            
        Case 9
            BG_AbreForm FormEstatAnaFact, "FormEstatAnaFact"
        
        Case 10
            BG_AbreForm FormEstatAna, "FormEstatAna"
        
        Case 11
            BG_AbreForm FormEstatTempoColheita, "FormEstatTempoColheita"
        Case 12
            BG_AbreForm FormEstatTemposValidacao, "FormEstatTemposValidacao"
        Case 13
            BG_AbreForm FormEstatTemposEstapas, "FormEstatTemposEstapas"
        Case 14
            BG_AbreForm FormEstatResAna, "FormEstatResAna"
    End Select

End Sub

Private Sub IDM_ANALISES_T_Click(Index As Integer)
    Select Case Index
        
        Case 0
            BG_AbreForm FormCodAna, "FormCodAna"
        
        Case 1
            BG_AbreForm FormAnaComplexas, "FormAnaComplexas"
            
        Case 2
            BG_AbreForm FormExames, "FormExames"
            
        Case 3
            BG_AbreForm FormPerfisAna, "FormPerfisAna"
            
        Case 4
            BG_AbreForm FormOrdenaAnaNew, "FormOrdenaAnaNew"
            
        Case 5
            BG_AbreForm FormGrAnalises, "FormGrAnalises"
            
        Case 6
            BG_AbreForm FormSubGrAnalises, "FormSubGrAnalises"
            
        Case 7
            BG_AbreForm FormClassesAnalises, "FormClassesAnalises"
    
        Case 8
            BG_AbreForm FormAnaFrase, "FormAnaFrase"
    
        Case 9
            BG_AbreForm FormAnaInterferencias, "FormAnaInterferencias"
        Case 10
            BG_AbreForm FormGrPerfis, "FormGrPerfis"
        Case 12
            BG_AbreForm FormGrImpr, "FormGrImpr"
        Case 13
            BG_AbreForm FormInformAna, "FormInformAna"
        Case 14
            BG_AbreForm FormTemposMaxAnalises, "FormTemposMaxAnalises"
        Case 15
            BG_AbreForm FormCodRelatorios, "FormCodRelatorios"
        Case 16
            BG_AbreForm FormAnaAutomaticos, "FormAnaAutomaticos"
    End Select
End Sub

Private Sub IDM_MENU_FACTURACAO_Click(Index As Integer)
    Select Case Index
    
        Case 0
            BG_AbreForm FormMapFact, "FormMapFact"
'            ' MENU 04.08
'            'Mapear Rubricas
'            Select Case gSISTEMA_FACTURACAO
'                ' FACTUS
'                Case cFACTURACAO_FACTUS
'                    'BG_AbreForm FormFACTUSNovasAnalises, "FormFACTUSNovasAnalises"
'                    BG_AbreForm FormGHNovasAnalises, "FormGHNovasAnalises"
'                ' GEST�O HOSPITALAR
'                Case cFACTURACAO_GH
'                    BG_AbreForm FormGHNovasAnalises, "FormGHNovasAnalises"
'                Case "SISBIT"
'                    BG_AbreForm FormSONHONovasAnalises, "FormSONHONovasAnalises"
'                ' SONHO
'                Case cFACTURACAO_SONHO
'                    BG_AbreForm FormSONHONovasAnalises, "FormSONHONovasAnalises"
'                Case cFACTURACAO_NOVAHIS
'                    BG_AbreForm FormGHNovasAnalises, "FormGHNovasAnalises"
'            End Select
        Case 1
            ' MENU 04.09
            'Gerar dados
            Select Case gSISTEMA_FACTURACAO
                ' FACTUS
                Case cFACTURACAO_FACTUS
                    If gTipoInstituicao = "PRIVADA" Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
                        If gPassaRecFactus <> mediSim Then
                            BG_AbreForm FormFactusEnvio, "FormFACTUSEnvio"
                        Else
                            BG_Mensagem mediMsgBox, "Op��o desativada.", vbExclamation, "ATEN��O"
                        End If
                        
                    Else
                        BG_AbreForm FormGeraDados, "FormGeraDados"
                    End If
                ' GEST�O HOSPITALAR
                Case cFACTURACAO_GH
                    'BG_AbreForm FormGHEnvio, "FormGHEnvio"
                    If gAbreEnvioFactusPrivado = mediSim Then
                        BG_AbreForm FormFactusEnvio, "FormFACTUSEnvio"
                    Else
                        BG_AbreForm FormGeraDados, "FormGeraDados"
                    End If
                ' SONHO
                Case cFACTURACAO_SONHO
                    'BG_AbreForm FormSONHOEnvio, "FormSONHOEnvio"
                    BG_AbreForm FormGeraDados, "FormGeradados"
            End Select
            
        Case 2
            'Tratamento de Rejeitados
            BG_AbreForm FormFACTUSTrataRejeitados, "FormFACTUSTrataRejeitados"
        Case 3
            Select Case gSISTEMA_FACTURACAO
                ' FACTUS
                Case cFACTURACAO_FACTUS
                    BG_AbreForm FormListaFacturacao, "FormListaFacturacao"
            End Select
        Case 4
            'Listar Devolvidos
            BG_AbreForm FormListarDevolvidosSONHO, "FormListarDevolvidosSONHO"
        Case 5
            'Requisi��es sem epis�dio
            BG_AbreForm FormListarRequisSemEpisodio, "FormListarRequisSemEpisodio"
        
        Case 6
            'Altera��o de Epis�dio Facturado
            BG_AbreForm FormAlteraEpisFacturado, "FormAlteraEpisFacturado"
        Case 7
            If gPassaRecFactus <> mediSim Then
                BG_AbreForm FormFACTUSConfirma, "FormFACTUSConfirma"
            Else
                BG_AbreForm FormFACTUSConfirmaNovo, "FormFACTUSConfirmaNovo"
            End If
        Case 8
            BG_AbreForm FormUtilFactExternos, "FormUtilFactExternos"
        Case 10
            BG_AbreForm FormFactusEnvioSerie, "FormFactusEnvioSerie"
        Case 11
            BG_AbreForm FormCodPortarias, "FormCodPortarias"
    End Select
    
End Sub

Private Sub IDM_MENU_FERRAMENTAS_Click(Index As Integer)

    Select Case Index
        Case 0
            ' GESCOM
            Call IDM_MENU_FERRAMENTAS_00
        Case 2
            ' Sistema de Interroga��o
            Call IDM_MENU_FERRAMENTAS_02
        Case 4
            ' Calculadora
            Call IDM_MENU_FERRAMENTAS_04
        Case 5
            ' Editor
            Call IDM_MENU_FERRAMENTAS_05
    End Select

End Sub

Private Sub IDM_METODOS_Click()
    
    BG_AbreForm FormMetodos, "formMetodos"

End Sub



Private Sub IDM_MICRO_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormMicro, "FormMicro"
        Case 1
            BG_AbreForm FormGruposMicro, "FormGruposMicro"
        Case 2
            BG_AbreForm FormIdProva, "FormIdProva"
        Case 3
            BG_AbreForm FormProva, "FormProva"
        Case 4
            BG_AbreForm FormCodFrasesMicro, "FormCodFrasesMicro"
    End Select
End Sub

Private Sub IDM_MODELOS_LIVRES_Click()
    
    BG_AbreForm FormModelo, "FormModelo"

End Sub

Sub IDM_MODIFICAR_Click()
    
    ' MENU 02.04
    Call Registo("Modificar")

End Sub

Private Sub IDM_MOTIVOS_CLI_Click()
    BG_AbreForm FormMotivosClinicos, "FormMotivosClinicos"
End Sub

Private Sub IDM_MUDAR_SENHA_Click()
    
    ' MENU 01.01
    BG_StackJanelas_Reset
    FormMudarSenha.Show vbModal

End Sub



Private Sub IDM_OR_ZONAS_Click()
    
    BG_AbreForm FormOutrasRazoes, "FormOutrasRazoes"

End Sub

Private Sub IDM_ORDENA_FRASE_Click()
    
    BG_AbreForm FormOrdenaAna, "FormOrdenaAna"

End Sub


Private Sub IDM_PARAM_ACESSOS_Click(Index As Integer)
    
    ' MENU 01.07.05
    Select Case Index
    Case 0
        If gUsaProfiles = mediSim Then
            BG_AbreForm FormAssociacaoProfiles, "FormAssociacaoProfiles"
        Else
            BG_AbreForm FormParametrizacaoAcessos, "FormParametrizacaoAcessos"
        End If
    Case 1
        BG_AbreForm FormParamAcessosGerarControlos, "FormParamAcessosGerarControlos"
    End Select

End Sub

Private Sub IDM_PASSACT_Click()
    
    ' MENU 04.05.02
    If MDIFormInicio.Tag = "HISTORICO" Then
        Unload FormHistorico
    End If
    MDIFormInicio.Tag = "UNDOHISTORICO"
    Call BG_AbreForm(FormHistorico, "FormUndoHistorico")
    
End Sub

Private Sub IDM_PASSHIST_Click()
    
    ' MENU 04.05.01
    If MDIFormInicio.Tag = "UNDOHISTORICO" Then
        Unload FormHistorico
    End If
    MDIFormInicio.Tag = "HISTORICO"
    Call BG_AbreForm(FormHistorico, "FormHistorico")
    
End Sub


Private Sub IDM_PED_NOVA_AMOSTRA_Click(Index As Integer)
    Select Case Index
    Case 0
        BG_AbreForm FormPedidoNovaAmostra, "FormPedidoNovaAmostra"
    Case 1
        BG_AbreForm FormEnviarAnaApar, "FormEnviarAnaApar"
    Case 2
        BG_AbreForm FormAbrirRequisFechadas, "FormAbrirRequisFechadas"
    Case 3
        'BG_AbreForm FormInterfaces, "FormInterfaces"
    Case 4
        BG_AbreForm FormCalculoPrecos, "FormCalculoPrecos"
    End Select
End Sub


Public Sub IDM_PORTUBO_Click(Index As Integer)
    Select Case Index
        Case 0
            gColheitaChegada = gEnumColheitaChegada.ChegadaTubo
            If gVersaoChegadaTubos = "V2" Then
                BG_AbreForm FormChegadaPorTubosV2, "FormChegadaPorTubosV2"
            Else
                BG_AbreForm FormChegadaPorTubos, "FormChegadaPorTubos"
            End If
        Case 1
            If gCodUtilizadorColheita = "" Then
                BG_Mensagem mediMsgBox, "O Utilizador n�o tem associado perfil de colheita na tabela de colheitas.", vbExclamation, "ATEN��O"
                Exit Sub
            End If
            gColheitaChegada = gEnumColheitaChegada.ColheitaTubo
            BG_AbreForm FormChegadaPorTubosV2, "FormChegadaPorTubosV2"
        Case 2
            BG_AbreForm FormFilaEspera, "FormFilaEspera"
    End Select
End Sub

Private Sub IDM_POSTAIS_Click()
    
    ' MENU 06.16.03
    BG_AbreForm FormCodPostal, "FormCodPostal"

End Sub


Sub IDM_PROCURAR_Click()
    
    ' MENU 02.03
    Call Registo("Procurar")

End Sub

Private Sub IDM_PRODUTOS_Click()
    
    BG_AbreForm FormProdutos, "FormProdutos"

End Sub

Private Sub IDM_PROFISSAO_Click()
    
    ' MENU 06.16.05
    BG_AbreForm FormProfissao, "FormProfissao"

End Sub


Private Sub IDM_PROTOCOLOS_Click(Index As Integer)
    Select Case Index
        Case 0
            ' MENU 03.04.08
            Call BG_AbreForm(FormImprimeProtocolo, "FormImprimeProtocolo")
        Case 1
            Call BG_AbreForm(FormConfirmacaoPorTubos, "FormConfirmacaoPorTubos")
        Case 2
            Call BG_AbreForm(FormBloqRequis, "FormBloqRequis")
        Case 3
            Call BG_AbreForm(FormDiarioPendentes, "FormDiarioPendentes")
    End Select
End Sub

Private Sub IDM_PROVEN_Click(Index As Integer)
    Select Case Index
    Case 0
        BG_AbreForm FormProveniencia, "FormProveniencia"
    Case 1
    Call BG_AbreForm(FormUtilProv, "FormUtilProv")
    End Select

End Sub

Private Sub IDM_REAGENTES_Click()
    
    BG_AbreForm FormReagentes, "FormReagentes"

End Sub

Private Sub IDM_REGISTO_ACESSOS_Click()
    
    ' MENU 01.07.07
    BG_AbreForm FormRegistoAcessos, "FormRegistoAcessos"

End Sub

Private Sub IDM_REL_ANA_AUTO_Click()
    
    BG_AbreForm FormRelAutRes, "FormRelAutAna"

End Sub

Private Sub IDM_REL_ANAFRASE_Click()
    
    BG_AbreForm FormAnaFrase, "FormAnaFrase"

End Sub

Private Sub IDM_REL_ANAPERFIS_Click()
    
    BG_AbreForm FormAnaPerfil, "FormAnaPerfil"

End Sub

Private Sub IDM_REL_GR_ANTIBIO_Click(Index As Integer)
    Select Case Index
    Case 0
        BG_AbreForm FormRelGrAntibio, "FormRelGrAntibio"
    Case 1
        BG_AbreForm FormClasseAntibio, "FormClasseAntibio"
    Case 2
        BG_AbreForm FormSubClasseAntibio, "FormSubClasseAntibio"
    End Select
End Sub


Private Sub IDM_REMOVER_Click()
    
    ' MENU 02.05
    Call Registo("Remover")

End Sub

Private Sub IDM_REQ_Click(Index As Integer)
    Select Case Index
        Case 0
            ' Requisi��es Previstas
            ' MENU 04.01
            BG_AbreForm FormReqPrev, "FormReqPrev"
        Case 2
            'Requisi��es Validadas
            ' MENU 04.03
            BG_AbreForm FormReqValidadas, "FormReqValidadas"
        Case 3
            'Requisi��es por Utilizador
            BG_AbreForm FormEstatReqUtil, "FormEstatReqUtil"
        Case 4
            'Requisi��es por Utilizador
            BG_AbreForm FormFolhasServico, "FormFolhasServico"
    End Select
End Sub

Private Sub IDM_REQCANCELADAS_Click()
    
    ' MENU 03.01.04
    BG_AbreForm FormRequisicoesCanceladas, "FormRequisicoesCanceladas"

End Sub



Private Sub IDM_REQPEND_Click(Index As Integer)
    Select Case Index
        Case 0
            'Requisi��es Pendentes por resultado

            ' MENU 04.02.01
            If MDIFormInicio.Tag <> "RESULTADO" Then
                Unload FormReqPend
            End If
            MDIFormInicio.Tag = "RESULTADO"
            BG_AbreForm FormReqPend, "FormReqPend"
     
        Case 1
            'Requisi��es pendentes por valida��o
            
            ' MENU 04.02.02
            If MDIFormInicio.Tag <> "VALIDA��O" Then
                Unload FormReqPend
            End If
            MDIFormInicio.Tag = "VALIDA��O"
            BG_AbreForm FormReqPend, "FormReqPend"
        Case 2
            
            
            ' MENU 04.02.02
            If MDIFormInicio.Tag <> "ASSINATURA" Then
                Unload FormReqPend
            End If
            MDIFormInicio.Tag = "ASSINATURA"
            BG_AbreForm FormReqPend, "FormReqPend"
        Case 3
            
            
            ' MENU 04.02.02
            If MDIFormInicio.Tag <> "ASSINADAS_NAO_IMPRESSAS" Then
                Unload FormReqPend
            End If
            MDIFormInicio.Tag = "ASSINADAS_NAO_IMPRESSAS"
            BG_AbreForm FormReqPend, "FormReqPend"
              
    End Select
    
End Sub

Private Sub IDM_REQPEND_GRUPO_Click(Index As Integer)
  Select Case Index
        Case 1
            BG_AbreForm FormAutomatizaResultadosSerie, "FormAutomatizaResultadosSerie"
        Case 3
            BG_AbreForm FormReqPendAna, "FormReqPendAna"
    End Select
End Sub



Private Sub IDM_REQUIS_Click(Index As Integer)
    Select Case Index
        Case 0
            ' Marca��o de Consultas
            BG_AbreForm FormGesReqCons, "FormGesReqCons"
        Case 1
            BG_AbreForm FormGestaoRequisicao, "FormGestaoRequisicao"
    End Select
End Sub

Private Sub IDM_REQUISICOES_Click(Index As Integer)
    
        ' PUBLICO
    Select Case Index
        Case 0
            If gTipoInstituicao = gTipoInstituicaoVet Then
                BG_AbreForm FormGestaoRequisicaoVet, "FormGestaoRequisicaoVet"
            ElseIf gTipoInstituicao = gTipoInstituicaoAguas Then
                BG_AbreForm FormGestaoRequisicaoAguas, "FormGestaoRequisicaoAguas"
            Else
                BG_AbreForm FormGestaoRequisicao, "FormGestaoRequisicao"
            End If
        Case 1
            BG_AbreForm FormEntregaResultados, "FormEntregaResultados"
    End Select
End Sub


Private Sub IDM_RES_Click(Index As Integer)
    Select Case Index
        Case 1
            gModoRes = cModoResMicro
            BG_AbreForm FormSelRes, "FormSelRes"
        Case 2
            gModoRes = cModoResValApar
            BG_AbreForm FormSelRes, "FormSelRes"
        Case 3
            BG_AbreForm FormResultadosNovo, "FormResultadosNovo"
        End Select
End Sub

Private Sub IDM_RESPONSAVEIS_Click()
    BG_AbreForm FormResponsaveis, "FormResponsaveis"
End Sub


Private Sub IDM_SAIR_Click(Index As Integer)
    Select Case Index
        Case 0
            ' MENU 01.08
            Unload Me
        Case 1
            
    End Select
End Sub


Sub IDM_SEGUINTE_Click()
    
    ' MENU 02.07
    gFormActivo.FuncaoSeguinte

End Sub


Private Sub IDM_ID_UTILIZADORES_Click()
    
    ' MENU 01.07.04
    BG_AbreForm FormIdUtilizadores, "FormIdUtilizadores"

End Sub




Private Sub IDM_SEP_SOROS_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormSepSoros, "FormSepSoros"
        Case 1
            BG_AbreForm FormEtiqSeroteca, "FormEtiqSeroteca"
        Case 2
            BG_AbreForm FormEtiqPreImpressas, "FormEtiqPreImpressas"
        Case 3
            BG_AbreForm FormEtiqAvulso, "FormEtiqAvulso"
        Case 4
            BG_AbreForm FormEtiqPorTubo, "FormEtiqPorTubo"
    End Select
End Sub


Private Sub IDM_SEROTECA_OPT_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormSerGestaoCaixas, "FormSerGestaoCaixas"
        Case 1
            BG_AbreForm FormSerAlteracaoEstados, "FormSerAlteracaoEstados"
    End Select
End Sub

' pferreira 2010.02.15
Private Sub IDM_STOCK_MENU_Click(Index As Integer)
    Select Case Index
        Case 0: BG_AbreForm FormControloStock, "FormControloStock"
        Case 1: BG_AbreForm FormStkMovimentos, "FormStkMovimentos"
    End Select
End Sub

Private Sub IDM_SUB_AUTORES_OPT_Click(Index As Integer)

    Select Case Index
        Case 0: IDM_AUTO_RES_Click
        Case 1: IDM_REL_ANA_AUTO_Click
    End Select
End Sub

Private Sub IDM_TERA_MED_Click()
    
    BG_AbreForm FormTeraMed, "FormTeraMed"

End Sub



Private Sub IDM_TIPOANULREC_Click(Index As Integer)
    
    Select Case Index
    Case 0
        BG_AbreForm FormTipoAnulamentoRecibos, "FormTipoAnulamentoRecibos"
    Case 1
        If gTipoInstituicao = "PRIVADA" Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
            If gSGBD = gOracle Then
                BG_AbreForm FormEntFinanceirasPRIVADO, "FormEntFinanceirasPRIVADO"
            ElseIf gSGBD = gSqlServer Then
                BG_AbreForm FormEntFinanceiras, "FormEntFinanceiras"
            End If
        ElseIf gTipoInstituicao = "HOSPITALAR" Then
            BG_AbreForm FormEntFinanceiras, "FormEntFinanceiras"
        End If
    Case 2
        BG_AbreForm FormCodGenero, "FormCodGenero"
    Case 3
        BG_AbreForm FormCodGenerico, "FormCodGenerico"
        FormCodGenerico.InicializaCodificacao "sl_racas", "Ra�as de Animais", "cod_raca", "descr_raca"
    Case 4
        BG_AbreForm FormCodGenerico, "FormCodGenerico"
        FormCodGenerico.InicializaCodificacao "sl_cod_local_entrega", "Locais de Entrega de Resultados", "cod_local_entrega", "descr_local_entrega"
    End Select
End Sub

Private Sub IDM_TRANS_PERMISSOES_Click()
    
    ' MENU 01.07.06
    BG_AbreForm FormTransPermissoes, "FormTransPermissoes"

End Sub

Private Sub IDM_TRANSFNUM_Click(Index As Integer)
    
    Select Case Index
    Case 0
        ' MENU 03.01.08.05
        BG_AbreForm FormTransferencia, "FormTransferencia"
    Case 1
        BG_AbreForm FormModifReq, "FormModifReq"
    Case 2
        BG_AbreForm FormCorreccaoRequis, "FormCorreccaoRequis"
    End Select
End Sub



Private Sub IDM_TUBOS_Click()
    
    BG_AbreForm FormTubos, "FormTubos"

End Sub

Private Sub IDM_TUBOS_SEC_Click()
    
    BG_AbreForm FormTubosSec, "FormTubosSec"

End Sub

Private Sub IDM_DELTA_CHECK_Click()
    
    BG_AbreForm FormDeltaCheck, "FormDeltaCheck"

End Sub



Private Sub IDM_VER_EST_Click(Index As Integer)
    Select Case Index
    
    Case 0
        BG_AbreForm FormVerificaEstados, "FormVerificaEstados"
    Case 1
        BG_AbreForm FormEstadosReq, "FormEstadosReq"
    End Select

End Sub

Private Sub IDM_VER_LOG_FILE_Click()
    
    ' MENU 01.07.08
    On Error Resume Next
    Dim X

    X = Shell("notepad.exe " & gDirCliente & "\bin\Logs\" & "SISLAB_LogFile.Log", 1)

End Sub

Private Sub IDM_VIGIL_EPID_Click(Index As Integer)
    Select Case Index
        Case 0
            BG_AbreForm FormCodifGruposEpid, "FormCodifGruposEpid"
        Case 1
            BG_AbreForm FormCodRegrasVigEpid, "FormCodRegrasVigEpid"
    End Select
End Sub

Private Sub MDIForm_DblClick()
    CallSislab
    'Timers_Timer 2
End Sub

Private Sub CallSislab()
    On Error GoTo ErrorHandler
    'BG_AbreForm FormAgendaV2, "FormAgendaV2"
'    FormInfoAna.EcSeqReqTubo.Text = "1567"
'    FormInfoAna.FuncaoProcurar
    'MICRO_retroativos
    'VE_TrataDadosAntigos
    'BG_AbreForm FormAnaAutomaticos, "FormAnaAutomaticos"
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CallSislab' in form FormFechoRequisicoes (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub



Sub MDIForm_Load()
    Dim menu As Object
    ' pferreira 2010.05.27
    
    If gLAB <> "HOSPOR" Then
        Set f_cSystray = mSystray.LoadSysTray(ImageList2.ListImages(13).Picture.handle)
    End If
    
    
    Me.caption = " " & cAPLICACAO_NOME
    DIC_Inicializacao_Idioma Me, gIdioma
    PreencheMenusLocais
    ' Se a base de dados n�o foi aberta ent�o
    ' implica que falhou nas inicializacoes da aplicacao ou
    ' na abertura da BD
    
    If Not (gConexao Is Nothing) Then
        If gConexao.state <> adStateClosed Then
            'Defenir parametrizacoes de acesso ao MDI
            If gUsaProfiles <> mediSim Then
                BG_ParametrizaPermissoes_ADO Me.Name
            End If
            'Verificar permissoes do utilizador sobre op��es na toolbar
            Toolbar2.Buttons("GESCOM").Visible = IDM_MENU_FERRAMENTAS(0).Visible
'            Toolbar2.Buttons("GESCOM").Visible = IDM_GESCOM.Visible
            Toolbar2.Buttons("SistemaInterrogacao").Visible = IDM_MENU_FERRAMENTAS(2).Visible
'            Toolbar2.Buttons("SistemaInterrogacao").Visible = IDM_SIA.Visible
            
            If Toolbar2.Buttons("GESCOM").Visible = True Then
'                Toolbar2.Buttons("GESCOM").Enabled = IDM_GESCOM.Enabled
                Toolbar2.Buttons("GESCOM").Enabled = IDM_MENU_FERRAMENTAS(0).Enabled
            End If
            
            If Toolbar2.Buttons("SistemaInterrogacao").Visible = True Then
'                Toolbar2.Buttons("SistemaInterrogacao").Enabled = IDM_SIA.Enabled
                Toolbar2.Buttons("SistemaInterrogacao").Enabled = IDM_MENU_FERRAMENTAS(2).Enabled
            End If
            If gPermiteMudarComputador = mediSim Then
                Toolbar2.Buttons("MudarComputador").Visible = True
            Else
                Toolbar2.Buttons("MudarComputador").Visible = False
            End If
            Toolbar2.Buttons("PESQFRASES").Visible = False
            'Separadores
            MDIFormInicio.Toolbar2.Buttons(7).Visible = False
            MDIFormInicio.Toolbar2.Buttons(8).Visible = False
            
        End If
    End If
    
    Dim rv As Integer

    ' Skins.
    Select Case gSKIN
        
        Case cANASIBAS
            rv = Transforma_ANASIBAS
            
        Case cSISLAB
            rv = Transforma_SISLAB
    
    End Select
    
    ' Sistema de factura��o.
    rv = Configura_Sistema_Faturacao()
    
    ' Verifica a op��o S� Valida��es M�dicas
'    If (gSoValMedica) Then
'        Call BL_So_Validacao_Medica
'    End If
    
    '-------------------------------------------------------
    ' Defini��es para impedir que se troque a ordem dos menus
    ' Defenir no sl_parama_acess conforme o utilizador ou grupo
    ' CoolBar1.FixedOrder = True
    ' CoolBar1.BandBorders = False
    ' MDIFormInicio.Toolbar1.Left = 100
    '-------------------------------------------------------
    
    If gMarcacoesPrevias = 1 Then MDIFormInicio.IDM_ID_DOENTES(2).Visible = True
    
    'If Dir$(gDirServidor & "\bin\Forms_Param.ini") <> "" Then
    '    AcessoMenus
    'End If
    
     ' pferreira 2007.11.14
    If (gActivarCorreio = mediSim) Then
        If (gTimerCorreio <> Empty And gTimerCorreio <> -1) Then: Timers(TimersEnum.e_timer_correio).Interval = gTimerCorreio
        BL_VerificaMsgCorreio
    End If
    
End Sub

Private Sub MDIForm_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Shift = 1 Then
        'BG_AbreForm FormGeralReq, "FormGeralReq"
    End If
End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error GoTo TrataErro
    If UnloadMode <> 1 Then
        If BG_Mensagem(mediMsgBox, "Deseja realmente sair do SISLAB ?", vbQuestion + vbYesNo, "Sislab") = vbNo Then
            'respondeu n�o
            Cancel = True
        End If
    End If
    If gLAB <> "HOSPOR" Then
        MDIFormInicio.f_cSystray.SysTrayShow False
        Set MDIFormInicio.f_cSystray = Nothing
    End If
Exit Sub
TrataErro:
End Sub

Sub MDIForm_Unload(Cancel As Integer)
    
    Dim j As Integer
    
    'Limpar o cursor animado
    BL_FimProcessamento Me
    DestroyCursor (CursorNor)
    DestroyCursor (CursorAni)
    If Not (gConexao Is Nothing) Then
        If gConexao.state <> adStateClosed Then
            RegistoAcessos_Saida
            'NELSONPSILVA 04.04.2018 Glintt-HS-18011
            If gATIVA_LOGS_RGPD = mediSim Then
                BL_REGISTA_LOGS_RGPD -1, BL_DevolveUtilizadorAtual(CStr(gCodUtilizador)), BG_SYS_GetComputerName, "", "", BL_DevolveTipOperecao(TipoOperacao.sa�da_apl) & " - " & MDIFormInicio.Name, _
                Bg_DaData_ADO & " " & Bg_DaHora_ADO, Bg_DaData_ADO & " " & Bg_DaHora_ADO, "", "{}", "{}"
            End If
        
            'Fazer o commit da transa��o caso ela exista
            BG_RollbackTransaction
            
            'Fechar a base de dados
            BG_Fecha_ConexaoBD_ADO
        End If
    End If
    
    'Fechar a conex�o ao HIS (caso exista)
    If Not gConnHIS Is Nothing Then
        If gConnHIS.state <> adStateClosed Then
            gConnHIS.Close
        End If
        Set gConnHIS = Nothing
    End If
    
    '!Salvaguardar a exist�ncia de forms n�o descarregados da mem�ria
    For j = 0 To forms.Count - 1
        ' Mart.
        On Error Resume Next
        If forms(j).Name <> Me.Name Then
            Unload forms(j)
        End If
    Next j
    
    Set MDIFormInicio = Nothing


End Sub

Private Sub MenuList_Click(Index As Integer)
    
    Dim j As Long
    
    If Trim(gFormActivo.Name) = "FormResultados" Then
        Call FormResultados.CriaNovaLinha(Mid(MenuList(Index).caption, 1, InStr(1, MenuList(Index).caption, " - ")), j)
    ElseIf Trim(gFormActivo.Name) = "FormResultadosNovo" Then
        Call FormResultadosNovo.InsereMembro(Mid(MenuList(Index).caption, 1, InStr(1, MenuList(Index).caption, " - ")), FormResultadosNovo.FgRes.row)
    ElseIf Trim(gFormActivo.Name) = "FormResMicro" Then
        Call FormResMicro.CriaNovaLinha(Mid(MenuList(Index).caption, 1, InStr(1, MenuList(Index).caption, " - ")), j)
    ElseIf Trim(gFormActivo.Name) = "FormGestaoRequisicao" Then
        Call FormGestaoRequisicao.InsereGhostMember(Mid(MenuList(Index).caption, 1, InStr(1, MenuList(Index).caption, " - ")))
    ElseIf Trim(gFormActivo.Name) = "FormChegadaPorTubos" Then
'        Call FormChegadaPorTubos.DesactivaChegadaTubo
    ElseIf Trim(gFormActivo.Name) = "FormGestaoRequisicaoPrivado" Then
        Call FormGestaoRequisicaoPrivado.InsereGhostMember(Mid(MenuList(Index).caption, 1, InStr(1, MenuList(Index).caption, " - ")))
    End If

End Sub

Private Sub SEP_MENU_FICHEIRO_4_Click(Index As Integer)
    Select Case Index
        Case 1
        
        FormVerificarUtilizador.Show vbModal
        
        MDIFormInicio.Show
        MDIForm_Load
        ' Fim de Inicializar variaveis globais ------------------------------------
        
        
        
        AplicacaoVazia False
        Set gFormActivo = MDIFormInicio
        BL_ToolbarEstadoN 0
        BG_LimpaPassaParams
        BG_StackJanelas_Inicializa
        
        BG_LogFile_Erros "Utilizador: " & gCodUtilizador & " - " & gIdUtilizador & vbCrLf
        gMudouUtilizador = True
        ActualizarEntradaNaAplicacao
        If gUsaProfiles <> mediSim Then
            BG_ParametrizaPermissoes_ADO "MDIFormInicio"
        End If
        BL_AplicaProfile gProfileUtilizador
        'Inicializa o Handle que cont�m a Form MDICLIENT para os Reports!!
        gParentChild = FindWindowEx(MDIFormInicio.hwnd, ByVal 0&, "MDICLIENT", vbNullString)
        
        'Inicializa o handle com o cursor normal
        CursorNor = GetClassLong(ByVal MDIFormInicio.hwnd, GCL_HCURSOR)
        
        'Impress�o de hist�rico de an�lises no boletim de resultados
        gImp_Res_Ant = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "IMP_RES_ANT")
        If gImp_Res_Ant = -1 Then gImp_Res_Ant = vbUnchecked
    
        If Trim(BG_DaParamAmbiente_ADO(mediAmbitoUtilizador, "ESTADO_MENU_PERSONALIZADO")) = "ABERTO" Then
            FormMenusPersonalizados.Show
            FormMenusPersonalizados.Inicializacoes
        End If
        
        DoEvents
        
        'Ajustar barra para o form ficar todo visivel
        MDIFormInicio.CoolBar1.Bands(1).Style = cc3BandFixedSize
        MDIFormInicio.CoolBar1.Bands(1).Style = cc3BandNormal
        MDIFormInicio.CoolBar1.Bands(1).Width = 8700
        MDIFormInicio.CoolBar1.Bands(2).Width = 2955
        
        If gBloqueiaAplicacao = mediSim Then
            MDIFormInicio.IDLETimer1.MinutosDeIntervalo = gTimeOutMinutos
        Else
            'MDIFormInicio.IDLETimer1.MinutosDeIntervalo = 1000
        End If
        'SOLIVEIRA 09.10.2007
        If Command <> "" Then
            BG_AbreForm FormGestaoRequisicao, "FormGestaoRequisicao"
        End If
    End Select
End Sub

Private Sub StatusBar1_PanelClick(ByVal Panel As Panel)
    
    If Panel.key = "Mensagem" Then
        BG_LimpaMensagem
    End If
    
End Sub

Private Sub StatusBar1_PanelDblClick(ByVal Panel As MSComctlLib.Panel)
    If Panel.key = "RES_CRIT" Then
        BG_AbreForm FormResCriticos, "FormResCriticos"
    End If
End Sub

Private Sub TimerAlertas_Timer()
  
    On Error GoTo TrataErro
    
    Dim Estado As estado_alertas
    
    If gCodUtilizador <= 0 Or gAtiva_Alertas <> mediSim Then
        Exit Sub
    End If
    
    If gAtivarAlertasTimer > 0 Then
        contador_timer_alertas = contador_timer_alertas + 1
    
        If contador_timer_alertas * TimerAlertas.Interval / 60000 > gAtivarAlertasTimer Then
            contador_timer_alertas = 0
            BL_Executa_Procedimento_Alertas (False)
            eAlertas = BL_Devolve_Existem_Alertas_Activos()
        End If
    End If
    ActualizarIconeAlertas eAlertas
Exit Sub
TrataErro:
        BG_LogFile_Erros "Timer_Alertas_Tick - ERRO: " & Err.Description
        Exit Sub
        Resume Next
  
  
End Sub

Private Sub Timers_Timer(Index As Integer)
    Dim i As Integer
    Select Case (Index)
        
        Case TimersEnum.e_timer_mensagens:
            BG_LimpaMensagem
            MDIFormInicio.Timers(TimersEnum.e_timer_mensagens).Interval = 0
        Case TimersEnum.e_timer_correio:
            If (gActivarCorreio = mediSim) Then: BL_VerificaMsgCorreio
        Case TimersEnum.e_timer_resultados_Criticos
        
        Case Else:  ' Empty
    End Select
End Sub

Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Call Registo(Button.key)
    
End Sub

Sub Toolbar2_ButtonClick(ByVal Button As Button)

    On Error GoTo Trata_Erro
    
    If Button.key = "DataActual" Then
        gFormActivo.Funcao_DataActual
        gFormActivo.SetFocus
    
    ElseIf Button.key = "SistemaInterrogacao" Then
        Call IDM_MENU_FERRAMENTAS_02
    
    ElseIf UCase(Button.key) = "COPIANUTE" Then
        gFormActivo.Funcao_CopiaUte
        gFormActivo.SetFocus
    ElseIf UCase(Button.key) = "COPIANREQ" Then
        gFormActivo.Funcao_CopiaReq
        gFormActivo.SetFocus
    ElseIf UCase(Button.key) = "MENUSPERSONALIZADOS" Then
        BL_ActualizaEstadoMenusPersonalizados "ABERTO"
        FormMenusPersonalizados.Show
    
    ElseIf UCase(Button.key) = "GESCOM" Then
        Call IDM_MENU_FERRAMENTAS_00
    ElseIf UCase(Button.key) = "PESQFRASES" Then
        Call BL_PesquisaFrases
    ElseIf UCase(Button.key) = "MUDARGRANA" Then
        FormMudarGrAnalises.Show
        FormMudarGrAnalises.caption = "Mudar Grupo de An�lises"
        FormMudarGrAnalises.CbComputador.Visible = False
        FormMudarGrAnalises.CbPostoColhe.Visible = False
        FormMudarGrAnalises.CbGrAnalises.Visible = True
    ElseIf UCase(Button.key) = "MUDARCOMPUTADOR" Then
        FormMudarGrAnalises.Show
        FormMudarGrAnalises.caption = "Associar Computador"
        FormMudarGrAnalises.CbComputador.Visible = True
        FormMudarGrAnalises.CbGrAnalises.Visible = False
        FormMudarGrAnalises.CbPostoColhe.Visible = False
    ElseIf UCase(Button.key) = "MUDARPOSTOCOLHE" Then
        FormMudarGrAnalises.Show
        FormMudarGrAnalises.caption = "Alterar Posto de Colheita"
        FormMudarGrAnalises.CbComputador.Visible = False
        FormMudarGrAnalises.CbGrAnalises.Visible = False
        FormMudarGrAnalises.CbPostoColhe.Visible = True
    ' pferreira 2010.02.15
    ElseIf Button.key = "Correio" Then
        If gVersaoModuloCorreio = "V2" Then
            BG_AbreForm FormMessages, "FormMessages"
        Else
            BG_AbreForm FormCorreio, "FormCorreio"
        End If
    ElseIf UCase(Button.key) = "SINAVE" Then
        If gSinave = mediSim And gCaminhoSinave <> "" And gCaminhoSinave <> "-1" Then
            Shell gCaminhoSinave & ";" & gCodUtilizador
        End If
    ElseIf UCase(Button.key) = "PRESCRICOES" Then
        If gRequisicaoActiva > 0 Then
            FormGestaoRequisicao.Enabled = False
            FormPrescricoesAssociadas.Show
            FormPrescricoesAssociadas.EcNumReq = gRequisicaoActiva
            FormPrescricoesAssociadas.FuncaoProcurar
        End If
    'NELSONPSILVA 12.01.2017 UnimedVitoria-1152
    ElseIf Button.key = "Mis_data_discovery" Then
        FormEscolheIndicador.Show
        FormEscolheIndicador.caption = "Escolha de Indicador"
        FormEscolheIndicador.CbGrIndicador.Visible = True
    ElseIf Button.key = "ALERTAS" And eAlertas.existem_alertas = True Then
        FormAlertas.Show
    End If
    
    Exit Sub
    
Trata_Erro:
    BG_Mensagem mediMsgStatus, "Opera��o n�o disponivel!", vbOKOnly
    BG_LogFile_Erros "Toolbar2_ButtonClick: " & Err.Description

End Sub

Public Sub Registo(ByVal key As String)
    
    On Local Error Resume Next
    
    MDIFormInicio.ActiveForm.ValidateControls
    
    'Cancel=True?
    If Err.Number = 380 Then
        Err.Clear
        Exit Sub
    End If
    
    Select Case key
        Case "Imprimir"
            gImprimirDestino = 1
            gFormActivo.FuncaoImprimir
        Case "ImprimirVerAntes"
            gImprimirDestino = 0
            gFormActivo.FuncaoImprimir
        Case "Limpar"
            gFormActivo.FuncaoLimpar
        Case "Inserir"
            gFormActivo.FuncaoInserir
        Case "Procurar"
            gFormActivo.FuncaoProcurar
        Case "Modificar"
            gFormActivo.FuncaoModificar
        Case "Remover"
            gFormActivo.FuncaoRemover
        Case "Anterior"
            gFormActivo.FuncaoAnterior
        Case "Seguinte"
            gFormActivo.FuncaoSeguinte
        Case "EstadoAnterior"
            gFormActivo.FuncaoEstadoAnterior
        Case "Sair"
            Unload Me
    End Select
    
End Sub

Private Sub IDM_MENU_AJUDA_00()
    
    Dim iRes As Variant
    Dim sTemp1 As String
    Dim sTemp2 As String

    On Error GoTo TrataErro

    sTemp1 = GetSetting(cREGISTRY_NOME, "Geral", "AplicacaoParaAjuda", "Nada")
    sTemp2 = GetSetting(cREGISTRY_NOME, "Geral", "FicheiroDeAjuda", "Nada")

    iRes = Shell(sTemp1 & " " & sTemp2, 1)

    Exit Sub
    
TrataErro:
    Beep
    BG_LogFile_Erros "Erro na abertura do Help. AplicacaoParaAjuda=" & sTemp1 & "; FicheiroDeAjuda=" & sTemp2
    Exit Sub
End Sub

Private Sub IDM_MENU_AJUDA_02()
    
    FormSobre.Show vbModal
'Form1.Show
End Sub

Private Sub IDM_MENU_FERRAMENTAS_00()
    
    Dim X
    Dim sTemp As String
    Dim pathGescom As String
    
    On Error Resume Next

    pathGescom = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "DIR_GESCOM")

    If (pathGescom <> "-1") Then
        sTemp = pathGescom & "GESCOM " & gIdUtilizador & " " & gCodUtilizador & " " & gCodGrupo
        BL_InicioProcessamento MDIFormInicio, "A iniciar GESCOM..."
        X = Shell(sTemp, vbNormalFocus)
    Else
        MsgBox "Op��o n�o dispon�vel.           ", vbExclamation, "GESCOM"
    End If
    
    BL_FimProcessamento MDIFormInicio
    
End Sub

Private Sub IDM_MENU_FERRAMENTAS_02()

    ' MENU 07.02
    Dim X
    Dim sTemp As String
    
    On Error Resume Next
    
    gSistemaInterrogacao = BG_DaParamAmbiente_NEW(mediAmbitoGeral, "SISTEMA_INTERROGACAO")
    sTemp = gSistemaInterrogacao & " " & cREGISTRY_NOME & " " & Right(gBDPwd, Len(gBDPwd) - InStr(gBDPwd, "="))

    X = Shell(sTemp, vbNormalFocus)

End Sub

Sub IDM_MENU_FERRAMENTAS_04()
    
    Dim X
    On Error Resume Next
    X = Shell("calc.exe", vbNormalFocus)

End Sub

Private Sub IDM_MENU_FERRAMENTAS_05()
    
    Dim X

    On Error Resume Next

    X = Shell("notepad.exe", vbNormalFocus)

End Sub

Private Sub AcessoMenus()
    Dim conteudoFx As String
    Dim linha As String
    Dim nomeMenu As String
    Dim propriedadeMenu As String
    Dim ValorPropriedade As String
    Dim strIniGrp As String
    Dim strFimGrp As String
    Dim indice As String
    Dim F As Integer
    Dim i As Integer
    Dim posInicialGrupo As Single
    Dim posFinalGrupo As Single
    Dim posP1 As Single
    Dim posP2 As Single
    Dim PosAux As Single
    Dim saiCiclo As Boolean

    On Error GoTo TrataErro

    F = FreeFile
    Open gDirServidor & "\bin\Forms_Param.ini" For Input As #F
    linha = ""

    While Not EOF(F)
        Line Input #F, linha
        conteudoFx = conteudoFx & linha
        linha = ""
    Wend

    Close F

    strIniGrp = "[I" & CStr(gCodGrupo) & "]"
    strFimGrp = "[F" & CStr(gCodGrupo) & "]"
    posInicialGrupo = InStr(1, conteudoFx, strIniGrp)
    posFinalGrupo = InStr(posInicialGrupo, conteudoFx, strFimGrp)
    i = posInicialGrupo

    saiCiclo = False
    While i <= posFinalGrupo And Not saiCiclo
        PosAux = InStr(i, conteudoFx, "nome" & CStr(gCodGrupo))
        posP1 = InStr(PosAux, conteudoFx, "|")
        posP2 = InStr(posP1 + 1, conteudoFx, "|")
        nomeMenu = Mid(conteudoFx, posP1 + 1, posP2 - posP1 - 1)

        PosAux = InStr(i, conteudoFx, "propriedade" & CStr(gCodGrupo))
        posP1 = InStr(PosAux, conteudoFx, "|")
        posP2 = InStr(posP1 + 1, conteudoFx, "|")
        propriedadeMenu = Mid(conteudoFx, posP1 + 1, posP2 - posP1 - 1)
    
        PosAux = InStr(i, conteudoFx, "estado" & CStr(gCodGrupo))
        posP1 = InStr(PosAux, conteudoFx, "|")
        posP2 = InStr(posP1 + 1, conteudoFx, "|")
        ValorPropriedade = Mid(conteudoFx, posP1 + 1, posP2 - posP1 - 1)

        AtribuirPermissoesMenus nomeMenu, propriedadeMenu, ValorPropriedade

        i = InStr(PosAux, conteudoFx, "nome" & CStr(gCodGrupo))

        If i <= 0 Then
            saiCiclo = True
        End If
    Wend

    i = i
    
    Exit Sub
TrataErro:
    Exit Sub
    Resume Next
End Sub

Private Sub AtribuirPermissoesMenus(nomeMenu As String, _
                                    propriedadeMenu As String, _
                                    ValorPropriedade As String)

    Dim indice As Integer
    Dim p1 As Integer
    Dim p2 As Integer

    On Error GoTo TrataErro

    If InStr(1, nomeMenu, "(") Then
        p1 = InStr(1, nomeMenu, "(")
        p2 = InStr(p1, nomeMenu, ")")
        indice = CInt(Mid(nomeMenu, p1 + 1, p2 - p1 - 1))
        nomeMenu = Mid(nomeMenu, 1, p1 - 1)
    End If

    Select Case nomeMenu

        Case "IDM_RES"
            If propriedadeMenu = "Visible" And ValorPropriedade = "True" Then
                IDM_RES(indice).Visible = True
            ElseIf propriedadeMenu = "Visible" And ValorPropriedade = "False" Then
                IDM_RES(indice).Visible = False
            ElseIf propriedadeMenu = "Enabled" And ValorPropriedade = "True" Then
                IDM_RES(indice).Enabled = True
            ElseIf propriedadeMenu = "Enabled" And ValorPropriedade = "False" Then
                IDM_RES(indice).Enabled = False
            End If
            
        Case "IDM_REQPEND_GRUPO"
            If propriedadeMenu = "Visible" And ValorPropriedade = "True" Then
                IDM_REQPEND_GRUPO(indice).Visible = True
            ElseIf propriedadeMenu = "Visible" And ValorPropriedade = "False" Then
                IDM_REQPEND_GRUPO(indice).Visible = False
            ElseIf propriedadeMenu = "Enabled" And ValorPropriedade = "True" Then
                IDM_REQPEND_GRUPO(indice).Enabled = True
            ElseIf propriedadeMenu = "Enabled" And ValorPropriedade = "False" Then
                IDM_REQPEND_GRUPO(indice).Enabled = False
            End If
            
        Case "IDM_PROTOCOLOS"
            If propriedadeMenu = "Visible" And ValorPropriedade = "True" Then
                IDM_PROTOCOLOS(indice).Visible = True
            ElseIf propriedadeMenu = "Visible" And ValorPropriedade = "False" Then
                IDM_PROTOCOLOS(indice).Visible = False
            ElseIf propriedadeMenu = "Enabled" And ValorPropriedade = "True" Then
                IDM_PROTOCOLOS(indice).Enabled = True
            ElseIf propriedadeMenu = "Enabled" And ValorPropriedade = "False" Then
                IDM_PROTOCOLOS(indice).Enabled = False
            End If
            
        Case "IDM_ANALISES_T"
            If propriedadeMenu = "Visible" And ValorPropriedade = "True" Then
                IDM_ANALISES_T(indice).Visible = True
            ElseIf propriedadeMenu = "Visible" And ValorPropriedade = "False" Then
                IDM_ANALISES_T(indice).Visible = False
            ElseIf propriedadeMenu = "Enabled" And ValorPropriedade = "True" Then
                IDM_ANALISES_T(indice).Enabled = True
            ElseIf propriedadeMenu = "Enabled" And ValorPropriedade = "False" Then
                IDM_ANALISES_T(indice).Enabled = False
            End If
    End Select
    
    
    Exit Sub
    
TrataErro:
    
End Sub


Private Sub IDM_ANA_Click(Index As Integer)
    Select Case Index
        Case 0 ' ACRESCENTAR AN�LISE
            If gTipoInstituicao = "HOSPITALAR" Then
                FormGestaoRequisicao.AcrescentaAnalise (FormGestaoRequisicao.FGAna.RowSel)
            ElseIf gTipoInstituicao = "PRIVADA" Then
                'soliveira LJM 29.10.2008
                If gF_REQUIS_PRIVADO = 1 Then
                    FormGestaoRequisicaoPrivado.AcrescentaAnalise (FormGestaoRequisicaoPrivado.FGAna.row)
                Else
                    FormFactusEnvio.AcrescentaLinha (FormFactusEnvio.FGAnalises.row)
                End If
            ElseIf gTipoInstituicao = "VETERINARIA" Then
                FormGestaoRequisicaoVet.AcrescentaAnalise (FormGestaoRequisicaoPrivado.FGAna.row)
            ElseIf gTipoInstituicao = gTipoInstituicaoAguas Then
                FormGestaoRequisicaoAguas.AcrescentaAnalise (FormGestaoRequisicaoPrivado.FGAna.row)
            End If
        Case 1 ' ELIMINAR LINHA
            If gTipoInstituicao = "HOSPITALAR" Then
                Call FormGestaoRequisicao.FgAna_KeyDown(46, 0)
            ElseIf gTipoInstituicao = "PRIVADA" Then
                'soliveira LJM 29.10.2008
                If gF_REQUIS_PRIVADO = 1 Then
                    Call FormGestaoRequisicaoPrivado.FgAna_KeyDown(46, 0)
                Else
                    Call FormFactusEnvio.EliminaLinha(FormFactusEnvio.FGAnalises.row)
                End If
            ElseIf gTipoInstituicao = gTipoInstituicaoVet Then
                'soliveira LJM 29.10.2008
                Call FormGestaoRequisicaoVet.FgAna_KeyDown(46, 0)
            ElseIf gTipoInstituicao = gTipoInstituicaoAguas Then
                'soliveira LJM 29.10.2008
                Call FormGestaoRequisicaoAguas.FgAna_KeyDown(46, 0)
            End If
    End Select

End Sub



' pferreira 2010.02.15
Private Sub IDM_CORREIO_MARCAR_Click(Index As Integer)

    Select Case Index
        Case 2:
            FormMessages.ChangeMessageState (ModuleMessages.MessageStateArchive)
        Case 4:
            
             FormMessages.ChangeMessageState (ModuleMessages.MessageStateDelete)
        Case 6:
            
            FormMessages.SelectAll
    End Select
     
End Sub

' pferreira 2010.02.15
Private Sub IDM_CORREIO_MARCAR_OPT_Click(Index As Integer)

    Select Case Index
        Case 0:
            FormMessages.ChangeMessageState (ModuleMessages.MessageStateRead)
        Case 1:
            
            FormMessages.ChangeMessageState (ModuleMessages.MessageStateUnread)
    End Select
     
End Sub

Private Sub IDM_RESU_NEW_VAL_click(Index As Integer)
    Select Case Index
        Case 0 ' NAO VALIDAR ANALISE
            FormResultadosNovo.NaoValidaranalise
        Case 1 ' NAO VALIDAR ATE AQUI
            FormResultadosNovo.NaoValidarAteAqui
        Case 2 ' VALIDAR ATE AQUI
            FormResultadosNovo.ValidarAteAqui
        Case 3 ' VALIDAR SO ESTA ANALISE
            FormResultadosNovo.ValidarSoEstaAna
        Case 4 ' RETIRAR RESTRICOES
            FormResultadosNovo.RetiraRestricoesAnalise
        Case 5
            FormResultadosNovo.ColocaAnaPendente
        Case 6
            FormResultadosNovo.BloquearAnalise
        Case 7
            FormResultadosNovo.DesbloquearAnalise
    End Select
End Sub

Private Sub IDM_RESU_NEW_REP_click(Index As Integer)
    Select Case Index
        Case 0 ' REPETIR ESTA ANALISE
            FormResultadosNovo.RepeteAnalise
        Case 1 ' CONSULTAR REPETICOES
            FormResultadosNovo.PreencheRepeticoes
        Case 2 ' CONSULTAR TODAS REPETICOES
            FormResultadosNovo.PreencheRepeticoesTodas
        Case 3 ' Repetir at� esta an�lise
            FormResultadosNovo.RepeteAnaliseAteEsta
        End Select
End Sub

Private Sub IDM_RESU_NEW_OBS_click(Index As Integer)
    Select Case Index
        Case 0 ' OBSERVACOES DA ANALISE
            FormResultadosNovo.MostraObsAna
        Case 1 ' MOSTRAR OBSERVACOES DO REGISTO
            FormResultadosNovo.MostraObsReg
        Case 2 ' Enviar Mensagem sobre an�lise
            FormResultadosNovo.EnviaMensagemAna
    End Select
End Sub

Private Sub IDM_RESU_NEW_RES_click(Index As Integer)
    Select Case Index
        Case 0 ' COLOCAR SEM RESULTADO
            FormResultadosNovo.SemResultado cSemResultado
        Case 1 ' COLOCAR SEM RESULTADO EM TODAS ANALISES
            FormResultadosNovo.SemResultadoTodasAnalises
        Case 2 ' COLOCAR VALOR POR DEFEITO
            FormResultadosNovo.ValorPorDefeito
            
        Case 3 ' ALTERNA RESULTADOS ANTERIORES
            FormResultadosNovo.AlternaResultadosAnteriores
        Case 4 ' MARCA 1 RESULTADO PARA NAO IMPRIMIR
            FormResultadosNovo.NaoImprimirResultado 1
        Case 5 ' MARCA 2 RESULTADO PARA NAO IMPRIMIR
            FormResultadosNovo.NaoImprimirResultado 2
        Case 7 ' CONSULTA DE ALTERACOES DE RESULTADOS
            FormResultadosNovo.ConsultaAlteracoes
    End Select
End Sub
Private Sub IDM_RESU_NEW_Click(Index As Integer)
    Select Case Index
        Case 0 ' ANALISE
            If InStr(1, IDM_RESU_New(0).caption, " ") > 0 Then
                FormResultadosNovo.SaltaCodificacaoAna (Mid(IDM_RESU_New(0).caption, 1, InStr(1, IDM_RESU_New(0).caption, " ") - 1))
            End If
        Case 4 ' OPERADOR
            FormResultadosNovo.MostraOperador
        Case 6 ' MOSTRA VOLUME DO PRODUTO
            FormResultadosNovo.MostraVolume
        Case 7 ' CONSULTAR GR�FICO
        Case 8 ' CONSULTAR HIST�RICO DA ANALISE
            FormResultadosNovo.HistoricoAnalise
        Case 9 ' ADICIONAR ANALISE
            FormResultadosNovo.AdicionaAnalise
        Case 10 ' ADICIONAR ANALISE top 1
            FormResultadosNovo.AdicionaAnaliseEspecif gCodAnaSimples1, False
        Case 11 ' ADICIONAR ANALISE top 2
            FormResultadosNovo.AdicionaAnaliseEspecif gCodAnaSimples2, False
        Case 13
            FormResultadosNovo.MostraDiario
    End Select
End Sub

Private Sub IDM_RESU_GRAF_click(Index As Integer)
    Select Case Index
        Case 0
            FormResultadosNovo.ConsultarGrafico
        Case 1
            FormResultadosNovo.NovoGrafico
        Case 2
            FormResultadosNovo.ApagaGrafico
    End Select
End Sub

Private Sub IDM_TUBO_Click(Index As Integer)
    Select Case Index
        Case 0 ' Desactiva chegada de tubo
            If gVersaoChegadaTubos = "V2" Then
                FormChegadaPorTubosV2.Desactiva_ChegadaTubo
            Else
                FormChegadaPorTubos.Desactiva_ChegadaTubo (FormChegadaPorTubos.FgTubos.RowSel)
            End If
        Case 1
            If gVersaoChegadaTubos = "V2" Then
                FormChegadaPorTubosV2.PedidoNovaAmostra
            End If
        Case 2
            If gVersaoChegadaTubos = "V2" Then
                FormChegadaPorTubosV2.DesactivaColheitaTubo
            End If
        Case 3
            If gVersaoChegadaTubos = "V2" Then
                FormChegadaPorTubosV2.RemarcaTubo
            End If
    End Select
End Sub

Private Sub IDM_NEW_REC_DOC_CLICK(Index As Integer)
    Dim res As Boolean
    res = True
    FormEmissaoRecibos.EcAdiantamento = "0"
    Select Case Index
        Case 0 'recibo
            If BG_Mensagem(mediMsgBox, "Deseja gerar  o recibo da entidade " & FormEmissaoRecibos.FgEntidades.TextMatrix(FormEmissaoRecibos.linha_ant, 1) & "?", vbYesNo, "Imprimir Recibo") = vbYes Then
                res = RECIBO_GeraReciboDetalhe(FormGestaoRequisicaoPrivado.EcNumReq, FormEmissaoRecibos.linhaRegistosR, 2, FormGestaoRequisicaoPrivado.RegistosA, _
                                  FormGestaoRequisicaoPrivado.RegistosR, FormGestaoRequisicaoPrivado.RegistosRM, FormEmissaoRecibos.flg_adiantamentos)
            End If
        Case 1 'documento caixa
            If BG_Mensagem(mediMsgBox, "Deseja gerar  o documento caixa da entidade " & FormEmissaoRecibos.FgEntidades.TextMatrix(FormEmissaoRecibos.linha_ant, 1) & "?", vbYesNo, "Imprimir Recibo") = vbYes Then
                res = RECIBO_GeraReciboDetalhe(FormGestaoRequisicaoPrivado.EcNumReq, FormEmissaoRecibos.linhaRegistosR, 1, FormGestaoRequisicaoPrivado.RegistosA, _
                                  FormGestaoRequisicaoPrivado.RegistosR, FormGestaoRequisicaoPrivado.RegistosRM, FormEmissaoRecibos.flg_adiantamentos)
            End If
    End Select
    If res = False Then
        BG_Mensagem mediMsgBox, "Erro ao gerar recibo.", vbOKOnly + vbCritical, "RECIBO_GeraRecibo"
    End If
End Sub

Private Sub IDM_FACT_ANA_click(Index As Integer)
    Select Case Index
        Case 0 'recibo
            If BG_Mensagem(mediMsgBox, "Deseja ReActivar a An�lise?", vbYesNo, "Activar An�lise") = vbYes Then
                FormFactusEnvio.Reactiva_Analise (FormFactusEnvio.FGAnalises.row)
            End If
    End Select
End Sub



' pferreira 2010.05.27
Private Sub f_cSystray_MouseUp(Button As Integer)
    
    mSystray.ShowMenu Me.f_cSystray, MDIFormInicio, MDIFormInicio.IDM_MENU_FICHEIRO
    
End Sub


Private Sub IDM_local_click(Index As Integer)
    FormChegadaPorTubosV2.MudaLocalAnaAcrescentadaAnalise CLng(Index)
End Sub


Private Sub PreencheMenusLocais()
    Dim i As Integer
    
    If Me.IDM_LOCAL.Count > 0 Then
        Exit Sub
    End If
    
    For i = 1 To gTotalLocais
        If i > 1 Then
            Load MDIFormInicio.IDM_LOCAL(i - 1)
            MDIFormInicio.IDM_LOCAL(i - 1).Visible = True
        End If
        MDIFormInicio.IDM_LOCAL(i - 1).caption = gEstruturaLocais(i).descr_local
    Next
End Sub
Private Sub IDM_AUTO_RES_Click()
    
    BG_AbreForm FormAutoRes, "FormAutoRes"

End Sub
'edgar.parada BACKLOG -11940 (COLHEITAS) 24.01.2019
Friend Sub ActualizarIconeAlertas(ByRef al As alertas)
    
    Dim i As Integer
     
    On Error GoTo TrataErro
    
    'estrutAlertas = al
    
    If al.existem_alertas = False Then
        Estado = estado_alertas.Verde
    Else
        If al.existem_alertas_por_visualizar Then
            If Estado = estado_alertas.cinzento Then
                Estado = estado_alertas.vermelho
            Else
                Estado = estado_alertas.cinzento
            End If
        Else
            Estado = estado_alertas.Amarelo
        End If
    End If
    
    Select Case Estado
        Case estado_alertas.cinzento
            Toolbar2.Buttons(25).Image = 21
        Case estado_alertas.Verde
            Toolbar2.Buttons(25).Image = 22
        Case estado_alertas.Amarelo
            Toolbar2.Buttons(25).Image = 24
        Case estado_alertas.vermelho
            Toolbar2.Buttons(25).Image = 23
        Case Else
            Toolbar2.Buttons(25).Image = 21
    End Select
    DoEvents
    DoEvents
    DoEvents
    DoEvents
    DoEvents
    DoEvents
    Exit Sub
TrataErro:
        BG_LogFile_Erros "MdiFormInicio - ActualizarIconeAlertas - ERRO: " & Err.Description
End Sub


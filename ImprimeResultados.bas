Attribute VB_Name = "ImprimeResultados"
Option Explicit
Dim params() As String

Private Type TipoSala
    cod_sala As String
    descr_sala As String
End Type
Dim descr_especif As String
Dim obs_especif As String

Private Type dadosFormula
    DataChegada As String
    HoraChegada As String
    DataColheita As String
    NumEpisodio As String
    NomeUtente As String
    nrBenef As String
    sala As TipoSala
    isencao As String
    ObsAnaReq As String
    ObsAutoBloq As String
    cod_destino As Integer
    flg_fax_destino As Integer
    destino As String
    DtEmissao As String
    HrEmissao As String
    DtSVia As String
    HrSVia As String
    SitReq As String
    CodProv As String
    ProvReq As String
    ObsProven As String
    NomeMed As String
    TipoUtente As String
    NumUtente As String
    CodSexo As String
    descrProfis As String
    SexoUtente As String
    DtNascUtente As String
    SeqUtente As String
    ficha As String
    NReqAssoc As String
    UserAssinatura As String
    MoradaUtente As String
    CodPostalUtente As String
    numExt As String
    dtImprCartao As String
    codGenero As String
    descr_genero As String
    DescrEFR As String
    MoradaEFR As String
    codPostalEFR As String
    NumProcUtente As String
    ExisteSegVia As Boolean
    DtConclusao As String
    DtPretend As String
    DtFecho As String
    nomeRelatorio As String
    tubosAtrasados As String
    localRequis As String
    DiasUtente As Long
    nome_dono As String
    cod_raca As String
    descr_raca As String
    Produtos As String
    Comentario As String
    FlagComent As String
    respTecnicos As String
    RespMedicos As String
    AnalisesPendentes As String
    AnalisesPendentesTubo As String
    aux_coment_assinatura As String
    totalExterior As Integer
    totalAcreditadas As Integer
    totalNaoAcreditadas As Integer
    totalPagar  As Double
    VerTabelaAnexo As Boolean
    DtValidacao As String
    HrValidacao As String
    codLocalEntrega As String
    descrLocalEntrega As String
    obs_req As String
    'AGUAS
    cod_tipo_amostra As String
    descr_tipo_amostra As String
    cod_origem_amostra As String
    descr_origem_amostra As String
    local_colheita As String
    cod_ponto_colheita As String
    descr_ponto_colheita As String
    responsavel_colheita As String
    'RGONCALVES 10.12.2015 CHSJ-2511
    totalAcreditadasCodAgrup As Integer
    totalNaoAcreditadasCodAgrup As Integer
    '
End Type

Private Type DadosAnalises
    codAnaS As String
    DescrAnaS As String
    CodAnaC As String
    DescrAnaC As String
    codPerfis As String
    descrPerfis As String
End Type
Dim analisesImpr() As DadosAnalises
Dim totalAnalisesImpr As Integer

'brunodsantos 03.12.2015
Private Type Seg_Res
    Seg_ResAlf1_Ant1 As String
    Seg_Dt_ResAlf1_Ant1 As String
    Seg_ResAlf1_Ant2 As String
    Seg_Dt_ResAlf1_Ant2 As String
    Seg_ResAlf1_Ant3 As String
    Seg_Dt_ResAlf1_Ant3 As String
    flg_res_ant_2 As Integer
End Type
'NELSONPSILVA CHVNG-7461 29.10.2018
Private Type AdicionaReutil
    n_prescricao As String
    dt_cri As String
    dt_val_canc_reutil As String
    hr_val_canc_reutil As String
    user_val_canc_reutil As String
    cod_med As String
    descr_proven As String
    flg_reutil As String
End Type
Dim ReutilAna As AdicionaReutil


'brunodsantos 04.12.2015
Private Function IR_CarregaComandos(ByRef CmdResDescrFrase As ADODB.Command, ByRef CmdFr As ADODB.Command)


    Set CmdResDescrFrase = New ADODB.Command
    CmdResDescrFrase.ActiveConnection = gConexao
    CmdResDescrFrase.CommandType = adCmdText
    CmdResDescrFrase.CommandText = "SELECT descr_frase FROM sl_dicionario WHERE cod_frase=?"
    CmdResDescrFrase.Prepared = True
    CmdResDescrFrase.Parameters.Append CmdResDescrFrase.CreateParameter("COD_FRASE", adVarChar, adParamInput, 10)
    
     'Preencher a estrutura com as frases e ordem das mesmas das an�lises complexas da requisi��o
    Set CmdFr = New ADODB.Command
    CmdFr.ActiveConnection = gConexao
    CmdFr.CommandType = adCmdText
    CmdFr.CommandText = "SELECT x1.ordem, x2.descr_frase FROM sl_membro x1, sl_dicionario x2 WHERE x1.cod_membro=x2.cod_frase AND x1.t_membro='F' AND x1.cod_ana_c=?"
    CmdFr.Prepared = True
    CmdFr.Parameters.Append CmdFr.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 10)
     
End Function

'
Public Function IR_ImprimeResultados(ByRef ReqComResultados As Boolean, _
                                     Optional InibeResultadosAnteriores As Boolean, _
                                     Optional NumReq As String, _
                                     Optional EstadoReq As String, _
                                     Optional SeqUtente As String, _
                                     Optional ListaAnalises As String, _
                                     Optional DataCalculo As String, _
                                     Optional NrRes As Integer, _
                                     Optional Multiplo As Boolean, _
                                     Optional GrupoAna As String, _
                                     Optional MultiReport As Integer, _
                                     Optional SubGrupoAna As String, _
                                     Optional InibeMsgJaImp As Boolean, _
                                     Optional ImpVerAntes As Boolean, _
                                     Optional CompletoPronto As TipoImpMult, _
                                     Optional NaoImprimirJaImpressos As Boolean, _
                                     Optional enviaEmail As Boolean, _
                                     Optional NumPagina As Integer, _
                                     Optional imprRelARS As Boolean, _
                                     Optional imprSegVia As Boolean, _
                                     Optional ImprComValTec As Boolean, _
                                     Optional ImprAnexos As Boolean, _
                                     Optional ImprimeListaExterior As Boolean, _
                                     Optional ImprimePreviewEmail As Boolean, _
                                     Optional ImprimeDoEcraReqCompletas As Boolean) As Boolean
                                     
    
    'Actualizado : 16/07/2002
    'Filipe Nogueira
    Dim Resp As VbMsgBoxResult
    Dim continua As Boolean, MostraCaixa As Boolean
    Dim sql As String, s As String
    Dim regRes As ADODB.recordset, RegFormulas As ADODB.recordset
    Dim total As Integer, i As Integer, totalProd As Integer
    Dim estadosAna As String
    Dim LastPerfil As String, LastComplexa As String, LastSimples As String
    Dim ContaLast As Long, registos As Long
    Dim AnaGraficoAux As String
    Dim GrupoUserVal As String
    ReDim params(gNumeroArgumentos)
    Dim formulas As dadosFormula
    
    Dim CmdResAlfan As ADODB.Command, CmdResDescrFrase As ADODB.Command, CmdResFrase As ADODB.Command, CmdResMicro As ADODB.Command, CmdResMicroTsq As ADODB.Command, CmdResTsq As ADODB.Command, CmdResRef As ADODB.Command, CmdResRefZonas As ADODB.Command, CmdResObserv As ADODB.Command, CmdResCom As ADODB.Command

    'Dim CmdProdutos As ADODB.Command

    Dim RegCmd As ADODB.recordset, RegCmdMTsq As ADODB.recordset, RegCmdResRef As ADODB.recordset, RegCmdZonasRef As ADODB.recordset, RegDescrFrase As ADODB.recordset

    'Constantes passadas para o Report
    Dim descrImp As String

    Dim RsDescrPostal As ADODB.recordset, FlgPosRes As Integer
    

    'Vari�veis dos Resultados
    Dim Grupo As String, subGrupo As String, Classe As String, Perfil As String, complexa As String, simples As String, codAgrup As String

    Dim VolumeSubGrupo As String, ResNum1Unid1  As String, ResNum1Unid2 As String, Unid1ResNum1 As String, Unid2ResNum1 As String, resAlf1 As String, TamResAlf1 As String, ValRef1 As String, ZonaRef1 As String

     ' Resultados Anteriores (1� Resultado)
    Dim ResAlf1_Ant1 As String, Dt_ResAlf1_Ant1 As String, ResAlf1_Ant2 As String, Dt_ResAlf1_Ant2 As String, ResAlf1_Ant3 As String, Dt_ResAlf1_Ant3 As String, LocalRes_Ant1 As String, LocalRes_Ant2 As String, LocalRes_Ant3 As String

    'Resultado para o 2�Resultado (2 unidades)
    Dim ResNum2Unid1 As String, ResNum2Unid2 As String, Unid1ResNum2 As String, Unid2ResNum2 As String

    'brunodsantos 03.12.2015
    Dim SegundoResultado As Seg_Res
    
    Dim resAlf2 As String, TamResAlf2 As String, ValRef2 As String, ZonaRef2 As String, Metodo1 As String, Metodo2 As String, MetodoPerfil As String, MetodoComplexa As String
    Dim obsRef As String
    'Dados do Utente
    Dim Especial As String, TipoEscRef As String, valoresRef As String, CodRef1 As String, CodRef2 As String

    Dim ObservAnalise As String, ObservResultado As String, ComAutomatico As String

    'Guarda as observacoes sem ser em formato RTF
    Dim ObservAnalise_Aux As String, ObservResultado_Aux As String

    Dim CodRespTecnicos As String, codRespMedicos As String
    Dim RespValMedMed As String, RespValMedTec As String
    
    Dim periodo As Integer, TipoPeriodo As String, CodFrase As String

    Dim RText As RichTextBox

    Dim FlagResComent As Boolean, FlagRes1Anormal As Boolean, FlagRes2Anormal As Boolean

    Dim cutoff As Boolean, UmParaX As Boolean, FlagRTFAnalise As Boolean, RegistoExclui As Boolean

    Dim str_aux As String

    Dim rv As Integer

    Dim EstrutFr() As Estrutura_Frases
    Dim TotalEstrutFr As Integer
    Dim rsAna As ADODB.recordset
    Dim CmdFr As ADODB.Command
    Dim PmtFr As ADODB.Parameter
    Dim RsFr As ADODB.recordset
    Dim flg_encontrou As Boolean
    Dim k As Integer
    Dim Simples2 As String

    Dim RsDescrEFR As ADODB.recordset
    Dim DtCri As String
    Dim dtVal As String
    Dim OrdemImpressao As Integer
    Dim LinhasPagina As Integer
    Dim RsContaMembros As ADODB.recordset
    Dim UltimaComplexa As String
    Dim ContaMembrosComplexa As Integer

    Dim ContaMembrosPerfil As Integer
    Dim UltimoPerfil As String
    Dim VecSplit() As String
    Dim l As Integer
    Dim UltimoGrupo As String
    Dim UltimoSubgrupo As String
    Dim UltimaClasse As String
    Dim UltimaDt1 As String
    Dim UltimaDt2 As String
    Dim UltimaDt3 As String

    Dim EstrutZonas() As String
    Dim TotalEstrutZonas As Integer
    Dim EstrutObsResultado() As String
    Dim TotalEstrutObsResultado As Integer
    Dim EstrutObsAnalise() As String
    Dim TotalEstrutObsAnalise As Integer
    Dim Flg_ResMicro As Boolean
    Dim TotalEstrutMicro As Integer
    Dim EstrutMicro() As String
    Dim aux_casas As Integer
    Dim grupoAnaP As String
    
    ' pferreira 2010.08.04
    Dim conclusoes_codificadas  As Boolean
    
    On Error GoTo Trata_Erro
    totalAnalisesImpr = 0
    ReDim analisesImpr(0)
    
    IR_ImprimeResultados = False
            
    Set RText = gFormActivo.Controls("RText")

    ReqComResultados = False
    formulas.VerTabelaAnexo = False
    Set regRes = New ADODB.recordset
    regRes.CursorLocation = adUseServer
    regRes.LockType = adLockReadOnly
    regRes.CursorType = adOpenForwardOnly
    regRes.ActiveConnection = gConexao
    
    Dim TabResReq As String
    Dim tabRes As String
    'Vai buscar os resultados do:
    ' - Mesmo Utente
    ' - Com as mesmas an�lises
    ' - Com a Data de Validade da an�lise da requisi��o em causa>Data Validade outras an�lises
                
    estadosAna = ""
    sql = ""
    
    If ImprimeListaExterior = False Then
        If gMostraAvisoIncompletas = mediSim And ImpVerAntes = False And InibeMsgJaImp = False Then
            If BL_SelCountAnaReq(CLng(NumReq), 1) > 0 Or BL_SelCountAnaReq(CLng(NumReq), 0) > 0 Or BL_SelCountAnaReq(CLng(NumReq), 5) > 0 Then
                If BG_Mensagem(mediMsgBox, "A requisi��o n�o est� completa." & Chr(13) & "Deseja imprimir os resultados?", vbYesNo + vbQuestion, "Requisi��o incompleta.") = vbNo Then
                    Exit Function
                End If
            End If
        End If
    End If
    If imprRelARS = True Then
        estadosAna = "('4')"
    Else
        If Multiplo = True Then
            If gImprimirComValTec = mediSim Then
                estadosAna = "('3','5')"
            Else
                If CompletoPronto = Completas Then
                    If NaoImprimirJaImpressos = True Then
                        estadosAna = "('3')"
                    Else
                        estadosAna = "('3','4')"
                    End If
                Else
                    estadosAna = "('3')"
                End If
            End If
        Else
            If ImpVerAntes = True Then
                estadosAna = "('1','3','4','5')"
            ElseIf ImprComValTec = True Then
                estadosAna = "('3','4','5')"
            Else
                If BL_SelCountAnaReq(CLng(NumReq), 4) > 0 Then
                    If InibeMsgJaImp = True Then
                        If NaoImprimirJaImpressos = True Then
                            estadosAna = "('3')"
                        Else
                            estadosAna = "('3','4')"
                        End If
                        
                    Else
                        If BG_Mensagem(mediMsgBox, "A requisi��o j� foi parcialmente impressa." & Chr(13) & "Deseja " & IIf(gImprimirDestino = 0, "visualizar", "reimprimir") & " os resultados que j� foram impressos ?", vbYesNo + vbQuestion, "Requisi��o parcialmente impressa.") = vbYes Then
                            estadosAna = "('3','4')"
                        Else
                            estadosAna = "('3')"
                        End If
                    End If
                Else
                    If EstadoReq <> gEstadoReqHistorico Then
                        If gImprimirComValTec = mediSim Then
                            estadosAna = "('3','5')"
                        Else
                            estadosAna = "('3')"
                        End If
                    Else
                        estadosAna = "('3','4')"
                    End If
                End If
            End If
        End If
    End If
     'retirar como comentario
    If EstadoReq <> gEstadoReqHistorico Then
        TabResReq = "sl_realiza RESREQ"
        tabRes = "sl_realiza RES"
    Else
        TabResReq = "sl_realiza_h RESREQ"
        tabRes = "sl_realiza_h RES"
    End If
    
    'RGONCALVES 10.12.2015 CHSJ-2511
'    'Preencher a estrutura com as frases e ordem das mesmas das an�lises complexas da requisi��o
'    Set CmdFr = New ADODB.Command
'    CmdFr.ActiveConnection = gConexao
'    CmdFr.CommandType = adCmdText
'    CmdFr.CommandText = "SELECT x1.ordem, x2.descr_frase FROM sl_membro x1, sl_dicionario x2 WHERE x1.cod_membro=x2.cod_frase AND x1.t_membro='F' AND x1.cod_ana_c=?"
'    CmdFr.Prepared = True
'    CmdFr.Parameters.Append CmdFr.CreateParameter("COD_ANA_C", adVarChar, adParamInput, 10)
     '
    'RGONCALVES 10.12.2015 CHSJ-2511
'    'brunodsantos 04.12.2015
'    Call IR_CarregaComandos(CmdResDescrFrase)
    Call IR_CarregaComandos(CmdResDescrFrase, CmdFr)
    '
    TotalEstrutFr = 0
    ReDim EstrutFr(TotalEstrutFr)
    sql = "SELECT distinct(cod_ana_c) Complexa FROM " & tabRes & " WHERE cod_ana_c <> '0' AND n_req=" & NumReq
    Set rsAna = New ADODB.recordset
    With rsAna
        .CursorLocation = adUseServer
        .CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sql
        .Open sql, gConexao
    End With
    If rsAna.RecordCount > 0 Then
        While Not rsAna.EOF
            CmdFr.Parameters(0).value = BL_HandleNull(rsAna!complexa, "0")
            Set RsFr = CmdFr.Execute
            If Not RsFr.EOF Then
                While Not RsFr.EOF
                    TotalEstrutFr = TotalEstrutFr + 1
                    ReDim Preserve EstrutFr(TotalEstrutFr)
                    EstrutFr(TotalEstrutFr).CodAnaC = BL_HandleNull(rsAna!complexa)
                    EstrutFr(TotalEstrutFr).ordem = BL_HandleNull(RsFr!ordem)
                    EstrutFr(TotalEstrutFr).frase = BL_HandleNull(RsFr!descr_frase)
                    EstrutFr(TotalEstrutFr).Ja_Foi_Inserida = False
                    RsFr.MoveNext
                Wend
            End If
            RsFr.Close
            
            rsAna.MoveNext
        Wend
    End If
    rsAna.Close
    Set rsAna = Nothing
    
    If Not RsFr Is Nothing Then
        Set RsFr = Nothing
    End If
    Set CmdFr = Nothing
    
    '-------------
    'BRUNODSANTOS - SCMVC-1073
    'IR_MudaEstadoAnaliseEcraCompletas ImprimeDoEcraReqCompletas, NumReq
    '
    sql = IR_ConstroiSqlRes(GrupoAna, SubGrupoAna, Multiplo, ListaAnalises, estadosAna, NumReq, tabRes, imprRelARS, ImprComValTec)
    regRes.Source = sql
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    regRes.CursorLocation = adUseClient
    regRes.LockType = adLockReadOnly
    regRes.CursorType = adOpenForwardOnly
    regRes.Open
    
    'Verifica se existem registos para imprimir
    total = regRes.RecordCount
    
    If total = 0 Then
        regRes.Close
        Set regRes = Nothing
        Call BL_FimProcessamento(gFormActivo, "")
        If Multiplo = False Then
            Call BG_Mensagem(mediMsgBox, "N�o foram encontrados nenhuns resultados associados � requisi��o " & NumReq & " para imprimir!", vbOKOnly + vbExclamation)
        Else
            ReqComResultados = False
        End If
        'Sai da Rotina
        Exit Function
    End If
    
    '___________________________________________________________
    
    'DADOS DA REQUISI��O
    
    Set RegFormulas = New ADODB.recordset
    With RegFormulas
        .CursorLocation = adUseServer
        .LockType = adLockReadOnly
        .CursorType = adOpenForwardOnly
        .ActiveConnection = gConexao
        .Source = IR_QueryRegFormulas(NumReq)
        If gModoDebug = mediSim Then BG_LogFile_Erros .Source
        .Open
    End With
    'Verifica se existem registos para imprimir
    If RegFormulas.RecordCount = 0 Then
        RegFormulas.Close
        Set RegFormulas = Nothing
        Call BL_FimProcessamento(gFormActivo, "")
        Call BG_Mensagem(mediMsgBox, "Requisi��o n�o existente!", vbOKOnly + vbExclamation)
        Exit Function
    End If
    
    IR_PreencheDados RegFormulas, NumReq, formulas

    
    ' ----------------------------------------------
    ' ALGUMAS ENTIDADES NAO SAO IMPRESSOS RESULTADOS
    ' SEM CREDENCIAL
    ' ----------------------------------------------
    If IR_VerificaInibeImprAnaEFR(BL_HandleNull(RegFormulas!cod_efr, ""), formulas.sala.cod_sala) = True Then
        Exit Function
    End If
    
    
    
        
    RegFormulas.Close
    
    '___________________________________________________________
    
    'Contactos da Proveni�ncia da Requisi��o em causa=>IMPRESS�O REMOTA
    
    descrImp = ""
    descrImp = BL_SeleccionaDescrImp(formulas.CodProv, gT_ImpressoraRede)
    
    '___________________________________________________________
       
    If Multiplo = True Then
        ReqComResultados = True
        MostraCaixa = False
    Else
        MostraCaixa = True
    End If
    
    If ImprimeListaExterior = False Then
        'Como n�o tem caixa fica a impressora definida no objecto Printer
        If gImprimirDestino = crptToPrinter Then
            If enviaEmail = True Then
                continua = BL_IniciaReport(NumReq, "Mapa de Resultados", crptToPrinter, MostraCaixa, , , , , descrImp, , , "MapaResultadosSimplesEmail")
            ElseIf imprRelARS = True Then
                continua = BL_IniciaReport("MapaResultadosSimplesARS", "Mapa de Resultados", crptToPrinter, MostraCaixa, , , , , descrImp)
            ElseIf formulas.flg_fax_destino = mediSim Then
                continua = BL_IniciaReport("MapaResultadosSimplesFax", "Mapa de Resultados", crptToPrinter, MostraCaixa, , , , , descrImp)
            Else
                continua = BL_IniciaReport(formulas.nomeRelatorio, "Mapa de Resultados", crptToPrinter, MostraCaixa, , , , , descrImp)
            End If
        Else
            If enviaEmail = True Then
                continua = BL_IniciaReport(NumReq, "Mapa de Resultados", crptToPrinter, MostraCaixa, , , , , descrImp, , , "MapaResultadosSimplesEmail")
            ElseIf imprRelARS = True Then
                continua = BL_IniciaReport("MapaResultadosSimplesARS", "Mapa de Resultados", crptToWindow, , , , , , descrImp)
            ElseIf formulas.flg_fax_destino = mediSim Then
                continua = BL_IniciaReport("MapaResultadosSimplesFax", "Mapa de Resultados", crptToWindow, , , , , , descrImp)
            'BRUNODSANTOS 05.09.2016 - LRV-388
'            ElseIf ImprimePreviewEmail = True Then
'               continua = IniciaReport_Email_Preview(descrImp)
            '
            Else
                continua = BL_IniciaReport(formulas.nomeRelatorio, "Mapa de Resultados", crptToWindow, , , , , , descrImp)
            End If
        End If
    End If
    If continua = False And ImprimeListaExterior = False Then
        regRes.Close
        Set regRes = Nothing
        Set RegFormulas = Nothing
        Exit Function
    End If
    
    '___________________________________________________________
          
    'Produtos da Requisi��o
    formulas.Produtos = ""
    formulas.Produtos = BL_SeleccionaDescrProduto(NumReq)
    '___________________________________________________________
    
    'An�lises Pendentes da Requisi��o em causa
    
    formulas.AnalisesPendentes = ""
    formulas.AnalisesPendentesTubo = IR_SeleccionaAnalisesPendentes(NumReq, GrupoAna, False)
    formulas.AnalisesPendentes = IR_SeleccionaAnalisesPendentes(NumReq, GrupoAna, True)
          
    '___________________________________________________________
          
    'Coment�rio final da Requisi��o em causa
    formulas.Comentario = ""
    formulas.Comentario = BL_SeleccionaComentarioFinal(NumReq)
    
   '******************************************************************************************************************************************************************
    'Cria as tabelas tempor�rias para a Impress�o dos Resultados
'    If Multiplo = False Then
'        Call BL_ImprimeResultTabela
'    End If
    
    '_________________________________________________________________________
    
    'Obt�m os resultados para cada an�lise e insere-os na tabela tempor�ria
    'Resultados NUM�RICOS E ALFANUM�RICOS
    
    Set CmdResAlfan = New ADODB.Command
    CmdResAlfan.ActiveConnection = gConexao
    CmdResAlfan.CommandType = adCmdText
    If EstadoReq <> gEstadoReqHistorico Then
        tabRes = "sl_res_alfan R"
    Else
        tabRes = "sl_res_alfan_h R"
    End If

    CmdResAlfan.CommandText = "SELECT " & vbCrLf & _
                              "     R.n_res,R.result," & vbCrLf & _
                              "     R.res_ant1,R.dt_res_ant1,local_ant1,  " & vbCrLf & _
                              "     R.res_ant2,R.dt_res_ant2,local_ant2,  " & vbCrLf & _
                              "     R.res_ant3,R.dt_res_ant3,local_ant3  " & vbCrLf & _
                              "FROM " & vbCrLf & _
                                    tabRes & "  " & vbCrLf & _
                              "WHERE " & vbCrLf & _
                              "     R.seq_realiza=? " & vbCrLf & _
                              "     AND (R.flg_imprimir = 1 or r.flg_imprimir is null) " & _
                              "ORDER BY " & vbCrLf & _
                              "     R.n_res"

    CmdResAlfan.Parameters.Append CmdResAlfan.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)


    CmdResAlfan.Prepared = True
'
'    Set CmdResDescrFrase = New ADODB.Command
'    CmdResDescrFrase.ActiveConnection = gConexao
'    CmdResDescrFrase.CommandType = adCmdText
'    CmdResDescrFrase.CommandText = "SELECT descr_frase FROM sl_dicionario WHERE cod_frase=?"
'    CmdResDescrFrase.Prepared = True
'    CmdResDescrFrase.Parameters.Append CmdResDescrFrase.CreateParameter("COD_FRASE", adVarChar, adParamInput, 10)
'
    '_________________________________________________________________________
   
    'Resultados FRASE
    Set CmdResFrase = New ADODB.Command
    CmdResFrase.ActiveConnection = gConexao
    CmdResFrase.CommandType = adCmdText
    If EstadoReq <> gEstadoReqHistorico Then
        tabRes = "sl_res_frase R"
    Else
        tabRes = "sl_res_frase_h R"
    End If
    If gLAB = "CHVNG" Then
        CmdResFrase.CommandText = "SELECT R.n_res,R.ord_frase,sl_dicionario.descr_frase FROM " & tabRes & ",sl_dicionario WHERE R.seq_realiza=? AND R.cod_frase=sl_dicionario.cod_frase"
        CmdResFrase.CommandText = CmdResFrase.CommandText & " AND r.cod_Frase <> 'SR' AND (R.flg_imprimir = 1 or r.flg_imprimir is null) ORDER BY R.n_res,R.ord_frase"
    Else
        CmdResFrase.CommandText = "SELECT R.n_res,R.ord_frase,sl_dicionario.descr_frase FROM " & tabRes & ",sl_dicionario WHERE R.seq_realiza=? AND R.cod_frase=sl_dicionario.cod_frase "
        CmdResFrase.CommandText = CmdResFrase.CommandText & " AND (R.flg_imprimir = 1 or r.flg_imprimir is null) ORDER BY R.n_res,R.ord_frase"
    End If
    CmdResFrase.Parameters.Append CmdResFrase.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)

    CmdResFrase.Prepared = True
    
    '_________________________________________________________________________
    
    'Resultados MICRO
    Set CmdResMicro = New ADODB.Command
    CmdResMicro.ActiveConnection = gConexao
    CmdResMicro.CommandType = adCmdText
    If EstadoReq <> gEstadoReqHistorico Then
        tabRes = "sl_res_micro R"
    Else
        tabRes = "sl_res_micro_h R"
    End If

    CmdResMicro.CommandText = "SELECT R.cod_micro,R.n_res,R.quantif,R.flg_tsq,M.descr_microrg, R.cod_gr_antibio, fm.descr_obs_ana, fm.descr_obs_ana_rtf, m.abrev_microrg FROM " & tabRes & " LEFT OUTER JOIN sl_obs_ana fm ON  r.seq_realiza = FM.seq_realiza ,sl_microrg M  WHERE R.seq_realiza=? AND R.flg_imp='S' AND R.cod_micro=M.cod_microrg "
    CmdResMicro.CommandText = CmdResMicro.CommandText & " AND (R.flg_imprimir = 1 or r.flg_imprimir is null) ORDER BY R.n_res,M.descr_microrg "
    CmdResMicro.Parameters.Append CmdResMicro.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)

    CmdResMicro.Prepared = True
    
    '_________________________________________________________________________
    
    'Resultados TSQ
    Set CmdResTsq = New ADODB.Command
    CmdResTsq.ActiveConnection = gConexao
    CmdResTsq.CommandType = adCmdText
    If EstadoReq <> gEstadoReqHistorico Then
        tabRes = "sl_res_tsq R"
    Else
        tabRes = "sl_res_tsq_h R"
    End If

    CmdResTsq.CommandText = "SELECT R.n_res,R.res_sensib,R.cmi,A.descr_antibio, r.user_val, r.dt_val, r.hr_val FROM " & tabRes & ",sl_antibio A WHERE R.seq_realiza=? AND R.cod_micro IS NULL  AND R.flg_imp='S' AND R.cod_antib=A.cod_antibio ORDER BY R.n_res,A.descr_antibio"
    CmdResTsq.Parameters.Append CmdResTsq.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
   
    CmdResTsq.Prepared = True
    
    '_________________________________________________________________________
    
    'Resultados MICROTSQ
    Set CmdResMicroTsq = New ADODB.Command
    CmdResMicroTsq.ActiveConnection = gConexao
    CmdResMicroTsq.CommandType = adCmdText
    If EstadoReq <> gEstadoReqHistorico Then
        tabRes = "sl_res_tsq R"
    Else
        tabRes = "sl_res_tsq_h R"
    End If

    CmdResMicroTsq.CommandText = "SELECT a.cod_antibio, R.res_sensib,R.cmi,A.descr_antibio, A.ORDEM_IMP, r.user_val, r.dt_val, r.hr_val FROM " & tabRes & ",sl_antibio A WHERE R.seq_realiza=? AND R.cod_micro=? AND R.n_res=? AND R.flg_imp='S' AND R.cod_antib=A.cod_antibio ORDER BY A.descr_antibio"
    CmdResMicroTsq.Parameters.Append CmdResMicroTsq.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    CmdResMicroTsq.Parameters.Append CmdResMicroTsq.CreateParameter("COD_MICRO", adVarChar, adParamInput, 15)
    CmdResMicroTsq.Parameters.Append CmdResMicroTsq.CreateParameter("N_RES", adInteger, adParamInput, 1)
    
    CmdResMicroTsq.Prepared = True
    
    '_________________________________________________________________________
    
    'Descri��es dos VALORES DE REFER�NCIA
    Set CmdResRef = New ADODB.Command
    CmdResRef.ActiveConnection = gConexao
    CmdResRef.CommandType = adCmdText
    
    'VERIFICA PELAS 2 TABELAS
    Especial = "(" & gT_Crianca & "," & gT_Adulto & ")"
    s = BL_QueryResRef(formulas.DtNascUtente, Especial, formulas.DiasUtente, formulas.codGenero)
    
    CmdResRef.CommandText = s
    CmdResRef.Prepared = True
    CmdResRef.Parameters.Append CmdResRef.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
    CmdResRef.Parameters.Append CmdResRef.CreateParameter("DATA_RES1", adDBTimeStamp, adParamInput, 7)
    CmdResRef.Parameters.Append CmdResRef.CreateParameter("DATA_RES2", adDBTimeStamp, adParamInput, 7)
    
    
    '_________________________________________________________________________
    
    'Descri��es dos VALORES DE REFER�NCIA COM EXCEP��ES
    Set CmdResRefZonas = New ADODB.Command
    CmdResRefZonas.ActiveConnection = gConexao
    CmdResRefZonas.CommandType = adCmdText
    
    s = IR_ConstroiSqlValRef(formulas.CodSexo, formulas.SeqUtente, formulas.DtNascUtente, formulas.DiasUtente, formulas.ObsProven)
     
    CmdResRefZonas.CommandText = s
    CmdResRefZonas.Prepared = True
    CmdResRefZonas.Parameters.Append CmdResRefZonas.CreateParameter("COD_ANA_REF", adInteger, adParamInput, 9)
    CmdResRefZonas.Parameters.Append CmdResRefZonas.CreateParameter("N_REQ", adInteger, adParamInput, 9)
    '_________________________________________________________________________
        
    'OBSERVA��ES E COMENT�RIOS DA AN�LISE
    Set CmdResObserv = New ADODB.Command
    CmdResObserv.ActiveConnection = gConexao
    CmdResObserv.CommandType = adCmdText
    CmdResObserv.CommandText = "SELECT sl_ana_s.coment OBSERV,'A' TIPO, null OBSERV_RTF FROM sl_ana_s WHERE sl_ana_s.seq_ana_s =? " & _
                               "UNION " & _
                               "SELECT sl_obs_ana.descr_obs_ana OBSERV,'R' TIPO,sl_obs_ana.descr_obs_ana_rtf  OBSERV_RTF FROM sl_obs_ana WHERE sl_obs_ana.seq_realiza =? "
    CmdResObserv.Prepared = True
    CmdResObserv.Parameters.Append CmdResObserv.CreateParameter("SEQ_ANA_S", adInteger, adParamInput, 9)
    CmdResObserv.Parameters.Append CmdResObserv.CreateParameter("SEQ_REALIZA", adInteger, adParamInput, 9)
    
    '_________________________________________________________________________
    
    'COMENT�RIOS AUTOM�TICOS DA AN�LISE
    Set CmdResCom = New ADODB.Command
    CmdResCom.ActiveConnection = gConexao
    CmdResCom.CommandType = adCmdText
    CmdResCom.CommandText = "SELECT sl_ana_conc.descr_formula,sl_ana_conc.descr_conc " & _
                            "FROM sl_ana_conc " & _
                            "WHERE sl_ana_conc.cod_ana_s=?"
    CmdResCom.Prepared = True
    CmdResCom.Parameters.Append CmdResObserv.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
    
    If ImprimeListaExterior = False Then
        If IR_ApagaTabelasResultados = False Then
            IR_ImprimeResultados = False
            Exit Function
        End If
    End If
    
    formulas.respTecnicos = ""
    CodRespTecnicos = ""
    formulas.RespMedicos = ""
    RespValMedMed = ""
    RespValMedTec = ""
    codRespMedicos = ""
    LastPerfil = ""
    LastComplexa = ""
    LastSimples = ""
    
    ContaLast = 0
    
    'Vari�vel para controlar a existencia de dados!
    ' (necess�rio por causa dos GOTOs)
    registos = 0
    
    LinhasPagina = CInt(gLinhasPagina)
    OrdemImpressao = 0
    UltimaComplexa = ""
    UltimoPerfil = ""
    UltimoGrupo = "1"
    UltimoSubgrupo = ""
    UltimaClasse = ""
    'UltimoGrupo = IIf(BL_HandleNull(RegRes!descr_gr_ana, "") = "", "Null", BL_TrataStringParaBD("" & RegRes!descr_gr_ana))
    'UltimoSubGrupo = IIf(BL_HandleNull(RegRes!descr_sgr_ana, "") = "", "Null", BL_TrataStringParaBD("" & RegRes!descr_sgr_ana))
        
    
    Set RsContaMembros = New ADODB.recordset
    RsContaMembros.CursorLocation = adUseServer
    RsContaMembros.CursorType = adOpenStatic
    
'    AnaGraficoAux = ""

    'Insere os registos nas tabelas criadas
    For i = 1 To total
        totalAnalisesImpr = totalAnalisesImpr + 1
        ReDim Preserve analisesImpr(totalAnalisesImpr)
        descr_especif = ""
        obs_especif = ""
        
        'NELSONPSILVA CHVNG-7461 29.10.2018
        IR_PreencheReutilAna regRes
        '
        
        '********************************************************
        'Informa��o Principal:
        If gImprimeRelOrderGrImp <> mediSim Then
            Grupo = IIf(BL_HandleNull(regRes!descr_gr_ana, "") = "", "Null", BL_TrataStringParaBD("" & regRes!descr_gr_ana))
        Else
            Grupo = IIf(BL_HandleNull(regRes!descr_gr_impr, "") = "", "Null", BL_TrataStringParaBD("" & regRes!descr_gr_impr))
        End If
        
        subGrupo = IIf(BL_HandleNull(regRes!descr_sgr_ana, "") = "", "Null", BL_TrataStringParaBD("" & regRes!descr_sgr_ana))
        
        VolumeSubGrupo = BL_RetornaVolumeProd(BL_HandleNull(regRes!flg_imprime_volume, "0"), Trim(BL_HandleNull(regRes!cod_ana_s, "")), NumReq)
        
        If (Not (left(str_aux, 1) = "P")) Then
        str_aux = Trim(BL_HandleNull(regRes!Cod_Perfil, ""))
            str_aux = Trim(BL_HandleNull(regRes!cod_ana_c, ""))
            If (Not (left(str_aux, 1) = "C")) Then
                str_aux = Trim(BL_HandleNull(regRes!cod_ana_s, ""))
                If (Not (left(str_aux, 1) = "S")) Then str_aux = ""
            End If
        End If
        
        Classe = BL_TrataStringParaBD(ANALISE_Descr_Classe(str_aux))
        'CodAgrup = regres!cod_agrup
        'RGONCALVES 10.12.2015 CHSJ-2511
        'Perfil = IIf(BL_HandleNull(regRes!descr_perfis, "") = "", "Null", IIf(BL_HandleNull(regRes!flg_inibe_descr_p, "0") = 1, BL_TrataStringParaBD("//" & regRes!descr_perfis), BL_TrataStringParaBD("" & regRes!descr_perfis)))
        'complexa = IIf(BL_HandleNull(regRes!Descr_Ana_C, "") = "", "Null", IIf(BL_HandleNull(regRes!flg_inibe_descr_c, "0") = 1, BL_TrataStringParaBD("//" & regRes!Descr_Ana_C), BL_TrataStringParaBD("" & regRes!Descr_Ana_C)))
        Perfil = IIf(BL_HandleNull(regRes!descr_perfis, "") = "", "Null", IIf(BL_HandleNull(regRes!flg_inibe_descr_p, "0") = 1, "//" & regRes!descr_perfis, "" & regRes!descr_perfis))
        complexa = IIf(BL_HandleNull(regRes!Descr_Ana_C, "") = "", "Null", IIf(BL_HandleNull(regRes!flg_inibe_descr_c, "0") = 1, "//" & regRes!Descr_Ana_C, "" & regRes!Descr_Ana_C))
        '
        simples = IIf(BL_HandleNull(regRes!descr_ana_s, "") = "", "Null", IIf(BL_HandleNull(regRes!flg_inibe_descr_s, "0") = 1, "//" & regRes!descr_ana_s, "" & regRes!descr_ana_s))
        
        IR_RetornaEspecif NumReq, BL_HandleNull(regRes!cod_produto, "")
        IR_PreencheDadosAnalises regRes

        'RGONCALVES 10.12.2015 CHSJ-2511
        'BL_VerificaExternasAcreditadas formulas.totalAcreditadas, BL_HandleNull(regRes!ana_acreditada, 1), simples, formulas.totalExterior, BL_HandleNull(regRes!ana_exterior, 0), formulas.totalNaoAcreditadas
        Dim sql_acreditadas_cod_agrup As String
        
        sql_acreditadas_cod_agrup = " SELECT cod_agrup, SUM ( Case cod_agrup  WHEN cod_perfil THEN NVL  (ana_p_acreditada, 1) WHEN cod_ana_c  THEN NVL (ana_c_acreditada, 1)  WHEN cod_ana_s THEN NVL (ana_acreditada, 1)  END) acreditada FROM ( " & regRes.Source & " ) GROUP BY cod_agrup "
        
        BL_VerificaExternasAcreditadas formulas.totalAcreditadas, BL_HandleNull(regRes!ana_acreditada, 1), simples, formulas.totalExterior, BL_HandleNull(regRes!ana_exterior, 0), formulas.totalNaoAcreditadas, _
                                    sql_acreditadas_cod_agrup, complexa, Perfil, BL_HandleNull(regRes!ana_c_acreditada, 1), BL_HandleNull(regRes!ana_p_acreditada, 1), formulas.totalAcreditadasCodAgrup, formulas.totalNaoAcreditadasCodAgrup
   
        '
        
        FlgPosRes = BL_HandleNull(regRes!flg_pos_res, 1)
        params(gArgumentoObsAutoAviso) = BL_VerificaObsAutoAviso(NumReq, regRes!seq_realiza)
        '----------------------------
        'soliveira 24.04.2006
        If AnaGraficoAux <> regRes!cod_ana_c Then
            FormGraficoReport.Picture1.Picture = Nothing
            
            sql = "SELECT x1.grafico grafico, x1.n_req FROM SL_RES_GRAFICO_BANDA x1 WHERE x1.n_req =" & regRes!n_req
            sql = sql & " AND x1.cod_ana = " & BL_TrataStringParaBD(regRes!cod_agrup)
            sql = sql & " AND (x1.flg_invisivel IS NULL OR x1.flg_invisivel = 0)"
            'sql = sql & " AND x1.cod_t_grafico IN( select  x2.cod_t_grafico FROM sl_tbf_t_grafico x2 WHERE x2.flg_imprimir = 1 )"
            On Error Resume Next
            IM_RetiraImagemBD gDirCliente & "\banda" & regRes!n_req & ".jpg", sql, FormGraficoReport.Picture1
            BL_GraficoReport regRes!n_req, regRes!cod_ana_c, complexa
            'Kill gDirCliente & "\banda.jpg"
            AnaGraficoAux = regRes!cod_agrup
            On Error GoTo Trata_Erro
        End If
        '----------------------------
        
        'soliveira 08-07-04
        If gImprimirDestino = 1 And ImprComValTec = False Then
            'Muda o estado do resultado da an�lise para "Impressa=4"
            BL_MudaEstadoAnalise regRes!flg_estado, regRes!seq_realiza, regRes!cod_agrup, NumReq
        End If
        '********************************************************
        'Unidades:
        
        'Calcula as Unidades para os 1� Resultados
        If "" & regRes!FLG_UNID_ACT1 = "1" Then
            'Valor registado em rela��o � unidade1?
            Unid1ResNum1 = "" & regRes!unid_1_res1
            Unid2ResNum1 = "" & regRes!unid_2_res1
        Else
            'Valor registado em rela��o � unidade2?
            Unid1ResNum1 = "" & regRes!unid_2_res1
            Unid2ResNum1 = "" & regRes!unid_1_res1
        End If
        
        'Calcula a Unidade em uso para os 2� Resultados
        If "" & regRes!FLG_UNID_ACT2 = "1" Then
            'Valor registado em rela��o � unidade1?
            Unid1ResNum2 = "" & regRes!unid_1_res2
            Unid2ResNum2 = "" & regRes!unid_2_res2
        Else
            'Valor registado em rela��o � unidade2?
            Unid1ResNum2 = "" & regRes!unid_2_res2
            Unid2ResNum2 = "" & regRes!unid_1_res2
        End If
        
        '********************************************************
        'M�todos usados para os valores de refer�ncia da an�lise
        Metodo1 = ""
        Metodo2 = ""
        MetodoPerfil = ""
        MetodoComplexa = ""
        params(gArgumentoCaracMetodoPerf) = ""
        params(gArgumentoCaracMetodoComp) = ""
        params(gArgumentoCaracMetodo1) = ""
        params(gArgumentoCaracMetodo2) = ""
        If BL_HandleNull(regRes!metodo_perfil, "") <> "" Then
            MetodoPerfil = BL_SeleccionaMetodo(regRes!metodo_perfil, params(gArgumentoCaracMetodoPerf))
        ElseIf BL_HandleNull(regRes!metodo_complexa, "") <> "" Then
            MetodoComplexa = BL_SeleccionaMetodo(regRes!metodo_complexa, params(gArgumentoCaracMetodoComp))
        Else
            Call IR_ObtemMetodo(regRes!cod_ana_s, BL_HandleNull(regRes!dt_cri, Bg_DaData_ADO), Metodo1, Metodo2, params(gArgumentoCaracMetodo1), params(gArgumentoCaracMetodo2))
        End If
        '********************************************************
        
        'Valores de Refer�ncia:
        'Devolve o Registo com os valores de refer�ncia (Valores/Escal�es) para os 1� e 2� Resultados !
        
        CodRef1 = ""
        CodRef2 = ""
        TipoEscRef = ""
        ValRef1 = ""
        ValRef2 = ""
        cutoff = False
        obsRef = ""
        
        'Verifica se � do tipo 1/X
        If Trim("" & regRes!flg_1_para_x) = "1" Then
            UmParaX = True
        Else
            UmParaX = False
        End If
        
        'Podem existir Zonas sem existir Valores/Escal�es de Refer�ncia!!!
        
        If BL_HandleNull(regRes!dt_val, "") = "" Then
            dtVal = BL_HandleNull(regRes!dt_tec_val, "")
        Else
            dtVal = regRes!dt_val
        End If
        
        If ImpVerAntes = True And (dtVal = "" Or dtVal = ("01-01-1900")) Then
            dtVal = Bg_DaData_ADO
        End If
        
        If ImpVerAntes = True And BL_HandleNull(regRes!dt_cri, "") = "" Then
            DtCri = Bg_DaData_ADO
        Else
            DtCri = BL_HandleNull(regRes!dt_cri, Bg_DaData_ADO)
        End If
        
        CmdResRef.Parameters("COD_ANA_S").value = regRes!cod_ana_s
        ' alterado a pedido do BM - Glintt-HS-4427
        CmdResRef.Parameters("DATA_RES1").value = DtCri
        CmdResRef.Parameters("DATA_RES2").value = DtCri
        Set RegCmdResRef = CmdResRef.Execute
        
        While RegCmdResRef.EOF <> True
            
            'Obt�m os c�digos de refer�ncia para mais tarde para as Zonas
            If RegCmdResRef!N_Res = 1 Then
                CodRef1 = RegCmdResRef!seq_ana_ref
            Else
                CodRef2 = RegCmdResRef!seq_ana_ref
            End If
            obsRef = BL_HandleNull(RegCmdResRef!obs_ref, "")
            'Verifica se � Valor ou Escal�o
            
            'VALORES
            If ((BL_HandleNull(RegCmdResRef!HEMin, "") = "" And Not BL_HandleNull(RegCmdResRef!HVmin, "") = "") Or BL_HandleNull(RegCmdResRef!HVmax, "") <> "" Or BL_HandleNull(RegCmdResRef!com_mih, "") <> "" Or BL_HandleNull(RegCmdResRef!com_mah, "") <> "") _
               Or ((BL_HandleNull(RegCmdResRef!MEMin, "") = "" And Not BL_HandleNull(RegCmdResRef!MVmin, "") = "") Or BL_HandleNull(RegCmdResRef!MVmax, "") <> "" Or BL_HandleNull(RegCmdResRef!com_mif, "") <> "") Or BL_HandleNull(RegCmdResRef!com_maf, "") <> "" Then
                'O Utente n�o ter o sexo definido
                If formulas.CodSexo = gT_Masculino Then
                    s = IIf(BL_HandleNull(RegCmdResRef!HVmin, "") = "" Or BL_HandleNull(RegCmdResRef!HVmax, "") = "", "", " - ")
                    If RegCmdResRef!N_Res = 1 Then
                        If UmParaX = True Then
                            ValRef1 = ValRef1 & IIf(Trim("" & RegCmdResRef!HVmin) = "", "", "1/" & RegCmdResRef!HVmin) & IIf(BL_HandleNull(RegCmdResRef!com_mih, "") = "", "", "(" & RegCmdResRef!com_mih & ")") & s & IIf(Trim("" & RegCmdResRef!HVmax) = "", "", "1/" & RegCmdResRef!HVmax) & IIf(BL_HandleNull(RegCmdResRef!com_mah, "") = "", "", "(" & RegCmdResRef!com_mah & ")") & vbNewLine
                        Else
                            'ValRef1 = ValRef1 & RegCmdResRef!HVMIN & IIf(BL_HandleNull(RegCmdResRef!com_mih, "") = "", "", "(" & RegCmdResRef!com_mih & ")") & s & RegCmdResRef!HVMax & IIf(BL_HandleNull(RegCmdResRef!com_mah, "") = "", "", "(" & RegCmdResRef!com_mah & ")") & vbNewLine
                            ValRef1 = ValRef1 & RegCmdResRef!HVmin & IIf(BL_HandleNull(RegCmdResRef!com_mih, "") = "", "", "(" & RegCmdResRef!com_mih & ")") & s & RegCmdResRef!HVmax & IIf(BL_HandleNull(RegCmdResRef!com_mah, "") = "", "", "(" & RegCmdResRef!com_mah & ")")
                        End If
                    Else
                        ValRef2 = ValRef2 & RegCmdResRef!HVmin & IIf(BL_HandleNull(RegCmdResRef!com_mih, "") = "", "", "(" & RegCmdResRef!com_mih & ")") & s & RegCmdResRef!HVmax & IIf(BL_HandleNull(RegCmdResRef!com_mah, "") = "", "", "(" & RegCmdResRef!com_mah & ")") & vbNewLine
                    End If
                End If
                If formulas.CodSexo = gT_Feminino Then
                    s = IIf(Trim(BL_HandleNull(RegCmdResRef!MVmin, "")) = "" Or Trim(BL_HandleNull(RegCmdResRef!MVmax, "")) = "", "", "-")
                    If RegCmdResRef!N_Res = 1 Then
                        If UmParaX = True Then
                            ValRef1 = ValRef1 & IIf(Trim("" & RegCmdResRef!MVmin) = "", "", "1/" & RegCmdResRef!MVmin) & IIf(BL_HandleNull(RegCmdResRef!com_mif, "") = "", "", "(" & RegCmdResRef!com_mif & ")") & s & IIf(Trim("" & RegCmdResRef!MVmax) = "", "", "1/" & RegCmdResRef!MVmax) & IIf(BL_HandleNull(RegCmdResRef!com_maf, "") = "", "", "(" & RegCmdResRef!com_maf & ")") & vbNewLine
                        Else
                            'ValRef1 = ValRef1 & RegCmdResRef!MVMIN & IIf(BL_HandleNull(RegCmdResRef!com_mif, "") = "", "", "(" & RegCmdResRef!com_mif & ")") & s & RegCmdResRef!MVMAX & IIf(BL_HandleNull(RegCmdResRef!com_maf, "") = "", "", "(" & RegCmdResRef!com_maf & ")") & vbNewLine
                            ValRef1 = ValRef1 & RegCmdResRef!MVmin & IIf(BL_HandleNull(RegCmdResRef!com_mif, "") = "", "", "(" & RegCmdResRef!com_mif & ")") & s & RegCmdResRef!MVmax & IIf(BL_HandleNull(RegCmdResRef!com_maf, "") = "", "", "(" & RegCmdResRef!com_maf & ")")
                        End If
                    Else
                        ValRef2 = ValRef2 & Trim(RegCmdResRef!MVmin) & IIf(BL_HandleNull(RegCmdResRef!com_mif, "") = "", "", "(" & RegCmdResRef!com_mif & ")") & s & Trim(RegCmdResRef!MVmax) & IIf(BL_HandleNull(RegCmdResRef!com_maf, "") = "", "", "(" & RegCmdResRef!com_maf & ")") & vbNewLine
                    End If
                End If
            End If
            
            'ESCAL�ES
            If Not (BL_HandleNull(RegCmdResRef!HEMax, "") = "" And Trim(BL_HandleNull(RegCmdResRef!HVmax, "")) = "" And Trim(BL_HandleNull(RegCmdResRef!HVmin, "")) = "" And _
                    BL_HandleNull(RegCmdResRef!MEMax, "") = "" And Trim(BL_HandleNull(RegCmdResRef!MVmax, "")) = "" And Trim(BL_HandleNull(RegCmdResRef!MVmin, "")) = "") Then
            
                TipoEscRef = ""
                If RegCmdResRef!t_esc_inf = gT_Adulto Then TipoEscRef = "Adulto:"
                If RegCmdResRef!t_esc_inf = gT_Crianca Then TipoEscRef = "Crian�a:"
                
                'O Utente n�o ter o sexo definido
                If formulas.CodSexo = gT_Masculino Then
                    s = IIf(BL_HandleNull(RegCmdResRef!HEMin, "") = "" Or BL_HandleNull(RegCmdResRef!HEMax, "") = "", "", " - ")
                    If RegCmdResRef!N_Res = 1 Then
                        If TipoEscRef = "" Then
                            'Refer�ncia Simples:
                            If InStr(1, ValRef1, "Adulto") = 0 And InStr(1, ValRef1, "Crian�a") = 0 Then
                                'N�o muda de linha se ainda n�o tem uma anterior refer�ncia tipo adulto ou crian�a
                                If UmParaX = True Then
                                    ValRef1 = ValRef1 & IIf(Trim("" & RegCmdResRef!HEMin) = "", "", "1/" & RegCmdResRef!HEMin) & s & IIf(Trim("" & RegCmdResRef!HEMax) = "", "", "1/" & RegCmdResRef!HEMax) & vbNewLine
                                Else
                                    'ValRef1 = ValRef1 & RegCmdResRef!HEMin & s & RegCmdResRef!HEMax & vbNewLine
                                    ValRef1 = ValRef1 & RegCmdResRef!HEMin & s & RegCmdResRef!HEMax
                                End If
                            Else
                                'Muda de linha se tiver j� tem uma anterior refer�ncia tipo adulto ou crian�a
                                If UmParaX = True Then
                                    ValRef1 = ValRef1 & vbNewLine & IIf(Trim("" & RegCmdResRef!HEMin) = "", "", "1/" & RegCmdResRef!HEMin) & s & IIf(Trim("" & RegCmdResRef!HEMax) = "", "", "1/" & RegCmdResRef!HEMax) & vbNewLine
                                Else
                                    'ValRef1 = ValRef1 & vbNewLine & RegCmdResRef!HEMin & s & RegCmdResRef!HEMax & vbNewLine
                                    ValRef1 = ValRef1 & vbNewLine & RegCmdResRef!HEMin & s & RegCmdResRef!HEMax
                                End If
                            End If
                        Else
                            'Refer�ncia tipo Adulto ou crian�a
                            If UmParaX = True Then
                                ValRef1 = ValRef1 & TipoEscRef & IIf(Trim("" & RegCmdResRef!HEMin) = "", "", "1/" & RegCmdResRef!HEMin) & s & IIf(Trim("" & RegCmdResRef!HEMax) = "", "", "1/" & RegCmdResRef!HEMax) & " "
                            Else
                                ValRef1 = ValRef1 & TipoEscRef & RegCmdResRef!HEMin & s & RegCmdResRef!HEMax & " "
                            End If
                        End If
                    Else
                        If TipoEscRef = "" Then
                            'Refer�ncia Simples:
                            If InStr(1, ValRef2, "Adulto") = 0 And InStr(1, ValRef2, "Crian�a") = 0 Then
                                'N�o muda de linha se ainda n�o tem uma anterior refer�ncia tipo adulto ou crian�a
                                ValRef2 = ValRef2 & RegCmdResRef!HEMin & s & RegCmdResRef!HEMax & vbNewLine
                            Else
                                'Muda de linha se tiver j� tem uma anterior refer�ncia tipo adulto ou crian�a
                                ValRef2 = ValRef2 & vbNewLine & RegCmdResRef!HEMin & s & RegCmdResRef!HEMax & vbNewLine
                            End If
                        Else
                            'Refer�ncia tipo Adulto ou crian�a
                            ValRef2 = ValRef2 & TipoEscRef & RegCmdResRef!HEMin & s & RegCmdResRef!HEMax & " "
                        End If
                    End If
                End If
                
                If formulas.CodSexo = gT_Feminino Then
                    s = IIf(BL_HandleNull(RegCmdResRef!MEMin, "") = "" Or BL_HandleNull(RegCmdResRef!MEMax, "") = "", "", " - ")
                    If RegCmdResRef!N_Res = 1 Then
                        If TipoEscRef = "" Then
                            'Refer�ncia Simples:
                            If InStr(1, ValRef1, "Adulto") = 0 And InStr(1, ValRef1, "Crian�a") = 0 Then
                                'N�o muda de linha se ainda n�o tem uma anterior refer�ncia tipo adulto ou crian�a
                                If UmParaX = True Then
                                    ValRef1 = ValRef1 & IIf(Trim("" & RegCmdResRef!MEMin) = "", "", "1/" & RegCmdResRef!MEMin) & s & IIf(Trim("" & RegCmdResRef!MEMax) = "", "", "1/" & RegCmdResRef!MEMax) & vbNewLine
                                Else
                                    'ValRef1 = ValRef1 & RegCmdResRef!MEMin & s & RegCmdResRef!MEMax & vbNewLine
                                    ValRef1 = ValRef1 & RegCmdResRef!MEMin & s & RegCmdResRef!MEMax
                                End If
                            Else
                                'Muda de linha se tiver j� tem uma anterior refer�ncia tipo adulto ou crian�a
                                If UmParaX = True Then
                                    ValRef1 = ValRef1 & vbNewLine & IIf(Trim("" & RegCmdResRef!MEMin) = "", "", "1/" & RegCmdResRef!MEMin) & s & IIf(Trim("" & RegCmdResRef!MEMax) = "", "", "1/" & RegCmdResRef!MEMax) & vbNewLine
                                Else
                                    ValRef1 = ValRef1 & vbNewLine & RegCmdResRef!MEMin & s & RegCmdResRef!MEMax & vbNewLine
                                End If
                            End If
                        Else
                            'Refer�ncia tipo Adulto ou crian�a
                            If UmParaX = True Then
                                ValRef1 = ValRef1 & TipoEscRef & IIf(Trim("" & RegCmdResRef!MEMin) = "", "", "1/" & RegCmdResRef!MEMin) & s & IIf(Trim("" & RegCmdResRef!MEMax) = "", "", "1/" & RegCmdResRef!MEMax) & " "
                            Else
                                ValRef1 = ValRef1 & TipoEscRef & RegCmdResRef!MEMin & s & RegCmdResRef!MEMax & " "
                            End If
                        End If
                    Else
                        If TipoEscRef = "" Then
                            'Refer�ncia Simples:
                            If InStr(1, ValRef2, "Adulto") = 0 And InStr(1, ValRef2, "Crian�a") = 0 Then
                                'N�o muda de linha se ainda n�o tem uma anterior refer�ncia tipo adulto ou crian�a
                                ValRef2 = ValRef2 & RegCmdResRef!MEMin & s & RegCmdResRef!MEMax & vbNewLine
                            Else
                                'Muda de linha se tiver j� tem uma anterior refer�ncia tipo adulto ou crian�a
                                ValRef2 = ValRef2 & vbNewLine & RegCmdResRef!MEMin & s & RegCmdResRef!MEMax & vbNewLine
                            End If
                        Else
                            'Refer�ncia tipo Adulto ou crian�a
                            ValRef2 = ValRef2 & TipoEscRef & RegCmdResRef!MEMin & s & RegCmdResRef!MEMax & " "
                        End If
                    End If
                End If
            End If
            
            
            'CUT-OFF
            If BL_HandleNull(RegCmdResRef!HEMin, "") = "" And BL_HandleNull(RegCmdResRef!HVmin, "") = "" Then
                'O CUT-OFF caso exista � sempre para o 1�Resultado
                
                'Cut-Off Negativo?
                If ("" & RegCmdResRef!val_neg_min <> "") Or ("" & RegCmdResRef!val_neg_max <> "") Then
                    If UmParaX = True Then
                        ValRef1 = ValRef1 & IIf("" & RegCmdResRef!val_neg_min = "", "", "1/" & RegCmdResRef!val_neg_min) & RegCmdResRef!t_comp_nmin & "NEGATIVO" & RegCmdResRef!t_comp_nmax & IIf("" & RegCmdResRef!val_neg_max = "", "", "1/" & RegCmdResRef!val_neg_max) & vbNewLine
                    Else
                        ValRef1 = ValRef1 & RegCmdResRef!val_neg_min & RegCmdResRef!t_comp_nmin & BL_HandleNull(RegCmdResRef!com_neg, "Negativo") & RegCmdResRef!t_comp_nmax & RegCmdResRef!val_neg_max & vbNewLine
                    End If
                    cutoff = True
                End If
                
                'Cut-Off Positivo?
                If ("" & RegCmdResRef!val_pos_min <> "") Or ("" & RegCmdResRef!val_pos_max <> "") Then
                    If UmParaX = True Then
                        ValRef1 = ValRef1 & IIf("" & RegCmdResRef!val_pos_min = "", "", "1/" & RegCmdResRef!val_pos_min) & RegCmdResRef!t_comp_pmin & "POSITIVO" & RegCmdResRef!t_comp_pmax & IIf("" & RegCmdResRef!val_pos_max = "", "", "1/" & RegCmdResRef!val_pos_max) & vbNewLine
                    Else
                        ValRef1 = ValRef1 & RegCmdResRef!val_pos_min & RegCmdResRef!t_comp_pmin & BL_HandleNull(RegCmdResRef!com_pos, "Positivo") & RegCmdResRef!t_comp_pmax & RegCmdResRef!val_pos_max & vbNewLine
                    End If
                    cutoff = True
                End If
                
                'Cut-Off Duvidoso?
                If ("" & RegCmdResRef!val_duv_min <> "") Or ("" & RegCmdResRef!val_duv_max <> "") Then
                    If UmParaX = True Then
                        ValRef1 = ValRef1 & IIf("" & RegCmdResRef!val_duv_min = "", "", "1/" & RegCmdResRef!val_duv_min) & RegCmdResRef!t_comp_dmin & "DUVIDOSO" & RegCmdResRef!t_comp_dmax & IIf("" & RegCmdResRef!val_duv_max = "", "", "1/" & RegCmdResRef!val_duv_max) & vbNewLine
                    Else
                        ValRef1 = ValRef1 & RegCmdResRef!val_duv_min & RegCmdResRef!t_comp_dmin & BL_HandleNull(RegCmdResRef!com_duv, "Duvidoso") & RegCmdResRef!t_comp_dmax & RegCmdResRef!val_duv_max & vbNewLine
                    End If
                    cutoff = True
                End If

            End If
            
            RegCmdResRef.MoveNext
        Wend
        RegCmdResRef.Close
        Set RegCmdResRef = Nothing
        
        
        '********************************************************
        'Valores de Refer�ncia para ZONAS:
        'Devolve o Registo com os valores de refer�ncia (Zonas) para os 1� e 2� Resultados !
        ZonaRef1 = ""
        ZonaRef2 = ""
        TotalEstrutZonas = 0
        ReDim EstrutZonas(TotalEstrutZonas)
        
        '1�Resultado
        If Trim(CodRef1) <> "" Then
            CmdResRefZonas.Parameters("COD_ANA_REF").value = CodRef1
            CmdResRefZonas.Parameters("N_REQ").value = regRes!n_req
            Set RegCmdZonasRef = CmdResRefZonas.Execute
            
            While Not RegCmdZonasRef.EOF
                
                If Trim("" & RegCmdZonasRef!ref_min) = "" Or Trim("" & RegCmdZonasRef!ref_max) = "" Then
                    s = ""
                Else
                    s = " - "
                End If
                
                TipoEscRef = ""
                If RegCmdZonasRef!t_idade_inf = gT_Adulto Then TipoEscRef = "Adulto"
                If RegCmdZonasRef!t_idade_inf = gT_Crianca Then TipoEscRef = "Crian�a"
                
                'Sexo
'                If Not BL_HandleNull(RegCmdZonasRef!Sexo, "") = "" Then
'                    'Idade
'                    If Not BL_HandleNull(RegCmdZonasRef!idade_inf, "") = "" Then
'                        If TipoEscRef = "" Then
'                            'Refer�ncia Simples:
'                            ZonaRef1 = ZonaRef1 & SexoUtente & "  (" & RegCmdZonasRef!idade_inf & " " & RegCmdZonasRef!DATAINF & "-" & RegCmdZonasRef!idade_sup & " " & RegCmdZonasRef!DATASUP & ")" & vbNewLine
'                        Else
'                            'Refer�ncia Tipo Adulto ou Crian�a:
'                            ZonaRef1 = ZonaRef1 & SexoUtente & "  (" & TipoEscRef & ")" & vbNewLine
'                        End If
'                    Else
'                        ZonaRef1 = ZonaRef1 & SexoUtente & vbNewLine
'                    End If
'                Else
'                    If Not BL_HandleNull(RegCmdZonasRef!idade_inf, "") = "" Then
'                        If TipoEscRef = "" Then
'                            'Refer�ncia Simples:
'                            ZonaRef1 = ZonaRef1 & RegCmdZonasRef!idade_inf & " " & RegCmdZonasRef!DATAINF & "-" & RegCmdZonasRef!idade_sup & " " & RegCmdZonasRef!DATASUP & vbNewLine
'                        Else
'                            'Refer�ncia Tipo Adulto ou Crian�a:
'                            ZonaRef1 = ZonaRef1 & TipoEscRef & vbNewLine
'                        End If
'                    End If
'                End If
                
                'Diagn�sticos
                If Not BL_HandleNull(RegCmdZonasRef!Descr_Diag, "") = "" Then
                    ZonaRef1 = ZonaRef1 & "Diagn�stico:" & RegCmdZonasRef!Descr_Diag & vbNewLine
                End If
                
                'Outros
                If Not BL_HandleNull(RegCmdZonasRef!descr_or_zonas, "") = "" Then
                    ZonaRef1 = ZonaRef1 & RegCmdZonasRef!descr_or_zonas & vbNewLine
                End If
                
                'Valores
                'An�lises tipo 1/X?
                If UmParaX = True Then
                    If (BL_HandleNull(RegCmdZonasRef!ref_min, "") <> "" Or BL_HandleNull(RegCmdZonasRef!ref_max, "") <> "") And BL_HandleNull(RegCmdZonasRef!flg_inibe_val, 0) = 0 Then
                        ZonaRef1 = ZonaRef1 & IIf(Trim("" & RegCmdZonasRef!ref_min) = "", "", "1/" & RegCmdZonasRef!ref_min) & s & IIf(Trim("" & RegCmdZonasRef!ref_max) = "", "", "1/" & RegCmdZonasRef!ref_max) & vbNewLine
                    Else
                        s = ""
                    End If
                Else
                    If (BL_HandleNull(RegCmdZonasRef!ref_min, "") <> "" Or BL_HandleNull(RegCmdZonasRef!ref_max, "") <> "") And BL_HandleNull(RegCmdZonasRef!flg_inibe_val, 0) = 0 Then
                        ZonaRef1 = ZonaRef1 & RegCmdZonasRef!ref_min & s & RegCmdZonasRef!ref_max & vbNewLine
                    Else
                        s = ""
                    End If
                End If
                
                'Texto Livre
                If Not BL_HandleNull(RegCmdZonasRef!txt_livre, "") = "" Then
                    ZonaRef1 = ZonaRef1 & RegCmdZonasRef!txt_livre
                End If
                
                If UCase(ZonaRef1) = "VER TABELA EM ANEXO" Then
                    formulas.VerTabelaAnexo = True
                End If
                RegCmdZonasRef.MoveNext
            Wend
            RegCmdZonasRef.Close
            Set RegCmdZonasRef = Nothing
        End If
        
        If Trim(CodRef2) <> "" Then
            CmdResRefZonas.Parameters("COD_ANA_REF").value = CodRef2
            CmdResRefZonas.Parameters("N_REQ").value = regRes!n_req
            Set RegCmdZonasRef = CmdResRefZonas.Execute
            While Not RegCmdZonasRef.EOF
                If Trim("" & RegCmdZonasRef!ref_min) = "" Or Trim("" & RegCmdZonasRef!ref_max) = "" Then
                    s = ""
                Else
                    s = " - "
                End If
                TipoEscRef = ""
                If RegCmdZonasRef!t_idade_inf = gT_Adulto Then TipoEscRef = "Adulto"
                If RegCmdZonasRef!t_idade_inf = gT_Crianca Then TipoEscRef = "Crian�a"
                'Sexo
                If Not BL_HandleNull(RegCmdZonasRef!Sexo, "") = "" Then
                    'Idade
                    If Not BL_HandleNull(RegCmdZonasRef!idade_inf, "") = "" Then
                        If TipoEscRef = "" Then
                            'Refer�ncia Simples:
                            ZonaRef2 = ZonaRef2 & formulas.SexoUtente & "  (" & RegCmdZonasRef!idade_inf & " " & RegCmdZonasRef!DATAINF & " - " & RegCmdZonasRef!idade_sup & " " & RegCmdZonasRef!DATASUP & ")" & vbNewLine
                        Else
                            'Refer�ncia Tipo Adulto ou Crian�a:
                            ZonaRef2 = ZonaRef2 & formulas.SexoUtente & "  (" & TipoEscRef & ")" & vbNewLine
                        End If
                    Else
                        ZonaRef2 = ZonaRef2 & formulas.SexoUtente & vbNewLine
                    End If
                Else
                    If Not BL_HandleNull(RegCmdZonasRef!idade_inf, "") = "" Then
                        If TipoEscRef = "" Then
                            'Refer�ncia Simples:
                            ZonaRef2 = ZonaRef2 & RegCmdZonasRef!idade_inf & " " & RegCmdZonasRef!DATAINF & " - " & RegCmdZonasRef!idade_sup & " " & RegCmdZonasRef!DATASUP & vbNewLine
                        Else
                            'Refer�ncia Tipo Adulto ou Crian�a:
                            ZonaRef2 = ZonaRef2 & TipoEscRef & vbNewLine
                        End If
                    End If
                End If
                
                'Diagn�sticos
                If Not BL_HandleNull(RegCmdZonasRef!Descr_Diag, "") = "" Then
                    ZonaRef2 = ZonaRef2 & "Diagn�stico:" & RegCmdZonasRef!Descr_Diag & vbNewLine
                End If
                
                'Outros
                If Not BL_HandleNull(RegCmdZonasRef!descr_or_zonas, "") = "" Then
                    ZonaRef2 = ZonaRef2 & RegCmdZonasRef!descr_or_zonas & vbNewLine
                End If
                
                'Valores
                ZonaRef2 = ZonaRef2 & RegCmdZonasRef!ref_min & s & RegCmdZonasRef!ref_max & vbNewLine
                
                'Texto Livre
                If Not BL_HandleNull(RegCmdZonasRef!txt_livre, "") = "" Then
                    ZonaRef2 = ZonaRef2 & RegCmdZonasRef!txt_livre
                End If
                
                RegCmdZonasRef.MoveNext
            Wend
            RegCmdZonasRef.Close
            Set RegCmdZonasRef = Nothing
        End If
        '********************************************************
        'OBT�M O COMENT�RIO E OBSERVA��O PARA A AN�LISE E REQUISI��O EM CAUSA
        ObservAnalise = ""
        ObservResultado = ""
        TotalEstrutObsAnalise = 0
        RText.text = ""
        ReDim EstrutObsAnalise(TotalEstrutObsAnalise)
        TotalEstrutObsResultado = 0
        ReDim EstrutObsResultado(TotalEstrutObsResultado)
'        If RegRes!N_Req = BL_String2Double(NumReq) Then
            CmdResObserv.Parameters("SEQ_ANA_S").value = regRes!seq_ana_s
            CmdResObserv.Parameters("SEQ_REALIZA").value = regRes!seq_realiza
            
            Set RegCmd = CmdResObserv.Execute
            While Not RegCmd.EOF
                If BL_HandleNull(RegCmd!OBSERV_rtf, "") <> "" Then
                    RText.TextRTF = "" & RegCmd!OBSERV_rtf
                Else
                    If Len(Trim(BL_HandleNull(RegCmd!OBSERV, ""))) >= 2 Then
                        If Mid(Trim(BL_HandleNull(RegCmd!OBSERV, "")), 1, 2) = "\\" Then
                            RText.TextRTF = "" & Replace(BL_HandleNull(RegCmd!OBSERV, ""), "\\", "")
                        Else
                            RText.TextRTF = "" & BL_HandleNull(RegCmd!OBSERV, "")
                        End If
                    End If
                End If
                RText.SelStart = 0
                RText.SelLength = Len(RText.TextRTF)

                
                If Trim(UCase(RegCmd!tipo)) = "A" Then
                    If InStr(1, BL_HandleNull(RegCmd!OBSERV, ""), "rtf1") > 0 Then
                        RText.TextRTF = BL_HandleNull(RegCmd!OBSERV, "")
                    Else
                        RText.text = BL_HandleNull(RegCmd!OBSERV, "")
                    End If
                    If Trim(RText.text) <> "" Then
                        ObservAnalise_Aux = RText.text & vbCrLf
                        ObservAnalise = RText.text
                    Else
                        ObservAnalise = ""
                    End If
                Else
                    If Trim(RText.text) <> "" Then
                        ObservResultado_Aux = RText.text & vbCrLf
                        ObservResultado = RText.TextRTF
                    Else
                        ObservResultado = ""
                    End If
                End If
                
                RegCmd.MoveNext
            Wend
            RegCmd.Close
            Set RegCmd = Nothing
'        End If
        
        
        
        IR_LimpaVariaveis ResNum1Unid1, ResNum1Unid2, ResNum2Unid1, ResNum2Unid2, resAlf1, resAlf2, FlagRes1Anormal, _
                          FlagRes2Anormal
        CmdResAlfan.Parameters("SEQ_REALIZA").value = regRes!seq_realiza

        Set RegCmd = CmdResAlfan.Execute
                     
        ResAlf1_Ant1 = ""
        Dt_ResAlf1_Ant1 = ""
        LocalRes_Ant1 = ""
        ResAlf1_Ant2 = ""
        Dt_ResAlf1_Ant2 = ""
        LocalRes_Ant2 = ""
        ResAlf1_Ant3 = ""
        Dt_ResAlf1_Ant3 = ""
        LocalRes_Ant3 = ""
        
        'brunodsantos 03.12.2015
'        SegundoResultado.Seg_ResAlf1_Ant1 = ""
'        SegundoResultado.Seg_Dt_ResAlf1_Ant1 = ""
'        SegundoResultado.Seg_ResAlf1_Ant2 = ""
'        SegundoResultado.Seg_Dt_ResAlf1_Ant2 = ""
'        SegundoResultado.Seg_ResAlf1_Ant3 = ""
'        SegundoResultado.Seg_Dt_ResAlf1_Ant3 = ""
        
                     
        'Pode haver mais do que um Resultado (1� e 2� resultados=>ORDENADOS!)
        While Not RegCmd.EOF
            If RegCmd!Result = cSemResultado Then
                GoTo continua
            'Resultado Alfanum�rico (c�digo de uma Frase)
            ElseIf InStr(1, RegCmd!Result, "\\") <> 0 Then
                CodFrase = UCase(Mid(RegCmd!Result, 3, Len(RegCmd!Result) - 2))
                CmdResDescrFrase.Parameters(0).value = CodFrase
                Set RegDescrFrase = CmdResDescrFrase.Execute
                If Not RegDescrFrase.EOF Then
                    If RegCmd!N_Res = 1 Then
                        '1� RESULTADO
                        ResNum1Unid1 = Trim("" & RegDescrFrase!descr_frase)
                    Else
                        '2� RESULTADO
                        ResNum2Unid1 = Trim("" & RegDescrFrase!descr_frase)
                    End If
                End If
                
                BL_ResultadosAnteriores regRes!cod_ana_s, BL_HandleNull(RegCmd!res_ant1, ""), BL_HandleNull(RegCmd!res_ant2, ""), BL_HandleNull(RegCmd!res_ant3, ""), _
                                        BL_HandleNull(RegCmd!dt_res_ant1, ""), BL_HandleNull(RegCmd!dt_res_ant2, ""), BL_HandleNull(RegCmd!dt_res_ant3, ""), _
                                        BL_HandleNull(RegCmd!local_ant1, ""), BL_HandleNull(RegCmd!local_ant2, ""), BL_HandleNull(RegCmd!local_ant3, ""), _
                                        ResAlf1_Ant1, ResAlf1_Ant2, ResAlf1_Ant3, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3, LocalRes_Ant1, LocalRes_Ant2, LocalRes_Ant3, regRes!seq_utente, InibeResultadosAnteriores
            Else
                'Resultado Num�rico/Alfanum�rico
                '------------------------------------------------------------------------
                'A UNIDADE ACTIVA � SEMPRE A UNIDADE1 (COLUNA1)
                
                '1�RESULTADO
                If RegCmd!N_Res = 1 Then
                    If IsNumeric("" & RegCmd!Result) And InStr(1, RegCmd!Result, "+") = 0 And InStr(1, RegCmd!Result, "-") = 0 Then
                        'Resultado Num�rico
                        '------------------
                        
                        If IsNull(regRes!Casas_dec_1) Then
                             aux_casas = 0
                        Else
                            aux_casas = CInt(regRes!Casas_dec_1)
                        End If
                        
                        '* Valor da Unidade Activa
                        
                        ResNum1Unid1 = BL_String2Double("" & RegCmd!Result, "" & regRes!Casas_dec_1)

                        If (CLng(aux_casas) >= 0) Then
                            ResNum1Unid1 = Round(BL_String2Double("" & RegCmd!Result), CLng(aux_casas))
                        Else
                            ResNum1Unid1 = BL_String2Double("" & RegCmd!Result)
                        End If
                        
                        'For�a as casas decimais do tipo 43.0
                        ResNum1Unid1 = BL_Forca_Casas_Decimais(ResNum1Unid1, aux_casas)
                        
                        'For�ar o Zero na ultima posi��o do resultado, caso este acabe em zero
                        'If Mid(RegCmd!Result, Len(RegCmd!Result), 1) = "0" And Len(ResNum1Unid1) < Len(RegCmd!Result) Then
                         '   ResNum1Unid1 = ResNum1Unid1 & "0"
                        'End If
                        
                        '------------------
                        
                        '* Valor da Unidade n�o Activa
                    
                        'Uma das unidades � nula?
                        'O resultado � do tipo '{Ver Observa��es}'?
                        'O resultado � do tipo 'Sem Resultado'?
                        'O factor de convers�o � nulo?
                        If ("" & regRes!unid_1_res1 = "") Or ("" & regRes!unid_2_res1 = "") Or _
                            ("" & RegCmd!Result = cVerObservacoes) Or ("" & RegCmd!Result = cSemResultado) Or _
                            ("" & regRes!fac_conv_unid1 = "") Then
                                ResNum1Unid2 = ""
                        Else
                            'O FACTOR DE CONVERS�O � SEMPRE DA UNIDADE1 EM RELA��O � UNIDADE2!
                            If "" & regRes!FLG_UNID_ACT1 = "1" Then
                                'Valor registado em rela��o � unidade1?
                                If BL_HandleNull(regRes!Casas_dec_1, "") = "" Then
                                    ResNum1Unid2 = BL_String2Double(ResNum1Unid1) * BL_String2Double(regRes!fac_conv_unid1)
                                Else
                                    ResNum1Unid2 = BL_String2Double(CStr(BL_String2Double(ResNum1Unid1) * BL_String2Double(regRes!fac_conv_unid1)), "" & regRes!Casas_dec_1)
                                End If
                            Else
                                'Valor registado em rela��o � unidade2?
                                If BL_HandleNull(regRes!Casas_dec_1, "") = "" Then
                                    ResNum1Unid2 = BL_String2Double(ResNum1Unid1) / BL_String2Double(regRes!fac_conv_unid1)
                                Else
                                    ResNum1Unid2 = BL_String2Double(CStr(BL_String2Double(ResNum1Unid1) / BL_String2Double(regRes!fac_conv_unid1)), "" & regRes!Casas_dec_1)
                                End If
                            End If
                        End If
                    Else
                        'Resultado Alfanum�rico
                        '----------------------
                        
                        ResNum1Unid1 = RegCmd!Result
                    End If
                    
                        ' Resultados Anteriores (Resultado 1)
                        BL_ResultadosAnteriores regRes!cod_ana_s, BL_HandleNull(RegCmd!res_ant1, ""), BL_HandleNull(RegCmd!res_ant2, ""), BL_HandleNull(RegCmd!res_ant3, ""), _
                                            BL_HandleNull(RegCmd!dt_res_ant1, ""), BL_HandleNull(RegCmd!dt_res_ant2, ""), BL_HandleNull(RegCmd!dt_res_ant3, ""), _
                                            BL_HandleNull(RegCmd!local_ant1, ""), BL_HandleNull(RegCmd!local_ant2, ""), BL_HandleNull(RegCmd!local_ant3, ""), _
                                            ResAlf1_Ant1, ResAlf1_Ant2, ResAlf1_Ant3, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3, LocalRes_Ant1, LocalRes_Ant2, LocalRes_Ant3, BL_HandleNull(regRes!seq_utente, 0), InibeResultadosAnteriores
                    
                Else
                    '2�RESULTADO
                    
                    If IsNumeric("" & RegCmd!Result) Then
                        'Resultado Num�rico
                        '------------------
                        ' For�a as casa decimais tipo 43.0.
                        If IsNull(regRes!casas_dec_2) Then
                             aux_casas = 0
                        Else
                            aux_casas = CInt(regRes!casas_dec_2)
                        End If
                        
                        '* Valor da Unidade Activa
                        ResNum2Unid1 = BL_String2Double("" & RegCmd!Result, "" & regRes!casas_dec_2)

                        If (CLng(aux_casas) >= 0) Then
                            ResNum2Unid1 = Round(BL_String2Double("" & RegCmd!Result), CLng(aux_casas))
                        Else
                            ResNum2Unid1 = BL_String2Double("" & RegCmd!Result)
                        End If
                        
                        ResNum2Unid1 = BL_Forca_Casas_Decimais(ResNum2Unid1, aux_casas)
                        
                    
                        'Uma das unidades � nula?
                        'O resultado � do tipo '{Ver Observa��es}'?
                        'O resultado � do tipo 'Sem Resultado'?
                        'O factor de convers�o � nulo?
                        If BL_HandleNull(regRes!unid_1_res2, "") = "" Or BL_HandleNull(regRes!unid_2_res2, "") = "" Or _
                            "" & RegCmd!Result = cVerObservacoes Or "" & RegCmd!Result = cSemResultado Or _
                            BL_HandleNull(regRes!fac_conv_unid2, "") = "" Then
                                ResNum2Unid2 = ""
                        Else
                            'O FACTOR DE CONVERS�O � SEMPRE DA UNIDADE1 EM RELA��O � UNIDADE2!
                            If "" & regRes!FLG_UNID_ACT2 = "1" Then
                                'Valor registado em rela��o � unidade1?
                                If BL_HandleNull(regRes!casas_dec_2, "") = "" Then
                                    ResNum2Unid2 = BL_String2Double(ResNum2Unid1) * BL_String2Double(regRes!fac_conv_unid2)
                                Else
                                    ResNum2Unid2 = BL_String2Double(CStr(BL_String2Double(ResNum2Unid1) * BL_String2Double(regRes!fac_conv_unid2)), "" & regRes!casas_dec_2)
                                End If
                            Else
                                'Valor registado em rela��o � unidade2?
                                If BL_HandleNull(regRes!casas_dec_2, "") = "" Then
                                    ResNum2Unid2 = BL_String2Double(ResNum2Unid1) / BL_String2Double(regRes!fac_conv_unid2)
                                Else
                                    ResNum2Unid2 = BL_String2Double(CStr(BL_String2Double(ResNum2Unid1) / BL_String2Double(regRes!fac_conv_unid2)), "" & regRes!casas_dec_2)
                                End If
                            End If
                        End If
                    Else
                        'Resultado Alfanum�rico
                        '----------------------
                        
                        ResNum2Unid1 = RegCmd!Result
                    End If
                    
                    'brunodsantos 03.12.2015
                   
                    BL_ResultadosAnteriores regRes!cod_ana_s, BL_HandleNull(RegCmd!res_ant1, ""), BL_HandleNull(RegCmd!res_ant2, ""), BL_HandleNull(RegCmd!res_ant3, ""), _
                                            BL_HandleNull(RegCmd!dt_res_ant1, ""), BL_HandleNull(RegCmd!dt_res_ant2, ""), BL_HandleNull(RegCmd!dt_res_ant3, ""), _
                                            BL_HandleNull(RegCmd!local_ant1, ""), BL_HandleNull(RegCmd!local_ant2, ""), BL_HandleNull(RegCmd!local_ant3, ""), _
                                            SegundoResultado.Seg_ResAlf1_Ant1, SegundoResultado.Seg_ResAlf1_Ant2, SegundoResultado.Seg_ResAlf1_Ant3, SegundoResultado.Seg_Dt_ResAlf1_Ant1, _
                                            SegundoResultado.Seg_Dt_ResAlf1_Ant2, SegundoResultado.Seg_Dt_ResAlf1_Ant3, "", "", "", BL_HandleNull(regRes!seq_utente, 0), InibeResultadosAnteriores
                
                End If
                '------------------------------------------------------------------------
            End If
            RegCmd.MoveNext
        Wend
        RegCmd.Close
        Set RegCmd = Nothing
        
        
        
        '___________________________________________________________________
        'Resultados Frase
        'RegRes!t_result=>(gT_Frase)
        CmdResFrase.Parameters("SEQ_REALIZA").value = regRes!seq_realiza

        Set RegCmd = CmdResFrase.Execute
        'Pode haver mais do que um Resultado (1� e 2� resultados=>ORDENADOS!)
        While Not RegCmd.EOF
            'Obt�m os Resultados
            If RegCmd!N_Res = 1 Then
                resAlf1 = resAlf1 & Trim("" & RegCmd!descr_frase) & vbNewLine
            Else
                resAlf2 = resAlf2 & Trim("" & RegCmd!descr_frase) & vbNewLine
            End If
            RegCmd.MoveNext
        Wend
        RegCmd.Close
        Set RegCmd = Nothing
        
        '___________________________________________________________________
        'Resultados Antibiograma (Tsq)
        CmdResTsq.Parameters("SEQ_REALIZA").value = regRes!seq_realiza

        Set RegCmd = CmdResTsq.Execute
        
        While Not RegCmd.EOF
            s = ""
            Select Case UCase(Trim("" & RegCmd!res_sensib))
                Case "R"
                    s = "Resistente"
                Case "S"
                    s = "Sens�vel"
                Case "I"
                    s = "Interm�dio"
                Case "P"
                    s = "Positivo"
                Case "N"
                    s = "Negativo"
                Case Else
                    s = ""
            End Select
            'Obt�m os Resultados
            If RegCmd!N_Res = 1 Then
                If gLAB = "HPD" Then
                    resAlf1 = resAlf1 & Trim("" & RegCmd!descr_antibio & Replace(Space(40 - Len("" & RegCmd!descr_antibio) - 1), " ", ".") & s & Space(12 - Len(s))) & vbNewLine
                Else
                    resAlf1 = resAlf1 & Trim("" & RegCmd!descr_antibio & Replace(Space(40 - Len("" & RegCmd!descr_antibio) - 1), " ", ".") & s & Space(12 - Len(s)) & IIf(Trim(BL_HandleNull(RegCmd!CMI, "")) = "", "", "C.M.I.: ") & Trim(BL_HandleNull(RegCmd!CMI, ""))) & vbNewLine
                End If
            Else
'                If gLAB = "HPD" Then
'                    ResAlf2 = ResAlf2 & Trim("" & RegCmd!descr_antibio & Replace(Space(40 - Len("" & RegCmd!descr_antibio) - 1), " ", ".") & s & Space(12 - Len(s))) & vbNewLine
'                Else
                    resAlf2 = resAlf2 & Trim("" & RegCmd!descr_antibio & Replace(Space(40 - Len("" & RegCmd!descr_antibio) - 1), " ", ".") & s & Space(12 - Len(s)) & IIf(Trim(BL_HandleNull(RegCmd!CMI, "")) = "", "", "C.M.I.: ") & Trim(BL_HandleNull(RegCmd!CMI, ""))) & vbNewLine
'                End If
            End If
            RegCmd.MoveNext
        Wend
        RegCmd.Close
        Set RegCmd = Nothing
        
        '___________________________________________________________________
        'Resultados Micro
        'RegRes!t_result=>(gT_Microrganismo)
        params(gArgumentoRespValMedMed) = RespValMedMed
        params(gArgumentoRespValMedTec) = RespValMedTec
        If gNovoRelatorioMicro = mediSim Then
            Flg_ResMicro = False
            Flg_ResMicro = IR_TrataMicro(NumReq, TotalEstrutMicro, EstrutMicro(), Flg_ResMicro, InibeResultadosAnteriores, regRes, CmdResMicro, _
                RegCmd, resAlf1, resAlf2, CmdResMicroTsq, RegCmdMTsq, VecSplit(), codRespMedicos, formulas.RespMedicos, CodRespTecnicos, _
                formulas.respTecnicos, Unid1ResNum1, ObservResultado, FlagResComent, ComAutomatico, _
                ResNum1Unid1, FlagRTFAnalise, Unid2ResNum2, ValRef2, ValRef1, ZonaRef1, Metodo1, _
                Metodo2, ResNum1Unid2, FlagRes1Anormal, FlagRes2Anormal, OrdemImpressao, LinhasPagina, RsContaMembros, UltimaComplexa, ContaMembrosComplexa, _
                ContaMembrosPerfil, UltimoPerfil, UltimoGrupo, UltimoSubgrupo, UltimaClasse, UltimaDt1, UltimaDt2, UltimaDt3, _
                EstrutZonas(), TotalEstrutZonas, EstrutObsResultado(), TotalEstrutObsResultado, ObservAnalise, Grupo, subGrupo, Classe, Perfil, complexa, simples, _
                cutoff, registos, BL_HandleNull(regRes!ord_grupo, "0"))
        Else
            TotalEstrutMicro = 0
            ReDim EstrutMicro(TotalEstrutMicro)
            Flg_ResMicro = False
            
            CmdResMicro.Parameters("SEQ_REALIZA").value = regRes!seq_realiza
            Set RegCmd = CmdResMicro.Execute
            While Not RegCmd.EOF
                Flg_ResMicro = True
                'Obt�m os Resultados
                If RegCmd!N_Res = 1 Then
                    resAlf1 = resAlf1 & Trim("" & RegCmd!descr_microrg & IIf(Trim(BL_HandleNull(RegCmd!Quantif, "")) = "", "", Replace(Space(50 - Len(RegCmd!descr_microrg) + 1), " ", ".")) & IIf(Trim(BL_HandleNull(RegCmd!Quantif, "")) = "", "", "  Qt.: ") & BL_HandleNull(RegCmd!Quantif, "")) & vbNewLine
                Else
                    resAlf2 = resAlf2 & Trim("" & RegCmd!descr_microrg & IIf(Trim(BL_HandleNull(RegCmd!Quantif, "")) = "", "", Replace(Space(50 - Len(RegCmd!descr_microrg) + 1), " ", ".")) & IIf(Trim(BL_HandleNull(RegCmd!Quantif, "")) = "", "", "  Qt.: ") & BL_HandleNull(RegCmd!Quantif, "")) & vbNewLine
                End If
                
                'Verifica se tem Tsq
                CmdResMicroTsq.Parameters("SEQ_REALIZA").value = regRes!seq_realiza
                CmdResMicroTsq.Parameters("COD_MICRO").value = RegCmd!cod_micro
                CmdResMicroTsq.Parameters("N_RES").value = RegCmd!N_Res
                Set RegCmdMTsq = CmdResMicroTsq.Execute
                While RegCmdMTsq.EOF <> True
                    Select Case UCase(Trim("" & RegCmdMTsq!res_sensib))
                        Case "R"
                            s = "Resistente"
                        Case "S"
                            s = "Sens�vel"
                        Case "I"
                            s = "Interm�dio"
                        Case "P"
                            s = "Positivo"
                        Case "N"
                            s = "Negativo"
                        Case Else
                            s = ""
                    End Select
                    
                    'Obt�m os Resultados
                    If RegCmd!N_Res = 1 Then
                        If gLAB = "HPD" Then
                            resAlf1 = resAlf1 & "  " & Trim("" & RegCmdMTsq!descr_antibio & IIf(Trim(BL_HandleNull(RegCmdMTsq!CMI, "")) = "" And s = "", "", Replace(Space(35 - Len("" & RegCmdMTsq!descr_antibio) - 1), " ", ".")) & s & Space(12 - Len(s))) & vbNewLine
                        Else
                            resAlf1 = resAlf1 & "  " & Trim("" & RegCmdMTsq!descr_antibio & IIf(Trim(BL_HandleNull(RegCmdMTsq!CMI, "")) = "" And s = "", "", Replace(Space(35 - Len("" & RegCmdMTsq!descr_antibio) - 1), " ", ".")) & s & Space(12 - Len(s)) & IIf(Trim(BL_HandleNull(RegCmdMTsq!CMI, "")) = "", "", "C.M.I.: ") & Trim(BL_HandleNull(RegCmdMTsq!CMI, ""))) & vbNewLine
                        End If
                    Else
                        If gLAB = "HPD" Then
                            resAlf2 = resAlf2 & "  " & Trim("" & RegCmdMTsq!descr_antibio & IIf(Trim(BL_HandleNull(RegCmdMTsq!CMI, "")) = "" And s = "", "", Replace(Space(35 - Len("" & RegCmdMTsq!descr_antibio) - 1), " ", ".")) & s & Space(12 - Len(s))) & vbNewLine
                        Else
                            resAlf2 = resAlf2 & "  " & Trim("" & RegCmdMTsq!descr_antibio & IIf(Trim(BL_HandleNull(RegCmdMTsq!CMI, "")) = "" And s = "", "", Replace(Space(35 - Len("" & RegCmdMTsq!descr_antibio) - 1), " ", ".")) & s & Space(12 - Len(s)) & IIf(Trim(BL_HandleNull(RegCmdMTsq!CMI, "")) = "", "", "C.M.I.: ") & Trim(BL_HandleNull(RegCmdMTsq!CMI, ""))) & vbNewLine
                        End If
                    End If
                    RegCmdMTsq.MoveNext
                Wend
                If RegCmd!N_Res = 1 Then
                    resAlf1 = resAlf1 & vbNewLine
                Else
                    resAlf2 = resAlf2 & vbNewLine
                End If
                RegCmd.MoveNext
            Wend
            RegCmd.Close
            Set RegCmd = Nothing
            
        End If
        '___________________________________________________________________
        
        '**********************************************************************
        'OBT�M OS COMENT�RIOS AUTOM�TICOS PARA CADA AN�LISE
        
        'O coment�rio substitui a an�lise?
        If Trim("" & regRes!flg_concl_res) = "1" Then
            FlagResComent = True
        Else
            FlagResComent = False
        End If
            
        'VERIFICA SE � PARA SAIR
        ComAutomatico = ""
        If ResNum1Unid1 <> cVerObservacoes And ResNum2Unid2 <> cVerObservacoes Then
            CmdResCom.Parameters("COD_ANA_S").value = regRes!cod_ana_s
            Set RegCmd = CmdResCom.Execute
            While Not RegCmd.EOF
                conclusoes_codificadas = True
                s = BL_FormulaDescodifica(NumReq, IIf(EstadoReq = gEstadoReqHistorico, True, False), RegCmd!descr_formula)
                If s <> "" And Trim(UCase(s)) <> "FALSE" Then
                    If InStr(1, s, "VEROBSERVA��ES") = 0 And InStr(1, s, "SEMRESULTADO") = 0 Then
                       'soliveira a acrescentar a nova vers�o
                        If InStr(1, RegCmd!descr_conc, "R=") = 1 Then
                            'HDM: resultado de formula, subsitui resultado
                            s = BL_FormulaDescodifica(NumReq, IIf(EstadoReq = gEstadoReqHistorico, True, False), Mid(RegCmd!descr_conc, 3))
                            'For�a as casas decimais do tipo 43.0
                            s = BL_Forca_Casas_Decimais(s, aux_casas)
                            ComAutomatico = ComAutomatico & s & vbNewLine
                            FlagResComent = True
                        Else
                            ComAutomatico = ComAutomatico & RegCmd!descr_conc & vbNewLine
                        End If
                    End If
                End If
                RegCmd.MoveNext
            Wend
        End If
        
        
        'Actualiza os Responsaveis T�cnicos e M�dicos pela valida��o da an�lise
        If Flg_ResMicro = False Or gNovoRelatorioMicro <> mediSim Then
        'M�dicos
        If gLAB = "HFM" And Grupo <> UltimoGrupo Then
            formulas.RespMedicos = ""
            codRespMedicos = ""
            RespValMedMed = ""
            RespValMedTec = ""
        End If
        If (InStr(1, formulas.RespMedicos, regRes!nome_med) = 0 Or Grupo <> UltimoGrupo) And BL_HandleNull(regRes!nome_med, "") <> "" Then
        'If InStr(1, CodRespMedicos, "-" & RegRes!cod_utilizador_med & "-") = 0 Then
            
            GrupoUserVal = regRes!cod_gr_util_med
                If Grupo = UltimoGrupo Then
                    If gValidacaoUnicaPorGrupo = 1 Then
                        formulas.RespMedicos = regRes!nome_med
                        If GrupoUserVal = gGrupoTecnicos Then
                            RespValMedTec = regRes!nome_med
                            RespValMedMed = ""
                        'rcoelho 22.08.2013 chvng-4412
                        ElseIf GrupoUserVal = gGrupoTecnicosSuperiores Then
                            If gGrupoTecSupValidacao = gGrupoTecnicos Then
                                RespValMedTec = regRes!nome_med
                                RespValMedMed = ""
                            Else
                                RespValMedTec = ""
                                RespValMedMed = regRes!nome_med
                            End If
                        Else
                            RespValMedTec = ""
                            RespValMedMed = regRes!nome_med
                        
                        End If
                    Else
                        formulas.RespMedicos = formulas.RespMedicos & regRes!nome_med
                        If GrupoUserVal = gGrupoTecnicos Then
                            RespValMedTec = RespValMedTec & regRes!nome_med
                        'rcoelho 22.08.2013 chvng-4412
                        ElseIf GrupoUserVal = gGrupoTecnicosSuperiores Then
                            If gGrupoTecSupValidacao = gGrupoTecnicos Then
                                RespValMedTec = RespValMedTec & regRes!nome_med
                            Else
                                RespValMedMed = RespValMedMed & regRes!nome_med
                            End If
                        Else
                            RespValMedMed = RespValMedMed & regRes!nome_med
                        
                        End If
                    End If
                Else
                    formulas.RespMedicos = regRes!nome_med
                    If GrupoUserVal = gGrupoTecnicos Then
                        RespValMedTec = regRes!nome_med
                        RespValMedMed = ""
                    'rcoelho 22.08.2013 chvng-4412
                    ElseIf GrupoUserVal = gGrupoTecnicosSuperiores Then
                        If gGrupoTecSupValidacao = gGrupoTecnicos Then
                            RespValMedTec = regRes!nome_med
                            RespValMedMed = ""
                        Else
                            RespValMedTec = ""
                            RespValMedMed = regRes!nome_med
                        End If
                    Else
                        RespValMedTec = ""
                        RespValMedMed = regRes!nome_med
                    
                    End If
                End If
            'End If
            
            'RespMedicos = RespMedicos & ", " & RegRes!Nome_med & vbNewLine
            If (gLAB = cHSMARTA) Then
                If (InStr(1, formulas.RespMedicos, regRes!nome_med)) = 0 Then
                    formulas.RespMedicos = formulas.RespMedicos & " - " & IIf(BL_HandleNull(regRes!titulo_med, "") = "", regRes!nome_med, regRes!titulo_med) & vbNewLine
                End If
            Else
                formulas.RespMedicos = formulas.RespMedicos & IIf(BL_HandleNull(regRes!titulo_med, "") = "", "" & vbCrLf, IIf(gLAB = "HPD_GEN", vbCrLf, " - ") & regRes!titulo_med & vbCrLf) & IIf(BL_HandleNull(regRes!telef_med, "") = "", "", "Tel.: " & regRes!telef_med & vbCrLf) & IIf(BL_HandleNull(regRes!email_med, "") = "", "", "e-mail: " & regRes!email_med & vbCrLf)
                If GrupoUserVal = gGrupoTecnicos Then
                    RespValMedTec = RespValMedTec & IIf(BL_HandleNull(regRes!titulo_med, "") = "", "" & vbCrLf, IIf(gLAB = "HPD_GEN", vbCrLf, " - ") & regRes!titulo_med & vbCrLf) & IIf(BL_HandleNull(regRes!telef_med, "") = "", "", "Tel.: " & regRes!telef_med & vbCrLf) & IIf(BL_HandleNull(regRes!email_med, "") = "", "", "e-mail: " & regRes!email_med & vbCrLf)
                'rcoelho 22.08.2013 chvng-4412
                ElseIf GrupoUserVal = gGrupoTecnicosSuperiores Then
                    If gGrupoTecSupValidacao = gGrupoTecnicos Then
                        RespValMedTec = RespValMedTec & IIf(BL_HandleNull(regRes!titulo_med, "") = "", "" & vbCrLf, IIf(gLAB = "HPD_GEN", vbCrLf, " - ") & regRes!titulo_med & vbCrLf) & IIf(BL_HandleNull(regRes!telef_med, "") = "", "", "Tel.: " & regRes!telef_med & vbCrLf) & IIf(BL_HandleNull(regRes!email_med, "") = "", "", "e-mail: " & regRes!email_med & vbCrLf)
                    Else
                        RespValMedMed = RespValMedMed & IIf(BL_HandleNull(regRes!titulo_med, "") = "", "" & vbCrLf, IIf(gLAB = "HPD_GEN", vbCrLf, " - ") & regRes!titulo_med & vbCrLf) & IIf(BL_HandleNull(regRes!telef_med, "") = "", "", "Tel.: " & regRes!telef_med & vbCrLf) & IIf(BL_HandleNull(regRes!email_med, "") = "", "", "e-mail: " & regRes!email_med & vbCrLf)
                    End If
                Else
                    RespValMedMed = RespValMedMed & IIf(BL_HandleNull(regRes!titulo_med, "") = "", "" & vbCrLf, IIf(gLAB = "HPD_GEN", vbCrLf, " - ") & regRes!titulo_med & vbCrLf) & IIf(BL_HandleNull(regRes!telef_med, "") = "", "", "Tel.: " & regRes!telef_med & vbCrLf) & IIf(BL_HandleNull(regRes!email_med, "") = "", "", "e-mail: " & regRes!email_med & vbCrLf)
                End If
            End If
            
            If gLAB = "HFM" Or gLAB = "CHVNG" Then
                If InStr(1, codRespMedicos, "-" & regRes!cod_utilizador_med & "-") = 0 Then
                    codRespMedicos = codRespMedicos & "-" & regRes!cod_utilizador_med & "-" & ","
                End If
            Else
                codRespMedicos = "-" & regRes!cod_utilizador_med & "-" & ","
            End If
        End If
        
        ' T�cnicos.
        ' Verifica se foi respons�vel por alguma Valida��o M�dica (em caso afirmativo � omitido).
        
        If (InStr(1, CodRespTecnicos, "-" & regRes!cod_utilizador_tec & "-") = 0 And (InStr(1, codRespMedicos, "-" & regRes!cod_utilizador_tec & "-") = 0 Or BL_HandleNull(regRes!permissao_res, 0) = cValMedRes)) Then
            
            If BL_HandleNull(regRes!titulo_tec, "") = "" Then
                
                Select Case regRes!cod_gr_util_tec
                    
                    Case gGrupoAdministradores
                        formulas.respTecnicos = formulas.respTecnicos & "Administrador"
                    Case gGrupoTecnicos
                        formulas.respTecnicos = formulas.respTecnicos & "T�cnico"
                    
                    Case gGrupoChefeSecretariado
                        formulas.respTecnicos = formulas.respTecnicos & "Chefe Secretariado"
                    
                    Case gGrupoSecretariado
                        formulas.respTecnicos = formulas.respTecnicos & "Secretariado"
                    
                    Case gGrupoMedicos
                        formulas.respTecnicos = formulas.respTecnicos & "M�dico"
                    
                    Case gGrupoEnfermeiros
                        formulas.respTecnicos = formulas.respTecnicos & "Enfermeiro"
                
                End Select
            
            Else
                'RespTecnicos = RespTecnicos & RegRes!titulo_tec
                formulas.respTecnicos = regRes!nome_tec
            End If

            formulas.respTecnicos = formulas.respTecnicos & IIf(gLAB = "HPD_GEN", vbCrLf, " - ") & IIf(BL_HandleNull(regRes!titulo_tec, "") = "", "", regRes!titulo_tec & vbCrLf) & IIf(BL_HandleNull(regRes!telef_tec, "") = "", "", "Tel.: " & regRes!telef_tec & vbCrLf) & IIf(BL_HandleNull(regRes!email_tec, "") = "", "", "e-mail: " & regRes!email_tec & vbCrLf)
            CodRespTecnicos = CodRespTecnicos & "-" & regRes!cod_utilizador_tec & "-" & ","     'soliveira (replace , por -)
            
        End If
        
        FlagRTFAnalise = False
        
        '___________________________________________________________________
        'Verifica os valores nulos para Grava��o:
        
        'Os resultados das an�lises tipo auxiliares s�o Alfanum�ricos!!
        
        'Verifica se o 2� Resultado � do tipo auxiliar=>Fica sem resultados
        If "" & regRes!Tipo2 = gT_Auxiliar Then
            resAlf2 = ""
            ResNum2Unid1 = ""
            ResNum2Unid2 = ""
            FlagRes2Anormal = False
            Unid1ResNum2 = ""
            Unid2ResNum2 = ""
            ValRef2 = ""
            ZonaRef2 = ""
            Metodo2 = ""
        End If
        
        'Verifica se o 1� Resultado � do tipo auxiliar
        '=>Fica com os resultados do 2�Resultado e limpa os do 2�!!
        If "" & regRes!Tipo1 = gT_Auxiliar Then
            If Trim(ObservResultado) <> "" Then
                'Voltar a colocar no formato normal=>Para o formato RTF s�o necess�rios muitos caracteres (controlo)
                RText.TextRTF = ObservResultado
                RText.SelStart = 0
                RText.SelLength = Len(RText.TextRTF)
'                RText.SelFontSize = gResTamLetra
'                'RText.SelFontName = "Courier New"
'                RText.SelFontName = "Microsoft Sans Serif"
                ObservResultado = RText.TextRTF
                resAlf1 = ObservResultado
                ObservResultado = ""
                
                ResNum1Unid1 = ""
                ResNum1Unid2 = ""
                FlagRTFAnalise = True
                FlagRes1Anormal = False
                Unid1ResNum1 = ""
                Unid2ResNum1 = ""
                ValRef1 = ""
                ZonaRef1 = ""
                Metodo1 = ""
                
            Else
                resAlf1 = resAlf2
                ResNum1Unid1 = ResNum2Unid1
                ResNum1Unid2 = ResNum2Unid2
                FlagRes1Anormal = FlagRes2Anormal
                Unid1ResNum1 = Unid1ResNum2
                Unid2ResNum1 = Unid2ResNum2
                ValRef1 = ValRef2
                ZonaRef1 = ZonaRef2
                Metodo1 = Metodo2
                ComAutomatico = ""
                FlagResComent = False
                ObservResultado = ""
    
                resAlf2 = ""
                ResNum2Unid1 = ""
                ResNum2Unid2 = ""
                Unid1ResNum2 = ""
                Unid2ResNum2 = ""
                ValRef2 = ""
                ZonaRef2 = ""
                Metodo2 = ""
                FlagRes2Anormal = False
            End If
            
        End If
        
        'A an�lise tem resultados
        ' fgoncalves - auxiliares nao vao para report nem a descricao (.
        If resAlf1 <> "" Or TotalEstrutMicro <> 0 Or ResNum1Unid1 <> "" Or ResNum1Unid2 <> "" Then
        'If ResAlf1 <> "" Or TotalEstrutMicro <> 0 Or ResNum1Unid1 <> "" Or ResNum1Unid2 <> "" Or regRes!Tipo1 = gT_Auxiliar  Then
            '_______________________________________________________________________________
    
            'Para o 1�Resultado verifica se o coment�rio autom�tico sobrep�e o resultado
            If FlagResComent = True And ComAutomatico <> "" Then
                'Sobrep�e o resultado alfanum�rico e limpa o num�rico
                If Len(ComAutomatico) > 20 Then
                    resAlf1 = ComAutomatico
                    ResNum1Unid1 = ""
                    ResNum1Unid2 = ""
                    Unid1ResNum1 = ""
                Else
                    ResNum1Unid1 = ComAutomatico
                    ResNum1Unid2 = ""
                    resAlf1 = ""
                    Unid1ResNum1 = ""
                End If
                ComAutomatico = ""
            End If
    
            '_______________________________________________________________________________
            
            
            'Para o 1� Resultado:
            
            'Verifica se omite o Resultado e Valores\Zonas de Refer�ncia
            'Observa��es no Resultado
            'Se apanhou An�lises com resultado TipoObserva��es=>ResNum1Unid1
            If (ResNum1Unid1 = cVerObservacoes) Or (resAlf1 = cVerObservacoes) Then
                RText.TextRTF = ObservResultado
                RText.SelStart = 0
                RText.SelLength = Len(RText.TextRTF)
                'RText.SelFontSize = gResTamLetra
                ''RText.SelFontName = "Courier New"
                'RText.SelFontName = "Microsoft Sans Serif"
                ObservResultado = RText.TextRTF
                resAlf1 = ObservResultado
                
                FlagRTFAnalise = True
                ResNum1Unid1 = ""
                ResNum1Unid2 = ""
                Unid1ResNum1 = ""
                Unid2ResNum1 = ""
                ValRef1 = ""
                ZonaRef1 = ""
                resAlf1 = ""
            End If
            
            'Verificar sem resultado
            If (ResNum1Unid1 = cSemResultado) Or (resAlf1 = cSemResultado) Then
                If "" & regRes!Casas_dec_1 = "" Then
                    resAlf1 = "--"
                Else
                    resAlf1 = "--." & Replace(Space(CLng("" & regRes!Casas_dec_1)), " ", "-")
                End If
                ResNum1Unid1 = ""
                ResNum1Unid2 = ""
                Unid1ResNum1 = ""
                Unid2ResNum1 = ""
                ValRef1 = ""
                ZonaRef1 = ""
                
                 resAlf1 = ""
                 ResNum1Unid1 = "--"
            
            End If
            '------------------------------------------------------------
            'Unidades:
            
            'Unidades 1� Resultado
            If Trim(Unid1ResNum1) = "" Then
                Unid1ResNum1 = "Null"
            Else
                Unid1ResNum1 = BL_TrataStringParaBD(Unid1ResNum1)
            End If
            If Trim(Unid2ResNum1) = "" Then
                Unid2ResNum1 = "Null"
            Else
                Unid2ResNum1 = BL_TrataStringParaBD(Unid2ResNum1 & "")
            End If
            
            'Unidades 2� Resultado
            If Trim(Unid1ResNum2) = "" Then
                Unid1ResNum2 = "Null"
            Else
                Unid1ResNum2 = BL_TrataStringParaBD(Unid1ResNum2)
            End If
            If Trim(Unid2ResNum2) = "" Then
                Unid2ResNum2 = "Null"
            Else
                Unid2ResNum2 = BL_TrataStringParaBD(Unid2ResNum2 & ")")
            End If
            
            
            'Resultados AlfaNum�ricos :
            
            '1� Resultado
            
            'Unidade1
            If Trim(ResNum1Unid1) = "" Then
                ResNum1Unid1 = "Null"
                Unid1ResNum1 = "Null"
            Else
                ResNum1Unid1 = BL_TrataStringParaBD("" & ResNum1Unid1)
            End If
            'Unidade2
            If Trim(ResNum1Unid2) = "" Then
                ResNum1Unid2 = "Null"
                Unid2ResNum1 = "Null"
            Else
                ResNum1Unid2 = BL_TrataStringParaBD("" & ResNum1Unid2 & IIf(Unid2ResNum1 = "Null", "", ""))
            End If
            If ResNum1Unid1 = "" And ResNum1Unid2 = "" Then
                'N�o interessa ter a indica��o se n�o tiver resultados!
                ValRef1 = ""
                ZonaRef1 = ""
            End If
    
            '2� Resultado
            
            'Unidade1
            If Trim(ResNum2Unid1) = "" Then
                ResNum2Unid1 = "Null"
                Unid1ResNum2 = "Null"
            Else
                ResNum2Unid1 = BL_TrataStringParaBD("" & ResNum2Unid1)
            End If
            'Unidade2
            If Trim(ResNum2Unid2) = "" Then
                ResNum2Unid2 = "Null"
                Unid2ResNum2 = "Null"
            Else
                ResNum2Unid2 = BL_TrataStringParaBD("" & ResNum2Unid2 & IIf(Unid2ResNum2 = "Null", "", ""))
            End If
            If ResNum2Unid1 = "" And ResNum2Unid2 = "" Then
                'N�o interessa ter a indica��o se n�o tiver resultados!
                ValRef2 = ""
                ZonaRef2 = ""
            End If
                    
            '------------------------------------------------------------
            
            If Trim(resAlf1) = "" Then
                TamResAlf1 = "0"
                resAlf1 = "Null"
            Else
                TamResAlf1 = Len(resAlf1)
                resAlf1 = BL_TrataStringParaBD("" & resAlf1)
            End If
            
            If Trim(resAlf2) = "" Then
                TamResAlf2 = "0"
                resAlf2 = "Null"
            Else
                TamResAlf2 = Len(resAlf2)
                resAlf2 = BL_TrataStringParaBD("" & resAlf2)
            End If
            
            If Trim(ValRef1) = "" Then
                ValRef1 = "Null"
            Else
                ValRef1 = BL_TrataStringParaBD("" & ValRef1)
            End If
            
             If Trim(obsRef) = "" Then
                obsRef = "Null"
            Else
                obsRef = BL_TrataStringParaBD("" & obsRef)
            End If
            params(gArgumentoObsRef) = obsRef
            If Trim(ValRef2) = "" Then
                ValRef2 = "Null"
            Else
                ValRef2 = BL_TrataStringParaBD("" & ValRef2)
            End If
            
            If Trim(ZonaRef1) = "" Then
                ZonaRef1 = "Null"
            Else
                ZonaRef1 = BL_TrataStringParaBD("" & ZonaRef1)
            End If
            
            If Trim(ZonaRef2) = "" Then
                ZonaRef2 = "Null"
            Else
                ZonaRef2 = BL_TrataStringParaBD("" & ZonaRef2)
            End If
            
            If Trim(ObservAnalise) = "" Then
                ObservAnalise = "Null"
            Else
                ObservAnalise = BL_TrataStringParaBD(ObservAnalise)
            End If
            
            If Trim(ObservResultado) = "" Then
                ObservResultado = "Null"
            Else
                ObservResultado = BL_TrataStringParaBD(ObservResultado)
            End If
            
            If Trim(ComAutomatico) = "" Then
                ComAutomatico = "Null"
            Else
                ComAutomatico = BL_TrataStringParaBD(ComAutomatico)
            End If
        
            
            'Verificar se existe alguma frase antes do membro da complexa(caso seja complexa)
            If BL_HandleNull(regRes!cod_ana_c, "0") <> "0" Then
                k = 1
                flg_encontrou = False
                While k <= TotalEstrutFr And flg_encontrou = False
                    If EstrutFr(k).CodAnaC = BL_HandleNull(regRes!cod_ana_c, "0") And EstrutFr(k).ordem < BL_HandleNull(regRes!Ord_Ana_Compl, 999) And EstrutFr(k).Ja_Foi_Inserida = False Then
                        Dim aux2 As String
                        flg_encontrou = True
                        EstrutFr(k).Ja_Foi_Inserida = True
                        Simples2 = "'" & EstrutFr(k).frase & "'"
                        aux2 = params(gArgumentoDescrRelatorio)
                        params(gArgumentoDescrRelatorio) = EstrutFr(k).frase
                        simples = simples
                        'Preenche a tabela tempor�ria=>Para cada registo do RecordSet
                        LinhasPagina = LinhasPagina - 1
                        
                        OrdemImpressao = OrdemImpressao + 1
                        'brunodsantos 03.12.2015
                        Dim SegundoResultadoAux As Seg_Res
                        '
                        params(gArgumentoRespValMedMed) = RespValMedMed
                        params(gArgumentoRespValMedTec) = RespValMedTec
                        IR_Insere_SLCRLR regRes!seq_realiza, NumReq, BL_HandleNull(regRes!n_req, ""), BL_HandleNull(regRes!dt_val, ""), Grupo, subGrupo, Perfil, complexa, Simples2, _
                            "Null", "Null", "Null", "Null", "Null", "'0'", "Null", "Null", "Null", "Null", "Null", "Null", "Null", "'0'", "Null", "Null", _
                            "Null", "Null", "Null", "Null", "Null", False, False, "", Dt_ResAlf1_Ant1, "", "", Dt_ResAlf1_Ant2, "", "", Dt_ResAlf1_Ant3, "", "Null", OrdemImpressao, _
                            "", "", "", "", "", BL_HandleNull(VolumeSubGrupo, ""), GrupoUserVal, BL_HandleNull(regRes!cod_produto, "-1"), BL_HandleNull(regRes!descr_produto, "Sem Produto"), _
                             BL_HandleNull(regRes!ord_produto, "0"), FlgPosRes, regRes, SegundoResultadoAux, ReutilAna

                        params(gArgumentoDescrRelatorio) = aux2
                    End If
                    
                    k = k + 1
                Wend
            End If
            
            'brunodsantos 03.12.2015
            SegundoResultado.flg_res_ant_2 = IR_DEVOLVE_FLG_RES_ANT_2(BL_HandleNull(regRes!cod_ana_s, "0"))
            '
            
            'Preenche a tabela tempor�ria=>Para cada registo do RecordSet
            LinhasPagina = LinhasPagina - 1
            OrdemImpressao = OrdemImpressao + 1
            params(gArgumentoRespValMedMed) = RespValMedMed
            params(gArgumentoRespValMedTec) = RespValMedTec
            IR_Insere_SLCRLR regRes!seq_realiza, NumReq, BL_HandleNull(regRes!n_req, ""), BL_HandleNull(regRes!dt_val, ""), Grupo, subGrupo, Perfil, complexa, simples, _
                            ResNum1Unid1, ResNum1Unid2, Unid1ResNum1, Unid2ResNum1, resAlf1, TamResAlf1, ValRef1, ZonaRef1, _
                            ResNum2Unid1, ResNum2Unid2, Unid1ResNum2, Unid2ResNum2, resAlf2, TamResAlf2, ValRef2, ZonaRef2, _
                            ObservAnalise, ObservResultado, ComAutomatico, BL_HandleNull(regRes!flg_anormal, ""), BL_HandleNull(regRes!flg_anormal_2, ""), cutoff, FlagRTFAnalise, _
                            ResAlf1_Ant1, Dt_ResAlf1_Ant1, LocalRes_Ant1, ResAlf1_Ant2, Dt_ResAlf1_Ant2, LocalRes_Ant2, ResAlf1_Ant3, Dt_ResAlf1_Ant3, LocalRes_Ant3, Classe, _
                            OrdemImpressao, formulas.RespMedicos, formulas.respTecnicos, Metodo1, MetodoPerfil, MetodoComplexa, VolumeSubGrupo, GrupoUserVal, BL_HandleNull(regRes!cod_produto, "-1"), _
                            BL_HandleNull(regRes!descr_produto, "Sem Produto"), BL_HandleNull(regRes!ord_produto, "0"), _
                            FlgPosRes, regRes, SegundoResultado, ReutilAna
            

            UltimoGrupo = Grupo
            UltimoSubgrupo = subGrupo
            UltimaClasse = Classe
            UltimaDt1 = Dt_ResAlf1_Ant1
            UltimaDt2 = Dt_ResAlf1_Ant2
            UltimaDt3 = Dt_ResAlf1_Ant3
            
            registos = registos + 1
        End If
    End If
'Aten��o LABEL
continua:
        
        regRes.MoveNext
    Next i
    BL_Devolve_Max_Dt_Hr_Validacao NumReq, formulas.DtValidacao, formulas.HrValidacao
    
    If registos = 0 Then
        'N�o inseriu nenhum resultado !
        regRes.Close
        Set regRes = Nothing
        Call BL_FimProcessamento(gFormActivo, "")
        If Multiplo = False Then
            Call BG_Mensagem(mediMsgBox, "N�o foram encontrados nenhuns resultados associados � requisi��o para imprimir!", vbOKOnly + vbExclamation)
        End If
        
        IR_FechaTodosRs CmdResAlfan, CmdResFrase, CmdResMicro, CmdResMicroTsq, CmdResTsq, CmdResRef, CmdResObserv, _
                        regRes, RegCmd, RegCmdMTsq, RegCmdResRef, RegFormulas, RegDescrFrase, RsFr, CmdFr, RsContaMembros
        
        Exit Function
    End If
    
    'Preenche a tabela tempor�ria=>Valores Fixos
    If Trim(formulas.Produtos) = "" Then
        formulas.Produtos = "Null"
    Else
        formulas.Produtos = BL_TrataStringParaBD("" & formulas.Produtos)
    End If
    
    If Trim(formulas.Comentario) = "" Then
        formulas.Comentario = "Null"
        formulas.FlagComent = "Null"
    Else
        RText.TextRTF = formulas.Comentario
        If Trim(RText.text) = "" Then
            formulas.Comentario = "Null"
            formulas.FlagComent = "Null"
        Else
            RText.SelStart = 0
            RText.SelLength = Len(RText.TextRTF)
            formulas.Comentario = RText.TextRTF
            
            formulas.Comentario = BL_TrataStringParaBD("" & formulas.Comentario)
            formulas.FlagComent = "'S'"
        End If
    End If
    
    IR_TrataResponsaveis formulas.respTecnicos, formulas.RespMedicos
    IR_TrataAnalisesPendentes formulas.AnalisesPendentes
    
    If gPrecosAmbienteHospitalar = mediSim Then
        formulas.totalPagar = IR_RetornaPrecoRequis(NumReq)
    End If
    ' Coment�rio de quem imprime.
    Select Case gCodGrupo
        Case cMedico
            formulas.aux_coment_assinatura = "O M�dico,"
        Case cTecnico
            formulas.aux_coment_assinatura = "O T�cnico,"
        Case Else
            formulas.aux_coment_assinatura = ""
    End Select
    
    
    If ImprComValTec = False Then
        IR_ActualizaDados NumReq, GrupoAna, EstadoReq, formulas.ExisteSegVia, formulas.DtEmissao, formulas.HrEmissao, formulas.DtSVia, formulas.HrSVia
    End If
    
    IR_FechaTodosRs CmdResAlfan, CmdResFrase, CmdResMicro, CmdResMicroTsq, CmdResTsq, CmdResRef, CmdResObserv, _
                    regRes, RegCmd, RegCmdMTsq, RegCmdResRef, RegFormulas, RegDescrFrase, RsFr, CmdFr, RsContaMembros
                    
    ' INSERE TABELA SL_CR_LR_VF
    IR_Insere_SL_CR_LR_VF formulas, NumReq
      
    'BRUNODSANTOS CHSJ-2670 - 05.05.2016
    If gValidaGeraVersaoReport = mediSim Then
        BL_GeraHash_ConteudoTabela NumReq
    End If
    '
        
    'Imprime o relat�rio no Crystal Reports
    If ImprimeListaExterior = False Then
        IR_ImprimeResultadosCrystal formulas, NumReq, imprRelARS, MultiReport, NumPagina, imprSegVia, ImprimeListaExterior
    End If
    
    
    IR_EnvioSMS enviaEmail, imprRelARS, NumReq
    If ImprAnexos = True Then
        IR_ImprimeAnexos NumReq
    End If
    If (Multiplo = False) Then
        If ImprimeListaExterior = False Then
            If IR_ApagaTabelasResultados = False Then
                IR_ImprimeResultados = False
                Exit Function
            End If
        End If
    End If
    
    Call BL_FimProcessamento(gFormActivo, "")
    
    IR_ImprimeResultados = True
Exit Function

Trata_Erro:
    BG_LogFile_Erros "Erro a imprimir a Req: " & NumReq & " - (" & Err.Number & ") " & Err.Description & " SQL: " & gSQLError
    IR_FechaTodosRs CmdResAlfan, CmdResFrase, CmdResMicro, CmdResMicroTsq, CmdResTsq, CmdResRef, CmdResObserv, _
                    regRes, RegCmd, RegCmdMTsq, RegCmdResRef, RegFormulas, RegDescrFrase, RsFr, CmdFr, RsContaMembros
    If Multiplo = False Then
        If ImprimeListaExterior = False Then
            If IR_ApagaTabelasResultados Then
                IR_ImprimeResultados = False
                Exit Function
            End If
        End If
    End If
    Call BL_FimProcessamento(gFormActivo, "")
    Resume Next
    IR_ImprimeResultados = False
End Function


'brunodsantos 03.12.2015
Private Function IR_DEVOLVE_FLG_RES_ANT_2(cod_ana_s As String) As Integer

    Dim temp As Integer
    On Error GoTo TrataErro
    Dim CmdFl As ADODB.Command
    Dim RsFl As ADODB.recordset
    Set CmdFl = New ADODB.Command
    CmdFl.ActiveConnection = gConexao
    CmdFl.CommandType = adCmdText
    CmdFl.CommandText = "SELECT slas.flg_res_ant_2 FROM sl_ana_s slas WHERE slas.cod_ana_s=?"
    CmdFl.Prepared = True
    CmdFl.Parameters.Append CmdFl.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
    CmdFl.Parameters(0).value = cod_ana_s
    Set RsFl = CmdFl.Execute
    If Not RsFl.EOF Then
        temp = CInt(BL_HandleNull(RsFl!flg_res_ant_2, "0"))
    End If
    RsFl.Close
    Set RsFl = Nothing
    Set CmdFl = Nothing
    IR_DEVOLVE_FLG_RES_ANT_2 = temp
    Exit Function
TrataErro:
    IR_DEVOLVE_FLG_RES_ANT_2 = 0
End Function
            '
Private Sub IR_PreencheDados(recordeSet As ADODB.recordset, n_req As String, ByRef formulas As dadosFormula)
    
    'DataChegada = recordeSet!Dt_Chega & " " & recordeSet!hr_chega
    formulas.DataChegada = recordeSet!dt_chega
    formulas.HoraChegada = BL_HandleNull(recordeSet!hr_chega, "")
    
    formulas.DataColheita = IR_PreencheDataColheita(n_req, BL_HandleNull(recordeSet!dt_colheita, ""), BL_HandleNull(recordeSet!hr_colheita, ""))
    
    formulas.NumEpisodio = "" & recordeSet!n_epis
    formulas.NomeUtente = "" & recordeSet!nome_ute
    If gTipoInstituicao = "PRIVADA" Then
        formulas.nrBenef = BL_HandleNull(recordeSet!nr_benef_req, "")
    Else
        formulas.nrBenef = BL_HandleNull(recordeSet!nr_benef, "")
    End If
    formulas.sala.cod_sala = BL_HandleNull(recordeSet!cod_sala, "")
    formulas.sala.descr_sala = BL_HandleNull(recordeSet!descr_sala, "")
    If BL_HandleNull(recordeSet!cod_isencao, "0") = gTipoIsencaoIsento Then
        formulas.isencao = "I"
    Else
        formulas.isencao = ""
    End If

    If gPassaRecFactus = mediSim Then
        formulas.isencao = Trim(formulas.isencao & FACTUS_DevolveEstadoRecibos(n_req))
    Else
        formulas.isencao = Trim(formulas.isencao & BL_DevolveEstadoRecibos(n_req))
    End If
    
    
    formulas.ObsAnaReq = "" & BL_DevolveObsAnaImprimir(n_req)
    formulas.ObsAutoBloq = "" & BL_VerificaObsAutoBloq(n_req)
    
    formulas.cod_destino = BL_HandleNull(recordeSet!cod_destino, mediComboValorNull)
    formulas.flg_fax_destino = BL_HandleNull(recordeSet!flg_fax, 0)
    Select Case formulas.cod_destino
        Case gCodDestinoLab
            formulas.destino = "L"
        Case gCodDestinoDomic
            formulas.destino = "D"
        Case gCodDestinoPosto
            formulas.destino = "P"
        Case gCodDestinoEmailMedico
            formulas.destino = "EM"
        Case gCodDestinoEmailPosto
            formulas.destino = "EP"
        Case gCodDestinoEmailProven
            formulas.destino = "EPR"
        Case gCodDestinoEmailUte
            formulas.destino = "EU"
        Case gCodDestinoSMSUte
            formulas.destino = "SMS"
    End Select
    
    formulas.DtEmissao = "" & recordeSet!dt_imp
    formulas.HrEmissao = "" & recordeSet!hr_imp
    formulas.DtSVia = "" & recordeSet!dt_imp2
    formulas.HrSVia = "" & recordeSet!hr_imp2
    
    formulas.SitReq = "" & recordeSet!descr_t_sit
    formulas.CodProv = "" & recordeSet!cod_proven
    formulas.ProvReq = "" & recordeSet!descr_proven
    formulas.ObsProven = "" & recordeSet!obs_proven
    If BL_HandleNull(recordeSet!tipo_urgencia, "") <> "" Then
        formulas.ObsProven = Trim(formulas.ObsProven & " �rea " & BL_HandleNull(recordeSet!tipo_urgencia))
    End If
    formulas.NomeMed = "" & recordeSet!nome_med & IIf(BL_HandleNull(recordeSet!servico, "") <> "", " / " & recordeSet!servico, "") & IIf(BL_HandleNull(recordeSet!csaude, "") <> "", " / " & recordeSet!csaude, "")
    
    formulas.TipoUtente = "" & recordeSet!t_utente
    If gLAB = "HPOVOA" And formulas.TipoUtente = "CHPVC" Then
        formulas.NumUtente = "" & recordeSet!n_proc_1
    Else
        formulas.NumUtente = "" & recordeSet!Utente
    End If
    formulas.CodSexo = "" & recordeSet!sexo_ute
    formulas.descrProfis = "" & recordeSet!descr_profis
    formulas.SexoUtente = "" & recordeSet!descr_sexo
    formulas.DtNascUtente = "" & recordeSet!dt_nasc_ute
    formulas.SeqUtente = "" & recordeSet!seq_utente
    formulas.MoradaUtente = ""
    formulas.CodPostalUtente = ""
    formulas.ficha = "" & recordeSet!ficha
    formulas.NReqAssoc = "" & recordeSet!n_Req_assoc
    
    formulas.NomeMed = "" & recordeSet!nome_med & IIf(BL_HandleNull(recordeSet!servico, "") <> "", " / " & recordeSet!servico, "") & IIf(BL_HandleNull(recordeSet!csaude, "") <> "", " / " & recordeSet!csaude, "")

    If formulas.NomeMed = "" Then

        formulas.NomeMed = "" & recordeSet!descr_medico

    End If
    If gAssinaturaActivo = mediSim Then
        formulas.UserAssinatura = IR_RetornaAssinaturaReq(n_req)
    End If
    ' pferreira 2009.07.27
    formulas.numExt = BL_HandleNull(recordeSet!num_ext, "")
    If gImprimeMoradaRes = mediSim Then
        If gTipoInstituicao = "PRIVADA" Then
            formulas.MoradaUtente = "" & BL_HandleNull(recordeSet!morada_req, "")
            formulas.CodPostalUtente = BL_HandleNull(recordeSet!cod_postal_req, "")
        Else
            formulas.CodPostalUtente = BL_HandleNull(recordeSet!cod_postal_ute, "")
            formulas.MoradaUtente = "" & BL_HandleNull(recordeSet!descr_mor_ute, "")
        End If
        If BL_SeleccionaDescrPostal(formulas.CodPostalUtente) = "" Then
            formulas.CodPostalUtente = formulas.CodPostalUtente
        Else
            formulas.CodPostalUtente = "" & formulas.CodPostalUtente & "  " & BL_SeleccionaDescrPostal(formulas.CodPostalUtente)
        End If
    End If
    formulas.dtImprCartao = BL_HandleNull(recordeSet!dt_impr_cartao, "")
    formulas.codGenero = BL_HandleNull(recordeSet!cod_genero, gCodGeneroDefeito)
    formulas.descr_genero = BL_HandleNull(recordeSet!descr_genero, gCodGeneroDefeito)
    IR_SeleccionaDadosEFR BL_HandleNull(recordeSet!cod_efr, 0), formulas
    
    If BL_HandleNull(recordeSet!n_proc_1, "") = "" Then
        formulas.NumProcUtente = "" & recordeSet!n_proc_2
    Else
        If BL_HandleNull(recordeSet!n_proc_2, "") = "" Then
            formulas.NumProcUtente = "" & recordeSet!n_proc_1
        Else
            formulas.NumProcUtente = recordeSet!n_proc_1 & "/" & recordeSet!n_proc_2
        End If
    End If
    
    If Not BL_HandleNull(recordeSet!dt_imp, "") = "" Then
        formulas.ExisteSegVia = True
    Else
        formulas.ExisteSegVia = False
    End If
    formulas.DtConclusao = "" & BL_HandleNull(recordeSet!dt_conclusao, "")
    formulas.DtPretend = "" & BL_HandleNull(recordeSet!dt_pretend, "")
    formulas.DtFecho = "" & BL_HandleNull(recordeSet!dt_fecho, "")
    formulas.nomeRelatorio = IR_DevolveNomeRelatorio(BL_HandleNull(recordeSet!local_requis, -1), formulas.codGenero)
    formulas.tubosAtrasados = IR_DevolveTubosAtrasados(n_req, formulas.DataChegada)
    formulas.localRequis = BL_HandleNull(recordeSet!local_requis, -1)
    formulas.cod_raca = BL_HandleNull(recordeSet!cod_raca, "")
    If formulas.cod_raca <> "" Then
        formulas.descr_raca = BL_SelCodigo("sl_racas", "descr_raca", "COD_RACA", formulas.cod_raca)
    End If
    formulas.codLocalEntrega = BL_HandleNull(recordeSet!cod_local_entrega, "")
    If formulas.codLocalEntrega <> "" Then
        formulas.descrLocalEntrega = BL_SelCodigo("sl_cod_local_entrega", "descr_local_entrega", "cod_local_entrega", formulas.codLocalEntrega)
    End If
    formulas.nome_dono = BL_HandleNull(recordeSet!nome_dono, "")
    formulas.obs_req = BL_HandleNull(recordeSet!obs_req, "")
    formulas.cod_tipo_amostra = BL_HandleNull(recordeSet!cod_tipo_amostra, "")
    If formulas.cod_tipo_amostra <> "" Then
        formulas.descr_tipo_amostra = BL_SelCodigo("sl_cod_tipo_amostra", "descr_tipo_amostra", "cod_tipo_amostra", formulas.cod_tipo_amostra)
    End If
    formulas.cod_origem_amostra = BL_HandleNull(recordeSet!cod_origem_amostra, "")
    If formulas.cod_origem_amostra <> "" Then
        formulas.descr_origem_amostra = BL_SelCodigo("sl_cod_origem_amostra", "descr_origem_amostra", "cod_origem_amostra", formulas.cod_origem_amostra)
    End If
    formulas.local_colheita = BL_HandleNull(recordeSet!local_colheita, "")
    formulas.cod_ponto_colheita = BL_HandleNull(recordeSet!cod_ponto_colheita, "")
    If formulas.cod_ponto_colheita <> "" Then
        formulas.descr_ponto_colheita = BL_SelCodigo("sl_cod_ponto_colheita", "descr_ponto_colheita", "cod_ponto_colheita", formulas.cod_ponto_colheita)
    End If
    formulas.responsavel_colheita = BL_HandleNull(recordeSet!user_colheita, "")
'    obs_req As String
'    'AGUAS
'    cod_tipo_amostra As String
'    descr_tipo_amostra As String
'    cod_origem_amostra As String
'    descr_origem_amostra As String
'    local_colheita As String
'    cod_ponto_colheita As String
'    descr_ponto_colheita As String
'    responsavel_colheita As String
    
    
    'Inicializa as restantes constantes passadas para o Report=>1� Registo
    If Trim(formulas.DtNascUtente) <> "Null" And Trim(formulas.DtNascUtente) <> "" Then
        formulas.DiasUtente = DateDiff("d", CDate(formulas.DtNascUtente), formulas.DataChegada)
        'DiasUtente = DateDiff("d", CDate(DtNascUtente), Date)
        If formulas.DiasUtente = 0 Then formulas.DiasUtente = 2
    End If
End Sub


' -----------------------------------------------------------------------------------------------

' VERIFICA SE PARA DETERMINADA ENTIDADE E POSTO, SE IMPRIME RESULTADOS
' MAIS UM PARA EVITAR PROCEDURE TO LARGE

' -----------------------------------------------------------------------------------------------
Private Function IR_VerificaInibeImprAnaEFR(cod_efr As String, cod_sala As String) As Boolean
    Dim sSql As String
    Dim RsEFR As New ADODB.recordset
    Dim rsSalas As New ADODB.recordset
    
    If cod_efr = "" Or gTipoInstituicao = "HOSPITALAR" Then
        IR_VerificaInibeImprAnaEFR = False
        Exit Function
    End If
    
    sSql = "SELECT flg_inibe_imp_analises FROM sl_efr WHERE cod_Efr = " & cod_efr
    RsEFR.CursorLocation = adUseServer
    RsEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEFR.Open sSql, gConexao
    If RsEFR.RecordCount > 0 Then
        If BL_HandleNull(RsEFR!flg_inibe_imp_analises, "0") = "1" Then
            If cod_sala <> "" Then
                sSql = "SELECT flg_inibe_imp_analises FROM sl_cod_salas WHERE cod_sala = " & cod_sala
                rsSalas.CursorLocation = adUseServer
                rsSalas.CursorType = adOpenStatic
                If gModoDebug = mediSim Then BG_LogFile_Erros sSql
                rsSalas.Open sSql, gConexao
                If rsSalas.RecordCount > 0 Then
                    If BL_HandleNull(rsSalas!flg_inibe_imp_analises, "0") = "1" Then
                        IR_VerificaInibeImprAnaEFR = True
                    Else
                        'If BG_Mensagem(mediMsgBox, "Requisi��o SEM CREDENCIAL. Deseja Imprimir?", vbYesNo + vbQuestion, "SEM CREDENCIAL.") = vbYes Then
                        '    BL_VerificaInibeImprAnaEFR = True
                        'Else
                            IR_VerificaInibeImprAnaEFR = False
                        'End If
                    End If
                End If
                rsSalas.Close
                Set rsSalas = Nothing
            Else
                IR_VerificaInibeImprAnaEFR = False
            End If
            'BL_VerificaInibeImprAnaEFR = False
        Else
            IR_VerificaInibeImprAnaEFR = False
        End If
    End If
    RsEFR.Close
    Set RsEFR = Nothing
End Function


' ------------------------------------------------------------------------------------------------

' PREENCHE DADOS DAS ANALISES PARA A IMPRESSAO

' ------------------------------------------------------------------------------------------------
Private Sub IR_PreencheDadosAnalises(regRes As ADODB.recordset)
    On Error GoTo TrataErro
    params(gArgumentoDescrRelatorio) = IIf(BL_HandleNull(regRes!descr_relatorio, "") = "", "Null", regRes!descr_relatorio)
    params(gArgumentoCodPerfil) = BL_HandleNull(regRes!Cod_Perfil, "Null")
    params(gArgumentoCodAnaC) = BL_HandleNull(regRes!cod_ana_c, "Null")
    params(gArgumentoCodAnaS) = BL_HandleNull(regRes!cod_ana_s, "Null")
    params(gArgumentoNaoAvaliaRes) = BL_HandleNull(regRes!flg_nao_avaliar, "0")
    params(gArgumentoUserCri) = BL_HandleNull(regRes!user_cri, "")
    params(gArgumentoOrdemARS) = BL_HandleNull(regRes!ordem_ars, "-1")

Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_PreencheDadosAnalises: " & Err.Description, "IR", "BL_PreencheDadosAnalises"
    Exit Sub
    Resume Next
End Sub

' ------------------------------------------------------------------------------------------------

' RETORNA O METODO CORRECTO PARA A ANALISE EM CAUSA

' ------------------------------------------------------------------------------------------------
Private Sub IR_ObtemMetodo(cod_ana_s As String, dt_cri As String, ByRef metodoRes1 As String, ByRef metodoRes2 As String, _
                            ByRef caracMetodo1 As String, ByRef caracMetodo2 As String)
    Dim sSql As String
    Dim rsMet As New ADODB.recordset
    On Error GoTo TrataErro
    sSql = "SELECT sl_ana_ref.n_res,sl_metodo.descr_metodo, sl_metodo.carac_metodo FROM sl_ana_ref,sl_metodo "
    sSql = sSql & " WHERE sl_ana_ref.cod_ana_s=" & BL_TrataStringParaBD(cod_ana_s) & " AND sl_ana_ref.cod_metod=sl_metodo.cod_metodo "
    sSql = sSql & " AND ((dt_valid_inf  <= " & BL_TrataDataParaBD(dt_cri) & " AND dt_valid_sup IS NULL) OR "
    sSql = sSql & "  (dt_valid_inf  <= " & BL_TrataDataParaBD(dt_cri) & " AND dt_valid_sup > " & BL_TrataDataParaBD(dt_cri) & "))"
    rsMet.CursorLocation = adUseServer
    rsMet.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsMet.Open sSql, gConexao
    If rsMet.RecordCount > 0 Then
        While Not rsMet.EOF
            If BL_HandleNull(rsMet!N_Res, 1) = 1 Then
                metodoRes1 = BL_HandleNull(rsMet!descr_metodo, "")
                caracMetodo1 = BL_HandleNull(rsMet!carac_metodo, "")
            ElseIf BL_HandleNull(rsMet!N_Res, 2) = 2 Then
                metodoRes2 = BL_HandleNull(rsMet!descr_metodo, "")
                caracMetodo2 = BL_HandleNull(rsMet!carac_metodo, "")
            End If
            rsMet.MoveNext
        Wend
    End If
    rsMet.Close
    Set rsMet = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "BibliotecaLocal: BL_ObtemMetodo: " & Err.Description, "IR", "BL_ObtemMetodo"
    Exit Sub
    Resume Next
End Sub


Private Sub IR_LimpaVariaveis(ByRef ResNum1Unid1 As String, ByRef ResNum1Unid2 As String, _
                              ByRef ResNum2Unid1 As String, ByRef ResNum2Unid2 As String, _
                              ByRef resAlf1 As String, ByRef resAlf2 As String, ByRef FlagRes1Anormal As Boolean, _
                              ByRef FlagRes2Anormal As Boolean)

    ResNum1Unid1 = ""
    ResNum1Unid2 = ""
    ResNum2Unid1 = ""
    ResNum2Unid2 = ""
    resAlf1 = ""
    resAlf2 = ""

    FlagRes1Anormal = False
    FlagRes2Anormal = False
End Sub
                              


Private Sub IR_FechaTodosRs(ByRef CmdResAlfan As ADODB.Command, ByRef CmdResFrase As ADODB.Command, _
                            ByRef CmdResMicro As ADODB.Command, ByRef CmdResMicroTsq As ADODB.Command, _
                            ByRef CmdResTsq As ADODB.Command, ByRef CmdResRef As ADODB.Command, _
                            ByRef CmdResObserv As ADODB.Command, ByRef regRes As ADODB.recordset, _
                            ByRef RegCmd As ADODB.recordset, ByRef RegCmdMTsq As ADODB.recordset, _
                            ByRef RegCmdResRef As ADODB.recordset, ByRef RegFormulas As ADODB.recordset, _
                            ByRef RegDescrFrase As ADODB.recordset, ByRef RsFr As ADODB.recordset, _
                            ByRef CmdFr As ADODB.Command, ByRef RsContaMembros As ADODB.recordset)
    'Fecha todos os Recordset e Comandos
    On Error Resume Next
    Set CmdResAlfan = Nothing
    Set CmdResFrase = Nothing
    Set CmdResMicro = Nothing
    Set CmdResMicroTsq = Nothing
    Set CmdResTsq = Nothing
    Set CmdResRef = Nothing
    Set CmdResObserv = Nothing
    If Not regRes Is Nothing Then
        regRes.Close
        Set regRes = Nothing
    End If
    If Not RegCmd Is Nothing Then
        RegCmd.Close
        Set RegCmd = Nothing
    End If
    If Not RegCmdMTsq Is Nothing Then
        RegCmdMTsq.Close
        Set RegCmdMTsq = Nothing
    End If
    If Not RegCmdResRef Is Nothing Then
        RegCmdResRef.Close
        Set RegCmdResRef = Nothing
    End If
    If Not RegFormulas Is Nothing Then
        Set RegFormulas = Nothing
    End If
    If Not RegDescrFrase Is Nothing Then
        Set RegDescrFrase = Nothing
    End If
    If Not RsFr Is Nothing Then
        Set RsFr = Nothing
    End If
    Set CmdFr = Nothing
    If Not RsContaMembros Is Nothing Then
        Set RsContaMembros = Nothing
    End If

End Sub

Private Sub IR_ActualizaDados(NumReq As String, GrupoAna As String, ByRef EstadoReq As String, _
                              ExisteSegVia As Boolean, ByRef DtEmissao As String, ByRef HrEmissao As String, _
                              ByRef DtSVia As String, ByRef HrSVia As String)
    Dim s As String
    '____________________________________________________________________________________
    'soliveira 08-07-04 s� muda o estado se imprimir directamente para a impressora
    If gImprimirDestino = 1 Then
        'Actualiza o Estado da Requisi��o
        '=>A Flg_Estado das an�lises nos resultados j� est�o actualizadas para esta impress�o!!
        EstadoReq = BL_MudaEstadoReq(CLng(NumReq), True, GrupoAna)
        
        '**** DtEmissao ,HrEmissao ,DtSVia,HrSVia j� obtidos (Podem estar vazios)!!
    
        'A 2� via � uma impress�o dos resultados quandos estes j� foram todos impressos!!
        If Trim(UCase(EstadoReq)) = gEstadoReqTodasImpressas Or Trim(UCase(EstadoReq)) = gEstadoReqImpressaoParcial Then
            'Acabou de ficar F?
            '� a 1� vez que as an�lises da requisi��o em causa est�o a ser todas impressas(ESTADO F) ?
            '=>Qdo n�o existe ainda 2�Via.
            If ExisteSegVia = False Then
                '1� Via: Actualiza novamente os dados
                '2� Via: Actualiza os dados pela 1� Via
                DtEmissao = Bg_DaData_ADO
                HrEmissao = left(BG_CvHora(Bg_DaHora_ADO), 5)
                DtSVia = DtEmissao
                HrSVia = HrEmissao
                s = "UPDATE sl_requis SET dt_imp='" & BG_CvDataParaWhere_ADO(DtEmissao) & "',hr_imp='" & HrEmissao & "',user_imp='" & gCodUtilizador & "' WHERE n_req=" & NumReq
                's = "UPDATE sl_requis SET dt_imp='" & BG_CvDataParaWhere_ADO(DtEmissao) & "',hr_imp='" & HrEmissao & "',user_imp='" & gCodUtilizador & "',dt_imp2='" & BG_CvDataParaWhere_ADO(DtSVia) & "',hr_imp2='" & HrSVia & "',user_imp2='" & gCodUtilizador & "' WHERE n_req=" & NumReq
                
            Else
                'J� estava F!!!
                '1� Via: Mant�m os dados
                '2� Via: Actualiza os dados pela 1� Via
                DtSVia = Bg_DaData_ADO
                HrSVia = left(BG_CvHora(Bg_DaHora_ADO), 5)
                s = "UPDATE sl_requis SET dt_imp2='" & BG_CvDataParaWhere_ADO(DtSVia) & "',hr_imp2='" & HrSVia & "',user_imp2='" & gCodUtilizador & "' WHERE n_req=" & NumReq
            End If
        Else
            '1� Via: Actualiza novamente os dados
            '2� Via: N�o escreve nada (Mant�m-se nulo)
            DtEmissao = Bg_DaData_ADO
            HrEmissao = left(BG_CvHora(Bg_DaHora_ADO), 5)
            DtSVia = ""
            HrSVia = ""
            s = "UPDATE sl_requis SET dt_imp='" & BG_CvDataParaWhere_ADO(DtEmissao) & "',hr_imp='" & HrEmissao & "',user_imp='" & gCodUtilizador & "' WHERE n_req=" & NumReq
        End If
        Call BG_ExecutaQuery_ADO(s)
    End If

End Sub

Private Sub IR_EnvioSMS(enviaEmail As Boolean, imprRelARS As Boolean, NumReq As String)
    ' ENVIO DE SMS
    If gImprimirDestino = crptToPrinter And enviaEmail = False And imprRelARS = False Then
        SMS_NovaMensagem NumReq
    End If

End Sub

Private Sub IR_TrataAnalisesPendentes(ByRef AnalisesPendentes As String)
    'An�lises Pendentes
    If Trim(AnalisesPendentes) = "" Then
        AnalisesPendentes = "Null"
    Else
        AnalisesPendentes = BL_TrataStringParaBD(AnalisesPendentes)
    End If

End Sub

Private Sub IR_TrataResponsaveis(ByRef respTecnicos As String, ByRef RespMedicos As String)
    'Respons�veis T�cnicos
    If Trim(respTecnicos) = "" Then
        respTecnicos = "Null"
    Else
        respTecnicos = BL_TrataStringParaBD(respTecnicos)
    End If
    
    'Respons�veis M�dicos
    If Trim(RespMedicos) = "" Then
        RespMedicos = "Null"
    Else
        RespMedicos = BL_TrataStringParaBD(RespMedicos)
    End If
    
    
    If (left(Trim(RespMedicos), 2) = "',") Then
        RespMedicos = "''"
    End If
    
    If (left(Trim(respTecnicos), 2) = "',") Then
        respTecnicos = "''"
    End If

End Sub


Private Sub IR_ImprimeResultadosCrystal(ByRef formulas As dadosFormula, ByVal NumReq As String, ByVal imprRelARS As Boolean, _
                                        ByVal MultiReport As Integer, ByVal numPag As Integer, ByVal imprSegVia As Boolean, ByVal ImprimeListaExterior As Boolean)

    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    If ImprimeListaExterior = False Then
        Report.formulas(0) = "NumUtente=" & BL_TrataStringParaCrystal(formulas.TipoUtente & "/" & formulas.NumUtente)
        Report.formulas(1) = "Processo=" & BL_TrataStringParaCrystal("" & formulas.NumProcUtente)
        Report.formulas(2) = "Episodio=" & BL_TrataStringParaCrystal("" & formulas.NumEpisodio)
        Report.formulas(3) = "NomeUtente=" & BL_TrataStringParaCrystal("" & formulas.NomeUtente)
        Report.formulas(4) = "DtNascUtente=" & BL_TrataStringParaCrystal("" & formulas.DtNascUtente)
        Report.formulas(5) = "SexoUtente=" & BL_TrataStringParaCrystal("" & formulas.SexoUtente)
        Report.formulas(6) = "NumReq=" & BL_TrataStringParaCrystal("" & NumReq)
        Report.formulas(7) = "SitReq=" & BL_TrataStringParaCrystal("" & formulas.SitReq)
        Report.formulas(8) = "ProvReq=" & BL_TrataStringParaCrystal("" & formulas.ProvReq)
        
        Report.formulas(9) = "DtChegada=" & BL_TrataStringParaCrystal("" & formulas.DataChegada)
        
        Report.formulas(10) = "DtEmissao=" & BL_TrataStringParaCrystal("" & formulas.DtEmissao)
        Report.formulas(11) = "HrEmissao=" & BL_TrataStringParaCrystal("" & formulas.HrEmissao)
        'Verifica se foi a 1�vez que actualizou a 2�via!
        If formulas.ExisteSegVia = True Then
            Report.formulas(12) = "DtSVia=" & BL_TrataStringParaCrystal("" & formulas.DtSVia)
            Report.formulas(13) = "HrSVia=" & BL_TrataStringParaCrystal("" & formulas.HrSVia)
        Else
            Report.formulas(12) = "DtSVia=''"
            Report.formulas(13) = "HrSVia=''"
        End If
        
        If gImprimeMoradaRes = mediSim Then
            'If gTipoInstituicao = gTipoInstituicaoVet Then
            '    Report.formulas(14) = "Morada=" & Replace(BL_TrataStringParaCrystal("" & formulas.MoradaEFR), vbCrLf, " - ")
            '    Report.formulas(15) = "CodPostal=" & BL_TrataStringParaCrystal("" & formulas.codPostalEFR)
            'Else
                Report.formulas(14) = "Morada=" & Replace(BL_TrataStringParaCrystal("" & formulas.MoradaUtente), vbCrLf, " - ")
                Report.formulas(15) = "CodPostal=" & BL_TrataStringParaCrystal("" & formulas.CodPostalUtente)
            'End If
        End If
        
        If gImprimirComValTec = mediSim And Mid(formulas.RespMedicos, 1, 2) = "'," Then
            Report.formulas(16) = "ValidacaoTec=" & BL_TrataStringParaCrystal("" & gTextoValidacaoTecnicaBoletim)
        End If
        
        If gImprimeEFRRes = mediSim Then
            Report.formulas(17) = "EFR=" & BL_TrataStringParaCrystal("" & formulas.DescrEFR)
        End If
        
        Report.formulas(18) = "ObsProven=" & BL_TrataStringParaCrystal("" & formulas.ObsProven)
        If formulas.NomeMed <> "" Then
            Report.formulas(19) = "Medico=" & BL_TrataStringParaCrystal("" & formulas.NomeMed)
        End If
        
        Dim IdadeUtente As String
        Dim DataNasc As Date
        Dim DataChega As Date
            
        DataNasc = BL_HandleNull(formulas.DtNascUtente, "01-01-1900")
        DataChega = formulas.DataChegada
        IdadeUtente = BG_CalculaIdade(DataNasc, DataChega)
        Report.formulas(20) = "IdadeUtente=" & BL_TrataStringParaCrystal("" & IdadeUtente)
    
        If formulas.ficha <> "" Then
            Report.formulas(21) = "Ficha=" & BL_TrataStringParaCrystal("" & formulas.ficha)
        End If
        Report.formulas(22) = "LocalRes=" & BL_TrataStringParaCrystal("" & gCodLocal)
        If InStr(1, BL_TrataStringParaCrystal("" & BL_DevolveGrupoRequis(CLng(NumReq))), ";") = 0 Then
            Report.formulas(23) = "GrAna=" & BL_TrataStringParaCrystal("" & BL_DevolveGrupoRequis(CLng(NumReq)))
        End If
        Report.formulas(24) = "DtColheita=" & BL_TrataStringParaCrystal("" & formulas.DataColheita)
        
        If formulas.nrBenef <> "" Then
            Report.formulas(25) = "Beneficiario=" & BL_TrataStringParaCrystal("" & Trim(Replace(formulas.nrBenef, vbCrLf, "")))
        End If
        Report.formulas(26) = "Isencao=" & BL_TrataStringParaCrystal("" & formulas.isencao)
        Report.formulas(27) = "Destino=" & BL_TrataStringParaCrystal("" & formulas.destino)
        Report.formulas(28) = "n_req_assoc=" & BL_TrataStringParaCrystal("" & formulas.NReqAssoc)
        Report.formulas(29) = "HrChegada=" & BL_TrataStringParaCrystal("" & formulas.HoraChegada)
        Report.formulas(30) = "LocalRequis=" & BL_TrataStringParaCrystal("" & formulas.localRequis)
        Report.formulas(31) = "ObsAnaReq=" & BL_TrataStringParaCrystal("" & formulas.ObsAnaReq)
        Report.formulas(32) = "ObsAutoBloq=" & BL_TrataStringParaCrystal("" & formulas.ObsAutoBloq)
        If formulas.VerTabelaAnexo = True Then
            Report.formulas(33) = "VerTabAnexo=" & BL_TrataStringParaCrystal("1")
        Else
            Report.formulas(33) = "VerTabAnexo=" & BL_TrataStringParaCrystal("0")
        End If
        Report.formulas(34) = "DtValidacao=" & BL_TrataStringParaCrystal("" & formulas.DtValidacao)
        Report.formulas(35) = "HrValidacao=" & BL_TrataStringParaCrystal("" & formulas.HrValidacao)
        
        ' pferreira 2009.07.27
        Report.formulas(36) = "NumExt=" & BL_TrataStringParaCrystal("" & formulas.numExt)
        Report.formulas(37) = "DTIMPRCARTAO=" & BL_TrataStringParaCrystal("" & formulas.dtImprCartao)
        Report.formulas(38) = "DESCRPROFIS=" & BL_TrataStringParaCrystal("" & formulas.descrProfis)
        Report.formulas(39) = "DTCONCLUSAO=" & BL_TrataStringParaCrystal("" & formulas.DtConclusao)
        If imprSegVia = True Then
            Report.formulas(40) = "SEGUNDAVIA=" & BL_TrataStringParaCrystal("1")
        Else
            Report.formulas(40) = "SEGUNDAVIA=" & BL_TrataStringParaCrystal("0")
        End If
        Report.formulas(41) = "GENERO=" & BL_TrataStringParaCrystal(formulas.descr_genero)
        Report.formulas(42) = "RACA=" & BL_TrataStringParaCrystal(formulas.descr_raca)
        Report.formulas(43) = "NOME_DONO=" & BL_TrataStringParaCrystal(formulas.nome_dono)
    End If
    
    Report.SQLQuery = IR_RetornaSqlQueryReport(NumReq, imprRelARS, ImprimeListaExterior)
    
    Report.PageShow Report.ReportStartPage
    Report.WindowState = crptMaximized
    gRelActivoFich = "MapaResultadosSimples"
    If numPag > 0 Then
        Report.PrinterStartPage = numPag
        Report.PrinterStopPage = numPag
        
    End If
    Call BL_ExecutaReport(, , NumReq, MultiReport)
    gRelActivoFich = ""
End Sub


Private Function IR_PreencheDataColheita(n_req As String, dt_colheita As String, hr_colheita As String) As String
    Dim sSql As String
    Dim rsTubo As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * FROM  sl_req_tubo WHERE n_req = " & n_req & " AND dt_colheita is NOT NULL ORDER BY dt_colheita DESC, hr_colheita DESC "
    rsTubo.CursorLocation = adUseServer
    rsTubo.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTubo.Open sSql, gConexao
    If rsTubo.RecordCount >= 1 Then
        IR_PreencheDataColheita = BL_HandleNull(rsTubo!dt_colheita, "") & " " & BL_HandleNull(rsTubo!hr_colheita, "")
    Else
        IR_PreencheDataColheita = BL_HandleNull(dt_colheita, "") & " " & BL_HandleNull(hr_colheita, "")
    End If
    rsTubo.Close
    Set rsTubo = Nothing
    
Exit Function
TrataErro:
    IR_PreencheDataColheita = ""
    BG_LogFile_Erros "BibliotecaLocal: BL_PreencheDataColheita: " & Err.Description, "IR", "BL_PreencheDataColheita"
    Exit Function
    Resume Next
End Function

Private Function IR_DevolveNomeRelatorio(cod_local As String, codGenero As String) As String
    Dim sSql As String
    Dim rsGenero As New ADODB.recordset
    Dim prefixo As String
    On Error GoTo TrataErro
    
    'PREFIXO
    prefixo = ""
    sSql = "SELECT * FROM  gr_empr_inst WHERE empresa_id = " & BL_TrataStringParaBD(cod_local)
    rsGenero.CursorLocation = adUseServer
    rsGenero.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsGenero.Open sSql, gConexao
    If rsGenero.RecordCount >= 1 Then
        prefixo = BL_HandleNull(rsGenero!prefixo_relatorio, "")
    End If
    rsGenero.Close
    
    If codGenero = "" Then
        IR_DevolveNomeRelatorio = prefixo & "MapaResultadosSimples"
        Exit Function
    End If
    sSql = "SELECT * FROM  sl_genero WHERE cod_genero = " & codGenero
    rsGenero.CursorLocation = adUseServer
    rsGenero.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsGenero.Open sSql, gConexao
    If rsGenero.RecordCount >= 1 Then
        IR_DevolveNomeRelatorio = prefixo & BL_HandleNull(rsGenero!nome_relatorio, "MapaResultadosSimples")
    Else
        IR_DevolveNomeRelatorio = prefixo & "MapaResultadosSimples"
    End If
    rsGenero.Close
    Set rsGenero = Nothing
    
Exit Function
TrataErro:
    IR_DevolveNomeRelatorio = "MapaResultadosSimples"
    BG_LogFile_Erros "BibliotecaLocal: BL_DevolveNomeRelatorio: " & Err.Description, "IR", "IR_DevolveNomeRelatorio"
    Exit Function
    Resume Next
End Function




' ------------------------------------------------------------------------------------------------

' DEVOLVE TODOS OS TUBOS QUE CHEGARAM DEPOIS DA DATA DA REQUISI��O

' ------------------------------------------------------------------------------------------------
Private Function IR_DevolveTubosAtrasados(n_req As String, dt_chega As String) As String
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsTubos As New ADODB.recordset
    
    IR_DevolveTubosAtrasados = ""
    sSql = "SELECT x1.cod_tubo, x2.descr_tubo FROM sl_req_tubo x1, sl_tubo x2 WHERE x1.n_Req = " & n_req
    sSql = sSql & " AND (x1.dt_chega IS NULL or x1.dt_chega >" & BL_TrataDataParaBD(dt_chega) & ")"
    sSql = sSql & " AND x1.cod_tubo = x2.cod_tubo "
    RsTubos.CursorLocation = adUseServer
    RsTubos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsTubos.Open sSql, gConexao
    If RsTubos.RecordCount > 0 Then
        While Not RsTubos.EOF
            IR_DevolveTubosAtrasados = IR_DevolveTubosAtrasados & BL_HandleNull(RsTubos!descR_tubo, "") & "; "
            RsTubos.MoveNext
        Wend
    End If
    RsTubos.Close
    Set RsTubos = Nothing
Exit Function
TrataErro:
    IR_DevolveTubosAtrasados = ""
    BG_LogFile_Erros "BibliotecaLocal: IR_DevolveTubosAtrasados: " & Err.Description, "IR", "IR_DevolveTubosAtrasados", False
    Exit Function
    Resume Next
End Function


Function IR_SeleccionaAnalisesPendentes(NumReq As String, GrupoAna As String, FlgDtChega As Boolean) As String

    Dim s As String
    Dim RsAnaPendentes As ADODB.recordset
    Dim AnalisesPendentes As String
    Dim i As Integer
    Dim TmpDescr As String
    Dim sufixo As String
    Dim descricao As String
    
    On Error GoTo TrataErro
    
    AnalisesPendentes = ""
    If Trim(NumReq) <> "" Then
        AnalisesPendentes = ""
        s = " SELECT sl_realiza.cod_agrup " & _
            " FROM sl_realiza LEFT OUTER JOIN sl_req_tubo ON sl_realiza.seq_req_tubo = sl_req_tubo.seq_req_tubo ,sl_ana_s " & _
            " WHERE sl_realiza.n_req=" & Trim(NumReq) & _
            " AND sl_realiza.flg_estado NOT IN ('3','4','2') " & _
            " AND sl_ana_s.t_result <> " & gT_Auxiliar & _
            " AND sl_realiza.cod_ana_s=sl_ana_s.cod_ana_s " & _
            " AND sl_realiza.cod_ana_s <> 'S99999' " & _
            " AND ((sl_ana_s.flg_opcional<>'1' OR sl_ana_s.flg_opcional is null) OR sl_realiza.cod_ana_c='0')" & _
            " AND sl_realiza.seq_realiza NOT IN (SELECT xx.seq_realiza FROM sl_obs_auto xx WHERE sl_realiza.n_req = " & NumReq & ")"
        If Trim(GrupoAna) <> "" Then
            s = s + " AND sl_ana_s.gr_ana='" + GrupoAna + "'"
        End If
        If FlgDtChega = True Then
            s = s & " AND (sl_req_tubo.dt_chega IS NOT NULL  OR SL_REALIZA.seq_req_tubo IS NULL ) "
        Else
            s = s & " AND sl_req_tubo.dt_chega IS NULL AND sl_req_tubo.seq_req_tubo IS NOT NULL "
        End If
        s = s & _
            " GROUP BY sl_realiza.cod_agrup"
        s = s & _
            " UNION " & _
            " SELECT sl_realiza.cod_agrup " & _
            " FROM sl_realiza LEFT OUTER JOIN sl_req_tubo ON sl_realiza.seq_req_tubo = sl_req_tubo.seq_req_tubo ,sl_ana_c  " & _
            " WHERE sl_realiza.n_req=" & Trim(NumReq) & _
            " AND sl_realiza.flg_estado NOT IN ('3','4','2') " & _
            " AND sl_realiza.cod_ana_c=sl_ana_c.cod_ana_c " & _
            " AND sl_realiza.cod_ana_s <> 'S99999' " & _
            " AND sl_realiza.seq_realiza NOT IN (SELECT xx.seq_realiza FROM sl_obs_auto xx WHERE n_req = " & NumReq & ")"
        If Trim(GrupoAna) <> "" Then
            s = s + " AND sl_ana_c.gr_ana='" + GrupoAna + "'"
        End If
        If FlgDtChega = True Then
            s = s & " AND (sl_req_tubo.dt_chega IS NOT NULL  OR SL_REALIZA.seq_req_tubo IS NULL ) "
        Else
            s = s & " AND sl_req_tubo.dt_chega IS NULL AND sl_req_tubo.seq_req_tubo IS NOT NULL "
        End If
        s = s + " GROUP BY sl_realiza.cod_agrup "
        s = s + " UNION "
        s = s + " SELECT sl_realiza.cod_agrup " & _
            " FROM sl_realiza LEFT OUTER JOIN sl_req_tubo ON sl_realiza.seq_req_tubo = sl_req_tubo.seq_req_tubo ,sl_perfis  " & _
            " WHERE sl_realiza.n_req=" & Trim(NumReq) & _
            " AND sl_realiza.flg_estado NOT IN ('3','4','2') " & _
            " AND sl_realiza.cod_perfil=sl_perfis.cod_perfis " & _
            " AND sl_realiza.cod_ana_s <> 'S99999' " & _
            " AND sl_realiza.seq_realiza NOT IN (SELECT xx.seq_realiza FROM sl_obs_auto xx WHERE n_req = " & NumReq & ")"
        If Trim(GrupoAna) <> "" Then
            s = s + " AND sl_perfis.gr_ana='" + GrupoAna + "'"
        End If
        If FlgDtChega = True Then
            s = s & " AND (sl_req_tubo.dt_chega IS NOT NULL  OR SL_REALIZA.seq_req_tubo IS NULL ) "
        Else
            s = s & " AND sl_req_tubo.dt_chega IS NULL AND sl_req_tubo.seq_req_tubo IS NOT NULL "
        End If
        s = s + " GROUP BY sl_realiza.cod_agrup "
        s = s + _
            " UNION " & _
            " SELECT x1.cod_agrup FROM sl_marcacoes x1 LEFT OUTER JOIN sl_req_tubo ON x1.seq_req_tubo = sl_req_tubo.seq_req_tubo , sl_ana_s x2 " & _
            " WHERE X1.cod_ana_s = x2.cod_ana_s And x1.n_req =" & Trim(NumReq) & _
            " AND x1.cod_ana_s <> 'S99999' " & _
            " AND x2.t_result <> " & gT_Auxiliar
            If Trim(GrupoAna) <> "" Then
                s = s + " AND x2.gr_ana='" + GrupoAna + "'"
            End If
        If FlgDtChega = True Then
            s = s & " AND (sl_req_tubo.dt_chega IS NOT NULL  OR x1.seq_req_tubo IS NULL ) "
        Else
            s = s & " AND sl_req_tubo.dt_chega IS NULL AND sl_req_tubo.seq_req_tubo IS NOT NULL "
        End If
        s = s + " GROUP BY x1.cod_agrup "
        s = s + _
            " UNION " & _
            " SELECT x1.cod_agrup FROM sl_marcacoes x1 LEFT OUTER JOIN sl_req_tubo ON x1.seq_req_tubo = sl_req_tubo.seq_req_tubo , sl_ana_c x2 " & _
            " WHERE X1.cod_ana_c = X2.Cod_Ana_C And x1.n_req =" & Trim(NumReq) & _
            " AND x1.cod_ana_s <> 'S99999' "
            If Trim(GrupoAna) <> "" Then
                s = s + " AND x2.gr_ana='" + GrupoAna + "'"
            End If
        If FlgDtChega = True Then
            s = s & " AND (sl_req_tubo.dt_chega IS NOT NULL  OR x1.seq_req_tubo IS NULL ) "
        Else
            s = s & " AND sl_req_tubo.dt_chega IS NULL "
        End If
        s = s + _
            " GROUP BY x1.cod_agrup " & _
            " UNION " & _
            " SELECT x1.cod_agrup FROM sl_marcacoes x1 LEFT OUTER JOIN sl_req_tubo ON x1.seq_req_tubo = sl_req_tubo.seq_req_tubo, sl_perfis x2 " & _
            " WHERE X1.cod_perfil = x2.cod_perfis And x1.n_req =" & Trim(NumReq) & _
            " AND x1.cod_ana_s <> 'S99999' "
            If Trim(GrupoAna) <> "" Then
                s = s + " AND x2.gr_ana='" + GrupoAna + "'"
            End If
        If FlgDtChega = True Then
            s = s & " AND (sl_req_tubo.dt_chega IS NOT NULL  OR x1.seq_req_tubo IS NULL ) "
        Else
            s = s & " AND sl_req_tubo.dt_chega IS NULL AND sl_req_tubo.seq_req_tubo IS NOT NULL "
        End If
        s = s + _
            " GROUP BY x1.cod_agrup "
                        
        Set RsAnaPendentes = New ADODB.recordset
        RsAnaPendentes.CursorLocation = adUseServer
        RsAnaPendentes.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros s
        RsAnaPendentes.Open s, gConexao
        For i = 1 To RsAnaPendentes.RecordCount
            descricao = ""
            descricao = BL_SelCodigo("SLV_ANALISES", "DESCR_ANA", "COD_ANA", BL_HandleNull(RsAnaPendentes!cod_agrup, ""), "V")
            sufixo = ""
            If Mid(RsAnaPendentes!cod_agrup, 1, 1) = "P" Then
                sufixo = IR_SeleccionaAnalisesPendentesPerfil(NumReq, RsAnaPendentes!cod_agrup)
            ElseIf Mid(RsAnaPendentes!cod_agrup, 1, 1) = "C" Then
                sufixo = IR_SeleccionaAnalisesPendentesComplexa(NumReq, RsAnaPendentes!cod_agrup)
            End If
            If Trim(Mid("" & descricao, 1, 2)) = "//" Then
                TmpDescr = Mid(descricao, 3, Len(descricao))
                AnalisesPendentes = AnalisesPendentes & sufixo
                AnalisesPendentes = AnalisesPendentes & TmpDescr & ", "
                
            Else
                AnalisesPendentes = AnalisesPendentes & descricao
               
                AnalisesPendentes = AnalisesPendentes & sufixo & ", "
            End If
            RsAnaPendentes.MoveNext
        Next i
        If Len(Trim(AnalisesPendentes)) <> 0 Then
            AnalisesPendentes = left(AnalisesPendentes, Len(AnalisesPendentes) - 2)
        End If
        RsAnaPendentes.Close
        Set RsAnaPendentes = Nothing
    End If
        
    IR_SeleccionaAnalisesPendentes = AnalisesPendentes
    
    Exit Function
    
TrataErro:
    BG_LogFile_Erros "IR_SeleccionaAnalisesPendentes: " & Err.Number & " - " & Err.Description & " " & s, "IR", "IR_SeleccionaAnalisesPendentes", False
    IR_SeleccionaAnalisesPendentes = ""
    Exit Function
    Resume Next
End Function

Private Function IR_SeleccionaAnalisesPendentesPerfil(n_req As String, Cod_Perfil As String) As String
    Dim sSql As String
    Dim rs As New ADODB.recordset
    Dim ana_c As String
    On Error GoTo TrataErro
    sSql = "SELECT * from sl_realiza WHERE flg_estado in('3','4') and n_req = " & n_req
    sSql = sSql & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil)
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
    If rs.RecordCount = 0 Then
        IR_SeleccionaAnalisesPendentesPerfil = ""
        rs.Close
        Set rs = Nothing
        Exit Function
    End If
    rs.Close
    
    sSql = "SELECT cod_ana_c, sl_ana_s.cod_ana_s FROM sl_realiza, sl_ana_s WHERE flg_estado not in('3','4','2') and n_req = " & n_req
    sSql = sSql & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil) & " AND sl_ana_s.cod_ana_s = sl_realiza.cod_ana_s "
    sSql = sSql & " AND sl_ana_s.t_result <> " & gT_Auxiliar & " AND (sl_ana_s.flg_opcional<>'1' OR sl_ana_s.flg_opcional is null) "
    ' FG -ANALISES -> OBSERVACOES.... eu sei que foi uma grande martelada!!!
    If gLAB = "CHVNG" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s NOT IN (  'S2150','SOBBK','S8075','S1020','S1022')"
    End If
    sSql = sSql & " UNION SELECT  cod_ana_c, sl_ana_s.cod_ana_s FROM sl_marcacoes, sl_ana_s WHERE sl_marcacoes.cod_ana_s <>'S99999' AND  n_req = " & n_req
    sSql = sSql & " AND cod_perfil = " & BL_TrataStringParaBD(Cod_Perfil) & " AND sl_ana_s.cod_ana_s = sl_marcacoes.cod_ana_s "
    sSql = sSql & " AND sl_ana_s.t_result <> " & gT_Auxiliar & " AND (sl_ana_s.flg_opcional<>'1' OR sl_ana_s.flg_opcional is null) "
    ' FG -ANALISES -> OBSERVACOES.... eu sei que foi uma grande martelada!!!
    If gLAB = "CHVNG" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s NOT IN (  'S2150','SOBBK','S8075','S1020','S1022')"
    End If
    If gSGBD = gOracle Then
        sSql = sSql & " ORDER BY  cod_ana_c ASC, cod_ana_s ASC "
    ElseIf gSGBD = gSqlServer Then
        sSql = sSql & " ORDER BY  cod_ana_c ASC, sl_ana_s.cod_ana_s ASC "
    End If
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
    If rs.RecordCount > 0 Then
        ana_c = ""
        IR_SeleccionaAnalisesPendentesPerfil = "  ("
        While Not rs.EOF
            If BL_HandleNull(rs!cod_ana_c, "0") <> "0" And ana_c <> BL_HandleNull(rs!cod_ana_c, "0") Then
                IR_SeleccionaAnalisesPendentesPerfil = IR_SeleccionaAnalisesPendentesPerfil & BL_SelCodigo("SL_ANA_C", "DESCR_ANA_C", "COD_ANA_C", rs!cod_ana_c) & IR_SeleccionaAnalisesPendentesComplexa(n_req, rs!cod_ana_c) & ", "
                ana_c = rs!cod_ana_c
            ElseIf BL_HandleNull(rs!cod_ana_s, "0") <> "0" And BL_HandleNull(rs!cod_ana_c, "0") = "0" Then
                IR_SeleccionaAnalisesPendentesPerfil = IR_SeleccionaAnalisesPendentesPerfil & BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", rs!cod_ana_s) & ", "
            End If
            rs.MoveNext
        Wend
         IR_SeleccionaAnalisesPendentesPerfil = left(IR_SeleccionaAnalisesPendentesPerfil, Len(IR_SeleccionaAnalisesPendentesPerfil) - 2) & ")"
    End If
    rs.Close
    Set rs = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "IR_SeleccionaAnalisesPendentesPerfil: " & Err.Number & " - " & Err.Description & " " & sSql, "IR", "IR_SeleccionaAnalisesPendentesPerfil", False
    IR_SeleccionaAnalisesPendentesPerfil = ""
    Exit Function
    Resume Next
End Function

Private Function IR_SeleccionaAnalisesPendentesComplexa(n_req As String, cod_ana_c As String) As String
    Dim sSql As String
    Dim rs As New ADODB.recordset
    On Error GoTo TrataErro
    
    sSql = "SELECT * from sl_realiza WHERE flg_estado in('3','4') and n_req = " & n_req
    sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c)
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
    If rs.RecordCount = 0 Then
        IR_SeleccionaAnalisesPendentesComplexa = ""
        rs.Close
        Set rs = Nothing
        Exit Function
    End If
    rs.Close
    sSql = "SELECT cod_ana_c, sl_ana_s.cod_ana_s FROM sl_realiza, sl_ana_s WHERE flg_estado not in('3','4','2') and n_req = " & n_req
    sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c) & " AND sl_ana_s.cod_ana_s = sl_realiza.cod_ana_s "
    sSql = sSql & " AND sl_ana_s.t_result <> " & gT_Auxiliar & " AND (sl_ana_s.flg_opcional<>'1' OR sl_ana_s.flg_opcional is null) "
    ' FG -ANALISES -> OBSERVACOES.... eu sei que foi uma grande martelada!!!
    If gLAB = "CHVNG" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s NOT IN (  'S2150','SOBBK','S8075','S1020','S1022')"
    End If
    sSql = sSql & " UNION SELECT  cod_ana_c, sl_ana_s.cod_ana_s FROM sl_marcacoes, sl_ana_s WHERE sl_marcacoes.cod_ana_s <>'S99999' AND  n_req = " & n_req
    sSql = sSql & " AND cod_ana_c = " & BL_TrataStringParaBD(cod_ana_c) & " AND sl_ana_s.cod_ana_s = sl_marcacoes.cod_ana_s "
    sSql = sSql & " AND sl_ana_s.t_result <> " & gT_Auxiliar & " AND (sl_ana_s.flg_opcional<>'1' OR sl_ana_s.flg_opcional is null) "
    ' FG -ANALISES -> OBSERVACOES.... eu sei que foi uma grande martelada!!!
    If gLAB = "CHVNG" Then
        sSql = sSql & " AND sl_ana_s.cod_ana_s NOT IN (  'S2150','SOBBK','S8075','S1020','S1022')"
    End If
    sSql = sSql & " ORDER  BY cod_ana_c ASC , cod_ana_s ASC "
    
    rs.CursorLocation = adUseServer
    rs.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rs.Open sSql, gConexao
    If rs.RecordCount > 0 Then
        IR_SeleccionaAnalisesPendentesComplexa = "  ("
        While Not rs.EOF
            If BL_HandleNull(rs!cod_ana_s, "0") <> "0" Then
                IR_SeleccionaAnalisesPendentesComplexa = IR_SeleccionaAnalisesPendentesComplexa & BL_SelCodigo("SL_ANA_S", "DESCR_ANA_S", "COD_ANA_S", rs!cod_ana_s) & ", "
            End If
            rs.MoveNext
        Wend
         IR_SeleccionaAnalisesPendentesComplexa = left(IR_SeleccionaAnalisesPendentesComplexa, Len(IR_SeleccionaAnalisesPendentesComplexa) - 2) & ")"
    End If
    rs.Close
    Set rs = Nothing
Exit Function
TrataErro:
    BG_LogFile_Erros "IR_SeleccionaAnalisesPendentesComplexa: " & Err.Number & " - " & Err.Description & " " & sSql, "IR", "IR_SeleccionaAnalisesPendentesComplexa", False
    IR_SeleccionaAnalisesPendentesComplexa = ""
    Exit Function
    Resume Next
End Function


Function IR_TrataMicro(NumReq As String, TotalEstrutMicro As Integer, EstrutMicro() As String, Flg_ResMicro As Boolean, _
    ResultadosAnteriores As Boolean, regRes As ADODB.recordset, CmdResMicro As ADODB.Command, _
    RegCmd As ADODB.recordset, resAlf1 As String, resAlf2 As String, CmdResMicroTsq As ADODB.Command, RegCmdMTsq As ADODB.recordset, _
    VecSplit() As String, codRespMedicos As String, _
    RespMedicos As String, CodRespTecnicos As String, respTecnicos As String, _
    Unid1ResNum1 As String, ObservResultado As String, _
    FlagResComent As Boolean, ComAutomatico As String, ResNum1Unid1 As String, _
    FlagRTFAnalise As Boolean, _
    Unid2ResNum1 As String, ValRef2 As String, ValRef1 As String, _
    ZonaRef1 As String, Metodo1 As String, Metodo2 As String, ResNum1Unid2 As String, _
    FlagRes1Anormal As Boolean, FlagRes2Anormal As Boolean, OrdemImpressao As Integer, _
    LinhasPagina As Integer, RsContaMembros As ADODB.recordset, UltimaComplexa As String, _
    ContaMembrosComplexa As Integer, ContaMembrosPerfil As Integer, UltimoPerfil As String, _
    UltimoGrupo As String, UltimoSubgrupo As String, UltimaClasse As String, UltimaDt1 As String, _
    UltimaDt2 As String, UltimaDt3 As String, EstrutZonas() As String, TotalEstrutZonas As Integer, _
    EstrutObsResultado() As String, TotalEstrutObsResultado As Integer, ObservAnalise As String, Grupo As String, _
    subGrupo As String, Classe As String, Perfil As String, complexa As String, simples As String, _
    cutoff As Boolean, registos As Long, ordem_grupo As String) As Boolean
    Dim sqlAntib As String
    Dim s As String
    Dim sql As String
    Dim l As Integer, i As Integer
    Dim RText As RichTextBox
    Dim TamResAlf1 As String
    Dim TamResAlf2 As String
    Dim Simples2 As String
    Dim RespValMedTec As String
    Dim RespValMedM As String
    Dim ResAlf1_Ant1 As String
    Dim ResAlf1_Ant2 As String
    Dim ResAlf1_Ant3 As String
    Dim Dt_ResAlf1_Ant1 As String
    Dim Dt_ResAlf1_Ant2 As String
    Dim Dt_ResAlf1_Ant3 As String
    Dim Unid1ResNum2 As String
    Dim Unid2ResNum2 As String
    Dim ZonaRef2 As String
    Dim ResNum2Unid2 As String
    Dim ResNum2Unid1 As String
    Dim FlagAntibiograma As String
    Dim abrev_micro As String
    
    Dim ObservAnalise_Aux As String
    Dim ObservResultado_Aux As String
    Dim ComAutomatico_Aux As String
    
    Dim codAntib As String
    Dim CodMicro As String
    Dim Micro_ant1 As String
    Dim Micro_ant2 As String
    Dim Micro_ant3 As String
    Dim CodGrAntibio As String
    Dim descrFraseMicro As String
    
    Dim InsereSimples As Boolean
    
    Dim MicroAssoc As String
    Dim CMI As String
    
    ObservAnalise_Aux = ObservAnalise
    ObservResultado_Aux = ObservResultado
    ComAutomatico_Aux = ComAutomatico
        
    IR_TrataMicro = False
    TotalEstrutMicro = 0
    ReDim EstrutMicro(TotalEstrutMicro)
    Flg_ResMicro = False
    InsereSimples = False
    
    'If ResultadosAnteriores = False Then
        CmdResMicro.Parameters("SEQ_REALIZA").value = regRes!seq_realiza
    'Else
        'Ver sources anteriores a 11-2002 FN
    'End If
    
    Set RegCmd = CmdResMicro.Execute
    While Not RegCmd.EOF
        
        CodGrAntibio = ""
        Dt_ResAlf1_Ant1 = ""
        Dt_ResAlf1_Ant2 = ""
        Dt_ResAlf1_Ant3 = ""
        Unid1ResNum2 = ""
        Unid2ResNum2 = ""
        ZonaRef1 = ""
        ZonaRef2 = ""
        ResNum2Unid2 = ""
        ResNum2Unid1 = ""
        Unid1ResNum1 = ""
        Unid2ResNum1 = ""
        ResNum1Unid2 = ""
        ResNum1Unid1 = ""
        ValRef2 = ""
        ValRef1 = ""
        ZonaRef1 = ""
        ZonaRef2 = ""
'        ObservAnalise = ""
'        ObservResultado = ""
        ComAutomatico = ""
        FlagAntibiograma = "Null"
        CodMicro = ""
        Micro_ant1 = ""
        Micro_ant2 = ""
        Micro_ant3 = ""
        IR_TrataMicro = True
        Flg_ResMicro = True
        MicroAssoc = ""
        CMI = ""
        descrFraseMicro = ""
        
        'Obt�m os Resultados
        resAlf1 = "" 'FN
        resAlf2 = "" 'FN
        If RegCmd!N_Res = 1 Then
            resAlf1 = BL_HandleNull(RegCmd!descr_microrg, "") 'FN
            abrev_micro = BL_HandleNull(RegCmd!abrev_microrg, "")
            'ResAlf1 = ResAlf1 & Trim("" & RegCmd!descr_microrg & IIf(Trim(BL_HandleNull(RegCmd!Quantif, "")) = "", "", Replace(Space(50 - Len(RegCmd!descr_microrg) + 1), " ", ".")) & IIf(Trim(BL_HandleNull(RegCmd!Quantif, "")) = "", "", "  Qt.: ") & BL_HandleNull(RegCmd!Quantif, ""))
            'ResAlf1 = ResAlf1 & Trim("" & RegCmd!descr_microrg & IIf(Trim(BL_HandleNull(RegCmd!Quantif, "")) = "", "", "   " & RegCmd!Quantif))
            CodMicro = BL_HandleNull(RegCmd!cod_micro, "")
        Else
            resAlf2 = BL_HandleNull(RegCmd!descr_microrg, "") 'FN
            abrev_micro = BL_HandleNull(RegCmd!abrev_microrg, "")
            'ResAlf2 = ResAlf2 & Trim("" & RegCmd!descr_microrg & IIf(Trim(BL_HandleNull(RegCmd!Quantif, "")) = "", "", Replace(Space(50 - Len(RegCmd!descr_microrg) + 1), " ", ".")) & IIf(Trim(BL_HandleNull(RegCmd!Quantif, "")) = "", "", "  Qt.: ") & BL_HandleNull(RegCmd!Quantif, "")) & vbNewLine
            'ResAlf2 = ResAlf2 & Trim("" & RegCmd!descr_microrg & IIf(Trim(BL_HandleNull(RegCmd!Quantif, "")) = "", "", "   " & RegCmd!Quantif))
        End If
        CodGrAntibio = BL_HandleNull(RegCmd!Cod_Gr_Antibio, "")
        If InStr(CodGrAntibio, ";") <> 0 Then
            CodGrAntibio = ""
        Else
            CodGrAntibio = SelDescrGrAntibio(CodGrAntibio)
        End If
        MicroAssoc = BL_DevolveMicroAssociados(CodMicro)
        descrFraseMicro = BL_HandleNull(RegCmd!descr_obs_ana_rtf, "")
'        ResAlf1_Ant1 = BL_HandleNull(RegCmd!res_ant1, "")
'        Micro_ant1 = BL_HandleNull(RegCmd!Micro_ant1, "")
'        Dt_ResAlf1_Ant1 = BL_HandleNull(RegCmd!dt_res_ant1, "")
'        ResAlf1_Ant2 = BL_HandleNull(RegCmd!res_ant2, "")
'        Micro_ant2 = BL_HandleNull(RegCmd!Micro_ant2, "")
'        Dt_ResAlf1_Ant2 = BL_HandleNull(RegCmd!dt_res_ant2, "")
'        ResAlf1_Ant3 = BL_HandleNull(RegCmd!res_ant3, "")
'        Micro_ant3 = BL_HandleNull(RegCmd!Micro_ant3, "")
'        Dt_ResAlf1_Ant3 = BL_HandleNull(RegCmd!dt_res_ant3, "")
        
        'Verifica se tem Tsq
        'If ResultadosAnteriores = False Then
            CmdResMicroTsq.Parameters("SEQ_REALIZA").value = regRes!seq_realiza
            CmdResMicroTsq.Parameters("COD_MICRO").value = RegCmd!cod_micro
            CmdResMicroTsq.Parameters("N_RES").value = RegCmd!N_Res
        'Else
            'ver sources anteriores a 11-2002 FN
        'End If
        Set RegCmdMTsq = CmdResMicroTsq.Execute
        If Not RegCmdMTsq.EOF = True Then
            While RegCmdMTsq.EOF <> True
                CMI = ""
                Select Case UCase(Trim("" & RegCmdMTsq!res_sensib))
                    Case "R"
                        s = "Resistente"
                    Case "S"
                        s = "Sens�vel"
                    Case "I"
                        s = "Interm�dio"
                    Case "P"
                        s = "Positivo"
                    Case "N"
                        s = "Negativo"
                    Case Else
                        s = ""
                End Select
                CMI = BL_HandleNull(RegCmdMTsq!CMI, "")
                'Obt�m os Resultados
                If RegCmd!N_Res = 1 Then
                    codAntib = BL_HandleNull(RegCmdMTsq!cod_antibio, "")
                    
                    sql = "INSERT INTO sl_cr_lr_micro(microrganismo, antibiotico, sensibilidade, quantificacao, " & _
                        "cmi, id_empresa, n_resultado, cod_micro, cod_gr_antibio, nome_computador, num_sessao, " & _
                        " ORDEM_ANTIB, N_REQ, MICRO_ASSOC,descr_frase_micro ) VALUES('" & _
                        resAlf1 & "', '" & RegCmdMTsq!descr_antibio & "', '" & s & "', '" & _
                        BL_HandleNull(RegCmd!Quantif, "") & "', " & BL_TrataStringParaBD(CMI) & "," & _
                        gCodLocal & " , " & regRes!seq_realiza & "," & BL_TrataStringParaBD(CodMicro) & "," & _
                        BL_TrataStringParaBD(CodGrAntibio) & ", " & BL_TrataStringParaBD(gComputador) & "," & gNumeroSessao & "," & _
                        BL_HandleNull(RegCmdMTsq!ordem_imp, 0) & ", " & NumReq & ", " & BL_TrataStringParaBD(MicroAssoc) & "," & _
                        BL_TrataStringParaBD(descrFraseMicro) & " )"
                    BG_ExecutaQuery_ADO sql
                        
                    'INSERE ANTIBIOTICO
                    sqlAntib = "INSERT INTO sl_cr_lr_micro_antib( n_resultado, microrganismo, quantificacao, cod_micro, cod_antib,"
                    sqlAntib = sqlAntib & " descr_antib, cod_Gr_antibio, nome_computador, num_sessao, ordem_antib, n_req, micro_Assoc, "
                    sqlAntib = sqlAntib & " descr_frase_micro, tipo, resultado, abrev_micro) VALUES("
                    sqlAntib = sqlAntib & regRes!seq_realiza & ","
                    sqlAntib = sqlAntib & BL_TrataStringParaBD(resAlf1) & ","
                    sqlAntib = sqlAntib & BL_TrataStringParaBD(BL_HandleNull(RegCmd!Quantif, "")) & ","
                    sqlAntib = sqlAntib & BL_TrataStringParaBD(CodMicro) & ","
                    sqlAntib = sqlAntib & BL_TrataStringParaBD(codAntib) & ","
                    sqlAntib = sqlAntib & BL_TrataStringParaBD(BL_HandleNull(RegCmdMTsq!descr_antibio, "")) & ","
                    sqlAntib = sqlAntib & BL_TrataStringParaBD(CodGrAntibio) & ","
                    sqlAntib = sqlAntib & BL_TrataStringParaBD(gComputador) & ","
                    sqlAntib = sqlAntib & gNumeroSessao & ","
                    sqlAntib = sqlAntib & BL_HandleNull(RegCmdMTsq!ordem_imp, 0) & ","
                    sqlAntib = sqlAntib & NumReq & ","
                    sqlAntib = sqlAntib & BL_TrataStringParaBD(MicroAssoc) & ","
                    sqlAntib = sqlAntib & BL_TrataStringParaBD(descrFraseMicro) & ","
                    
                    sql = sqlAntib & BL_TrataStringParaBD("Interpreta��o") & ","
                    sql = sql & BL_TrataStringParaBD(s) & ","
                    sql = sql & BL_TrataStringParaBD(abrev_micro) & ")"
                    BG_ExecutaQuery_ADO sql
                    
                    sql = sqlAntib & BL_TrataStringParaBD("CMI(mg/L)") & ","
                    sql = sql & BL_TrataStringParaBD(CMI) & ","
                    sql = sql & BL_TrataStringParaBD(abrev_micro) & ")"
                    BG_ExecutaQuery_ADO sql
                    
                    FlagAntibiograma = "'1'"
                    TotalEstrutMicro = TotalEstrutMicro + 1 'FN
                Else
                    resAlf2 = resAlf2 & "  " & Trim("" & RegCmdMTsq!descr_antibio & IIf(Trim(BL_HandleNull(RegCmdMTsq!CMI, "")) = "" And s = "", "", Replace(Space(30 - Len("" & RegCmdMTsq!descr_antibio) - 1), " ", ".")) & s & Space(12 - Len(s)) & IIf(Trim(BL_HandleNull(RegCmdMTsq!CMI, "")) = "", "", "C.M.I.: ") & Trim(BL_HandleNull(RegCmdMTsq!CMI, ""))) & vbNewLine
                End If
                RegCmdMTsq.MoveNext
            Wend
        Else
            'se n�o tiver antibiograma
            If RegCmd!N_Res = 1 Then
                sql = "INSERT INTO sl_cr_lr_micro(microrganismo, antibiotico, sensibilidade, quantificacao, " & _
                    "cmi, id_empresa, n_resultado, cod_micro, cod_gr_antibio, nome_computador, num_sessao, " & _
                    "ORDEM_ANTIB, N_REQ, MICRO_ASSOC, descr_Frase_micro) VALUES('" & _
                    resAlf1 & "', Null, Null, '" & _
                    BL_HandleNull(RegCmd!Quantif, "") & "', " & _
                     BL_TrataStringParaBD(CMI) & "," & gCodLocal & ", " & regRes!seq_realiza & "," & BL_TrataStringParaBD(CodMicro) & "," & _
                    BL_TrataStringParaBD(CodGrAntibio) & ", " & BL_TrataStringParaBD(gComputador) & "," & gNumeroSessao & ",null," & _
                    NumReq & ", " & BL_TrataStringParaBD(MicroAssoc) & "," & BL_TrataStringParaBD(descrFraseMicro) & ")"
                
                BG_ExecutaQuery_ADO sql
                
                'INSERE ANTIBIOTICO
                sqlAntib = "INSERT INTO sl_cr_lr_micro_antib( n_resultado, microrganismo, quantificacao, cod_micro, cod_antib,"
                sqlAntib = sqlAntib & " descr_antib, cod_Gr_antibio, nome_computador, num_sessao, ordem_antib, n_req, micro_Assoc, "
                sqlAntib = sqlAntib & " descr_frase_micro, tipo, resultado, abrev_micro) VALUES("
                sqlAntib = sqlAntib & regRes!seq_realiza & ","
                sqlAntib = sqlAntib & BL_TrataStringParaBD(resAlf1) & ","
                sqlAntib = sqlAntib & BL_TrataStringParaBD(BL_HandleNull(RegCmd!Quantif, "")) & ","
                sqlAntib = sqlAntib & BL_TrataStringParaBD(CodMicro) & ","
                sqlAntib = sqlAntib & " NULL,"
                sqlAntib = sqlAntib & " NULL,"
                sqlAntib = sqlAntib & BL_TrataStringParaBD(CodGrAntibio) & ","
                sqlAntib = sqlAntib & BL_TrataStringParaBD(gComputador) & ","
                sqlAntib = sqlAntib & gNumeroSessao & ","
                sqlAntib = sqlAntib & " NULL,"
                sqlAntib = sqlAntib & NumReq & ","
                sqlAntib = sqlAntib & BL_TrataStringParaBD(MicroAssoc) & ","
                sqlAntib = sqlAntib & BL_TrataStringParaBD(descrFraseMicro) & ","
                
                sql = sqlAntib & BL_TrataStringParaBD("Interpreta��o") & ","
                sql = sql & " NULL,"
                sql = sql & BL_TrataStringParaBD(abrev_micro) & ")"
                BG_ExecutaQuery_ADO sql
                
                FlagAntibiograma = "'1'"
                TotalEstrutMicro = TotalEstrutMicro + 1 'FN
                    
                'ResAlf1 = ResAlf1 & IIf(Trim(BL_HandleNull(RegCmd!Quantif, "")) = "", "", "   " & RegCmd!Quantif)
            End If
        End If
    
    
    '___________________________________________________________________
    
    'Actualiza os Responsaveis T�cnicos e M�dicos pela valida��o da an�lise
    If Flg_ResMicro = True Then
        'M�dicos
        If InStr(1, codRespMedicos, "-" & BL_HandleNull(regRes!cod_utilizador_med, "") & "-") = 0 Then
            
            If BL_HandleNull(regRes!titulo_med, "") = "" And gLAB <> "HOSPOR" Then
                
                Select Case regRes!cod_gr_util_med
                    
                    Case gGrupoAdministradores
                        RespMedicos = RespMedicos & "Administrador"
                    
                    Case gGrupoTecnicos
                        RespMedicos = RespMedicos & "T�cnico"
                    
                    Case gGrupoChefeSecretariado
                        RespMedicos = RespMedicos & "Chefe Secretariado"
                    
                    Case gGrupoSecretariado
                        RespMedicos = RespMedicos & "Secretariado"
                    
                    Case gGrupoMedicos
                        RespMedicos = RespMedicos & "M�dico"
                        
                    Case gGrupoEnfermeiros
                        RespMedicos = RespMedicos & "Enfermeiro"
                    
                    Case gGrupoFarmaceuticos
                        RespMedicos = RespMedicos & "Farmac�utico"
                
                End Select
            Else
                'RespMedicos = RespMedicos & RegRes!titulo_med
                RespMedicos = RespMedicos & BL_HandleNull(regRes!nome_med, "")
                RespValMedM = RespValMedM & BL_HandleNull(regRes!nome_med, "")
            End If
            
            'RespMedicos = RespMedicos & ", " & RegRes!Nome_med & vbNewLine
            RespMedicos = RespMedicos & " - " & IIf(BL_HandleNull(regRes!titulo_med, "") = "", BL_HandleNull(regRes!nome_med, ""), BL_HandleNull(regRes!titulo_med, "")) & vbNewLine
            RespValMedM = RespValMedM & " - " & IIf(BL_HandleNull(regRes!titulo_med, "") = "", BL_HandleNull(regRes!nome_med, ""), BL_HandleNull(regRes!titulo_med, "")) & vbNewLine
            'CodRespMedicos = CodRespMedicos & "-" & RegRes!cod_utilizador_med & "-" & "," 'soliveira (replace , por -)
            ' Bruno 14-03-2003 Esta linha � para controlar os utilizadores que validam mais do que um grupo
            ' � necess�rio controlar o Sta. Marta
            codRespMedicos = "-" & BL_HandleNull(regRes!cod_utilizador_med, "") & "-" & ","
        ElseIf RespValMedM = "" Then
                RespValMedM = BL_HandleNull(regRes!nome_med, "")
                RespValMedM = RespValMedM & " - " & IIf(BL_HandleNull(regRes!titulo_med, "") = "", "", regRes!titulo_med) & vbNewLine
        End If
        IR_PreencheValMedAntib regRes!seq_realiza, BL_HandleNull(RegCmd!cod_micro, ""), codRespMedicos, RespMedicos, RespValMedM
        ' T�cnicos.
        ' Verifica se foi respons�vel por alguma Valida��o M�dica (em caso afirmativo � omitido).
        
        If (InStr(1, CodRespTecnicos, "-" & BL_HandleNull(regRes!cod_utilizador_tec, "") & "-") = 0 And InStr(1, codRespMedicos, "-" & BL_HandleNull(regRes!cod_utilizador_tec, "") & "-") = 0) Then
            
            If BL_HandleNull(regRes!titulo_tec, "") = "" Then
                
                Select Case BL_HandleNull(regRes!cod_gr_util_tec, "")
                    
                    Case gGrupoAdministradores
                        respTecnicos = respTecnicos & "Administrador"
                    
                    Case gGrupoTecnicos
                        respTecnicos = respTecnicos & "T�cnico"
                    
                    Case gGrupoChefeSecretariado
                        respTecnicos = respTecnicos & "Chefe Secretariado"
                    
                    Case gGrupoSecretariado
                        respTecnicos = respTecnicos & "Secretariado"
                    
                    Case gGrupoMedicos
                        respTecnicos = respTecnicos & "M�dico"
                    
                    Case gGrupoEnfermeiros
                        respTecnicos = respTecnicos & "Enfermeiro"
                
                End Select
            
            Else
                'RespTecnicos = RespTecnicos & RegRes!titulo_tec
                respTecnicos = BL_HandleNull(regRes!nome_tec, "")
                RespValMedTec = BL_HandleNull(regRes!nome_tec, "")
            End If
            
            'RespTecnicos = RespTecnicos & ", " & RegRes!nome_tec & vbNewLine
            respTecnicos = respTecnicos & " - " & IIf(BL_HandleNull(regRes!titulo_tec, "") = "", BL_HandleNull(regRes!nome_tec, ""), BL_HandleNull(regRes!titulo_tec, "")) & vbNewLine
            RespValMedTec = RespValMedTec & " - " & IIf(BL_HandleNull(regRes!titulo_tec, "") = "", BL_HandleNull(regRes!nome_tec, ""), BL_HandleNull(regRes!titulo_tec, "")) & vbNewLine
            CodRespTecnicos = CodRespTecnicos & "-" & BL_HandleNull(regRes!cod_utilizador_tec, "") & "-" & ","     'soliveira (replace , por -)
        
        End If
        
        FlagRTFAnalise = False
        
        '___________________________________________________________________
        'Verifica os valores nulos para Grava��o:
        
        'Os resultados das an�lises tipo auxiliares s�o Alfanum�ricos!!
        
        'Verifica se o 2� Resultado � do tipo auxiliar=>Fica sem resultados
        If "" & regRes!Tipo2 = gT_Auxiliar Then
            resAlf2 = ""
            ResNum2Unid1 = ""
            ResNum2Unid2 = ""
            FlagRes2Anormal = False
            Unid1ResNum2 = ""
            Unid2ResNum2 = ""
            ValRef2 = ""
            ZonaRef2 = ""
            Metodo2 = ""
        End If
        
        'Verifica se o 1� Resultado � do tipo auxiliar
        '=>Fica com os resultados do 2�Resultado e limpa os do 2�!!
        If "" & regRes!Tipo1 = gT_Auxiliar Then
            If Trim(ObservResultado) <> "" Then
                'Voltar a colocar no formato normal=>Para o formato RTF s�o necess�rios muitos caracteres (controlo)
                RText.TextRTF = ObservResultado
                RText.SelStart = 0
                RText.SelLength = Len(RText.TextRTF)
                RText.SelFontSize = gResTamLetra
                'RText.SelFontName = "Courier New"
                RText.SelFontName = "Microsoft Sans Serif"
                ObservResultado = RText.TextRTF
                resAlf1 = ObservResultado
                ObservResultado = ""
                
                ResNum1Unid1 = ""
                ResNum1Unid2 = ""
                FlagRTFAnalise = True
                FlagRes1Anormal = False
                Unid1ResNum1 = ""
                Unid2ResNum1 = ""
                ValRef1 = ""
                ZonaRef1 = ""
                Metodo1 = ""
            Else
                resAlf1 = resAlf2
                ResNum1Unid1 = ResNum2Unid1
                ResNum1Unid2 = ResNum2Unid2
                FlagRes1Anormal = FlagRes2Anormal
                Unid1ResNum1 = Unid1ResNum2
                Unid2ResNum1 = Unid2ResNum2
                ValRef1 = ValRef2
                ZonaRef1 = ZonaRef2
                Metodo1 = Metodo2
                ComAutomatico = ""
                FlagResComent = False
                ObservResultado = ""
    
                resAlf2 = ""
                ResNum2Unid1 = ""
                ResNum2Unid2 = ""
                Unid1ResNum2 = ""
                Unid2ResNum2 = ""
                ValRef2 = ""
                ZonaRef2 = ""
                Metodo2 = ""
                FlagRes2Anormal = False
            End If
            
        End If
        
        'A an�lise tem resultados
        If resAlf1 <> "" Or TotalEstrutMicro <> 0 Or ResNum1Unid1 <> "" Or ResNum1Unid2 <> "" Then
            '_______________________________________________________________________________
    
            'Para o 1�Resultado verifica se o coment�rio autom�tico sobrep�e o resultado
            If FlagResComent = True And ComAutomatico <> "" Then
                'Sobrep�e o resultado alfanum�rico e limpa o num�rico
                If Len(ComAutomatico) > 20 Then
                    resAlf1 = ComAutomatico
                    ResNum1Unid1 = ""
                    ResNum1Unid2 = ""
                    Unid1ResNum1 = ""
                Else
                    ResNum1Unid1 = ComAutomatico
                    ResNum1Unid2 = ""
                    resAlf1 = ""
                    Unid1ResNum1 = ""
                End If
                ComAutomatico = ""
            End If
    
            '_______________________________________________________________________________
            
            
            'Para o 1� Resultado:
            
            'Verifica se omite o Resultado e Valores\Zonas de Refer�ncia
            'Observa��es no Resultado
            'Se apanhou An�lises com resultado TipoObserva��es=>ResNum1Unid1
            If (ResNum1Unid1 = cVerObservacoes) Or (resAlf1 = cVerObservacoes) Then
                RText.TextRTF = ObservResultado
                RText.SelStart = 0
                RText.SelLength = Len(RText.TextRTF)
                RText.SelFontSize = gResTamLetra
                'RText.SelFontName = "Courier New"
                RText.SelFontName = "Microsoft Sans Serif"
                ObservResultado = RText.TextRTF
                resAlf1 = ObservResultado
                ObservResultado = ""
                FlagRTFAnalise = True
                ResNum1Unid1 = ""
                ResNum1Unid2 = ""
                Unid1ResNum1 = ""
                Unid2ResNum1 = ""
                ValRef1 = ""
                ZonaRef1 = ""
            End If
            
            'Verificar sem resultado
            If (ResNum1Unid1 = cSemResultado) Or (resAlf1 = cSemResultado) Then
                If "" & regRes!Casas_dec_1 = "" Then
                    resAlf1 = "--"
                Else
                    resAlf1 = "--." & Replace(Space(CLng("" & regRes!Casas_dec_1)), " ", "-")
                End If
                ResNum1Unid1 = ""
                ResNum1Unid2 = ""
                Unid1ResNum1 = ""
                Unid2ResNum1 = ""
                ValRef1 = ""
                ZonaRef1 = ""
            End If
            '------------------------------------------------------------
            
            
            'Unidades:
            
            'Unidades 1� Resultado
            If Trim(Unid1ResNum1) = "" Then
                Unid1ResNum1 = "Null"
            Else
                Unid1ResNum1 = BL_TrataStringParaBD(Unid1ResNum1)
            End If
            If Trim(Unid2ResNum1) = "" Then
                Unid2ResNum1 = "Null"
            Else
                Unid2ResNum1 = BL_TrataStringParaBD(Unid2ResNum1 & ")")
            End If
            
            'Unidades 2� Resultado
            If Trim(Unid1ResNum2) = "" Then
                Unid1ResNum2 = "Null"
            Else
                Unid1ResNum2 = BL_TrataStringParaBD(Unid1ResNum2)
            End If
            If Trim(Unid2ResNum2) = "" Then
                Unid2ResNum2 = "Null"
            Else
                Unid2ResNum2 = BL_TrataStringParaBD(Unid2ResNum2 & ")")
            End If
            
            
            'Resultados AlfaNum�ricos :
            
            '1� Resultado
            
            'Unidade1
            If Trim(ResNum1Unid1) = "" Then
                ResNum1Unid1 = "Null"
                Unid1ResNum1 = "Null"
            Else
                ResNum1Unid1 = BL_TrataStringParaBD("" & ResNum1Unid1)
            End If
            'Unidade2
            If Trim(ResNum1Unid2) = "" Then
                ResNum1Unid2 = "Null"
                Unid2ResNum1 = "Null"
            Else
                ResNum1Unid2 = BL_TrataStringParaBD("(" & ResNum1Unid2 & IIf(Unid2ResNum1 = "Null", ")", ""))
            End If
            If ResNum1Unid1 = "" And ResNum1Unid2 = "" Then
                'N�o interessa ter a indica��o se n�o tiver resultados!
                ValRef1 = ""
                ZonaRef1 = ""
            End If
    
            '2� Resultado
            
            'Unidade1
            If Trim(ResNum2Unid1) = "" Then
                ResNum2Unid1 = "Null"
                Unid1ResNum2 = "Null"
            Else
                ResNum2Unid1 = BL_TrataStringParaBD("" & ResNum2Unid1)
            End If
            'Unidade2
            If Trim(ResNum2Unid2) = "" Then
                ResNum2Unid2 = "Null"
                Unid2ResNum2 = "Null"
            Else
                ResNum2Unid2 = BL_TrataStringParaBD("(" & ResNum2Unid2 & IIf(Unid2ResNum2 = "Null", ")", ""))
            End If
            If ResNum2Unid1 = "" And ResNum2Unid2 = "" Then
                'N�o interessa ter a indica��o se n�o tiver resultados!
                ValRef2 = ""
                ZonaRef2 = ""
            End If
                    
            '------------------------------------------------------------
            
            If Trim(resAlf1) = "" Then
                TamResAlf1 = "0"
                resAlf1 = "Null"
            Else
                TamResAlf1 = Len(resAlf1)
                resAlf1 = BL_TrataStringParaBD("" & resAlf1)
            End If
            
            If Trim(resAlf2) = "" Then
                TamResAlf2 = "0"
                resAlf2 = "Null"
            Else
                TamResAlf2 = Len(resAlf2)
                resAlf2 = BL_TrataStringParaBD("" & resAlf2)
            End If
            
            If Trim(ValRef1) = "" Then
                ValRef1 = "Null"
            Else
                ValRef1 = BL_TrataStringParaBD("" & ValRef1)
            End If
    
            If Trim(ValRef2) = "" Then
                ValRef2 = "Null"
            Else
                ValRef2 = BL_TrataStringParaBD("" & ValRef2)
            End If
            
            If Trim(ZonaRef1) = "" Then
                ZonaRef1 = "Null"
            Else
                ZonaRef1 = BL_TrataStringParaBD("" & ZonaRef1)
            End If
            
            If Trim(ZonaRef2) = "" Then
                ZonaRef2 = "Null"
            Else
                ZonaRef2 = BL_TrataStringParaBD("" & ZonaRef2)
            End If
            
            If Trim(ObservAnalise) = "" Then
                ObservAnalise = "Null"
            Else
                ObservAnalise = BL_TrataStringParaBD(ObservAnalise)
            End If

            If Trim(ObservResultado) = "" Then
                ObservResultado = "Null"
            Else
                ObservResultado = BL_TrataStringParaBD(ObservResultado)
            End If

            If Trim(ComAutomatico) = "" Then
                ComAutomatico = "Null"
            Else
                ComAutomatico = BL_TrataStringParaBD(ComAutomatico)
            End If
        
            If gModeloMapaRes = 3 Then '(Modelo 3 -> HPP)
                If UltimoGrupo <> Grupo Then
                    If UltimoGrupo = "1" Then '1� vez
                        LinhasPagina = LinhasPagina - 2
                    ElseIf Grupo <> "" And Grupo <> "Null" Then
                        OrdemImpressao = OrdemImpressao + 1
                        BL_Insert_LinhaBranco regRes!seq_realiza, Grupo, subGrupo, Perfil, complexa, simples, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3, LinhasPagina, False
                        LinhasPagina = LinhasPagina - 2
                    End If
                End If
            
                If UltimoSubgrupo <> subGrupo Then
                    If subGrupo <> "" And subGrupo <> "Null" Then
                        LinhasPagina = LinhasPagina - 1
                    End If
                End If
                
                If UltimaClasse <> Classe Then
                    If Classe <> "" And Classe <> "Null" Then
                        LinhasPagina = LinhasPagina - 1
                    End If
                End If
                
'                If (UltimaDt1 <> dt_resalf1_ant1 And (dt_resalf1_ant1 <> "" And dt_resalf1_ant1 <> "Null")) Then
'                    LinhasPagina = LinhasPagina - 1
'                    UltimaDt1 = dt_resalf1_ant1
'                End If
                If (UltimaDt1 <> Dt_ResAlf1_Ant1 And (Dt_ResAlf1_Ant1 <> "" And Dt_ResAlf1_Ant1 <> "Null")) Or _
                    (UltimaDt1 = Dt_ResAlf1_Ant1 And (Dt_ResAlf1_Ant1 <> "" And Dt_ResAlf1_Ant1 <> "Null") And LinhasPagina = gLinhasPagina) Then 'Mudou de pagina
                        LinhasPagina = LinhasPagina - 1
                        UltimaDt1 = Dt_ResAlf1_Ant1
                End If
                
                If UltimoPerfil <> Perfil Then
                    If Perfil <> "" And Perfil <> "Null" And Mid(complexa, 1, 3) <> "'//" Then
                        LinhasPagina = LinhasPagina - 1
                    End If
                End If
                
                If UltimaComplexa <> complexa Then
                    If complexa <> "" And complexa <> "Null" And Mid(complexa, 1, 3) <> "'//" Then
                        LinhasPagina = LinhasPagina - 1
                    End If
                End If
                
                If LinhasPagina <= 0 Then
                    OrdemImpressao = OrdemImpressao + 1
                    LinhasPagina = gLinhasPagina
                    If UltimoGrupo = Grupo And UltimoSubgrupo = subGrupo Then
                        BL_Insert_QuebraPagina regRes!seq_realiza, Grupo, subGrupo, Perfil, complexa, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
                    ElseIf UltimoGrupo <> Grupo And UltimoSubgrupo <> subGrupo Then
                        LinhasPagina = LinhasPagina - 3
                        BL_Insert_QuebraPagina regRes!seq_realiza, "'_'", "''", Perfil, complexa, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
                    ElseIf UltimoGrupo = Grupo And UltimoSubgrupo <> subGrupo Then
                        LinhasPagina = LinhasPagina - 1
                        BL_Insert_QuebraPagina regRes!seq_realiza, Grupo, "''", Perfil, complexa, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
                    ElseIf UltimoGrupo <> Grupo And UltimoSubgrupo = subGrupo Then
                        LinhasPagina = LinhasPagina - 2
                        BL_Insert_QuebraPagina regRes!seq_realiza, "'_'", subGrupo, Perfil, complexa, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
                    End If
                    
'                    If (UltimaDt1 <> dt_resalf1_ant1 And (dt_resalf1_ant1 <> "" And dt_resalf1_ant1 <> "Null")) Then
'                        LinhasPagina = LinhasPagina - 1
'                    End If
                    If (UltimaDt1 <> Dt_ResAlf1_Ant1 And (Dt_ResAlf1_Ant1 <> "" And Dt_ResAlf1_Ant1 <> "Null")) Or _
                        (UltimaDt1 = Dt_ResAlf1_Ant1 And (Dt_ResAlf1_Ant1 <> "" And Dt_ResAlf1_Ant1 <> "Null") And LinhasPagina = gLinhasPagina) Then 'Mudou de pagina
                        LinhasPagina = LinhasPagina - 1
                        UltimaDt1 = Dt_ResAlf1_Ant1
                    End If
                    
                    If UltimoPerfil <> Perfil Then
                        If Perfil <> "" And Perfil <> "Null" And Mid(Perfil, 1, 3) <> "'//" Then
                            LinhasPagina = LinhasPagina - 1
                        End If
                    End If
                    
                    If UltimaComplexa <> complexa Then
                        If complexa <> "" And complexa <> "Null" And Mid(complexa, 1, 3) <> "'//" Then
                            LinhasPagina = LinhasPagina - 1
                        End If
                    End If
                    'Por causa da repeti��o do grupo e subgrupo qd se muda de p�gina
                    'UltimoGrupo = ""
                    'UltimoSubgrupo = ""
                End If
                
                'Conta membros do perfil (sl_realiza)
                ContaMembrosPerfil = -1
                If BL_HandleNull(regRes!Cod_Perfil, "0") <> "0" And UltimoPerfil <> Perfil Then
                    BL_ContaMembrosPerfil regRes!seq_realiza, NumReq, Grupo, subGrupo, Perfil, complexa, UltimoPerfil, ContaMembrosPerfil, RsContaMembros, OrdemImpressao, LinhasPagina, regRes!Cod_Perfil, UltimoGrupo, UltimoSubgrupo, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
                    UltimoPerfil = Perfil
                Else
                    UltimoPerfil = Perfil
                End If
                'Conta membros da complexa
                ContaMembrosComplexa = -1
                If BL_HandleNull(regRes!cod_ana_c, "0") <> "0" And UltimaComplexa <> complexa Then
                    BL_ContaMembrosComplexa regRes!seq_realiza, NumReq, Grupo, subGrupo, regRes!Cod_Perfil, complexa, UltimaComplexa, ContaMembrosComplexa, RsContaMembros, OrdemImpressao, LinhasPagina, regRes!cod_ana_c, UltimoGrupo, UltimoSubgrupo, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
                    UltimaComplexa = complexa
                Else
                    UltimaComplexa = complexa
                End If
                'Linhas do antibiograma
                If TotalEstrutMicro > LinhasPagina Then
                    OrdemImpressao = OrdemImpressao + 1
                    LinhasPagina = gLinhasPagina
                    BL_Insert_QuebraPagina regRes!seq_realiza, Grupo, subGrupo, Perfil, complexa, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
                End If
            End If
            
            If Perfil = "Null" And complexa = "Null" Then
                complexa = simples
                Perfil = simples
            'complexa
            ElseIf Perfil = "Null" And complexa <> "Null" Then
                Perfil = complexa
            ElseIf Perfil <> "Null" And complexa = "Null" Then
                complexa = Perfil
            End If
            
            'Preenche a tabela tempor�ria=>Para cada registo do RecordSet
            LinhasPagina = LinhasPagina - 1
            OrdemImpressao = OrdemImpressao + 1
            If InsereSimples = False Then
            sql = " INSERT INTO SL_CR_LR "
            sql = sql & " (n_resultado,data,grupo,subgrupo,perfil,complexa,simples,resnum1unid1,resnum1unid2,Unid1ResNum1, Unid2ResNum1,resalf1,tamresalf1,valref1,zonaref1,resnum2unid1,resnum2unid2,Unid1ResNum2, Unid2ResNum2,resalf2,tamresalf2,valref2,zonaref2,"
            sql = sql & "observanalise,observresultado,comautomatico,flagresalf2,flagobserv,flagcomaut,flagresult,flagresanormal1,flagresanormal2,flagcutoff,flagRTFanalise,"
            sql = sql & "id_empresa, res_ant1, dt_res_ant1, res_ant2, dt_res_ant2, res_ant3, dt_res_ant3, classe, ordem_imp, respmedicos, resptecnicos, flagantibiograma, cod_micro,micro_ant1,micro_ant2,micro_ant3,nome_computador, ordem_grupo,  num_sessao, n_req,local_exec "
            sql = sql & ", dt_val, hr_val, resp_val_med_med,resp_val_med_tec,descr_gr_impr,ordem_gr_impr,cod_gr_impr, user_cri, flg_nao_avaliar,descr_relatorio,cod_perfil,cod_ana_c, cod_ana_s, obs_auto_aviso,ordem_sgr_impr  ) VALUES ( " & regRes!seq_realiza & "," & IIf(regRes!n_req = NumReq, "Null", BL_TrataDataParaBD("" & regRes!dt_val)) & "," & Grupo & "," & subGrupo & "," & Perfil & "," & complexa & "," & simples & ","
            sql = sql & ResNum1Unid1 & "," & ResNum1Unid2 & "," & Unid1ResNum1 & "," & Unid2ResNum1 & "," & resAlf1 & "," & TamResAlf1 & "," & ValRef1 & "," & ZonaRef1 & ","
            sql = sql & ResNum2Unid1 & "," & ResNum2Unid2 & "," & Unid1ResNum2 & "," & Unid2ResNum2 & "," & resAlf2 & "," & TamResAlf2 & "," & ValRef2 & "," & ZonaRef2 & ","
            sql = sql & ObservAnalise & ",NULL," & ComAutomatico & ","
            sql = sql & IIf(resAlf2 = "Null", "Null", "'S'") & "," & IIf(ObservAnalise = "Null", "Null", "'S'") & "," & IIf(ComAutomatico = "Null", "Null", "'S'") & ",null ,"
            sql = sql & BL_TrataStringParaBD(left(BL_HandleNull(regRes!flg_anormal, ""), 1)) & "," & BL_TrataStringParaBD(left(BL_HandleNull(regRes!flg_anormal_2, ""), 1)) & "," & IIf(cutoff = False, "Null", "'S'") & "," & IIf(FlagRTFAnalise = False, "Null", "'S'") & ","
            sql = sql & gCodLocal & ", "
            sql = sql & "" & BL_TrataStringParaBD(ResAlf1_Ant1) & "," & BL_TrataDataParaBD(Dt_ResAlf1_Ant1) & ","
            sql = sql & "" & BL_TrataStringParaBD(ResAlf1_Ant2) & "," & BL_TrataDataParaBD(Dt_ResAlf1_Ant2) & ","
            sql = sql & "" & BL_TrataStringParaBD(ResAlf1_Ant3) & "," & BL_TrataDataParaBD(Dt_ResAlf1_Ant3) & ","
            sql = sql & Classe & ", " & OrdemImpressao & "," & BL_TrataStringParaBD(RespMedicos) & "," & BL_TrataStringParaBD(respTecnicos) & ", "
            sql = sql & FlagAntibiograma & "," & BL_TrataStringParaBD(CodMicro) & "," & BL_TrataStringParaBD(Micro_ant1) & "," & BL_TrataStringParaBD(Micro_ant2) & "," & BL_TrataStringParaBD(Micro_ant3) & ",'" & gComputador & "'," & ordem_grupo
            sql = sql & ", " & gNumeroSessao & "," & NumReq & ",1,"
            sql = sql & BL_TrataDataParaBD(BL_HandleNull(regRes!dt_val, "")) & "," & BL_TrataStringParaBD(BL_HandleNull(regRes!hr_val, "")) & ","
            sql = sql & BL_TrataStringParaBD(RespValMedM) & ", "
            sql = sql & BL_TrataStringParaBD(RespValMedTec) & "," & BL_TrataStringParaBD(BL_HandleNull(regRes!descr_gr_impr, "")) & "," & BL_HandleNull(regRes!ordem_gr_imp, 99) & "," & BL_TrataStringParaBD(BL_HandleNull(regRes!gr_impr, "")) & ","
            sql = sql & BL_TrataStringParaBD(params(gArgumentoUserCri)) & "," & params(gArgumentoNaoAvaliaRes) & ", " & BL_TrataStringParaBD(params(gArgumentoDescrRelatorio)) & "," & BL_TrataStringParaBD(params(gArgumentoCodPerfil)) & "," & BL_TrataStringParaBD(params(gArgumentoCodAnaC))
            sql = sql & "," & BL_TrataStringParaBD(params(gArgumentoCodAnaS)) & "," & BL_TrataStringParaBD(params(gArgumentoObsAutoAviso)) & "," & BL_HandleNull(regRes!ordem_sgr_impr, 99) & " )"
            BG_ExecutaQuery_ADO sql
            InsereSimples = True
            End If
            
            If gModeloMapaRes = 3 Then
                If LinhasPagina <= 0 Then
                    LinhasPagina = gLinhasPagina
                    If UltimoGrupo = Grupo And UltimoSubgrupo = subGrupo Then
                        BL_Insert_QuebraPagina regRes!seq_realiza, Grupo, subGrupo, Perfil, complexa, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
                    ElseIf UltimoGrupo <> Grupo And UltimoSubgrupo <> subGrupo Then
                        LinhasPagina = LinhasPagina - 3
                        BL_Insert_QuebraPagina regRes!seq_realiza, "'_'", "''", Perfil, complexa, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
                    ElseIf UltimoGrupo = Grupo And UltimoSubgrupo <> subGrupo Then
                        LinhasPagina = LinhasPagina - 1
                        BL_Insert_QuebraPagina regRes!seq_realiza, Grupo, "''", Perfil, complexa, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
                    ElseIf UltimoGrupo <> Grupo And UltimoSubgrupo = subGrupo Then
                        LinhasPagina = LinhasPagina - 2
                        BL_Insert_QuebraPagina regRes!seq_realiza, "'_'", subGrupo, Perfil, complexa, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
                    End If
                    
                    If (UltimaDt1 <> Dt_ResAlf1_Ant1 And (Dt_ResAlf1_Ant1 <> "" And Dt_ResAlf1_Ant1 <> "Null")) Then
                        LinhasPagina = LinhasPagina - 1
                    End If
                    
                    If UltimoPerfil <> Perfil Then
                        If Perfil <> "" And Perfil <> "Null" And Mid(complexa, 1, 3) <> "'//" Then
                            LinhasPagina = LinhasPagina - 1
                        End If
                    End If
                    
                    If UltimaComplexa <> complexa Then
                        If complexa <> "" And complexa <> "Null" And Mid(complexa, 1, 3) <> "'//" Then
                            LinhasPagina = LinhasPagina - 1
                        End If
                    End If
                    'Por causa da repeti��o do grupo e subgrupo qd se muda de p�gina
                    'UltimoGrupo = ""
                    'UltimoSubgrupo = ""
                End If
                                
                Simples2 = BL_HandleNull(regRes!cod_ana_s, "'0'")
                BL_Insert_LinhaBranco regRes!seq_realiza, Grupo, subGrupo, Perfil, complexa, Simples2, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3, LinhasPagina, True
                
                If TotalEstrutMicro > 0 Then
                    LinhasPagina = LinhasPagina - TotalEstrutMicro
                    If LinhasPagina <= 0 Then
                        BL_Insert_QuebraPagina regRes!seq_realiza, Grupo, subGrupo, Perfil, complexa, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
                        LinhasPagina = gLinhasPagina
                    End If
                    TotalEstrutMicro = 0
                End If
                
            End If
            
            UltimoGrupo = Grupo
            UltimoSubgrupo = subGrupo
            UltimaClasse = Classe
            UltimaDt1 = Dt_ResAlf1_Ant1
            UltimaDt2 = Dt_ResAlf1_Ant2
            UltimaDt3 = Dt_ResAlf1_Ant3
            
            registos = registos + 1
            
        End If
    End If
    
        RegCmd.MoveNext
    Wend
    RegCmd.Close
    Set RegCmd = Nothing
        
    'S� faz o update aos comentarios no fim de ter escrito todos os micros
    If Flg_ResMicro = True And gModeloMapaRes <> 3 Then
        ObservAnalise = ObservAnalise_Aux
        ObservResultado = ObservResultado_Aux
        ComAutomatico = ComAutomatico_Aux
    
        If Trim(ObservAnalise) = "" Or Trim(ObservAnalise) = "Null" Then
            ObservAnalise = "Null"
        Else
            ObservAnalise = BL_TrataStringParaBD(ObservAnalise)
        End If
        
        If Trim(ObservResultado) = "" Or Trim(ObservResultado) = "Null" Then
            ObservResultado = "Null"
        Else
            ObservResultado = BL_TrataStringParaBD(ObservResultado)
        End If
        
        If Trim(ComAutomatico) = "" Or Trim(ComAutomatico) = "Null" Then
            ComAutomatico = "Null"
        Else
            ComAutomatico = BL_TrataStringParaBD(ComAutomatico)
        End If
    
        sql = "UPDATE SL_CR_LR SET observanalise=" & ObservAnalise & ", flagobserv=" & IIf(ObservAnalise = "Null", "Null", "'S'") & _
            ", observresultado=null, flagresult=" & IIf(ObservResultado = "Null", "Null", "'S'") & _
            ", comautomatico=" & ComAutomatico & ", flagcomaut=" & IIf(ComAutomatico = "Null", "Null", "'S'") & _
            " WHERE ordem_imp=" & OrdemImpressao & " and nome_computador = '" & gComputador & "'"
        BG_ExecutaQuery_ADO sql
    ElseIf Flg_ResMicro = True And gModeloMapaRes = 3 Then
        BL_InsereObsAnalise regRes!seq_realiza, Grupo, subGrupo, Classe, Perfil, complexa, simples, OrdemImpressao, LinhasPagina, EstrutObsResultado(), TotalEstrutObsResultado, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
        LinhasPagina = LinhasPagina - TotalEstrutObsResultado
        If LinhasPagina <= 0 Then
            BL_Insert_QuebraPagina regRes!seq_realiza, Grupo, subGrupo, Perfil, complexa, OrdemImpressao, Dt_ResAlf1_Ant1, Dt_ResAlf1_Ant2, Dt_ResAlf1_Ant3
            LinhasPagina = gLinhasPagina
        End If
    End If
    
End Function




Function IR_Insere_SLCRLR(seqRealiza As String, NumReq As String, NReq As String, dtVal As String, Grupo As String, subGrupo As String, Perfil As String, complexa As String, simples As String, _
                            ResNum1Unid1 As String, ResNum1Unid2 As String, Unid1ResNum1 As String, Unid2ResNum1 As String, resAlf1 As String, TamResAlf1 As String, ValRef1 As String, ZonaRef1 As String, _
                            ResNum2Unid1 As String, ResNum2Unid2 As String, Unid1ResNum2 As String, Unid2ResNum2 As String, resAlf2 As String, TamResAlf2 As String, ValRef2 As String, ZonaRef2 As String, _
                            ObservAnalise As String, ObservResultado As String, ComAutomatico As String, _
                            FlgAnormal As String, FlagRes2Anormal As String, cutoff As Boolean, FlagRTFAnalise As Boolean, _
                            ResAlf1_Ant1 As String, Dt_ResAlf1_Ant1 As String, LocalRes_Ant1 As String, _
                            ResAlf1_Ant2 As String, Dt_ResAlf1_Ant2 As String, LocalRes_Ant2 As String, _
                            ResAlf1_Ant3 As String, Dt_ResAlf1_Ant3 As String, LocalRes_Ant3 As String, _
                            Classe As String, OrdemImpressao As Integer, RespMedicos As String, respTecnicos As String, Metodo1 As String, MetodoPerfil As String, MetodoComplexa As String, Volume As String, gr_user_val_med As String, cod_produto As String, _
                            descr_produto As String, ordem_produto As String, _
                            FlgPosRes As Integer, _
                            RsRes As ADODB.recordset, SegundoResultado As Seg_Res, ReutilAna As AdicionaReutil)
    
    Dim sql As String
    Dim ordem_grupo As String
    Dim LocalExec As Integer
    
    gSQLError = 0
    If Perfil = "Null" And complexa = "Null" Then
        complexa = simples
        Perfil = simples
    'complexa
    ElseIf Perfil = "Null" And complexa <> "Null" Then
        Perfil = complexa
    ElseIf Perfil <> "Null" And complexa = "Null" Then
        complexa = Perfil
    End If
    
    LocalExec = BL_HandleNull(RsRes!local_exec, -1)
    ordem_grupo = BL_HandleNull(RsRes!ord_grupo, "0")
    
    sql = " INSERT INTO SL_CR_LR(n_resultado,data,grupo,subgrupo,perfil,complexa,simples,resnum1unid1,resnum1unid2,"
    sql = sql & " Unid1ResNum1, Unid2ResNum1,resalf1,tamresalf1,valref1,zonaref1,resnum2unid1,resnum2unid2,Unid1ResNum2, "
    sql = sql & " Unid2ResNum2,resalf2,tamresalf2,valref2,zonaref2,observanalise,observresultado,comautomatico, "
    sql = sql & " flagresalf2,flagobserv,flagcomaut,flagresult,flagresanormal1,flagresanormal2,flagcutoff,"
    sql = sql & " flagRTFanalise,id_empresa, res_ant1, dt_res_ant1,local_res_ant1, res_ant2, dt_res_ant2,"
    sql = sql & " local_res_ant2, res_ant3, dt_res_ant3,local_res_ant3, classe, ordem_imp, respmedicos, "
    sql = sql & " resptecnicos, metodo, metodo_perfil, metodo_complexa, volume, gr_user_val_med, nome_computador,"
    sql = sql & " cod_produto, descr_produto, ordem_produto,  ordem_grupo, descr_especif, obs_especif,"
    sql = sql & " local_exec, num_sessao, flg_pos_res, n_req, OBS_AUTO_AVISO, COD_PERFIL, COD_ANA_C, COD_ANA_S, "
    sql = sql & " DESCR_RELATORIO,FLG_NAO_AVALIAR, OBS_REF, USER_CRI, ordem_ars, carac_metodo1, carac_metodo2, "
    sql = sql & " carac_met_perf, carac_met_comp, dt_val, hr_val,cod_gr_impr,descr_gr_impr, ordem_gr_impr, "
    sql = sql & " resp_val_med_med,resp_val_med_tec,ordem_sgr_impr, flg_bold, respmedicos_funcao, resptecnicos_funcao ,"
    'brunodsantos 03.12.2015
    sql = sql & " seg_res_ant1, seg_dt_res_ant1, seg_res_ant2, seg_dt_res_ant2, seg_res_ant3, seg_dt_res_ant3, flg_res_ant_2 ,"
    '
    'NELSONPSILVA CHVNG-7461 29.10.2018
    sql = sql & " prescricao, dt_val_canc_reutil, hr_val_canc_reutil, user_val_canc_reutil, flg_reutil) "
    '
    sql = sql & " VALUES ( " & seqRealiza & "," & IIf(NReq = NumReq, "Null", BL_TrataDataParaBD("" & dtVal)) & ","
    sql = sql & Grupo & "," & subGrupo & "," & Perfil & "," & complexa & "," & simples & ","
    sql = sql & ResNum1Unid1 & "," & ResNum1Unid2 & "," & Unid1ResNum1 & "," & Unid2ResNum1 & "," & resAlf1 & ","
    sql = sql & TamResAlf1 & "," & ValRef1 & "," & ZonaRef1 & "," & ResNum2Unid1 & "," & ResNum2Unid2 & ","
    sql = sql & Unid1ResNum2 & "," & Unid2ResNum2 & "," & resAlf2 & "," & TamResAlf2 & "," & ValRef2 & ","
    sql = sql & ZonaRef2 & "," & ObservAnalise & "," & ObservResultado & "," & ComAutomatico & ","
    sql = sql & IIf(resAlf2 = "Null", "Null", "'S'") & "," & IIf(ObservAnalise = "Null", "Null", "'S'") & ","
    sql = sql & IIf(ComAutomatico = "Null", "Null", "'S'") & "," & IIf(ObservResultado = "Null", "Null", "'S'")
    sql = sql & "," & IIf(FlgAnormal = "Null", "Null", BL_TrataStringParaBD(left(FlgAnormal, 1))) & ","
    sql = sql & IIf(FlagRes2Anormal = "Null", "Null", BL_TrataStringParaBD(left(FlagRes2Anormal, 1))) & ","
    sql = sql & IIf(cutoff = False, "Null", "'S'") & "," & IIf(FlagRTFAnalise = False, "Null", "'S'") & ","
    sql = sql & gCodLocal & ", " & BL_TrataStringParaBD(ResAlf1_Ant1) & "," & BL_TrataDataParaBD(Dt_ResAlf1_Ant1)
    sql = sql & "," & BL_TrataStringParaBD(LocalRes_Ant1) & "," & BL_TrataStringParaBD(ResAlf1_Ant2) & ","
    sql = sql & BL_TrataDataParaBD(Dt_ResAlf1_Ant2) & "," & BL_TrataStringParaBD(LocalRes_Ant2) & ","
    sql = sql & BL_TrataStringParaBD(ResAlf1_Ant3) & "," & BL_TrataDataParaBD(Dt_ResAlf1_Ant3) & ","
    sql = sql & BL_TrataStringParaBD(LocalRes_Ant3) & "," & Classe & ", " & OrdemImpressao & ","
    sql = sql & BL_TrataStringParaBD(RespMedicos) & "," & BL_TrataStringParaBD(respTecnicos) & ","
    sql = sql & BL_TrataStringParaBD(Metodo1) & "," & BL_TrataStringParaBD(MetodoPerfil) & ","
    sql = sql & BL_TrataStringParaBD(MetodoComplexa) & ", " & Volume & ", " & BL_TrataStringParaBD(gr_user_val_med)
    sql = sql & "," & BL_TrataStringParaBD(gComputador) & "," & BL_TrataStringParaBD(cod_produto)
    sql = sql & "," & BL_TrataStringParaBD(descr_produto) & "," & ordem_produto
    sql = sql & "," & ordem_grupo & "," & BL_TrataStringParaBD(descr_especif) & "," & BL_TrataStringParaBD(obs_especif)
    sql = sql & "," & LocalExec & ", " & gNumeroSessao & "," & FlgPosRes & "," & NReq & "," & BL_TrataStringParaBD(params(gArgumentoObsAutoAviso))
    sql = sql & "," & BL_TrataStringParaBD(params(gArgumentoCodPerfil)) & "," & BL_TrataStringParaBD(params(gArgumentoCodAnaC))
    sql = sql & "," & BL_TrataStringParaBD(params(gArgumentoCodAnaS)) & ", " & BL_TrataStringParaBD(params(gArgumentoDescrRelatorio))
    sql = sql & "," & params(gArgumentoNaoAvaliaRes) & "," & params(gArgumentoObsRef) & ", "
    sql = sql & BL_TrataStringParaBD(params(gArgumentoUserCri)) & ", " & params(gArgumentoOrdemARS) & ","
    sql = sql & BL_TrataStringParaBD(params(gArgumentoCaracMetodo1)) & "," & BL_TrataStringParaBD(params(gArgumentoCaracMetodo2))
    sql = sql & "," & BL_TrataStringParaBD(params(gArgumentoCaracMetodoPerf)) & "," & BL_TrataStringParaBD(params(gArgumentoCaracMetodoComp)) & ","
    sql = sql & BL_TrataDataParaBD(BL_HandleNull(RsRes!dt_val, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(RsRes!hr_val, ""))
    sql = sql & "," & BL_TrataStringParaBD(BL_HandleNull(RsRes!gr_impr, "")) & ", " & BL_TrataStringParaBD(BL_HandleNull(RsRes!descr_gr_impr, ""))
    sql = sql & ", " & BL_HandleNull(RsRes!ordem_gr_imp, 99) & "," & BL_TrataStringParaBD(params(gArgumentoRespValMedMed)) & ", "
    sql = sql & BL_TrataStringParaBD(params(gArgumentoRespValMedTec)) & "," & BL_HandleNull(RsRes!ordem_sgr_impr, 99) & "," & BL_HandleNull(RsRes!negrito, 0) & ","
    sql = sql & BL_TrataStringParaBD(BL_HandleNull(RsRes!funcao_med, "")) & "," & BL_TrataStringParaBD(BL_HandleNull(RsRes!funcao_tec, "")) & ","
    'brunodsantos 03.12.2015
    sql = sql & BL_TrataStringParaBD(BL_HandleNull(SegundoResultado.Seg_ResAlf1_Ant1, "")) & ","
    sql = sql & BL_TrataDataParaBD(SegundoResultado.Seg_Dt_ResAlf1_Ant1, "") & ","
    sql = sql & BL_TrataStringParaBD(BL_HandleNull(SegundoResultado.Seg_ResAlf1_Ant2, "")) & ","
    sql = sql & BL_TrataDataParaBD(SegundoResultado.Seg_Dt_ResAlf1_Ant2, "") & ","
    sql = sql & BL_TrataStringParaBD(BL_HandleNull(SegundoResultado.Seg_ResAlf1_Ant3, "")) & "," '")"
    sql = sql & BL_TrataDataParaBD(SegundoResultado.Seg_Dt_ResAlf1_Ant3, "") & "," & BL_TrataStringParaBD(CStr(SegundoResultado.flg_res_ant_2)) & ","
    sql = sql & BL_TrataStringParaBD(BL_HandleNull(ReutilAna.n_prescricao, "")) & "," & BL_TrataDataParaBD(BL_HandleNull(ReutilAna.dt_val_canc_reutil, "")) & ","
    sql = sql & BL_TrataStringParaBD(BL_HandleNull(ReutilAna.hr_val_canc_reutil, "")) & "," & BL_TrataStringParaBD(BL_HandleNull(ReutilAna.user_val_canc_reutil, "")) & ","
    sql = sql & BL_HandleNull(ReutilAna.flg_reutil, 0) & ")"
    '
    BG_ExecutaQuery_ADO sql

    If gSQLError <> 0 Then
        BG_Mensagem mediMsgBox, "Erro a inserir na tabela tempor�ria SL_CR_LR ", vbCritical + vbOKOnly, "Erro"
    End If
End Function



' -----------------------------------------------------------------------------------------------

' MAIS UM PROCEDIMENTO PARA TIRAR O "PROCEDURE TO LARGE"

' -----------------------------------------------------------------------------------------------
Public Function IR_ApagaTabelasResultados() As Boolean
    Dim sql As String
    On Error GoTo TrataErro
    sql = "DELETE FROM sl_cr_lr_micro WHERE nome_computador='" & gComputador & "' AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    sql = "DELETE FROM sl_cr_lr_micro_antib WHERE nome_computador='" & gComputador & "' AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    sql = "DELETE FROM SL_RES_GRAFICO_IM WHERE NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    gConexao.Execute sql
    
    sql = "DELETE FROM SL_CR_LR WHERE NOME_COMPUTADOR = '" & gComputador & "'AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sql
    
    sql = "DELETE FROM SL_CR_LRVF WHERE NOME_COMPUTADOR = '" & gComputador & "'AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sql

    IR_ApagaTabelasResultados = True
Exit Function
TrataErro:
    IR_ApagaTabelasResultados = False
    BG_LogFile_Erros "IR_ApagaTabelasResultados - " & Err.Description
End Function
Function IR_ConstroiSqlValRef(CodSexo As String, SeqUtente As String, DtNascUtente As String, DiasUtente As Long, ObsProven As String)
    Dim s As String

    'VERIFICA PELAS 2 TABELAS
    s = "SELECT sl_zona_ref.sexo,sl_zona_ref.t_idade_inf,sl_zona_ref.idade_inf,sl_zona_ref.t_idade_sup,sl_zona_ref.idade_sup,sl_zona_ref.ref_min,"
    s = s & " sl_zona_ref.ref_max,sl_zona_ref.txt_livre,sl_or_zonas.descr_or_zonas,A.descr_t_data DATAINF,B.descr_t_data DATASUP,sl_diag.descr_diag,sl_zona_ref.flg_inibe_val  "
    'OUTER JOIN
    If gSGBD = gInformix Then
        s = s & "FROM sl_zona_ref,OUTER sl_or_zonas,OUTER sl_tbf_t_data A,OUTER sl_tbf_t_data B,OUTER sl_diag WHERE "
    Else
        s = s & "FROM sl_zona_ref,sl_or_zonas,sl_tbf_t_data A,sl_tbf_t_data B,sl_diag WHERE "
    End If
    
    s = s & "sl_zona_ref.cod_ana_ref=? AND " & _
        "" & _
        "(sl_zona_ref.sexo IS NULL " & _
        IIf(CodSexo = "", "", "OR (sl_zona_ref.sexo IS NOT NULL AND sl_zona_ref.sexo='" & CodSexo & "')") & ") AND " & _
        "" & _
        "(sl_zona_ref.cod_diag IS NULL " & _
        "OR (sl_zona_ref.cod_diag IS NOT NULL AND sl_zona_ref.cod_diag IN (SELECT sl_diag_pri.cod_diag FROM sl_diag_pri WHERE sl_diag_pri.seq_utente = " & SeqUtente & " UNION SELECT sl_diag_sec.cod_diag FROM sl_diag_sec WHERE sl_diag_sec.N_Req = ?) )" & ") AND " & _
        "" & _
        "(sl_zona_ref.t_idade_inf IS NULL " & _
        IIf(DtNascUtente = "", "", "OR (sl_zona_ref.t_idade_inf IS NOT NULL AND (sl_zona_ref.t_idade_inf IN (3,4) OR (sl_zona_ref.t_idade_inf NOT IN (3,4) AND sl_zona_ref.idade_inf_dias < " & DiasUtente & " AND sl_zona_ref.idade_sup_dias >=" & DiasUtente & ")))") & ") AND " & _
        "" & _
        "(sl_zona_ref.s_gestacao IS NULL )AND "
        'IIf(ObsProven = "", "", "OR (sl_zona_ref.s_gestacao IS NOT NULL AND sl_zona_ref.s_gestacao='" & BL_HandleNull(ObsProven, "") & "')") & ") AND "

    Select Case gSGBD
        Case gOracle
            s = s & "sl_zona_ref.cod_or_zonas=sl_or_zonas.cod_or_zonas(+) AND sl_zona_ref.t_idade_inf=A.cod_t_data(+) AND sl_zona_ref.t_idade_sup=B.cod_t_data(+) AND sl_zona_ref.cod_diag=sl_diag.cod_diag(+) "
        Case gInformix
            s = s & "sl_zona_ref.cod_or_zonas=sl_or_zonas.cod_or_zonas AND sl_zona_ref.t_idade_inf=A.cod_t_data AND sl_zona_ref.t_idade_sup=B.cod_t_data AND sl_zona_ref.cod_diag=sl_diag.cod_diag "
        Case gSqlServer
            s = s & "sl_zona_ref.cod_or_zonas*=sl_or_zonas.cod_or_zonas AND sl_zona_ref.t_idade_inf*=A.cod_t_data AND sl_zona_ref.t_idade_sup*=B.cod_t_data AND sl_zona_ref.cod_diag*=sl_diag.cod_diag "
    End Select
    
    IR_ConstroiSqlValRef = s

End Function



Function IR_ConstroiSqlRes(GrupoAna As String, SubGrupoAna As String, Multiplo As Boolean, _
                            ListaAnalises As String, estadosAna As String, NumReq As String, _
                            tabRes As String, imprRelARS As Boolean, ImprComValTec As Boolean)
    Dim sql As String
    Dim sqlAssinatura As String
    Dim rsAssinatura As New ADODB.recordset
    
    BL_MudaEstadoReq (CLng(NumReq))
    'RGONCALVES 10.12.2015 CHSJ-2511 - acrescentado ana_c_acreditada e ana_p_acreditada
    ''NELSONPSILVA CHVNG-7461 29.10.2018 Adicionadas novos campos para a reutiliza��o das colheitas
    sql = "SELECT " & _
        "slv_analises.cod_gr_ana,slv_analises.cod_sgr_ana," & _
        "RES.seq_realiza,RES.n_req,RES.flg_estado,RES.flg_anormal,RES.flg_anormal_2,RES.ord_ana,RES.cod_agrup,RES.cod_perfil,RES.ord_ana_perf,RES.cod_ana_c,RES.ord_ana_compl,RES.cod_ana_s,RES.dt_val,res.hr_val, RES.dt_tec_val, " & _
        "RES.flg_unid_act1,RES.unid_1_res1,RES.unid_2_res1,RES.fac_conv_unid1,RES.flg_unid_act2,RES.unid_1_res2,RES.unid_2_res2,RES.fac_conv_unid2," & _
        "sl_perfis.descr_perfis,sl_perfis.ordem,sl_perfis.metodo metodo_perfil, " & _
        "sl_ana_c.descr_ana_c,sl_ana_c.ordem,sl_ana_c.metodo metodo_complexa, " & _
        "sl_ana_s.seq_ana_s,sl_ana_s.descr_ana_s,sl_ana_s.flg_concl_res,sl_ana_s.casas_dec Casas_dec_1,sl_ana_s.flg_1_para_x,sl_ana_s.ordem,sl_ana_s.t_result Tipo1,sl_ana_s.flg_inibe_descr flg_inibe_descr_s," & _
        "sl_segundo_res.casas_dec Casas_dec_2,sl_segundo_res.t_result Tipo2," & _
        "slv_analises.descr_gr_ana,sgr.descr_sgr_ana," & _
        "TEC.nome nome_tec,TEC.titulo titulo_tec,TEC.cod_gr_util cod_gr_util_tec,TEC.cod_utilizador cod_utilizador_tec,TEC.telefone telef_tec, TEC.email email_tec,TEC.permissao_res permissao_res, MED.nome nome_med," & _
        "MED.titulo titulo_med,MED.cod_gr_util cod_gr_util_med,MED.cod_utilizador cod_utilizador_med,MED.telefone telef_med, MED.email email_med, sgr.flg_imprime_volume, " & _
        "sl_ana_s.ana_exterior, sl_ana_s.ana_acreditada,slv_analises.cod_produto,pro.descr_produto,pro.ordem ord_produto,slv_analises.ord_grupo, RES.seq_utente, slv_analises.cod_local local_exec, " & _
        "sl_ana_s.flg_pos_res,sl_ana_s.DESCR_RELATORIO, sl_ana_S.flg_nao_avaliar, RES.dt_cri, RES.user_cri, slv_analises.ordem_ars " & _
        ",slv_analises.gr_impr, sl_gr_impr.descr_gr_impr, sl_gr_impr.ordem_imp ordem_gr_imp, sgr.ordem_imp ordem_sgr_impr, sl_ana_c.flg_inibe_descr flg_inibe_descr_c, sl_perfis.flg_inibe_descr flg_inibe_descr_p,sl_ana_S.negrito,  " & _
        "med.funcao funcao_med, tec.funcao funcao_tec ,sl_ana_c.ana_acreditada ana_c_acreditada,sl_perfis.ana_acreditada ana_p_acreditada, colh.dt_val_canc_reutil, " & _
        "colh.hr_val_canc_reutil, colh.user_val_canc_reutil, colh.motivo_n_reutil, colh.obs_reutil, colh.seq_colheita, colh.flg_reutil, (select n_prescricao from sl_requis where n_req = res.n_req) prescricao"
        
    sql = sql & " FROM " & tabRes & " LEFT OUTER JOIN sl_req_colheitas colh ON res.n_req = colh.n_req AND colh.seq_colheita in (select a.seq_colheita from sl_req_colheitas a where a.n_req = res.n_req and a.flg_reutil = 1 and a.estado_reutil = 1) LEFT OUTER JOIN sl_perfis ON RES.cod_perfil=sl_perfis.cod_perfis "
    sql = sql & " LEFT OUTER JOIN sl_ana_c ON RES.cod_ana_c=sl_ana_c.cod_ana_c "
    If gSGBD = gPostGres Then
        sql = sql & " LEFT OUTER JOIN sl_idutilizador TEC ON to_number(RES.user_tec_val,'999999999')=TEC.cod_utilizador "
        sql = sql & " LEFT OUTER JOIN sl_idutilizador MED ON to_number(RES.user_val,'999999999')=MED.cod_utilizador "
    Else
        sql = sql & " LEFT OUTER JOIN sl_idutilizador TEC ON RES.user_tec_val=TEC.cod_utilizador "
        sql = sql & " LEFT OUTER JOIN sl_idutilizador MED ON RES.user_val=MED.cod_utilizador "
    End If

    sql = sql & " LEFT OUTER JOIN sl_segundo_res  ON RES.cod_ana_s=sl_segundo_res.cod_ana_s "
    sql = sql & " LEFT OUTER JOIN sl_ana_s on res.cod_ana_s = sl_ana_s.cod_ana_s, slv_analises "
    sql = sql & " LEFT OUTER JOIN sl_sgr_ana sgr ON slv_analises.cod_sgr_ana = sgr.cod_sgr_ana "
    sql = sql & " LEFT OUTER JOIN sl_produto pro ON slv_analises.cod_produto = pro.cod_produto "
    sql = sql & " LEFT OUTER JOIN sl_gr_impr  ON slv_analises.gr_impr = sl_gr_impr.cod_gr_impr "
    sql = sql & " WHERE (slv_analises.seq_sinonimo is null OR slv_analises.seq_sinonimo = -1) AND slv_analises.cod_ana=RES.cod_agrup "
    If imprRelARS = True Then
        sql = sql & " AND slv_analises.ordem_ars IS NOT NULL "
    End If
    
    ' FGONCALVES
    ' APENAS IMPRIME REQUISI��ES ASSINADASS
    If gAssinaturaActivo = mediSim And ImprComValTec = False Then
        If gApenasImprimeAssinadas = mediSim And gImprimirDestino = 1 Then
            sql = sql & " AND RES.FLG_ASSINADO = 1 "
        End If
    ElseIf gAssinaturaActivo <> mediSim Then
        If gApenasImprimeAssinadas = mediSim And gImprimirDestino = 1 Then
            sqlAssinatura = "SELECT 1 FROM sl_tbf_t_destino WHERE flg_usa_assinatura = 1 AND cod_t_dest IN (SELECT cod_destino FROM sl_requis WHERE n_req = " & NumReq & ")"
            rsAssinatura.CursorType = adOpenStatic
            rsAssinatura.CursorLocation = adUseServer
            If gModoDebug = mediSim Then BG_LogFile_Erros sqlAssinatura
            rsAssinatura.Open sqlAssinatura, gConexao
            If rsAssinatura.RecordCount > 0 Then
                sql = sql & " AND RES.FLG_ASSINADO = 1 "
            End If
            rsAssinatura.Close
            Set rsAssinatura = Nothing
        End If
    End If
    
    If gUsaInterferencia = mediSim Then
        sql = sql & " AND res.seq_realiza not in (Select seq_realiza from sl_obs_auto where seq_realiza = res.seq_realiza and flg_bloqueio =0 )"
    End If
    'NELSONPSILVA 27.11.2017 CHVNG-8858
    If Trim(GrupoAna) <> "" Then sql = sql & " AND slv_analises.cod_gr_ana=" & BL_TrataStringParaBD(GrupoAna)
    If Trim(SubGrupoAna) <> "" Then sql = sql + " AND slv_analises.cod_sgr_ana=" & BL_TrataStringParaBD(SubGrupoAna)
    '
    sql = sql & IIf(Multiplo = False And Trim(ListaAnalises) <> "", " AND slv_analises.seq_ana IN (" & ListaAnalises & ") ", "") & _
        " AND RES.flg_estado IN " & estadosAna & _
        " AND RES.n_req=" & NumReq
    If gImprimeRelOrderGrImp <> mediSim Then
        sql = sql & " ORDER BY slv_analises.cod_gr_ana,sgr.ordem_imp,RES.ord_ana,RES.cod_agrup,RES.cod_perfil,RES.ord_ana_perf,RES.cod_ana_c,RES.ord_ana_compl,RES.cod_ana_s,RES.dt_val DESC "
    Else
        sql = sql & " ORDER BY slv_analises.gr_impr,sgr.ordem_imp,RES.ord_ana,RES.cod_agrup,RES.cod_perfil,RES.ord_ana_perf,RES.cod_ana_c,RES.ord_ana_compl,RES.cod_ana_s,RES.dt_val DESC "
    End If
            
    
    IR_ConstroiSqlRes = sql

End Function

' -----------------------------------------------------------------------------------------------

' PARA CADA PRODUTO VERIFICA ESPECIFICACAO E OBSERVACAO DA ESPECIFICACAO

' -----------------------------------------------------------------------------------------------
Private Function IR_RetornaEspecif(ByVal n_req As String, ByVal cod_prod As String) As Boolean
    On Error GoTo TrataErro
    Dim sSql As String
    Dim RsEspecif As New ADODB.recordset
    If gSGBD = gOracle Then
        sSql = "SELECT x2.descr_especif, x1.obs_especif FROM sl_req_prod x1, sl_especif x2 WHERE x1.n_req = " & n_req
        sSql = sSql & " AND x1.cod_prod = " & BL_TrataStringParaBD(cod_prod)
        sSql = sSql & " AND x1.cod_especif = x2.cod_especif (+) "
    Else
        sSql = "SELECT x2.descr_especif, x1.obs_especif FROM sl_req_prod x1 LEFT OUTER JOIN sl_especif x2 ON x1.cod_especif = x2.cod_especif  WHERE x1.n_req = " & n_req
        sSql = sSql & " AND x1.cod_prod = " & BL_TrataStringParaBD(cod_prod)
    End If
    RsEspecif.CursorLocation = adUseServer
    RsEspecif.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    RsEspecif.Open sSql, gConexao
    If RsEspecif.RecordCount = 1 Then
        If BL_HandleNull(RsEspecif!descr_especif, "") <> "" Then
            descr_especif = BL_HandleNull(RsEspecif!descr_especif, "")
            obs_especif = BL_HandleNull(RsEspecif!obs_especif, "")
        Else
            descr_especif = ""
            obs_especif = ""
        End If
    Else
        descr_especif = ""
        obs_especif = ""
    End If
    RsEspecif.Close
    Set RsEspecif = Nothing
    
Exit Function
TrataErro:
    IR_RetornaEspecif = False
    BG_LogFile_Erros "IR_RetornaEspecif: " & Err.Number & " - " & Err.Description & " " & sSql, "IR", "IR_RetornaEspecif"
End Function

' ---------------------------------------------------------------------------------------------------------------------------------------------------

' RETORNA O UTILIZADOR QUE ASSINOU REQUISI��O

' ---------------------------------------------------------------------------------------------------------------------------------------------------
Private Function IR_RetornaAssinaturaReq(n_req As String) As String
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsAssin As New ADODB.recordset
    sSql = "SELECT * FROM sl_req_assinatura WHERE n_Req = " & n_req
    rsAssin.CursorLocation = adUseServer
    rsAssin.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAssin.Open sSql, gConexao
    If rsAssin.RecordCount >= 1 Then
        IR_RetornaAssinaturaReq = BL_HandleNull(rsAssin!user_cri, "")
    Else
        rsAssin.Close
        sSql = "SELECT user_assinado FROM sl_Realiza WHERE n_Req = " & n_req & " AND flg_assinado = 1 "
        rsAssin.CursorLocation = adUseServer
        rsAssin.CursorType = adOpenStatic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsAssin.Open sSql, gConexao
        If rsAssin.RecordCount >= 1 Then
            IR_RetornaAssinaturaReq = BL_HandleNull(rsAssin!user_assinado, "")
        Else
            IR_RetornaAssinaturaReq = ""
        End If
    End If
    rsAssin.Close
    Set rsAssin = Nothing
Exit Function
TrataErro:
    IR_RetornaAssinaturaReq = ""
    BG_LogFile_Erros "IR_RetornaAssinaturaReq: " & Err.Number & " - " & Err.Description & " " & sSql, "IR", "IR_RetornaAssinaturaReq", True
End Function

Private Sub IR_ImprimeAnexos(n_req As String)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim ficheiro As String
    Dim rsAnexos As New ADODB.recordset
    sSql = "SELECT * FROM sl_anexos WHERE seq_realiza IN (Select seq_realiza FROM Sl_realiza WHERE n_req = " & n_req & ")"
    rsAnexos.CursorLocation = adUseServer
    rsAnexos.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnexos.Open sSql, gConexao
    If rsAnexos.RecordCount >= 0 Then
        While Not rsAnexos.EOF
            ficheiro = ""
            ficheiro = RB_RetiraFicheiroBD(BL_HandleNull(rsAnexos!seq_anexo, -1))
            If ficheiro = "" Then
                GoTo TrataErro
            Else
                BL_ShellExecute ficheiro, "Print"
            End If
            rsAnexos.MoveNext
        Wend
        rsAnexos.Close
        Set rsAnexos = Nothing
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "IR_ImprimeAnexos: " & Err.Number & " - " & Err.Description & " " & sSql, "IR", "IR_ImprimeAnexos", False
End Sub

Private Function IR_RetornaPrecoRequis(n_req As String) As Double
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsRec As New ADODB.recordset
    sSql = "SELECT sum(total_pagar) total_pagar FROM sl_recibos WHERE n_req = " & n_req
    rsRec.CursorLocation = adUseServer
    rsRec.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsRec.Open sSql, gConexao
    If rsRec.RecordCount = 1 Then
        IR_RetornaPrecoRequis = BL_HandleNull(rsRec!total_pagar, 0)
    Else
        IR_RetornaPrecoRequis = 0
    End If
    rsRec.Close
    Set rsRec = Nothing
Exit Function
TrataErro:
    IR_RetornaPrecoRequis = 0
    BG_LogFile_Erros "IR_RetornaPrecoRequis: " & Err.Number & " - " & Err.Description & " " & sSql, "IR", "IR_RetornaPrecoRequis"
    Exit Function
    Resume Next
End Function

Function IR_QueryRegFormulas(NumReq As String) As String
    Dim sql As String
    
    sql = "SELECT sl_requis.cod_efr,sl_requis.dt_chega,sl_requis.hr_chega,sl_requis.n_epis,sl_requis.cod_proven,"
    sql = sql & " sl_requis.obs_proven,sl_requis.dt_imp,sl_requis.hr_imp,sl_requis.user_imp,sl_requis.dt_imp2,"
    sql = sql & " sl_requis.hr_imp2,sl_requis.user_imp2,sl_requis.ficha,"
    sql = sql & "sl_identif.seq_utente,sl_identif.n_proc_1,sl_identif.n_proc_2,sl_identif.sexo_ute,sl_identif.utente,"
    sql = sql & " sl_identif.t_utente,sl_identif.nome_ute,sl_identif.dt_nasc_ute,sl_identif.descr_mor_ute, "
    sql = sql & " sl_requis.morada morada_req, sl_requis.cod_postal cod_postal_req, sl_identif.cod_postal_ute,"
    sql = sql & "sl_tbf_t_sit.descr_t_sit,sl_proven.descr_proven,sl_tbf_sexo.descr_sexo,sl_requis.obs_proven, "
    sql = sql & " sl_medicos.nome_med, sl_medicos.servico, sl_medicos.csaude, sl_requis.dt_colheita, sl_requis.hr_colheita,"
    sql = sql & " SL_REQUIS.COD_SALA,sl_identif.n_benef_ute nr_benef, sl_requis.n_benef nr_benef_req, sl_requis.cod_sala, "
    sql = sql & " sl_requis.n_req_assoc, sl_requis.cod_destino, sl_requis.cod_isencao, sl_requis.cod_local local_requis,"
    sql = sql & " sl_requis.descr_medico, tipo_urgencia, sl_identif.num_ext, sl_identif.dt_impr_cartao,"
    sql = sql & " sl_identif.cod_genero, sl_profis.descr_profis, sl_requis.dt_conclusao, sl_requis.dt_pretend,"
    sql = sql & " sl_requis.dt_fecho,sl_cod_salas.descr_sala, sl_identif.cod_raca, sl_identif.nome_dono, "
    sql = sql & " sl_genero.descr_genero, sl_tbf_t_destino.descr_t_dest, sl_tbf_t_destino.flg_fax, "
    sql = sql & " sl_requis.cod_local_entrega, sl_requis.user_colheita, sl_requis.obs_req, "
    sql = sql & " sl_requis_aguas.cod_tipo_amostra, sl_requis_aguas.cod_origem_amostra, sl_requis_aguas.cod_ponto_colheita, "
    sql = sql & " sl_requis_aguas.local_colheita "
    sql = sql & " FROM sl_requis LEFT OUTER JOIN sl_tbf_t_destino ON sl_Requis.cod_destino = sl_tbf_t_destino.cod_t_dest "
    sql = sql & " LEFT OUTER JOIN sl_tbf_t_sit ON sl_requis.t_sit=sl_tbf_t_sit.cod_t_sit "
    sql = sql & " LEFT OUTER JOIN sl_proven ON sl_requis.cod_proven=sl_proven.cod_proven "
    sql = sql & " LEFT OUTER JOIN sl_medicos ON sl_requis.cod_med=sl_medicos.cod_med "
    sql = sql & " LEFT OUTER JOIN sl_cod_salas ON sl_requis.cod_sala = sl_cod_salas.cod_sala  "
    sql = sql & " LEFT OUTER JOIN sl_requis_aguas ON sl_requis.n_req = sl_requis_aguas.n_req "
    sql = sql & ",sl_identif "
    sql = sql & " LEFT OUTER JOIN sl_profis ON sl_identif.cod_profis_ute = sl_profis.cod_profis "
    sql = sql & " LEFT OUTER JOIN sl_tbf_sexo ON sl_identif.sexo_ute=sl_tbf_sexo.cod_sexo, sl_genero "
    sql = sql & " WHERE sl_requis.n_req=" & Trim(NumReq) & " AND sl_requis.seq_utente=sl_identif.seq_utente "
    sql = sql & " AND sl_requis.estado_Req NOT IN (" & BL_TrataStringParaBD(gEstadoReqBloqueada) & "," & BL_TrataStringParaBD(gEstadoReqCancelada) & ")"
    sql = sql & " AND sl_identif.cod_genero = sl_genero.cod_genero "
    
    IR_QueryRegFormulas = sql
    
End Function


Private Sub IR_SeleccionaDadosEFR(ByVal codEfr As Long, ByRef formulas As dadosFormula)
    On Error GoTo TrataErro
    Dim sql As String
    Dim RsDescrEFR As ADODB.recordset
    
    sql = "SELECT * FROM sl_efr WHERE cod_efr=" & codEfr
    Set RsDescrEFR = New ADODB.recordset
    RsDescrEFR.CursorLocation = adUseServer
    RsDescrEFR.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sql
    RsDescrEFR.Open sql, gConexao
    If RsDescrEFR.RecordCount > 0 Then
        formulas.DescrEFR = BL_HandleNull(RsDescrEFR!descr_efr, "")
        formulas.MoradaEFR = BL_HandleNull(RsDescrEFR!mor_efr, "")
        formulas.codPostalEFR = BL_HandleNull(RsDescrEFR!cod_postal_efr, "")
    Else
        formulas.DescrEFR = ""
        formulas.MoradaEFR = ""
        formulas.codPostalEFR = ""
    End If
    RsDescrEFR.Close
    Set RsDescrEFR = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "IR_SeleccionaDadosEFR: " & Err.Number & " - " & Err.Description & " " & sql, "IR", "IR_SeleccionaDadosEFR"
    Exit Sub
    Resume Next
End Sub


Private Sub IR_Insere_SL_CR_LR_VF(formulas As dadosFormula, NumReq As String)
    Dim sSql As String
    Dim grupoAnaP As String
    grupoAnaP = BL_DevolveGrupoRequis(CLng(NumReq))
    
    'RGONCALVES 10.12.2015 CHSJ-2511 adicionado ana_acreditada_cod_agrup e ana_nao_acreditada_cod_agrup
    sSql = "INSERT INTO SL_CR_LRVF ( Produtos, Comentario, Flagcoment, RespTecnicos, RespMedicos, AnalisesPend,"
    sSql = sSql & " FlagPendente, quem_imprime, cat_quem_imprime,id_empresa,ana_acreditada,ana_exterior, ana_nao_acreditada, "
    sSql = sSql & " nome_computador, num_sessao, n_req, user_assinatura,dt_pretend, dt_fecho, tubos_atrasados,cod_sala, descr_sala, total_pagar,"
    sSql = sSql & " num_utente, processo, episodio, nomeutente, dt_nasc_ute, sexo_ute, situacao, descr_proven, dt_chegada, dt_emissao, hr_emissao,"
    sSql = sSql & " dt_seg_via, hr_seg_via, morada, cod_postal, validacao_tec, descr_efr, obs_proven, descr_med,idade, ficha,local_res, gr_ana, dt_colheita, "
    sSql = sSql & " nr_benef_ute, isencao,destino, n_req_assoc, hr_chega, local_req, obs_ana_req, obs_auto_bloq, ver_tab_anexo, dt_validacao, hr_validacao, "
    sSql = sSql & " num_ext, dt_impr_cartao, descr_profis, dt_conclusao,  descr_genero, descr_raca, nome_dono, analisespendtubo,flagpendentetubo, "
    sSql = sSql & " cod_local_entrega, descr_local_entrega, obs_req, cod_tipo_amostra, descr_tipo_amostra, cod_origem_amostra, descr_origem_amostra, "
    sSql = sSql & " cod_ponto_colheita, descr_ponto_colheita, local_colheita, responsavel_colheita,ana_acreditada_cod_agrup,ana_nao_acreditada_cod_agrup)VALUES ("
    
    sSql = sSql & formulas.Produtos & "," & formulas.Comentario & "," & formulas.FlagComent & ","
    sSql = sSql & formulas.respTecnicos & "," & formulas.RespMedicos & ","
    If (grupoAnaP <> CStr(gCodGrupoMicrobiologia) And grupoAnaP <> CStr(gCodGrupoMicobacteriologia)) Or gImprimeMicroAnaPendentes = mediSim Then
        If gLAB = "CHVNG" And grupoAnaP = "7" Then
            sSql = sSql & " NULL,"
        Else
            sSql = sSql & formulas.AnalisesPendentes & ","
        End If
    Else
        sSql = sSql & " NULL,"
    End If
    sSql = sSql & IIf(formulas.AnalisesPendentes = "Null", "Null", "'S'")
    sSql = sSql & ",'" & gTituloUtilizador & " " & gNomeUtilizador & "', '" & formulas.aux_coment_assinatura & "','" & gCodLocal & "',"
    sSql = sSql & formulas.totalAcreditadas & ", " & formulas.totalExterior & ", " & formulas.totalNaoAcreditadas & "," & BL_TrataStringParaBD(gComputador)
    sSql = sSql & ", " & gNumeroSessao & "," & NumReq & ", " & BL_TrataStringParaBD(Trim(formulas.UserAssinatura)) & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.DtPretend) & ", " & BL_TrataStringParaBD(formulas.DtFecho) & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.tubosAtrasados) & "," & BL_HandleNull(formulas.sala.cod_sala, "null")
    sSql = sSql & ", " & BL_TrataStringParaBD(formulas.sala.descr_sala) & "," & Replace(formulas.totalPagar, ",", ".") & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.TipoUtente & "/" & formulas.NumUtente) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.NumProcUtente) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.NumEpisodio) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.NomeUtente) & ","
    sSql = sSql & BL_TrataDataParaBD("" & formulas.DtNascUtente) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.SexoUtente) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.SitReq) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.ProvReq) & ","
    sSql = sSql & BL_TrataDataParaBD("" & formulas.DataChegada) & ","
    sSql = sSql & BL_TrataDataParaBD("" & formulas.DtEmissao) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.HrEmissao) & ","
    
    'Verifica se foi a 1�vez que actualizou a 2�via!
    If formulas.ExisteSegVia = True Then
        sSql = sSql & BL_TrataDataParaBD("" & formulas.DtSVia) & ","
        sSql = sSql & BL_TrataStringParaBD("" & formulas.HrSVia) & ","
    Else
        sSql = sSql & "Null,NULL,"
    End If
    
    If gImprimeMoradaRes = mediSim Then
        'If gTipoInstituicao = gTipoInstituicaoVet Then
        '    sSql = sSql & Replace(BL_TrataStringParaBD("" & formulas.MoradaEFR), vbCrLf, " - ") & ","
        '    sSql = sSql & BL_TrataStringParaBD("" & formulas.codPostalEFR) & ","
       ' Else
            sSql = sSql & Replace(BL_TrataStringParaBD("" & formulas.MoradaUtente), vbCrLf, " - ") & ","
            sSql = sSql & BL_TrataStringParaBD("" & formulas.CodPostalUtente) & ","
        'End If
    Else
        sSql = sSql & "NULL, NULL,"
    End If
    
    If gImprimirComValTec = mediSim And Mid(formulas.RespMedicos, 1, 2) = "'," Then
       sSql = sSql & BL_TrataStringParaBD("" & gTextoValidacaoTecnicaBoletim) & ","
    Else
        sSql = sSql & "Null,"
    End If
    
    sSql = sSql & BL_TrataStringParaBD("" & formulas.DescrEFR) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.ObsProven) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.NomeMed) & ","
    sSql = sSql & BL_TrataStringParaBD("" & BG_CalculaIdade(CDate(formulas.DtNascUtente), CDate(formulas.DataChegada))) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.ficha) & ","
    sSql = sSql & BL_TrataStringParaBD("" & gCodLocal) & ","
    
    If InStr(1, BL_TrataStringParaBD("" & BL_DevolveGrupoRequis(CLng(NumReq))), ";") = 0 Then
        sSql = sSql & BL_TrataStringParaBD("" & BL_DevolveGrupoRequis(CLng(NumReq))) & ","
    Else
        sSql = sSql & "NULL,"
    End If
    sSql = sSql & BL_TrataDataParaBD("" & formulas.DataColheita) & ","
    sSql = sSql & BL_TrataStringParaBD("" & Trim(Replace(formulas.nrBenef, vbCrLf, ""))) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.isencao) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.destino) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.NReqAssoc) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.HoraChegada) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.localRequis) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.ObsAnaReq) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.ObsAutoBloq) & ","
    If formulas.VerTabelaAnexo = True Then
        sSql = sSql & "1,"
    Else
        sSql = sSql & "0,"
    End If
    sSql = sSql & BL_TrataDataParaBD("" & formulas.DtValidacao) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.HrValidacao) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.numExt) & ","
    sSql = sSql & BL_TrataDataParaBD("" & formulas.dtImprCartao) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.descrProfis) & ","
    sSql = sSql & BL_TrataDataParaBD("" & formulas.DtConclusao) & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.descr_genero) & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.descr_raca) & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.nome_dono) & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.AnalisesPendentesTubo) & ","
    sSql = sSql & IIf(formulas.AnalisesPendentesTubo = "", "0", "1") & ","
    sSql = sSql & BL_HandleNull(formulas.codLocalEntrega, "NULL") & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.descrLocalEntrega) & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.obs_req) & ","
    sSql = sSql & BL_HandleNull(formulas.cod_tipo_amostra, "NULL") & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.descr_tipo_amostra) & ","
    sSql = sSql & BL_HandleNull(formulas.cod_origem_amostra, "NULL") & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.descr_origem_amostra) & ","
    sSql = sSql & BL_HandleNull(formulas.cod_ponto_colheita, "NULL") & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.descr_ponto_colheita) & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.local_colheita) & ","
    sSql = sSql & BL_TrataStringParaBD(formulas.responsavel_colheita) & ","
    'RGONCALVES 10.12.2015 CHSJ-2511
    sSql = sSql & BL_TrataStringParaBD("" & formulas.totalAcreditadasCodAgrup) & ","
    sSql = sSql & BL_TrataStringParaBD("" & formulas.totalNaoAcreditadasCodAgrup) & ")"
    '
    
    BG_ExecutaQuery_ADO sSql

End Sub

 Private Function IR_RetornaSqlQueryReport(NumReq As String, imprRelARS As Boolean, Optional ImprimeListaExterior As Boolean) As String
    Dim sSql As String
    If gTipoInstituicao = "PRIVADA" Or gTipoInstituicao = gTipoInstituicaoVet Or gTipoInstituicao = gTipoInstituicaoAguas Then
        sSql = "SELECT GR_EMPR_INST.NOME_EMPR, GR_EMPR_INST.TELEFONES, GR_EMPR_INST.N_FAX, GR_EMPR_INST.LOCALIDADE, GR_EMPR_INST.COD_POSTAL, GR_EMPR_INST.N_SEQ_POSTAL, GR_EMPR_INST.MORADA,"
        sSql = sSql & "SL_CR_LR.DATA, SL_CR_LR.GRUPO, SL_CR_LR.SUBGRUPO, SL_CR_LR.PERFIL, SL_CR_LR.COMPLEXA, SL_CR_LR.SIMPLES, SL_CR_LR.RESNUM1UNID1, SL_CR_LR.RESNUM1UNID2, SL_CR_LR.UNID1RESNUM1, SL_CR_LR.UNID2RESNUM1, SL_CR_LR.RESALF1, SL_CR_LR.VALREF1, SL_CR_LR.ZONAREF1, SL_CR_LR.RESNUM2UNID1, SL_CR_LR.RESNUM2UNID2, SL_CR_LR.UNID1RESNUM2, SL_CR_LR.UNID2RESNUM2, SL_CR_LR.RESALF2, SL_CR_LR.VALREF2, SL_CR_LR.ZONAREF2, SL_CR_LR.OBSERVANALISE, SL_CR_LR.OBSERVRESULTADO, SL_CR_LR.COMAUTOMATICO, SL_CR_LR.FLAGCOMAUT, SL_CR_LR.FLAGRESALF2, SL_CR_LR.FLAGOBSERV, SL_CR_LR.FLAGRESULT, SL_CR_LR.FLAGRESANORMAL1, SL_CR_LR.FLAGRESANORMAL2, SL_CR_LR.FLAGCUTOFF, SL_CR_LR.FLAGRTFANALISE, SL_CR_LR.TAMRESALF1, SL_CR_LR.TAMRESALF2,"
        sSql = sSql & "SL_CR_LRVF.PRODUTOS, SL_CR_LRVF.COMENTARIO, SL_CR_LRVF.FLAGCOMENT, SL_CR_LRVF.RESPTECNICOS, "
        sSql = sSql & "SL_CR_LRVF.RESPMEDICOS, SL_CR_LR.RES_ANT1, SL_CR_LR.DT_RES_ANT1, "
        sSql = sSql & "SL_CR_LR.RES_ANT2, SL_CR_LR.DT_RES_ANT2, SL_CR_LR.RES_ANT3, SL_CR_LR.DT_RES_ANT3, "
        sSql = sSql & "SL_CR_LRVF.QUEM_IMPRIME,SL_CR_LR.COD_PRODUTO,SL_CR_LR.DESCR_PRODUTO,SL_CR_LR.ORDEM_PRODUTO, "
        sSql = sSql & "SL_CR_LR.COD_SALA, SL_COD_SALAS.ABREV, SL_COD_SALAS.DESCR_SALA, SL_COD_SALAS.FLG_COLHEITA, "
        sSql = sSql & " SL_IDUTILIZADOR.ASSINATURA,SL_CR_LR.cod_Ana_c, sl_cr_lr.n_Req FROM GR_EMPR_INST GR_EMPR_INST,"
        sSql = sSql & "SL_CR_LR LEFT OUTER JOIN SL_COD_SALAS ON SL_CR_LR.COD_SALA = SL_COD_SALAS.COD_SALA,"
        If gSGBD = gPostGres Then
            sSql = sSql & "SL_CR_LRVF LEFT OUTER JOIN SL_IDUTILIZADOR  ON to_number(SL_CR_LRVF.user_assinatura,'999999999') = SL_IDUTILIZADOR.cod_utilizador "
        Else
            sSql = sSql & "SL_CR_LRVF LEFT OUTER JOIN SL_IDUTILIZADOR  ON SL_CR_LRVF.user_assinatura = SL_IDUTILIZADOR.cod_utilizador "
        End If
        sSql = sSql & "WHERE GR_EMPR_INST.EMPRESA_ID =sl_cr_lrvf.local_req and sl_cr_lr.nome_computador = " & BL_TrataStringParaBD(gComputador) & " and sl_cr_lrvf.nome_computador = " & BL_TrataStringParaBD(gComputador)
        sSql = sSql & " AND  sl_cr_lr.n_req =  sl_cr_lrvf.n_req "
        If ImprimeListaExterior = False Then
            sSql = sSql & " AND sl_cr_lr.n_req  = " & NumReq
        End If
        sSql = sSql & " AND  sl_cr_lr.num_sessao = " & gNumeroSessao & " and sl_cr_lrvf.num_sessao = " & gNumeroSessao
        If imprRelARS = True Then
            sSql = sSql & " ORDER BY SL_CR_LR.ordem_ars"
        ElseIf gImprimeRelOrderGrImp = mediSim Then
            sSql = sSql & " ORDER BY SL_CR_LR.ordem_gr_impr, SL_CR_LR.ordem_imp"
        Else
            sSql = sSql & " ORDER BY SL_CR_LR.ordem_imp"
        End If
    ElseIf gTipoInstituicao = "HOSPITALAR" Then
        sSql = "SELECT GR_EMPR_INST.NOME_EMPR, GR_EMPR_INST.TELEFONES, GR_EMPR_INST.N_FAX, GR_EMPR_INST.LOCALIDADE, GR_EMPR_INST.COD_POSTAL, GR_EMPR_INST.N_SEQ_POSTAL, GR_EMPR_INST.MORADA,"
        sSql = sSql & "SL_CR_LR.DATA, SL_CR_LR.GRUPO, SL_CR_LR.SUBGRUPO, SL_CR_LR.PERFIL, SL_CR_LR.COMPLEXA, SL_CR_LR.SIMPLES, SL_CR_LR.RESNUM1UNID1, SL_CR_LR.RESNUM1UNID2, SL_CR_LR.UNID1RESNUM1, SL_CR_LR.UNID2RESNUM1, SL_CR_LR.RESALF1, SL_CR_LR.VALREF1, SL_CR_LR.ZONAREF1, SL_CR_LR.RESNUM2UNID1, SL_CR_LR.RESNUM2UNID2, SL_CR_LR.UNID1RESNUM2, SL_CR_LR.UNID2RESNUM2, SL_CR_LR.RESALF2, SL_CR_LR.VALREF2, SL_CR_LR.ZONAREF2, SL_CR_LR.OBSERVANALISE, SL_CR_LR.OBSERVRESULTADO, SL_CR_LR.COMAUTOMATICO, SL_CR_LR.FLAGCOMAUT, SL_CR_LR.FLAGRESALF2, SL_CR_LR.FLAGOBSERV, SL_CR_LR.FLAGRESULT, SL_CR_LR.FLAGRESANORMAL1, SL_CR_LR.FLAGRESANORMAL2, SL_CR_LR.FLAGCUTOFF, SL_CR_LR.FLAGRTFANALISE, SL_CR_LR.TAMRESALF1, SL_CR_LR.TAMRESALF2,"
        sSql = sSql & "SL_CR_LRVF.PRODUTOS, SL_CR_LRVF.COMENTARIO, SL_CR_LRVF.FLAGCOMENT, SL_CR_LRVF.RESPTECNICOS, "
        sSql = sSql & "SL_CR_LRVF.RESPMEDICOS, SL_CR_LR.RES_ANT1, SL_CR_LR.DT_RES_ANT1, "
        sSql = sSql & "SL_CR_LR.RES_ANT2, SL_CR_LR.DT_RES_ANT2, SL_CR_LR.RES_ANT3, SL_CR_LR.DT_RES_ANT3, "
        sSql = sSql & "SL_CR_LRVF.QUEM_IMPRIME,SL_CR_LR.COD_PRODUTO,SL_CR_LR.DESCR_PRODUTO,SL_CR_LR.ORDEM_PRODUTO,SL_CR_LR.cod_Ana_c, sl_cr_lr.n_Req,SL_IDUTILIZADOR.ASSINATURA "
        sSql = sSql & "FROM GR_EMPR_INST GR_EMPR_INST,"
        sSql = sSql & "SL_CR_LR ,"
        sSql = sSql & "SL_CR_LRVF, SL_IDUTILIZADOR   "
        sSql = sSql & "WHERE GR_EMPR_INST.ESTADO='S' and sl_cr_lr.nome_computador = " & BL_TrataStringParaBD(gComputador) & " and sl_cr_lrvf.nome_computador = " & BL_TrataStringParaBD(gComputador)
        sSql = sSql & " AND  sl_cr_lr.n_req =  sl_cr_lrvf.n_req "
        sSql = sSql & " AND sl_cr_lr.n_req  = " & NumReq
        sSql = sSql & " AND  sl_cr_lr.num_sessao = " & gNumeroSessao & " and sl_cr_lrvf.num_sessao = " & gNumeroSessao
        sSql = sSql & " AND SL_CR_LRVF.user_assinatura = SL_IDUTILIZADOR.cod_utilizador(+) "
        
        If imprRelARS = True Then
            sSql = sSql & " ORDER BY SL_CR_LR.ordem_ars"
        ElseIf gImprimeRelOrderGrImp = mediSim Then
            sSql = sSql & " ORDER BY SL_CR_LR.ordem_gr_impr, SL_CR_LR.ordem_imp"
        Else
            sSql = sSql & " ORDER BY SL_CR_LR.ordem_imp"
        End If
    End If
    IR_RetornaSqlQueryReport = sSql
    
End Function

Public Function IR_ImprimeResultadosCrystalLista()
    Dim formulas As dadosFormula
    IR_ImprimeResultadosCrystal formulas, "", False, 0, mediComboValorNull, False, True
End Function
    

Private Sub IR_PreencheValMedAntib(ByVal seq_realiza As Long, ByVal cod_micro As String, ByRef codRespMedicos As String, _
                                   ByRef RespMedicos As String, ByRef RespValMedM As String)
    On Error GoTo TrataErro
    Dim sSql As String
    Dim rsTSQ As ADODB.recordset
    Dim Titulo As String
    sSql = "SELECT * FROM sl_idutilizador WHERE cod_utilizador in (SELECT user_val FROM sl_res_tsq WHERE seq_realiza = " & seq_realiza
    sSql = sSql & " AND cod_micro = " & BL_TrataStringParaBD(cod_micro) & ")"
    
    Set rsTSQ = New ADODB.recordset
    rsTSQ.CursorLocation = adUseServer
    rsTSQ.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsTSQ.Open sSql, gConexao
    If rsTSQ.RecordCount > 0 Then
        While Not rsTSQ.EOF
            If InStr(1, codRespMedicos, "-" & BL_HandleNull(rsTSQ!cod_utilizador, "") & "-") = 0 Then
                
                If BL_HandleNull(rsTSQ!Titulo, "") = "" Then
                    
                    Select Case rsTSQ!cod_gr_util
                        
                        Case gGrupoAdministradores
                            Titulo = "Administrador"
                        
                        Case gGrupoTecnicos
                            Titulo = "T�cnico"
                        
                        Case gGrupoChefeSecretariado
                            Titulo = "Chefe Secretariado"
                        
                        Case gGrupoSecretariado
                            RespMedicos = RespMedicos & "Secretariado"
                        
                        Case gGrupoMedicos
                            Titulo = "M�dico"
                            
                        Case gGrupoEnfermeiros
                            Titulo = "Enfermeiro"
                        
                        Case gGrupoFarmaceuticos
                            Titulo = "Farmac�utico"
                    
                    End Select
                Else
                    Titulo = BL_HandleNull(rsTSQ!Titulo, "")
                End If
                
                RespMedicos = RespMedicos & BL_HandleNull(rsTSQ!nome, "") & " - " & Titulo
                RespValMedM = RespValMedM & BL_HandleNull(rsTSQ!nome, "") & " - " & Titulo
                codRespMedicos = codRespMedicos & "-" & BL_HandleNull(rsTSQ!cod_utilizador, "") & "-" & ","
            ElseIf RespValMedM = "" Then
                RespValMedM = BL_HandleNull(rsTSQ!nome, "")
                RespValMedM = RespValMedM & " - " & IIf(BL_HandleNull(rsTSQ!Titulo, "") = "", "", rsTSQ!Titulo) & vbNewLine
            End If
            
            rsTSQ.MoveNext
        Wend
    End If
    rsTSQ.Close
    Set rsTSQ = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "IR_PreencheValMedAntib: " & Err.Number & " - " & Err.Description & " " & sSql, "IR", "IR_PreencheValMedAntib"
    Exit Sub
    Resume Next

End Sub
 'BRUNODSANTOS 05.09.2016 - LRV-388
'Private Function IniciaReport_Email_Preview(descrImp As String) As Boolean
'
'    If gUsaMesmoRelatorioEmail = mediSim Then
'        IniciaReport_Email_Preview = BL_IniciaReport("MapaResultadosSimplesEmail", "Mapa de Resultados", crptToWindow, , , , , , descrImp)
'    Else
'        IniciaReport_Email_Preview = BL_IniciaReport("MapaResultadosSimples", "Mapa de Resultados", crptToWindow, , , , , , descrImp)
'    End If
'
'End Function

'BRUNODSANTOS - SCMVC-1073
Sub IR_MudaEstadoAnaliseEcraCompletas(ImprimeEcraCompletas As Boolean, NumReq As String)
On Error GoTo TrataErro

    If ImprimeEcraCompletas = True Then
        Call BG_ExecutaQuery_ADO("UPDATE sl_realiza SET flg_estado='4' WHERE n_req = " & BL_TrataStringParaBD(NumReq))
    End If

Exit Sub

TrataErro:
    BG_LogFile_Erros "IR_MudaEstadoAnaliseEcraCompletas: " & Err.Number & " - " & Err.Description, "IR", "IR_MudaEstadoAnaliseEcraCompletas"
    Exit Sub
    Resume Next

End Sub

Private Sub IR_PreencheReutilAna(regRes As ADODB.recordset)
On Error GoTo TrataErro
    
    On Error GoTo TrataErro
    
    ReutilAna.n_prescricao = BL_HandleNull(regRes!prescricao, "")
    ReutilAna.dt_val_canc_reutil = BL_HandleNull(regRes!dt_val_canc_reutil, "")
    ReutilAna.hr_val_canc_reutil = BL_HandleNull(regRes!hr_val_canc_reutil, "")
    ReutilAna.user_val_canc_reutil = BL_SelNomeUtil(BL_HandleNull(regRes!user_val_canc_reutil, ""))
    ReutilAna.flg_reutil = BL_HandleNull(regRes!flg_reutil, "")

 Exit Sub
TrataErro:
    BG_LogFile_Erros "PreencheReutilAna: " & Err.Number & " - " & Err.Description, "IR", "IRPreencheReutilAna"
    Exit Sub
    Resume Next

End Sub



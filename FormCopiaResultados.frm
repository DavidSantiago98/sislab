VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FormCopiaResultados 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   10365
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   9435
   Icon            =   "FormCopiaResultados.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10365
   ScaleWidth      =   9435
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton BtCopia 
      Height          =   375
      Left            =   7320
      Picture         =   "FormCopiaResultados.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Efectua a c�pia dos resultados das an�lises selecionadas"
      Top             =   7800
      Width           =   2055
   End
   Begin VB.Frame Frame3 
      Caption         =   " Requisi��o Destino "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   120
      TabIndex        =   4
      Top             =   3960
      Width           =   9255
      Begin VB.TextBox EcUtenteDestino 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   10
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox EcNomeUtenteDestino 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3600
         TabIndex        =   9
         Top             =   240
         Width           =   5535
      End
      Begin VB.TextBox EcReqDestino 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   1695
      End
      Begin MSComctlLib.ListView LvDestino 
         Height          =   3015
         Left            =   120
         TabIndex        =   6
         Top             =   600
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   5318
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   0
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   " Requisi��o Origem "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   9255
      Begin VB.TextBox EcUtenteOrigem 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   8
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox EcNomeUtenteOrigem 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3600
         TabIndex        =   7
         Top             =   240
         Width           =   5535
      End
      Begin VB.TextBox EcReqOrigem 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   1695
      End
      Begin MSComctlLib.ListView LvOrigem 
         Height          =   3135
         Left            =   120
         TabIndex        =   3
         Top             =   600
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   5530
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   0
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   1800
      ScaleHeight     =   315
      ScaleWidth      =   435
      TabIndex        =   0
      Top             =   9000
      Width           =   495
   End
   Begin MSComctlLib.ImageList ImageListIconsOrigem 
      Left            =   240
      Top             =   8880
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormCopiaResultados.frx":685E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormCopiaResultados.frx":D0C0
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageListIconsDestino 
      Left            =   960
      Top             =   9000
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormCopiaResultados.frx":13922
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FormCopiaResultados.frx":1A184
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label LbInformacao 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   12
      Top             =   7800
      Width           =   6975
   End
End
Attribute VB_Name = "FormCopiaResultados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'      .............................
'     .                             .
'    .   Paulo Ferreira 2010.02.01   .
'     .       � 2010 Glintt-HS      .
'      .............................

Option Explicit

Private estado As Integer
Private item As ListItem
Private CampoActivo As Object
Private CampoDeFocus As Object

Private Sub BtCopia_Click()
    
    ExecutaCopia
    
End Sub

Private Sub EcReqDestino_Validate(Cancel As Boolean)
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A procurar registos..."
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    LvDestino.ListItems.Clear
    EcUtenteDestino.Text = Empty
    EcNomeUtenteDestino.Text = Empty
    If (EcReqDestino.Text <> Empty) Then
        PreencheUtente EcReqDestino.Text, EcUtenteDestino, EcNomeUtenteDestino
        If (ValidaUtentes And ValidaRequisicoes) Then: ProcuraAnalises EcReqDestino.Text, "sl_marcacoes", LvDestino
    End If
    BL_FimProcessamento Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'BtProcuraDestino_Click' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    BL_FimProcessamento Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
End Sub

Private Sub EcReqOrigem_Validate(Cancel As Boolean)
    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, " A procurar registos..."
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    LvOrigem.ListItems.Clear
    EcUtenteOrigem.Text = Empty
    EcNomeUtenteOrigem.Text = Empty
    If (EcReqOrigem.Text <> Empty) Then
        PreencheUtente EcReqOrigem.Text, EcUtenteOrigem, EcNomeUtenteOrigem
        If (ValidaUtentes And ValidaRequisicoes) Then: ProcuraAnalises EcReqOrigem.Text, "sl_realiza", LvOrigem
    End If
    BL_FimProcessamento Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'BtProcuraOrigem_Click' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    BL_FimProcessamento Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
End Sub

Private Sub Form_Deactivate()
    
    Call BL_FimProcessamento(Screen.ActiveForm, "")
    
End Sub

Sub Form_Load()

    EventoLoad
    
End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    On Error GoTo ErrorHandler
    BL_InicioProcessamento Me, "Inicializar �cran..."
    Inicializacoes
    IniciaListas
    Set CampoActivo = Me.ActiveControl
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EventoLoad' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub Inicializacoes()

    On Error GoTo ErrorHandler
    Me.caption = " C�pia de resultados de an�lises"
    Me.left = 540
    Me.top = 450
    Me.Width = 9585
    Me.Height = 8670
    Set CampoDeFocus = EcReqOrigem
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'Inicializacoes' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub EventoActivate()

    On Error GoTo ErrorHandler
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus
    BL_Toolbar_BotaoEstado "Imprimir", "InActivo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "InActivo"
    BL_Toolbar_BotaoEstado "Remover", "InActivo"
    BL_Toolbar_BotaoEstado "Inserir", "InActivo"
    BL_Toolbar_BotaoEstado "Modificar", "InActivo"
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EventoActivate' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub EventoUnload()

    On Error GoTo ErrorHandler
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
0
    Set FormCopiaResultados = Nothing
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'EventoUnload' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub LimpaCampos()
    
    On Error GoTo ErrorHandler
    EcReqOrigem.Text = Empty
    EcUtenteOrigem.Text = Empty
    EcNomeUtenteOrigem.Text = Empty
    LvOrigem.ListItems.Clear
    EcReqDestino.Text = Empty
    EcUtenteDestino.Text = Empty
    EcNomeUtenteDestino.Text = Empty
    LvDestino.ListItems.Clear
    LbInformacao.caption = Empty
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LimpaCampos' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Public Sub FuncaoLimpar()
    
    On Error GoTo ErrorHandler
    LimpaCampos
    CampoDeFocus.SetFocus
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'FuncaoLimpar' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Public Sub PreencheValoresDefeito()
   
End Sub

Private Sub IniciaListas()
           
    On Error GoTo ErrorHandler
    With LvOrigem
        .ColumnHeaders.Add(, , "C�digo", 1500, lvwColumnLeft).Tag = "CODE"
        .ColumnHeaders.Add(, , "Descri��o", 7230, lvwColumnLeft).Tag = "DESCRIPTION"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .SmallIcons = ImageListIconsOrigem
        .AllowColumnReorder = False
        .FullRowSelect = True
        .Checkboxes = False
        .MultiSelect = False
    End With
    
    With LvDestino
        .ColumnHeaders.Add(, , "C�digo", 1500, lvwColumnLeft).Tag = "CODE"
        .ColumnHeaders.Add(, , "Descri��o", 7230, lvwColumnLeft).Tag = "DESCRIPTION"
        .View = lvwReport
        .FlatScrollBar = False
        .LabelEdit = lvwManual
        .SmallIcons = ImageListIconsDestino
        .AllowColumnReorder = False
        .FullRowSelect = True
        .Checkboxes = False
        .MultiSelect = False
    End With
    
    BG_SetListViewColor LvOrigem, Picture1, &H8000000F, vbWhite
    BG_SetListViewColor LvDestino, Picture1, &H8000000F, vbWhite
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'IniciaListas' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub PreencheUtente(requisicao As String, Utente As TextBox, nome As TextBox)
 
    Dim sql As String
    Dim rs As ADODB.recordset
    
    On Error GoTo ErrorHandler
    sql = "select t_utente, utente, nome_ute from sl_identif, sl_requis where sl_identif.seq_utente = sl_requis.seq_utente and n_req = " & requisicao
    Set rs = BG_ExecutaSELECT(sql)
    If (rs.RecordCount > Empty) Then
        Utente.Text = BL_HandleNull(rs!t_utente, Empty) & Space(1) & BL_HandleNull(rs!Utente, Empty)
        nome.Text = UCase(BL_HandleNull(rs!nome_ute, Empty))
    End If
    BG_DestroiRecordSet rs
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'PreencheUtente' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub ProcuraAnalises(requisicao As String, Tabela As String, lista As ListView)

    Dim sql As String
    Dim rs As ADODB.recordset
    
    On Error GoTo ErrorHandler
    LockWindowUpdate lista.hwnd
    lista.MousePointer = MousePointerConstants.vbArrowHourglass
    sql = "select distinct cod_agrup, descr_ana from " & Tabela & ", slv_analises where n_req = " & _
          requisicao & " and cod_agrup = cod_ana and seq_sinonimo is null group by cod_agrup, descr_ana"
    Set rs = BG_ExecutaSELECT(sql)
    If (rs.RecordCount = Empty) Then: BG_Mensagem mediMsgBox, "N�o foram encontradas an�lises pendentes!", vbInformation, " An�lises"
    While (Not rs.EOF)
        With lista.ListItems.Add(, BL_HandleNull(rs!cod_agrup, Empty), BL_HandleNull(rs!cod_agrup, Empty), , 1)
            .ListSubItems.Add , BL_HandleNull(rs!cod_agrup, Empty), BL_HandleNull(rs!descr_ana, Empty)
        End With
        rs.MoveNext
    Wend
    BG_DestroiRecordSet rs
    LockWindowUpdate 0&
    lista.MousePointer = MousePointerConstants.vbArrow
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ProcuraAnalises' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    BG_DestroiRecordSet rs
    LockWindowUpdate 0&
    lista.MousePointer = MousePointerConstants.vbArrow
    Exit Sub
    
End Sub

Private Sub LvOrigem_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    On Error GoTo ErrorHandler
    If (Button = vbLeftButton And Not LvOrigem.SelectedItem Is Nothing) Then
        Set LvOrigem.SelectedItem = item
        LvOrigem.DragIcon = ImageListIconsOrigem.ListImages(1).Picture
        LvOrigem.Drag vbBeginDrag
    End If
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvOrigem_MouseMove' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
   
End Sub

Private Sub LvOrigem_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
 
    On Error GoTo ErrorHandler
    Set item = LvOrigem.HitTest(X, Y)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ListViewResults_MouseDown' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub LvDestino_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
 
    On Error GoTo ErrorHandler
    Set item = LvDestino.HitTest(X, Y)
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvDestino_MouseDown' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub LvDestino_DragDrop(Source As Control, X As Single, Y As Single)
   
    On Error GoTo ErrorHandler
    SelecionaAnalise item.Text
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvDestino_DragDrop' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub LvOrigem_DragDrop(Source As Control, X As Single, Y As Single)
   
    On Error GoTo ErrorHandler
    DeselecionaAnalise item.Text
    Exit Sub
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'LvOrigem_DragDrop' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub SelecionaAnalise(analise As String)
   
    Dim item_local As Object
    
    On Error GoTo ErrorHandler
    If (LvDestino.ListItems.Count > Empty) Then
        Set item_local = LvDestino.FindItem(analise, , 1, lvwWhole)
        If (item_local Is Nothing) Then: Exit Sub
        item_local.Bold = True
        item_local.ListSubItems(1).Bold = True
        item_local.SmallIcon = 2
        LvDestino.Refresh
    End If
    Exit Sub
      
ErrorHandler:
    BG_LogFile_Erros "Error in function 'SelecionaAnalise' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Sub DeselecionaAnalise(analise As String)
   
    Dim item_local As Object
    
    On Error GoTo ErrorHandler
    If (LvDestino.ListItems.Count > Empty) Then
        Set item_local = LvDestino.FindItem(analise, , 1, lvwWhole)
        If (item_local Is Nothing) Then: Exit Sub
        item_local.Bold = False
        item_local.ListSubItems(1).Bold = False
        item_local.SmallIcon = 1
        LvDestino.Refresh
    End If
    Exit Sub
      
ErrorHandler:
    BG_LogFile_Erros "Error in function 'DeselecionaAnalise' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Sub
    
End Sub

Private Function ValidaUtentes() As Boolean

    On Error GoTo ErrorHandler
    If (EcNomeUtenteOrigem.Text <> Empty And EcNomeUtenteDestino <> Empty) Then
        If (EcNomeUtenteOrigem.Text <> EcNomeUtenteDestino <> Empty) Then
            BG_Mensagem mediMsgBox, "As requisi��es s�o de utentes diferentes!", vbExclamation, " Aten��o"
            Exit Function
        End If
    End If
    ValidaUtentes = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ValidaUtentes' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function GeraSequencia(ByRef sequencia As Long) As Boolean

    Dim tamanho As Integer
    
    On Error GoTo ErrorHandler
    sequencia = -1
    tamanho = 0
    While (sequencia = -1 And tamanho <= 10)
        sequencia = BL_GeraNumero("SEQ_REALIZA")
        tamanho = tamanho + 1
    Wend
    If (sequencia = -1) Then: BG_Mensagem mediMsgBox, "Erro ao gerar n�mero de sequencial de realiza��o!", vbExclamation, " ERRO": Exit Function
    GeraSequencia = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'GeraSequencia' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function

End Function

Private Function ExecutaCopia() As Boolean
  
    Dim sequencia_destino As Long
    Dim item As ListItem
    Dim sequencia_origem As Variant
    Dim analises_selecionadas As Integer
    Dim anaTemp As String
    
    On Error GoTo ErrorHandler
    LbInformacao.caption = Empty
    BL_InicioProcessamento Me, " A copiar registos..."
    Me.MousePointer = vbArrowHourglass
    MDIFormInicio.MousePointer = vbArrowHourglass
    gConexao.BeginTrans
    anaTemp = ""
    For Each item In LvDestino.ListItems
        If (item.Bold = True) Then
            analises_selecionadas = analises_selecionadas + 1
            For Each sequencia_origem In ExtraiSequencias(EcReqOrigem.Text, EcReqDestino.Text, item.Text)
                If (Not GeraSequencia(sequencia_destino)) Then: gConexao.RollbackTrans: Exit Function
                If (Not RealizaAnalise(CLng(sequencia_origem), sequencia_destino, EcReqDestino.Text)) Then: gConexao.RollbackTrans: Exit Function
                If anaTemp <> item.Text Then
                    If (Not CopiaResultados(CLng(sequencia_origem), sequencia_destino, item.Text, True)) Then: gConexao.RollbackTrans: Exit Function
                Else
                    If (Not CopiaResultados(CLng(sequencia_origem), sequencia_destino, item.Text, False)) Then: gConexao.RollbackTrans: Exit Function
                End If
                If (Not ApagaMarcacao(EcReqDestino.Text, item.Text)) Then: gConexao.RollbackTrans: Exit Function
            anaTemp = item.Text
            Next
        End If
    Next
    If (Not RegistaUtilizador(EcReqOrigem.Text, EcReqDestino.Text)) Then: gConexao.RollbackTrans: Exit Function
    gConexao.CommitTrans
    ExecutaCopia = True
    BL_FimProcessamento Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    If (analises_selecionadas > Empty) Then
        LbInformacao.ForeColor = vbBlack
        LbInformacao.caption = "Registos copiados: " & analises_selecionadas
    Else
        LbInformacao.ForeColor = vbRed
        LbInformacao.caption = "N�o foram copiados registos"
    End If
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ExecutaCopia' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    gConexao.RollbackTrans
    BL_FimProcessamento Me
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    Exit Function

End Function

Private Function RealizaAnalise(sequencia_origem As Long, sequencia_destino As Long, requisicao_destino As String) As Boolean
  
    Dim sql As String
    
    On Error GoTo ErrorHandler
    sql = "insert into sl_realiza (seq_realiza, n_req, cod_perfil, cod_ana_c, cod_ana_s, ord_ana,ord_ana_compl, ord_ana_perf, cod_agrup, " & _
          "n_folha_trab, flg_estado, user_val, dt_val, hr_val, dt_cri, user_cri, hr_cri, dt_act, user_act, hr_act, dt_chega, flg_anormal," & _
          "flg_anormal_2, seq_utente, user_tec_val, dt_tec_val, hr_tec_val, flg_unid_act1, flg_unid_act2, unid_1_res1, unid_2_res1," & _
          "fac_conv_unid1, unid_1_res2, unid_2_res2, fac_conv_unid2, ord_marca, flg_facturado, hr_colheita, dt_colheita, hr_ult_admin," & _
          "dt_ult_admin) (select " & sequencia_destino & ", " & requisicao_destino & ", cod_perfil, cod_ana_c, cod_ana_s, " & _
          "ord_ana, ord_ana_compl, ord_ana_perf, cod_agrup, n_folha_trab, " & _
          "flg_estado, user_val, dt_val, hr_val, dt_cri, user_cri, hr_cri, dt_act, user_act, hr_act, dt_chega, flg_anormal, " & _
          "flg_anormal_2, seq_utente, user_tec_val, dt_tec_val, hr_tec_val, flg_unid_act1, flg_unid_act2, unid_1_res1, unid_2_res1, " & _
          "fac_conv_unid1, unid_1_res2, unid_2_res2, fac_conv_unid2, ord_marca, flg_facturado, hr_colheita, " & _
          "dt_colheita, hr_ult_admin, dt_ult_admin  from sl_realiza  where seq_realiza = " & sequencia_origem & ")"
    If (BG_ExecutaQuery_ADO(sql) = -1) Then: Exit Function
    BG_Trata_BDErro
    RealizaAnalise = (gSQLError = Empty)
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'RealizaAnalise' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function CopiaResultados(sequencia_origem As Long, sequencia_destino As Long, analise As String, FlgExecuta As Boolean) As Boolean
  
    Dim sql As String
    
    On Error GoTo ErrorHandler
    If (Not CopiaResultado("sl_res_alfan", sequencia_origem, sequencia_destino, analise)) Then: Exit Function
    If (Not CopiaResultado("sl_res_frase", sequencia_origem, sequencia_destino, analise)) Then: Exit Function
    If (Not CopiaResultado("sl_res_micro", sequencia_origem, sequencia_destino, analise)) Then: Exit Function
    If (Not CopiaResultado("sl_res_tsq", sequencia_origem, sequencia_destino, analise)) Then: Exit Function
    If FlgExecuta = True Then
        If (Not CopiaResultado("sl_res_grafico", sequencia_origem, sequencia_destino, analise)) Then: Exit Function
    End If
    If (Not CopiaResultado("sl_obs_ana", sequencia_origem, sequencia_destino, analise)) Then: Exit Function
    CopiaResultados = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CopiaResultados' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function ApagaMarcacao(requisicao As String, analise As String) As Boolean
  
    Dim sql As String
    
    On Error GoTo ErrorHandler
    sql = "delete from sl_marcacoes where n_req = " & requisicao & " and cod_agrup = " & BL_TrataStringParaBD(analise)
    If (BG_ExecutaQuery_ADO(sql) = -1) Then: Exit Function
    BG_Trata_BDErro
    ApagaMarcacao = (gSQLError = Empty)
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ApagaMarcacao' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function ExtraiSequencias(requisicao_origem As String, requisicao_destino As String, analise As String) As Collection
  
    Dim rs As ADODB.recordset
    Dim Erro As Boolean
    Dim sql As String
    
    On Error GoTo ErrorHandler
    Set ExtraiSequencias = New Collection
    sql = "select distinct seq_realiza from sl_realiza, sl_marcacoes where sl_realiza.n_req = " & requisicao_origem & " and " & _
          "sl_marcacoes.n_req = " & requisicao_destino & " and sl_realiza.cod_perfil = sl_marcacoes.cod_perfil " & " and " & _
          "sl_realiza.cod_ana_c = sl_marcacoes.cod_ana_c and sl_realiza.cod_ana_s = sl_marcacoes.cod_ana_s and " & _
          "sl_realiza.cod_agrup = sl_realiza.cod_agrup and sl_realiza.cod_agrup = " & BL_TrataStringParaBD(analise)
    Set rs = BG_ExecutaSELECT(sql, Erro)
    If (Not Erro) Then: Exit Function
    While (Not rs.EOF)
        ExtraiSequencias.Add BL_HandleNull(rs!seq_realiza, Empty)
        rs.MoveNext
    Wend
    BG_DestroiRecordSet rs
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ExtraiSequencias' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function CopiaResultado(Tabela As String, sequencia_origem As Long, sequencia_destino As Long, analise As String) As Boolean
    
    Dim sql As String
    Dim Campos As String
       
    On Error GoTo ErrorHandler
    Select Case UCase(Tabela)
        Case "SL_RES_ALFAN":
            Campos = "n_res, RESULT, res_ant1, res_ant2, res_ant3, dt_res_ant1, dt_res_ant2, dt_res_ant3, local_ant1, local_ant2, " & _
                     "local_ant3, flg_apar, flg_res_ant1, flg_res_ant2, flg_res_ant3, flg_imprimir "
        Case "SL_RES_FRASE":
            Campos = "n_res, cod_frase, ord_frase, flg_imprimir, flg_apar"
        Case "SL_RES_MICRO":
            Campos = "n_res, cod_micro, quantif, flg_imp, flg_tsq, cod_gr_antibio, prova, flg_testes, res_ant1, dt_res_ant1, res_ant2, " & _
                     "dt_res_ant2, res_ant3, dt_res_ant3, datainsc, ncontrolo, analise, elemento, ele_antib, seq, flg_imprimir, flg_apar "
        Case "SL_RES_TSQ":
            Campos = " n_res, cod_antib, cod_micro, res_sensib, cmi, flg_imp"
        Case "SL_RES_GRAFICO":
            sql = "insert into sl_res_grafico (grafico, cod_ana_c, n_req, sombreado) (SELECT grafico, cod_ana_c," & EcReqDestino & ",sombreado FROM sl_res_grafico"
            sql = sql & " WHERE n_req = " & EcReqOrigem & " AND COD_ANA_C = " & BL_TrataStringParaBD(analise) & ")"
             If CopiaGraficoBanda(EcReqOrigem, EcReqDestino, analise) = False Then Exit Function
        Case "SL_OBS_ANA":
            Campos = " seq_obs_ana, descr_obs_ana"
        Case Else:
            Exit Function
    End Select
    If UCase(Tabela) <> "SL_RES_GRAFICO" Then
        sql = "insert into " & Tabela & " (seq_realiza, " & Campos & ") (select " & sequencia_destino & ", " & _
              Campos & " from " & Tabela & " where seq_realiza = " & sequencia_origem & ")"
    End If
    If (BG_ExecutaQuery_ADO(sql) = -1) Then: Exit Function
    BG_Trata_BDErro
    If (gSQLError <> Empty) Then: Exit Function
    CopiaResultado = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'CopiaResultado' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    Resume Next
End Function

Private Function ValidaRequisicoes() As Boolean
    
    On Error GoTo ErrorHandler
    If (EcReqOrigem.Text <> Empty And EcReqDestino.Text <> Empty) Then
         If (EcReqOrigem.Text = EcReqDestino.Text) Then: BG_Mensagem mediMsgBox, "N�o pode copiar de requisi��es iguais!", vbExclamation, " Aten��o"
    End If
    ValidaRequisicoes = True
    Exit Function
       
ErrorHandler:
    BG_LogFile_Erros "Error in function 'ValidaRequisicoes' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function

Private Function RegistaUtilizador(requisicao_origem As String, requisicao_destino As String) As Boolean
    
    Dim sql As String
    
    On Error GoTo ErrorHandler
    sql = "insert into sl_copia_resultados (req_origem, req_destino, user_cri, dt_cri, hr_cri) values (" & _
          requisicao_origem & "," & requisicao_destino & "," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & "," & _
          BL_TrataDataParaBD(Bg_DaData_ADO) & "," & BL_TrataStringParaBD(Bg_DaHora_ADO) & ")"
    If (BG_ExecutaQuery_ADO(sql) = -1) Then: Exit Function
    BG_Trata_BDErro
    If (gSQLError <> Empty) Then: Exit Function
    RegistaUtilizador = True
    Exit Function
    
ErrorHandler:
    BG_LogFile_Erros "Error in function 'RegistaUtilizador' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
    
End Function
Private Function CopiaGraficoBanda(req_o As String, req_d As String, analise As String) As Boolean
    Dim sSql As String
    Dim rsO As New ADODB.recordset
    Dim rsD As New ADODB.recordset
    Dim ByteData() As Byte   'Byte array for Blob data.
    Dim FileLength As Long  'Used in Command1 and Command2 procedures.
    Dim Numblocks As Integer
    Dim LeftOver As Long
    Const BlockSize = 100000
    Dim i As Integer
    CopiaGraficoBanda = False
    On Error GoTo TrataErro
    sSql = "INSERT INTO sl_res_grafico_banda (cod_ana, n_req)(SELECT cod_ana, " & req_d & " From sl_res_grafico_banda "
    sSql = sSql & " WHERE n_req = " & req_o & " AND cod_ana = " & BL_TrataStringParaBD(analise) & ")"
    BG_ExecutaQuery_ADO sSql
    
    sSql = "SELECT grafico FROM sl_res_grafico_banda WHERE n_req = " & req_o & "  and cod_ana = " & BL_TrataStringParaBD(analise)
    rsO.CursorType = adOpenStatic
    rsO.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsO.Open sSql, gConexao
    If rsO.RecordCount = 1 Then
        sSql = "SELECT grafico FROM sl_res_grafico_banda WHERE n_req = " & req_d & "  and cod_ana = " & BL_TrataStringParaBD(analise)
        rsD.CursorType = adOpenKeyset
        rsD.CursorLocation = adUseServer
        rsD.LockType = adLockOptimistic
        If gModoDebug = mediSim Then BG_LogFile_Erros sSql
        rsD.Open sSql, gConexao
        If rsD.RecordCount = 1 Then
            ReDim ByteData(BlockSize)
            ByteData() = rsO(0).GetChunk(rsO(0).ActualSize)
            rsD(0).AppendChunk ByteData()
    
            rsD.Update   'Commit the new data.
        End If
    End If
    CopiaGraficoBanda = True

Exit Function
TrataErro:
    CopiaGraficoBanda = False
    BG_LogFile_Erros "Error in function 'CopiaGraficoBanda' in form FormCopiaResultados (" & Error & " - " & Err.Number & " - " & Err.Description & ")"
    Exit Function
End Function

VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormMapFact 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FormMapFact"
   ClientHeight    =   8295
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   13035
   Icon            =   "FormMapFact.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8295
   ScaleWidth      =   13035
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcHoraCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4860
      TabIndex        =   51
      Top             =   11880
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcHoraAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4860
      TabIndex        =   50
      Top             =   12240
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcDataCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4440
      TabIndex        =   49
      Top             =   11880
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox EcUtilizadorCriacao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   48
      Top             =   11880
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox EcDataAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   4440
      TabIndex        =   47
      Top             =   12240
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.TextBox EcUtilizadorAlteracao 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      TabIndex        =   46
      Top             =   11160
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.Frame Frame1 
      Height          =   3735
      Left            =   120
      TabIndex        =   25
      Top             =   0
      Width           =   12855
      Begin VB.TextBox EcDescrPortaria 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8400
         TabIndex        =   57
         Top             =   840
         Width           =   3975
      End
      Begin VB.TextBox EcCodPortaria 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7320
         TabIndex        =   3
         Top             =   840
         Width           =   1095
      End
      Begin VB.CommandButton BtPesquisaPortaria 
         Height          =   315
         Left            =   12360
         Picture         =   "FormMapFact.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   56
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de R�bricas"
         Top             =   840
         Width           =   375
      End
      Begin VB.OptionButton OptTipoPesq 
         Caption         =   "Ana. N�o Mapeadas"
         Height          =   255
         Index           =   1
         Left            =   9720
         TabIndex        =   45
         Top             =   360
         Width           =   1935
      End
      Begin VB.OptionButton OptTipoPesq 
         Caption         =   "Todas"
         Height          =   255
         Index           =   2
         Left            =   11760
         TabIndex        =   44
         Top             =   360
         Width           =   855
      End
      Begin VB.OptionButton OptTipoPesq 
         Caption         =   "Ana. Mapeadas"
         Height          =   255
         Index           =   0
         Left            =   7680
         TabIndex        =   43
         Top             =   360
         Width           =   1935
      End
      Begin VB.CheckBox CkSoFacturacao 
         Caption         =   "Rubrica usada s� na factura��o (N�o entra na passagem da GH para o SISLAB)"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   1800
         Width           =   6015
      End
      Begin VB.CheckBox CkContaMembros 
         Caption         =   "Envia Membros para FACTUS"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   2145
         Width           =   3855
      End
      Begin VB.TextBox EcCodAnaRegra 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3960
         TabIndex        =   8
         Top             =   2520
         Width           =   735
      End
      Begin VB.CheckBox CkFlgRegra 
         Caption         =   "S� factura se n�o marcada a an�lise"
         Height          =   195
         Left            =   240
         TabIndex        =   7
         Top             =   2520
         Width           =   2895
      End
      Begin VB.CheckBox CkFlgfacturarAna 
         Caption         =   "Pergunta se pretende facturar an�lise"
         Height          =   195
         Left            =   240
         TabIndex        =   9
         Top             =   2880
         Width           =   3015
      End
      Begin VB.TextBox EcCodAnaFacturar 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3960
         TabIndex        =   10
         Top             =   2880
         Width           =   735
      End
      Begin VB.TextBox EcPercent 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8760
         TabIndex        =   16
         Top             =   2880
         Width           =   735
      End
      Begin VB.TextBox EcQtd 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8760
         TabIndex        =   14
         Top             =   2520
         Width           =   735
      End
      Begin VB.TextBox EcCentroCusto 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   11760
         TabIndex        =   15
         Top             =   2520
         Width           =   735
      End
      Begin VB.CheckBox CkMarcadores 
         Caption         =   "An�lise Sujeita a Regra de Marcadores"
         Height          =   195
         Left            =   240
         TabIndex        =   11
         Top             =   3240
         Width           =   3135
      End
      Begin VB.TextBox EcNumMinMarcadores 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3960
         TabIndex        =   12
         Top             =   3240
         Width           =   735
      End
      Begin VB.TextBox EcNumMedioMarcadores 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5520
         TabIndex        =   13
         Top             =   3240
         Width           =   735
      End
      Begin VB.CommandButton BtPesquisaRubr 
         Height          =   315
         Left            =   12360
         Picture         =   "FormMapFact.frx":0596
         Style           =   1  'Graphical
         TabIndex        =   37
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida dePortarias"
         Top             =   1320
         Width           =   375
      End
      Begin VB.CommandButton BtPesquisaAnaFact 
         Height          =   315
         Left            =   6240
         Picture         =   "FormMapFact.frx":0B20
         Style           =   1  'Graphical
         TabIndex        =   36
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida deAn�lises"
         Top             =   840
         Width           =   375
      End
      Begin VB.TextBox EcDescrAna 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2160
         TabIndex        =   30
         Top             =   840
         Width           =   4095
      End
      Begin VB.TextBox EcCodAna 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   840
         Width           =   1095
      End
      Begin VB.TextBox EcSeqAna 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   0
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox EcCodGH 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   7320
         TabIndex        =   4
         Top             =   1320
         Width           =   1095
      End
      Begin VB.TextBox EcDescrGH 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   8400
         TabIndex        =   29
         Top             =   1320
         Width           =   3975
      End
      Begin VB.TextBox EcCodEfr 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   1320
         Width           =   1095
      End
      Begin VB.TextBox EcDescrEfr 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   28
         Top             =   1320
         Width           =   4095
      End
      Begin VB.CommandButton BtPesquisaEntFin 
         Height          =   315
         Left            =   6240
         Picture         =   "FormMapFact.frx":10AA
         Style           =   1  'Graphical
         TabIndex        =   27
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisa R�pida de Entidades Financeiras  "
         Top             =   1320
         Width           =   375
      End
      Begin VB.ListBox EcLocais 
         Appearance      =   0  'Flat
         Height          =   705
         Left            =   7320
         Style           =   1  'Checkbox
         TabIndex        =   26
         Top             =   1800
         Width           =   5175
      End
      Begin VB.Label LaSistemaFacturacao 
         Caption         =   "Portaria"
         Height          =   255
         Index           =   1
         Left            =   6720
         TabIndex        =   58
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Valor do desconto"
         Height          =   255
         Index           =   0
         Left            =   7320
         TabIndex        =   42
         Top             =   2880
         Width           =   1455
      End
      Begin VB.Label Label4 
         Caption         =   "Quantidade"
         Height          =   255
         Index           =   0
         Left            =   7320
         TabIndex        =   41
         Top             =   2520
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "Centro Custo"
         Height          =   255
         Index           =   1
         Left            =   10560
         TabIndex        =   40
         Top             =   2520
         Width           =   1095
      End
      Begin VB.Label Label5 
         Caption         =   "Min."
         Height          =   255
         Index           =   1
         Left            =   3600
         TabIndex        =   39
         Top             =   3240
         Width           =   375
      End
      Begin VB.Label Label6 
         Caption         =   "M�dio"
         Height          =   255
         Left            =   4920
         TabIndex        =   38
         Top             =   3240
         Width           =   495
      End
      Begin VB.Label Label2 
         Caption         =   "Sislab"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   35
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo"
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   360
         Width           =   855
      End
      Begin VB.Label LaSistemaFacturacao 
         Caption         =   "R�brica"
         Height          =   255
         Index           =   0
         Left            =   6720
         TabIndex        =   33
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Entidade"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   32
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label LbLocal 
         AutoSize        =   -1  'True
         Caption         =   "Locais"
         Height          =   195
         Index           =   0
         Left            =   6720
         TabIndex        =   31
         Top             =   1800
         Width           =   465
      End
   End
   Begin MSFlexGridLib.MSFlexGrid FgRes 
      Height          =   3855
      Left            =   120
      TabIndex        =   24
      Top             =   3840
      Width           =   12855
      _ExtentX        =   22675
      _ExtentY        =   6800
      _Version        =   393216
      BackColorBkg    =   -2147483633
      BorderStyle     =   0
      Appearance      =   0
   End
   Begin VB.Frame Frame2 
      Height          =   585
      Left            =   0
      TabIndex        =   17
      Top             =   7680
      Width           =   13005
      Begin VB.Label LaDataAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   10080
         TabIndex        =   23
         Top             =   240
         Width           =   2175
      End
      Begin VB.Label LaDataCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   22
         Top             =   195
         Width           =   2175
      End
      Begin VB.Label LaUtilAlteracao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   8280
         TabIndex        =   21
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label LaUtilCriacao 
         Enabled         =   0   'False
         Height          =   255
         Left            =   1080
         TabIndex        =   20
         Top             =   200
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Altera��o"
         Height          =   255
         Index           =   0
         Left            =   7440
         TabIndex        =   19
         Top             =   240
         Width           =   705
      End
      Begin VB.Label Label8 
         Caption         =   "Cria��o"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   195
         Width           =   675
      End
   End
   Begin VB.Label Label13 
      Caption         =   "EcDataAlteracao"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3000
      TabIndex        =   55
      Top             =   12240
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label12 
      Caption         =   "EcDataCriacao"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3120
      TabIndex        =   54
      Top             =   11880
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "EcUtilizadorAlteracao"
      Enabled         =   0   'False
      Height          =   375
      Left            =   0
      TabIndex        =   53
      Top             =   11160
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label10 
      Caption         =   "EcUtilizadorCriacao"
      Enabled         =   0   'False
      Height          =   375
      Left            =   120
      TabIndex        =   52
      Top             =   11880
      Visible         =   0   'False
      Width           =   1455
   End
End
Attribute VB_Name = "FormMapFact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim NumCampos As Integer

Dim CamposBD() As String
Dim CamposEc() As Object
Dim TextoCamposObrigatorios() As String

Dim ChaveBD As String
Dim ChaveEc As Object

Dim NomeTabela As String
Dim CriterioTabela As String

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Dim MarcaInicial As Variant  ' Bookmark do primeiro registo para poder ser utilizado na ListBox 'EcLista'

Dim CamposBDparaListBox
Dim NumEspacos

Public rs As ADODB.recordset

Const colSeqAna = 0
Const colCodAna = 1
Const ColDescrAna = 2
Const colCodAnaGh = 3
Const colDescrAnaGH = 4
Const colCodEfr = 5
Const colDescrEfr = 6
Const colQuantidade = 7

'BRUNODSANTOS HCVP-4619 - 19.07.2016
Dim flg_chk_estado_anterior As Boolean
'



Private Sub FGRes_Click()
    If FgRes.row > 0 And FgRes.row < FgRes.rows - 1 Then
        rs.Move FgRes.row - 1, MarcaInicial
        LimpaCampos
        PreencheCampos
    Else
        LimpaCampos
    End If

End Sub

Sub Form_Load()
    
    EventoLoad

End Sub

Sub Form_Activate()
    
    EventoActivate

End Sub

Sub Form_Unload(Cancel As Integer)
    
    EventoUnload

End Sub

Sub Inicializacoes()
    
    Me.caption = " Mapeamento para Fatura��o"
    Me.left = 50
    Me.top = 50
    Me.Width = 13125
    Me.Height = 8730 ' Normal
    'Me.Height = 8330 ' Campos Extras
    
    NomeTabela = "sl_ana_facturacao"
    Set CampoDeFocus = EcSeqAna
    
    NumCampos = 23
    ReDim CamposBD(0 To NumCampos - 1)
    ReDim CamposEc(0 To NumCampos - 1)
    ReDim TextoCamposObrigatorios(0 To NumCampos - 1)
    
    ' Campos da Base de Dados
    
    CamposBD(0) = "seq_ana"
    CamposBD(1) = "cod_ana"
    CamposBD(2) = "cod_ana_gh"
    CamposBD(3) = "flg_so_facturacao"
    CamposBD(4) = "cod_efr"
    CamposBD(5) = "flg_conta_membros"
    CamposBD(6) = "cod_ana_regra"
    CamposBD(7) = "flg_regra"
    CamposBD(8) = "cod_ana_Facturar"
    CamposBD(9) = "flg_Ana_Facturar"
    CamposBD(10) = "perc_Facturar"
    CamposBD(11) = "qtd"
    CamposBD(12) = "centro_custo"
    CamposBD(13) = "flg_marcador"
    CamposBD(14) = "min_marcadores"
    CamposBD(15) = "media_marcadores"
    CamposBD(16) = "user_cri"
    CamposBD(17) = "dt_cri"
    CamposBD(18) = "hr_cri"
    CamposBD(19) = "user_act"
    CamposBD(20) = "dt_act"
    CamposBD(21) = "hr_act"
    CamposBD(22) = "cod_portaria"
    
    ' Campos do Ecr�
    Set CamposEc(0) = EcSeqAna
    Set CamposEc(1) = EcCodAna
    Set CamposEc(2) = EcCodGH
    Set CamposEc(3) = CkSoFacturacao
    Set CamposEc(4) = EcCodEfr
    Set CamposEc(5) = CkContaMembros
    Set CamposEc(6) = EcCodAnaRegra
    Set CamposEc(7) = CkFlgRegra
    Set CamposEc(8) = EcCodAnaFacturar
    Set CamposEc(9) = CkFlgfacturarAna
    Set CamposEc(10) = EcPercent
    Set CamposEc(11) = EcQtd
    Set CamposEc(12) = EcCentroCusto
    Set CamposEc(13) = CkMarcadores
    Set CamposEc(14) = EcNumMinMarcadores
    Set CamposEc(15) = EcNumMedioMarcadores
    Set CamposEc(16) = EcUtilizadorCriacao
    Set CamposEc(17) = EcDataCriacao
    Set CamposEc(18) = EcHoraCriacao
    Set CamposEc(19) = EcUtilizadorAlteracao
    Set CamposEc(20) = EcDataAlteracao
    Set CamposEc(21) = EcHoraAlteracao
    Set CamposEc(22) = EcCodPortaria
    
    TextoCamposObrigatorios(0) = "Sequencial"
    TextoCamposObrigatorios(1) = "C�digo da An�lise"
    TextoCamposObrigatorios(2) = "C�digo da R�brica"
    TextoCamposObrigatorios(22) = "C�digo da Portaria"
    
    ' Campo Chave da BD e correspondente no Ecr�
    ' Apenas � admiss�vel 1 campo. Para mais campos, devem-se fazer
    ' as altera��es nas rotinas BD_...
    ChaveBD = "seq_ana"
    Set ChaveEc = EcSeqAna
    
    CamposBDparaListBox = Array("seq_ana", "cod_ana", "descr_ana", "cod_ana_gh", "descr_ana_gh", "cod_efr", "descr_efr", "qtd")

End Sub

Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma
    
    BL_InicioProcessamento Me, "Inicializar �cran."
    Inicializacoes
    
    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 1
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me

End Sub

Sub EventoActivate()
    
    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    If estado = 2 Then BL_Toolbar_BotaoEstado "Remover", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow

End Sub

Sub EventoUnload()
    
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set FormMapFact = Nothing

End Sub

Sub LimpaCampos()
    Dim i As Integer
    Me.SetFocus

    BG_LimpaCampo_Todos CamposEc
    LaUtilCriacao.caption = ""
    LaUtilAlteracao.caption = ""
    LaDataCriacao.caption = ""
    LaDataAlteracao.caption = ""
    CkContaMembros.value = vbGrayed
    CkFlgRegra.value = vbGrayed
    CkMarcadores.value = vbGrayed
    CkFlgfacturarAna.value = vbGrayed
    CkSoFacturacao.value = vbGrayed
    
    
    'BRUNODSANTOS HCVP 4619 - 19.07.2016
    If flg_chk_estado_anterior = False Then
       OptTipoPesq(0).value = True
    End If
    '
    
    EcDescrAna.Text = ""
    EcDescrEfr.Text = ""
    EcDescrGH.Text = ""
    EcDescrPortaria.Text = ""
    For i = 0 To EcLocais.ListCount - 1
        EcLocais.Selected(i) = False
    Next i
    EcSeqAna.locked = False
    
End Sub

Sub DefTipoCampos()
    
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_cri", EcDataCriacao, mediTipoData
    BG_DefTipoCampoEc_ADO NomeTabela, "dt_act", EcDataAlteracao, mediTipoData
    BG_DefTipoCampoEc_Todos_ADO NomeTabela, CamposBD, CamposEc, mediTipoDefeito
    BG_PreencheComboBD_ADO "gr_empr_inst", "cod_empr", "nome_empr", EcLocais, mediAscComboCodigo
    
    With FgRes
        .rows = 2
        .FixedRows = 1
        .Cols = 8
        .FixedCols = 0
        .AllowBigSelection = False
        .ScrollBars = flexScrollBarVertical
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusHeavy
        .MousePointer = flexDefault
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionByRow
        .AllowUserResizing = flexResizeNone
        .GridLines = flexGridFlat
        .GridLinesFixed = flexGridInset
        .TextStyle = flexTextFlat
        .TextStyleFixed = flexTextInsetLight
        .MergeCells = flexMergeRestrictColumns
        .PictureType = flexPictureColor
        .RowHeightMin = 285
        .row = 0
        
        .ColWidth(colSeqAna) = 500
        .Col = colSeqAna
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, colSeqAna) = "C�d."
        
        .ColWidth(colCodAna) = 1000
        .Col = colCodAna
        .ColAlignment(0) = flexAlignLeftCenter
        .TextMatrix(0, colCodAna) = "An�lise"
        
        .ColWidth(ColDescrAna) = 3000
        .Col = ColDescrAna
        .TextMatrix(0, ColDescrAna) = "An�lise"
        
        .ColWidth(colCodAnaGh) = 1000
        .Col = colCodAnaGh
        .TextMatrix(0, colCodAnaGh) = "R�brica"
        
        .ColWidth(colDescrAnaGH) = 3000
        .Col = colDescrAnaGH
        .TextMatrix(0, colDescrAnaGH) = "R�brica"
        
        .ColWidth(colCodEfr) = 1000
        .Col = colCodEfr
        .TextMatrix(0, colCodEfr) = "EFR"
        
        .ColWidth(colDescrEfr) = 2000
        .Col = colDescrEfr
        .TextMatrix(0, colDescrEfr) = "EFR"
        
        .ColWidth(colQuantidade) = 700
        .Col = colQuantidade
        .TextMatrix(0, colQuantidade) = "Qtd."
        
        .WordWrap = False
        .row = 1
        .Col = 0
    End With
    FgRes.MergeCol(colCodAna) = True
    FgRes.MergeCol(ColDescrAna) = True

End Sub

Sub PreencheValoresDefeito()
    CkContaMembros.value = vbGrayed
    CkFlgRegra.value = vbGrayed
    CkMarcadores.value = vbGrayed
    CkFlgfacturarAna.value = vbGrayed
    CkSoFacturacao.value = vbGrayed
    OptTipoPesq(0).value = True
End Sub

Sub PreencheCampos()
        
    Me.SetFocus
    
    If rs.RecordCount = 0 Then
        LimpaCampos
    Else
        EcSeqAna.locked = True
        BG_PreencheCampoEc_Todos_ADO rs, CamposBD, CamposEc
        BL_PreencheControloAcessos EcUtilizadorCriacao, EcUtilizadorAlteracao, LaUtilCriacao, LaUtilAlteracao
        LaDataCriacao = EcDataCriacao & " " & EcHoraCriacao
        LaDataAlteracao = EcDataAlteracao & " " & EcHoraAlteracao
        CarregaLocaisMapeam EcSeqAna
        EcDescrAna.Text = BL_HandleNull(rs!descr_ana, "")
        EcDescrGH.Text = BL_HandleNull(rs!descr_ana_gh, "")
        EcDescrEfr.Text = BL_HandleNull(rs!descr_efr, "")
        EcDescrPortaria.Text = BL_HandleNull(rs!descr_Portaria, "")
        If CkMarcadores.value = vbChecked Then
            EcNumMedioMarcadores.Enabled = True
            EcNumMinMarcadores.Enabled = True
        Else
            EcNumMedioMarcadores.Enabled = False
            EcNumMinMarcadores.Enabled = False
            EcNumMedioMarcadores.Text = ""
            EcNumMinMarcadores.Text = ""
        End If
    End If
    
    
End Sub

Sub FuncaoLimpar()
    'BRUNODSANTOS HCVP-4619 - 19.07.2016
        flg_chk_estado_anterior = False
    '
    If estado = 2 Then
        FuncaoEstadoAnterior
        LimpaFgRes
    Else
        LimpaCampos
        LimpaFgRes
        CampoDeFocus.SetFocus
    End If

End Sub

Sub FuncaoEstadoAnterior()
    
    If estado = 2 Then
        estado = 1
        BL_ToolbarEstadoN estado
        
        LimpaCampos
        CampoDeFocus.SetFocus
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        Unload Me
    End If

End Sub

Function ValidaCamposEc() As Integer
    Dim iRes, i As Integer

    For i = 0 To NumCampos - 1
        If TextoCamposObrigatorios(i) <> "" Then
            iRes = BG_ValidaCampo(gFormActivo.caption, CamposEc(i), TextoCamposObrigatorios(i))
            If iRes = vbOK Or iRes = vbCancel Then
                ValidaCamposEc = False
                Exit Function
            End If
        End If
    Next i
    
    ValidaCamposEc = True

End Function

Sub FuncaoProcurar()
            
    Dim SelTotal As Boolean
    Dim SimNao As Integer
    Dim str_aux1 As String
    Dim str_aux2 As String
    Dim str_aux3 As String
    
    
    BL_InicioProcessamento Me, "A pesquisar registos."
    Set rs = New ADODB.recordset
               
    CriterioTabela = ConstroiCriterio

              
    'estas especifica��es s�o necess�rias para poder usar propriedades como RecordCount, Bookmark
    'e AbsolutePosition
    rs.CursorType = adOpenStatic
    rs.CursorLocation = adUseServer
    If gModoDebug = mediSim Then BG_LogFile_Erros CriterioTabela
    rs.Open CriterioTabela, gConexao
    
    If rs.RecordCount <= 0 Then
        BL_FimProcessamento Me
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo !", vbExclamation, "Procurar"
        FuncaoLimpar
    Else
        estado = 2
        
        'BRUNODSANTOS HCVP-4619 19.07.2016
        flg_chk_estado_anterior = True
        '
        
        LimpaCampos

        ' Inicio do preenchimento de 'EcLista'
        MarcaInicial = 1
        
        PreencheFGRes
        PreencheCampos
        
        ' Fim do preenchimento de 'EcLista'

        BL_ToolbarEstadoN estado
        BL_Toolbar_BotaoEstado "Remover", "Activo"
        
        BL_FimProcessamento Me
    End If
    
End Sub

Sub FuncaoAnterior()
    
    BL_InicioProcessamento Me, "A pesquisar registo anterior."
    rs.MovePrevious
    
    If rs.BOF Then
        rs.MoveNext
        BL_FimProcessamento Me
        BG_MensagemAnterior
    Else
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoSeguinte()
    
    BL_InicioProcessamento Me, "A pesquisar registo seguinte."
    rs.MoveNext
    
    If rs.EOF Then
        rs.MovePrevious
        BL_FimProcessamento Me
        BG_MensagemSeguinte
    Else
        BL_FimProcessamento Me
    End If

End Sub

Sub FuncaoInserir()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Inserir"
    gMsgMsg = "Tem a certeza que quer Inserir estes dados ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    Me.SetFocus
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A inserir registo."
        EcUtilizadorCriacao = gCodUtilizador
        EcDataCriacao = CDate(Bg_DaData_ADO)
        EcHoraCriacao = CDate(Bg_DaHora_ADO)
        EcSeqAna.Text = BL_HandleNull(BG_DaMAX(NomeTabela, ChaveBD), 0) + 1
        iRes = ValidaCamposEc
        
        If iRes = True Then
            BD_Insert
            GravaLocaisMapeam CLng(EcSeqAna.Text)
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Insert()
    
    Dim SQLQuery As String
    
    gSQLError = 0
    
    SQLQuery = BG_ConstroiCriterio_INSERT_ADO(NomeTabela, CamposBD, CamposEc)
    BG_ExecutaQuery_ADO SQLQuery
    'If GravaAntibioticos(EcCodigo.text) = False Then
    '    Exit Sub
    'End If
    
End Sub

Sub FuncaoModificar()
    
    Dim iRes As Integer
    
    gMsgTitulo = "Modificar"
    gMsgMsg = "Tem a certeza que quer validar as altera��es efectuadas ?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = CDate(Bg_DaData_ADO)
        EcHoraAlteracao = Bg_DaHora_ADO
        EcUtilizadorAlteracao = gCodUtilizador
        EcHoraAlteracao = CDate(Bg_DaHora_ADO)
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Update
            GravaLocaisMapeam CLng(EcSeqAna.Text)
        End If
        BL_FimProcessamento Me
    End If

End Sub

Sub BD_Update()

    Dim condicao As String
    Dim SQLQuery As String
    Dim MarcaLocal As Variant
    'If GravaAntibioticos(EcCodigo.text) = False Then
    '    Exit Sub
    'End If
    gSQLError = 0
    condicao = ChaveBD & " = " & BG_CvPlica(rs(ChaveBD))
    SQLQuery = BG_ConstroiCriterio_UPDATE_ADO(NomeTabela, CamposBD, CamposEc, condicao)
    BG_ExecutaQuery_ADO SQLQuery
        

    'propriedades precisam de ser estas pois � necessario utilizar
    'o Bookmark
    MarcaLocal = rs.Bookmark
    rs.Requery

    If rs.BOF And rs.EOF Then
        FuncaoEstadoAnterior
        Exit Sub
    End If

        
'    If MarcaLocal <= rs.RecordCount Then rs.Move EcLista.ListIndex, MarcaLocal
 '   If MarcaLocal <= rs.RecordCount Then rs.Bookmark = MarcaLocal

End Sub

Sub FuncaoRemover()
    Dim iRes As Boolean
    gMsgTitulo = "Remover"
    gMsgMsg = "Tem a certeza que quer cancelar o registo?"
    gMsgResp = BG_Mensagem(mediMsgBox, gMsgMsg, vbYesNo + vbDefaultButton2 + vbQuestion, gMsgTitulo)
    
    If gMsgResp = vbYes Then
        BL_InicioProcessamento Me, "A modificar registo."
        EcDataAlteracao = CDate(Bg_DaData_ADO & " " & Bg_DaHora_ADO)
        EcHoraAlteracao = CDate(Bg_DaHora_ADO)
        EcUtilizadorAlteracao = gCodUtilizador
        iRes = ValidaCamposEc
        If iRes = True Then
            BD_Delete
        End If
        BL_FimProcessamento Me
    End If
    
End Sub

Private Function ConstroiCriterio() As String
    Dim sSql As String
    sSql = "SELECT x1.seq_ana, x0.cod_ana, x1.cod_ana_gh, x1.flg_so_facturacao, x1.cod_efr, x1.flg_conta_membros, x1.cod_ana_regra, "
    sSql = sSql & " x1.flg_regra, x1.cod_ana_Facturar, x1.flg_Ana_Facturar, x1.perc_Facturar, x1.qtd, x1.centro_custo, "
    sSql = sSql & " x1.flg_marcador, x1.min_marcadores, x1.media_marcadores, x0.descr_ana, x3.descr_efr, x5.cod_portaria, x5.descr_portaria,  "
    sSql = sSql & " x1.user_cri, x1.dt_cri, x1.hr_cri, x1.user_act, x1.dt_act, x1.hr_act, "
    'If gSISTEMA_FACTURACAO = "FACTUS" Or gSISTEMA_FACTURACAO = "GH" Then
        sSql = sSql & " descr_rubr as descr_ana_gh "
    'Else
    '    sSql = sSql & " NULL descr_ana_gh "
    'End If
    sSql = sSql & " FROM slv_analises_factus x0 LEFT OUTER JOIN sl_ana_facturacao x1 ON x0.cod_ana = x1.cod_ana"
    sSql = sSql & " LEFT OUTER JOIN sl_efr x3 ON  x1.cod_efr = x3.cod_efr "
    'If gSISTEMA_FACTURACAO = "FACTUS" Or gSISTEMA_FACTURACAO = "GH" Then
        'CHMA-1439
        'sSql = sSql & " LEFT OUTER JOIN fa_rubr x4 ON  x1.cod_ana_gh = x4.cod_rubr "
        sSql = sSql & " LEFT OUTER JOIN fa_rubr x4 ON  x1.cod_ana_gh = to_char(x4.cod_rubr) "
    'End If
    'sSql = sSql & " ,SL_PORTARIAS x5 "
    sSql = sSql & " LEFT OUTER JOIN sl_portarias x5 ON x1.cod_portaria = x5.cod_portaria "
    'sSql = sSql & " WHERE x1.cod_ana = x1.cod_ana "
    sSql = sSql & " WHERE 1 = 1 "
    If OptTipoPesq(0).value = True Then
        sSql = sSql & " AND x0.cod_ana = x1.cod_ana "
    ElseIf OptTipoPesq(1).value = True Then
    'BRUNODSANTOS 17.06.2016 - HCVP-4619 -> Adicionado alias(x2) � tabela sl_ana_facturacao
        sSql = sSql & " AND x0.cod_ana NOT IN (select x2.cod_ana from sl_ana_facturacao x2 WHERE x2.cod_ana = x0.cod_ana)"
    '
    End If
    'sSql = sSql & " AND x1.cod_portaria = x5.cod_portaria "
    sSql = sSql & ConstroiWHERE & " ORDER BY descr_ana, cod_ana "
    ConstroiCriterio = sSql
End Function

Private Function ConstroiWHERE() As String
    Dim nTipo As Integer
    Dim i As Integer
    Dim nPassou As Integer
    Dim iCount As Integer
    Dim res As Integer
    Dim iTempDecimal As Integer
    Dim Criterio As String
    Dim sTempDecimal As String
    Dim ValorCampo, ValorControl As Variant
    Dim pos As Integer
    
    i = 0
    'Tratar e retirar o conteudo dos campos para criar o criterio
    For Each ValorCampo In CamposBD
        'TextBox, Label, MaskEdBox
        If TypeOf CamposEc(i) Is TextBox Or TypeOf CamposEc(i) Is Label Or TypeOf CamposEc(i) Is MaskEdBox Then
            ValorControl = CamposEc(i)
            ValorControl = BG_CvPlica(ValorControl)
        ElseIf TypeOf CamposEc(i) Is RichTextBox Then
        'RichtTextBox: Se estiver vazia (o TEXT) n�o inclui no crit�rio
        '(caso contr�rio entrava na query a formata��o do texto vazio!)
            If Trim(CamposEc(i).Text) <> "" Then
                ValorControl = CamposEc(i)
                ValorControl = BG_CvPlica(ValorControl)
            Else
                ValorControl = ""
            End If
        'DataCombo
        ElseIf TypeOf CamposEc(i) Is DataCombo Then
            ValorControl = BG_DataComboSel(CamposEc(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        'ComboBox
        ElseIf TypeOf CamposEc(i) Is ComboBox Then
            ValorControl = BG_DaComboSel(CamposEc(i))
            If ValorControl = mediComboValorNull Then
                ValorControl = ""
            End If
        'CheckBox
        ElseIf TypeOf CamposEc(i) Is CheckBox Then
            If CamposEc(i).value = 0 Then
                ValorControl = "0"
            ElseIf CamposEc(i).value = 1 Then
                ValorControl = "1"
            Else
                ValorControl = ""
            End If
        ' Nada ?
        Else
        End If
        
        'se o campo do form esta vazio nao entra no criterio
        If ValorControl = "" Then
            iCount = iCount + 1
        'se o campo do form tem conteudo, formata o conteudo para o statment SQL
        Else
            'controlar o primeiro campo para nao levar virgula
            Criterio = Criterio & " AND "

            If Trim(CamposEc(i).Tag) = "" Then
                'retirar o tipo de campo da base de dados
                BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                nTipo = gFieldObjectProperties(0).tipo
            Else
                'caso j� esteja defenido
                pos = InStr(CamposEc(i).Tag, "(")
                If pos <> 0 Then
                    'tratar o decimal
                    nTipo = CInt(Mid(CamposEc(i).Tag, 1, pos - 1))
                Else
                    nTipo = CInt(CamposEc(i).Tag)
                End If
                
                'verificar se n�o se trata duma defini��o local
                If nTipo = mediTipoCapitalizado Or nTipo = mediTipoData _
                   Or nTipo = mediTipoDefeito Or nTipo = mediTipoHora _
                   Or nTipo = mediTipoMaiusculas Or nTipo = mediTipoMaiusculas _
                   Or nTipo = mediTipoMinusculas Then
                    BGAux_DaFieldObjectProperties_ADO NomeTabela, ValorCampo
                    nTipo = gFieldObjectProperties(0).tipo
                End If
            End If

        
            Select Case nTipo
            'numericos: adDecimal, adNumeric, adSingle, adVarNumeric,
            '           adDouble, adInteger
            Case adDecimal, adNumeric, adSingle, adVarNumeric, adDouble, adInteger
                Criterio = Criterio & "x0." & ValorCampo & " = " & BG_CvDecimalParaWhere_ADO(ValorControl)
            'data e hora: adDate, adDBDate, adDBTimeStamp, adDBTime
            Case adDate, adDBDate, adDBTimeStamp, adDBTime
                If CamposEc(i).Tag = CStr(mediTipoHora) Then
                    'hora
                    Criterio = Criterio & "x0." & ValorCampo & " = " & "'" & BG_CvHora(ValorControl) & "'"
                Else
                    'data
                    Criterio = Criterio & "x0." & ValorCampo & " = " & "'" & BG_CvDataParaWhere_ADO(ValorControl) & "'"
                End If
            'string: adBSTR, adChar, adCurrency, adLongVarChar, adLongVarWChar,
            '        adVarChar, adVariant, adVarWChar, adWChar
            '(controlar os wilcards - '*' e '?' )
            Case adBSTR, adChar, adCurrency, adLongVarChar, adLongVarWChar, adVarChar, adVariant, adVarWChar, adWChar
                If InStr(ValorControl, "*") <> 0 Or InStr(ValorControl, "?") <> 0 Then
                    ValorControl = BG_CvWilcard(ValorControl)
                    Criterio = Criterio & "x0." & ValorCampo & " LIKE " & "'" & ValorControl & "'"
                Else
                    Criterio = Criterio & "x0." & ValorCampo & " = " & "'" & ValorControl & "'"
                End If
            'outro
            Case Else
                Criterio = Criterio & "x0." & ValorCampo & " = " & ValorControl
            End Select
            
            nPassou = 1
        End If
        
        i = i + 1
    Next
    ConstroiWHERE = Criterio
End Function

' ------------------------------------------------------------------------------------------------

' CARREGA LOCAIS ASSOCIADOS A PROVENIENCIA

' ------------------------------------------------------------------------------------------------
Public Sub CarregaLocaisMapeam(seq_ana As String)
    Dim sSql As String
    Dim rsAnaLocais As New ADODB.recordset
    On Error GoTo TrataErro
    Dim i As Integer
    
    sSql = "SELECT * FROM sl_ass_ana_fact_locais WHERE seq_ana = " & seq_ana
    Set rsAnaLocais = BG_ExecutaSELECT(sSql)
    If rsAnaLocais.RecordCount > 0 Then
        While Not rsAnaLocais.EOF
            For i = 0 To EcLocais.ListCount - 1
                If EcLocais.ItemData(i) = BL_HandleNull(rsAnaLocais!cod_local, "") Then
                    EcLocais.Selected(i) = True
                End If
            Next
            rsAnaLocais.MoveNext
        Wend
    End If
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao carregar locais: " & sSql & " " & Err.Description, Me.Name, "CarregaLocaisMapeam", False
    Exit Sub
    Resume Next
End Sub


' ------------------------------------------------------------------------------------------------

' GRAVA LOCAIS ASSOCIADOS A

' ------------------------------------------------------------------------------------------------

Public Sub GravaLocaisMapeam(seq_ana As Long)
    Dim sSql As String
    Dim i As Integer
    On Error GoTo TrataErro
    
    sSql = "DELETE FROM sl_ass_ana_fact_locais WHERE seq_ana = " & seq_ana
    BG_ExecutaQuery_ADO sSql
    
    For i = 0 To EcLocais.ListCount - 1
        If EcLocais.Selected(i) = True Then
            sSql = "INSERT into sl_ass_ana_fact_locais (seq_ana, cod_local) VALUES("
            sSql = sSql & (seq_ana) & ","
            sSql = sSql & EcLocais.ItemData(i) & ")"
            BG_ExecutaQuery_ADO sSql
        End If
    Next
Exit Sub
TrataErro:
    BG_LogFile_Erros "Erro  ao Gravar locais: " & sSql & " " & Err.Description, Me.Name, "GravaLocaisMapeam", False
    Exit Sub
    Resume Next
End Sub

Private Sub LimpaFgRes()
    Dim i As Integer
    FgRes.rows = 2
    For i = 0 To FgRes.Cols - 1
        FgRes.TextMatrix(1, i) = ""
    Next i
End Sub


Public Sub PreencheFGRes()

' Preenche uma listbox com varios campos da BD

    Dim NomeCampoBD As Variant
    Dim Marca As Variant
    Dim sLinha As String
    Dim bValorNaCombo As Boolean
    Dim sValorParaMostrar As String
    Dim i As Long
    Dim j, k, l, m As Integer
    Dim iAux As Integer
    Dim CampoAux As Variant
    Dim bEncontrou As Boolean
    Dim lPosicao, lNumTotal As Long

    On Error GoTo TrataErro

    lPosicao = FgRes.row
    lNumTotal = FgRes.rows
    
    LimpaFgRes
    FgRes.Visible = False
    Marca = rs.Bookmark
    
    i = 1
    Do Until rs.EOF
        j = 0
        For Each NomeCampoBD In CamposBDparaListBox
            sLinha = ""
            If (IsNull(rs(NomeCampoBD))) Or (rs(NomeCampoBD) = "") Then
                sLinha = sLinha & String(NumEspacos(j), " ")
            Else

                ' In�cio: Trata Campos do tipo ComboBox e DataCombo
                k = 0
                bEncontrou = False
                For Each CampoAux In CamposBDparaListBox
                    If NomeCampoBD = CampoAux Then
                        bEncontrou = True
                        Exit For
                    End If
                    k = k + 1
                Next
                
                If bEncontrou = True Then
                    'ComboBox
                    If TypeOf CamposEc(k) Is ComboBox Then
                        iAux = CInt(rs(NomeCampoBD))
                        For l = 0 To CamposEc(k).ListCount - 1
                            If iAux = CamposEc(k).ItemData(l) Then
                                bValorNaCombo = True
                                sValorParaMostrar = CamposEc(k).List(l)
                                Exit For
                            End If
                        Next
                     End If
                End If
                
                ' Fim: Trata Campos do tipo ComboBox e DataCombo
IGNORA_COMBO:
                If bValorNaCombo = False Then
                    sValorParaMostrar = rs(NomeCampoBD)

                    ' In�cio: Trata convers�es para Decimal, Data e Hora
                    m = 0
                    For Each CampoAux In CamposBDparaListBox
                        If NomeCampoBD = CampoAux Then

                            ' adDouble, adDecimal
                            If left(FgRes.Tag, 2) = adDecimal Or left(FgRes.Tag, 1) = adDouble Or left(FgRes.Tag, 3) = adNumeric Then
                                sValorParaMostrar = BG_CvDecimalParaDisplay_ADO(sValorParaMostrar)
                            ElseIf CamposEc(m).Tag = mediTipoData Then
                                sValorParaMostrar = BG_CvDataParaDisplay_ADO(sValorParaMostrar)
                            ElseIf CamposEc(m).Tag = mediTipoHora Then
                                sValorParaMostrar = BG_CvHora(sValorParaMostrar)
                            End If
                            
                            Exit For
                        End If
                        m = m + 1
                    Next
                    ' Fim: Trata convers�es para Decimal, Data e Hora
                End If

                sLinha = sLinha & sValorParaMostrar
            End If
            FgRes.TextMatrix(i, j) = sLinha
            j = j + 1
        Next
        FgRes.AddItem sLinha
        rs.MoveNext
        i = i + 1
        If i > 32767 Then
            BG_Mensagem mediMsgBox, "N�o � poss�vel mostrar nesta lista mais do que " & i - 1 & " registos!", vbExclamation
            Exit Do
        End If
    Loop
    
    FgRes.row = 0
    
    
    rs.Bookmark = Marca
    FgRes.Visible = True
    Exit Sub
    
TrataErro:
    Err.Clear
    Resume Next

End Sub

Private Sub BtPesquisaEntFin_Click()
   PA_PesquisaEFR EcCodEfr, EcDescrEfr, ""
    
End Sub

Public Sub EcCodEFR_Validate(Cancel As Boolean)
    Cancel = PA_ValidateEFR(EcCodEfr, EcDescrEfr, "")
End Sub

Private Sub BtPesquisaRubr_Click()
   PA_PesquisaRubr EcCodGH, EcDescrGH, ""
End Sub

Public Sub EcCodGH_Validate(Cancel As Boolean)
    If HIS.nome = cFACTURACAO_NOVAHIS Or HIS.nome = cFACTURACAO_SONHO Then
        Exit Sub
    End If
    Cancel = PA_ValidateRubr(EcCodGH, EcDescrGH, "")
End Sub
Private Sub BtPesquisaAnaFact_Click()
   PA_PesquisaAnaFact EcCodAna, EcDescrAna, ""
End Sub

Public Sub EcCodana_Validate(Cancel As Boolean)
    EcCodAna.Text = UCase(EcCodAna.Text)
    Cancel = PA_ValidateAnaFact(EcCodAna, EcDescrAna, "")

End Sub
Public Sub EcCodPortaria_Validate(Cancel As Boolean)
    EcCodPortaria.Text = UCase(EcCodPortaria.Text)
    Cancel = PA_ValidatePortaria(EcCodPortaria, EcDescrPortaria, "")

End Sub
Private Sub BtPesquisaPortaria_Click()
   PA_PesquisaPortaria EcCodPortaria, EcDescrPortaria, ""
End Sub


' Here you can add scrolling support to controls that don't normally respond
Public Sub MouseWheel(ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
  Dim ctl As Control
  
  For Each ctl In Me.Controls
    If TypeOf ctl Is MSFlexGrid Then
      If IsOver(ctl.hwnd, Xpos, Ypos) Then FlexGridScroll ctl, MouseKeys, Rotation, Xpos, Ypos
    End If
  Next ctl
End Sub

Sub BD_Delete()
    Dim linha As Integer
    Dim condicao As String
    Dim SQLQuery As String
           
    condicao = ChaveBD & " = '" & BG_CvPlica(rs(ChaveBD)) & "'"
    SQLQuery = "DELETE FROM " & NomeTabela & " WHERE " & condicao
    BG_ExecutaQuery_ADO SQLQuery
    linha = FgRes.row
    FgRes.RemoveItem FgRes.row
    FgRes.row = linha
    If rs.BOF And rs.EOF Then
        BG_Mensagem mediMsgBox, "O registo desactivado era �nico nesta pesquisa!", vbExclamation, "Desactivar"
        FuncaoLimpar
        Exit Sub
    End If
    

End Sub

Attribute VB_Name = "RNU"
Option Explicit
Private Type EntidadeResponsavel
    codigo As String
    descricao As String
    NumeroBenefEntidade As String
End Type
Private Type isencao
    Motivo As String
    descricao As String
    DataInicio As String
    DataFim As String
End Type
Private Type beneficio
    IsencaoTaxa As isencao
End Type
Private Type identificacao
    NumeroSNS As String
    NomeCompleto As String
    NomesProprios As String
    Apelidos As String
    DataNascimento As String
    Sexo As String
    PaisNacionalidade As String
    Obito As String
    Duplicado As String
    'BRUNODSANTOS GLINTT-HS-15750 27.09.2017
    CodProfissao As String
    DescrProfissao As String
    codPais As String
    DescrPais As String
    NDocIdentif As String
    '
End Type

Public Type identifRNU
    identificacao As identificacao
    EntidadeResponsavel As EntidadeResponsavel
    beneficios As beneficio
End Type
Dim idRNU As identifRNU

' ----------------------------------------------------------------------------------------------

' CONSTRU��O DO XML

' ----------------------------------------------------------------------------------------------
Public Function RNU_GetPatientBySNS(SNS As String) As identifRNU
    Dim seq_log_rnu As Long
    Dim strSoapAction As String
    Dim strXml As String
    Dim strParam As String
    Dim RetornoXML As String
    Dim urlRNU As String
    On Error GoTo TrataErro
    RNU_InicializaEstrut idRNU
    urlRNU = BL_DevolveURLWebService("RNU")
    strParam = "MyParameterString"
    If urlRNU = "" Then
        Exit Function
    End If
    
    
     'BRUNODSANTOS GLINTT-HS-15750 27.09.2017
    'strSoapAction = "http://www.glintths.com/pe/rnu/GetPatientBySNS"
   
'    strXml = ""
'    strXml = strXml & "<?xml version=""1.0"" encoding=""utf-8""?>"
'    strXml = strXml & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
'    strXml = strXml & "     <soap:Body>"
'    strXml = strXml & "         <GetPatientBySNS xmlns=""http://www.glintths.com/pe/rnu"">"
'    strXml = strXml & "             <sns>" & SNS & "</sns>"
'    strXml = strXml & "         </GetPatientBySNS>"
'    strXml = strXml & "     </soap:Body>"
'    strXml = strXml & "</soap:Envelope>"

    strXml = ""
    strXml = strXml & "<?xml version=""1.0"" encoding=""utf-8""?>"
    strXml = strXml & "<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:glin=""http://www.glintths.com"">"
    strXml = strXml & "     <soap:Body>"
    strXml = strXml & "         <glin:GetPatientBySNS>"
    strXml = strXml & "             <glin:sns>" & SNS & "</glin:sns>"
    strXml = strXml & "         </glin:GetPatientBySNS>"
    strXml = strXml & "     </soap:Body>"
    strXml = strXml & "</soap:Envelope>"

    seq_log_rnu = RNU_InsereLog(strXml, urlRNU)
    If seq_log_rnu > mediComboValorNull Then
        RetornoXML = RNU_PostWebservice(urlRNU, strSoapAction, strXml)
        RNU_ConstroiEstrutura idRNU, UCase(RetornoXML)
        RNU_ActualizaLog seq_log_rnu, RetornoXML
    End If
    RNU_GetPatientBySNS = idRNU
Exit Function
TrataErro:
    BG_LogFile_Erros "RNU_GetPatientBySNS: " & Err.Description, "RNU", "RNU_GetPatientBySNS", True
    Exit Function
    Resume Next
End Function


' ----------------------------------------------------------------------------------------------

' CHAMADA AO WEBSERVICE

' ----------------------------------------------------------------------------------------------
Public Function RNU_PostWebservice(ByVal AsmxUrl As String, ByVal SoapActionUrl As String, ByVal XmlBody As String) As String
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim strRet As String
    Dim intPos1 As Long
    Dim intPos2 As Long
    
    On Error GoTo TrataErro
    
    ' Create objects to DOMDocument and XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
    
    ' Load XML
    objDom.async = False
    objDom.loadXML XmlBody

    ' Open the webservice
    objXmlHttp.Open "POST", AsmxUrl, False
    
    ' Create headings
    objXmlHttp.SetRequestHeader "Content-Type", "text/xml; charset=utf-8"
    objXmlHttp.SetRequestHeader "SOAPAction", SoapActionUrl
    
    ' Send XML command
    objXmlHttp.Send objDom.xml

    ' Get all response text from webservice
    strRet = objXmlHttp.ResponseText
    
    ' Close object
    Set objXmlHttp = Nothing
    
    ' Return result
    RNU_PostWebservice = strRet
    
Exit Function
TrataErro:
    RNU_PostWebservice = ""
    BG_LogFile_Erros "RNU_PostWebservice: " & Err.Description, "RNU", "RNU_PostWebservice", False
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------

' INSERE LOG NA TABELA DE LOG COM A CHAMADA AO WEBSERVICE

' ----------------------------------------------------------------------------------------------
Public Function RNU_InsereLog(xml_request As String, xml_url As String) As Long
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsLog As New adodb.recordset
    Dim seq_log As Long
    Dim iReg As Integer
    RNU_InsereLog = mediComboValorNull
    ssql = "SELECT seq_log_rnu.nextval proximo FROM dual"
    rsLog.CursorLocation = adUseServer
    rsLog.CursorType = adOpenStatic
    rsLog.Open ssql, gConexao
    If rsLog.RecordCount = 1 Then
        seq_log = BL_HandleNull(rsLog!proximo, "")
    End If
    rsLog.Close
    Set rsLog = Nothing
    If seq_log > mediComboValorNull Then
        ssql = "INSERT INTO sl_log_rnu(Seq_log_rnu, xml_request,xml_url, dt_cri, user_Cri) VALUES("
        ssql = ssql & seq_log & "," & BL_TrataStringParaBD(xml_request) & "," & BL_TrataStringParaBD(xml_url) & ","
        ssql = ssql & " sysdate," & BL_TrataStringParaBD(CStr(gCodUtilizador)) & ")"
        iReg = BG_ExecutaQuery_ADO(ssql)
        If iReg = 1 Then
            RNU_InsereLog = seq_log
        End If
    End If
Exit Function
TrataErro:
    RNU_InsereLog = mediComboValorNull
    BG_LogFile_Erros "RNU_InsereLog: " & Err.Description, "RNU", "RNU_InsereLog", True
    Exit Function
    Resume Next
End Function

' ----------------------------------------------------------------------------------------------

' ACTUALIZA A TABELA DE LOG COM A RESPOSTA DO WEBSERVICE

' ----------------------------------------------------------------------------------------------
Public Function RNU_ActualizaLog(seq_log As Long, xml_response As String) As Boolean
    On Error GoTo TrataErro
    Dim ssql As String
    Dim rsLog As New adodb.recordset
    Dim iReg As Integer
    
    RNU_ActualizaLog = False
    If seq_log > mediComboValorNull Then
        ssql = "UPDATE sl_log_rnu SET xml_response = " & BL_TrataStringParaBD(xml_response) & ","
        ssql = ssql & " dt_act = sysdate, user_act = " & BL_TrataStringParaBD(CStr(gCodUtilizador))
        ssql = ssql & " WHERE seq_log_rnu = " & seq_log
        iReg = BG_ExecutaQuery_ADO(ssql)
        If iReg = 1 Then
            RNU_ActualizaLog = True
        End If
    End If
Exit Function
TrataErro:
    RNU_ActualizaLog = False
    BG_LogFile_Erros "RNU_ActualizaLog: " & Err.Description, "RNU", "RNU_ActualizaLog", True
    Exit Function
    Resume Next
End Function



Private Function RNU_ConstroiEstrutura(estrutRNU As identifRNU, textoXML As String) As Boolean
     On Error GoTo TrataErro
     RNU_ConstroiEstrutura = False
     If RNU_ConstroiEstruturaIdentif(estrutRNU, textoXML) = False Then
        GoTo TrataErro
    End If
     If RNU_ConstroiEstruturaBeneficios(estrutRNU, textoXML) = False Then
        GoTo TrataErro
    End If
    RNU_ConstroiEstrutura = True
Exit Function
TrataErro:
    RNU_ConstroiEstrutura = False
    BG_LogFile_Erros "RNU_ConstroiEstrutura: " & Err.Description, "RNU", "RNU_ConstroiEstrutura", True
    Exit Function
    Resume Next
End Function

Private Function RNU_ConstroiEstruturaIdentif(estrutRNU As identifRNU, textoXML As String) As Boolean
    Dim varString As String
    On Error GoTo TrataErro
    RNU_ConstroiEstruturaIdentif = False
    
    'NUMERO SNS
    estrutRNU.identificacao.NumeroSNS = RNU_RetornaConteudoTag(textoXML, "NUMEROSNS")
    ' NOME COMPLETO
    estrutRNU.identificacao.NomeCompleto = RNU_RetornaConteudoTag(textoXML, "NOMECOMPLETO")
    ' NOME PROPRIO
    estrutRNU.identificacao.NomesProprios = RNU_RetornaConteudoTag(textoXML, "NOMESPROPRIOS")
    ' APELIDO
    estrutRNU.identificacao.Apelidos = RNU_RetornaConteudoTag(textoXML, "APELIDOS")
    ' DATA NASCIMENTO
    estrutRNU.identificacao.DataNascimento = RNU_RetornaConteudoTag(textoXML, "DATANASCIMENTO")
    estrutRNU.identificacao.DataNascimento = Format(Mid(estrutRNU.identificacao.DataNascimento, 1, 10), "DD-MM-YYYY")
    'SEXO UTENTE
    estrutRNU.identificacao.Sexo = RNU_RetornaConteudoTag(textoXML, "SEXO")
    If estrutRNU.identificacao.Sexo = "F" Then
        estrutRNU.identificacao.Sexo = gT_Feminino
    Else
        estrutRNU.identificacao.Sexo = gT_Masculino
    End If
    ' NACIONALIDADE
    estrutRNU.identificacao.PaisNacionalidade = RNU_RetornaConteudoTag(textoXML, "PAISNACIONALIDADE")
    ' OBITO
    estrutRNU.identificacao.Obito = RNU_RetornaConteudoTag(textoXML, "OBITO")
    ' DUPLICADO
    estrutRNU.identificacao.Duplicado = RNU_RetornaConteudoTag(textoXML, "DUPLICADO")
    'BRUNODSANTOS GLINTT-HS-15750 27.09.2017
    'CODIGO PROFISS�O
    estrutRNU.identificacao.CodProfissao = RNU_RetornaConteudoTag(textoXML, "CodigoProfissao")
    '
    'DESCRI��O PROFISS�O
    estrutRNU.identificacao.DescrProfissao = RNU_RetornaConteudoTag(textoXML, "DescricaoProfissao")
    '
    'CODIGO PA�S
    estrutRNU.identificacao.codPais = RNU_RetornaConteudoTag(textoXML, "PaisNaturalidade")
    '
    'DESCRI��O PA�S
    estrutRNU.identificacao.DescrPais = RNU_RetornaConteudoTag(textoXML, "DescricaoPaisNaturalidade")
    '
    RNU_ConstroiEstruturaIdentif = True
Exit Function
TrataErro:
    RNU_ConstroiEstruturaIdentif = False
    BG_LogFile_Erros "RNU_ConstroiEstruturaIdentif: " & Err.Description, "RNU", "RNU_ConstroiEstruturaIdentif", True
    Exit Function
    Resume Next
End Function

Private Function RNU_ConstroiEstruturaBeneficios(estrutRNU As identifRNU, textoXML As String) As Boolean
    Dim varString As String
    Dim beneficios As String
    Dim IsencaoTaxa As String
    On Error GoTo TrataErro
    RNU_ConstroiEstruturaBeneficios = False
    
    estrutRNU.beneficios.IsencaoTaxa.Motivo = ""
    estrutRNU.beneficios.IsencaoTaxa.descricao = ""
    estrutRNU.beneficios.IsencaoTaxa.DataInicio = ""
    estrutRNU.beneficios.IsencaoTaxa.DataInicio = ""
    estrutRNU.beneficios.IsencaoTaxa.DataFim = ""
    estrutRNU.beneficios.IsencaoTaxa.DataFim = ""
    
    beneficios = RNU_RetornaConteudoTag(textoXML, "BENEFICIOS")
    If beneficios = "" Then
        RNU_ConstroiEstruturaBeneficios = True
        Exit Function
    End If
    IsencaoTaxa = RNU_RetornaConteudoTag(textoXML, "ISENCAOTAXA")
    If IsencaoTaxa = "" Then
        RNU_ConstroiEstruturaBeneficios = True
        Exit Function
    End If
    
    ' isencao
    estrutRNU.beneficios.IsencaoTaxa.Motivo = RNU_RetornaConteudoTag(IsencaoTaxa, "MOTIVO")
    estrutRNU.beneficios.IsencaoTaxa.descricao = RNU_RetornaConteudoTag(IsencaoTaxa, "DESCRICAO")
    estrutRNU.beneficios.IsencaoTaxa.DataInicio = RNU_RetornaConteudoTag(IsencaoTaxa, "DATAINICIO")
    estrutRNU.beneficios.IsencaoTaxa.DataInicio = Format(Mid(estrutRNU.beneficios.IsencaoTaxa.DataInicio, 1, 10), "DD-MM-YYYY")
    estrutRNU.beneficios.IsencaoTaxa.DataFim = RNU_RetornaConteudoTag(IsencaoTaxa, "DATAFIM")
    estrutRNU.beneficios.IsencaoTaxa.DataFim = Format(Mid(estrutRNU.beneficios.IsencaoTaxa.DataFim, 1, 10), "DD-MM-YYYY")
    RNU_ConstroiEstruturaBeneficios = True
Exit Function
TrataErro:
    RNU_ConstroiEstruturaBeneficios = False
    BG_LogFile_Erros "RNU_ConstroiEstruturaBeneficios: " & Err.Description, "RNU", "RNU_ConstroiEstruturaBeneficios", True
    Exit Function
    Resume Next
End Function

Public Function RNU_RetornaConteudoTag(textoXML As String, variavel As String) As String
    On Error GoTo TrataErro
    Dim iInicio As Long
    Dim iTamanho As Long
    If InStr(1, textoXML, "<" & variavel & ">") > 0 Then
       iInicio = InStr(1, textoXML, "<" & variavel & ">") + Len("<" & variavel & ">")
       iTamanho = InStr(1, textoXML, "</" & variavel & ">") - iInicio
       RNU_RetornaConteudoTag = Mid(textoXML, iInicio, iTamanho)
    End If
    
Exit Function
TrataErro:
    RNU_RetornaConteudoTag = ""
    BG_LogFile_Erros "RNU_RetornaConteudoTag: " & Err.Description, "RNU", "RNU_RetornaConteudoTag", True
    Exit Function
    Resume Next
End Function

Private Function RNU_InicializaEstrut(estrutRNU As identifRNU)
    
    estrutRNU.identificacao.Apelidos = ""
    estrutRNU.identificacao.DataNascimento = ""
    estrutRNU.identificacao.Duplicado = ""
    estrutRNU.identificacao.NomeCompleto = ""
    estrutRNU.identificacao.NomesProprios = ""
    estrutRNU.identificacao.NumeroSNS = ""
    estrutRNU.identificacao.Obito = ""
    estrutRNU.identificacao.PaisNacionalidade = ""
    estrutRNU.identificacao.Sexo = ""
End Function

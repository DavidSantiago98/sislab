Attribute VB_Name = "FolhaResumo"
Option Explicit

Private Type dados_ute
    ObsUtente As String
    n_Req_assoc As String
End Type
Dim Utente As dados_ute


' ----------------------------------------------------------------------

' IMPRIME FOLHA DE RESUMO PARA IMPRESSORA DE RECIBOS

' ----------------------------------------------------------------------
Public Sub FR_ImprimeResumoParaImpressoraRecibos(NumRequis As String)
    Dim sSql
    Dim rsUte As ADODB.recordset
    Dim PrinterResumo As String
    Dim nome_ute As String
    Dim dt_nasc_ute As String
    Dim dt_chega As String
    Dim Utente As String
    Dim dt_conclusao As String
    Dim dt_pretendida As String
    Dim n_Req_assoc As String
    Dim analises As String
    Dim valorDoente As Double
    Dim valorEntidade As Double
    Dim nif As String
    Dim codEfr As String
    Dim isencao As String
    Dim codAnaEfr As String
    Dim descrAnaEfr As String
    Dim precoUnit As Double
    Dim TUtente As String
    Dim nrC As String
    Dim v As String
    Dim analises_efr As String
    Dim totalRec As String
    On Error GoTo TrataErro
    
    Set rsUte = New ADODB.recordset
    rsUte.CursorLocation = adUseClient
    rsUte.CursorType = adOpenStatic
    rsUte.Source = "SELECT  x1.nome_ute, x1.dt_nasc_ute, x2.dt_chega,x1.t_utente, x1.utente, x2.dt_conclusao, x2.dt_pretend, "
    rsUte.Source = rsUte.Source & " x2.cod_Efr, x2.cod_sala, x2.t_urg, x2.n_req_assoc,x1.n_contrib_ute, x2.cod_efr, x2.cod_isencao FROM sl_identif x1, sl_requis x2 "
    rsUte.Source = rsUte.Source & " WHERE x2.n_req = " & NumRequis & " AND x1.seq_utente = x2.seq_utente "
    If gModoDebug = mediSim Then BG_LogFile_Erros rsUte.Source
    rsUte.Open , gConexao
    If rsUte.RecordCount <= 0 Then
        Exit Sub
    End If
    
    nome_ute = BL_HandleNull(rsUte!nome_ute, "")
    dt_nasc_ute = BL_HandleNull(rsUte!dt_nasc_ute, "")
    dt_chega = BL_HandleNull(rsUte!dt_chega, "")
    Utente = BL_HandleNull(rsUte!Utente, "")
    TUtente = BL_HandleNull(rsUte!t_utente, "")
    dt_conclusao = BL_HandleNull(rsUte!dt_conclusao, "")
    dt_pretendida = BL_HandleNull(rsUte!dt_pretend, "")
    n_Req_assoc = BL_HandleNull(rsUte!n_Req_assoc, "")
    nif = BL_HandleNull(rsUte!n_contrib_ute, "")
    codEfr = BL_HandleNull(rsUte!cod_efr, "")
    isencao = BL_HandleNull(rsUte!cod_isencao, gTipoIsencaoNaoIsento)
    rsUte.Close
    sSql = "SELECT distinct a.cod_ana cod_agrup, a.descr_ana, b.ordem_marcacao ord_marca, b.quantidade,b.cod_efr FROM  slv_analises a,sl_Recibos_det b"
    sSql = sSql & " WHERE (a.seq_sinonimo is null or a.seq_sinonimo = 0) AND b.n_req = " & NumRequis
    sSql = sSql & " AND  a.cod_ana = b.cod_facturavel  "
    If gLAB = "LHL" Then
        sSql = sSql & " AND b.cod_efr = " & codEfr
    End If
    sSql = sSql & " AND  (flg_retirado = 0 or flg_retirado is null) and (flg_adicionada = 0  or flg_adicionada is null) and (flg_marcacao_retirada = 0  or flg_marcacao_retirada is null) "
    sSql = sSql & " ORDER BY ord_marca "
    rsUte.CursorLocation = adUseClient
    rsUte.CursorType = adOpenStatic
    rsUte.Source = sSql
    If gModoDebug = mediSim Then BG_LogFile_Erros rsUte.Source
    rsUte.Open , gConexao
    If rsUte.RecordCount <= 0 Then
        Exit Sub
    End If
    analises = ""
    valorEntidade = 0
    While Not rsUte.EOF
        If isencao = gTipoIsencaoIsento Then
            v = 0
        Else
            v = IF_RetornaTaxaAnaliseEntidade(BL_HandleNull(rsUte!cod_agrup, ""), BL_HandleNull(rsUte!cod_efr, codEfr), dt_chega, codAnaEfr, _
                               TUtente, Utente, precoUnit, BL_HandleNull(rsUte!quantidade, 1), nrC, False, descrAnaEfr, mediComboValorNull)
        End If
        valorEntidade = valorEntidade + CDbl(Replace(v, ".", ","))
        'analises_efr = analises_efr & Mid(BL_HandleNull(rsUte!cod_agrup, "") & "-" & BL_HandleNull(rsUte!descr_ana, ""), 1, 34) & v & vbCrLf
        'analises_efr = analises_efr & Mid(BL_HandleNull(codAnaEfr, "") & "-" & BL_HandleNull(rsUte!descr_ana, ""), 1, 34) & Replace(v, ".", ",") & vbCrLf
        analises_efr = analises_efr & Mid(BL_HandleNull(codAnaEfr, "") & "-" & BL_HandleNull(rsUte!descr_ana, ""), 1, 34) & Space(40 - Len(Mid(BL_HandleNull(codAnaEfr, "") & "-" & BL_HandleNull(rsUte!descr_ana, ""), 1, 34)) - Len(Replace(v, ".", ","))) & IIf(Replace(v, ".", ",") = "0", "", Replace(v, ".", ",")) & vbCrLf
        analises = analises & Mid(BL_HandleNull(rsUte!cod_agrup, "") & "-" & BL_HandleNull(rsUte!descr_ana, ""), 1, 26) & vbCrLf
        
        rsUte.MoveNext
    Wend
    rsUte.Close
    
    sSql = "SELECT total_pagar FROM sl_recibos WHERE n_req = " & NumRequis
    sSql = sSql & " AND estado <> 'A' "
    rsUte.CursorLocation = adUseClient
    rsUte.CursorType = adOpenStatic
    rsUte.Source = sSql
    If gModoDebug = mediSim Then BG_LogFile_Erros rsUte.Source
    rsUte.Open , gConexao
    valorDoente = 0
    If rsUte.RecordCount > 0 Then
        While Not rsUte.EOF
            valorDoente = valorDoente + CDbl(Replace(BL_HandleNull(rsUte!total_pagar, 0), ".", ","))
            rsUte.MoveNext
        Wend
    End If
    rsUte.Close
    
    If Not BL_LerEtiqIni("FOLHARESUMO" & "_" & codEfr & ".INI") Then
        If Not BL_LerEtiqIni("FOLHARESUMO.ini") Then
            MsgBox "Ficheiro de inicializa��o da Folha de Resumo ausente!"
            Exit Sub
        End If
    End If
    PrinterResumo = BL_SelImpressora("FolhaResumo.rpt")
    
    If Not FR_EtiqOpenPrinter(PrinterResumo) Then
        MsgBox "Imposs�vel abrir impressora de Folhas de Resumo"
        Exit Sub
    End If
    
    totalRec = Space(40 - 29 - Len(CStr(valorEntidade))) & IIf(CStr(valorEntidade) = "0", "", CStr(valorEntidade))
    Call FR_EtiqPrintResumo(NumRequis, n_Req_assoc, dt_chega, Utente, nome_ute, dt_nasc_ute, dt_pretendida, dt_conclusao, analises, _
                            CStr(valorDoente), nif, analises_efr, totalRec)
   

    FR_EtiqClosePrinter
    
    Set rsUte = Nothing

Exit Sub
TrataErro:
    BG_LogFile_Erros "FR_ImprimeResumoParaImpressoraRecibos: " & Err.Number & " - " & Err.Description, "FR", "FR_ImprimeResumoParaImpressoraRecibos", True
    Exit Sub
    Resume Next
End Sub

Private Function FR_EtiqPrintResumo(n_req As String, n_Req_assoc As String, dt_chega As String, Utente As String, _
                                 nome_ute As String, dt_nasc_ute As String, dt_pretendida As String, dt_conclusao As String, _
                                 analises As String, valor As String, nif As String, analises_efr As String, valor_efr As String) As Boolean
     On Error GoTo TrataErro
   
    'soliveira terrugem n_req_assoc
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String


    sWrittenData = BL_TrocaBinarios(EtiqJob)
    sWrittenData = Replace(sWrittenData, "{N_REQ}", n_req)
    sWrittenData = Replace(sWrittenData, "{N_REQ_ASSOC}", n_Req_assoc)
    sWrittenData = Replace(sWrittenData, "{DT_CHEGA}", dt_chega)
    sWrittenData = Replace(sWrittenData, "{UTENTE}", Utente)
    sWrittenData = Replace(sWrittenData, "{NOME_UTE}", nome_ute)
    sWrittenData = Replace(sWrittenData, "{DT_NASC_UTE}", dt_nasc_ute)
    sWrittenData = Replace(sWrittenData, "{DT_PRETENDIDA}", dt_pretendida)
    sWrittenData = Replace(sWrittenData, "{DT_CONCLUSAO}", dt_conclusao)
    sWrittenData = Replace(sWrittenData, "{ANALISES}", analises)
    sWrittenData = Replace(sWrittenData, "{ANALISES_EFR}", analises_efr)
    sWrittenData = Replace(sWrittenData, "{VALOR}", valor)
    sWrittenData = Replace(sWrittenData, "{VALOR_EFR}", valor_efr)
    sWrittenData = Replace(sWrittenData, "{NIF}", nif)
    
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    FR_EtiqPrintResumo = True
Exit Function
TrataErro:
    BG_LogFile_Erros "FR_EtiqPrintResumo: " & Err.Number & " - " & Err.Description, "FR", "FR_EtiqPrintResumo", False
    Exit Function
    Resume Next
End Function



Private Function FR_EtiqOpenPrinter(ByVal PrinterName As String) As Boolean
    On Error GoTo TrataErro
    
    Dim lReturn As Long
    Dim MyDocInfo As DOCINFO
    Dim lpcWritten As Long
    Dim sWrittenData As String
    
    lReturn = OpenPrinter(PrinterName, EtiqHPrinter, 0)
    If lReturn = 0 Then Exit Function
    
    MyDocInfo.pDocName = "Etiquetas"
    MyDocInfo.pOutputFile = vbNullString
    MyDocInfo.pDatatype = vbNullString
    lReturn = StartDocPrinter(EtiqHPrinter, 1, MyDocInfo)
    If lReturn = 0 Then Exit Function
    
    lReturn = StartPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    sWrittenData = BL_TrocaBinarios(EtiqStartJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    
    FR_EtiqOpenPrinter = True
Exit Function
TrataErro:
    BG_LogFile_Erros "FR_EtiqOpenPrinter: " & Err.Number & " - " & Err.Description, "FR", "FR_EtiqOpenPrinter"
    Exit Function
    Resume Next
End Function



Private Function FR_EtiqClosePrinter() As Boolean
    On Error GoTo TrataErro
    
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String

    sWrittenData = BL_TrocaBinarios(EtiqEndJob)
    If sWrittenData <> "" Then
        lReturn = WritePrinter(EtiqHPrinter, ByVal sWrittenData, _
           Len(sWrittenData), lpcWritten)
        If lReturn = 0 Then Exit Function
        If lpcWritten <> Len(sWrittenData) Then Exit Function
    End If
    lReturn = EndPagePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = EndDocPrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    lReturn = ClosePrinter(EtiqHPrinter)
    If lReturn = 0 Then Exit Function
    
    FR_EtiqClosePrinter = True
Exit Function
TrataErro:
    BG_LogFile_Erros "FR_EtiqClosePrinter: " & Err.Number & " - " & Err.Description, "FR", "FR_EtiqClosePrinter"
    Exit Function
    Resume Next
End Function




' -----------------------------------------------------------------

' IMPRIME FOLHA DE RESUMO ATRAV�S DO CRYSTAL REPORTS

' -----------------------------------------------------------------
Public Sub FR_ImprimeResumoCrystal_HOSP(n_req As String)
    Dim continua As Boolean
    Dim ObsUtente As String
    Dim codProfis As String
    Dim descrProfis As String
    Dim sSql As String
    On Error GoTo TrataErro
    
    BL_ApagaRegistosTabelaTemp ("sl_Cr_fr")
    'brunodsantos 30.11.2015 SCMVC-801 Adicionado: x2.telef_ute, X2.telemovel
    sSql = "SELECT x1.n_Req,x1.cod_local, x2.obs,x2.cod_profis_ute, x3.descr_profis,x2.nome_ute, x2.dt_nasc_ute, "
    sSql = sSql & " x1.dt_chega, x2.utente,x2.n_proc_1, x4.descr_t_sit, x5.descr_proven, x2.seq_utente, x1.inf_complementar, "
    sSql = sSql & " x6.descr_t_urg, x7.descr_sala,x1.dt_conclusao, x8.descr_sexo, x2.telef_ute, X2.telemovel,  "
    sSql = sSql & " x8.nome_med, x9.descr_efr, x1.descr_medico "
    sSql = sSql & "  FROM sl_Requis x1 LEFT OUTER JOIN sl_tbf_t_sit x4 ON x1.t_sit = x4.cod_t_sit "
    sSql = sSql & "  LEFT OUTER JOIN sl_proven x5 ON x1.cod_proven = x5.cod_proven "
    sSql = sSql & "  LEFT OUTER JOIN sl_tbf_t_urg x6 ON x1.t_urg = x6.cod_t_urg "
    sSql = sSql & "  LEFT OUTER JOIN sl_cod_salas x7 ON x1.cod_sala= x7.cod_sala "
    sSql = sSql & "  LEFT OUTER JOIN sl_medicos x8 ON x1.cod_med= x8.cod_med "
    sSql = sSql & "  LEFT OUTER JOIN sl_efr x9 ON x1.cod_efr = x9.cod_efr"
    sSql = sSql & " , sl_identif x2  LEFT OUTER JOIN sl_profis x3 ON x2.cod_profis_ute = x3.cod_profis, sl_tbf_sexo x8 "
    sSql = sSql & " WHERE x1.seq_utente =x2.seq_utente AND x1.n_req = " & n_req
    sSql = sSql & " AND x2.sexo_ute = x8.cod_sexo"
    Dim rsOBS As ADODB.recordset
    Set rsOBS = New ADODB.recordset
    rsOBS.CursorLocation = adUseClient
    rsOBS.CursorType = adOpenStatic
    If gModoDebug = mediSim Then BG_LogFile_Erros rsOBS.Source
    rsOBS.Open sSql, gConexao
    If rsOBS.RecordCount > 0 Then
    
        continua = BL_IniciaReport("FolhaResumo", "Folha de Resumo", crptToPrinter)
        
        If continua = False Then Exit Sub
        'BL_MudaCursorRato mediMP_Espera, Me
        
        FR_ImprimeResumoAnalises_HOSP n_req, BL_HandleNull(rsOBS!cod_local, "")
    
        ObsUtente = BL_HandleNull(rsOBS!obs, "")
        codProfis = BL_HandleNull(rsOBS!cod_profis_ute, "")
        descrProfis = BL_HandleNull(rsOBS!descr_profis, "")
    
        'Imprime o relat�rio no Crystal Reports
        Dim Report As CrystalReport
        Set Report = forms(0).Controls("Report")
        
        Report.SQLQuery = ""
        
        'F�rmulas do Report
        Report.formulas(0) = "NumReq=" & BL_TrataStringParaBD("" & n_req)
        Report.formulas(1) = "NomeUtente=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!nome_ute, ""))
        Report.formulas(2) = "DtNascUtente=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!dt_nasc_ute, ""))
        Report.formulas(4) = "DtChegada=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!dt_chega, ""))
        Report.formulas(6) = "Processo=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!n_proc_1, ""))
        Report.formulas(7) = "DtValidade=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!dt_conclusao, ""))
        Report.formulas(8) = "NSC=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!Utente, ""))
        Report.formulas(9) = "Situacao=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!descr_t_sit, ""))
        Report.formulas(10) = "Servico=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!descr_proven, ""))
        Report.formulas(11) = "SeqUtente=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!seq_utente, ""))
        'tania.oliveira GX-31071 | CHPVVC 05.06.2020
        'Report.formulas(12) = "InfComplementar=" & BL_TrataStringParaBD("" & Replace(BL_HandleNull(rsOBS!inf_complementar, ""), vbCrLf, ""))
        Report.formulas(13) = "Prioridade=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!descr_t_urg, ""))
        Report.formulas(14) = "DescrProfis=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!descr_profis, ""))
        Report.formulas(15) = "Sala=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!descr_sala, ""))
        Report.formulas(16) = "Sexo=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!descr_sexo, ""))
        'brunodsantos 30.11.2015 SCMVC-801
        Report.formulas(17) = "telefone=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!telef_ute, ""))
        Report.formulas(18) = "telemovel=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!telemovel, ""))

        If gListaMedicos = mediSim Then
                Report.formulas(19) = "Medico=" & BL_TrataStringParaBD("" & Trim(BL_HandleNull(rsOBS!nome_med, "")))
            Else
                Report.formulas(19) = "Medico=" & BL_TrataStringParaBD("" & Trim(BL_HandleNull(rsOBS!descr_medico, "")))
        End If
        
        Report.formulas(20) = "Idade=" & BL_TrataStringParaBD("" & BG_CalculaIdade(CDate(BL_HandleNull(rsOBS!dt_nasc_ute, "01-01-1900")), CDate(BL_HandleNull(rsOBS!dt_chega, "01-01-1900"))))
        Report.formulas(21) = "EFR=" & BL_TrataStringParaBD("" & BL_HandleNull(rsOBS!descr_efr, ""))
        '
        
        Report.SQLQuery = "SELECT sl_cr_fr.nome_computador, sl_cr_fr.num_sessao, sl_cr_fr.n_req, sl_cr_fr.cod_ana, sl_cr_fr.descr_ana, sl_cr_fr.ordem, sl_cr_fr.cod_local, sl_cr_fr.cod_prod, sl_cr_fr.descr_prod, sl_cr_fr.cod_especif, sl_cr_fr.obs_especif, sl_cr_fr.cod_informacao, sl_cr_fr.descr_informacao, sl_cr_fr.OBS_UTENTE, sl_cr_fr.N_REQ_ASSOC, sl_cr_fr.ID_GARRAFA,sl_cr_fr.INF_COMPLEMENTAR, sl_cr_fr.credencial"
        Report.SQLQuery = Report.SQLQuery & " FROM sl_cr_fr "
        Report.SQLQuery = Report.SQLQuery & " WHERE SL_CR_FR.N_REQ = " & n_req
        Report.SQLQuery = Report.SQLQuery & " AND SL_CR_FR.NOME_COMPUTADOR = " & BL_TrataStringParaBD(gComputador)
        Report.SQLQuery = Report.SQLQuery & " AND SL_CR_FR.NUM_SESSAO = " & gNumeroSessao
        Report.SQLQuery = Report.SQLQuery & " ORDER BY SL_CR_FR.ORDEM ASC "
        Call BL_ExecutaReport
    End If
    rsOBS.Close
    Set rsOBS = Nothing
    sSql = "DELETE FROM Sl_cr_fr WHERE nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND num_sessao = " & gNumeroSessao
    BG_ExecutaQuery_ADO sSql
Exit Sub
TrataErro:
    BG_LogFile_Erros "FR_ImprimeResumoCrystal: " & Err.Number & " - " & Err.Description, "FR", "FR_ImprimeResumoCrystal"
    Exit Sub
    Resume Next
End Sub

' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARIA AS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub FR_ImprimeResumoAnalises_HOSP(n_req As String, cod_local As String)
    Dim i As Integer
    Dim sSql As String
    Dim rsAnaInfo As New ADODB.recordset
    On Error GoTo TrataErro
    gSQLError = 0
    BL_ApagaRegistosTabelaTemp "sl_cr_fr"
    FR_ImprimeResumoTubos_HOSP n_req
    'BRUNODSANTOS LRV-651 23.10.2018
    Dim iRes As Long
    '
    
    'BRUNODSANTOS 01.12.2015 - Adicionada vari�vel global gInformacaoParaUtente em -> x5.cod_t_informacao = gInformacaoParaUtente
    'NELSONPSILVA 19.07.2019 - Adicionado left join com a sl_credenciais
    sSql = sSql & "SELECT  x1.cod_ana, x1.descr_ana, x1.cod_tubo,  x1.cod_produto, x4.cod_informacao, "
    'sSql = sSql & " x6.cod_especif, x6.obs_especif, x7.ord_marca, x8.n_req "
    'tania.oliveira GX-31071 | CHPVVC 05.06.2020
    sSql = sSql & " x6.cod_especif, x6.obs_especif, x7.ord_marca, x8.n_req, (select inf_complementar from sl_requis where n_req =" & n_req & ") INF_COMPLEMENTAR "
    sSql = sSql & " from slv_analises x1 LEFT OUTER JOIN sl_ana_informacao X4 ON x1.cod_ana = x4.cod_ana and x4.cod_informacao in (Select x5.cod_informacao FROM sl_informacao x5 where x5.cod_t_informacao = " & gInformacaoParaUtente & ") "
    sSql = sSql & " LEFT OUTER JOIN sl_req_prod x6 ON " & n_req & " = x6.n_req AND x1.cod_produto = x6.cod_prod, sl_marcacoes x7 LEFT OUTER JOIN sl_credenciais x8 on x7.n_req = x8.n_req_orig AND x7.cod_agrup = x8.cod_ana "
    sSql = sSql & " Where x1.cod_ana = x7.cod_agrup "
    sSql = sSql & " and x7.n_req =" & n_req
    sSql = sSql & " UNION SELECT  x1.cod_ana, x1.descr_ana, x1.cod_tubo, x1.cod_produto, x4.cod_informacao, "
    'sSql = sSql & " x6.cod_especif, x6.obs_especif, x7.ord_marca, x8.n_req "
    'tania.oliveira GX-31071 | CHPVVC 05.06.2020
    sSql = sSql & " x6.cod_especif, x6.obs_especif, x7.ord_marca, x8.n_req, (select inf_complementar from sl_requis where n_req =" & n_req & ") INF_COMPLEMENTAR "
    sSql = sSql & " from slv_analises x1 LEFT OUTER JOIN sl_ana_informacao X4 ON x1.cod_ana = x4.cod_ana and x4.cod_informacao in (Select x5.cod_informacao FROM sl_informacao x5 where x5.cod_t_informacao = " & gInformacaoParaUtente & ") "
    sSql = sSql & " LEFT OUTER JOIN sl_req_prod x6 ON " & n_req & " = x6.n_req AND x1.cod_produto = x6.cod_prod, sl_realiza x7 LEFT OUTER JOIN sl_credenciais x8 on x7.n_req = x8.n_req_orig AND x7.cod_agrup = x8.cod_ana "
    sSql = sSql & " Where x1.cod_ana = x7.cod_agrup "
    sSql = sSql & " and x7.n_req =" & n_req
    'BRUNODSANTOS LRV-651 23.10.2018
    sSql = sSql & " ORDER BY cod_ana, ord_marca"
    '
    
    rsAnaInfo.CursorLocation = adUseClient
    rsAnaInfo.CursorType = adOpenStatic
    rsAnaInfo.Source = sSql
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaInfo.Open sSql, gConexao
    If rsAnaInfo.RecordCount > 0 Then
        While Not rsAnaInfo.EOF
            
            'BRUNODSANTOS LRV-651 23.10.2018
            'Faz update caso j� exista cod_ana para manter a �ltima ordem de marca��o
            sSql = "UPDATE sl_cr_fr SET ordem = " & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!Ord_Marca, "0"))
            sSql = sSql & " WHERE cod_ana = " & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!cod_ana, "")) & " AND "
            sSql = sSql & " nome_computador = " & BL_TrataStringParaBD(gComputador) & " AND "
            sSql = sSql & " num_sessao = " & gNumeroSessao
            
            iRes = BG_ExecutaQuery_ADO(sSql)
            
            If iRes = 0 Then
                'NELSONPSILVA 19.07.2019 - Adicionado nova coluna -> credencial
                sSql = "INSERT INTO sl_cr_fr (nome_computador,num_sessao, n_req,cod_ana,descr_ana,ordem,"
                sSql = sSql & " cod_local, cod_prod, descr_prod, cod_especif, obs_especif, cod_tubo, descr_tubo, "
                'sSql = sSql & " cod_informacao, descr_informacao, credencial) VALUES("
                'tania.oliveira GX-31071 | CHPVVC 05.06.2020
                sSql = sSql & " cod_informacao, descr_informacao, credencial, INF_COMPLEMENTAR) VALUES("
                sSql = sSql & BL_TrataStringParaBD(gComputador) & ", "
                sSql = sSql & gNumeroSessao & ", "
                sSql = sSql & gRequisicaoActiva & ", "
                sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!cod_ana, "")) & ", "
                sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!descr_ana, "")) & "," & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!Ord_Marca, "0")) & ", "
                sSql = sSql & cod_local & ", "
                sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!cod_produto, "")) & ", "
                sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_PRODUTO", "DESCR_PRODUTO", "COD_PRODUTO", BL_HandleNull(rsAnaInfo!cod_produto, ""), "V")) & ", "
                sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!cod_Especif, "")) & ", "
                sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!obs_especif, "")) & ", "
                sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!cod_tubo, "")) & ", "
                sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_TUBO", "DESCR_TUBO", "COD_TUBO", BL_HandleNull(rsAnaInfo!cod_tubo, ""), "V")) & ", "
                sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!cod_informacao, "")) & ", "
                sSql = sSql & BL_TrataStringParaBD(BL_SelCodigo("SL_INFORMACAO", "INFORMACAO", "COD_INFORMACAO", BL_HandleNull(rsAnaInfo!cod_informacao, ""), "V")) & ", "
                'sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!n_req, "")) & ") "
                'tania.oliveira GX-31071 | CHPVVC 05.06.2020
                sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!n_req, "")) & ", "
                sSql = sSql & BL_TrataStringParaBD(BL_HandleNull(rsAnaInfo!inf_complementar, "")) & ")"
                
                BG_ExecutaQuery_ADO sSql
            End If
            
            
            rsAnaInfo.MoveNext
        Wend
    End If
    rsAnaInfo.Close
Exit Sub
TrataErro:
    BG_LogFile_Erros "FR_ImprimeResumoAnalises: " & Err.Number & " - " & Err.Description, "FR", "FR_ImprimeResumoAnalises"
    Exit Sub
    Resume Next
End Sub


' -----------------------------------------------------------------

' INSERE NA TABELA TEMPORARAIA OS TUBOS DAS AN�LISES REGISTADAS

' -----------------------------------------------------------------
Private Sub FR_ImprimeResumoTubos_HOSP(n_req As String)
    Dim i As Integer
    Dim sSql As String
    Dim rsAnaInfo As New ADODB.recordset
    On Error GoTo TrataErro
    
    BL_ApagaRegistosTabelaTemp ("sl_Cr_fr_tubo")
    sSql = "SELECT x1.cod_tubo, x2.descr_tubo FROM sl_req_tubo x1, sl_tubo x2 WHERE  x1.n_Req = " & n_req & " AND x1.cod_tubo = x2.cod_tubo "
    sSql = sSql & " ORDER BY x1.seq_req_tubo "
    rsAnaInfo.CursorLocation = adUseClient
    rsAnaInfo.CursorType = adOpenStatic
    rsAnaInfo.Source = sSql
    If gModoDebug = mediSim Then BG_LogFile_Erros sSql
    rsAnaInfo.Open sSql, gConexao
    If rsAnaInfo.RecordCount > 0 Then
        While Not rsAnaInfo.EOF
            gSQLError = 0
            sSql = "INSERT INTO sl_cr_fr_tubo (nome_computador, num_sessao, n_req,cod_tubo, descr_tubo) VALUES("
            sSql = sSql & BL_TrataStringParaBD(BG_SYS_GetComputerName) & ", "
            sSql = sSql & gNumeroSessao & ", "
            sSql = sSql & gRequisicaoActiva & ", "
            sSql = sSql & BL_TrataStringParaBD(rsAnaInfo!cod_tubo) & ","
            sSql = sSql & BL_TrataStringParaBD(rsAnaInfo!descR_tubo) & ")"
            BG_ExecutaQuery_ADO sSql
            
            rsAnaInfo.MoveNext
        Wend
    End If
    rsAnaInfo.Close
    Set rsAnaInfo = Nothing
Exit Sub
TrataErro:
    BG_LogFile_Erros "FR_ImprimeResumoTubos_HOSP: " & Err.Number & " - " & Err.Description, "FR", "FR_ImprimeResumoTubos_HOSP"
    Exit Sub
    Resume Next
End Sub

'edgar.parada LJMANSO-252 29.03.2018 Procedimento para ir buscar o n_req_assoc e o obs
Private Sub devolve_obs_req_assoc(ByVal n_req As String)

Dim sSql As String
Dim rs As New ADODB.recordset

On Error GoTo TrataErro

'sSql = "SELECT x1.obs, x2.n_req_assoc FROM sl_identif x1, sl_requis x2 WHERE x2.n_req=" & BL_TrataStringParaBD(n_req) & " AND x1.seq_utente = x2.seq_utente"
sSql = "SELECT x1.descr_obs, x2.n_req_assoc FROM sl_obs_ana_req x1 full outer join sl_requis x2 on ( x1.seq_utente = x2.seq_utente) WHERE x2.n_req=" & BL_TrataStringParaBD(n_req)
'
rs.CursorLocation = adUseClient
rs.CursorType = adOpenStatic

If gModoDebug = mediSim Then BG_LogFile_Erros sSql
rs.Open sSql, gConexao

If rs.RecordCount > 0 Then
   Utente.n_Req_assoc = BL_HandleNull(rs.Fields("n_req_assoc").value, "")
   Utente.ObsUtente = BL_HandleNull(rs.Fields("descr_obs").value, "")
End If

rs.Close
Set rs = Nothing
 
Exit Sub
TrataErro:
   BG_LogFile_Erros "devolve_obs_req_assoc: " & Err.Number & " - " & Err.Description, "", "devolve_obs_req_assoc"
   Exit Sub
   Resume Next
End Sub
'


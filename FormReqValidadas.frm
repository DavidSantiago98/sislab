VERSION 5.00
Begin VB.Form FormReqValidadas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Requisi��es Validadas"
   ClientHeight    =   4440
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7755
   Icon            =   "FormReqValidadas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4440
   ScaleWidth      =   7755
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox EcGrAnalises 
      Height          =   285
      Left            =   1800
      TabIndex        =   12
      Top             =   1320
      Width           =   855
   End
   Begin VB.TextBox EcGrTrabalho 
      Height          =   285
      Left            =   1800
      TabIndex        =   11
      Top             =   1800
      Width           =   855
   End
   Begin VB.ComboBox CbSituacao 
      Height          =   315
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   2280
      Width           =   1455
   End
   Begin VB.TextBox EcDescrGrAnalises 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   2640
      Locked          =   -1  'True
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   1320
      Width           =   3375
   End
   Begin VB.TextBox EcDescrGrTrabalho 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   2640
      Locked          =   -1  'True
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1800
      Width           =   3375
   End
   Begin VB.TextBox EcDtIni 
      Height          =   285
      Left            =   1800
      TabIndex        =   7
      Top             =   960
      Width           =   1335
   End
   Begin VB.TextBox EcDtFim 
      Height          =   285
      Left            =   4320
      TabIndex        =   6
      Top             =   960
      Width           =   1335
   End
   Begin VB.CommandButton BtPesquisaRapidaGrTrab 
      Height          =   375
      Left            =   6000
      Picture         =   "FormReqValidadas.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   1800
      Width           =   375
   End
   Begin VB.CommandButton BtPesquisaRapidaGrAnalises 
      Height          =   375
      Left            =   6000
      Picture         =   "FormReqValidadas.frx":0596
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Pesquisa R�pida de Proveni�ncia "
      Top             =   1320
      Width           =   375
   End
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   3735
      Begin VB.OptionButton OptNao 
         Caption         =   "N�o"
         Height          =   255
         Left            =   2760
         TabIndex        =   2
         Top             =   240
         Value           =   -1  'True
         Width           =   615
      End
      Begin VB.OptionButton OptSim 
         Caption         =   "Sim"
         Height          =   255
         Left            =   1920
         TabIndex        =   1
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label4 
         Caption         =   "Discriminar An�lises ?"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Grupo de An�lises"
      Height          =   255
      Left            =   240
      TabIndex        =   17
      Top             =   1320
      Width           =   1455
   End
   Begin VB.Label Label5 
      Caption         =   "Grupo de Trabalho"
      Height          =   255
      Left            =   240
      TabIndex        =   16
      Top             =   1800
      Width           =   1455
   End
   Begin VB.Label Label6 
      Caption         =   "Situa��o"
      Height          =   255
      Left            =   240
      TabIndex        =   15
      Top             =   2280
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "Data Inicial"
      Height          =   255
      Left            =   240
      TabIndex        =   14
      Top             =   960
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "Data Final"
      Height          =   255
      Left            =   3360
      TabIndex        =   13
      Top             =   960
      Width           =   855
   End
End
Attribute VB_Name = "FormReqValidadas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Actualiza��o : 25/02/2002
' T�cnico Paulo Costa

' Vari�veis Globais para este Form.

Dim estado As Integer ' 1 - quando entra no Form; 2 - quando est� a procurar alguma coisa.

Dim CampoActivo As Object
Dim CampoDeFocus As Object

Private Sub CriaTabReqPendRes()
    
    Dim TmpReqPend(1 To 4) As DefTable

    TmpReqPend(1).NomeCampo = "N_req"
    TmpReqPend(1).tipo = "INTEGER"
    
    TmpReqPend(2).NomeCampo = "Cod_Agrup"
    TmpReqPend(2).tipo = "STRING"
    TmpReqPend(2).tamanho = 10
    
    TmpReqPend(3).NomeCampo = "Descr_Analise"
    TmpReqPend(3).tipo = "STRING"
    TmpReqPend(3).tamanho = 80

    TmpReqPend(4).NomeCampo = "Id_Empr"
    TmpReqPend(4).tipo = "STRING"
    TmpReqPend(4).tamanho = 8
    
    Call BL_CriaTabela("SL_CR_RPDR" & gNumeroSessao, TmpReqPend)
    
End Sub

Private Sub CriaTabReqPendVal()
    
    Dim TmpReqPend(1 To 2) As DefTable

    TmpReqPend(1).NomeCampo = "StrReq"
    TmpReqPend(1).tipo = "STRING"
    TmpReqPend(1).tamanho = 100

    TmpReqPend(2).NomeCampo = "Id_Empr"
    TmpReqPend(2).tipo = "STRING"
    TmpReqPend(2).tamanho = 8
    
    Call BL_CriaTabela("SL_CR_RPDV" & gNumeroSessao, TmpReqPend)
    
End Sub

Public Sub FuncaoImprimir()
    
    'Tabela Marca��es ou Realiza��es
    Dim RegReq As ADODB.recordset
    Dim StringReq As String
    
    'RecordSet e Comando com a descri��o da an�lise
    Dim RegCmd As ADODB.recordset
    Dim CmdDescrAnaS As ADODB.Command
    
    Dim SQLQuery As String
    Dim continua As Boolean
    Dim i As Integer
    Dim ReqAnt As String
    Dim cont As Integer
    Dim s As String
    
    '1.Verifica se a Form Preview j� estava aberta!!
    'If BL_PreviewAberto("Requisi��es Validadas") = True Then Exit Sub
    
    '2.Verifica se os campos obrigat�rios est�o nulos
    If EcDtIni.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Inicial da Requisi��o.", vbOKOnly + vbInformation, App.ProductName)
        EcDtIni.SetFocus
        Exit Sub
    End If

    If EcDtFim.Text = "" Then
        Call BG_Mensagem(mediMsgBox, "Indique a Data Final da Requisi��o.", vbOKOnly + vbInformation, App.ProductName)
        EcDtFim.SetFocus
        Exit Sub
    End If
    
    '3.C�lculo das Requisi��es Pendentes
    Set RegReq = New ADODB.recordset
    RegReq.CursorLocation = adUseServer
    RegReq.CursorType = adOpenStatic
    RegReq.ActiveConnection = gConexao
    
    
        'Grupo de Trabalho preenchido?
        If (Trim(EcGrTrabalho.Text) <> "") Then
            'Tipo de Situa��o preenchido?
            If (CbSituacao.ListIndex <> CbSituacao.ListCount - 1) And (CbSituacao.ListIndex <> -1) Then
                SQLQuery = " SELECT sl_realiza.N_req,sl_realiza.Cod_ana_s " & _
                           " FROM sl_requis,sl_realiza,sl_ana_trab " & _
                           " WHERE sl_realiza.dt_val between " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text) & _
                           " AND sl_realiza.flg_estado in ('3', '4') " & _
                           " AND sl_realiza.Cod_ana_s=sl_ana_trab.cod_analise " & _
                           " AND sl_ana_trab.cod_gr_trab=" & BL_TrataStringParaBD(Trim(EcGrTrabalho.Text)) & _
                           " AND sl_requis.n_req=sl_marcacoes.n_req " & _
                           " AND sl_requis.T_sit=" & CbSituacao.ItemData(CbSituacao.ListIndex) & _
                           " ORDER BY sl_realiza.N_req"
            Else
                SQLQuery = " SELECT sl_realiza.N_req,sl_realiza.Cod_ana_s " & _
                           " FROM sl_realiza,sl_ana_trab " & _
                           " WHERE sl_realiza.dt_val between " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text) & _
                           " AND sl_realiza.flg_estado in ('3', '4')  " & _
                           " AND sl_realiza.Cod_ana_s=sl_ana_trab.cod_analise " & _
                           " AND sl_ana_trab.cod_gr_trab=" & BL_TrataStringParaBD(Trim(EcGrTrabalho.Text)) & _
                           " ORDER BY sl_realiza.N_req"
            End If
        Else
            'Tipo de Situa��o preenchido?
            If (CbSituacao.ListIndex <> CbSituacao.ListCount - 1) And (CbSituacao.ListIndex <> -1) Then
                SQLQuery = " SELECT sl_realiza.N_req,sl_realiza.Cod_ana_s " & _
                           " FROM sl_requis,sl_realiza " & _
                           " WHERE sl_realiza.dt_val between " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text) & _
                           " AND sl_realiza.flg_estado in ('3', '4')" & _
                           " AND sl_requis.n_req=sl_marcacoes.n_req " & _
                           " AND sl_requis.T_sit=" & CbSituacao.ItemData(CbSituacao.ListIndex) & _
                           " ORDER BY sl_realiza.N_req"
            Else
                SQLQuery = " SELECT sl_realiza.N_req,sl_realiza.Cod_ana_s " & _
                           " FROM sl_realiza " & _
                           " WHERE sl_realiza.dt_val between " & BL_TrataDataParaBD(EcDtIni.Text) & " AND " & BL_TrataDataParaBD(EcDtFim.Text) & _
                           " AND sl_realiza.flg_estado in ('3', '4')" & _
                           " ORDER BY sl_realiza.N_req"
            End If
        End If
    
    
    RegReq.Source = SQLQuery
    RegReq.Open
    If RegReq.RecordCount <= 0 Then
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo!", vbOKOnly + vbInformation, App.ProductName
        RegReq.Close
        Set RegReq = Nothing
        Exit Sub
    End If
        
    '*****************************************************
        
    '4.Inicia o processo de impress�o do relat�rio

    'Printer Common Dialog
    If gImprimirDestino = 1 Then
        If OptSim.value = True Then
            continua = BL_IniciaReport("MapaRequisValidadas", "Requisi��es Validadas", crptToPrinter)
        Else
            continua = BL_IniciaReport("MapaRequisValidadasStr", "Requisi��es Validadas", crptToPrinter)
        End If
    Else
        If OptSim.value = True Then
            continua = BL_IniciaReport("MapaRequisValidadas", "Requisi��es Validadas", crptToWindow)
        Else
            continua = BL_IniciaReport("MapaRequisValidadasStr", "Requisi��es Validadas", crptToWindow)
        End If
    End If
    If continua = False Then Exit Sub
        
        
    '5.Inicializa��o dos comandos para ir buscar a descri��o da an�lise

    'An�lises Simples
    Set CmdDescrAnaS = New ADODB.Command
    CmdDescrAnaS.ActiveConnection = gConexao
    CmdDescrAnaS.CommandType = adCmdText
    SQLQuery = " SELECT Descr_ana_s " & _
               " FROM sl_ana_s WHERE cod_ana_s=?"
    'Grupo de An�lises preenchido?
    If Trim(EcGrAnalises.Text) <> "" Then
        SQLQuery = SQLQuery & " AND Gr_ana=" & BL_TrataStringParaBD(Trim(EcGrAnalises.Text))
    End If
    CmdDescrAnaS.CommandText = SQLQuery
    CmdDescrAnaS.Prepared = True
    CmdDescrAnaS.Parameters.Append CmdDescrAnaS.CreateParameter("COD_ANA_S", adVarChar, adParamInput, 10)
    
    'S� cria a tabela tempor�ria para a inser��o dos registos discriminados

    If OptSim.value = True Then
        Call CriaTabReqPendRes
    Else
        Call CriaTabReqPendVal
    End If
    
    'Percorre as Requisi��es
    StringReq = ""
    cont = 0
    continua = False
    ReqAnt = ""
    For i = 1 To RegReq.RecordCount
        'Obt�m a descri��o da an�lise=>DEVOLVE APENAS UM REGISTO!!
        s = "" & RegReq!cod_ana_s
        CmdDescrAnaS.Parameters("COD_ANA_S").value = s
        Set RegCmd = CmdDescrAnaS.Execute
        'Insere na Tabela tempor�ria?
        If Not (RegCmd.BOF And RegCmd.EOF) Then
            'Se tem registos ent�o mostra o Report!
            continua = True
            
            'Report discriminado?
            If OptSim.value = True Then
                SQLQuery = " INSERT INTO SL_CR_RPDR" & gNumeroSessao & " (N_Req,Cod_agrup,Descr_analise,Id_Empr) " & _
                            " VALUES (" & RegReq.Fields!n_req & "," & BL_TrataStringParaBD("" & RegReq!cod_ana_s) & "," & BL_TrataStringParaBD("" & RegCmd!descr_ana_s) & ",Null)"
                'Executa a instru��o Sql
                BG_ExecutaQuery_ADO SQLQuery
            Else
                'Report n�o discriminado
                s = RegReq.Fields("N_req").value
                If s <> ReqAnt Then
                    cont = cont + 1
                    ReqAnt = s
                    StringReq = StringReq & Space(2) & s & Space(8 - Len(s))
                
                    '10 Requisi��es por linha
                    If cont = 10 Then
                        SQLQuery = "INSERT INTO SL_CR_RPDV" & gNumeroSessao & " (StrReq,Id_Empr) " & _
                                   " VALUES (" & BL_TrataStringParaBD(LTrim(StringReq)) & ",Null)"
                        'Executa a instru��o Sql
                        BG_ExecutaQuery_ADO SQLQuery
                        'Inicializa novamente a String das Requisi��es e o contador
                        StringReq = ""
                        cont = 0
                    End If
                End If
            End If
        End If
        RegCmd.Close
        Set RegCmd = Nothing
        
        RegReq.MoveNext
    Next i
    
    'Se o n� de requisi��es � inferior a 10 ent�o � necess�rio inserir a string
    If OptSim.value = False And cont > 0 And cont < 10 Then
        SQLQuery = " INSERT INTO SL_CR_RPDV" & gNumeroSessao & " (StrReq,Id_Empr) " & _
                   " VALUES (" & BL_TrataStringParaBD(LTrim(StringReq)) & ",Null)"
        'Executa a instru��o Sql
        BG_ExecutaQuery_ADO SQLQuery
    End If
    
    
    If continua = False Then
        BG_Mensagem mediMsgBox, "N�o foi seleccionado nenhum registo!", vbOKOnly, "ATEN��O"
        RegReq.Close
        Set RegReq = Nothing
        Set CmdDescrAnaS = Nothing
        If OptSim.value = True Then
            Call BL_RemoveTabela("SL_CR_RPDR" & gNumeroSessao)
        Else
            Call BL_RemoveTabela("SL_CR_RPDV" & gNumeroSessao)
        End If
        Exit Sub
    End If
           
   'Imprime o relat�rio no Crystal Reports
    Dim Report As CrystalReport
    Set Report = forms(0).Controls("Report")
    Report.formulas(0) = "DataInicio=" & BL_TrataStringParaBD(EcDtIni.Text)
    Report.formulas(1) = "DataFim=" & BL_TrataStringParaBD(EcDtFim.Text)
    
   ' Report.Formulas(2) = "Tipo='VALIDADAS'"
     
    If OptSim.value = True Then
        Report.SQLQuery = " SELECT SL_CR_RPDR.N_REQ,SL_CR_RPDR.DESCR_ANALISE " & _
                          " FROM SL_CR_RPDR" & gNumeroSessao & " SL_CR_RPDR"
                          
    Else
        Report.SQLQuery = " SELECT SL_CR_RPDV.STRREQ" & _
                          " FROM SL_CR_RPDV" & gNumeroSessao & " SL_CR_RPDV "
            
    End If
    Call BL_ExecutaReport
    RegReq.Close
    Set RegReq = Nothing
    Set CmdDescrAnaS = Nothing
    
    If OptSim.value = True Then
        Call BL_RemoveTabela("SL_CR_RPDR" & gNumeroSessao)
    Else
        Call BL_RemoveTabela("SL_CR_RPDV" & gNumeroSessao)
    End If
                
End Sub

Private Sub BtPesquisaRapidaGrAnalises_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_ana"
    CamposEcran(1) = "cod_gr_ana"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_ana"
    CamposEcran(2) = "descr_gr_ana"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_ana"
    CampoPesquisa1 = "descr_gr_ana"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupos de An�lises")
    
    mensagem = "N�o foi encontrada nenhum Grupo de An�lise."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcGrAnalises.Text = resultados(1)
            EcDescrGrAnalises.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub

Private Sub BtPesquisaRapidaGrAnalises_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub BtPesquisaRapidaGrTrab_Click()
    
    'Campos do RecordSet
    Dim ChavesPesq(1 To 2) As String
    
    'Campo do Crit�rio na Query do RecordSet
    Dim CampoPesquisa1 As String
    
    'Campos do RecordSet a Visualizar no ecran
    Dim CamposEcran(1 To 2) As String
    
    'Contru��o da Query
    Dim ClausulaWhere As String
    Dim ClausulaFrom As String
    
    'Classe
    Dim CamposRetorno As New ClassPesqResultados
    
    'Tamanho dos campos Ecran da List
    Dim Tamanhos(1 To 2) As Long
    
    'Cabe�alhos
    Dim Headers(1 To 2) As String
    
    'Mensagem do resultado da pesquisa
    Dim mensagem As String
    
    Dim PesqRapida As Boolean
    Dim CancelouPesquisa As Boolean
    
    Dim resultados(1 To 2) As Variant
    
    'Defini��o dos campos a retornar
    ChavesPesq(1) = "cod_gr_trab"
    CamposEcran(1) = "cod_gr_trab"
    Tamanhos(1) = 2000
    
    ChavesPesq(2) = "descr_gr_trab"
    CamposEcran(2) = "descr_gr_trab"
    Tamanhos(2) = 3000
    
    'Cabe�alhos
    Headers(1) = "C�digo"
    Headers(2) = "Descri��o"
    
    'N� de Campos a pesquisar na Classe=>Resultaods
    CamposRetorno.InicializaResultados 2

    'Query
    ClausulaFrom = "sl_gr_trab"
    CampoPesquisa1 = "descr_gr_trab"
    
    PesqRapida = FormPesqRapidaAvancada.InicializaFormPesqAvancada(gConexao, ChavesPesq, CamposEcran, CamposRetorno, Tamanhos, Headers, ClausulaWhere, ClausulaFrom, "", CampoPesquisa1, "", " Pesquisar Grupos de Trabalho")
    
    mensagem = "N�o foi encontrada nenhum Grupo de Trabalho."
    
    If PesqRapida = True Then
        FormPesqRapidaAvancada.Show vbModal
        CamposRetorno.RetornaResultados resultados, CancelouPesquisa
        If Not CancelouPesquisa Then
            EcGrTrabalho.Text = resultados(1)
            EcDescrGrTrabalho.Text = resultados(2)
        End If
    Else
        BG_Mensagem mediMsgBox, mensagem, vbExclamation, "..."
    End If
    
End Sub

Private Sub BtPesquisaRapidaGrTrab_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbSituacao_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub CbSituacao_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 46 And Shift = 0 Then CbSituacao.ListIndex = -1

End Sub

Private Sub EcDescrGrAnalises_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDescrGrTrabalho_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcDtFim_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
   If Trim(EcDtFim.Text) = "" Then
        EcDtFim.Text = Bg_DaData_ADO
        Sendkeys ("{HOME}+{END}")
    End If
        
End Sub

Private Sub EcDtFim_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtFim)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub EcDtIni_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
   If Trim(EcDtIni.Text) = "" Then
        EcDtIni.Text = Bg_DaData_ADO
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub EcDtini_Validate(Cancel As Boolean)
    
    Cancel = Not BG_ValidaTipoCampo_ADO(Me, EcDtIni)
    If Cancel Then
        Sendkeys ("{HOME}+{END}")
    End If
    
End Sub

Private Sub EcGrAnalises_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcGrAnalises_Validate(Cancel As Boolean)
    
    Dim RsDescrGrAnalises As ADODB.recordset
    
    If Trim(EcGrAnalises.Text) <> "" Then
        Set RsDescrGrAnalises = New ADODB.recordset
        
        With RsDescrGrAnalises
            .Source = "SELECT descr_gr_ana FROM sl_gr_ana WHERE cod_gr_ana= " & BL_TrataStringParaBD(EcGrAnalises.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrAnalises.RecordCount > 0 Then
            EcDescrGrAnalises.Text = RsDescrGrAnalises!descr_gr_ana
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
        Else
            RsDescrGrAnalises.Close
            Set RsDescrGrAnalises = Nothing
            EcDescrGrAnalises.Text = ""
            BG_Mensagem mediMsgBox, "O Grupo de An�lise indicado n�o existe!", vbOKOnly + vbExclamation, "Aten��o!"
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrGrAnalises.Text = ""
    End If
    
End Sub

Private Sub EcGrTrabalho_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub EcGrTrabalho_Validate(Cancel As Boolean)
    
    Dim RsDescrGrTrab As ADODB.recordset
    
    If Trim(EcGrTrabalho.Text) <> "" Then
        Set RsDescrGrTrab = New ADODB.recordset
        
        With RsDescrGrTrab
            .Source = "SELECT descr_gr_trab FROM sl_gr_trab WHERE cod_gr_trab= " & BL_TrataStringParaBD(EcGrTrabalho.Text)
            .CursorLocation = adUseServer
            .CursorType = adOpenStatic
            .ActiveConnection = gConexao
            .Open
        End With
    
        If RsDescrGrTrab.RecordCount > 0 Then
            EcDescrGrTrabalho.Text = RsDescrGrTrab!descr_gr_trab
            RsDescrGrTrab.Close
            Set RsDescrGrTrab = Nothing
        Else
            RsDescrGrTrab.Close
            Set RsDescrGrTrab = Nothing
            EcDescrGrTrabalho.Text = ""
            BG_Mensagem mediMsgBox, "O Grupo de Trabalho indicado n�o existe!", vbOKOnly + vbExclamation, "Aten��o!"
            Cancel = True
            Sendkeys ("{HOME}+{END}")
        End If
    Else
        EcDescrGrTrabalho.Text = ""
    End If
    
End Sub

Private Sub Form_Activate()
    
    EventoActivate
    
End Sub

Public Function Funcao_DataActual()
    
    Select Case CampoActivo.Name
        Case "EcDtIni", "EcDtFim"
            CampoActivo.Text = Bg_DaData_ADO
    End Select

End Function

Private Sub Form_Load()
    
    EventoLoad
    
End Sub

Private Sub EventoActivate()

    BG_StackJanelas_Actualiza Me
    Set gFormActivo = Me
    'CampoDeFocus.SetFocus

    BL_ToolbarEstadoN estado
    BL_Toolbar_BotaoEstado "Limpar", "Activo"
    BL_Toolbar_BotaoEstado "DataActual", "Activo"
    BL_Toolbar_BotaoEstado "ImprimirVerAntes", "Activo"
    BL_Toolbar_BotaoEstado "Imprimir", "Activo"
    
    Me.MousePointer = vbArrow
    MDIFormInicio.MousePointer = vbArrow
    
    EcDtIni.SetFocus
    Set CampoActivo = EcDtIni
    
End Sub

Private Sub EventoLoad()
  DIC_Inicializacao_Idioma Me, gIdioma

    BL_InicioProcessamento Me, "Inicializar ecran."
    Inicializacoes

    DefTipoCampos
    PreencheValoresDefeito
    BG_ParametrizaPermissoes_ADO Me.Name
    
    estado = 0
    BG_StackJanelas_Push Me
    BL_FimProcessamento Me
    
End Sub

Private Sub DefTipoCampos()

    'Tipo Data
    EcDtIni.Tag = adDate
    EcDtFim.Tag = adDate
    EcDtIni.MaxLength = 10
    EcDtFim.MaxLength = 10
    
    'Tipo VarChar
    
    EcGrTrabalho.Tag = adVarChar
    EcGrAnalises.Tag = adVarChar
    EcGrTrabalho.MaxLength = 5
    EcGrAnalises.MaxLength = 5
    
    BG_PreencheComboBD_ADO "sl_tbf_t_sit", "cod_t_sit", "descr_t_sit", CbSituacao
    CbSituacao.AddItem ""
    
End Sub

Private Sub EventoUnload()
    
    MDIFormInicio.Tag = ""
    BG_StackJanelas_Pop
    Set gFormActivo = MDIFormInicio
    BL_ToolbarEstadoN 0
    
    Set FormReqValidadas = Nothing
    
    Call BL_FechaPreview("Requisi��es Pendentes")
     
End Sub

Public Sub FuncaoLimpar()
    
    Me.SetFocus
    EcDtIni.SetFocus
    
    
    EcDtFim.Text = ""
    EcDtIni.Text = ""
    OptNao.value = True
    EcGrAnalises.Text = ""
    EcDescrGrAnalises.Text = ""
    EcGrTrabalho.Text = ""
    EcDescrGrTrabalho.Text = ""
    CbSituacao.ListIndex = mediComboValorNull
        
End Sub

Private Sub Inicializacoes()
    
    Me.caption = " Requisi��es Validadas"
    
    Me.left = 540
    Me.top = 450
    Me.Width = 7000
    Me.Height = 3120
   
    Set CampoDeFocus = EcDtIni
    
    EcDtIni.Tag = adDBTimeStamp
    EcDtFim.Tag = adDBTimeStamp
   
End Sub

Private Sub PreencheValoresDefeito()
       
    EcDtIni.Text = Bg_DaData_ADO
    EcDtFim.Text = Bg_DaData_ADO
    OptNao.value = True
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    EventoUnload
    
End Sub

Private Sub OptNao_Click()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub

Private Sub OptSim_GotFocus()
    
    Set CampoActivo = Me.ActiveControl
    
End Sub




